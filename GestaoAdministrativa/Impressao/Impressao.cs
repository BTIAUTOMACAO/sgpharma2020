﻿using iTextSharp.text;
using iTextSharp.text.pdf;

using System;
using System.IO;
using System.Runtime.InteropServices;
using static GestaoAdministrativa.Classes.Imprimir;

namespace GestaoAdministrativa.Impressao
{
    class Impressao
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] Classes.Imprimir.DOCINFO docInfo);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);


        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            // Open the file.
            FileStream fs = new FileStream(szFileName, FileMode.Open);
            // Create a BinaryReader on the file.
            BinaryReader br = new BinaryReader(fs);
            // Dim an array of bytes big enough to hold the file's contents.
            Byte[] bytes = new Byte[fs.Length];
            bool bSuccess = false;
            // Your unmanaged pointer.
            IntPtr pUnmanagedBytes = new IntPtr(0);
            int nLength;

            nLength = Convert.ToInt32(fs.Length);
            // Read the contents of the file into the array.
            bytes = br.ReadBytes(nLength);
            // Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
            // Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
            // Send the unmanaged bytes to the printer.
            bSuccess = ImpressaoComprovante(szPrinterName, pUnmanagedBytes, nLength);
            // Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes);
            return bSuccess;
        }

        //public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        //{
        //    Int32 dwError = 0, dwWritten = 0;
        //    IntPtr hPrinter = new IntPtr(0);
        //    DOCINFOA di = new DOCINFOA();
        //    bool bSuccess = false; // Assume failure unless you specifically succeed.

        //    di.pDocName = "i9 RAW Document";
        //    di.pDataType = "RAW";
        //    // Open the printer.
        //    if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
        //    {
        //        // Start a document.
        //        if (StartDocPrinter(hPrinter, 1, di))
        //        {
        //            // Start a page.
        //            if (StartPagePrinter(hPrinter))
        //            {
        //                // Write your bytes.
        //                bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
        //                EndPagePrinter(hPrinter);
        //            }
        //            EndDocPrinter(hPrinter);
        //        }
        //        ClosePrinter(hPrinter);
        //    }
        //    // If you did not succeed, GetLastError may give more information
        //    // about why not.
        //    if (bSuccess == false)
        //    {
        //        dwError = Marshal.GetLastWin32Error();
        //    }
        //    return bSuccess;
        //}

        public static bool ImpressaoComprovante(string NomeImpressora, IntPtr pBytes, Int32 dwCount)
        {
            int NumError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFO docInfo = new DOCINFO();
            bool bSuccesso = false;

            docInfo.pDocName = "Comprovante";
            docInfo.pOutputFile = "";


            //ABRE IMPRESSORA//
            if (OpenPrinter(NomeImpressora.Normalize(), out hPrinter, IntPtr.Zero))
            {
                //INICIALIZA DOCUMENTO//
                if (StartDocPrinter(hPrinter, 1, docInfo))
                {
                    if (StartPagePrinter(hPrinter))
                    {
                        bSuccesso = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }


            //CASO TENHA ERRO NA IMPRESSAO//
            if (bSuccesso == false)
            {
                NumError = Marshal.GetLastWin32Error();
                //ERRO CASO NOME DA IMPRESSORA NÃO EXISTA//
                if (NumError == 1801)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
            return bSuccesso;
        }
        
        public static bool Imprimir(string NomeImpressora, string Comprovante)
        {
            IntPtr pBytes;
            Int32 dwCount;

            dwCount = Comprovante.Length;
            pBytes = Marshal.StringToCoTaskMemAnsi(Comprovante);
            ImpressaoComprovante(NomeImpressora, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }

        public static bool ImpressaoCodBarra(String texto, String impressora)
        {
            try
            {
                EscPos esc = new EscPos();
                esc.barcode_height(impressora, 10);
                esc.barcode_width(impressora, 3);
                esc.barcodeHRI_chars(impressora, 1);
                esc.barcodeHRIPostion(impressora,0);
                esc.printBarcodeB(impressora, "{A" + texto, 73);
                esc.lineFeed(impressora, 1);
             
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Image GetBarcode128(PdfContentByte pdfContentByte, string code, bool extended, int codeType)
        {
            Barcode128 code128 = new Barcode128 { Code = code, Extended = extended, CodeType = codeType };
            return code128.CreateImageWithBarcode(pdfContentByte, null, null);
        }

        public static bool ImpressaoQRCode(String texto, String impressora)
        {
            try
            {
                EscPos esc = new GestaoAdministrativa.Impressao.EscPos();
            
                esc.printQrcode(texto, impressora);
                esc.lineFeed(impressora, 3);
                esc.CutPaper(impressora);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Guilhotina(String impressora)
        {
            try
            {
                EscPos esc = new GestaoAdministrativa.Impressao.EscPos();
                if (esc.lineFeed(impressora, 1).Equals(false))
                    return false;

                if (esc.CutPaper(impressora).Equals(false))
                    return false;

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
