﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Impressao
{
    class InterfaceEpsonNF
    {
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int IniciaPorta(string pszPorta);
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int FechaPorta();
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int ConfiguraCodigoBarras(int dwAltura, int dwLargura, int dwHRI, int dwFonte, int dwMargem);
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int ImprimeCodigoBarrasCODE128(string pszCodigo);
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int ImprimeCodigoQRCODE(int dwRestauracao, int dwModulo, int dwTipo, int dwVersao, int dwModo, string pszCodigo);
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int AcionaGuilhotina(int dwTipoCorte);
        [DllImport("InterfaceEpsonNF.dll")]
        public static extern int ImprimeTexto(string pszTexto);
    }
}
