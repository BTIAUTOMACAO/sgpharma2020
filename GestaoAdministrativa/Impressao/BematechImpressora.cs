﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Impressao
{

    class BematechImpressora
    {
        [DllImport("MP2032.dll")]
        public static extern int ConfiguraModeloImpressora(int model);

        [DllImport("MP2032.dll")]
        public static extern int IniciaPorta(String porta);

        [DllImport("MP2032.dll")]
        public static extern int ImprimeCodigoBarrasCODE128(String texto);

        [DllImport("MP2032.dll")]
        public static extern int ConfiguraCodigoBarras(int Altura, int Largura, int Posicao, int Fonte, int Margem);

        [DllImport("MP2032.dll")]
        public static extern int ImprimeCodigoQRCODE(int errorCorrectionLevel, int moduleSize, int codeType, int QRCodeVersion, int encodingModes, String codeQr);

        [DllImport("MP2032.dll")]
        public static extern int ComandoTX(String comando, int tComando);

        [DllImport("MP2032.dll")]
        public static extern int FormataTX(String texto, int TipoLetra, int italico, int sublinhado, int expandido, int enfatizado);

        [DllImport("MP2032.dll")]
        public static extern int AcionaGuilhotina(int parcial_full);

        [DllImport("MP2032.dll")]
        public static extern int FechaPorta();
    }
}
