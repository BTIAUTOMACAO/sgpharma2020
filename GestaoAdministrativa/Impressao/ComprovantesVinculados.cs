﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.Impressao
{
    public class ComprovantesVinculados
    {
        public static bool VinculadoDebitoCreditoSAT(int vias, string numeroNota, string xml, double valorCompra, double valorPagamento, string especieDescricao, string vendedor)
        {
            try
            {
                string comprovante, tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);

                for (int i = 1; i <= vias; i++)
                {
                    comprovante = "";
                    xnResul = xmlSAT.GetElementsByTagName("emit");
                    if (xnResul.Count > 0)
                    {
                        tamanho = xnResul[0]["xNome"].InnerText;
                        comprovante = Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n\n";
                        comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IM:" + xnResul[0]["IM"].InnerText, 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                    }

                    comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("COMPROVANTE CREDITO OU DEBITO", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(especieDescricao, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(i + " VIA", 43) + "\n";
                    comprovante += "COD do documento vinculado:" + numeroNota + "\n";
                    comprovante += "Valor da compra           :" + String.Format("{0:N}", valorCompra).Replace(",", ".") + "\n";
                    comprovante += "Valor do pagamento        :" + String.Format("{0:N}", valorPagamento).Replace(",", ".") + "\n";
                    comprovante += "Vendedor: " + vendedor + "\n";
                    xnResul = xmlSAT.GetElementsByTagName("ide");
                    if (xnResul.Count > 0)
                    {
                        comprovante += "Data: " + xnResul[0]["dEmi"].InnerText.Substring(6, 2) + "/" + xnResul[0]["dEmi"].InnerText.Substring(4, 2)
                       + "/" + xnResul[0]["dEmi"].InnerText.Substring(0, 4) + " - Hora: " + xnResul[0]["hEmi"].InnerText.Substring(0, 2)
                       + ":" + xnResul[0]["hEmi"].InnerText.Substring(2, 2) + ":" + xnResul[0]["hEmi"].InnerText.Substring(4, 2) + "\n";
                    }
                    comprovante += "__________________________________________" + "\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F") 
                            && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);
                            
                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                  && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                            if (iRetorno != 1)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovante += "\n\n\n\n";
                            Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                    else
                    {
                        comprovante += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool VinculadoDrogabellaPlantaoSAT(int vias, string numeroNota, string xml, double valorCompra, double valorPagamento, string especieDescricao,
            string autorizacaoSemReceita, string autorizacaoComReceita, long pedido, string comprovanteSemReceita, string comprovanteComReceita, string empresa, string codEmpresa,
            string cartao, string conveniado, bool entrega, string comprovanteDasAutorizacoes, string comprovanteMedico, string vendedor, int parcelaDrogabella)
        {
            try
            {
                string comprovante, tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);

                for (int i = 1; i <= vias; i++)
                {
                    comprovante = "";
                    xnResul = xmlSAT.GetElementsByTagName("emit");
                    if (xnResul.Count > 0)
                    {
                        tamanho = xnResul[0]["xNome"].InnerText;
                        comprovante = Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n\n";
                        comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IM:" + xnResul[0]["IM"].InnerText, 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                    }

                    comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("RELATORIO GERENCIAL", 43) + "\n";
                    comprovante += "COD do documento vinculado:" + numeroNota.PadLeft(20 - numeroNota.Length, ' ') + "\n";
                    comprovante += "Valor da compra:" + String.Format("{0:N}", valorCompra).Replace(",", ".").PadLeft(30 - valorCompra.ToString().Length, ' ') + "\n";
                    comprovante += "Valor do pagamento:" + String.Format("{0:N}", valorPagamento).Replace(",", ".").PadLeft(27 - valorPagamento.ToString().Length, ' ') + "\n";
                    comprovante += "Forma de pagamento:" + especieDescricao.PadLeft(30 - especieDescricao.Length, ' ') + "\n";
                    comprovante += "Vendedor: " + vendedor + "\n\n"; ;
                    if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                    {
                        comprovante += "Autorizacao:" + autorizacaoSemReceita.PadLeft(38 - autorizacaoSemReceita.Length, ' ') + "\n";
                    }
                    if (!String.IsNullOrEmpty(autorizacaoComReceita))
                    {
                        comprovante += "Autorizacao com Receita:" + autorizacaoComReceita.PadLeft(26 - autorizacaoComReceita.Length, ' ') + "\n";
                    }
                    comprovante += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                    comprovante += "Cartao:                  " + cartao + "\n";
                    comprovante += "Conveniado:  " + (conveniado.Length > 33 ? conveniado.Substring(0, 32) : conveniado.PadLeft(33 - conveniado.Length, ' ')) + "\n";
                    comprovante += "EMPRESA: " + ((codEmpresa + " - " + empresa).Length > 32 ? (codEmpresa + " - " + empresa).Substring(0, 32)
                        : (codEmpresa + " - " + empresa).PadLeft(32 - (codEmpresa + " - " + empresa).Length, ' ')) + "\n";
                    if (!parcelaDrogabella.Equals(1))
                    {
                        comprovante += "N. Parcelas: " + parcelaDrogabella.ToString().PadLeft(29 - parcelaDrogabella.ToString().Length);
                    }
                    if (!String.IsNullOrEmpty(comprovanteMedico))
                    {
                        comprovante += comprovanteMedico;
                    }
                    comprovante += "Num. Pedido:" + pedido.ToString().PadLeft(31 - pedido.ToString().Length, ' ') + "\n\n";
                    xnResul = xmlSAT.GetElementsByTagName("ide");
                    if (xnResul.Count > 0)
                    {
                        comprovante += "Data: " + xnResul[0]["dEmi"].InnerText.Substring(6, 2) + "/" + xnResul[0]["dEmi"].InnerText.Substring(4, 2)
                       + "/" + xnResul[0]["dEmi"].InnerText.Substring(0, 4) + " - Hora: " + xnResul[0]["hEmi"].InnerText.Substring(0, 2)
                       + ":" + xnResul[0]["hEmi"].InnerText.Substring(2, 2) + ":" + xnResul[0]["hEmi"].InnerText.Substring(4, 2) + "\n";
                    }
                    if (entrega)
                    {
                        comprovante += "AUTORIZADO MEDIANTE A SENHA DE ENTREGA" + "\n";
                    }
                    else
                    {
                        comprovante += "AUTORIZADO MEDIANTE A SENHA PESSOAL" + "\n";
                    }
                    comprovante += "__________________________________________" + "\n";
                    comprovante += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovante += "__________________________________________" + "\n";

                    if (!String.IsNullOrEmpty(comprovanteSemReceita))
                    {
                        comprovante += comprovanteSemReceita;
                    }

                    if (!String.IsNullOrEmpty(comprovanteComReceita))
                    {
                        comprovante += comprovanteComReceita;
                    }

                    comprovante += "__________________________________________" + "\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }
                            
                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                            if (iRetorno != 1)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovante += "\n\n\n\n";
                            Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                    else
                    {
                        //IMPRIME COMPROVANTE
                        comprovante += "\n\n\n\n";
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    if (comprovanteDasAutorizacoes.Equals("S"))
                    {
                        xnResul = xmlSAT.GetElementsByTagName("emit");
                        comprovante = Funcoes.CentralizaTexto("COMPROVANTE", 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n\n";
                        comprovante = Funcoes.CentralizaTexto(xnResul[0]["xFant"].InnerText, 43) + "\n";
                        tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                        if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                        {
                            comprovante += "Autorizacao:" + autorizacaoSemReceita.PadLeft(38 - autorizacaoSemReceita.Length, ' ') + "\n";
                        }
                        if (!String.IsNullOrEmpty(autorizacaoComReceita))
                        {
                            comprovante += "Autorizacao com Receita:" + autorizacaoComReceita.PadLeft(26 - autorizacaoComReceita.Length, ' ') + "\n";
                        }
                        comprovante += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                        comprovante += "Cartao:                  " + cartao + "\n";
                        comprovante += "Conveniado:  " + (conveniado.Length > 33 ? conveniado.Substring(0, 32) : conveniado.PadLeft(33 - conveniado.Length, ' ')) + "\n";
                        comprovante += "EMPRESA: " + ((codEmpresa + " - " + empresa).Length > 32 ? (codEmpresa + " - " + empresa).Substring(0, 32)
                            : (codEmpresa + " - " + empresa).PadLeft(32 - (codEmpresa + " - " + empresa).Length, ' ')) + "\n";
                        comprovante += "__________________________________________" + "\n";

                        if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                        {
                            comprovante += Funcoes.CentralizaTexto("COMPRAS SEM RECEITA", 43) + "\n";
                            comprovante += comprovanteSemReceita;
                            comprovante += "__________________________________________" + "\n";
                        }

                        if (!String.IsNullOrEmpty(autorizacaoComReceita))
                        {
                            comprovante += Funcoes.CentralizaTexto("COMPRAS COM RECEITA", 43) + "\n";
                            comprovante += comprovanteComReceita;
                            comprovante += "__________________________________________" + "\n";
                        }

                        comprovante += "Declaro ter verificado a classificacao" + "\n";
                        comprovante += "das compras com e sem receita para fins" + "\n";
                        comprovante += "de desconto." + "\n\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += "DROGA BELLA AGRADECE TUDO DE BOM P/ VOCE" + "\n";
                        comprovante += "__________________________________________" + "\n\n\n";

                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);
                                
                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                                if (iRetorno != 1)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                                Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                        else
                        {
                            comprovante += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool VinculadoParticular(int vias, string numeroNota, string xml, double valorCompra, double valorPagamento, string especieDescricao, string vendedor,
            string comprovanteProdutos, List<VendasFormaPagamento> vendaPedFormaPagto, string nome, string codConveniada)
        {
            try
            {
                string comprovante, tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);

                if(Funcoes.LeParametro(2,"35", false).Equals("S"))
                {
                    vias = vendaPedFormaPagto.Count;
                }

                for (int i = 1; i <= vias; i++)
                {
                    comprovante = "";
                    xnResul = xmlSAT.GetElementsByTagName("emit");
                    if (xnResul.Count > 0)
                    {
                        tamanho = xnResul[0]["xNome"].InnerText;
                        comprovante = Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n\n";
                        comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IM:" + xnResul[0]["IM"].InnerText, 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                    }

                    comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("RELATORIO GERENCIAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(especieDescricao, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(i + " VIA", 43) + "\n";
                    comprovante += "COD do documento vinculado:" + numeroNota + "\n";
                    comprovante += "Valor da compra           :" + String.Format("{0:N}", valorCompra).Replace(",", ".") + "\n";
                    comprovante += "Valor do pagamento        :" + String.Format("{0:N}", valorPagamento).Replace(",", ".") + "\n";
                    comprovante += "Vendedor: " + vendedor + "\n";
                    comprovante += "Nome: " + nome + "\n";
                    comprovante += "Empresa:" + codConveniada + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", codConveniada) + "\n";
                    xnResul = xmlSAT.GetElementsByTagName("ide");
                    if (xnResul.Count > 0)
                    {
                        comprovante += "Data: " + xnResul[0]["dEmi"].InnerText.Substring(6, 2) + "/" + xnResul[0]["dEmi"].InnerText.Substring(4, 2)
                       + "/" + xnResul[0]["dEmi"].InnerText.Substring(0, 4) + " - Hora: " + xnResul[0]["hEmi"].InnerText.Substring(0, 2)
                       + ":" + xnResul[0]["hEmi"].InnerText.Substring(2, 2) + ":" + xnResul[0]["hEmi"].InnerText.Substring(4, 2) + "\n";
                    }
                    comprovante += "__________________________________________" + "\n";
                    comprovante += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovante += "__________________________________________" + "\n";
                    comprovante += comprovanteProdutos;
                    comprovante += "__________________________________________" + "\n";

                    if (Funcoes.LeParametro(6, "93", false).Equals("S"))
                    {
                        comprovante += Funcoes.CentralizaTexto("DESCRICAO DAS PARCELAS", 43) + "\n";
                        comprovante += "PARC  VENCIMENTO  VALOR\n";
                        for (int x = 0; x < vendaPedFormaPagto.Count; x++)
                        {
                            comprovante += Funcoes.FormataZeroAEsquerda(vendaPedFormaPagto[x].VendaParcela.ToString(), 4) + "  " + vendaPedFormaPagto[x].VendaParcelaVencimento.ToString("dd/MM/yyyy") + "  " + String.Format("{0:N}", vendaPedFormaPagto[x].VendaValorParcela).Replace(",", ".") + "\n";
                        }
                        comprovante += "__________________________________________" + "\n\n\n";
                    }

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);
                            
                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                            if (iRetorno != 1)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovante += "\n\n\n\n";
                            Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                    else
                    {
                        comprovante += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool CupomVinculadoBeneficios(string cupomBeneficio, string tipoBeneficio = "", string numeroNota = "", int vias = 1)
        {
            try
            {
                string comprovante, tamanho;
                
                for (int y = 1; y <= Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); y++)
                {
                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        comprovante = "";
                        var dadosEstabelecimento = new Estabelecimento();
                        List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                        if (retornoEstab.Count > 0)
                        {
                            comprovante = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovante += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovante += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovante += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovante += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovante += retornoEstab[0].EstBairro + "\n";
                            comprovante += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n\n";
                        }

                        comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("RELATORIO GERENCIAL", 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(y + " VIA", 43) + "\n";

                        if (tipoBeneficio.Equals("FARMACIAPOPULAR"))
                        {
                            comprovante += "\n";
                            comprovante += "COD do documento vinculado:" + numeroNota + "\n";
                        }
                        
                        string[] stringCupom = cupomBeneficio.Split('@');

                        for (int i = 0; i < stringCupom.Length; i++)
                        {
                            comprovante += Funcoes.CentralizaTexto(stringCupom[i], 43) + "\n";
                        }

                        comprovante += "\n\n";

                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                                if (iRetorno != 1)
                                {
                                    return false;
                                }

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                                if (iRetorno != 1)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovante += "\n\n\n\n";
                                Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                                Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                        else
                        {
                            comprovante += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        Principal.ReimprimeGerencial = false;

                        if (!Imprimir.ImpFiscalAbrirGerencial("COD do documento vinculado:" + numeroNota + "\n"))
                        {
                            if (Principal.ReimprimeGerencial)
                            {
                                Imprimir.ImpFiscalFechaGerencial();
                                return CupomVinculadoBeneficios(cupomBeneficio, tipoBeneficio, numeroNota);
                            }
                            else if(tipoBeneficio.Equals("PORTALDROGARIA"))
                            {
                                Imprimir.ImpFiscalFechaGerencial();
                                AnulaVendaPortalDrogaria();
                                return false;
                            }
                            else
                                return false;
                        }

                        string[] stringCupom = cupomBeneficio.Split('@');

                        for (int i = 0; i < stringCupom.Length; i++)
                        {
                            if (!Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto(stringCupom[i], 40) + "\n" ))
                            {
                                if (Principal.ReimprimeGerencial)
                                {
                                    Imprimir.ImpFiscalFechaGerencial();
                                    return CupomVinculadoBeneficios(cupomBeneficio, tipoBeneficio, numeroNota);
                                }
                                else if (tipoBeneficio.Equals("PORTALDROGARIA"))
                                {
                                    AnulaVendaPortalDrogaria();
                                    return false;
                                }
                                else
                                    return false;
                            }

                            if (tipoBeneficio.Equals("FARMACIAPOPULAR"))
                            {
                                Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                            }
                        }

                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                        
                        if (!Imprimir.ImpFiscalFechaGerencial())
                            return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static void AnulaVendaPortalDrogaria()
        {
            Negocio.Beneficios.PortalDaDrogaria vendaPortal = new Negocio.Beneficios.PortalDaDrogaria();
            var buscaPendente = new BeneficioNovartisPendente();
            DataTable dtRetorno = buscaPendente.BuscaVendasPendentes();
            if (dtRetorno.Rows.Count > 0)
            {
                for (int i = 0; i < dtRetorno.Rows.Count; i++)
                {
                    if (dtRetorno.Rows[i]["TIPO_TRANSACAO"].ToString().Equals("V"))
                    {
                        string nsu = dtRetorno.Rows[i]["NSU"].ToString();

                        vendaPortal.EnviaAnulaVenda(dtRetorno.Rows[i]["OPERADORA"].ToString(), nsu);

                        string caminho, erro;
                        List<RetornoPortalDrogaria> retono = vendaPortal.RetornoAnulaVendaErro(out erro, out caminho);
                        if (retono.Count > 0)
                        {
                            MessageBox.Show("Houve algum erro na venda com NSU: " + nsu + ", por esse motivo foi realizado o cancelamento da mesma!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            buscaPendente.ExcluirDadosPorNSU(nsu);

                            var excluiEfetivado = new BeneficioNovartisEfetivado();
                            excluiEfetivado.ExcluirDadosPorNSU(nsu);

                            var excluiProduto = new BeneficioNovartisProdutos();
                            excluiProduto.ExcluirDadosPorNSU(nsu);
                        }
                    }
                }
            }

        }

        public static bool CupomVinculadoFuncionalSAT(ClienteCartaoFuncional objClienteCartaoFuncional, ConfirmaVendaResponse objConfirmaVendaResponse, 
            PreAutorizacaoFuncionalResponse obj, VendaAutoriadaResponse objVendaAutorizadaResponse, string numeroNota, string tipoVenda, 
            double diferencaDoValorAutorizado = 0, DataTable DadosComandaFuncional = null, string produtos = "", int qtdeVias = 1)
        {
            try
            {
                string comprovante, tamanho;
                for (int y = 1; y <= qtdeVias; y++)
                {
                    comprovante = "";
                    var dadosEstabelecimento = new Estabelecimento();
                    List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                    if (retornoEstab.Count > 0)
                    {
                        comprovante = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovante += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovante += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovante += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovante += retornoEstab[0].EstBairro + "\n";
                        comprovante += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n\n";

                    }

                    comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("RELATORIO GERENCIAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(y + " VIA", 43) + "\n";


                    comprovante += "\n";
                    comprovante += "\n";
                    comprovante += "COD do documento vinculado:" + numeroNota + "\n";


                    if (DadosComandaFuncional != null)
                    {
                        comprovante += "CARTAO: " + DadosComandaFuncional.Rows[0]["CARTAO"].ToString() + "\n";
                        comprovante += "NOME: " + DadosComandaFuncional.Rows[0]["NOME"].ToString() + "\n";
                        string a = DadosComandaFuncional.Rows[0]["DATA"].ToString();
                        string dataTransacao = a;

                        string horaTransacao = DateTime.ParseExact(DadosComandaFuncional.Rows[0]["HORA"].ToString(), "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                        comprovante += "DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n";


                        if (!String.IsNullOrEmpty(DadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString()))
                        {
                            comprovante += DadosComandaFuncional.Rows[0]["CONSELHO"].ToString() + ": " + DadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString() + "\n";
                        }
                        comprovante += "AUTORIZACAO: " + DadosComandaFuncional.Rows[0]["NSU_AUTORIZADOR"].ToString() + "\n";
                        comprovante += "NSU: " + DadosComandaFuncional.Rows[0]["NSU"].ToString() + "\n";

                        comprovante += "TOTAL DA COMPRA: " + DadosComandaFuncional.Rows[0]["TOTAL_VENDA"].ToString() + "\n";
                        comprovante += "DESCONTO: " + DadosComandaFuncional.Rows[0]["DESCONTO"].ToString() + "\n";
                        comprovante += "TOTAL A PAGAR: " + DadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString() + "\n";
                        comprovante += "(" + Util.toExtenso(Convert.ToDecimal(DadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString())) + ")" + "\n";
                        if (tipoVenda == "T")
                        {
                            comprovante += "VALOR A PAGAR NO CARTAO: " + DadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n";
                        }
                        else if (tipoVenda == "P")
                        {
                            comprovante += "VALOR A PAGAR A VISTA: " + DadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n";
                            comprovante += "VALOR A PAGAR NO CARTAO: " + DadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n";
                        }
                        else
                        {
                            comprovante += "VALOR A PAGAR A VISTA: " + DadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n";
                        }
                    }
                    else
                    {
                        comprovante += "CARTAO: " + objClienteCartaoFuncional.Trilha2DoCartao + "\n";
                        comprovante += "NOME: " + objClienteCartaoFuncional.NomeDoPortador + "\n";
                        string a = objConfirmaVendaResponse.DataTransacao;
                        string dataTransacao = DateTime.ParseExact(a, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                        string horaTransacao = DateTime.ParseExact(objConfirmaVendaResponse.HoraTransacao, "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                        //string hora = objConfirmaVendaResponse.HoraTransacao;
                        comprovante += "DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n";

                        if (objConfirmaVendaResponse.VendaComReceita == "S")
                        {
                            comprovante += objVendaAutorizadaResponse.TipoConselhoDaVenda + ": " + Principal.crm + "\n";
                        }
                        comprovante += "AUTORIZACAO: " + objConfirmaVendaResponse.NSUDoAutorizador + "\n";
                        comprovante += "NSU: " + objConfirmaVendaResponse.NSU + "\n";

                        comprovante += "TOTAL DA COMPRA: " + objVendaAutorizadaResponse.ValorTotal.ToString("C") + "\n";
                        comprovante += "DESCONTO: " + objVendaAutorizadaResponse.Desconto.ToString("C") + "\n";
                        comprovante += "TOTAL A PAGAR: " + objVendaAutorizadaResponse.TotalAPagar.ToString("C") + "\n";
                        comprovante += "(" + Util.toExtenso(Convert.ToDecimal(objVendaAutorizadaResponse.TotalAPagar)) + ")" + "\n";
                        if (tipoVenda == "T")
                        {
                            comprovante += "VALOR A PAGAR NO CARTAO: " + objVendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n";
                        }
                        else if (tipoVenda == "P")
                        {
                            comprovante += "VALOR A PAGAR A VISTA: " + objVendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n";
                            comprovante += "VALOR A PAGAR NO CARTAO: " + objVendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n";
                        }
                        else
                        {
                            comprovante += "VALOR A PAGAR A VISTA: " + objVendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n";
                        }
                    }

                    comprovante += "__________________________________________" + "\n";
                    comprovante += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovante += "__________________________________________" + "\n";

                    if (!String.IsNullOrEmpty(produtos))
                    {
                        comprovante += produtos;
                    }
                    comprovante += "__________________________________________";

                    if (Convert.ToInt32(Funcoes.LeParametro(2, "26", false)).Equals(1))
                    {
                        comprovante += "\n\n\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n\n\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += Funcoes.CentralizaTexto("RG", 43) + "\n\n\n";
                    }
                    else if (y == 2)
                    {
                        comprovante += "\n\n\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n\n\n";
                        comprovante += "__________________________________________" + "\n";
                        comprovante += Funcoes.CentralizaTexto("RG", 43) + "\n\n\n";
                    }

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);
                            
                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                            if (iRetorno != 1)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovante += "\n\n\n\n";
                            Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                    else
                    {
                        comprovante += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool CupomVinculadoFuncionalFiscal(ClienteCartaoFuncional objClienteCartaoFuncional, ConfirmaVendaResponse objConfirmaVendaResponse, 
            PreAutorizacaoFuncionalResponse obj, VendaAutoriadaResponse objVendaAutorizadaResponse, string numeroNota, string tipoVenda, double diferencaDoValorAutorizado = 0, 
            DataTable DadosComandaFuncional = null, string produtos = "", int qtdeVias = 1)
        {
            try
            {
                for (int y = 1; y <= qtdeVias; y++)
                {
                    if (!Imprimir.ImpFiscalAbrirGerencial("COD do documento vinculado:" + numeroNota + "\n"))
                        return false;

                    if (DadosComandaFuncional != null)
                    {
                        Imprimir.ImpFiscalTextoVinculado("CARTAO: " + DadosComandaFuncional.Rows[0]["CARTAO"].ToString() + "\n");
                        Imprimir.ImpFiscalTextoVinculado("NOME: " + DadosComandaFuncional.Rows[0]["NOME"].ToString() + "\n");
                        string a = DadosComandaFuncional.Rows[0]["DATA"].ToString();
                        string dataTransacao = a;

                        string horaTransacao = DateTime.ParseExact(DadosComandaFuncional.Rows[0]["HORA"].ToString(), "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                        Imprimir.ImpFiscalTextoVinculado("DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n");
                        
                        if (!String.IsNullOrEmpty((DadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString())))
                        {
                            Imprimir.ImpFiscalTextoVinculado(DadosComandaFuncional.Rows[0]["CONSELHO"].ToString() + ": " + DadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString() + "\n");
                        }
                        Imprimir.ImpFiscalTextoVinculado("AUTORIZACAO: " + DadosComandaFuncional.Rows[0]["NSU_AUTORIZADOR"].ToString() + "\n");
                        Imprimir.ImpFiscalTextoVinculado("NSU: " + DadosComandaFuncional.Rows[0]["NSU"].ToString() + "\n");

                        Imprimir.ImpFiscalTextoVinculado("TOTAL DA COMPRA: " + DadosComandaFuncional.Rows[0]["TOTAL_VENDA"].ToString() + "\n");
                        Imprimir.ImpFiscalTextoVinculado("DESCONTO: " + DadosComandaFuncional.Rows[0]["DESCONTO"].ToString() + "\n");
                        Imprimir.ImpFiscalTextoVinculado("TOTAL A PAGAR: " + DadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString() + "\n");
                        Imprimir.ImpFiscalTextoVinculado("(" + Util.toExtenso(Convert.ToDecimal(DadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString())) + ")" + "\n");
                        if (tipoVenda == "T")
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR NO CARTAO: " + DadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n");
                        }
                        else if (tipoVenda == "P")
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR A VISTA: " + DadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n");
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR NO CARTAO: " + DadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n");
                        }
                        else
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR A VISTA: " + DadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n");
                        }
                    }
                    else
                    {
                        Imprimir.ImpFiscalTextoVinculado("CARTAO: " + objClienteCartaoFuncional.Trilha2DoCartao + "\n");
                        Imprimir.ImpFiscalTextoVinculado("NOME: " + objClienteCartaoFuncional.NomeDoPortador + "\n");
                        string a = objConfirmaVendaResponse.DataTransacao;
                        string dataTransacao = DateTime.ParseExact(a, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                        string horaTransacao = DateTime.ParseExact(objConfirmaVendaResponse.HoraTransacao, "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                        
                        Imprimir.ImpFiscalTextoVinculado("DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n");

                        if (objConfirmaVendaResponse.VendaComReceita == "S")
                        {
                            Imprimir.ImpFiscalTextoVinculado(objVendaAutorizadaResponse.TipoConselhoDaVenda + ": " + Principal.crm + "\n");
                        }
                        Imprimir.ImpFiscalTextoVinculado("AUTORIZACAO: " + objConfirmaVendaResponse.NSUDoAutorizador + "\n");
                        Imprimir.ImpFiscalTextoVinculado("NSU: " + objConfirmaVendaResponse.NSU + "\n");

                        Imprimir.ImpFiscalTextoVinculado("TOTAL DA COMPRA: " + objVendaAutorizadaResponse.ValorTotal.ToString("C") + "\n");
                        Imprimir.ImpFiscalTextoVinculado("DESCONTO: " + objVendaAutorizadaResponse.Desconto.ToString("C") + "\n");
                        Imprimir.ImpFiscalTextoVinculado("TOTAL A PAGAR: " + objVendaAutorizadaResponse.TotalAPagar.ToString("C") + "\n");
                        Imprimir.ImpFiscalTextoVinculado("(" + Util.toExtenso(Convert.ToDecimal(objVendaAutorizadaResponse.TotalAPagar)) + ")" + "\n");
                        if (tipoVenda == "T")
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR NO CARTAO: " + objVendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n");
                        }
                        else if (tipoVenda == "P")
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR A VISTA: " + objVendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n");
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR NO CARTAO: " + objVendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n");
                        }
                        else
                        {
                            Imprimir.ImpFiscalTextoVinculado("VALOR A PAGAR A VISTA: " + objVendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n");
                        }
                    }

                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                    Imprimir.ImpFiscalTextoVinculado("#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n");
                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                    if (!String.IsNullOrEmpty(produtos))
                    {
                        string[] stringCupom = produtos.Split('\n');
                        for (int x = 0; x < stringCupom.Length; x++)
                        {
                            Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                        }
                    }

                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                    if (Convert.ToInt32(Funcoes.LeParametro(2, "26", false)).Equals(1))
                    {
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40));
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40));
                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("RG", 40) + "\n");
                    }
                    else if (y == 2)
                    {
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40) + "\n");
                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40));
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.PrencherEspacoEmBranco(" ", 40));
                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("RG", 40) + "\n");
                    }

                    if (!Imprimir.ImpFiscalFechaGerencial())
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public static bool VinculadoDebitoCreditoFiscal(int vias, string numeroNota, double valorCompra, double valorPagamento, string especieDescricao, string vendedor)
        {
            try
            {
                for (int i = 1; i <= vias; i++)
                {
                    if (!Imprimir.ImpFiscalAbrirVinculado(especieDescricao, "", numeroNota, valorPagamento.ToString()))
                        return false;

                    if (!Imprimir.ImpFiscalFechaVinculado())
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool VinculadoDrogabellaPlantaoFiscal(int vias, string numeroNota, DateTime data, double valorCompra, double valorPagamento, string especieDescricao,
          string autorizacaoSemReceita, string autorizacaoComReceita, long pedido, string comprovanteSemReceita, string comprovanteComReceita, string empresa, string codEmpresa,
          string cartao, string conveniado, bool entrega, string comprovanteDasAutorizacoes, string comprovanteMedico, string vendedor, int parcelaDrogabella)
        {
            try
            {
                for (int i = 1; i <= vias; i++)
                {
                    if (comprovanteDasAutorizacoes.Equals("S"))
                    {
                        if (!Imprimir.ImpFiscalAbrirGerencial("COD do documento vinculado:" + numeroNota + "\n"))
                            return false;

                        Imprimir.ImpFiscalTextoVinculado("Vendedor: " + vendedor + "     Num.Pedido:" + pedido + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Cartao: " + cartao + "\n");
                        if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado("Autorizacao:" + autorizacaoSemReceita + "\n");
                        }
                        if (!String.IsNullOrEmpty(autorizacaoComReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado("Autorizacao com Receita:" + autorizacaoComReceita + "\n");
                        }
                        Imprimir.ImpFiscalTextoVinculado("Administradora:   DROGABELLA/PLANTAO CARD" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Cartao: " + cartao + "\n");
                        if(!parcelaDrogabella.Equals(1))
                        {
                            Imprimir.ImpFiscalTextoVinculado("N. Parcelas: " + parcelaDrogabella + "\n");
                        }
                        if (!String.IsNullOrEmpty(comprovanteMedico))
                        {
                            Imprimir.ImpFiscalTextoVinculado(comprovanteMedico + "\n");
                        }

                        if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("COMPRAS SEM RECEITA", 40) + "\n");
                            string[] stringCupom = comprovanteSemReceita.Split('\n');
                            for (int x = 0; x < stringCupom.Length; x++)
                            {
                                Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                            }
                        }

                        if (!String.IsNullOrEmpty(autorizacaoComReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("COMPRAS COM RECEITA", 40) + "\n");
                            string[] stringCupom = comprovanteComReceita.Split('\n');
                            for (int x = 0; x < stringCupom.Length; x++)
                            {
                                Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                            }
                        }

                        Imprimir.ImpFiscalTextoVinculado("Declaro ter verificado a classificacao" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("das compras com e sem receita para fins" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("de desconto." + "\n");
                        Imprimir.ImpFiscalTextoVinculado("----------------------------------------" + "\n");
                        Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("ASSINATURA", 40) + "\n");

                        if (!Imprimir.ImpFiscalFechaGerencial())
                            return false;
                    }
                    else
                    {
                        if (!Imprimir.ImpFiscalAbrirGerencial("COD do documento vinculado:" + numeroNota + "\n"))
                            return false;

                        Imprimir.ImpFiscalTextoVinculado("Valor da compra:" + String.Format("{0:N}", valorCompra).Replace(",", ".") + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Valor do pagamento:" + String.Format("{0:N}", valorPagamento).Replace(",", ".") + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Vendedor: " + vendedor + "     Num.Pedido:" + pedido + "\n");
                        if (!String.IsNullOrEmpty(autorizacaoSemReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado("Autorizacao:" + autorizacaoSemReceita + "\n");
                        }
                        if (!String.IsNullOrEmpty(autorizacaoComReceita))
                        {
                            Imprimir.ImpFiscalTextoVinculado("Autorizacao com Receita:" + autorizacaoComReceita + "\n");
                        }
                        Imprimir.ImpFiscalTextoVinculado("Administradora:   DROGABELLA/PLANTAO CARD" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Cartao: " + cartao + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Conveniado: " + (conveniado.Length > 37 ? conveniado.Substring(0, 37) : conveniado) + "\n");
                        Imprimir.ImpFiscalTextoVinculado("Empresa: " + ((codEmpresa + " - " + empresa).Length > 40 ? (codEmpresa + " - " + empresa).Substring(0, 40)
                            : (codEmpresa + " - " + empresa)) + "\n");
                        if (!String.IsNullOrEmpty(comprovanteMedico))
                        {
                            Imprimir.ImpFiscalTextoVinculado(comprovanteMedico);
                        }

                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n");
                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                        if (!String.IsNullOrEmpty(comprovanteSemReceita))
                        {
                            string[] stringCupom = comprovanteSemReceita.Split('\n');
                            for (int x = 0; x < stringCupom.Length; x++)
                            {
                                Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                            }
                        }

                        if (!String.IsNullOrEmpty(comprovanteComReceita))
                        {
                            string[] stringCupom = comprovanteComReceita.Split('\n');
                            for (int x = 0; x < stringCupom.Length; x++)
                            {
                                Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                            }
                        }

                        Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                        if (!Imprimir.ImpFiscalFechaGerencial())
                            return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool VinculadoParticularFiscal(int vias, string numeroNota, long pedido, double valorCompra, double valorPagamento, string especieDescricao, string vendedor,
            List<VendasFormaPagamento> vendaPedFormaPagto, string nome, string codConveniada, string produtosComprov)
        {
            try
            {
                string comprovante;

                if (Funcoes.LeParametro(2, "35", false).Equals("S"))
                {
                    vias = vendaPedFormaPagto.Count;
                }
                
                for (int i = 1; i <= vias; i++)
                {
                    if (!Imprimir.ImpFiscalAbrirGerencial("COD do documento vinculado:" + numeroNota + "\n"))
                        return false;

                    Imprimir.ImpFiscalTextoVinculado("Valor da compra           : " + String.Format("{0:N}", valorCompra).Replace(",", ".") + "\n");
                    Imprimir.ImpFiscalTextoVinculado("Valor do pagamento        : " + String.Format("{0:N}", valorPagamento).Replace(",", ".") + "\n");
                    Imprimir.ImpFiscalTextoVinculado("Vendedor   : " + vendedor + "\n");
                    Imprimir.ImpFiscalTextoVinculado("Num.Pedido : " + pedido + "\n");
                    Imprimir.ImpFiscalTextoVinculado("Nome       : " + nome + "\n");
                    Imprimir.ImpFiscalTextoVinculado("Empresa    : " + codConveniada + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", codConveniada) + "\n");
                    Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("DESCRICAO DAS PARCELAS", 40) + "\n");
                    Imprimir.ImpFiscalTextoVinculado(Funcoes.CentralizaTexto("PARC  VENCIMENTO  VALOR", 40) + "\n");

                    for (int x = 0; x < vendaPedFormaPagto.Count; x++)
                    {
                        comprovante = Funcoes.PrencherEspacoEmBranco(Funcoes.FormataZeroAEsquerda(vendaPedFormaPagto[x].VendaParcela.ToString(), 4) + "  " + vendaPedFormaPagto[x].VendaParcelaVencimento.ToString("dd/MM/yyyy") + "  " + String.Format("{0:N}", vendaPedFormaPagto[x].VendaValorParcela).Replace(",", "."), 40);

                        Imprimir.ImpFiscalTextoVinculado(comprovante + "\n");
                    }

                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");
                    Imprimir.ImpFiscalTextoVinculado("#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n");
                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                    if (!String.IsNullOrEmpty(produtosComprov))
                    {
                        string[] stringCupom = produtosComprov.Split('\n');
                        for (int x = 0; x < stringCupom.Length; x++)
                        {
                            Imprimir.ImpFiscalTextoVinculado(stringCupom[x] + "\n");
                        }
                    }

                    Imprimir.ImpFiscalTextoVinculado("__________________________________________" + "\n");

                    if (!Imprimir.ImpFiscalFechaGerencial())
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool VinculadoDesconto(string numeroNota, string xml, double valorCompra, double valorPagamento, string vendedor,
           string comprovanteProdutos)
        {
            try
            {
                string comprovante, tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);

                for (int i = 1; i <= 1; i++)
                {
                    comprovante = "";
                    xnResul = xmlSAT.GetElementsByTagName("emit");
                    if (xnResul.Count > 0)
                    {
                        tamanho = xnResul[0]["xNome"].InnerText;
                        comprovante = Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n\n";
                        comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                        comprovante += Funcoes.CentralizaTexto("IM:" + xnResul[0]["IM"].InnerText, 43) + "\n";
                        comprovante += "__________________________________________" + "\n";
                    }

                    comprovante += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("RELATORIO GERENCIAL", 43) + "\n";
                    comprovante += "COD do documento vinculado:" + numeroNota + "\n";
                    comprovante += "Valor Total          :" + String.Format("{0:N}", valorCompra).Replace(",", ".") + "\n";
                    comprovante += "Valor do Pagamento   :" + String.Format("{0:N}", valorPagamento).Replace(",", ".") + "\n";
                    comprovante += "Valor de Desconto    :" + String.Format("{0:N}", valorCompra - valorPagamento).Replace(",", ".") + "\n";
                    comprovante += "Vendedor: " + vendedor + "\n\n";
                    comprovante += Funcoes.CentralizaTexto("PRODUTOS COM DESCONTO", 43) + "\n";
                    comprovante += "__________________________________________" + "\n";
                    comprovante += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovante += "__________________________________________" + "\n";
                    comprovante += comprovanteProdutos;
                    comprovante += "__________________________________________" + "\n";
                    
                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                            if (iRetorno != 1)
                            {
                                return false;
                            }

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                            if (iRetorno != 1)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                    else
                    {
                        comprovante += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Vinculado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
