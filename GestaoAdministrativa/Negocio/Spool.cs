﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    class Spool
    {
        /// <summary>
        /// DELETA OS REGISTROS DAS TABELAS SE SPOOL DA ENTRADA DE NOTAS
        /// </summary>
        /// <param name="estacao">Nome da Estação</param>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="empCodigo">Cód. da Empresa</param>
        public void ExcluiRegistrosSpoolEntradasTodas(string estacao, int estCodigo, int empCodigo)
        {
            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_ENTRADA_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null);
            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_ENTRADA_ITENS_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null);
            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_ENTRADA_DETALHES_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null);
        }

        public bool InsereRegistrosSpoolEntrada(Entrada spoolEntrada, string estacao)
        {
            string strCmd = "INSERT INTO SPOOL_ENTRADA_NF(ESTACAO, EMP_CODIGO, EST_CODIGO, ENT_ID, ENT_DTLANC, TIP_CODIGO, ENT_DTEMIS, CF_DOCTO, ENT_DOCTO,"
                + " ENT_SERIE, NO_CODIGO, NO_COMP, PAG_DOCTO, ENT_TOTAL, ENT_BASEICMS, ENT_IMPICMS, ENT_BASESUBST, ENT_SUBST, ENT_ISENTOIPI, ENT_OUTIPI,"
                + " ENT_BASEISS, ENT_ISENTOICMS, ENT_DIVERSAS, ENT_FRETE, ENT_IMPFRETE, ENT_DESCONTO, ENT_OUTRAS, ENT_IMPIPI, ENT_BASEIPI, ENT_IRRF, ENT_IMPISS,"
                + " PAG_COMPRADOR, COMP_NUMERO, LOGIN_ID, ENT_STATUS, ENT_OBS, ENT_VIA_XML, DTCADASTRO, OPCADASTRO, CHAVE_ACESSO, DATA_HORA_EMISSAO) VALUES ('"
                + estacao + "',"
                + spoolEntrada.EmpCodigo + ","
                + spoolEntrada.EstCodigo + ","
                + spoolEntrada.EntId + ","
                + Funcoes.BData(Convert.ToDateTime(spoolEntrada.EntDtLanc)) + ","
                + spoolEntrada.TipCodigo + ","
                + Funcoes.BData(Convert.ToDateTime(spoolEntrada.EntDtEmis)) + ",'"
                + spoolEntrada.CfDocto + "','"
                + spoolEntrada.EntDocto + "','"
                + spoolEntrada.EntSerie + "','"
                + spoolEntrada.NoCodigo + "','"
                + spoolEntrada.NoComp + "','"
                + spoolEntrada.PagDocto + "',"
                + Funcoes.BValor(spoolEntrada.EntTotal) + ","
                + Funcoes.BValor(spoolEntrada.EntBaseIcms) + ","
                + Funcoes.BValor(spoolEntrada.EntValorIcms) + ","
                + Funcoes.BValor(spoolEntrada.EntBaseIcmsST) + ","
                + Funcoes.BValor(spoolEntrada.EntValorIcmsSt) + ","
                + Funcoes.BValor(spoolEntrada.EntValorImpImportacao) + ","
                + Funcoes.BValor(spoolEntrada.EntValorIcmsUFRemet) + ","
                + Funcoes.BValor(spoolEntrada.EntValorFCP) + ","
                + Funcoes.BValor(spoolEntrada.EntValorPIS) + ","
                + Funcoes.BValor(spoolEntrada.EntValorProdutos) + ","
                + Funcoes.BValor(spoolEntrada.EntValorFrete) + ","
                + Funcoes.BValor(spoolEntrada.EntValorDesconto) + ","
                + Funcoes.BValor(spoolEntrada.EntValorSeguro) + ","
                + Funcoes.BValor(spoolEntrada.EntOutasDespesas) + ","
                + Funcoes.BValor(spoolEntrada.EntValorIPI) + ","
                + Funcoes.BValor(spoolEntrada.EntValorICMSUFDest) + ","
                + Funcoes.BValor(spoolEntrada.EntTotalTributos) + ","
                + Funcoes.BValor(spoolEntrada.EntValorCofins) + ","
                + spoolEntrada.ColCodigo + ",'"
                + spoolEntrada.CompNumero + "','"
                + spoolEntrada.OpCadastro + "','"
                + spoolEntrada.EntStatus + "','"
                + spoolEntrada.EntObs + "','"
                + spoolEntrada.EntViaXML + "',"
                + Funcoes.BDataHora(spoolEntrada.DtCadastro) + ",'"
                + spoolEntrada.OpCadastro + "','"
                + spoolEntrada.ChaveAcesso + "','"
                + spoolEntrada.DataHoraEmissao + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InsereRegistrosSpoolEntradaDetalhe(EntradaDetalhe spoolEntradaDetalhe, string estacao)
        {
            string strCmd = "INSERT INTO SPOOL_ENTRADA_DETALHES_NF (ESTACAO, EST_CODIGO, EMP_CODIGO, ENT_ID, CF_DOCTO, ENT_DOCTO, ENT_SERIE, ENT_PARCELA, ENT_VALPARC, "
                        + "ENT_VENCTO, PAG_DOCTO, DTCADASTRO, OPCADASTRO) VALUES ('"
                        + estacao + "',"
                        + spoolEntradaDetalhe.EstCodigo + ","
                        + spoolEntradaDetalhe.EmpCodigo + ","
                        + spoolEntradaDetalhe.EntId + ",'"
                        + spoolEntradaDetalhe.CfDocto + "','"
                        + spoolEntradaDetalhe.EntDocto + "','"
                        + spoolEntradaDetalhe.EntSerie + "',"
                        + spoolEntradaDetalhe.EntParcela + ","
                        + Funcoes.BValor(spoolEntradaDetalhe.EntValParc) + ","
                        + Funcoes.BData(spoolEntradaDetalhe.EntVencto) + ",'"
                        + spoolEntradaDetalhe.PagDocto + "',"
                        + Funcoes.BDataHora(spoolEntradaDetalhe.DtCadastro) + ",'"
                        + spoolEntradaDetalhe.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InsereRegistrosSpoolEntradaItens(EntradaItens spoolEntradaItens, string estacao)
        {
            string strCmd = "INSERT INTO SPOOL_ENTRADA_ITENS_NF(ESTACAO,EMP_CODIGO, EST_CODIGO, ENT_ID, CF_DOCTO, ENT_DOCTO, ENT_SERIE, ENT_SEQUENCIA"
                + ", PROD_CODIGO, ITEM_DESCR, ENT_APLICACAO, ENT_QTDESTOQUE, ENT_QTDE, ENT_UNIDADE, ENT_UNITARIO, ENT_DESCONTOITEM"
                + ", ENT_TOTITEM, ENT_MARGEM, ENT_VALOR_VENDA, ENT_LOTE, ENT_ICMS, ENT_ALIQICMS, ENT_VALOR_BC_ICMS"
                + ", ENT_VALORICMS, PROD_CST, ENT_BC_ICMS_ST, ENT_ICMS_ST, ENT_REDUCAOBASEICMS, ENT_ICMS_RET, ENT_ENQ_IPI, ENT_CST_IPI"
                + ", ENT_BASE_IPI, ENT_ALIQIPI, ENT_VALORIPI, ENT_ORIGEM, NCM, PROD_CFOP, ENT_CST_COFINS, ENT_CST_PIS, NO_CODIGO, NO_COMP"
                + ", COMP_NUMERO, ENT_COMISSAO, ENT_DATAVALIDADE, ENT_PRODUTO_NOVO, EST_INDICE, ENT_PRE_COMPRA, PROD_CEST, ENT_PMC, ENT_REGISTRO_MS, DTCADASTRO, OPCADASTRO) VALUES ('"
                + estacao + "',"
                + spoolEntradaItens.EmpCodigo + ","
                + spoolEntradaItens.EstCodigo + ","
                + spoolEntradaItens.EntId + ",'"
                + spoolEntradaItens.CfDocto + "','"
                + spoolEntradaItens.EntDocto + "','"
                + spoolEntradaItens.EntSerie + "',"
                + spoolEntradaItens.EntSequencia + ",'"
                + spoolEntradaItens.ProdCodigo + "','"
                + spoolEntradaItens.ItemDescr + "','"
                + spoolEntradaItens.EntAplicacao + "',"
                + spoolEntradaItens.EntQtdeEstoque + ","
                + spoolEntradaItens.EntQtde + ",'"
                + spoolEntradaItens.EntUnidade + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntUnitario) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntDescontoItem) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntTotalItem) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntMargem) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntValorVenda) + ",'"
                + spoolEntradaItens.EntLote + "','"
                + spoolEntradaItens.EntICMS + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntAliqIcms) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntValorBcIcms) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntValorIcms) + ",'"
                + spoolEntradaItens.ProdCST + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntBcIcmsSt) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntIcmsSt) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntRedBaseIcms) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntIcmsRet) + ",'"
                + spoolEntradaItens.EntEnqIpi + "','"
                + spoolEntradaItens.EntCstIpi + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntBaseIpi) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntAliqIpi) + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntValorIpi) + ","
                + spoolEntradaItens.EntOrigem + ",'"
                + spoolEntradaItens.ProdNCM + "','"
                + spoolEntradaItens.ProdCFOP + "','"
                + spoolEntradaItens.EntCstCofins + "','"
                + spoolEntradaItens.EntCstPis + "','"
                + spoolEntradaItens.NoCodigo.Trim() + "','"
                + spoolEntradaItens.NoComp.Trim() + "','"
                + spoolEntradaItens.CompNumero + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntComissao) + ",";
            if (spoolEntradaItens.EntDataValidade == Convert.ToDateTime("01/01/2001 00:00:00"))
            {
                strCmd += "null,";
            }
            else
            {
                strCmd += Funcoes.BData(spoolEntradaItens.EntDataValidade) + ",";
            }

            strCmd += "'" + spoolEntradaItens.ProdutoNovo + "',"
                + spoolEntradaItens.EstIndice + ","
                + Funcoes.BFormataValor(spoolEntradaItens.EntPreCompra) + ",'"
                + spoolEntradaItens.EntProdCest + "',"
                + Funcoes.BFormataValor(spoolEntradaItens.EntPmc) + ",'"
                + spoolEntradaItens.EntRegistroMS + "',"
                + Funcoes.BDataHora(spoolEntradaItens.DtCadastro) + ",'"
                + spoolEntradaItens.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public List<Entrada> CarregaSpoolEntrada(string estacao, int estCodigo, int empCodigo)
        {
            List<Entrada> listaEntrada = new List<Entrada>();

            using (DataTable table = BancoDados.GetDataTable("SELECT * FROM SPOOL_ENTRADA_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Entrada dEntrada = new Entrada();

                    dEntrada.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    dEntrada.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEntrada.EntId = Convert.ToInt32(row["ENT_ID"]);
                    dEntrada.EntDtLanc = row["ENT_DTLANC"].ToString();
                    dEntrada.TipCodigo = Convert.ToInt32(row["TIP_CODIGO"]);
                    dEntrada.EntDtEmis = row["ENT_DTEMIS"].ToString();
                    dEntrada.CfDocto = row["CF_DOCTO"].ToString();
                    dEntrada.EntDocto = row["ENT_DOCTO"].ToString();
                    dEntrada.EntSerie = row["ENT_SERIE"].ToString();
                    dEntrada.NoCodigo = Convert.ToInt32(row["NO_CODIGO"]);
                    dEntrada.NoComp = Convert.ToInt32(row["NO_COMP"]);
                    dEntrada.PagDocto = row["PAG_DOCTO"].ToString();
                    dEntrada.EntTotal = Convert.ToDouble(row["ENT_TOTAL"]);
                    dEntrada.EntBaseIcms = Convert.ToDouble(row["ENT_BASEICMS"]);
                    dEntrada.EntValorIcms = Convert.ToDouble(row["ENT_IMPICMS"]);
                    dEntrada.EntBaseIcmsST = Convert.ToDouble(row["ENT_BASESUBST"]);
                    dEntrada.EntValorIcmsSt = Convert.ToDouble(row["ENT_SUBST"]);
                    dEntrada.EntValorImpImportacao = Convert.ToDouble(row["ENT_ISENTOIPI"]);
                    dEntrada.EntValorIcmsUFRemet = Convert.ToDouble(row["ENT_OUTIPI"]);
                    dEntrada.EntValorFCP = Convert.ToDouble(row["ENT_BASEISS"]);
                    dEntrada.EntValorPIS = Convert.ToDouble(row["ENT_ISENTOICMS"]);
                    dEntrada.EntValorProdutos = Convert.ToDouble(row["ENT_DIVERSAS"]);
                    dEntrada.EntValorFrete = Convert.ToDouble(row["ENT_FRETE"]);
                    dEntrada.EntValorSeguro = Convert.ToDouble(row["ENT_IMPFRETE"]);
                    dEntrada.EntValorDesconto = Convert.ToDouble(row["ENT_DESCONTO"]);
                    dEntrada.EntOutasDespesas = Convert.ToDouble(row["ENT_OUTRAS"]);
                    dEntrada.EntValorIPI = Convert.ToDouble(row["ENT_IMPIPI"]);
                    dEntrada.EntValorICMSUFDest = Convert.ToDouble(row["ENT_BASEIPI"]);
                    dEntrada.EntTotalTributos = Convert.ToDouble(row["ENT_IRRF"]);
                    dEntrada.EntValorCofins = Convert.ToDouble(row["ENT_IMPISS"]);
                    dEntrada.ColCodigo = Convert.ToInt32(row["PAG_COMPRADOR"]);
                    dEntrada.CompNumero = row["COMP_NUMERO"].ToString();
                    dEntrada.OpCadastro = row["OPCADASTRO"].ToString();
                    dEntrada.EntStatus = row["ENT_STATUS"].ToString();
                    dEntrada.EntObs = row["ENT_OBS"].ToString();
                    dEntrada.EntViaXML = row["ENT_VIA_XML"].ToString();
                    dEntrada.DtCadastro = Convert.ToDateTime(row["DTCADASTRO"]);
                    dEntrada.OpCadastro = row["OPCADASTRO"].ToString();
                    dEntrada.ChaveAcesso = row["CHAVE_ACESSO"].ToString();
                    listaEntrada.Add(dEntrada);
                }
            }

            return listaEntrada;
        }

        public List<EntradaDetalhe> CarregaSpoolEntradasDetalhe(string estacao, int estCodigo, int empCodigo)
        {
            List<EntradaDetalhe> listaEntradaDetalhe = new List<EntradaDetalhe>();

            using (DataTable table = BancoDados.GetDataTable("SELECT * FROM SPOOL_ENTRADA_DETALHES_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    EntradaDetalhe dEntradaDetalhe = new EntradaDetalhe();
                    dEntradaDetalhe.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    dEntradaDetalhe.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEntradaDetalhe.EntId = Convert.ToInt32(row["ENT_ID"]);
                    dEntradaDetalhe.CfDocto = row["CF_DOCTO"].ToString();
                    dEntradaDetalhe.EntDocto = row["ENT_DOCTO"].ToString();
                    dEntradaDetalhe.EntSerie = row["ENT_SERIE"].ToString();
                    dEntradaDetalhe.EntParcela = Convert.ToInt32(row["ENT_PARCELA"]);
                    dEntradaDetalhe.EntValParc = Convert.ToDouble(row["ENT_VALPARC"]);
                    dEntradaDetalhe.EntVencto = Convert.ToDateTime(row["ENT_VENCTO"]);
                    dEntradaDetalhe.PagDocto = row["PAG_DOCTO"].ToString();
                    dEntradaDetalhe.DtCadastro = Convert.ToDateTime(row["DTCADASTRO"]);
                    dEntradaDetalhe.OpCadastro = row["OPCADASTRO"].ToString();
                    listaEntradaDetalhe.Add(dEntradaDetalhe);
                }
            }

            return listaEntradaDetalhe;
        }

        public List<EntradaItens> CarregaSpoolEntradasItens(string estacao, int estCodigo, int empCodigo)
        {
            List<EntradaItens> listaEntradaItens = new List<EntradaItens>();

            using (DataTable table = BancoDados.GetDataTable("SELECT * FROM SPOOL_ENTRADA_ITENS_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " ORDER BY ENT_SEQUENCIA", null))
            {
                foreach (DataRow row in table.Rows)
                {
                    EntradaItens dEntradaItens = new EntradaItens();
                    dEntradaItens.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    dEntradaItens.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEntradaItens.EntId = Convert.ToInt32(row["ENT_ID"]);
                    dEntradaItens.CfDocto = row["CF_DOCTO"].ToString();
                    dEntradaItens.EntDocto = row["ENT_DOCTO"].ToString();
                    dEntradaItens.EntSerie = row["ENT_SERIE"].ToString();
                    dEntradaItens.EntSequencia = Convert.ToInt32(row["ENT_SEQUENCIA"]);
                    dEntradaItens.ProdCodigo = row["PROD_CODIGO"].ToString();
                    dEntradaItens.ItemDescr = row["ITEM_DESCR"].ToString();
                    dEntradaItens.EntAplicacao = row["ENT_APLICACAO"].ToString();
                    dEntradaItens.EntQtdeEstoque = Convert.ToInt32(row["ENT_QTDESTOQUE"]);
                    dEntradaItens.EntQtde = Convert.ToInt32(row["ENT_QTDE"]);
                    dEntradaItens.EntUnidade = row["ENT_UNIDADE"].ToString();
                    dEntradaItens.EntUnitario = Convert.ToDouble(row["ENT_UNITARIO"]);
                    dEntradaItens.EntDescontoItem = Convert.ToDouble(row["ENT_DESCONTOITEM"]);
                    dEntradaItens.EntTotalItem = Convert.ToDouble(row["ENT_TOTITEM"]);
                    dEntradaItens.EntMargem = Convert.ToDouble(row["ENT_MARGEM"]);
                    dEntradaItens.EntValorVenda = Convert.ToDouble(row["ENT_VALOR_VENDA"]);
                    dEntradaItens.EntLote = row["ENT_LOTE"].ToString();
                    dEntradaItens.EntICMS = row["ENT_ICMS"].ToString();
                    dEntradaItens.EntAliqIcms = Convert.ToDouble(row["ENT_ALIQICMS"]);
                    dEntradaItens.EntValorBcIcms = Convert.ToDouble(row["ENT_VALOR_BC_ICMS"]);
                    dEntradaItens.EntValorIcms = Convert.ToDouble(row["ENT_VALORICMS"]);
                    dEntradaItens.ProdCST = row["PROD_CST"].ToString();
                    dEntradaItens.EntBcIcmsSt = Convert.ToDouble(row["ENT_BC_ICMS_ST"]);
                    dEntradaItens.EntIcmsSt = Convert.ToDouble(row["ENT_ICMS_ST"]);
                    dEntradaItens.EntRedBaseIcms = Convert.ToDouble(row["ENT_REDUCAOBASEICMS"]);
                    dEntradaItens.EntIcmsRet = Convert.ToDouble(row["ENT_ICMS_RET"]);
                    dEntradaItens.EntEnqIpi = row["ENT_ENQ_IPI"].ToString();
                    dEntradaItens.EntCstIpi = row["ENT_CST_IPI"].ToString();
                    dEntradaItens.EntBaseIpi = Convert.ToDouble(row["ENT_BASE_IPI"]);
                    dEntradaItens.EntAliqIpi = Convert.ToDouble(row["ENT_ALIQIPI"]);
                    dEntradaItens.EntValorIpi = Convert.ToDouble(row["ENT_VALORIPI"]);
                    dEntradaItens.EntOrigem = Convert.ToInt32(row["ENT_ORIGEM"]);
                    dEntradaItens.ProdNCM = row["NCM"].ToString();
                    dEntradaItens.ProdCFOP = row["PROD_CFOP"].ToString();
                    dEntradaItens.EntCstCofins = row["ENT_CST_COFINS"].ToString();
                    dEntradaItens.EntCstPis = row["ENT_CST_PIS"].ToString();
                    dEntradaItens.NoCodigo = row["NO_CODIGO"].ToString();
                    dEntradaItens.NoComp = row["NO_COMP"].ToString();
                    dEntradaItens.CompNumero = row["COMP_NUMERO"].ToString();
                    dEntradaItens.EntComissao = Convert.ToDouble(row["ENT_COMISSAO"]);
                    dEntradaItens.EntDataValidade = row["ENT_DATAVALIDADE"].ToString() == "" ? Convert.ToDateTime("01/01/2001 00:00:00") : Convert.ToDateTime(row["ENT_DATAVALIDADE"]);
                    dEntradaItens.ProdutoNovo = row["ENT_PRODUTO_NOVO"].ToString();
                    dEntradaItens.EstIndice = Convert.ToInt32(row["EST_INDICE"]);
                    dEntradaItens.EntPreCompra = Convert.ToDouble(row["ENT_PRE_COMPRA"]);
                    dEntradaItens.EntProdCest = row["PROD_CEST"].ToString();
                    dEntradaItens.EntPmc = row["ENT_PMC"].ToString() == "" ? 0 : Convert.ToDouble(row["ENT_PMC"]);
                    dEntradaItens.EntRegistroMS = row["ENT_REGISTRO_MS"].ToString();
                    dEntradaItens.DtCadastro = Convert.ToDateTime(row["DTCADASTRO"]);
                    dEntradaItens.OpCadastro = row["OPCADASTRO"].ToString();
                    listaEntradaItens.Add(dEntradaItens);
                }
            }

            return listaEntradaItens;
        }

        public void ExcluiSpoolEntrada(string estacao, int estCodigo, int empCodigo)
        {
            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_ENTRADA_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null);
        }

        public void ExcluiSpoolEntradasDetalhe(string estacao, int estCodigo, int empCodigo)
        {
            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_ENTRADA_DETALHES_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo, null);
        }

        public void ExcluiSpoolEntradasItens(string estacao, int estCodigo, int empCodigo, int entradaID = 0, string prod_codigo = "")
        {
            string strCmd = "DELETE FROM SPOOL_ENTRADA_ITENS_NF WHERE ESTACAO = '" + estacao + "' AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo;
            if (!entradaID.Equals(0))
            {
                strCmd += "  AND ENT_ID = " + entradaID;
            }
            if (!String.IsNullOrEmpty(prod_codigo))
            {
                strCmd += " AND PROD_CODIGO = '" + prod_codigo + "'";
            }
            BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public int AtualizaSequenciaSpoolEntradaItens(EntradaItens entItens, string estacao)
        {
            string strCmd = "UPDATE SPOOL_ENTRADA_ITENS_NF SET ENT_SEQUENCIA = " + entItens.EntSequencia + ", DTALTERACAO = " + Funcoes.BDataHora(entItens.DtAlteracao)
                          + ", OPALTERACAO = '" + entItens.OpAlteracao + "' WHERE EST_CODIGO = " + entItens.EstCodigo + " AND EMP_CODIGO = " + entItens.EmpCodigo
                          + " AND ENT_ID = " + entItens.EntId + " AND PROD_CODIGO = '" + entItens.ProdCodigo + "' AND ESTACAO = '" + estacao + "'";
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }


    }
}
