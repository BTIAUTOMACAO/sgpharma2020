﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    class LogUltimoCusto
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public string DtCadastro { get; set; }
        public double UltimoCustoAtual { get; set; }
        public double UltimoCustoNovo { get; set; }
        public string OpCadastro { get; set; }
        public string Observacao { get; set; }
        
        public LogUltimoCusto() { }
     
        public LogUltimoCusto(int empCodigo, int estCodigo, string prodCodigo, string dtCadastro, double ultimoCustoAtual, double ultimoCustoNovo, string opCadastro, string observacao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ProdCodigo = prodCodigo;
            this.DtCadastro = dtCadastro;
            this.UltimoCustoAtual = ultimoCustoAtual;
            this.UltimoCustoNovo = ultimoCustoNovo;
            this.OpCadastro = opCadastro;
            this.Observacao = observacao;
        }

        public bool InsereRegistros(LogUltimoCusto dados)
        {
            string strCmd = "INSERT INTO LOG_ALTERA_ULTIMO_CUSTO(EMP_CODIGO, EST_CODIGO, PROD_CODIGO, DT_CADASTRO, ULTIMO_CUSTO_ANTIGO, ULTIMO_CUSTO_NOVO, USUARIO, OBSERVACAO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ",'"
                + dados.ProdCodigo + "',"
                + Funcoes.BDataHora(Convert.ToDateTime(dados.DtCadastro)) + ","
                + Funcoes.BFormataValor(dados.UltimoCustoAtual) + ","
                + Funcoes.BFormataValor(dados.UltimoCustoNovo) + ",'"
                + dados.OpCadastro + "','"
                + dados.Observacao + "')";

            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDados(LogUltimoCusto dados)
        {
            string strSql;

            strSql = "SELECT A.EMP_CODIGO, A.EST_CODIGO, A.DT_CADASTRO, A.USUARIO, A.PROD_CODIGO, B.PROD_DESCR, A.ULTIMO_CUSTO_ANTIGO, A.ULTIMO_CUSTO_NOVO, A.OBSERVACAO "
                     + " FROM LOG_ALTERA_ULTIMO_CUSTO A "
                     + " INNER JOIN PRODUTOS B ON (A.PROD_CODIGO = B.PROD_CODIGO)"
                     + " WHERE A.EMP_CODIGO = " + dados.EmpCodigo + " AND A.EST_CODIGO = " + dados.EstCodigo;
            if (!String.IsNullOrEmpty(dados.ProdCodigo))
            {
                strSql += " AND A.PROD_CODIGO = '" + dados.ProdCodigo + "'";
            }

            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(DtCadastro)))
            {
                strSql += " AND A.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(DtCadastro + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(DtCadastro + " 23:59:59"));
            }

            if (!String.IsNullOrEmpty(dados.OpCadastro))
            {
                strSql += " AND A.USUARIO = '" + dados.OpCadastro + "'";
            }
            strSql += "  ORDER BY A.PROD_CODIGO,A.DT_CADASTRO";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
