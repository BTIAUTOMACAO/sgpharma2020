﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ReceberCusto
    {
        public int EmpCodigo { get; set; }
        public int RecID { get; set; }
        public string RecDocto { get; set; }
        public int RccSeq { get; set; }
        public int PcCodigo { get; set; }
        public int CprNum { get; set; }
        public double RccValor { get; set; }

        public ReceberCusto() { }

    }
}
