﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class VendasItens
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaID { get; set; }
        public int VendaItem { get; set; }
        public int ProdID { get; set; }
        public string ProdCodigo { get; set; }
        public int VendaItemQtde { get; set; }
        public double VendaItemUnitario { get; set; }
        public double VendaItemSubTotal { get; set; }
        public double VendaItemDiferenca { get; set; }
        public double VendaItemTotal { get; set; }
        public string VendaItemUnidade { get; set; }
        public double VendaPreValor { get; set; }
        public string VendaPromocao { get; set; }
        public string VendaDescLiberado { get; set; }
        public string VendaItemLote { get; set; }
        public string VendaItemReceita { get; set; }
        public long VendaItemIndice { get; set; }
        public double VendaItemComissao { get; set; }
        public int ColCodigo { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }

        public VendasItens() { }

        public bool ExcluirDados(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "DELETE FROM VENDAS_ITENS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND VENDA_ID = " + vendaID;

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public string IdentificaSeInsereOuAtualiza(int empCodigo, int estCodigo, string prodCodigo, long vendaID)
        {
            string sql = "SELECT PROD_CODIGO FROM VENDAS_ITENS  WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo + " AND VENDA_ID = " + vendaID + " AND PROD_CODIGO = '" + prodCodigo + "'";

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public bool InserirItensPreVenda(VendasItens dados, bool troca = false, string colCodigo = "0")
        {
            string strCmd;
            
            if (String.IsNullOrEmpty(IdentificaSeInsereOuAtualiza(dados.EmpCodigo, dados.EstCodigo, dados.ProdCodigo, dados.VendaID)))
            {
                strCmd = "INSERT INTO VENDAS_ITENS(EMP_CODIGO,EST_CODIGO,VENDA_ID,VENDA_ITEM,PROD_ID,PROD_CODIGO,VENDA_ITEM_QTDE,VENDA_ITEM_UNITARIO,VENDA_ITEM_SUBTOTAL,"
                + "VENDA_ITEM_DIFERENCA,VENDA_ITEM_TOTAL,VENDA_ITEM_UNIDADE,VENDA_PRE_VALOR,VENDA_PROMOCAO,VENDA_DESC_LIBERADO,VENDA_ITEM_LOTE,VENDA_ITEM_RECEITA,"
                + "VENDA_ITEM_INDICE, VENDA_ITEM_COMISSAO, COL_CODIGO, OP_CADASTRO,DT_CADASTRO) "
                + "VALUES (" + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.VendaID + ","
                + dados.VendaItem + ","
                + dados.ProdID + ",'"
                + dados.ProdCodigo + "',"
                + dados.VendaItemQtde + ","
                + Funcoes.BValor(dados.VendaItemUnitario) + ","
                + Funcoes.BValor(dados.VendaItemSubTotal) + ","
                + Funcoes.BValor(dados.VendaItemDiferenca) + ","
                + Funcoes.BValor(dados.VendaItemTotal) + ",'"
                + dados.VendaItemUnidade + "',"
                + Funcoes.BValor(dados.VendaPreValor) + ",'"
                + dados.VendaPromocao + "','"
                + dados.VendaDescLiberado + "',";
                if (!String.IsNullOrEmpty(dados.VendaItemLote))
                {
                    strCmd += "'" + dados.VendaItemLote + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.VendaItemReceita))
                {
                    strCmd += "'" + dados.VendaItemReceita + "',";
                }
                else
                    strCmd += "null,";

                strCmd += dados.VendaItemIndice + ","
                    + Funcoes.BValor(dados.VendaItemComissao) + ",";
                if(Funcoes.LeParametro(6,"383", false).Equals("S"))
                {
                    strCmd += colCodigo + ",";
                }
                else
                {
                    strCmd += dados.ColCodigo + ",";
                }
                strCmd += "'" + dados.OpCadastro + "',"
                    + Funcoes.BDataHora(dados.DtCadastro) + ")";
              
            }
            else
            {
                strCmd = "UPDATE VENDAS_ITENS SET VENDA_ITEM_QTDE = " + dados.VendaItemQtde + ", VENDA_ITEM_UNITARIO = " + Funcoes.BValor(dados.VendaItemUnitario) + ","
                    + " VENDA_ITEM_SUBTOTAL = " + Funcoes.BValor(dados.VendaItemSubTotal) + ", VENDA_ITEM_DIFERENCA = " + Funcoes.BValor(dados.VendaItemDiferenca) + ","
                    + " VENDA_ITEM_TOTAL = " + Funcoes.BValor(dados.VendaItemTotal) + ", VENDA_ITEM_UNIDADE = '" + dados.VendaItemUnidade + "', VENDA_PRE_VALOR = "
                    + Funcoes.BValor(dados.VendaPreValor) + ", VENDA_PROMOCAO = '" + dados.VendaPromocao + "', VENDA_DESC_LIBERADO = '" + dados.VendaDescLiberado + "',";

                if (!String.IsNullOrEmpty(dados.VendaItemLote))
                {
                    strCmd += "VENDA_ITEM_LOTE = '" + dados.VendaItemLote + "',";
                }
                else
                    strCmd += "VENDA_ITEM_LOTE = null,";

                if (!String.IsNullOrEmpty(dados.VendaItemReceita))
                {
                    strCmd += "VENDA_ITEM_RECEITA = '" + dados.VendaItemReceita + "',";
                }
                else
                    strCmd += "VENDA_ITEM_RECEITA = null,";

                if(troca)
                {
                    strCmd += " COL_CODIGO = " + dados.ColCodigo + ",";
                }
                
                strCmd += " VENDA_ITEM_INDICE = " + dados.VendaItemIndice + ", VENDA_ITEM_COMISSAO = " + Funcoes.BValor(dados.VendaItemComissao) + ", OP_ALTERACAO = '" + dados.OpCadastro + "',"
                    + " DT_ALTERACAO = " + Funcoes.BDataHora(dados.DtCadastro) + " WHERE EMP_CODIGO = " + dados.EmpCodigo + " AND EST_CODIGO = " + dados.EstCodigo + " AND VENDA_ID = "
                    + dados.VendaID + " AND PROD_CODIGO = '" + dados.ProdCodigo + "'";
            }

            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaItensDaVendaPorID(long vendaID, int estCodigo, int empCodigo)
        {
            string strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.VENDA_ID, A.VENDA_ITEM, A.PROD_ID, A.PROD_CODIGO, A.VENDA_ITEM_QTDE, A.VENDA_ITEM_UNITARIO, A.VENDA_ITEM_SUBTOTAL,"
                    + " A.VENDA_ITEM_DIFERENCA, A.VENDA_ITEM_TOTAL, A.VENDA_ITEM_UNIDADE, A.VENDA_PRE_VALOR, A.VENDA_PROMOCAO, A.VENDA_DESC_LIBERADO, A.VENDA_ITEM_LOTE, A.VENDA_ITEM_RECEITA,"
                    + " B.PROD_DESCR, B.PROD_CONTROLADO, COALESCE(C.PROD_ESTATUAL,0) AS PROD_ESTATUAL, COALESCE(C.PROD_ULTCUSME,0) AS PROD_ULTCUSME, COALESCE(C.PROD_CUSME,0) AS PROD_CUSME,"
                    + " B.NCM, COALESCE(C.PROD_ECF,'FF') AS PROD_ECF, COALESCE(G.ALIQ_NAC, 0) AS ALIQ_NAC, COALESCE(VENDA_ITEM_COMISSAO, 0) AS VENDA_ITEM_COMISSAO, H.VENDA_COL_CODIGO, COALESCE(C.DEP_CODIGO,0) AS DEP_CODIGO,"
                    + " COALESCE(C.CLAS_CODIGO,0) AS CLAS_CODIGO, COALESCE(C.SUB_CODIGO,0) AS SUB_CODIGO"
                    + " FROM VENDAS_ITENS A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_ID = B.PROD_ID"
                    + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_ID = C.PROD_ID"
                    + " LEFT JOIN DEPARTAMENTOS D ON (C.DEP_CODIGO = D.DEP_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO)"
                    + " LEFT JOIN CLASSES E ON (E.CLAS_CODIGO = C.CLAS_CODIGO AND A.EMP_CODIGO = E.EMP_CODIGO)"
                    + " LEFT JOIN SUBCLASSES F ON (F.SUB_CODIGO = C.SUB_CODIGO AND A.EMP_CODIGO = F.EMP_CODIGO)"
                    + " LEFT JOIN TABELA_IBPT G ON G.NCM = B.NCM"
                    + " INNER JOIN VENDAS H ON A.VENDA_ID = H.VENDA_ID"
                    + " WHERE A.EMP_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = " + empCodigo
                    + " AND A.VENDA_ID = " + vendaID
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = H.EST_CODIGO"
                    + " AND A.EMP_CODIGO = H.EMP_CODIGO"
                    + " ORDER BY A.VENDA_ITEM";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaItensDaVendaParaCancelamento(int estCodigo, int empCodigo, long vendaID)
        {
            string strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.VENDA_ITEM, A.PROD_ID, A.PROD_CODIGO, A.VENDA_ITEM_QTDE, COALESCE(C.PROD_ESTATUAL,0) AS PROD_ESTATUAL,"
                    + " A.VENDA_ITEM_INDICE "
                    + " FROM VENDAS_ITENS A"
                    + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_ID = C.PROD_ID"
                    + " WHERE A.EMP_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = " + empCodigo
                    + " AND A.VENDA_ID = " + vendaID
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " ORDER BY A.VENDA_ITEM";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaItensControleDeEntrega(int estCodigo, int empCodigo, long vendaID)
        {
            string strSql = "SELECT  A.VENDA_ITEM, A.VENDA_ITEM_QTDE, B.PROD_DESCR, A.PROD_CODIGO"
                    + " FROM VENDAS_ITENS A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_ID = B.PROD_ID"
                    + " WHERE A.EMP_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = " + empCodigo
                    + " AND A.VENDA_ID = " + vendaID
                    + " ORDER BY A.VENDA_ITEM";

            return BancoDados.GetDataTable(strSql, null);
        }

        public int NumeroVendasPorPeriodo(DateTime dtInicial, DateTime dtFinal)
        {
            string sql = "SELECT COUNT(*) FROM VENDAS WHERE VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(dtInicial)
                       + " AND " + Funcoes.BDataHora(dtFinal);
            var retorno = BancoDados.ExecuteScalar(sql, null);
            return Convert.ToInt32(retorno);
        }
        public int NumeroEntregasPorPeriodo(DateTime dtInicial, DateTime dtFinal, char status)
        {
            string sql = " SELECT COUNT(VENDA_ENTREGA) FROM VENDAS WHERE VENDA_ENTREGA = '"+ status + "' AND VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(dtInicial)
                       + " AND " + Funcoes.BDataHora(dtFinal);
            var retorno = BancoDados.ExecuteScalar(sql, null);
            return Convert.ToInt32(retorno);
        }
        public int NumeroCuponsPorPeriodo(DateTime dtInicial, DateTime dtFinal, char status)
        {
            string sql = " SELECT COUNT(VENDA_EMITIDO) FROM VENDAS WHERE VENDA_EMITIDO = '" + status + "' AND VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(dtInicial)
                       + " AND " + Funcoes.BDataHora(dtFinal);
            var retorno = BancoDados.ExecuteScalar(sql, null);
            return Convert.ToInt32(retorno);
        }

        public bool ExcluirProdutos(int estCodigo, int empCodigo, long vendaID, string prodCodigo, bool transAberta = false)
        {
            string strCmd = "DELETE FROM VENDAS_ITENS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND VENDA_ID = " + vendaID + " AND PROD_CODIGO = '" + prodCodigo + "'";

            if (transAberta)
            {
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public int BuscaMaxVendaItem(int empCodigo, int estCodigo, long vendaID)
        {
            string sql = " SELECT COALESCE(MAX(VENDA_ITEM),0) + 1 AS VENDA_ITEM FROM VENDAS_ITENS WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo
                       + " AND VENDA_ID = " + vendaID;
            var retorno = BancoDados.ExecuteScalar(sql, null);
            return Convert.ToInt32(retorno);
        }

        public bool AtualizaProdutoTroca(long vendaID, int empCodigo, int estCodigo, string prodCodigo, int qtde, double valorUnitario, double desconto, double valorTotal)
        {
            string strCmd = "UPDATE VENDAS_ITENS SET VENDA_ITEM_QTDE = " + qtde + " , VENDA_ITEM_SUBTOTAL = " + Funcoes.BValor(valorUnitario * qtde) + ", VENDA_ITEM_DIFERENCA = ";
            
            if(desconto > 0)
            {
                strCmd += " (VENDA_ITEM_DIFERENCA / VENDA_ITEM_QTDE) * " + qtde + ",";
            }
            else
            {
                strCmd += "0,";
            }

            strCmd += " VENDA_ITEM_TOTAL =  " + Math.Round(Convert.ToDecimal((valorUnitario * qtde)) - Convert.ToDecimal(((valorUnitario * qtde) * desconto) / 100),2).ToString().Replace(",",".")
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID + " AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaCodBarrasPorVendaID(int estCodigo, int empCodigo, long vendaID)
        {
            string strSql = "SELECT  A.PROD_CODIGO"
                    + " FROM VENDAS_ITENS A"
                    + " WHERE A.EMP_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = " + empCodigo
                    + " AND A.VENDA_ID = " + vendaID;

            return BancoDados.GetDataTable(strSql, null);
        }

        public string IdentificaSeCodBarrasPorID(int estCodigo, int empCodigo, long vendaID, string prodCodigo)
        {
            string strSql = "SELECT  A.PROD_CODIGO"
                  + " FROM VENDAS_ITENS A"
                  + " WHERE A.EMP_CODIGO = " + estCodigo
                  + " AND A.EST_CODIGO = " + empCodigo
                  + " AND A.VENDA_ID = " + vendaID
                  + " AND A.PROD_CODIGO = '" + prodCodigo + "'";

            var r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public bool AtualizaIndiceEntrega(long vendaID, int empCodigo, int estCodigo, string prodCodigo, long indice)
        {
            string strCmd = "UPDATE VENDAS_ITENS SET VENDA_ITEM_INDICE = " + indice + ", OP_ALTERACAO = '" + Principal.usuario + "', DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID + " AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaProdutosNFE(int estCodigo, int empCodigo, string vendaID, string cliId = "", string dtInicial = "", string dtFinal = "", bool descrPrincipio = false)
        {
            string strSql;
            if (!vendaID.Equals("0"))
            {
                if (!descrPrincipio)
                {
                    strSql = "SELECT A.PROD_CODIGO, C.PROD_CFOP, B.PROD_DESCR, B.NCM, C.PROD_CEST, A.VENDA_ITEM_UNIDADE, A.VENDA_ITEM_QTDE, A.VENDA_ITEM_UNITARIO,"
                                    + "    A.VENDA_ITEM_SUBTOTAL, A.VENDA_ITEM_DIFERENCA, C.PROD_LISTA, B.PROD_REGISTRO_MS, C.PROD_CFOP_DEVOLUCAO, C.PROD_CST"
                                    + "    FROM VENDAS_ITENS A"
                                    + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                                    + "    INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                                    + "    WHERE A.EMP_CODIGO = " + empCodigo
                                    + "    AND A.EST_CODIGO = " + estCodigo
                                    + "    AND A.VENDA_ID = " + vendaID
                                    + "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                                    + "    AND A.EST_CODIGO = C.EST_CODIGO";
                }
                else
                {
                    strSql = "SELECT A.PROD_CODIGO, C.PROD_CFOP, COALESCE(D.PRI_DESCRICAO,B.PROD_DESCR) AS PROD_DESCR, B.NCM, C.PROD_CEST, A.VENDA_ITEM_UNIDADE, A.VENDA_ITEM_QTDE, A.VENDA_ITEM_UNITARIO,"
                                   + "    A.VENDA_ITEM_SUBTOTAL, A.VENDA_ITEM_DIFERENCA, C.PROD_LISTA, B.PROD_REGISTRO_MS, C.PROD_CFOP_DEVOLUCAO, C.PROD_CST"
                                   + "    FROM VENDAS_ITENS A"
                                   + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                                   + "    INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                                   + "    LEFT JOIN PRINCIPIO_ATIVO D ON (B.PRI_CODIGO = D.PRI_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO)"
                                   + "    WHERE A.EMP_CODIGO = " + empCodigo
                                   + "    AND A.EST_CODIGO = " + estCodigo
                                   + "    AND A.VENDA_ID = " + vendaID
                                   + "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                                   + "    AND A.EST_CODIGO = C.EST_CODIGO";
                }
            }
            else
            {
                strSql = " SELECT A.PROD_CODIGO, C.PROD_CFOP, B.PROD_DESCR, B.NCM, C.PROD_CEST, A.VENDA_ITEM_UNIDADE, SUM(A.VENDA_ITEM_QTDE) AS VENDA_ITEM_QTDE, A.VENDA_ITEM_UNITARIO,"
                            + "    SUM(A.VENDA_ITEM_SUBTOTAL) AS VENDA_ITEM_SUBTOTAL, SUM(A.VENDA_ITEM_DIFERENCA) AS VENDA_ITEM_DIFERENCA, C.PROD_LISTA, B.PROD_REGISTRO_MS, C.PROD_CFOP_DEVOLUCAO, C.PROD_CST"
                            + "    FROM VENDAS_ITENS A"
                            + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "    INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                            + "    INNER JOIN VENDAS D ON A.VENDA_ID = D.VENDA_ID"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = C.EST_CODIGO"
                            + "    AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = D.EST_CODIGO"
                            + "    AND D.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal + " 00:00:00"))
                            + "    AND D.VENDA_STATUS = 'F'"
                            + "    AND D.VENDA_CF_ID = " + cliId
                            + "    GROUP BY A.PROD_CODIGO,"
                            + "    C.PROD_CFOP,"
                            + "    B.PROD_DESCR,"
                            + "    B.NCM,"
                            + "    C.PROD_CEST,"
                            + "    A.VENDA_ITEM_UNIDADE,"
                            + "    C.PROD_LISTA,"
                            + "    B.PROD_REGISTRO_MS,"
                            + "    C.PROD_CFOP_DEVOLUCAO,"
                            + "    C.PROD_CST,"
                            + "    A.VENDA_ITEM_UNITARIO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }



        public DataTable BuscaProdutosNFeDevolucao(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql;

            strSql = "SELECT B.PROD_CODIGO, C.PROD_CFOP, B.PROD_DESCR, B.NCM, C.PROD_CEST,B.PROD_UNIDADE,"
                                + "    C.PROD_LISTA, B.PROD_REGISTRO_MS, C.PROD_CFOP_DEVOLUCAO, C.PROD_CST, D.MED_PCO18"
                                + "    FROM PRODUTOS B"
                                + "    INNER JOIN PRODUTOS_DETALHE C ON B.PROD_CODIGO = C.PROD_CODIGO"
                                + "    INNER JOIN PRECOS D ON B.PROD_CODIGO = D.PROD_CODIGO"
                                + "    WHERE C.EMP_CODIGO = " + empCodigo
                                + "    AND C.EST_CODIGO = " + estCodigo
                                + "    AND D.EMP_CODIGO = C.EMP_CODIGO"
                                + "    AND D.EST_CODIGO = D.EST_CODIGO"
                                + "    AND B.PROD_CODIGO IN ('" + prodCodigo + "')";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosPorVendaId(int estCodigo, int empCodigo, string vendaID)
        {
            string strSql;

            strSql = " SELECT A.PROD_CODIGO, "
                    + "        B.PROD_DESCR, "
                    + "        A.VENDA_ITEM_QTDE, "
                    + "        A.VENDA_ITEM_UNITARIO, "
                    + "        A.VENDA_ITEM_SUBTOTAL, "
                    + "        A.VENDA_ITEM_DIFERENCA, "
                    + "        A.VENDA_ITEM_TOTAL "
                    + "    FROM VENDAS_ITENS A "
                    + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO "
                    + "    WHERE VENDA_ID = " + vendaID
                    + "    AND A.EMP_CODIGO = " + empCodigo
                    + "    AND A.EST_CODIGO = " + estCodigo;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendaConveniadPorID(string vendaID, int estCodigo, int empCodigo, string dataInicial, string dataFinal)
        {
            string strSql;

            strSql = " SELECT VENDA_NOME_CARTAO, VENDA_TOTAL, VENDA_ID FROM VENDAS "
                    + "    WHERE VENDA_CON_CODIGO = '" + vendaID + "'"
                    + "    AND EMP_CODIGO = " + empCodigo
                    + "    AND EST_CODIGO = " + estCodigo
                    + "    AND VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));


            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendaConveniadPorEmissao(string vendaID, int estCodigo, int empCodigo, string dataInicial, string dataFinal)
        {
            string strSql;

            strSql = " SELECT SUM(VENDA_TOTAL) AS TOTAL, VENDA_EMISSAO FROM VENDAS "
                    + "    WHERE VENDA_CON_CODIGO = '" + vendaID + "'"
                    + "    AND EMP_CODIGO = " + empCodigo
                    + "    AND EST_CODIGO = " + estCodigo
                    + "    AND VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal))
                    + "    GROUP BY VENDA_EMISSAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaItens(string id, int empCodigo, int estCodigo)
        {
            string sql = "SELECT B.PROD_CODIGO,"
                    + "       C.PROD_DESCR,"
                    + "       B.VENDA_ITEM_QTDE AS QTDE,"
                    + "       B.VENDA_ITEM_UNITARIO AS PRECO"
                    + "  FROM VENDAS A"
                    + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                    + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + "   AND A.EST_CODIGO = " + estCodigo
                    + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                    + "   AND A.EST_CODIGO = B.EST_CODIGO"
                    + "   AND A.VENDA_ID = " + id;
            return BancoDados.GetDataTable(sql, null);
        }
    }
    
}
