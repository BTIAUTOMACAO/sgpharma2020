﻿using System;

namespace GestaoAdministrativa.Negocio
{
    public class ReceberMovto
    {
        public int EstCodigo { get; set; }
        public int RmCodigo { get; set; }
        public int RecCodigo { get; set; }
        public int RdParcela { get; set; }
        public string RmOperacao { get; set; }
        public DateTime RmData { get; set; }
        public decimal RmValor { get; set; }
        public string RmHistorico { get; set; }
        public int RmTipoPagto { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }
        public int MovfinSeq { get; set; }

        public ReceberMovto() { }
    }
}
