﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Desconto
    {
        public int EmpCodigo { get; set; }
        public int DescCodigo { get; set; }
        public string DescDescr { get; set; }
        public int ConId { get; set; }
        public int ProdID { get; set; }
        public int DepCodigo { get; set; }
        public int ClasCodigo { get; set; }
        public int SubCodigo { get; set; }
        public double DescMin { get; set; }
        public double DescMax { get; set; }
        public string DescLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Desconto() { }

        public Desconto(int estCodigo, int descCodigo, string descDescr, int condID, int prodID, int depCodigo, int clasCodigo, int subCodigo,
            double descMin, double descMax, string descLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = estCodigo;
            this.DescCodigo = descCodigo;
            this.DescDescr = descDescr;
            this.ConId = condID;
            this.ProdID = prodID;
            this.DepCodigo = depCodigo;
            this.ClasCodigo = clasCodigo;
            this.SubCodigo = subCodigo;
            this.DescMin = descMin;
            this.DescMax = descMax;
            this.DescLiberado = descLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public DataTable BuscaDados(int empCodigo, string prodID, string descricao, string liberado, out string strOrdem)
        {
            string strSql = "  SELECT A.EMP_CODIGO, A.DESC_CODIGO, A.DESC_DESCR, B.CON_NOME, C.PROD_CODIGO,";
            strSql += " D.DEP_DESCR, E.CLAS_DESCR, F.SUB_DESCR, A.DESC_MIN, A.DESC_MAX,";
            strSql += " CASE WHEN A.DESC_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS DESC_DESABILITADO, A.DTALTERACAO,";
            strSql += " A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO";
            strSql += " FROM DESCONTOS A";
            strSql += " LEFT JOIN CONVENIADAS B ON (B.CON_ID = A.CON_ID)";
            strSql += " LEFT JOIN PRODUTOS C ON (A.PROD_ID = C.PROD_ID)";
            strSql += " LEFT JOIN DEPARTAMENTOS D ON (A.DEP_CODIGO = D.DEP_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO)";
            strSql += " LEFT JOIN CLASSES E ON (A.CLAS_CODIGO = E.CLAS_CODIGO AND A.EMP_CODIGO = E.EMP_CODIGO)";
            strSql += " LEFT JOIN SUBCLASSES F ON (A.SUB_CODIGO = F.SUB_CODIGO AND A.EMP_CODIGO = F.EMP_CODIGO)";
            strSql += " WHERE A.EMP_CODIGO = " + empCodigo;

            //BUSCA SEM NENHUM FILTRO//
            if (prodID == "" && descricao == "" && liberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.DESC_CODIGO";
            }
            else
            {
                if (prodID != "")
                {
                    strSql += " AND C.PROD_ID = '" + prodID + "'";
                }
                if (descricao != "")
                {
                    strSql += " AND A.DESC_DESCR LIKE '%" + descricao + "%'";
                }
                if (liberado != "TODOS")
                {
                    liberado = liberado == "N" ? "S" : "N";
                    strSql += " AND A.DESC_DESABILITADO = '" + liberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.DESC_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(Desconto dados)
        {
            string strCmd = "INSERT INTO DESCONTOS (DESC_CODIGO, EMP_CODIGO, DESC_DESCR, CON_ID, PROD_ID, DEP_CODIGO,"
                + "CLAS_CODIGO, SUB_CODIGO, DESC_MIN, DESC_MAX, DESC_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.DescCodigo + ","
                + dados.EmpCodigo + ",'"
                + dados.DescDescr + "',"
                + dados.ConId + ","
                + dados.ProdID + ","
                + dados.DepCodigo + ","
                + dados.ClasCodigo + ","
                + dados.SubCodigo + ",";
            strCmd += Funcoes.BFormataValor(dados.DescMin) + "," + Funcoes.BFormataValor(dados.DescMax) + ",'" + dados.DescLiberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Desconto dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[11, 3];
            int contMatriz = 0;
            string strCmd;

            strCmd = "UPDATE DESCONTOS SET ";

            if (!dadosNew.DescDescr.Equals(dadosOld.Rows[0]["DESC_DESCR"]))
            {
                strCmd += " DESC_DESCR = '" + dadosNew.DescDescr + "',";

                dados[contMatriz, 0] = "DESC_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESC_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DescDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConId.Equals(dadosOld.Rows[0]["CON_ID"]))
            {
                strCmd += " CON_ID = " + dadosNew.ConId + ",";

                dados[contMatriz, 0] = "CON_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_ID"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConId + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ProdID.Equals(dadosOld.Rows[0]["PROD_ID"].ToString()))
            {
                strCmd += " PROD_ID = " + dadosNew.ProdID + ",";

                dados[contMatriz, 0] = "PROD_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROD_ID"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ProdID + "'";
                contMatriz = contMatriz + 1;

            }
            if (!dadosNew.DepCodigo.Equals(dadosOld.Rows[0]["DEP_CODIGO"].ToString()))
            {
                strCmd += " DEP_CODIGO = " + dadosNew.DepCodigo + ",";

                dados[contMatriz, 0] = "DEP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DEP_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DepCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ClasCodigo.Equals(dadosOld.Rows[0]["CLAS_CODIGO"].ToString()))
            {
                strCmd += " CLAS_CODIGO = " + dadosNew.ClasCodigo + ",";

                dados[contMatriz, 0] = "CLAS_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLAS_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ClasCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.SubCodigo.Equals(dadosOld.Rows[0]["SUB_CODIGO"].ToString()))
            {
                strCmd += " SUB_CODIGO = " + dadosNew.SubCodigo + ",";

                dados[contMatriz, 0] = "SUB_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SUB_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.SubCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.DescMin).Equals(String.Format("{0:N}", dadosOld.Rows[0]["DESC_MIN"])))
            {
                strCmd += " DESC_MIN = " + Funcoes.BFormataValor(dadosNew.DescMin) + ",";

                dados[contMatriz, 0] = "DESC_MIN";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESC_MIN"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DescMin + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.DescMax).Equals(String.Format("{0:N}", dadosOld.Rows[0]["DESC_MAX"])))
            {
                strCmd += " DESC_MAX = " + Funcoes.BFormataValor(dadosNew.DescMax) + ",";

                dados[contMatriz, 0] = "DESC_MAX";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESC_MAX"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DescMax + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DescLiberado.Equals(dadosOld.Rows[0]["DESC_DESABILITADO"]))
            {
                strCmd += " DESC_DESABILITADO = '" + dadosNew.DescLiberado + "',";

                dados[contMatriz, 0] = "DESC_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESC_DESABILITADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DescLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and desc_codigo = " + dadosNew.DescCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd ,null) != -1)
                {
                    Funcoes.GravaLogAlteracao("DESC_CODIGO", dadosNew.DescCodigo.ToString(), Principal.usuario, "DESCONTOS", dados, contMatriz, dadosNew.EmpCodigo);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(int empCodigo, string descCodigo)
        {
            string strCmd = "DELETE FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo + " AND DESC_CODIGO = " + descCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }


    }
}
