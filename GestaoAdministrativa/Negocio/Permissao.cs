﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Permissao
    {
        public int PermissoesID { get; set; }
        public int GrupoID { get; set; }
        public int ModuloID { get; set; }
        public char Acessa { get; set; }
        public char Inclui { get; set; }
        public char Altera { get; set; }
        public char Exclui { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Permissao() { }

        public bool InsereRegistroPermissao(Permissao dados)
        {
            string strCmd = "INSERT INTO PERMISSOES (PERMISSOES_ID, GRUPO_ID, MODULO_ID, ACESSA, INCLUI, ALTERA, EXCLUI, DTCADASTRO, OPCADASTRO) VALUES ("
                + dados.PermissoesID + ","
                + dados.GrupoID + ","
                + dados.ModuloID + ",'"
                + dados.Acessa + "','"
                + dados.Inclui + "','"
                + dados.Altera + "','"
                + dados.Exclui + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaPermissoesPorGrupoDeUsuario(int grupoUsuarioID)
        {
            string strSql = "SELECT * FROM PERMISSOES WHERE GRUPO_ID = " + grupoUsuarioID;
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaPermissoesEModulo(Permissao dadosBusca)
        {
            string strSql = "SELECT P.MODULO_ID, M.DESCRICAO, P.ACESSA, P.INCLUI, P.ALTERA, P.EXCLUI, "
                                + "P.GRUPO_ID, P.PERMISSOES_ID FROM PERMISSOES P, MODULO_MENU M WHERE P.MODULO_ID = M.MODULO_ID AND P.GRUPO_ID = " + dadosBusca.GrupoID
                                + " ORDER BY M.MENU, P.MODULO_ID, M.DESCRICAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaPermissoes(string colNome, string valor, int grupoID, int moduloID)
        {
            string strCmd = "UPDATE PERMISSOES SET " + colNome + " = '" + valor
                + "' WHERE GRUPO_ID = " + grupoID + " AND MODULO_ID = " + moduloID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool GravaLogPermissoes(int id, string usuarioID, string logCampo, string logAlterado)
        {
            string strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA,"
                        + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO) VALUES ('PERMISSOES_ID','" + id
                        + "','" + usuarioID + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PERMISSOES','" + logCampo + "',";
            if (logAlterado == "S")
            {
                strCmd += "'N','";
            }
            else
            {
                strCmd += "'S','";
            }
            strCmd += logAlterado + "'," + Principal.estAtual + "," + Principal.empAtual + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InsereNovasPermisssoes()
        {
            try
            {
                Permissao permissoes = new Permissao();
                DataTable retornoBuscaPermissoes = new DataTable();
                DataTable retornoBuscaGrupoDeUsuarios = new DataTable();
                DataTable retornoBuscaModulos = new DataTable();

                retornoBuscaGrupoDeUsuarios = Util.SelecionaRegistrosTodosOuEspecifico("GRUPO_USUARIOS", "GRUPO_USU_ID, ADMINISTRADOR");

                for (int i = 0; i < retornoBuscaGrupoDeUsuarios.Rows.Count; i++)
                {
                    retornoBuscaModulos = BancoDados.selecionarRegistros("SELECT MODULO_ID FROM MODULO_MENU ORDER BY MODULO_ID");
                    for (int x = 0; x < retornoBuscaModulos.Rows.Count; x++)
                    {
                        retornoBuscaPermissoes = BancoDados.selecionarRegistros("SELECT * FROM PERMISSOES WHERE MODULO_ID = " + retornoBuscaModulos.Rows[x]["MODULO_ID"] + " AND GRUPO_ID = " + retornoBuscaGrupoDeUsuarios.Rows[i]["GRUPO_USU_ID"]);
                        if (retornoBuscaPermissoes.Rows.Count == 0)
                        {
                            permissoes.PermissoesID = Convert.ToInt32(Funcoes.IdentificaVerificaID("PERMISSOES", "PERMISSOES_ID"));
                            permissoes.GrupoID = Convert.ToInt32(retornoBuscaGrupoDeUsuarios.Rows[i]["GRUPO_USU_ID"]);
                            permissoes.ModuloID = Convert.ToInt32(retornoBuscaModulos.Rows[x]["MODULO_ID"]);
                            if (retornoBuscaGrupoDeUsuarios.Rows[i]["ADMINISTRADOR"].ToString() == "S")
                            {
                                permissoes.Acessa = 'S';
                                permissoes.Inclui = 'S';
                                permissoes.Altera = 'S';
                                permissoes.Exclui = 'S';
                            }
                            else
                            {
                                permissoes.Acessa = 'N';
                                permissoes.Inclui = 'N';
                                permissoes.Altera = 'N';
                                permissoes.Exclui = 'N';
                            }
                            permissoes.DtCadastro = DateTime.Now;
                            permissoes.OpCadastro = Principal.usuario;

                            if (InsereRegistroPermissao(permissoes).Equals(false))
                            {
                                return false;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }


        public DataTable BuscaPermissoesEModuloPorMenu(Permissao dadosBusca, string menu)
        {
            string strSql = "SELECT P.MODULO_ID, M.DESCRICAO, P.ACESSA, P.INCLUI, P.ALTERA, P.EXCLUI, "
                                + "P.GRUPO_ID, P.PERMISSOES_ID FROM PERMISSOES P, MODULO_MENU M WHERE P.MODULO_ID = M.MODULO_ID AND P.GRUPO_ID = " + dadosBusca.GrupoID
                                + " AND M.MENU in (" + menu + ")"
                                + " ORDER BY M.DESCRICAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaPermissoesGeral(int grupoID, int moduloID)
        {
            string strCmd = "UPDATE PERMISSOES SET INCLUI = 'S', ALTERA = 'S', EXCLUI = 'S' "
                + " WHERE GRUPO_ID = " + grupoID + " AND MODULO_ID = " + moduloID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
