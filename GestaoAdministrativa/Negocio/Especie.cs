﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Especie
    {
        public int EspCodigo { get; set; }
        public string EspDescricao { get; set; }
        public string EspCheque { get; set; }
        public string EspCaixa { get; set; }
        public string EspLiberado { get; set; }
        public string EspVinculado { get; set; }
        public string EspSAT { get; set; }
        public string EspEcf { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public Especie() { }

        public Especie(int espCodigo, string espDescricao, string espCheque, string espCaixa, string espLiberado, string espVinculado, string espSAT, string espEcf, 
            DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EspCodigo = espCodigo;
            this.EspDescricao = espDescricao;
            this.EspCheque = espCheque;
            this.EspCaixa = espCaixa;
            this.EspLiberado = espLiberado;
            this.EspVinculado = espVinculado;
            this.EspSAT = espSAT;
            this.EspEcf = espEcf;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscarDados(int espCodigo, string espDescricao, string espLiberado, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.ESP_CODIGO, A.ESP_DESCRICAO, A.ESP_CHEQUE, CASE WHEN A.ESP_CAIXA = 'S' THEN 'SIM' ELSE 'NÃO' END ESP_CAIXA,"
                + " CASE A.ESP_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS ESP_DESABILITADO , A.DAT_ALTERACAO, A.OP_ALTERACAO, A.ESP_VINCULADO, A.ESP_SAT, A.ESP_ECF, "
                + " A.DT_CADASTRO, A.OP_CADASTRO FROM CAD_ESPECIES A ";
            if (espCodigo == 0 && espDescricao == "" && espLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.ESP_CODIGO";
            }
            else
            {
                if (espCodigo != 0)
                {
                    strSql += " WHERE A.ESP_CODIGO = " + espCodigo;
                }
                if (espDescricao != "")
                {
                    if (espCodigo != 0)
                        strSql += " AND A.ESP_DESCRICAO LIKE '%" + espDescricao + "%'";
                    else
                        strSql += " WHERE A.ESP_DESCRICAO LIKE '%" + espDescricao + "%'";
                }
                if (espLiberado != "TODOS")
                {
                    if (espCodigo != 0 || espDescricao != "")
                        strSql += " AND A.ESP_DESABILITADO = '" + espLiberado + "'";
                    else
                        strSql += " WHERE A.ESP_DESABILITADO = '" + espLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.ESP_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InserirDados(Especie dados)
        {
            string strCmd = "INSERT INTO CAD_ESPECIES(ESP_CODIGO,ESP_DESCRICAO,ESP_CHEQUE,ESP_CAIXA,ESP_DESABILITADO,ESP_VINCULADO,ESP_SAT, ESP_ECF, DT_CADASTRO,OP_CADASTRO) VALUES("
                + dados.EspCodigo + ",'"
                + dados.EspDescricao + "','"
                + dados.EspCheque + "','"
                + dados.EspCaixa + "','"
                + dados.EspLiberado + "','"
                + dados.EspVinculado + "','"
                + dados.EspSAT + "','"
                + dados.EspEcf + "',"
                + Funcoes.BDataHora(DateTime.Now) + ",'"
                + Principal.usuario + "')";

            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Especie dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[9, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE CAD_ESPECIES SET ";

            if (!dadosNew.EspDescricao.Equals(dadosOld.Rows[0]["ESP_DESCRICAO"]))
            {
                if (Util.RegistrosPorEstabelecimento("CAD_ESPECIES", "ESP_DESCRICAO", dadosNew.EspDescricao).Rows.Count != 0)
                {
                    Principal.mensagem = "Espécie já cadastrada.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " ESP_DESCRICAO = '" + dadosNew.EspDescricao + "',";

                dados[contMatriz, 0] = "ESP_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspDescricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspCheque.Equals(Principal.dtBusca.Rows[0]["ESP_CHEQUE"]))
            {
                strCmd += " ESP_CHEQUE = '" + dadosNew.EspCheque + "',";

                dados[contMatriz, 0] = "ESP_CHEQUE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_CHEQUE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspCheque + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspCaixa.Equals(Principal.dtBusca.Rows[0]["ESP_CAIXA"]))
            {
                strCmd += " ESP_CAIXA = '" + dadosNew.EspCaixa + "',";

                dados[contMatriz, 0] = "ESP_CAIXA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_CAIXA"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspCaixa + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspVinculado.Equals(Principal.dtBusca.Rows[0]["ESP_VINCULADO"]))
            {
                strCmd += " ESP_VINCULADO = '" + dadosNew.EspVinculado + "',";

                dados[contMatriz, 0] = "ESP_VINCULADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_VINCULADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspVinculado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspSAT.Equals(Principal.dtBusca.Rows[0]["ESP_SAT"]))
            {
                strCmd += " ESP_SAT = '" + dadosNew.EspSAT + "',";

                dados[contMatriz, 0] = "ESP_SAT";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_SAT"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspSAT + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspEcf.Equals(Principal.dtBusca.Rows[0]["ESP_ECF"]))
            {
                strCmd += " ESP_ECF = '" + dadosNew.EspEcf + "',";

                dados[contMatriz, 0] = "ESP_ECF";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_ECF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspEcf + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EspLiberado.Equals(Principal.dtBusca.Rows[0]["ESP_DESABILITADO"]))
            {
                strCmd += " ESP_DESABILITADO = '" + dadosNew.EspLiberado + "',";

                dados[contMatriz, 0] = "ESP_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ESP_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EspLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OP_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OP_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OP_ALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNew.DtAlteracao) + ",";
                strCmd += " op_alteracao = '" + dadosNew.OpAlteracao + "'";
                strCmd += " WHERE esp_codigo = " + dadosNew.EspCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("ESP_CODIGO", dadosNew.EspCodigo.ToString(), Principal.usuario, "CAD_ESPECIES", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(string espCodigo = "0", char todos = 'N')
        {
            string strCmd = " DELETE FROM CAD_ESPECIES ";
            if (todos.Equals('N'))
            {
                strCmd += " WHERE ESP_CODIGO = " + espCodigo;
            }
            var retorno = BancoDados.ExecuteNoQuery(strCmd, null);
            return retorno;
        }

        public string ConsideraEspecie(int espCodigo)
        {
            string strSql = "SELECT ESP_CAIXA FROM CAD_ESPECIES WHERE ESP_CODIGO = " + espCodigo;

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public string IdentificaEspecieVinculado(int espCodigo)
        {
            string strSql = "SELECT ESP_VINCULADO FROM CAD_ESPECIES WHERE ESP_CODIGO = " + espCodigo;

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public string IdentificaEspecieSAT(int espCodigo)
        {
            string strSql = "SELECT ESP_SAT FROM CAD_ESPECIES WHERE ESP_CODIGO = " + espCodigo;

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public string IdentificaEspecieEcf(int espCodigo)
        {
            string strSql = "SELECT ESP_ECF FROM CAD_ESPECIES WHERE ESP_CODIGO = " + espCodigo;

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public DataTable BuscaDados(char liberado = 'S', char todos = 'S')
        {
            string sql = "SELECT ESP_CODIGO, ESP_DESCRICAO FROM CAD_ESPECIES ";
            if (liberado.Equals('N'))
            {
                sql += " WHERE ESP_DESABILITADO = '" + liberado + "'";
            }
            if (todos.Equals('N'))
            {
                if (liberado.Equals('N'))
                {
                    sql += " AND ESP_CODIGO IN (1,2,3,6)";
                }
                else
                {
                    sql += " WHERE ESP_CODIGO IN (1,2,3,6)";
                }
            }

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable VendasPorEspecie(DateTime dtInicial, DateTime dtFinal)
        {
            string sql = "SELECT SUM(A.VENDA_ESPECIE_VALOR) AS TOTAL, B.ESP_DESCRICAO"
                        + "        FROM VENDAS_ESPECIES A"
                        + "        INNER JOIN CAD_ESPECIES B ON A.ESP_CODIGO = B.ESP_CODIGO"
                        + "        INNER JOIN VENDAS C ON A.VENDA_ID = C.VENDA_ID"
                        + "        WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "        AND A.EST_CODIGO = " + Principal.estAtual
                        + "        AND A.EMP_CODIGO = C.EMP_CODIGO"
                        + "        AND A.EST_CODIGO = C.EST_CODIGO AND C.VENDA_STATUS = 'F'"
                        + "        AND C.VENDA_EMISSAO BETWEEN "
                        + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                        + "        GROUP BY B.ESP_DESCRICAO";
            return BancoDados.GetDataTable(sql, null);
        }

        public string BuscaDescricao(int espCodigo)
        {
            string sql = "SELECT ESP_DESCRICAO FROM CAD_ESPECIES  WHERE ESP_CODIGO = " + espCodigo;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }
    }
}
