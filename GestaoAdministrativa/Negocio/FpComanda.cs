﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FpComanda
    {
        public long VendaID { get; set; }
        public string NSU { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public FpComanda() { }

        public FpComanda(long vendaID, string nsu, DateTime dtCadastro, string opCadastro)
        {
            this.VendaID = vendaID;
            this.NSU = nsu;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable IdentificaSeVendaEPorComanda(string nsu, long vendaID = 0)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM FP_COMANDA "
                + "WHERE DATASUS = '" + nsu + "'";
            if (vendaID != 0)
            {
                strSql += " AND VENDA_ID = " + vendaID;
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(FpComanda dados)
        {
            string strCmd = "INSERT INTO FP_COMANDA(VENDA_ID, DATASUS, DT_CADASTRO, OP_CADASTRO) VALUES ("
                + dados.VendaID + ",'"
                + dados.NSU + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID = 0, bool transAberta = false)
        {
            string strCmd = "DELETE FROM FP_COMANDA WHERE DATASUS = '" + nsu + "'";
            if (vendaID != 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            if(!transAberta)
                return BancoDados.ExecuteNoQuery(strCmd, null);
            else
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }

        public DataTable BuscaComandaPorVendaID(long vendaID)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM FP_COMANDA "
                + "WHERE  VENDA_ID = " + vendaID;

            return BancoDados.GetDataTable(strSql, null);
        }
    }

}
