﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ApiIbpt
    {
        public string Codigo { get; set; }
        public string UF { get; set; }
        public int EX { get; set; }
        public string Descricao { get; set; }
        public double Nacional { get; set; }
        public double Estadual { get; set; }
        public double Importado { get; set; }
        public double Municipal { get; set; }
        public string Tipo { get; set; }
        public string VigenciaInicio { get; set; }
        public string VigenciaFim { get; set; }
        public string Chave { get; set; }
        public string Versao { get; set; }
        public string Fonte { get; set; }
        public double Valor { get; set; }
        public double ValorTributoNacional { get; set; }
        public double ValorTributoEstadual { get; set; }
        public double ValorTributoImportado { get; set; }
        public double ValorTributoMunicipal { get; set; }
    }
}
