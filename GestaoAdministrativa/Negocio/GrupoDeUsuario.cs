﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class GrupoDeUsuario
    {
        public int GrupoUsuId { get; set; }
        public string Descricao { get; set; }
        public string Administrador { get; set; }
        public string Liberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public GrupoDeUsuario() { }


        public bool InsereRegistroGrupoUsuario(GrupoDeUsuario dadosGrupoUsuario)
        {
            string strCmd = "INSERT INTO GRUPO_USUARIOS (GRUPO_USU_ID, DESCRICAO, ADMINISTRADOR, LIBERADO,DTCADASTRO, OPCADASTRO) VALUES (" + dadosGrupoUsuario.GrupoUsuId + ",'"
                + dadosGrupoUsuario.Descricao + "','"
                + dadosGrupoUsuario.Administrador + "','"
                + dadosGrupoUsuario.Liberado + "',"
                + Funcoes.BDataHora(dadosGrupoUsuario.DtCadastro) + ",'"
                + dadosGrupoUsuario.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        public void AtualizaGrupoAdministrador()
        {
            var dt = new DataTable();
            string strSql = "SELECT * FROM USUARIOS";

            dt = BancoDados.GetDataTable(strSql, null);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strSql = "UPDATE USUARIOS SET ID = " + Funcoes.IdentificaVerificaID("USUARIOS","ID") + ", LIBERADO = 'S',"
                    + " DTCADASTRO = " + Funcoes.BDataHora(DateTime.Now) + ", OPCADASTRO = '" + Principal.usuario + "', GRUPO_ID = 1"
                    + " WHERE LOGIN_ID = '" + dt.Rows[i]["LOGIN_ID"] + "'";
                BancoDados.ExecuteNoQuery(strSql, null);
            }
        }

        public DataTable BuscaGrupoDeUsuarios(GrupoDeUsuario dadosBusca, out string strOrdem)
        {
            string strSql = "SELECT A.GRUPO_USU_ID, A.DESCRICAO, A.ADMINISTRADOR, A.LIBERADO, A.DTALTERACAO, A.OPALTERACAO, "
                    + " A.DTCADASTRO, A.OPCADASTRO FROM GRUPO_USUARIOS A ";
            if (dadosBusca.GrupoUsuId == 0 && dadosBusca.Descricao == "" && dadosBusca.Liberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_USU_ID";
            }
            else
            {
                if (dadosBusca.GrupoUsuId != 0)
                {
                    strSql += " WHERE A.GRUPO_USU_ID = " + dadosBusca.GrupoUsuId;
                }
                if (dadosBusca.Descricao != "")
                {
                    if (dadosBusca.GrupoUsuId == 0)
                    {
                        strSql += " WHERE A.DESCRICAO LIKE '%" + dadosBusca.Descricao + "%'";
                    }
                    else
                        strSql += " AND A.DESCRICAO LIKE '%" + dadosBusca.Descricao + "%'";
                }
                if (dadosBusca.Liberado != "TODOS")
                {
                    if ((dadosBusca.GrupoUsuId == 0) && (dadosBusca.Descricao == ""))
                    {
                        strSql += " WHERE A.LIBERADO = '" + dadosBusca.Liberado + "'";
                    }
                    else
                        strSql += " AND A.LIBERADO = '" + dadosBusca.Liberado + "'";
                }

                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_USU_ID";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable IdentificaGrupoDeUsuarioCadastradoPorId(GrupoDeUsuario dadosBusca)
        {
            string strSql = "SELECT * FROM GRUPO_USUARIOS WHERE GRUPO_USU_ID = " + dadosBusca.GrupoUsuId;
            return BancoDados.GetDataTable(strSql, null);
        }

        public string IdentificaGrupoDeUsuarioCadastradoPorDescricao(GrupoDeUsuario dadosBusca)
        {
            string strSql = "SELECT DESCRICAO FROM GRUPO_USUARIOS WHERE DESCRICAO = '" + dadosBusca.Descricao + "'";
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public bool AtualizaDados(GrupoDeUsuario dadosNovos, DataTable dadosCadastrados)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE GRUPO_USUARIOS SET ";
            if (!dadosNovos.Descricao.Equals(dadosCadastrados.Rows[0]["DESCRICAO"]))
            {
                if (!String.IsNullOrEmpty(IdentificaGrupoDeUsuarioCadastradoPorDescricao(dadosNovos)))
                {
                    Principal.mensagem = "Grupo já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " DESCRICAO = '" + dadosNovos.Descricao + "',";

                dados[contMatriz, 0] = "DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.Descricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.Administrador.Equals(dadosCadastrados.Rows[0]["ADMINISTRADOR"]))
            {
                strCmd += " ADMINISTRADOR = '" + dadosNovos.Administrador + "',";

                dados[contMatriz, 0] = "ADMINISTRADOR";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ADMINISTRADOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.Administrador + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.Liberado.Equals(dadosCadastrados.Rows[0]["LIBERADO"]))
            {
                strCmd += " LIBERADO = '" + dadosNovos.Liberado + "',";

                dados[contMatriz, 0] = "LIBERADO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.Liberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosCadastrados.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosCadastrados.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(dadosNovos.DtAlteracao);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosCadastrados.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosCadastrados.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OpAlteracao + "'";
                contMatriz = contMatriz + 1;


                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",";
                strCmd += " OPALTERACAO = '" + dadosNovos.OpAlteracao + "'";
                strCmd += " WHERE GRUPO_USU_ID = " + dadosNovos.GrupoUsuId;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("GRUPO_USU_ID", dadosNovos.GrupoUsuId.ToString(), dadosNovos.OpAlteracao, "GRUPO_USUARIOS", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirGrupoDeUsuario(GrupoDeUsuario dadosGrupo)
        {
            string strCmd = "DELETE FROM GRUPO_USUARIOS WHERE GRUPO_USU_ID = " + dadosGrupo.GrupoUsuId;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
