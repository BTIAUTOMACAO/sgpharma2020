﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ErroAbcFarma
    {
        public string data { get; set; }
        public string status { get; set; }
        public string error_message { get; set; }
        public string error_code { get; set; }
        public string link { get; set; }
    }
}
