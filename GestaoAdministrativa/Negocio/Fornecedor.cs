﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Fornecedor
    {
        public int FornCodigo { get; set; }
        public string FornCgc { get; set; }
        public string FornInsest { get; set; }
        public int FornEnquadra { get; set; }
        public string FornRazao { get; set; }
        public string FornFantasia { get; set; }
        public string FornEndereco { get; set; }
        public string FornBairro { get; set; }
        public string FornCidade { get; set; }
        public string FornCep { get; set; }
        public string FornUf { get; set; }
        public string FornPais { get; set; }
        public string FornFone { get; set; }
        public string FornCelular { get; set; }
        public string FornEmail { get; set; }
        public string FornContato { get; set; }
        public string FornLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string FornNumero { get; set; }

        public Fornecedor() { }
        public Fornecedor(int fornCodigo, string fornCgc, string fornInsest, int fornEnquadra, string fornRazao, string fornFantasia,
                           string fornEndereco, string fornBairro, string fornCidade, string fornCep, string fornUf, string fornPais,
                           string fornFone, string fornCelular, string fornEmail, string fornContato, string fornLiberado, DateTime dtAlteracao,
                           string opAlteracao, DateTime dtCadastro, string opCadastro, string fornNumero)
        {

            this.FornCodigo = fornCodigo;
            this.FornCgc = fornCgc;
            this.FornInsest = fornInsest;
            this.FornEnquadra = fornEnquadra;
            this.FornRazao = fornRazao;
            this.FornFantasia = fornFantasia;
            this.FornEndereco = fornEndereco;
            this.FornBairro = fornBairro;
            this.FornCidade = fornCidade;
            this.FornCep = fornCep;
            this.FornUf = fornUf;
            this.FornPais = fornPais;
            this.FornFone = fornFone;
            this.FornCelular = fornCelular;
            this.FornEmail = fornEmail;
            this.FornContato = fornContato;
            this.FornLiberado = fornLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.FornNumero = fornNumero;

        }

        public DataTable BuscaDados(Fornecedor dados, out string strOrdem)
        {
            string sql = " SELECT CF_ID, CF_DOCTO, CF_INSEST, CF_NOME, CF_APELIDO, CF_ENDER, CF_BAIRRO, CF_CIDADE, CF_UF, CF_CEP,  "
                       + " CF_PAIS, CF_FONE, CF_CELULAR, CF_EMAIL, CF_CNOME, CF_ENQUADRA, CASE WHEN CF_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS CF_DESABILITADO,"
                       + " DAT_ALTERACAO, OPALTERACAO, DTCADASTRO, OPCADASTRO,  CF_END_NUM FROM CLIFOR WHERE CF_TIPO_DOCTO = 1 AND CF_STATUS NOT IN (2,3)";
            
            if (dados.FornCodigo != 0)
            {
                sql += " AND CF_ID = " + dados.FornCodigo;
            }
            if (dados.FornRazao != "")
            {
                sql += " AND CF_NOME LIKE '%" + dados.FornRazao + "%'";
            }
            if (dados.FornFantasia != "")
            {
                sql += " AND CF_APELIDO LIKE '%" + dados.FornFantasia + "%'";
            }
            if (Funcoes.RemoveCaracter(dados.FornCgc) != "")
            {
                sql += " AND CF_DOCTO = '" + dados.FornCgc + "'";
            }
            if (dados.FornCidade != "")
            {
                sql += " AND CF_CIDADE LIKE '%" + dados.FornCidade + "%'";
            }
            if (dados.FornLiberado != "TODOS")
            {
                string desab = dados.FornLiberado == "S" ? "N" : "S";
                sql += "AND CF_DESABILITADO = '" + desab + "'";
            }

            sql += " ORDER BY CF_ID";
            strOrdem = sql;
            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(Fornecedor dadosNovos, DataTable DadosAntigos)
        {
            string status = dadosNovos.FornLiberado == "N" ? "0" : "1";
            string sql = " UPDATE CLIFOR SET "
                       + " CF_INSEST = '" + dadosNovos.FornInsest + "'"
                       + " ,CF_NOME =  '" + dadosNovos.FornRazao + "'"
                       + " ,CF_APELIDO =  '" + dadosNovos.FornFantasia + "'"
                       + " ,CF_ENDER =  '" + dadosNovos.FornEndereco + "'"
                       + " ,CF_BAIRRO = '" + dadosNovos.FornBairro + "'"
                       + " ,CF_CIDADE = '" + dadosNovos.FornCidade + "'"
                       + " ,CF_UF = '" + dadosNovos.FornUf + "'"
                       + " ,CF_CEP = '" + dadosNovos.FornCep + "'"
                       + " ,CF_PAIS = '" + dadosNovos.FornPais + "'"
                       + " ,CF_FONE = '" + dadosNovos.FornFone + "'"
                       + " ,CF_CELULAR = '" + dadosNovos.FornCelular + "'"
                       + " ,CF_EMAIL = '" + dadosNovos.FornEmail + "'"
                       + " ,CF_CNOME = '" + dadosNovos.FornContato + "'"
                       + " ,CF_ENQUADRA =  '" + dadosNovos.FornEnquadra + "'"
                       + " ,CF_DESABILITADO = '" + dadosNovos.FornLiberado + "'"
                       + " ,DAT_ALTERACAO =  " + Funcoes.BDataHora(DateTime.Now)
                       + " ,OPALTERACAO = '" + Principal.usuario + "'"
                       + ", CF_STATUS = " + status 
                       + ", CF_END_NUM = '" + dadosNovos.FornNumero + "'"
                       + ", CF_DOCTO = '" + dadosNovos.FornCgc + "'"
                       + " WHERE CF_ID = " + dadosNovos.FornCodigo;


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                string[,] dados = new string[16, 3];
                int contMatriz = 0;
                
                if (!Funcoes.RemoveCaracter(dadosNovos.FornInsest).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_INSEST"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_INSEST";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_INSEST"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornInsest + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornRazao.Equals(DadosAntigos.Rows[0]["CF_NOME"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_NOME";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_NOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornRazao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornFantasia.Equals(DadosAntigos.Rows[0]["CF_APELIDO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_APELIDO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_APELIDO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornFantasia + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornEndereco.Equals(DadosAntigos.Rows[0]["CF_ENDER"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_ENDER";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_ENDER"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornEndereco + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornNumero.Equals(DadosAntigos.Rows[0]["CF_END_NUM"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_END_NUM";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_END_NUM"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornNumero + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornBairro.Equals(DadosAntigos.Rows[0]["CF_BAIRRO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_BAIRRO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_BAIRRO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornBairro + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornCidade.Equals(DadosAntigos.Rows[0]["CF_CIDADE"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CIDADE";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornCidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornUf.Equals(DadosAntigos.Rows[0]["CF_UF"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_UF";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_UF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornUf + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.FornCep).Equals(DadosAntigos.Rows[0]["CF_CEP"].ToString().Replace(",00", "")))
                {
                    dados[contMatriz, 0] = "CF_CEP";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CEP"] + "'";
                    dados[contMatriz, 2] = "'" + Funcoes.RemoveCaracter(dadosNovos.FornCep) + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornPais.Equals(DadosAntigos.Rows[0]["CF_PAIS"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_PAIS";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_PAIS"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornPais + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.FornFone).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_FONE"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_FONE";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_FONE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornFone + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.FornCelular).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_CELULAR"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_CELULAR";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CELULAR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornCelular + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornEmail.Equals(DadosAntigos.Rows[0]["CF_EMAIL"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_EMAIL";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_EMAIL"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornEmail + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornContato.Equals(DadosAntigos.Rows[0]["CF_CNOME"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CNOME";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CNOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornContato + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornEnquadra.ToString().Equals(DadosAntigos.Rows[0]["CF_ENQUADRA"].ToString() == "" ? "" : Convert.ToInt32(DadosAntigos.Rows[0]["CF_ENQUADRA"]).ToString()))
                {
                    dados[contMatriz, 0] = "CF_ENQUADRA";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_ENQUADRA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornEnquadra + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FornLiberado.Equals(DadosAntigos.Rows[0]["CF_DESABILITADO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_DESABILITADO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FornLiberado + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("CF_ID", dadosNovos.FornCodigo.ToString(), Principal.usuario, "CLIFOR", dados, contMatriz, 1);
                return true;
            }
            else
                return false;
        }

        public bool InsereDados(Fornecedor dados)
        {
            string status = dados.FornLiberado == "N" ? "0" : "1";
            string sql = " INSERT INTO CLIFOR (CF_ID, CF_TIPO_DOCTO, CF_DOCTO, CF_INSEST, CF_NOME, CF_APELIDO, CF_ENDER, CF_BAIRRO, CF_CIDADE, "
                       + " CF_UF, CF_CEP, CF_PAIS, CF_FONE, CF_CELULAR, CF_EMAIL, CF_CNOME, CF_ENQUADRA, CF_DESABILITADO, CF_STATUS, "
                       + " DTCADASTRO, OPCADASTRO, CF_END_NUM) VALUES ( "
                       + dados.FornCodigo + ","
                       + 1 + ",'"
                       + dados.FornCgc + "','"
                       + dados.FornInsest + "','"
                       + dados.FornRazao + "','"
                       + dados.FornFantasia + "','"
                       + dados.FornEndereco + "','"
                       + dados.FornBairro + "','"
                       + dados.FornCidade + "','"
                       + dados.FornUf + "','"
                       + dados.FornCep + "','"
                       + dados.FornPais + "','"
                       + dados.FornFone + "','"
                       + dados.FornCelular + "','"
                       + dados.FornEmail + "','"
                       + dados.FornContato + "','"
                       + dados.FornEnquadra + "','"
                       + dados.FornLiberado + "',"
                       + status + ","
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "','" + dados.FornNumero + "')";


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("CF_ID", dados.FornCodigo.ToString(), Principal.usuario, "CLIFOR", dados.FornRazao, 1);
                return true;
            }
            else
            {
                return false;
            }


        }


        public DataTable BuscaFornecedor(string condicao, string parametro)
        {


            string sql = " SELECT CF_ID, CF_APELIDO, CF_BAIRRO, CF_NOME, CF_CIDADE, CF_UF, CF_DOCTO  FROM CLIFOR WHERE CF_TIPO_DOCTO = 1";

            if (condicao.Equals("CODIGO"))
            {
                sql += "AND CF_ID = " + parametro;
            }
            if (condicao.Equals("NOME"))
            {
                sql += "AND CF_NOME LIKE '%" + parametro + "%'";
            }
            if (condicao.Equals("CNPJ"))
            {
                sql += "AND CF_DOCTO = '" + parametro + "'";
            }

            sql += "ORDER BY CF_NOME";

            return BancoDados.GetDataTable(sql, null);
        }

        public int ExcluirDados(string fornCodigo)
        {
            string strCmd = "DELETE FROM CLIFOR WHERE CF_ID = " + fornCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
