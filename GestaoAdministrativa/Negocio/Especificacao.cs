﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class Especificacao
    {

        public int EmpCodigo { get; set; }
        public int EspCodigo { get; set; }
        public string EspDescr { get; set; }
        public string EspDesabilitado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Especificacao() { }

        public Especificacao(int empCodigo, int espCodigo, string espDescr, string espDesabilitado, DateTime dtAlteracao,
                             string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EspCodigo = espCodigo;
            this.EspDescr = espDescr;
            this.EspDesabilitado = espDesabilitado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public DataTable BuscaDados(string espCodigo, string espDescr, string espDesabilitado, out string strOrdem)
        {
            string sql = " SELECT EMP_CODIGO, ESP_CODIGO, ESP_DESCRICAO, CASE ESP_DESABILITADO "
                       + " WHEN 'N' THEN 'S' "
                       + " ELSE 'N' END "
                       + " AS ESP_DESABILITADO, "
                       + " OPALTERACAO, DTALTERACAO, DTCADASTRO , "
                       + " OPCADASTRO FROM ESPECIFICACOES WHERE EMP_CODIGO = " + Principal.estAtual;

                if (espCodigo != "")
                {
                    sql += " AND ESP_CODIGO = " + espCodigo;
                }
                if (espDescr != "")
                {
                    sql += " AND ESP_DESCRICAO LIKE '%" + espDescr.ToUpper() + "%'";
                }
                if (espDesabilitado != "TODOS")
                {
                    string desab = espDesabilitado == "S" ? "N" : "S";
                    sql += " AND ESP_DESABILITADO = '" + desab+ "'";
                }

                strOrdem = sql;
                sql += "ORDER BY ESP_CODIGO";
       

            return BancoDados.GetDataTable(sql, null);
        }

        public bool InserirDados(Especificacao dados)
        {
            string sql = " INSERT INTO ESPECIFICACOES (EMP_CODIGO, ESP_CODIGO, ESP_DESCRICAO, ESP_DESABILITADO, DTCADASTRO , OPCADASTRO) VALUES ( " 
                       + dados.EmpCodigo + ","
                       + dados.EspCodigo + ",'"
                       + dados.EspDescr + "','"
                       + dados.EspDesabilitado + "',"
                       + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                       + Principal.usuario.ToUpper() + "')";


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("ESP_CODIGO", dados.EspCodigo.ToString(), Principal.usuario, "ESPECIFICACOES", dados.EspDescr.ToUpper(), dados.EmpCodigo);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(Especificacao dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE ESPECIFICACOES SET "
                       + " ESP_DESCRICAO = '" + dadosNovos.EspDescr + "'"
                       + " ,ESP_DESABILITADO = '" + dadosNovos.EspDesabilitado + "'"
                       + " ,DTALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " WHERE ESP_CODIGO = " + dadosNovos.EspCodigo;
           
            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                if (!dadosNovos.EspDescr.Equals(dadosAntigos.Rows[0]["ESP_DESCRICAO"]))
                {
                    dados[contMatriz, 0] = "ESP_DESCRICAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ESP_DESCRICAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EspDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EspDesabilitado.Equals(dadosAntigos.Rows[0]["ESP_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "ESP_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ESP_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EspDesabilitado + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("ESP_CODIGO", dadosNovos.EspCodigo.ToString(), Principal.usuario, "ESPECIFICACOES", dados, contMatriz, dadosNovos.EmpCodigo);

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool ExcluirDados(string espID)
        {

            string sql = "DELETE FROM ESPECIFICACOES WHERE EMP_CODIGO= " + Principal.empAtual + " AND ESP_CODIGO = " + Math.Truncate(Convert.ToDouble(espID));

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}