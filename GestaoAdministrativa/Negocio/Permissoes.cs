﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    class Permissoes
    {
        public int PermissoesID { get; set; }
        public int GrupoID { get; set; }
        public int ModuloID { get; set; }
        public char Acessa { get; set; }
        public char Inclui { get; set; }
        public char Altera { get; set; }
        public char Exclui { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public Permissoes() { }

        public static bool InserirDados(Permissoes dados)
        {
            MontadorSql mont = new MontadorSql("permissoes", MontadorType.Insert);
            mont.AddField("permissoes_id", dados.PermissoesID);
            mont.AddField("grupo_id", dados.GrupoID);
            mont.AddField("modulo_id", dados.ModuloID);
            mont.AddField("acessa", dados.Acessa);
            mont.AddField("inclui", dados.Inclui);
            mont.AddField("altera", dados.Altera);
            mont.AddField("exclui", dados.Exclui);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static DataTable GetCarregaPermissoes(int grupoID)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("grupo_id", grupoID));

            Principal.strSql = "SELECT P.MODULO_ID, M.DESCRICAO, P.ACESSA, P.INCLUI, P.ALTERA, P.EXCLUI, "
                                + "P.GRUPO_ID, P.PERMISSOES_ID FROM PERMISSOES P, MODULO_MENU M WHERE P.MODULO_ID = M.MODULO_ID AND P.GRUPO_ID = @grupo_id"
                                + " ORDER BY M.MODULO_ID";

            return BancoDados.GetDataTable(Principal.strSql, ps);
        }


        public static bool GetUpPermissoes(string colNome, string valor, int grupoID, int moduloID)
        {
            Principal.strCmd = "UPDATE PERMISSOES SET " + colNome + " = '" + valor 
                + "' WHERE GRUPO_ID = " + grupoID + " AND MODULO_ID = " + moduloID;
            if (BancoDados.ExecuteNoQuery(Principal.strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public static bool GetLogPermissoes(int id, int usuarioID, string logCampo, string logAlterado)
        {
            Principal.strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA,"
                        + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO) VALUES ('PERMISSOES_ID','" + id
                        + "'," + usuarioID + ", 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PERMISSOES','" + logCampo + "',";
            if (logAlterado == "S")
            {
                Principal.strCmd += "'N','";
            }
            else
            {
                Principal.strCmd += "'S','";
            }
            Principal.strCmd += logAlterado + "')";
            if (BancoDados.ExecuteNoQuery(Principal.strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public static bool InserirPermisssoes()
        {
            try
            {
                Permissoes permissoes = new Permissoes();

                Principal.dtBusca = Util.GetSelecionarRegistros("GRUPO_USUARIOS", "GRUPO_USU_ID, ADMINISTRADOR");

                for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                {
                    Principal.dtRetorno = BancoDados.selecionarRegistros("SELECT MODULO_ID FROM MODULO_MENU ORDER BY MODULO_ID");
                    for (int x = 0; x < Principal.dtRetorno.Rows.Count; x++)
                    {
                        Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM PERMISSOES WHERE MODULO_ID = " + Principal.dtRetorno.Rows[x]["MODULO_ID"] + " AND GRUPO_ID = " + Principal.dtBusca.Rows[i]["GRUPO_USU_ID"]);
                        if (Principal.dtPesq.Rows.Count == 0)
                        {
                            permissoes.PermissoesID = Convert.ToInt32(Funcoes.GetSequence("SPERMISSOES_ID"));
                            permissoes.GrupoID = Convert.ToInt32(Principal.dtBusca.Rows[i]["GRUPO_USU_ID"]);
                            permissoes.ModuloID = Convert.ToInt32(Principal.dtRetorno.Rows[x]["MODULO_ID"]);
                            if (Principal.dtBusca.Rows[i]["ADMINISTRADOR"].ToString() == "S")
                            {
                                permissoes.Acessa = 'S';
                                permissoes.Inclui = 'S';
                                permissoes.Altera = 'S';
                                permissoes.Exclui = 'S';
                            }
                            else
                            {
                                permissoes.Acessa = 'N';
                                permissoes.Inclui = 'N';
                                permissoes.Altera = 'N';
                                permissoes.Exclui = 'N';
                            }
                            permissoes.DtCadastro = DateTime.Now;
                            permissoes.OpCadastro = Convert.ToInt32(Principal.usuarioID);

                            if (Permissoes.InserirDados(permissoes).Equals(false))
                            {
                                return false;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "BTI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
