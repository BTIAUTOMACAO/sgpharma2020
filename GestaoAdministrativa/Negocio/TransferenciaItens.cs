﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class TransferenciaItens
    {
        public int ID { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public Double Valor { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
        
        public TransferenciaItens() { }

        public bool InsereRegistros(int estCodigo, int empCodigo, int id, string prodCodigo, int qtde, double valor)
        {
            string strCmd = "INSERT INTO TRANSFERENCIA_ITENS(EST_CODIGO,EMP_CODIGO, ID, PROD_CODIGO, QTDE, VALOR, DTCADASTRO, OPCADASTRO) VALUES (" +
                estCodigo + "," +
                empCodigo + "," +
                id + ",'" +
                prodCodigo + "'," +
                qtde + "," +
                Funcoes.BValor(valor) + "," + 
                Funcoes.BDataHora(DateTime.Now) + ",'" +
                Principal.usuario + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaItensTransferencias(string id, int empCodigo, int estCodigo)
        {
            string sql = "SELECT A.ID, A.PROD_CODIGO, B.PROD_DESCR, A.QTDE, COALESCE(A.VALOR, " + (Funcoes.LeParametro(9, "58", true) == "PROD_ULTCUSME" ? "C.PROD_ULTCUSME)" : "D.PRE_VALOR)") +  " AS PRECO "
                            + " FROM TRANSFERENCIA_ITENS A"
                            + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                            + " INNER JOIN PRECOS D ON A.PROD_CODIGO = D.PROD_CODIGO"
                            + " INNER JOIN TRANSFERENCIA E ON A.ID = E.ID"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.ID = " + id
                            + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + " AND A.EST_CODIGO = C.EST_CODIGO"
                            + " AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + " AND A.EST_CODIGO = D.EST_CODIGO"
                            + " AND A.EST_CODIGO = E.EST_CODIGO"
                            + " AND A.EMP_CODIGO = E.EMP_CODIGO";
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
