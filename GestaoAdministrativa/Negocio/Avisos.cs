﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Avisos
    {
        public string Aviso { get; set; }
        public string Data { get; set; }
        public string Todos { get; set; }
        public string CodEstabelecimento { get; set; }
        public string Lida { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
        public string Url { get; set; }
    }
}
