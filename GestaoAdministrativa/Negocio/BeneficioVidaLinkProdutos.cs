﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioVidaLinkProdutos
    {
        public string NSU { get; set; }
        public long VendaID { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public double PrecoMaximo { get; set; }
        public double PrecoVenda { get; set; }
        public double PrecoClienteAVista { get; set; }
        public double PrecoClienteAReceber { get; set; }
        public double ValorSubsidio { get; set; }
        public double ValorReembolso { get; set; }
        public double PorcentagemDesconto { get; set; }
        public double ComissaoVidaLink { get; set; }
        public DateTime Data { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        
        public BeneficioVidaLinkProdutos() { }

        public BeneficioVidaLinkProdutos(string nsu, long vendaID, string prodCodigo, int qtde, double precoMaximo, double precoVenda, double precoClienteAVista, double precoClienteAReceber, 
            double valorSubsidio, double valorReembolso, double porcentagemDesconto, double comissaoVidaLink, DateTime data, DateTime dtCadastro, string opCadastro)
        {
            this.NSU = nsu;
            this.VendaID = vendaID;
            this.ProdCodigo = prodCodigo;
            this.Qtde = qtde;
            this.PrecoMaximo = precoMaximo;
            this.PrecoVenda = precoVenda;
            this.PrecoClienteAVista = precoClienteAVista;
            this.PrecoClienteAReceber = precoClienteAReceber;
            this.ValorSubsidio = valorSubsidio;
            this.ValorReembolso = valorReembolso;
            this.PorcentagemDesconto = porcentagemDesconto;
            this.ComissaoVidaLink = comissaoVidaLink;
            this.Data = data;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID = 0)
        {
            string strCmd = "DELETE FROM BENEFICIO_VIDA_LINK_PRODUTOS WHERE NSU = '" + nsu + "'";
            if(vendaID != 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public bool InsereRegistros(BeneficioVidaLinkProdutos dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_VIDA_LINK_PRODUTOS(NSU, PROD_CODIGO, QTD, PR_MAX, PR_VENDA, PR_CLIENTE_AVISTA, PR_CLIENTE_ARECEB, VL_SUBSIDIO, VL_REEMBOLSO,"
                + " PORCENT_DESC, COMISSAO_VLINK, DATA, DTCADASTRO, OPCADASTRO) VALUES ('"
                + dados.NSU + "','"
                + dados.ProdCodigo + "',"
                + dados.Qtde + ","
                + Funcoes.BValor(dados.PrecoMaximo) + ","
                + Funcoes.BValor(dados.PrecoVenda) + ","
                + Funcoes.BValor(dados.PrecoClienteAVista) + ","
                + Funcoes.BValor(dados.PrecoClienteAReceber) + ","
                + Funcoes.BValor(dados.ValorSubsidio) + ","
                + Funcoes.BValor(dados.ValorReembolso) + ","
                + Funcoes.BValor(dados.PorcentagemDesconto) + ","
                + Funcoes.BValor(dados.ComissaoVidaLink) + ","
                + Funcoes.BData(dados.Data) + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }


        public DataTable BuscaProdutosPorNSU(string nsu)
        {
            string strSql;

            strSql = "SELECT A.QTD, A.NSU, A.PROD_CODIGO, B.PROD_DESCR, A.PR_VENDA, A.PR_CLIENTE_AVISTA, A.PR_CLIENTE_ARECEB, A.VL_SUBSIDIO, C.TIPO_AUTORIZACAO "
                + "FROM BENEFICIO_VIDA_LINK_PRODUTOS A "
                + "INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO "
                + "INNER JOIN BENEFICIO_VIDA_LINK C ON A.NSU = C.NSU "
                + "WHERE A.NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable IdentificaSomatorioVenda(string nsu)
        {
            string strSql;

            strSql = "SELECT SUM(A.PR_CLIENTE_AVISTA * qtd) AS AVISTA, SUM(A.PR_CLIENTE_ARECEB * qtd) AS RECEBER, SUM(QTD) AS QTD "
                + "FROM BENEFICIO_VIDA_LINK_PRODUTOS A "
                + "WHERE A.NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaVendaIdPorNsu(string nsu, long vendaID)
        {
            string strCmd = "UPDATE BENEFICIO_VIDA_LINK_PRODUTOS SET VENDA_ID = " + vendaID + " WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

       

    }
}
