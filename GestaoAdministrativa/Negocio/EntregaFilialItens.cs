﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class EntregaFilialItens
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int Codigo { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public double Preco { get; set; }
        public double Subtotal { get; set; }
        public double Desconto { get; set; }
        public double Total { get; set; }
        public double Comissao { get; set; }
        public int ID { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public EntregaFilialItens() { }

        public bool InsereRegistros(EntregaFilialItens dados)
        {
            string strCmd = "INSERT INTO ENTREGA_FILIAL_ITENS(EMP_CODIGO,EST_CODIGO, CODIGO, PROD_CODIGO, QTDE, PRECO, SUBTOTAL, DESCONTO,TOTAL,COMISSAO) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.Codigo + ",'" +
                dados.ProdCodigo + "'," +
                dados.Qtde + "," +
                Funcoes.BValor(dados.Preco) + "," +
                Funcoes.BValor(dados.Subtotal) + "," +
                Funcoes.BValor(dados.Desconto) + "," +
                Funcoes.BValor(dados.Total) + "," +
                Funcoes.BValor(dados.Comissao) + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
        
        public DataTable BuscaItensEntrega(string id, int empCodigo, int estCodigo)
        {
            string sql = "SELECT A.CODIGO, A.PROD_CODIGO, B.PROD_DESCR, A.QTDE, A.PRECO, A.SUBTOTAL, A.DESCONTO, A.TOTAL"
                            + " FROM ENTREGA_FILIAL_ITENS A"
                            + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.CODIGO = " + id;
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
