﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class CaixasStatus
    {

        public static string GetCaixaStatus(int estCodigo, string status, string usuarioID)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("cx_status", status));
            ps.Add(new Fields("login_id",usuarioID));

            Principal.strSql = "SELECT CX_DATA FROM CAIXAS_STATUS WHERE EST_CODIGO = @est_codigo AND CX_STATUS = @cx_status AND LOGIN_ID = @login_id";

            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

    }
}
