﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FpMedicamentosSolicitacao
    {
        public string AutorizacaoSolicitacao { get; set; }
        public string ProdCodigo { get; set; }
        public string UnidadeApresentacao { get; set; }
        public string AutorizacaoEstorno { get; set; }
        public string AutorizacaoMedicamento { get; set; }
        public int QtdAutorizada { get; set; }
        public int QtdDevolvida { get; set; }
        public int QtdEstornada { get; set; }
        public int QtdPrescrita { get; set; }
        public int QtdSolicitada { get; set; }
        public double SubsidioMS { get; set; }
        public double SubsidioMsPosEstorno { get; set; }
        public double SubsidioCliente { get; set; }
        public double SubsidioClientePosEstorno { get; set; }
        public double PrecoVenda { get; set; }
        public double TotalVendaEstorno { get; set; }

        public FpMedicamentosSolicitacao() { }

        public DataTable BuscaProdutosPorNSU(string nsu)
        {
            string strSql;

            strSql = "SELECT A.AUTORIZACAOSOLICITACAO, A.CODBARRAS, B.PROD_DESCR, A.PRECOVENDA, A.QTDPRESCRITA, A.QTDAUTORIZADA, COALESCE(C.PROD_QTDE_UN_COMP_FP,PROD_QTDE_UN_COMP) AS  PROD_QTDE_UN_COMP_FP  "
                + "FROM FP_MEDICAMENTOS_SOLICITACAO A "
                + "INNER JOIN PRODUTOS B ON A.CODBARRAS = B.PROD_CODIGO "
                + "INNER JOIN PRODUTOS_DETALHE C ON C.PROD_CODIGO = A.CODBARRAS "
                + "WHERE A.AUTORIZACAOSOLICITACAO = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable IdentificaSomatorioVenda(string nsu)
        {
            string strSql;

            strSql = "SELECT SUM(A.SUBSIDIADOMS) AS SUBSIDIADOMS, SUM(A.SUBSIDIADOCLIENTE) AS SUBSIDIADOCLIENTE "
                + "FROM FP_MEDICAMENTOS_SOLICITACAO A "
                + "WHERE A.AUTORIZACAOSOLICITACAO = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
