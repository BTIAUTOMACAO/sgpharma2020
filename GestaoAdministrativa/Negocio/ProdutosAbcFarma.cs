﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class ProdutosAbcFarma
    {
        public string ProdCodigo { get; set; }
        public string ProdDescricao { get; set; }
        public double MedPco18 { get; set; }
        public double MedPla18 { get; set; }
        public double MedFra18 { get; set; }
        public string CodAbcFarma { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public ProdutosAbcFarma() { }

        public ProdutosAbcFarma(string prodCodigo, string prodDescricao, double medPco18, double medPla18, double medFra18, string codAbcFarma, DateTime dtCadastro, string opCadastro)
        {
            this.ProdCodigo = prodCodigo;
            this.ProdDescricao = prodDescricao;
            this.MedPco18 = medFra18;
            this.MedPla18 = medFra18;
            this.MedFra18 = medFra18;
            this.CodAbcFarma = codAbcFarma;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaDados()
        {
            string sql = "SELECT PROD_CODIGO FROM PRODUTOS_ABCFARMA ORDER BY PROD_CODIGO";
            return BancoDados.GetDataTable(sql, null);
        }

        public bool InserirDados(ProdutosAbcFarma dados)
        {
            string strCmd = "INSERT INTO PRODUTOS_ABCFARMA(PROD_CODIGO,PROD_DESCR,MED_PCO18,MED_PLA18,MED_FRA18,CODIGO_ABCFARMA,DT_CADASTRO,OP_CADASTRO) VALUES ('"
                + dados.ProdCodigo + "','"
                + dados.ProdDescricao + "',"
                + Funcoes.BFormataValor(dados.MedPco18) + ","
                + Funcoes.BFormataValor(dados.MedPla18) + ","
                + Funcoes.BFormataValor(dados.MedFra18) + ",'"
                + dados.CodAbcFarma + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(ProdutosAbcFarma dados)
        {
            string strCmd = "UPDATE PRODUTOS_ABCFARMA SET MED_PCO18 = "
                + Funcoes.BFormataValor(dados.MedPco18) + ", MED_PLA18 = "
                + Funcoes.BFormataValor(dados.MedPla18) + ", MED_FRA18 = "
                + Funcoes.BFormataValor(dados.MedFra18) + ", DT_CADASTRO = "
                + Funcoes.BDataHora(dados.DtCadastro) + ", OP_CADASTRO = '" + dados.OpCadastro + "', CODIGO_ABCFARMA = '" + dados.CodAbcFarma
                + "' WHERE PROD_CODIGO = '" + dados.ProdCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public string BuscaCodigoAbcFarma(string codBarra)
        {
            string strSql = "SELECT CODIGO_ABCFARMA FROM PRODUTOS_ABCFARMA  WHERE  PROD_CODIGO = '"
                + codBarra + "' ";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }


        public bool InsereTabelaAbcFarma(double pmc, string prodCodigo, string prodDescr, double medPla, double medFra, string codAbc)
        {
            string sql = "INSERT INTO PRODUTOS_ABCFARMA (PROD_CODIGO,PROD_DESCR,MED_PCO18,MED_PLA18,MED_FRA18,CODIGO_ABCFARMA,DT_CADASTRO,OP_CADASTRO) VALUES ('"
                + prodCodigo + "','" + prodDescr + "'," + pmc.ToString().Replace(",", ".") + "," + medPla.ToString().Replace(",", ".") + "," + medFra.ToString().Replace(",", ".") + ",'" + codAbc + "',"
                + " to_date('" + DateTime.Now + "', 'DD/MM/YYYY HH24:MI:SS'),'gadm')";

            if (!BancoDados.ExecuteNoQuery(sql,null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
