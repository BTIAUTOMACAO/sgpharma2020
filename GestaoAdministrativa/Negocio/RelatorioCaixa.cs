﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class RelatorioCaixa
    {
        string enderecoWebService = Funcoes.LeParametro(9, "54", true);
        string grupoID = Funcoes.LeParametro(9, "53", true);
        string codEstab = Funcoes.LeParametro(9, "52", true);

        #region Inserir dados 
        public async Task<bool> InserirCaixaEspecieSintetico(int especieID, double total, string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/InserirCaixaSinteticoEspecie?especieID=" + especieID + "&grupoID=" + grupoID + "&codEstab=" + codEstab + "&total=" + total.ToString().Replace(',', '.') + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public async Task<bool> InserirRecebimentoParticular(int especieID, double total, string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/InserirRecebimentosParticular?especieID=" + especieID + "&grupoID=" + grupoID + "&codEstab=" + codEstab + "&total=" + total.ToString().Replace(',', '.') + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> InserirMovimento(double valor, string descricao, string operador, DateTime dataFechamento, int espCodigo)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/InserirMovimentoCaixa?grupoID=" + grupoID + "&codEstab=" + codEstab + "&valor=" + valor.ToString().Replace(',', '.') +
                                                                          "&descricao=" + descricao + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy")
                                                                          + "&especie=" + espCodigo);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Excluir dados 
        public async Task<bool> ExcluirCaixaEspecieSintetico(string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/ExcluirCaixaSinteticoEspecie?grupoID=" + grupoID + "&codEstab=" + codEstab + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ExcluirCaixaSinteticoFormaPagamento(string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/ExcluirCaixaSinteticoFormaPagamento?grupoID=" + grupoID + "&codEstab=" + codEstab + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ExcluirCaixaRecebimentosParticular(string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/ExcluirRecebimentosParticular?grupoID=" + grupoID + "&codEstab=" + codEstab + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<bool> ExcluirMovimentoCaixa(string operador, DateTime dataFechamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatoriosCaixa/ExcluirMovimentoCaixa?grupoID=" + grupoID + "&codEstab=" + codEstab + "&operador=" + operador + "&dataFechamento=" + dataFechamento.ToString("MM/dd/yyyy"));
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception)
            {

                return false;
            }
        }
        #endregion
    }
}
