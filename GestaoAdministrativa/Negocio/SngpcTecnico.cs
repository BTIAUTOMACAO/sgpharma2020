﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcTecnico
    {
        public int TecCodigo { get; set; }
        public string TecCpf { get; set; }
        public string TecNome { get; set; }
        public string TecLogin { get; set; }
        public string TecSenha { get; set; }
        public string TecLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public SngpcTecnico() { }

        public SngpcTecnico(int tecCodigo, string tecCpf, string tecNome, string tecLogin, string tecSenha, string tecLiberado,
                            DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.TecCodigo = tecCodigo;
            this.TecCpf = tecCpf;
            this.TecNome = tecNome;
            this.TecLogin = tecLogin;
            this.TecSenha = tecSenha;
            this.TecLiberado = tecLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }
        public DataTable BuscarTecnico(SngpcTecnico dados, out string strOrdem)
        {

            string sql = " SELECT TEC_CODIGO, TEC_CPF, TEC_NOME, TEC_LOGIN, "
                       + " CASE TEC_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS TEC_DESABILITADO, "
                       + " TEC_SENHA, DAT_ALTERACAO, DT_CADASTRO, OP_CADASTRO, OP_ALTERACAO FROM SNGPC_TECNICOS "
                       + " WHERE 1=1 ";

            if (dados.TecLiberado != "TODOS")
            {
                string desab = dados.TecLiberado == "SIM" ? "N" : "S";
                sql += "AND TEC_DESABILITADO = '" + desab + "'";
            }
            if (dados.TecCpf != "")
            {
                sql += "AND TEC_CPF = '" + dados.TecCpf + "'";
            }
            if (dados.TecCodigo != 0)
            {

                sql += "AND TEC_CODIGO = " + dados.TecCodigo;
            }

            strOrdem = sql;
            sql += " ORDER BY TEC_CODIGO";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(SngpcTecnico dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE SNGPC_TECNICOS SET"
                       + " TEC_CPF = '" + dadosNovos.TecCpf + "'"
                       + " ,TEC_NOME = '" + dadosNovos.TecNome + "'"
                       + " ,TEC_DESABILITADO = '" + dadosNovos.TecLiberado + "'"
                       + " ,TEC_LOGIN = '" + dadosNovos.TecLogin + "'"
                       + " ,TEC_SENHA = '" + dadosNovos.TecSenha + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                       + " ,OP_ALTERACAO = '" + Principal.usuario.ToUpper() + "'"
                       + " WHERE TEC_CODIGO = " + dadosNovos.TecCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                string[,] dados = new string[5, 3];
                int contMatriz = 0;

                if (!dadosNovos.TecCpf.Equals(dadosAntigos.Rows[0]["TEC_CPF"]))
                {
                    dados[contMatriz, 0] = "TEC_CPF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TEC_CPF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TecCpf + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TecNome.Equals(dadosAntigos.Rows[0]["TEC_NOME"]))
                {
                    dados[contMatriz, 0] = "TEC_NOME";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TEC_NOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TecNome + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TecLiberado.Equals(dadosAntigos.Rows[0]["TEC_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "TEC_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TEC_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TecLiberado + "'";
                    contMatriz = contMatriz + 1;
                }

                if (!dadosNovos.TecLogin.Equals(dadosAntigos.Rows[0]["TEC_LOGIN"]))
                {
                    dados[contMatriz, 0] = "TEC_LOGIN";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TEC_LOGIN"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TecLogin + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TecSenha.Equals(dadosAntigos.Rows[0]["TEC_SENHA"]))
                {
                    dados[contMatriz, 0] = "TEC_SENHA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TEC_SENHA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TecSenha + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("TEC_CODIGO", dadosNovos.TecCodigo.ToString(), Principal.usuario, "SNGPC_TECNICOS", dados, contMatriz, Principal.estAtual);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool InsereDados(SngpcTecnico dados)
        {

            string sql = " INSERT INTO SNGPC_TECNICOS (TEC_CODIGO, TEC_CPF, TEC_NOME, TEC_DESABILITADO, TEC_LOGIN, TEC_SENHA, "
                       + " DT_CADASTRO, OP_CADASTRO) VALUES ("
                       + dados.TecCodigo + ",'"
                       + dados.TecCpf + "','"
                       + dados.TecNome + "','"
                       + dados.TecLiberado + "','"
                       + dados.TecLogin + "','"
                       + dados.TecSenha + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario.ToUpper() + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                // GRAVA LOG
                Funcoes.GravaLogInclusao("TEC_CODIGO", dados.TecCodigo.ToString(), Principal.usuario.ToUpper(), "SNGPC_TECNICOS", dados.TecNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ExcluirDados(int TecCodigo)
        {
            string sql = "DELETE FROM SNGPC_TECNICOS WHERE TEC_CODIGO = " + TecCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogExclusao("TEC_CODIGO", TecCodigo.ToString(), Principal.usuario.ToUpper(), Convert.ToDateTime(Principal.data), "SNGPC_TECNICOS", TecCodigo.ToString(), Principal.motivo, Principal.estAtual);
                return true;
            }
            else
            {
                return false;

            }

        }

        public DataTable BuscaTecnicoPorCod(int cod) {

            string sql = "SELECT TEC_CODIGO, TEC_NOME, TEC_LOGIN, TEC_SENHA , TEC_CPF  FROM SNGPC_TECNICOS WHERE TEC_CODIGO = " + cod;

            return BancoDados.GetDataTable(sql, null);

        }
    }
}