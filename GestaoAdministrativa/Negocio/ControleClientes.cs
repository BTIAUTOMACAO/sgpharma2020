﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ControleClientes
    {
        public int CodLoja { get; set; }
        public string Cnpj { get; set; }
        public string Fantasia { get; set; }
        public string Contato { get; set; }
        public string Telefone { get; set; }
        public string Liberado { get; set; }
        public string Atualiza { get; set; }
        public string ExibeMensagem { get; set; }
        public string Mensagem { get; set; }

        public ControleClientes() { }

        public ControleClientes(int codLoja, string cnpj, string fantasia, string contato, string telefone, string liberado, string atualiza, string exibeMensagem, string mensagem)
        {
            this.CodLoja = codLoja;
            this.Cnpj = cnpj;
            this.Fantasia = fantasia;
            this.Contato = contato;
            this.Telefone = telefone;
            this.Liberado = liberado;
            this.Atualiza = atualiza;
            this.ExibeMensagem = exibeMensagem;
            this.Mensagem = mensagem;
        }


    }
}
