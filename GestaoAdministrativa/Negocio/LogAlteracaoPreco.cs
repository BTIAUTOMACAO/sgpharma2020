﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class LogAlteracaoPreco
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProCodigo { get; set; }
        public DateTime DataAlteracao { get; set; }
        public double ValorAntigo { get; set; }
        public double ValorNovo { get; set; }
        public string Usuario { get; set; }


        public LogAlteracaoPreco() { }

        public bool InsereLog(LogAlteracaoPreco log)
        {

            string sql = " INSERT INTO LOG_ALTERA_PRECO ( EMP_CODIGO, EST_CODIGO, PROD_CODIGO, DATA_ALTERA, PRE_VALOR_ANTIGO, PRE_VALOR_NOVO, USUARIO ) "
                       + " VALUES ("
                       + log.EmpCodigo + " , "
                       + log.EstCodigo + " , '"
                       + log.ProCodigo + "',"
                       + Funcoes.BDataHora(log.DataAlteracao) + " ,'"
                       + log.ValorAntigo + "','"
                       + log.ValorNovo + "','"
                       + log.Usuario + "')";


            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
