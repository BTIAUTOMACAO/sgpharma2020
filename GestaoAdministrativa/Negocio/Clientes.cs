﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    class Clientes
    {
        public int CliId { get; set; }          
        public string CliCpf {get; set;}        
        public string CliNome {get; set;}       
        public string CliApelido {get; set;}    
        public string CliCodigo {get; set;}     
        public string GrupoCodigo {get; set;}    
        public string CliEnder {get; set;}       
        public string CliBairro {get; set;}     
        public string CliCidade {get; set;}      
        public string CliCep {get; set;}          
        public string CliUf {get; set;}           
        public string CliTelefone {get; set;}    
        public string CliCelular {get; set;}     
        public string CliRecado {get; set;}       
        public string CliEndercob {get; set;}    
        public string CliBairrocob {get; set;}    
        public string CliCidadecob {get; set;}    
        public string CliCepcob {get; set;}      
        public string CliUfcob {get; set;}        
        public string CliEnderent {get; set;}     
        public string CliBairroent {get; set;}    
        public string CliCidadeent {get; set;}    
        public string CliCepent {get; set;}       
        public string CliUfent {get; set;}        
        public string CliEmail {get; set;}        
        public string CliContato {get; set;}      
        public string CliCondicao {get; set;}      
        public string CliConCodigo {get; set;}   
        public string CliCodCartao {get; set;}   
        public string CliLimite {get; set;}       
        public string CliObservacao {get; set;} 
        public string CliLiberado {get; set;}     
        public DateTime DtAlteracao {get; set;}      
        public int OpAlteracao {get; set;}     
        public DateTime DtCadastro {get; set;}
        public int OpCadastro { get; set; } 
   
        public Clientes() { }

        public static bool InserirDados(Clientes dados)
        {
            MontadorSql mont = new MontadorSql("clientes", MontadorType.Insert);
            mont.AddField("cli_id", dados.CliId);          
            mont.AddField("cli_cpf", dados.CliCpf);
            mont.AddField("cli_nome", dados.CliNome);
            mont.AddField("cli_apelido", dados.CliApelido);      
            mont.AddField("cli_codigo", dados.CliCodigo);
            mont.AddField("grupo_codigo", dados.GrupoCodigo);
            mont.AddField("cli_ender", dados.CliEnder);
            mont.AddField("cli_bairro", dados.CliBairro);
            mont.AddField("cli_cidade", dados.CliCidade);
            mont.AddField("cli_cep", dados.CliCep);
            mont.AddField("cli_uf", dados.CliUf);
            mont.AddField("cli_telefone", dados.CliTelefone);
            mont.AddField("cli_celular", dados.CliCelular);
            mont.AddField("cli_recado", dados.CliRecado);
            mont.AddField("cli_endercob", dados.CliEndercob);
            mont.AddField("cli_bairrocob", dados.CliBairrocob);
            mont.AddField("cli_cidadecob", dados.CliCidadecob);
            mont.AddField("cli_cepcob", dados.CliCepcob);
            mont.AddField("cli_ufcob", dados.CliUfcob);
            mont.AddField("cli_enderent", dados.CliEnderent);
            mont.AddField("cli_bairroent", dados.CliBairroent);
            mont.AddField("cli_cidadeent", dados.CliCidadeent);
            mont.AddField("cli_cepent", dados.CliCepent);
            mont.AddField("cli_ufent", dados.CliUfent);
            mont.AddField("cli_email", dados.CliEmail);
            mont.AddField("cli_contato", dados.CliContato);
            mont.AddField("cli_condicao", dados.CliCondicao);
            mont.AddField("cli_con_codigo", dados.CliConCodigo);
            mont.AddField("cli_cod_cartao", dados.CliCodCartao);
            mont.AddField("cli_limite", dados.CliLimite);
            mont.AddField("cli_observacao", dados.CliObservacao);
            mont.AddField("cli_liberado", dados.CliLiberado);     
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if(BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static bool AtualizaDados(Clientes dadosNew, DataTable dadosOld)
        {
            #region ATUALIZACAO
            string[,] dados = new string[33, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("clientes", MontadorType.Update);
            //VERIFICA SE OS DADOS ALTERADOS E INSERE NUMA MATRIZ PARA INCLUSAO NA TABELA LOG_ALTERAÇÕES//
            if (!dadosNew.CliCpf.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_CPF"].ToString())))
            {
                if (Util.GetRegistros("CLIENTES","CLI_CPF", dadosNew.CliCpf).Rows.Count > 1)
                {
                    Principal.mensagem = "Cliente já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }
                
                dados[contMatriz, 0] = "CLI_CPF";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CPF"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CPF"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCpf == null ? "null" : "'" + dadosNew.CliCpf + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cpf", dadosNew.CliCpf);
            }
            if (!dadosNew.CliNome.Equals(dadosOld.Rows[0]["CLI_NOME"]))
            {
                dados[contMatriz, 0] = "CLI_NOME";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_NOME"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliNome + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_nome", dadosNew.CliNome);
            }
            if (!dadosNew.CliApelido.Equals(dadosOld.Rows[0]["CLI_APELIDO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_APELIDO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_APELIDO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_APELIDO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliApelido == "" ? "null" : "'" + dadosNew.CliApelido + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_apelido", dadosNew.CliApelido);
            }
            if (!dadosNew.CliCodigo.ToUpper().Equals(dadosOld.Rows[0]["CLI_CODIGO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CODIGO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CODIGO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CODIGO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCodigo == "" ? "null" : "'" + dadosNew.CliCodigo + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_codigo", dadosNew.CliCodigo);
            }
            if (!dadosNew.GrupoCodigo.Equals(dadosOld.Rows[0]["GRUPO_CODIGO"].ToString()))
            {
                dados[contMatriz, 0] = "GRUPO_CODIGO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["GRUPO_CODIGO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["GRUPO_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = dadosNew.GrupoCodigo == "" ? "null" : "'" + dadosNew.GrupoCodigo + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("grupo_codigo", dadosNew.GrupoCodigo);
            }
            if (!dadosNew.CliEnder.Equals(dadosOld.Rows[0]["CLI_ENDER"]))
            {
                dados[contMatriz, 0] = "CLI_ENDER";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_ENDER"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliEnder + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_ender", dadosNew.CliEnder);
            }
            if (!dadosNew.CliBairro.Equals(dadosOld.Rows[0]["CLI_BAIRRO"]))
            {
                dados[contMatriz, 0] = "CLI_BAIRRO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_BAIRRO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_BAIRRO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliBairro == "" ? "null" : "'" + dadosNew.CliBairro + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_bairro", dadosNew.CliBairro);
            }
            if (!dadosNew.CliCidade.Equals(dadosOld.Rows[0]["CLI_CIDADE"]))
            {
                dados[contMatriz, 0] = "CLI_CIDADE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_CIDADE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliCidade + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cidade", dadosNew.CliCidade);
            }
            if (!dadosNew.CliCep.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_CEP"].ToString())))
            {
                dados[contMatriz, 0] = "CLI_CEP";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_CEP"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliCep + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cep", dadosNew.CliCep);
            }
            if (!dadosNew.CliUf.Equals(dadosOld.Rows[0]["CLI_UF"]))
            {
                dados[contMatriz, 0] = "CLI_UF";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_UF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliUf + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_uf", dadosNew.CliUf);
            }
            if (!dadosNew.CliTelefone.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_TELEFONE"].ToString())))
            {
                dados[contMatriz, 0] = "CLI_TELEFONE";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_TELEFONE"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_TELEFONE"] + "'";
                dados[contMatriz, 2] = dadosNew.CliTelefone == "" ? "null" : "'" + dadosNew.CliTelefone + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_telefone", dadosNew.CliTelefone);
            }
            if (!dadosNew.CliCelular.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_CELULAR"].ToString())))
            {
                dados[contMatriz, 0] = "CLI_CELULAR";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CELULAR"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CELULAR"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCelular == "" ? "null" : "'" + dadosNew.CliCelular + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_celular", dadosNew.CliCelular);
            }
            if (!dadosNew.CliRecado.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_RECADO"].ToString())))
            {
                dados[contMatriz, 0] = "CLI_RECADO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_RECADO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_RECADO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliRecado == "" ? "null" : "'" + dadosNew.CliRecado + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_recado", dadosNew.CliRecado);
            }
            if (!dadosNew.CliEndercob.ToUpper().Equals(dadosOld.Rows[0]["CLI_ENDERCOB"]))
            {
                dados[contMatriz, 0] = "CLI_ENDERCOB";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_ENDERCOB"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliEndercob + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_endercob", dadosNew.CliEndercob);
            }
            if (!dadosNew.CliBairrocob.Equals(dadosOld.Rows[0]["CLI_BAIRROCOB"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_BAIRROCOB";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_BAIRROCOB"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_BAIRROCOB"] + "'";
                dados[contMatriz, 2] = dadosNew.CliBairrocob == "" ? "null" : "'" + dadosNew.CliBairrocob + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_bairrocob", dadosNew.CliBairrocob);
            }
            if (!dadosNew.CliCidadecob.Equals(dadosOld.Rows[0]["CLI_CIDADECOB"]))
            {
                dados[contMatriz, 0] = "CLI_CIDADECOB";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_CIDADECOB"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliCidadecob + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cidadecob", dadosNew.CliCidadecob);
            }
            if (!dadosNew.CliCepcob.Equals(dadosOld.Rows[0]["CLI_CEPCOB"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CEPCOB";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_CEPCOB"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliCepcob + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cepcob", dadosNew.CliCepcob);
            }
            if (!dadosNew.CliUfcob.Equals(dadosOld.Rows[0]["CLI_UFCOB"]))
            {
                dados[contMatriz, 0] = "CLI_UFCOB";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_UFCOB"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliUfcob + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_ufcob", dadosNew.CliUfcob);
            }
            if (!dadosNew.CliEnderent.Equals(dadosOld.Rows[0]["CLI_ENDERENT"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_ENDERENT";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_ENDERENT"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_ENDERENT"] + "'";
                dados[contMatriz, 2] = dadosNew.CliEnderent == "" ? "null" : "'" + dadosNew.CliEnderent + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_enderent", dadosNew.CliEnderent);
            }
            if (!dadosNew.CliBairroent.Equals(dadosOld.Rows[0]["CLI_BAIRROENT"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_BAIRROENT";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_BAIRROENT"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_BAIRROENT"] + "'";
                dados[contMatriz, 2] = dadosNew.CliBairroent == "" ? "null" : "'" + dadosNew.CliBairroent + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_bairroent", dadosNew.CliBairroent);
            }
            if (!dadosNew.CliCidadeent.Equals(dadosOld.Rows[0]["CLI_CIDADEENT"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CIDADEENT";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CIDADEENT"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CIDADEENT"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCidadeent == "" ? "null" : "'" + dadosNew.CliCidadeent + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cidadeent", dadosNew.CliCidadeent);
            }
            if (!dadosNew.CliCepent.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CLI_CEPENT"].ToString())))
            {
                dados[contMatriz, 0] = "CLI_CEPENT";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CEPENT"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CEPENT"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCepent == "" ? "null" : "'" + dadosNew.CliCepent + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cepent", dadosNew.CliCepent);
            }
            if (!dadosNew.CliUfent.Equals(dadosOld.Rows[0]["CLI_UFENT"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_UFENT";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_UFENT"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_UFENT"] + "'";
                dados[contMatriz, 2] = dadosNew.CliUfent == "" ? "null" : "'" + dadosNew.CliUfent + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_ufent", dadosNew.CliUfent);
            }
            if (!dadosNew.CliEmail.Equals(dadosOld.Rows[0]["CLI_EMAIL"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_EMAIL";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_EMAIL"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_EMAIL"] + "'";
                dados[contMatriz, 2] = dadosNew.CliEmail == "" ? "null" : "'" + dadosNew.CliEmail + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_email", dadosNew.CliEmail);
            }
            if (!dadosNew.CliContato.Equals(dadosOld.Rows[0]["CLI_CONTATO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CONTATO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CONTATO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CONTATO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliContato == "" ? "null" : "'" + dadosNew.CliContato + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_contato", dadosNew.CliContato);
            }
            if (!dadosNew.CliCondicao.Equals(dadosOld.Rows[0]["CLI_CONDICAO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CONDICAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_CONDICAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_CONDICAO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCondicao == "" ? "null" : "'" + dadosNew.CliCondicao + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_condicao", dadosNew.CliCondicao);
            }
            if (!dadosNew.CliConCodigo.Equals(dadosOld.Rows[0]["CLI_CON_CODIGO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_CON_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_CON_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliConCodigo + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_con_codigo", dadosNew.CliConCodigo);
            }
            if (!dadosNew.CliCodCartao.Equals(dadosOld.Rows[0]["CLI_COD_CARTAO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_COD_CARTAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_COD_CARTAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_COD_CARTAO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliCodCartao == "" ? "null" : "'" + dadosNew.CliCodCartao + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_cod_cartao", dadosNew.CliCodCartao);
            }
            if (!String.Format("{0:N}",dadosNew.CliLimite).Equals(String.Format("{0:N}", dadosOld.Rows[0]["CLI_LIMITE"])))
            {
                dados[contMatriz, 0] = "CLI_LIMITE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_LIMITE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliLimite + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_limite", dadosNew.CliLimite);
            }
            if (!dadosNew.CliObservacao.Equals(dadosOld.Rows[0]["CLI_OBSERVACAO"].ToString()))
            {
                dados[contMatriz, 0] = "CLI_OBSERVACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CLI_OBSERVACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CLI_OBSERVACAO"] + "'";
                dados[contMatriz, 2] = dadosNew.CliObservacao == "" ? "null" : "'" + dadosNew.CliObservacao + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_observacao", dadosNew.CliObservacao);
            }
            if (!dadosNew.CliLiberado.Equals(dadosOld.Rows[0]["CLI_LIBERADO"]))
            {
                dados[contMatriz, 0] = "CLI_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CliLiberado + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("cli_liberado", dadosNew.CliLiberado);
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuarioID);
                mont.SetWhere("WHERE CLI_ID = " + dadosNew.CliId, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) == 1)
                {
                    if (Funcoes.LogAlteracao("CLI_ID", Convert.ToString(dadosNew.CliId), Principal.usuarioID, "CLIENTES", dados, contMatriz).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
            #endregion
        }

        public static int ExcluirDados(string clasCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("cli_id", clasCodigo));

            Principal.strCmd = "DELETE FROM CLIENTES WHERE CLI_ID = @cli_id";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);
        }

        public static DataTable GetBuscar(Clientes dadosBusca, out bool todos, out string strOrdem)
        {
            DataTable dt = new DataTable();

            Principal.strSql = "SELECT A.CLI_ID, A.CLI_CPF, A.CLI_NOME, A.CLI_APELIDO, A.CLI_CODIGO, B.GRUPO_DESCR, "
              + "A.CLI_ENDER, A.CLI_BAIRRO, A.CLI_CIDADE, A.CLI_CEP, A.CLI_UF, A.CLI_TELEFONE, A.CLI_CELULAR, A.CLI_RECADO, "
              + "A.CLI_ENDERCOB, A.CLI_BAIRROCOB, A.CLI_CIDADECOB, A.CLI_CEPCOB, A.CLI_UFCOB, A.CLI_ENDERENT, A.CLI_BAIRROENT, "
              + "A.CLI_CIDADEENT, A.CLI_CEPENT, A.CLI_UFENT, A.CLI_EMAIL, A.CLI_CONTATO, C.CPR_DESCR, E.CON_CODIGO, "
              + "A.CLI_COD_CARTAO, A.CLI_LIMITE, A.CLI_OBSERVACAO, A.CLI_LIBERADO, A.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, "
              + "A.DTCADASTRO, D.NOME AS OPCADASTRO ";
            Principal.strSql += "FROM CLIENTES A ";
            Principal.strSql += "LEFT JOIN GRUPO_CLIENTES AS B ON (A.GRUPO_CODIGO = B.GRUPO_CODIGO) ";
            Principal.strSql += "LEFT JOIN FORMAS_PAGTO AS C ON (A.CLI_CONDICAO = C.CPR_CODIGO) ";
            Principal.strSql += "LEFT JOIN CONVENIADAS AS E ON (A.CLI_CON_CODIGO = E.CON_ID), ";
            Principal.strSql += "USUARIO_SYSTEM D WHERE A.OPCADASTRO = D.LOGIN_ID";

            //BUSCA SEM NENHUM FILTRO//
            if (dadosBusca.CliId == 0 && dadosBusca.CliNome == "" && Funcoes.RemoverCaracter(dadosBusca.CliCpf) == "" && dadosBusca.GrupoCodigo == "0" && dadosBusca.CliCidade == ""
                && dadosBusca.CliConCodigo == "" && dadosBusca.CliLiberado == "TODOS")
            {
                if (MessageBox.Show("Está solicitando uma busca sem passar por nenhum filtro\neste processo pode demorar dependendo da quantidade de\nregistros encontados.\nConfirma Solicitação?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    strOrdem = Principal.strSql;
                    Principal.strSql += " ORDER BY A.CLI_ID";
                    todos = true;
                    return BancoDados.GetDataTable(Principal.strSql, null);
                }
                else
                {
                    Principal.strSql = "";
                    strOrdem = "";
                    todos = false;
                    return dt;
                }
            }
            else
            {
                if (dadosBusca.CliId != 0)
                {
                    Principal.strSql += " AND A.CLI_ID = " + dadosBusca.CliId;
                }
                if (dadosBusca.CliNome != "")
                {
                    Principal.strSql += " AND A.CLI_NOME LIKE '%" + dadosBusca.CliNome + "%'";
                }
                if (dadosBusca.CliCpf != "")
                {
                    Principal.strSql += " AND A.CLI_CPF = '" + dadosBusca.CliCpf + "'";
                }
                if (dadosBusca.GrupoCodigo != "0")
                {
                    Principal.strSql += " AND A.GRUPO_CODIGO = " + dadosBusca.GrupoCodigo;
                }
                if (dadosBusca.CliCidade != "")
                {
                    Principal.strSql += " AND A.CLI_CIDADE = '" + dadosBusca.CliCidade + "'";
                }
                if (dadosBusca.CliConCodigo != "")
                {
                    Principal.strSql += " AND E.CON_CODIGO = " + dadosBusca.CliConCodigo;
                }
                if (dadosBusca.CliLiberado != "TODOS")
                {
                    Principal.strSql += " AND A.CLI_LIBERADO = '" + dadosBusca.CliLiberado + "'";
                }
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.CLI_ID";
                todos = true;

                return BancoDados.GetDataTable(Principal.strSql, null);
            }
        }


        public static DataTable GetDadosClientes(string cliente)
        {
            Principal.strSql = "SELECT CLI_ID AS FORN_CODIGO, CLI_NOME AS FORN_RAZAO, CLI_APELIDO AS FORN_FANTASIA, CLI_OBSERVACAO, CLI_CPF AS FORN_CGC,"
                                    + "CLI_ENDER AS FORN_ENDERECO, CLI_BAIRRO AS FORN_BAIRRO, CLI_CIDADE AS FORN_CIDADE, "
                                    + "CLI_UF AS FORN_UF FROM CLIENTES WHERE CLI_NOME LIKE '%" + cliente + "%'";

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

    }
}
