﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ProdutoVencimento
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public string Lote { get; set; }
        public DateTime Validade { get; set; }
        public int Quantidade { get; set; }
        public string EntradaID { get; set; }

        public ProdutoVencimento() { }

        public ProdutoVencimento(int empCodigo, int estCodigo, string prodCodigo, string lote, DateTime validade, int quantidade, string entradaID)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ProdCodigo = prodCodigo;
            this.Lote = lote;
            this.Validade = validade;
            this.Quantidade = quantidade;
            this.EntradaID = entradaID;
        }

        public bool InsereRegistros(ProdutoVencimento dados)
        {
            string lote = dados.Lote == "" ? "0" : dados.Lote; 
            string strCmd = "INSERT INTO PRODUTOS_VENCIMENTO (EMP_CODIGO, EST_CODIGO, PROD_CODIGO, LOTE, VALIDADE, QTDE, "
                + "ENTRADA_ID) VALUES("
                + Principal.empAtual + ","
                + Principal.estAtual + ",'"
                + dados.ProdCodigo + "','"
                + lote + "',"
                + Funcoes.BData(dados.Validade) + ","
                + dados.Quantidade + ",'"
                + dados.EntradaID + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluiProdutoViaEntradaDeNotas(int estCodigo, int empCodigo, string entradaID)
        {
            string strCmd = "DELETE FROM PRODUTOS_VENCIMENTO WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + "AND ENTRADA_ID = '" + entradaID + "'";
            return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }

        public DataTable BuscaPorCodigoLote(string codBarras, string lote)
        {
            string sql = " SELECT * FROM PRODUTOS_VENCIMENTO WHERE PROD_CODIGO = '" + codBarras + "' AND LOTE = '" + lote + "'";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(ProdutoVencimento dadosNovos, DataTable dadosAntigos, string numDocto)
        {
            string sql = " UPDATE PRODUTOS_VENCIMENTO "
                       + " SET  LOTE = '" + dadosNovos.Lote + "'"
                       + " ,VALIDADE = " + Funcoes.BData(dadosNovos.Validade)
                       + " ,QTDE = " + dadosNovos.Quantidade
                       + " WHERE EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO =" + Principal.estAtual
                       + " AND ENTRADA_ID = '" + numDocto + "'";
            if (BancoDados.ExecuteNoQueryTrans(sql, null) != -1)
            {
                string[,] dados = new string[3, 3];
                int contMatriz = 0;
                if (!dadosNovos.Lote.Equals(dadosAntigos.Rows[0]["LOTE"]))
                {
                    dados[contMatriz, 0] = "LOTE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["LOTE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.Lote + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.Validade.Equals(dadosAntigos.Rows[0]["VALIDADE"]))
                {
                    dados[contMatriz, 0] = "VALIDADE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VALIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.Validade + "'";
                }
                if (!dadosNovos.Quantidade.Equals(dadosAntigos.Rows[0]["QTDE"]))
                {
                    dados[contMatriz, 0] = "QTDE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["QTDE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.Quantidade + "'";
                }
                Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosNovos.ProdCodigo.ToString(), Principal.usuario, "PRODUTOS_VENCIMENTO", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return true;
            }

        }
    }
}
