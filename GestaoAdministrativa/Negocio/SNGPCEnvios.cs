﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SNGPCEnvios
    {
        public string HashEnviado { get; set; }
        public string HashRetorno { get; set; }
        public string Aceito { get; set; }
        public DateTime DataInicial { get; set; }
        public DateTime DataFinal { get; set; }
        public DateTime DataRecebimento { get; set; }
        public DateTime DataValidacao { get; set; }
        public int TecCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string MensagemValidacao { get; set; }

        public SNGPCEnvios() { }

        public bool InsereEnvios(SNGPCEnvios dados)
        {

            string sql = " INSERT INTO SNGPC_ENVIOS(HASH_ENVIO, HASH_RETORNO, ACEITO, TEC_CODIGO, EST_CODIGO,MENSAGEMVALIDACAO) VALUES ("
                       + "'" + dados.HashEnviado + "'"
                       + ",'" + dados.HashRetorno + "'"
                       + ",'" + dados.Aceito + "'"
                       + "," + dados.TecCodigo
                       + "," + dados.EstCodigo + ",'"
                       + Funcoes.RemoverAcentuacao(dados.MensagemValidacao) + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaEnvios(SNGPCEnvios dadosNovos)
        {
            string sql = " UPDATE SNGPC_ENVIOS SET "
                       + " ACEITO = '" + dadosNovos.Aceito + "'"
                       + " ,DATA_INICIAL = " + Funcoes.BData(dadosNovos.DataInicial)
                       + " ,DATA_FINAL = " + Funcoes.BData(dadosNovos.DataFinal)
                       + " ,DATA_RECEBIMENTO = " + Funcoes.BData(dadosNovos.DataRecebimento)
                       + " ,DATA_VALIDACAO = " + Funcoes.BData(dadosNovos.DataValidacao)
                       + " ,MENSAGEMVALIDACAO = '" + Funcoes.RemoverAcentuacao(dadosNovos.MensagemValidacao) + "'"
                       + " WHERE HASH_RETORNO = '" + dadosNovos.HashRetorno + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
