﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FProdutosPrecos
    {
        public int ProdID { get; set; }
        public string ProdCodigo { get; set; }
        public double PrecoFP { get; set; }
        public string Habilitado { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }

        public FProdutosPrecos() { }

        public FProdutosPrecos(int prodID, string prodCodigo, double precoFP, string habilitado, string opCadastro, DateTime dtCadastro, 
            string opAlteracao, DateTime dtAlteracao)
        {
            this.ProdID = prodID;
            this.ProdCodigo = prodCodigo;
            this.PrecoFP = precoFP;
            this.Habilitado = habilitado;
            this.OpCadastro = opCadastro;
            this.DtCadastro = dtCadastro;
            this.OpAlteracao = opAlteracao;
            this.DtAlteracao = dtAlteracao;
        }

        public DataTable BuscaDados(string prodCodigo, string liberado, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.PROD_CODIGO, A.PRE_COM_DESC, A.HABILITADO, A.PROD_ID, B.PROD_DESCR, "
                     + "  A.DT_ALTERACAO, A.OP_ALTERACAO, A.DT_CADASTRO, A.OP_CADASTRO, COALESCE(C.PROD_PRECOMPRA,0) AS PROD_PRECOMPRA, "
                     + " COALESCE(C.PROD_ULTCUSME,0) AS PROD_ULTCUSME,  COALESCE(C.PROD_QTDE_UN_COMP_FP,0) AS PROD_QTDE_UN_COMP_FP,  COALESCE(D.PRE_VALOR,0) AS PRE_VALOR"
                     + " FROM FP_PRODUTOS_PRECOS A"
                     + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                     + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                     + " INNER JOIN PRECOS D ON D.PROD_CODIGO = A.PROD_CODIGO"
                     + " WHERE C.EMP_CODIGO = " + Principal.empAtual
                     + " AND C.EST_CODIGO = " + Principal.estAtual
                     + " AND C.EST_CODIGO = D.EST_CODIGO"
                     + " AND C.EMP_CODIGO = D.EMP_CODIGO";

            if (prodCodigo != "")
            {
                strSql += " AND A.PROD_CODIGO = '" + prodCodigo + "'";
            }
            if (liberado != "TODOS")
            {
                strSql += "  AND A.HABILITADO = '" + liberado + "'";
            }
            strOrdem = strSql;
            strSql += " ORDER BY B.PROD_DESCR";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(FProdutosPrecos dados)
        {
            string strCmd = "INSERT INTO FP_PRODUTOS_PRECOS(PROD_CODIGO, PRE_COM_DESC, HABILITADO, PROD_ID,  DT_CADASTRO, OP_CADASTRO) VALUES ('" +
                dados.ProdCodigo + "'," +
                Funcoes.BValor(dados.PrecoFP) + ",'" +
                dados.Habilitado + "'," +
                dados.ProdID + "," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(FProdutosPrecos dadosNovos, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;
            StringBuilder cmd = new StringBuilder();
            cmd.Append("UPDATE FP_PRODUTOS_PRECOS SET ");
            if (!dadosNovos.PrecoFP.Equals(Convert.ToDouble(dadosOld.Rows[0]["PRE_COM_DESC"])))
            {
                cmd.Append("PRE_COM_DESC = " + Funcoes.BValor(dadosNovos.PrecoFP) + ",");
                dados[contMatriz, 0] = "PRE_COM_DESC";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PRE_COM_DESC"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.PrecoFP + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.Habilitado.Equals(dadosOld.Rows[0]["HABILITADO"]))
            {
                cmd.Append("HABILITADO = '" + dadosNovos.Habilitado + "',");
                dados[contMatriz, 0] = "HABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["HABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.Habilitado + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {
                cmd.Append(" DT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",");
                cmd.Append(" OP_ALTERACAO = '" + dadosNovos.OpAlteracao + "' ");
                cmd.Append(" WHERE PROD_CODIGO = '" + dadosNovos.ProdCodigo + "'");
                if (BancoDados.ExecuteNoQuery(cmd.ToString(), null) != -1)
                {
                    Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosNovos.ProdCodigo, dadosNovos.OpAlteracao, "FP_PRODUTOS_PRECOS", dados, contMatriz, Principal.estAtual,Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public bool ExcluiProduto(string prodCodigo)
        {
            string strCmd = "DELETE FROM FP_PRODUTOS_PRECOS WHERE PROD_CODIGO ='" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable AtualizaUnidadeCompra()
        {
            string strSql;

            strSql = "SELECT A.PROD_CODIGO, COALESCE(C.PROD_QTDE_UN_COMP,0) AS PROD_QTDE_UN_COMP"
                     + " FROM FP_PRODUTOS_PRECOS A"
                     + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
