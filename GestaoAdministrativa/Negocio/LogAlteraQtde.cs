﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class LogAlteraQtde
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public DateTime DataAlteracao { get; set; }
        public int QtdeAntigo { get; set; }
        public int QtdeNovo { get; set; }
        public string Usuario { get; set; }

        public LogAlteraQtde() { }

        public LogAlteraQtde(int empCodigo, int estCodigo, string prodCodigo, DateTime dataAlteracao, int qtdeAntigo, int qtdeNovo, string usuario)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ProdCodigo = prodCodigo;
            this.DataAlteracao = dataAlteracao;
            this.QtdeAntigo = qtdeAntigo;
            this.QtdeNovo = qtdeNovo;
            this.Usuario = usuario;
        }

        public bool InsereRegistros(LogAlteraQtde dados)
        {
            string strCmd = "INSERT INTO LOG_ALTERA_QTDE(EMP_CODIGO, EST_CODIGO, PROD_CODIGO, DATA_ALTERA, QTDE_ANTIGO,QTDE_NOVO,USUARIO)" +
                " VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + ",'" +
                dados.ProdCodigo + "'," +
                Funcoes.BDataHora(dados.DataAlteracao) + "," +
                dados.QtdeAntigo + "," +
                dados.QtdeNovo + ",'" +
                dados.Usuario + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
