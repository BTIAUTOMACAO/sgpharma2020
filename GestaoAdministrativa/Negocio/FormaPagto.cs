﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class FormaPagto
    {
        public int EmpCodigo { get; set; }
        public int CprNum { get; set; }
        public string CprDescr { get; set; }
        public string CprOperacao { get; set; }
        public double CprValor { get; set; }
        public string CprApartir { get; set; }
        public string CprCarencia { get; set; }
        public string CprVencto { get; set; }
        public string CprDesabilitado{ get; set; }
        public string CprDiaMes { get; set; }
        public double CprTaxa { get; set; }
        public int CprPos { get; set; }
        public string CprPermParc { get; set; }
        public string CprUtilComanda { get; set; }
        public string CprUtilConvenio { get; set; }
        public string CprUtilParticular { get; set; }
        public string CprViasImpressao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public FormaPagto() { }

        public FormaPagto(int estCodigo, int cprNum, string cprDescr, string cprOperacao, double cprValor,
                           string cprApartir, string cprCarencia, string cprVencto, string cprDesabilitado, string cprDiaMes,
                           DateTime dtAlteracao, double cprTaxa, int cprPos, string cprPermParc, string cprUtiComanda,
                           string cprUtiConvenio, string cprUtiParticular, string cprViasImpressao, string opAlteracao,
                           DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = estCodigo;
            this.CprNum = cprNum;
            this.CprDescr = cprDescr;
            this.CprOperacao = cprOperacao;
            this.CprValor = cprValor;
            this.CprApartir = cprApartir;
            this.CprCarencia = cprCarencia;
            this.CprVencto = cprVencto;
            this.CprDesabilitado = cprDesabilitado;
            this.CprDiaMes = cprDiaMes;
            this.CprTaxa = cprTaxa;
            this.CprPos = cprPos;
            this.CprPermParc = cprPermParc;
            this.CprUtilComanda = cprUtiComanda;
            this.CprUtilConvenio = cprUtiConvenio;
            this.CprUtilParticular = cprUtiParticular;
            this.CprViasImpressao = cprViasImpressao;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InsereRegistros(FormaPagto dados)
        {
            string strCmd = "INSERT INTO COND_PAGTO (EMP_CODIGO, CPR_NUM, CPR_DESCRICAO, CPR_OPERACAO, CPR_VALOR, CPR_APARTIR, "
                + "CPR_CARENCIA, CPR_VENCTO, CPR_DESABILITADO, CPR_DIA_MES, CPR_TAXA, CPR_POS, CPR_PERM_PARC, CPR_UTILIZA_COMANDA, "
                + "CPR_UTILIZA_CONVENIO,CPR_UTILIZA_PARTICULAR, CPR_VIAS_IMPRESSAO,CPR_OP_CADASTRO,CPR_DT_CADASTRO) VALUES("
                + dados.EmpCodigo + ","
                + dados.CprNum + ",'"
                + dados.CprDescr + "','"
                + dados.CprOperacao + "',"
                + Funcoes.BFormataValor(dados.CprValor) + ",'"
                + dados.CprApartir + "','"
                + dados.CprCarencia + "','"
                + dados.CprVencto + "','"
                + dados.CprDesabilitado + "','"
                + dados.CprDiaMes + "',"
                + Funcoes.BFormataValor(dados.CprTaxa) + ","
                + dados.CprPos + ",'"
                + dados.CprPermParc + "','"
                + dados.CprUtilComanda + "','"
                + dados.CprUtilConvenio + "','"
                + dados.CprUtilParticular + "',"
                + dados.CprViasImpressao + ",'"
                + dados.OpCadastro + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(FormaPagto dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[17, 3];
            int contMatriz = 0;
            string strCmd = " UPDATE COND_PAGTO SET ";
            //VERIFICA SE OS DADOS ALTERADOS E INSERE NUMA MATRIZ PARA INCLUSAO NA TABELA LOG_ALTERAÇÕES//
            if (!dadosNew.CprDescr.Equals(dadosOld.Rows[0]["CPR_DESCRICAO"].ToString()))
            {
                strCmd += " CPR_DESCRICAO = '" + dadosNew.CprDescr + "',";

                dados[contMatriz, 0] = "CPR_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_DESCRICAO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprOperacao.Equals(dadosOld.Rows[0]["CPR_OPERACAO"].ToString()))
            {
                strCmd += " CPR_OPERACAO = '" + dadosNew.CprOperacao + "',";

                dados[contMatriz, 0] = "CPR_OPERACAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_OPERACAO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprOperacao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprValor.Equals(Convert.ToDouble(dadosOld.Rows[0]["CPR_VALOR"])))
            {
                strCmd += " CPR_VALOR = " + Funcoes.BFormataValor(dadosNew.CprValor) + ",";

                dados[contMatriz, 0] = "CPR_VALOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_VALOR"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprValor + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprApartir.Equals(dadosOld.Rows[0]["CPR_APARTIR"].ToString()))
            {
                strCmd += " CPR_APARTIR = '" + dadosNew.CprApartir + "',";

                dados[contMatriz, 0] = "CPR_APARTIR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_APARTIR"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprApartir + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprCarencia.Equals(dadosOld.Rows[0]["CPR_CARENCIA"].ToString()))
            {
                strCmd += " CPR_CARENCIA = '" + dadosNew.CprCarencia + "',";

                dados[contMatriz, 0] = "CPR_CARENCIA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_CARENCIA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprCarencia + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprVencto.Equals(dadosOld.Rows[0]["CPR_VENCTO"].ToString()))
            {
                strCmd += " CPR_VENCTO = '" + dadosNew.CprVencto + "',";

                dados[contMatriz, 0] = "CPR_VENCTO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_VENCTO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprVencto + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprDiaMes.Equals(dadosOld.Rows[0]["CPR_DIA_MES"].ToString()))
            {
                strCmd += " CPR_DIA_MES = '" + dadosNew.CprDiaMes + "',";

                dados[contMatriz, 0] = "CPR_DIA_MES";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_DIA_MES"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprDiaMes + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprTaxa.Equals(Convert.ToDouble(dadosOld.Rows[0]["CPR_TAXA"])))
            {
                strCmd += " CPR_TAXA = " + Funcoes.BFormataValor(dadosNew.CprTaxa) + ",";

                dados[contMatriz, 0] = "CPR_TAXA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_TAXA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprTaxa.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprPos.Equals(Convert.ToInt32(dadosOld.Rows[0]["CPR_POS"])))
            {
                strCmd += " CPR_POS = " + Funcoes.BFormataValor(dadosNew.CprTaxa) + ",";

                dados[contMatriz, 0] = "CPR_POS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_POS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprPos + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprPermParc.Equals(dadosOld.Rows[0]["CPR_PERM_PARC"]))
            {
                strCmd += " CPR_PERM_PARC = '" + dadosNew.CprDiaMes + "',";

                dados[contMatriz, 0] = "CPR_PERM_PARC";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_PERM_PARC"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprPermParc + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprUtilComanda.Equals(dadosOld.Rows[0]["CPR_UTILIZA_COMANDA"]))
            {
                strCmd += " CPR_UTILIZA_COMANDA = '" + dadosNew.CprUtilComanda + "',";

                dados[contMatriz, 0] = "CPR_UTILIZA_COMANDA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_UTILIZA_COMANDA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprUtilComanda + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprUtilConvenio.Equals(dadosOld.Rows[0]["CPR_UTILIZA_CONVENIO"]))
            {
                strCmd += " CPR_UTILIZA_CONVENIO = '" + dadosNew.CprUtilConvenio + "',";

                dados[contMatriz, 0] = "CPR_UTILIZA_CONVENIO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_UTILIZA_CONVENIO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprUtilConvenio + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprUtilParticular.Equals(dadosOld.Rows[0]["CPR_UTILIZA_PARTICULAR"]))
            {
                strCmd += " CPR_UTILIZA_PARTICULAR = '" + dadosNew.CprUtilParticular + "',";

                dados[contMatriz, 0] = "CPR_UTILIZA_PARTICULAR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_UTILIZA_PARTICULAR"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprUtilParticular + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprViasImpressao.Equals(dadosOld.Rows[0]["CPR_VIAS_IMPRESSAO"].ToString()))
            {
                strCmd += " CPR_VIAS_IMPRESSAO = " + dadosNew.CprViasImpressao + ",";

                dados[contMatriz, 0] = "CPR_VIAS_IMPRESSAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_VIAS_IMPRESSAO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprViasImpressao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.CprDesabilitado.Equals(dadosOld.Rows[0]["CPR_DESABILITADO"]))
            {
                strCmd += " CPR_DESABILITADO = '" + dadosNew.CprDesabilitado + "',";

                dados[contMatriz, 0] = "CPR_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CPR_DESABILITADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.CprDesabilitado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "CPR_OP_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CPR_OP_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CPR_OP_ALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " CPR_OP_ALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and CPR_NUM = " + dadosNew.CprNum;

                if (BancoDados.ExecuteNoQueryTrans(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("CPR_NUM", dadosNew.CprNum.ToString(), Principal.usuario, "COND_PAGTO", dados, contMatriz, Principal.empAtual, dadosNew.EmpCodigo, true);
                    return true;
                }
                else
                    return false;
            }
            return true;

        }

        public int ExcluirDados(int empCodigo, string cprCodigo)
        {
            string strCmd = "DELETE FROM COND_PAGTO WHERE EMP_CODIGO =" + empCodigo + " AND CPR_NUM = " + cprCodigo;

            return BancoDados.ExecuteNoQueryTrans(strCmd,null);
        }


        public DataTable BuscaDados(int empCodigo, int cprNum, string cprDescr, string cprLiberado, out string strOrdem)
        {
            string strSql = "SELECT A.EMP_CODIGO, A.CPR_NUM, A.CPR_DESCRICAO, A.CPR_OPERACAO, A.CPR_VALOR, A.CPR_APARTIR, A.CPR_CARENCIA, "
                + " A.CPR_VENCTO, CASE WHEN A.CPR_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS CPR_DESABILITADO, A.CPR_DIA_MES, A.DAT_ALTERACAO, A.CPR_TAXA,"
                + " A.CPR_POS, A.CPR_PERM_PARC, A.CPR_UTILIZA_COMANDA, A.CPR_UTILIZA_CONVENIO, A.CPR_UTILIZA_PARTICULAR,"
                + " A.CPR_VIAS_IMPRESSAO, A.CPR_OP_CADASTRO, A.CPR_DT_CADASTRO, A.CPR_OP_ALTERACAO"
                + " FROM COND_PAGTO A  WHERE A.EMP_CODIGO = " + empCodigo;
            //BUSCA SEM NENHUM FILTRO//
            if (cprNum == 0 && cprDescr == "" && cprLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY CPR_NUM";
            }
            else
            {
                if (cprNum != 0)
                {
                    strSql += " AND CPR_NUM = " + cprNum;
                }
                if (cprDescr != "")
                {
                    strSql += " AND CPR_DESCR LIKE '%" + cprDescr + "%'";
                }
                if (cprLiberado != "TODOS")
                {
                    cprLiberado = cprLiberado == "N" ? "S" : "N";
                    strSql += " AND CPR_DESABILITADO = '" + cprLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY CPR_NUM";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

    }
}
