﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class MovEstoque
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long EstIndice { get; set; }
        public string ProdCodigo { get; set; }
        public int EntQtde { get; set; }
        public double EntValor { get; set; }
        public int SaiQtde { get; set; }
        public double SaiValor { get; set; }
        public string OperacaoCodigo { get; set; }
        public DateTime DataMovimento { get; set; }
        public int PcCodigo { get; set; }
        public int CcCodigo { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public MovEstoque() { }


        public bool ManutencaoMovimentoEstoque(string opcao, MovEstoque dadosMovimento)
        {
            string strSql;

            switch (opcao)
            {
                case "I":
                    strSql = "INSERT INTO MOV_ESTOQUE(EST_CODIGO, EMP_CODIGO, EST_INDICE, PROD_CODIGO, ENT_QTDE, ENT_VALOR, SAI_QTDE, SAI_VALOR, OPE_CODIGO, DAT_MOVTO, "
                        + "DTCADASTRO, OPCADASTRO) VALUES(" + dadosMovimento.EstCodigo + "," + dadosMovimento.EmpCodigo + "," + dadosMovimento.EstIndice + ",";
                    strSql += dadosMovimento.ProdCodigo == "" ? "null," : "'" + dadosMovimento.ProdCodigo + "',";
                    strSql += dadosMovimento.EntQtde + ",";
                    strSql += Funcoes.BValor(dadosMovimento.EntValor) + ",";
                    strSql += dadosMovimento.SaiQtde + ",";
                    strSql += Funcoes.BValor(dadosMovimento.SaiValor) + ",";
                    strSql += dadosMovimento.OperacaoCodigo == "" ? "null," : "'" + dadosMovimento.OperacaoCodigo + "',";
                    strSql += Funcoes.BData(dadosMovimento.DataMovimento) + ",";
                    strSql += Funcoes.BDataHora(dadosMovimento.DtCadastro) + ",'" + dadosMovimento.OpCadastro + "')";
                    if (BancoDados.ExecuteNoQueryTrans(strSql, null).Equals(-1))
                    {
                        return false;
                    }
                    break;
                case "D":
                    strSql = "DELETE FROM MOV_ESTOQUE WHERE EST_CODIGO = " + dadosMovimento.EstCodigo + " AND EMP_CODIGO = " + dadosMovimento.EmpCodigo + " AND EST_INDICE = " + dadosMovimento.EstIndice;
                    if (BancoDados.ExecuteNoQueryTrans(strSql, null).Equals(-1))
                    {
                        return false;
                    }
                    break;
            }
            return true;
        }
    }
}
