﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaVendas
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int NumTrans { get; set; }
        public int NSU { get; set; }
        public int NumDoc { get; set; }
        public int PedNumero { get; set; }
        public string Cartao { get; set; }
        public string Identificacao { get; set; }
        public int Tp_conv { get; set; }
        public int ECF { get; set; }
        public string PDV { get; set; }
        public DateTime DtVenda { get; set; }

        public EpharmaVendas() { }

        public bool InsereVenda(EpharmaVendas dados)
        {
            string sql = " INSERT INTO EPHARMA_VENDA( EMP_CODIGO, EST_CODIGO, NUM_TRANS, NSU, NUM_DOC, PED_NUMERO, CARTAO, "
                       + " IDENTIFICACAO, TP_CONV, ECF , PDV, DT_VENDA) VALUES ("
                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.NumTrans + ","
                       + dados.NSU + ","
                       + dados.NumDoc + ","
                       + dados.PedNumero + ",'"
                       + dados.Cartao + "','"
                       + dados.Identificacao + "',"
                       + dados.Tp_conv + ","
                       + dados.ECF + ",'"
                       + dados.PDV + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ")";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }


}
