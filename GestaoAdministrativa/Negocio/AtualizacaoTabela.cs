﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    /// <summary>
    /// Classe para criar tabelas, campos, views no banco de dados
    /// </summary>
    public class AtualizacaoTabela
    {
        /// <summary>
        /// Identifica se Tabela já existe
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <returns></returns>
        public int IdentificaTabelaExistente(string nomeTabela)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM USER_TABLES WHERE TABLE_NAME = '" + nomeTabela + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Verifica se Synonyms está criado
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <returns></returns>
        public int IdentificaSynonyms(string nomeTabela)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM ALL_SYNONYMS WHERE SYNONYM_NAME = '" + nomeTabela + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Cria Synonyms
        /// </summary>
        public void VerificaSynonyms()
        {
            DataTable dtTabelas = BancoDados.GetDataTable(" SELECT TABLE_NAME FROM USER_TABLES ", null);

            for (int i = 0; i < dtTabelas.Rows.Count; ++i)
            {
                if (IdentificaSynonyms(dtTabelas.Rows[i]["TABLE_NAME"].ToString()).Equals(0))
                {
                    CriaSynonyms(dtTabelas.Rows[i]["TABLE_NAME"].ToString());
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Identifica se Coluna já foi criada na tabela
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <param name="nomeColuna">Nome da Coluna</param>
        /// <returns></returns>
        public int IdentificaColunaExistente(string nomeTabela, string nomeColuna)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM USER_TAB_COLUMNS WHERE TABLE_NAME = '" + nomeTabela + "' AND COLUMN_NAME = '" + nomeColuna + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Cria Synonyms
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        public void CriaSynonyms(string nomeTabela)
        {
            BancoDados.ExecuteNoQuery("CREATE PUBLIC SYNONYM " + nomeTabela + " FOR GADM." + nomeTabela, null);
        }

        /// <summary>
        /// Identifica se o tipo do campo para poder efetuar modificação, utilizado para aumentar tamanho, modificar o tipo de dados, etc.
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <param name="nomeCampo">Nome da Coluna</param>
        /// <returns></returns>
        public DataTable IdentificaAlteracaoTipoCampo(string nomeTabela, string nomeCampo)
        {
            DataTable r = new DataTable();
            return r = BancoDados.GetDataTable("SELECT * FROM USER_TAB_COLUMNS WHERE TABLE_NAME = '" + nomeTabela + "' AND COLUMN_NAME = '" + nomeCampo + "'", null);
        }

        /// <summary>
        /// Script de alteração no Banco de Dados, roda quando faz a primeira migração
        /// </summary>
        /// <param name="formulario"></param>
        public void ScriptAlteracaoBancoDeDados(frmAtualiza formulario)
        {
            try
            {
                string strSql;
                DataTable dt = new DataTable();

                VerificaSynonyms();
                
                #region SCRIPT USUARIO_SYSTEM
                formulario.lblMsg.Text = "TABELA USUARIO SYSTEM";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("USUARIO_SYSTEM", "LIBERADO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD LIBERADO char(1)", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "GRUPO_ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD GRUPO_ID int", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("USUARIO_SYSTEM", "ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE USUARIO_SYSTEM ADD ID int", null);
                    AtualizaID("USUARIO_SYSTEM", "ID", "LOGIN_ID", "0", true);
                }
                #endregion

                #region SCRIPT PRODUTOS
                formulario.lblMsg.Text = "TABELA PRODUTOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PRODUTOS", "PROD_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD PROD_ID INT NULL", null);
                    AtualizaID("PRODUTOS", "PROD_ID", "PROD_CODIGO", "0", true);
                }

                if (IdentificaColunaExistente("PRODUTOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PRODUTOS", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD DTALTERACAO DATE NULL", null);
               
                if (IdentificaColunaExistente("PRODUTOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS", "NCM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD NCM varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS", "PRO_TIPO_CONTROLADO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD PRO_TIPO_CONTROLADO varchar2(1) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS", "PROD_CAIXA_CARTELADO").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD PROD_CAIXA_CARTELADO varchar2(1) default 'N'", null);
                    BancoDados.ExecuteNoQuery("UPDATE PRODUTOS SET PROD_CAIXA_CARTELADO = 'N'", null);
                }

                if (IdentificaColunaExistente("PRODUTOS", "PROD_CODIGO_CARTELADO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD PROD_CODIGO_CARTELADO varchar2(13) NULL", null);
                #endregion

                #region SCRIPT PRODUTOS_DETALHE
                formulario.lblMsg.Text = "TABELA PRODUTOS DETALHE";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ID INT NULL", null);
                    AtualizaIDTabelaExtrangeira("PRODUTOS", "PRODUTOS_DETALHE", "PROD_ID", "PROD_ID", "PROD_CODIGO", true, true, true, false);
                }

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ICMS INTEGER", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_VALOR_BC_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_VALOR_BC_ICMS NUMBER(18,2) default 0 ", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_VALOR_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_VALOR_ICMS NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_BC_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_BC_ICMS_ST NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_BC_ICMS_RET").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_BC_ICMS_RET NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ICMS_RET").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ICMS_RET NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ENQ_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ENQ_IPI VARCHAR2(5)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CST_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CST_IPI VARCHAR2(5)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_BASE_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_BASE_IPI NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ALIQ_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ALIQ_IPI NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ORIGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ORIGEM INTEGER", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CST_COFINS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CST_COFINS VARCHAR2(5)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_VALOR_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_VALOR_IPI NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CST_PIS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CST_PIS VARCHAR2(5)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_VALOR_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_VALOR_ICMS_ST NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ALIQ_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ALIQ_ICMS NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CSOSN").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CSOSN VARCHAR2(10)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_DCB").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_DCB VARCHAR2(30)", null);

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_QTDE_UN_COMP_FP").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_QTDE_UN_COMP_FP INTEGER", null);
                    var produto = new ProdutoDetalhe();
                    produto.AtualizaUnidadeCompraFP();
                }

                if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CFOP_DEVOLUCAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CFOP_DEVOLUCAO VARCHAR2(4)", null);
                #endregion

                #region SCRIPT PRECOS
                formulario.lblMsg.Text = "TABELA PRECOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PRECOS", "PROD_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD PROD_ID INT NULL", null);
                    AtualizaIDTabelaExtrangeira("PRODUTOS", "PRECOS", "PROD_ID", "PROD_ID", "PROD_CODIGO", true, true, true, false);
                }

                if (IdentificaColunaExistente("PRECOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRECOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PRECOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRECOS", "PRE_DESCONTO_PORC").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD PRE_DESCONTO_PORC NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRECOS", "PRE_FP_COM_DESC").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD PRE_FP_COM_DESC NUMBER(18,2) default 0", null);

                if (IdentificaColunaExistente("PRECOS", "BLOQUEIA_DESCONTO").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD BLOQUEIA_DESCONTO varchar2(1) default 'N'", null);

                    BancoDados.ExecuteNoQuery("UPDATE PRECOS SET BLOQUEIA_DESCONTO = 'N'", null);
                }

                if (IdentificaColunaExistente("PRECOS", "ATUALIZADO_ABCFARMA").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRECOS ADD ATUALIZADO_ABCFARMA varchar2(1) default 'S'", null);

                    BancoDados.ExecuteNoQuery("UPDATE PRECOS SET ATUALIZADO_ABCFARMA = 'S'", null);
                }
                #endregion

                #region SCRIPT ENTRADA
                formulario.lblMsg.Text = "TABELA ENTRADA";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("ENTRADA", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD ENT_ID INT", null);
                    AtualizaID("ENTRADA", "ENT_ID", "ENT_DOCTO", "0", true, true, true);
                }

                if (IdentificaColunaExistente("ENTRADA", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ENTRADA", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("ENTRADA", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ENTRADA", "ENT_VIA_XML").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD ENT_VIA_XML varchar2(1) NULL", null);

                if (IdentificaColunaExistente("ENTRADA", "COMP_NUMERO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD COMP_NUMERO varchar2(25)", null);
                #endregion

                #region SCRIPT ENTRADA_DETALHES
                formulario.lblMsg.Text = "TABELA ENTRADA DETALHE";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("ENTRADA_DETALHES", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_DETALHES ADD ENT_ID INT", null);
                    AtualizaIDTabelaExtrangeira("ENTRADA", "ENTRADA_DETALHES", "ENT_ID", "ENT_ID", "ENT_DOCTO", true, true, true);
                }

                if (IdentificaColunaExistente("ENTRADA_DETALHES", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_DETALHES ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("ENTRADA_DETALHES", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_DETALHES ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ENTRADA_DETALHES", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_DETALHES ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("ENTRADA_DETALHES", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_DETALHES ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT ENTRADA_ITENS
                formulario.lblMsg.Text = "TABELA ENTRADA ITENS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_ID INT", null);
                    AtualizaIDTabelaExtrangeira("ENTRADA", "ENTRADA_ITENS", "ENT_ID", "ENT_ID", "ENT_DOCTO", true, true, true);
                }

                if (IdentificaColunaExistente("ENTRADA_ITENS", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_VALOR_BC_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_VALOR_BC_ICMS NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_BC_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_BC_ICMS_ST NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_ICMS_ST NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_ICMS_RET").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_ICMS_RET NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_ENQ_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_ENQ_IPI varchar2(5)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_CST_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_CST_IPI varchar2(5)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_BASE_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_BASE_IPI NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_ORIGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_ORIGEM INTEGER", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_CST_COFINS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_CST_COFINS varchar2(5)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_CST_PIS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_CST_PIS varchar2(5)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_LOTE").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_LOTE varchar2(15)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_MARGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_MARGEM NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_VALOR_VENDA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_VALOR_VENDA NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_COMISSAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_COMISSAO NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_PRODUTO_NOVO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_PRODUTO_NOVO varchar2(1) NULL", null);

                dt = IdentificaAlteracaoTipoCampo("ENTRADA_ITENS", "ENT_ICMS");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 3)
                        BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS MODIFY (ENT_ICMS VARCHAR2(3))", null);

                dt = IdentificaAlteracaoTipoCampo("ENTRADA_ITENS", "COMP_NUMERO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 55)
                        BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS MODIFY (COMP_NUMERO VARCHAR2(55))", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_PRE_COMPRA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_PRE_COMPRA NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_PMC").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_PMC NUMBER(18,2)", null);

                if (IdentificaColunaExistente("ENTRADA_ITENS", "ENT_REGISTRO_MS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD ENT_REGISTRO_MS varchar2(13)", null);
                #endregion

                #region SCRIPT SPOOL_ENTRADA_NF
                formulario.lblMsg.Text = "TABELA SPOOL ENTRADA NF";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD ENT_ID INT", null);
                    AtualizaID("SPOOL_ENTRADA_NF", "ENT_ID", "ENT_DOCTO", "0", true, true, true);
                }

                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "ENT_VIA_XML").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD ENT_VIA_XML varchar2(1) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "COMP_NUMERO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD COMP_NUMERO varchar2(25)", null);
                #endregion

                #region SCRIPT SPOOL_ENTRADA_DETALHES_NF
                formulario.lblMsg.Text = "TABELA SPOOL ENTRADA DETALHE";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SPOOL_ENTRADA_DETALHES_NF", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_DETALHES_NF ADD ENT_ID INT", null);
                    AtualizaIDTabelaExtrangeira("SPOOL_ENTRADA_NF", "SPOOL_ENTRADA_DETALHES_NF", "ENT_ID", "ENT_ID", "ENT_DOCTO", true, true, true);
                }

                if (IdentificaColunaExistente("SPOOL_ENTRADA_DETALHES_NF", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_DETALHES_NF ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_DETALHES_NF", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_DETALHES_NF ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_DETALHES_NF", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_DETALHES_NF ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_DETALHES_NF", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_DETALHES_NF ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT SPOOL_ENTRADA_ITENS_NF
                formulario.lblMsg.Text = "TABELA SPOOL ENTRADA ITENS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_ID INT", null);
                    AtualizaIDTabelaExtrangeira("SPOOL_ENTRADA_NF", "SPOOL_ENTRADA_ITENS_NF", "ENT_ID", "ENT_ID", "ENT_DOCTO", true, true, true);
                }

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_VALOR_BC_ICMS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_VALOR_BC_ICMS NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_BC_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_BC_ICMS_ST NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_ICMS_ST").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_ICMS_ST NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_ICMS_RET").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_ICMS_RET NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_ENQ_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_ENQ_IPI varchar2(5)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_CST_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_CST_IPI varchar2(5)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_BASE_IPI").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_BASE_IPI NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_ORIGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_ORIGEM INTEGER", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_CST_COFINS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_CST_COFINS varchar2(5)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_CST_PIS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_CST_PIS varchar2(5)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_LOTE").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_LOTE varchar2(15)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_MARGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_MARGEM NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_VALOR_VENDA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_VALOR_VENDA NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_COMISSAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_COMISSAO NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_PRODUTO_NOVO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_PRODUTO_NOVO varchar2(1) NULL", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "COMP_NUMERO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD COMP_NUMERO varchar2(20)", null);

                dt = IdentificaAlteracaoTipoCampo("SPOOL_ENTRADA_ITENS_NF", "ENT_ICMS");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 2)
                        BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF MODIFY (ENT_ICMS VARCHAR2(2))", null);

                dt = IdentificaAlteracaoTipoCampo("SPOOL_ENTRADA_ITENS_NF", "COMP_NUMERO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 55)
                        BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF MODIFY (COMP_NUMERO VARCHAR2(55))", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_PRE_COMPRA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_PRE_COMPRA NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_PMC").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_PMC NUMBER(18,2)", null);

                if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "ENT_REGISTRO_MS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD ENT_REGISTRO_MS varchar2(13)", null);
                #endregion

                #region SCRIPT SNGPC_ENTRADAS
                formulario.lblMsg.Text = "TABELA SNGPC ENTRADAS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SNGPC_ENTRADAS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENTRADAS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_ENTRADAS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENTRADAS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_ENTRADAS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENTRADAS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_ENTRADAS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENTRADAS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT CLIFOR
                formulario.lblMsg.Text = "TABELA CLIFOR";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("CLIFOR", "CF_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_ID INT NULL", null);
                    AtualizaID("CLIFOR", "CF_ID", "CF_DOCTO", "0", true);
                }

                if (IdentificaColunaExistente("CLIFOR", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "CF_NUMEROCOB").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_NUMEROCOB varchar2(5) NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "CF_NUMEROENT").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_NUMEROENT varchar2(5) NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "CF_CONTATO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_CONTATO varchar2(50) NULL", null);

                if (IdentificaColunaExistente("CLIFOR", "CF_FONE_RECADO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_FONE_RECADO varchar2(50) NULL", null);

                #endregion

                #region SCRIPT PAGAR
                formulario.lblMsg.Text = "TABELA PAGAR";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PAGAR", "PAG_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD PAG_CODIGO INTEGER", null);

                if (IdentificaColunaExistente("PAGAR", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PAGAR", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PAGAR", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PAGAR", "PAG_ORIGEM").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD PAG_ORIGEM varchar2(1) NULL", null);
                    BancoDados.ExecuteNoQuery("UPDATE PAGAR SET PAG_ORIGEM = 'E'", null);
                }

                if (IdentificaColunaExistente("PAGAR", "PAG_STATUS").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD PAG_STATUS varchar2(1) NULL", null);
                    BancoDados.ExecuteNoQuery("UPDATE PAGAR SET PAG_STATUS = 'N'", null);
                }

                if (IdentificaColunaExistente("PAGAR", "ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD ID INTEGER", null);

                if (IdentificaColunaExistente("PAGAR", "DESP_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD DESP_CODIGO INTEGER", null);

                if (IdentificaColunaExistente("PAGAR", "HISTORICO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR ADD HISTORICO varchar2(50) NULL", null);
                #endregion

                #region SCRIPT PAGAR_DETALHE
                formulario.lblMsg.Text = "TABELA PAGAR DETALHE";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PAGAR_DETALHE", "PAG_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_DETALHE ADD PAG_CODIGO INTEGER", null);

                if (IdentificaColunaExistente("PAGAR_DETALHE", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_DETALHE ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PAGAR_DETALHE", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_DETALHE ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PAGAR_DETALHE", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_DETALHE ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT PAGAR_MOVTO
                formulario.lblMsg.Text = "TABELA PAGAR MOVTO";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PAGAR_MOVTO", "PAG_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD PAG_CODIGO INTEGER", null);

                if (IdentificaColunaExistente("PAGAR_MOVTO", "PM_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD PM_CODIGO INTEGER", null);

                if (IdentificaColunaExistente("PAGAR_MOVTO", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("PAGAR_MOVTO", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PAGAR_MOVTO", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PAGAR_MOVTO", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PAGAR_MOVTO ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT MOV_ESTOQUE
                formulario.lblMsg.Text = "TABELA MOVIMENTO ESTOQUE";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("MOV_ESTOQUE", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MOV_ESTOQUE ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("MOV_ESTOQUE", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MOV_ESTOQUE ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("MOV_ESTOQUE", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MOV_ESTOQUE ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT POS
                formulario.lblMsg.Text = "TABELA POS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("POS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE POS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("POS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE POS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("POS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE POS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT CLASSES
                formulario.lblMsg.Text = "TABELA CLASSES";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("CLASSES", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLASSES ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("CLASSES", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLASSES ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("CLASSES", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CLASSES ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT SUB_CLASSES
                formulario.lblMsg.Text = "TABELA SUBCLASSES";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SUBCLASSES", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SUBCLASSES ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SUBCLASSES", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SUBCLASSES ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SUBCLASSES", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SUBCLASSES ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT NAT_OPERACAO
                formulario.lblMsg.Text = "TABELA NAT OPERACAO";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("NAT_OPERACAO", "NO_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE NAT_OPERACAO ADD NO_ID INT", null);
                    AtualizaID("NAT_OPERACAO", "NO_ID", "NO_CODIGO", "0", false);
                }

                if (IdentificaColunaExistente("NAT_OPERACAO", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE NAT_OPERACAO ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("NAT_OPERACAO", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE NAT_OPERACAO ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("NAT_OPERACAO", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE NAT_OPERACAO ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT PRODUTOS_NCM
                formulario.lblMsg.Text = "TABELA PRODUTOS NCM";
                formulario.lblMsg.Refresh();
                dt = IdentificaAlteracaoTipoCampo("PRODUTOS_NCM", "PROD_CODIGO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 20)
                        BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_NCM MODIFY (PROD_CODIGO VARCHAR2(20))", null);
                #endregion

                #region SCRIPT DEPARTAMENTOS
                formulario.lblMsg.Text = "TABELA DEPARTAMENTOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("DEPARTAMENTOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE DEPARTAMENTOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("DEPARTAMENTOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE DEPARTAMENTOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("DEPARTAMENTOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE DEPARTAMENTOS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT PRINCIPIO_ATIVO
                formulario.lblMsg.Text = "TABELA PRINCIPIO ATIVO";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("PRINCIPIO_ATIVO", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRINCIPIO_ATIVO ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("PRINCIPIO_ATIVO", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRINCIPIO_ATIVO ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PRINCIPIO_ATIVO", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRINCIPIO_ATIVO ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("PRINCIPIO_ATIVO", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRINCIPIO_ATIVO ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT ESPECIFICACOES
                formulario.lblMsg.Text = "TABELA ESPECIFICAÇÕES";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("ESPECIFICACOES", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESPECIFICACOES ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ESPECIFICACOES", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESPECIFICACOES ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("ESPECIFICACOES", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESPECIFICACOES ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("ESPECIFICACOES", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESPECIFICACOES ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region TABELA DE PRECO
                formulario.lblMsg.Text = "TABELA TABELA DE PRECOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("TAB_PRECOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TAB_PRECOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("TAB_PRECOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TAB_PRECOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("TAB_PRECOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TAB_PRECOS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region TABELAS
                formulario.lblMsg.Text = "TABELA MODULO MENU";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("MODULO_MENU").Equals(0))
                {
                    strSql = "CREATE TABLE MODULO_MENU"
                            + "("
                            + " MODULO_ID   int NOT NULL,"
                            + " MENU        varchar(25) NULL,"
                            + " DESCRICAO   varchar2(300) NULL,"
                            + " NOME        varchar2(200) NULL,"
                            + " FORMULARIO  varchar2(200) NULL,"
                            + " PRIMARY KEY(MODULO_ID) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("MODULO_MENU").Equals(0))
                    {
                        CriaSynonyms("MODULO_MENU");
                    }
                }

                formulario.lblMsg.Text = "TABELA PERMISSOES";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("PERMISSOES").Equals(0))
                {
                    strSql = "CREATE TABLE PERMISSOES"
                            + "("
                            + " PERMISSOES_ID  int NOT NULL,"
                            + " GRUPO_ID       int NOT NULL,"
                            + " MODULO_ID      int NOT NULL,"
                            + " ACESSA         varchar2(1) NULL,"
                            + " INCLUI         varchar2(1) NULL,"
                            + " ALTERA         varchar2(1) NULL,"
                            + " EXCLUI         varchar2(1) NULL,"
                            + " DTALTERACAO    DATE NULL,"
                            + " OPALTERACAO    varchar2(25) NULL,"
                            + " DTCADASTRO     DATE NULL,"
                            + " OPCADASTRO     varchar2(25) NULL,"
                            + " PRIMARY KEY(PERMISSOES_ID, GRUPO_ID, MODULO_ID) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("PERMISSOES").Equals(0))
                    {
                        CriaSynonyms("PERMISSOES");
                    }
                }

                formulario.lblMsg.Text = "TABELA GRUPO USUARIOS";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("GRUPO_USUARIOS").Equals(0))
                {
                    strSql = "CREATE TABLE GRUPO_USUARIOS"
                            + "("
                            + "GRUPO_USU_ID    int NOT NULL,"
                            + "DESCRICAO       varchar2(25) NOT NULL,"
                            + "ADMINISTRADOR   varchar2(1) NULL,"
                            + "LIBERADO        varchar2(1) NULL,"
                            + "DTALTERACAO     DATE NULL,"
                            + "OPALTERACAO     varchar2(25) NULL,"
                            + "DTCADASTRO      DATE NULL,"
                            + "OPCADASTRO      varchar2(25) NULL,"
                            + "PRIMARY KEY(GRUPO_USU_ID) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("GRUPO_USUARIOS").Equals(0))
                    {
                        CriaSynonyms("GRUPO_USUARIOS");
                    }

                    var dadosGrupoUsuario = new GrupoDeUsuario
                    {
                        GrupoUsuId = 1,
                        Descricao = "ADMINISTRADORES",
                        Administrador = "S",
                        Liberado = "S",
                        OpCadastro = Principal.usuario,
                        DtCadastro = DateTime.Now,
                    };

                    dadosGrupoUsuario.InsereRegistroGrupoUsuario(dadosGrupoUsuario);
                }

                formulario.lblMsg.Text = "TABELA LOG ALTERACAO";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("LOG_ALTERACAO").Equals(0))
                {
                    strSql = "CREATE TABLE LOG_ALTERACAO"
                            + "("
                            + "LOG_ID         varchar2(60) NOT NULL,"
                            + "LOG_IDVALOR    varchar2(60) NOT NULL,"
                            + "LOG_OPERADOR   varchar2(20) NOT NULL,"
                            + "LOG_OPERACAO   varchar2(10) NOT NULL,"
                            + "LOG_DATA       DATE NULL,"
                            + "LOG_TABELA     varchar2(60) NULL,"
                            + "LOG_CAMPO      varchar2(60) NULL,"
                            + "LOG_ANTERIOR   varchar2(500) NULL,"
                            + "LOG_ALTERADO   varchar2(500) NULL,"
                            + "LOG_MOTIVO     varchar2(500) NULL,"
                            + "EST_CODIGO     int NULL,"
                            + "EMP_CODIGO     int NULL"
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_ALTERACAO").Equals(0))
                    {
                        CriaSynonyms("LOG_ALTERACAO");
                    }
                }

                formulario.lblMsg.Text = "TABELA TIPO UNIDADE";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("TIPO_UNIDADE").Equals(0))
                {
                    strSql = "CREATE TABLE TIPO_UNIDADE"
                            + "("
                            + "UNI_CODIGO      int NOT NULL,"
                            + "UNI_DESCRICAO   varchar2(30) NOT NULL,"
                            + "UNI_DESC_ABREV  varchar2(2) NOT NULL,"
                            + "UNI_LIBERADO    varchar2(1) NULL,"
                            + "DTALTERACAO     DATE NULL,"
                            + "OPALTERACAO     varchar2(25) NULL,"
                            + "DTCADASTRO      DATE NULL,"
                            + "OPCADASTRO      varchar2(25) NULL,"
                            + "PRIMARY KEY(UNI_CODIGO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("TIPO_UNIDADE").Equals(0))
                    {
                        CriaSynonyms("TIPO_UNIDADE");
                    }
                }
                
                if(BancoDados.selecionarRegistros("SELECT * FROM TIPO_UNIDADE").Rows.Count.Equals(0))
                {
                    var tUnidade = new TipoUnidade();

                    tUnidade.UniCodigo = 1;
                    tUnidade.UniDescricao = "UNIDADE";
                    tUnidade.UniDescAbrev = "UN";
                    tUnidade.UniLiberado = "S";
                    tUnidade.DtCadastro = DateTime.Now;
                    tUnidade.OpCadastro = Principal.usuario;

                    tUnidade.InsereRegistros(tUnidade);

                    tUnidade.UniCodigo = 2;
                    tUnidade.UniDescricao = "CAIXA";
                    tUnidade.UniDescAbrev = "CX";
                    tUnidade.UniLiberado = "S";
                    tUnidade.DtCadastro = DateTime.Now;
                    tUnidade.OpCadastro = Principal.usuario;

                    tUnidade.InsereRegistros(tUnidade);

                    tUnidade.UniCodigo = 3;
                    tUnidade.UniDescricao = "FRASCO";
                    tUnidade.UniDescAbrev = "FR";
                    tUnidade.UniLiberado = "S";
                    tUnidade.DtCadastro = DateTime.Now;
                    tUnidade.OpCadastro = Principal.usuario;

                    tUnidade.InsereRegistros(tUnidade);
                }

                formulario.lblMsg.Text = "TABELA FABRICANTES";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("FABRICANTES").Equals(0))
                {
                    strSql = "CREATE TABLE FABRICANTES"
                            + "("
                            + "EST_CODIGO      int NOT NULL,"
                            + "FAB_CODIGO      int NOT NULL,"
                            + "FAB_DESCRICAO   varchar2(80) NOT NULL,"
                            + "FAB_LIBERADO    varchar2(1) NULL,"
                            + "DTALTERACAO     DATE NULL,"
                            + "OPALTERACAO     varchar2(25) NULL,"
                            + "DTCADASTRO      DATE NULL,"
                            + "OPCADASTRO      varchar2(25) NULL,"
                            + "PRIMARY KEY(EST_CODIGO,FAB_CODIGO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("FABRICANTES").Equals(0))
                    {
                        CriaSynonyms("FABRICANTES");
                    }
                }

                formulario.lblMsg.Text = "TABELA DESCONTOS";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("DESCONTOS").Equals(0))
                {
                    strSql = "CREATE TABLE DESCONTOS"
                            + "("
                            + "DESC_CODIGO           int NOT NULL,"
                            + "EMP_CODIGO            int NOT NULL,"
                            + "DESC_DESCR            varchar2(50) NOT NULL,"
                            + "CON_ID                int NULL,"
                            + "PROD_ID               int NULL,"
                            + "DEP_CODIGO            int NULL,"
                            + "CLAS_CODIGO           int NULL,"
                            + "SUB_CODIGO            int NULL,"
                            + "DESC_MIN              numeric(18,2) default 0 NULL,"
                            + "DESC_MAX              numeric(18,2) default 0 NULL,"
                            + "DESC_DESABILITADO     varchar2(1) NULL,"
                            + "DTALTERACAO           DATE,"
                            + "OPALTERACAO           varchar2(25) NULL,"
                            + "DTCADASTRO            DATE,"
                            + "OPCADASTRO            varchar2(25) NULL,"
                            + "PRIMARY KEY(DESC_CODIGO, EMP_CODIGO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("DESCONTOS").Equals(0))
                    {
                        CriaSynonyms("DESCONTOS");
                    }
                }

                formulario.lblMsg.Text = "TABELA PROMOÇÕES";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("PROMOCOES").Equals(0))
                {
                    strSql = "CREATE TABLE PROMOCOES"
                            + "("
                            + "PROMO_CODIGO          int NOT NULL,"
                            + "EMP_CODIGO            int NOT NULL,"
                            + "PROMO_DESCR           varchar2(50) NOT NULL,"
                            + "PROMO_TIPO            varchar2(1) NULL,"
                            + "DATA_INI              varchar2(10) NULL,"
                            + "DATA_FIM              varchar2(10) NULL,"
                            + "DIA_INI               varchar2(2) NULL,"
                            + "DIA_FIM               varchar2(2) NULL,"
                            + "PROD_ID               int NULL,"
                            + "DEP_CODIGO            int NULL,"
                            + "CLAS_CODIGO           int NULL,"
                            + "SUB_CODIGO            int NULL,"
                            + "PROMO_PRECO           numeric(18,2) default 0 NULL,"
                            + "PROMO_PORC            numeric(18,2) default 0 NULL,"
                            + "PROMO_DESABILITADO    varchar2(1) NULL,"
                            + "DTALTERACAO           DATE,"
                            + "OPALTERACAO           varchar2(25) NULL,"
                            + "DTCADASTRO            DATE,"
                            + "OPCADASTRO            varchar2(25) NULL,"
                            + "PRIMARY KEY(PROMO_CODIGO, EMP_CODIGO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("PROMOCOES").Equals(0))
                    {
                        CriaSynonyms("PROMOCOES");
                    }
                }

                formulario.lblMsg.Text = "TABELA GRUPO DE CLIENTES";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("GRUPO_CLIENTES").Equals(0))
                {
                    strSql = "CREATE TABLE GRUPO_CLIENTES"
                            + "("
                            + "GRUPO_CODIGO          int NOT NULL,"
                            + "EMP_CODIGO            int NOT NULL,"
                            + "GRUPO_DESCR           varchar2(30) NOT NULL,"
                            + "GRUPO_DESABILITADO    varchar2(1) NULL,"
                            + "DTALTERACAO           DATE,"
                            + "OPALTERACAO           varchar2(25) NULL,"
                            + "DTCADASTRO            DATE,"
                            + "OPCADASTRO            varchar2(25) NULL,"
                            + "PRIMARY KEY(GRUPO_CODIGO, EMP_CODIGO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("GRUPO_CLIENTES").Equals(0))
                    {
                        CriaSynonyms("GRUPO_CLIENTES");
                    }

                    var grupoCliente = new GrupoDeCliente(
                        1,
                        1,
                        "GERAL",
                        "N",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );

                    grupoCliente.InsereRegistros(grupoCliente);

                    BancoDados.ExecuteNoQuery("UPDATE CLIFOR SET GRU_CODIGO = 1",null);
                }

                formulario.lblMsg.Text = "TABELA SNGPC";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("SNGPC_CONTROLE_VENDAS").Equals(0))
                {
                    strSql = "CREATE TABLE SNGPC_CONTROLE_VENDAS"
                            + "("
                            + "PROD_CODIGO          VARCHAR2(20)        NOT NULL,"
                            + "LOTE                 VARCHAR2(20)        NOT NULL,"
                            + "QTDE                 INT                 NOT NULL,"
                            + "DATA                 DATE                NULL"
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("SNGPC_CONTROLE_VENDAS").Equals(0))
                    {
                        CriaSynonyms("SNGPC_CONTROLE_VENDAS");
                    }
                }

                if (IdentificaColunaExistente("SNGPC_CONTROLE_VENDAS", "DATA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_CONTROLE_VENDAS ADD DATA DATE NULL", null);

                formulario.lblMsg.Text = "TABELA CAIXA";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("CAIXA_ABERTO").Equals(0))
                {
                    strSql = "CREATE TABLE CAIXA_ABERTO"
                            + "("
                            + "EMP_CODIGO           INT                 NOT NULL,"
                            + "EST_CODIGO           INT                 NOT NULL,"
                            + "USUARIO              VARCHAR2(50)        NOT NULL,"
                            + "CAIXA_ABERTO         CHAR(1)             NOT NULL,"
                            + "DATA_ABERTURA        DATE                NOT NULL,"
                            + "DATA_FECHAMENTO      DATE,                "
                            + "PRIMARY KEY(EMP_CODIGO, EST_CODIGO, USUARIO, DATA_ABERTURA) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CAIXA_ABERTO").Equals(0))
                    {
                        CriaSynonyms("CAIXA_ABERTO");
                    }
                }
                #endregion

                #region SCRIPT COLABORADORES
                formulario.lblMsg.Text = "TABELA COLABORADORES";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("COLABORADORES", "COL_DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD COL_DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("COLABORADORES", "COL_SENHA_SUPERVISOR").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD COL_SENHA_SUPERVISOR varchar2(150) NULL", null);

                if (IdentificaColunaExistente("COLABORADORES", "COL_SENHA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD COL_SENHA varchar2(150) NULL", null);

                if (IdentificaColunaExistente("COLABORADORES", "EMP_CODIGO").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD EMP_CODIGO INTEGER NULL", null);
                    BancoDados.ExecuteNoQuery("UPDATE COLABORADORES SET EMP_CODIGO = 1 ", null);
                }

                if (IdentificaColunaExistente("COLABORADORES", "COL_OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD COL_OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("COLABORADORES", "COL_OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COLABORADORES ADD COL_OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT ESTABELECIMENTOS
                formulario.lblMsg.Text = "TABELA ESTABELECIMENTOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("ESTABELECIMENTOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESTABELECIMENTOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("ESTABELECIMENTOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESTABELECIMENTOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("ESTABELECIMENTOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE ESTABELECIMENTOS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region SCRIPT EMPRESA
                formulario.lblMsg.Text = "TABELA EMPRESA";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("EMPRESA", "EMP_NUMERO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE EMPRESA ADD EMP_NUMERO INT NULL", null);

                if (IdentificaColunaExistente("EMPRESA", "EMP_OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE EMPRESA ADD EMP_OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("EMPRESA", "EMP_OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE EMPRESA ADD EMP_OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("EMPRESA", "EMP_DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE EMPRESA ADD EMP_DTCADASTRO DATE NULL", null);

                #endregion

                #region SCRIPT FABRICANTES
                formulario.lblMsg.Text = "TABELA FABRICANTES";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("FABRICANTES", "FAB_OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FABRICANTES ADD FAB_OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("FABRICANTES", "FAB_OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FABRICANTES ADD FAB_OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("FABRICANTES", "FAB_DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FABRICANTES ADD FAB_DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("FABRICANTES", "FAB_DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FABRICANTES ADD FAB_DTALTERACAO DATE NULL", null);
                #endregion

                #region SCRIPT TIPO DOCUMENTO
                formulario.lblMsg.Text = "TABELA TIPO DOCUMENTO";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("TIPO_DOCTO", "TIP_EMITE_NF").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE TIPO_DOCTO ADD TIP_EMITE_NF varchar2(1) NULL", null);
                    BancoDados.ExecuteNoQuery("UPDATE TIPO_DOCTO SET TIP_EMITE_NF = 'N'", null);
                }

                if (IdentificaColunaExistente("TIPO_DOCTO", "TIP_OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TIPO_DOCTO ADD TIP_OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("TIPO_DOCTO", "TIP_OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TIPO_DOCTO ADD TIP_OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("TIPO_DOCTO", "TIP_DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TIPO_DOCTO ADD TIP_DTCADASTRO DATE NULL", null);
                #endregion

                #region SCRIPT CONVENIADAS
                formulario.lblMsg.Text = "TABELA CONVENIADAS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("CONVENIADAS", "CON_OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_OP_ALTERACAO VARCHAR2(25) NULL", null);

                if (IdentificaColunaExistente("CONVENIADAS", "CON_OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_OP_CADASTRO VARCHAR2(25) NULL", null);

                if (IdentificaColunaExistente("CONVENIADAS", "CON_DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("CONVENIADAS", "CON_DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("CONVENIADAS", "CON_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_ID INT NULL", null);
                    AtualizaID("CONVENIADAS", "CON_ID", "CON_CODIGO", "0");
                }

                if (IdentificaColunaExistente("CONVENIADAS", "CON_FORMA_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_FORMA_ID INT NULL", null);

                    BancoDados.ExecuteNoQuery("UPDATE CONVENIADAS SET CON_FORMA_ID = 13 WHERE CON_WEB = 4", null);
                }

                if (IdentificaColunaExistente("CONVENIADAS", "CON_PARCELA").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_PARCELA VARCHAR2(1) NULL", null);

                    BancoDados.ExecuteNoQuery("UPDATE CONVENIADAS SET CON_PARCELA = 'N' WHERE CON_WEB = 4", null);
                }


                #endregion

                #region SCRIPT COMANDAS
                formulario.lblMsg.Text = "TABELA COMANDA";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("CAD_COMANDA").Equals(0))
                {
                    strSql = "CREATE TABLE CAD_COMANDA"
                            + "("
                            + "  EMP_CODIGO     NUMBER(3),"
                            + "  EST_CODIGO     NUMBER(3),"
                            + "  COM_NUMERO     NUMBER(9),"
                            + "  OPCADASTRO     VARCHAR2(25),"
                            + "  DTCADASTRO     DATE,"
                            + " PRIMARY KEY(EMP_CODIGO, EST_CODIGO, COM_NUMERO) "
                            + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CAD_COMANDA").Equals(0))
                    {
                        CriaSynonyms("CAD_COMANDA");
                    }
                }

                if (IdentificaColunaExistente("CAD_COMANDA", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_COMANDA ADD OPCADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("CAD_COMANDA", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_COMANDA ADD DTCADASTRO DATE NULL", null);
                #endregion

                #region SNGPC TECNICOS
                formulario.lblMsg.Text = "TABELA SNGPC";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("SNGPC_TECNICOS", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TECNICOS ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_TECNICOS", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TECNICOS ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_TECNICOS", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TECNICOS ADD OP_ALTERACAO varchar2(25) NULL", null);

                #endregion

                #region SNGPC_TABELAS
                if (IdentificaColunaExistente("SNGPC_TABELAS", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TABELAS ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_TABELAS", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TABELAS ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_TABELAS", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_TABELAS ADD OP_ALTERACAO varchar2(25) NULL", null);

                #endregion

                #region SNGPC_LAC_INVENTARIO

                if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD OP_ALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "LAC_INV_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD LAC_INV_ID INT NULL", null);
                    AtualizaID("SNGPC_LAC_INVENTARIO", "LAC_INV_ID", "LAC_INV_MS", "LAC_INV_LOTE");
                }

                #endregion

                #region SNGPC_PERDA

                if (IdentificaColunaExistente("SNGPC_PERDA", "DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_PERDA ADD DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_PERDA", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_PERDA ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_PERDA", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_PERDA ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_PERDA", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_PERDA ADD OP_ALTERACAO varchar2(25) NULL", null);

                #endregion

                #region SNGPC_ENTRADAS

                if (IdentificaColunaExistente("SNGPC_ENTRADAS", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENTRADAS ADD DTALTERACAO DATE NULL", null);
                #endregion

                #region SNGPC_VENDAS 
                if (IdentificaColunaExistente("SNGPC_VENDAS", "DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_VENDAS  ADD DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_VENDAS", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_VENDAS  ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("SNGPC_VENDAS", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_VENDAS  ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("SNGPC_VENDAS", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_VENDAS  ADD OP_ALTERACAO varchar2(25) NULL", null);

                #endregion

                #region FORMAS_PAGAMENTO
                formulario.lblMsg.Text = "TABELA FORMAS PAGAMENTO";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("FORMAS_PAGAMENTO").Equals(0))
                {
                    strSql = "CREATE TABLE FORMAS_PAGAMENTO"
                        + " ("
                        + "   EMP_CODIGO      INTEGER not null,"
                        + "   FORMA_ID        INTEGER not null,"
                        + "   FORMA_DESCRICAO VARCHAR2(50) not null,"
                        + "   VENCTO_DIA_FIXO VARCHAR2(1) not null,"
                        + "   QTDE_PARCELAS   INTEGER,"
                        + "   DIA_VENCTO_FIXO INTEGER,"
                        + "   QTDE_VIAS       INTEGER,"
                        + "   OPERACAO_CAIXA  VARCHAR2(1) not null,"
                        + "   CONV_BENEFICIO  VARCHAR2(1),"
                        + "   FORMA_LIBERADO  VARCHAR2(1),"
                        + "   OP_ALTERACAO    VARCHAR2(25),"
                        + "   DT_ALTERACAO    DATE,"
                        + "   OP_CADASTRO     VARCHAR2(25),"
                        + "   DT_CADASTRO     DATE,"
                        + "   PRIMARY KEY(EMP_CODIGO, FORMA_ID) "
                        + " )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("FORMAS_PAGAMENTO").Equals(0))
                    {
                        CriaSynonyms("FORMAS_PAGAMENTO");
                    }
                }
                #endregion

                #region PRODUTOS_VENCIMENTO
                formulario.lblMsg.Text = "TABELA PRODUTOS VENCIMENTO ";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("PRODUTOS_VENCIMENTO").Equals(0))
                {
                    strSql = "CREATE TABLE PRODUTOS_VENCIMENTO"
                        + " ("
                        + "   EMP_CODIGO      INTEGER not null,"
                        + "   EST_CODIGO      INTEGER not null,"
                        + "   PROD_CODIGO     VARCHAR2(20) not null,"
                        + "   LOTE            VARCHAR2(20) not null,"
                        + "   VALIDADE        DATE,"
                        + "   QTDE            INTEGER,"
                        + "   ENTRADA_ID      INTEGER,"
                        + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,PROD_CODIGO,LOTE) "
                        + " )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("PRODUTOS_VENCIMENTO").Equals(0))
                    {
                        CriaSynonyms("PRODUTOS_VENCIMENTO");
                    }
                }
                #endregion

                #region VENDAS
                formulario.lblMsg.Text = "TABELAS VENDAS";
                formulario.lblMsg.Refresh();
                if (IdentificaTabelaExistente("VENDAS").Equals(0))
                {
                    strSql = "CREATE TABLE VENDAS"
                               + "("
                               + "   EMP_CODIGO                 INTEGER not null,"
                               + "   EST_CODIGO                 INTEGER not null,"
                               + "   VENDA_ID                   INTEGER not null,"
                               + "   VENDA_CF_ID                INTEGER not null,"
                               + "   VENDA_DATA                 DATE not null,"
                               + "   VENDA_EMISSAO              DATE,"
                               + "   VENDA_DATA_HORA            DATE,"
                               + "   VENDA_COL_CODIGO           INTEGER not null,"
                               + "   VENDA_TOTAL                NUMBER(18, 2) not null,"
                               + "   VENDA_SUBTOTAL             NUMBER(18, 2) not null,"
                               + "   VENDA_DIFERENCA            NUMBER(18, 2) not null,"
                               + "   VENDA_QTDE                 INTEGER not null,"
                               + "   VENDA_STATUS               VARCHAR2(1) not null,"
                               + "   VENDA_COMANDA              INTEGER,"
                               + "   VENDA_CARTAO               VARCHAR2(30),"
                               + "   VENDA_AUTORIZACAO          VARCHAR2(15),"
                               + "   VENDA_NOME_CARTAO          VARCHAR2(50),"
                               + "   VENDA_CON_CODIGO           INTEGER,"
                               + "   VENDA_EMITIDO              VARCHAR2(1),"
                               + "   VENDA_NUM_NOTA             VARCHAR2(15),"
                               + "   VENDA_ENTREGA              VARCHAR2(1),"
                               + "   MOVIMENTO_FINANCEIRO_SEQ   NUMBER,"
                               + "   VENDA_NF_DEVOLUCAO         VARCHAR2(1),"
                               + "   VENDA_MEDICO               VARCHAR2(20),"
                               + "   VENDA_NUM_RECEITA          VARCHAR2(20),"
                               + "   VENDA_BENEFICIO            VARCHAR2(20),"
                               + "   VENDA_TRANS_ID             VARCHAR2(10),"
                               + "   VENDA_TRANS_ID_RECEITA     VARCHAR2(10),"
                               + "   VENDA_AUTORIZACAO_RECEITA  VARCHAR2(15),"
                               + "   VENDA_CARTAO_CODIGO        VARCHAR2(15),"
                               + "   VENDA_AJUSTE_GERAL         NUMBER(18, 2),"
                               + "   VENDA_CONVENIO_PARCELA     INTEGER,"
                               + "   VENDA_OBRIGA_RECEITA       VARCHAR2(1),"
                               + "   OP_CADASTRO                VARCHAR2(25),"
                               + "   DT_CADASTRO                DATE,"
                               + "   OP_ALTERACAO               VARCHAR2(25),"
                               + "   DT_ALTERACAO               DATE,"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,VENDA_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("VENDAS").Equals(0))
                    {
                        CriaSynonyms("VENDAS");
                    }
                }

                if (IdentificaColunaExistente("VENDAS", "VENDA_CONVENIO_PARCELA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE VENDAS ADD VENDA_CONVENIO_PARCELA INTEGER NULL", null);

                if (IdentificaColunaExistente("VENDAS", "VENDA_OBRIGA_RECEITA").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE VENDAS ADD VENDA_OBRIGA_RECEITA VARCHAR2(1) NULL", null);

                    BancoDados.ExecuteNoQuery("UPDATE VENDAS SET VENDA_OBRIGA_RECEITA = 'N'", null);
                }
                #endregion

                #region VENDAS_ITENS
                if (IdentificaTabelaExistente("VENDAS_ITENS").Equals(0))
                {
                    strSql = "CREATE TABLE VENDAS_ITENS"
                               + "("
                               + "     EMP_CODIGO           INTEGER not null,"
                               + "     EST_CODIGO           INTEGER not null,"
                               + "     VENDA_ID             INTEGER not null,"
                               + "     VENDA_ITEM           INTEGER not null,"
                               + "     PROD_ID              INTEGER not null,"
                               + "     PROD_CODIGO          VARCHAR2(20) not null,"
                               + "     VENDA_ITEM_QTDE      INTEGER not null,"
                               + "     VENDA_ITEM_UNITARIO  NUMBER(18, 2),"
                               + "     VENDA_ITEM_SUBTOTAL  NUMBER(18, 2) not null,"
                               + "     VENDA_ITEM_DIFERENCA NUMBER(18, 2),"
                               + "     VENDA_ITEM_TOTAL     NUMBER(18, 2) not null,"
                               + "     VENDA_ITEM_UNIDADE   VARCHAR2(2),"
                               + "     VENDA_PRE_VALOR      NUMBER(18, 2),"
                               + "     VENDA_PROMOCAO       VARCHAR2(1),"
                               + "     VENDA_DESC_LIBERADO  VARCHAR2(1),"
                               + "     VENDA_ITEM_LOTE      VARCHAR2(10),"
                               + "     VENDA_ITEM_RECEITA   VARCHAR2(1),"
                               + "     VENDA_ITEM_INDICE    INTEGER,"
                               + "     VENDA_ITEM_COMISSAO  NUMBER(18,2),"
                               + "     OP_CADASTRO          VARCHAR2(25),"
                               + "     DT_CADASTRO          DATE,"
                               + "     OP_ALTERACAO         VARCHAR2(25),"
                               + "     DT_ALTERACAO         DATE,"
                               + "     COL_CODIGO           INTEGER,"
                               + "     PRIMARY KEY(EMP_CODIGO, EST_CODIGO,VENDA_ID,VENDA_ITEM,PROD_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("VENDAS_ITENS").Equals(0))
                    {
                        CriaSynonyms("VENDAS_ITENS");
                    }
                }

                if (IdentificaColunaExistente("VENDAS_ITENS", "COL_CODIGO").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE VENDAS_ITENS ADD COL_CODIGO INTEGER NULL", null);
                }

                
                #endregion

                #region VENDAS_FORMA_PAGAMENTO
                if (IdentificaTabelaExistente("VENDAS_FORMA_PAGAMENTO").Equals(0))
                {
                    strSql = "CREATE TABLE VENDAS_FORMA_PAGAMENTO"
                               + "("
                               + "   EMP_CODIGO                 INTEGER not null,"
                               + "   EST_CODIGO                 INTEGER not null,"
                               + "   VENDA_ID                   INTEGER not null,"
                               + "   VENDA_FORMA_PAGTO_ID       INTEGER not null,"
                               + "   VENDA_FORMA_ID             INTEGER not null,"
                               + "   VENDA_PARCELA              INTEGER,"
                               + "   VENDA_VALOR_PARCELA        NUMBER(18, 2),"
                               + "   VENDA_PARCELA_VENCIMENTO   DATE,"
                               + "   OP_CADASTRO                VARCHAR2(25),"
                               + "   DT_CADASTRO                DATE,"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,VENDA_ID,VENDA_FORMA_PAGTO_ID,VENDA_FORMA_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("VENDAS_FORMA_PAGAMENTO").Equals(0))
                    {
                        CriaSynonyms("VENDAS_FORMA_PAGAMENTO");
                    }
                }
                #endregion

                #region VENDAS_ESPECIES
                if (IdentificaTabelaExistente("VENDAS_ESPECIES").Equals(0))
                {
                    strSql = "CREATE TABLE VENDAS_ESPECIES"
                               + "("
                               + "   EMP_CODIGO                 INTEGER not null,"
                               + "   EST_CODIGO                 INTEGER not null,"
                               + "   VENDA_ID                   INTEGER not null,"
                               + "   VENDA_ESPECIE_ID           INTEGER not null,"
                               + "   ESP_CODIGO                 INTEGER not null,"
                               + "   VENDA_ESPECIE_VALOR        NUMBER(18, 2),"
                               + "   OP_CADASTRO                VARCHAR2(25),"
                               + "   DT_CADASTRO                DATE,"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,VENDA_ID,VENDA_ESPECIE_ID,ESP_CODIGO) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("VENDAS_ESPECIES").Equals(0))
                    {
                        CriaSynonyms("VENDAS_ESPECIES");
                    }
                }
                #endregion

                #region MOVIMENTO_FINANCEIRO
                if (IdentificaTabelaExistente("MOVIMENTO_FINANCEIRO").Equals(0))
                {
                    strSql = "CREATE TABLE MOVIMENTO_FINANCEIRO"
                               + "("
                               + "   EMP_CODIGO                     INTEGER not null,"
                               + "   EST_CODIGO                     INTEGER not null,"
                               + "   MOVIMENTO_FINANCEIRO_ID        NUMBER not null,"
                               + "   MOVIMENTO_FINANCEIRO_DATA      DATE,"
                               + "   MOVIMENTO_FINANCEIRO_DATAHORA  DATE,"
                               + "   MOVIMENTO_FINANCEIRO_TIPO      VARCHAR2(1),"
                               + "   MOVIMENTO_FINANCEIRO_USUARIO   VARCHAR2(25),"
                               + "   DT_CADASTRO                    DATE,"
                               + "   OP_CADASTRO                    VARCHAR2(25),"
                               + "   MOVIMENTO_FINANCEIRO_VALOR     NUMBER(18, 2),"
                               + "   MOVIMENTO_FINANCEIRO_SEQ       NUMBER,"
                               + "   VENDA_ID                       INTEGER,"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,MOVIMENTO_FINANCEIRO_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("MOVIMENTO_FINANCEIRO").Equals(0))
                    {
                        CriaSynonyms("MOVIMENTO_FINANCEIRO");
                    }
                }
                #endregion

                #region MOVIMENTO_CAIXA
                if (IdentificaTabelaExistente("MOVIMENTO_CAIXA").Equals(0))
                {
                    strSql = "CREATE TABLE MOVIMENTO_CAIXA"
                               + "("
                               + "   EMP_CODIGO                  INTEGER not null,"
                               + "   EST_CODIGO                  INTEGER not null,"
                               + "   MOVIMENTO_CAIXA_ID          NUMBER not null,"
                               + "   MOVIMENTO_CAIXA_DATA        DATE,"
                               + "   MOVIMENTO_CAIXA_ODC_ID      INTEGER ,"
                               + "   MOVIMENTO_CAIXA_ODC_CLASSE  VARCHAR2(1),"
                               + "   MOVIMENTO_CAIXA_VALOR       NUMBER(18,2),"
                               + "   MOVIMENTO_CAIXA_USUARIO     VARCHAR2(25),"
                               + "   MOVIMENTO_FINANCEIRO_ID     NUMBER(18,2),"
                               + "   MOVIMENTO_FINANCEIRO_SEQ    NUMBER(18,2),"
                               + "   MOVIMENTO_CAIXA_OBS         VARCHAR2(25),"
                               + "   DT_CADASTRO                 DATE,"
                               + "   OP_CADASTRO                 VARCHAR2(25),"
                               + "   VENDA_ID                    INTEGER,"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,MOVIMENTO_CAIXA_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("MOVIMENTO_CAIXA").Equals(0))
                    {
                        CriaSynonyms("MOVIMENTO_CAIXA");
                    }
                }
                #endregion

                #region MOVIMENTO_CAIXA_ESPECIE

                if (IdentificaTabelaExistente("MOVIMENTO_CAIXA_ESPECIE").Equals(0))
                {
                    strSql = "CREATE TABLE MOVIMENTO_CAIXA_ESPECIE"
                               + "("
                               + "   EMP_CODIGO                     INTEGER not null,"
                               + "   EST_CODIGO                     INTEGER not null,"
                               + "   MOVIMENTO_CX_ESPECIE_ID        NUMBER not null,"
                               + "   MOVIMENTO_CX_ESPECIE_DATA      DATE,"
                               + "   MOVIMENTO_CX_ESPECIE_CODIGO    INTEGER,"
                               + "   MOVIMENTO_CX_ESPECIE_VALOR     NUMBER(18, 2),"
                               + "   MOVIMENTO_CX_ESPECIE_USUARIO   VARCHAR2(25),"
                               + "   MOVIMENTO_FINANCEIRO_ID        NUMBER,"
                               + "   NUMERO_LOTE_ID                 NUMBER,"
                               + "   DT_CADASTRO                    DATE,"
                               + "   OP_CADASTRO                    VARCHAR2(25),"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,MOVIMENTO_CX_ESPECIE_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("MOVIMENTO_CAIXA_ESPECIE").Equals(0))
                    {
                        CriaSynonyms("MOVIMENTO_CAIXA_ESPECIE");
                    }
                }
                #endregion

                #region COBRANCA
                if (IdentificaTabelaExistente("COBRANCA").Equals(0))
                {
                    strSql = "CREATE TABLE COBRANCA"
                               + "("
                               + "   EMP_CODIGO             INTEGER not null,"
                               + "   EST_CODIGO             INTEGER not null,"
                               + "   COBRANCA_ID            INTEGER not null,"
                               + "   COBRANCA_CF_ID         INTEGER,"
                               + "   COBRANCA_DATA          DATE,"
                               + "   COBRANCA_TOTAL         NUMBER(18, 2),"
                               + "   COBRANCA_FORMA_ID      INTEGER,"
                               + "   COBRANCA_COL_CODIGO    INTEGER,"
                               + "   COBRANCA_VENDA_ID      NUMBER,"
                               + "   COBRANCA_ORIGEM        VARCHAR2(1),"
                               + "   COBRANCA_STATUS        VARCHAR2(1),"
                               + "   COBRANCA_USUARIO       VARCHAR2(25),"
                               + "   DT_CADASTRO            DATE,"
                               + "   OP_CADASTRO            VARCHAR2(25),"
                               + "   DT_ALTERACAO           DATE,"
                               + "   OP_ALTERACAO           VARCHAR2(25),"
                               + "   PRIMARY KEY(EMP_CODIGO, EST_CODIGO,COBRANCA_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("COBRANCA").Equals(0))
                    {
                        CriaSynonyms("COBRANCA");
                    }
                }
                #endregion

                #region SNGPC_ENVIOS
                if (IdentificaTabelaExistente("SNGPC_ENVIOS").Equals(0))
                {
                    strSql = "CREATE TABLE SNGPC_ENVIOS"
                               + "("
                               + " HASH_ENVIO         VARCHAR2(32) not null,"
                               + " HASH_RETORNO       VARCHAR2(32) not null,"
                               + " ACEITO             VARCHAR2(1) not null,"
                               + " DATA_INICIAL       DATE,"
                               + " DATA_FINAL         DATE,"
                               + " DATA_RECEBIMENTO   DATE,"
                               + " DATA_VALIDACAO     DATE,"
                               + " TEC_CODIGO         INTEGER,"
                               + " EST_CODIGO         NUMBER(18,2),"
                               + " PRIMARY KEY(EST_CODIGO, HASH_ENVIO) "
                               + ")";

                    BancoDados.ExecuteNoQuery(strSql, null);
                    BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENVIOS ADD CONSTRAINTS SNGPC_TECNICOS_FK FOREIGN KEY(TEC_CODIGO)REFERENCES SNGPC_TECNICOS(TEC_CODIGO)", null);

                    if (IdentificaSynonyms("SNGPC_ENVIOS").Equals(0))
                    {
                        CriaSynonyms("SNGPC_ENVIOS");
                    }
                }
                #endregion

                #region MOVIMENTO_LOTE
                if (IdentificaTabelaExistente("MOVIMENTO_LOTE").Equals(0))
                {
                    strSql = "CREATE TABLE MOVIMENTO_LOTE"
                               + "("
                               + " EMP_CODIGO                 INTEGER not null,"
                               + " EST_CODIGO                 INTEGER not null,"
                               + " MOVIMENTO_LOTE_NUM_LOTE    INTEGER not null,"
                               + " MOVIMENTO_LOTE_VL_PAGO     NUMBER(18, 2),"
                               + " COBRANCA_PARCELA_ID        INTEGER,"
                               + " COBRANCA_ID                INTEGER,"
                               + " COBRANCA_MOV_ID            INTEGER,"
                               + " MOVIMENTO_CAIXA_ID         INTEGER,"
                               + " MOVIMENTO_LOTE_STATUS      CHAR(1),"
                               + " DT_CADASTRO                DATE,"
                               + " OP_CADASTRO                VARCHAR2(25),"
                               + " DT_ALTERACAO               DATE,"
                               + " OP_ALTERACAO               VARCHAR2(25),"
                               + " PRIMARY KEY(EST_CODIGO, EMP_CODIGO,MOVIMENTO_LOTE_NUM_LOTE) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("MOVIMENTO_LOTE").Equals(0))
                    {
                        CriaSynonyms("MOVIMENTO_LOTE");
                    }
                }
                #endregion

                #region COBRANCA_MOVIMENTO
                if (IdentificaTabelaExistente("COBRANCA_MOVIMENTO").Equals(0))
                {
                    strSql = "CREATE TABLE COBRANCA_MOVIMENTO"
                               + "("
                               + " EMP_CODIGO                   INTEGER not null,"
                               + " EST_CODIGO                   INTEGER not null,"
                               + " COBRANCA_MOV_ID              INTEGER not null,"
                               + " COBRANCA_PARCELA_ID          INTEGER,"
                               + " COBRANCA_ID                  INTEGER,"
                               + " COBRANCA_MOV_DT_BAIXA        DATE,"
                               + " COBRANCA_MOV_VL_PAGO         NUMBER(18, 2),"
                               + " COBRANCA_MOV_VL_ACRESCIMO    NUMBER(18, 2),"
                               + " DT_CADASTRO                  DATE,"
                               + " OP_CADASTRO                  VARCHAR2(25),"
                               + " DT_ALTERACAO                 DATE,"
                               + " OP_ALTERACAO                 VARCHAR2(25),"
                               + " COBRANCA_MOV_VALOR_PARCELA   NUMBER(9, 2),"
                               + " COBRANCA_MOV_VALOR_JUROS     NUMBER(9, 2),"
                               + " PRIMARY KEY(EST_CODIGO, EMP_CODIGO,COBRANCA_MOV_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("COBRANCA_MOVIMENTO").Equals(0))
                    {
                        CriaSynonyms("COBRANCA_MOVIMENTO");
                    }
                }

                if (IdentificaColunaExistente("COBRANCA_MOVIMENTO", "COBRANCA_MOV_VALOR_PARCELA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COBRANCA_MOVIMENTO ADD COBRANCA_MOV_VALOR_PARCELA NUMBER(9, 2) NULL", null);

                if (IdentificaColunaExistente("COBRANCA_MOVIMENTO", "COBRANCA_MOV_VALOR_JUROS").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE COBRANCA_MOVIMENTO ADD COBRANCA_MOV_VALOR_JUROS NUMBER(9, 2) NULL", null);
                #endregion

                #region COBRANCA_PARCELA
                if (IdentificaTabelaExistente("COBRANCA_PARCELA").Equals(0))
                {
                    strSql = "CREATE TABLE COBRANCA_PARCELA"
                               + "("
                               + "   EMP_CODIGO                     INTEGER not null,"
                               + "   EST_CODIGO                     INTEGER not null,"
                               + "   COBRANCA_PARCELA_ID            INTEGER not null,"
                               + "   COBRANCA_ID                    INTEGER,"
                               + "   COBRANCA_PARCELA_NUM           INTEGER,"
                               + "   COBRANCA_PARCELA_VALOR         NUMBER(18, 2),"
                               + "   COBRANCA_PARCELA_SALDO         NUMBER(18, 2),"
                               + "   COBRANCA_PARCELA_STATUS        VARCHAR2(1),"
                               + "   DT_RECEBIMENTO                 DATE,"
                               + "   COBRANCA_PARCELA_VENCIMENTO    DATE,"
                               + "   DT_CADASTRO                    DATE,"
                               + "   OP_CADASTRO                    VARCHAR2(25),"
                               + "   DT_ALTERACAO                   DATE,"
                               + "   OP_ALTERACAO                   VARCHAR2(25),"
                               + "   PRIMARY KEY(EST_CODIGO, EMP_CODIGO,COBRANCA_PARCELA_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("COBRANCA_PARCELA").Equals(0))
                    {
                        CriaSynonyms("COBRANCA_PARCELA");
                    }
                }
                #endregion

                #region  OPERACOES_DC 
                if (IdentificaColunaExistente("OPERACOES_DC", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE OPERACOES_DC ADD OP_ALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("OPERACOES_DC", "DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE OPERACOES_DC ADD DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("OPERACOES_DC", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE OPERACOES_DC ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("OPERACOES_DC", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE OPERACOES_DC ADD OP_CADASTRO varchar2(25) NULL", null);
                #endregion

                #region CUPONS_SAT
                if (IdentificaTabelaExistente("CUPONS_SAT").Equals(0))
                {
                    strSql = "CREATE TABLE CUPONS_SAT"
                               + "("
                               + "    EMP_CODIGO            INTEGER not null,"
                               + "    EST_CODIGO            INTEGER not null,"
                               + "    VENDA_ID              INTEGER not null,"
                               + "    VENDA_NUM_NOTA        VARCHAR2(15) not null,"
                               + "    VENDA_NUM_CFE         VARCHAR2(60),"
                               + "    VENDA_TOTAL           NUMBER(18, 2),"
                               + "    VENDA_EMITIDO         VARCHAR2(1),"
                               + "    VENDA_CANCELADO       VARCHAR2(60),"
                               + "    VENDA_NUMERO_SESSAO   VARCHAR2(25),"
                               + "    DT_CADASTRO           DATE,"
                               + "    OP_CADASTRO           VARCHAR2(25),"
                               + "    DT_ALTERACAO          DATE,"
                               + "    OP_ALTERACAO          VARCHAR2(25),"
                               + "    PRIMARY KEY(EST_CODIGO, EMP_CODIGO,VENDA_ID,VENDA_NUM_NOTA) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CUPONS_SAT").Equals(0))
                    {
                        CriaSynonyms("CUPONS_SAT");
                    }
                }
                #endregion

                #region CONTROLE_ENTREGA
                if (IdentificaTabelaExistente("CONTROLE_ENTREGA").Equals(0))
                {
                    strSql = "CREATE TABLE CONTROLE_ENTREGA"
                               + "("
                               + "   EMP_CODIGO                 INTEGER not null,"
                               + "   EST_CODIGO                 INTEGER not null,"
                               + "   ENT_CONTROLE_ID            INTEGER not null,"
                               + "   VENDA_ID                   INTEGER not null,"
                               + "   CLI_ID                     INTEGER,"
                               + "   COL_CODIGO                 INTEGER,"
                               + "   ENT_CONTROLE_ROMANEIO      INTEGER,"
                               + "   ENT_CONTROLE_STATUS        INTEGER,"
                               + "   ENT_CONTROLE_DATA          DATE,"
                               + "   ENT_CONTROLE_ENDER         VARCHAR2(50),"
                               + "   ENT_CONTROLE_NUMERO        INTEGER,"
                               + "   ENT_CONTROLE_BAIRRO        VARCHAR2(20),"
                               + "   ENT_CONTROLE_CEP           VARCHAR2(9),"
                               + "   ENT_CONTROLE_CIDADE        VARCHAR2(30),"
                               + "   ENT_CONTROLE_UF            VARCHAR2(2),"
                               + "   ENT_CONTROLE_FONE          VARCHAR2(15),"
                               + "   ENT_CONTROLE_VR_TROCO      NUMBER(18, 2),"
                               + "   ENT_CONTROLE_NUM_ENT       VARCHAR2(20),"
                               + "   ENT_CONTROLE_OBSERVACAO    VARCHAR2(100),"
                               + "   DT_CADASTRO                DATE,"
                               + "   OP_CADASTRO                VARCHAR2(25),"
                               + "   DT_ALTERACAO               DATE,"
                               + "   OP_ALTERACAO               VARCHAR2(25),"
                               + "   PRIMARY KEY(EST_CODIGO, EMP_CODIGO,ENT_CONTROLE_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CONTROLE_ENTREGA").Equals(0))
                    {
                        CriaSynonyms("CONTROLE_ENTREGA");
                    }
                }
                #endregion

                #region CONTROLE_ENTREGA_STATUS
                if (IdentificaTabelaExistente("CONTROLE_ENTREGA_STATUS").Equals(0))
                {
                    strSql = "CREATE TABLE CONTROLE_ENTREGA_STATUS"
                               + "("
                               + "   EMP_CODIGO         INTEGER not null,"
                               + "   EST_CODIGO         INTEGER not null,"
                               + "   ENT_ST_ID          INTEGER not null,"
                               + "   ENT_CONTROLE_ID    INTEGER not null,"
                               + "   ENT_ST_SEQUENCIA   INTEGER,"
                               + "   ENT_ST_STATUS      INTEGER,"
                               + "   ENT_ST_DATAHORA    DATE,"
                               + "   ENT_ST_USUARIO     VARCHAR2(25),"
                               + "   ENT_ST_OBSERVACAO  VARCHAR2(100),"
                               + "   DT_CADASTRO        DATE,"
                               + "   OP_CADASTRO        VARCHAR2(25),"
                               + "   DT_ALTERACAO       DATE,"
                               + "   OP_ALTERACAO       VARCHAR2(25),"
                               + "   PRIMARY KEY(EST_CODIGO, EMP_CODIGO,ENT_ST_ID,ENT_CONTROLE_ID) "
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CONTROLE_ENTREGA_STATUS").Equals(0))
                    {
                        CriaSynonyms("CONTROLE_ENTREGA_STATUS");
                    }
                }
                #endregion

                #region PRODUTOS_ABCFARMA
                if (IdentificaColunaExistente("PRODUTOS_ABCFARMA", "CODIGO_ABCFARMA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_ABCFARMA ADD CODIGO_ABCFARMA VARCHAR2(15) NULL", null);

                if (IdentificaColunaExistente("PRODUTOS_ABCFARMA", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_ABCFARMA ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("PRODUTOS_ABCFARMA", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_ABCFARMA ADD OP_CADASTRO varchar2(25) NULL", null);
                #endregion

                #region BENEFICIO E-PHARMA

                if (IdentificaColunaExistente("EPHARMA_COMANDA", "VENDA_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("DROP TABLE EPHARMA_COMANDA", null);
                    if (IdentificaTabelaExistente("EPHARMA_COMANDA").Equals(0))
                    {
                        strSql = " CREATE TABLE EPHARMA_COMANDA"
                                 + "   ("
                                 + "     VENDA_ID      INTEGER,"
                                 + "     NSU           VARCHAR2(12) not null,"
                                 + "     DTCADASTRO    DATE,"
                                 + "     OPCADASTRO    VARCHAR2(25),"
                                 + "     PRIMARY KEY(NSU)"
                                 + "   )";
                        BancoDados.ExecuteNoQuery(strSql, null);

                        if (IdentificaSynonyms("EPHARMA_COMANDA").Equals(0))
                        {
                            CriaSynonyms("EPHARMA_COMANDA");
                        }
                    }
                }

                if (IdentificaTabelaExistente("EPHARMA_VENDA").Equals(0))
                {
                    strSql = "CREATE TABLE EPHARMA_VENDA"
                               + "("
                               + "   EMP_CODIGO            INTEGER not null,"
                               + "   EST_CODIGO            INTEGER not null,"
                               + "   NUM_TRANS             INTEGER not null,"
                               + "   NSU                   INTEGER not null,"
                               + "   NUM_DOC               INTEGER,"
                               + "   VENDA_ID              INTEGER,"
                               + "   CARTAO                VARCHAR2(19) not null,"
                               + "   IDENTIFICACAO         VARCHAR2(30) not null,"
                               + "   TP_CONV               INTEGER,"
                               + "   ECF                   VARCHAR2(9),"
                               + "   PDV                   VARCHAR2(4),"
                               + "   STATUS                CHAR(1),"
                               + "   AUTORIZACAO           VARCHAR(15),"
                               + "   DT_VENDA              DATE"
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("EPHARMA_VENDA").Equals(0))
                    {
                        CriaSynonyms("EPHARMA_VENDA");
                    }
                }

                if (IdentificaTabelaExistente("EPHARMA_ITENS_VENDA").Equals(0))
                {
                    strSql = "CREATE TABLE EPHARMA_ITENS_VENDA"
                               + "("
                               + "   EMP_CODIGO                     INTEGER not null,"
                               + "   EST_CODIGO                     INTEGER not null,"
                               + "   NUM_TRANS                      INTEGER not null,"
                               + "   NSU                            INTEGER not null,"
                               + "   PROD_CODIGO                    VARCHAR2(13) not null,"
                               + "   QTD                            INTEGER not null,"
                               + "   VL_MAXIMO                      NUMBER(18,2),"
                               + "   VL_PFINAL                      NUMBER(18,2),"
                               + "   VL_PFABRICA                    NUMBER(18,2),"
                               + "   VL_AQUISICAO_UNITARIO          NUMBER(18,2),"
                               + "   VL_REPASSE                     NUMBER(18,2),"
                               + "   DT_VENDA                       DATE"
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("EPHARMA_ITENS_VENDA").Equals(0))
                    {
                        CriaSynonyms("EPHARMA_ITENS_VENDA");
                    }
                }

                if (IdentificaTabelaExistente("EPHARMA_CANCELAMENTO").Equals(1))
                    BancoDados.ExecuteNoQuery("DROP TABLE EPHARMA_CANCELAMENTO", null);
                if (IdentificaTabelaExistente("EPHARMA_PENDENTES").Equals(1))
                    BancoDados.ExecuteNoQuery("DROP TABLE EPHARMA_PENDENTES", null);
                if (IdentificaTabelaExistente("EPHARMA_PRODUTOS").Equals(1))
                    BancoDados.ExecuteNoQuery("DROP TABLE EPHARMA_PRODUTOS", null);
                if (IdentificaTabelaExistente("EPHARMA_ITENS").Equals(1))
                    BancoDados.ExecuteNoQuery("DROP TABLE EPHARMA_ITENS", null);

                #endregion

                #region BENEFICIO_VIDA_LINK
                if (IdentificaTabelaExistente("BENEFICIO_VIDA_LINK").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_VIDA_LINK"
                             + "   ("
                             + "     NSU               VARCHAR2(12) not null,"
                             + "     VENDA_ID          INTEGER,"
                             + "     PLANO_CONVENIO    VARCHAR2(8),"
                             + "     CARTAO_CONVENIO   VARCHAR2(30),"
                             + "     NOME              VARCHAR2(40),"
                             + "     TIPO_AUTORIZACAO  INTEGER,"
                             + "     DATA              DATE,"
                             + "     STATUS            VARCHAR2(1),"
                             + "     ESTACAO           VARCHAR2(40),"
                             + "     DTCADASTRO        DATE,"
                             + "     OPCADASTRO        VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_VIDA_LINK").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_VIDA_LINK");
                    }
                }
                #endregion

                #region BENEFICIO_VIDA_LINK_COMANDA
                if (IdentificaTabelaExistente("BENEFICIO_VIDA_LINK_COMANDA").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_VIDA_LINK_COMANDA"
                             + "   ("
                             + "     VENDA_ID      INTEGER,"
                             + "     NSU           VARCHAR2(12) not null,"
                             + "     NUM_SEQ       NUMBER(4),"
                             + "     DTCADASTRO    DATE,"
                             + "     OPCADASTRO    VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_VIDA_LINK_COMANDA").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_VIDA_LINK_COMANDA");
                    }
                }
                #endregion

                #region BENEFICIO_VIDA_LINK_PRODUTOS
                if (IdentificaTabelaExistente("BENEFICIO_VIDA_LINK_PRODUTOS").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_VIDA_LINK_PRODUTOS"
                             + "   ("
                             + "     NSU               VARCHAR2(12) not null,"
                             + "     VENDA_ID          INTEGER,"
                             + "     PROD_CODIGO       VARCHAR2(13) not null,"
                             + "     QTD               INTEGER,"
                             + "     PR_MAX            NUMBER(18, 2),"
                             + "     PR_VENDA          NUMBER(18, 2),"
                             + "     PR_CLIENTE_AVISTA NUMBER(18, 2),"
                             + "     PR_CLIENTE_ARECEB NUMBER(18, 2),"
                             + "     VL_SUBSIDIO       NUMBER(18, 2),"
                             + "     VL_REEMBOLSO      NUMBER(18, 2),"
                             + "     PORCENT_DESC      NUMBER(18, 2),"
                             + "     COMISSAO_VLINK    NUMBER(18, 2),"
                             + "     DATA              DATE,"
                             + "     DTCADASTRO         DATE,"
                             + "     OPCADASTRO        VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU, PROD_CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_VIDA_LINK_PRODUTOS").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_VIDA_LINK_PRODUTOS");
                    }
                }
                #endregion

                #region BENEFICIO_NOVARTIS_PRODUTOS
                if (IdentificaTabelaExistente("BENEFICIO_NOVARTIS_PRODUTOS").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_NOVARTIS_PRODUTOS"
                             + "   ("
                             + "     NSU             VARCHAR2(20) not null,"
                             + "     VENDA_ID        INTEGER,"
                             + "     PROD_CODIGO     VARCHAR2(13) not null,"
                             + "     QTD             INTEGER,"
                             + "     PRE_BRUTO       NUMBER(18,2),"
                             + "     PRE_LIQUIDO     NUMBER(18,2),"
                             + "     VALOR_REC_LOJA  NUMBER(18,2),"
                             + "     VALOR_DESCONTO  NUMBER(18,2),"
                             + "     STATUS          VARCHAR2(1),"
                             + "     CARTAO          VARCHAR2(30),"
                             + "     DATA            DATE,"
                             + "     VALOR_SUBSIDIO  NUMBER(18,2),"
                             + "     REPOSICAO       VARCHAR2(1),"
                             + "     DTCADASTRO      DATE,"
                             + "     OPCADASTRO      VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU,PROD_CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_NOVARTIS_PRODUTOS").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_NOVARTIS_PRODUTOS");
                    }
                }
                #endregion

                #region BENEFICIO_NOVARTIS_EFETIVADO
                if (IdentificaTabelaExistente("BENEFICIO_NOVARTIS_EFETIVADO").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_NOVARTIS_EFETIVADO"
                             + "   ("
                             + "     NSU                  VARCHAR2(20) not null,"
                             + "     VENDA_ID             INTEGER,"
                             + "     LOTE                 VARCHAR2(20),"
                             + "     COD_AUTORIZACAO      VARCHAR2(20),"
                             + "     PRE_BRUTO            NUMBER(18,2),"
                             + "     PRE_LIQUIDO          NUMBER(18,2),"
                             + "     CARTAO               VARCHAR2(30),"
                             + "     OPERADORA            VARCHAR2(30),"
                             + "     VALOR_RECEBER_LOJA   NUMBER(18,2),"
                             + "     DATA                 DATE,"
                             + "     STATUS               VARCHAR2(1),"
                             + "     DTCADASTRO           DATE,"
                             + "     OPCADASTRO           VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_NOVARTIS_EFETIVADO").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_NOVARTIS_EFETIVADO");
                    }
                }
                #endregion

                #region BENEFICIO_NOVARTIS_PENDENTE
                if (IdentificaTabelaExistente("BENEFICIO_NOVARTIS_PENDENTE").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_NOVARTIS_PENDENTE"
                             + "   ("
                             + "     NSU                  VARCHAR2(20) not null,"
                             + "     OPERADORA            VARCHAR2(30),"
                             + "     TIPO_TRANSACAO       VARCHAR2(1),"
                             + "     ESTACAO              VARCHAR(50),"
                             + "     DATA                 DATE,"
                             + "     DTCADASTRO           DATE,"
                             + "     OPCADASTRO           VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_NOVARTIS_PENDENTE").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_NOVARTIS_PENDENTE");
                    }
                }
                #endregion

                #region BENEFICIO_FUNCIONAL_COMANDA
                if (IdentificaTabelaExistente("BENEFICIO_FUNCIONAL_COMANDA").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_FUNCIONAL_COMANDA"
                             + "   ("
                             + "     NSU                  VARCHAR2(20) NOT NULL,"
                             + "     NSU_AUTORIZADOR      VARCHAR2(50) NOT NULL,"
                             + "     CARTAO               VARCHAR2(30),"
                             + "     CONSELHO             VARCHAR2(10),"
                             + "     COD_CONSELHO         VARCHAR2(30),"
                             + "     DATA                 DATE,"
                             + "     HORA                 VARCHAR(15),"
                             + "     TOTAL_VENDA          NUMBER(18,2),"
                             + "     TOTAL_A_PAGAR        NUMBER(18,2),"
                             + "     DESCONTO             NUMBER(18,2),"
                             + "     VALOR_CARTAO         NUMBER(18,2),"
                             + "     VALOR_AVISTA         NUMBER(18,2),"
                             + "     TIPO_TRANSACAO       VARCHAR2(25),"
                             + "     NOME                 VARCHAR2(50),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_FUNCIONAL_COMANDA").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_FUNCIONAL_COMANDA");
                    }
                }
                #endregion

                #region BENEFICIO_NOVARTIS_COMANDA
                if (IdentificaTabelaExistente("BENEFICIO_NOVARTIS_COMANDA").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_NOVARTIS_COMANDA"
                             + "   ("
                             + "     VENDA_ID      INTEGER,"
                             + "     NSU           VARCHAR2(12) not null,"
                             + "     CARTAO        VARCHAR2(30),"
                             + "     DTCADASTRO    DATE,"
                             + "     OPCADASTRO    VARCHAR2(25),"
                             + "     PRIMARY KEY(NSU)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_NOVARTIS_COMANDA").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_NOVARTIS_COMANDA");
                    }
                }
                #endregion

                #region BENEFICIO_ORIZON

                if (IdentificaTabelaExistente("ORIZON_AUTORIZACAO").Equals(0))
                {
                    strSql = " CREATE TABLE ORIZON_AUTORIZACAO"
                             + "   ("
                             + "     VENDA_ID      INTEGER,"
                             + "     AUTORIZACAO   VARCHAR2(12) not null,"
                             + "     NSU_TRANS     VARCHAR2(7),"
                             + "     NSU_HOST      VARCHAR2(12),"
                             + "     STATUS        CHAR(1),"
                             + "     DTCADASTRO    DATE,"
                             + "     OPCADASTRO    VARCHAR2(25),"
                             + "     PRIMARY KEY(AUTORIZACAO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ORIZON_AUTORIZACAO").Equals(0))
                    {
                        CriaSynonyms("ORIZON_AUTORIZACAO");
                    }
                }

                if (IdentificaTabelaExistente("ORIZON_ITENS").Equals(0))
                {
                    strSql = " CREATE TABLE ORIZON_ITENS"
                             + "   ("
                             + "     VENDA_ID               INTEGER,"
                             + "     AUTORIZACAO            VARCHAR2(12),"
                             + "     COD_BARRAS             VARCHAR2(13),"
                             + "     QUATIDADE              INTEGER,"
                             + "     PRECO_CONSUMIDOR       NUMBER(18,2),"
                             + "     PRECO_VENDA            NUMBER(18,2),"
                             + "     PRECO_COM_DESCONTO     NUMBER(18,2),"
                             + "     FLAG                   VARCHAR2(7),"
                             + "     VALOR_REEMBOLSO        NUMBER(18,2),"
                             + "     DTCADASTRO             DATE,"
                             + "     OPCADASTRO             VARCHAR2(25)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ORIZON_ITENS").Equals(0))
                    {
                        CriaSynonyms("ORIZON_ITENS");
                    }
                }

                #endregion

                #region FP_COMANDA
                if (IdentificaColunaExistente("FP_COMANDA", "VENDA_ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_COMANDA ADD VENDA_ID INTEGER NULL", null);

                if (IdentificaColunaExistente("FP_COMANDA", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_COMANDA ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("FP_COMANDA", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_COMANDA ADD OP_CADASTRO varchar2(25) NULL", null);
                #endregion

                #region FP_SOLICITACAO
                if (IdentificaTabelaExistente("FP_SOLICITACAO").Equals(0))
                {
                    strSql = " CREATE TABLE FP_SOLICITACAO"
                             + "   ("
                             + "    SOLICITACAO_ID          VARCHAR2(10) NOT NULL PRIMARY KEY,"
                             + "    DNAESTACAO              VARCHAR2(2000) NOT NULL,"
                             + "    DTEMISSAORECEITA        DATE NOT NULL,"
                             + "    CNPJ                    VARCHAR2(20) NOT NULL,"
                             + "    CPFVENDEDOR             VARCHAR2(20) NOT NULL,"
                             + "    CPFCLIENTE              VARCHAR2(20) NOT NULL,"
                             + "    NOMECLIENTE             VARCHAR2(200) NOT NULL,"
                             + "    CRMMEDICO               VARCHAR2(10) NOT NULL,"
                             + "    UFCRM                   CHAR(2) NOT NULL,"
                             + "    NUMEROAUTORIZACAO       VARCHAR2(30) NOT NULL,"
                             + "    RETORNOSOLICITACAO      VARCHAR2(10) NOT NULL,"
                             + "    MENSAGEMSOLICITACAO     VARCHAR2(400) NOT NULL,"
                             + "    RETORNOCONFIRMACAO      VARCHAR2(10) NULL,"
                             + "    MENSAGEMCONFIRMACAO     VARCHAR2(400) NULL,"
                             + "    RETORNORECEBIMENTO      VARCHAR2(10) NULL,"
                             + "    MENSAGEMRECEBIMENTO     VARCHAR2(400) NULL,"
                             + "    STATUSESTORNO           VARCHAR2(10) NULL,"
                             + "    MENSAGEMESTORNO         VARCHAR2(400) NULL,"
                             + "    CUPOM                   VARCHAR2(2000) NULL,"
                             + "    SITUACAOSOLICITACAO     CHAR(1) NULL,"
                             + "    DATAVENDA               DATE NOT NULL,"
                             + "    DATAALTERACAO           DATE NOT NULL,"
                              + "   VENDA_ID                NUMBER(15) NULL"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("FP_SOLICITACAO").Equals(0))
                    {
                        CriaSynonyms("FP_SOLICITACAO");
                    }
                }
                #endregion

                #region FP_MEDICAMENTOS_SOLICITACAO
                if (IdentificaTabelaExistente("FP_MEDICAMENTOS_SOLICITACAO").Equals(0))
                {
                    strSql = " CREATE TABLE FP_MEDICAMENTOS_SOLICITACAO"
                            + "   ("
                            + "        AUTORIZACAOSOLICITACAO           VARCHAR2(30) NOT NULL,"
                            + "        CODBARRAS                        VARCHAR2(15) NOT NULL,"
                            + "        UNIDAPRESENTACAO                 VARCHAR2(20) NOT NULL,"
                            + "        AUTORIZACAOESTORNO               VARCHAR2(20) NULL,"
                            + "        AUTORIZACAOMEDICAMENTO           VARCHAR2(80) NOT NULL,"
                            + "        QTDAUTORIZADA                    NUMBER(9, 2) NOT NULL,"
                            + "        QTDDEVOLVIDA                     NUMBER(9, 2) NULL,"
                            + "        QTDESTORNADA                     NUMBER(9, 2) NULL,"
                            + "        QTDPRESCRITA                     NUMBER(9, 2) NOT NULL,"
                            + "        QTDSOLICITADA                    NUMBER(9, 2) NOT NULL,"
                            + "        SUBSIDIADOMS                     NUMBER(9, 2) NOT NULL,"
                            + "        SUBSIDIADOMSPOSESTORNO           NUMBER(9, 2) NULL,"
                            + "        SUBSIDIADOCLIENTE                NUMBER(9, 2) NOT NULL,"
                            + "        SUBSIDIADOCLIENTEPOSESTORNO      NUMBER(9, 2) NULL,"
                            + "        PRECOVENDA                       NUMBER(9, 2) NOT NULL,"
                            + "        TOTALVENDAPOSESTORNO             NUMBER(9, 2) NULL"
                            + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("FP_MEDICAMENTOS_SOLICITACAO").Equals(0))
                    {
                        CriaSynonyms("FP_MEDICAMENTOS_SOLICITACAO");
                    }
                }
                #endregion

                #region FP_VENDEDORES
                if (IdentificaColunaExistente("FP_VENDEDORES", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_VENDEDORES ADD OPALTERACAO VARCHAR(25) NULL", null);

                if (IdentificaColunaExistente("FP_VENDEDORES", "DAT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_VENDEDORES ADD DAT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("FP_VENDEDORES", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_VENDEDORES ADD OPCADASTRO VARCHAR(25) NULL", null);

                dt = IdentificaAlteracaoTipoCampo("FP_VENDEDORES", "SENHA_DATASUS");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) != 30)
                        BancoDados.ExecuteNoQuery("ALTER TABLE FP_VENDEDORES MODIFY (SENHA_DATASUS VARCHAR2(30))", null);
                #endregion

                #region Produtos

                if (IdentificaTabelaExistente("LOG_PRODUTOS").Equals(0))
                {
                    strSql = "CREATE TABLE LOG_PRODUTOS"
                               + "("
                               + "   LOG_OPERADOR          VARCHAR2(25),"
                               + "   LOG_OPERACAO          VARCHAR2(20),"
                               + "   COD_BARRAS            VARCHAR2(20),"
                               + "   PROD_DESCR            VARCHAR2(50),"
                               + "   LOG_CAMPO             VARCHAR2(50),"
                               + "   LOG_ANTERIOR          VARCHAR2(100),"
                               + "   LOG_ALTERADO          VARCHAR2(100),"
                               + "   LOG_DATA              DATE,"
                               + "   EST_CODIGO            INT,"
                               + "   EMP_CODIGO            INT"
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_PRODUTOS").Equals(0))
                    {
                        CriaSynonyms("LOG_PRODUTOS");
                    }
                }

                #endregion

                #region CAD_ESPECIES
                if (IdentificaTabelaExistente("CAD_ESPECIES").Equals(0))
                {
                    strSql = "CREATE TABLE CAD_ESPECIES"
                               + "("
                               + "   ESP_CODIGO            INTEGER NOT NULL PRIMARY KEY,"
                               + "   ESP_DESCRICAO         VARCHAR2(20) NOT NULL ,"
                               + "   DAT_ALTERACAO         DATE,"
                               + "   ESP_DESABILITADO      VARCHAR2(1),"
                               + "   ESP_CHEQUE            VARCHAR2(1),"
                               + "   ESP_CAIXA             VARCHAR2(1),"
                               + "   DT_CADASTRO           DATE,"
                               + "   OP_CADASTRO           VARCHAR2(25),"
                               + "   OP_ALTERACAO          VARCHAR2(25),"
                               + "   ESP_VINCULADO         VARCHAR2(1),"
                               + "   ESP_SAT               VARCHAR2(2),"
                               + "   ESP_ECF               VARCHAR2(25)"
                               + ")";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CAD_ESPECIES").Equals(0))
                    {
                        CriaSynonyms("CAD_ESPECIES");
                    }
                }

                if (IdentificaColunaExistente("CAD_ESPECIES", "ESP_ECF").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_ESPECIES ADD ESP_ECF varchar2(25) NULL", null);

                #endregion

                #region LOG_PRECOS
                if (IdentificaTabelaExistente("LOG_PRECOS").Equals(0))
                {
                    strSql = " CREATE TABLE LOG_PRECOS"
                            + "   ("
                            + "        LOG_OPERADOR                     VARCHAR2(30) NOT NULL,"
                            + "        CODBARRAS                        VARCHAR2(15) NOT NULL,"
                            + "        LOG_ANTERIOR                     NUMBER(9, 2) NOT NULL,"
                            + "        LOG_ALTERADO                     NUMBER(9, 2) NOT NULL,"
                            + "        LOG_DATA                         DATE,"
                            + "        EST_CODIGO                       INTEGER,"
                            + "        EMP_CODIGO                       INTEGER"
                            + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_PRECOS").Equals(0))
                    {
                        CriaSynonyms("LOG_PRECOS");
                    }
                }
                #endregion

                #region DROGABELLA_COMANDA
                if (IdentificaTabelaExistente("DROGABELLA_COMANDA").Equals(0))
                {
                    strSql = " CREATE TABLE DROGABELLA_COMANDA"
                             + "   ("
                             + "     VENDA_ID            INTEGER,"
                             + "     VALOR_AUTORIZADO    NUMBER(18,2),"
                             + "     VALOR_DIFERENCA     NUMBER(18,2),"
                             + "     NUMERO_COMANDA      INTEGER,"
                             + "     PRIMARY KEY(VENDA_ID)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("DROGABELLA_COMANDA").Equals(0))
                    {
                        CriaSynonyms("DROGABELLA_COMANDA");
                    }
                }
                #endregion

                #region BENEFICIO_FUNCIONAL_COMANDA
                if (IdentificaTabelaExistente("BENEFICIO_FUNCIONAL_COMANDA").Equals(0))
                {
                    strSql = " CREATE TABLE BENEFICIO_FUNCIONAL_COMANDA"
                             + "   ("
                             + "        NSU VARCHAR2(20)    not null,"
                             + "        NSU_AUTORIZADOR     VARCHAR2(50) not null,"
                             + "        CARTAO              VARCHAR2(30),"
                             + "        CONSELHO            VARCHAR2(10),"
                             + "        COD_CONSELHO        VARCHAR2(30),"
                             + "        DATA                DATE,"
                             + "        HORA                VARCHAR2(15),"
                             + "        TOTAL_VENDA         NUMBER(18, 2),"
                             + "        TOTAL_A_PAGAR       NUMBER(18, 2),"
                             + "        DESCONTO            NUMBER(18, 2),"
                             + "        VALOR_CARTAO        NUMBER(18, 2),"
                             + "        VALOR_AVISTA        NUMBER(18, 2),"
                              + "       TIPO_TRANSACAO      VARCHAR2(25)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("BENEFICIO_FUNCIONAL_COMANDA").Equals(0))
                    {
                        CriaSynonyms("BENEFICIO_FUNCIONAL_COMANDA");
                    }
                }
                #endregion

                #region IBPT
                if (IdentificaTabelaExistente("IBPT").Equals(0))
                {
                    strSql = " CREATE TABLE IBPT"
                             + "   ("
                             + "        NCM VARCHAR2(10),"
                             + "        NACIONAL  NUMBER(18, 2),"
                             + "        IMPORTADO NUMBER(18, 2),"
                             + "        ESTADUAL  NUMBER(18, 2),"
                             + "        MUNICIPAL NUMBER(18, 2)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("IBPT").Equals(0))
                    {
                        CriaSynonyms("IBPT");
                    }
                }
                #endregion

                #region USUARIOS
                if (IdentificaTabelaExistente("USUARIOS").Equals(0))
                {
                    strSql = " create table USUARIOS"
                             + "    ("
                             + "     LOGIN_ID    VARCHAR2(50) NOT NULL,"
                             + "     SENHA       VARCHAR2(50),"
                             + "     SUPERVISOR  VARCHAR2(1),"
                             + "     LIBERADO    CHAR(1),"
                             + "     GRUPO_ID    INTEGER,"
                             + "     DTALTERACAO DATE,"
                             + "     OPALTERACAO VARCHAR2(25),"
                             + "     DTCADASTRO  DATE,"
                             + "     OPCADASTRO  VARCHAR2(25),"
                             + "     ID          INTEGER NOT NULL PRIMARY KEY"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("USUARIOS").Equals(0))
                    {
                        CriaSynonyms("USUARIOS");
                    }

                    Usuario dados = new Usuario(
                        1,
                        "gadm",
                        Funcoes.CriptografaSenha("inicial"),
                        "S",
                        "S",
                        1,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario);
                    dados.InserirDados(dados);
                }

                if (BancoDados.selecionarRegistros("SELECT * FROM USUARIOS").Rows.Count.Equals(1))
                {
                    Usuario dados = new Usuario();

                    Principal.dtBusca = BancoDados.selecionarRegistros("SELECT * FROM USUARIO_SYSTEM WHERE LOGIN_ID <> 'gadm' AND (LIBERADO = 'S' OR LIBERADO IS NULL)");
                    int contador = 2;

                    for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                    {
                        dados.ID = contador;
                        dados.LoginID = Principal.dtBusca.Rows[i]["LOGIN_ID"].ToString();
                        dados.Senha = Funcoes.CriptografaSenha(Principal.dtBusca.Rows[i]["SENHA"].ToString());
                        dados.Liberado = "S";
                        dados.Administrador = "S";
                        dados.GrupoID = 1;
                        dados.DtCadastro = DateTime.Now;
                        dados.OpCadastro = Principal.usuario;
                        dados.InserirDados(dados);

                        contador = contador + 1;
                    }
                }
                #endregion

                #region ALTERACAO_ESTOQUE
                if (IdentificaTabelaExistente("ALTERACAO_ESTOQUE").Equals(0))
                {
                    strSql = " CREATE TABLE ALTERACAO_ESTOQUE("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        ID              INTEGER NOT NULL,"
                             + "        PROD_CODIGO     VARCHAR2(20),"
                             + "        OPERACAO        VARCHAR2(1),"
                             + "        QTDE            INTEGER,"
                             + "        QTDE_ANTERIOR   INTEGER,"
                             + "        OBSERVACAO      VARCHAR2(50),"
                             + "        PROD_CUSME      NUMBER(18,2),"
                             + "        INDICE          INTEGER,"
                             + "        DTCADASTRO      DATE,"
                             + "        OPCADASTRO      VARCHAR2(25),"
                             + "        ESTACAO         VARCHAR2(25),"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,ID)"

                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ALTERACAO_ESTOQUE").Equals(0))
                    {
                        CriaSynonyms("ALTERACAO_ESTOQUE");
                    }
                }
                #endregion

                #region TRANSFERENCIA
                if (IdentificaTabelaExistente("TRANSFERENCIA").Equals(0))
                {
                    strSql = " CREATE TABLE TRANSFERENCIA("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        ID              INTEGER NOT NULL,"
                             + "        OBSERVACAO      VARCHAR2(50),"
                             + "        DTCADASTRO      DATE,"
                             + "        OPCADASTRO      VARCHAR2(25),"
                             + "        ESTACAO         VARCHAR2(25),"
                             + "        LOJA            VARCHAR2(100),"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,ID)"

                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("TRANSFERENCIA").Equals(0))
                    {
                        CriaSynonyms("TRANSFERENCIA");
                    }
                }

                if (IdentificaColunaExistente("TRANSFERENCIA", "CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TRANSFERENCIA ADD CODIGO INTEGER", null);

                if (IdentificaColunaExistente("TRANSFERENCIA", "CODIGO_LOJA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TRANSFERENCIA ADD CODIGO_LOJA INTEGER", null);

                #endregion

                #region TRANSFERENCIA_ITENS
                if (IdentificaTabelaExistente("TRANSFERENCIA_ITENS").Equals(0))
                {
                    strSql = " CREATE TABLE TRANSFERENCIA_ITENS("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        ID              INTEGER NOT NULL,"
                             + "        PROD_CODIGO     VARCHAR2(20),"
                             + "        QTDE            INTEGER,"
                             + "        VALOR           NUMBER(18,2),"
                             + "        DTCADASTRO      DATE,"
                             + "        OPCADASTRO      VARCHAR2(25),"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,ID,PROD_CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("TRANSFERENCIA_ITENS").Equals(0))
                    {
                        CriaSynonyms("TRANSFERENCIA_ITENS");
                    }
                }

                if (IdentificaColunaExistente("TRANSFERENCIA_ITENS", "VALOR").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE TRANSFERENCIA_ITENS ADD VALOR  NUMBER(18,2) NULL", null);

                #endregion

                #region LACAMENTO_FALTAS
                if (IdentificaTabelaExistente("LANCAMENTO_FALTAS").Equals(0))
                {
                    strSql = " CREATE TABLE LANCAMENTO_FALTAS("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        ID              INTEGER NOT NULL,"
                             + "        PROD_CODIGO     VARCHAR2(20),"
                             + "        QTDE            INTEGER,"
                             + "        OBSERVACAO      VARCHAR2(50),"
                             + "        CLIENTE         VARCHAR2(50),"
                             + "        STATUS          VARCHAR2(1),"
                             + "        ESTACAO         VARCHAR2(25),"
                             + "        DTCADASTRO      DATE,"
                             + "        OPCADASTRO      VARCHAR2(25),"
                             + "        DTALTERACAO     DATE,"
                             + "        OPALTERACAO     VARCHAR2(25),"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,ID)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LANCAMENTO_FALTAS").Equals(0))
                    {
                        CriaSynonyms("LANCAMENTO_FALTAS");
                    }
                }

                if (IdentificaColunaExistente("LANCAMENTO_FALTAS", "ID_TRANSFERENCIA").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE LANCAMENTO_FALTAS ADD ID_TRANSFERENCIA INTEGER", null);
                #endregion

                #region MODULO_MENU
                dt = IdentificaAlteracaoTipoCampo("MODULO_MENU", "DESCRICAO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) == 100)
                        BancoDados.ExecuteNoQuery("ALTER TABLE MODULO_MENU MODIFY (DESCRICAO VARCHAR2(300))", null);

                dt = IdentificaAlteracaoTipoCampo("MODULO_MENU", "NOME");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) == 30)
                        BancoDados.ExecuteNoQuery("ALTER TABLE MODULO_MENU MODIFY (NOME VARCHAR2(200))", null);

                dt = IdentificaAlteracaoTipoCampo("MODULO_MENU", "FORMULARIO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) == 30)
                        BancoDados.ExecuteNoQuery("ALTER TABLE MODULO_MENU MODIFY (FORMULARIO VARCHAR2(200))", null);
                #endregion

                #region LOG_LIBERACAO_DESC
                if (IdentificaTabelaExistente("LOG_LIBERACAO_DESC").Equals(0))
                {
                    strSql = " CREATE TABLE LOG_LIBERACAO_DESC("
                             + "     LOG_OPERADOR  VARCHAR2(30),"
                             + "     CODBARRAS     VARCHAR2(15),"
                             + "     PRE_VALOR     NUMBER(9, 2),"
                             + "     QTDE          INTEGER,"
                             + "     DESC_MAX      NUMBER(9, 2),"
                             + "     DESC_LIBERADO NUMBER(9, 2),"
                             + "     VALOR_VENDA   NUMBER(9, 2),"
                             + "     COL_CODIGO    INTEGER,"
                             + "     EST_CODIGO    INTEGER,"
                             + "     EMP_CODIGO    INTEGER,"
                             + "     VENDA_ID      INTEGER,"
                             + "     LOG_DATA      date"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_LIBERACAO_DESC").Equals(0))
                    {
                        CriaSynonyms("LOG_LIBERACAO_DESC");
                    }
                }
                #endregion

                #region PRODUTOS_TROCA
                if (IdentificaTabelaExistente("PRODUTOS_TROCA").Equals(0))
                {
                    strSql = " CREATE TABLE PRODUTOS_TROCA("
                            + "  ID             INTEGER NOT NULL,"
                            + "  EMP_CODIGO     INTEGER NOT NULL,"
                            + "  EST_CODIGO     INTEGER NOT NULL,"
                            + "  PROD_CODIGO    VARCHAR2(13),"
                            + "  QTDE           INTEGER,"
                            + "  PRECO          NUMBER(18, 2),"
                            + "  DESCONTO       NUMBER(18, 2),"
                            + "  TOTAL          NUMBER(18, 2),"
                            + "  OPCADASTRO     VARCHAR2(30),"
                            + "  DTCADASTRO     DATE,"
                            + "  COMANDA        INTEGER,"
                            + "  STATUS         VARCHAR2(1),"
                            + "  VENDA_ID       INTEGER,"
                            + "  COL_CODIGO     INTEGER"
                            + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("PRODUTOS_TROCA").Equals(0))
                    {
                        CriaSynonyms("PRODUTOS_TROCA");
                    }
                }

                if (IdentificaColunaExistente("PRODUTOS_TROCA", "VENDA_ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_TROCA ADD VENDA_ID INTEGER NULL", null);


                if (IdentificaColunaExistente("PRODUTOS_TROCA", "COL_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_TROCA ADD COL_CODIGO INTEGER NULL", null);
                #endregion

                #region PRODUTOS_DEVOLVIDOS
                if (IdentificaTabelaExistente("PRODUTOS_DEVOLVIDOS").Equals(0))
                {
                    strSql = " CREATE TABLE PRODUTOS_DEVOLVIDOS("
                            + "  ID             INTEGER NOT NULL,"
                            + "  EMP_CODIGO     INTEGER NOT NULL,"
                            + "  EST_CODIGO     INTEGER NOT NULL,"
                            + "  PROD_CODIGO    VARCHAR2(13),"
                            + "  QTDE           INTEGER,"
                            + "  PRECO          NUMBER(18, 2),"
                            + "  DESCONTO       NUMBER(18, 2),"
                            + "  TOTAL          NUMBER(18, 2),"
                            + "  OPCADASTRO     VARCHAR2(30),"
                            + "  DTCADASTRO     DATE,"
                            + "  COMANDA        INTEGER,"
                            + "  STATUS         VARCHAR2(1),"
                            + "  VENDA_ID       INTEGER,"
                            + "  COL_CODIGO     INTEGER"
                            + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("PRODUTOS_DEVOLVIDOS").Equals(0))
                    {
                        CriaSynonyms("PRODUTOS_DEVOLVIDOS");
                    }
                }

                if (IdentificaColunaExistente("PRODUTOS_DEVOLVIDOS", "VENDA_ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DEVOLVIDOS ADD VENDA_ID INTEGER NULL", null);


                if (IdentificaColunaExistente("PRODUTOS_DEVOLVIDOS", "COL_CODIGO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DEVOLVIDOS ADD COL_CODIGO INTEGER NULL", null);
                #endregion

                #region DCB
                if (IdentificaTabelaExistente("DCB").Equals(0))
                {
                    strSql = " CREATE TABLE DCB"
                             + "   ("
                             + "        NUM_DCB      VARCHAR2(10),"
                             + "        DESCRICAO    VARCHAR2(150),"
                             + "        CAS          VARCHAR2(50)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("DCB").Equals(0))
                    {
                        CriaSynonyms("DCB");
                    }
                }
                #endregion

                #region FP_PRODUTOS_PRECOS
                if (IdentificaColunaExistente("FP_PRODUTOS_PRECOS", "DT_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_PRODUTOS_PRECOS ADD DT_ALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("FP_PRODUTOS_PRECOS", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_PRODUTOS_PRECOS ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("FP_PRODUTOS_PRECOS", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_PRODUTOS_PRECOS ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("FP_PRODUTOS_PRECOS", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_PRODUTOS_PRECOS ADD OP_ALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("FP_PRODUTOS_PRECOS", "PROD_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE FP_PRODUTOS_PRECOS ADD PROD_ID INT NULL", null);
                    AtualizaIDTabelaExtrangeira("PRODUTOS", "FP_PRODUTOS_PRECOS", "PROD_ID", "PROD_ID", "PROD_CODIGO", true, true, true, false);
                }
                #endregion

                #region VENDAS_CONTADOR
                if (IdentificaTabelaExistente("VENDAS_CONTADOR").Equals(0))
                {
                    strSql = " CREATE TABLE VENDAS_CONTADOR"
                             + "   ("
                             + "        VENDA_ID INTEGER"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("VENDAS_CONTADOR").Equals(0))
                    {
                        CriaSynonyms("VENDAS_CONTADOR");
                    }

                    long vendaID = Funcoes.GeraIDLong("VENDAS", "VENDA_ID", Principal.estAtual, "", Principal.empAtual);

                    var contador = new VendasContador();
                    contador.InserirDados(vendaID);
                }
                #endregion

                #region CAD_DESPESAS
                if (IdentificaColunaExistente("CAD_DESPESAS", "DT_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_DESPESAS ADD DT_CADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("CAD_DESPESAS", "OP_CADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_DESPESAS ADD OP_CADASTRO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("CAD_DESPESAS", "OP_ALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE CAD_DESPESAS ADD OP_ALTERACAO varchar2(25) NULL", null);
                #endregion

                #region NOTAS_FISCAIS_EMITIDAS
                if (IdentificaTabelaExistente("NOTAS_FISCAIS_EMITIDAS").Equals(0))
                {
                    strSql = " CREATE TABLE NOTAS_FISCAIS_EMITIDAS"
                             + "   ("
                             + "         EMP_CODIGO                         INTEGER not null,"
                             + "         EST_CODIGO                         INTEGER not null,"
                             + "         VENDA_ID                           INTEGER not null,"
                             + "         CHAVE_NFE                          VARCHAR2(50) not null,"
                             + "         NUM_LOTE                           VARCHAR2(20),"
                             + "         NUM_RECIBO                         VARCHAR2(20),"
                             + "         NUM_PROTOCOLO                      VARCHAR2(20),"
                             + "         PATH                               VARCHAR2(100),"
                             + "         NUM_CORRECAO                       INTEGER,"
                             + "         NFE_CANCELADA                      VARCHAR2(1),"
                             + "         NFE_DATA                           DATE,"
                             + "         NFE_DEVOLUCAO                      VARCHAR2(1),"
                             + "         OPCADASTRO                         VARCHAR2(30),"
                             + "         DTCADASTRO                         DATE,"
                             + "         STATUS                             VARCHAR2(1),"
                             + "         NFE_DATA_CANCELAMENTO              DATE,"
                             + "         NUM_PROTOCOLO_CCE                  VARCHAR2(20),"
                             + "         NUM_PROTOCOLO_CANCELAMENTO         VARCHAR2(20),"
                             + "         OBSERVACAO                         VARCHAR2(100),"
                             + "         CF_ID                              INTEGER"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("NOTAS_FISCAIS_EMITIDAS").Equals(0))
                    {
                        CriaSynonyms("NOTAS_FISCAIS_EMITIDAS");
                    }
                }

                if (IdentificaColunaExistente("NOTAS_FISCAIS_EMITIDAS", "OBSERVACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE NOTAS_FISCAIS_EMITIDAS ADD OBSERVACAO VARCHAR2(100) NULL", null);

                if (IdentificaColunaExistente("NOTAS_FISCAIS_EMITIDAS", "CF_ID").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE NOTAS_FISCAIS_EMITIDAS ADD CF_ID INTEGER NULL", null);

                #endregion

                #region RESERVA
                if (IdentificaTabelaExistente("RESERVA").Equals(0))
                {
                    strSql = " CREATE TABLE RESERVA"
                             + "   ("
                             + "         EMP_CODIGO                         INTEGER not null,"
                             + "         EST_CODIGO                         INTEGER not null,"
                             + "         CODIGO                             INTEGER not null,"
                             + "         ID                                 INTEGER,"
                             + "         SOLICITANTE                        VARCHAR2(40),"
                             + "         LOJA_ORIGEM                        INTEGER,"
                             + "         LOJA_DESTINO                       INTEGER,"
                             + "         OPCADASTRO                         VARCHAR2(30),"
                             + "         DTCADASTRO                         DATE,"
                             + "         STATUS                             VARCHAR2(1),"
                             + "         PRIMARY KEY(EMP_CODIGO, EST_CODIGO, CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("RESERVA").Equals(0))
                    {
                        CriaSynonyms("RESERVA");
                    }
                }
                #endregion

                #region RESERVA_ITENS
                if (IdentificaTabelaExistente("RESERVA_ITENS").Equals(0))
                {
                    strSql = " CREATE TABLE RESERVA_ITENS("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        CODIGO          INTEGER NOT NULL,"
                             + "        PROD_CODIGO     VARCHAR2(20),"
                             + "        QTDE            INTEGER,"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,CODIGO,PROD_CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("RESERVA_ITENS").Equals(0))
                    {
                        CriaSynonyms("RESERVA_ITENS");
                    }
                }
                #endregion

                #region CFOP
                if (IdentificaTabelaExistente("CFOP").Equals(0))
                {
                    strSql = " CREATE TABLE CFOP"
                             + "   ("
                             + "         CFOP           VARCHAR2(4) not null,"
                             + "         DESCRICAO      VARCHAR2(500)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("CFOP").Equals(0))
                    {
                        CriaSynonyms("CFOP");
                    }

                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6654', 'Venda de combustível ou lubrificante . adquiridos ou recebidos de terceiros . destinados à industrialização subseqüente ')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6655', 'Venda de combustível ou lubrificante . adquiridos ou recebidos de terceiros . destinados à comercialização')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6656', 'Venda de combustível ou lubrificante . adquiridos ou recebidos de terceiros . destinados a consumidor ou usuário final')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6657', 'Remessa de combustível ou lubrificante . adquiridos ou recebidos de terceiros . p/ venda fora do estabelecimento')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6658', 'Transferência de combustível ou lubrificante de produção do estabelecimento')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6659', 'Transferência de combustível ou lubrificante adquiridos ou recebidos de terceiros')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6660', 'Devolução de compra de combustível ou lubrificante adquiridos p/ industrialização subseqüente')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6661', 'Devolução de compra de combustível ou lubrificante adquiridos p/ comercialização')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6662', 'Devolução de compra de combustível ou lubrificante adquiridos por consumidor ou usuário final')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6663', 'Remessa p/ armazenagem de combustível ou lubrificante')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6664', 'Retorno de combustível ou lubrificante recebidos p/ armazenagem')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6665', 'Retorno simbólico de combustível ou lubrificante recebidos p/ armazenagem')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6666', 'Remessa . por conta e ordem de terceiros . de combustível ou lubrificante recebidos p/ armazenagem')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6667', 'Venda de combustível ou lubrificante a consumidor ou usuário final estabelecido em outra UF diferente da que ocorrer o consumo')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6901', 'Remessa p/ industrialização por encomenda')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6902', 'Retorno de mercadoria utilizada na industrialização por encomenda')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6903', 'Retorno de mercadoria recebida p/ industrialização e não aplicada no referido processo')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6904', 'Remessa p/ venda fora do estabelecimento')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6905', 'Remessa p/ depósito fechado ou armazém geral')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6906', 'Retorno de mercadoria depositada em depósito fechado ou armazém geral')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6907', 'Retorno simbólico de mercadoria depositada em depósito fechado ou armazém geral')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6908', 'Remessa de bem por conta de contrato de comodato')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6909', 'Retorno de bem recebido por conta de contrato de comodato')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6910', 'Remessa em bonificação . doação ou brinde')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6911', 'Remessa de amostra grátis')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6912', 'Remessa de mercadoria ou bem p/ demonstração')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6913', 'Retorno de mercadoria ou bem recebido p/ demonstração')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6914', 'Remessa de mercadoria ou bem p/ exposição ou feira')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6915', 'Remessa de mercadoria ou bem p/ conserto ou reparo')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6916', 'Retorno de mercadoria ou bem recebido p/ conserto ou reparo')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6917', 'Remessa de mercadoria em consignação mercantil ou industrial')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6918', 'Devolução de mercadoria recebida em consignação mercantil ou industrial')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6919', 'Devolução simbólica de mercadoria vendida ou utilizada em processo industrial . recebida anteriormente em consignação mercantil ou industrial')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6920', 'Remessa de vasilhame ou sacaria')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6921', 'Devolução de vasilhame ou sacaria')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6922', 'Lançamento efetuado a título de simples faturamento decorrente de venda p/ entrega futura')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6923', 'Remessa de mercadoria por conta e ordem de terceiros . em venda à ordem ou em operações com armazém geral ou depósito fechado')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6924', 'Remessa p/ industrialização por conta e ordem do adquirente da mercadoria . quando esta não transitar pelo estabelecimento do adquirente')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6925', 'Retorno de mercadoria recebida p/ industrialização por conta e ordem do adquirente da mercadoria . quando aquela não transitar pelo estabelecimento do adquirente')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6929', 'Lançamento efetuado em decorrência de emissão de documento fiscal relativo a operação ou prestação também registrada em equipamento Emissor de Cupom Fiscal - ECF')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6931', 'Lançamento efetuado em decorrência da responsabilidade de retenção do imposto por substituição tributária . atribuída ao remetente ou alienante da mercadoria . pelo serviço de transporte realizado por transportador autônomo ou por transportador não inscrito na UF onde iniciado o serviço')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6932', 'Prestação de serviço de transporte iniciada em UF diversa daquela onde inscrito o prestador')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6933', 'Prestação de serviço tributado pelo Imposto Sobre Serviços de Qualquer Natureza ')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6934', 'Remessa simbólica de mercadoria depositada em armazém geral ou depósito fechado')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('6949', 'Outra saída de mercadoria ou prestação de serviço não especificado')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7101', 'Venda de produção do estabelecimento')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7102', 'Venda de mercadoria adquirida ou recebida de terceiros')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7105', 'Venda de produção do estabelecimento . que não deva por ele transitar')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7106', 'Venda de mercadoria adquirida ou recebida de terceiros . que não deva por ele transitar')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7127', 'Venda de produção do estabelecimento sob o regime de drawback ')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7129', 'Venda de produção do estabelecimento ao mercado externo de mercadoria industrializada sob o amparo do Regime Aduaneiro Especial de Entreposto Industrial (Recof-Sped)')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7201', 'Devolução de compra p/ industrialização ou produção rural')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7202', 'Devolução de compra p/ comercialização')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7205', 'Anulação de valor relativo à aquisição de serviço de comunicação')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7206', 'Anulação de valor relativo a aquisição de serviço de transporte')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7207', 'Anulação de valor relativo à compra de energia elétrica')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7210', 'Devolução de compra p/ utilização na prestação de serviço')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7211', 'Devolução de compras p/ industrialização sob o regime de drawback ')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7212', 'Devolução de compras para industrialização sob o regime de Regime Aduaneiro Especial de Entreposto Industrial (Recof-Sped)')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7251', 'Venda de energia elétrica p/ o exterior')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7301', 'Prestação de serviço de comunicação p/ execução de serviço da mesma natureza')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7358', 'Prestação de serviço de transporte')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7501', 'Exportação de mercadorias recebidas com fim específico de exportação')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7551', 'Venda de bem do ativo imobilizado')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7553', 'Devolução de compra de bem p/ o ativo imobilizado')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7556', 'Devolução de compra de material de uso ou consumo')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7651', 'Venda de combustível ou lubrificante de produção do estabelecimento')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7654', 'Venda de combustível ou lubrificante adquiridos ou recebidos de terceiros')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7667', 'Venda de combustível ou lubrificante a consumidor ou usuário final')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7930', 'Lançamento efetuado a título de devolução de bem cuja entrada tenha ocorrido sob amparo de regime especial aduaneiro de admissão temporária')", null);
                    BancoDados.ExecuteNoQuery("INSERT INTO CFOP VALUES('7949', 'Outra saída de mercadoria ou prestação de serviço não especificado')", null);
                }
                #endregion

                #region SCRIPT MEDICOS
                formulario.lblMsg.Text = "TABELA  MEDICOS";
                formulario.lblMsg.Refresh();
                if (IdentificaColunaExistente("MEDICOS", "MED_ID").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE MEDICOS ADD MED_ID INTEGER", null);
                    AtualizaID("MEDICOS", "MED_ID", "MED_CODIGO", "0", true);
                }

                if (IdentificaColunaExistente("MEDICOS", "OPALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MEDICOS ADD OPALTERACAO varchar2(25) NULL", null);

                if (IdentificaColunaExistente("MEDICOS", "DTALTERACAO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MEDICOS ADD DTALTERACAO DATE NULL", null);

                if (IdentificaColunaExistente("MEDICOS", "DTCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MEDICOS ADD DTCADASTRO DATE NULL", null);

                if (IdentificaColunaExistente("MEDICOS", "OPCADASTRO").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE MEDICOS ADD OPCADASTRO varchar2(25) NULL", null);
                #endregion

                #region ENTREGA_FILIAL
                if (IdentificaTabelaExistente("ENTREGA_FILIAL").Equals(0))
                {
                    strSql = " CREATE TABLE ENTREGA_FILIAL"
                             + "   ("
                             + "         EMP_CODIGO                         INTEGER not null,"
                             + "         EST_CODIGO                         INTEGER not null,"
                             + "         CODIGO                             INTEGER not null,"
                             + "         ID                                 INTEGER,"
                             + "         SOLICITANTE                        VARCHAR2(40),"
                             + "         LOJA_ORIGEM                        INTEGER,"
                             + "         LOJA_DESTINO                       INTEGER,"
                             + "         CF_DOCTO                           VARCHAR2(18),"
                             + "         CF_ENDERECO                        VARCHAR2(50),"
                             + "         CF_NUMERO                          VARCHAR2(6),"
                             + "         CF_BAIRRO                          VARCHAR2(20),"
                             + "         CF_CIDADE                          VARCHAR2(30),"
                             + "         CF_TELEFONE                        VARCHAR2(15),"
                             + "         TROCO                              NUMBER(9,2),"
                             + "         TOTAL_ENTREGA                      NUMBER(9,2),"
                             + "         OBSERVACAO                         VARCHAR2(50),"
                             + "         OPCADASTRO                         VARCHAR2(30),"
                             + "         DTCADASTRO                         DATE,"
                             + "         STATUS                             VARCHAR2(1),"
                             + "         PRIMARY KEY(EMP_CODIGO, EST_CODIGO, CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ENTREGA_FILIAL").Equals(0))
                    {
                        CriaSynonyms("ENTREGA_FILIAL");
                    }
                }
                #endregion

                #region ENTREGA_FILIAL_ITENS
                if (IdentificaTabelaExistente("ENTREGA_FILIAL_ITENS").Equals(0))
                {
                    strSql = " CREATE TABLE ENTREGA_FILIAL_ITENS("
                             + "        EMP_CODIGO      INTEGER NOT NULL,"
                             + "        EST_CODIGO      INTEGER NOT NULL,"
                             + "        CODIGO          INTEGER NOT NULL,"
                             + "        PROD_CODIGO     VARCHAR2(20),"
                             + "        QTDE            INTEGER,"
                             + "        PRECO           NUMBER(9,2),"
                             + "        SUBTOTAL        NUMBER(9,2),"
                             + "        DESCONTO        NUMBER(9,2),"
                             + "        TOTAL           NUMBER(9,2),"
                             + "        COMISSAO        NUMBER(9,2),"
                             + "        PRIMARY KEY(EMP_CODIGO,EST_CODIGO,CODIGO,PROD_CODIGO)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ENTREGA_FILIAL_ITENS").Equals(0))
                    {
                        CriaSynonyms("ENTREGA_FILIAL_ITENS");
                    }
                }
                #endregion

                #region LOG_ALTERA_QTDE
                if (IdentificaTabelaExistente("LOG_ALTERA_QTDE").Equals(0))
                {
                    strSql = " CREATE TABLE LOG_ALTERA_QTDE("
                             + "          EMP_CODIGO    NUMBER(18,2) default 0 not null,"
                             + "          EST_CODIGO    NUMBER(18, 2) default 0 not null,"
                             + "          PROD_CODIGO   VARCHAR2(20) not null,"
                             + "          DATA_ALTERA   DATE,"
                             + "          QTDE_ANTIGO   NUMBER(18, 2) default 0,"
                             + "          QTDE_NOVO     NUMBER(18, 2) default 0,"
                             + "          USUARIO       VARCHAR2(20) not null"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_ALTERA_QTDE").Equals(0))
                    {
                        CriaSynonyms("LOG_ALTERA_QTDE");
                    }
                }
                #endregion

                #region ALTERACAO ESPECIE
                if (Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_CAIXA", "ESP_CODIGO", "2").Equals("N"))
                {
                    BancoDados.ExecuteNoQuery("UPDATE CAD_ESPECIES SET ESP_CAIXA = 'S' WHERE ESP_CODIGO IN (2,3,6)", null);
                }
                #endregion

                #region PROMOCOES
                if (IdentificaColunaExistente("PROMOCOES", "DESCONTO_PROGRESSIVO").Equals(0))
                {
                    BancoDados.ExecuteNoQuery("ALTER TABLE PROMOCOES ADD DESCONTO_PROGRESSIVO varchar2(1) default 'N'", null);
                    BancoDados.ExecuteNoQuery("UPDATE PROMOCOES SET DESCONTO_PROGRESSIVO = 'N'", null);
                }

                if (IdentificaColunaExistente("PROMOCOES", "LEVE").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PROMOCOES ADD LEVE INTEGER NULL", null);

                if (IdentificaColunaExistente("PROMOCOES", "PAGUE").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PROMOCOES ADD PAGUE INTEGER NULL", null);

                if (IdentificaColunaExistente("PROMOCOES", "PORCENTAGEM").Equals(0))
                    BancoDados.ExecuteNoQuery("ALTER TABLE PROMOCOES ADD PORCENTAGEM NUMBER(9, 2) NULL", null);
                #endregion

                #region LOG_ALTERA_QTDE
                if (IdentificaTabelaExistente("LOG_ALTERA_ULTIMO_CUSTO").Equals(0))
                {
                    strSql = " CREATE TABLE LOG_ALTERA_ULTIMO_CUSTO("
                             + "          EMP_CODIGO            NUMBER(18,2) default 0 not null,"
                             + "          EST_CODIGO            NUMBER(18, 2) default 0 not null,"
                             + "          PROD_CODIGO           VARCHAR2(20) not null,"
                             + "          DT_CADASTRO           DATE,"
                             + "          ULTIMO_CUSTO_ANTIGO   NUMBER(18, 2) default 0,"
                             + "          ULTIMO_CUSTO_NOVO     NUMBER(18, 2) default 0,"
                             + "          USUARIO               VARCHAR2(20) not null,"
                             + "          OBSERVACAO            VARCHAR2(100) not null"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("LOG_ALTERA_ULTIMO_CUSTO").Equals(0))
                    {
                        CriaSynonyms("LOG_ALTERA_ULTIMO_CUSTO");
                    }
                }
                #endregion

                #region ABCFARMA
                if (IdentificaTabelaExistente("ABCFARMA").Equals(0))
                {
                    strSql = " CREATE TABLE ABCFARMA("
                             + "          MED_ABC       VARCHAR2(10),"
                             + "          LAB_NOM       VARCHAR2(30),"
                             + "          MED_DES       VARCHAR2(100),"
                             + "          MED_APR       VARCHAR2(150),"
                             + "          MED_PCO18     NUMBER(18, 2),"
                             + "          MED_PLA18     NUMBER(18, 2),"
                             + "          MED_FRA18     NUMBER(18, 2),"
                             + "          MED_BARRA     VARCHAR2(16),"
                             + "          MED_GENE      VARCHAR2(3),"
                             + "          MED_REGIMS    VARCHAR2(15),"
                             + "          MED_NCM       VARCHAR2(10),"
                             + "          MED_NEGPOS    VARCHAR2(2),"
                             + "          MED_CAS       VARCHAR2(250),"
                             + "          MED_DCB       VARCHAR2(240),"
                             + "          MED_CEST      VARCHAR2(9),"
                             + "          MED_PCO20     NUMBER(18, 2)"
                             + "   )";
                    BancoDados.ExecuteNoQuery(strSql, null);

                    if (IdentificaSynonyms("ABCFARMA").Equals(0))
                    {
                        CriaSynonyms("ABCFARMA");
                    }
                }
                #endregion

                #region LOG_ALTERACAO
                dt = IdentificaAlteracaoTipoCampo("LOG_ALTERACAO", "LOG_ANTERIOR");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) == 60)
                        BancoDados.ExecuteNoQuery("ALTER TABLE LOG_ALTERACAO MODIFY (LOG_ANTERIOR VARCHAR2(1000))", null);

                dt = IdentificaAlteracaoTipoCampo("LOG_ALTERACAO", "LOG_ALTERADO");
                if (dt.Rows.Count > 0)
                    if (Convert.ToInt32(dt.Rows[0]["DATA_LENGTH"]) == 60)
                        BancoDados.ExecuteNoQuery("ALTER TABLE LOG_ALTERACAO MODIFY (LOG_ALTERADO VARCHAR2(1000))", null);
                #endregion
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
            }
        }

        /// <summary>
        /// Cria o ID da Tabela
        /// </summary>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="campo">Campo do ID</param>
        /// <param name="cond">where 1</param>
        /// <param name="cond2">where 2</param>
        /// <param name="condLetra">Campo String para colocar aspas</param>
        /// <param name="empCodigo">Código da Empresa</param>
        /// <param name="estCodigo">Código do Estabelecimento</param>
        /// <returns></returns>
        public bool AtualizaID(string tabela, string campo, string cond, string cond2, bool condLetra = false, bool empCodigo = false, bool estCodigo = false)
        {
            if (cond2 == "0")
            {
                string sql = " SELECT " + cond + " FROM " + tabela + " WHERE " + campo + "  IS NULL ";

                if (empCodigo)
                {
                    sql += " AND EMP_CODIGO = 1";
                }

                if (estCodigo)
                {
                    sql += " AND EST_CODIGO = 1";
                }

                DataTable dtDados = BancoDados.GetDataTable(sql, null);

                for (int i = 0; i < dtDados.Rows.Count; i++)
                {

                    sql = " UPDATE " + tabela + " SET " + campo + " = " + (i + 1)
                                + " WHERE " + cond + " = ";
                    if (condLetra)
                    {
                        sql += "'" + dtDados.Rows[i][cond] + "'";
                    }
                    else
                        sql += dtDados.Rows[i][cond].ToString().Replace(",",".");

                    if (empCodigo)
                    {
                        sql += " AND EMP_CODIGO = 1";
                    }

                    if (estCodigo)
                    {
                        sql += " AND EST_CODIGO = 1";
                    }

                    BancoDados.ExecuteNoQuery(sql, null);
                }
            }
            else
            {
                string sql = " SELECT " + cond + " , " + cond2 + " FROM " + tabela + " WHERE " + campo + "  IS NULL ";
                if (empCodigo)
                {
                    sql += " AND EMP_CODIGO = 1";
                }

                if (estCodigo)
                {
                    sql += " AND EST_CODIGO = 1";
                }

                DataTable dtDados = BancoDados.GetDataTable(sql, null);

                for (int i = 0; i < dtDados.Rows.Count; i++)
                {

                    sql = " UPDATE " + tabela + " SET " + campo + " = " + (i + 1)
                        + " WHERE " + cond + " = '" + dtDados.Rows[i][cond] + "' AND " + cond2 + " = '" + dtDados.Rows[i][cond2] + "'";
                    if (empCodigo)
                    {
                        sql += " AND EMP_CODIGO = 1";
                    }

                    if (estCodigo)
                    {
                        sql += " AND EST_CODIGO = 1";
                    }
                    BancoDados.ExecuteNoQuery(sql, null);
                }
            }
            return true;
        }


        public bool AtualizaIDTabelaExtrangeira(string tabelaPrincipal, string tabelaEstrangeira, string campo, string cond, string cond2, bool condLetra = false,
             bool empCodigo = false, bool estCodigo = false, bool principalComCodigo = true)
        {
            try
            {
                string sql = " SELECT " + cond + " , " + cond2 + " FROM " + tabelaPrincipal;
                if (empCodigo && principalComCodigo)
                {
                    sql += " WHERE EMP_CODIGO = 1";
                }

                if (estCodigo && principalComCodigo)
                {
                    if (empCodigo)
                        sql += " AND EST_CODIGO = 1";
                    else
                        sql += " WHERE EST_CODIGO = 1";
                }
                DataTable dtDados = BancoDados.GetDataTable(sql, null);

                for (int i = 0; i < dtDados.Rows.Count; i++)
                {
                    sql = " UPDATE " + tabelaEstrangeira + " SET " + campo + " = " + dtDados.Rows[i][cond]
                        + " WHERE " + cond2 + " = ";
                    if (condLetra)
                    {
                        sql += "'" + dtDados.Rows[i][cond2] + "'";
                    }
                    else
                        sql += dtDados.Rows[i][cond2];

                    BancoDados.ExecuteNoQuery(sql, null);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
