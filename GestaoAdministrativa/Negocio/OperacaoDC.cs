﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class OperacaoDC
    {
        public int OdcID { get; set; }
        public string OdcTipo { get; set; }
        public string OdcDescricao { get; set; }
        public int OdcClasse { get; set; }
        public string OdcLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public OperacaoDC()
        {
        }

        public OperacaoDC(int odcID, string odcTipo, string odcDescricao, int odcClasse, string odcLiberado, DateTime dtAlteracao,
                          string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.OdcID = odcID;
            this.OdcTipo = odcTipo;
            this.OdcDescricao = odcDescricao;
            this.OdcClasse = odcClasse;
            this.OdcLiberado = odcLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaDados(OperacaoDC dados)
        {
            string sql = " SELECT ODC_NUM, ODC_DESCRICAO, "
                       + " CASE ODC_TIPO WHEN 'C' THEN 'CAIXA' ELSE 'BANCOS' END AS ODC_TIPO, "
                       + " CASE ODC_CLASSE WHEN 0 THEN 'CREDITO' WHEN 1 THEN 'DEBITO'  WHEN 2 THEN 'NULO' END AS ODC_CLASSE, "
                       + " CASE ODC_DESABILITADO WHEN 'N' THEN 'S' ElSE 'N' END AS ODC_DESABILITADO,"
                       + " OP_ALTERACAO, DT_ALTERACAO, "
                       + " DT_CADASTRO,OP_CADASTRO FROM OPERACOES_DC WHERE 1=1 ";
            if (dados.OdcID != 0)
            {
                sql += " AND ODC_TIPO = " + OdcID;
            }
            if (dados.OdcTipo != "TODOS")
            {
                string tipo = OdcTipo == "BANCOS" ? "B" : "C";
                sql += "AND ODC_TIPO = '" + tipo + "'";
            }
            if (dados.OdcDescricao != "")
            {
                sql += "AND ODC_DESCRICAO LIKE '%" + OdcDescricao + "%'";
            }
            if (dados.OdcLiberado != "TODOS")
            {
                string desab = OdcLiberado == "S" ? "N" : "S";
                sql += "AND ODC_DESABILITADO = '" + desab + "'";
            }
            sql += "ORDER BY ODC_NUM";
            return BancoDados.GetDataTable(sql, null);
        }

        public bool InsereDados(OperacaoDC dados)
        {
            string sql = "INSERT INTO OPERACOES_DC  (ODC_TIPO, ODC_NUM, ODC_DESCRICAO, ODC_CLASSE, ODC_DESABILITADO,  DT_CADASTRO,OP_CADASTRO )"
                       + "VALUES ("
                       + " '" + dados.OdcTipo + "'"
                       + "," + dados.OdcID
                       + ",'" + dados.OdcDescricao + "'"
                       + ",'" + dados.OdcClasse + "'"
                       + ",'" + dados.OdcLiberado + "'"
                       + "," + Funcoes.BDataHora(dados.DtCadastro)
                       + ",'" + dados.OpCadastro + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                // GRAVA LOG
                Funcoes.GravaLogInclusao("ODC_NUM", dados.OdcID.ToString(), Principal.usuario.ToUpper(), "OPERACOES_DC", dados.OdcDescricao, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool AtualizaDados(OperacaoDC dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE OPERACOES_DC SET "
                       + " ODC_TIPO = '" + dadosNovos.OdcTipo + "'"
                       + " ,ODC_DESCRICAO = '" + dadosNovos.OdcDescricao + "'"
                       + " ,ODC_CLASSE = " + dadosNovos.OdcClasse
                       + " ,ODC_DESABILITADO = '" + dadosNovos.OdcLiberado + "'";

            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            if (dadosNovos.OdcTipo != dadosAntigos.Rows[0]["ODC_TIPO"].ToString())
            {
                dados[contMatriz, 0] = "ODC_TIPO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ODC_TIPO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OdcTipo + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.OdcDescricao != dadosAntigos.Rows[0]["ODC_DESCRICAO"].ToString())
            {
                dados[contMatriz, 0] = "ODC_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ODC_DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OdcDescricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.OdcClasse != Convert.ToInt32(dadosAntigos.Rows[0]["ODC_CLASSE"]))
            {
                dados[contMatriz, 0] = "ODC_CLASSE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ODC_CLASSE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OdcClasse + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.OdcLiberado != dadosAntigos.Rows[0]["ODC_DESABILITADO"].ToString())
            {
                dados[contMatriz, 0] = "ODC_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ODC_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OdcLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                sql += " ,OP_ALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                    + " ,DT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                    + " WHERE ODC_NUM = " + OdcID;

                if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("ODC_NUM", dadosNovos.OdcID.ToString(), Principal.usuario.ToUpper(), "OPERACOES_DC", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool ExcluiDados(int OdcID)
        {
            string sql = "DELETE FROM OPERACOES_DC WHERE ODC_NUM = " + OdcID;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {

                return false;
            }
        }
    }
}
