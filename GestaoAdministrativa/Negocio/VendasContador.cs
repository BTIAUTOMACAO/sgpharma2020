﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public  class VendasContador
    {
        public long VendaId { get; set; }

        public VendasContador() { }

        public bool InserirDados(long dadoID)
        {
            string sql = "INSERT INTO VENDAS_CONTADOR (VENDA_ID) VALUES ("
                       + dadoID + ")";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(long id)
        {
            string sql = " UPDATE VENDAS_CONTADOR SET"
                       + " VENDA_ID = " + id;

            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
