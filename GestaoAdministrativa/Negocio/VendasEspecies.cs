﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class VendasEspecies
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaId { get; set; }
        public int VendaEspecieID { get; set; }
        public int EspCodigo { get; set; }
        public double Valor { get; set; }
        public string EspDescricao { get; set; }
        public string EspVinculado { get; set; }
        public string EspSAT { get; set; }
        public string EspEcf { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public VendasEspecies() { }

        public VendasEspecies(int empCodigo, int estCodigo, long vendaID, int vendaEspecieID, int espCodigo, double valor,string espDescricao, 
            string espVinculado, string espSAT, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.VendaId = vendaID;
            this.VendaEspecieID = vendaEspecieID;
            this.EspCodigo = espCodigo;
            this.Valor = valor;
            this.EspDescricao = espDescricao;
            this.EspVinculado = espVinculado;
            this.EspSAT = espSAT;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InserirDados(VendasEspecies dados)
        {
            string strCmd = "INSERT INTO VENDAS_ESPECIES(EMP_CODIGO,EST_CODIGO,VENDA_ID,VENDA_ESPECIE_ID,ESP_CODIGO,VENDA_ESPECIE_VALOR,"
                + "DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.VendaId + ","
                + dados.VendaEspecieID + ","
                + dados.EspCodigo + ","
                + Funcoes.BValor(dados.Valor) + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaEspeciesPorVendaID(int empCodigo, int estCodigo, long vendaID)
        {
            string strSql = "SELECT * FROM VENDAS_ESPECIES WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo
                + " AND VENDA_ID = " + vendaID;
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaEspeciesESatPorVendaID(int empCodigo, int estCodigo, long vendaID)
        {
            string strSql = "SELECT A.VENDA_ESPECIE_VALOR, B.ESP_SAT FROM VENDAS_ESPECIES A "
                + " INNER JOIN CAD_ESPECIES B ON A.ESP_CODIGO = B.ESP_CODIGO"
                + " WHERE A.EMP_CODIGO = " + empCodigo + " AND A.EST_CODIGO = " + estCodigo
                + " AND A.VENDA_ID = " + vendaID;
            return BancoDados.GetDataTable(strSql, null);
        }

        public List<VendasEspecies> DadosEspeciesPorVendaID(long vendaId, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.ESP_CODIGO, A.VENDA_ESPECIE_VALOR, B.ESP_DESCRICAO, B.ESP_VINCULADO, B.ESP_SAT, B.ESP_ECF"
                + " FROM VENDAS_ESPECIES A"
                + " INNER JOIN CAD_ESPECIES B ON A.ESP_CODIGO = B.ESP_CODIGO"
                + " WHERE A.EMP_CODIGO = " + empCodigo
                + " AND A.EST_CODIGO = " + estCodigo
                + " AND A.VENDA_ID = " + vendaId;

            List<VendasEspecies> lista = new List<VendasEspecies>();
            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    VendasEspecies dEspecies = new VendasEspecies();
                    dEspecies.EspCodigo = Convert.ToInt32(row["ESP_CODIGO"]);
                    dEspecies.Valor = Convert.ToDouble(row["VENDA_ESPECIE_VALOR"]);
                    dEspecies.EspDescricao = row["ESP_DESCRICAO"].ToString();
                    dEspecies.EspVinculado = row["ESP_VINCULADO"].ToString();
                    dEspecies.EspSAT = row["ESP_SAT"].ToString();
                    dEspecies.EspEcf = row["ESP_ECF"].ToString();
                    lista.Add(dEspecies);
                }
            }
            return lista;
        }

        public bool AtualizaOperador(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "UPDATE VENDAS_ESPECIES SET OP_CADASTRO = '" + Principal.usuario + "', DT_CADASTRO = " + Funcoes.BDataHora(DateTime.Now)
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
