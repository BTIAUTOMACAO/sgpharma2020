﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ControleDeDados
    {
        public int GrupoId { get; set; }
        public int CodEstabelecimento { get; set; }
        public string Atualizado { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
