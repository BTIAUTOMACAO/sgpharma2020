﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class Estabelecimento
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public string EstCNPJ { get; set; }
        public string EstInscricaoEstadual { get; set; }
        public string EstRazao { get; set; }
        public string EstFantasia { get; set; }
        public string EstEndereco { get; set; }
        public int EstNum { get; set; }
        public string EstBairro { get; set; }
        public string EstCidade { get; set; }
        public string EstCep { get; set; }
        public string EstUf { get; set; }
        public string EstFone { get; set; }
        public string EstContato { get; set; }
        public string EstInscricaoMunicipal { get; set; }
        public string EstLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Estabelecimento() { }

        public Estabelecimento(int estCodigo, int empCodigo, string estCnpj, string estIncricaoEstadual, string estRazao, string estFantasia, string estEndereco,
            int estNum, string estBairro, string estCidade, string estCep, string estUf, string estFone, string estContato, string estIncricaoMunicipal,
            string estLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.EstCNPJ = estCnpj;
            this.EstInscricaoEstadual = estIncricaoEstadual;
            this.EstRazao = estRazao;
            this.EstFantasia = estFantasia;
            this.EstEndereco = estEndereco;
            this.EstNum = estNum;
            this.EstBairro = estBairro;
            this.EstCidade = estCidade;
            this.EstCep = estCep;
            this.EstUf = estUf;
            this.EstFone = estFone;
            this.EstContato = estContato;
            this.EstInscricaoMunicipal = estIncricaoMunicipal;
            this.EstLiberado = estLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaDados(int empCodigo, string estCodigo, string estNome, string estLiberado, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.EST_RAZAO, A.EST_FANTASIA, A.EST_CGC, A.EST_ENDERECO, A.EST_CIDADE, A.EST_UF, A.EST_CODIGO, A.EMP_CODIGO, "
                     + "A.EST_INSEST, A.EST_FONE, A.EST_BAIRRO, A.EST_CEP, A.EST_CONTATO, CASE WHEN A.EST_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS EST_DESABILITADO, "
                     + "A.EST_END_NUM, A.EST_INSMUN, A.DAT_ALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO"
                     + " FROM ESTABELECIMENTOS A WHERE A.EMP_CODIGO = " + empCodigo;
            if (estCodigo == "" && estNome == "" && estLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.EST_CODIGO";
            }
            else
            {
                if (estCodigo != "")
                {
                    strSql += " AND A.EMP_CODIGO = " + estCodigo;
                }
                if (estNome != "")
                {
                    strSql += " AND A.EST_NOME LIKE '%" + estNome + "%'";
                }
                if (estLiberado != "TODOS")
                {
                    estLiberado = estLiberado == "N" ? "S" : "N";
                    strSql += " AND A.EST_DESABILITADO = '" + estLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.EST_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(Estabelecimento dados)
        {
            string strCmd = "INSERT INTO ESTABELECIMENTOS (EST_CODIGO, EMP_CODIGO, EST_CGC, EST_INSEST, EST_RAZAO, EST_FANTASIA,"
                + "EST_ENDERECO, EST_END_NUM, EST_BAIRRO, EST_CIDADE,"
                + "EST_CEP, EST_UF, EST_FONE, EST_CONTATO, EST_INSMUN, EST_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.EstCodigo + ","
                + dados.EmpCodigo + ",'"
                + dados.EstCNPJ + "',";
            if (String.IsNullOrEmpty(dados.EstInscricaoEstadual))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.EstInscricaoEstadual + "',";

            strCmd += "'" + dados.EstRazao + "','"
                + dados.EstFantasia + "','"
                + dados.EstEndereco + "','"
                + dados.EstNum + "',";

            if (String.IsNullOrEmpty(dados.EstBairro))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.EstBairro + "',";

            strCmd += "'" + dados.EstCidade + "',"
                + dados.EstCep + ",'"
                + dados.EstUf + "',";

            if (String.IsNullOrEmpty(dados.EstFone))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.EstFone + "',";

            if (String.IsNullOrEmpty(dados.EstContato))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.EstContato + "',";

            strCmd += "'" + dados.EstInscricaoMunicipal + "',";
            strCmd += "'" + dados.EstLiberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public List<Estabelecimento> IdentificaEstabelecimentos()
        {
            string strSql = "SELECT EST_CODIGO, EMP_CODIGO FROM ESTABELECIMENTOS WHERE EST_DESABILITADO = 'N'";

            List<Estabelecimento> listaEstabelecimento = new List<Estabelecimento>();

            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Estabelecimento dEstabelecimento = new Estabelecimento();
                    dEstabelecimento.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEstabelecimento.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    listaEstabelecimento.Add(dEstabelecimento);
                }
            }

            return listaEstabelecimento;
        }

        public bool AtualizaDados(Estabelecimento dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[18, 3];
            int contMatriz = 0;

            string strCmd;

            strCmd = "UPDATE ESTABELECIMENTOS SET ";

            if (!dadosNew.EmpCodigo.Equals(Convert.ToInt32(Math.Round(Convert.ToDecimal(dadosOld.Rows[0]["EMP_CODIGO"]), 0))))
            {
                if (Util.RegistrosPorEmpresa("ESTABELECIMENTOS", "EMP_CODIGO", dadosNew.EmpCodigo.ToString()).Rows.Count == 0)
                {
                    Principal.mensagem = "Estabelecimento não cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " EMP_CODIGO = " + dadosNew.EmpCodigo + ",";

                dados[contMatriz, 0] = "EMP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EMP_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EmpCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstCNPJ.Equals(dadosOld.Rows[0]["EST_CGC"]))
            {
                if (Util.RegistrosPorEstabelecimento("ESTABELECIMENTOS", "EST_CGC", dadosNew.EstCNPJ, false, true).Rows.Count != 0)
                {
                    Principal.mensagem = "Estabelecimento já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " EST_CGC = '" + dadosNew.EstCNPJ + "',";

                dados[contMatriz, 0] = "EST_CGC";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_CGC"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstCNPJ + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstInscricaoEstadual.Equals(dadosOld.Rows[0]["EST_INSEST"].ToString()))
            {
                strCmd += dadosNew.EstInscricaoEstadual == "" ? " EST_INSEST = null," : " EST_INSEST = '" + dadosNew.EstInscricaoEstadual + "',";

                dados[contMatriz, 0] = "EST_INSEST";
                dados[contMatriz, 1] = dadosOld.Rows[0]["EST_INSEST"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["EST_INSEST"] + "'";
                dados[contMatriz, 2] = dadosNew.EstInscricaoEstadual == "" ? "null" : "'" + dadosNew.EstInscricaoEstadual + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstRazao.Equals(dadosOld.Rows[0]["EST_RAZAO"]))
            {
                strCmd += " EST_RAZAO = '" + dadosNew.EstRazao + "',";

                dados[contMatriz, 0] = "EST_RAZAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_RAZAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstRazao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstFantasia.Equals(dadosOld.Rows[0]["EST_FANTASIA"]))
            {
                strCmd += " EST_FANTASIA = '" + dadosNew.EstFantasia + "',";

                dados[contMatriz, 0] = "EST_FANTASIA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_FANTASIA"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstFantasia + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstEndereco.Equals(dadosOld.Rows[0]["EST_ENDERECO"]))
            {
                strCmd += " EST_ENDERECO = '" + dadosNew.EstEndereco + "',";

                dados[contMatriz, 0] = "EST_ENDERECO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_ENDERECO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstEndereco + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstNum.ToString().Equals(dadosOld.Rows[0]["EST_END_NUM"].ToString()))
            {
                strCmd += " EST_END_NUM = '" + dadosNew.EstNum + "',";

                dados[contMatriz, 0] = "EST_END_NUM";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_END_NUM"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstNum + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstBairro.Equals(dadosOld.Rows[0]["EST_BAIRRO"].ToString()))
            {
                strCmd += dadosNew.EstBairro == "" ? " EST_BAIRRO = null," : " EST_BAIRRO = '" + dadosNew.EstBairro + "',";

                dados[contMatriz, 0] = "EST_BAIRRO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["EST_BAIRRO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["EST_BAIRRO"] + "'";
                dados[contMatriz, 2] = dadosNew.EstBairro == "" ? "null" : "'" + dadosNew.EstBairro + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstCidade.Equals(dadosOld.Rows[0]["EST_CIDADE"]))
            {
                strCmd += " EST_CIDADE = '" + dadosNew.EstCidade + "',";

                dados[contMatriz, 0] = "EST_CIDADE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_CIDADE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstCidade + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstCep.ToString().Equals(Funcoes.RemoveCaracter(dadosOld.Rows[0]["EST_CEP"].ToString()) == "" ? "" : Convert.ToDouble(dadosOld.Rows[0]["EST_CEP"].ToString()).ToString()))
            {
                strCmd += " EST_CEP = " + dadosNew.EstCep + ",";

                dados[contMatriz, 0] = "EST_CEP";
                dados[contMatriz, 1] = "'" + Funcoes.RemoveCaracter(dadosOld.Rows[0]["EST_CEP"].ToString()) + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstCep + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstUf.Equals(dadosOld.Rows[0]["EST_UF"]))
            {
                strCmd += " EST_UF = '" + dadosNew.EstUf + "',";

                dados[contMatriz, 0] = "EST_UF";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_UF"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstUf + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstFone.Equals(Funcoes.RemoveCaracter(dadosOld.Rows[0]["EST_FONE"].ToString())))
            {
                strCmd += dadosNew.EstFone == "" ? " EST_FONE = null," : " EST_FONE = '" + dadosNew.EstFone + "',";

                dados[contMatriz, 0] = "EST_FONE";
                dados[contMatriz, 1] = Funcoes.RemoveCaracter(dadosOld.Rows[0]["EST_FONE"].ToString()) == "" ? "null" : "'" + Funcoes.RemoveCaracter(dadosOld.Rows[0]["EST_FONE"].ToString()) + "'";
                dados[contMatriz, 2] = dadosNew.EstFone == "" ? "null" : "'" + dadosNew.EstFone + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstContato.Equals(dadosOld.Rows[0]["EST_CONTATO"].ToString()))
            {
                strCmd += dadosNew.EstContato == "" ? " EST_CONTATO = null," : " EST_CONTATO = '" + dadosNew.EstContato + "',";

                dados[contMatriz, 0] = "EST_CONTATO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["EST_CONTATO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["EST_CONTATO"] + "'";
                dados[contMatriz, 2] = dadosNew.EstContato == "" ? "null" : "'" + dadosNew.EstContato + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstInscricaoMunicipal.Equals(dadosOld.Rows[0]["EST_INSMUN"].ToString()))
            {
                strCmd += " EST_INSMUN = '" + dadosNew.EstInscricaoMunicipal + "',";

                dados[contMatriz, 0] = "EST_INSMUN";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_INSMUN"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstInscricaoMunicipal + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EstLiberado.Equals(dadosOld.Rows[0]["EST_DESABILITADO"]))
            {
                strCmd += " EST_DESABILITADO = '" + dadosNew.EstLiberado + "',";

                dados[contMatriz, 0] = "EST_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["EST_DESABILITADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EstLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EST_CODIGO = " + dadosNew.EstCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("EST_CODIGO", dadosNew.EstCodigo.ToString(), Principal.usuario, "ESTABELECIMENTOS", dados, contMatriz, dadosNew.EstCodigo, dadosNew.EmpCodigo);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(string estCodigo, string empCodigo)
        {
            string strCmd = "DELETE FROM ESTABELECIMENTOS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + Math.Round(Convert.ToDecimal(empCodigo), 0);

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable BuscaDadosPorID(int estabID)
        {
            string sql = "SELECT EST_CGC FROM ESTABELECIMENTOS WHERE EST_CODIGO = " + estabID;

            return BancoDados.GetDataTable(sql, null);
        }

        public List<Estabelecimento> DadosFiscaisEstabelecimento(int estCodigo, int empCodigo)
        {
            string strSql = "SELECT EST_RAZAO, EST_FANTASIA, EST_ENDERECO, EST_END_NUM, EST_BAIRRO, EST_CIDADE, EST_UF, EST_CGC, EST_CEP,"
                + "EST_FONE, EST_INSEST,EST_INSMUN FROM ESTABELECIMENTOS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo;

            List<Estabelecimento> listaEstabelecimento = new List<Estabelecimento>();

            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Estabelecimento dEstabelecimento = new Estabelecimento();
                    dEstabelecimento.EstCNPJ = row["EST_CGC"].ToString();
                    dEstabelecimento.EstRazao = row["EST_RAZAO"].ToString();
                    dEstabelecimento.EstInscricaoEstadual = row["EST_INSEST"].ToString();
                    dEstabelecimento.EstInscricaoMunicipal = row["EST_INSMUN"].ToString();
                    dEstabelecimento.EstFantasia = row["EST_FANTASIA"].ToString();
                    dEstabelecimento.EstEndereco = row["EST_ENDERECO"].ToString();
                    dEstabelecimento.EstNum = String.IsNullOrEmpty(row["EST_END_NUM"].ToString()) ? 0 : Convert.ToInt32(row["EST_END_NUM"]); //Alterado desta forma pois no banco pode retornar null. 
                    dEstabelecimento.EstBairro = row["EST_BAIRRO"].ToString();
                    dEstabelecimento.EstCidade = row["EST_CIDADE"].ToString();
                    dEstabelecimento.EstUf = row["EST_UF"].ToString();
                    dEstabelecimento.EstCep = row["EST_CEP"].ToString();
                    dEstabelecimento.EstFone = row["EST_FONE"].ToString();
                    listaEstabelecimento.Add(dEstabelecimento);
                }
            }

            return listaEstabelecimento;
        }


    }
}
