﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class EstabelecimentoFilial
    {
        public int CodEstabelecimento { get; set; }
        public string NomeEstabelecimento { get; set; }
        public string Telefone { get; set; }
        public int GrupoID { get; set; }
        public string AtualizaTabela { get; set; }
        public string Cnpj { get; set; }
        public string Liberado { get; set; }
        public string ExibeMensagem { get; set; }
        public string MensagemCliente { get; set; }
        public string VersaoAbcFarma { get; set; }
        public string AtualizaIBPT { get; set; }
        public string AtualizaDCB { get; set; }
        public string AtualizaABCFarma { get; set; }
        public string AtualizaFormulario { get; set; }
        public string EnviaDadosVenda { get; set; }
        public string ExibeAviso { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
