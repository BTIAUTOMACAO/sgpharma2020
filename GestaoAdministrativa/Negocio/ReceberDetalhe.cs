﻿using System;

namespace GestaoAdministrativa.Negocio
{
    public class ReceberDetalhe
    {
        public int EmpCodigo { get; set; }
        public int RecID { get; set; }
        public string RecDocto { get; set; }
        public int RdParcela { get; set; }
        public double RdValor { get; set; }
        public DateTime RdVencimento { get; set; }
        public double RdSaldo { get; set; }
        public string RdStatus { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public ReceberDetalhe() { }

        public ReceberDetalhe(int empCodigo, int recId, string recDocto, int rdParcela, double rdValor, DateTime rdVencimento, double rdSaldo, string rdStatus, DateTime dtAlteracao,
            string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.RecID = recId;
            this.RecDocto = recDocto;
            this.RdParcela = rdParcela;
            this.RdValor = rdValor;
            this.RdVencimento = rdVencimento;
            this.RdSaldo = rdSaldo;
            this.RdStatus = rdStatus;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }
    }
}
