﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class Produtos
    {
        public static DataTable GetProdCodigo(int estCodigo, string codBarra)
        {
            Principal.strSql = "SELECT P.PROD_CODIGO FROM PRODUTOS_BARRA P WHERE P.EST_CODIGO = " + estCodigo+ " AND P.COD_BARRA LIKE '%" 
                + codBarra + "%' AND P.PROD_CODIGO IN (SELECT PROD_CODIGO FROM PRODUTOS WHERE EST_CODIGO = " 
                + estCodigo + " AND TAB_CODIGO = 1 AND PROD_LIBERADO = 'S')";

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

        public static DataTable GetDadosProdutos(int estCodigo, string produto)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("codBarra",produto));

            Principal.strSql = "SELECT P.PROD_DESCR, U.UNI_DESC_ABREV, P.PRE_VALOR, P.PROD_CODIGO, P.PROD_CONTROLADO, P.PROD_ESTATUAL, "
                   + " P.DEP_CODIGO, P.CLAS_CODIGO, P.SUB_CODIGO, P.PROD_ECF, P.PROD_ULTCUSME, N.NCM, P.PROD_CUSME, P.PROD_UNIDADE "
                   + " FROM PRODUTOS P "
                   + " INNER JOIN TIPO_UNIDADE U ON U.UNI_CODIGO = P.PROD_UNIDADE "
                   + " LEFT JOIN PRODUTOS_NCM N ON N.COD_BARRA = @codBarra"
                   + " WHERE P.EST_CODIGO = @est_codigo";
            if (!String.IsNullOrEmpty(produto))
            {
                Principal.strSql += "  AND P.PROD_CODIGO = (SELECT PROD_CODIGO FROM PRODUTOS_BARRA WHERE EST_CODIGO = @est_codigo AND COD_BARRA = @codBarra) ";
            }
            Principal.strSql += " AND P.TAB_CODIGO = 1 AND P.PROD_LIBERADO = 'S'";

            return BancoDados.GetDataTable(Principal.strSql,ps);
        }

        public static DataTable GetBuscaProdutos(int estCodigo, string descr = "", string codBarras = "", string preco = "", string prinAtivo = "", string filtro = "")
        {
            Principal.strSql = "SELECT A.PROD_CODIGO, B.COD_BARRA, A.PROD_DESCR, A.PROD_ESTATUAL, A.PRE_VALOR, A.MED_PMC, 0 AS DESC_MIN, 0 AS DESC_MAX, C.DEP_DESCR, ";
            Principal.strSql += "D.FAB_DESCR, E.PRI_DESCR, 'N' AS PROD_PROMO, C.DEP_CODIGO, F.CLAS_CODIGO, G.SUB_CODIGO ";
            Principal.strSql += "FROM PRODUTOS A ";
            Principal.strSql += "LEFT JOIN DEPARTAMENTOS C ON (A.DEP_CODIGO = C.DEP_CODIGO AND A.EST_CODIGO = C.EST_CODIGO) ";
            Principal.strSql += "LEFT JOIN FABRICANTES D ON (A.FAB_CODIGO = D.FAB_CODIGO AND A.EST_CODIGO = D.EST_CODIGO) ";
            Principal.strSql += "LEFT JOIN PRINCIPIO_ATIVO E ON (A.PRI_CODIGO = E.PRI_CODIGO AND A.EST_CODIGO = E.EST_CODIGO) ";
            Principal.strSql += "LEFT JOIN CLASSES F ON (A.CLAS_CODIGO = F.CLAS_CODIGO AND A.EST_CODIGO = F.EST_CODIGO) ";
            Principal.strSql += "LEFT JOIN SUBCLASSES G ON (A.SUB_CODIGO = G.SUB_CODIGO AND A.EST_CODIGO = G.EST_CODIGO), ";
            Principal.strSql += "PRODUTOS_BARRA B ";
            Principal.strSql += "WHERE A.EST_CODIGO = " + estCodigo + " AND A.PROD_CODIGO = B.PROD_CODIGO ";
            Principal.strSql += "AND B.DTCADASTRO = (SELECT MAX(DTCADASTRO) FROM PRODUTOS_BARRA WHERE EST_CODIGO = " + estCodigo + " AND PROD_CODIGO = A.PROD_CODIGO) ";
            Principal.strSql += "AND A.PROD_LIBERADO = 'S' AND A.TAB_CODIGO = 1 ";
            if (descr != "")
            {
                Principal.strSql += "AND A.PROD_DESCR LIKE '%" + descr + "%' ";
            }
            if (preco != "")
            {
                Principal.strSql += " AND A.PRE_VALOR = " + preco;
             
            }
            if (codBarras != "")
            {
                Principal.strSql += "AND B.COD_BARRA  LIKE '%" + codBarras + "%' ";
            }
            if (prinAtivo != "")
            {
                Principal.strSql += "AND A.PRI_CODIGO = " + prinAtivo + " ";
            }
            if (!String.IsNullOrEmpty(filtro))
            {
                Principal.strSql += " AND " + filtro;
            }
            Principal.strSql += "ORDER BY A.PROD_CODIGO";

            return BancoDados.GetDataTable(Principal.strSql, null);
        }
    }
}
