﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    class FPVendedores
    {
        public int est_codigo { get; set; }
        public string col_codigo { get; set; }
        public string usuario_datasus { get; set; }
        public string senha { get; set; }
        public DateTime dtalteracao { get; set; }
        public string opalteracao { get; set; }
        public DateTime dtcadastro { get; set; }
        public string opcadastro { get; set; }

        public FPVendedores() { }

        public static bool InserirDados(FPVendedores dados)
        {

            string sql = "INSERT INTO FP_VENDEDORES VALUES(" + dados.est_codigo + ", " + dados.est_codigo + ", '" + dados.col_codigo + "', '"
                         + dados.usuario_datasus + "', '" + dados.senha + "', " + Funcoes.BDataHora(DateTime.Now) + ", '"
                         + dados.opcadastro + "', " + Funcoes.BDataHora(DateTime.Now) + ", '" + dados.opcadastro + "')";
            
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static DataTable GetRegistros(string clasCampo, string clasDescr, bool texto = false)
        {
            Principal.strSql = "SELECT * FROM FP_VENDEDORES WHERE EST_CODIGO  = " + Principal.estAtual + " AND " + clasCampo + " = ";
            if(texto)
            {
                Principal.strSql += "'" + Convert.ToDouble(clasDescr).ToString() + "'";
            }
            else
            {
                Principal.strSql += Convert.ToDouble(clasDescr).ToString();
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

        public static bool AtualizaDados(FPVendedores dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;
            string sql;

            sql = "UPDATE FP_VENDEDORES SET USUARIO_DATASUS = '" + dadosNew.usuario_datasus + "', SENHA_DATASUS = '" + dadosNew.senha +
                      "', DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = '" + dadosNew.opalteracao + "' WHERE EST_CODIGO = " +
                      dadosNew.est_codigo + " AND COL_CODIGO = '" + dadosNew.col_codigo + "'";

            if (!dadosNew.usuario_datasus.Equals(dadosOld.Rows[0]["usuario_datasus"]))
            {
                if (GetRegistros("USUARIO_DATASUS", dadosNew.usuario_datasus, true).Rows.Count > 1)
                {
                    Principal.mensagem = "Usuário já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                dados[contMatriz, 0] = "USUARIO_DATASUS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["USUARIO_DATASUS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.usuario_datasus + "'";
                contMatriz = contMatriz + 1;
            }
            
            if (!dadosNew.senha.Equals(dadosOld.Rows[0]["SENHA_DATASUS"]))
            {
                dados[contMatriz, 0] = "SENHA_DATASUS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SENHA_DATASUS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.senha + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                if (BancoDados.ExecuteNoQuery(sql, null) == 1)
                {
                    if (Funcoes.GravaLogAlteracao("COL_CODIGO", dadosNew.col_codigo, Principal.usuario, "FP_VENDEDORES", dados, contMatriz, Principal.estAtual).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;

        }

        public static DataTable GetBuscar(FPVendedores dadosBusca)
        {
            Principal.strSql = "SELECT A.EST_CODIGO, A.COL_CODIGO, C.COL_NOME, A.USUARIO_DATASUS, A.DAT_ALTERACAO, "
                     + " (SELECT LOGIN_ID FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, "
                     + " SENHA_DATASUS FROM FP_VENDEDORES A "
                     + " LEFT JOIN COLABORADORES C ON (C.COL_CODIGO = A.COL_CODIGO AND C.EMP_CODIGO = A.EST_CODIGO) "
                     + " WHERE C.COL_STATUS IN ('X','V') AND A.EST_CODIGO = " + Principal.estAtual;
            if (dadosBusca.col_codigo == "" && dadosBusca.usuario_datasus == "")
            {
                Principal.strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.COL_CODIGO";
            }
            else
            {
                if (dadosBusca.col_codigo != "")
                {
                    Principal.strSql += " AND A.COL_CODIGO = " + dadosBusca.col_codigo;
                }
                if (dadosBusca.usuario_datasus != "")
                {
                    Principal.strSql += " AND A.USUARIO_DATASUS LIKE '%" + dadosBusca.usuario_datasus + "%'";
                }
                Principal.strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.COL_CODIGO";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

        public static int ExcluirDados(string colCodigo)
        {
            Principal.strCmd = "DELETE FROM FP_VENDEDORES WHERE EST_CODIGO = " + Principal.estAtual + " AND COL_CODIGO = '" + colCodigo + "'";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, null);
        }

        public static string GetConsultaVendedor(int estCodigo, string cpf)
        {
            Principal.strSql = "SELECT COALESCE(SENHA_DATASUS,'0') SENHA FROM FP_VENDEDORES WHERE EST_CODIGO = " + estCodigo + " AND USUARIO_DATASUS = " + cpf;

            String result = BancoDados.ExecuteScalar(Principal.strSql, null).ToString();
            if (String.IsNullOrEmpty(result))
                return string.Empty;
            else
                return result;
        }
    }
}
