﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class LogLiberacaoDesc
    {
        public string LogOperador { get; set; }
        public string CodBarras { get; set; }
        public double PreValor { get; set; }
        public int Qtde { get; set; }
        public double DescontoMax { get; set; }
        public double DescontoLib { get; set; }
        public double ValorVenda { get; set; }
        public int ColCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaID { get; set; }
        public DateTime LogData { get; set; }

        public LogLiberacaoDesc() { }

        public LogLiberacaoDesc(string logOperador, string codBarras, double preValor, int qtde, double descontoMax, double descontoLib, double valorVenda, int colCodigo, int empCodigo, int estCodigo,
            long vendaId, DateTime logData)
        {
            this.LogOperador = logOperador;
            this.CodBarras = codBarras;
            this.PreValor = preValor;
            this.Qtde = qtde;
            this.DescontoMax = descontoMax;
            this.DescontoLib = descontoLib;
            this.ValorVenda = valorVenda;
            this.ColCodigo = colCodigo;
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.VendaID = vendaId;
            this.LogData = logData;
        }

        public bool InsereRegistros(LogLiberacaoDesc dados)
        {
            string strCmd = "INSERT INTO LOG_LIBERACAO_DESC(LOG_OPERADOR, CODBARRAS, PRE_VALOR, QTDE, DESC_MAX, DESC_LIBERADO, VALOR_VENDA, COL_CODIGO, EMP_CODIGO, EST_CODIGO,"
                + " VENDA_ID, LOG_DATA) VALUES ('"
                + dados.LogOperador + "','"
                + dados.CodBarras + "',"
                + Funcoes.BFormataValor(dados.PreValor) + ","
                + dados.Qtde + ","
                + Funcoes.BFormataValor(dados.DescontoMax) + ","
                + Funcoes.BFormataValor(dados.DescontoLib) + ","
                + Funcoes.BFormataValor(dados.ValorVenda) + ","
                + dados.ColCodigo + ","
                 + dados.EstCodigo + ","
                + dados.EmpCodigo + ","
                + dados.VendaID + ","
                + Funcoes.BDataHora(dados.LogData) + ")";
               
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool ExcluirDados(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "DELETE FROM LOG_LIBERACAO_DESC WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND VENDA_ID = " + vendaID;

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

    }
}
