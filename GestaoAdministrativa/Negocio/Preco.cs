﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class Preco
    {
        public int ProdID { get; set; }
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public int TabCodigo { get; set; }
        public double PreValor { get; set; }
        public double PreDesconto { get; set; }
        public double PreDescontoPorc { get; set; }
        public double PreDescProm { get; set; }
        public double PrePromocao { get; set; }
        public DateTime PreInicioPromocao { get; set; }
        public DateTime PreFimPromocao { get; set; }
        public double ProdPMC { get; set; }
        public double PreFpComDesc { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string BloqDesconto { get; set; }
        public string AtualizadoABCFarma { get; set; }

        public Preco() { }

        public Preco(int prodId, int empCodigo, int estCodigo, string prodCodigo, int tabCodigo, double preValor, double preDesconto, double preDescontoPorc,
            double preDescProm, double prePromocao, DateTime preInicioPromocao, DateTime preFimPromocao, double prodPmc, DateTime dtAlteracao, string opAlteracao,
            DateTime dtCadastro, string opCadastro, string bloqdesconto, string atualizadoAbcFarma)
        {
            this.ProdID = prodId;
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ProdCodigo = prodCodigo;
            this.TabCodigo = tabCodigo;
            this.PreValor = preValor;
            this.PreDesconto = preDesconto;
            this.PreDescontoPorc = preDescontoPorc;
            this.PreDescProm = preDescProm;
            this.PrePromocao = prePromocao;
            this.PreInicioPromocao = preInicioPromocao;
            this.PreFimPromocao = preFimPromocao;
            this.ProdPMC = prodPmc;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.BloqDesconto = bloqdesconto;
            this.AtualizadoABCFarma = atualizadoAbcFarma;
        }


        public bool AtualizaPreValorEntradaNotas(Preco dadosPrecos)
        {
            string[,] logAtualiza = new string[4, 3];
            int contMatriz = 0;
            string strCmd;
            var dt = new DataTable();

            dt = BancoDados.GetDataTable("SELECT A.PRE_VALOR,"
                                   + " A.DAT_ALTERACAO,"
                                   + " A.OPALTERACAO,"
                                   + " MED_PCO18,"
                                   + " B.PROD_CAIXA_CARTELADO,"
                                   + " B.PROD_CODIGO_CARTELADO"
                             + " FROM PRECOS A"
                             + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                             + " WHERE EST_CODIGO = " + dadosPrecos.EmpCodigo
                             + "  AND EMP_CODIGO = " + dadosPrecos.EstCodigo
                             + "  AND A.PROD_CODIGO = '" + dadosPrecos.ProdCodigo + "'", null);

            strCmd = "UPDATE PRECOS SET ";
            if (!dadosPrecos.PreValor.ToString().Equals(String.Format("{0:N}", dt.Rows[0]["PRE_VALOR"])))
            {
                logAtualiza[contMatriz, 0] = "PRE_VALOR";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PRE_VALOR"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosPrecos.PreValor + "'";
                strCmd += "PRE_VALOR = " + Funcoes.BValor(dadosPrecos.PreValor) + ",";
                contMatriz = contMatriz + 1;
            }
            
            logAtualiza[contMatriz, 0] = "MED_PCO18";
            logAtualiza[contMatriz, 1] = dt.Rows[0]["MED_PCO18"].ToString() == "" ? "null" : "'" + dt.Rows[0]["MED_PCO18"] + "'";
            logAtualiza[contMatriz, 2] = Funcoes.BValor(dadosPrecos.ProdPMC);
            strCmd += " MED_PCO18 = " + Funcoes.BValor(dadosPrecos.ProdPMC) + ",";
            contMatriz = contMatriz + 1;

            logAtualiza[contMatriz, 0] = "DAT_ALTERACAO";
            logAtualiza[contMatriz, 1] = dt.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["DAT_ALTERACAO"] + "'";
            logAtualiza[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
            strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
            contMatriz = contMatriz + 1;

            logAtualiza[contMatriz, 0] = "OPALTERACAO";
            logAtualiza[contMatriz, 1] = dt.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["OPALTERACAO"] + "'";
            logAtualiza[contMatriz, 2] = "'" + dadosPrecos.OpAlteracao + "'";
            strCmd += " OPALTERACAO = '" + dadosPrecos.OpAlteracao + "' ";
            contMatriz = contMatriz + 1;

            strCmd += " WHERE EST_CODIGO = " + dadosPrecos.EstCodigo + " AND EMP_CODIGO = " + dadosPrecos.EmpCodigo + "  AND PROD_CODIGO = '" + dadosPrecos.ProdCodigo + "'";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
            {
                if (dt.Rows[0]["PROD_CAIXA_CARTELADO"].ToString().Equals("S"))
                {
                    strCmd = "UPDATE PRECOS SET PRE_VALOR = " + Funcoes.BValor(dadosPrecos.PreValor) + " WHERE PROD_CODIGO = '" + dt.Rows[0]["PROD_CODIGO_CARTELADO"].ToString() + "' AND EMP_CODIGO = " + dadosPrecos.EmpCodigo
                        + " AND EST_CODIGO = " + dadosPrecos.EstCodigo;
                    BancoDados.ExecuteNoQueryTrans(strCmd, null);
                }
                Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosPrecos.ProdCodigo, dadosPrecos.OpAlteracao, "PRECOS", logAtualiza, contMatriz, dadosPrecos.EstCodigo, dadosPrecos.EmpCodigo, true);
            }
            

            return true;
        }

        public bool InsereProdutosPrecosNovos(Preco dados)
        {
            string strCmd = "INSERT INTO PRECOS (EMP_CODIGO, EST_CODIGO,PROD_CODIGO, PRE_VALOR , TAB_CODIGO, DTCADASTRO, OPCADASTRO, PROD_ID) VALUES("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ",'"
                + dados.ProdCodigo + "',"
                + Funcoes.BFormataValor(dados.PreValor) + ","
                + dados.TabCodigo + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "'," + dados.ProdID + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool IndentificaProdutoCadastradoNaPrecos(Preco dadosProduto)
        {
            string strSql = "SELECT PROD_CODIGO FROM PRECOS"
                        + " WHERE EST_CODIGO = " + dadosProduto.EstCodigo
                        + " AND EMP_CODIGO = " + dadosProduto.EmpCodigo
                        + " AND PROD_CODIGO = '" + dadosProduto.ProdCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return false;
            else
                return true;
        }


        public DataTable DadosPrecos(string prodCodigo, int prodID)
        {
            string strSql = "SELECT * FROM PRECOS A" +
                 " WHERE A.PROD_CODIGO = '" + prodCodigo + "'" +
                 " AND A.PROD_ID = " + prodID +
                 " AND A.EMP_CODIGO = " + Principal.empAtual +
                 " AND A.EST_CODIGO = " + Principal.estAtual;

            return BancoDados.GetDataTable(strSql, null);
        }

        // Cadastro Produto
        public bool InsereProdutos(Preco dadosPreco)
        {
            string sql = " INSERT INTO PRECOS (PROD_ID, EMP_CODIGO, EST_CODIGO, PROD_CODIGO, TAB_CODIGO, PRE_VALOR, BLOQUEIA_DESCONTO, "
                       + " DTCADASTRO, OPCADASTRO, ATUALIZADO_ABCFARMA, MED_PCO18) VALUES ( "
                       + dadosPreco.ProdID + ",'"
                       + dadosPreco.EmpCodigo + "','"
                       + dadosPreco.EstCodigo + "','"
                       + dadosPreco.ProdCodigo + "','"
                       + dadosPreco.TabCodigo + "',"
                       + Funcoes.BFormataValor(dadosPreco.PreValor) + ",'"
                       + dadosPreco.BloqDesconto + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "','" + dadosPreco.AtualizadoABCFarma + "',"
                       + Funcoes.BFormataValor(dadosPreco.ProdPMC) + ")";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("PROD_ID", dadosPreco.ProdID.ToString(), Principal.usuario, "PRECOS", dadosPreco.ProdCodigo, Principal.empAtual, Principal.estAtual, true);
                return true;
            }
            else
            {
                return false;
            }

        }

        public DataTable ProdutosDevolucao(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT A.PROD_CODIGO, A.PROD_DESCR, B.EMP_CODIGO, B.EST_CODIGO, B.PRE_VALOR, A.PROD_ID"
                + " FROM PRODUTOS A "
                + " INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                + " INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO = C.PROD_CODIGO"
                + " WHERE B.EST_CODIGO = " + estCodigo
                + " AND B.EMP_CODIGO = " + empCodigo
                + " AND B.EMP_CODIGO = C.EMP_CODIGO"
                + " AND B.EST_CODIGO = C.EST_CODIGO"
                + " AND A.PROD_CODIGO = C.PROD_CODIGO"
                + " AND A.PROD_CODIGO = '" + prodCodigo + "'"
                + " AND A.PROD_ID = B.PROD_ID"
                + " AND C.PROD_SITUACAO = 'A'"
                + " AND B.TAB_CODIGO = 1";
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaPreco(Preco dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE PRECOS SET "
                       + " TAB_CODIGO = " + dadosNovos.TabCodigo
                       + " ,PRE_VALOR =  " + Funcoes.BValor(dadosNovos.PreValor)
                       + " ,OPALTERACAO = '" + Principal.usuario + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                       + " , BLOQUEIA_DESCONTO = '" + dadosNovos.BloqDesconto + "'"
                       + " , ATUALIZADO_ABCFARMA = '" + dadosNovos.AtualizadoABCFarma + "'"
                       + " , MED_PCO18 = " + Funcoes.BValor(dadosNovos.ProdPMC)
                        + " WHERE PROD_ID = " + dadosNovos.ProdID + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[5, 3];
                int contMatriz = 0;

                if (!dadosNovos.TabCodigo.ToString().Equals(dadosAntigos.Rows[0]["TAB_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["TAB_CODIGO"]).ToString()))
                {
                    dados[contMatriz, 0] = "TAB_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_CODIGO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.BloqDesconto.ToString().Equals(dadosAntigos.Rows[0]["BLOQUEIA_DESCONTO"].ToString()))
                { 
                    dados[contMatriz, 0] = "BLOQUEIA_DESCONTO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["BLOQUEIA_DESCONTO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.BloqDesconto + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.PreValor.ToString().Equals(dadosAntigos.Rows[0]["PRE_VALOR"].ToString() == "" ? "0" : Convert.ToDouble(dadosAntigos.Rows[0]["PRE_VALOR"]).ToString()))
                {
                    dados[contMatriz, 0] = "PRE_VALOR";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PRE_VALOR"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.PreValor + "'";
                    contMatriz = contMatriz + 1;
                    
                    BancoDados.ExecuteNoQueryTrans("INSERT INTO LOG_ALTERA_PRECO VALUES(" + Principal.empAtual + "," + Principal.estAtual + ",'" + dadosNovos.ProdCodigo + "',"
                        + Funcoes.BDataHora(DateTime.Now) + "," + Funcoes.BValor(Convert.ToDouble(dadosAntigos.Rows[0]["PRE_VALOR"])) + "," + Funcoes.BValor(dadosNovos.PreValor) + ",'" + Principal.usuario + "')", null);
                }

                Funcoes.GravaLogAlteracao("PROD_ID", dadosNovos.ProdID.ToString(), Principal.usuario, "PRECOS", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                return true;
            }
            else
                return false;
        }

        public bool ExcluirPorCodigoBarras(string codBarras)
        {
            string sql = "DELETE FROM PRECOS WHERE PROD_CODIGO = '" + codBarras + "'  AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;
            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }


        public bool AtualizaPrecoAbcFarmaPorProdCodigo(Preco dadosNovos, double preValor)
        {

            string sql = " UPDATE PRECOS SET "
                       + "  PRE_VALOR =  " + Funcoes.BValor(dadosNovos.PreValor)
                       + " ,MED_PCO18 = " + Funcoes.BValor(dadosNovos.PreValor)
                       + " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " , ATUALIZADO_ABCFARMA = '" + dadosNovos.AtualizadoABCFarma + "'"
                       + " WHERE PROD_CODIGO = '" + dadosNovos.ProdCodigo + "' AND EST_CODIGO = " + dadosNovos.EstCodigo + " AND EMP_CODIGO = " + dadosNovos.EmpCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                string[,] dados = new string[1, 3];

                dados[0, 0] = "PRE_VALOR";
                dados[0, 1] = "'" + Funcoes.BValor(preValor) + "'";
                dados[0, 2] = "'" + dadosNovos.PreValor + "'";

                Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosNovos.ProdCodigo, Principal.usuario, "PRECOS", dados, 0, Principal.estAtual, Principal.empAtual, true);

                var log = new LogPrecos();

                log.EmpCodigo = dadosNovos.EmpCodigo;
                log.EstCodigo = dadosNovos.EstCodigo;
                log.CodBarras = dadosNovos.ProdCodigo;
                log.LogData = DateTime.Now;
                log.LogAnterior = preValor;
                log.LogAlterado = dadosNovos.PreValor;
                log.LogOperador = Principal.usuario;

                log.InsereRegistros(log);

                return true;
            }
            else
                return false;
        }

        public bool InsereProdutosPrecosNovosAbcFarma(Preco dados)
        {
            string strCmd = "INSERT INTO PRECOS (EMP_CODIGO, EST_CODIGO,PROD_CODIGO, PRE_VALOR , TAB_CODIGO, DTCADASTRO, OPCADASTRO, MED_PCO18, PROD_ID) VALUES("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ",'"
                + dados.ProdCodigo + "',"
                + Funcoes.BFormataValor(dados.PreValor) + ","
                + dados.TabCodigo + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "',"
                + Funcoes.BFormataValor(dados.ProdPMC) + ","
                + dados.ProdID + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaPrecoPorID(int id, double valorNovo, double valorAntigo)
        {
            string sql = " UPDATE PRECOS SET PRE_VALOR = '" + valorNovo + "'"
                       + " WHERE PROD_ID =" + id + " AND EST_CODIGO = " + Principal.estAtual + " AND EMP_CODIGO = " + Principal.empAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[1, 3];
                dados[0, 0] = "PRECOS";
                dados[0, 1] = "'" + valorAntigo + "'";
                dados[0, 2] = "'" + valorNovo + "'";

                Funcoes.GravaLogAlteracao("PROD_ID", id.ToString(), Principal.usuario, "PRECOS", dados, 1, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaPrecoProduto(int empCodigo, int estCodigo, string prodCodigo)
        {
            string strSql = "SELECT A.PROD_DESCR, B.PRE_VALOR"
                        + "      FROM PRODUTOS A"
                        + "     INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                        + "     WHERE B.EMP_CODIGO = " + empCodigo
                        + "       AND B.EST_CODIGO = " + estCodigo
                        + "       AND A.PROD_CODIGO = '" + prodCodigo + "'";

            return BancoDados.selecionarRegistros(strSql);
        }

        public bool AtualizaPrecoAbc(int empAtual, int estAtual)
        {
            string sql = " UPDATE PRECOS SET ATUALIZADO_ABCFARMA = 'N' WHERE EMP_CODIGO = " + empAtual + " AND EST_CODIGO = " + estAtual;

            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public bool ExcluiLogPrecoAbcFarma(int empCodigo, int estCodigo)
        {
            string sql = "DELETE FROM LOG_PRECOS WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;
            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable RelatorioSinteticoAbcFarma(int empCodigo, int estCodigo, string alterado, int ordem, string filtro)
        {
            string strSql = "SELECT A.PROD_CODIGO, C.PROD_DESCR, COALESCE(B.PRE_VALOR, 0) AS PRE_VALOR, B.ATUALIZADO_ABCFARMA,COALESCE(D.LOG_ANTERIOR, 0) AS LOG_ANTERIOR"
                            + "      FROM PRODUTOS_DETALHE A"
                            + "     INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "     INNER JOIN PRODUTOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                            + "     LEFT JOIN LOG_PRECOS D ON (A.PROD_CODIGO = D.CODBARRAS AND"
                            + "     D.EMP_CODIGO = A.EMP_CODIGO AND"
                            + "     A.EST_CODIGO = D.EST_CODIGO)"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "       AND A.EST_CODIGO = " + estCodigo
                            + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = B.EST_CODIGO"
                            + "       AND B.ATUALIZADO_ABCFARMA = '" + alterado + "'";
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            if (ordem.Equals(0))
            {
                strSql += " ORDER BY C.PROD_DESCR";
            }
            else
            {
                strSql += " ORDER BY A.PROD_CODIGO";
            }

            return BancoDados.selecionarRegistros(strSql);
        }
    }
}
