﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Despesa
    {
        public int DespCodigo { get; set; }
        public string DespDescr { get; set; }
        public string DespLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Despesa() { }

        public Despesa(int despCodigo, string despDescr, string despLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.DespCodigo = despCodigo;
            this.DespDescr = despDescr;
            this.DespLiberado = despLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.OpCadastro = opCadastro;
            this.DtCadastro = dtCadastro;
        }

        public DataTable BuscarDados(Despesa dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.DESP_CODIGO, A.DESP_DESCRICAO, CASE WHEN A.DESP_DESABILITADO = 'S' THEN 'N' ELSE 'S' END AS DESP_DESABILITADO, A.DAT_ALTERACAO, A.OP_ALTERACAO, A.DT_CADASTRO, A.OP_CADASTRO"
                       + " FROM CAD_DESPESAS A  ";
            if (dadosBusca.DespCodigo == 0 && dadosBusca.DespDescr == "" && dadosBusca.DespLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY DESP_CODIGO";
            }
            else
            {
                if (dadosBusca.DespCodigo != 0)
                {
                    strSql += " WHERE A.DESP_CODIGO = " + dadosBusca.DespCodigo;
                }
                if (dadosBusca.DespDescr != "")
                {
                    if (dadosBusca.DespCodigo != 0)
                    {
                        strSql += " AND A.DESP_DESCR LIKE '%" + dadosBusca.DespDescr + "%'";
                    }
                    else
                        strSql += " WHERE A.DESP_DESCR LIKE '%" + dadosBusca.DespDescr + "%'";
                }
                if (dadosBusca.DespLiberado != "TODOS")
                {
                    if (dadosBusca.DespCodigo != 0 || dadosBusca.DespDescr != "")
                    {
                        strSql += " AND A.DESP_DESABILITADO = '" + dadosBusca.DespLiberado + "'";
                    }
                    else
                        strSql += " WHERE A.DESP_DESABILITADO = '" + dadosBusca.DespLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.DESP_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(Despesa dados)
        { 
            string strCmd = "INSERT INTO CAD_DESPESAS(DESP_CODIGO, DESP_DESCRICAO, DESP_DESABILITADO, DT_CADASTRO,  OP_CADASTRO) VALUES (" +
                dados.DespCodigo + ",'" +
                dados.DespDescr + "','" +
                dados.DespLiberado + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Despesa dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;
            StringBuilder cmd = new StringBuilder();
            cmd.Append("UPDATE CAD_DESPESAS SET ");

            if (!dadosNew.DespDescr.Equals(dadosOld.Rows[0]["DESP_DESCRICAO"]))
            {
                //VERIFICA SE CLASSE JA ESTA CADASTRADA//
                if (Util.RegistrosPorEstabelecimento("CAD_DESPESAS", "DESP_DESCRICAO", dadosNew.DespDescr).Rows.Count != 0)
                {
                    Principal.mensagem = "Despesa já cadastrada.";
                    Funcoes.Avisa();
                    return false;
                }

                cmd.Append("DESP_DESCRICAO = '" + dadosNew.DespDescr + "',");
                dados[contMatriz, 0] = "DESP_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESP_DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DespDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DespLiberado.Equals(dadosOld.Rows[0]["DESP_DESABILITADO"]))
            {
                cmd.Append("DESP_DESABILITADO = '" + dadosNew.DespLiberado + "',");
                dados[contMatriz, 0] = "DESP_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESP_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DespLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OP_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OP_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OP_ALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                cmd.Append("DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",");
                cmd.Append("OP_ALTERACAO = '" + Principal.usuario + "'");
                cmd.Append("WHERE DESP_CODIGO = " + dadosNew.DespCodigo);

                if (BancoDados.ExecuteNoQuery(cmd.ToString(),null) == 1)
                {
                    if (Funcoes.GravaLogAlteracao("DESP_CODIGO", dadosNew.DespCodigo.ToString(), Principal.usuario, "CAD_DESPESAS", dados, contMatriz, Principal.estAtual).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return true;
        }

        public string IdentificaDespesa(int despID, int empCodigo)
        {
            string strSql = "SELECT DESP_DESCRICAO FROM CAD_DESPESAS WHERE DESP_CODIGO = " + despID;


            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public int ExcluirDados(string despCodigo)
        {
            string strCmd = "DELETE FROM CAD_DESPESAS WHERE DESP_CODIGO = " + Convert.ToDouble(despCodigo).ToString();

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
