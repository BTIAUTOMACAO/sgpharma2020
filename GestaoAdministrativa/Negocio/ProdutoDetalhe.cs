﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    public class ProdutoDetalhe
    {
        public int ProdID { get; set; }
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public int DepCodigo { get; set; }
        public int ClasCodigo { get; set; }
        public int SubCodigo { get; set; }
        public int FabCodigo { get; set; }
        public string ProdBloqCompra { get; set; }
        public DateTime ProdDTEstIni { get; set; }
        public int ProdEstIni { get; set; }
        public int ProdEstMin { get; set; }
        public int ProdEstMax { get; set; }
        public int ProdEstAtual { get; set; }
        public double ProdCusIni { get; set; }
        public double ProdCusme { get; set; }
        public double ProdUltCusme { get; set; }
        public string ProdEcf { get; set; }
        public string ProdUnComp { get; set; }
        public double ProdQtdeUnComp { get; set; }
        public double ProdComissao { get; set; }
        public double ProdPreCompra { get; set; }
        public double ProdMargem { get; set; }
        public string ProdLista { get; set; }
        public string ProdLiberado { get; set; }
        public string ProdNcm { get; set; }
        public string ProdMsg { get; set; }
        public string ProdCfop { get; set; }
        public string ProdCst { get; set; }
        public string ProdCest { get; set; }
        public string ProdIcms { get; set; }
        public string ProdCsosn { get; set; }
        public double ProdValorBaseIcms { get; set; }
        public double ProdAliqIcms { get; set; }
        public double ProdValorIcms { get; set; }
        public double ProdBaseIcmsST { get; set; }
        public double ProdBaseIcmsRet { get; set; }
        public double ProdIcmsRet { get; set; }
        public double ProdValorIcmsSt { get; set; }
        public string ProdEnqIpi { get; set; }
        public string ProdCstIpi { get; set; }
        public double ProdBaseIpi { get; set; }
        public double ProdAliqIpi { get; set; }
        public string ProdOrigem { get; set; }
        public string ProdCstCofins { get; set; }
        public double ProdValorIpi { get; set; }
        public string ProdCstPis { get; set; }
        public string ProdDCB { get; set; }
        public double ProdQtdeUnCompFP { get; set; }
        public string ProdCfopDevolucao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public ProdutoDetalhe() { }

        public ProdutoDetalhe(int prodId, int estCodigo, int empCodigo, string prodCodigo, int depCodigo, int clasCodigo, int subCodigo, int fabCodigo,
            string prodBloqCompra, DateTime prodDtEstIni, int prodEstIni, int prodEstMin, int prodEstMax, int prodEstAtual, double prodCusIni, double prodCusme,
            double prodUltCusme, string prodEcf, string prodUnComp, double prodQtdeUnComp, double prodComissao, double prodPreCompra, double prodMargem,
            string prodLista, string prodLiberado, string prodNcm, string prodMsg, string prodCfop, string prodCst, string prodCest,
            string prodIcms, double prodValorBaseIcms, double prodAliqIcms, double prodValorIcms, double prodBaseIcmsSt, double prodBaseIcmsRet, double prodIcmsRet,
            double prodValorIcmsSt, string prodEnqIpi, string prodCstIpi, double prodBaseIpi, double prodAliqIpi, string prodOrigem, string prodCstCofins, double prodValorIpi,
            string prodCstPis, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro, string prodCsosn, string prodDCB, double prodQtdeUnCompFP, string prodCfopDevolucao)
        {
            this.ProdID = ProdID;
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.ProdCodigo = prodCodigo;
            this.DepCodigo = depCodigo;
            this.ClasCodigo = clasCodigo;
            this.SubCodigo = subCodigo;
            this.FabCodigo = fabCodigo;
            this.ProdBloqCompra = prodBloqCompra;
            this.ProdDTEstIni = prodDtEstIni;
            this.ProdEstIni = prodEstIni;
            this.ProdEstMin = prodEstMin;
            this.ProdEstMax = prodEstMax;
            this.ProdEstAtual = prodEstAtual;
            this.ProdCusIni = prodCusIni;
            this.ProdCusme = prodCusme;
            this.ProdUltCusme = prodUltCusme;
            this.ProdEcf = prodEcf;
            this.ProdUnComp = prodUnComp;
            this.ProdQtdeUnComp = prodQtdeUnComp;
            this.ProdComissao = prodComissao;
            this.ProdPreCompra = prodPreCompra;
            this.ProdMargem = prodMargem;
            this.ProdLista = prodLista;
            this.ProdLiberado = prodLiberado;
            this.ProdNcm = prodNcm;
            this.ProdMsg = prodMsg;
            this.ProdCfop = prodCfop;
            this.ProdCst = prodCst;
            this.ProdCest = prodCest;
            this.ProdIcms = prodIcms;
            this.ProdValorBaseIcms = prodValorBaseIcms;
            this.ProdAliqIcms = prodAliqIcms;
            this.ProdValorIcms = prodValorIcms;
            this.ProdBaseIcmsST = prodBaseIcmsSt;
            this.ProdBaseIcmsRet = prodBaseIcmsRet;
            this.ProdValorIcmsSt = prodValorIcmsSt;
            this.ProdEnqIpi = prodEnqIpi;
            this.ProdCstIpi = prodCstIpi;
            this.ProdBaseIpi = prodBaseIpi;
            this.ProdAliqIpi = prodAliqIpi;
            this.ProdOrigem = prodOrigem;
            this.ProdCstCofins = prodCstCofins;
            this.ProdValorIpi = prodValorIpi;
            this.ProdCstPis = prodCstIpi;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.ProdCsosn = prodCsosn;
            this.ProdDCB = prodDCB;
            this.ProdQtdeUnCompFP = prodQtdeUnCompFP;
            this.ProdCfopDevolucao = prodCfopDevolucao;
        }

        public bool InsereProdutosDetalhe(ProdutoDetalhe dados)
        {

            string sql = " INSERT INTO PRODUTOS_DETALHE (EMP_CODIGO, EST_CODIGO, PROD_ID, PROD_CODIGO, PROD_DTESTINI,DEP_CODIGO, CLAS_CODIGO, SUB_CODIGO, FAB_CODIGO, "
                       + " PROD_CFOP, PROD_CST, PROD_CEST, PROD_ICMS "
                       + " ,PROD_CSOSN, PROD_CST_IPI, PROD_CST_COFINS,  PROD_ENQ_IPI, PROD_ESTINI, PROD_ESTATUAL "
                       + " ,PROD_ESTMIN, PROD_ESTMAX ,DTCADASTRO, OPCADASTRO, PROD_CUSINI, PROD_CUSME, PROD_PRECOMPRA"
                       + " ,PROD_MARGEM, PROD_COMISSAO, PROD_BLOQ_COMPRA, PROD_SITUACAO, PROD_ECF, PROD_UN_COMP,  PROD_QTDE_UN_COMP, PROD_LISTA,PROD_QTDE_UN_COMP_FP,"
                       + " PROD_DCB, PROD_CFOP_DEVOLUCAO) VALUES("

                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.ProdID + ",'"
                       + dados.ProdCodigo + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + dados.DepCodigo + "','"
                       + dados.ClasCodigo + "','"
                       + dados.SubCodigo + "','"
                       + dados.FabCodigo + "','"
                       + dados.ProdCfop + "','"
                       + dados.ProdCst + "','"
                       + dados.ProdCest + "','"
                       + dados.ProdIcms + "','"
                       + dados.ProdCsosn + "','"
                       + dados.ProdCstIpi + "','"
                       + dados.ProdCstCofins + "','"
                       + dados.ProdEnqIpi + "','"
                       + dados.ProdEstIni + "','"
                       + dados.ProdEstAtual + "','"
                       + dados.ProdEstMin + "','"
                       + dados.ProdEstMax + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "','"
                       + dados.ProdCusIni + "','"
                       + dados.ProdCusme + "','"
                       + dados.ProdPreCompra + "','"
                       + dados.ProdMargem + "','"
                       + dados.ProdComissao + "','"
                       + dados.ProdBloqCompra + "','"
                       + dados.ProdLiberado + "','"
                       + dados.ProdEcf + "','"
                       + dados.ProdUnComp + "','"
                       + dados.ProdQtdeUnComp + "','"
                       + dados.ProdLista + "'," + dados.ProdQtdeUnCompFP + ",'" + dados.ProdDCB + "','" + dados.ProdCfopDevolucao + "')";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("PROD_ID", dados.ProdID.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados.ProdCodigo, Principal.empAtual, Principal.estAtual, true);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaCadastro(ProdutoDetalhe dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE PRODUTOS_DETALHE SET "
                       + " DEP_CODIGO = " + dadosNovos.DepCodigo
                       + " ,CLAS_CODIGO = " + dadosNovos.ClasCodigo
                       + " ,SUB_CODIGO = " + dadosNovos.SubCodigo
                       + " ,FAB_CODIGO = " + dadosNovos.FabCodigo
                       + " ,PROD_CFOP = '" + dadosNovos.ProdCfop + "'"
                       + " ,PROD_CST = '" + dadosNovos.ProdCst + "'"
                       + " ,PROD_CEST = '" + dadosNovos.ProdCest + "'"
                       + " ,PROD_ICMS = '" + dadosNovos.ProdIcms + "'"
                       + " ,PROD_CSOSN = '" + dadosNovos.ProdCsosn + "'"
                       + " ,PROD_CST_IPI = '" + dadosNovos.ProdCstIpi + "'"
                       + " ,PROD_CST_PIS ='" + dadosNovos.ProdCstPis + "'"
                       + " ,PROD_CST_COFINS = '" + dadosNovos.ProdCstCofins + "'"
                       + " ,PROD_ENQ_IPI ='" + dadosNovos.ProdEnqIpi + "'"
                       + " ,PROD_ESTINI = '" + dadosNovos.ProdEstIni + "'"
                       + " ,PROD_ESTMIN = '" + dadosNovos.ProdEstMin + "'"
                       + " ,PROD_ESTMAX = '" + dadosNovos.ProdEstMax + "'"
                       + " ,PROD_CUSINI = '" + dadosNovos.ProdCusIni + "'"
                       + " ,PROD_CUSME = '" + dadosNovos.ProdCusme + "'"
                       + " ,PROD_PRECOMPRA= '" + dadosNovos.ProdPreCompra + "'"
                       + " ,PROD_MARGEM = '" + dadosNovos.ProdMargem + "'"
                       + " ,PROD_COMISSAO= '" + dadosNovos.ProdComissao + "'"
                       + " ,PROD_BLOQ_COMPRA= '" + dadosNovos.ProdBloqCompra + "'"
                       + " ,PROD_SITUACAO= '" + dadosNovos.ProdLiberado + "'"
                       + " ,PROD_ECF = '" + dadosNovos.ProdEcf + "'"
                       + " ,PROD_UN_COMP = '" + dadosNovos.ProdUnComp + "'"
                       + " ,PROD_QTDE_UN_COMP = '" + dadosNovos.ProdQtdeUnComp + "'"
                       + " ,PROD_LISTA = '" + dadosNovos.ProdLista + "'"
                       + " , PROD_QTDE_UN_COMP_FP = " + dadosNovos.ProdQtdeUnCompFP + ""
                       + " , PROD_DCB = '" + dadosNovos.ProdDCB + "'"
                       + " , PROD_CFOP_DEVOLUCAO = '" + dadosNovos.ProdCfopDevolucao + "'"
                       + " ,OPALTERACAO = '" + Principal.usuario + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                       + " WHERE PROD_ID = " + dadosNovos.ProdID + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[26, 3];
                int contMatriz = 0;

                if (!dadosNovos.DepCodigo.ToString().Equals(dadosAntigos.Rows[0]["DEP_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["DEP_CODIGO"]).ToString()))
                {
                    dados[contMatriz, 0] = "DEP_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["DEP_CODIGO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.DepCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ClasCodigo.ToString().Equals(dadosAntigos.Rows[0]["CLAS_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["CLAS_CODIGO"]).ToString()))
                {

                    dados[contMatriz, 0] = "CLAS_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CLAS_CODIGO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ClasCodigo + "'";
                    contMatriz = contMatriz + 1;
                }

                if (!dadosNovos.ProdCstPis.Equals(dadosAntigos.Rows[0]["PROD_CST_PIS"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CST_PIS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CST_PIS"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCstPis + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.SubCodigo.ToString().Equals(dadosAntigos.Rows[0]["SUB_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["SUB_CODIGO"]).ToString()))
                {

                    dados[contMatriz, 0] = "SUB_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["SUB_CODIGO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.SubCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.FabCodigo.ToString().Equals(dadosAntigos.Rows[0]["FAB_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["FAB_CODIGO"]).ToString()))
                {

                    dados[contMatriz, 0] = "FAB_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["FAB_CODIGO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.FabCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCfop.Equals(dadosAntigos.Rows[0]["PROD_CFOP"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CFOP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CFOP"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCfop + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCst.Equals(dadosAntigos.Rows[0]["PROD_CST"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CST";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CST"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCst + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCest.Equals(dadosAntigos.Rows[0]["PROD_CEST"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CEST";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CEST"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCest + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdIcms.Equals(dadosAntigos.Rows[0]["PROD_ICMS"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_ICMS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ICMS"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdIcms + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCsosn.Equals(dadosAntigos.Rows[0]["PROD_CSOSN"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CSOSN";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CSOSN"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCsosn + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCstIpi.Equals(dadosAntigos.Rows[0]["PROD_CST_IPI"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CST_IPI";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CST_IPI"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCstIpi + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCstCofins.Equals(dadosAntigos.Rows[0]["PROD_CST_COFINS"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_CST_COFINS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CST_COFINS"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCstCofins + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdEnqIpi.Equals(dadosAntigos.Rows[0]["PROD_ENQ_IPI"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_ENQ_IPI";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ENQ_IPI"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdEnqIpi + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdEstIni.ToString().Equals(dadosAntigos.Rows[0]["PROD_ESTINI"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["PROD_ESTINI"]).ToString()))
                {

                    dados[contMatriz, 0] = "PROD_ESTINI";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ESTINI"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdEstIni + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdEstMin.ToString().Equals(dadosAntigos.Rows[0]["PROD_ESTMIN"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["PROD_ESTMIN"]).ToString()))
                {
                    dados[contMatriz, 0] = "PROD_ESTMIN";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ESTMIN"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdEstMin + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdEstMax.ToString().Equals(dadosAntigos.Rows[0]["PROD_ESTMAX"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["PROD_ESTMAX"]).ToString()))
                {
                    dados[contMatriz, 0] = "PROD_ESTMAX";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ESTMAX"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdEstMax + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCusIni.Equals(dadosAntigos.Rows[0]["PROD_CUSINI"].ToString() == "" ?  0: Convert.ToDouble(dadosAntigos.Rows[0]["PROD_CUSINI"])))
                {
                    dados[contMatriz, 0] = "PROD_CUSINI";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CUSINI"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCusIni + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdCusme.Equals(dadosAntigos.Rows[0]["PROD_CUSME"].ToString() == "" ? 0 : Convert.ToDouble(dadosAntigos.Rows[0]["PROD_CUSME"])))
                {
                    dados[contMatriz, 0] = "PROD_CUSME";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CUSME"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdCusme + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdPreCompra.Equals(dadosAntigos.Rows[0]["PROD_PRECOMPRA"].ToString() == "" ?  0 : Convert.ToDouble(dadosAntigos.Rows[0]["PROD_PRECOMPRA"])))
                {
                    dados[contMatriz, 0] = "PROD_PRECOMPRA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_PRECOMPRA"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdPreCompra + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdMargem.Equals(dadosAntigos.Rows[0]["PROD_MARGEM"].ToString() == "" ? 0 : Convert.ToDouble(dadosAntigos.Rows[0]["PROD_MARGEM"])))
                {
                    dados[contMatriz, 0] = "PROD_MARGEM";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_MARGEM"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdMargem + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdComissao.Equals(dadosAntigos.Rows[0]["PROD_COMISSAO"].ToString() == "" ? 0 : Convert.ToDouble(dadosAntigos.Rows[0]["PROD_COMISSAO"])))
                {
                    dados[contMatriz, 0] = "PROD_COMISSAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_COMISSAO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdComissao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdBloqCompra.Equals(dadosAntigos.Rows[0]["PROD_BLOQ_COMPRA"].ToString()))
                {
                    dados[contMatriz, 0] = "PROD_BLOQ_COMPRA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_BLOQ_COMPRA"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdBloqCompra + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdLiberado.Equals(dadosAntigos.Rows[0]["PROD_SITUACAO"].ToString()))
                {
                    dados[contMatriz, 0] = "PROD_BLOQ_COMPRA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_SITUACAO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdEcf.Equals(dadosAntigos.Rows[0]["PROD_ECF"].ToString()))
                {
                    dados[contMatriz, 0] = "PROD_ECF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ECF"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdEcf + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdUnComp.Equals(dadosAntigos.Rows[0]["PROD_UN_COMP"].ToString()))
                {

                    dados[contMatriz, 0] = "PROD_UN_COMP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_UN_COMP"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdUnComp + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ProdQtdeUnComp.ToString().Equals(dadosAntigos.Rows[0]["PROD_QTDE_UN_COMP"].ToString() == "" ? "" : Convert.ToInt32(dadosAntigos.Rows[0]["PROD_QTDE_UN_COMP"]).ToString()))
                {

                    dados[contMatriz, 0] = "PROD_QTDE_UN_COMP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_QTDE_UN_COMP"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdQtdeUnComp + "'";
                    contMatriz = contMatriz + 1;
                }
                Funcoes.GravaLogAlteracao("PROD_ID", dadosNovos.ProdID.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ExcluirPorCodigoBarras(string codBarras)
        {

            string sql = "DELETE FROM PRODUTOS_DETALHE WHERE PROD_CODIGO = '" + codBarras + "' AND EST_CODIGO = " + Principal.estAtual + " AND EMP_CODIGO = " + Principal.empAtual;

            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public int AtualizaEstoqueAtual(ProdutoDetalhe prodDetalhe, String operacao, bool transacao)
        {
            string strCmd;
            var dt = new DataTable();
            dt = BancoDados.GetDataTable("SELECT PROD_CAIXA_CARTELADO, PROD_CODIGO_CARTELADO "
                + "FROM PRODUTOS WHERE PROD_CODIGO = '" + prodDetalhe.ProdCodigo + "'", null);
            if(dt.Rows[0]["PROD_CAIXA_CARTELADO"].ToString().Equals("S"))
            {
                strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL " + operacao + " " + prodDetalhe.ProdQtdeUnComp
                    + " WHERE EST_CODIGO = " + prodDetalhe.EstCodigo + " AND EMP_CODIGO = " + prodDetalhe.EmpCodigo + " AND PROD_CODIGO = '" + prodDetalhe.ProdCodigo + "'";
                if (transacao.Equals(true))
                {
                    if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                        return -1;

                }
                else
                {
                    if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
                        return -1;
                }

                strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL " + operacao + " " + prodDetalhe.ProdEstAtual
                  + " WHERE EST_CODIGO = " + prodDetalhe.EstCodigo + " AND EMP_CODIGO = " + prodDetalhe.EmpCodigo + " AND PROD_CODIGO = '" + dt.Rows[0]["PROD_CODIGO_CARTELADO"].ToString() + "'";
            }
            else
            {
                strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL " + operacao + " " + prodDetalhe.ProdEstAtual
                    + " WHERE EST_CODIGO = " + prodDetalhe.EstCodigo + " AND EMP_CODIGO = " + prodDetalhe.EmpCodigo + " AND PROD_CODIGO = '" + prodDetalhe.ProdCodigo + "'";
            }
            
            if (transacao.Equals(true))
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
            else
                return BancoDados.ExecuteNoQuery(strCmd, null);
        }


        public bool AtualizaComissaoMargemEstoqueAtualImpostosEntradaNota(ProdutoDetalhe dadosProduto)
        {
            string[,] logAtualiza = new string[28, 3];
            int contMatriz = 0;
            string strCmd;
            var dt = new DataTable();

            dt = BancoDados.GetDataTable("SELECT COALESCE(A.PROD_COMISSAO,0) AS PROD_COMISSAO, COALESCE(A.PROD_MARGEM,0) AS PROD_MARGEM,COALESCE(A.PROD_ESTATUAL,0) AS PROD_ESTATUAL,"
                + "A.PROD_CFOP, A.PROD_CST, A.PROD_ICMS, COALESCE(A.PROD_VALOR_BC_ICMS,0) AS PROD_VALOR_BC_ICMS, COALESCE(A.PROD_VALOR_ICMS,0) AS PROD_VALOR_ICMS, COALESCE(A.PROD_BC_ICMS_ST,0) AS PROD_BC_ICMS_ST,"
                + "COALESCE(A.PROD_BC_ICMS_RET,0) AS PROD_BC_ICMS_RET, COALESCE(A.PROD_ICMS_RET,0) AS PROD_ICMS_RET, A.PROD_ENQ_IPI, A.PROD_CST_IPI, COALESCE(A.PROD_BASE_IPI,0) AS PROD_BASE_IPI,"
                + "COALESCE(A.PROD_ALIQ_IPI,0) AS PROD_ALIQ_IPI, A.PROD_ORIGEM, A.PROD_CST_COFINS, COALESCE(A.PROD_VALOR_IPI,0) AS PROD_VALOR_IPI, A.PROD_CST_PIS, COALESCE(A.PROD_VALOR_ICMS_ST,0) AS PROD_VALOR_ICMS_ST,"
                + "COALESCE(A.PROD_ALIQ_ICMS,0) AS PROD_ALIQ_ICMS, A.PROD_LISTA, A.PROD_CEST, COALESCE(A.PROD_PRECOMPRA,0) AS PROD_PRECOMPRA, A.PROD_SITUACAO, A.DAT_ALTERACAO, A.OPALTERACAO, "
                + "B.PROD_CAIXA_CARTELADO, B.PROD_CODIGO_CARTELADO "
                + "FROM PRODUTOS_DETALHE A "
                + "INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                + " WHERE A.EST_CODIGO = " + dadosProduto.EstCodigo + " AND A.EMP_CODIGO = " + dadosProduto.EmpCodigo + " AND A.PROD_CODIGO = '" + dadosProduto.ProdCodigo + "'", null);

            strCmd = " UPDATE PRODUTOS_DETALHE SET ";
            if (Funcoes.LeParametro(9, "37", true).Equals("S"))
            {
                if (!dadosProduto.ProdComissao.Equals(Convert.ToDouble(dt.Rows[0]["PROD_COMISSAO"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_COMISSAO";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_COMISSAO"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdComissao + "'";
                    strCmd += "PROD_COMISSAO = " + Funcoes.BValor(dadosProduto.ProdComissao) + ",";
                    contMatriz = contMatriz + 1;
                }
            }
            if (Funcoes.LeParametro(9, "46", true).Equals("S"))
            {
                if (!dadosProduto.ProdMargem.Equals(Convert.ToDouble(dt.Rows[0]["PROD_MARGEM"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_MARGEM";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_MARGEM"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdMargem + "'";
                    strCmd += "PROD_MARGEM = " + Funcoes.BValor(dadosProduto.ProdMargem) + ",";
                    contMatriz = contMatriz + 1;
                }
            }

            if(dt.Rows[0]["PROD_CAIXA_CARTELADO"].ToString().Equals("S"))
            {
                logAtualiza[contMatriz, 0] = "PROD_ESTATUAL";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ESTATUAL"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdEstAtual + "'";
                contMatriz = contMatriz + 1;

                BancoDados.ExecuteNoQueryTrans("UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL + " + dadosProduto.ProdEstAtual + " WHERE PROD_CODIGO = '" + dt.Rows[0]["PROD_CODIGO_CARTELADO"].ToString() 
                    + "' AND EMP_CODIGO = " + dadosProduto.EmpCodigo + " AND EST_CODIGO = " + dadosProduto.EstCodigo, null);
            }
            else
            {
                logAtualiza[contMatriz, 0] = "PROD_ESTATUAL";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ESTATUAL"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdEstAtual + "'";
                strCmd += " PROD_ESTATUAL = PROD_ESTATUAL + " + dadosProduto.ProdEstAtual + ",";
                contMatriz = contMatriz + 1;
            }

            if (Principal.AtualizaImposto.Equals("S"))
            {
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CFOP"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CFOP";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CFOP"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCfop + "'";
                    strCmd += "PROD_CFOP = '" + dadosProduto.ProdCfop + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CST"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CST";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CST"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCst + "'";
                    strCmd += "PROD_CST = '" + dadosProduto.ProdCst + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_ICMS"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ICMS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ICMS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdIcms + "'";
                    strCmd += "PROD_ICMS = " + dadosProduto.ProdIcms + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_VALOR_BC_ICMS"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_VALOR_BC_ICMS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_VALOR_BC_ICMS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdValorBaseIcms + "'";
                    strCmd += "PROD_VALOR_BC_ICMS = " + Funcoes.BValor(dadosProduto.ProdValorBaseIcms) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_VALOR_ICMS"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_VALOR_ICMS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_VALOR_ICMS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdValorIcms + "'";
                    strCmd += "PROD_VALOR_ICMS = " + Funcoes.BValor(dadosProduto.ProdValorIcms) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_BC_ICMS_ST"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_BC_ICMS_ST";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_BC_ICMS_ST"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdBaseIcmsST + "'";
                    strCmd += "PROD_BC_ICMS_ST = " + Funcoes.BValor(dadosProduto.ProdBaseIcmsST) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_BC_ICMS_RET"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_BC_ICMS_RET";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_BC_ICMS_RET"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdBaseIcmsRet + "'";
                    strCmd += "PROD_BC_ICMS_RET = " + Funcoes.BValor(dadosProduto.ProdBaseIcmsRet) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_ICMS_RET"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ICMS_RET";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ICMS_RET"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdIcmsRet + "'";
                    strCmd += "PROD_ICMS_RET = " + Funcoes.BValor(dadosProduto.ProdIcmsRet) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_ENQ_IPI"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ENQ_IPI";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ENQ_IPI"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdEnqIpi + "'";
                    strCmd += "PROD_ENQ_IPI = '" + dadosProduto.ProdEnqIpi + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CST_IPI"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CST_IPI";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CST_IPI"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCstIpi + "'";
                    strCmd += "PROD_CST_IPI = '" + dadosProduto.ProdCstIpi + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_BASE_IPI"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_BASE_IPI";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_BASE_IPI"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdBaseIpi + "'";
                    strCmd += "PROD_BASE_IPI = " + Funcoes.BValor(dadosProduto.ProdBaseIpi) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_ALIQ_IPI"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ALIQ_IPI";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ALIQ_IPI"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdAliqIpi + "'";
                    strCmd += "PROD_ALIQ_IPI = " + Funcoes.BValor(dadosProduto.ProdAliqIpi) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_ORIGEM"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ORIGEM";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ORIGEM"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdOrigem + "'";
                    strCmd += "PROD_ORIGEM = " + dadosProduto.ProdOrigem + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CST_COFINS"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CST_COFINS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CST_COFINS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCstCofins + "'";
                    strCmd += "PROD_CST_COFINS = '" + dadosProduto.ProdCstCofins + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_VALOR_IPI"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_VALOR_IPI";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_VALOR_IPI"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdValorIpi + "'";
                    strCmd += "PROD_VALOR_IPI = " + Funcoes.BValor(dadosProduto.ProdValorIpi) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CST_PIS"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CST_PIS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CST_PIS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCstIpi + "'";
                    strCmd += "PROD_CST_PIS = '" + dadosProduto.ProdCstIpi + "',";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_VALOR_ICMS_ST"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_VALOR_ICMS_ST";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_VALOR_ICMS_ST"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdValorIcmsSt + "'";
                    strCmd += "PROD_VALOR_ICMS_ST = " + Funcoes.BValor(dadosProduto.ProdValorIcmsSt) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["PROD_ALIQ_ICMS"])))
                {
                    logAtualiza[contMatriz, 0] = "PROD_ALIQ_ICMS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_ALIQ_ICMS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdAliqIcms + "'";
                    strCmd += "PROD_ALIQ_ICMS = " + Funcoes.BValor(dadosProduto.ProdAliqIcms) + ",";
                    contMatriz = contMatriz + 1;
                }
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_CEST"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_CEST";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_CEST"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdCest + "'";
                    strCmd += "PROD_CEST = '" + dadosProduto.ProdCest + "',";
                    contMatriz = contMatriz + 1;
                }
            }
            if (!dadosProduto.ProdPreCompra.Equals(Convert.ToString(dt.Rows[0]["PROD_PRECOMPRA"])))
            {
                logAtualiza[contMatriz, 0] = "PROD_PRECOMPRA";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_PRECOMPRA"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdPreCompra + "'";
                strCmd += "PROD_PRECOMPRA = " + Funcoes.BValor(dadosProduto.ProdPreCompra) + ",";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                logAtualiza[contMatriz, 0] = "PROD_SITUACAO";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_SITUACAO"] + "'";
                logAtualiza[contMatriz, 2] = "'A'";
                strCmd += "PROD_SITUACAO = 'A',";
                contMatriz = contMatriz + 1;

                logAtualiza[contMatriz, 0] = "DAT_ALTERACAO";
                logAtualiza[contMatriz, 1] = dt.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["DAT_ALTERACAO"] + "'";
                logAtualiza[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                contMatriz = contMatriz + 1;

                logAtualiza[contMatriz, 0] = "OPALTERACAO";
                logAtualiza[contMatriz, 1] = dt.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["OPALTERACAO"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.OpAlteracao + "'";
                strCmd += " OPALTERACAO = '" + dadosProduto.OpAlteracao + "' ";
                contMatriz = contMatriz + 1;

                strCmd += " WHERE EST_CODIGO = " + dadosProduto.EstCodigo + " AND EMP_CODIGO = " + dadosProduto.EmpCodigo + "  AND PROD_CODIGO = '" + dadosProduto.ProdCodigo + "'";
                if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return false;
                }
                else
                    Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosProduto.ProdCodigo, dadosProduto.OpAlteracao, "PRODUTOS_DETALHE", logAtualiza, contMatriz, dadosProduto.EstCodigo, dadosProduto.EmpCodigo, true);
            }

            return true;
        }

        public bool AtualizaCusto(ProdutoDetalhe dadosProdutoDetalhe)
        {
            try
            {
                var dtRetorno = new DataTable();
                var dtMovimento = new DataTable();
                int contMatriz = 0;
                string[,] logAtualiza = new string[4, 3];
                int estoque;
                double custoAtual;
                double custoNovo;
                double ultCusto;

                string strSql = "SELECT COALESCE(A.PROD_CUSINI,0) AS PROD_CUSINI, COALESCE(A.PROD_ESTINI,0) AS PROD_ESTINI, COALESCE(A.PROD_CUSME,0) AS PROD_CUSME, "
                    + " COALESCE(A.PROD_ULTCUSME,0) AS PROD_ULTCUSME, A.PROD_DTESTINI, A.PROD_ESTATUAL, COALESCE(A.DTCADASTRO,A.DAT_ALTERACAO) AS DAT_ALTERACAO, A.OPALTERACAO, A.PROD_CODIGO  "
                    + " FROM PRODUTOS_DETALHE A "
                    + " WHERE A.EST_CODIGO = " + dadosProdutoDetalhe.EstCodigo + " AND A.EMP_CODIGO = " + dadosProdutoDetalhe.EmpCodigo + " AND A.PROD_CODIGO = '" + dadosProdutoDetalhe.ProdCodigo + "'";
                dtRetorno = BancoDados.GetDataTable(strSql, null);
                if (dtRetorno.Rows.Count != 0)
                {
                    ultCusto = Math.Round(dadosProdutoDetalhe.ProdCusIni, 2);

                    custoAtual = Math.Round(Convert.ToInt32(dtRetorno.Rows[0]["PROD_ESTATUAL"]) * Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]), 2);
                    custoNovo = Math.Round(dadosProdutoDetalhe.ProdEstAtual * dadosProdutoDetalhe.ProdCusIni, 2);
                    estoque = Convert.ToInt32(dtRetorno.Rows[0]["PROD_ESTATUAL"]) + dadosProdutoDetalhe.ProdEstAtual;
                    estoque = estoque == 0 ? 1 : estoque;

                    strSql = "UPDATE PRODUTOS_DETALHE SET PROD_CUSME = " + Funcoes.BValor(Math.Round((custoAtual + custoNovo) / estoque, 2)) + ", PROD_ULTCUSME = " + Funcoes.BValor(Math.Round(ultCusto, 2))
                        + ", DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = '" + dadosProdutoDetalhe.OpAlteracao
                        + "' WHERE EST_CODIGO = " + dadosProdutoDetalhe.EstCodigo + " AND EMP_CODIGO = " + dadosProdutoDetalhe.EmpCodigo + " AND PROD_CODIGO = '" + dadosProdutoDetalhe.ProdCodigo + "'";
                    if (BancoDados.ExecuteNoQueryTrans(strSql, null).Equals(-1))
                    {
                        return false;
                    }
                    else
                    {
                        logAtualiza[contMatriz, 0] = "PROD_CUSME";
                        logAtualiza[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PROD_CUSME"] + "'";
                        logAtualiza[contMatriz, 2] = "'" + custoAtual + "'";
                        contMatriz = contMatriz + 1;

                        logAtualiza[contMatriz, 0] = "PROD_ULTCUSME";
                        logAtualiza[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PROD_ULTCUSME"] + "'";
                        logAtualiza[contMatriz, 2] = "'" + ultCusto + "'";
                        contMatriz = contMatriz + 1;

                        logAtualiza[contMatriz, 0] = "DAT_ALTERACAO";
                        logAtualiza[contMatriz, 1] = dtRetorno.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dtRetorno.Rows[0]["DAT_ALTERACAO"] + "'";
                        logAtualiza[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                        contMatriz = contMatriz + 1;

                        logAtualiza[contMatriz, 0] = "OPALTERACAO";
                        logAtualiza[contMatriz, 1] = dtRetorno.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dtRetorno.Rows[0]["OPALTERACAO"] + "'";
                        logAtualiza[contMatriz, 2] = "'" + dadosProdutoDetalhe.OpAlteracao + "'";
                        contMatriz = contMatriz + 1;

                        if (Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosProdutoDetalhe.ProdCodigo, dadosProdutoDetalhe.OpAlteracao, "PRODUTOS", logAtualiza, contMatriz, dadosProdutoDetalhe.EstCodigo, dadosProdutoDetalhe.EmpCodigo, true).Equals(false))
                            return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InsereProdutosDetalheNovos(ProdutoDetalhe dados)
        {
            string strCmd = "INSERT INTO PRODUTOS_DETALHE (EMP_CODIGO, EST_CODIGO,PROD_CODIGO, DEP_CODIGO, PROD_DTESTINI, PROD_SITUACAO, "
                + "PROD_PRECOMPRA, PROD_ECF, DTCADASTRO, OPCADASTRO, PROD_ID, PROD_CUSINI) VALUES("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ",'"
                + dados.ProdCodigo + "',"
                + dados.DepCodigo + ","
                + Funcoes.BData(dados.ProdDTEstIni) + ",'"
                + dados.ProdLiberado + "',"
                + Funcoes.BValor(dados.ProdPreCompra) + ",'"
                + dados.ProdEcf + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "'," + dados.ProdID + "," + Funcoes.BValor(dados.ProdPreCompra) + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool IndentificaProdutoCadastradoProdutoDetalhe(ProdutoDetalhe dadosProduto)
        {
            string strSql = "SELECT A.PROD_CODIGO FROM PRODUTOS_DETALHE A, PRODUTOS B, PRECOS C"
                        + " WHERE A.EST_CODIGO = " + dadosProduto.EstCodigo
                        + " AND A.EMP_CODIGO = " + dadosProduto.EmpCodigo
                        + " AND A.PROD_CODIGO = '" + dadosProduto.ProdCodigo + "'"
                        + " AND A.PROD_CODIGO = B.PROD_CODIGO"
                        + " AND A.PROD_CODIGO = C.PROD_CODIGO"
                        + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                        + " AND A.EST_CODIGO = C.EST_CODIGO";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return false;
            else
                return true;
        }

        public DataTable BuscaPrecoCompraOuUltimoCusto(ProdutoDetalhe dadosProduto)
        {
            string strSql = "SELECT COALESCE(PROD_PRECOMPRA, 0.00) AS PROD_PRECOMPRA, COALESCE(PROD_ULTCUSME, 0) AS PROD_ULTCUSME FROM PRODUTOS_DETALHE WHERE EMP_CODIGO = " + dadosProduto.EmpCodigo
                + " AND EST_CODIGO = " + dadosProduto.EstCodigo + " AND PROD_CODIGO = '" + dadosProduto.ProdCodigo + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DadosProdutoDetalhe(string prodCodigo, int prodID)
        {
            string strSql = "SELECT * FROM PRODUTOS_DETALHE" +
                 " WHERE PROD_CODIGO = '" + prodCodigo + "'" +
                 " AND PROD_ID = " + prodID +
                 " AND EMP_CODIGO = " + Principal.empAtual +
                 " AND EST_CODIGO = " + Principal.estAtual;

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaEstoqueAtualPorCodigo(string prodCodigo, int prodID, int empCodigo, int estCodigo, int qtde, String operacao)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL " + operacao + " " + qtde + ", DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                     + ", OPALTERACAO = '" + Principal.usuario + "' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND PROD_CODIGO = '" + prodCodigo + "' AND PROD_ID = " + prodID;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaMargem(int id, double margemAntigo, double margemNova)
        {
            string sql = " UPDATE PRODUTOS_DETALHE SET "
                       + " PROD_MARGEM = '" + margemNova + "'"
                       + " WHERE PROD_ID =" + id
                       + " AND EMP_CODIGO = " + Principal.empAtual
                       + " AND EST_CODIGO = " + Principal.estAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[1, 3];
                dados[0, 0] = "PROD_MARGEM";
                dados[0, 1] = "'" + margemAntigo + "'";
                dados[0, 2] = "'" + margemNova + "'";

                Funcoes.GravaLogAlteracao("PROD_ID", id.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados, 1, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaQuantidade(int id, int quantidadeAntiga, int quantidadeNova)
        {
            string sql = " UPDATE PRODUTOS_DETALHE SET "
                       + " PROD_ESTATUAL = " + quantidadeNova
                       + " WHERE PROD_ID =" + id
                       + " AND EST_CODIGO = " + Principal.estAtual
                       + " AND EMP_CODIGO = " + Principal.empAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {

                string[,] dados = new string[1, 3];
                dados[0, 0] = "PROD_ESTATUAL";
                dados[0, 1] = "'" + quantidadeAntiga + "'";
                dados[0, 2] = "'" + quantidadeNova + "'";

                Funcoes.GravaLogAlteracao("PROD_ID", id.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados, 1, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDepartamento(int id, int depcodigo, string depAnterior, string depNovo)
        {
            string sql = " UPDATE PRODUTOS_DETALHE SET "
                       + " DEP_CODIGO = " + depcodigo
                       + " WHERE PROD_ID =" + id
                       + " AND EST_CODIGO = " + Principal.estAtual
                       + " AND EMP_CODIGO = " + Principal.empAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[1, 3];
                dados[0, 0] = "DEP_CODIGO";
                dados[0, 1] = "'" + depAnterior + "'";
                dados[0, 2] = "'" + depNovo + "'";

                Funcoes.GravaLogAlteracao("d", id.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados, 1, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaProdListaAbcFarma(ProdutoDetalhe dados)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_LISTA = '" + dados.ProdLista + "',";

            if (!String.IsNullOrEmpty(dados.ProdCest))
            {
                strCmd += " PROD_CEST = '" + dados.ProdCest + "',";
            }

            if (!String.IsNullOrEmpty(dados.ProdDCB))
            {
                strCmd += " PROD_DCB = '" + dados.ProdDCB + "',";
            }

            strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(dados.DtAlteracao)
                 + ", OPALTERACAO = '" + dados.OpAlteracao + "' WHERE EST_CODIGO = " + dados.EstCodigo + " AND EMP_CODIGO = " + dados.EmpCodigo
                 + " AND PROD_CODIGO = '" + dados.ProdCodigo + "'";
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool InsereProdutosDetalheNovosAbcFarna(ProdutoDetalhe dados)
        {
            string strCmd = "INSERT INTO PRODUTOS_DETALHE (EMP_CODIGO, EST_CODIGO,PROD_CODIGO, DEP_CODIGO, PROD_DTESTINI, PROD_SITUACAO, "
                + " PROD_ECF, DTCADASTRO, OPCADASTRO, PROD_ID, PROD_LISTA, PROD_CEST, PROD_DCB) VALUES("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ",'"
                + dados.ProdCodigo + "',"
                + dados.DepCodigo + ","
                + Funcoes.BData(dados.ProdDTEstIni) + ",'"
                + dados.ProdLiberado + "','"
                + dados.ProdEcf + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "',"
                + dados.ProdID + ",'"
                + dados.ProdLista + "',";
            if (!String.IsNullOrEmpty(dados.ProdCest))
            {
                strCmd += "'" + dados.ProdCest + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.ProdDCB))
            {
                strCmd += "'" + dados.ProdDCB + "')";
            }
            else
                strCmd += "null)";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaDepClasSub(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT A.PROD_ID, COALESCE(A.DEP_CODIGO,0) AS DEP_CODIGO, COALESCE(A.CLAS_CODIGO, 0) AS CLAS_CODIGO, COALESCE(A.SUB_CODIGO,0) AS SUB_CODIGO, B.PRE_VALOR FROM PRODUTOS_DETALHE A "
                + " INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                + " WHERE A.EMP_CODIGO = " + empCodigo + " AND A.EST_CODIGO = " + estCodigo + " AND A.PROD_CODIGO = '" + prodCodigo + "' AND A.EMP_CODIGO = B.EMP_CODIGO AND B.EST_CODIGO = B.EST_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public int AtualizaUnidadeCompraFP()
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_QTDE_UN_COMP_FP = PROD_QTDE_UN_COMP";
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable BuscaProdutosVendosPorPeriodo(DateTime dtInicial, DateTime dtFinal, bool quebraDia, string colCodigo, string filtro)
        {
            string sql = " SELECT SUM(VEN.VENDA_ITEM_TOTAL) AS TOTAL, LTRIM(DEP.DEP_DESCR) AS DESCRICAO , A.DEP_CODIGO ";
            if (quebraDia.Equals(true))
            {
                sql += " ,VENDAS.VENDA_EMISSAO AS DATA";
            }
            sql += " FROM VENDAS_ITENS VEN "
                + " INNER JOIN PRODUTOS_DETALHE A ON(A.PROD_CODIGO = VEN.PROD_CODIGO) "
                + " INNER JOIN DEPARTAMENTOS DEP ON(A.DEP_CODIGO = DEP.DEP_CODIGO) "
                + " JOIN VENDAS VENDAS ON (VENDAS.VENDA_ID = VEN.VENDA_ID)"
                + " WHERE VEN.EMP_CODIGO = " + Principal.empAtual + " AND VEN.EST_CODIGO = " + Principal.estAtual
                + " AND A.EMP_CODIGO = VEN.EMP_CODIGO "
                + " AND A.EST_CODIGO = VEN.EST_CODIGO "
                + " AND DEP.EMP_CODIGO = VEN.EMP_CODIGO ";
            if (!colCodigo.Equals("0"))
            {
                sql += " AND VEN.COL_CODIGO = " + colCodigo;
            }
            if (!String.IsNullOrEmpty(filtro))
            {
                sql += " AND " + filtro;
            }
            
            sql +=  " AND VENDAS.VENDA_STATUS = 'F'"
                + " AND VENDAS.VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                + " GROUP BY A.DEP_CODIGO, DEP.DEP_DESCR ";
            if (quebraDia.Equals(true))
            {
                sql += ", VENDAS.VENDA_EMISSAO  ";
            }
            sql += " ORDER BY LTRIM(DEP.DEP_DESCR) ";

            return BancoDados.GetDataTable(sql, null);
        }


        public DataTable BuscaProdutosVendosPorPeriodoAnalitico(DateTime dtInicial, DateTime dtFinal, bool quebraDia, bool ordemAlfabetica, string tipoQuebra, string filtroProduto, string colCodigo)
        {
            string sql = " SELECT VEN.PROD_CODIGO, SUM(VEN.VENDA_ITEM_QTDE) AS QTDE , SUM(VEN.VENDA_ITEM_TOTAL) AS TOTAL , PROD.PROD_DESCR, A.PROD_ESTATUAL ";
            if (quebraDia.Equals(true))
            {
                sql += " ,VENDAS.VENDA_EMISSAO AS DATA ";
            }
            if (!tipoQuebra.Equals("Nenhuma"))
            {
                if (tipoQuebra.Equals("Departamento"))
                {
                    sql += " ,DEP.DEP_DESCR";
                }
                else if (tipoQuebra.Equals("Classe"))
                {
                    sql += " ,CLA.CLAS_DESCR";
                }
                else
                {
                    sql += " ,SUBCLA.SUB_DESCR ";
                }
            }
            sql += " FROM VENDAS_ITENS VEN "
                       + " INNER JOIN PRODUTOS_DETALHE A ON(VEN.PROD_CODIGO = A.PROD_CODIGO) "
                       + " INNER JOIN PRODUTOS PROD ON(PROD.PROD_CODIGO = A.PROD_CODIGO) "
                       + " INNER JOIN VENDAS VENDAS ON (VENDAS.VENDA_ID = VEN.VENDA_ID)"
                       + " LEFT JOIN DEPARTAMENTOS DEP ON (DEP.DEP_CODIGO = A.DEP_CODIGO AND DEP.EMP_CODIGO = " + Principal.empAtual + ") "
                       + " LEFT JOIN CLASSES CLA ON(CLA.CLAS_CODIGO = A.CLAS_CODIGO  AND CLA.EMP_CODIGO = " + Principal.empAtual + ")"
                       + " LEFT JOIN SUBCLASSES SUBCLA ON (SUBCLA.SUB_CODIGO = A.SUB_CODIGO  AND SUBCLA.EMP_CODIGO = " + Principal.empAtual + ")"
                       + " WHERE VEN.EMP_CODIGO = " + Principal.empAtual + " AND VEN.EST_CODIGO = " + Principal.estAtual
                       + " AND A.EMP_CODIGO = " + Principal.empAtual + " AND A.EST_CODIGO = " + Principal.estAtual
                       + " AND VENDAS.EMP_CODIGO =" + Principal.empAtual+ " AND VENDAS.EST_CODIGO = " + Principal.estAtual
                       + " AND VENDAS.VENDA_STATUS = 'F'"
                       + " AND VENDAS.VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            if(!colCodigo.Equals("0"))
            {
                if (Funcoes.LeParametro(6, "383", false).Equals("S"))
                {
                    sql += " AND VEN.COL_CODIGO = " + colCodigo;
                }
                else
                {
                    sql += " AND VENDAS.VENDA_COL_CODIGO = " + colCodigo;
                }
            }
            if (!String.IsNullOrEmpty(filtroProduto))
            {
                sql += " AND " + filtroProduto;
            }

            sql += " GROUP BY VEN.PROD_CODIGO,PROD.PROD_DESCR, A.PROD_ESTATUAL ";

            if (!tipoQuebra.Equals("Nenhuma"))
            {
                if (tipoQuebra.Equals("Departamento"))
                {
                    sql += " ,DEP.DEP_DESCR";
                }
                else if (tipoQuebra.Equals("Classe"))
                {
                    sql += " ,CLA.CLAS_DESCR";
                }
                else
                {
                    sql += " ,SUBCLA.SUB_DESCR ";
                }
            }
            if (quebraDia.Equals(true))
            {
                sql += ", VENDAS.VENDA_EMISSAO  ";
            }
            if (ordemAlfabetica.Equals(true))
            {
                sql += " ORDER BY PROD.PROD_DESCR  ";
            }
            else
            {
                sql += " ORDER BY PROD.PROD_DESCR  ";
            }
            return BancoDados.GetDataTable(sql, null);
        }


        public bool AtualizaEstoquePorDepartamentoOuNao(int empCodigo, int estCodigo, bool todos, int codDepartamento)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = 0, DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", "
                    + " OPALTERACAO = '" + Principal.usuario + "' WHERE EMP_CODIGO = " + empCodigo
                    + " AND EST_CODIGO = " + estCodigo;
            if(!todos)
            {
                strCmd += " AND DEP_CODIGO = " + codDepartamento;
            }
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaUltimoCusto(int empCodigo, int estCodigo, double ultimoCusto, string prodCodigo)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ULTCUSME = " + Funcoes.BValor(ultimoCusto) + ", DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", "
                    + " OPALTERACAO = '" + Principal.usuario + "' WHERE EMP_CODIGO = " + empCodigo
                    + " AND EST_CODIGO = " + estCodigo + " AND PROD_CODIGO = '" + prodCodigo + "'";
         
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaComissaoPorDepartamentoOuNao(int empCodigo, int estCodigo, bool todos, int codDepartamento, double comissao)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_COMISSAO = " + Funcoes.BValor(comissao) + ", DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", "
                    + " OPALTERACAO = '" + Principal.usuario + "' WHERE EMP_CODIGO = " + empCodigo
                    + " AND EST_CODIGO = " + estCodigo;
            if (!todos)
            {
                strCmd += " AND DEP_CODIGO = " + codDepartamento;
            }
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaComissao(int id, double comissaoAntiga, double comissaoNova)
        {
            string sql = " UPDATE PRODUTOS_DETALHE SET "
                       + " PROD_COMISSAO = '" + comissaoNova + "'"
                       + " WHERE PROD_ID =" + id
                       + " AND EMP_CODIGO = " + Principal.empAtual
                       + " AND EST_CODIGO = " + Principal.estAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                string[,] dados = new string[1, 3];
                dados[0, 0] = "PROD_COMISSAO";
                dados[0, 1] = "'" + comissaoAntiga + "'";
                dados[0, 2] = "'" + comissaoNova + "'";

                Funcoes.GravaLogAlteracao("PROD_ID", id.ToString(), Principal.usuario, "PRODUTOS_DETALHE", dados, 1, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }
    }
}
