﻿using System;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class Entrada
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public int EntId { get; set; }
        public string EntDtLanc { get; set; }
        public int TipCodigo { get; set; }
        public string EntDtEmis { get; set; }
        public string CfDocto { get; set; }
        public string EntDocto { get; set; }
        public string EntSerie { get; set; }
        public int NoCodigo { get;  set; }
        public int NoComp { get; set; }
        public string PagDocto { get; set; }
        public double EntTotal { get; set; }
        public double EntBaseIcms { get; set; }
        public double EntValorIcms { get; set; }
        public double EntBaseIcmsST { get; set; }
        public double EntValorIcmsSt { get; set; }
        public double EntValorImpImportacao { get; set; }
        public double EntValorIcmsUFRemet { get; set; }
        public double EntValorFCP { get; set; }
        public double EntValorPIS { get; set; }
        public double EntValorProdutos { get; set; }
        public double EntValorFrete { get; set; }
        public double EntValorSeguro { get; set; }
        public double EntValorDesconto { get; set; }
        public double EntOutasDespesas { get; set; }
        public double EntValorIPI { get; set; }
        public double EntValorICMSUFDest { get; set; }
        public double EntTotalTributos { get; set; }
        public double EntValorCofins { get; set; }
        public int ColCodigo { get; set; }
        public string CompNumero { get; set; }
        public string EntStatus { get; set; }
        public string EntObs { get; set; }
        public string EntViaXML { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string ChaveAcesso { get; set; }
        public string DataHoraEmissao { get; set; }

        public Entrada() { }

        public Entrada(int estCodigo, int empCodigo, int entId, string entDtLanc, int tipCodigo, string entDtEmis, string cfDocto, string entDocto,
            string entSerie, int noCodigo, int noComp, string pagDocto, double entTotal, double entBaseIcms, double entImpIcms, double entBaseIcmsST, double entValorIcmsSt,
            double entValorImpImportacao, double entValorIcmsUFRemet, double entValorFCP, double entValorPIS, double entValorProdutos, double entValorFrete, double entValorSeguro, double entValorDesconto,
            double entOutasDespesas, double entValorIPI, double entValorICMSUFDest, double entTotalTributos, double entValorCofins, int colCodigo, string compNumero, string entStatus,
            string entObs, string entViaXml, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro, string chaveAcesso, string dataHoraEmissao)
        {
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.EntId = entId;
            this.EntDtLanc = entDtLanc;
            this.TipCodigo = tipCodigo;
            this.EntDtEmis = entDtEmis;
            this.CfDocto = cfDocto;
            this.EntDocto = entDocto;
            this.EntSerie = entSerie;
            this.NoCodigo = noCodigo;
            this.NoComp = noComp;
            this.PagDocto = pagDocto;
            this.EntTotal = entTotal;
            this.EntBaseIcms = entBaseIcms;
            this.EntValorIcms = entImpIcms;
            this.EntBaseIcmsST = entBaseIcmsST;
            this.EntValorIcmsSt = entValorIcmsSt;
            this.EntValorImpImportacao = entValorImpImportacao;
            this.EntValorIcmsUFRemet = entValorIcmsUFRemet;
            this.EntValorFCP = entValorFCP;
            this.EntValorPIS = entValorPIS;
            this.EntValorProdutos = entValorProdutos;
            this.EntValorFrete = entValorFrete;
            this.EntValorSeguro = entValorSeguro;
            this.EntValorDesconto = entValorDesconto;
            this.EntOutasDespesas = entOutasDespesas;
            this.EntValorIPI = entValorIPI;
            this.EntValorICMSUFDest = entValorICMSUFDest;
            this.EntTotalTributos = entTotalTributos;
            this.EntValorCofins = entValorCofins;
            this.ColCodigo = colCodigo;
            this.CompNumero = compNumero;
            this.EntStatus = entStatus;
            this.EntObs = entObs;
            this.EntViaXML = entViaXml;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.ChaveAcesso = chaveAcesso;
            this.DataHoraEmissao = dataHoraEmissao;
        }

        public bool InsereRegistrosEntrada(Entrada dadosEntrada)
        {
            string strCmd = "INSERT INTO ENTRADA(EMP_CODIGO, EST_CODIGO, ENT_ID, ENT_DTLANC, TIP_CODIGO, ENT_DTEMIS, CF_DOCTO, ENT_DOCTO,"
                + " ENT_SERIE, NO_CODIGO, NO_COMP, PAG_DOCTO, ENT_TOTAL, ENT_BASEICMS, ENT_IMPICMS, ENT_BASESUBST, ENT_SUBST, ENT_ISENTOIPI, ENT_OUTIPI,"
                + " ENT_BASEISS, ENT_ISENTOICMS, ENT_DIVERSAS, ENT_FRETE, ENT_IMPFRETE, ENT_DESCONTO, ENT_OUTRAS, ENT_IMPIPI, ENT_BASEIPI, ENT_IRRF, ENT_IMPISS,"
                + " PAG_COMPRADOR, COMP_NUMERO, LOGIN_ID, ENT_STATUS, ENT_OBS, ENT_VIA_XML, DTCADASTRO, OPCADASTRO,CHAVE_ACESSO, DATA_HORA_EMISSAO) VALUES ("
                + dadosEntrada.EmpCodigo + ","
                + dadosEntrada.EstCodigo + ","
                + dadosEntrada.EntId + ","
                + Funcoes.BData(Convert.ToDateTime(dadosEntrada.EntDtLanc)) + ","
                + dadosEntrada.TipCodigo + ","
                + Funcoes.BData(Convert.ToDateTime(dadosEntrada.EntDtEmis)) + ",'"
                + dadosEntrada.CfDocto + "','"
                + dadosEntrada.EntDocto + "','"
                + dadosEntrada.EntSerie + "','"
                + dadosEntrada.NoCodigo + "','"
                + dadosEntrada.NoComp + "','"
                + dadosEntrada.PagDocto + "',"
                + Funcoes.BValor(dadosEntrada.EntTotal) + ","
                + Funcoes.BValor(dadosEntrada.EntBaseIcms) + ","
                + Funcoes.BValor(dadosEntrada.EntValorIcms) + ","
                + Funcoes.BValor(dadosEntrada.EntBaseIcmsST) + ","
                + Funcoes.BValor(dadosEntrada.EntValorIcmsSt) + ","
                + Funcoes.BValor(dadosEntrada.EntValorImpImportacao) + ","
                + Funcoes.BValor(dadosEntrada.EntValorIcmsUFRemet) + ","
                + Funcoes.BValor(dadosEntrada.EntValorFCP) + ","
                + Funcoes.BValor(dadosEntrada.EntValorPIS) + ","
                + Funcoes.BValor(dadosEntrada.EntValorProdutos) + ","
                + Funcoes.BValor(dadosEntrada.EntValorFrete) + "," 
                + Funcoes.BValor(dadosEntrada.EntValorDesconto) + ","
                + Funcoes.BValor(dadosEntrada.EntValorSeguro) + ","
                + Funcoes.BValor(dadosEntrada.EntOutasDespesas) + ","
                + Funcoes.BValor(dadosEntrada.EntValorIPI) + ","
                + Funcoes.BValor(dadosEntrada.EntValorICMSUFDest) + ","
                + Funcoes.BValor(dadosEntrada.EntTotalTributos) + ","
                + Funcoes.BValor(dadosEntrada.EntValorCofins) + ","
                + dadosEntrada.ColCodigo + ",'"
                + dadosEntrada.CompNumero + "','"
                + dadosEntrada.OpCadastro + "','"
                + dadosEntrada.EntStatus + "','"
                + dadosEntrada.EntObs + "','"
                + dadosEntrada.EntViaXML + "',"
                + Funcoes.BDataHora(dadosEntrada.DtCadastro) + ",'"
                + dadosEntrada.OpCadastro + "','"
                + dadosEntrada.ChaveAcesso + "','"
                + dadosEntrada.DataHoraEmissao + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscarEntradaDeNotas(int estCodigo, int empCodigo,string dtLancto, int tipDocto, string dtEmissao, string entDocto, string entSerie,
            string pagDocto, double entTotal, int noCodigo, int colCodigo, string cfDocto, out string strOrdem, bool buscaSemFiltro)
        {
            string strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.ENT_ID, A.ENT_DTLANC, B.TIP_DESC_ABREV AS TIP_CODIGO, A.ENT_DTEMIS, A.ENT_DOCTO, A.ENT_SERIE, A.PAG_DOCTO, A.ENT_TOTAL, "
                    + "C.NO_CODIGO ||' /'||  C.NO_COMP ||' - '|| C.NO_DESCR AS NO_CODIGO, D.COL_NOME AS PAG_COMPRADOR, A.CF_DOCTO, A.ENT_OBS, A.ENT_BASEICMS, A.ENT_IMPICMS, A.ENT_BASESUBST, "
                    + "A.ENT_SUBST, A.ENT_ISENTOIPI, A.ENT_OUTIPI, A.ENT_BASEISS, A.ENT_ISENTOICMS, A.ENT_DIVERSAS, A.ENT_FRETE, A.ENT_IMPFRETE, A.ENT_DESCONTO, A.ENT_OUTRAS, "
                    + "A.ENT_IMPIPI, A.ENT_BASEIPI, A.ENT_IRRF, A.ENT_IMPISS, A.DAT_ALTERACAO, A.OPALTERACAO,"
                    + "A.DTCADASTRO, A.OPCADASTRO, A.ENT_VIA_XML, A.CHAVE_ACESSO, A.DATA_HORA_EMISSAO FROM ENTRADA A "
                    + "LEFT JOIN TIPO_DOCTO B ON (A.TIP_CODIGO = B.TIP_CODIGO) "
                    + "LEFT JOIN NAT_OPERACAO C ON (A.NO_CODIGO = C.NO_CODIGO AND A.EMP_CODIGO = C.EMP_CODIGO) "
                    + "LEFT JOIN COLABORADORES D ON (D.EMP_CODIGO = A.EMP_CODIGO AND D.COL_CODIGO = A.PAG_COMPRADOR) "
                    + "WHERE A.EST_CODIGO = " + estCodigo + " AND A.EMP_CODIGO = " + empCodigo
                    + " AND A.ENT_ID IS NOT NULL";
            if (buscaSemFiltro.Equals(true))
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.ENT_ID ";
            }
            else
            {
                if (Funcoes.RemoveCaracter(dtLancto) != "")
                {
                    strSql += " AND A.ENT_DTLANC = " + Funcoes.BData(Convert.ToDateTime(dtLancto));
                }
                if (tipDocto != 0)
                {
                    strSql += " AND A.TIP_CODIGO = " + tipDocto;
                }
                if (Funcoes.RemoveCaracter(dtEmissao) != "")
                {
                    strSql += " AND A.ENT_DTEMIS = " + Funcoes.BData(Convert.ToDateTime(dtEmissao));
                }
                if (entDocto != "")
                {
                    strSql += " AND A.ENT_DOCTO = '" + entDocto + "'";
                }
                if (entSerie != "")
                {
                    strSql += " AND A.ENT_SERIE = '" + entSerie + "'";
                }
                if (pagDocto != "")
                {
                    strSql += " AND A.PAG_DOCTO = '" + pagDocto + "'";
                }
                if (entTotal != 0)
                {
                    strSql += " AND A.ENT_TOTAL = " + Funcoes.BValor(entTotal);
                }
                if (noCodigo != 0)
                {
                    strSql += " AND A.NO_CODIGO = " + noCodigo;
                }
                if (colCodigo != 0)
                {
                    strSql += " AND A.PAG_COMPRADOR = " + colCodigo;
                }
                if (Funcoes.RemoveCaracter(cfDocto) != "")
                {
                    strSql += " AND A.CF_DOCTO = '" + cfDocto + "'";
                }

                strOrdem = strSql;
                strSql += " ORDER BY A.ENT_ID";
            }
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable IdentificaEntradaCadastradaPorId(Entrada dadosBusca)
        {
            string strSql = "SELECT * FROM ENTRADA WHERE EST_CODIGO = " + dadosBusca.EstCodigo + " AND EMP_CODIGO = " + dadosBusca.EmpCodigo + " AND ENT_ID = " + dadosBusca.EntId;
            return BancoDados.GetDataTable(strSql, null);
        }


        public string IdentificaEntradaCadastradaPorFornecedorDocto(Entrada dadosBusca)
        {
            string strSql = "SELECT ENT_DOCTO FROM ENTRADA WHERE EST_CODIGO = " + dadosBusca.EstCodigo + " AND EMP_CODIGO = " + dadosBusca.EmpCodigo + " AND ENT_DOCTO = '" + dadosBusca.EntDocto 
                + "' AND ENT_SERIE = '" + dadosBusca.EntSerie + "' AND CF_DOCTO = '" + dadosBusca.CfDocto + "'";
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }


        public bool AtualizaDadosEntradaNotas(Entrada dadosNovos, DataTable dadosCadastrados)
        {
            string[,] dados = new string[32, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE ENTRADA SET ";
            if (!dadosNovos.EntDtLanc.Equals(dadosCadastrados.Rows[0]["ENT_DTLANC"].ToString()))
            {
                dados[contMatriz, 0] = "ENT_DTLANC";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_DTLANC"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntDtLanc + "'";
                strCmd += "ENT_DTLANC = " + Funcoes.BData(Convert.ToDateTime(dadosNovos.EntDtLanc)) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.TipCodigo.Equals(Convert.ToInt32(dadosCadastrados.Rows[0]["TIP_CODIGO"])))
            {
                dados[contMatriz, 0] = "TIP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["TIP_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.TipCodigo + "'";
                strCmd += "TIP_CODIGO = " + dadosNovos.TipCodigo + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntDtEmis.Equals(dadosCadastrados.Rows[0]["ENT_DTEMIS"].ToString()))
            {
                dados[contMatriz, 0] = "ENT_DTEMIS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_DTEMIS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntDtEmis + "'";
                strCmd += "ENT_DTEMIS = " + Funcoes.BData(Convert.ToDateTime(dadosNovos.EntDtEmis)) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.CfDocto.Equals(dadosCadastrados.Rows[0]["CF_DOCTO"].ToString()))
            {
                dados[contMatriz, 0] = "CF_DOCTO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["CF_DOCTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.CfDocto + "'";
                strCmd += "CF_DOCTO = '" + dadosNovos.CfDocto + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntDocto.Equals(dadosCadastrados.Rows[0]["ENT_DOCTO"]))
            {
                dados[contMatriz, 0] = "ENT_DOCTO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_DOCTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntDocto + "'";
                strCmd += "ENT_DOCTO = '" + dadosNovos.EntDocto + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntSerie.Equals(dadosCadastrados.Rows[0]["ENT_SERIE"]))
            {
                dados[contMatriz, 0] = "ENT_SERIE";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_SERIE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntSerie + "'";
                strCmd += "ENT_SERIE = '" + dadosNovos.EntSerie + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.NoCodigo.Equals(Convert.ToInt32(dadosCadastrados.Rows[0]["NO_CODIGO"])))
            {
                dados[contMatriz, 0] = "NO_CODIGO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["NO_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.NoCodigo + "'";
                strCmd += "NO_CODIGO = '" + dadosNovos.NoCodigo + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.NoComp.Equals(Convert.ToInt32(dadosCadastrados.Rows[0]["NO_COMP"])))
            {
                dados[contMatriz, 0] = "NO_COMP";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["NO_COMP"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.NoComp + "'";
                strCmd += "NO_COMP = '" + dadosNovos.NoComp + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.PagDocto.Equals(dadosCadastrados.Rows[0]["PAG_DOCTO"]))
            {
                dados[contMatriz, 0] = "PAG_DOCTO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["PAG_DOCTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.PagDocto + "'";
                strCmd += "PAG_CODIGO= '" + dadosNovos.PagDocto + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntTotal.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_TOTAL"])))
            {
                dados[contMatriz, 0] = "ENT_TOTAL";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_TOTAL"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntTotal + "'";
                strCmd += "ENT_TOTAL = " + Funcoes.BValor(dadosNovos.EntTotal) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntBaseIcms.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_BASEICMS"])))
            {
                dados[contMatriz, 0] = "ENT_BASEICMS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_BASEICMS"] + "'";
                dados[contMatriz, 2] = "'" +dadosNovos.EntBaseIcms + "'";
                strCmd += "ENT_BASEICMS = " + Funcoes.BValor(dadosNovos.EntBaseIcms) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorIcms.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_IMPICMS"])))
            {
                dados[contMatriz, 0] = "ENT_IMPICMS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_IMPICMS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorIcms + "'";
                strCmd += "ENT_IMPICMS = " + Funcoes.BValor(dadosNovos.EntValorIcms) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntBaseIcmsST.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_BASESUBST"])))
            {
                dados[contMatriz, 0] = "ENT_BASESUBST";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_BASESUBST"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntBaseIcmsST + "'";
                strCmd += "ENT_BASESUBST = " + Funcoes.BValor(dadosNovos.EntBaseIcmsST) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorIcmsSt.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_SUBST"])))
            {
                dados[contMatriz, 0] = "ENT_SUBST";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_SUBST"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorIcmsSt + "'";
                strCmd += "ENT_SUBST = " + Funcoes.BValor(dadosNovos.EntValorIcmsSt) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorImpImportacao.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_ISENTOIPI"])))
            {
                dados[contMatriz, 0] = "ENT_ISENTOIPI";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_ISENTOIPI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorImpImportacao + "'";
                strCmd += "ENT_ISENTOIPI = " + Funcoes.BValor(dadosNovos.EntValorImpImportacao) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorIcmsUFRemet.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_OUTIPI"])))
            {
                dados[contMatriz, 0] = "ENT_OUTIPI";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_OUTIPI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorIcmsUFRemet + "'";
                strCmd += "ENT_OUTIPI = " + Funcoes.BValor(dadosNovos.EntValorIcmsUFRemet) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorFCP.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_BASEISS"])))
            {
                dados[contMatriz, 0] = "ENT_BASEISS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_BASEISS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorFCP + "'";
                strCmd += "ENT_BASEISS = " + Funcoes.BValor(dadosNovos.EntValorFCP) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorPIS.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_ISENTOICMS"])))
            {
                dados[contMatriz, 0] = "ENT_ISENTOICMS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_ISENTOICMS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorPIS + "'";
                strCmd += "ENT_ISENTOICMS = " + Funcoes.BValor(dadosNovos.EntValorPIS) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorProdutos.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_DIVERSAS"])))
            {
                dados[contMatriz, 0] = "ENT_DIVERSAS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_DIVERSAS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorProdutos + "'";
                strCmd += "ENT_DIVERSAS = " + Funcoes.BValor(dadosNovos.EntValorProdutos) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorFrete.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_FRETE"])))
            {
                dados[contMatriz, 0] = "ENT_FRETE";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_FRETE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorFrete + "'";
                strCmd += "ENT_FRETE = " + Funcoes.BValor(dadosNovos.EntValorFrete) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorSeguro.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_IMPFRETE"])))
            {
                dados[contMatriz, 0] = "ENT_IMPFRETE";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_IMPFRETE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorSeguro + "'";
                strCmd += "ENT_IMPFRETE = " + Funcoes.BValor(dadosNovos.EntValorSeguro) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorDesconto.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_DESCONTO"])))
            {
                dados[contMatriz, 0] = "ENT_DESCONTO";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_DESCONTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorDesconto + "'";
                strCmd += "ENT_DESCONTO = " + Funcoes.BValor(dadosNovos.EntValorDesconto) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntOutasDespesas.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_OUTRAS"])))
            {
                dados[contMatriz, 0] = "ENT_OUTRAS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_OUTRAS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntOutasDespesas + "'";
                strCmd += "ENT_OUTRAS = " + Funcoes.BValor(dadosNovos.EntOutasDespesas) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorIPI.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_IMPIPI"])))
            {
                dados[contMatriz, 0] = "ENT_IMPIPI";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_IMPIPI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorIPI + "'";
                strCmd += "ENT_IMPIPI = " + Funcoes.BValor(dadosNovos.EntValorIPI) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorICMSUFDest.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_BASEIPI"])))
            {
                dados[contMatriz, 0] = "ENT_BASEIPI";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_BASEIPI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorICMSUFDest + "'";
                strCmd += "ENT_BASEIPI = " + Funcoes.BValor(dadosNovos.EntValorICMSUFDest) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntTotalTributos.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_IRRF"])))
            {
                dados[contMatriz, 0] = "ENT_IRRF";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_IRRF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntTotalTributos + "'";
                strCmd += "ENT_IRRF = " + Funcoes.BValor(dadosNovos.EntTotalTributos) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntValorCofins.Equals(Convert.ToDouble(dadosCadastrados.Rows[0]["ENT_IMPISS"])))
            {
                dados[contMatriz, 0] = "ENT_IMPISS";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["ENT_IMPISS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntValorCofins + "'";
                strCmd += "ENT_IMPISS = " + Funcoes.BValor(dadosNovos.EntValorCofins) + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ColCodigo.Equals(Convert.ToInt32(dadosCadastrados.Rows[0]["PAG_COMPRADOR"])))
            {
                dados[contMatriz, 0] = "PAG_COMPRADOR";
                dados[contMatriz, 1] = "'" + dadosCadastrados.Rows[0]["PAG_COMPRADOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ColCodigo + "'";
                strCmd += "PAG_COMPRADOR = " + dadosNovos.ColCodigo + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.EntObs.Equals(dadosCadastrados.Rows[0]["ENT_OBS"].ToString()))
            {
                dados[contMatriz, 0] = "ENT_OBS";
                dados[contMatriz, 1] = dadosCadastrados.Rows[0]["ENT_OBS"].ToString() == "" ? "null" : "'" + dadosCadastrados.Rows[0]["ENT_OBS"] + "'";
                dados[contMatriz, 2] = dadosNovos.EntObs == "" ? "null" : "'" +dadosNovos.EntObs + "'";
                strCmd += "ENT_OBS = ";
                strCmd += dadosNovos.EntObs == "" ? "null," : "'" + dadosNovos.EntObs + "',";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosCadastrados.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosCadastrados.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(dadosNovos.DtAlteracao);
                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",";
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosCadastrados.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosCadastrados.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OpAlteracao + "'";
                strCmd += " OPALTERACAO = '" +dadosNovos.OpAlteracao + "' ";
                contMatriz = contMatriz + 1;

                strCmd += "WHERE EST_CODIGO = " + dadosNovos.EstCodigo + " AND EMP_CODIGO = " + dadosNovos.EmpCodigo 
                    + " AND ENT_ID = " + dadosNovos.EntId;

                if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return false;
                }
                else
                    Funcoes.GravaLogAlteracao("ENT_ID", dadosNovos.EntId.ToString(), dadosNovos.OpAlteracao, "ENTRADA", dados, contMatriz, dadosNovos.EmpCodigo, dadosNovos.EstCodigo, true);
            }

            return true;
        }

        public bool ExcluiEntradaNota(int estCodigo, int empCodigo, int entId)
        {
            string strCmd = "DELETE FROM ENTRADA WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + "  AND ENT_ID = " + entId;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable IdentificaUltimasCompras(string prodCodigo)
        {
            string strSql = "select a.ent_dtlanc, b.ent_totitem, b.ent_valoripi, b.ent_qtdestoque, c.cf_nome, c.cf_apelido" +
                     " from entrada a, entrada_itens b, clifor c" +
                     " where b.emp_codigo = " + Principal.empAtual +
                     " and b.est_codigo = " + Principal.estAtual +
                     " and b.prod_codigo = '" + prodCodigo + "'" +
                     " and a.emp_codigo = b.emp_codigo" +
                     " and a.est_codigo = b.est_codigo" +
                     " and a.cf_docto = b.cf_docto" +
                     " and a.ent_docto = b.ent_docto" +
                     " and a.ent_serie = b.ent_serie" +
                     " and a.cf_docto = c.cf_docto" +
                     " order by a.ent_dtlanc desc";
            
            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable RelatorioSintetico(int empCodigo, int estCodigo, int tipoData, string dataInicial, string dataFinal, string numDocto)
        {
            string strSql = "SELECT A.ENT_ID AS END_ID, ";
            if(tipoData.Equals(0))
            {
                strSql += " A.ENT_DTEMIS AS DATA,";
            }
            else
            {
                strSql += " A.ENT_DTLANC AS DATA,";
            }

            strSql += "           B.CF_NOME,"
                    + "           A.ENT_DOCTO,"
                    + "           A.ENT_SERIE,"
                    + "           A.PAG_DOCTO,";

            if (tipoData.Equals(0))
            {
                strSql += " A.ENT_DTLANC AS DATA2,";
            }
            else
            {
                strSql += " A.ENT_DTEMIS AS DATA2,";
            }

            strSql += "           SUM(C.ENT_QTDE) AS QTDE,"
                    + "           A.ENT_TOTAL"
                    + "      FROM ENTRADA A"
                    + "     INNER JOIN CLIFOR B ON A.CF_DOCTO = B.CF_DOCTO"
                    + "     INNER JOIN ENTRADA_ITENS C ON A.ENT_ID = C.ENT_ID"
                    + "     WHERE A.EMP_CODIGO = " + empCodigo
                    + "     AND A.EST_CODIGO = " + estCodigo
                    + "     AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + "     AND A.EST_CODIGO = C.EST_CODIGO";
            if(tipoData.Equals(0))
            {
                strSql += " AND A.ENT_DTEMIS BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));
            }
            else
            {
                strSql += " AND A.ENT_DTLANC BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));
            }

            if(!String.IsNullOrEmpty(numDocto))
            {
                strSql += " AND A.ENT_DOCTO '" + numDocto + "'";
            }

            strSql += "     GROUP BY A.ENT_DTLANC,"
                    + "              B.CF_NOME,"
                    + "              A.ENT_DOCTO,"
                    + "              A.ENT_SERIE,"
                    + "              A.PAG_DOCTO,"
                    + "              A.ENT_DTEMIS,"
                    + "              A.ENT_ID,"
                    + "              A.ENT_TOTAL";

            if (tipoData.Equals(0))
            {
                strSql += " ORDER BY A.ENT_DTLANC, B.CF_NOME";
            }
            else
            {
                strSql += " ORDER BY A.ENT_DTEMIS, B.CF_NOME";
            }
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaItensPorEntID(int empCodigo, int estCodigo, string entID)
        {
            string strSql = "SELECT A.PROD_CODIGO,"
                            + "       B.PROD_DESCR,"
                            + "       A.ENT_QTDE,"
                            + "       A.ENT_QTDESTOQUE,"
                            + "       A.ENT_UNITARIO,"
                            + "       A.ENT_VALOR_VENDA,"
                            + "       A.ENT_TOTITEM,"
                            + "       A.ENT_ID AS END_ID"
                            + "  FROM ENTRADA_ITENS A"
                            + "  INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "  WHERE A.EMP_CODIGO = " + empCodigo
                            + "  AND A.EST_CODIGO = " + estCodigo
                            + "  AND A.ENT_ID = " + entID;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaParcelasPorEntID(int empCodigo, int estCodigo, string entID)
        {
            string strSql = "SELECT  A.ENT_PARCELA,"
                            + "       A.ENT_VALPARC,"
                            + "       A.ENT_VENCTO,"
                            + "       A.ENT_ID AS END_ID"
                            + "  FROM ENTRADA_DETALHES A"
                            + "  WHERE A.EMP_CODIGO = " + empCodigo
                            + "  AND A.EST_CODIGO = " + estCodigo
                            + "  AND A.ENT_ID = " + entID;

            return BancoDados.GetDataTable(strSql, null);
        }

    }
}
