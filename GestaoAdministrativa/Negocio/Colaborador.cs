﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class Colaborador
    {
        public int EmpCodigo { get; set; }
        public int ColCodigo { get; set; }
        public string ColNome { get; set; }
        public string ColApelido { get; set; }
        public string ColEndereco { get; set; }
        public string ColBairro { get; set; }
        public string ColCidade { get; set; }
        public string ColUf { get; set; }
        public string ColCep { get; set; }
        public string ColFone { get; set; }
        public string ColCelular { get; set; }
        public string ColEmail { get; set; }
        public string ColStatus { get; set; }
        public string ColRg { get; set; }
        public decimal ColValorHora { get; set; }
        public decimal ColComissao { get; set; }
        public string ColSenha { get; set; }
        public string ColSenhaSup { get; set; }
        public string ColLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Colaborador() { }

        public Colaborador(int empCodigo, int colCodigo, string colNome, string colApelido, string colEndereco, string colBairro,
                           string colCidade, string colUf, string colCep, string colFone, string colCelular, string colEmail,
                           string colStatus, string colRg, decimal colValorHora, decimal colComissao, string colSenha,
                           string colSenhaSup, string colLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro,
                           string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.ColCodigo = colCodigo;
            this.ColNome = colNome;
            this.ColApelido = colApelido;
            this.ColEndereco = colEndereco;
            this.ColBairro = colBairro;
            this.ColCidade = colCidade;
            this.ColUf = colUf;
            this.ColCep = colCep;
            this.ColFone = colFone;
            this.ColCelular = colCelular;
            this.ColEmail = colEmail;
            this.ColStatus = colStatus;
            this.ColRg = colRg;
            this.ColValorHora = colValorHora;
            this.ColComissao = colComissao;
            this.ColSenha = colSenha;
            this.ColSenhaSup = colSenhaSup;
            this.ColLiberado = colLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscaDados(int estCodigo, int colCodigo, string colNome, string desabilitado, out string strOrdem)
        {
            string sql = " SELECT EMP_CODIGO, COL_CODIGO, COL_NOME, COL_APELIDO, COL_ENDER, COL_BAIRRO, COL_CIDADE, COL_UF, "
                       + " COL_CEP,COL_FONE, COL_CELULAR, COL_EMAIL, COL_STATUS, DAT_ALTERACAO, COL_RG, "
                       + " CASE COL_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS COL_DESABILITADO, "
                       + " COL_COMISSAO, COL_SENHA, COL_SENHA_SUPERVISOR, COL_DTCADASTRO, COL_OPALTERACAO, COL_OPCADASTRO, COL_VALOR_HORA "
                       + " FROM COLABORADORES "
                       + " WHERE EMP_CODIGO = " + estCodigo;

            if (colCodigo != 0)
            {
                sql += " AND COL_CODIGO = " + colCodigo;
            }
            if (colNome != "")
            {
                sql += " AND COL_NOME LIKE '%" + colNome + "%'";
            }
            if (desabilitado != "TODOS")
            {
                desabilitado = desabilitado == "S" ? "N" : "S";
                sql += " AND COL_DESABILITADO = '" + desabilitado + "'";
            }

            strOrdem = sql;
            sql += " ORDER BY EMP_CODIGO, COL_NOME";


            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(Colaborador dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE COLABORADORES SET "
                       + " COL_NOME = '" + dadosNovos.ColNome + "'"
                       + " ,COL_APELIDO = '" + dadosNovos.ColApelido + "'"
                       + " ,COL_ENDER = '" + dadosNovos.ColEndereco + "'"
                       + " ,COL_BAIRRO =  '" + dadosNovos.ColBairro + "'"
                       + " ,COL_CIDADE = '" + dadosNovos.ColCidade + "'"
                       + " ,COL_UF = '" + dadosNovos.ColUf + "'"
                       + " ,COL_CEP = '" + dadosNovos.ColCep + "'"
                       + " ,COL_FONE = '" + dadosNovos.ColFone + "'"
                       + " ,COL_CELULAR = '" + dadosNovos.ColCelular + "'"
                       + " ,COL_EMAIL = '" + dadosNovos.ColEmail + "'"
                       + " ,COL_STATUS = '" + dadosNovos.ColStatus + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " ,COL_RG = '" + dadosNovos.ColRg + "'"
                       + " ,COL_DESABILITADO = '" + dadosNovos.ColLiberado + "'"
                       + " ,COL_COMISSAO = '" + dadosNovos.ColComissao + "'"
                       + " ,COL_OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " ,COL_VALOR_HORA = '" + dadosNovos.ColValorHora + "'"
                       + " ,COL_SENHA = '" + dadosNovos.ColSenha + "'";


            if (dadosNovos.ColSenhaSup != "")
            {
                sql += ", COL_SENHA_SUPERVISOR = '" + dadosNovos.ColSenhaSup + "'";
            }
            sql += "WHERE COL_CODIGO = " + dadosNovos.ColCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[18, 3];
                int contMatriz = 0;

                if (!dadosNovos.ColNome.Equals(dadosAntigos.Rows[0]["COL_NOME"]))
                {
                    dados[contMatriz, 0] = "COL_NOME";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_NOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColNome + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColApelido.Equals(dadosAntigos.Rows[0]["COL_APELIDO"]))
                {
                    dados[contMatriz, 0] = "COL_APELIDO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_APELIDO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColApelido + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColEndereco.Equals(dadosAntigos.Rows[0]["COL_ENDER"]))
                {
                    dados[contMatriz, 0] = "COL_ENDER";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_ENDER"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColEndereco + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColBairro.Equals(Convert.ToString(dadosAntigos.Rows[0]["COL_BAIRRO"])))
                {
                    dados[contMatriz, 0] = "COL_BAIRRO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_BAIRRO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColBairro + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColCidade.Equals(Convert.ToString(dadosAntigos.Rows[0]["COL_CIDADE"])))
                {
                    dados[contMatriz, 0] = "COL_CIDADE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_CIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColCidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColUf.Equals(dadosAntigos.Rows[0]["COL_UF"]))
                {
                    dados[contMatriz, 0] = "COL_UF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_UF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColUf + "'";
                    contMatriz = contMatriz + 1;
                }
                if (Convert.ToDecimal(dadosNovos.ColCep) != Convert.ToDecimal(dadosAntigos.Rows[0]["COL_CEP"]))
                {
                    dados[contMatriz, 0] = "COL_CEP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_CEP"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColCep + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColFone.Equals(Convert.ToString(dadosAntigos.Rows[0]["COL_FONE"])))
                {
                    dados[contMatriz, 0] = "COL_FONE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_FONE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColFone + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColCelular.Equals(Convert.ToString(dadosAntigos.Rows[0]["COL_CELULAR"])))
                {
                    dados[contMatriz, 0] = "COL_CELULAR";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_CELULAR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColCelular + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColEmail.Equals(Convert.ToString(dadosAntigos.Rows[0]["COL_EMAIL"])))
                {
                    dados[contMatriz, 0] = "COL_EMAIL";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_EMAIL"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColEmail + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColStatus.Equals(dadosAntigos.Rows[0]["COL_STATUS"]))
                {
                    dados[contMatriz, 0] = "COL_STATUS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_STATUS"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColStatus + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColRg.Equals(dadosAntigos.Rows[0]["COL_RG"]))
                {
                    dados[contMatriz, 0] = "COL_RG";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_RG"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColRg + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColLiberado.Equals(dadosAntigos.Rows[0]["COL_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "COL_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColComissao.Equals(dadosAntigos.Rows[0]["COL_COMISSAO"]))
                {
                    dados[contMatriz, 0] = "COL_COMISSAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_COMISSAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColComissao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ColComissao.Equals(dadosAntigos.Rows[0]["COL_VALOR_HORA"]))
                {
                    dados[contMatriz, 0] = "COL_VALOR_HORA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_VALOR_HORA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColComissao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.DescriptografaSenha(dadosNovos.ColSenha).Equals(Funcoes.DescriptografaSenha(dadosAntigos.Rows[0]["COL_SENHA"].ToString())))
                {
                    dados[contMatriz, 0] = "COL_SENHA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_SENHA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColSenha + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.DescriptografaSenha(dadosNovos.ColSenhaSup).Equals(Funcoes.DescriptografaSenha(dadosAntigos.Rows[0]["COL_SENHA_SUPERVISOR"].ToString())))
                {
                    dados[contMatriz, 0] = "COL_SENHA_SUPERVISOR";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["COL_SENHA_SUPERVISOR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ColSenhaSup + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("COL_CODIGO", dadosNovos.ColCodigo.ToString(), Principal.usuario, "COLABORADORES", dados, contMatriz, Principal.estAtual, dadosNovos.EmpCodigo);
                return true;
            }
            else
            {
                return false;
            }

        }


        public bool InsereDados(Colaborador dados)
        {

            string sql = " INSERT INTO COLABORADORES(EMP_CODIGO, COL_CODIGO, COL_NOME, COL_APELIDO, COL_ENDER, COL_BAIRRO, "
                       + " COL_CIDADE, COL_UF, COL_CEP, COL_FONE, COL_CELULAR, COL_EMAIL, COL_STATUS,"
                       + " COL_RG, COL_VALOR_HORA, COL_DESABILITADO, COL_COMISSAO, COL_DTCADASTRO, COL_SENHA_SUPERVISOR, "
                       + " COL_OPCADASTRO, COL_SENHA) VALUES ("
                       + dados.EmpCodigo + ","
                       + dados.ColCodigo + ",'"
                       + dados.ColNome + "','"
                       + dados.ColApelido + "','"
                       + dados.ColEndereco + "','"
                       + dados.ColBairro + "','"
                       + dados.ColCidade + "','"
                       + dados.ColUf + "',"
                       + dados.ColCep + ",'"
                       + dados.ColFone + "','"
                       + dados.ColCelular + "','"
                       + dados.ColEmail + "','"
                       + dados.ColStatus + "','"
                       + dados.ColRg + "','"
                       + dados.ColValorHora + "','"
                       + dados.ColLiberado + "','"
                       + dados.ColComissao + "',"
                       + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                       + dados.ColSenhaSup + "','"
                       + dados.OpCadastro + "','"
                       + dados.ColSenha + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("COL_CODIGO", Convert.ToString(dados.ColCodigo), Principal.usuario, "COLABORADORES", dados.ColNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public bool ExcluirDados(int colCodigo)
        {
            string sql = "DELETE FROM COLABORADORES WHERE EMP_CODIGO= " + Principal.empAtual + " AND COL_CODIGO = " + colCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;


        }

        public string DadosColaborador(int empCodigo, string colCodigo, bool colVenda, string colStatus1, string colStatus2 = "", string colStatus3 = "")
        {
            string strSql = "SELECT COL_STATUS FROM COLABORADORES WHERE EMP_CODIGO = " + empCodigo + " AND COL_CODIGO = " + colCodigo + " AND COL_DESABILITADO = 'N'";

            if (colVenda.Equals(true))
            {
                strSql += " AND COL_STATUS IN ('" + colStatus1 + "','" + colStatus2 + "','" + colStatus3 + "')";
            }
            else
            {
                strSql += " AND COL_STATUS = '" + colStatus1 + "'";
            }

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public string ValidaSenhaColaborador(int empCodigo, string colCodigo, string colStatus1, string colStatus2, string colStatus3)
        {
            string strSql = " SELECT COL_SENHA FROM COLABORADORES WHERE COL_CODIGO = " + colCodigo +
                " AND EMP_CODIGO = " + empCodigo + " AND (COL_STATUS = '" + colStatus1 + "' or COL_STATUS = '" + colStatus2 + "' or COL_STATUS = '" + colStatus3 + "')";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public string StatusColaborador(int empCodigo, string colCodigo, string colStatus)
        {
            string strSql = "SELECT COL_CODIGO FROM COLABORADORES WHERE EMP_CODIGO = " + empCodigo + " AND COL_CODIGO = " + colCodigo +
                " AND COL_STATUS = '" + colStatus + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public string NomeColaborador(string colCodigo, int empCodigo)
        {
            string strSql = "SELECT COL_NOME FROM COLABORADORES WHERE COL_CODIGO = " + colCodigo + " AND EMP_CODIGO = " + empCodigo;

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public DataTable BuscaDadosColaborador(int empCodigo, string colStatus)
        {
            string strSql = "SELECT COL_CODIGO, COL_NOME FROM COLABORADORES WHERE EMP_CODIGO = " + empCodigo + " AND COL_STATUS IN ('X','V')"
                + " AND COL_DESABILITADO = 'N' ORDER BY COL_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

      


        public double TotalVendasPorPeriodo(DateTime dtInicial, DateTime dtFinal, string usuario, string operacao)
        {
            string sql = " SELECT COALESCE (SUM(B.MOVIMENTO_CX_ESPECIE_VALOR),0) FROM MOVIMENTO_CAIXA A "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE B ON A.MOVIMENTO_CAIXA_DATA = B.MOVIMENTO_CX_ESPECIE_DATA "
                       + " WHERE B.MOVIMENTO_CX_ESPECIE_USUARIO = '" + usuario + "'"
                       + " AND B.MOVIMENTO_CX_ESPECIE_CODIGO = 1"
                       + " AND A.MOVIMENTO_CAIXA_ODC_CLASSE = '" + operacao + "'"
                       + " AND A.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            return Convert.ToDouble(BancoDados.ExecuteScalar(sql, null));
        }

    }
}
