﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    class GrupoUsuarios
    {
        public int GrupoUsuId { get; set; }
        public string Descricao { get; set; }
        public string Administrador { get; set; }
        public string Liberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public GrupoUsuarios() { }

        public static bool InserirDados(GrupoUsuarios dados)
        {
            MontadorSql mont = new MontadorSql("grupo_usuarios", MontadorType.Insert);
            mont.AddField("grupo_usu_id", dados.GrupoUsuId);
            mont.AddField("descricao", dados.Descricao);
            mont.AddField("administrador", dados.Administrador);
            mont.AddField("liberado", dados.Liberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static bool AtualizaDados(GrupoUsuarios dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("grupo_usuarios", MontadorType.Update);
            if (!dadosNew.Descricao.Equals(dadosOld.Rows[0]["DESCRICAO"]))
            {
                if (Util.GetRegistros("GRUPO_USUARIOS","DESCRICAO", dadosNew.Descricao).Rows.Count > 1)
                {
                    Principal.mensagem = "Grupo já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                mont.AddField("descricao", dadosNew.Descricao);

                dados[contMatriz, 0] = "DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Descricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Administrador.Equals(dadosOld.Rows[0]["ADMINISTRADOR"]))
            {
                mont.AddField("administrador", dadosNew.Administrador);

                dados[contMatriz, 0] = "ADMINISTRADOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ADMINISTRADOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Administrador + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Liberado.Equals(dadosOld.Rows[0]["LIBERADO"]))
            {
                mont.AddField("liberado", dadosNew.Liberado);

                dados[contMatriz, 0] = "LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Liberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuarioID);
                mont.SetWhere("WHERE GRUPO_USU_ID = " + dadosNew.GrupoUsuId, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) == 1)
                {
                    if (Funcoes.LogAlteracao("GRUPO_USU_ID", dadosNew.GrupoUsuId.ToString(), Principal.usuarioID, "GRUPO_USUARIOS", dados, contMatriz).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        public static int ExcluirDados(string gCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("grupo_usu_id", gCodigo));

            Principal.strCmd = "DELETE FROM GRUPO_USUARIOS WHERE GRUPO_USU_ID = @grupo_usu_id";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);
        }

        public static DataTable GetBuscar(GrupoUsuarios dadosBusca, out string strOrdem)
        {
            Principal.strSql = "SELECT A.GRUPO_USU_ID, A.DESCRICAO, A.ADMINISTRADOR, A.LIBERADO, A.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = A.OPALTERACAO)"
                    + " AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO FROM GRUPO_USUARIOS A, USUARIO_SYSTEM B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.GrupoUsuId == 0 && dadosBusca.Descricao == "" && dadosBusca.Liberado == "TODOS")
            {
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.GRUPO_USU_ID";
            }
            else
            {
                if (dadosBusca.GrupoUsuId != 0)
                {
                    Principal.strSql += " AND A.GRUPO_USU_ID = " + dadosBusca.GrupoUsuId;
                }
                if (dadosBusca.Descricao != "")
                {
                    Principal.strSql += " AND A.DESCRICAO LIKE '%" + dadosBusca.Descricao + "%'";
                }
                if (dadosBusca.Liberado != "TODOS")
                {
                    Principal.strSql += " AND A.LIBERADO = '" + dadosBusca.Liberado + "'";
                }

                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.GRUPO_USU_ID";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

    }
}
