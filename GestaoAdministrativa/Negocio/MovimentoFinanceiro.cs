﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class MovimentoFinanceiro
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long MovimentoFinanceiroID { get; set; }
        public DateTime MovimentoFinanceiroData { get; set; }
        public DateTime MovimentoFinanceiroDataHora { get; set; }
        public string MovimentoFinanceiroTipo { get; set; }
        public string MovimentoFinanceiroUsuario { get; set; }
        public double MovimentoFinanceiroValor { get; set; }
        public long MovimentoFinanceiroSeq { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public long VendaId { get; set; }

        public MovimentoFinanceiro() { }

        public MovimentoFinanceiro(int empCodigo, int estCodigo, long movimentoFinanceiroID, DateTime movimentoFinanceiroData, DateTime movimentoFinanceiroDataHora, string movimentoFinanceiroTipo,string movimentoFinanceiroUsuario,
            double movimentoFinanceiroValor, long movimentoFinanceiroSeq, DateTime dtCadastro, string opCadastro, long vendaID)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.MovimentoFinanceiroID = movimentoFinanceiroID;
            this.MovimentoFinanceiroData = movimentoFinanceiroData;
            this.MovimentoFinanceiroDataHora = movimentoFinanceiroDataHora;
            this.MovimentoFinanceiroTipo = movimentoFinanceiroTipo;
            this.MovimentoFinanceiroUsuario = movimentoFinanceiroUsuario;
            this.MovimentoFinanceiroValor = movimentoFinanceiroValor;
            this.MovimentoFinanceiroSeq = movimentoFinanceiroSeq;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.VendaId = vendaID;
        }

        public bool InserirDados(MovimentoFinanceiro dados)
        {
            string strCmd = "INSERT INTO MOVIMENTO_FINANCEIRO(EMP_CODIGO,EST_CODIGO,MOVIMENTO_FINANCEIRO_ID,MOVIMENTO_FINANCEIRO_DATA, MOVIMENTO_FINANCEIRO_DATAHORA,"
                + "MOVIMENTO_FINANCEIRO_TIPO,MOVIMENTO_FINANCEIRO_USUARIO,"
                + "MOVIMENTO_FINANCEIRO_VALOR,MOVIMENTO_FINANCEIRO_SEQ,DT_CADASTRO,OP_CADASTRO, VENDA_ID)"
                + " VALUES(" + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.MovimentoFinanceiroID + ","
                + Funcoes.BData(dados.MovimentoFinanceiroData) + ","
                + Funcoes.BDataHora(dados.MovimentoFinanceiroDataHora) + ",'"
                + dados.MovimentoFinanceiroTipo + "','"
                + dados.MovimentoFinanceiroUsuario + "',"
                + Funcoes.BValor(dados.MovimentoFinanceiroValor) + ","
                + dados.MovimentoFinanceiroSeq + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "',"
                + dados.VendaId + ")";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable VerficaMovimentoFinanceiro(string usuario, DateTime dtInicial, DateTime dtFinal)
        {
            string sql = " SELECT * FROM MOVIMENTO_FINANCEIRO WHERE MOVIMENTO_FINANCEIRO_USUARIO ='" + usuario + "' AND "
                       + " MOVIMENTO_FINANCEIRO_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            return BancoDados.GetDataTable(sql, null);

        }

        public bool ExcluiMovimentoFinanceiroPorDataHora(DateTime dtMovimento)
        {
            string sql = "DELETE FROM MOVIMENTO_FINANCEIRO WHERE MOVIMENTO_FINANCEIRO_DATAHORA = " + Funcoes.BDataHora(dtMovimento);

            try
            {
                BancoDados.ExecuteNoQueryTrans(sql, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
