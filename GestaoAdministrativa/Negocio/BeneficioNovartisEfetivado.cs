﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioNovartisEfetivado
    {
        public string NSU { get; set; }
        public long VendaId { get; set; }
        public string Lote { get; set; }
        public string CodAutorizacao { get; set; }
        public double PrecoBruto { get; set; }
        public double PrecoLiquido { get; set; }
        public string Cartao { get; set; }
        public string Operadora { get; set; }
        public double ValorReceberLoja { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }  

        public BeneficioNovartisEfetivado() { }

        public BeneficioNovartisEfetivado(string nsu, long vendaId, string lote, string codAutorizacao, double precoBruto, double precoLiquido, string cartao,
            string operadora, double valorReceberLoja, DateTime data, string status, DateTime dtCadastro, string opCadastro)
        {
            this.NSU = nsu;
            this.VendaId = vendaId;
            this.Lote = lote;
            this.CodAutorizacao = codAutorizacao;
            this.PrecoBruto = precoBruto;
            this.PrecoLiquido = precoLiquido;
            this.Cartao = cartao;
            this.Operadora = operadora;
            this.ValorReceberLoja = valorReceberLoja;
            this.Data = data;
            this.Status = status;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InsereRegistros(BeneficioNovartisEfetivado dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_NOVARTIS_EFETIVADO(NSU, PRE_BRUTO, PRE_LIQUIDO, CARTAO,"
                + " OPERADORA, VALOR_RECEBER_LOJA, DATA, STATUS, DTCADASTRO, OPCADASTRO) VALUES ('"
                + dados.NSU + "',"
                + Funcoes.BValor(dados.PrecoBruto) + ","
                + Funcoes.BValor(dados.PrecoLiquido) + ",'"
                + dados.Cartao + "','"
                + dados.Operadora + "',"
                + Funcoes.BValor(dados.ValorReceberLoja) + ","
                + Funcoes.BData(dados.Data) + ",'"
                + dados.Status + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public int ExcluirDadosPorNSU(string nsu)
        {
            string strCmd = "DELETE FROM BENEFICIO_NOVARTIS_EFETIVADO WHERE NSU = '" + nsu + "'";

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable BuscaVendasPorNsu(string nsu)
        {
            string strSql;

            strSql = "SELECT  * FROM BENEFICIO_NOVARTIS_EFETIVADO A"
                       + " WHERE A.NSU = '" + nsu + "' AND A.STATUS = 'A'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaDadosNsu(string nsu, string status, string cartao, long vendaID)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_EFETIVADO SET STATUS = '" + status + "', CARTAO = '" + cartao + "'"
                + ", VENDA_ID = " + vendaID + " WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDadosLoteECod(string nsu, string lote, string codAutorizacao)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_EFETIVADO SET LOTE = '" + lote + "', COD_AUTORIZACAO = '" + codAutorizacao + "' WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }


        public bool AtualizaValoresEfetivado(string nsu, double bruto, double liquido, double receber)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_EFETIVADO SET PRE_BRUTO = " + Funcoes.BValor(bruto) + ",  PRE_LIQUIDO = " + Funcoes.BValor(liquido)
                + ", VALOR_RECEBER_LOJA = " + Funcoes.BValor(receber) + " WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
