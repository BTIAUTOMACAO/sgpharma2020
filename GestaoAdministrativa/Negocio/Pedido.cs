﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Pedido
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public long PedNumero { get; set; }
        public string PedComplemento { get; set; }
        public string CfDocto { get; set; }
        public string PedEmissao { get; set; }
        public double PedTotal { get; set; }
        public int ColCodigo { get; set; }
        public int TabCodigo { get; set; }
        public int PedFormaPagto { get; set; }
        public DateTime PedData { get; set; }
        public string PedNatOper { get; set; }
        public string PedNoComp { get; set; }
        public double PedSubTotal { get; set; }
        public double PedDifer { get; set; }
        public int PedRomEnt { get; set; }
        public int PedRomProd { get; set; }
        public int PedAuxiliar { get; set; }
        public long PedNF { get; set; }
        public string PedSerieNF { get; set; }
        public int PedStatus { get; set; }
        public string PedObs { get; set; }
        public string LoginId { get; set; }
        public string PedCliente { get; set; }
        public string PedCnpj { get; set; }
        public string PedInscricao { get; set; }
        public string PedEndereco { get; set; }
        public string PedNumEndereco { get; set; }
        public string PedBairro { get; set; }
        public string PedCep { get; set; }
        public string PedCidade { get; set; }
        public string PedUF { get; set; }
        public string PedTelefone { get; set; }
        public string RecDocto { get; set; }
        public string PedNumCF { get; set; }
        public int PedNumCaixa { get; set; }
        public long MovtoSeq { get; set; }
        public string PedTipo { get; set; }
        public DateTime DataAlteracao { get; set; }
        public DateTime PedHoraVenda { get; set; }
        public string PedAutorizacao { get; set; }
        public string MedCrm { get; set; }
        public string PedCartao { get; set; }
        public string PedNomeCartao { get; set; }
        public int PosNumero { get; set; }
        public string PedTransferencia { get; set; }
        public string PedChapa { get; set; }
        public string CfDoctoPos { get; set; }
        public string ConCodigo { get; set; }
        public string ConCodConv { get; set; }
        public string ConWeb { get; set; }
        public string PedNFeDevolucao { get; set; }
        public string PedEmitido { get; set; }
        public string PedConvenioEntrega { get; set; }
        public string PedConvenioObrigaSenha { get; set; }
        public string PedNumExtratoSat { get; set; }
        public string PedEspecieSat { get; set; }
        public string PedCloseUp { get; set; }
        public int CprNum { get; set; }

        public Pedido() { }

        public string IdentificaUltimaVendaProduto(string prod_codigo)
        {
            string strSql = "SELECT MAX(A.VENDA_DATA) AS PED_DATA FROM VENDAS A, VENDAS_ITENS B" +
                " WHERE B.PROD_CODIGO = '" + prod_codigo + "'" +
                " AND A.EMP_CODIGO = " + Principal.empAtual +
                " AND A.EST_CODIGO = " + Principal.estAtual +
                " AND A.EMP_CODIGO = B.EMP_CODIGO AND A.EST_CODIGO = B.EST_CODIGO" +
                " AND A.VENDA_ID = B.VENDA_ID AND A.VENDA_STATUS = 'F'";
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }


        public bool InserirDadosPreVenda(Pedido dados, bool inserir)
        {
            string strCmd = "";

            if (inserir.Equals(true))
            {
                strCmd = "INSERT INTO PEDIDOS (EMP_CODIGO, EST_CODIGO, PED_NUMERO, PED_COMPL, CF_DOCTO, PED_CNPJ, PED_INSCRICAO, "
                    + "PED_ENDERECO, PED_NUM_ENDERECO, PED_BAIRRO, PED_CEP, PED_CIDADE, PED_UF, PED_FONE, CON_CODIGO, CON_COD_CONV, "
                    + "CON_WEB, PED_TOTAL, PED_SUBTOTAL, PED_DIFER, COL_CODIGO, TAB_CODIGO, LOGIN_ID, PED_DATA,PED_HORA_VENDA, PED_TIPO, PED_NATOPER, "
                    + "PED_NOCOMP, PED_EMISSAO, PED_STATUS, PED_AUXILIAR, MED_CRM, MOVTO_SEQ, PED_NF, PED_SERIENF, PED_NFE_DEVOLUCAO, CPR_NUM) VALUES ("
                    + dados.EmpCodigo + ","
                    + dados.EstCodigo + ","
                    + dados.PedNumero + ",'"
                    + dados.PedComplemento + "','"
                    + dados.CfDocto + "',";
                if (!String.IsNullOrEmpty(dados.PedCnpj))
                {
                    strCmd += "'" + dados.PedCnpj + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedInscricao))
                {
                    strCmd += "'" + dados.PedInscricao + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedEndereco))
                {
                    strCmd += "'" + dados.PedEndereco + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedNumEndereco))
                {
                    strCmd += "'" + dados.PedNumEndereco + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedBairro))
                {
                    strCmd += "'" + dados.PedBairro + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedCep))
                {
                    strCmd +=  Convert.ToDouble(dados.PedCep).ToString() + ",";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedCidade))
                {
                    strCmd += "'" + dados.PedCidade + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedUF))
                {
                    strCmd += "'" + dados.PedUF + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.PedTelefone))
                {
                    strCmd += "'" + dados.PedTelefone + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.ConCodigo))
                {
                    strCmd +=  dados.ConCodigo + ",";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.ConCodConv))
                {
                    strCmd +=  dados.ConCodConv + ",";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.ConWeb))
                {
                    strCmd +=  dados.ConWeb + ",";
                }
                else
                    strCmd += "null,";

                strCmd += Funcoes.BValor(dados.PedTotal) + ","
                    + Funcoes.BValor(dados.PedSubTotal) + ","
                    + Funcoes.BValor(dados.PedDifer) + ","
                    + dados.ColCodigo + ","
                    + dados.TabCodigo + ",'"
                    + dados.LoginId + "',"
                    + Funcoes.BData(dados.PedData) + ","
                    + Funcoes.BDataHora(dados.PedHoraVenda) + ",'"
                    + dados.PedTipo + "','"
                    + dados.PedNatOper + "','"
                    + dados.PedNoComp + "',";
                if (!String.IsNullOrEmpty(dados.PedEmissao))
                {
                    strCmd += Funcoes.BData(Convert.ToDateTime(dados.PedEmissao)) + ",";
                }
                else
                    strCmd += "null,";

                strCmd += dados.PedStatus + ","
                    + dados.PedAuxiliar + ",";

                if (!String.IsNullOrEmpty(dados.MedCrm))
                {
                    strCmd += "'" + dados.MedCrm + "',";
                }
                else
                    strCmd += "null,";

                strCmd += dados.MovtoSeq + ","
                    + dados.PedNF + ",'"
                    + dados.PedSerieNF + "','"
                    + dados.PedNFeDevolucao + "',";

                if (!(dados.CprNum).Equals(0))
                {
                    strCmd += "" + dados.CprNum + ")";
                }
                else
                    strCmd += "null)";

            }
            else
            {
                strCmd = "UPDATE PEDIDOS SET CF_DOCTO = '" + dados.CfDocto + "',";
                if (!String.IsNullOrEmpty(dados.PedCnpj))
                {
                    strCmd += " PED_CNPJ = '" + dados.PedCnpj + "',";
                }
                else
                    strCmd += " PED_CNPJ = null,";

                if (!String.IsNullOrEmpty(dados.PedInscricao))
                {
                    strCmd += " PED_INSCRICAO = '" + dados.PedInscricao + "',";
                }
                else
                    strCmd += " PED_INSCRICAO = null,";

                if (!String.IsNullOrEmpty(dados.PedEndereco))
                {
                    strCmd += " PED_ENDERECO = '" + dados.PedEndereco + "',";
                }
                else
                    strCmd += " PED_ENDERECO = null,";

                if (!String.IsNullOrEmpty(dados.PedNumEndereco))
                {
                    strCmd += " PED_NUM_ENDERECO = '" + dados.PedNumEndereco + "',";
                }
                else
                    strCmd += " PED_NUM_ENDERECO = null,";

                if (!String.IsNullOrEmpty(dados.PedBairro))
                {
                    strCmd += " PED_BAIRRO = '" + dados.PedBairro + "',";
                }
                else
                    strCmd += " PED_BAIRRO = null,";

                if (!String.IsNullOrEmpty(dados.PedCep))
                {
                    strCmd += " PED_CEP = " + Convert.ToDouble(dados.PedCep).ToString() + ",";
                }
                else
                    strCmd += " PED_CEP = null,";

                if (!String.IsNullOrEmpty(dados.PedCidade))
                {
                    strCmd += " PED_CIDADE = '" + dados.PedCidade + "',";
                }
                else
                    strCmd += " PED_CIDADE = null,";

                if (!String.IsNullOrEmpty(dados.PedUF))
                {
                    strCmd += " PED_UF = '" + dados.PedUF + "',";
                }
                else
                    strCmd += " PED_UF = null,";

                if (!String.IsNullOrEmpty(dados.PedTelefone))
                {
                    strCmd += " PED_FONE = '" + dados.PedTelefone + "',";
                }
                else
                    strCmd += " PED_FONE = null,";

                if (!String.IsNullOrEmpty(dados.ConCodigo))
                {
                    strCmd += " CON_CODIGO = " + dados.ConCodigo + ",";
                }
                else
                    strCmd += " CON_CODIGO = null,";

                if (!String.IsNullOrEmpty(dados.ConCodConv))
                {
                    strCmd += " CON_COD_CONV = " + dados.ConCodConv + ",";
                }
                else
                    strCmd += " CON_COD_CONV = null,";

                if (!String.IsNullOrEmpty(dados.ConWeb))
                {
                    strCmd += " CON_WEB = " + dados.ConWeb + ",";
                }
                else
                    strCmd += " CON_WEB = null,";

                strCmd += " PED_TOTAL = " + Funcoes.BValor(dados.PedTotal) + ","
                    + " PED_SUBTOTAL = " + Funcoes.BValor(dados.PedSubTotal) + ","
                    + " PED_DIFER = " + Funcoes.BValor(dados.PedDifer) + ","
                    + " COL_CODIGO = " + dados.ColCodigo + ","
                    + " TAB_CODIGO = " + dados.TabCodigo + ","
                    + " LOGIN_ID = '" + dados.LoginId + "',"
                    + " PED_DATA = " + Funcoes.BData(dados.PedData) + ","
                    + " PED_HORA_VENDA = " + Funcoes.BDataHora(dados.PedHoraVenda) + ","
                    + " PED_TIPO = '" + dados.PedTipo + "',"
                    + " PED_NATOPER = '" + dados.PedNatOper + "',"
                    + " PED_NOCOMP = '" + dados.PedNoComp + "',";
                if (!String.IsNullOrEmpty(dados.PedEmissao))
                {
                    strCmd += " PED_EMISSAO = " + Funcoes.BData(Convert.ToDateTime(dados.PedEmissao)) + ",";
                }
                else
                    strCmd += " PED_EMISSAO = null,";

                strCmd += " PED_STATUS = " + dados.PedStatus + ","
                    + " PED_AUXILIAR = " + dados.PedAuxiliar + ",";

                if (!String.IsNullOrEmpty(dados.MedCrm))
                {
                    strCmd += " MED_CRM = '" + dados.MedCrm + "',";
                }
                else
                    strCmd += " MED_CRM = null,";

                strCmd += " MOVTO_SEQ = " + dados.MovtoSeq + ","
                    + " PED_NF = " + dados.PedNF + ","
                    + " PED_SERIENF = '" + dados.PedSerieNF + "',"
                    + " PED_NFE_DEVOLUCAO = '" + dados.PedNFeDevolucao + "',";
                if (!(dados.CprNum).Equals(0))
                {
                    strCmd += " CPR_NUM = " + dados.CprNum + "";
                }
                else
                    strCmd += " CPR_NUM = null";

                strCmd += " WHERE EMP_CODIGO = " + dados.EmpCodigo + " AND EST_CODIGO = " + dados.EstCodigo + " AND PED_NUMERO = "
                    + dados.PedNumero + " AND PED_COMPL = '" + dados.PedComplemento + "'";
            }

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
