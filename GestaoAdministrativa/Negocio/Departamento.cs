﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class Departamento
    {

        public int EstCodigo { get; set; }
        public int DepCodigo { get; set; }
        public string DepDescr { get; set; }
        public string DepLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Departamento() { }

        public Departamento(int estCodigo, int depCodigo, string depDescr, string depLiberado, DateTime dtAlteracao,
                            string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            // TODO: Complete member initialization
            this.EstCodigo = estCodigo;
            this.DepCodigo = depCodigo;
            this.DepDescr = depDescr;
            this.DepLiberado = depLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaDados(int estCodigo, int depCodigo, string depDescr, string depLiberado, out string strOrdem)
        {
            string strSql = " SELECT EMP_CODIGO, DEP_CODIGO, DEP_DESCR, DAT_ALTERACAO,CASE DEP_DESABILITADO "
                          + " WHEN 'N' THEN 'S' "
                          + " ELSE 'N' END "
                          + " AS DEP_DESABILITADO, "
                          + " OPALTERACAO , DTCADASTRO, OPCADASTRO FROM DEPARTAMENTOS "
                          + "WHERE EMP_CODIGO = " + estCodigo;


            if (depCodigo != 0)
            {
                strSql += " AND DEP_CODIGO = " + depCodigo;
            }
            if (depDescr != "")
            {
                strSql += " AND DEP_DESCR LIKE '%" + depDescr + "%'";
            }
            if (depLiberado != "TODOS")
            {
                depLiberado = depLiberado == "S" ? "N" : "S";
                strSql += " AND DEP_DESABILITADO = '" + depLiberado + "'";
            }

            strOrdem = strSql;
            strSql += " ORDER BY DEP_CODIGO ";


            return BancoDados.GetDataTable(strSql, null);

        }

        public bool InsereRegistros(Departamento dados)
        {
            string strCmd = "INSERT INTO DEPARTAMENTOS(EMP_CODIGO, DEP_CODIGO, DEP_DESCR, DAT_ALTERACAO, DEP_DESABILITADO, OPALTERACAO,  OPCADASTRO, DTCADASTRO) VALUES (" +
                dados.EstCodigo + "," +
                dados.DepCodigo + ", '" +
                dados.DepDescr + "'," +
                Funcoes.BDataHora(dados.DtAlteracao) + ",'" +
                dados.DepLiberado + "','" +
                dados.OpAlteracao + "','" +
                Principal.usuario.ToUpper() + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ")";


            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("DEP_CODIGO", dados.DepCodigo.ToString(), Principal.usuario, "DEPARTAMENTOS", dados.DepDescr, dados.EstCodigo);
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Departamento dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE DEPARTAMENTOS SET "
                       + " DEP_DESCR = '" + dadosNovos.DepDescr + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ""
                       + " ,DEP_DESABILITADO = '" + dadosNovos.DepLiberado + "'"
                       + " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " WHERE DEP_CODIGO = " + dadosNovos.DepCodigo;


            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                if (!dadosNovos.DepDescr.Equals(dadosAntigos.Rows[0]["DEP_DESCR"]))
                {
                    dados[contMatriz, 0] = "DEP_DESCR";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["DEP_DESCR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.DepDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.DepLiberado.Equals(dadosAntigos.Rows[0]["DEP_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "DEP_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["DEP_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.DepLiberado + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("DEP_CODIGO", dadosNovos.DepCodigo.ToString(), Principal.usuario, "DEPARTAMENTOS", dados, contMatriz, dadosNovos.EstCodigo);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExcluirDados(string departamento)
        {

            string sql = "DELETE FROM DEPARTAMENTOS WHERE EMP_CODIGO= " + Principal.empAtual + " AND DEP_CODIGO = " + Math.Truncate(Convert.ToDouble(departamento));

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaTotalVendasPorDepartamento(DateTime dtInicial, DateTime dtFinal)
        {
            string sql = " SELECT COALESCE(SUM(VI.VENDA_ITEM_SUBTOTAL),0) AS SUBTOTAL ,COALESCE(SUM(ABS(VI.VENDA_ITEM_DIFERENCA)),0) AS DESCONTO , "
                       + " COALESCE(SUM(VI.VENDA_ITEM_TOTAL), 0) AS TOTAL, "
                       + " TRIM(DEP.DEP_DESCR) AS DEP_DESCR  FROM VENDAS_ITENS VI "
                       + " INNER JOIN PRODUTOS_DETALHE PRO ON(PRO.PROD_ID = VI.PROD_ID) "
                       + " INNER JOIN VENDAS VEN ON(VEN.VENDA_ID = VI.VENDA_ID) "
                       + " LEFT JOIN DEPARTAMENTOS DEP ON(PRO.DEP_CODIGO = DEP.DEP_CODIGO AND DEP.EMP_CODIGO = PRO.EMP_CODIGO) "
                       + " WHERE VEN.VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND VI.EMP_CODIGO = " + Principal.empAtual 
                       + " AND VI.EST_CODIGO = " + Principal.estAtual 
                       + " AND VI.EMP_CODIGO = PRO.EMP_CODIGO"
                       + " AND VI.EST_CODIGO = PRO.EST_CODIGO"
                       + " AND VI.EMP_CODIGO = VEN.EMP_CODIGO"
                       + " AND VI.EST_CODIGO = VEN.EST_CODIGO AND VEN.VENDA_STATUS = 'F'"
                       + " GROUP BY DEP.DEP_DESCR "
                       + " ORDER BY TRIM(DEP.DEP_DESCR) ";

            return BancoDados.GetDataTable(sql, null);
        }
        public DataTable BuscaDepartamento(int codigoEmp, char desabilitado)
        {
            string sql = " SELECT DEP_CODIGO, DEP_DESCR FROM DEPARTAMENTOS WHERE EMP_CODIGO = " + codigoEmp
                       + " AND DEP_DESABILITADO ='" + desabilitado + "'";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}