﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FormasPagamento
    {
        public int EmpCodigo { get; set; }
        public int FormaID { get; set; }
        public string FormaDescricao { get; set; }
        public string VenctoDiaFixo { get; set; }
        public int QtdeParcelas { get; set; }
        public int DiaVenctoFixo { get; set; }
        public int QtdeVias { get; set; }
        public string OperacaoCaixa { get; set; }
        public string ConvBeneficio { get; set; }
        public string FormaLiberado { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }

        public FormasPagamento() { }

        public FormasPagamento(int empCodigo, int formaID, string formaDescricao, string venctoDiaFixo, int qtdeParcelas, int diaVenctoFixo, int qtdeVias, string operacaoCaixa,
            string convBeneficio, string formaLiberado, string opAlteracao, DateTime dtAlteracao, string opCadastro, DateTime dtCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.FormaID = formaID;
            this.FormaDescricao = formaDescricao;
            this.VenctoDiaFixo = venctoDiaFixo;
            this.QtdeParcelas = qtdeParcelas;
            this.DiaVenctoFixo = diaVenctoFixo;
            this.QtdeVias = qtdeVias;
            this.OperacaoCaixa = operacaoCaixa;
            this.ConvBeneficio = convBeneficio;
            this.FormaLiberado = formaLiberado;
            this.OpAlteracao = opAlteracao;
            this.DtAlteracao = dtAlteracao;
            this.OpCadastro = opCadastro;
            this.DtCadastro = dtCadastro;
        }

        public DataTable BuscaDados(int empCodigo, int formaId, string formaDescricao, string formaLiberado, out string strOrdem)
        {
            string strSql = "SELECT A.EMP_CODIGO, A.FORMA_ID, A.FORMA_DESCRICAO, A.VENCTO_DIA_FIXO, A.QTDE_PARCELAS, A.DIA_VENCTO_FIXO, "
                + " A.QTDE_VIAS, A.OPERACAO_CAIXA, A.CONV_BENEFICIO, A.FORMA_LIBERADO, A.OP_ALTERACAO, A.DT_ALTERACAO,"
                + " A.OP_CADASTRO, A.DT_CADASTRO "
                + " FROM FORMAS_PAGAMENTO A  WHERE A.EMP_CODIGO = " + empCodigo;
            //BUSCA SEM NENHUM FILTRO//
            if (formaId == 0 && formaDescricao == "" && formaLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.FORMA_ID";
            }
            else
            {
                if (formaId != 0)
                {
                    strSql += " AND A.FORMA_ID = " + formaId;
                }
                if (formaDescricao != "")
                {
                    strSql += " AND A.FORMA_DESCRICAO LIKE '%" + formaDescricao + "%'";
                }
                if (formaLiberado != "TODOS")
                {
                    strSql += " AND A.FORMA_LIBERADO = '" + formaLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.FORMA_ID";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(FormasPagamento dados)
        {
            string strCmd = "INSERT INTO FORMAS_PAGAMENTO (EMP_CODIGO, FORMA_ID, FORMA_DESCRICAO, VENCTO_DIA_FIXO, QTDE_PARCELAS, DIA_VENCTO_FIXO, "
                + "QTDE_VIAS, OPERACAO_CAIXA, CONV_BENEFICIO, FORMA_LIBERADO, OP_CADASTRO, DT_CADASTRO) VALUES("
                + dados.EmpCodigo + ","
                + dados.FormaID + ",'"
                + dados.FormaDescricao + "','"
                + dados.VenctoDiaFixo + "',"
                + dados.QtdeParcelas + ","
                + dados.DiaVenctoFixo + ","
                + dados.QtdeVias + ",'"
                + dados.OperacaoCaixa + "','"
                + dados.ConvBeneficio + "','"
                + dados.FormaLiberado + "','"
                + Principal.usuario + "',"
                + Funcoes.BDataHora(DateTime.Now) + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(FormasPagamento dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[10, 3];
            int contMatriz = 0;
            string strCmd = " UPDATE FORMAS_PAGAMENTO SET ";
            if (!dadosNew.FormaDescricao.Equals(dadosOld.Rows[0]["FORMA_DESCRICAO"].ToString()))
            {
                strCmd += " FORMA_DESCRICAO = '" + dadosNew.FormaDescricao + "',";

                dados[contMatriz, 0] = "FORMA_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["FORMA_DESCRICAO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.FormaDescricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.VenctoDiaFixo.Equals(dadosOld.Rows[0]["VENCTO_DIA_FIXO"].ToString()))
            {
                strCmd += " VENCTO_DIA_FIXO = '" + dadosNew.VenctoDiaFixo + "',";

                dados[contMatriz, 0] = "VENCTO_DIA_FIXO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["VENCTO_DIA_FIXO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.VenctoDiaFixo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.QtdeParcelas.Equals(Convert.ToInt32(dadosOld.Rows[0]["QTDE_PARCELAS"])))
            {
                strCmd += " QTDE_PARCELAS = " + dadosNew.QtdeParcelas + ",";

                dados[contMatriz, 0] = "QTDE_PARCELAS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["QTDE_PARCELAS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.QtdeParcelas + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DiaVenctoFixo.Equals(Convert.ToInt32(dadosOld.Rows[0]["DIA_VENCTO_FIXO"])))
            {
                strCmd += " DIA_VENCTO_FIXO = " + dadosNew.DiaVenctoFixo + ",";

                dados[contMatriz, 0] = "DIA_VENCTO_FIXO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DIA_VENCTO_FIXO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DiaVenctoFixo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.QtdeVias.Equals(Convert.ToInt32(dadosOld.Rows[0]["QTDE_VIAS"])))
            {
                strCmd += " QTDE_VIAS = " + dadosNew.QtdeVias + ",";

                dados[contMatriz, 0] = "QTDE_VIAS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["QTDE_VIAS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.QtdeVias + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.OperacaoCaixa.Equals(dadosOld.Rows[0]["OPERACAO_CAIXA"].ToString()))
            {
                strCmd += " OPERACAO_CAIXA = '" + dadosNew.OperacaoCaixa + "',";

                dados[contMatriz, 0] = "OPERACAO_CAIXA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["OPERACAO_CAIXA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.OperacaoCaixa + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConvBeneficio.Equals(dadosOld.Rows[0]["CONV_BENEFICIO"].ToString()))
            {
                strCmd += " CONV_BENEFICIO = '" + dadosNew.ConvBeneficio + "',";

                dados[contMatriz, 0] = "CONV_BENEFICIO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CONV_BENEFICIO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConvBeneficio + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.FormaLiberado.Equals(dadosOld.Rows[0]["FORMA_LIBERADO"].ToString()))
            {
                strCmd += " FORMA_LIBERADO = '" + dadosNew.FormaLiberado + "',";

                dados[contMatriz, 0] = "FORMA_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["FORMA_LIBERADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.FormaLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OP_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OP_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OP_ALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OP_ALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and FORMA_ID = " + dadosNew.FormaID;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("FORMA_ID", dadosNew.FormaID.ToString(), Principal.usuario, "FORMAS_PAGAMENTO", dados, contMatriz, Principal.estAtual, dadosNew.EmpCodigo);
                    return true;
                }
                else
                    return false;
            }
            return true;

        }

        public int ExcluirDados(int empCodigo, string cprCodigo)
        {
            string strCmd = "DELETE FROM FORMAS_PAGAMENTO WHERE EMP_CODIGO =" + empCodigo + " AND FORMA_ID = " + cprCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable DadosDaFormaDePagamento(int empCodigo, int formaID)
        {
            string strSql = "SELECT * FROM FORMAS_PAGAMENTO WHERE EMP_CODIGO = " + empCodigo + " AND FORMA_ID = " + formaID;

            return BancoDados.GetDataTable(strSql, null);
        }

        public string IdentificaFormaDePagamento(int formaID, int empCodigo)
        {
            string strSql = "SELECT FORMA_DESCRICAO FROM FORMAS_PAGAMENTO WHERE FORMA_ID = " + formaID + "  AND EMP_CODIGO = " + empCodigo;


            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public DataTable VendasPorFormaDePagamento(DateTime dtInicial, DateTime dtFinal)
        {
            string sql = "SELECT SUM(A.VENDA_VALOR_PARCELA) AS TOTAL, B.FORMA_DESCRICAO"
                        + "        FROM VENDAS_FORMA_PAGAMENTO A"
                        + "        INNER JOIN FORMAS_PAGAMENTO B ON A.VENDA_FORMA_ID = B.FORMA_ID"
                        + "        INNER JOIN VENDAS C ON A.VENDA_ID = C.VENDA_ID"
                        + "        WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "        AND A.EST_CODIGO = " + Principal.estAtual
                        + "        AND A.EMP_CODIGO = C.EMP_CODIGO"
                        + "        AND A.EST_CODIGO = C.EST_CODIGO AND C.VENDA_STATUS = 'F'"
                        + "        AND C.VENDA_EMISSAO BETWEEN "
                        + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                        + "               GROUP BY B.FORMA_DESCRICAO";
            
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
