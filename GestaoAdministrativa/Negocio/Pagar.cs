﻿using System;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class Pagar
    {
        public int EmpCodigo { get; set; }
        public int PagCodigo { get; set; }
        public string CfDocto { get; set; }
        public string PagDocto { get; set; }
        public DateTime PagData { get; set; }
        public double PagValorPrincipal { get; set; }
        public int PagComprador { get; set; }
        public string LoginId { get; set; }
        public int TipoCodigo { get; set; }
        public int BancoCodigo { get; set; }
        public string PagListou { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public int DespCodigo { get; set; }
        public string PagOrigem { get; set; }
        public string PagStatus { get; set; }
        public int ID { get; set; }
        public string Historico { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public Pagar() { }

        public string IdentificaFaturaCadastradaPorDoctoFornecedor(Pagar dadosBusca)
        {
            string strSql = "SELECT PAG_CODIGO FROM PAGAR WHERE EMP_CODIGO = " + dadosBusca.EmpCodigo + " AND PAG_DOCTO = '" + dadosBusca.PagDocto
                + "' AND CF_DOCTO = '" + dadosBusca.CfDocto + "'";
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public bool InsereRegistroFatura(Pagar dadosPagar)
        {
            string strCmd = "INSERT INTO PAGAR (EMP_CODIGO, PAG_CODIGO, CF_DOCTO, PAG_DOCTO, PAG_DATA, PAG_VALPRIN, PAG_COMPRADOR, TIP_CODIGO, "
                + "PAG_LISTOU, DTCADASTRO, OPCADASTRO, LOGIN_ID, PAG_ORIGEM, PAG_STATUS, DESP_CODIGO, HISTORICO";
            if(dadosPagar.ID != 0)
            {
                strCmd += " ,ID";
            }
            strCmd += ") VALUES (" + dadosPagar.EmpCodigo + ","
                + dadosPagar.PagCodigo + ",'"
                + dadosPagar.CfDocto + "','"
                + dadosPagar.PagDocto + "',"
                + Funcoes.BData(dadosPagar.PagData) + ","
                + Funcoes.BValor(dadosPagar.PagValorPrincipal) + ","
                + dadosPagar.PagComprador + ","
                + dadosPagar.TipoCodigo + ",'"
                + dadosPagar.PagListou + "',"
                + Funcoes.BDataHora(dadosPagar.DtCadastro) + ",'"
                + dadosPagar.OpCadastro + "','"
                + dadosPagar.OpCadastro + "','"
                + dadosPagar.PagOrigem + "','"
                + dadosPagar.PagStatus + "',"
                + dadosPagar.DespCodigo + ",'"
                + dadosPagar.Historico + "'";
            if(dadosPagar.ID != 0)
            {
                strCmd += "," + dadosPagar.ID;
            }
            strCmd += ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }


        public int ExcluiFatura(int empCodigo, string identificador, bool porID)
        {
            if(porID.Equals(true))
                return BancoDados.ExecuteNoQueryTrans("DELETE FROM PAGAR WHERE EMP_CODIGO = " + empCodigo + " AND PAG_CODIGO = " + identificador, null);
            else
                return BancoDados.ExecuteNoQueryTrans("DELETE FROM PAGAR WHERE EMP_CODIGO = " + empCodigo + " AND PAG_DOCTO = '" + identificador + "'", null);
        }


        public DataTable BuscarDados(Pagar dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.PAG_CODIGO, A.PAG_DATA, A.PAG_VALPRIN, D.TIP_DESC_ABREV, A.PAG_DOCTO, B.DESP_DESCRICAO, A.CF_DOCTO, C.CF_NOME, A.HISTORICO, "
                + "CASE A.PAG_STATUS WHEN 'N' THEN 'ABERTO' ELSE CASE A.PAG_STATUS WHEN 'P' THEN 'PARCIAL' ELSE 'QUITADO' END END AS PAG_STATUS, "
                + "A.DAT_ALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO "
                + "FROM PAGAR A "
                + "INNER JOIN CAD_DESPESAS B ON B.DESP_CODIGO = A.DESP_CODIGO "
                + "INNER JOIN CLIFOR C ON C.CF_DOCTO = A.CF_DOCTO "
                + "INNER JOIN TIPO_DOCTO D ON D.TIP_CODIGO = A.TIP_CODIGO "
                + "WHERE A.PAG_ORIGEM = 'D' AND A.EMP_CODIGO = " + dadosBusca.EmpCodigo;

            if (dadosBusca.PagCodigo == 0 && dadosBusca.PagData.ToString("dd/MM/yyyy") == "01/01/2001" && dadosBusca.DespCodigo == 0 && dadosBusca.PagDocto == "")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            }
            else
            {
                if (dadosBusca.PagCodigo != 0)
                {
                    strSql += " AND A.PAG_CODIGO = " + dadosBusca.PagCodigo;
                }
                if (dadosBusca.DespCodigo != 0)
                {
                    strSql += " AND A.DESP_CODIGO = " + dadosBusca.DespCodigo;
                }
                if (dadosBusca.PagData.ToString("dd/MM/yyyy") != "01/01/2001")
                {
                    strSql += " AND A.PAG_DATA = " + Funcoes.BData(dadosBusca.PagData);
                }
                if (dadosBusca.PagDocto != "")
                {
                    strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
                }

                strOrdem = strSql;

                strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluiParcPagar(int empCodigo, string pagCodigo)
        {
            BancoDados.AbrirTrans();

            string strCmd = "DELETE FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + empCodigo + " AND PAG_CODIGO = " + pagCodigo;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                strCmd = "DELETE FROM PAGAR WHERE EMP_CODIGO = " + empCodigo + " AND PAG_CODIGO = " + pagCodigo;
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    BancoDados.FecharTrans();
                    return 1;
                }
            }

            BancoDados.ErroTrans();
            return 0;
        }

        public DataTable ListarContasPagar(Pagar dadosBusca, string origem, string dtIni, string dtFinal, int empCodigo)
        {
            string strSql =  " SELECT COALESCE(B.PAG_ORIGEM, 'E') AS PAG_ORIGEM,"
                    + "           COALESCE(B.PAG_STATUS,'N') AS PAG_STATUS,"
                    + "           A.PD_VENCTO,"
                    + "           B.PAG_DOCTO,"
                    + "           A.PD_PARCELA,"
                    + "           A.PD_VALOR,"
                    + "           A.PD_SALDO,"
                    + "           B.PAG_DATA,"
                    + "           COALESCE(C.CF_NOME,'FORNECEDOR NÃO CADASTRADO') AS CF_NOME,"
                    + "           A.CF_DOCTO,"
                    + "           A.PAG_CODIGO,"
                    + "           A.PD_STATUS,"
                    + "           C.CF_ID, COALESCE(B.ID,0) AS ID"
                    + "      FROM PAGAR_DETALHE A"
                    + "     INNER JOIN PAGAR B ON(A.PAG_CODIGO = B.PAG_CODIGO AND"
                    + "                           A.EMP_CODIGO = B.EMP_CODIGO)"
                    + "     LEFT JOIN CLIFOR C ON A.CF_DOCTO = C.CF_DOCTO"
                    + "     WHERE A.EMP_CODIGO = " + empCodigo;

            if(origem != "T")
            {
                strSql += " AND B.PAG_ORIGEM = '" + origem + "'";
            }

            if (!String.IsNullOrEmpty(dadosBusca.PagDocto))
            {
                strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
            }

            if (!String.IsNullOrEmpty(dadosBusca.CfDocto))
            {
                strSql += " AND C.CF_NOME LIKE '" + dadosBusca.CfDocto + "%'";
            }

            if (!dadosBusca.PagStatus.Equals("T"))
            {
                if (dadosBusca.PagStatus.Equals("N"))
                {
                    strSql += " AND A.PD_STATUS = 'N'";
                }
                else if (dadosBusca.PagStatus.Equals("Q"))
                {
                    strSql += " AND A.PD_STATUS = 'Q'";
                }
            }
            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)) && !String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
            {
                Principal.strPesq += " AND A.PD_VENCTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtIni)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            }

            strSql += " ORDER BY A.PD_VENCTO, C.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);

        }

        public  int QtdeParcelas(Pagar dadosBusca, bool status)
        {
            string strSql = "SELECT COUNT(*) AS QTDE FROM PAGAR_DETALHE WHERE PAG_CODIGO = " + dadosBusca.PagCodigo
                         + " AND EMP_CODIGO = " + dadosBusca.EmpCodigo;

            if (status == true)
            {
                strSql += " AND PD_STATUS = '" + dadosBusca.PagStatus + "'";
            }

            return Convert.ToInt32(BancoDados.ExecuteScalar(strSql, null));
        }

        public bool AtualizaStatusPagto(int empCodigo, int pagCodigo, string status, string statusAnt)
        {
            string strCmd = "UPDATE PAGAR SET PAG_STATUS = '" + status + "', DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = '" + Principal.usuario
                + "' WHERE EMP_CODIGO =" + empCodigo + " and PAG_CODIGO = " + pagCodigo;

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null) != -1)
            {
                strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EMP_CODIGO)"
                       + " VALUES ('PAG_STATUS','" + pagCodigo + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PAGAR', 'PAG_STATUS','"
                       + statusAnt + "','" + status + "'," + empCodigo + ")";
                if (BancoDados.ExecuteNoQueryTrans(strCmd, null) == -1)
                {
                    return false;
                }

                return true;
            }
            else
                return false;
        }
    }
}
