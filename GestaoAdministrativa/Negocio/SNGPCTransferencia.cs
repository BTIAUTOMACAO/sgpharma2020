﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SNGPCTransferencia
    {

        public DateTime BuscaDataUltimoEnvio(int tecId, string aceito)
        {

            string sql = " SELECT  NVL(MAX(DATA_FINAL),'01/01/2001') AS DATA_FINAL FROM SNGPC_ENVIOS "
                       + " WHERE ACEITO = '" + aceito + "' AND TEC_CODIGO = " + tecId;

            return (DateTime)BancoDados.ExecuteScalar(sql, null);
        }

        public DataTable BuscaProdutos(int tipoEntrada, DateTime dtInicial, DateTime dtFinal)
        {

            string sql = " SELECT ENT.PROD_CODIGO, ENT.ENT_MED_LOTE, ENT.ENT_MED_QTDE, UPPER(PRO.PROD_UNIDADE) AS PROD_UNIDADE, "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' WHEN '2' THEN  'SUJEITO A CONTROLE ESPECIAL' ELSE '' END AS PROD_CLASSE_TERAP, "
                       + " PRO.PROD_REGISTRO_MS, PRO.PROD_DESCR "
                       + " FROM SNGPC_ENTRADAS ENT "
                       + " INNER JOIN PRODUTOS PRO ON(ENT.PROD_CODIGO = PRO.PROD_CODIGO) "
                       + " WHERE  ENT.EST_CODIGO = " + Principal.estAtual
                       + " AND ENT_TIPO_OPER = " + tipoEntrada
                       + " AND ENT.ENT_DATA BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal)
                       + " AND ENT.EMP_CODIGO = " + Principal.empAtual;

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaPorStatus(string status)
        {
            string sql = " SELECT HASH_RETORNO, ACEITO , TEC.TEC_LOGIN, TEC.TEC_SENHA , EST.EST_CGC, MENSAGEMVALIDACAO FROM SNGPC_ENVIOS ENV "
                       + " INNER JOIN SNGPC_TECNICOS TEC ON(TEC.TEC_CODIGO = ENV.TEC_CODIGO) "
                       + " INNER JOIN ESTABELECIMENTOS EST ON(EST.EMP_CODIGO = ENV.EST_CODIGO) ";

            if (status != "TODOS")
            {
                sql += "WHERE ACEITO = '" + status + "'";
            }

            sql += " ORDER BY ENV.DATA_RECEBIMENTO";
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaEntrada(string dtInicial, string dtFinal)
        {
            string sql = " SELECT ENT_DOCTO, ENT_CNPJ, ENT_DATA_NF, ENT.PROD_CODIGO, ENT.ENT_MED_LOTE, ENT.ENT_MED_QTDE, ENT.ENT_DATA,"
                       + " TAB.TAB_CODIGO, "
                       + " PRO.PROD_CLASSE_TERAP, "
                       + " ENT_MS AS PROD_REGISTRO_MS, "
                       + " UPPER(PRO.PROD_UNIDADE) AS PROD_UNIDADE "
                       + " FROM SNGPC_ENTRADAS ENT "
                       + " INNER JOIN SNGPC_TABELAS TAB ON (TAB.TAB_SEQ = ENT.ENT_TIPO_OPER) "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = ENT.PROD_CODIGO)"
                       + " WHERE ENT_DATA BETWEEN  TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') "
                       + " AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                       + " AND ENT.EMP_CODIGO = " + Principal.empAtual + " AND ENT.EST_CODIGO = " + Principal.estAtual;

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaVendasPorPeriodo(string dtInicial, string dtFinal)
        {
            string sql = " SELECT REC.TAB_CODIGO AS TIPO_RECEITA, VEN.VEN_NUM_NOT, VEN.VEN_PRESC_DT, VEN.VEN_PRESC_NOME, VEN.VEN_PRESC_NUMERO, "
                       + " COS.TAB_CODIGO AS CONSELHO, UF.TAB_CODIGO AS UF, "
                       + " UPPER(VEN.VEN_PAC_NOME) AS VEN_PAC_NOME, VEN.VEN_PAC_IDADE, UN.TAB_CODIGO AS UNIDADE, SEXO.TAB_CODIGO AS SEXO, "
                       + " VEN.VEN_MED_LOTE, VEN.VEN_MED_LOTE, UPPER(PROD.PROD_USO_CONTINUO) AS PROD_USO_CONTINUO, UPPER(PROD.PROD_UNIDADE)AS PROD_UNIDADE, "
                       + " VEN.VEN_MS AS PROD_REGISTRO_MS, VEN.VEN_DATA, VEN.VEN_MED_QTDE, VEN.VEN_USO, PROD.PROD_CLASSE_TERAP,"
                       + " VEN.VEN_COMP_NOME, VEN.VEN_COMP_TD, VEN.VEN_COMP_DOCTO, VEN.VEN_COMP_OE, VEN.VEN_COMP_UF"
                       + " FROM SNGPC_VENDAS VEN "
                       + " INNER JOIN SNGPC_TABELAS REC ON(VEN.VEN_TP_RECEITA = REC.TAB_SEQ) "
                       + " INNER JOIN SNGPC_TABELAS COS ON(VEN.VEN_PRESC_CP = COS.TAB_SEQ) "
                       + " INNER JOIN SNGPC_TABELAS UF ON(VEN.VEN_PRESC_UF = UF.TAB_SEQ) "
                       + " INNER JOIN SNGPC_TABELAS UN ON(VEN.VEN_PAC_UNIDADE = UN.TAB_SEQ) "
                       + " INNER JOIN SNGPC_TABELAS SEXO ON(VEN.VEN_PAC_SEXO = SEXO.TAB_SEQ) "
                       + " INNER JOIN PRODUTOS PROD ON(VEN.PROD_CODIGO = PROD.PROD_CODIGO) "
                       + " WHERE VEN_DATA BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') "
                       + " AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                       + " AND VEN.EMP_CODIGO = " + Principal.empAtual + " AND VEN.EST_CODIGO = " + Principal.estAtual;

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaProduto(string codigoBarras, string campo, string valorNovo)
        {

            if (campo == "PROD_CLASSE_TERAP") {
                valorNovo = valorNovo == "ANTIMICROBIANO" ? "1" : "2";
            }

            string sql = " UPDATE PRODUTOS  SET "
                       + campo + "= '" + valorNovo + "'"
                       + " WHERE PROD_CODIGO = '" + codigoBarras + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
