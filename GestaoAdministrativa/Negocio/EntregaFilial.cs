﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class EntregaFilial
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int Codigo { get; set; }
        public int ID { get; set; }
        public string Solicitante { get; set; }
        public int LojaOrigem { get; set; }
        public int LojaDestino { get; set; }
        public string CfDocto { get; set; }
        public string CfNome { get; set; }
        public string CfEndereco { get; set; }
        public string CfNumero { get; set; }
        public string CfBairro { get; set; }
        public string CfCidade { get; set; }
        public string CfTelefone { get; set; }
        public double Troco { get; set; }
        public double TotalEntrega { get; set; }
        public string Observacao { get; set; }
        public string Status { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
        public string NomeFilial { get; set; }

        public EntregaFilial() { }

        public bool InsereRegistros(EntregaFilial dados)
        {
            string strCmd = "INSERT INTO ENTREGA_FILIAL(EMP_CODIGO,EST_CODIGO, CODIGO, SOLICITANTE, LOJA_ORIGEM, LOJA_DESTINO, CF_DOCTO, CF_ENDERECO, CF_NUMERO,CF_BAIRRO,CF_CIDADE,CF_TELEFONE,"
                + "TROCO,TOTAL_ENTREGA,OBSERVACAO,OPCADASTRO,DTCADASTRO,STATUS) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.Codigo + ",'" +
                dados.Solicitante + "'," +
                dados.LojaOrigem + "," +
                dados.LojaDestino + ",'" +
                dados.CfDocto + "','" + 
                dados.CfEndereco + "','" + 
                dados.CfNumero + "','" + 
                dados.CfBairro + "','" + 
                dados.CfCidade + "','" + 
                dados.CfTelefone + "'," + 
                Funcoes.BValor(dados.Troco) + "," + 
                Funcoes.BValor(dados.TotalEntrega) + ",'" + 
                dados.Observacao + "','" + 
                dados.OpCadastro + "'," + 
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.Status + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
