﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    class PedidosParcelas
    {
        public int est_codigo { get; set; }
        public long ped_codigo { get; set; }
        public int crp_codigo { get; set; }
        public int parc_parcela { get; set; }
        public DateTime parc_vencto { get; set; }
        public decimal parc_valor { get; set; }

        public PedidosParcelas() { }

        public static bool InserirDados(PedidosParcelas dados)
        {
            MontadorSql mont = new MontadorSql("pedidos_parcelas", MontadorType.Insert);
            mont.AddField("est_codigo", dados.est_codigo);
            mont.AddField("ped_codigo", dados.ped_codigo);
            mont.AddField("cpr_codigo", dados.crp_codigo);
            mont.AddField("parc_parcela", dados.parc_parcela);
            mont.AddField("parc_vencto", dados.parc_vencto.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("parc_valor", dados.parc_valor);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
