﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioVidaLink
    {
        public string NSU { get; set; }
        public long VendaID { get; set; }
        public string PlanoConvenio { get; set; }
        public string CartaoConvenio { get; set; }
        public string Nome { get; set; }
        public int TipoAutorizacao { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public string Estacao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public BeneficioVidaLink() { }

        public BeneficioVidaLink(string nsu, long vendaId, string planoConvenio, string cartaoConvenio, string nome, int tipoAutorizacao, DateTime data, string status, string estacao, DateTime dtCadastro,
            string opCadastro)
        {
            this.NSU = nsu;
            this.VendaID = vendaId;
            this.PlanoConvenio = planoConvenio;
            this.CartaoConvenio = cartaoConvenio;
            this.Nome = nome;
            this.TipoAutorizacao = tipoAutorizacao;
            this.Data = data;
            this.Status = status;
            this.Estacao = estacao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public string IdentificaVendaVidaLink(string nsu, long vendaID = 0)
        {
            string strSql = "SELECT NSU FROM BENEFICIO_VIDA_LINK WHERE NSU = '" + nsu + "' AND DATA < " + Funcoes.BData(DateTime.Now);
            if (vendaID != 0)
            {
                strSql += " AND VENDA_ID = " + vendaID;
            }
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID = 0)
        {
            string strCmd = "DELETE FROM BENEFICIO_VIDA_LINK WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public string IdentificaVendaVidaLinkPorStatusENsu(string nsu, string status)
        {
            string strSql = "SELECT ESTACAO FROM BENEFICIO_VIDA_LINK WHERE NSU = '" + nsu + "' AND STATUS = '" + status + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public bool InsereRegistros(BeneficioVidaLink dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_VIDA_LINK(NSU, PLANO_CONVENIO, CARTAO_CONVENIO, NOME, TIPO_AUTORIZACAO, DATA, STATUS, ESTACAO, DTCADASTRO, OPCADASTRO) VALUES ('" 
                + dados.NSU + "','"
                + dados.PlanoConvenio + "','"
                + dados.CartaoConvenio + "','"
                + dados.Nome + "',"
                + dados.TipoAutorizacao + ","
                + Funcoes.BData(dados.Data) + ",'"
                + dados.Status + "','"
                + dados.Estacao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'" 
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDadosDaVendaPorNsu(string nsu)
        {
            string strSql;

            strSql = "SELECT A.NSU,B.PLANO_CONVENIO, B.CARTAO_CONVENIO, B.TIPO_AUTORIZACAO,"
                    + " A.PR_MAX, A.PR_VENDA, A.PR_CLIENTE_AVISTA, A.PR_CLIENTE_ARECEB,"
                    + " A.VL_SUBSIDIO, A.VL_REEMBOLSO, PORCENT_DESC, A.COMISSAO_VLINK, A.QTD"
                    + " FROM BENEFICIO_VIDA_LINK_PRODUTOS A"
                    + " INNER JOIN BENEFICIO_VIDA_LINK B ON A.NSU = B.NSU"
                    + " WHERE A.NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaVendaIdPorNsu(string nsu, long vendaID)
        {
            string strCmd = "UPDATE BENEFICIO_VIDA_LINK SET VENDA_ID = " + vendaID + " WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaStatusPorNsu(string nsu, string status)
        {
            string strCmd = "UPDATE BENEFICIO_VIDA_LINK SET STATUS = '" + status + "' WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
