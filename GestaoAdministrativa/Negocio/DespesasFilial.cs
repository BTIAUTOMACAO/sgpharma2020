﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class DespesasFilial
    {
        public string PagOrigem { get; set; }
        public string PagDocto { get; set; }
        public string CfDocto { get; set; }
        public double PdSaldo { get; set; }
        public DateTime PdVencimento { get; set; }
        public int PdParcela { get; set; }
        public string PdStatus { get; set; }
        public string Loja { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
