﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ClienteContinuo
    {
        public string CfDocto { get; set; }
        public string ProdCodigo { get; set; }
        public int ContQtdeDia { get; set; }
        public int ContQtdeCompra { get; set; }
        public DateTime ContDataCompra { get; set; }
        public DateTime ContDataProximaCompra { get; set; }

        public ClienteContinuo() { }

        public ClienteContinuo(string cfDocto, string prodCodigo, int contQtdeDia, int contQtdeCompra, DateTime contDataCompra, DateTime contDataProximaCompra)
        {
            this.CfDocto = cfDocto;
            this.ProdCodigo = prodCodigo;
            this.ContQtdeDia = contQtdeDia;
            this.ContQtdeCompra = contQtdeCompra;
            this.ContDataCompra = contDataCompra;
            this.ContDataProximaCompra = contDataProximaCompra;
        }

        public DataTable IdentificaClienteXProduto(string prodCodigo, string cfDocto)
        {
            string strSql = "SELECT * FROM CLIENTES_CONTINUO " +
                 " WHERE PROD_CODIGO = '" + prodCodigo + "'" +
                 " AND CF_DOCTO = '" + cfDocto + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(ClienteContinuo dados)
        {
            string strCmd = "INSERT INTO CLIENTES_CONTINUO(CF_DOCTO, PROD_CODIGO, CONT_QTDE_DIA, CONT_QTDE_COMPRA, CONT_DT_COMPRA) VALUES ('" +
                dados.CfDocto + "','" +
                dados.ProdCodigo + "'," +
                dados.ContQtdeDia + "," +
                dados.ContQtdeCompra + "," +
                Funcoes.BDataHora(dados.ContDataCompra) + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(DateTime contDataCompra, int qtdeCompra, DateTime contDataProximaCompra, string cfDocto, string prodCodigo)
        {
            string strCmd = "UPDATE CLIENTES_CONTINUO SET CONT_DT_COMPRA = " + Funcoes.BData(contDataCompra) + ", CONT_QTDE_COMPRA = "
                + qtdeCompra + ", CONT_DT_PROXIMA " + Funcoes.BData(contDataProximaCompra) + " WHERE CF_DOCTO = '" + cfDocto
                + "' AND PROD_CODIGO = '" + ProdCodigo;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
