﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class VendasDados
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaID { get; set; }
        public int VendaCfID { get; set; }
        public DateTime VendaData { get; set; }
        public string VendaEmissao { get; set; }
        public DateTime VendaDataHora { get; set; }
        public int VendaColCodigo { get; set; }
        public double VendaTotal { get; set; }
        public double VendaSubTotal { get; set; }
        public double VendaDiferenca { get; set; }
        public int VendaQtde { get; set; }
        public string VendaStatus { get; set; }
        public int VendaComanda { get; set; }
        public string VendaCartao { get; set; }
        public string VendaAutorizacao { get; set; }
        public string VendaAutorizacaoReceita { get; set; }
        public string VendaNomeCartao { get; set; }
        public string VendaConCodigo { get; set; }
        public string VendaEmitido { get; set; }
        public string VendaNumNota { get; set; }
        public string VendaEntrega { get; set; }
        public int VendaFinanceiroSeq { get; set; }
        public string VendaNfDevolucao { get; set; }
        public string VendaMedico { get; set; }
        public string VendaNumReceita { get; set; }
        public string VendaTransID { get; set; }
        public string VendaTransIDReceita { get; set; }
        public string VendaCartaoCodigo { get; set; }
        public String VendaBeneficio { get; set; }
        public double VendaAjusteGeral { get; set; }
        public int VendaConvenioParcela { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string VendaObrigaReceita { get; set; }

        public VendasDados() { }

        public bool InserirDadosPreVenda(VendasDados dados, bool inserir)
        {
            string strCmd = "";

            if (inserir.Equals(true))
            {
                strCmd = "INSERT INTO VENDAS (EMP_CODIGO, EST_CODIGO, VENDA_ID, VENDA_CF_ID,VENDA_DATA,VENDA_EMISSAO,VENDA_DATA_HORA,VENDA_COL_CODIGO,VENDA_TOTAL,"
                    + "VENDA_SUBTOTAL,VENDA_DIFERENCA,VENDA_QTDE,VENDA_STATUS,VENDA_COMANDA,VENDA_CON_CODIGO,VENDA_ENTREGA,VENDA_NF_DEVOLUCAO,"
                    + "VENDA_MEDICO,VENDA_NUM_RECEITA, VENDA_BENEFICIO, VENDA_AJUSTE_GERAL, VENDA_OBRIGA_RECEITA, OP_CADASTRO,DT_CADASTRO) VALUES ("
                    + dados.EmpCodigo + ","
                    + dados.EstCodigo + ","
                    + dados.VendaID + ","
                    + dados.VendaCfID + ","
                    + Funcoes.BData(dados.VendaData) + ",";
                if (!String.IsNullOrEmpty(dados.VendaEmissao))
                {
                    strCmd += Funcoes.BData(Convert.ToDateTime(dados.VendaEmissao)) + ",";
                }
                else
                    strCmd += "null,";

                strCmd += Funcoes.BDataHora(dados.VendaDataHora) + ","
                    + dados.VendaColCodigo + ","
                    + Funcoes.BValor(dados.VendaTotal) + ","
                    + Funcoes.BValor(dados.VendaSubTotal) + ","
                    + Funcoes.BValor(dados.VendaDiferenca) + ","
                    + dados.VendaQtde + ",'"
                    + dados.VendaStatus + "',"
                    + dados.VendaComanda + ",";

                if (!String.IsNullOrEmpty(dados.VendaConCodigo))
                {
                    strCmd += dados.VendaConCodigo + ",'";
                }
                else
                    strCmd += "null,'";

                strCmd += dados.VendaEntrega + "','"
                    + dados.VendaNfDevolucao + "',";

                if (!String.IsNullOrEmpty(dados.VendaMedico))
                {
                    strCmd += "'" + dados.VendaMedico + "',";
                }
                else
                    strCmd += "null,";

                if (!String.IsNullOrEmpty(dados.VendaNumReceita))
                {
                    strCmd += "'" + dados.VendaNumReceita + "',";
                }
                else
                    strCmd += "null,";

                strCmd += "'" + dados.VendaBeneficio + "'," + Funcoes.BValor(dados.VendaAjusteGeral) + ",'" + dados.VendaObrigaReceita + "',"
                    + "'" + dados.OpCadastro + "',"
                    + Funcoes.BDataHora(dados.DtCadastro) + ")";
            }
            else
            {
                strCmd = "UPDATE VENDAS SET VENDA_CF_ID = " + dados.VendaCfID + ","
                    + " VENDA_DATA_HORA = " + Funcoes.BDataHora(dados.VendaDataHora) + ","
                    + " VENDA_TOTAL = " + Funcoes.BValor(dados.VendaTotal) + ","
                    + " VENDA_SUBTOTAL = " + Funcoes.BValor(dados.VendaSubTotal) + ","
                    + " VENDA_DIFERENCA = " + Funcoes.BValor(dados.VendaDiferenca) + ","
                    + " VENDA_QTDE = " + dados.VendaQtde + ","
                    + " VENDA_STATUS = '" + dados.VendaStatus + "',"
                    + " VENDA_COMANDA = " + dados.VendaComanda + ",";
                if (!String.IsNullOrEmpty(dados.VendaConCodigo))
                {
                    strCmd += " VENDA_CON_CODIGO = '" + dados.VendaConCodigo + "',";
                }
                else
                    strCmd += " VENDA_CON_CODIGO = null,";

                strCmd += " VENDA_ENTREGA = '" + dados.VendaEntrega + "',"
                    + " VENDA_NF_DEVOLUCAO = '" + dados.VendaNfDevolucao + "',";

                if (!String.IsNullOrEmpty(dados.VendaMedico))
                {
                    strCmd += " VENDA_MEDICO = '" + dados.VendaMedico + "',";
                }

                if (!String.IsNullOrEmpty(dados.VendaEmissao))
                {
                    strCmd += " VENDA_EMISSAO = " + Funcoes.BData(Convert.ToDateTime(dados.VendaEmissao)) + ",";
                }
                
                if (!String.IsNullOrEmpty(dados.VendaNumReceita))
                {
                    strCmd += " VENDA_NUM_RECEITA = '" + dados.VendaNumReceita + "',";
                }

                strCmd += " VENDA_BENEFICIO = '" + dados.VendaBeneficio + "',VENDA_AJUSTE_GERAL = " + Funcoes.BValor(dados.VendaAjusteGeral) + ","
                    + " OP_ALTERACAO = '" + dados.OpAlteracao + "',"
                    + " DT_ALTERACAO = " + Funcoes.BDataHora(dados.DtAlteracao);

                strCmd += " WHERE EMP_CODIGO = " + dados.EmpCodigo + " AND EST_CODIGO = " + dados.EstCodigo + " AND VENDA_ID = " + dados.VendaID;
            }

            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizarMovimentoFinanceiro(long sequencia, int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "UPDATE VENDAS SET MOVIMENTO_FINANCEIRO_SEQ = " + sequencia + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDadosDaVendaPorComanda(string numComanda, int estCodigo, int empCodigo)
        {
            string strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.VENDA_ID, A.VENDA_DATA, A.VENDA_DATA_HORA, A.VENDA_COL_CODIGO, A.VENDA_TOTAL, A.VENDA_SUBTOTAL, A.VENDA_DIFERENCA,"
                    + " A.VENDA_CF_ID, C.CF_DOCTO, A.VENDA_CARTAO, A.VENDA_NOME_CARTAO, A.VENDA_CON_CODIGO, A.VENDA_MEDICO, A.VENDA_NUM_RECEITA, A.VENDA_TRANS_ID, A.VENDA_TRANS_ID_RECEITA,"
                    + " A.VENDA_AUTORIZACAO_RECEITA, A.VENDA_CARTAO_CODIGO, A.VENDA_BENEFICIO, A.VENDA_AUTORIZACAO, A.VENDA_AJUSTE_GERAL, A.VENDA_CONVENIO_PARCELA, A.VENDA_OBRIGA_RECEITA "
                    + " FROM VENDAS A"
                    + " INNER JOIN CLIFOR C ON C.CF_ID = A.VENDA_CF_ID"
                    + " WHERE A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = " + empCodigo
                    + " AND A.VENDA_COMANDA = " + numComanda
                    + " AND VENDA_STATUS = 'A'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaAutorizacaoFuncional(string numCartao, string nomeCartao, string empresaConv, long vendaID, int empCodigo, int estCodigo,
            string autorizacao, string nsuTransacao, string vendaMedico = "")
        {
            string strCmd = "UPDATE VENDAS SET VENDA_CARTAO = '" + numCartao + "', VENDA_NOME_CARTAO = '" + (nomeCartao.Length > 50 ? nomeCartao.Substring(0, 49) : nomeCartao) + "',"
                + " VENDA_CON_CODIGO = " + empresaConv + "," + " VENDA_MEDICO = '" + vendaMedico + "'," + " VENDA_TRANS_ID = " + autorizacao + ",";


            if (!String.IsNullOrEmpty(nsuTransacao))
            {
                strCmd += " VENDA_AUTORIZACAO = '" + nsuTransacao + "'";
            }

            strCmd += " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaAutorizacaoDrogabellaPlantao(string numCartao, string nomeCartao, string empresaConv, string codCartao, long vendaID, int empCodigo, int estCodigo,
            string transID = "", string transIDReceita = "", string autorizacao = "", string autorizacaoReceita = "", int vendaConvenioParcela = 1)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_CARTAO = '" + numCartao + "', VENDA_NOME_CARTAO = '" + (nomeCartao.Length > 50 ? nomeCartao.Substring(0, 49) : nomeCartao) + "',"
                + " VENDA_CON_CODIGO = " + empresaConv + ", VENDA_CARTAO_CODIGO = '" + codCartao + "', VENDA_CONVENIO_PARCELA = " + vendaConvenioParcela;
            if (!String.IsNullOrEmpty(transID))
            {
                strCmd += " ,VENDA_TRANS_ID = '" + transID + "'";
            }

            if (!String.IsNullOrEmpty(transIDReceita))
            {
                strCmd += " , VENDA_TRANS_ID_RECEITA = '" + transIDReceita + "'";
            }

            if (!String.IsNullOrEmpty(autorizacao))
            {
                strCmd += " , VENDA_AUTORIZACAO = '" + autorizacao + "'";
            }

            if (!String.IsNullOrEmpty(autorizacaoReceita))
            {
                strCmd += " , VENDA_AUTORIZACAO_RECEITA = '" + autorizacaoReceita + "'";
            }

            strCmd += " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizarInformacoesFiscais(int estCodigo, int empCodigo, long vendaID, string numNota, bool transAberta = false)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_NUM_NOTA = " + numNota + ", VENDA_EMITIDO = 'S' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (transAberta)
            {
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }

        }

        public DataTable BuscaDadosDaVendaPorEmissao(DateTime dataFiltro, int estCodigo, int empCodigo, string vendaId = "")
        {
            string strSql = "SELECT A.EMP_CODIGO, A.EST_CODIGO, A.VENDA_ID, A.VENDA_NUM_NOTA, A.VENDA_TOTAL, A.VENDA_EMISSAO, A.OP_CADASTRO,"
                    + " B.CF_NOME, A.VENDA_BENEFICIO, A.VENDA_DATA_HORA, COALESCE(A.MOVIMENTO_FINANCEIRO_SEQ,0) AS MOVIMENTO_FINANCEIRO_SEQ, COALESCE(A.VENDA_TRANS_ID,'0') AS VENDA_TRANS_ID,"
                    + " COALESCE(A.VENDA_TRANS_ID_RECEITA,'0') AS VENDA_TRANS_ID_RECEITA, A.VENDA_COL_CODIGO, A.VENDA_SUBTOTAL, A.VENDA_DIFERENCA,"
                    + " A.VENDA_AUTORIZACAO, A.VENDA_NOME_CARTAO, A.VENDA_CON_CODIGO, A.VENDA_MEDICO, A.VENDA_NUM_RECEITA, A.VENDA_AUTORIZACAO_RECEITA, C.MED_NOME, "
                    + " D.CON_NOME, B.CF_DOCTO, A.VENDA_CARTAO "
                    + " FROM VENDAS A"
                    + " INNER JOIN CLIFOR B ON A.VENDA_CF_ID = B.CF_ID"
                    + " LEFT JOIN MEDICOS C ON C.MED_CODIGO = A.VENDA_MEDICO"
                    + " LEFT JOIN CONVENIADAS D ON D.CON_CODIGO = A.VENDA_CON_CODIGO"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.VENDA_EMISSAO = " + Funcoes.BData(dataFiltro)
                    + " AND A.VENDA_STATUS = 'F'";
            if (!String.IsNullOrEmpty(vendaId))
            {
                strSql += " AND A.VENDA_ID = " + vendaId;
            }
            strSql += " ORDER BY A.VENDA_ID";
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool CancelaVenda(int estCodigo, int empCodigo, long vendaID, DateTime dtAlteracao, string operador)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_STATUS = 'C', DT_ALTERACAO = " + Funcoes.BDataHora(dtAlteracao) + ", OP_ALTERACAO = '"
                + operador + "' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizarMovimentoFinanceiroEStatus(long sequencia, int estCodigo, int empCodigo, long vendaID, string status)
        {
            string strCmd = "UPDATE VENDAS SET ";
            strCmd += "MOVIMENTO_FINANCEIRO_SEQ = " + sequencia + ",";
            strCmd += " VENDA_STATUS = '" + status + "', VENDA_EMISSAO = " + Funcoes.BData(DateTime.Now) + ", DT_ALTERACAO = "
                + Funcoes.BDataHora(DateTime.Now) + ",  OP_ALTERACAO = '" + Principal.usuario + "' WHERE EST_CODIGO = "
                + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaAutorizacaoBeneficio(long vendaID, int empCodigo, int estCodigo, string autorizacao)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_AUTORIZACAO = '" + autorizacao + "'";
            strCmd += " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaComissaoPorVendedor(int colCodigo, int estCodigo, int empCodigo)
        {
            string strSql = "SELECT A.VENDA_ID, A.VENDA_EMISSAO, B.PROD_CODIGO, C.PROD_DESCR, B.VENDA_ITEM_COMISSAO, "
                            + "              round((((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                            + "      (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "     (B.VENDA_ITEM_COMISSAO / 100),"
                            + "     2) AS VALOR"
                            + "    FROM VENDAS A"
                            + "    INNER JOIN VENDAS_ITENS B ON (A.VENDA_ID = B.VENDA_ID)"
                            + "    INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'"
                            + "    AND B.VENDA_ITEM_COMISSAO > 0"
                            + "    AND B.COL_CODIGO = " + colCodigo;
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND A.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND A.VENDA_BENEFICIO NOT IN('ENTREGA')";

            var mes = DateTime.Now;
            var primeiroDia = new DateTime(mes.Year, mes.Month, 1);
            var ultimoDia = new DateTime(mes.Year, mes.Month,
                    DateTime.DaysInMonth(mes.Year, mes.Month));

            strSql += " AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(primeiroDia) + " AND " + Funcoes.BData(ultimoDia);
            strSql += "    GROUP BY A.VENDA_ID, A.VENDA_EMISSAO, B.PROD_CODIGO, C.PROD_DESCR, B.VENDA_ITEM_COMISSAO, A.VENDA_DIFERENCA, A.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL ORDER BY A.VENDA_ID,A.VENDA_EMISSAO, B.VENDA_ITEM_TOTAL";

            return BancoDados.GetDataTable(strSql, null);
        }

        #region RELATORIOS
        public DataTable BuscaRelatorioPeriodoSintetico(int estCodigo, int empCodigo, string dtInicial, string dtFinal, string horaInicial, string horaFinal,
            int usuario, int formaPagamento, string beneficio)
        {
            string strSql = " SELECT A.VENDA_EMISSAO AS DATA, ";

            if (formaPagamento > 0)
            {
                strSql += " SUM(B.VENDA_VALOR_PARCELA) AS TOTAL, ";
            }
            else
            {
                strSql += "   SUM(A.VENDA_TOTAL) AS TOTAL, ";
            }

            strSql += " COUNT(A.VENDA_ID) AS VENDAS"
                          + " FROM VENDAS A";
            if (formaPagamento > 0)
            {
                strSql += " INNER JOIN VENDAS_FORMA_PAGAMENTO B ON (A.VENDA_ID = B.VENDA_ID)";
            }

            strSql += " WHERE A.EMP_CODIGO = " + empCodigo
                          + " AND A.EST_CODIGO = " + estCodigo
                          + " AND A.VENDA_STATUS = 'F'";

            if (formaPagamento > 0)
            {
                strSql += " AND A.EMP_CODIGO = B.EMP_CODIGO AND A.EST_CODIGO = B.EST_CODIGO AND B.VENDA_FORMA_ID = " + formaPagamento;
            }

            if (horaInicial != "00:00:00" || horaFinal != "00:00:00")
            {
                strSql += " AND  A.VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial + " " + horaInicial)) + " AND "
                    + Funcoes.BDataHora(Convert.ToDateTime(dtFinal + " " + horaFinal));
            }
            else
            {
                strSql += " AND  A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            }

            if (usuario > 0)
            {
                strSql += " AND VENDA_COL_CODIGO = " + usuario;
            }
            if (!beneficio.Equals("NORMAL"))
                strSql += " AND A.VENDA_BENEFICIO = '" + beneficio + "'";

            strSql += " GROUP BY A.VENDA_EMISSAO"
                          + " ORDER BY A.VENDA_EMISSAO";

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaRelatorioPeriodoAnalitico(int estCodigo, int empCodigo, string dtInicial, string dtFinal, string horaInicial, string horaFinal,
            int usuario, int formaPagamento, string beneficio)
        {
            string strSql = " SELECT SUM(A.VENDA_VALOR_PARCELA) AS TOTAL, B.FORMA_DESCRICAO AS FORMA_PAGAMENTO, "
                            + "   D.CF_DOCTO,"
                            + "   CASE"
                            + "     WHEN D.CF_CODIGO IS NULL THEN"
                            + "      D.CF_NOME"
                            + "     ELSE"
                            + "      CONCAT(CONCAT(D.CF_CODIGO, ' - '), D.CF_NOME)"
                            + "   END AS NOME,"
                            + "   C.VENDA_EMISSAO AS DATA,"
                            + "   C.VENDA_DATA_HORA AS HORA,"
                            + "   C.VENDA_ID "
                            + " FROM VENDAS_FORMA_PAGAMENTO A"
                            + " INNER JOIN FORMAS_PAGAMENTO B ON(A.VENDA_FORMA_ID = B.FORMA_ID)"
                            + " INNER JOIN VENDAS C ON(A.VENDA_ID = C.VENDA_ID)"
                            + " INNER JOIN CLIFOR D ON(C.VENDA_CF_ID = D.CF_ID)"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.EST_CODIGO = C.EST_CODIGO"
                            + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + " AND C.VENDA_STATUS = 'F'";

            if (horaInicial != "00:00:00" || horaFinal != "00:00:00")
            {
                strSql += " AND  C.VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial + " " + horaInicial)) + " AND "
                    + Funcoes.BDataHora(Convert.ToDateTime(dtFinal + " " + horaFinal));
            }
            else
            {
                strSql += " AND  C.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            }

            if (usuario > 0)
            {
                strSql += " AND C.VENDA_COL_CODIGO = " + usuario;
            }

            if (formaPagamento > 0)
            {
                strSql += " AND A.VENDA_FORMA_ID = " + formaPagamento;
            }
            if (!beneficio.Equals("NORMAL"))
                strSql += " AND C.VENDA_BENEFICIO = '" + beneficio + "'";

            strSql += "    GROUP BY B.FORMA_DESCRICAO,D.CF_DOCTO, D.CF_CODIGO, D.CF_NOME, C.VENDA_EMISSAO,C.VENDA_DATA_HORA, C.VENDA_ID "
                             + " ORDER BY C.VENDA_EMISSAO,C.VENDA_ID ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioFormaPagamento(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int usuario, int formaPagamento, bool quebra)
        {
            string strSql = "SELECT ";
            if (quebra)
            {
                strSql += " A.VENDA_COL_CODIGO,"
                           + "       B.COL_NOME,";
            }
            strSql += "        C.VENDA_FORMA_ID,"
                           + "        D.FORMA_DESCRICAO,";
            if (quebra)
            {
                strSql += "        CASE"
                        + "          WHEN D.OPERACAO_CAIXA = 'S' THEN"
                        + "           SUM(C.VENDA_VALOR_PARCELA)"
                        + "          ELSE"
                        + "           0"
                        + "        END AS RECEBIDO,"
                        + "        CASE"
                        + "           WHEN D.OPERACAO_CAIXA = 'N' THEN"
                        + "           SUM(C.VENDA_VALOR_PARCELA)"
                        + "          ELSE"
                        + "           0"
                        + "       END AS ARECEBER";
            }
            else
            {
                strSql += "  SUM(C.VENDA_VALOR_PARCELA) AS VENDA";
            }

            strSql += "   FROM VENDAS A"
                           + "   INNER JOIN COLABORADORES B ON(A.VENDA_COL_CODIGO = B.COL_CODIGO)"
                           + "   INNER JOIN VENDAS_FORMA_PAGAMENTO C ON(A.VENDA_ID = C.VENDA_ID)"
                           + "   INNER JOIN FORMAS_PAGAMENTO D ON(D.FORMA_ID = C.VENDA_FORMA_ID)"
                           + "   WHERE A.EMP_CODIGO = " + empCodigo
                           + "   AND A.EST_CODIGO = " + estCodigo
                           + "   AND A.EST_CODIGO = C.EST_CODIGO"
                           + "   AND A.EST_CODIGO = C.EST_CODIGO"
                           + "   AND D.EMP_CODIGO = A.EMP_CODIGO"
                           + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                           + "   AND A.VENDA_STATUS = 'F'"
                           + "   AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (usuario > 0)
            {
                strSql += " AND A.VENDA_COL_CODIGO = " + usuario;
            }

            if (formaPagamento > 0)
            {
                strSql += " AND C.VENDA_FORMA_ID = " + formaPagamento;
            }

            strSql += "    GROUP BY ";
            if (quebra)
            {
                strSql += " A.VENDA_COL_CODIGO, B.COL_NOME, ";
            }

            strSql += "C.VENDA_FORMA_ID, D.FORMA_DESCRICAO, D.OPERACAO_CAIXA "
                             + " ORDER BY C.VENDA_FORMA_ID ";

            if (quebra)
            {
                strSql += ",A.VENDA_COL_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioComissaoPorVendedorResumido(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, int formaPagamento)
        {
            string strSql = "SELECT  A.COL_CODIGO, B.COL_NOME AS NOME, SUM(VALOR) AS VALOR FROM ("
                            + "    SELECT A.VENDA_ID, B.COL_CODIGO,"
                            + "           round((((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                            + "                 (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "                 (B.VENDA_ITEM_COMISSAO / 100),"
                            + "                 2) AS VALOR"
                            + "    FROM VENDAS A"
                            + "    INNER JOIN VENDAS_ITENS B ON (A.VENDA_ID = B.VENDA_ID)"
                            + "    INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)";
            if (formaPagamento > 0)
            {
                strSql += "  INNER JOIN VENDAS_FORMA_PAGAMENTO D ON (A.VENDA_ID = D.VENDA_ID) ";
            }

            strSql += "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'"
                            + "    AND B.VENDA_ITEM_COMISSAO > 0"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND A.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND A.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (formaPagamento > 0)
            {
                strSql += " AND D.VENDA_FORMA_ID = " + formaPagamento + " AND A.EST_CODIGO = D.EST_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO";
            }

            strSql += "    GROUP BY B.COL_CODIGO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        A.VENDA_DIFERENCA,"
                      + "        A.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL, A.VENDA_ID"
                      + "  ORDER BY B.COL_CODIGO) A, COLABORADORES B"
                      + "  WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "  GROUP BY A.COL_CODIGO, B.COL_NOME ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioComissaoPorVendedorSintetico(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, int formaPagamento)
        {
            string strSql = "SELECT VENDA_EMISSAO,A.COL_CODIGO, B.COL_NOME AS NOME, SUM(VALOR) AS COMISSAO, SUM(TOTAL) AS VENDA"
                            + "  FROM(SELECT A.VENDA_ID, B.COL_CODIGO, A.VENDA_EMISSAO, sum(B.VENDA_ITEM_TOTAL) AS TOTAL,"
                            + "               round((((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                            + "                     (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "                     (B.VENDA_ITEM_COMISSAO / 100),"
                            + "                     2) AS VALOR"
                            + "          FROM VENDAS A"
                            + "         INNER JOIN VENDAS_ITENS B ON(A.VENDA_ID = B.VENDA_ID)"
                            + "         INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)";
            if (formaPagamento > 0)
            {
                strSql += "  INNER JOIN VENDAS_FORMA_PAGAMENTO D ON (A.VENDA_ID = D.VENDA_ID) ";
            }

            strSql += "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'"
                            + "    AND B.VENDA_ITEM_COMISSAO > 0"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND A.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND A.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (formaPagamento > 0)
            {
                strSql += " AND D.VENDA_FORMA_ID = " + formaPagamento + " AND A.EST_CODIGO = D.EST_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO";
            }

            strSql += "    GROUP BY B.COL_CODIGO, A.VENDA_EMISSAO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        A.VENDA_DIFERENCA,"
                      + "        A.VENDA_SUBTOTAL,B.VENDA_ITEM_TOTAL, A.VENDA_ID"
                      + "               ORDER BY A.VENDA_COL_CODIGO) A,"
                      + "             COLABORADORES B"
                      + "       WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "       GROUP BY A.COL_CODIGO, B.COL_NOME,VENDA_EMISSAO"
                      + "       ORDER BY A.COL_CODIGO ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioComissaoPorVendedorAnalitico(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, int formaPagamento)
        {
            string strSql = "SELECT A.VENDA_ID,"
                            + "   A.VENDA_EMISSAO,"
                            + "   D.COL_NOME,"
                            + "   CASE WHEN E.CF_CODIGO IS NULL THEN E.CF_NOME ELSE CONCAT(CONCAT(E.CF_CODIGO, ' - '), E.CF_NOME)END AS NOME,"
                            + "   B.VENDA_ITEM_SUBTOTAL,"
                            + "   B.VENDA_ITEM_COMISSAO,"
                            + "   round((((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                            + "         (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "         (B.VENDA_ITEM_COMISSAO / 100),"
                            + "        2) AS VALOR"
                            + "     FROM VENDAS A"
                            + "     INNER JOIN VENDAS_ITENS B ON (A.VENDA_ID = B.VENDA_ID)"
                            + "     INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)"
                            + "     INNER JOIN COLABORADORES D ON(B.COL_CODIGO = D.COL_CODIGO)"
                             + "    INNER JOIN CLIFOR E ON(A.VENDA_CF_ID = E.CF_ID)";
            if (formaPagamento > 0)
            {
                strSql += "  INNER JOIN VENDAS_FORMA_PAGAMENTO F ON (A.VENDA_ID = F.VENDA_ID) ";
            }

            strSql += "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "  AND A.EMP_CODIGO = B.EMP_CODIGO "
                            + "   AND A.EST_CODIGO = B.EST_CODIGO "
                            + "   AND A.VENDA_STATUS = 'F' "
                            + "   AND B.VENDA_ITEM_COMISSAO > 0  AND D.EMP_CODIGO = A.EMP_CODIGO AND D.EMP_CODIGO = A.EMP_CODIGO"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND A.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND A.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (formaPagamento > 0)
            {
                strSql += " AND F.VENDA_FORMA_ID = " + formaPagamento + " AND A.EST_CODIGO = F.EST_CODIGO AND A.EMP_CODIGO = F.EMP_CODIGO";
            }

            strSql += "     GROUP BY A.VENDA_ID,D.COL_NOME,E.CF_NOME,E.CF_CODIGO,B.VENDA_ITEM_SUBTOTAL,"
                        + "          A.VENDA_EMISSAO,"
                        + "          B.VENDA_ITEM_COMISSAO,"
                        + "          A.VENDA_DIFERENCA,"
                        + "          A.VENDA_SUBTOTAL,B.VENDA_ITEM_TOTAL"
                        + "  ORDER BY D.COL_NOME,A.VENDA_ID, A.VENDA_EMISSAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioEspecie(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int usuario, int especieId, bool quebra)
        {
            string strSql = "SELECT ESP_CODIGO, ESP_DESCRICAO, ";
            if (quebra)
            {
                strSql += " VENDA_COL_CODIGO,COL_NOME, ESP_DESCRICAO, SUM(RECEBIDO) AS RECEBIDO, SUM(ARECEBER)AS ARECEBER FROM(";
            }
            else
            {
                strSql += " SUM(VENDA) AS VENDA FROM(";
            }

            strSql += "SELECT DISTINCT(C.ESP_CODIGO), D.ESP_DESCRICAO,";

            if (quebra)
            {
                strSql += " A.VENDA_COL_CODIGO,"
                           + "       B.COL_NOME, A.VENDA_ID, ";
            }
            
            if (quebra)
            {
                strSql += "        CASE"
                        + "          WHEN D.ESP_CAIXA = 'S' THEN"
                        + "           SUM(C.VENDA_ESPECIE_VALOR)"
                        + "          ELSE"
                        + "           0"
                        + "        END AS RECEBIDO,"
                        + "        CASE"
                        + "           WHEN D.ESP_CAIXA = 'N' THEN"
                        + "           SUM(C.VENDA_ESPECIE_VALOR)"
                        + "          ELSE"
                        + "           0"
                        + "       END AS ARECEBER";
            }
            else
            {
                strSql += "  SUM(C.VENDA_ESPECIE_VALOR) AS VENDA";
            }

            strSql += "   FROM VENDAS A"
                           + "   INNER JOIN COLABORADORES B ON(A.VENDA_COL_CODIGO = B.COL_CODIGO)"
                           + "   INNER JOIN VENDAS_ESPECIES C ON(A.VENDA_ID = C.VENDA_ID)"
                           + "   INNER JOIN CAD_ESPECIES D ON (D.ESP_CODIGO = C.ESP_CODIGO)"
                           + "   WHERE A.EMP_CODIGO = " + empCodigo
                           + "   AND A.EST_CODIGO = " + estCodigo
                           + "   AND A.EST_CODIGO = C.EST_CODIGO"
                           + "   AND A.EST_CODIGO = C.EST_CODIGO"
                           + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                           + "   AND A.VENDA_STATUS = 'F'"
                           + "   AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (usuario > 0)
            {
                strSql += " AND A.VENDA_COL_CODIGO = " + usuario;
            }

            if (especieId > 0)
            {
                strSql += " AND C.ESP_CODIGO = " + especieId;
            }

            strSql += "    GROUP BY ";
            if (quebra)
            {
                strSql += " A.VENDA_COL_CODIGO, B.COL_NOME, ";
            }

            strSql += "  C.ESP_CODIGO, D.ESP_DESCRICAO, D.ESP_CAIXA, A.VENDA_ID, C.VENDA_ESPECIE_ID"
                             + " ORDER BY C.ESP_CODIGO ";

            if (quebra)
            {
                strSql += ",A.VENDA_COL_CODIGO";
            }

            strSql += " ) A  GROUP BY ";

            if (quebra)
            {
                strSql += " ESP_CODIGO, VENDA_COL_CODIGO ,COL_NOME, ESP_DESCRICAO ORDER BY ESP_CODIGO, VENDA_COL_CODIGO";
            }
            else
            {
                strSql += " ESP_CODIGO, ESP_DESCRICAO  ORDER BY ESP_CODIGO";
            }

           return BancoDados.GetDataTable(strSql, null);
        }
        #endregion


        public bool AtualizaInformacoesDevolucao(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_SUBTOTAL = (SELECT SUM(VENDA_ITEM_SUBTOTAL) FROM VENDAS_ITENS WHERE VENDA_ID = " + vendaID + "),"
                     + " VENDA_TOTAL = (SELECT SUM(VENDA_ITEM_TOTAL) FROM VENDAS_ITENS WHERE VENDA_ID = " + vendaID + "),"
                     + " VENDA_QTDE = (SELECT SUM(VENDA_ITEM_QTDE) FROM VENDAS_ITENS WHERE VENDA_ID = " + vendaID + ") WHERE VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaInformacoesDevolucaoTotal(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "UPDATE VENDAS SET VENDA_TOTAL = VENDA_TOTAL - (VENDA_DIFERENCA * -1) , OP_ALTERACAO = '" + Principal.usuario + "', DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                + " WHERE VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaComandaAberta(string numComanda)
        {
            string strSql = " SELECT A.PROD_CODIGO,"
                                 + " B.PROD_DESCR, "
                                 + " C.VENDA_ID, "
                                 + " A.VENDA_ITEM_SUBTOTAL "
                            + " FROM VENDAS_ITENS A, PRODUTOS B, VENDAS C "
                            + " WHERE A.VENDA_ID = C.VENDA_ID "
                            + " AND A.PROD_CODIGO = B.PROD_CODIGO "
                            + " AND C.VENDA_STATUS = 'A'"
                            + " AND C.VENDA_COMANDA = " + numComanda
                            + " AND C.EST_CODIGO = " + Principal.estAtual
                            + " AND C.EMP_CODIGO = " + Principal.empAtual;

            return BancoDados.GetDataTable(strSql, null);

        }


        public bool ExcluiComandaAberta(string numComanda, long vendaID)
        {
            string strSql = "  UPDATE VENDAS "
                                 + " SET VENDA_STATUS = 'E', OP_ALTERACAO = '" + Principal.usuario + "', DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                            + " WHERE EST_CODIGO = " + Principal.estAtual
                            + " AND VENDA_ID = " + vendaID + " AND VENDA_COMANDA = " + numComanda
                            + " AND EMP_CODIGO = " + Principal.empAtual;

            if (BancoDados.ExecuteNoQuery(strSql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public double SomatorioVenda(int empCodigo, int estCodigo, long vendaID)
        {
            string sql = "SELECT COALESCE(VENDA_TOTAL,0) AS TOTAL FROM VENDAS WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo + " AND VENDA_ID = " + vendaID;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToDouble(r);
        }

        public DataTable RelatorioComandaAberta(int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.VENDA_COMANDA, A.VENDA_DATA, A.VENDA_ID, B.COL_NOME, A.VENDA_TOTAL "
                            + "    FROM VENDAS A "
                            + "    INNER JOIN COLABORADORES B ON A.VENDA_COL_CODIGO = B.COL_CODIGO "
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND VENDA_STATUS = 'A'";

            return BancoDados.GetDataTable(strSql, null);

        }

        public bool CancelaEntrega(long vendaID)
        {
            string strSql = "  UPDATE VENDAS "
                                 + " SET VENDA_STATUS = 'E', OP_ALTERACAO = '" + Principal.usuario + "', DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                            + " WHERE EST_CODIGO = " + Principal.estAtual
                            + " AND VENDA_ID = " + vendaID
                            + " AND EMP_CODIGO = " + Principal.empAtual;

            if (BancoDados.ExecuteNoQueryTrans(strSql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }


        public DataTable BuscaEmissaoNFE(int empCodigo, int estCodigo, string data, string vendaId, string nomeCliente)
        {
            string strSql = "SELECT A.VENDA_EMISSAO, A.VENDA_ID, B.CF_NOME, COUNT(C.VENDA_ITEM) AS PRODUTOS, SUM(C.VENDA_ITEM_QTDE) QTDE_PRODUTO, A.VENDA_DATA_HORA, B.CF_DOCTO, A.VENDA_TOTAL "
                            + "    FROM VENDAS A"
                            + "    INNER JOIN CLIFOR B ON A.VENDA_CF_ID = B.CF_ID"
                            + "    INNER JOIN VENDAS_ITENS C ON A.VENDA_ID = C.VENDA_ID"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = C.EST_CODIGO"
                            + "    AND A.VENDA_ID NOT IN (SELECT D.VENDA_ID FROM NOTAS_FISCAIS_EMITIDAS D) AND A.VENDA_STATUS = 'F' ";
            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(data)))
            {
                strSql += " AND A.VENDA_EMISSAO = " + Funcoes.BData(Convert.ToDateTime(data));
            }

            if (!String.IsNullOrEmpty(vendaId))
            {
                strSql += " AND A.VENDA_ID = " + vendaId;
            }

            if (!String.IsNullOrEmpty(nomeCliente))
            {
                strSql += " AND B.CF_NOME LIKE  '" + nomeCliente + "%'";
            }

            strSql += "    GROUP BY  A.VENDA_EMISSAO, A.VENDA_ID, B.CF_NOME, A.VENDA_EMITIDO, A.VENDA_DATA_HORA, B.CF_DOCTO, A.VENDA_TOTAL ORDER BY  A.VENDA_EMISSAO, B.CF_NOME";


            return BancoDados.GetDataTable(strSql, null);

        }

        public DataTable BuscaEmissaoNFETotal(int empCodigo, int estCodigo, string dataInicial, string dataFinal, string idCliente)
        {
            string strSql = "SELECT '' AS VENDA_EMISSAO,"
                            + "    '' AS VENDA_ID,"
                            + "    B.CF_NOME,"
                            + "    '' AS PRODUTOS,"
                            + "    '' AS QTDE_PRODUTO,"
                            + "    '' AS VENDA_DATA_HORA,"
                            + "    B.CF_DOCTO,"
                            + "    SUM(A.VENDA_TOTAL) AS VENDA_TOTAL "
                            + "    FROM VENDAS A"
                            + "    INNER JOIN CLIFOR B ON A.VENDA_CF_ID = B.CF_ID"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.VENDA_CF_ID = " + idCliente
                            + "    AND A.VENDA_STATUS = 'F' "
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal + " 00:00:00"))
                            + "    GROUP BY B.CF_NOME,B.CF_DOCTO";


            return BancoDados.GetDataTable(strSql, null);

        }

        public DataTable BuscaTotalizadoresNFE(int empCodigo, int estCodigo, string vendaId, string cliId = "", string dtInicial = "", string dtFinal = "")
        {
            string strSql;
            if (!vendaId.Equals("0"))
            {

                strSql = "SELECT SUM(B.VENDA_ITEM_SUBTOTAL) AS TOTAL, SUM(B.VENDA_ITEM_DIFERENCA) + A.VENDA_DIFERENCA AS DESCONTO, A.VENDA_TOTAL, A.VENDA_ID"
                                + "    FROM VENDAS A"
                                + "    INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                                + "    WHERE A.EMP_CODIGO = " + empCodigo
                                + "    AND A.EST_CODIGO = " + estCodigo
                                + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                                + "    AND A.EST_CODIGO = B.EST_CODIGO"
                                + "    AND A.VENDA_ID = " + vendaId
                                + "    GROUP BY  A.VENDA_DIFERENCA, A.VENDA_TOTAL, A.VENDA_ID";
            }
            else
            {
                strSql = " SELECT SUM(A.VENDA_ITEM_SUBTOTAL) AS TOTAL, SUM(ABS(A.VENDA_ITEM_DIFERENCA)) + C.DIFERENCA AS DESCONTO, C.VENDA_TOTAL"
                        + "    FROM(SELECT SUM(A.VENDA_TOTAL) AS VENDA_TOTAL, SUM(ABS(A.VENDA_DIFERENCA)) AS DIFERENCA"
                        + "      FROM VENDAS A"
                        + "     WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "       AND A.EST_CODIGO = " + Principal.estAtual
                        + "       AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal + " 00:00:00"))
                        + "       AND A.VENDA_STATUS = 'F'"
                        + "       AND A.VENDA_CF_ID = " + cliId + ") C, VENDAS_ITENS A"
                        + "    INNER JOIN VENDAS B ON A.VENDA_ID = B.VENDA_ID"
                        + "     WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "       AND A.EST_CODIGO =" + Principal.empAtual
                        + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + "       AND A.EST_CODIGO = B.EST_CODIGO"
                        + "       AND B.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal + " 00:00:00"))
                        + "       AND B.VENDA_STATUS = 'F'"
                        + "       AND B.VENDA_CF_ID = " + cliId
                        + "       GROUP BY C.VENDA_TOTAL, C.DIFERENCA";

            }

            return BancoDados.GetDataTable(strSql, null);

        }

        public DataTable BuscaRelatorioComissaoPorProdutoSinteticoSemQuebra(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, bool comissao)
        {
            string strSql = "SELECT  A.COL_CODIGO, B.COL_NOME AS NOME, SUM(VALOR_TOTAL) AS TOTAL, SUM(VALOR) AS VALOR, A.VENDA_ITEM_COMISSAO AS COMISSAO FROM ("
                            + "    SELECT D.VENDA_ID, B.COL_CODIGO,"
                            + "          round(((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                (B.VENDA_ITEM_TOTAL)) +(B.VENDA_ITEM_TOTAL),2) AS VALOR_TOTAL,"
                            + "          round((((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                 (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "                 (B.VENDA_ITEM_COMISSAO / 100),"
                            + "                 2) AS VALOR,"
                            + "          B.VENDA_ITEM_COMISSAO"
                            + "    FROM VENDAS D"
                            + "    INNER JOIN VENDAS_ITENS B ON (D.VENDA_ID = B.VENDA_ID)"
                            + "    INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)";
            strSql += "    WHERE D.EMP_CODIGO = " + empCodigo
                            + "    AND D.EST_CODIGO = " + estCodigo
                            + "    AND D.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND D.EST_CODIGO = B.EST_CODIGO"
                            + "    AND D.VENDA_STATUS = 'F'"
                            + "    AND D.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND D.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND D.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (!comissao)
            {
                strSql += "   AND B.VENDA_ITEM_COMISSAO > 0";
            }

            strSql += "    GROUP BY B.COL_CODIGO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        D.VENDA_DIFERENCA,"
                      + "        D.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL, D.VENDA_ID"
                      + "  ORDER BY B.COL_CODIGO) A, COLABORADORES B"
                      + "  WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "  GROUP BY A.COL_CODIGO, B.COL_NOME, A.VENDA_ITEM_COMISSAO "
                      + "  ORDER BY B.COL_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaRelatorioComissaoPorProdutoSinteticoComQuebra(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, bool comissao, string filtro, int quebra)
        {
            string strSql = "SELECT  A.COL_CODIGO, B.COL_NOME AS NOME, SUM(VALOR_TOTAL) AS TOTAL, SUM(VALOR) AS VALOR, A.COMISSAO, A.CODIGO, A.DESCRICAO FROM("
                            + "    SELECT D.VENDA_ID, B.COL_CODIGO,"
                            + "          round(((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                (B.VENDA_ITEM_TOTAL)) +(B.VENDA_ITEM_TOTAL),2) AS VALOR_TOTAL,"
                            + "          round((((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                 (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "                 (B.VENDA_ITEM_COMISSAO / 100),"
                            + "                 2) AS VALOR,"
                            + "          B.VENDA_ITEM_COMISSAO AS COMISSAO, ";
            if (quebra.Equals(1))
            {
                strSql += " A.DEP_CODIGO AS CODIGO,"
                        + " E.DEP_DESCR AS DESCRICAO";
            }
            else if (quebra.Equals(2))
            {
                strSql += " COALESCE(A.CLAS_CODIGO,0) AS CODIGO,"
                        + " COALESCE(E.CLAS_DESCR,'NENHUM') AS DESCRICAO";
            }
            else if (quebra.Equals(3))
            {
                strSql += " COALESCE(A.SUB_CODIGO,0) AS CODIGO,"
                        + " COALESCE(E.SUB_DESCR,'NENHUM') AS DESCRICAO";
            }

            strSql += "    FROM VENDAS D"
                            + "    INNER JOIN VENDAS_ITENS B ON (D.VENDA_ID = B.VENDA_ID)"
                            + "    INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)"
                            + "    INNER JOIN PRODUTOS_DETALHE A ON (A.PROD_CODIGO = B.PROD_CODIGO)";

            if (quebra.Equals(1))
            {
                strSql += "  INNER JOIN DEPARTAMENTOS E ON (A.DEP_CODIGO = E.DEP_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }
            else if (quebra.Equals(2))
            {
                strSql += "  LEFT JOIN CLASSES E ON (A.CLAS_CODIGO = E.CLAS_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }
            else if (quebra.Equals(3))
            {
                strSql += "  LEFT JOIN SUBCLASSES E ON (A.SUB_CODIGO = E.SUB_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }

            strSql += "     WHERE D.EMP_CODIGO = " + empCodigo
                            + "    AND D.EST_CODIGO = " + estCodigo
                            + "    AND D.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND D.EST_CODIGO = B.EST_CODIGO"
                            + "    AND D.EMP_CODIGO = A.EMP_CODIGO"
                            + "    AND D.EST_CODIGO = A.EST_CODIGO"
                            + "    AND D.VENDA_STATUS = 'F'"
                            + "    AND D.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND D.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND D.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (!comissao)
            {
                strSql += "   AND B.VENDA_ITEM_COMISSAO > 0";
            }
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += "    GROUP BY B.COL_CODIGO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        D.VENDA_DIFERENCA,"
                      + "        D.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL, D.VENDA_ID,";
            if (quebra.Equals(1))
            {
                strSql += " A.DEP_CODIGO, E.DEP_DESCR";
            }
            else if (quebra.Equals(2))
            {
                strSql += " A.CLAS_CODIGO, E.CLAS_DESCR";
            }
            else if (quebra.Equals(3))
            {
                strSql += " A.SUB_CODIGO, E.SUB_DESCR";
            }

            strSql += "   ORDER BY B.COL_CODIGO) A, COLABORADORES B"
                      + "  WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "  GROUP BY A.COL_CODIGO, B.COL_NOME, A.COMISSAO, A.CODIGO, A.DESCRICAO "
                      + "  ORDER BY B.COL_NOME, A.DESCRICAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioComissaoPorProdutoAnalitico(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, bool comissaoZero)
        {
            string strSql = "SELECT B.COL_NOME, VALOR_TOTAL AS TOTAL, VALOR AS VALOR, A.VENDA_ITEM_COMISSAO AS COMISSAO, A.PROD_CODIGO, A.PROD_DESCR FROM("
                           + "    SELECT A.VENDA_ID, B.COL_CODIGO,"
                           + "          round(((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                           + "                (B.VENDA_ITEM_TOTAL)) +(B.VENDA_ITEM_TOTAL),2) AS VALOR_TOTAL,"
                           + "          round((((A.VENDA_DIFERENCA / A.VENDA_SUBTOTAL) *"
                           + "                 (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                           + "                 (B.VENDA_ITEM_COMISSAO / 100),"
                           + "                 2) AS VALOR,"
                           + "          B.VENDA_ITEM_COMISSAO,B.PROD_CODIGO, C.PROD_DESCR "
                           + "     FROM VENDAS A"
                            + "     INNER JOIN VENDAS_ITENS B ON (A.VENDA_ID = B.VENDA_ID)"
                            + "     INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)"
                            + "     INNER JOIN COLABORADORES D ON(B.COL_CODIGO = D.COL_CODIGO)"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND A.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND A.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (!comissaoZero)
            {
                strSql += "   AND B.VENDA_ITEM_COMISSAO > 0";
            }

            strSql += "    GROUP BY B.COL_CODIGO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        A.VENDA_DIFERENCA,"
                      + "        A.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL, A.VENDA_ID, B.PROD_CODIGO, C.PROD_DESCR"
                      + "  ORDER BY B.COL_CODIGO) A, COLABORADORES B"
                      + "  WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "  ORDER BY B.COL_NOME, A.PROD_DESCR";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaRelatorioComissaoPorProdutoAnaliticoComQuebra(int colCodigo, int estCodigo, int empCodigo, string dtInicial, string dtFinal, bool comissao, string filtro, int quebra)
        {
            string strSql = "SELECT  B.COL_NOME AS NOME, VALOR_TOTAL AS TOTAL, VALOR AS VALOR, A.COMISSAO, A.CODIGO, A.DESCRICAO, A.PROD_CODIGO, A.PROD_DESCR FROM("
                            + "    SELECT D.VENDA_ID, B.COL_CODIGO, B.PROD_CODIGO, C.PROD_DESCR, "
                            + "          round(((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                (B.VENDA_ITEM_TOTAL)) +(B.VENDA_ITEM_TOTAL),2) AS VALOR_TOTAL,"
                            + "          round((((D.VENDA_DIFERENCA / D.VENDA_SUBTOTAL) *"
                            + "                 (B.VENDA_ITEM_TOTAL)) + (B.VENDA_ITEM_TOTAL)) *"
                            + "                 (B.VENDA_ITEM_COMISSAO / 100),"
                            + "                 2) AS VALOR,"
                            + "          B.VENDA_ITEM_COMISSAO AS COMISSAO, ";
            if (quebra.Equals(1))
            {
                strSql += " A.DEP_CODIGO AS CODIGO,"
                        + " E.DEP_DESCR AS DESCRICAO";
            }
            else if (quebra.Equals(2))
            {
                strSql += " COALESCE(A.CLAS_CODIGO,0) AS CODIGO,"
                        + " COALESCE(E.CLAS_DESCR,'NENHUM') AS DESCRICAO";
            }
            else if (quebra.Equals(3))
            {
                strSql += " COALESCE(A.SUB_CODIGO,0) AS CODIGO,"
                        + " COALESCE(E.SUB_DESCR,'NENHUM') AS DESCRICAO";
            }

            strSql += "    FROM VENDAS D"
                            + "    INNER JOIN VENDAS_ITENS B ON (D.VENDA_ID = B.VENDA_ID)"
                            + "    INNER JOIN PRODUTOS C ON(B.PROD_CODIGO = C.PROD_CODIGO)"
                            + "    INNER JOIN PRODUTOS_DETALHE A ON (A.PROD_CODIGO = B.PROD_CODIGO)";

            if (quebra.Equals(1))
            {
                strSql += "  INNER JOIN DEPARTAMENTOS E ON (A.DEP_CODIGO = E.DEP_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }
            else if (quebra.Equals(2))
            {
                strSql += "  LEFT JOIN CLASSES E ON (A.CLAS_CODIGO = E.CLAS_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }
            else if (quebra.Equals(3))
            {
                strSql += "  LEFT JOIN SUBCLASSES E ON (A.SUB_CODIGO = E.SUB_CODIGO AND E.EMP_CODIGO = A.EMP_CODIGO)";
            }

            strSql += "     WHERE D.EMP_CODIGO = " + empCodigo
                            + "    AND D.EST_CODIGO = " + estCodigo
                            + "    AND D.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND D.EST_CODIGO = B.EST_CODIGO"
                            + "    AND D.EMP_CODIGO = A.EMP_CODIGO"
                            + "    AND D.EST_CODIGO = A.EST_CODIGO"
                            + "    AND D.VENDA_STATUS = 'F'"
                            + "    AND D.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (colCodigo > 0)
            {
                strSql += "    AND B.COL_CODIGO = " + colCodigo;
            }
            if (Funcoes.LeParametro(6, "312", false).Equals("N"))
            {
                strSql += "    AND D.VENDA_BENEFICIO NOT IN('FARMACIAPOPULAR')";
            }

            strSql += "    AND D.VENDA_BENEFICIO NOT IN('ENTREGA')";

            if (!comissao)
            {
                strSql += "   AND B.VENDA_ITEM_COMISSAO > 0";
            }
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += "    GROUP BY B.COL_CODIGO,"
                      + "        B.VENDA_ITEM_COMISSAO,"
                      + "        D.VENDA_DIFERENCA,"
                      + "        D.VENDA_SUBTOTAL, B.VENDA_ITEM_TOTAL, D.VENDA_ID, B.PROD_CODIGO, C.PROD_DESCR, ";
            if (quebra.Equals(1))
            {
                strSql += " A.DEP_CODIGO, E.DEP_DESCR";
            }
            else if (quebra.Equals(2))
            {
                strSql += " A.CLAS_CODIGO, E.CLAS_DESCR";
            }
            else if (quebra.Equals(3))
            {
                strSql += " A.SUB_CODIGO, E.SUB_DESCR";
            }

            strSql += "   ORDER BY B.COL_CODIGO) A, COLABORADORES B"
                      + "  WHERE A.COL_CODIGO = B.COL_CODIGO AND B.EMP_CODIGO = " + empCodigo
                      + "  ORDER BY B.COL_NOME, A.DESCRICAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendasPorCfop(int empCodigo, int estCodigo, string dataInicial, string dataFinal, string notaOpcao)
        {
            string strSql = "SELECT COALESCE(A.PROD_CFOP,'VAZIO') AS CFOP, COALESCE(C.DESCRICAO,'SEM DESCRICAO/VAZIO') AS DESCRICAO, SUM(A.VENDA_ITEM_QTDE) AS QUANTIDADE,SUM(A.VENDA_ITEM_DIFERENCA) AS DESCONTO,SUM(A.VENDA_ITEM_TOTAL) AS TOTAL "
                + "FROM (SELECT V.VENDA_ITEM_QTDE,V.VENDA_ITEM_DIFERENCA,V.VENDA_ITEM_TOTAL,P.PROD_CFOP "
                + "FROM PRODUTOS_DETALHE P, VENDAS_ITENS V, CUPONS_SAT S "
                + "WHERE P.PROD_CODIGO = V.PROD_CODIGO "
                + "AND P.EMP_CODIGO = " + empCodigo + " "
                + "AND P.EST_CODIGO = " + estCodigo + " "
                + "AND V.EMP_CODIGO = " + empCodigo + " "
                + "AND V.EST_CODIGO = " + estCodigo + " "
                + "AND S.EMP_CODIGO = " + empCodigo + " "
                + "AND S.EST_CODIGO = " + estCodigo + " "
                + "AND V.VENDA_ID = S.VENDA_ID ";
            if (notaOpcao.Equals("Concluído"))
                strSql += "AND S.VENDA_EMITIDO = 'S' ";
            else if (notaOpcao.Equals("Cancelado"))
                strSql += "AND S.VENDA_EMITIDO = 'C' ";

            strSql += "AND S.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 23:59:59"))+ " ) A  "
                + "LEFT JOIN CFOP C ON A.PROD_CFOP = C.CFOP "
                + "GROUP BY A.PROD_CFOP,C.DESCRICAO";
            return BancoDados.GetDataTable(strSql, null);
        }
        public DataTable BuscaVendasPorNumeroNota(int empCodigo, int estCodigo, string dataInicial, string dataFinal, string notaOpcao)
        {
            string strSql = "SELECT A.VENDA_NUM_NOTA AS NUMERO, TO_CHAR(A.DT_CADASTRO,'DD/MM/YYYY') AS DATA_EMISSAO, C.CF_NOME AS DESTINATARIO,   "
                + "CASE WHEN A.VENDA_CANCELADO IS NULL THEN 'Concluído' ELSE 'Cancelado' END AS SITUACAO , A.VENDA_TOTAL AS TOTAL "
                + "FROM CUPONS_SAT A, VENDAS B, CLIFOR C "
                + "WHERE A.VENDA_ID = B.VENDA_ID "
                + "AND C.CF_ID = B.VENDA_CF_ID "
                + "AND B.EMP_CODIGO = " + empCodigo + " "
                + "AND B.EST_CODIGO = " + estCodigo + " "
                + "AND A.EMP_CODIGO = " + empCodigo + " "
                + "AND A.EST_CODIGO = " + estCodigo + " ";
                
            if (notaOpcao.Equals("Concluído"))
                strSql += "AND A.VENDA_EMITIDO = 'S' ";
            else if (notaOpcao.Equals("Cancelado"))
                strSql += "AND A.VENDA_EMITIDO = 'C' ";

            strSql += "AND A.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 23:59:59"))
                + " ORDER BY A.DT_CADASTRO, A.VENDA_NUM_NOTA";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaImpostosPorData(int empCodigo, int estCodigo, string dataInicial, string dataFinal, string notaOpcao)
        {
            string strSql = "SELECT TO_CHAR(A.DT_CADASTRO,'DD/MM/YYYY') AS DATA_VENDA, SUM(B.VENDA_ITEM_SUBTOTAL) AS TOTAL_VENDA, SUM(B.VENDA_ITEM_DIFERENCA) AS TOTAL_DESCONTO, SUM(B.VENDA_ITEM_TOTAL) AS TOTAL_CUPONS, A.VENDA_EMITIDO AS STATUS "
                + "FROM CUPONS_SAT A, VENDAS_ITENS B "
                + "WHERE A.VENDA_ID = B.VENDA_ID "
                + "AND B.EMP_CODIGO = " + empCodigo + " "
                + "AND B.EST_CODIGO = " + estCodigo + " "
                + "AND A.EMP_CODIGO = " + empCodigo + " "
                + "AND A.EST_CODIGO = " + estCodigo + " ";

            if (notaOpcao.Equals("Concluído"))
                strSql += "AND A.VENDA_EMITIDO = 'S' ";
            else if (notaOpcao.Equals("Cancelado"))
                strSql += "AND A.VENDA_EMITIDO = 'C' ";

            strSql += "AND A.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 23:59:59"))
                + " GROUP BY TO_CHAR(A.DT_CADASTRO,'DD/MM/YYYY'), A.VENDA_EMITIDO "
                + "ORDER BY TO_DATE(TO_CHAR(A.DT_CADASTRO,'DD/MM/YYYY'),'DD/MM/YYYY')";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaICMSPorData(int empCodigo, int estCodigo, string dataInicial,string notaOpcao)
        {
            string strSql = "SELECT SUM(B.VENDA_ITEM_UNITARIO) AS BASE, A.PROD_ALIQ_ICMS AS ICMS, ROUND(SUM(B.VENDA_ITEM_UNITARIO) * A.PROD_ALIQ_ICMS /100,2) AS VALOR_ICMS"
                + " FROM PRODUTOS_DETALHE A, VENDAS_ITENS B, CUPONS_SAT C"
                + " WHERE A.PROD_CODIGO = B.PROD_CODIGO"
                + " AND B.VENDA_ID = C.VENDA_ID"
                + " AND B.EMP_CODIGO = " + empCodigo + " "
                + " AND B.EST_CODIGO = " + estCodigo + " "
                + " AND A.EMP_CODIGO = " + empCodigo + " "
                + " AND A.EST_CODIGO = " + estCodigo + " "
                + " AND C.EMP_CODIGO = " + empCodigo + " "
                + " AND C.EST_CODIGO = " + estCodigo + " ";

            if (notaOpcao.Equals("S"))
                strSql += "AND C.VENDA_EMITIDO = 'S' ";
            else if (notaOpcao.Equals("C"))
                strSql += "AND C.VENDA_EMITIDO = 'C' ";

            strSql += " AND C.VENDA_EMITIDO = 'S'"
                + " AND A.PROD_ALIQ_ICMS >0"
                + " AND TO_CHAR(B.DT_CADASTRO,'DD/MM/YYYY') = '"+dataInicial+"'"
                + " GROUP BY A.PROD_ALIQ_ICMS"

                ;
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable CurvaAbc(int empCodigo, int estCodigo, string dtInicial, string dtFinal, int depCodigo)
        {

            string strSql = "SELECT PROD_CODIGO,"
                            + "       PROD_DESCR,"
                            + "       QTDE,"
                            + "       VENDA_ITEM_UNITARIO,"
                            + "       TOTAL,"
                            + "       PORCENTAGEM,"
                            + "       SUM(PORCENTAGEM)OVER(ORDER BY PORCENTAGEM DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS ACUMULADO"
                            + "   FROM("
                            + "   SELECT B.PROD_CODIGO,"
                            + "       C.PROD_DESCR,"
                            + "       SUM(B.VENDA_ITEM_QTDE) AS QTDE,"
                            + "       B.VENDA_ITEM_UNITARIO,"
                            + "       SUM(B.VENDA_ITEM_TOTAL) AS TOTAL,"
                            + "          ((SUM(B.VENDA_ITEM_TOTAL) * 100) /"
                            + "           (SELECT SUM(VENDA_ITEM_TOTAL) AS TOTAL"
                            + "               FROM VENDAS_ITENS E"
                            + "              INNER JOIN VENDAS F ON E.VENDA_ID = F.VENDA_ID"
                            + "              WHERE F.VENDA_STATUS = 'F'"
                            + "                AND F.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial))
                            + "                AND " + Funcoes.BData(Convert.ToDateTime(dtFinal))
                            + "                AND E.EMP_CODIGO = " + empCodigo
                            + "                AND E.EST_CODIGO = " + estCodigo
                            + "                AND E.EMP_CODIGO = F.EMP_CODIGO"
                            + "                AND E.EST_CODIGO = F.EST_CODIGO)) AS PORCENTAGEM"
                            + "   FROM VENDAS A"
                            + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO";
            if (depCodigo != 0)
            {
                strSql += " INNER JOIN PRODUTOS_DETALHE D ON B.PROD_CODIGO = D.PROD_CODIGO";
            }
            strSql += "  WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "   AND A.EST_CODIGO = B.EST_CODIGO"
                            + "   AND A.VENDA_STATUS = 'F'"
                            + "   AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial))
                            + "   AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if (depCodigo != 0)
            {
                strSql += "  AND B.EST_CODIGO = D.EST_CODIGO"
                    + " AND B.EMP_CODIGO = D.EMP_CODIGO"
                    + " AND D.DEP_CODIGO = " + depCodigo;
            }
            strSql += " GROUP BY B.PROD_CODIGO, C.PROD_DESCR, B.VENDA_ITEM_UNITARIO, VENDA_ITEM_SUBTOTAL"
                            + " ORDER BY 5 DESC) "
                            + " ORDER BY TOTAL DESC";
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaListaPositivaNegativa(int empCodigo, int estCodigo, string dataInicial, string dataFinal, bool lista)
        {
            string strSql = "SELECT A.VENDA_EMISSAO, B.PROD_CODIGO, C.PROD_DESCR, D.PROD_LISTA, SUM(B.VENDA_ITEM_QTDE) AS QTDE, E.PRE_VALOR, B.VENDA_ITEM_TOTAL"
                            + "    FROM VENDAS A"
                            + "    INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "    INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "    INNER JOIN PRODUTOS_DETALHE D ON B.PROD_CODIGO = D.PROD_CODIGO"
                            + "    INNER JOIN PRECOS E ON E.PROD_CODIGO = B.PROD_CODIGO"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND B.EMP_CODIGO = D.EMP_CODIGO"
                            + "    AND B.EST_CODIGO = D.EST_CODIGO"
                            + "    AND B.EMP_CODIGO = E.EMP_CODIGO"
                            + "    AND B.EST_CODIGO = E.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'";
            if(!lista)
            {
                strSql += " AND (D.PROD_LISTA IS NOT NULL OR D.PROD_LISTA <> '')";
            }
            strSql += "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal))
                            + " GROUP BY  A.VENDA_EMISSAO, B.PROD_CODIGO, C.PROD_DESCR, D.PROD_LISTA,E.PRE_VALOR, B.VENDA_ITEM_TOTAL"
                            + "    ORDER BY A.VENDA_EMISSAO, B.PROD_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendasCanceladasSintetico(int empCodigo, int estCodigo, string dataInicial, string dataFinal)
        {
            string strSql = "SELECT A.VENDA_EMISSAO, SUM(A.VENDA_TOTAL) AS TOTAL"
                            + "    FROM VENDAS A"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.VENDA_STATUS = 'C'"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal))
                            + "    GROUP BY A.VENDA_EMISSAO"
                            + "    ORDER BY A.VENDA_EMISSAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendasCanceladasAnalitico(int empCodigo, int estCodigo, string dataInicial, string dataFinal)
        {
            string strSql = "SELECT A.VENDA_EMISSAO, A.VENDA_ID, B.CF_DOCTO, B.CF_NOME, A.VENDA_TOTAL, A.OP_ALTERACAO "
                            + "    FROM VENDAS A"
                            + "    INNER JOIN CLIFOR B ON A.VENDA_CF_ID = B.CF_ID"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.VENDA_STATUS = 'C'"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal))
                            + "    ORDER BY A.VENDA_EMISSAO, B.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosPorEspecie(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int usuario, int espCodigo)
        {
            string strSql = "SELECT B.PROD_CODIGO, C.PROD_DESCR, SUM(B.VENDA_ITEM_QTDE) AS QTDE, SUM(B.VENDA_ITEM_TOTAL) AS TOTAL, D.ESP_CODIGO, E.ESP_DESCRICAO"
                            + "    FROM VENDAS A"
                            + "    INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "    INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "    INNER JOIN VENDAS_ESPECIES D ON A.VENDA_ID = D.VENDA_ID"
                            + "    INNER JOIN CAD_ESPECIES E ON D.ESP_CODIGO = E.ESP_CODIGO"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.EST_CODIGO = B.EST_CODIGO"
                            + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "    AND A.EST_CODIGO = D.EST_CODIGO"
                            + "    AND A.EMP_CODIGO = D.EST_CODIGO"
                            + "    AND A.VENDA_STATUS = 'F'"
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));

            if (usuario > 0)
            {
                strSql += " AND C.VENDA_COL_CODIGO = " + usuario;
            }

            if (espCodigo > 0)
            {
                strSql += " AND D.ESP_CODIGO = " + espCodigo;
            }

            strSql += "    GROUP BY B.PROD_CODIGO, C.PROD_DESCR,D.ESP_CODIGO, E.ESP_DESCRICAO"
                            + "    ORDER BY E.ESP_DESCRICAO, C.PROD_DESCR";

            return BancoDados.GetDataTable(strSql, null);
        }
        

        public DataTable BuscaEntregaSolicitadaSintetico(int estCodigo, int empCodigo, string dtInicial, string dtFinal,
            int usuario, string status)
        {
            string strSql = "SELECT A.DTCADASTRO AS DATA, SUM(A.TOTAL_ENTREGA) AS TOTAL, COUNT(A.CODIGO)AS VENDAS"
                + " FROM ENTREGA_FILIAL A"
                + " WHERE A.EMP_CODIGO = " + empCodigo
                          + " AND A.EST_CODIGO = " + estCodigo
                          + " AND A.STATUS = '" + status + "'"
                          + " AND  A.DTCADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial + " 00:00:00")) + " AND "
                          + Funcoes.BDataHora(Convert.ToDateTime(dtFinal + " 23:59:59"))
                          + " GROUP BY A.DTCADASTRO"
                          + " ORDER BY A.DTCADASTRO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaEntregaSolicitadaAnalitico(int estCodigo, int empCodigo, string dtInicial, string dtFinal,
           int usuario, string status)
        {
            string strSql = "SELECT  A.CODIGO, A.ID, A.CF_DOCTO,B.CF_NOME, A.TOTAL_ENTREGA, A.DTCADASTRO, A.SOLICITANTE, A.STATUS, A.LOJA_DESTINO"
                + " FROM ENTREGA_FILIAL A"
                + " INNER JOIN CLIFOR B ON A.CF_DOCTO = B.CF_DOCTO"
                + " WHERE A.EMP_CODIGO = " + empCodigo
                          + " AND A.EST_CODIGO = " + estCodigo
                          + " AND A.STATUS = '" + status + "'"
                          + " AND  A.DTCADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial + " 00:00:00")) + " AND "
                          + Funcoes.BDataHora(Convert.ToDateTime(dtFinal + " 23:59:59"))
                          + " ORDER BY A.CODIGO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendasPorPeriodo(int empCodigo, int estCodigo, string dataInicial, string dataFinal, string vendaID)
        {
            string strSql = "SELECT A.VENDA_ID, A.VENDA_EMISSAO, B.CF_NOME, A.VENDA_TOTAL, A.VENDA_BENEFICIO"
                            + "    FROM VENDAS A"
                            + "    INNER JOIN CLIFOR B ON A.VENDA_CF_ID = B.CF_ID"
                            + "    WHERE A.VENDA_STATUS = 'F'"
                            + "    AND A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));
            if(!String.IsNullOrEmpty(vendaID))
            {
                strSql += " AND A.VENDA_ID = " + vendaID;
            }
            strSql += " ORDER BY A.VENDA_EMISSAO, A.VENDA_ID";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaFormaPagtoPorVendaID(string vendaID, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.VENDA_ID, B.FORMA_DESCRICAO AS DESCRICAO, A.VENDA_VALOR_PARCELA AS VALOR"
                            + "    FROM VENDAS_FORMA_PAGAMENTO A"
                            + "    INNER JOIN FORMAS_PAGAMENTO B ON A.VENDA_FORMA_ID = B.FORMA_ID"
                            + "    WHERE VENDA_ID = " + vendaID
                            + "    AND A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND B.EMP_CODIGO = " + empCodigo;
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaEspeciePorVendaID(string vendaID, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.VENDA_ID, B.ESP_DESCRICAO AS DESCRICAO, A.VENDA_ESPECIE_VALOR AS VALOR"
                            + "    FROM VENDAS_ESPECIES A"
                            + "    INNER JOIN CAD_ESPECIES B ON A.ESP_CODIGO = B.ESP_CODIGO"
                            + "    WHERE A.VENDA_ID = " + vendaID
                            + "    AND A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo;
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaLucroXCustoSintetico(int empCodigo, int estCodigo, string dataInicial, string dataFinal)
        {
            string strSql = "SELECT SUM(B.VENDA_ITEM_SUBTOTAL) AS TOTAL_VENDAS,"
                            + "       (SUM(B.VENDA_ITEM_DIFERENCA) + abs(SUM(A.VENDA_DIFERENCA))) AS DIFERENCA,"
                            + "       SUM(B.VENDA_ITEM_SUBTOTAL) - (SUM(B.VENDA_ITEM_DIFERENCA)  + abs(SUM(A.VENDA_DIFERENCA))) AS LIQUIDO_VENDAS,"
                            + "       SUM(C.PROD_ULTCUSME) AS CUSTO,"
                            + "       (SUM(B.VENDA_ITEM_SUBTOTAL) - (SUM(B.VENDA_ITEM_DIFERENCA) + abs(SUM(A.VENDA_DIFERENCA)))) -"
                            + "       SUM(C.PROD_ULTCUSME) AS LUCRO"
                            + "  FROM VENDAS A"
                            + "  INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "  LEFT JOIN PRODUTOS_DETALHE C ON (B.PROD_CODIGO = C.PROD_CODIGO AND A.EST_CODIGO = C.EST_CODIGO AND A.EMP_CODIGO = C.EMP_CODIGO)"
                            + "  WHERE A.EMP_CODIGO = " + empCodigo
                            + "  AND A.EST_CODIGO = " + estCodigo
                            + "  AND A.EST_CODIGO = B.EST_CODIGO"
                            + "  AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "  AND A.VENDA_STATUS = 'F'"
                            + "  AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaLucroXCustoAnalitico(int empCodigo, int estCodigo, string dataInicial, string dataFinal)
        {
            string strSql = "SELECT A.VENDA_ID, B.PROD_CODIGO, "
                            + "           B.VENDA_ITEM_SUBTOTAL AS TOTAL_VENDAS,"
                            + "           B.VENDA_ITEM_DIFERENCA AS DIFERENCA,"
                            + "           B.VENDA_ITEM_SUBTOTAL - B.VENDA_ITEM_DIFERENCA AS LIQUIDO_VENDAS,"
                            + "           C.PROD_ULTCUSME AS CUSTO,"
                            + "           (B.VENDA_ITEM_SUBTOTAL - B.VENDA_ITEM_DIFERENCA) - C.PROD_ULTCUSME AS LUCRO"
                            + "      FROM VENDAS A"
                            + "     INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "     INNER JOIN PRODUTOS_DETALHE C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "       AND A.EST_CODIGO = " + estCodigo
                            + "       AND A.EST_CODIGO = B.EST_CODIGO"
                            + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = C.EST_CODIGO"
                            + "       AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "       AND A.VENDA_STATUS = 'F'"
                            + "  AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal))
                            + "  ORDER BY A.VENDA_ID";
            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaVendasConveniadas(int empCodigo, int estCodigo, string dataInicial, string dataFinal, int codConveniada)
        {
            string strSql = "SELECT DISTINCT A.VENDA_CON_CODIGO AS VENDA_CON_CODIGO, B.CON_NOME"
                            + "      FROM VENDAS A"
                            + "     INNER JOIN CONVENIADAS B ON A.VENDA_CON_CODIGO = B.CON_CODIGO"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "       AND A.EST_CODIGO = " + estCodigo
                            + "       AND A.VENDA_STATUS = 'F'"
                            + "       AND B.CON_WEB <> 4"
                            + "       AND A.VENDA_BENEFICIO <> 'N'"
                            + "  AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal));
            if(codConveniada != 0)
            {
                strSql += "  AND A.VENDA_CON_CODIGO = '" + codConveniada + "'";
            }
            strSql += "  ORDER BY B.CON_NOME";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaVendaPorVendaId(int estCodigo, int empCodigo, long venda_id)
        {
            string strSql = "SELECT * FROM VENDAS WHERE VENDA_ID = " + venda_id + " AND EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaSomatorioDiferenca(DateTime dtInicial, DateTime dtFinal)
        {
            string strSql = "SELECT ABS(SUM(VENDA_DIFERENCA)) AS DIFERENCA"
                            + "      FROM VENDAS"
                            + "     WHERE VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                            + "       AND VENDA_STATUS = 'F'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable RelatorioDeVendasMensalAPI(DateTime dtInicial, DateTime dtFinal)
        {
            string strSql = " SELECT SUM(B.VENDA_ITEM_QTDE) AS QTDE, B.PROD_CODIGO, C.PROD_DESCR, TRIM(E.DEP_DESCR) AS DEP_DESCR"
                            + "     FROM VENDAS A"
                            + "     INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "     INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "     INNER JOIN PRODUTOS_DETALHE D ON C.PROD_CODIGO = D.PROD_CODIGO"
                            + "     LEFT JOIN DEPARTAMENTOS E ON (D.DEP_CODIGO = E.DEP_CODIGO AND D.EMP_CODIGO = E.EMP_CODIGO)"
                            + "     WHERE A.VENDA_STATUS = 'F'"
                            + "     AND A.EMP_CODIGO = " + Funcoes.LeParametro(6,"367",false)
                            + "     AND A.EST_CODIGO = " + Funcoes.LeParametro(6,"366", false)
                            + "     AND A.EST_CODIGO = B.EST_CODIGO"
                            + "     AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "     AND A.EST_CODIGO = D.EST_CODIGO"
                            + "     AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "     AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal)
                            + "     GROUP BY B.PROD_CODIGO, C.PROD_DESCR, E.DEP_DESCR"
                            + "     ORDER BY 1 DESC";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
