﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class AtualizaPrecoFilial
    {
        public int CodLojaOrigem { get; set; }
        public int CodLojaDestino { get; set; }
        public int CodGrupo { get; set; }
        public string CodDeBarras { get; set; }
        public double ValorDeVenda { get; set; }
        public char Atualiza { get; set; }
        public string Operador { get; set; }
        public string Observacao { get; set; }
        public DateTime Data { get; set; }
        public string DataAlteracao { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
