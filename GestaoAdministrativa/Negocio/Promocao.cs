﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Promocao
    {
        public int EmpCodigo { get; set; }
        public int grupoid { get; set; }
        public int PromoCodigo { get; set; }
        public string PromoDescr { get; set; }
        public string PromoTipo { get; set; }
        public string DataIni { get; set; }
        public string DataFim { get; set; }
        public string DiaIni { get; set; }
        public string DiaFim { get; set; }
        public int ProdID { get; set; }
        public int DepCodigo { get; set; }
        public int ClasCodigo { get; set; }
        public int SubCodigo { get; set; }
        public double PromoPreco { get; set; }
        public double PromoPorc { get; set; }
        public string PromoLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string DescontoProgressivo { get; set; }
        public int Leve { get; set; }
        public int Pague { get; set; }
        public double Porcentagem { get; set; }

        public long CodBarras { get; set; }
        public string codOrigem { get; set; }
        public string codDestino { get; set; }
        public string atualiza { get; set; }
        public string observacao { get; set; }
        public string dataCad { get; set; }
        public string dataAlt { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public Promocao() { }

        public Promocao(int empCodigo, int promoCodigo, string promoDescr, string promoTipo, string dataIni, string dataFim, string diaIni, string diaFim,
            int prodId, int depCodigo, int clasCodigo, int subCodigo, double promoPreco, double promoPorc, string promoLiberado, DateTime dtAlteracao,
            string opAlteracao, DateTime dtCadastro, string opCadastro, string descontoProgressivo, int leve, int pague, double porcentagem)
        {
            this.EmpCodigo = empCodigo;
            this.PromoCodigo = promoCodigo;
            this.PromoDescr = promoDescr;
            this.PromoTipo = promoTipo;
            this.DataIni = dataIni;
            this.DataFim = dataFim;
            this.DiaIni = diaIni;
            this.DiaFim = diaFim;
            this.ProdID = prodId;
            this.DepCodigo = depCodigo;
            this.ClasCodigo = clasCodigo;
            this.SubCodigo = subCodigo;
            this.PromoPreco = promoPreco;
            this.PromoPorc = promoPorc;
            this.PromoLiberado = promoLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DescontoProgressivo = descontoProgressivo;
            this.Leve = leve;
            this.Pague = pague;
            this.Porcentagem = porcentagem;
        }

        public Promocao(int empCodigo, int promoCodigo, string promoDescr, string promoTipo, string dataIni, string dataFim, string diaIni, string diaFim,
            int prodId, int depCodigo, int clasCodigo, int subCodigo, double promoPreco, double promoPorc, string promoLiberado, string dataAlt,
            string opAlteracao, string dataCad, string opCadastro, string descontoProgressivo, int leve, int pague, double porcentagem)
        {
            this.EmpCodigo = empCodigo;
            this.PromoCodigo = promoCodigo;
            this.PromoDescr = promoDescr;
            this.PromoTipo = promoTipo;
            this.DataIni = dataIni;
            this.DataFim = dataFim;
            this.DiaIni = diaIni;
            this.DiaFim = diaFim;
            this.ProdID = prodId;
            this.DepCodigo = depCodigo;
            this.ClasCodigo = clasCodigo;
            this.SubCodigo = subCodigo;
            this.PromoPreco = promoPreco;
            this.PromoPorc = promoPorc;
            this.PromoLiberado = promoLiberado;
            this.dataAlt = dataAlt;
            this.OpAlteracao = opAlteracao;
            this.dataCad = dataCad;
            this.OpCadastro = opCadastro;
            this.DescontoProgressivo = descontoProgressivo;
            this.Leve = leve;
            this.Pague = pague;
            this.Porcentagem = porcentagem;
        }



        public Promocao(int empCodigo, int grupoid, int promoCodigo, string promoDescr, string promoTipo, string dataIni, string dataFim, string diaIni, string diaFim,
            int prodId, int depCodigo, int clasCodigo, int subCodigo, double promoPreco, double promoPorc, string promoLiberado, DateTime dtCadastro, string opCadastro, string descontoProgressivo, int leve, int pague, double porcentagem)
        {
            this.EmpCodigo = empCodigo;
            this.grupoid = grupoid;
            this.PromoCodigo = promoCodigo;
            this.PromoDescr = promoDescr;
            this.PromoTipo = promoTipo;
            this.DataIni = dataIni;
            this.DataFim = dataFim;
            this.DiaIni = diaIni;
            this.DiaFim = diaFim;
            this.ProdID = prodId;
            this.DepCodigo = depCodigo;
            this.ClasCodigo = clasCodigo;
            this.SubCodigo = subCodigo;
            this.PromoPreco = promoPreco;
            this.PromoPorc = promoPorc;
            this.PromoLiberado = promoLiberado;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DescontoProgressivo = descontoProgressivo;
            this.Leve = leve;
            this.Pague = pague;
            this.Porcentagem = porcentagem;
        }

        public Promocao(string promoTipo, string dataIni, string dataFim, string diaIni, string diaFim,
           long CodBarras, double promoPreco, double promoPorc, string descontoProgressivo, int leve, int pague, double porcentagem,
           string codOrigem, string codDestino, string atualiza, DateTime dataCad, string opCadastro, DateTime dataAlt, string observacao)

        {
            this.PromoTipo = promoTipo;
            this.DataIni = dataIni;
            this.DataFim = dataFim;
            this.DiaIni = diaIni;
            this.DiaFim = diaFim;
            this.CodBarras = CodBarras;
            this.PromoPreco = promoPreco;
            this.PromoPorc = promoPorc;
            this.DescontoProgressivo = descontoProgressivo;
            this.Leve = leve;
            this.Pague = pague;
            this.Porcentagem = porcentagem;
            this.codOrigem = codOrigem;
            this.codDestino = codDestino;
            this.atualiza = atualiza;

            this.DtCadastro = dataCad;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dataAlt;
            this.observacao = observacao;
        }

        public DataTable BuscarDados(string codBarras, int empCodigo, string promoDescr, string promoTipo, string promoLiberado, out string strOrdem)
        {
            string strSql;

            strSql = " SELECT A.EMP_CODIGO, A.PROMO_CODIGO, A.PROMO_DESCR, CASE A.PROMO_TIPO WHEN 'D' THEN 'DINAMICO' ELSE 'FIXO' END AS PROMO_TIPO,";
            strSql += "    CASE PROMO_TIPO";
            strSql += "     WHEN 'F' THEN";
            strSql += "      DIA_INI";
            strSql += "     ELSE";
            strSql += "        DATA_INI END AS DATA_INI,";
            strSql += "   CASE PROMO_TIPO";
            strSql += "     WHEN 'F' THEN";
            strSql += "      DIA_FIM";
            strSql += "     ELSE";
            strSql += "      DATA_FIM  END AS DATA_FIM,";
            strSql += " E.PROD_CODIGO, B.DEP_DESCR, C.CLAS_DESCR, D.SUB_DESCR, A.PROMO_PRECO, A.PROMO_PORC,";
            strSql += " CASE WHEN A.PROMO_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS PROMO_DESABILITADO,";
            strSql += " A.DTALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO, A.DESCONTO_PROGRESSIVO, A.LEVE, A.PAGUE, A.PORCENTAGEM";
            strSql += " FROM PROMOCOES A";
            strSql += " LEFT JOIN DEPARTAMENTOS B ON (A.DEP_CODIGO = B.DEP_CODIGO AND A.EMP_CODIGO = B.EMP_CODIGO)";
            strSql += " LEFT JOIN CLASSES C ON (A.CLAS_CODIGO = C.CLAS_CODIGO AND A.EMP_CODIGO = C.EMP_CODIGO)";
            strSql += " LEFT JOIN SUBCLASSES D ON (A.SUB_CODIGO = D.SUB_CODIGO AND A.EMP_CODIGO = D.EMP_CODIGO)";
            strSql += " LEFT JOIN PRODUTOS E ON (A.PROD_ID = E.PROD_ID)";
            strSql += " WHERE A.EMP_CODIGO = " + empCodigo;

            //BUSCA SEM NENHUM FILTRO//
            if (codBarras == "" && promoDescr == "" && promoTipo == "" && promoLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.PROMO_CODIGO";
            }
            else
            {
                if (codBarras != "")
                {
                    strSql += " AND E.PROD_CODIGO = '" + codBarras + "'";
                }
                if (promoDescr != "")
                {
                    strSql += " AND A.PROMO_DESCR LIKE '%" + promoDescr + "%'";
                }
                if (promoTipo != "")
                {
                    strSql += " AND A.PROMO_TIPO = '" + promoTipo + "'";
                }
                if (promoLiberado != "TODOS")
                {
                    promoLiberado = promoLiberado == "N" ? "S" : "N";
                    strSql += " AND A.PROMO_DESABILITADO = '" + promoLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.PROMO_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(Promocao dados)
        {
            string strCmd = "INSERT INTO PROMOCOES (PROMO_CODIGO, EMP_CODIGO, PROMO_DESCR, PROMO_TIPO, DATA_INI, DATA_FIM,"
                + "DIA_INI, DIA_FIM, PROD_ID, DEP_CODIGO, CLAS_CODIGO, SUB_CODIGO, PROMO_PRECO, PROMO_PORC, PROMO_DESABILITADO, DTCADASTRO, OPCADASTRO, DESCONTO_PROGRESSIVO, LEVE, PAGUE, PORCENTAGEM) VALUES("
                + dados.PromoCodigo + ","
                + dados.EmpCodigo + ",'"
                + dados.PromoDescr + "','"
                + dados.PromoTipo + "',";
            if (dados.DataIni == "")
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.DataIni + "',";

            if (dados.DataFim == "")
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.DataFim + "',";

            if (dados.DiaIni == "")
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.DiaIni + "',";

            if (dados.DiaFim == "")
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.DiaFim + "',";

            strCmd += dados.ProdID + ","
                + dados.DepCodigo + ","
                + dados.ClasCodigo + ","
                + dados.SubCodigo + ","
                + Funcoes.BFormataValor(dados.PromoPreco) + "," + Funcoes.BFormataValor(dados.PromoPorc) + ",'" + dados.PromoLiberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "','"
                + dados.DescontoProgressivo + "',"
                + dados.Leve + ","
                + dados.Pague + ","
                + Funcoes.BFormataValor(dados.Porcentagem) + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Promocao dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[19, 3];
            int contMatriz = 0;
            string strCmd;

            strCmd = "UPDATE PROMOCOES SET ";

            if (!dadosNew.PromoDescr.Equals(dadosOld.Rows[0]["PROMO_DESCR"]))
            {
                strCmd += " PROMO_DESCR = '" + dadosNew.PromoDescr + "',";

                dados[contMatriz, 0] = "PROMO_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROMO_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PromoDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.PromoTipo.Equals(dadosOld.Rows[0]["PROMO_TIPO"]))
            {
                strCmd += " PROMO_TIPO = '" + dadosNew.PromoTipo + "',";

                dados[contMatriz, 0] = "PROMO_TIPO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROMO_TIPO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PromoTipo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DataIni.Equals(dadosOld.Rows[0]["DATA_INI"].ToString() == "" ? "" : dadosOld.Rows[0]["DATA_INI"].ToString().Substring(0, 10)))
            {
                strCmd += dadosNew.DataIni == "" ? " DATA_INI = null," : " DATA_INI = '" + dadosNew.DataIni + "',";

                dados[contMatriz, 0] = "DATA_INI";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DATA_INI"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DATA_INI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DataIni + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DataFim.Equals(dadosOld.Rows[0]["DATA_FIM"].ToString() == "" ? "" : dadosOld.Rows[0]["DATA_FIM"].ToString().Substring(0, 10)))
            {
                strCmd += dadosNew.DataFim == "" ? " DATA_FIM = null," : " DATA_FIM = '" + dadosNew.DataFim + "',";

                dados[contMatriz, 0] = "DATA_FIM";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DATA_FIM"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DATA_FIM"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DataFim + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DiaIni.Equals(dadosOld.Rows[0]["DIA_INI"].ToString()))
            {
                strCmd += dadosNew.DiaIni == "" ? " DIA_INI = null," : " DIA_INI = '" + dadosNew.DiaIni + "',";

                dados[contMatriz, 0] = "DIA_INI";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DIA_INI"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DIA_INI"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DiaIni + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DiaFim.Equals(dadosOld.Rows[0]["DIA_FIM"].ToString()))
            {
                strCmd += dadosNew.DiaFim == "" ? " DIA_FIM = null," : " DIA_FIM = '" + dadosNew.DiaFim + "',";

                dados[contMatriz, 0] = "DIA_FIM";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DIA_FIM"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DIA_FIM"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DiaFim + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ProdID.Equals(dadosOld.Rows[0]["PROD_ID"].ToString()))
            {
                strCmd += " PROD_ID = " + dadosNew.ProdID + ",";

                dados[contMatriz, 0] = "PROD_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROD_ID"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ProdID + "'";
                contMatriz = contMatriz + 1;

            }
            if (!dadosNew.DepCodigo.Equals(dadosOld.Rows[0]["DEP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dadosOld.Rows[0]["DEP_CODIGO"])))
            {
                strCmd += " DEP_CODIGO = " + dadosNew.DepCodigo + ",";

                dados[contMatriz, 0] = "DEP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DEP_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DepCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ClasCodigo.Equals(dadosOld.Rows[0]["CLAS_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dadosOld.Rows[0]["CLAS_CODIGO"])))
            {
                strCmd += " CLAS_CODIGO = " + dadosNew.ClasCodigo + ",";

                dados[contMatriz, 0] = "CLAS_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLAS_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ClasCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.SubCodigo.Equals(dadosOld.Rows[0]["SUB_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dadosOld.Rows[0]["SUB_CODIGO"])))
            {
                strCmd += " SUB_CODIGO = " + dadosNew.SubCodigo + ",";

                dados[contMatriz, 0] = "SUB_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SUB_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.SubCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.PromoPreco).Equals(String.Format("{0:N}", dadosOld.Rows[0]["PROMO_PRECO"])))
            {
                strCmd += " PROMO_PRECO = " + Funcoes.BFormataValor(dadosNew.PromoPreco) + ",";

                dados[contMatriz, 0] = "PROMO_PRECO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROMO_PRECO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PromoPreco + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.PromoPorc).Equals(String.Format("{0:N}", dadosOld.Rows[0]["PROMO_PORC"])))
            {
                strCmd += " PROMO_PORC = " + Funcoes.BFormataValor(dadosNew.PromoPorc) + ",";

                dados[contMatriz, 0] = "PROMO_PORC";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROMO_PORC"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PromoPorc + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.PromoLiberado.Equals(dadosOld.Rows[0]["PROMO_DESABILITADO"].ToString()))
            {
                strCmd += " PROMO_DESABILITADO = '" + dadosNew.PromoLiberado + "',";

                dados[contMatriz, 0] = "PROMO_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PROMO_DESABILITADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PromoLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.DescontoProgressivo.Equals(dadosOld.Rows[0]["DESCONTO_PROGRESSIVO"].ToString()))
            {
                strCmd += " DESCONTO_PROGRESSIVO = '" + dadosNew.DescontoProgressivo + "',";

                dados[contMatriz, 0] = "DESCONTO_PROGRESSIVO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESCONTO_PROGRESSIVO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.DescontoProgressivo + "'";
                contMatriz = contMatriz + 1;

                if (!dadosNew.Leve.Equals(dadosOld.Rows[0]["LEVE"]))
                {
                    strCmd += " LEVE = " + dadosNew.Leve + ",";

                    dados[contMatriz, 0] = "LEVE";
                    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LEVE"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNew.Leve + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNew.Pague.Equals(dadosOld.Rows[0]["PAGUE"]))
                {
                    strCmd += " PAGUE = " + dadosNew.Pague + ",";

                    dados[contMatriz, 0] = "PAGUE";
                    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PAGUE"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNew.Pague + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!String.Format("{0:N}", dadosNew.Porcentagem).Equals(String.Format("{0:N}", dadosOld.Rows[0]["PORCENTAGEM"])))
                {
                    strCmd += " PORCENTAGEM = " + Funcoes.BFormataValor(dadosNew.Porcentagem) + ",";

                    dados[contMatriz, 0] = "PORCENTAGEM";
                    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PORCENTAGEM"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNew.Porcentagem + "'";
                    contMatriz = contMatriz + 1;
                }
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and promo_codigo = " + dadosNew.PromoCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("PROMO_CODIGO", dadosNew.PromoCodigo.ToString(), Principal.usuario, "PROMOCOES", dados, contMatriz, Principal.empAtual).Equals(true);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(int empCodigo, string promoCodigo)
        {
            string strCmd = "DELETE FROM PROMOCOES WHERE EMP_CODIGO = " + empCodigo + " AND PROMO_CODIGO = " + promoCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable VerificaProdID(string prodid)
        {
            string sql = "SELECT PROD_ID FROM PROMOCOES WHERE PROD_ID = " + prodid;
            return BancoDados.GetDataTable(sql, null);
        }

        public bool DisablePromo(string prodid)
        {
            string sql = "UPDATE PROMOCOES SET PROMO_DESABILITADO = 'S' WHERE PROD_ID = " + prodid;

            int retorno = BancoDados.ExecuteNoQuery(sql, null);
            return retorno >= 1 ? true : false;
        }


    }
}
