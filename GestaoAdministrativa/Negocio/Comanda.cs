﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class Comanda
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int ComNumero { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Comanda() { }

        public Comanda(int empCodigo, int estCodigo, int comNumero, DateTime dtCadastro, string opCadastro) 
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ComNumero = comNumero;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public string ValidaComanda(int empCodigo, int estCodigo, string comCodigo)
        {
            string strSql = "SELECT COM_NUMERO FROM CAD_COMANDA WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo + 
                " AND COM_NUMERO = '" + comCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public string IdentificaComandaAberta(int estCodigo, int empCodigo, string comNumero)
        {
            string strSql = " SELECT VENDA_STATUS FROM VENDAS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_COMANDA = " + comNumero + " AND VENDA_STATUS = 'A'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "F";
            else
                return Convert.ToString(r);
        }

        public DataTable BuscarDados(string comNumero, int estCodigo, int empCodigo, out string strOrdem)
        {
            string strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.COM_NUMERO, A.DTCADASTRO, A.OPCADASTRO"
                  + " FROM CAD_COMANDA A WHERE A.EST_CODIGO = " + estCodigo + " AND A.EMP_CODIGO = " + empCodigo;
            if (comNumero == "")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.COM_NUMERO";
            }
            else
            {
                if (comNumero != "")
                {
                    strSql += " AND A.COM_NUMERO = " + comNumero;
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.COM_NUMERO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(Comanda dados)
        {
            string strCmd = "INSERT INTO CAD_COMANDA (EMP_CODIGO, EST_CODIGO, COM_NUMERO, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.EmpCodigo + ","
                + dados.EmpCodigo + ","
                + dados.ComNumero + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluirDados(int estCodigo, int empCodigo, string cCodigo)
        {
            string strCmd = "DELETE FROM CAD_COMANDA WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND COM_NUMERO = " + cCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
