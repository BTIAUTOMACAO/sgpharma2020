﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class RetornoABCFarma
    {
        public int pagina { get; set; }
        public string limit { get; set; }
        public int total_paginas { get; set; }
        public int total_itens { get; set; }
        public int total_data { get; set; }
        public ProdutosAbc[] data { get; set; }
    }
}
