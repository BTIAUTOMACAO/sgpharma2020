﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class TabelaAbcFarma
    {
        public string MedAbc { get; set; }
        public string LabNome { get; set; }
        public string MedDescricao { get; set; }
        public string MedApresentacao { get; set; }
        public double MedPco { get; set; }
        public double MedPla { get; set; }
        public double MedFra { get; set; }
        public string MedBarra { get; set; }
        public string MedRegistroMS { get; set; }
        public string MedNcm { get; set; }
        public string MedLista { get; set; }
        public string MedGen { get; set; }
        public string MedCas { get; set; }
        public string MedDCB { get; set; }
        public string MedCest { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
