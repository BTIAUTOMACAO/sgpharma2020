﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class TaxaEntrega
    {
        public int EstCodigo { get; set; }
        public int EntrCodigo { get; set; }
        public string EntrDescr { get; set; }
        public decimal EntrValor { get; set; }
        public decimal EntrIsencao { get; set; }
        public string EntrRegra { get; set; }
        public string EntrEmpres { get; set; }
        public string EntrLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public TaxaEntrega() { }

    }
}
