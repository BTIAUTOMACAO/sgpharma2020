﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    class Pedidos
    {
        public int est_codigo { get; set; }
        public long ped_codigo { get; set; }
        public int cli_id { get; set; }
        public string ped_data { get; set; }
        public decimal ped_total { get; set; }
        public int col_codigo { get; set; }
        public int cpr_codigo { get; set; }
        public int tab_codigo { get; set; }
        public string ped_entrega { get; set; }
        public int no_id { get; set; }
        public decimal ped_subtotal { get; set; }
        public decimal ped_difer { get; set; }
        public string ped_comanda { get; set; }
        public string ped_status { get; set; }
        public string ped_obs { get; set; }
        public int login_id { get; set; }
        public int id_caixa { get; set; }
        public int rec_codigo { get; set; }
        public int movto_seq { get; set; }
        public string ped_hora { get; set; }
        public int ped_autorizacao { get; set; }
        public string med_crm { get; set; }
        public string ped_cartao { get; set; }
        public int pos_codigo { get; set; }
        public int con_id { get; set; }

        public Pedidos() { }

        public static long GetContador(int estCodigo)
        {
            long id = 0;
            MontadorSql mont;
            if (String.IsNullOrEmpty(GetConsultaContador(estCodigo)))
            {
                mont = new MontadorSql("pedidos_contador", MontadorType.Insert);
                id = 1;
            }
            else
            {
                mont = new MontadorSql("pedidos_contador", MontadorType.Update);
                mont.SetWhere("WHERE est_codigo = " + estCodigo, null);
                id = Funcoes.VerificaID("PEDIDOS_CONTADOR", "PED_CODIGO", estCodigo);
            }
            mont.AddField("est_codigo", estCodigo);
            mont.AddField("ped_codigo", id);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return id;
            }
            else
                return 0;
        }

        public static string GetConsultaContador(int estCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));

            Principal.strSql = "SELECT PED_CODIGO FROM PEDIDOS_CONTADOR WHERE EST_CODIGO = @est_codigo";

            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public static string GetConsultaPedido(int estCodigo, long pedCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("ped_codigo", pedCodigo));

            Principal.strSql = "SELECT PED_CODIGO FROM PEDIDOS WHERE EST_CODIGO = @est_codigo AND PED_CODIGO = @ped_codigo";

            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public static int GetUpFormaPagto(int estCodigo, long pedCodigo, int cprCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("ped_codigo", pedCodigo));
            ps.Add(new Fields("cpr_codigo", cprCodigo));

            Principal.strCmd = "UPDATE PEDIDOS SET CPR_CODIGO = @cpr_codigo WHERE EST_CODIGO = @est_codigo AND PED_CODIGO = @ped_codigo";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);
        }

        public static bool GetInserirPreVenda(Pedidos dados)
        {
            MontadorSql mont;
            if (String.IsNullOrEmpty(GetConsultaPedido(dados.est_codigo, dados.ped_codigo)))
            {
                mont = new MontadorSql("pedidos", MontadorType.Insert);
            }
            else
            {
                mont = new MontadorSql("pedidos", MontadorType.Update);
                mont.SetWhere("WHERE est_codigo = " + dados.est_codigo + " AND ped_codigo = " + dados.ped_codigo, null);
            }
            mont.AddField("est_codigo", dados.est_codigo);
            mont.AddField("ped_codigo", dados.ped_codigo);
            mont.AddField("cli_id", dados.cli_id);
            mont.AddField("ped_data", dados.ped_data);
            mont.AddField("ped_total", dados.ped_total);
            mont.AddField("col_codigo", dados.col_codigo);
            mont.AddField("tab_codigo", dados.tab_codigo);
            mont.AddField("ped_entrega", dados.ped_entrega);
            mont.AddField("no_id", dados.no_id);
            mont.AddField("ped_subtotal", dados.ped_subtotal);
            mont.AddField("ped_difer", dados.ped_difer);
            mont.AddField("ped_status", dados.ped_status);
            if(!String.IsNullOrEmpty(dados.ped_comanda))
                mont.AddField("ped_comanda", dados.ped_comanda);
            mont.AddField("login_id", dados.login_id);
            mont.AddField("ped_hora", dados.ped_hora);
            if(!String.IsNullOrEmpty(dados.ped_comanda))
                mont.AddField("med_crm", dados.med_crm);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
