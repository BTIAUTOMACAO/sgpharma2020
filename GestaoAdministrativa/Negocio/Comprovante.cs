﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Comprovante
    {

        public DataTable BuscaComprovantePeriodo(DateTime dtIncial, DateTime dtFinal, string nomeCliente)
        {

            string sql = " SELECT VEN.VENDA_ID , VEN.VENDA_DATA_HORA, CLI.CF_NOME, VEN.VENDA_TOTAL "
                       + " FROM VENDAS VEN "
                       + " INNER JOIN CLIFOR CLI ON(CLI.CF_ID = VEN.VENDA_CF_ID) "
                       + " WHERE  VEN.VENDA_DATA_HORA BETWEEN " + Funcoes.BDataHora(dtIncial) + "  AND " + Funcoes.BDataHora(dtFinal)
                       + " AND VEN.VENDA_STATUS = 'F'";
            if(!String.IsNullOrEmpty(nomeCliente))
            {
                sql += " AND CLI.CF_NOME LIKE '" + nomeCliente + "%'";
            }
            sql += " ORDER BY VENDA_DATA_HORA DESC";

            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BuscaItensComprovante(long vendaID)
        {

            string sql = "  SELECT LPAD(VI.PROD_CODIGO, 13, 0) AS PROD_CODIGO, "
                       + " VI.VENDA_ITEM_QTDE, "
                       + " VI.VENDA_ITEM_UNITARIO, "
                       + " VI.VENDA_ITEM_DIFERENCA, "
                       + " (VI.VENDA_ITEM_DIFERENCA / VI.VENDA_ITEM_QTDE) AS VALOR_DESCONTO, "
                       + " (( VENDA_ITEM_UNITARIO- VI.VENDA_ITEM_DIFERENCA /VI.VENDA_ITEM_QTDE)) AS VALOR_UNI_COM_DES,"
                       + "  VI.VENDA_ITEM_TOTAL, "
                       + " VI.VENDA_ITEM_SUBTOTAL, "
                       + " PROD.PROD_DESCR "
                       + " FROM VENDAS_ITENS VI "
                       + " INNER JOIN PRODUTOS PROD ON(PROD.PROD_ID = VI.PROD_ID) "
                       + " WHERE VI.VENDA_ID = " + vendaID
                       + " ORDER BY VENDA_ITEM";

            return BancoDados.GetDataTable(sql, null);
        }
        public DataTable BuscaVendaPorId(long vendaID)
        {
            string sql = " SELECT TO_CHAR(VENDA_DATA_HORA, 'HH24:MI:SS') AS HORA , TO_CHAR(VENDA_DATA_HORA, 'DD/MM/YYYY') AS DATA,  VENDA_TOTAL FROM VENDAS "
                       + " WHERE VENDA_ID = " + vendaID;

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaFormaPagamento(long vendaID)
        {
            string sql = " SELECT VFP.VENDA_VALOR_PARCELA , FP.FORMA_DESCRICAO  "
                       + " FROM VENDAS_FORMA_PAGAMENTO VFP "
                       + " INNER JOIN FORMAS_PAGAMENTO FP ON(FP.FORMA_ID = VFP.VENDA_FORMA_ID) "
                       + " WHERE VENDA_ID = " + vendaID;

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaProdutosComprovantePorCobranca(int cobrancaID)
        {
            string sql = " SELECT VI.VENDA_ITEM, VI.PROD_CODIGO, VI.VENDA_ITEM_QTDE, VI.VENDA_ITEM_UNITARIO, "
                       + " (VI.VENDA_ITEM_DIFERENCA / VI.VENDA_ITEM_QTDE) AS VALOR_DESCONTO ,  VI.VENDA_ITEM_TOTAL, "
                       + " (VI.VENDA_ITEM_UNITARIO -(VI.VENDA_ITEM_DIFERENCA / VI.VENDA_ITEM_QTDE)) AS VENDA_VALOR_ITEM, "
                       + " PRO.PROD_DESCR, PRO.PROD_UNIDADE "
                       + " FROM VENDAS_ITENS VI "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_ID = VI.PROD_ID) "
                       + " INNER JOIN COBRANCA COB ON(COB.COBRANCA_VENDA_ID = VI.VENDA_ID) "
                       + " WHERE COB.COBRANCA_ID = " + cobrancaID
                       + " ORDER BY VI.VENDA_ITEM";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaDadosComprovantePorCobranca(int cobrancaID)
        {
            string sql = " SELECT TO_CHAR(CO.DT_CADASTRO, 'DD/MM/YYYY') AS DT_VENDA , TO_CHAR(CO.DT_CADASTRO, 'HH24:MI:SS') AS HORA_VENDA, "
                       + " CO.COBRANCA_TOTAL "
                       + " ,FP.FORMA_DESCRICAO, "
                       + " CL.CF_NOME "
                       + " FROM COBRANCA CO "
                       + " INNER JOIN CLIFOR CL ON(CL.CF_ID = CO.COBRANCA_CF_ID) "
                       + " INNER JOIN  FORMAS_PAGAMENTO FP ON(FP.FORMA_ID = CO.COBRANCA_FORMA_ID) "
                       + " WHERE CO.COBRANCA_ID IN " + cobrancaID + " AND CO.COBRANCA_ORIGEM = 'C'";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
