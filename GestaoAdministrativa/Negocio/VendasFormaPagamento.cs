﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class VendasFormaPagamento
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaId { get; set; }
        public long VendaFormaPagamentoID { get; set; }
        public int VendaFormaID { get; set; }
        public int VendaParcela { get; set; }
        public double VendaValorParcela { get; set; }
        public DateTime VendaParcelaVencimento { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string QtdeVias { get; set; }

        public VendasFormaPagamento() { }

        public VendasFormaPagamento(int empCodigo, int estCodigo, long vendaID, int vendaFormaPagamentoID, int vendaFormaId, int vendaParcela, double vendaValorParcela, DateTime vendaParcelaVencimento,
            DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.VendaId = vendaID;
            this.VendaFormaPagamentoID = vendaFormaPagamentoID;
            this.VendaFormaID = vendaFormaId;
            this.VendaParcela = vendaParcela;
            this.VendaValorParcela = vendaValorParcela;
            this.VendaParcelaVencimento = vendaParcelaVencimento;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InserirDados(VendasFormaPagamento dados)
        {
            string strCmd = "INSERT INTO VENDAS_FORMA_PAGAMENTO(EMP_CODIGO,EST_CODIGO,VENDA_ID,VENDA_FORMA_PAGTO_ID,VENDA_FORMA_ID,VENDA_PARCELA,VENDA_VALOR_PARCELA,VENDA_PARCELA_VENCIMENTO,"
                + "DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.VendaId + ","
                + dados.VendaFormaPagamentoID + ","
                + dados.VendaFormaID + ","
                + dados.VendaParcela + ","
                + Funcoes.BValor(dados.VendaValorParcela) + ","
                + Funcoes.BData(dados.VendaParcelaVencimento) + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaFormasPagamentosPorVendaID(int empCodigo, int estCodigo, long vendaID)
        {
            string strSql = "SELECT A.*, B.QTDE_VIAS FROM VENDAS_FORMA_PAGAMENTO A "
                + " INNER JOIN FORMAS_PAGAMENTO B ON (A.VENDA_FORMA_ID = B.FORMA_ID AND A.EMP_CODIGO = B.EMP_CODIGO)"
                + " WHERE A.EMP_CODIGO = " + empCodigo + " AND A.EST_CODIGO = " + estCodigo
                + " AND A.VENDA_ID = " + vendaID;
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool ExcluirDados(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "DELETE FROM VENDAS_FORMA_PAGAMENTO WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND VENDA_ID = " + vendaID;

            return BancoDados.executarSql(strCmd);
        }


        public DataTable BuscaFormasPagamentosParaEntrega(int empCodigo, int estCodigo, long vendaID)
        {
            string strSql = "SELECT A.EMP_CODIGO, A.EST_CODIGO, A.VENDA_ID, A.VENDA_PARCELA, A.VENDA_PARCELA_VENCIMENTO, A.VENDA_VALOR_PARCELA,"
                        + " B.FORMA_DESCRICAO AS FORMA_ID, B.OPERACAO_CAIXA, A.VENDA_FORMA_PAGTO_ID "
                        + " FROM VENDAS_FORMA_PAGAMENTO A"
                        + " INNER JOIN FORMAS_PAGAMENTO B ON A.VENDA_FORMA_ID = B.FORMA_ID"
                        + " WHERE A.EMP_CODIGO = " + empCodigo
                        + " AND A.EST_CODIGO = " + estCodigo
                        + " AND A.VENDA_ID = " + vendaID
                        + " AND B.EMP_CODIGO = " + empCodigo
                        + " ORDER BY A.VENDA_PARCELA";
            return BancoDados.GetDataTable(strSql, null);
        }
        
        public bool AtualizaFormaDePagamento(int formaID, int estCodigo, int empCodigo, long vendaID, int vendaFormaID, string opCadastro)
        {
            string strCmd = "UPDATE VENDAS_FORMA_PAGAMENTO SET VENDA_FORMA_ID = " + formaID + ", OP_CADASTRO = '" + opCadastro + "', DT_CADASTRO = " + Funcoes.BDataHora(DateTime.Now) 
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = "
                + empCodigo + " AND VENDA_ID = " + vendaID + " AND VENDA_FORMA_PAGTO_ID = " + vendaFormaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public List<VendasFormaPagamento> DadosFormaPagamentoPorVendaID(long vendaId, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.VENDA_PARCELA, A.VENDA_PARCELA_VENCIMENTO, A.VENDA_VALOR_PARCELA"
                + " FROM VENDAS_FORMA_PAGAMENTO A"
                + " WHERE A.EMP_CODIGO = " + empCodigo
                + " AND A.EST_CODIGO = " + estCodigo
                + " AND A.VENDA_ID = " + vendaId;

            List<VendasFormaPagamento> lista = new List<VendasFormaPagamento>();
            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    VendasFormaPagamento dFormas = new VendasFormaPagamento();
                    dFormas.VendaParcela = Convert.ToInt32(row["VENDA_PARCELA"]);
                    dFormas.VendaValorParcela = Convert.ToDouble(row["VENDA_VALOR_PARCELA"]);
                    dFormas.VendaParcelaVencimento = Convert.ToDateTime(row["VENDA_PARCELA_VENCIMENTO"]);
                    lista.Add(dFormas);
                }
            }
            return lista;
        }

        public DataTable BuscaFormaPagtoPorVendaID(int empCodigo, int estCodigo, long vendaID)
        {
            string strSql = "SELECT * FROM VENDAS_FORMA_PAGAMENTO WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo
                + " AND VENDA_ID = " + vendaID;
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaOperador(int estCodigo, int empCodigo, long vendaID)
        {
            string strCmd = "UPDATE VENDAS_FORMA_PAGAMENTO SET OP_CADASTRO = '" + Principal.usuario + "', DT_CADASTRO = " + Funcoes.BDataHora(DateTime.Now)
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
