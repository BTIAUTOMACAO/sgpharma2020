﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    class GrupoClientes
    {
        public int GrupoCodigo { get; set; }
        public string GrupoDescr { get; set; }
        public string GrupoLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public GrupoClientes() { }

        public static bool InserirDados(GrupoClientes dados)
        {
            MontadorSql mont = new MontadorSql("grupo_clientes", MontadorType.Insert);
            mont.AddField("grupo_codigo", dados.GrupoCodigo);
            mont.AddField("grupo_descr", dados.GrupoDescr);
            mont.AddField("grupo_liberado", dados.GrupoLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }


        public static bool AtualizaDados(GrupoClientes dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("grupo_clientes", MontadorType.Update);

            if (!dadosNew.GrupoDescr.Equals(dadosOld.Rows[0]["GRUPO_DESCR"]))
            {
                //VERIFICA SE GRUPO JA ESTA CADASTRADO//
                if (Util.GetRegistros("GRUPO_CLIENTES", "GRUPO_DESCR", dadosNew.GrupoDescr).Rows.Count != 0)
                {
                    Principal.mensagem = "Grupo de Cliente já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }
                mont.AddField("grupo_descr", dadosNew.GrupoDescr);

                dados[contMatriz, 0] = "GRUPO_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.GrupoLiberado.Equals(dadosOld.Rows[0]["GRUPO_LIBERADO"]))
            {
                mont.AddField("grupo_liberado", dadosNew.GrupoLiberado);

                dados[contMatriz, 0] = "GRUPO_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuarioID);
                mont.SetWhere("WHERE grupo_codigo = " + dadosNew.GrupoCodigo, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) == 1)
                {
                    if (Funcoes.LogAlteracao("GRUPO_CODIGO", dadosNew.GrupoCodigo.ToString(), Principal.usuarioID, "GRUPO_CLIENTES", dados, contMatriz).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        public static int ExcluirDados(string grupCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("grupo_codigo", grupCodigo));

            Principal.strCmd = "DELETE FROM GRUPO_CLIENTES WHERE GRUPO_CODIGO = @grupo_codigo";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);
        }

        public static DataTable GetBuscar(GrupoClientes dadosBusca, out string strOrdem)
        {
            Principal.strSql = "SELECT A.GRUPO_CODIGO, A.GRUPO_DESCR, A.GRUPO_LIBERADO, A.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = A.OPALTERACAO)"
                       + " AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO FROM GRUPO_CLIENTES A, USUARIO_SYSTEM B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.GrupoCodigo == 0 && dadosBusca.GrupoDescr == "" && dadosBusca.GrupoLiberado == "TODOS")
            {
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.GRUPO_CODIGO";
            }
            else
            {
                if (dadosBusca.GrupoCodigo != 0)
                {
                    Principal.strSql += " AND A.GRUPO_CODIGO = " + dadosBusca.GrupoCodigo;
                }
                if (dadosBusca.GrupoDescr != "")
                {
                    Principal.strSql += " AND A.GRUPO_DESCR LIKE '%" + dadosBusca.GrupoDescr + "%'";
                }
                if (dadosBusca.GrupoLiberado != "TODOS")
                {
                    Principal.strSql += " AND A.GRUPO_LIBERADO = '" + dadosBusca.GrupoLiberado + "'";
                }
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.GRUPO_CODIGO";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }
    }
}
