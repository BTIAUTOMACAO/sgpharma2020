﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class PedidoParcela
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public long PedNumero { get; set; }
        public string PedComplemento { get; set; }
        public int PedParcNumero { get; set; }
        public DateTime PedParcDataVencimento { get; set; }
        public double PedParcValor { get; set; }
        public string PedParcCondicao { get; set; }

        public PedidoParcela() { }

        public PedidoParcela(int estCodigo, int empCodigo, long pedNumero, string pedComplemento, int pedParcNumero, DateTime pedParcDataVencimento, 
            double pedParcValor, string pedParcCondicao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.PedNumero = pedNumero;
            this.PedComplemento = pedComplemento;
            this.PedParcNumero = pedParcNumero;
            this.PedParcDataVencimento = pedParcDataVencimento;
            this.PedParcValor = pedParcValor;
            this.PedParcCondicao = pedParcCondicao;
        }
    }
}
