﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    class ControleEntregaStatus
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int EntStatusID { get; set; }
        public int EntControleID { get; set; }
        public int EntStatusSequencia { get; set; }
        public int EntStatus { get; set; }
        public DateTime EntStatusDataHora { get; set; }
        public string EntStatusUsuario { get; set; }
        public string EntStatusObservacao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }

        public ControleEntregaStatus() { }

        public ControleEntregaStatus(int empCodigo, int estCodigo, int entStID, int entControleID, int entStSequencia, int entStStatus, DateTime entStDataHora,
            string entStUsuario, string entStObservacao, DateTime dtCadastro, string opCadastro, DateTime dtAlteracao, string opAlteracao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.EntStatusID = entStID;
            this.EntControleID = entControleID;
            this.EntStatusSequencia = entStSequencia;
            this.EntStatus = entStStatus;
            this.EntStatusDataHora = entStDataHora;
            this.EntStatusUsuario = entStUsuario;
            this.EntStatusObservacao = entStObservacao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
        }

        public bool InsereRegistros(ControleEntregaStatus dados)
        {
            string strCmd = "INSERT INTO CONTROLE_ENTREGA_STATUS(EMP_CODIGO, EST_CODIGO, ENT_ST_ID, ENT_CONTROLE_ID,ENT_ST_SEQUENCIA,"
                + "ENT_ST_STATUS, ENT_ST_DATAHORA, ENT_ST_USUARIO, ENT_ST_OBSERVACAO,DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.EntStatusID + ","
                + dados.EntControleID + ",";
            if (dados.EntStatusSequencia.Equals(0))
            {
                strCmd += IdentificaNovaSequencia(dados.EmpCodigo, dados.EstCodigo, dados.EntControleID) + ",";
            }
            else
            {
                strCmd += dados.EntStatusSequencia + ",";
            }

            strCmd += dados.EntStatus + ","
                + Funcoes.BDataHora(dados.EntStatusDataHora) + ",'"
                + dados.EntStatusUsuario + "','"
                + dados.EntStatusObservacao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int IdentificaNovaSequencia(int empCodigo, int estCodigo, int entControleID)
        {
            string strSql = "SELECT MAX(ENT_ST_SEQUENCIA) AS ENT_ST_SEQUENCIA FROM CONTROLE_ENTREGA_STATUS WHERE EMP_CODIGO = " + empCodigo
                + " AND EST_CODIGO = " + estCodigo + " AND ENT_CONTROLE_ID = " + entControleID;

            DataTable dt = BancoDados.selecionarRegistros(strSql);
            if (dt.Rows[0]["ENT_ST_SEQUENCIA"].ToString() != "")
            {
                return Convert.ToInt32(dt.Rows[0]["ENT_ST_SEQUENCIA"]) + 1;
            }
            else
                return 1;
        }
    }
}
