﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ModuloMenu
    {
        public string ModuloID { get; set; }
        public string Menu { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
        public string Formulario { get; set; }
        public string Atualiza { get; set; }

        public bool InsereRegistros(ModuloMenu dados)
        {
            string strCmd = "INSERT INTO MODULO_MENU (MODULO_ID, MENU,DESCRICAO,NOME,FORMULARIO) VALUES("
                + dados.ModuloID + ",'"
                + dados.Menu + "','"
                + dados.Descricao + "','"
                + dados.Nome + "','"
                + dados.Formulario + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public string BuscaSeExisteModulo(string moduloID)
        {
            string sql = "SELECT MODULO_ID FROM MODULO_MENU  WHERE MODULO_ID = " + moduloID;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

    }
}
