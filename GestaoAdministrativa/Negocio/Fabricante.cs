﻿using GestaoAdministrativa.Classes;
using SqlNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class Fabricante
    {
        public int EmpCodigo { get; set; }
        public int FabCodigo { get; set; }
        public string FabDescricao { get; set; }
        public string FabLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Fabricante() { }

        public Fabricante(int empCodigo, int fabCodigo, string fabDescricao, string fabLiberado,
                         DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.FabCodigo = fabCodigo;
            this.FabDescricao = FabDescricao;
            this.FabLiberado = FabLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable GetBuscar(Fabricante dadosBusca, out string strOrdem)
        {
            string strSql = " SELECT EMP_CODIGO, FAB_CODIGO, FAB_DESCRICAO, CASE FAB_DESABILITADO "
                          + " WHEN 'N' THEN 'S' "
                          + " ELSE 'N' END AS FAB_DESABILITADO, "
                          + " FAB_OPALTERACAO, FAB_OPCADASTRO, FAB_DTCADASTRO, FAB_DTALTERACAO"
                          + " FROM FABRICANTES WHERE EMP_CODIGO = " + dadosBusca.EmpCodigo;

            if (dadosBusca.FabCodigo != 0)
            {
                strSql += " AND FAB_CODIGO = " + dadosBusca.FabCodigo;
            }
            if (dadosBusca.FabDescricao != "")
            {
                strSql += " AND FAB_DESCRICAO LIKE '%" + dadosBusca.FabDescricao + "%'";
            }
            if (dadosBusca.FabLiberado != "TODOS")
            {
                string desab = dadosBusca.FabLiberado == "S" ? "N" : "S";
                strSql += " AND FAB_DESABILITADO = '" + desab + "'";
            }
            strOrdem = strSql;
            strSql += " ORDER BY FAB_CODIGO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InserirDados(Fabricante dados)
        {
            string sql = "INSERT INTO FABRICANTES(EMP_CODIGO, FAB_CODIGO, FAB_DESCRICAO, FAB_DESABILITADO, "
                       + " FAB_DTCADASTRO ,FAB_OPCADASTRO) "
                       + " VALUES (" + dados.EmpCodigo + "," + dados.FabCodigo + ",'" +
                        dados.FabDescricao + "','" + dados.FabLiberado + "'," + Funcoes.BDataHora(dados.DtCadastro) + ",'" + dados.OpCadastro + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("FAB_CODIGO", dados.FabCodigo.ToString(), Principal.usuario, "FABRICANTES", dados.FabDescricao, dados.EmpCodigo);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(Fabricante dadosNew, DataTable dadosOld)
        {
            MontadorSql mont = new MontadorSql("fabricantes", MontadorType.Update);

            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE FABRICANTES SET ";
            if (!dadosNew.FabDescricao.Equals(dadosOld.Rows[0]["FAB_DESCRICAO"]))
            {
                if (Util.RegistrosPorEmpresa("FABRICANTES", "FAB_DESCRICAO", dadosNew.FabDescricao, true,true).Rows.Count != 0)
                {
                    Principal.mensagem = "Fabricante já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd = strCmd + " fab_descricao = '" + dadosNew.FabDescricao + "',";

                dados[contMatriz, 0] = "FAB_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["FAB_DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.FabDescricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.FabLiberado.Equals(dadosOld.Rows[0]["FAB_DESABILITADO"]))
            {
                strCmd = strCmd + " FAB_DESABILITADO = '" + dadosNew.FabLiberado + "',";

                dados[contMatriz, 0] = "FAB_DESABILITADO";
                dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["FAB_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.FabLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "fab_dtalteracao";
                dados[contMatriz, 1] = dadosOld.Rows[0]["fab_dtalteracao"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["fab_dtalteracao"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "FAB_OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["FAB_OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["FAB_OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " fab_dtalteracao = " + Funcoes.BDataHora(dadosNew.DtAlteracao) + ",";
                strCmd += " FAB_OPALTERACAO = '" + dadosNew.OpAlteracao + "' ";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " AND FAB_CODIGO = " + dadosNew.FabCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("FAB_CODIGO", dadosNew.FabCodigo.ToString(), Principal.usuario, "FABRICANTES", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public bool ExcluirDados(string fabCodigo)
        {

            string strCmd = "DELETE FROM FABRICANTES WHERE FAB_CODIGO = " + fabCodigo;

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                return true;
            else
                return false;
        }
    }
}