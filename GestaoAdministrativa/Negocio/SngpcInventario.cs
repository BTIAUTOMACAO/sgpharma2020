﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcInventario
    {
        public DataTable BuscaInvetario(int estCodigo, int empCodigo)
        {
            string sql = " SELECT PRO.PROD_REGISTRO_MS , UPPER(PRO.PROD_UNIDADE) AS PROD_UNIDADE, PRO.PROD_CLASSE_TERAP, "
                       + " INV.LAC_INV_LOTE, INV.LAC_INV_QTDE "
                       + " FROM SNGPC_LAC_INVENTARIO INV "
                       + " INNER JOIN produtos PRO ON(PRO.PROD_CODIGO = INV.LAC_INV_CODIGO) "
                       + " WHERE EST_CODIGO = " + estCodigo
                       + " AND EMP_CODIGO = " + empCodigo;
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable RelatorioDeInventario(int estCodigo, int empCodigo)
        {
            string sql = " SELECT INV.LAC_INV_CODIGO AS PROD_CODIGO, PRO.PROD_DESCR, PRO.PROD_REGISTRO_MS , UPPER(PRO.PROD_UNIDADE) AS PROD_UNIDADE, "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANOS' ELSE  case  PRO.PROD_CLASSE_TERAP WHEN '2' THEN"
                       + " 'SUJEITO A CONTROLE ESPECIAL' ELSE '' END END AS PROD_CLASSE_TERAP, "
                       + " INV.LAC_INV_LOTE, INV.LAC_INV_QTDE "
                       + " FROM SNGPC_LAC_INVENTARIO INV "
                       + " INNER JOIN produtos PRO ON(PRO.PROD_CODIGO = INV.LAC_INV_CODIGO) "
                       + " WHERE EST_CODIGO = " + estCodigo
                       + " AND EMP_CODIGO = " + empCodigo
                       + " ORDER BY PRO.PROD_DESCR";
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
