﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class DrogabellaComanda
    {
        public long VendaId { get; set; }
        public double ValorAutorizado { get; set; }
        public double ValorDiferenca { get; set; }
        public String NumeroComanda { get; set; }

        public DrogabellaComanda() { }

        public DrogabellaComanda(long vendaID, double valorAutorizado, double valorDiferenca, string numeroComanda)
        {
            this.VendaId = vendaID;
            this.ValorAutorizado = valorAutorizado;
            this.ValorDiferenca = valorDiferenca;
            this.NumeroComanda = numeroComanda;
        }

        public DataTable IdentificaSeVendaEPorComanda(long vendaID )
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM DROGABELLA_COMANDA "
                + "WHERE VENDA_ID = " + vendaID;
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(DrogabellaComanda dados)
        {
            string strCmd = "INSERT INTO DROGABELLA_COMANDA(VENDA_ID, VALOR_AUTORIZADO,VALOR_DIFERENCA, NUMERO_COMANDA) VALUES ("
                + dados.VendaId + ","
                + Funcoes.BValor(dados.ValorAutorizado) + ","
                + Funcoes.BValor(dados.ValorDiferenca) + ","
                + dados.NumeroComanda + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluirDadosPorVendaId(long vendaID, bool transAberta = false)
        {
            string strCmd = "DELETE FROM DROGABELLA_COMANDA WHERE VENDA_ID = " + vendaID;
            
            if (!transAberta)
                return BancoDados.ExecuteNoQuery(strCmd, null);
            else
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }
    }
}
