﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class CobrancaParcela
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int CobrancaParcelaID { get; set; }
        public int CobrancaID { get; set; }
        public int CobrancaParcelaNum { get; set; }
        public double CobrancaParcelValor { get; set; }
        public double CobrancaParcelaSaldo { get; set; }
        public string CobrancaParcelaStatus { get; set; }
        public DateTime CobrancaParcelaVencimento { get; set; }
        public DateTime DtCadastro { get; set; }
        public DateTime DtRecebimento { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }

        public CobrancaParcela() { }

        public CobrancaParcela(int empCodigo, int estCodigo, int cobrancaParcelaID, int cobrancaID, int cobrancaParcelaNum, double cobrancaParcelaValor, double cobrancaParcelaSaldo,
            string cobrancaParcelaStatus,DateTime cobrancaParcelaVencimento, DateTime dtCadastro, DateTime dtRecebimento, string opCadastro, DateTime dtAlteracao, string opAlteracao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.CobrancaParcelaID = cobrancaParcelaID;
            this.CobrancaID = cobrancaID;
            this.CobrancaParcelaNum = cobrancaParcelaNum;
            this.CobrancaParcelValor = cobrancaParcelaValor;
            this.CobrancaParcelaSaldo = cobrancaParcelaSaldo;
            this.CobrancaParcelaStatus = cobrancaParcelaStatus;
            this.CobrancaParcelaVencimento = cobrancaParcelaVencimento;
            this.DtCadastro = dtCadastro;
            this.DtRecebimento = dtRecebimento;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
        }

        public bool InserirDados(CobrancaParcela dados)
        {
            string strCmd = "INSERT INTO COBRANCA_PARCELA(EMP_CODIGO,EST_CODIGO,COBRANCA_PARCELA_ID,COBRANCA_ID,COBRANCA_PARCELA_NUM,COBRANCA_PARCELA_VALOR,COBRANCA_PARCELA_SALDO,COBRANCA_PARCELA_STATUS,"
                + "DT_CADASTRO,OP_CADASTRO,COBRANCA_PARCELA_VENCIMENTO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.CobrancaParcelaID + ","
                + dados.CobrancaID + ","
                + dados.CobrancaParcelaNum + ","
                + Funcoes.BValor(dados.CobrancaParcelValor) + ","
                + Funcoes.BValor(dados.CobrancaParcelaSaldo) + ",'"
                + dados.CobrancaParcelaStatus + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "',"
                + Funcoes.BData(dados.CobrancaParcelaVencimento) + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaCobrancaParcelada(int clienteId, char status)
        {
            string sql = " SELECT COBRANCA_PARCELA_ID, COBRANCA_PARCELA_VENCIMENTO, COBRANCA_PARCELA_NUM, COBRANCA_PARCELA_VALOR, COBRANCA_PARCELA_SALDO, 0 AS ACRESCIMO, "
                       + " 0 AS  VALOR_ATUALIZADO,  DT_CADASTRO, COBRANCA_PARCELA_STATUS AS RD_PAGO,  DT_ALTERACAO, OP_ALTERACAO,  COBRANCA_ID"
                       + " FROM COBRANCA_PARCELA "
                       + " WHERE COBRANCA_PARCELA_STATUS = '" + status + "' AND COBRANCA_ID IN "
                       + " (SELECT COBRANCA_ID FROM COBRANCA C WHERE C.COBRANCA_STATUS = '" + status + "' AND C.COBRANCA_CF_ID = " + clienteId + ") "
                       + " ORDER BY COBRANCA_PARCELA_VENCIMENTO, DT_CADASTRO ";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaParcela(CobrancaParcela cobrancaParcela)
        {
            string sql = " UPDATE COBRANCA_PARCELA SET "
                       + " COBRANCA_PARCELA_STATUS = '" + cobrancaParcela.CobrancaParcelaStatus + "',"
                       + " COBRANCA_PARCELA_SALDO = '" + cobrancaParcela.CobrancaParcelaSaldo + "',"
                       + " DT_RECEBIMENTO = " + Funcoes.BDataHora(cobrancaParcela.DtRecebimento) + " , "
                       + " DT_ALTERACAO = " + Funcoes.BDataHora(cobrancaParcela.DtAlteracao) + ","
                       + " OP_ALTERACAO = '" + cobrancaParcela.OpAlteracao + "'"
                       + " WHERE COBRANCA_PARCELA_ID = " + cobrancaParcela.CobrancaParcelaID + " AND EMP_CODIGO = " + cobrancaParcela.EmpCodigo + " AND EST_CODIGO = " + cobrancaParcela.EstCodigo;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaParcelaPorData(DateTime dtRecebimento, char status)
        {
            string sql = " UPDATE COBRANCA_PARCELA CP SET "
                       + " CP.COBRANCA_PARCELA_STATUS = '"+ status + "',"
                       + " CP.DT_RECEBIMENTO = '',"
                       + " CP.COBRANCA_PARCELA_SALDO = COBRANCA_PARCELA_SALDO + (SELECT (CM.COBRANCA_MOV_VL_PAGO - CM.COBRANCA_MOV_VL_ACRESCIMO) FROM COBRANCA_MOVIMENTO CM WHERE CM.COBRANCA_PARCELA_ID = CP.COBRANCA_PARCELA_ID AND CM.COBRANCA_MOV_DT_BAIXA = " + Funcoes.BDataHora(dtRecebimento) + ")"
                       + " WHERE CP.DT_RECEBIMENTO = " + Funcoes.BDataHora(dtRecebimento);

            if (BancoDados.ExecuteNoQueryTrans(sql, null) >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public double SaldoDataMenorQueVencimento(int empCodigo, int estCodigo, DateTime data, int id)
        {
            string strSql = "SELECT COALESCE(SUM(A.COBRANCA_PARCELA_SALDO),0) AS SALDO"
                        + " FROM COBRANCA_PARCELA A"
                        + " INNER JOIN COBRANCA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                        + " WHERE B.EMP_CODIGO = " + empCodigo
                        + " AND B.EST_CODIGO = " + estCodigo
                        + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + " AND A.EST_CODIGO = B.EST_CODIGO"
                        + " AND A.COBRANCA_PARCELA_SALDO > 0"
                        + " AND A.COBRANCA_PARCELA_STATUS = 'A'"
                        + " AND A.COBRANCA_PARCELA_VENCIMENTO < " + Funcoes.BData(data)
                        + " AND B.COBRANCA_CF_ID = " + id;
            return Convert.ToDouble(BancoDados.ExecuteScalar(strSql, null));
        }

        public double SaldoDataMaiorQueVencimento(int empCodigo, int estCodigo, DateTime data, int id)
        {
            string strSql = "SELECT COALESCE(SUM(A.COBRANCA_PARCELA_SALDO),0) AS SALDO"
                        + " FROM COBRANCA_PARCELA A"
                        + " INNER JOIN COBRANCA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                        + " WHERE B.EMP_CODIGO = " + empCodigo
                        + " AND B.EST_CODIGO = " + estCodigo
                        + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + " AND A.EST_CODIGO = B.EST_CODIGO"
                        + " AND A.COBRANCA_PARCELA_SALDO > 0"
                        + " AND A.COBRANCA_PARCELA_STATUS = 'A'"
                        + " AND A.COBRANCA_PARCELA_VENCIMENTO >= " + Funcoes.BData(data)
                        + " AND B.COBRANCA_CF_ID = " + id;
            return Convert.ToDouble(BancoDados.ExecuteScalar(strSql, null));
        }

        public bool CancelaCobrancaParcela(int estCodigo, int empCodigo, string idCobranca, DateTime dtAlteracao, string operador)
        {
            string strCmd = "UPDATE COBRANCA_PARCELA SET COBRANCA_PARCELA_STATUS = 'C', DT_ALTERACAO = " + Funcoes.BDataHora(dtAlteracao) + ", OP_ALTERACAO = '"
                + operador + "' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND COBRANCA_ID = " + idCobranca;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaCobrancaParcelaPorCliente(string clienteId, DateTime dtInicial, DateTime dtFinal, int empCodigo, int estCodigo)
        {
            string sql = " SELECT A.COBRANCA_CF_ID AS ID, C.VENDA_DATA,"
                        + "       C.VENDA_ID,"
                        + "        B.COBRANCA_PARCELA_VENCIMENTO,"
                        + "        B.COBRANCA_PARCELA_SALDO"
                        + "   FROM COBRANCA A"
                        + "  INNER JOIN COBRANCA_PARCELA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                        + "  INNER JOIN VENDAS C ON A.COBRANCA_VENDA_ID = C.VENDA_ID"
                        + "  WHERE B.COBRANCA_PARCELA_STATUS <> 'T'"
                        + "  AND B.COBRANCA_PARCELA_SALDO <> 0"
                        + "  AND B.COBRANCA_PARCELA_VENCIMENTO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal)
                        + "  AND A.COBRANCA_CF_ID = " + clienteId
                        + "  AND A.EMP_CODIGO = " + empCodigo
                        + "  AND A.EST_CODIGO = " + estCodigo
                        + "  AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + "  AND A.EST_CODIGO = B.EST_CODIGO"
                        + "  AND A.EMP_CODIGO = C.EMP_CODIGO"
                        + "  AND A.EST_CODIGO = C.EST_CODIGO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaCobrancaParcelaPorID(int idCobranca, int empCodigo, int estCodigo)
        {
            string sql = " SELECT A.*"
                        + "   FROM COBRANCA_PARCELA A "
                        + "  WHERE A.EMP_CODIGO = " + empCodigo
                        + "  AND A.EST_CODIGO = " + estCodigo
                        + "  AND A.COBRANCA_ID = " + idCobranca;

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
