﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Entrega
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int PedNumero { get; set; }
        public string PedCompl { get; set; }
        public int ColCodigo { get; set; }
        public int EntRomaneio { get; set; }
        public int EntStatus { get; set; }
        public DateTime EntData { get; set; }
        public string EntEndereco { get; set; }
        public string EntBairro { get; set; }
        public int EntCep { get; set; }
        public string EntCidade { get; set; }
        public string EntUf { get; set; }
        public int EntFormaPagto { get; set; }
        public double EntValorTroco { get; set; }
        public int EntNumEntrega { get; set; }
        public string EntObservacao { get; set; }
        
        public Entrega() { }


        public DataTable UltimasEntregas(string cfDocto, int estCodigo, int empCodigo)
        {
            string strSql;

            strSql = "SELECT A.ENT_ENDER, A.ENT_BAIRRO, A.ENT_CEP, A.ENT_CIDADE, A.ENT_UF "
             + " FROM ENTREGAS A"
             + " INNER JOIN PEDIDOS B ON (A.PED_NUMERO = B.PED_NUMERO AND A.PED_COMPL = B.PED_COMPL)"
             + " WHERE A.EMP_CODIGO = " + empCodigo
             + " AND A.EST_CODIGO = " + estCodigo
             + " AND A.EMP_CODIGO = B.EMP_CODIGO"
             + " AND A.EST_CODIGO = B.EST_CODIGO"
             + " AND B.CF_DOCTO = '" + cfDocto + "'"
             + " order by a.ent_data desc";
            
            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
