﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class RelatorioFechaDiario
    {
        public DataTable MovimentoPorEspecie(DateTime dtInicial, DateTime dtFinal, string usuario, bool zerados = true, bool relCaixa = false)
        {
            string sql = " SELECT COALESCE(SUM(TOTAL), 0) AS TOTAL, ESP. ESP_DESCRICAO, ESP.ESP_CODIGO"
                       + "    FROM(SELECT DISTINCT(VE.ESP_CODIGO), SUM(VE.VENDA_ESPECIE_VALOR) AS Total, VE.VENDA_ID "
                       + "            FROM VENDAS_ESPECIES VE "
                       + "           INNER JOIN CAD_ESPECIES E ON(E.ESP_CODIGO = VE.ESP_CODIGO) "
                       + "           INNER JOIN VENDAS VEN ON (VEN.VENDA_ID = VE.VENDA_ID)"
                       + "           WHERE (VE.OP_CADASTRO = '" + usuario + "' OR  VE.OP_CADASTRO = '" + usuario + " ') "
                       + "             AND VE.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + "             AND VEN.EMP_CODIGO = VE.EMP_CODIGO"
                       + "             AND VEN.EST_CODIGO = VE.EST_CODIGO"
                       + "             AND VEN.VENDA_STATUS = 'F'"
                       + "           GROUP BY VE.ESP_CODIGO,VE.VENDA_ID, VE.VENDA_ESPECIE_ID "
                     // + "          UNION  "
                      // + "          SELECT  MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO,"
                      // + "          SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS Total, "
                      // + "          0 "
                      // + "            FROM MOVIMENTO_CAIXA_ESPECIE MCE, MOVIMENTO_CAIXA MC "
                      // + "           WHERE (MCE.OP_CADASTRO  = '" + usuario + "' or MCE.OP_CADASTRO  = '" + usuario + " ')"
                      // + "             AND MCE.MOVIMENTO_CX_ESPECIE_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal) + " AND "
                       //+ "     MC.EMP_CODIGO = MCE.EMP_CODIGO AND "
                     //  + "     MC.EST_CODIGO = MCE.EST_CODIGO AND "
                      // + "     MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA AND "
                      // + "     MC.MOVIMENTO_CAIXA_ODC_CLASSE NOT IN('-')  AND MC.MOVIMENTO_CAIXA_ODC_ID NOT IN (" + Funcoes.LeParametro(6, "76", false) + "," + Funcoes.LeParametro(6, "75", false) + ")"
                       //+ "           GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO
                       + " ) T "
                       + "   RIGHT JOIN CAD_ESPECIES ESP ON (T.ESP_CODIGO = ESP.ESP_CODIGO) ";
            if (!zerados.Equals(true))
            {
                sql += " WHERE TOTAL > 0 ";
            }

            if(relCaixa)
            {
                if(zerados)
                {
                    sql += " WHERE ESP.ESP_SAT <> 99";
                }
                else
                    sql += " AND ESP.ESP_SAT <> 99";
            }
            sql += "   GROUP BY ESP.ESP_DESCRICAO, ESP.ESP_CODIGO "
            + "   ORDER BY ESP.ESP_DESCRICAO ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable MovimentoPorEspecieEntrada(DateTime dtInicial, DateTime dtFinal, string usuario, bool zerados = true, bool relCaixa = false)
        {
            string sql = " SELECT COALESCE(SUM(TOTAL), 0) AS TOTAL, ESP. ESP_DESCRICAO, ESP.ESP_CODIGO"
                       + "    FROM("
                       + "          SELECT  MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO,"
                       + "          SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS Total, "
                       + "          0 "
                       + "            FROM MOVIMENTO_CAIXA_ESPECIE MCE, MOVIMENTO_CAIXA MC "
                       + "           WHERE (MCE.OP_CADASTRO  = '" + usuario + "' or MCE.OP_CADASTRO  = '" + usuario + " ')"
                       + "             AND MCE.MOVIMENTO_CX_ESPECIE_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal) + " AND "
                       + "     MC.EMP_CODIGO = MCE.EMP_CODIGO AND "
                       + "     MC.EST_CODIGO = MCE.EST_CODIGO AND "
                       + "     MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA AND "
                       + "     MC.MOVIMENTO_CAIXA_ODC_CLASSE NOT IN('-')  AND MC.MOVIMENTO_CAIXA_ODC_ID NOT IN (" + Funcoes.LeParametro(6, "76", false) + "," + Funcoes.LeParametro(6, "75", false) + ")"
                       + "           GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO) T "
                       + "   RIGHT JOIN CAD_ESPECIES ESP ON (T.ESP_CODIGO = ESP.ESP_CODIGO) ";
            if (!zerados.Equals(true))
            {
                sql += " WHERE TOTAL > 0 ";
            }

            if (relCaixa)
            {
                if (zerados)
                {
                    sql += " WHERE ESP.ESP_SAT <> 99";
                }
                else
                    sql += " AND ESP.ESP_SAT <> 99";
            }
            sql += "   GROUP BY ESP.ESP_DESCRICAO, ESP.ESP_CODIGO "
            + "   ORDER BY ESP.ESP_DESCRICAO ";

             return BancoDados.GetDataTable(sql, null);
        }

        public DataTable MovimentoPorEspecieBeneficio(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT COALESCE(SUM(TOTAL), 0) AS TOTAL, ESP. ESP_DESCRICAO, ESP.ESP_CODIGO"
                       + "    FROM(SELECT DISTINCT(VE.ESP_CODIGO), SUM(VE.VENDA_ESPECIE_VALOR) AS Total, ve.venda_id"
                       + "            FROM VENDAS_ESPECIES VE "
                       + "           INNER JOIN CAD_ESPECIES E ON(E.ESP_CODIGO = VE.ESP_CODIGO) "
                       + "           INNER JOIN VENDAS VEN ON (VEN.VENDA_ID = VE.VENDA_ID)"
                       + "           WHERE  (VE.OP_CADASTRO = '" + usuario + "' OR  VE.OP_CADASTRO = '" + usuario + " ')  "
                       + "             AND VE.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + "             AND VEN.EMP_CODIGO = VE.EMP_CODIGO"
                       + "             AND VEN.EST_CODIGO = VE.EST_CODIGO"
                       + "             AND VEN.VENDA_STATUS = 'F'"
                       + "          GROUP BY VE.ESP_CODIGO,VE.VENDA_ID, VE.VENDA_ESPECIE_ID "
                       + "          UNION  "
                       + "          SELECT   MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO, "
                       + "          SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS Total , 0 as venda_id"  
                       + "            FROM MOVIMENTO_CAIXA_ESPECIE MCE, MOVIMENTO_CAIXA MC "
                       + "           WHERE MCE.OP_CADASTRO  = '" + usuario + "' "
                       + "             AND MCE.MOVIMENTO_CX_ESPECIE_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal) + " AND "
                       + "     MC.EMP_CODIGO = MCE.EMP_CODIGO AND "
                       + "     MC.EST_CODIGO = MCE.EST_CODIGO AND "
                       + "     MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA AND "
                       + "     MC.MOVIMENTO_CAIXA_ODC_CLASSE NOT IN('-')  AND MCE.MOVIMENTO_FINANCEIRO_ID = 0"
                       + "           GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO) T "
                       + "   RIGHT JOIN CAD_ESPECIES ESP ON (T.ESP_CODIGO = ESP.ESP_CODIGO) ";
            sql += " WHERE ESP.ESP_SAT = 99";
            sql += "   GROUP BY ESP.ESP_DESCRICAO, ESP.ESP_CODIGO "
            + "   ORDER BY ESP.ESP_DESCRICAO ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable MovimentoPorFormaPagamento(DateTime dtInicial, DateTime dtFinal, string usuario, bool zerados = true)
        {
            string sql = " SELECT coalesce(SUM(VFP.VENDA_VALOR_PARCELA),0) as VendaValor,  FP.FORMA_ID, FP.OPERACAO_CAIXA, "
                       + " FP.FORMA_DESCRICAO as FormaDescricao "
                       + " FROM FORMAS_PAGAMENTO FP "
                       + " INNER JOIN VENDAS_FORMA_PAGAMENTO VFP ON (FP.FORMA_ID = VFP.VENDA_FORMA_ID "
                       + " AND VFP.OP_CADASTRO = '" + usuario + "' AND VFP.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal) + ")"
                       + " INNER JOIN VENDAS VEN ON (VFP.VENDA_ID = VEN.VENDA_ID AND VFP.EMP_CODIGO = VEN.EMP_CODIGO AND VFP.EST_CODIGO = VEN.EST_CODIGO  AND VEN.VENDA_STATUS = 'F')";
                        if (!zerados.Equals(true))
                        {
                            sql += " HAVING SUM(VENDA_VALOR_PARCELA) > 0";
                        }
                        sql += " GROUP BY VFP.VENDA_FORMA_ID, FP.FORMA_DESCRICAO, FP.FORMA_ID,FP.OPERACAO_CAIXA"
                            + " ORDER BY FP.FORMA_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable RecargaDeCelular(DateTime dtInicial, DateTime dtFinal, string usuario, string codDepartamento)
        {
            string sql = "SELECT COALESCE(SUM(B.VENDA_ITEM_TOTAL),0) AS TOTAL"
                         + "   FROM VENDAS A"
                         + "   INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                         + "   INNER JOIN PRODUTOS_DETALHE C ON C.PROD_CODIGO = B.PROD_CODIGO"
                         + "   WHERE A.EMP_CODIGO = " + Principal.empAtual
                         + "   AND A.EST_CODIGO = " + Principal.estAtual
                         + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                         + "   AND A.EST_CODIGO = B.EST_CODIGO"
                         + "   AND C.EMP_CODIGO = A.EMP_CODIGO"
                         + "   AND C.DEP_CODIGO = " + codDepartamento
                         + "   AND (A.OP_CADASTRO = '" + usuario + "' or A.OP_ALTERACAO = '" + usuario + "')"
                         + "   AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            return BancoDados.GetDataTable(sql, null);
        }

        #region Relatorio Analitico
        public DataTable VendasPorPeriodo(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT SUM(VENDA_ESPECIE_VALOR) AS VENDA_ESPECIE_VALOR,VENDA_ID, CLIENTE, ESP_DESCRICAO FROM ("
                       + " SELECT DISTINCT(VE.ESP_CODIGO), VEN.VENDA_ID, "
                       + " SUBSTR(CF.CF_NOME, 1, 10) AS CLIENTE, "
                       + " SUBSTR(ESP.ESP_DESCRICAO , 1, 8) AS ESP_DESCRICAO, "
                       + " VE.VENDA_ESPECIE_VALOR "
                       + " FROM VENDAS VEN "
                       + " INNER JOIN CLIFOR CF ON(CF.CF_ID = VEN.VENDA_CF_ID) "
                       + " INNER JOIN VENDAS_ESPECIES VE ON(VE.VENDA_ID = VEN.VENDA_ID) "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = VE.ESP_CODIGO) "
                       + " WHERE VEN.VENDA_EMISSAO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND (VEN.OP_CADASTRO = '" + usuario + "' OR VEN.OP_ALTERACAO = '" + usuario + "') AND VEN.VENDA_STATUS = 'F' ORDER BY VEN.VENDA_ID)"
                       + " GROUP BY VENDA_ID, CLIENTE, ESP_DESCRICAO ORDER BY VENDA_ID";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable RecebimentosPorPeriodo(DateTime dtInicial, DateTime dtFinal, string usuario)
        {

            string sql = " SELECT CM.COBRANCA_ID, CM.COBRANCA_MOV_VL_PAGO, CM.COBRANCA_MOV_VL_ACRESCIMO , SUBSTR(CF.CF_NOME,1 ,15) AS CLIENTE FROM COBRANCA_MOVIMENTO CM "
                       + " INNER JOIN COBRANCA C ON(C.COBRANCA_ID = CM.COBRANCA_ID) "
                       + " INNER JOIN CLIFOR CF ON(CF.CF_ID = C.COBRANCA_CF_ID) "
                       + " WHERE COBRANCA_MOV_DT_BAIXA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND CM.OP_CADASTRO = '" + usuario + "'"
                       + " ORDER BY CM.COBRANCA_MOV_DT_BAIXA DESC ";

            return BancoDados.GetDataTable(sql, null);

        }
        #endregion

    }
}
