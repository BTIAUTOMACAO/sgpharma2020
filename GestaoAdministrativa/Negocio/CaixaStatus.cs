﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using SqlNegocio;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class CaixaStatus
    {
        public int EmpCodigo { get; set; }
        public string LoginId { get; set; }
        public DateTime CxData { get; set; }
        public decimal CxSaldo { get; set; }
        public string CxStatus { get; set; }
        public string CxFundo { get; set; }

        public CaixaStatus() { }

        public CaixaStatus(int estCodigo, string loginId, DateTime cxData, decimal cxSaldo, string cxStatus, string cxFundo)
        {
            this.EmpCodigo = estCodigo;
            this.LoginId = loginId;
            this.CxData = cxData;
            this.CxSaldo = cxSaldo;
            this.CxStatus = cxStatus;
            this.CxFundo = cxFundo;


        }

        public List<CaixaStatus> DataCaixa(int empCodigo, string status, string usuarioID)
        {
            string strSql = "SELECT MAX(DATA_ABERTURA) AS DATA_ABERTURA, CAIXA_ABERTO FROM CAIXA_ABERTO WHERE EMP_CODIGO  = " + empCodigo +
                 " AND CAIXA_ABERTO = '" + status + "' AND USUARIO = '" + usuarioID + "' GROUP BY CAIXA_ABERTO";

            DataTable table = BancoDados.GetDataTable(strSql, null);

            List<CaixaStatus> lista = new List<CaixaStatus>();
            foreach (DataRow row in table.Rows)
            {
                CaixaStatus cStatus = new CaixaStatus();
                cStatus.CxData = Convert.ToDateTime(row["DATA_ABERTURA"]);
                cStatus.CxStatus = row["CAIXA_ABERTO"].ToString();
                lista.Add(cStatus);
            }

            return lista;
        }

        public DataTable MovimentoPorEspecieDoDia(DateTime dtInicial, DateTime dtFinal)
        {
            string sql = " SELECT COALESCE(SUM(TOTAL), 0) AS TOTAL, ESP. ESP_DESCRICAO"
                       + "    FROM(SELECT DISTINCT(VE.ESP_CODIGO), SUM(VE.VENDA_ESPECIE_VALOR) AS Total, VE.VENDA_ID "
                       + "            FROM VENDAS_ESPECIES VE "
                       + "           INNER JOIN CAD_ESPECIES E ON(E.ESP_CODIGO = VE.ESP_CODIGO) "
                       + "           INNER JOIN VENDAS VEN ON (VEN.VENDA_ID = VE.VENDA_ID)"
                       + "           WHERE VE.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial.ToString("dd/MM/yyyy") + " 00:00:00"))
                       + "             AND " + Funcoes.BDataHora(Convert.ToDateTime(dtFinal.ToString("dd/MM/yyyy") + " 23:59:59"))
                       + "             AND VEN.EMP_CODIGO = VE.EMP_CODIGO"
                       + "             AND VEN.EST_CODIGO = VE.EST_CODIGO"
                       + "             AND VEN.VENDA_STATUS = 'F' "
                       + "           GROUP BY VE.ESP_CODIGO,VE.VENDA_ID, VE.VENDA_ESPECIE_ID "
                       + "          UNION  "
                       + "          SELECT MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO,"
                       + "          SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS Total, "
                       + "                  0 "
                       + "            FROM MOVIMENTO_CAIXA_ESPECIE MCE, MOVIMENTO_CAIXA MC "
                       + "           WHERE MCE.MOVIMENTO_CX_ESPECIE_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial.ToString("dd/MM/yyyy") + " 00:00:00"))
                       + "           AND " + Funcoes.BDataHora(Convert.ToDateTime(dtFinal.ToString("dd/MM/yyyy") + " 23:59:59")) + " AND "
                       + "     MC.EMP_CODIGO = MCE.EMP_CODIGO AND "
                       + "     MC.EST_CODIGO = MCE.EST_CODIGO AND "
                       + "     MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA  AND "
                       + "     MC.MOVIMENTO_CAIXA_ODC_CLASSE NOT IN('-') AND MC.MOVIMENTO_CAIXA_ODC_ID IN (" + Funcoes.LeParametro(4, "13", false) + ")"
                       + "           GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO) T "
                       + "   RIGHT JOIN CAD_ESPECIES ESP ON (T.ESP_CODIGO = ESP.ESP_CODIGO) "
                       + "   WHERE ESP.ESP_SAT <> 99"
                       + "   GROUP BY ESP.ESP_DESCRICAO, ESP.ESP_CODIGO "
                       + "   ORDER BY ESP.ESP_DESCRICAO ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaMovimentoPorOperacaoCaixa(List<object> excecoes, DateTime dtInicial, DateTime dtFinal)
        {
            string operacaoDCExcecoes = string.Join(" ,", excecoes);

            string sql = " SELECT SUM(MC.MOVIMENTO_CAIXA_VALOR) AS TOTAL, ESP.ESP_DESCRICAO from MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE ME ON (MC.MOVIMENTO_CAIXA_DATA = ME.MOVIMENTO_CX_ESPECIE_DATA)"
                       + " INNER JOIN CAD_ESPECIES ESP ON(ME.MOVIMENTO_CX_ESPECIE_CODIGO = ESP.ESP_CODIGO)"
                       + " WHERE MOVIMENTO_CAIXA_ODC_ID NOT IN(" + operacaoDCExcecoes + ")  "
                       + " AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' "
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dtInicial.ToString("dd/MM/yyyy") + " 00:00:00"))
                       + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dtFinal.ToString("dd/MM/yyyy") + " 23:59:59"))
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " GROUP BY ODC.ODC_DESCRICAO, ESP.ESP_DESCRICAO"
                       + " ORDER BY  ESP.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaDespesas(DateTime vencimento, string status, int empCodigo, bool todos)
        {
            string sql = " SELECT A.PAG_DOCTO, A.CF_DOCTO, C.CF_NOME, B.PD_PARCELA, B.PD_SALDO, B.PD_VENCTO, B.PD_STATUS, COALESCE(A.PAG_ORIGEM,'E') AS PAG_ORIGEM"
                        + "    FROM PAGAR A"
                        + "    INNER JOIN PAGAR_DETALHE B ON A.PAG_CODIGO = B.PAG_CODIGO"
                        + "    INNER JOIN CLIFOR C ON A.CF_DOCTO = C.CF_DOCTO"
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + "    AND B.PD_STATUS = '" + status + "'";
            if(!todos)
            {
                sql += " AND PD_VENCTO = " + Funcoes.BData(vencimento);
            }
            else
            {
                sql += " AND PD_VENCTO <= " + Funcoes.BData(vencimento);
            }

            sql += " ORDER BY PD_VENCTO, PD_SALDO";
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
