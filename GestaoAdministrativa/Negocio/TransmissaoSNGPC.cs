﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class TransmissaoSNGPC
    {
        public string GerarArquivoZIP(string caminhoZIP, string arquivoXML, string arquivoZip)
        {

            // VERIFICA SE O ARQUIVO .ZIP EXISTE SE EXISTIR DELETAR 
            if (System.IO.File.Exists(caminhoZIP + arquivoXML.Replace("xml", "zip")))
            {
                System.IO.File.Delete(caminhoZIP + arquivoXML.Replace("xml", "zip"));
            }
            //GERA ZIP VAZIO
            using (ZipArchive archive = ZipFile.Open(caminhoZIP + arquivoXML.Replace("xml", "zip"), ZipArchiveMode.Create))
            {
                //ADICIONA UM ARQUIVO NO ZIP
                archive.CreateEntryFromFile(
                     caminhoZIP + arquivoXML,
                     arquivoXML,
                    CompressionLevel.Optimal);
            }

            return caminhoZIP + arquivoZip;
        }


        public string CalculateMD5Hash(string arquivo)
        {

            // Primeiro passo, calcular o MD5 hash a partir da string
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(arquivo);
            byte[] hash = md5.ComputeHash(inputBytes);

            // Segundo passo, converter o array de bytes em uma string haxadecimal
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


    }
}


