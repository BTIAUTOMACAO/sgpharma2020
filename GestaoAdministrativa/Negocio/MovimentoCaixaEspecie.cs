﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class MovimentoCaixaEspecie
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long MovimentoCxEspecieID { get; set; }
        public DateTime MovimentoCxEspecieData { get; set; }
        public int MovimentoCxEspecieCodigo { get; set; }
        public double MovimentoCxEspecieValor { get; set; }
        public string MovimentoCxEspecieUsuario { get; set; }
        public long MovimentoFinanceiroID { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public MovimentoCaixaEspecie() { }

        public MovimentoCaixaEspecie(int empCodigo, int estCodigo, long movimentoCxEspecieID, DateTime movimentoCxEspecieData, int movimentoCxEspecieCodigo, double movimentoCxEspecieValor,
            string movimentoCxEspecieUsuario, long movimentoFinanceiroID, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.MovimentoCxEspecieID = movimentoCxEspecieID;
            this.MovimentoCxEspecieData = movimentoCxEspecieData;
            this.MovimentoCxEspecieCodigo = movimentoCxEspecieCodigo;
            this.MovimentoCxEspecieValor = movimentoCxEspecieValor;
            this.MovimentoCxEspecieUsuario = movimentoCxEspecieUsuario;
            this.MovimentoFinanceiroID = movimentoFinanceiroID;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InserirDados(MovimentoCaixaEspecie dados)
        {
            string strCmd = "INSERT INTO MOVIMENTO_CAIXA_ESPECIE(EMP_CODIGO,EST_CODIGO,MOVIMENTO_CX_ESPECIE_ID,MOVIMENTO_CX_ESPECIE_DATA,MOVIMENTO_CX_ESPECIE_CODIGO,MOVIMENTO_CX_ESPECIE_VALOR,"
                + "MOVIMENTO_CX_ESPECIE_USUARIO,MOVIMENTO_FINANCEIRO_ID,DT_CADASTRO,OP_CADASTRO) VALUES(" + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.MovimentoCxEspecieID + ","
                + Funcoes.BDataHora(dados.MovimentoCxEspecieData) + ","
                + dados.MovimentoCxEspecieCodigo + ","
                + Funcoes.BValor(dados.MovimentoCxEspecieValor) + ",'"
                + dados.MovimentoCxEspecieUsuario + "',"
                + dados.MovimentoFinanceiroID + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable VerficaMovimentoCaixaEspecie(string usuario, DateTime dtInicial, DateTime dtFinal)
        {

            string sql = " SELECT * FROM MOVIMENTO_CAIXA_ESPECIE WHERE MOVIMENTO_CX_ESPECIE_USUARIO ='" + usuario + "' AND "
                       + " MOVIMENTO_CX_ESPECIE_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            return BancoDados.GetDataTable(sql, null);

        }
        public bool ExcluirMovimentoCaixaEspeciePorDataHora(DateTime dtMovimentoCaixa)
        {
            string sql = "DELETE FROM MOVIMENTO_CAIXA_ESPECIE WHERE MOVIMENTO_CX_ESPECIE_DATA = " + Funcoes.BDataHora(dtMovimentoCaixa);

            try
            {
                BancoDados.ExecuteNoQueryTrans(sql, null);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataTable BuscaEntradaSaidaPorEspecie(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT SUM(A.ENTRADA) AS ENTRADA, SUM(A.SAIDA) AS SAIDA, A.ESP_DESCRICAO FROM ("
                       + " SELECT DISTINCT MC.MOVIMENTO_CAIXA_ID, COALESCE(SUM(CASE  WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '+' THEN MCE.MOVIMENTO_CX_ESPECIE_VALOR END),0) AS ENTRADA, "
                       + " COALESCE(SUM(CASE  WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' THEN MCE.MOVIMENTO_CX_ESPECIE_VALOR END), 0) AS SAIDA, "
                       + " ESP.ESP_DESCRICAO "
                       + " FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " INNER JOIN MOVIMENTO_CAIXA MC ON(MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA) "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " WHERE (MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " GROUP BY ESP.ESP_DESCRICAO ,MC.MOVIMENTO_CAIXA_ID "
                       + " ORDER BY ESP_DESCRICAO ) A  GROUP BY A.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);

        }
        public DataTable BuscaEntradaSaidaPorOperacao(DateTime dtInicial, DateTime dtFinal, string usuario, int caixaEspecie)
        {
            string sql = " SELECT distinct MC.MOVIMENTO_CAIXA_ID, COALESCE(CASE  WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '+' THEN MCE.MOVIMENTO_CX_ESPECIE_VALOR END, 0) AS ENTRADA, "
                       + " COALESCE(CASE  WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' THEN MCE.MOVIMENTO_CX_ESPECIE_VALOR END, 0) AS SAIDA, "
                       + " MC.MOVIMENTO_CAIXA_DATA, ODC.ODC_DESCRICAO, "
                       + " MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO "
                       + " FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN MOVIMENTO_CAIXA MC ON(MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA) "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " WHERE (MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND MCE.MOVIMENTO_CX_ESPECIE_CODIGO = " + caixaEspecie 
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " ORDER BY MOVIMENTO_CAIXA_DATA";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable CalculaMovimentoPorEspecie(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT COALESCE(SUM(TOTAL), 0) AS ENTRADA,"
                        + "               ESP.ESP_DESCRICAO,"
                        + "               COALESCE(B.SAIDA, 0) AS SAIDA"
                        + "          FROM(SELECT DISTINCT(VE.ESP_CODIGO),"
                        + "                                SUM(VE.VENDA_ESPECIE_VALOR) AS Total,"
                        + "                                VE.VENDA_ID"
                        + "                  FROM VENDAS_ESPECIES VE"
                        + "                 INNER JOIN CAD_ESPECIES E ON(E.ESP_CODIGO = VE.ESP_CODIGO)"
                        + "                 INNER JOIN VENDAS VEN ON(VEN.VENDA_ID = VE.VENDA_ID)"
                        + "                 WHERE(VE.OP_CADASTRO = '" + usuario + "' OR VE.OP_CADASTRO = '" + usuario + " ')"
                        + "                   AND VE.DT_CADASTRO BETWEEN"
                        + "                       " + Funcoes.BDataHora(dtInicial) + " AND "
                        + "                       " + Funcoes.BDataHora(dtFinal)
                        + "                   AND VEN.EMP_CODIGO = VE.EMP_CODIGO"
                        + "                   AND VEN.EST_CODIGO = VE.EST_CODIGO"
                        + "                   AND VEN.VENDA_STATUS = 'F'"
                        + "                 GROUP BY VE.ESP_CODIGO, VE.VENDA_ID, VE.VENDA_ESPECIE_ID"
                        + "                UNION"
                        + "                SELECT MCE.MOVIMENTO_CX_ESPECIE_CODIGO AS ESP_CODIGO,"
                        + "                       SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS Total,"
                        + "                       0"
                        + "                  FROM MOVIMENTO_CAIXA_ESPECIE MCE, MOVIMENTO_CAIXA MC"
                        + "                 WHERE (MCE.OP_CADASTRO = '" + usuario + "' OR MCE.OP_CADASTRO = '" + usuario + " ')"
                        + "                   AND MCE.MOVIMENTO_CX_ESPECIE_DATA BETWEEN"
                        + "                       " + Funcoes.BDataHora(dtInicial) + " AND "
                        + "                       " + Funcoes.BDataHora(dtFinal)
                        + "                   AND MC.EMP_CODIGO = MCE.EMP_CODIGO"
                        + "                   AND MC.EST_CODIGO = MCE.EST_CODIGO"
                        + "                   AND MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA"
                        + "                   AND MC.MOVIMENTO_CAIXA_ODC_CLASSE NOT IN('-')"
                        + "                   AND MC.MOVIMENTO_CAIXA_ODC_ID NOT IN(" + Funcoes.LeParametro(6,"75", false) + ")"
                        + "                 GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO) T"
                        + "         RIGHT JOIN CAD_ESPECIES ESP ON (T.ESP_CODIGO = ESP.ESP_CODIGO)"
                        + "          LEFT JOIN((SELECT COALESCE(SUM(CASE"
                        + "                                           WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' THEN"
                        + "                                            MCE.MOVIMENTO_CX_ESPECIE_VALOR"
                        + "                                         END),"
                        + "                                     0) AS SAIDA,"
                        + "                            MCE.MOVIMENTO_CX_ESPECIE_CODIGO"
                        + "                       FROM MOVIMENTO_CAIXA_ESPECIE MCE"
                        + "                      INNER JOIN MOVIMENTO_CAIXA MC ON(MC.MOVIMENTO_CAIXA_DATA ="
                        + "                                                       MCE.MOVIMENTO_CX_ESPECIE_DATA)"
                        + "                      RIGHT JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO ="
                        + "                                                 MCE.MOVIMENTO_CX_ESPECIE_CODIGO)"
                        + "                      WHERE (MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' OR MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                        + "                        AND MC.MOVIMENTO_CAIXA_DATA BETWEEN"
                        + "                            " + Funcoes.BDataHora(dtInicial) + " AND "
                        + "                            " + Funcoes.BDataHora(dtFinal)
                        + "                        AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-'"
                        + "                      GROUP BY MCE.MOVIMENTO_CX_ESPECIE_CODIGO, ESP.ESP_DESCRICAO)) B ON (T.ESP_CODIGO ="
                        + "                                                                                         B.MOVIMENTO_CX_ESPECIE_CODIGO)"
                        + "         GROUP BY ESP.ESP_DESCRICAO, ESP.ESP_CODIGO, B.SAIDA"
                        + "         ORDER BY ESP.ESP_DESCRICAO";
            return BancoDados.GetDataTable(sql, null);
        }
        //public DataTable BuscaRecebimentoParticularPorData(List<object> dtRecebimentos, string usuario)
        //{

        //    string sql = " SELECT SUM(MOVIMENTO_CX_ESPECIE_VALOR), ESP.ESP_DESCRICAO FROM MOVIMENTO_CAIXA_ESPECIE MCE "
        //               + " INNER JOIN ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
        //               + " WHERE MCE.OP_CADASTRO = '" + usuario + "' AND MCE.MOVIMENTO_CX_ESPECIE_DATA IN ( " +
        //               +" GROUP BY ESP.ESP_DESCRICAO";

        //    return BancoDados.GetDataTable(sql, null);

        //  }

    }
}
