﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class TipoUnidade
    {
        public int UniCodigo { get; set; }
        public string UniDescricao { get; set; }
        public string UniDescAbrev { get; set; }
        public string UniLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public TipoUnidade() { }

        public TipoUnidade(int uniCodigo, string uniDescricao, string uniDescrAbrev, string uniLiberado,  
            DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.UniCodigo = uniCodigo;
            this.UniDescricao = uniDescricao;
            this.UniDescAbrev = uniDescrAbrev;
            this.UniLiberado = uniLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public DataTable BuscarDados(string uniCodigo, string uniDescricao, string uniLiberado, out string strOrdem)
        {
            string strSql = "SELECT UNI_CODIGO, UNI_DESCRICAO, UNI_DESC_ABREV, A.UNI_LIBERADO, A.DTALTERACAO, "
                       + " A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO FROM TIPO_UNIDADE A WHERE 1=1";

                if (uniCodigo != "")
                {
                    strSql += " AND A.UNI_CODIGO = " + uniCodigo;
                }
                if (uniDescricao != "")
                {
                    strSql += " AND A.UNI_DESCRICAO LIKE '%" + uniDescricao + "%'";
                }
                if (uniLiberado != "TODOS")
                {
                    string desab = uniLiberado == "S" ? "S" : "N";
                    strSql += " AND A.UNI_LIBERADO = '" + desab + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.UNI_CODIGO";


            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable TodosDados() {
            string sql = "SELECT  UNI_DESC_ABREV , UNI_CODIGO FROM TIPO_UNIDADE ";
            return BancoDados.GetDataTable(sql, null);

        }
        public bool InsereRegistros(TipoUnidade dados)
        {
            string strCmd = "INSERT INTO TIPO_UNIDADE(UNI_CODIGO, UNI_DESCRICAO, UNI_DESC_ABREV, UNI_LIBERADO,  DTCADASTRO, OPCADASTRO) VALUES (" +
                dados.UniCodigo  + ",'" +
                dados.UniDescricao + "','" +
                dados.UniDescAbrev + "','" +
                dados.UniLiberado + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("UNI_CODIGO", dados.UniCodigo.ToString(), Principal.usuario, "TIPO_UNIDADE", dados.UniDescricao, Principal.empAtual);
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(TipoUnidade dadosNovos, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE TIPO_UNIDADE SET ";
            if (!dadosNovos.UniDescricao.Equals(dadosOld.Rows[0]["UNI_DESCRICAO"]))
            {
                strCmd = strCmd + " uni_descricao = '" +  dadosNovos.UniDescricao + "',";

                dados[contMatriz, 0] = "UNI_DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["UNI_DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.UniDescricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.UniDescAbrev.Equals(dadosOld.Rows[0]["UNI_DESC_ABREV"]))
            {
                strCmd = strCmd + " uni_desc_abrev = '" + dadosNovos.UniDescAbrev + "',";

                dados[contMatriz, 0] = "UNI_DESC_ABREV";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["UNI_DESC_ABREV"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.UniDescAbrev + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.UniLiberado.Equals(dadosOld.Rows[0]["UNI_LIBERADO"]))
            {
                strCmd = strCmd + " UNI_LIBERADO = '" + dadosNovos.UniLiberado + "',";

                dados[contMatriz, 0] = "UNI_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["UNI_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.UniLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;
                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",";

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OpAlteracao + "'";
                strCmd += " OPALTERACAO = '" + dadosNovos.OpAlteracao + "' ";
                contMatriz = contMatriz + 1;

                strCmd += " WHERE UNI_CODIGO = " + dadosNovos.UniCodigo;
                if (BancoDados.ExecuteNoQuery(strCmd,null) != -1)
                {
                    Funcoes.GravaLogAlteracao("UNI_CODIGO", dadosNovos.UniCodigo.ToString(), dadosNovos.OpAlteracao, "TIPO_UNIDADE", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public bool ExcluiTipoUnidade(int uniCodigo)
        {
            string strCmd = "DELETE FROM TIPO_UNIDADE WHERE UNI_CODIGO = " + uniCodigo;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

    }
}
