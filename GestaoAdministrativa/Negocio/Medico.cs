﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Medico
    {
        public int MedID { get; set; }
        public string MedCodigo { get; set; }
        public string MedNome { get; set; }
        public string MedCpf { get; set; }
        public string MedRg { get; set; }
        public string MedCrm { get; set; }
        public string MedNascimento { get; set; }
        public string MedEnder { get; set; }
        public string MedBairro { get; set; }
        public string MedCep { get; set; }
        public string MedCidade { get; set; }
        public string MedUf { get; set; }
        public string MedFone { get; set; }
        public string MedObs { get; set; }
        public string MedLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Medico() { }

        public Medico(int medID, string medCodigo, string medNome, string medCpf, string medRg, string medCrm, string medNascimento, string medEnder,
                       string medBairro, string medCep, string medCidade, string medUf, string medFone, string medObs, string medLiberado,
                       DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.MedID = medID;
            this.MedCodigo = medCodigo;
            this.MedNome = medNome;
            this.MedCpf = medCpf;
            this.MedRg = medRg;
            this.MedCrm = medCrm;
            this.MedNascimento = medNascimento;
            this.MedEnder = medEnder;
            this.MedBairro = medBairro;
            this.MedCep = medCep;
            this.MedCidade = medCidade;
            this.MedUf = medUf;
            this.MedFone = medFone;
            this.MedObs = medObs;
            this.MedLiberado = medLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public string BuscaMedicoPorCRM(string crm)
        {
            string sql = "SELECT MED_NOME FROM MEDICOS WHERE MED_CRM = '" + crm + "'";

            return (string)BancoDados.ExecuteScalar(sql, null);
        }

        public DataTable BuscaTodosMedicos(Medico med, out string strOrdem)
        {

            string sql = " SELECT  MED_NOME, MED_CRM, MED_ENDERECO, MED_BAIRRO, MED_CEP, MED_CIDADE, MED_UF,"
                       + " MED_CPF, MED_RG,  MED_NASCIMENTO, MED_FONE, MED_OBS, "
                       + " CASE MED_DESABILITADO WHEN 'N' THEN 'S' "
                       + " ELSE 'N' END AS MED_DESABILITADO , DTALTERACAO, "
                       + " OPALTERACAO, DTCADASTRO, OPCADASTRO, MED_ID FROM MEDICOS WHERE 1=1";


            if (med.MedCrm != "")
            {
                sql += " AND MED_CRM = '" + med.MedCrm + "'";
            }
            if (med.MedNome != "")
            {
                sql += " AND MED_NOME LIKE '%" + med.MedNome + "%'";
            }
            if (med.MedLiberado != "TODOS")
            {
                string des = med.MedLiberado == "S" ? "N" : "S";
                sql += " AND MED_DESABILITADO = '" + des + "'";
            }
            strOrdem = sql;
            sql += " ORDER BY MED_NOME";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool IncluirDados(Medico dados)
        {
            string sql = " INSERT INTO MEDICOS(MED_CODIGO, MED_ID,MED_NOME, MED_ENDERECO, MED_BAIRRO, MED_CEP, MED_CIDADE, MED_UF, MED_CPF,"
                       + " MED_RG, MED_CRM, MED_NASCIMENTO, MED_FONE, MED_OBS, MED_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES ( "
                       + dados.MedCodigo + ",'"
                       + dados.MedID + "','"
                       + dados.MedNome + "','"
                       + dados.MedEnder + "','"
                       + dados.MedBairro + "','"
                       + dados.MedCep + "','"
                       + dados.MedCidade + "','"
                       + dados.MedUf + "','"
                       + dados.MedCpf + "','"
                       + dados.MedRg + "','"
                       + dados.MedCrm + "',";
            if(String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados.MedNascimento)))
            {
                sql += "null,'";
            }
            else
            {
                sql += Funcoes.BData(Convert.ToDateTime(dados.MedNascimento)) + ",'";
            }

            sql += dados.MedFone + "','"
                    + dados.MedObs + "','"
                    + dados.MedLiberado + "',"
                    + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                    + dados.OpCadastro + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                // GRAVA LOG
                Funcoes.GravaLogInclusao("MED_ID", dados.MedID.ToString(), Principal.usuario.ToUpper(), "MEDICOS", dados.MedNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool AtualizaDados(Medico dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE MEDICOS SET "
                       + " MED_CODIGO = " + dadosNovos.MedCodigo
                       + " ,MED_NOME = '" + dadosNovos.MedNome + "'"
                       + " ,MED_ENDERECO = '" + dadosNovos.MedEnder + "'"
                       + " ,MED_BAIRRO = '" + dadosNovos.MedBairro + "'"
                       + " ,MED_CEP = '" + dadosNovos.MedCep + "'"
                       + " ,MED_CIDADE = '" + dadosNovos.MedCidade + "'"
                       + " ,MED_UF = '" + dadosNovos.MedUf + "'"
                       + " ,MED_CPF = '" + dadosNovos.MedCpf + "'"
                       + " ,MED_RG = '" + dadosNovos.MedRg + "'"
                       + " ,MED_CRM = '" + dadosNovos.MedCrm + "'";
            if (!String.IsNullOrEmpty(dadosNovos.MedNascimento))
            {
                sql += " ,MED_NASCIMENTO = " + Funcoes.BData(Convert.ToDateTime(dadosNovos.MedNascimento));
            }
            sql += " ,MED_FONE = '" + dadosNovos.MedFone + "'"
                + " ,MED_OBS = '" + dadosNovos.MedObs + "'"
                + " ,MED_DESABILITADO = '" + dadosNovos.MedLiberado + "'";

            string[,] dados = new string[14, 3];
            int contMatriz = 0;


            if (dadosNovos.MedCodigo != dadosAntigos.Rows[0]["MED_CODIGO"].ToString())
            {
                dados[contMatriz, 0] = "MED_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedCodigo + "'";
                contMatriz = contMatriz + 1;
            }

            if (dadosNovos.MedNome != dadosAntigos.Rows[0]["MED_NOME"].ToString())
            {
                dados[contMatriz, 0] = "MED_NOME";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_NOME"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedNome + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedEnder != dadosAntigos.Rows[0]["MED_ENDERECO"].ToString())
            {
                dados[contMatriz, 0] = "MED_ENDERECO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_ENDERECO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedEnder + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedBairro != dadosAntigos.Rows[0]["MED_BAIRRO"].ToString())
            {
                dados[contMatriz, 0] = "MED_BAIRRO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_BAIRRO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedBairro + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedCep != dadosAntigos.Rows[0]["MED_CEP"].ToString())
            {
                dados[contMatriz, 0] = "MED_CEP";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_CEP"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedCep + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedCidade != dadosAntigos.Rows[0]["MED_CIDADE"].ToString())
            {
                dados[contMatriz, 0] = "MED_CIDADE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_CIDADE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedCidade + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedUf != dadosAntigos.Rows[0]["MED_UF"].ToString())
            {
                dados[contMatriz, 0] = "MED_UF";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_UF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedUf + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedCpf != dadosAntigos.Rows[0]["MED_CPF"].ToString())
            {
                dados[contMatriz, 0] = "MED_CPF";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_CPF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedCpf + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedRg != dadosAntigos.Rows[0]["MED_RG"].ToString())
            {
                dados[contMatriz, 0] = "MED_RG";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_RG"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedRg + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedCrm != dadosAntigos.Rows[0]["MED_CRM"].ToString())
            {
                dados[contMatriz, 0] = "MED_CRM";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_CRM"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedCrm + "'";
                contMatriz = contMatriz + 1;
            }
            if (Convert.ToString(dadosNovos.MedNascimento) != dadosAntigos.Rows[0]["MED_NASCIMENTO"].ToString())
            {
                dados[contMatriz, 0] = "MED_NASCIMENTO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_NASCIMENTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedNascimento + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedFone != dadosAntigos.Rows[0]["MED_FONE"].ToString())
            {
                dados[contMatriz, 0] = "MED_FONE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_FONE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedFone + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedObs != dadosAntigos.Rows[0]["MED_OBS"].ToString())
            {
                dados[contMatriz, 0] = "MED_OBS";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_OBS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedObs + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.MedLiberado != dadosAntigos.Rows[0]["MED_DESABILITADO"].ToString())
            {
                dados[contMatriz, 0] = "MED_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["MED_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.MedLiberado + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {
                sql += " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                     + " ,DTALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                     + " WHERE MED_ID = " + dadosNovos.MedID;

                if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
                {

                    Funcoes.GravaLogAlteracao("MED_ID", dadosNovos.MedID.ToString(), Principal.usuario, "MEDICOS", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        public bool ExcluirDados(string medCRM)
        {

            string sql = "DELETE FROM MEDICOS WHERE MED_CODIGO = '" + medCRM + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Medico> BuscaDadosMedico(string numCrm)
        {
            string strSql = "SELECT * FROM MEDICOS WHERE MED_CRM = '" + numCrm + "'";
            DataTable table = BancoDados.GetDataTable(strSql, null);
            List<Medico> lista = new List<Medico>();
            foreach (DataRow row in table.Rows)
            {
                Medico dados = new Medico();
                dados.MedCodigo = row["MED_CODIGO"].ToString();
                dados.MedNome = row["MED_NOME"].ToString();
                dados.MedEnder = row["MED_ENDERECO"].ToString();
                dados.MedBairro = row["MED_BAIRRO"].ToString();
                dados.MedCep = row["MED_CEP"].ToString();
                dados.MedCidade = row["MED_CIDADE"].ToString();
                dados.MedUf = row["MED_UF"].ToString();
                dados.MedRg = row["MED_CPF"].ToString();
                dados.MedCrm = row["MED_CRM"].ToString();
                dados.MedNascimento = row["MED_NASCIMENTO"].ToString();
                dados.MedFone = row["MED_FONE"].ToString();
                dados.MedObs = row["MED_OBS"].ToString();
                dados.MedLiberado = row["MED_DESABILITADO"].ToString();
                lista.Add(dados);
            }

            return lista;
        }

        public bool InsereRegistros(Medico dados)
        {
            string strCmd = "INSERT INTO MEDICOS (MED_ID, MED_CODIGO, MED_NOME, MED_CRM, MED_UF, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.MedID + ","
                + dados.MedCodigo + ",'"
                + dados.MedNome + "','"
                + dados.MedCrm + "','"
                + dados.MedUf + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("MED_ID", dados.MedID.ToString(), dados.OpCadastro, "MEDICOS", dados.MedNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;


        }

        public DataTable BuscaDados(string crmCro, string nome, string desabilitado = "N")
        {
            string sql = " SELECT MED_CRM, MED_NOME, MED_CIDADE, MED_UF FROM MEDICOS WHERE "
                       + " MED_DESABILITADO = '" + desabilitado + "' ";

            if (!String.IsNullOrEmpty(crmCro))
            {

                sql += " AND MED_CRM = '" + crmCro + "'";
            }
            if (!String.IsNullOrEmpty(nome))
            {
                sql += " AND MED_NOME LIKE '%" + nome + "%'";
            }
            return BancoDados.GetDataTable(sql, null);

        }
    }
}
