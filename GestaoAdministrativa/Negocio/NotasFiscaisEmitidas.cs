﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class NotasFiscaisEmitidas
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaId { get; set; }
        public string ChaveNfe { get; set; }
        public string NumeroLote { get; set; }
        public string NumeroRecibo { get; set; }
        public string NumeroProtocolo { get; set; }
        public string Caminho { get; set; }
        public string NumeroCorrecao { get; set; }
        public string Cancelada { get; set; }
        public string Devolucao { get; set; }
        public DateTime DataEmisssao { get; set; }
        public DateTime DataCancelamento { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public string Status { get; set; }
        public string NumeroProtocoloCCe { get; set; }
        public string NumeroProtocoloCancelamento { get; set; }
        public string Observacao { get; set; }
        public int CfID { get; set; }

        public NotasFiscaisEmitidas() { }

        public bool InserePreNota(NotasFiscaisEmitidas dados)
        {
            string strCmd = "INSERT INTO NOTAS_FISCAIS_EMITIDAS(EMP_CODIGO, EST_CODIGO, VENDA_ID, CHAVE_NFE, NFE_DEVOLUCAO, DTCADASTRO, OPCADASTRO, NFE_DATA, OBSERVACAO, CF_ID) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.VendaId + ",'" +
                dados.ChaveNfe + "','" +
                dados.Devolucao + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "'," + 
                Funcoes.BData(dados.DataEmisssao) + ",'" + 
                dados.Observacao + "'," + 
                dados.CfID + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool ExcluiNotaPorVendaID(long vendaID, int empCodigo, int estCodigo)
        {
            string strCmd = "DELETE FROM NOTAS_FISCAIS_EMITIDAS WHERE VENDA_ID = " + vendaID + " AND EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo;
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaLote(long vendaID, int empCodigo, int estCodigo, string lote)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET NUM_LOTE = '" + lote + "'"
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaNumeroRecibo(long vendaID, int empCodigo, int estCodigo, string numRecibo)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET NUM_RECIBO = '" + numRecibo + "'"
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaProtocolo(long vendaID, int empCodigo, int estCodigo, string protocolo)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET NUM_PROTOCOLO = '" + protocolo + "'"
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaCaminho(long vendaID, int empCodigo, int estCodigo, string caminho)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET PATH = '" + caminho + "', STATUS = 'F'" 
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaNfeEmitidas(int estCodigo, int empCodigo, string vendaID, string data, string chaveDeAcesso)
        {
            string strSql = "SELECT A.NFE_DATA,"
                            + "           A.VENDA_ID,"
                            + "           C.CF_NOME,"
                            + "           A.STATUS,"
                            + "           A.CHAVE_NFE,"
                            + "           A.NUM_LOTE,"
                            + "           A.NUM_RECIBO,"
                            + "           A.NFE_CANCELADA,"
                            + "           A.NFE_DATA_CANCELAMENTO,"
                            + "           A.NFE_DEVOLUCAO,"
                            + "           A.OPCADASTRO, A.PATH, A.NUM_PROTOCOLO"
                            + "    FROM NOTAS_FISCAIS_EMITIDAS A"
                            + "    LEFT JOIN VENDAS B ON (A.VENDA_ID = B.VENDA_ID AND A.EMP_CODIGO = B.EMP_CODIGO AND A.EST_CODIGO = B.EST_CODIGO)"
                            + "    LEFT JOIN CLIFOR C ON C.CF_ID = A.CF_ID"
                            + "    WHERE A.EMP_CODIGO = " + empCodigo
                            + "    AND A.EST_CODIGO = " + estCodigo
                            + "    AND A.STATUS = 'F'";
            if (!String.IsNullOrEmpty(vendaID))
            {
                strSql += " AND A.VENDA_ID = " + vendaID;
            }

            if(!String.IsNullOrEmpty(Funcoes.RemoveCaracter(data)))
            {
                strSql += " AND A.NFE_DATA = " + Funcoes.BData(Convert.ToDateTime(data));
            }
            
            if(!String.IsNullOrEmpty(chaveDeAcesso))
            {
                strSql += " AND A.CHAVE_NFE = '" + chaveDeAcesso + "'";
            }

            strSql += " ORDER BY A.NFE_DATA, C.CF_NOME ";
            return BancoDados.GetDataTable(strSql, null);
        }

        public int BuscaNumeroCorrecao(int empCodigo, int estCodigo)
        {
            string sql = "SELECT MAX(NUM_CORRECAO) AS NUM_CORRECAO FROM NOTAS_FISCAIS_EMITIDAS WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return 1;
            else
                return Convert.ToInt32(r) + 1;
        }

        public bool AtualizaNumeroCorrecaoEProtocolo(long vendaID, int empCodigo, int estCodigo, int correcao, string protocolo)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET NUM_CORRECAO = " + correcao + ", NUM_PROTOCOLO_CCE = '" + protocolo + "'"
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDadosCancelamento(long vendaID, int empCodigo, int estCodigo, string protocolo)
        {
            string strCmd = "UPDATE NOTAS_FISCAIS_EMITIDAS SET NUM_PROTOCOLO_CANCELAMENTO = '" + protocolo + "', NFE_CANCELADA = 'S', NFE_DATA_CANCELAMENTO = " + Funcoes.BDataHora(DateTime.Now)
                + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
