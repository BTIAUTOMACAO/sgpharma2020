﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioNovartisPendente
    {
        public string NSU { get; set; }
        public string Operadora { get; set; }
        public string TipoTransacao { get; set; }
        public DateTime Data { get; set; }
        public string Estacao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public BeneficioNovartisPendente() { }

        public BeneficioNovartisPendente(string nsu, string operadora, string tipoTransacao, DateTime data, string estacao, DateTime dtCadastro, string opCadastro)
        {
            this.NSU = nsu;
            this.TipoTransacao = tipoTransacao;
            this.Data = data;
            this.Estacao = estacao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InsereRegistros(BeneficioNovartisPendente dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_NOVARTIS_PENDENTE(NSU, OPERADORA, TIPO_TRANSACAO, DATA, ESTACAO,  DTCADASTRO, OPCADASTRO) VALUES ('"
                + dados.NSU + "','"
                + dados.Operadora + "','"
                + dados.TipoTransacao + "',"
                + Funcoes.BData(dados.Data) + ",'"
                + dados.Estacao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaVendasPendentes()
        {
            string strSql;

            strSql = "SELECT  * "
                    + " FROM BENEFICIO_NOVARTIS_PENDENTE";

            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluirDadosPorNSU(string nsu)
        {
            string strCmd = "DELETE FROM BENEFICIO_NOVARTIS_PENDENTE WHERE NSU = '" + nsu + "'";
       
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
