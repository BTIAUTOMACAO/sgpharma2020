﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SNGPCRelatorios
    {
        public string HashEnvio { get; set; }
        public string HashRetorno { get; set; }
        public string Aceito { get; set; }
        public DateTime DataInicial { get; set; }
        public DateTime DataFinal { get; set; }
        public DateTime DataRecebimento { get; set; }
        public DateTime DataValidacao { get; set; }

        public SNGPCRelatorios() { }

        public SNGPCRelatorios(string hashEnvio, string hashRetorno, string aceito, DateTime dataInicial, DateTime dataFinal,
                               DateTime dataRecebimento, DateTime dataValidacao)
        {
            this.HashEnvio = hashEnvio;
            this.HashRetorno = hashRetorno;
            this.Aceito = aceito;
            this.DataInicial = dataInicial;
            this.DataFinal = dataFinal;
            this.DataRecebimento = dataRecebimento;
            this.DataValidacao = dataValidacao;
        }

        public DataTable BuscaEnviosPorTecnicos(int tecID, string aceitos)
        {
            string sql = " SELECT ENV.HASH_ENVIO, ENV.HASH_RETORNO, ENV.ACEITO, ENV.DATA_INICIAL, ENV.DATA_FINAL, "
                       + " ENV.DATA_RECEBIMENTO, ENV.DATA_VALIDACAO, "
                       + " TEC.TEC_LOGIN, TEC.TEC_SENHA, TEC.TEC_NOME,"
                       + " EST.EST_CGC, ENV.MENSAGEMVALIDACAO "
                       + " FROM SNGPC_ENVIOS ENV "
                       + " INNER JOIN SNGPC_TECNICOS TEC ON(TEC.TEC_CODIGO = ENV.TEC_CODIGO) "
                       + " INNER JOIN ESTABELECIMENTOS EST ON(EST.EST_CODIGO = ENV.EST_CODIGO)";

            if (aceitos != "TODOS") {
                sql += " WHERE ENV.ACEITO ='" + aceitos + "'";
            }
            sql += "ORDER BY ENV.DATA_RECEBIMENTO DESC";

         return  BancoDados.GetDataTable(sql, null);

        }

        public DataTable BalancoCompleto(string ano, bool anual, int trimestre, int empCodigo, int estCodigo, int classTera = 0)
        {
            string sql = " SELECT A.PROD_CODIGO,"
                        + "           PROD_DCB,"
                        + "          LAC_INV_QTDE,"
                        + "           ENT_MED_QTDE,"
                        + "           VEN_MED_QTDE,"
                        + "           SUM(B.PER_MED_QTDE) AS PER_MED_QTDE,"
                        + "           C.DESCRICAO,"
                        + "           D.PROD_DESCR"
                        + "      FROM(SELECT A.PROD_CODIGO,"
                        + "                   PROD_DCB,"
                        + "                   LAC_INV_QTDE,"
                        + "                   A.ENT_MED_QTDE,"
                        + "                   SUM(B.VEN_MED_QTDE) AS VEN_MED_QTDE"
                        + "              FROM(SELECT A.PROD_CODIGO,"
                        + "                          C.PROD_DCB,"
                        + "                           B.LAC_INV_QTDE,"
                        + "                           SUM(A.ENT_MED_QTDE) AS ENT_MED_QTDE"
                        + "                      FROM SNGPC_ENTRADAS A"
                        + "                      LEFT JOIN SNGPC_LAC_INVENTARIO B ON(A.PROD_CODIGO ="
                        + "                                                          B.LAC_INV_CODIGO AND"
                        + "                                                          A.EMP_CODIGO ="
                        + "                                                          B.EMP_CODIGO AND"
                        + "                                                          A.EST_CODIGO ="
                        + "                                                          B.EST_CODIGO)"
                        + "                     INNER JOIN PRODUTOS_DETALHE C ON A.PROD_CODIGO ="
                        + "                                                      C.PROD_CODIGO"
                        + "                     WHERE A.EMP_CODIGO = " + empCodigo
                        + "                       AND A.EST_CODIGO = " + estCodigo
                        + "                       AND C.EMP_CODIGO = A.EMP_CODIGO"
                        + "                       AND C.EST_CODIGO = A.EST_CODIGO";
            if(anual)
            {
                sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
            }
            else
            {
                if(trimestre == 1)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/03/" + ano));
                }
                else if (trimestre == 2)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/04/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/06/" + ano));
                }
                else if (trimestre == 3)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/07/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/09/" + ano));
                }
                else if (trimestre == 4)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/10/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
                }
            }

            sql += "                     GROUP BY A.PROD_CODIGO, C.PROD_DCB, B.LAC_INV_QTDE) A"
                        + "              LEFT JOIN SNGPC_VENDAS B ON(A.PROD_CODIGO = B.PROD_CODIGO AND"
                        + "                                          B.EMP_CODIGO = " + empCodigo + " AND B.EST_CODIGO = " + estCodigo;

            if (anual)
            {
                sql += " AND B.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
            }
            else
            {
                if (trimestre == 1)
                {
                    sql += " AND B.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/03/" + ano));
                }
                else if (trimestre == 2)
                {
                    sql += " AND B.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/04/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/06/" + ano));
                }
                else if (trimestre == 3)
                {
                    sql += " AND B.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/07/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/09/" + ano));
                }
                else if (trimestre == 4)
                {
                    sql += " AND B.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/10/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
                }
            }
            sql += " ) GROUP BY A.PROD_CODIGO, PROD_DCB, LAC_INV_QTDE, A.ENT_MED_QTDE) A"
                        + "      LEFT JOIN SNGPC_PERDA B ON (A.PROD_CODIGO = B.PROD_CODIGO AND"
                        + "                                 B.EMP_CODIGO = " + empCodigo + " AND B.EST_CODIGO = " + estCodigo;
            if (anual)
            {
                sql += " AND B.PER_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
            }
            else
            {
                if (trimestre == 1)
                {
                    sql += " AND B.PER_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/03/" + ano));
                }
                else if (trimestre == 2)
                {
                    sql += " AND B.PER_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/04/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/06/" + ano));
                }
                else if (trimestre == 3)
                {
                    sql += " AND B.PER_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/07/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/09/" + ano));
                }
                else if (trimestre == 4)
                {
                    sql += " AND B.PER_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/10/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
                }
            }

            sql += " )          LEFT JOIN DCB C ON A.PROD_DCB = C.NUM_DCB"
                        + "     INNER JOIN PRODUTOS D ON A.PROD_CODIGO = D.PROD_CODIGO";

            if (classTera == 1)
                sql += "    WHERE D.PROD_CLASSE_TERAP = '1'";
            else if (classTera == 2)
                sql += "    WHERE D.PROD_CLASSE_TERAP = '2'";

            sql+= "     GROUP BY A.PROD_CODIGO, PROD_DCB, LAC_INV_QTDE, ENT_MED_QTDE, VEN_MED_QTDE,C.DESCRICAO,D.PROD_DESCR";

            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BalancoAquisicoes(string ano, bool anual, int trimestre, int empCodigo, int estCodigo)
        {
            string sql = "SELECT D.PROD_DCB, E.DESCRICAO, B.PROD_DESCR, C.CF_NOME, A.ENT_CNPJ, A.ENT_DOCTO, A.ENT_MED_QTDE"
                        + "    FROM SNGPC_ENTRADAS A"
                        + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                        + "    INNER JOIN CLIFOR C ON A.ENT_CNPJ = C.CF_DOCTO"
                        + "    INNER JOIN PRODUTOS_DETALHE D ON A.PROD_CODIGO = D.PROD_CODIGO"
                        + "    LEFT JOIN DCB E ON D.PROD_DCB = E.NUM_DCB"
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EST_CODIGO = " + estCodigo
                        + "    AND A.EMP_CODIGO = D.EMP_CODIGO"
                        + "    AND A.EST_CODIGO = D.EST_CODIGO";
            if (anual)
            {
                sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
            }
            else
            {
                if (trimestre == 1)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/03/" + ano));
                }
                else if (trimestre == 2)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/04/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/06/" + ano));
                }
                else if (trimestre == 3)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/07/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/09/" + ano));
                }
                else if (trimestre == 4)
                {
                    sql += " AND A.ENT_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/10/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
                }
            }
            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BalancoReceitasAouB(string ano, string mes, int empCodigo, int estCodigo, int tipoReceita)
        {
            string sql = "SELECT D.PROD_DCB, E.DESCRICAO, B.PROD_DESCR,  A.VEN_NUM_NOT, A.VEN_PRESC_DT,A.VEN_PRESC_NOME, A.VEN_PRESC_NUMERO, A.VEN_MED_QTDE, A.PROD_CODIGO"
                        + "    FROM SNGPC_VENDAS A"
                        + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                        + "    INNER JOIN PRODUTOS_DETALHE D ON A.PROD_CODIGO = D.PROD_CODIGO"
                        + "    LEFT JOIN DCB E ON D.PROD_DCB = E.NUM_DCB"
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EST_CODIGO = " + estCodigo
                        + "    AND A.EMP_CODIGO = D.EMP_CODIGO"
                        + "    AND A.EST_CODIGO = D.EST_CODIGO"
                        + "    AND A.VEN_TP_RECEITA = " + tipoReceita;

            if(tipoReceita.Equals(4))
            {
                sql += " AND B.PROD_PORTARIA_CONTROLADO IN ('A1','A2','A3')";
            }
            else
            {
                sql += " AND B.PROD_PORTARIA_CONTROLADO IN ('B2')";
            }

            if(mes.Equals("JANEIRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/01/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/01/" + ano));
            }
            else if (mes.Equals("FEVEREIRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/02/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("01/02/" + ano).AddMonths(1).AddDays(-1));
            }
            else if (mes.Equals("MARÇO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/03/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/03/" + ano));
            }
            else if (mes.Equals("ABRIL"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/04/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/04/" + ano));
            }
            else if (mes.Equals("MAIO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/05/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/05/" + ano));
            }
            else if (mes.Equals("JUNHO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/06/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/06/" + ano));
            }
            else if (mes.Equals("JULHO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/07/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/07/" + ano));
            }
            else if (mes.Equals("AGOSTO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/08/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/08/" + ano));
            }
            else if (mes.Equals("SETEMBRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/09/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/09/" + ano));
            }
            else if (mes.Equals("OUTUBRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/10/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/10/" + ano));
            }
            else if (mes.Equals("NOVEMBRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/11/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("30/11/" + ano));
            }
            else if (mes.Equals("DEZEMBRO"))
            {
                sql += " AND A.VEN_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime("01/12/" + ano)) + " AND " + Funcoes.BData(Convert.ToDateTime("31/12/" + ano));
            }

            sql += " ORDER BY B.PROD_DESCR";

            return BancoDados.GetDataTable(sql, null);

        }

        public string BuscaDataLancInventario()
        {
            string sql = "SELECT DT_CADASTRO FROM SNGPC_LAC_INVENTARIO WHERE ROWNUM = 1";

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public DataTable BuscaEstoqueEntradaSaida(string dataInventario, int ordem)
        {
            string sql = " SELECT E.ENTRADA,"
                        + "           COALESCE(SAIDA, 0) AS SAIDA,"
                        + "           E.ENTRADA - COALESCE(SAIDA, 0) AS ESTOQUE_ATUAL,"
                        + "           E.PROD_CODIGO,"
                        + "           E.PROD_DESCR,"
                        + "           E.LOTE,"
                        + "           E.REGISTRO"
                        + "      FROM(SELECT SUM(QTDE) AS ENTRADA,"
                        + "                   C.PROD_CODIGO,"
                        + "                   C.REGISTRO,"
                        + "                   C.LOTE,"
                        + "                   D.PROD_DESCR"
                        + "              FROM(SELECT A.LAC_INV_CODIGO AS PROD_CODIGO,"
                        + "                           A.LAC_INV_MS     AS REGISTRO,"
                        + "                           A.LAC_INV_LOTE   AS LOTE,"
                        + "                           A.LAC_INV_QTDE   AS QTDE"
                        + "                      FROM SNGPC_LAC_INVENTARIO A"
                        + "                      WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "                      AND A.EST_CODIGO = " + Principal.estAtual
                        + "                    UNION ALL"
                        + "                    SELECT B.PROD_CODIGO,"
                        + "                           B.ENT_MS AS REGISTRO,"
                        + "                           B.ENT_MED_LOTE AS LOTE,"
                        + "                           B.ENT_MED_QTDE AS QTDE"
                        + "                      FROM SNGPC_ENTRADAS B"
                        + "                     WHERE B.ENT_DATA > " + Funcoes.BData(Convert.ToDateTime(dataInventario))
                        + "                     AND B.EMP_CODIGO = " + Principal.empAtual
                        + "                     AND B.EST_CODIGO = " + Principal.estAtual + ") C"
                        + "             INNER JOIN PRODUTOS D ON C.PROD_CODIGO = D.PROD_CODIGO"
                        + "             GROUP BY C.PROD_CODIGO, C.REGISTRO, C.LOTE, D.PROD_DESCR"
                        + "             ORDER BY D.PROD_DESCR) E"
                        + "      LEFT JOIN(SELECT SUM(C.QTDE) AS SAIDA,"
                        + "                        C.PROD_CODIGO,"
                        + "                        C.REGISTRO,"
                        + "                        C.LOTE,"
                        + "                        D.PROD_DESCR"
                        + "                   FROM(SELECT A.PROD_CODIGO,"
                        + "                                A.VEN_MS AS REGISTRO,"
                        + "                                A.VEN_MED_LOTE AS LOTE,"
                        + "                                A.VEN_MED_QTDE AS QTDE"
                        + "                           FROM SNGPC_VENDAS A"
                        + "                          WHERE A.EMP_CODIGO = " + Principal.empAtual
                        + "                            AND A.EST_CODIGO = " + Principal.estAtual
                        + "                            AND A.VEN_DATA > " + Funcoes.BData(Convert.ToDateTime(dataInventario))
                        + "                         UNION ALL"
                        + "                         SELECT B.PROD_CODIGO,"
                        + "                                B.PER_MS AS REGISTRO,"
                        + "                                B.PER_MED_LOTE AS LOTE,"
                        + "                                B.PER_MED_QTDE AS QTDE"
                        + "                           FROM SNGPC_PERDA B"
                        + "                          WHERE B.EMP_CODIGO = " + Principal.empAtual
                        + "                            AND B.EST_CODIGO = " + Principal.estAtual
                        + "                            AND B.PER_DATA >" + Funcoes.BData(Convert.ToDateTime(dataInventario)) + ") C"
                        + "                  INNER JOIN PRODUTOS D ON C.PROD_CODIGO = D.PROD_CODIGO"
                        + "                  GROUP BY C.PROD_CODIGO, C.REGISTRO, C.LOTE, D.PROD_DESCR"
                        + "                  ORDER BY D.PROD_DESCR) F ON (E.PROD_CODIGO = F.PROD_CODIGO AND"
                        + "                                              E.LOTE = F.LOTE)";
            if(ordem == 0)
            {
                sql += " ORDER BY E.PROD_DESCR";
            }
            else
            {
                sql += " ORDER BY E.PROD_CODIGO";
            }
            
            return BancoDados.GetDataTable(sql, null);

        }

       
    }
}
