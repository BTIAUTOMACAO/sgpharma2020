﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcEntrada
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int EntSeq { get; set; }
        public string EntDocto { get; set; }
        public int EntTipoOperacao { get; set; }
        public DateTime EntDataNF { get; set; }
        public string EntCNPJ { get; set; }
        public string ProdCodigo { get; set; }
        public string EntMedLote { get; set; }
        public int EntMedQtde { get; set; }
        public DateTime EntData { get; set; }
        public string EntNomeAux { get; set; }
        public string EntRegistroMS { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public SngpcEntrada() { }

        public SngpcEntrada(int empCodigo, int estCodigo, int entSeq, string entDocto, int entTipoOperacao, DateTime entDataNF,
                            string entCNPJ, string prodCodigo, string entMedLote, int entMedQtde, DateTime entData, string entNomeAux,
                            string entRegistroMS, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)

        {

            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.EntSeq = entSeq;
            this.EntDocto = entDocto;
            this.EntTipoOperacao = entTipoOperacao;
            this.EntDataNF = entDataNF;
            this.EntCNPJ = entCNPJ;
            this.ProdCodigo = prodCodigo;
            this.EntMedLote = entMedLote;
            this.EntMedQtde = entMedQtde;
            this.EntData = entData;
            this.EntNomeAux = entNomeAux;
            this.EntRegistroMS = entRegistroMS;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }


        public DataTable BuscaEntrada(SngpcEntrada dados, out string strOrdem)
        {

            string sql = " SELECT ENT.ENT_SEQ, ENT.ENT_DOCTO, ENT.ENT_DATA, ENT.ENT_DATA_NF, ENT.ENT_MS, ENT.ENT_MED_QTDE, "
                       + " ENT.ENT_MED_LOTE, ENT.DTCADASTRO, ENT.OPCADASTRO, ENT.OPALTERACAO, ENT.DTALTERACAO, TAB.TAB_DESCRICAO, "
                       + " ENT_CNPJ, ENT.PROD_CODIGO, PRO.PROD_DESCR , PRO.PROD_CONTROLADO,  "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' WHEN '2' THEN "
                       + " 'SUJEITO A CONTROLE ESPECIAL' ELSE '' END AS PROD_CLASSE_TERAP "
                       + " FROM SNGPC_ENTRADAS ENT "
                       + " INNER JOIN SNGPC_TABELAS TAB ON(TAB.TAB_SEQ = ENT.ENT_TIPO_OPER) "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = ENT.PROD_CODIGO) "
                       + " WHERE ENT.EMP_CODIGO = " + Principal.empAtual + " AND ENT.EST_CODIGO = " + Principal.estAtual;


            if (!String.IsNullOrEmpty(dados.EntDocto))
            {
                sql += " AND ENT.ENT_DOCTO = '" + dados.EntDocto + "'";
            }
            if (dados.EntTipoOperacao != 0)
            {
                sql += " AND ENT.ENT_TIPO_OPER = " + EntTipoOperacao;
            }
            if (!String.IsNullOrEmpty(dados.ProdCodigo))
            {
                sql += " AND ENT.PROD_CODIGO = '" + dados.ProdCodigo + "'";
            }
            if (!String.IsNullOrEmpty(dados.EntRegistroMS))
            {
                sql += " AND ENT.ENT_MS = '" + dados.EntRegistroMS + "'";
            }

            strOrdem = sql;
            sql += " ORDER BY ENT_DATA";

            return BancoDados.GetDataTable(sql, null);

        }

        public bool IncluirDados(SngpcEntrada dados)
        {
            string sql = " INSERT INTO SNGPC_ENTRADAS ( EMP_CODIGO, EST_CODIGO, ENT_SEQ, ENT_DOCTO, ENT_TIPO_OPER, "
                       + " ENT_DATA_NF, ENT_CNPJ, PROD_CODIGO, ENT_MED_LOTE, ENT_MED_QTDE, ENT_DATA, ENT_MS, "
                       + " DTCADASTRO, OPCADASTRO  ) VALUES ( "
                       + dados.EmpCodigo + ","
                       + dados.EstCodigo + ","
                       + dados.EntSeq + ",'"
                       + dados.EntDocto + "',"
                       + dados.EntTipoOperacao + ","
                       + Funcoes.BData(dados.EntDataNF) + ",'"
                       + dados.EntCNPJ + "','"
                       + dados.ProdCodigo + "','"
                       + dados.EntMedLote + "',"
                       + dados.EntMedQtde + ","
                       + Funcoes.BData(dados.EntData) + ", '"
                       + dados.EntRegistroMS + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario+ "')";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("ENT_SEQ", dados.EntSeq.ToString(), Principal.usuario.ToUpper(), "SNGPC_ENTRADAS", dados.ProdCodigo, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaDados(SngpcEntrada dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE SNGPC_ENTRADAS SET "
                       + " ENT_DOCTO = '" + dadosNovos.EntDocto + "'"
                       + " ,ENT_TIPO_OPER = '" + dadosNovos.EntTipoOperacao + "'"
                       + " ,ENT_DATA_NF = " + Funcoes.BData(dadosNovos.EntDataNF)
                       + " ,ENT_CNPJ = '" + dadosNovos.EntCNPJ + "'"
                       + " ,PROD_CODIGO = '" + dadosNovos.ProdCodigo + "'"
                       + " ,ENT_MED_LOTE = '" + dadosNovos.EntMedLote + "'"
                       + " ,ENT_MED_QTDE = " + dadosNovos.EntMedQtde
                       + " ,ENT_DATA = " + Funcoes.BData(dadosNovos.EntData)
                       + " ,ENT_MS = '" + dadosNovos.EntRegistroMS + "'"
                       + " ,OPALTERACAO = '" + Principal.usuario + "'"
                       + " ,DTCADASTRO = " + Funcoes.BDataHora(DateTime.Now)
                       + " WHERE ENT_SEQ = " + dadosNovos.EntSeq + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

            string[,] dados = new string[9, 3];
            int contMatriz = 0;

            if (dadosNovos.EntDocto != dadosAntigos.Rows[0]["ENT_DOCTO"].ToString())
            {
                dados[contMatriz, 0] = "ENT_DOCTO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_DOCTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntDocto + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntTipoOperacao != Convert.ToInt32(dadosAntigos.Rows[0]["ENT_TIPO_OPER"]))
            {
                dados[contMatriz, 0] = "ENT_TIPO_OPER";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_TIPO_OPER"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntTipoOperacao + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntDataNF != Convert.ToDateTime(dadosAntigos.Rows[0]["ENT_DATA_NF"]))
            {
                dados[contMatriz, 0] = "ENT_DATA_NF";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_DATA_NF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntDataNF + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntCNPJ != dadosAntigos.Rows[0]["ENT_CNPJ"].ToString())
            {
                dados[contMatriz, 0] = "ENT_CNPJ";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_CNPJ"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntCNPJ + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.ProdCodigo != dadosAntigos.Rows[0]["PROD_CODIGO"].ToString())
            {
                dados[contMatriz, 0] = "PROD_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntMedLote != dadosAntigos.Rows[0]["ENT_MED_LOTE"].ToString())
            {
                dados[contMatriz, 0] = "ENT_MED_LOTE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_MED_LOTE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntMedLote + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntMedQtde != Convert.ToInt32(dadosAntigos.Rows[0]["ENT_MED_QTDE"]))
            {
                dados[contMatriz, 0] = "ENT_MED_QTDE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_MED_QTDE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntMedQtde + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntData != Convert.ToDateTime(dadosAntigos.Rows[0]["ENT_DATA"]))
            {
                dados[contMatriz, 0] = "ENT_DATA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_DATA"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntData + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EntRegistroMS != dadosAntigos.Rows[0]["ENT_MS"].ToString())
            {
                dados[contMatriz, 0] = "ENT_MS";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ENT_MS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EntRegistroMS + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {
                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("ENT_SEQ", dadosNovos.EntSeq.ToString(), Principal.usuario.ToUpper(), "SNGPC_ENTRADAS", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return true;
            }

        }

        public bool ExcluirDados(int codigo)
        {
            string sql = "DELETE FROM SNGPC_ENTRADAS WHERE ENT_SEQ = " + codigo + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public int ExcluiProdutoViaEntradaDeNotas(SngpcEntrada prodSngpcEntrada, bool transacao)
        {
            string strCmd = "DELETE FROM SNGPC_ENTRADAS WHERE EST_CODIGO = " + prodSngpcEntrada.EstCodigo + " AND EMP_CODIGO = " + prodSngpcEntrada.EmpCodigo
                + "AND ENT_DOCTO = " + prodSngpcEntrada.EntDocto + " AND ENT_CNPJ = '" + prodSngpcEntrada.EntCNPJ + "' AND PROD_CODIGO = " + prodSngpcEntrada.ProdCodigo;
            if (transacao.Equals(true))
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
            else
                return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public bool InsereRegistroSngpcEntrada(SngpcEntrada dadosSngpcEntrada)
        {
            string strCmd = "INSERT INTO SNGPC_ENTRADAS (EMP_CODIGO, EST_CODIGO, ENT_SEQ, ENT_DOCTO, ENT_TIPO_OPER, ENT_DATA_NF, "
                               + "ENT_CNPJ, PROD_CODIGO, ENT_MED_LOTE, ENT_MED_QTDE, ENT_DATA, ENT_MS, DTCADASTRO, OPCADASTRO) VALUES ("
                               + dadosSngpcEntrada.EmpCodigo + ","
                               + dadosSngpcEntrada.EstCodigo + ","
                               + dadosSngpcEntrada.EntSeq + ","
                               + dadosSngpcEntrada.EntDocto + ",'"
                               + dadosSngpcEntrada.EntTipoOperacao + "',"
                               + Funcoes.BData(dadosSngpcEntrada.EntDataNF) + ",'"
                               + dadosSngpcEntrada.EntCNPJ + "','"
                               + dadosSngpcEntrada.ProdCodigo + "','"
                               + dadosSngpcEntrada.EntMedLote + "',"
                               + dadosSngpcEntrada.EntMedQtde + ","
                               + Funcoes.BData(dadosSngpcEntrada.EntData) + ",'"
                               + dadosSngpcEntrada.EntRegistroMS + "',"
                               + Funcoes.BDataHora(dadosSngpcEntrada.DtCadastro) + ",'"
                               + dadosSngpcEntrada.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaEntrasPorPeriodo(string dtInicial, string dtFinal, char ordem, string tipoOperacao, int tipoMed)
        {
            string sql = " SELECT SE.ENT_DOCTO, SE.ENT_DATA_NF, SE.ENT_DATA, SE.ENT_MED_LOTE, SE.ENT_MED_QTDE, SE.PROD_CODIGO,SE.ENT_CNPJ,  "
                       + " PRO.PROD_DESCR , CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' WHEN '2' THEN 'SUJEITO A CONTROLE ESPECIAL' ELSE '' END AS PROD_CLASSE_TERAP, "
                       + " PRO.PROD_UNIDADE, SE.ENT_MS AS PROD_REGISTRO_MS , "
                       + " CLI.CF_NOME "
                       + " FROM SNGPC_ENTRADAS SE "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = SE.PROD_CODIGO) "
                       + " INNER JOIN CLIFOR CLI ON(CLI.CF_DOCTO = SE.ENT_CNPJ) "
                       + " WHERE SE.ENT_DATA BETWEEN  TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') "
                       + " AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                       + " AND SE.EMP_CODIGO = " + Principal.empAtual + " AND SE.EST_CODIGO = " + Principal.estAtual;
            if (!tipoOperacao.Equals("TODOS"))
            {
                sql += " AND SE.ENT_TIPO_OPER = " + tipoOperacao;
            }
            if (!tipoMed.Equals(0))
            {
                sql += " AND PRO.PROD_CLASSE_TERAP = " + tipoMed;
            }
            if (ordem.Equals('C'))
            {
                sql += "ORDER BY SE.PROD_CODIGO  ";
            }
            else
            {
                sql += "ORDER BY PRO.PROD_DESCR  ";
            }


            return BancoDados.GetDataTable(sql, null);
        }



    }
}
