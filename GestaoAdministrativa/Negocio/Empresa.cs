﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Empresa
    {
        public int EmpCodigo { get; set; }
        public string EmpRazao { get; set; }
        public string EmpFantasia { get; set; }
        public string EmpCgc { get; set; }
        public string EmpEndereco { get; set; }
        public string EmpNum { get; set; }
        public string EmpCidade { get; set; }
        public string EmpUF { get; set; }
        public string EmpFone { get; set; }
        public string EmpInsest { get; set; }
        public string EmpLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Empresa() { }


        public Empresa(int empCodigo, string empRazao, string empFantasia, string empCgc, string empEndereco, string empNum,
                       string empCidade, string empUF, string empFone, string empInsest,
                       string empLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro,
                       string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EmpRazao = empRazao;
            this.EmpFantasia = empFantasia;
            this.EmpCgc = empCgc;
            this.EmpEndereco = empEndereco;
            this.EmpNum = empNum;
            this.EmpCidade = empCidade;
            this.EmpUF = empUF;
            this.EmpFone = empFone;
            this.EmpInsest = empInsest;
            this.EmpLiberado = empLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaDados(int estCodigo, int empCodigo, string empFantasia, string desabilitado, out string strOrdem)
        {
            string sql = " SELECT EMP_CODIGO, EMP_CGC, EMP_INSEST, EMP_RAZAO, EMP_FANTASIA, EMP_ENDERECO, EMP_NUMERO, "
                       + " EMP_CIDADE, EMP_UF, EMP_FONE, CASE EMP_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS EMP_DESABILITADO ,"
                       + " DAT_ALTERACAO, EMP_OPALTERACAO, EMP_DTCADASTRO, EMP_OPCADASTRO FROM EMPRESA "
                       + " WHERE 1 = 1 ";

            if (empCodigo != 0)
            {
                sql += " AND EMP_CODIGO = " + estCodigo;
            }
            if (empFantasia != "")
            {
                sql += " AND EMP_FANTASIA LIKE '%" + empFantasia + "%'";
            }
            if (desabilitado != "TODOS")
            {
                sql += " AND EMP_DESABILITADO = '" + desabilitado + "'";
            }
            
            sql += " ORDER BY EMP_CODIGO, EMP_RAZAO";
            strOrdem = sql;

            return BancoDados.GetDataTable(sql, null);

        }

        public bool InserirDados(Empresa dados)
        {
            string sql = " INSERT INTO EMPRESA (EMP_CODIGO, EMP_RAZAO, EMP_FANTASIA, EMP_CGC, EMP_ENDERECO,  EMP_CIDADE, "
                       + " EMP_UF, EMP_FONE, EMP_INSEST, EMP_DESABILITADO,EMP_NUMERO, EMP_DTCADASTRO, EMP_OPCADASTRO ) "
                       + " VALUES("
                       + dados.EmpCodigo + ",'"
                       + dados.EmpRazao + "','"
                       + dados.EmpFantasia + "','"
                       + dados.EmpCgc + "','"
                       + dados.EmpEndereco + "','"
                       + dados.EmpCidade + "','"
                       + dados.EmpUF + "','"
                       + dados.EmpFone + "','"
                       + dados.EmpInsest + "','"
                       + dados.EmpLiberado + "',"
                       + dados.EmpNum + ","
                       + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                       + dados.OpCadastro + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Empresa dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE EMPRESA SET "
                       + " EMP_RAZAO = '" + dadosNovos.EmpRazao + "'"
                       + " ,EMP_FANTASIA = '" + dadosNovos.EmpFantasia + "'"
                       + " ,EMP_CGC = '" + dadosNovos.EmpCgc + "'"
                       + " ,EMP_ENDERECO = '" + dadosNovos.EmpEndereco + "'"
                       + " ,EMP_CIDADE = '" + dadosNovos.EmpCidade + "'"
                       + " ,EMP_UF = '" + dadosNovos.EmpUF + "'"
                       + " ,EMP_NUMERO = " + dadosNovos.EmpNum
                       + " ,EMP_FONE = '" + dadosNovos.EmpFone + "'"
                       + " ,EMP_INSEST = '" + dadosNovos.EmpInsest + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " ,EMP_DESABILITADO = '" + dadosNovos.EmpLiberado + "'"
                       + " ,EMP_OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " WHERE EMP_CODIGO = " + dadosNovos.EmpCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[9, 3];
                int contMatriz = 0;

                if (!dadosNovos.EmpRazao.Equals(dadosAntigos.Rows[0]["EMP_RAZAO"].ToString()))
                {
                    dados[contMatriz, 0] = "EMP_RAZAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_RAZAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpRazao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpFantasia.Equals(dadosAntigos.Rows[0]["EMP_FANTASIA"]))
                {

                    dados[contMatriz, 0] = "EMP_FANTASIA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_FANTASIA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpFantasia + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpCgc.Equals(dadosAntigos.Rows[0]["EMP_CGC"]))
                {

                    dados[contMatriz, 0] = "EMP_CGC";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_CGC"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpCgc + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpEndereco.Equals(dadosAntigos.Rows[0]["EMP_ENDERECO"]))
                {

                    dados[contMatriz, 0] = "EMP_ENDERECO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_ENDERECO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpEndereco + "'";
                    contMatriz = contMatriz + 1;
                }

                if (!dadosNovos.EmpCidade.Equals(dadosAntigos.Rows[0]["EMP_CIDADE"]))
                {

                    dados[contMatriz, 0] = "EMP_CIDADE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_CIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpCidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpUF.Equals(dadosAntigos.Rows[0]["EMP_UF"]))
                {

                    dados[contMatriz, 0] = "EMP_UF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_UF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpUF + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpFone.Equals(dadosAntigos.Rows[0]["EMP_FONE"].ToString()))
                {

                    dados[contMatriz, 0] = "EMP_FONE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_FONE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpFone + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpInsest.Equals(dadosAntigos.Rows[0]["EMP_INSEST"]))
                {

                    dados[contMatriz, 0] = "EMP_INSEST";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_INSEST"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpInsest + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.EmpLiberado.Equals(dadosAntigos.Rows[0]["EMP_DESABILITADO"]))
                {

                    dados[contMatriz, 0] = "EMP_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.EmpLiberado + "'";
                    contMatriz = contMatriz + 1;
                }



                Funcoes.GravaLogAlteracao("EMP_CODIGO", dadosNovos.EmpCodigo.ToString(), Principal.usuario, "EMPRESA", dados, contMatriz, Principal.estAtual, dadosNovos.EmpCodigo);
                return true;
            }
            else
                return false;

        }

        public int ExcluirDados(string empCodigo)
        {
            string sql = "DELETE FROM EMPRESA WHERE EMP_CODIGO = " + empCodigo;

            return BancoDados.ExecuteNoQuery(sql, null);
        }
    }
}
