﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class GrupoFilial
    {
        public int GrupoId { get; set; }
        public string GrupoDescricao { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
