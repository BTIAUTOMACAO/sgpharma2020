﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ListaAbcFarma
    {
        public string MedAbc { get; set; }
        public string LabNome { get; set; }
        public string MedDescricao { get; set; }
        public string MedApresentacao { get; set; }
        public string MedPco { get; set; }
        public string MepPla { get; set; }
        public string MedFra { get; set; }
        public string MedBarra { get; set; }
        public string MedRegistroMS { get; set; }
        public string MedNcm { get; set; }
        public string MedLista { get; set; }
        public string MedGen { get; set; }
        public string MedCas { get; set; }
        public string MedDCB { get; set; }
        public string MedCest { get; set; }
        public string MedPco19 { get; set; }

        public bool InsereAbcFarma(List<ListaAbcFarma> dados)
        {
            BancoDados.ExecuteNoQuery("DELETE FROM ABCFARMA", null);

            for (int i = 0; i < dados.Count; i++)
            {
                string strCmd = "INSERT INTO ABCFARMA(MED_ABC, LAB_NOM, MED_DES, MED_APR, MED_PCO18, MED_PLA18, MED_FRA18, MED_BARRA, MED_REGIMS, "
                          + "MED_NCM, MED_NEGPOS, MED_GENE,MED_CAS,MED_DCB,MED_CEST,MED_PCO20) VALUES ('"
                          + dados[i].MedAbc + "','"
                          + dados[i].LabNome.Replace("'", "") + "','"
                          + (dados[i].MedDescricao.Length > 45 ? dados[i].MedDescricao.Replace("'", "").Substring(0, 44) : dados[i].MedDescricao.Replace("'", "")) + "','"
                          + dados[i].MedApresentacao.Replace("'", "") + "',";
                if (Convert.ToDouble(dados[i].MedFra) > 0)
                {
                    strCmd += dados[i].MedFra.ToString().Replace(",", ".") + ",";
                }
                else
                    strCmd += dados[i].MedPco.ToString().Replace(",", ".") + ",";

                strCmd += dados[i].MepPla.ToString().Replace(",", ".") + ","
                    + dados[i].MedFra.ToString().Replace(",", ".") + ",'"
                    + dados[i].MedBarra + "','"
                    + dados[i].MedRegistroMS + "','"
                    + dados[i].MedNcm + "','"
                    + dados[i].MedLista + "',";

                if (String.IsNullOrEmpty(dados[i].MedGen))
                {
                    strCmd += "'',";
                }
                else
                {
                    strCmd += "'" + dados[i].MedGen + "',";
                }

                strCmd += "'" + dados[i].MedCas + "','" + dados[i].MedDCB + "','" + dados[i].MedCest + "'," + dados[i].MedPco19.ToString().Replace(",", ".") + ")";

                BancoDados.ExecuteNoQuery(strCmd, null);
            }

            return true;
        }
    }
}
