﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class AlteracaoEstoque
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int ID { get; set; }
        public string ProdCodigo { get; set; }
        public string Operacao { get; set; }
        public int Qtde { get; set; }
        public int QtdeAnterior { get; set; }
        public string Observacao { get; set; }
        public long Indice { get; set; }
        public double ProdCusme { get; set; }
        public string Estacao { get; set; }
        public string DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public AlteracaoEstoque() { }

        public AlteracaoEstoque(int empCodigo, int estCodigo, int id, string prodCodigo, string operacao, int qtde, int qtdeAnterior, string observacao,
            long indice, double prodCusme, string estacao, string dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ID = id;
            this.ProdCodigo = prodCodigo;
            this.Operacao = operacao;
            this.Qtde = qtde;
            this.QtdeAnterior = qtdeAnterior;
            this.Observacao = observacao;
            this.Indice = indice;
            this.ProdCusme = prodCusme;
            this.Estacao = estacao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InsereRegistros(AlteracaoEstoque dados)
        {
            string strCmd = "INSERT INTO ALTERACAO_ESTOQUE(EMP_CODIGO, EST_CODIGO, ID, PROD_CODIGO, OPERACAO, QTDE,QTDE_ANTERIOR,OBSERVACAO,PROD_CUSME,INDICE,ESTACAO," +
                "DTCADASTRO,OPCADASTRO) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," + 
                dados.ID + ",'" + 
                dados.ProdCodigo + "','" +
                dados.Operacao + "'," +
                dados.Qtde + "," + 
                dados.QtdeAnterior + ",'" + 
                dados.Observacao + "'," + 
                Funcoes.BValor(dados.ProdCusme) + "," + 
                dados.Indice + ",'" + 
                dados.Estacao + "'," + 
                Funcoes.BDataHora(Convert.ToDateTime(dados.DtCadastro)) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDados(AlteracaoEstoque dados)
        {
            string strSql;

            strSql = "SELECT A.EMP_CODIGO, A.EST_CODIGO, A.ID, A.DTCADASTRO, A.OPCADASTRO, A.PROD_CODIGO, B.PROD_DESCR, A.OPERACAO, A.QTDE, A.QTDE_ANTERIOR"
                     + " FROM ALTERACAO_ESTOQUE A "
                     + " INNER JOIN PRODUTOS B ON (A.PROD_CODIGO = B.PROD_CODIGO)"
                     + " WHERE A.EMP_CODIGO = " + dados.EmpCodigo + " AND A.EST_CODIGO = " + dados.EstCodigo;
            if(!String.IsNullOrEmpty(dados.ProdCodigo))
            {
                strSql += " AND A.PROD_CODIGO = '" + dados.ProdCodigo + "'";
            }

            if(!String.IsNullOrEmpty(Funcoes.RemoveCaracter(DtCadastro)))
            {
                strSql += " AND A.DTCADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(DtCadastro + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(DtCadastro + " 23:59:59"));
            }

            if(!String.IsNullOrEmpty(dados.OpCadastro))
            {
                strSql += " AND A.OPCADASTRO = '" + dados.OpCadastro + "'";
            }
            strSql += "  ORDER BY A.PROD_CODIGO,A.OPCADASTRO";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
