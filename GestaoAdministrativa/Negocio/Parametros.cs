﻿
using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ParametrosBanco
    {
        public static int IdentificaColunaExistente(string nomeTabela, string nomeColuna)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM USER_TAB_COLUMNS WHERE TABLE_NAME = '" + nomeTabela + "' AND COLUMN_NAME = '" + nomeColuna + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        public static void ParametrosCriaouAltera()
        {
            Funcoes.GravaParametro(6, "380", "N", "GERAL", "Libera alteração na promoção", "VENDAS", "Identifica se libera alteração de desconto de produto na promoção", false);

            Funcoes.GravaParametro(6, "381", "N", "GERAL", "Troca Preço de Venda pelo PMC", "VENDAS", "Identifica se troca preço de venda pelo preço do PCM, caso o mesmo esteja maior", false);

            Funcoes.GravaParametro(6, "382", "N", "GERAL", "Somente estações Caixa", "VENDAS", "Identifica se estabelecimento possui somente estações caixa", false);

            Funcoes.GravaParametro(6, "383", "N", "GERAL", "Muda colaborador para itens adicionados", "VENDAS", "Identifica se muda colaborador para pagar comissao em produto adicionados na comanda no caixa", false);

            Funcoes.GravaParametro(6, "384", "S", "GERAL", "Bloqueia estação Caixa Fechado", "VENDAS", "Identifica se valida estação caixa para bloqueio com caixa fechado", false);

            Funcoes.GravaParametro(6, "385", "N", "GERAL", "Permite alterar Venda Particular", "VENDAS", "Identifica se bloquea a alteração de venda Particular na Comanda no Caixa", false);

            Funcoes.GravaParametro(6, "386", "N", "GERAL", "Exibe autorização Drogabella", "VENDAS", "Identifica se exibe número da autorização Drogabella na tela de Vendas", false);

            Funcoes.GravaParametro(6, "387", "N", "GERAL", "Imprime SAT pela Espécies", "VENDAS", "Identifica se imprime espécies do SAT pelo dados da venda.", false);

            Funcoes.GravaParametro(6, "388", "N", "GERAL", "Verifica se tira produto da Promoção", "VENDAS", "Identifica se tira produto da promoção automaticamente, quando venda for de Benefício", false);

            Funcoes.GravaParametro(6, "389", "DROGABELLA/PLANTAO", "GERAL", "Tipo de Venda", "VENDAS", "Resgata o tipo de venda para aplicar ou não o desconto", false);
            
            Funcoes.GravaParametro(6, "390", "N", "GERAL", "Esconde Forma/Espécie de Pagamento", "VENDAS", "Identifica se não mostra a o Formulário de Forma de Pagamento e Espécie na Finalização de alguns tipos de venda.", false);
            
            Funcoes.GravaParametro(6, "392", "07", "GERAL", "Numero PIS/COFINS", "VENDAS", "Numero PIS/COFINS para a geração do xml via SAT", false); //Antes era fixo, por solicitação da Ariane, passa a ser parametro

            Funcoes.GravaParametro(6, "393", "N", "GERAL", "Imprime Protocolo de Entrega no Fiscal", "VENDAS", "Identifica se imprime o protocolo de entrega quando a venda e feita no fiscal", false);
            //ESTOQUE
            Funcoes.GravaParametro(9, "66", "01", "GERAL", "Campo de Preço para Transferencia de Produtos", "ESTOQUE", "Identifica qual campo de realizar a transferencia 1 - Preço Custo | 2 - Preço Venda", false); 

            Funcoes.GravaParametro(9, "63", "N", Principal.nomeEstacao, "Estação Servidor?", "ESTOQUE", "Identifica se estação e servidor para execução do Backup", false);

            Funcoes.GravaParametro(9, "64", "201924", "GERAL", "Data Ultimo Backup", "ESTOQUE", "Identifica data do último backup", false);

            Funcoes.GravaParametro(9, "65", "PROD_ULTCUSME", "GERAL", "Campo de Custo da Tela de Consulta Rápida de Produtos", "ESTOQUE", "Identifica o campo exibido do custo do produto na tela de consulta rápida de produtos", false);

            //IMPRESSORA
            Funcoes.GravaParametro(2, "23", "N", Principal.nomeEstacao, "Salva impressão TXT", "IMPRESSORA", "Identifica se salva impressão em TXT para impressao futura", false);

            Funcoes.GravaParametro(2, "24", "", "GERAL", "Caminho do Arquivo TXT", "IMPRESSORA", "Identifica o caminho que foi salvo a impressão em TXT", false);

            Funcoes.GravaParametro(2, "34", "N", "GERAL", "Verifica impressão pendente?", "IMPRESSORA", "Identifica se verifica impressão pendente, usado para quando se tem Epson e estação quer imprimir", false);

            Funcoes.GravaParametro(2, "35", "N", "GERAL", "Verifica qtde de vias particular?", "IMPRESSORA", "Identifica se verifica qtde de vias do gerencial do particular de acordo com a qtde de parcelas", false);

            Funcoes.GravaParametro(2, "36", "N", "GERAL", "Número da Porta COM", "IMPRESSORA", "Identifica o número da porta COM utilizado em impressora fiscal", false);

            Funcoes.GravaParametro(2, "37", "N", "GERAL", "Verifica qtde de vias por formas de pagamento?", "IMPRESSORA", "Identifica a quantidade de vias de acordo com a forma de pagamento", false);

            //CAIXA
            Funcoes.GravaParametro(4, "80", "N", "GERAL", "Pagamento de particular com Desconto", "CAIXA", "Identifica se estabelecimento efetua baixa de contas particular com desconto na parcela", false);

            Funcoes.GravaParametro(4, "81", "N", "GERAL", "Monsta somente Espécies de Caixa?", "CAIXA", "Identifica se no recebimento de particular so mostra as espécies que estão como ESP_CAIXA igual a S.", false);

            //FUNCIONAL 
            Funcoes.GravaParametro(14, "49", "testefc", "GERAL", "Login para ApiKey Funcional", "BENEFICIOS", "Login para enviar na requisição para realizar a geração da ApiKey", false);

            Funcoes.GravaParametro(14, "50", "teste2013", "GERAL", "Senha para ApiKey Funcional", "BENEFICIOS", "Senha para enviar na requisição para realizar a geração da ApiKey", false);

            //SAT POR ESTAÇÃO//
            Funcoes.GravaParametro(15, "03", Funcoes.LeParametro(6, "334", false) == "" ? "1" : Funcoes.LeParametro(15, "03", false), Principal.nomeEstacao, "Assinatura SAT",
                   "SAT", "Identifica a assinatura do SAT", false);

            Funcoes.GravaParametro(15, "05", Funcoes.LeParametro(6, "329", false) == "" ? "1" : Funcoes.LeParametro(15, "05", false), Principal.nomeEstacao, "Código de Ativação SAT",
                 "SAT", "Identifica o código de ativação do SAT", false);

            Funcoes.GravaParametro(15, "09", Funcoes.LeParametro(15, "09", false), Principal.nomeEstacao, "Fabricante Impressora Nao-Fiscal",
                   "SAT", "Identifica o fabricante da impressora usado para impressão nao-fiscal SAT", false);

            Funcoes.GravaParametro(15, "10", Funcoes.LeParametro(6, "333", false) == "" ? "1" : Funcoes.LeParametro(15, "10", false), Principal.nomeEstacao, "Número de Série SAT ",
              "SAT", "Número de identificação do SAT", false);

            Funcoes.GravaParametro(15, "11", Funcoes.LeParametro(15, "11", false), Principal.nomeEstacao, "Modelo Impressora Nao-Fiscal",
                "SAT", "Identifica o modelo da impressora usado para impressão nao-fiscal SAT", false);

            //GERAL
            Funcoes.GravaParametro(0, "52", "N", "GERAL", "Zera limite após Data Fechamento", "GERAL", "Identifica se não contabiliza parcelas em atraso a partir da data de fechamento vinculada a empresa conveniada.", false);

            //TRIBUTARIOS
            Funcoes.GravaParametro(16, "01", "07", "GERAL", "Numeracao do tipo de operacao CST do PIS E COFINS", "GERAL", "Identifica o tipo de operacao para se calcular certos atributos PIS E COFINS na Nota de Devolução", false);            
        }

        public static void ScriptBancoDeDados()
        {
            if (IdentificaColunaExistente("BENEFICIO_FUNCIONAL_COMANDA", "NOME").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE BENEFICIO_FUNCIONAL_COMANDA ADD NOME VARCHAR2(50) NULL", null);

            if (IdentificaColunaExistente("MOVIMENTO_FINANCEIRO", "VENDA_ID").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE MOVIMENTO_FINANCEIRO ADD VENDA_ID INTEGER NULL", null);

            if (IdentificaColunaExistente("MOVIMENTO_CAIXA", "VENDA_ID").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE MOVIMENTO_CAIXA ADD VENDA_ID INTEGER NULL", null);

            if (IdentificaColunaExistente("MOVIMENTO_CAIXA", "VENDA_ID").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE MOVIMENTO_CAIXA ADD VENDA_ID INTEGER NULL", null);

            if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "CHAVE_ACESSO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD CHAVE_ACESSO VARCHAR2(48) NULL", null);

            if (IdentificaColunaExistente("ENTRADA", "CHAVE_ACESSO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD CHAVE_ACESSO VARCHAR2(48) NULL", null);

            if (IdentificaColunaExistente("SPOOL_ENTRADA_NF", "DATA_HORA_EMISSAO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_NF ADD DATA_HORA_EMISSAO VARCHAR2(19) NULL", null);

            if (IdentificaColunaExistente("ENTRADA", "DATA_HORA_EMISSAO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA ADD DATA_HORA_EMISSAO VARCHAR2(19) NULL", null);


            Principal.dtBusca = IdentificaAlteracaoTipoCampo("CLIFOR", "CF_CEP");
            if (Principal.dtBusca.Rows.Count > 0)
            {
                if (Principal.dtBusca.Rows[0]["DATA_TYPE"].ToString() == "NUMBER")
                {
                    Principal.dtBusca = Util.SelecionaRegistrosTodosOuEspecifico("CLIFOR", "", "CF_TIPO_DOCTO", "1");
                    if (Principal.dtBusca.Rows.Count > 0)
                    {
                        BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR DROP COLUMN CF_CEP", null);

                        if (IdentificaColunaExistente("CLIFOR", "CF_CEP").Equals(0))
                            BancoDados.ExecuteNoQuery("ALTER TABLE CLIFOR ADD CF_CEP VARCHAR2(15) NULL", null);

                        for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                        {
                            BancoDados.ExecuteNoQuery("UPDATE CLIFOR SET CF_CEP = '" + Convert.ToDouble(Principal.dtBusca.Rows[i]["CF_CEP"]).ToString() + "' WHERE CF_DOCTO = '" + Principal.dtBusca.Rows[i]["CF_DOCTO"] + "'", null);
                        }
                    }
                }
            }

            if (IdentificaColunaExistente("SNGPC_ENVIOS", "MENSAGEMVALIDACAO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_ENVIOS ADD MENSAGEMVALIDACAO VARCHAR2(4000) NULL", null);

            if (IdentificaColunaExistente("CONVENIADAS", "CON_OBRIGA_REGRA_DESCONTO").Equals(0))
            {
                BancoDados.ExecuteNoQuery("ALTER TABLE CONVENIADAS ADD CON_OBRIGA_REGRA_DESCONTO VARCHAR2(1) default 'N'", null);

                BancoDados.ExecuteNoQuery("UPDATE CONVENIADAS SET CON_OBRIGA_REGRA_DESCONTO = 'N'", null);
            }

            #region VIEW DA INTEGRAÇÃO COM INTEGRAFARMA
            string strSql;

            strSql = "CREATE OR REPLACE VIEW VIEW_STORES AS"
                        + "   SELECT PAR_DESCRICAO AS ID, B.CNPJ FROM"
                        + "   (SELECT REPLACE(REPLACE(REPLACE(EST_CGC, '.', ''), '/', ''), '-', '') AS CNPJ FROM ESTABELECIMENTOS WHERE EST_DESABILITADO = 'N') B,"
                        + "   PARAMETROS WHERE PAR_TABELA = 9 AND PAR_POSICAO = 52";
            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLOUT_PAYMENT AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           CONCAT(CONCAT(TO_CHAR(B.VENDA_DATA_HORA, 'YYYY-MM-DD'), 'T'),"
                    + "                  TO_CHAR(B.VENDA_DATA_HORA, 'hh24:mi:ss')) AS sellout_timestamp,"
                    + "           B.VENDA_ID AS id,"
                    + "           C.VENDA_ESPECIE_ID AS payment_id,"
                    + "           D.ESP_DESCRICAO AS method,"
                    + "           '' AS bin,"
                    + "           '' AS ccflag_id,"
                    + "           CASE WHEN D.Esp_Caixa = 'S' THEN 'A VISTA' ELSE 'PRAZO' END AS condition,"
                    + "           '' AS operation_id,"
                    + "           '' AS nsu,"
                    + "           '' AS authorization"
                    + "      FROM PARAMETROS A, VENDAS B, VENDAS_ESPECIES C, CAD_ESPECIES D"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.VENDA_ID = C.VENDA_ID"
                    + "       AND B.EMP_CODIGO = C.EMP_CODIGO"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "       AND C.ESP_CODIGO = D.ESP_CODIGO"
                    + "     ORDER BY B.VENDA_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLOUT_ITEMS AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.VENDA_ID AS id,"
                    + "           CONCAT(CONCAT(TO_CHAR(B.VENDA_DATA_HORA, 'YYYY-MM-DD'), 'T'),"
                    + "                  TO_CHAR(B.VENDA_DATA_HORA, 'hh24:mi:ss')) AS sellout_timestamp,"
                    + "           TRIM(C.PROD_CODIGO)AS sku,"
                    + "           C.PROD_ID AS code,"
                    + "           '' AS isbn,"
                    + "           D.PROD_DESCR AS description,"
                    + "           CASE"
                    + "             WHEN B.VENDA_STATUS = 'F' THEN"
                    + "              'N'"
                    + "             ELSE"
                    + "              'S'"
                    + "           END AS cancellation_flag,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as item_addition,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as frete,"
                    + "           COALESCE(E.PROD_CFOP, '5405') AS cfop,"
                    + "           ABS(C.VENDA_ITEM_DIFERENCA) AS item_discount,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as ipi,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as icms,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as item_icms_st,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as pis,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as cofins,"
                    + "           D.PROD_UNIDADE AS measurement_unit,"
                    + "           CAST(C.VENDA_ITEM_UNITARIO AS NUMBER(10, 4)) as unit_value,"
                    + "           CAST(C.VENDA_ITEM_QTDE AS NUMBER(10, 4)) as quantity,"
                    + "           TO_CHAR(E.FAB_CODIGO) AS manufacturer_code"
                    + "      FROM PARAMETROS       A,"
                    + "           VENDAS           B,"
                    + "           VENDAS_ITENS     C,"
                    + "           PRODUTOS         D,"
                    + "           PRODUTOS_DETALHE E"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.VENDA_ID = C.VENDA_ID"
                    + "       AND B.EMP_CODIGO = C.EMP_CODIGO"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "       AND TRIM(C.PROD_CODIGO) = D.PROD_CODIGO"
                    + "       AND TRIM(C.PROD_CODIGO) = E.PROD_CODIGO"
                    + "       AND C.EMP_CODIGO = E.EMP_CODIGO"
                    + "       AND C.EST_CODIGO = E.EST_CODIGO"
                    + "     ORDER BY B.VENDA_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLOUT AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "       B.VENDA_ID AS id,"
                    + "       CONCAT(CONCAT(TO_CHAR(B.VENDA_DATA_HORA, 'YYYY-MM-DD'), 'T'),"
                    + "              TO_CHAR(B.VENDA_DATA_HORA, 'hh24:mi:ss')) AS sellout_timestamp,"
                    + "       COALESCE(B.OP_ALTERACAO, B.OP_CADASTRO) AS nome_operador,"
                    + "       C.COL_NOME AS nome_vendedor,"
                    + "       REPLACE(REPLACE(REPLACE(D.EST_CGC, '.', ''), '/', ''), '-', '') AS CNPJ,"
                    + "       '' as buyer_taxpayer_id,"
                    + "       '' as checkout_id,"
                    + "       E.VENDA_NUM_NOTA AS receipt_number,"
                    + "       '' AS receipt_series_number,"
                    + "       '' AS coo,"
                    + "       E.VENDA_NUM_CFE AS nfe_access_key,"
                    + "       '' AS device_id,"
                    + "       CAST(CASE"
                    + "              WHEN B.VENDA_DIFERENCA >= 0 THEN"
                    + "               B.VENDA_DIFERENCA"
                    + "              ELSE"
                    + "               0"
                    + "            END AS NUMBER(10, 4)) as sales_addition,"
                    + "       CAST(CASE"
                    + "              WHEN B.VENDA_DIFERENCA < 0 THEN"
                    + "               ABS(B.VENDA_DIFERENCA)"
                    + "              ELSE"
                    + "               0"
                    + "            END AS NUMBER(10, 4)) as sales_discount,"
                    + "       B.VENDA_TOTAL AS subtotal,"
                    + "       CASE"
                    + "         WHEN B.VENDA_STATUS = 'F' THEN"
                    + "          'N'"
                    + "         ELSE"
                    + "          'S'"
                    + "       END AS cancellation_flag,"
                    + "       'S' AS operation,"
                    + "       'S' AS transaction_type,"
                    + "       CAST('0,0000' AS NUMBER(10, 4)) as ipi,"
                    + "       CAST('0,0000' AS NUMBER(10, 4)) as iss,"
                    + "       CAST('0,0000' AS NUMBER(10, 4)) as frete,"
                    + "       CASE"
                    + "         WHEN E.VENDA_NUM_CFE IS NULL THEN"
                    + "          'pedido'"
                    + "         else"
                    + "          CASE"
                    + "         WHEN(SELECT PAR_DESCRICAO"
                    + "                 FROM PARAMETROS"
                    + "                WHERE PAR_TABELA = 6"
                    + "                  AND PAR_POSICAO = '355'"
                    + "                  AND ROWNUM = 1) = 'S' THEN"
                    + "          'sat'"
                    + "         ELSE"
                    + "          'ecf'"
                    + "       END END AS tipo"
                    + "  FROM PARAMETROS A, VENDAS B"
                    + "  LEFT JOIN CUPONS_SAT E ON B.VENDA_ID = E.VENDA_ID, COLABORADORES C,"
                    + " ESTABELECIMENTOS D"
                    + " WHERE A.PAR_TABELA = 9"
                    + "   AND A.PAR_POSICAO = 52"
                    + "   AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                         FROM PARAMETROS"
                    + "                        WHERE PAR_TABELA = 6"
                    + "                          AND PAR_POSICAO = '367')"
                    + "   AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                         FROM PARAMETROS"
                    + "                        WHERE PAR_TABELA = 6"
                    + "                          AND PAR_POSICAO = '366')"
                    + "   AND B.VENDA_COL_CODIGO = C.COL_CODIGO"
                    + "   AND B.EMP_CODIGO = C.EMP_CODIGO"
                    + "   AND B.EMP_CODIGO = D.EMP_CODIGO"
                    + " ORDER BY B.VENDA_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLIN_PAYMENT AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.DATA_HORA_EMISSAO AS sellin_timestamp,"
                    + "           CONCAT(B.ENT_ID, B.ENT_DOCTO) AS id,"
                    + "            C.PAG_DOCTO as payment_method_id,"
                    + "           'Boleto' as method,"
                    + "           '' as bin,"
                    + "           '' as ccflag_id,"
                    + "           CASE"
                    + "             WHEN SUM(C.ENT_PARCELA) = 1 THEN"
                    + "              'A VISTA'"
                    + "             ELSE"
                    + "              'PARCELADO'"
                    + "           END AS condition,"
                    + "           '' AS operation_id,"
                    + "           '' AS nsu,"
                    + "           '' AS authorization"
                    + "      FROM PARAMETROS A, ENTRADA B, ENTRADA_DETALHES C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND CONCAT(B.ENT_ID, B.ENT_DOCTO) = CONCAT(C.ENT_ID, C.ENT_DOCTO)"
                    + "       AND C.EMP_CODIGO = B.EMP_CODIGO"
                    + "       AND C.EST_CODIGO = B.EST_CODIGO"
                    + "     GROUP BY A.PAR_DESCRICAO, B.DATA_HORA_EMISSAO, B.ENT_ID, C.PAG_DOCTO,B.ENT_DOCTO"
                    + "     ORDER BY B.ENT_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLIN_ITEMS AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.DATA_HORA_EMISSAO AS sellin_timestamp,"
                    + "           CONCAT(B.ENT_ID, B.ENT_DOCTO) AS id,"
                    + "            D.PROD_ID AS code,"
                    + "           '' as isbn,"
                    + "           D.PROD_DESCR AS description,"
                    + "           TRIM(C.PROD_CODIGO) AS ean,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as addition,"
                    + "           CAST(C.ENT_DESCONTOITEM AS NUMBER(10, 4)) as discount,"
                    + "           CAST(ABS(C.ENT_TOTITEM) AS NUMBER(10, 4)) as net_total,"
                    + "           UPPER(C.ENT_UNIDADE) AS measurement_unit,"
                    + "           CAST((C.ENT_UNITARIO * C.ENT_QTDE) AS NUMBER(10, 4)) as gross_total,"
                    + "           CAST(C.ENT_UNITARIO AS NUMBER(10, 4)) as unit_value,"
                    + "           CAST(C.ENT_QTDESTOQUE AS NUMBER(10, 4)) as quantity,"
                    + "           '' as manufacturer_code,"
                    + "           CAST(C.ENT_VALORICMS AS NUMBER(10, 4)) as icms"
                    + "      FROM PARAMETROS A, ENTRADA B, ENTRADA_ITENS C, PRODUTOS D"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND CONCAT(B.ENT_ID, B.ENT_DOCTO) = CONCAT(C.ENT_ID, C.ENT_DOCTO)"
                    + "       AND B.EMP_CODIGO = C.EMP_CODIGO"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "       AND TRIM(C.PROD_CODIGO) = D.PROD_CODIGO"
                    + "     ORDER BY B.ENT_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_SELLIN AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           CONCAT(B.ENT_ID, B.ENT_DOCTO) AS id,"
                    + "            REPLACE(REPLACE(REPLACE(B.CF_DOCTO, '.', ''), '/', ''), '-', '') AS supplier_taxpayer_id,"
                    + "            B.DATA_HORA_EMISSAO AS sellin_timestamp,"
                    + "           B.CHAVE_ACESSO AS nfe_access_key,"
                    + "           CAST(B.ENT_OUTRAS AS NUMBER(10, 4)) as other_expenses,"
                    + "           '' as seller_id,"
                    + "           CAST(B.ENT_IMPIPI AS NUMBER(10, 4)) as ipi,"
                    + "           CAST(B.ENT_IMPISS AS NUMBER(10, 4)) as iss,"
                    + "           CAST(B.ENT_DESCONTO AS NUMBER(10, 4)) as sales_discount,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as insurance_price,"
                    + "           CAST(B.ENT_TOTAL AS NUMBER(10, 4)) as gross_total,"
                    + "           'N' AS cancellation_flag,"
                    + "           B.ENT_SERIE AS nfe_series_number,"
                    + "           B.ENT_DOCTO AS nfe_number,"
                    + "           CAST('0,0000' AS NUMBER(10, 4)) as sales_addition,"
                    + "           REPLACE(REPLACE(REPLACE(EST_CGC, '.', ''), '/', ''), '-', '') AS store_taxpayer_id,"
                    + "           CAST(B.ENT_DIVERSAS AS NUMBER(10, 4)) AS net_total,"
                    + "           CAST(B.ENT_FRETE AS NUMBER(10, 4)) as freight_price,"
                    + "           CAST(B.ENT_IMPICMS AS NUMBER(10, 4)) as icms"
                    + "      FROM PARAMETROS A, ENTRADA B, ESTABELECIMENTOS C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "       AND C.EST_DESABILITADO = 'N'"
                    + "     ORDER BY B.ENT_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_PRODUCT_SUPPLIERS AS"
                    + "    SELECT DISTINCT (D.PROD_ID)AS code,"
                    + "                    REPLACE(REPLACE(REPLACE(B.CF_DOCTO, '.', ''), '/', ''),"
                    + "                            '-',"
                    + "                            '') AS view_product_suppliers,"
                    + "                    A.PAR_DESCRICAO AS store_id"
                    + "      FROM PARAMETROS A, ENTRADA B, ENTRADA_ITENS C, PRODUTOS_DETALHE D"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.ENT_ID = C.ENT_ID"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "       AND B.EMP_CODIGO = B.EMP_CODIGO"
                    + "       AND C.PROD_CODIGO = D.PROD_CODIGO";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_PRODUCT_RELATED_PRODUCTS AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.PROD_ID AS code,"
                    + "           B.PROD_ID AS internal_code,"
                    + "           B.PROD_CODIGO AS sku"
                    + "    FROM PARAMETROS A, PRODUTOS B, PRODUTOS_DETALHE C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND C.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND C.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.PROD_CODIGO = C.PROD_CODIGO"
                    + "     ORDER BY B.PROD_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_PRODUCT_CUSTOM_ATTRIBUTES AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.PROD_ID AS code,"
                    + "           '' AS attibute_code,"
                    + "           '' AS value"
                    + "      FROM PARAMETROS A, PRODUTOS B, PRODUTOS_DETALHE C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND C.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND C.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.PROD_CODIGO = C.PROD_CODIGO"
                    + "     ORDER BY B.PROD_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_PRODUCT_CATEGORIES AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.DEP_CODIGO AS code,"
                    + "           B.DEP_DESCR AS category"
                    + "    FROM PARAMETROS A, DEPARTAMENTOS B"
                    + "    WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.DEP_DESABILITADO = 'N'"
                    + "     ORDER BY B.DEP_CODIGO";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_PRODUCT AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           D.DAT_ALTERACAO AS dt_ultima_alt,"
                    + "           B.PROD_ID AS code,"
                    + "           B.PROD_CODIGO AS sku,"
                    + "           'simple' AS product_type,"
                    + "           B.PROD_DESCR AS name,"
                    + "           '' AS brand,"
                    + "           'T' AS visibility,"
                    + "           '' AS sale_start_timestamp,"
                    + "           '' AS sale_finish_timestamp,"
                    + "           '' AS current_supplier,"
                    + "           C.PROD_SITUACAO AS status,"
                    + "           B.PROD_DESCR AS description,"
                    + "           B.PROD_ABREV AS short_description,"
                    + "           E.FAB_DESCRICAO AS manufacturer,"
                    + "           '' AS dimension_height_centimeter,"
                    + "           '' AS dimension_weight,"
                    + "           '' AS dimension_measurement_quantity,"
                    + "           '' AS dimension_length_centimeter,"
                    + "           '' AS dimension_width_centimeter,"
                    + "           B.PROD_UNIDADE AS dimension_measurement_unit,"
                    + "           CASE"
                    + "             WHEN C.PROD_ESTATUAL > 0 THEN"
                    + "              'True'"
                    + "             else"
                    + "              'False'"
                    + "           end as inventory_is_in_stock,"
                    + "           'True' as inventory_manage_stock,"
                    + "           C.PROD_ESTATUAL AS inventory_quantity,"
                    + "           '' AS meta_description,"
                    + "           '' AS meta_information_meta_title,"
                    + "           '' AS meta_information_meta_keywords,"
                    + "           D.PRE_VALOR AS price_info_price,"
                    + "           '' AS price_info_special_price_from,"
                    + "           '' AS price_info_special_price_to,"
                    + "           '' AS price_info_special_price,"
                    + "           TO_CHAR(C.FAB_CODIGO)AS manufacturer_code"
                    + "      FROM PARAMETROS A, PRODUTOS B, PRECOS D, PRODUTOS_DETALHE C"
                    + "      LEFT JOIN FABRICANTES E ON(E.FAB_CODIGO = C.FAB_CODIGO AND"
                    + "                                 E.EMP_CODIGO = C.EMP_CODIGO)"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND C.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND C.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.PROD_CODIGO = C.PROD_CODIGO"
                    + "       AND B.PROD_CODIGO = D.PROD_CODIGO"
                    + "       AND C.EMP_CODIGO = D.EMP_CODIGO"
                    + "       AND C.EST_CODIGO = D.EST_CODIGO";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_DIALY_SELLIN_TOTAL AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           TO_CHAR(C.ENT_VENCTO, 'YYYY-MM-DD') AS DATA,"
                    + "           'nfe' as type,"
                    + "           count(c.ent_id) as quantity,"
                    + "           sum(C.ENT_VALPARC) AS amount"
                    + "      FROM PARAMETROS A, ENTRADA B, ENTRADA_DETALHES C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.ENT_ID = C.ENT_ID"
                    + "       AND C.EMP_CODIGO = B.EMP_CODIGO"
                    + "       AND C.EST_CODIGO = B.EST_CODIGO"
                    + "     GROUP BY A.PAR_DESCRICAO, C.ENT_VENCTO"
                    + "     ORDER BY C.ENT_VENCTO";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW VIEW_DAILY_SELLOUT_TOTAL AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           TO_CHAR(B.VENDA_EMISSAO, 'YYYY-MM-DD') AS DATA,"
                    + "           CASE"
                    + "             WHEN(SELECT PAR_DESCRICAO"
                    + "                     FROM PARAMETROS"
                    + "                    WHERE PAR_TABELA = 6"
                    + "                      AND PAR_POSICAO = '355'"
                    + "                      AND ROWNUM = 1) = 'S' THEN"
                    + "              'sat'"
                    + "             ELSE"
                    + "              'ecf'"
                    + "           END AS type,"
                    + "           count(B.VENDA_ID) as quantity,"
                    + "           SUM(B.VENDA_TOTAL) AS amount"
                    + "      FROM PARAMETROS A, VENDAS B"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "     GROUP BY A.PAR_DESCRICAO, B.VENDA_EMISSAO"
                    + "     ORDER BY B.VENDA_EMISSAO";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW SELLOUT_PAYMENT_INSTALLMENTS AS"
                    + "     SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           CONCAT(CONCAT(TO_CHAR(B.VENDA_DATA_HORA, 'YYYY-MM-DD'), 'T'),"
                    + "                  TO_CHAR(B.VENDA_DATA_HORA, 'hh24:mi:ss')) AS sellout_timestamp,"
                    + "           B.VENDA_ID AS id,"
                    + "           1 AS installment_number,"
                    + "           C.VENDA_ESPECIE_ID AS payment_method_id,"
                    + "           CAST(C.VENDA_ESPECIE_VALOR AS NUMBER(20, 4)) as amount,"
                    + "           1 AS payment_term"
                    + "      FROM PARAMETROS A, VENDAS B, VENDAS_ESPECIES C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND B.VENDA_ID = C.VENDA_ID"
                    + "       AND B.EMP_CODIGO = C.EMP_CODIGO"
                    + "       AND B.EST_CODIGO = C.EST_CODIGO"
                    + "     ORDER BY B.VENDA_ID";

            BancoDados.ExecuteNoQuery(strSql, null);

            strSql = "CREATE OR REPLACE VIEW SELLIN_PAYMENT_INSTALLMENTS AS"
                    + "    SELECT A.PAR_DESCRICAO AS store_id,"
                    + "           B.DATA_HORA_EMISSAO AS sellin_timestamp,"
                    + "           CONCAT(B.ENT_ID, B.ENT_DOCTO) AS id,"
                    + "            c.pag_docto as payment_method_id,"
                    + "           C.ENT_PARCELA AS installment_number,"
                    + "           CAST(C.ENT_VALPARC AS NUMBER(10, 4)) as discount,"
                    + "           C.ENT_VENCTO - B.ENT_DTEMIS AS payment_term"
                    + "      FROM PARAMETROS A, ENTRADA B, ENTRADA_DETALHES C"
                    + "     WHERE A.PAR_TABELA = 9"
                    + "       AND A.PAR_POSICAO = 52"
                    + "       AND B.EMP_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '367')"
                    + "       AND B.EST_CODIGO = (SELECT PAR_DESCRICAO"
                    + "                             FROM PARAMETROS"
                    + "                            WHERE PAR_TABELA = 6"
                    + "                              AND PAR_POSICAO = '366')"
                    + "       AND CONCAT(B.ENT_ID, B.ENT_DOCTO) = CONCAT(C.ENT_ID, C.ENT_DOCTO)"
                    + "       AND C.EMP_CODIGO = B.EMP_CODIGO"
                    + "       AND C.EST_CODIGO = B.EST_CODIGO"
                    + "     ORDER BY B.ENT_ID";

            BancoDados.ExecuteNoQuery(strSql, null);
            #endregion

            if (IdentificaColunaExistente("PRODUTOS", "PROD_PORTARIA_CONTROLADO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS ADD PROD_PORTARIA_CONTROLADO VARCHAR2(2) NULL", null);

            if (IdentificaColunaExistente("SNGPC_LAC_INVENTARIO", "LAC_INV_ENVIADO").Equals(0))
            {
                BancoDados.ExecuteNoQuery("ALTER TABLE SNGPC_LAC_INVENTARIO ADD LAC_INV_ENVIADO VARCHAR2(1) default 'N' ", null);

                BancoDados.ExecuteNoQuery("UPDATE SNGPC_LAC_INVENTARIO SET LAC_INV_ENVIADO = 'S'", null);
            }

            #region CAMPOS FALTANTES DO SISTEMA ANTIGO
            if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CFOP").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CFOP VARCHAR2(10) NULL", null);

            if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CST VARCHAR2(10) NULL", null);

            if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "PROD_CFOP").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD PROD_CFOP VARCHAR2(5) NULL", null);

            if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "PROD_CST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD PROD_CST VARCHAR2(5) NULL", null);

            if (IdentificaColunaExistente("ENTRADA_ITENS", "PROD_CFOP").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD PROD_CFOP VARCHAR2(5) NULL", null);

            if (IdentificaColunaExistente("ENTRADA_ITENS", "PROD_CST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD PROD_CST VARCHAR2(5) NULL", null);

            if (IdentificaColunaExistente("SPOOL_ENTRADA_ITENS_NF", "PROD_CEST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE SPOOL_ENTRADA_ITENS_NF ADD PROD_CEST VARCHAR2(10) NULL", null);

            if (IdentificaColunaExistente("ENTRADA_ITENS", "PROD_CEST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE ENTRADA_ITENS ADD PROD_CEST VARCHAR2(10) NULL", null);

            if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CEST").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CEST VARCHAR2(10) NULL", null);

            if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_ALIQ_ICMS").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_ALIQ_ICMS NUMBER(18,2)", null);

            if (IdentificaColunaExistente("PRODUTOS_DETALHE", "PROD_CFOP_DEVOLUCAO").Equals(0))
                BancoDados.ExecuteNoQuery("ALTER TABLE PRODUTOS_DETALHE ADD PROD_CFOP_DEVOLUCAO VARCHAR2(4) NULL", null);
            #endregion

            #region LOG_PRECOS
            if (IdentificaTabelaExistente("LOG_PRECOS").Equals(0))
            {
                strSql = "create table LOG_PRECOS"
                        + "        ("
                        + "          LOG_OPERADOR VARCHAR2(30) not null,"
                        + "          CODBARRAS    VARCHAR2(15) not null,"
                        + "          LOG_ANTERIOR NUMBER(9, 2) not null,"
                        + "          LOG_ALTERADO NUMBER(9, 2) not null,"
                        + "          LOG_DATA     DATE,"
                        + "          EST_CODIGO   INTEGER,"
                        + "          EMP_CODIGO   INTEGER"
                        + "        )";
                BancoDados.ExecuteNoQuery(strSql, null);

                if (IdentificaSynonyms("LOG_PRECOS").Equals(0))
                {
                    CriaSynonyms("LOG_PRECOS");
                }
            }
            #endregion
            
        }

        public static DataTable IdentificaAlteracaoTipoCampo(string nomeTabela, string nomeCampo)
        {
            DataTable r = new DataTable();
            return r = BancoDados.GetDataTable("SELECT * FROM USER_TAB_COLUMNS WHERE TABLE_NAME = '" + nomeTabela + "' AND COLUMN_NAME = '" + nomeCampo + "'", null);
        }

        public static int IdentificaViewExistente(string nomeView)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM USER_VIEWS WHERE VIEW_NAME = '" + nomeView + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Identifica se Tabela já existe
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <returns></returns>
        public static int IdentificaTabelaExistente(string nomeTabela)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM USER_TABLES WHERE TABLE_NAME = '" + nomeTabela + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Verifica se Synonyms está criado
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        /// <returns></returns>
        public static int IdentificaSynonyms(string nomeTabela)
        {
            using (DataTable r = BancoDados.GetDataTable("SELECT * FROM ALL_SYNONYMS WHERE SYNONYM_NAME = '" + nomeTabela + "'", null))
            {
                if (r.Rows.Count > 0)
                    return 1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Cria Synonyms
        /// </summary>
        public static void VerificaSynonyms()
        {
            DataTable dtTabelas = BancoDados.GetDataTable(" SELECT TABLE_NAME FROM USER_TABLES ", null);

            for (int i = 0; i < dtTabelas.Rows.Count; ++i)
            {
                if (IdentificaSynonyms(dtTabelas.Rows[i]["TABLE_NAME"].ToString()).Equals(0))
                {
                    CriaSynonyms(dtTabelas.Rows[i]["TABLE_NAME"].ToString());
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Cria Synonyms
        /// </summary>
        /// <param name="nomeTabela">Nome da Tabela</param>
        public static void CriaSynonyms(string nomeTabela)
        {
            BancoDados.ExecuteNoQuery("CREATE PUBLIC SYNONYM " + nomeTabela + " FOR GADM." + nomeTabela, null);
        }

    }
}
