﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Titulo
    {
        public int TilCodigo { get; set; }
        public string TilDescr { get; set; }
        public string TilLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public Titulo() { }
    }
}
