﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ProdutosTroca
    {
        public int ID { get; set; }
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public double Preco { get; set; }
        public double Desconto { get; set; }
        public double Total { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public int Identificacao { get; set; }
        public string Status { get; set; }
        public long VendaID { get; set; }
        public int ColCodigo { get; set; }

        public ProdutosTroca() { }

        public ProdutosTroca(int id, int empCodigo, int estCodigo, string prodCodigo, int qtde, double preco, double desconto, double total, string opCadastro, DateTime dtCadastro,
            int identificacao, string status, long vendaID, int colCodigo)
        {
            this.ID = id;
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.ProdCodigo = prodCodigo;
            this.Qtde = qtde;
            this.Preco = preco;
            this.Desconto = desconto;
            this.Total = total;
            this.OpCadastro = opCadastro;
            this.DtCadastro = dtCadastro;
            this.Identificacao = identificacao;
            this.Status = status;
            this.VendaID = vendaID;
            this.ColCodigo = colCodigo;
        }

        public bool InsereRegistros(ProdutosTroca dados)
        {
            string strCmd = "INSERT INTO PRODUTOS_TROCA(ID, EMP_CODIGO, EST_CODIGO, PROD_CODIGO, QTDE, PRECO, DESCONTO, TOTAL, DTCADASTRO, OPCADASTRO, COMANDA, STATUS,VENDA_ID, COL_CODIGO) VALUES (" +
               dados.ID + "," +
               dados.EmpCodigo + "," +
               dados.EstCodigo + ",'" +
               dados.ProdCodigo + "'," +
               dados.Qtde + "," +
               Funcoes.BValor(dados.Preco) + "," +
               Funcoes.BValor(dados.Desconto) + "," +
               Funcoes.BValor(dados.Total) + "," +
               Funcoes.BDataHora(dados.DtCadastro) + ",'" +
               dados.OpCadastro + "'," + dados.Identificacao + ",'" + dados.Status + "'," + dados.VendaID + "," + dados.ColCodigo + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public string VerificaNumIdentificacaoAberta(int empCodigo, int estCodigo, int numIdentificacao, string status)
        {
            string sql = "SELECT STATUS FROM PRODUTOS_TROCA  WHERE STATUS = '" + status + "' AND COMANDA = " + numIdentificacao + " AND EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public DataTable BuscaDevolucaoPorIdentificacao(int empCodigo, int estCodigo, int numIdentificao, string status)
        {
            string sql = " SELECT A.*, B.PROD_ID, B.PROD_DESCR FROM PRODUTOS_TROCA A, PRODUTOS B WHERE A.EMP_CODIGO = " + empCodigo + " AND A.EST_CODIGO = " + estCodigo + " AND A.COMANDA = " + numIdentificao
                + " AND A.PROD_CODIGO = B.PROD_CODIGO AND A.STATUS = '" + status + "'";
            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaStatus(int empCodigo, int estCodigo, int numIdentificacao, string status, long vendaID = 0)
        {
            string strCmd = "UPDATE PRODUTOS_TROCA SET STATUS = '" + status + "' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND COMANDA = " + numIdentificacao;
            if (vendaID > 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;

        }

        public int IdentificaSeOProdutoEoMesmoEQtde(int empCodigo, int estCodigo, int numIdentificacao, long vendaID, string prodCodigo)
        {
            string sql = "SELECT COALESCE(B.VENDA_ITEM_QTDE - A.QTDE,0) AS QTDE"
                        + "    FROM PRODUTOS_DEVOLVIDOS A"
                        + "    INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                        + "    WHERE A.PROD_CODIGO = B.PROD_CODIGO"
                        + "    AND A.COMANDA = " + numIdentificacao + " AND A.EMP_CODIGO = " + empCodigo + " AND A.EST_CODIGO = " + estCodigo
                        + "    AND A.PROD_CODIGO = '" + prodCodigo + "' AND A.VENDA_ID = " + vendaID
                        + "    AND A.EST_CODIGO = B.EST_CODIGO AND A.EMP_CODIGO = B.EMP_CODIGO";

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }
    }
}
