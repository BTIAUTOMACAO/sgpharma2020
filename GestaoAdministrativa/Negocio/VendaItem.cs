﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class VendaItem
    {
        public int ProdID { get; set; }
        public string ItemECF { get; set; }
        public string ItemQtde { get; set; }
        public string ItemVlUnit { get; set; }
        public string ItemTipoDesc { get; set; }
        public string ItemVlDesc { get; set; }
        public string ItemCodBarra { get; set; }
        public string ItemUniade { get; set; }
        public string ItemDescr { get; set; }
        public string ItemNcm { get; set; }
        public string ItemTipo { get; set; }
        public string ItemAliquota { get; set; }
        public double ItemFederal { get; set; }
        public double ItemEstadual { get; set; }
        public double ItemMunicipal { get; set; }
    }
}
