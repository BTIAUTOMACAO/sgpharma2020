﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class RelatorioProdutosComprovante
    {
        public int CobrancaID { get; set; }
        public string ProdCodigo { get; set; }
        public string ProdDescricao { get; set; }
        public int ItemQtde { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal TotalItens { get; set; }


        public DataTable BuscaProdutosComprovante(List<object> cobrancaID)
        {
            string id = string.Join(", ", cobrancaID);
            string sql = " SELECT COB.COBRANCA_ID as CobrancaID, "
                       + " VI.PROD_CODIGO as ProdCodigo, "
                       + " PRO.PROD_DESCR as ProdDescricao, "
                       + " VI.VENDA_ITEM_QTDE as ItemQtde, "
                       + " VI.VENDA_ITEM_UNITARIO as ValorUnitario, "
                       + " (VI.VENDA_ITEM_DIFERENCA / VI.VENDA_ITEM_QTDE) AS ValorDesconto, "
                       + " VI.VENDA_ITEM_TOTAL as TotalItens, "
                       + " PRO.PROD_UNIDADE "
                       + " FROM VENDAS_ITENS VI "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_ID = VI.PROD_ID) "
                       + " INNER JOIN COBRANCA COB ON(COB.COBRANCA_VENDA_ID = VI.VENDA_ID) "
                       + " WHERE COB.COBRANCA_ID IN(" + id + ") "
                       + " ORDER BY COB.COBRANCA_ID, VI.VENDA_ITEM";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
