﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcControleVendas
    {
        public string ProCodigo { get; set; }
        public string Lote { get; set; }
        public int Qtde { get; set; }
        public DateTime Data { get; set; }

        public SngpcControleVendas() { }

        public SngpcControleVendas(string proCodigo, string lote, int qtde, DateTime data)
        {
            this.ProCodigo = proCodigo;
            this.Lote = lote;
            this.Qtde = qtde;
            this.Data = data;
        }

        public DataTable BuscaLotes(string codigo)
        {
            string sql = "SELECT QTDE, LOTE FROM SNGPC_CONTROLE_VENDAS  WHERE PROD_CODIGO = '" + codigo + "' AND QTDE > 0";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaProduto(string codigoBarra, string lote)
        {
            string sql = " SELECT PROD_CODIGO, LOTE, QTDE  FROM SNGPC_CONTROLE_VENDAS WHERE ROWNUM <= 1  AND  PROD_CODIGO = '" + codigoBarra + "'"
                       + " AND LOTE = '" + lote + "'";

            return BancoDados.GetDataTable(sql, null);
        }
        
        public bool AtualizaQtde(SngpcControleVendas dadosNovos)
        {
            try
            {
                string sql = " UPDATE SNGPC_CONTROLE_VENDAS SET "
                           + " QTDE = " + dadosNovos.Qtde
                           + " WHERE PROD_CODIGO = '" + dadosNovos.ProCodigo + "' AND LOTE = '" + dadosNovos.Lote + "'";

                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Incluir(SngpcControleVendas dados)
        {
            string sql = " INSERT INTO SNGPC_CONTROLE_VENDAS (PROD_CODIGO, LOTE, QTDE, DATA) VALUES ('"
                       + dados.ProCodigo + "','"
                       + dados.Lote + "',"
                       + dados.Qtde + ","
                       + Funcoes.BDataHora(dados.Data) + ")";
            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("PROD_CODIGO", dados.ProCodigo, Principal.usuario.ToUpper(), "SNGPC_CONTROLE_VENDAS", dados.Lote, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExcluirDados(string prod_codigo, string lote)
        {
            string sql = "DELETE FROM SNGPC_CONTROLE_VENDAS WHERE "
                + " PROD_CODIGO = '" + prod_codigo + "' AND LOTE = '" + lote + "'";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }



        public bool ExcluirPorCodigoLote(string prodCodigo, string lote)
        {
            string sql = "DELETE FROM SNGPC_CONTROLE_VENDAS WHERE PROD_CODIGO = '" + prodCodigo + "' AND LOTE = '" + Lote + "'";
            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public int IdentificaSeJaTemLoteEProduto(string prodCodigo, string lote)
        {
            string sql = "SELECT QTDE FROM SNGPC_CONTROLE_VENDAS  WHERE PROD_CODIGO = '" + prodCodigo + "' AND LOTE = '" + lote + "'";
            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public bool SomaQtdeMesmoLoteEProduto(SngpcControleVendas dadosNovos)
        {
            try
            {
                string sql = " UPDATE SNGPC_CONTROLE_VENDAS SET "
                           + " QTDE = QTDE + " + dadosNovos.Qtde
                           + " , DATA = " + Funcoes.BDataHora(dadosNovos.Data)
                           + " WHERE PROD_CODIGO = '" + dadosNovos.ProCodigo + "' AND LOTE = '" + dadosNovos.Lote + "'";

                if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool SubtraiQtdeMesmoLoteEProduto(SngpcControleVendas dadosNovos)
        {
            try
            {
                string sql = " UPDATE SNGPC_CONTROLE_VENDAS SET "
                           + " QTDE = QTDE - " + dadosNovos.Qtde
                           + " , DATA = " + Funcoes.BDataHora(dadosNovos.Data)
                           + " WHERE PROD_CODIGO = '" + dadosNovos.ProCodigo + "' AND LOTE = '" + dadosNovos.Lote + "'";

                if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool ExcluirTodosRegistros()
        {
            string sql = "DELETE FROM SNGPC_CONTROLE_VENDAS";
            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public int IdentificaSeJaTemLoteEProdutoExclusao(string prodCodigo, string lote)
        {
            string sql = "SELECT QTDE FROM SNGPC_CONTROLE_VENDAS  WHERE PROD_CODIGO = '" + prodCodigo + "' AND LOTE = '" + lote + "'";
            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return -1;
            else
                return Convert.ToInt32(r);
        }

    }
}
