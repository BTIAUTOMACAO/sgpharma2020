﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    class PedidosItens
    {
        public int est_codigo { get; set; }
        public long ped_codigo { get; set; }
        public int ped_item_seq { get; set; }
        public int prod_codigo { get; set; }
        public int ped_item_qtde { get; set; }
        public decimal ped_item_vlunit { get; set; }
        public decimal ped_item_subtotal { get; set; }
        public decimal ped_item_difer { get; set; }
        public decimal ped_item_total { get; set; }
        public int uni_codigo { get; set; }
        public decimal ped_item_comissao { get; set; }
        public decimal pre_valor { get; set; }
        public decimal prod_cusme { get; set; }
        public string imp_ecf { get; set; }
        public int col_codigo { get; set; }
        public string ped_item_promo { get; set; }
        public string ped_item_desc_lib { get; set; }

        public PedidosItens() { }

        public static bool InserirDados(PedidosItens dados)
        {
            MontadorSql mont;
            if (String.IsNullOrEmpty(GetConsultaProduto(dados.est_codigo, dados.ped_codigo, dados.prod_codigo)))
            {
                mont = new MontadorSql("pedidos_itens", MontadorType.Insert);
            }
            else
            {
                mont = new MontadorSql("pedidos_itens", MontadorType.Update);
                mont.SetWhere("WHERE est_codigo = " + dados.est_codigo + " and ped_codigo = " + dados.ped_codigo + " and prod_codigo = " + dados.prod_codigo, null);
            }
            mont.AddField("est_codigo", dados.est_codigo);
            mont.AddField("ped_codigo", dados.ped_codigo);
            mont.AddField("ped_item_seq", dados.ped_item_seq);
            mont.AddField("prod_codigo", dados.prod_codigo);
            mont.AddField("ped_item_qtde", dados.ped_item_qtde);
            mont.AddField("ped_item_vlunit", dados.ped_item_vlunit);
            mont.AddField("ped_item_subtotal", dados.ped_item_subtotal);
            mont.AddField("ped_item_difer", dados.ped_item_difer);
            mont.AddField("uni_codigo", dados.uni_codigo);
            mont.AddField("ped_item_comissao", dados.ped_item_comissao);
            mont.AddField("pre_valor", dados.pre_valor);
            mont.AddField("prod_cusme", dados.prod_cusme);
            mont.AddField("imp_ecf", dados.imp_ecf);
            mont.AddField("col_codigo", dados.col_codigo);
            mont.AddField("ped_item_promo", dados.ped_item_promo);
            mont.AddField("ped_item_desc_lib", dados.ped_item_desc_lib);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static string GetConsultaProduto(int estCodigo, long pedCodigo, int prodCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("ped_codigo", pedCodigo));
            ps.Add(new Fields("prod_codigo", prodCodigo));

            Principal.strSql = "SELECT PROD_CODIGO FROM PEDIDOS_ITENS WHERE EST_CODIGO = @est_codigo AND PED_CODIGO = @ped_codigo AND PROD_CODIGO = @prod_codigo";

            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

    }
}
