﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class CobrancaMovimento
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int CobrancaMovID { get; set; }
        public int CobrancaParcelaID { get; set; }
        public int CobrancaID { get; set; }
        public DateTime CobrancaMovDataBaixa { get; set; }
        public double CobrancaMovValorPago { get; set; }
        public double CobrancaMovValorAcrecimo { get; set; }
        public DateTime DataCadastro { get; set; }
        public string OperadorCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }
        public string OperadorAlteracao { get; set; }
        public double CobrancaMovValorParcela { get; set; }
        public double CobrancaMovValorJuros { get; set; }

        public CobrancaMovimento() { }

        public CobrancaMovimento(int empCodigo, int estCodigo, int cobrancaMovID, int cobrancaParcelaID, int cobrancaID, DateTime cobrancaMovDataBaixa,
                                 double cobrancaMovValorPago, double cobrancaMovValorAcrecimo, DateTime dataCadastro, string operadorCadastro,
                                 DateTime dataAlteracao, string operadorAlteracao, double cobrancaMovValorParcela, double cobrancaMovValorJuros)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.CobrancaMovID = cobrancaMovID;
            this.CobrancaParcelaID = cobrancaParcelaID;
            this.CobrancaID = cobrancaID;
            this.CobrancaMovDataBaixa = cobrancaMovDataBaixa;
            this.CobrancaMovValorPago = cobrancaMovValorPago;
            this.CobrancaMovValorAcrecimo = cobrancaMovValorAcrecimo;
            this.DataCadastro = dataCadastro;
            this.OperadorCadastro = operadorCadastro;
            this.DataAlteracao = dataAlteracao;
            this.OperadorAlteracao = operadorAlteracao;
            this.CobrancaMovValorParcela = cobrancaMovValorParcela;
            this.CobrancaMovValorJuros = cobrancaMovValorJuros;
        }

        public bool InsereDados(CobrancaMovimento dados)
        {
            string sql = " INSERT INTO COBRANCA_MOVIMENTO ( EMP_CODIGO, EST_CODIGO, COBRANCA_MOV_ID, COBRANCA_PARCELA_ID, COBRANCA_ID, COBRANCA_MOV_DT_BAIXA,"
                       + " COBRANCA_MOV_VL_PAGO, COBRANCA_MOV_VL_ACRESCIMO, DT_CADASTRO, OP_CADASTRO,COBRANCA_MOV_VALOR_PARCELA, COBRANCA_MOV_VALOR_JUROS) VALUES("
                       + dados.EmpCodigo + ","
                       + dados.EstCodigo + ","
                       + dados.CobrancaMovID + ","
                       + dados.CobrancaParcelaID + ","
                       + dados.CobrancaID + ","
                       + Funcoes.BDataHora(dados.CobrancaMovDataBaixa) + ","
                       + Funcoes.BValor(dados.CobrancaMovValorPago) + ","
                       + Funcoes.BValor(dados.CobrancaMovValorAcrecimo) + ","
                       + Funcoes.BDataHora(dados.DataCadastro) + ",'"
                       + dados.OperadorCadastro + "',"
                       + Funcoes.BValor(dados.CobrancaMovValorParcela) + ","
                       + Funcoes.BValor(dados.CobrancaMovValorJuros) + ")";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool ExcluirCobrancaMovimento(DateTime dtRecebimento)
        {
            string sql = "DELETE FROM COBRANCA_MOVIMENTO WHERE COBRANCA_MOV_DT_BAIXA = " + Funcoes.BDataHora(dtRecebimento);

            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public DataTable BuscaMovimentoPorData(DateTime dtRecebimento)
        {
            string sql = " SELECT CM.COBRANCA_MOV_ID,  "
                       + "        TO_CHAR(CP.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY') AS COBRANCA_PARCELA_VENCIMENTO,"
                       + " COBRANCA_MOV_VL_ACRESCIMO,  "
                       + " CP.COBRANCA_PARCELA_NUM, "
                       + " CM.COBRANCA_MOV_VL_PAGO, CM.COBRANCA_MOV_VALOR_PARCELA, CM.COBRANCA_MOV_VALOR_JUROS "
                       + " FROM COBRANCA_MOVIMENTO CM "
                       + " INNER JOIN COBRANCA_PARCELA CP ON(CP.COBRANCA_PARCELA_ID = CM.COBRANCA_PARCELA_ID) "
                       + " WHERE COBRANCA_MOV_DT_BAIXA = " + Funcoes.BDataHora(dtRecebimento)
                       + " ORDER BY CM.COBRANCA_MOV_ID";

            return BancoDados.GetDataTable(sql, null);
        }
        public DataTable BuscaMovimentoPorIntervaloDatas(DateTime dtIncial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT CM.COBRANCA_MOV_ID, "
                       + " TO_CHAR(CP.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY') AS COBRANCA_PARCELA_VENCIMENTO, "
                       + " COBRANCA_MOV_VL_ACRESCIMO, "
                       + " CP.COBRANCA_PARCELA_NUM, "
                       + " CM.COBRANCA_MOV_VL_PAGO, "
                       + " CLI.CF_NOME, "
                       + "        TO_CHAR(CM.COBRANCA_MOV_DT_BAIXA, 'DD/MM/YYYY') AS COBRANCA_MOV_DT_BAIXA"
                       + " FROM COBRANCA_MOVIMENTO CM "
                       + " INNER JOIN COBRANCA_PARCELA CP ON(CP.COBRANCA_PARCELA_ID = CM.COBRANCA_PARCELA_ID) "
                       + " INNER JOIN COBRANCA COB ON(COB.COBRANCA_ID = CM.COBRANCA_ID) "
                       + " INNER JOIN CLIFOR CLI ON(CLI.CF_ID = COB.COBRANCA_CF_ID) "
                       + " WHERE CM.COBRANCA_MOV_DT_BAIXA BETWEEN " + Funcoes.BDataHora(dtIncial) + " AND " + Funcoes.BDataHora(dtFinal);

            if (!usuario.Equals("TODOS"))
            {
                sql += " AND CM.OP_CADASTRO = '" + usuario + "'";
            }
            sql += " ORDER BY CM.COBRANCA_MOV_ID";

            return BancoDados.GetDataTable(sql, null);
        }


    }


}