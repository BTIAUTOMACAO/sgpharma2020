﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class PrincipioAtivo
    {
        public int EstCodigo { get; set; }
        public int PriCodigo { get; set; }
        public string PriDescr { get; set; }
        public string PriLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public PrincipioAtivo() { }

        public PrincipioAtivo(int estCodigo, int priCodigo, string priDescr, string priLiberado, DateTime dtAlteracao,
                         string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.EstCodigo = estCodigo;
            this.PriCodigo = priCodigo;
            this.PriDescr = priDescr;
            this.PriLiberado = priLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public DataTable BuscarDados(int empCodigo, string priCodigo, string priDescricao, string desabilitado, out string strOrdem)
        {
            string sql = " SELECT EMP_CODIGO, PRI_CODIGO, PRI_DESCRICAO,"
                       + " CASE PRI_DESABILITADO "
                       + " WHEN 'N' THEN 'S' "
                       + " ELSE 'N' END  AS PRI_DESABILITADO, "
                       + " OPALTERACAO, DTCADASTRO, OPCADASTRO,DTALTERACAO FROM PRINCIPIO_ATIVO WHERE EMP_CODIGO = " + empCodigo;
            if (priCodigo == "" && priDescricao == "" && desabilitado == "TODOS")
            {
                strOrdem = sql;
                sql += " ORDER BY PRI_DESCRICAO ASC";
            }
            else
            {
                if (priCodigo != "")
                {

                    sql += "AND PRI_CODIGO = " + priCodigo;
                }
                if (priDescricao != "")
                {

                    sql += "AND PRI_DESCRICAO LIKE '%" + priDescricao + "%'";
                }
                if (desabilitado != "TODOS")
                {
                    desabilitado = desabilitado == "S" ? "N" : "S";
                    sql += "AND PRI_DESABILITADO = '" + desabilitado + "'";
                }

                strOrdem = sql;
                sql += " ORDER BY PRI_DESCRICAO ASC";
            }

            return BancoDados.GetDataTable(sql, null);
        }

        public bool InserirDados(PrincipioAtivo dados)
        {
            string sql = " INSERT INTO PRINCIPIO_ATIVO(EMP_CODIGO, PRI_CODIGO, PRI_DESCRICAO, PRI_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES( "
                       + dados.EstCodigo + ","
                       + dados.PriCodigo + ",'"
                       + dados.PriDescr + "','"
                       + dados.PriLiberado + "',"
                       + Funcoes.BDataHora(dados.DtAlteracao) + ",'"
                       + dados.OpAlteracao + "')";
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(PrincipioAtivo dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE PRINCIPIO_ATIVO SET "
                       + " PRI_DESCRICAO = '" + dadosNovos.PriDescr + "'"
                       + " ,PRI_DESABILITADO = '" + dadosNovos.PriLiberado + "'"
                       + " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " ,DTALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " WHERE PRI_CODIGO = " + dadosNovos.PriCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[2, 3];
                int contMatriz = 0;

                if (!dadosNovos.PriDescr.Equals(dadosAntigos.Rows[0]["PRI_DESCRICAO"]))
                {
                    dados[contMatriz, 0] = "PRI_DESCRICAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PRI_DESCRICAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.PriDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.PriLiberado.Equals(dadosAntigos.Rows[0]["PRI_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "PRI_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PRI_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.PriLiberado + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("PRI_CODIGO", dadosNovos.PriCodigo.ToString(), Principal.usuario, "PRINCIPIO_ATIVO", dados, contMatriz, dadosNovos.EstCodigo);

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExcluirDados(int priCodigo) {

            string sql = "  DELETE FROM PRINCIPIO_ATIVO WHERE EMP_CODIGO= " + Principal.empAtual + " AND PRI_CODIGO = " + Math.Truncate(Convert.ToDouble(priCodigo));
           
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
