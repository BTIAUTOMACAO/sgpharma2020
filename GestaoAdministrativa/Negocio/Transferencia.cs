﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Transferencia
    {
        public int ID { get; set; }
        public string Observacao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public int CodLoja { get; set; }
        public int GrupoID { get; set; }
        public int CodLojaDestino { get; set; }
        public int Codigo { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public Transferencia() { }

        public bool InsereRegistros(int estCodigo, int empCodigo, int id, string observacao, string estacao, string loja, int codLoja)
        {
            string strCmd = "INSERT INTO TRANSFERENCIA(EST_CODIGO,EMP_CODIGO, ID, OBSERVACAO, ESTACAO, LOJA, DTCADASTRO, OPCADASTRO, CODIGO_LOJA) VALUES (" +
                estCodigo + "," +
                empCodigo + "," +
                id + ",'" +
                observacao + "','" +
                estacao + "','" +
                loja + "'," + 
                Funcoes.BDataHora(DateTime.Now) + ",'" +
                Principal.usuario + "'," + codLoja + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaTransferencias(string dtInicial, string dtFinal, int empCodigo, int estCodigo, string usuario)
        {
            string sql = "SELECT A.ID,A.DTCADASTRO, A.OPCADASTRO, A.LOJA, A.OBSERVACAO "
                        + "    FROM TRANSFERENCIA A "
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EST_CODIGO = " + estCodigo
                        + "   AND A.DTCADASTRO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
            if(!String.IsNullOrEmpty(usuario))
            {
                sql += " AND A.OPCADASTRO = '" + usuario + "'";
            }
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable ReimpressaoDeTransferencias(int empCodigo, int estCodigo, string id)
        {
            string sql = "SELECT A.Id, A.OBSERVACAO, A.DTCADASTRO, A.OPCADASTRO, A.LOJA, B.PROD_CODIGO, C.PROD_DESCR, B.QTDE, B.VALOR"
                        + "    FROM TRANSFERENCIA A"
                        + "    INNER JOIN TRANSFERENCIA_ITENS B ON A.ID = B.ID"
                        + "    INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EST_CODIGO = " + estCodigo
                        + "    AND B.EMP_CODIGO = A.EMP_CODIGO"
                        + "    AND B.EST_CODIGO = B.EST_CODIGO";
       
            if (!String.IsNullOrEmpty(id))
            {
                sql += " AND A.ID = " + id;
            }
            
            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizarCodigoTransferencia(long codigo, int estCodigo, int empCodigo, long id)
        {
            string strCmd = "UPDATE TRANSFERENCIA SET CODIGO = " + codigo + " WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND ID = " + id;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool ExcluirDados(int estCodigo, int empCodigo, long id)
        {
            string strCmd = "DELETE FROM TRANSFERENCIA_ITENS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND ID = " + id;

            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                strCmd = "DELETE FROM TRANSFERENCIA WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND ID = " + id;
                if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}
