﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class Conveniadas
    {
        public int ConID { get; set; }
        public int ConCodigo { get; set; }
        public string ConNome { get; set; }
        public string ConEndereco { get; set; }
        public string ConBairro { get; set; }
        public string ConCidade { get; set; }
        public string ConCep { get; set; }
        public string ConUf { get; set; }
        public int ConFechamento { get; set; }
        public int ConVencimento { get; set; }
        public decimal ConDesconto { get; set; }
        public int ConWeb { get; set; }
        public string ConRegras { get; set; }
        public string ConLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public Conveniadas() { }

        public static bool InserirDados(Conveniadas dados)
        {
            MontadorSql mont = new MontadorSql("conveniadas", MontadorType.Insert);
            mont.AddField("con_id", dados.ConID);
            mont.AddField("con_codigo", dados.ConCodigo);
            mont.AddField("con_nome", dados.ConNome);
            mont.AddField("con_endereco", dados.ConEndereco);
            mont.AddField("con_bairro", dados.ConBairro);
            mont.AddField("con_cidade", dados.ConCidade);
            mont.AddField("con_cep", dados.ConCep);
            mont.AddField("con_uf", dados.ConUf);
            mont.AddField("con_fechamento", dados.ConFechamento);
            mont.AddField("con_vencimento", dados.ConVencimento);
            mont.AddField("con_desconto", dados.ConDesconto);
            mont.AddField("con_web", dados.ConWeb);
            mont.AddField("con_regras", dados.ConRegras);
            mont.AddField("con_liberado", dados.ConLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static DataTable GetDadosConveniadas(string conveniada)
        {
            Principal.strSql = "SELECT CON_CODIGO, CON_NOME, CON_ID, CON_WEB FROM CONVENIADAS WHERE CON_WEB = 4 AND CON_LIBERADO = 'S'  "
                + " AND CON_CODIGO LIKE '%" + conveniada + "%'";

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

        public static DataTable GetBuscar(Conveniadas dadosBusca, out string strOrdem)
        {
            Principal.strSql = "SELECT A.CON_ID, A.CON_CODIGO, A.CON_NOME, A.CON_ENDERECO, A.CON_BAIRRO, A.CON_CIDADE, A.CON_CEP, A.CON_UF, "
                + "A.CON_FECHAMENTO, A.CON_VENCIMENTO, A.CON_DESCONTO, CASE A.CON_WEB WHEN 4 THEN 'PARTICULAR' WHEN 99 THEN 'CONVENIO DROGABELLA/REDE PLANTAO' WHEN 1 "
                + "THEN 'CONVENIO DROGABELLA/REDE PLANTAO' END AS CON_WEB, A.CON_REGRAS, A.CON_LIBERADO, A.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = A.OPALTERACAO) "
                + "AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO FROM CONVENIADAS A, USUARIO_SYSTEM B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.ConCodigo == 0 && dadosBusca.ConNome == "" && dadosBusca.ConCidade == "" && dadosBusca.ConWeb == 0 && dadosBusca.ConLiberado == "TODOS")
            {
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.CON_CODIGO, A.CON_ID";
            }
            else
            {
                if (dadosBusca.ConCodigo != 0)
                {
                    Principal.strSql += " AND A.CON_CODIGO = " + dadosBusca.ConCodigo;
                }
                if (dadosBusca.ConNome != "")
                {
                    Principal.strSql += " AND A.CON_NOME LIKE '%" + dadosBusca.ConNome + "%'";
                }
                if (dadosBusca.ConCidade != "")
                {
                    Principal.strSql += " AND A.CON_CIDADE LIKE '%" + dadosBusca.ConCidade + "%'";
                }
                if (dadosBusca.ConWeb != 0)
                {
                    Principal.strSql += " AND A.CON_WEB = " + dadosBusca.ConWeb;
                }
                if (dadosBusca.ConLiberado != "TODOS")
                {
                    Principal.strSql += " AND A.CON_LIBERADO = '" + dadosBusca.ConLiberado + "'";
                }
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.CON_CODIGO, A.CON_ID";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

        public static bool AtualizaDados(Conveniadas dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[14, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("conveniadas", MontadorType.Update);
            if (!dadosNew.ConCodigo.Equals(dadosOld.Rows[0]["CON_CODIGO"]))
            {
                if (Util.GetRegistros("CONVENIADAS", "CON_CODIGO", dadosNew.ConCodigo.ToString()).Rows.Count > 0)
                {
                    Principal.mensagem = "Código já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                mont.AddField("con_codigo", dadosNew.ConCodigo);

                dados[contMatriz, 0] = "CON_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConNome.Equals(dadosOld.Rows[0]["CON_NOME"]))
            {
                mont.AddField("con_nome", dadosNew.ConNome);

                dados[contMatriz, 0] = "CON_NOME";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_NOME"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConNome + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConEndereco.Equals(dadosOld.Rows[0]["CON_ENDERECO"]))
            {
                mont.AddField("con_endereco", dadosNew.ConEndereco);

                dados[contMatriz, 0] = "CON_ENDERECO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_ENDERECO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConEndereco + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConBairro.Equals(dadosOld.Rows[0]["CON_BAIRRO"].ToString()))
            {
                mont.AddField("con_bairro", dadosNew.ConBairro);

                dados[contMatriz, 0] = "CON_BAIRRO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["CON_BAIRRO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["CON_BAIRRO"] + "'";
                dados[contMatriz, 2] = dadosNew.ConBairro == "" ? "null" : "'" + dadosNew.ConBairro + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConCidade.Equals(dadosOld.Rows[0]["CON_CIDADE"]))
            {
                mont.AddField("con_cidade", dadosNew.ConCidade);

                dados[contMatriz, 0] = "CON_CIDADE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_CIDADE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConCidade + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConCep.Equals(Funcoes.RemoverCaracter(dadosOld.Rows[0]["CON_CEP"].ToString())))
            {
                mont.AddField("con_cep", dadosNew.ConCep);

                dados[contMatriz, 0] = "CON_CEP";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_CEP"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConCep + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConUf.Equals(dadosOld.Rows[0]["CON_UF"]))
            {
                mont.AddField("con_uf", dadosNew.ConUf);

                dados[contMatriz, 0] = "CON_UF";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_UF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConUf + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConFechamento.Equals(dadosOld.Rows[0]["CON_FECHAMENTO"]))
            {
                mont.AddField("con_fechamento", dadosNew.ConFechamento);

                dados[contMatriz, 0] = "CON_FECHAMENTO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_FECHAMENTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConFechamento + "'"; 
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConVencimento.Equals(dadosOld.Rows[0]["CON_VENCIMENTO"]))
            {
                mont.AddField("con_vencimento", dadosNew.ConVencimento);

                dados[contMatriz, 0] = "CON_VENCIMENTO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_VENCIMENTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConVencimento + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}",dadosNew.ConDesconto).Equals(String.Format("{0:N}",dadosOld.Rows[0]["CON_DESCONTO"])))
            {
                mont.AddField("con_desconto", dadosNew.ConDesconto);

                dados[contMatriz, 0] = "CON_DESCONTO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_DESCONTO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConDesconto + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConWeb.Equals(dadosOld.Rows[0]["CON_WEB"]))
            {
                mont.AddField("con_web", dadosNew.ConWeb);

                dados[contMatriz, 0] = "CON_WEB";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_WEB"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConWeb + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.ConLiberado.Equals(dadosOld.Rows[0]["CON_LIBERADO"]))
            {
                mont.AddField("con_liberado", dadosNew.ConLiberado);

                dados[contMatriz, 0] = "CON_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CON_LIBERADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.ConLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuarioID);
                mont.SetWhere("WHERE CON_ID = " + dadosNew.ConID, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) == 1)
                {
                    if (Funcoes.LogAlteracao("CON_ID", dadosNew.ConID.ToString(), Principal.usuarioID, "CONVENIADAS", dados, contMatriz).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        public static int ExcluirDados(string conCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("con_id", conCodigo));

            Principal.strCmd = "DELETE FROM CONVENIADAS WHERE CON_ID = @con_id";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);

        }
    }
}
