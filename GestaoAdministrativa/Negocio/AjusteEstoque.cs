﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class AjusteEstoque
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public int AjuCodigo { get; set; }
        public string ProdCodigo { get; set; }
        public string AjuData { get; set; }
        public string AjuHistorico { get; set; }
        public int AjuQtde { get; set; }
        public char AjuOperacao { get; set; }
        public string AjuEstacao { get; set; }
        public double ProdCusme { get; set; }
        public long EstIndice { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public string Estacao { get; set; }

        public AjusteEstoque() { }

        public AjusteEstoque(int estCodigo, int empCodigo, int ajuCodigo, string prodCodigo, string ajuData, string ajuHistorico, int ajuQtde, char ajuOperacao, string ajuEstacao,
            double prodCusme, long estIndice, DateTime dtAlteracao, string opAlteracao, string estacao)
        {
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.AjuCodigo = ajuCodigo;
            this.ProdCodigo = prodCodigo;
            this.AjuData = ajuData;
            this.AjuHistorico = ajuHistorico;
            this.AjuQtde = ajuQtde;
            this.AjuOperacao = ajuOperacao;
            this.AjuEstacao = ajuEstacao;
            this.ProdCusme = prodCusme;
            this.EstIndice = estIndice;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.Estacao = estacao;
        }

        public DataTable BuscarDados(AjusteEstoque dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.EST_CODIGO, A.EMP_CODIGO, A.AJU_CODIGO, A.AJU_DATA, A.PROD_CODIGO, C.PROD_DESCR, A.AJU_HISTORICO, A.AJU_OPERACAO, "
                       + " A.AJU_QUANTIDADE , A.PROD_CUSME, A.ESTACAO, A.EST_INDICE, B.PROD_ESTATUAL, A.DAT_ALTERACAO, "
                       + " A.USUARIO FROM AJUSTES_ESTOQUE A "
                       + " INNER JOIN PRODUTOS C ON (C.PROD_CODIGO = A.PROD_CODIGO) "
                       + " INNER JOIN PRODUTOS_DETALHE B ON (A.PROD_CODIGO = B.PROD_CODIGO)"
                       + " WHERE A.EST_CODIGO = " + dadosBusca.EstCodigo + " AND  A.EMP_CODIGO = " + dadosBusca.EmpCodigo
                       + " AND A.EST_CODIGO = B.EST_CODIGO AND A.EMP_CODIGO = B.EMP_CODIGO"
                       + " AND C.PROD_CODIGO = B.PROD_CODIGO";

            if (dadosBusca.ProdCodigo == "" && dadosBusca.AjuData.ToString() == "")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.AJU_CODIGO";
            }
            else
            {
                if (dadosBusca.ProdCodigo != "")
                {
                    strSql += " AND B.COD_BARRA = '" + dadosBusca.ProdCodigo + "'";
                }
                if (dadosBusca.AjuData.ToString() != "")
                {
                    strSql += " AND A.AJU_DATA = " + Funcoes.BData(Convert.ToDateTime(dadosBusca.AjuData.ToString()));
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.AJU_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        //public bool InsereRegistros(AjusteEstoque dados)
        //{
        //    //string strCmd = "INSERT INTO AJUSTES_ESTOQUE (EMP_CODIGO,EST_CODIGO, AJU_CODIGO, PROD_CODIGO, AJU_DATA, AJU_HISTORICO, AJU_QTDE, AJU_OPERACAO,"
        //    //                + " AJU_ESTACAO, PROD_CUSME, ENT_CODIGO, DTCADASTRO, OPCADASTRO) VALUES " +
        //    //    dados.EmpCodigo + "," +
        //    //    dados.ClasCodigo + ",'" +
        //    //    dados.ClasDescr + "','" +
        //    //    dados.ClasLiberado + "'," +
        //    //    Funcoes.BDataHora(dados.DtCadastro) + ",'" +
        //    //    dados.OpCadastro + "')";
        //    //if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
        //    //{
        //    //    return true;
        //    //}
        //    //else
        //    //    return false;

        //}
    }
}
