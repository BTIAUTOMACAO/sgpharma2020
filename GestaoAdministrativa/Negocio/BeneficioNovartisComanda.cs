﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioNovartisComanda
    {
        public long VendaID { get; set; }
        public string NSU { get; set; }
        public string Cartao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public BeneficioNovartisComanda() { }

        public BeneficioNovartisComanda(long vendaID, string nsu, string cartao, DateTime dtCadastro, string opCadastro)
        {
            this.VendaID = vendaID;
            this.NSU = nsu;
            this.Cartao = cartao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable IdentificaSeVendaEPorComanda(string nsu, long vendaID = 0)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM BENEFICIO_NOVARTIS_COMANDA "
                + "WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                strSql += " AND VENDA_ID = " + vendaID;
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(BeneficioNovartisComanda dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_NOVARTIS_COMANDA(VENDA_ID, NSU,CARTAO, DTCADASTRO, OPCADASTRO) VALUES ("
                + dados.VendaID + ",'"
                + dados.NSU + "','"
                + dados.Cartao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaComandaPorVendaID(long vendaID)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM BENEFICIO_NOVARTIS_COMANDA "
                + "WHERE  VENDA_ID = " + vendaID;

            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID = 0, bool transAberta = false)
        {
            string strCmd = "DELETE FROM BENEFICIO_NOVARTIS_COMANDA WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            if (!transAberta)
                return BancoDados.ExecuteNoQuery(strCmd, null);
            else
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }
    }
}
