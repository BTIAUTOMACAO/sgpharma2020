﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    class Conveniada
    {
        public int ConID { get; set; }
        public int ConCodigo { get; set; }
        public string ConNome { get; set; }
        public string ConEndereco { get; set; }
        public string ConBairro { get; set; }
        public string ConCidade { get; set; }
        public string ConCep { get; set; }
        public string ConUf { get; set; }
        public int ConFechamento { get; set; }
        public int ConVencimento { get; set; }
        public decimal ConDesconto { get; set; }
        public int ConWeb { get; set; }
        public string ConRegras { get; set; }
        public string ConLiberado { get; set; }
        public int ConFormaID { get; set; }
        public string ConParcela { get; set; }
        public string ConObrigaRegraDesconto { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Conveniada() { }

        public Conveniada(int conID, int conCodigo, string conNome, string conEndereco, string conBairro, string conCidade,
                           string conCep, string conUf, int conFechamento, int conVencimento, decimal conDesconto, int conWeb,
                           string conRegras, string conLiberado, int conFormaId, string conParcela, string conObrigaRegraDesconto, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.ConID = conID;
            this.ConCodigo = conCodigo;
            this.ConNome = conNome;
            this.ConEndereco = conEndereco;
            this.ConBairro = conBairro;
            this.ConCidade = conCidade;
            this.ConCep = conCep;
            this.ConUf = conUf;
            this.ConFechamento = conFechamento;
            this.ConVencimento = conVencimento;
            this.ConDesconto = conDesconto;
            this.ConWeb = conWeb;
            this.ConRegras = conRegras;
            this.ConLiberado = conLiberado;
            this.ConFormaID = conFormaId;
            this.ConParcela = conParcela;
            this.ConObrigaRegraDesconto = conObrigaRegraDesconto;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscaDados(Conveniada dados, out string strOrdem)
        {

            string sql = " SELECT CON_CODIGO, CON_NOME, CON_ENDERECO, CON_BAIRRO, CON_CEP, CON_CIDADE, CON_UF , "
                       + " CASE CON_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS CON_DESABILITADO, "
                       + " CON_FECHAMENTO, CON_VENCIMENTO, CON_DESCONTO, CON_WEB, CON_REGRAS, CON_ID, CON_OP_ALTERACAO, CON_OP_CADASTRO, "
                       + " CON_DT_CADASTRO, CON_DT_ALTERACAO,CON_FORMA_ID, COALESCE(CON_PARCELA,'N') AS CON_PARCELA, CON_OBRIGA_REGRA_DESCONTO  FROM CONVENIADAS WHERE 1=1 ";
            
            if (dados.ConCodigo != 0)
            {
                sql += "AND CON_CODIGO = " + dados.ConCodigo;
            }
            if (!String.IsNullOrEmpty(dados.ConNome))
            {
                sql += "AND CON_NOME LIKE '%" + dados.ConNome + "%'";
            }
            if (!String.IsNullOrEmpty(dados.ConCidade))
            {

                sql += "AND CON_CIDADE LIKE '%" + dados.ConCidade + "%'";
            }
            if (dados.ConWeb != 0)
            {

                sql += " AND CON_WEB = " + dados.ConWeb;
            }
            if (dados.ConLiberado != "TODOS")
            {
                string desab = dados.ConLiberado == "S" ? "N" : "S";
                sql += "AND CON_DESABILITADO = '" + desab + "'";
            }
            strOrdem = sql;
            sql += "ORDER BY CON_CODIGO";
            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(Conveniada dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE CONVENIADAS SET "
                       + " CON_NOME = '" + dadosNovos.ConNome + "'"
                       + " ,CON_ENDERECO = '" + dadosNovos.ConEndereco + "'"
                       + " ,CON_BAIRRO = '" + dadosNovos.ConBairro + "'"
                       + " ,CON_CEP = '" + dadosNovos.ConCep + "'"
                       + " ,CON_CIDADE = '" + dadosNovos.ConCidade + "'"
                       + " ,CON_UF = '" + dadosNovos.ConUf + "'"
                       + " ,CON_CODIGO =" + dadosNovos.ConCodigo
                       + " ,CON_DESABILITADO = '" + dadosNovos.ConLiberado + "'"
                       + " ,CON_FECHAMENTO = " + dadosNovos.ConFechamento
                       + " ,CON_VENCIMENTO = " + dadosNovos.ConVencimento
                       + " ,CON_DESCONTO = '" + dadosNovos.ConDesconto + "'"
                       + " ,CON_WEB = " + dadosNovos.ConWeb
                       + " ,CON_REGRAS = '" + dadosNovos.ConRegras + "'"
                       + " ,CON_OP_ALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " , CON_DT_ALTERACAO =  " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " , CON_FORMA_ID = " + dadosNovos.ConFormaID
                       + " , CON_PARCELA = '" + dadosNovos.ConParcela + "'"
                       + " , CON_OBRIGA_REGRA_DESCONTO = '" + dadosNovos.ConObrigaRegraDesconto + "'"
                       + " WHERE CON_ID = " + dadosNovos.ConID;


            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[14, 3];
                int contMatriz = 0;

                if (!dadosNovos.ConNome.Equals(dadosAntigos.Rows[0]["CON_NOME"]))
                {
                    dados[contMatriz, 0] = "CON_NOME";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_NOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConNome + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConEndereco.Equals(dadosAntigos.Rows[0]["CON_ENDERECO"]))
                {

                    dados[contMatriz, 0] = "CON_ENDERECO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_ENDERECO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConEndereco + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConBairro.Equals(dadosAntigos.Rows[0]["CON_BAIRRO"]))
                {

                    dados[contMatriz, 0] = "CON_BAIRRO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_BAIRRO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConBairro + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConCep.Equals(dadosAntigos.Rows[0]["CON_CEP"]))
                {

                    dados[contMatriz, 0] = "CON_CEP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_CEP"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConCep + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConCidade.Equals(dadosAntigos.Rows[0]["CON_CIDADE"]))
                {

                    dados[contMatriz, 0] = "CON_CIDADE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_CIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConCidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConUf.Equals(dadosAntigos.Rows[0]["CON_UF"]))
                {

                    dados[contMatriz, 0] = "CON_UF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_UF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConUf + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConCodigo.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["CON_CODIGO"])))
                {

                    dados[contMatriz, 0] = "CON_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_CODIGO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConCodigo + "'";
                    contMatriz = contMatriz + 1;
                }

                if (!dadosNovos.ConLiberado.Equals(dadosAntigos.Rows[0]["CON_DESABILITADO"]))
                {

                    dados[contMatriz, 0] = "CON_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConFechamento.Equals(dadosAntigos.Rows[0]["CON_FECHAMENTO"]))
                {

                    dados[contMatriz, 0] = "CON_FECHAMENTO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_FECHAMENTO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConFechamento + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConVencimento.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["CON_VENCIMENTO"])))
                {

                    dados[contMatriz, 0] = "CON_VENCIMENTO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_VENCIMENTO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConVencimento + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConDesconto.Equals(Convert.ToDecimal(dadosAntigos.Rows[0]["CON_DESCONTO"])))
                {

                    dados[contMatriz, 0] = "CON_DESCONTO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_DESCONTO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConDesconto + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConWeb.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["CON_WEB"])))
                {

                    dados[contMatriz, 0] = "CON_WEB";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_WEB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConWeb + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConRegras.Equals(dadosAntigos.Rows[0]["CON_REGRAS"]))
                {

                    dados[contMatriz, 0] = "CON_REGRAS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_REGRAS"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConRegras + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.ConFormaID.ToString().Equals(Convert.ToString(dadosAntigos.Rows[0]["CON_FORMA_ID"])))
                {

                    dados[contMatriz, 0] = "CON_FORMA_ID";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["CON_FORMA_ID"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ConFormaID + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("CON_ID", dadosNovos.ConID.ToString(), Principal.usuario, "CONVENIADAS", dados, contMatriz, 1);

                return true;
            }
            else
            {

                return false;
            }
        }

        public bool InsereDados(Conveniada empConveniadas)
        {

            string sql = " INSERT INTO CONVENIADAS( CON_CODIGO, CON_NOME, CON_ENDERECO, CON_BAIRRO, CON_CEP, CON_CIDADE, "
                       + " CON_UF, CON_DESABILITADO, CON_FECHAMENTO, CON_VENCIMENTO, CON_DESCONTO, CON_WEB, CON_REGRAS, CON_OP_CADASTRO, "
                       + " CON_DT_CADASTRO, CON_ID,CON_FORMA_ID,CON_COD_CONV, CON_PARCELA, CON_OBRIGA_REGRA_DESCONTO) VALUES ("
                       + empConveniadas.ConCodigo + ",'"
                       + empConveniadas.ConNome + "','"
                       + empConveniadas.ConEndereco + "','"
                       + empConveniadas.ConBairro + "',"
                       + empConveniadas.ConCep + ",'"
                       + empConveniadas.ConCidade + "','"
                       + empConveniadas.ConUf + "','"
                       + empConveniadas.ConLiberado + "',"
                       + empConveniadas.ConFechamento + " , "
                       + empConveniadas.ConVencimento + " ,'"
                       + empConveniadas.ConDesconto + "',"
                       + empConveniadas.ConWeb + ",'"
                       + empConveniadas.ConRegras + "','"
                       + empConveniadas.OpCadastro + "',"
                       + Funcoes.BDataHora(empConveniadas.DtCadastro) + ","
                       + empConveniadas.ConID + ","
                       + empConveniadas.ConFormaID + "," + empConveniadas.ConCodigo + ",'" + empConveniadas.ConParcela + "','" + empConveniadas.ConObrigaRegraDesconto + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("CON_ID", Convert.ToString(empConveniadas.ConID), Principal.usuario, "CONVENIADAS", empConveniadas.ConNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;

            }
        }

        public bool ExcluiDados(int conId)
        {

            string sql = "DELETE FROM CONVENIADAS WHERE CON_ID =" + conId;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {

                return true;
            }
            else
                return false;

        }

        public DataTable BuscaConveniadaParticular(string conveniada)
        {
            string strSql = "SELECT CON_CODIGO, CON_NOME, CON_ID, CON_WEB,CON_COD_CONV FROM CONVENIADAS WHERE CON_WEB = 4 AND CON_DESABILITADO = 'N'  "
                + " AND CON_CODIGO LIKE '%" + conveniada + "%' ORDER BY CON_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InserirConveniadaConsultaCartao(string conCodigo, string conNome, string opCadastro, DateTime dtCadastro)
        {
            int id = Funcoes.IdentificaVerificaID("CONVENIADAS", "CON_ID");

            string strCmd = "INSERT INTO CONVENIADAS (CON_ID,CON_CODIGO,CON_NOME,CON_COD_CONV,CON_WEB,CON_DESABILITADO,CON_OP_CADASTRO,CON_DT_CADASTRO, CON_PARCELA) VALUES ("
                + id.ToString() + ","
                + conCodigo + ",'"
                + conNome + "',1,1,'N','"
                + opCadastro + "',"
                + Funcoes.BDataHora(dtCadastro) + ",'N')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("CON_ID", Convert.ToString(id), opCadastro, "CONVENIADAS", conNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;

        }

        public double IdentificaDescontoConveniada(string conCodigo)
        {
            string strSql = "SELECT CON_DESCONTO FROM CONVENIADAS"
                        + " WHERE CON_CODIGO = " + conCodigo;

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToDouble(r);
        }

        public string IdentificaParcelado(string conCodigo)
        {
            string strSql = "SELECT COALESCE(CON_PARCELA,'N') AS CON_PARCELA FROM CONVENIADAS"
                        + " WHERE CON_CODIGO = " + conCodigo;

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "N";
            else
                return Convert.ToString(r);
        }


        public DataTable BuscaParcelarPorConveniada(string conveniada, string dtInicial, string dtFinal, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.EMP_CODIGO,"
                        + "           A.EST_CODIGO,"
                        + "           A.COBRANCA_ID,"
                        + "           C.CF_DOCTO,"
                        + "           C.CON_CODIGO,"
                        + "           C.CF_NOME,"
                        + "           A.COBRANCA_VENDA_ID,"
                        + "           B.COBRANCA_PARCELA_NUM,"
                        + "           B.COBRANCA_PARCELA_VALOR,"
                        + "           B.COBRANCA_PARCELA_SALDO,"
                        + "           B.COBRANCA_PARCELA_VENCIMENTO,"
                        + "           A.COBRANCA_DATA,"
                        + "           A.DT_CADASTRO,"
                        + "           A.OP_CADASTRO,"
                        + "           B.COBRANCA_PARCELA_STATUS, B.COBRANCA_PARCELA_ID"
                        + "      FROM COBRANCA A"
                        + "     INNER JOIN COBRANCA_PARCELA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                        + "     INNER JOIN CLIFOR C ON A.COBRANCA_CF_ID = C.CF_ID"
                        + "     WHERE A.EMP_CODIGO = " + empCodigo
                        + "       AND A.EST_CODIGO = " + estCodigo
                        + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + "       AND A.EST_CODIGO = B.EST_CODIGO"
                        + "       AND C.CON_CODIGO = " + conveniada
                        + "       AND B.COBRANCA_PARCELA_SALDO > 0"
                        + "       AND B.COBRANCA_PARCELA_STATUS = 'A'"
                        + "       AND B.COBRANCA_PARCELA_VENCIMENTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal))
                        + "       ORDER BY B.COBRANCA_PARCELA_VENCIMENTO, C.CF_NOME";
            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
