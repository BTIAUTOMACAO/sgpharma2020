﻿using GestaoAdministrativa.Classes;
using System;

namespace GestaoAdministrativa.Negocio
{
    public class Receber
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long RecID { get; set; }
        public string RecDocto { get; set; }
        public string CfDocto { get; set; }
        public string RecData { get; set; }
        public double RecValPrin { get; set; }
        public int RecCondPag { get; set; }
        public int RecVendedor { get; set; }
        public double RecValorCp { get; set; }
        public string LoginId { get; set; }
        public string RecCliente { get; set; }
        public string RecCnpj { get; set; }
        public string RecInscricao { get; set; }
        public string RecEndereco { get; set; }
        public string RecBairro { get; set; }
        public string RecCep { get; set; }
        public string RecCidade { get; set; }
        public string RecUf { get; set; }
        public string RecFone { get; set; }
        public long PedNumero { get; set; }
        public string PedCompl { get; set; }
        public string RecStatus { get; set; }
        public string RecOrigem { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Receber() { }

        public Receber(int empCodigo, int estCodigo, long recID, string recDocto, string cfDocto, string recData, double recValPrin, int recCondPag, int recVendedor,
            double recValorCp, string loginId, string recCliente, string recCnpj, string recInscricao, string recEndereco, string recBairro, string recCep, string recCidade, string recUF, string recFone,
            long pedNumero, string pedComplemento, string recStatus, string recOrigem, DateTime dtAleracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.RecID = recID;
            this.CfDocto = cfDocto;
            this.RecData = recData;
            this.RecValPrin = recValPrin;
            this.RecCondPag = recCondPag;
            this.RecVendedor = recVendedor;
            this.RecValorCp = recValorCp;
            this.LoginId = loginId;
            this.RecCliente = recCliente;
            this.RecCnpj = recCnpj;
            this.RecInscricao = recInscricao;
            this.RecEndereco = recEndereco;
            this.RecBairro = recBairro;
            this.RecCep = recCep;
            this.RecCidade = recCidade;
            this.RecUf = recUF;
            this.RecFone = recFone;
            this.PedNumero = pedNumero;
            this.PedCompl = pedComplemento;
            this.RecStatus = recStatus;
            this.RecOrigem = recOrigem;
            this.DtAlteracao = dtAleracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InserirDados(Receber dados)
        {
            string strCmd = "INSERT INTO RECEBER(EMP_CODIGO,EST_CODIGO,PED_NUMERO,PED_COMPL,REC_ID,REC_DOCTO,CF_DOCTO,REC_DATA,REC_VALPRIN,REC_CONDPAG,"
                + "REC_VENDEDOR,REC_VALORCP,LOGIN_ID,REC_CLIENTE,REC_CNPJ,REC_INSCRICAO,REC_ENDERECO,REC_BAIRRO,REC_CEP,REC_CIDADE,REC_UF,"
                + "REC_FONE,REC_STATUS,REC_ORIGEM,DTCADASTRO,OPCADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.PedNumero + ",'"
                + dados.PedCompl + "',"
                + dados.RecID + ",'"
                + dados.RecDocto + "','"
                + dados.CfDocto + "',"
                + Funcoes.BData(Convert.ToDateTime(dados.RecData)) + ","
                + Funcoes.BValor(dados.RecValPrin) + ","
                + dados.RecCondPag + ","
                + dados.RecVendedor + ","
                + Funcoes.BValor(dados.RecValorCp) + ",'"
                + dados.LoginId + "',";
            if (!String.IsNullOrEmpty(dados.RecCliente))
            {
                strCmd += "'" + dados.RecCliente + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecCnpj))
            {
                strCmd += "'" + dados.RecCnpj + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecInscricao))
            {
                strCmd += "'" + dados.RecInscricao + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecEndereco))
            {
                strCmd += "'" + dados.RecEndereco + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecBairro))
            {
                strCmd += "'" + dados.RecBairro + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecCep))
            {
                strCmd +=  dados.RecCep + ",";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecCidade))
            {
                strCmd += "'" + dados.RecCidade + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecUf))
            {
                strCmd += "'" + dados.RecUf + "',";
            }
            else
                strCmd += "null,";

            if (!String.IsNullOrEmpty(dados.RecFone))
            {
                strCmd += "'" + dados.RecFone + "','";
            }
            else
                strCmd += "null,'";

            strCmd += dados.RecStatus + "','"
                + dados.RecOrigem + "','"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }

}
