﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    class Usuarios
    {
        public int LoginID { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Administrador { get; set; }
        public string Liberado { get; set; }
        public int GrupoID { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public Usuarios() { }

        public static bool InserirDados(Usuarios dados)
        {
            MontadorSql mont = new MontadorSql("usuario_system", MontadorType.Insert);
            mont.AddField("login_id", dados.LoginID);
            mont.AddField("nome", dados.Nome);
            mont.AddField("senha", dados.Senha);
            mont.AddField("administrador", dados.Administrador);
            mont.AddField("liberado", dados.Liberado);
            mont.AddField("grupo_id", dados.GrupoID);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static DataTable GetDadosLogin(string usuario, string senha)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("nome", usuario));
            ps.Add(new Fields("senha", Funcoes.CriptografaSenha(senha)));

            Principal.strSql = "SELECT LOGIN_ID, ADMINISTRADOR FROM USUARIO_SYSTEM WHERE NOME = @nome AND SENHA = @senha AND LIBERADO = 'S'";

            return BancoDados.GetDataTable(Principal.strSql, ps);
        }

        public static DataTable GetPermissoes(string usuario)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("usuario", usuario));

            //SELECIONA PERMISSAO DE ACESSO AOS MENUS DO MODULO CADASTRO DE ACORDO COM USUARIO//
            string sql = "SELECT M.MENU, M.NOME, P.ACESSA, U.ADMINISTRADOR"
                + " FROM MODULO_MENU M, PERMISSOES P, USUARIO_SYSTEM U WHERE M.MODULO_ID = P.MODULO_ID AND"
                + " U.NOME = @usuario AND P.GRUPO_ID = U.GRUPO_ID  ORDER BY P.MODULO_ID";

            return BancoDados.GetDataTable(sql, ps);
        }

        public static bool AtualizaDados(Usuarios dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[7, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("usuario_system", MontadorType.Update);
            if (!dadosNew.Nome.Equals(dadosOld.Rows[0]["NOME"]))
            {
                //VERIFICA SE USUARIO JÁ ESTA CADASTRADO//
                if (Util.GetRegistros("USUARIO_SYSTEM", "NOME", dadosNew.Nome).Rows.Count > 0)
                {
                    Principal.mensagem = "Usuário já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }
                mont.AddField("nome", dadosNew.Nome);

                dados[contMatriz, 0] = "NOME";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NOME"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Nome + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNew.Senha.Equals(dadosOld.Rows[0]["SENHA"].ToString()))
            {
                mont.AddField("senha", dadosNew.Senha);

                dados[contMatriz, 0] = "SENHA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SENHA"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Senha + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Administrador.Equals(dadosOld.Rows[0]["ADMINISTRADOR"]))
            {
                mont.AddField("administrador", dadosNew.Administrador);

                dados[contMatriz, 0] = "ADMINISTRADOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ADMINISTRADOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Administrador + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Liberado.Equals(dadosOld.Rows[0]["LIBERADO"]))
            {
                mont.AddField("liberado", dadosNew.Liberado);

                dados[contMatriz, 0] = "LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Liberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.GrupoID.Equals(dadosOld.Rows[0]["GRUPO_ID"]))
            {
                mont.AddField("grupo_id", dadosNew.GrupoID);

                dados[contMatriz, 0] = "GRUPO_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_ID"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoID + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuarioID);
                mont.SetWhere("WHERE LOGIN_ID = " + dadosNew.LoginID, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) == 1)
                {
                    if (Funcoes.LogAlteracao("LOGIN_ID",dadosNew.LoginID.ToString(), Principal.usuarioID, "USUARIO_SYSTEM", dados, contMatriz).Equals(true))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        public static int ExcluirDados(string uCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("login_id", uCodigo));

            Principal.strCmd = "DELETE FROM USUARIO_SYSTEM WHERE LOGIN_ID = @login_id";

            return BancoDados.ExecuteNoQuery(Principal.strCmd, ps);
        }

        public static DataTable GetBuscar(Usuarios dadosBusca, out string strOrdem)
        {
            Principal.strSql = "SELECT U.LOGIN_ID, U.NOME, U.ADMINISTRADOR, U.LIBERADO, U.GRUPO_ID, "
                       + "G.DESCRICAO, U.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = U.OPALTERACAO) AS OPALTERACAO, U.DTCADASTRO, "
                       + "(SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = U.OPCADASTRO) AS OPCADASTRO, U.SENHA FROM USUARIO_SYSTEM U, GRUPO_USUARIOS G  WHERE U.GRUPO_ID = G.GRUPO_USU_ID";
            if (dadosBusca.LoginID  == 0 && dadosBusca.Nome == "" && dadosBusca.GrupoID == -1 && dadosBusca.Liberado == "TODOS")
            {
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY LOGIN_ID";
            }
            else 
            {
                if (dadosBusca.LoginID != 0)
                {
                    Principal.strSql += " AND U.LOGIN_ID = " + dadosBusca.LoginID;
                }
                if (dadosBusca.Nome != "")
                {
                    Principal.strSql += " AND U.NOME LIKE '%" + dadosBusca.Nome + "%'";
                }
                if (dadosBusca.GrupoID != -1)
                {
                    Principal.strSql += " AND U.GRUPO_ID = " + dadosBusca.GrupoID;
                }
                if (dadosBusca.Liberado != "TODOS")
                {
                    Principal.strSql += " AND U.LIBERADO = '" + dadosBusca.Liberado + "'";
                }
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY LOGIN_ID";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }
    }
}
