﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlNegocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class Pos
    {
        public int EmpCodigo{ get; set; }
        public int EstCodigo { get; set; }
        public int PosNumero { get; set; }
        public string PosDescricao {get; set;}
        public int PosUltimoDocto { get; set; }
        public string CfDocto { get; set; }
        public string PosCodAcesso { get; set; }
        public string PosSenhaAcesso { get; set; }
        public string PosConexao {get; set;}   
        public string PosAbreviacao {get; set;}   
        public string PosLiberado {get; set;}
        public string PosContaVencida { get; set; }
        public string CompStatusTransf { get; set; }
        public string CompMotivo { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Pos() { }

        public string IdentificaEnderecoDeConexao(Pos dadosBusca)
        {
            string strSql = "SELECT POS_CONEXAO FROM POS"
                       + " WHERE POS_NUMERO = " + dadosBusca.PosNumero + " AND EMP_CODIGO = " + dadosBusca.EmpCodigo + " AND EST_CODIGO = " + dadosBusca.EstCodigo;

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public bool InsereRegistroPOS()
        {
            var dadosPos = new Pos
            {
                EmpCodigo = 1,
                EstCodigo = 1,
                PosNumero = 100,
                PosDescricao = "DATA CENTER",
                PosUltimoDocto = 0,
                CfDocto = "000.000.000-00",
                PosConexao = "200.155.3.84",
                PosAbreviacao = "D.BELLA",
                PosLiberado = "S",
                DtCadastro = DateTime.Now,
                OpCadastro = Principal.usuario,
            };

            string strCmd = "INSERT INTO POS (EMP_CODIGO, EST_CODIGO, POS_NUMERO, POS_DESCRICAO, POS_ULTDOCTO, CF_DOCTO, POS_DESABILITADO, POS_CONEXAO, "
                        + "POS_ABREV, DTCADASTRO, OPCADASTRO) VALUES ("
                        + dadosPos.EstCodigo + ","
                        + dadosPos.EmpCodigo + ","
                        + dadosPos.PosNumero + ",'"
                        + dadosPos.PosDescricao + "',"
                        + dadosPos.PosUltimoDocto + ",'"
                        + dadosPos.CfDocto  + "','"
                        + dadosPos.PosLiberado + "','"
                        + dadosPos.PosConexao + "','"
                        + dadosPos.PosAbreviacao + "',"
                        + Funcoes.BDataHora(dadosPos.DtCadastro) + ",'"
                        + dadosPos.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Principal.endConexao = "200.155.3.84";
                return true;
            }
            else
                return false;
        }
        

    }
}
