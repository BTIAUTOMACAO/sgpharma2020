﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class AtualizacaoEstacao
    {
        public string Aplicativo { get; set; }
        public string Estacao { get; set; }
        public string Atualiza { get; set; }
        public string MensagemCliente { get; set; }
        public string Endereco { get; set; }
        public string CodErro { get; set; }
        public string MensagemErro { get; set; }
        public int CodEstabelecimento { get; set; }
        public int GrupoID { get; set; }
    }
}
