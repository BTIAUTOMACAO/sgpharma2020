﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcVenda
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int VenID { get; set; }
        public string VenTipoReceita { get; set; }
        public string VenNumNota { get; set; }
        public DateTime VenPrescData { get; set; }
        public string VenPrescNome { get; set; }
        public string VenPrescNum { get; set; }
        public string VenPrescCP { get; set; }
        public string VenPrescUF { get; set; }
        public string VenUso { get; set; }
        public string VenCompNome { get; set; }
        public string VenCompTd { get; set; }
        public string VenCompDocto { get; set; }
        public string VenCompOE { get; set; }
        public string VenCompUF { get; set; }
        public string ProdCodigo { get; set; }
        public string VenMedLote { get; set; }
        public int VenMedQtde { get; set; }
        public DateTime VenData { get; set; }
        public string VenStatus { get; set; }
        public string VenMS { get; set; }
        public int PedNumero { get; set; }
        public string PedCompl { get; set; }
        public string VenPacNome { get; set; }
        public int VenPacIdade { get; set; }
        public string VenPacUnidade { get; set; }
        public string VenPacSexo { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public SngpcVenda() { }

        public SngpcVenda(int empCodigo, int estCodigo, int venID, string venTipoReceita, string venNumNota, DateTime venPrescData,
                          string venPrescNome, string venPrescNum, string venPrescCP, string venPrescUF, string venUso, string venCompNome,
                          string venCompTd, string venCompDocto, string venCompOE, string venCompUF, string prodCodigo, string venMedLote,
                          int venMedQtde, DateTime venData, string venStatus, string venMS, int pedNumero, string pedCompl, string venPacNome,
                          int VenPacIdade, string venPacUnidade, string venPacSexo, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro,
                          string opCadastro)
        {

            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.VenID = venID;
            this.VenTipoReceita = venTipoReceita;
            this.VenNumNota = venNumNota;
            this.VenPrescData = venPrescData;
            this.VenPrescNome = venPrescNome;
            this.VenPrescNum = venPrescNum;
            this.VenPrescCP = venPrescCP;
            this.VenPrescUF = venPrescUF;
            this.VenUso = venUso;
            this.VenCompNome = venCompNome;
            this.VenCompTd = venCompTd;
            this.VenCompDocto = venCompDocto;
            this.VenCompOE = venCompOE;
            this.VenCompUF = venCompUF;
            this.ProdCodigo = prodCodigo;
            this.VenMedLote = venMedLote;
            this.VenMedQtde = venMedQtde;
            this.VenData = venData;
            this.VenStatus = venStatus;
            this.VenMS = venMS;
            this.PedNumero = pedNumero;
            this.PedCompl = pedCompl;
            this.VenPacNome = venPacNome;
            this.VenPacIdade = VenPacIdade;
            this.VenPacUnidade = venPacUnidade;
            this.VenPacSexo = venPacSexo;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }
        public DataTable BuscaDados(SngpcVenda dados, out string strOrdem)
        {
            string sql = " SELECT VEN.EST_CODIGO, VEN.VEN_SEQ, TIPODOC.TAB_DESCRICAO AS VEN_COMP_TD, "
                       + " ORGEXP.TAB_DESCRICAO AS VEN_COMP_OE, UF.TAB_CODIGO AS VEN_COMP_UF, "
                       + " VEN.VEN_COMP_DOCTO, VEN.VEN_COMP_NOME, VEN.VEN_DATA, VEN.VEN_PAC_NOME, "
                       + " VEN.VEN_PAC_IDADE, UNI.TAB_DESCRICAO AS  VEN_PAC_UNIDADE, SEXO.TAB_DESCRICAO AS  VEN_PAC_SEXO, "
                       + " VEN.VEN_PRESC_NUMERO, VEN.VEN_PRESC_NOME, CONS.TAB_DESCRICAO AS  VEN_PRESC_CP, VEN.VEN_NUM_NOT, "
                       + " VEN.VEN_PRESC_DT, UFRES.TAB_CODIGO AS VEN_PRESC_UF, USO.TAB_DESCRICAO AS  VEN_USO, "
                       + " TIPOREC.TAB_DESCRICAO AS  VEN_TP_RECEITA, VEN.PROD_CODIGO, PROD.PROD_DESCR, VEN.VEN_MS AS PROD_REGISTRO_MS, "
                       + " VEN.VEN_MED_QTDE, VEN.VEN_MED_LOTE, VEN.DT_ALTERACAO, VEN.DT_CADASTRO, VEN.OP_CADASTRO, VEN.OP_ALTERACAO "
                       + " FROM SNGPC_VENDAS VEN "
                       + " LEFT JOIN SNGPC_TABELAS TIPODOC ON(TIPODOC.TAB_SEQ = VEN.VEN_COMP_TD) "
                       + " LEFT JOIN SNGPC_TABELAS ORGEXP ON(ORGEXP.TAB_SEQ = VEN.VEN_COMP_OE) "
                       + " LEFT JOIN SNGPC_TABELAS UF ON(UF.TAB_SEQ = VEN.VEN_COMP_UF) "
                       + " LEFT JOIN SNGPC_TABELAS UNI ON(UNI.TAB_SEQ = VEN.VEN_PAC_UNIDADE) "
                       + " LEFT JOIN SNGPC_TABELAS CONS ON(CONS.TAB_SEQ = VEN.VEN_PRESC_CP) "
                       + " LEFT JOIN SNGPC_TABELAS SEXO ON(SEXO.TAB_SEQ = VEN.VEN_PAC_SEXO) "
                       + " LEFT JOIN SNGPC_TABELAS UFRES ON(UFRES.TAB_SEQ = VEN.VEN_PRESC_UF) "
                       + " LEFT JOIN SNGPC_TABELAS USO ON(USO.TAB_SEQ = VEN.VEN_USO) "
                       + " LEFT JOIN SNGPC_TABELAS TIPOREC ON(TIPOREC.TAB_SEQ = VEN.VEN_TP_RECEITA) "
                       + " LEFT JOIN PRODUTOS PROD ON(PROD.PROD_CODIGO = VEN.PROD_CODIGO) "
                       + " WHERE VEN.EST_CODIGO = " + Principal.estAtual + " AND VEN.EMP_CODIGO = " + Principal.empAtual;

            if (dados.VenCompDocto != "")
            {
                sql += " AND  REPLACE(REPLACE(REPLACE(VEN_COMP_DOCTO, '/', ''), '-', ''), '.', '') =  '" + dados.VenCompDocto + "'";
            }
            if (dados.VenCompNome != "")
            {
                sql += " AND VEN_COMP_NOME LIKE '%" + dados.VenCompNome + "%'";
            }
            if (dados.VenData != Convert.ToDateTime("01/01/0001"))
            {

                sql += " AND VEN_DATA = " + Funcoes.BData(dados.VenData) ;
            }
            if (dados.VenMS != "")
            {

                sql += " AND VEN_MS = '" + dados.VenMS + "'";
            }
            strOrdem = sql;

            sql += " ORDER BY VEN.VEN_DATA, VEN.VEN_PAC_NOME ";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool IncluirVenda(SngpcVenda dados)
        {

            string sql = " INSERT INTO SNGPC_VENDAS ( EMP_CODIGO, EST_CODIGO, VEN_SEQ, VEN_TP_RECEITA, VEN_NUM_NOT, VEN_PRESC_DT, "
                       + " VEN_PRESC_NOME, VEN_PRESC_NUMERO, VEN_PRESC_CP, VEN_PRESC_UF, VEN_USO, VEN_COMP_NOME, VEN_COMP_TD, "
                       + " VEN_COMP_DOCTO, VEN_COMP_OE, VEN_COMP_UF, PROD_CODIGO, VEN_MED_LOTE, VEN_MED_QTDE, VEN_DATA, VEN_STATUS, "
                       + " VEN_MS, VEN_PAC_NOME, VEN_PAC_IDADE, VEN_PAC_UNIDADE, VEN_PAC_SEXO, DT_CADASTRO, OP_CADASTRO ) "
                       + " VALUES ("
                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.VenID + ",'"
                       + dados.VenTipoReceita + "','"
                       + dados.VenNumNota + "',"
                       + Funcoes.BData(dados.VenPrescData) + ",'"
                       + dados.VenPrescNome + "','"
                       + dados.VenPrescNum + "','"
                       + dados.VenPrescCP + "','"
                       + dados.VenPrescUF + "','"
                       + dados.VenUso + "','"
                       + dados.VenCompNome + "','"
                       + dados.VenCompTd + "','"
                       + dados.VenCompDocto + "','"
                       + dados.VenCompOE + "','"
                       + dados.VenCompUF + "','"
                       + dados.ProdCodigo + "','"
                       + dados.VenMedLote + "',"
                       + dados.VenMedQtde + ","
                       + Funcoes.BData(dados.VenData) + ","
                       + "'F','"
                       + dados.VenMS + "','"
                       + dados.VenPacNome + "','"
                       + dados.VenPacIdade + "','"
                       + dados.VenPacUnidade + "','"
                       + dados.VenPacSexo + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {

                return false;
            }
        }

        public bool AtualizaVenda(SngpcVenda dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE SNGPC_VENDAS SET "
                       + "  EMP_CODIGO = " + dadosNovos.EmpCodigo
                       + ", EST_CODIGO = " + dadosNovos.EstCodigo
                       + ", VEN_TP_RECEITA =  '" + dadosNovos.VenTipoReceita + "'"
                       + ", VEN_NUM_NOT = '" + dadosNovos.VenNumNota + "'"
                       + ", VEN_PRESC_DT = " + Funcoes.BData(dadosNovos.VenPrescData)
                       + ", VEN_PRESC_NOME = '" + dadosNovos.VenPrescNome + "'"
                       + ", VEN_PRESC_NUMERO = '" + dadosNovos.VenPrescNum + "'"
                       + ", VEN_PRESC_CP = '" + dadosNovos.VenPrescCP + "'"
                       + ", VEN_PRESC_UF = '" + dadosNovos.VenPrescUF + "'"
                       + ", VEN_USO = '" + dadosNovos.VenUso + "'"
                       + ", VEN_COMP_NOME = '" + dadosNovos.VenCompNome + "'"
                       + ", VEN_COMP_TD = '" + dadosNovos.VenCompTd + "'"
                       + ", VEN_COMP_DOCTO = '" + dadosNovos.VenCompDocto + "'"
                       + ", VEN_COMP_OE = '" + dadosNovos.VenCompOE + "'"
                       + ", VEN_COMP_UF = '" + dadosNovos.VenCompUF + "'"
                       + ", PROD_CODIGO = '" + dadosNovos.ProdCodigo + "'"
                       + ", VEN_MED_LOTE = '" + dadosNovos.VenMedLote + "'"
                       + ", VEN_MED_QTDE = " + dadosNovos.VenMedQtde
                       + ", VEN_DATA = " + Funcoes.BData(dadosNovos.VenData)
                       + ", VEN_MS = '" + dadosNovos.VenMS + "'"
                       + ", VEN_PAC_NOME = '" + dadosNovos.VenPacNome + "'"
                       + ", VEN_PAC_IDADE ='" + dadosNovos.VenPacIdade + "'"
                       + ", VEN_PAC_UNIDADE ='" + dadosNovos.VenPacUnidade + "'"
                       + ", VEN_PAC_SEXO ='" + dadosNovos.VenPacSexo + "'"
                       + ", DT_ALTERACAO =" + Funcoes.BDataHora(DateTime.Now)
                       + ", OP_ALTERACAO ='" + Principal.usuario.ToUpper() + "'";

            string[,] dados = new string[6, 3];
            int contMatriz = 0;

            if (dadosNovos.EmpCodigo != Convert.ToInt32(dadosAntigos.Rows[0]["EMP_CODIGO"]))
            {
                dados[contMatriz, 0] = "EMP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EMP_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EmpCodigo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.EstCodigo != Convert.ToInt32(dadosAntigos.Rows[0]["EST_CODIGO"]))
            {
                dados[contMatriz, 0] = "EST_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["EST_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EstCodigo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenTipoReceita != dadosAntigos.Rows[0]["VEN_TP_RECEITA"].ToString())
            {
                dados[contMatriz, 0] = "VEN_TP_RECEITA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_TP_RECEITA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenTipoReceita.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenNumNota != dadosAntigos.Rows[0]["VEN_NUM_NOT"].ToString())
            {
                dados[contMatriz, 0] = "VEN_NUM_NOT";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_NUM_NOT"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenNumNota.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPrescData.ToString("dd/MM/yyyy") != Convert.ToDateTime(dadosAntigos.Rows[0]["VEN_PRESC_DT"]).ToString("dd/MM/yyyy"))
            {
                dados[contMatriz, 0] = "VEN_PRESC_DT";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PRESC_DT"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPrescData.ToString("dd/MM/yyyy") + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPrescCP != dadosAntigos.Rows[0]["VEN_PRESC_CP"].ToString())
            {
                dados[contMatriz, 0] = "VEN_PRESC_CP";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PRESC_CP"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPrescCP.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPrescUF != dadosAntigos.Rows[0]["VEN_PRESC_UF"].ToString())
            {
                dados[contMatriz, 0] = "VEN_PRESC_UF";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PRESC_UF"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPrescUF.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenUso != dadosAntigos.Rows[0]["VEN_USO"].ToString())
            {
                dados[contMatriz, 0] = "VEN_USO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_USO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenUso.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenCompNome != dadosAntigos.Rows[0]["VEN_COMP_NOME"].ToString())
            {
                dados[contMatriz, 0] = "VEN_COMP_NOME";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_COMP_NOME"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenCompNome.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenCompTd != dadosAntigos.Rows[0]["VEN_COMP_TD"].ToString())
            {
                dados[contMatriz, 0] = "VEN_COMP_TD";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_COMP_TD"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenCompTd.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenCompDocto != dadosAntigos.Rows[0]["VEN_COMP_DOCTO"].ToString())
            {
                dados[contMatriz, 0] = "VEN_COMP_DOCTO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_COMP_DOCTO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenCompDocto.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenCompOE != dadosAntigos.Rows[0]["VEN_COMP_OE"].ToString())
            {
                dados[contMatriz, 0] = "VEN_COMP_OE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_COMP_OE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenCompOE.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenCompUF != dadosAntigos.Rows[0]["VEN_COMP_UF"].ToString())
            {
                dados[contMatriz, 0] = "VEN_COMP_UF";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_COMP_UF"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenCompUF.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.ProdCodigo != dadosAntigos.Rows[0]["PROD_CODIGO"].ToString())
            {
                dados[contMatriz, 0] = "PROD_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdCodigo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenMedLote != dadosAntigos.Rows[0]["VEN_MED_LOTE"].ToString())
            {
                dados[contMatriz, 0] = "VEN_MED_LOTE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_MED_LOTE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenMedLote.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenMedQtde != Convert.ToUInt32(dadosAntigos.Rows[0]["VEN_MED_QTDE"]))
            {
                dados[contMatriz, 0] = "VEN_MED_QTDE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_MED_QTDE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenMedQtde.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenData.ToString("dd/MM/yyyy") != Convert.ToDateTime(dadosAntigos.Rows[0]["VEN_DATA"]).ToString("dd/MM/yyyy"))
            {
                dados[contMatriz, 0] = "VEN_DATA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_DATA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenData.ToString("dd/MM/yyyy") + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenMS != dadosAntigos.Rows[0]["VEN_MS"].ToString())
            {
                dados[contMatriz, 0] = "VEN_MS";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_MS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenMS.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPacNome != dadosAntigos.Rows[0]["VEN_PAC_NOME"].ToString())
            {
                dados[contMatriz, 0] = "VEN_PAC_NOME";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PAC_NOME"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPacNome.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPacIdade != Convert.ToInt32(dadosAntigos.Rows[0]["VEN_PAC_IDADE"]))
            {
                dados[contMatriz, 0] = "VEN_PAC_IDADE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PAC_IDADE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPacIdade.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPacUnidade != dadosAntigos.Rows[0]["VEN_PAC_UNIDADE"].ToString())
            {
                dados[contMatriz, 0] = "VEN_PAC_UNIDADE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PAC_UNIDADE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPacUnidade.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.VenPacSexo != dadosAntigos.Rows[0]["VEN_PAC_SEXO"].ToString())
            {
                dados[contMatriz, 0] = "VEN_PAC_SEXO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["VEN_PAC_SEXO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.VenPacSexo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {
                sql += "WHERE VEN_SEQ = " + dadosNovos.VenID + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("VEN_SEQ", dadosNovos.VenID.ToString(), Principal.usuario, "SNGPC_VENDAS", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool ExcluiDados(int vendaID)
        {
            string sql = "DELETE SNGPC_VENDAS WHERE VEN_SEQ = " + vendaID + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;
            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable BuscaDadosVendaPeriodo(string dtInicial, string dtFinal, char ordem, int tipoReceita)
        {
            string sql = " SELECT TAB.TAB_DESCRICAO, VEN.VEN_DATA, VEN.PROD_CODIGO, VEN.VEN_MED_QTDE, VEN.VEN_MED_LOTE, "
                       + " VEN.VEN_PAC_IDADE, PRO.PROD_DESCR, UPPER(VEN.VEN_PAC_NOME) AS VEN_PAC_NOME, "
                       + " UPPER(PRO.PROD_UNIDADE)AS PROD_UNIDADE, VEN.VEN_NUM_NOT, VEN.VEN_MS, "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' ELSE 'SUJEITO A CONTROLE ESPECIAL' "
                       + " END AS PROD_CLASSE_TERAP FROM SNGPC_VENDAS VEN "
                       + " INNER JOIN SNGPC_TABELAS TAB ON(TAB.TAB_SEQ = VEN.VEN_TP_RECEITA) "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = VEN.PROD_CODIGO) "
                       + " WHERE VEN.VEN_DATA BETWEEN  TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') "
                       + " AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                       + " AND VEN.EMP_CODIGO = " + Principal.empAtual + " AND VEN.EST_CODIGO = " + Principal.estAtual;
            if (tipoReceita != 0)
            {
                sql += " AND TAB.TAB_SEQ = " + tipoReceita;
            }
            if (ordem == 'C')
            {
                sql += " ORDER BY  VEN.PROD_CODIGO";
            }
            else
            {
                sql += " ORDER BY  PRO.PROD_DESCR";
            }

            return BancoDados.GetDataTable(sql, null);
        }

        public int IdentificaSeJaTemLoteEProdutoVendido(string prodCodigo, string lote, DateTime dataNota)
        {
            string sql = "SELECT VEN_MED_QTDE FROM SNGPC_VENDAS WHERE PROD_CODIGO = '" + prodCodigo + "' AND VEN_MED_LOTE = '" + lote + "' AND VEN_DATA = " + Funcoes.BData(dataNota);
            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }
    }
}
