﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Cobranca
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int CobrancaID { get; set; }
        public int CobrancaCfID { get; set; }
        public DateTime CobrancaData { get; set; }
        public double CobrancaTotal { get; set; }
        public int CobrancaFormaID { get; set; }
        public int CobrancaColCodigo { get; set; }
        public long CobrancaVendaID { get; set; }
        public string CobrancaOrigem { get; set; }
        public string CobrancaStatus { get; set; }
        public string CobrancaUsuario { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }

        public Cobranca() { }

        public Cobranca(int empCodigo, int estCodigo, int cobrancaID, int cobrancaCfId, DateTime cobrancaData, double cobrancaTotal, int cobrancaFormaID, int cobrancaColCodigo, long cobrancaVendaID,
            string cobrancaOrigem, string cobrancaStatus, string cobrancaUsuario, DateTime dtCadastro, string opCadastro, DateTime dtAlteracao, string opAlteracao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.CobrancaID = cobrancaID;
            this.CobrancaCfID = cobrancaCfId;
            this.CobrancaData = cobrancaData;
            this.CobrancaTotal = cobrancaTotal;
            this.CobrancaFormaID = cobrancaFormaID;
            this.CobrancaColCodigo = cobrancaColCodigo;
            this.CobrancaVendaID = cobrancaVendaID;
            this.CobrancaOrigem = cobrancaOrigem;
            this.CobrancaStatus = cobrancaStatus;
            this.CobrancaUsuario = cobrancaUsuario;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
        }

        public bool InserirDados(Cobranca dados)
        {
            string strCmd = "INSERT INTO COBRANCA(EMP_CODIGO,EST_CODIGO,COBRANCA_ID,COBRANCA_CF_ID,COBRANCA_DATA,COBRANCA_TOTAL,COBRANCA_FORMA_ID,COBRANCA_COL_CODIGO,COBRANCA_VENDA_ID,"
                + "COBRANCA_ORIGEM,COBRANCA_STATUS,COBRANCA_USUARIO,DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.CobrancaID + ","
                + dados.CobrancaCfID + ","
                + Funcoes.BData(dados.CobrancaData) + ","
                + Funcoes.BValor(dados.CobrancaTotal) + ","
                + dados.CobrancaFormaID + ","
                + dados.CobrancaColCodigo + ","
                + dados.CobrancaVendaID + ",'"
                + dados.CobrancaOrigem + "','"
                + dados.CobrancaStatus + "','"
                + dados.CobrancaUsuario + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaCobranca(int cobrancaID)
        {
            string sql = " UPDATE COBRANCA SET COBRANCA_STATUS = 'F' WHERE COBRANCA_ID = " + cobrancaID + " AND ( "
                       + " SELECT SUM(COBRANCA_PARCELA_SALDO)AS SOMA FROM COBRANCA_PARCELA WHERE COBRANCA_ID = " + cobrancaID + ") = 0";

            var retorno = BancoDados.ExecuteNoQueryTrans(sql, null);
            if (retorno.Equals(1) || retorno.Equals(0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CancelaCobranca(int estCodigo, int empCodigo, long vendaID, DateTime dtAlteracao, string operador)
        {
            string strCmd = "UPDATE COBRANCA SET COBRANCA_STATUS = 'C', DT_ALTERACAO = " + Funcoes.BDataHora(dtAlteracao) + ", OP_ALTERACAO = '"
                + operador + "' WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND COBRANCA_VENDA_ID = " + vendaID;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaCobrancaPorPeriodo(DateTime dtRecebimento, char status)
        {

            string sql = " UPDATE COBRANCA SET COBRANCA_STATUS = '" + status + "'"
                       + " WHERE COBRANCA_ID IN ( SELECT COBRANCA_ID FROM COBRANCA_MOVIMENTO WHERE COBRANCA_MOV_DT_BAIXA = " + Funcoes.BDataHora(dtRecebimento) + ")";


            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable BuscaDadosCobranca(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = "SELECT COB.COBRANCA_ID,"
                        + "           SUM(COBM.COBRANCA_MOV_VL_PAGO) as MOVIMENTO_CX_ESPECIE_VALOR,"
                        + "           COBP.COBRANCA_PARCELA_NUM,"
                        + "           CL.CF_DOCTO,"
                        + "           CL.CF_NOME,"
                        + "           ESP.ESP_DESCRICAO"
                        + "      FROM cobranca_movimento COBM"
                        + "     INNER JOIN COBRANCA COB ON COBM.COBRANCA_ID = COB.COBRANCA_ID"
                        + "     INNER JOIN COBRANCA_PARCELA COBP ON COBP.COBRANCA_ID = COBM.COBRANCA_ID"
                        + "     INNER JOIN CLIFOR CL ON COB.COBRANCA_CF_ID = CL.CF_ID"
                        + "     INNER JOIN MOVIMENTO_CAIXA_ESPECIE MCE ON(MCE.MOVIMENTO_CX_ESPECIE_DATA ="
                        + "                                               COBM.COBRANCA_MOV_DT_BAIXA)"
                        + "     INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO ="
                        + "                                MCE.MOVIMENTO_CX_ESPECIE_CODIGO)"
                        + "     WHERE COBM.COBRANCA_MOV_DT_BAIXA BETWEEN"
                        + "           " + Funcoes.BDataHora(dtInicial) + " AND "
                        + "           " + Funcoes.BDataHora(dtFinal)
                        + "       AND MCE.MOVIMENTO_CX_ESPECIE_USUARIO = '" + usuario + "'"
                        + "     GROUP BY CL.CF_DOCTO,"
                        + "              CL.CF_NOME,"
                        + "              ESP.ESP_DESCRICAO,"
                        + "              COBM.COBRANCA_MOV_DT_BAIXA,"
                        + "              COB.COBRANCA_ID,"
                        + "              COBP.COBRANCA_PARCELA_NUM"
                        + "     ORDER BY COBM.COBRANCA_MOV_DT_BAIXA";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaDadosCobrancaCarta(DateTime dtInicial, DateTime dtFinal, int empCodigo, int estCodigo, string nome)
        {
            string sql = "select c.cf_docto,"
                       + "      c.cf_codigo,"
                       + "        c.cf_nome,"
                       + "        c.cf_fone, c.cf_id,"
                       + "        sum(a.cobranca_parcela_saldo) as valor"
                       + "   from clifor c, cobranca b, cobranca_parcela a"
                       + "   where a.emp_codigo = " + empCodigo
                       + "   and a.est_codigo = " + estCodigo
                       + "   and a.cobranca_parcela_vencimento between " + Funcoes.BData(dtInicial) + " and "
                       + Funcoes.BData(dtFinal)
                       + "    and a.cobranca_parcela_status <> 'T'"
                       + "    and a.cobranca_parcela_saldo <> 0"
                       + "    and a.emp_codigo = b.emp_codigo"
                       + "    and a.est_codigo = b.est_codigo"
                       + "    and a.cobranca_id = b.cobranca_id"
                       + "    and b.cobranca_cf_id = c.cf_id";
            if (!String.IsNullOrEmpty(nome))
            {
                sql += " and c.cf_nome like '%" + nome + "%'";
            }
            sql += "  group by c.cf_docto,"
                       + "           c.cf_codigo,"
                       + "            c.cf_nome,"
                       + "           c.cf_apelido,"
                       + "           c.cf_fone, c.cf_id"
                       + "  order by c.cf_nome, c.cf_codigo, c.cf_docto";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaDadosCartaClienteSelecionados(string ID)
        {
            string sql = "SELECT A.CF_ID, A.CF_NOME, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_FONE, A.CF_DOCTO, A.CF_CEP"
                    + " FROM CLIFOR A"
                    + " WHERE A.CF_ID IN (" + ID.Substring(0, ID.Length - 1) + ")";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaTotalPorEmpresaConveniadaSintetico(int empCodigo, int estCodigo, bool vencimento, string dataInicial, string dataFinal, string conCodigo, bool pagos)
        {
            string strSql = " SELECT C.CF_NOME AS NOME, SUM(B.COBRANCA_PARCELA_VALOR) AS VALOR, SUM(B.COBRANCA_PARCELA_SALDO)AS SALDO"
                          + "    FROM COBRANCA A"
                          + "      INNER JOIN COBRANCA_PARCELA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                          + "      INNER JOIN CLIFOR C ON A.COBRANCA_CF_ID = C.CF_ID"
                          + "      WHERE A.EMP_CODIGO = " + empCodigo
                          + "      AND A.EST_CODIGO = " + estCodigo
                          + "      AND A.EMP_CODIGO = B.EMP_CODIGO"
                          + "      AND A.EST_CODIGO = B.EST_CODIGO"
                          + "      AND C.CON_CODIGO = " + conCodigo;
            if(!pagos)
            {
                strSql += " AND B.COBRANCA_PARCELA_STATUS = 'A'";
            }
            else
                strSql += " AND B.COBRANCA_PARCELA_STATUS <> 'C'";

            if (vencimento)
            {
                strSql += "  AND B.COBRANCA_PARCELA_VENCIMENTO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }
            else
            {
                strSql += "  AND A.COBRANCA_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }
            strSql += "      GROUP BY C.CF_NOME"
                + " ORDER BY C.CF_NOME";
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaTotalPorEmpresaConveniadaAnalitico(int empCodigo, int estCodigo, bool vencimento, string dataInicial, string dataFinal, string conCodigo, bool pagos)
        {
            string strSql = " SELECT C.CF_NOME,"
                          + "      A.COBRANCA_VENDA_ID,"
                          + "      B.COBRANCA_PARCELA_NUM,"
                          + "      A.COBRANCA_DATA AS DT_CADASTRO,"
                          + "      SUM(B.COBRANCA_PARCELA_VALOR) AS VALOR,"
                          + "      SUM(B.COBRANCA_PARCELA_SALDO) AS SALDO,";
            if(vencimento)
            {
                strSql += "  B.COBRANCA_PARCELA_VENCIMENTO AS DATA";
            }
            else
            {
                strSql += "  A.COBRANCA_DATA AS DATA";
            }

            strSql += "    FROM COBRANCA A"
                          + "      INNER JOIN COBRANCA_PARCELA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                          + "      INNER JOIN CLIFOR C ON A.COBRANCA_CF_ID = C.CF_ID"
                          + "      WHERE A.EMP_CODIGO = " + empCodigo
                          + "      AND A.EST_CODIGO = " + estCodigo
                          + "      AND A.EMP_CODIGO = B.EMP_CODIGO"
                          + "      AND A.EST_CODIGO = B.EST_CODIGO"
                          + "      AND C.CON_CODIGO = " + conCodigo;
            if (!pagos)
            {
                strSql += " AND B.COBRANCA_PARCELA_STATUS = 'A'";
            }

            if (vencimento)
            {
                strSql += "  AND B.COBRANCA_PARCELA_VENCIMENTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }
            else
            {
                strSql += "  AND A.COBRANCA_DATA BETWEEN " + Funcoes.BData(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BData(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }

            strSql += " GROUP BY C.CF_NOME, A.COBRANCA_VENDA_ID, B.COBRANCA_PARCELA_NUM,B.COBRANCA_PARCELA_VENCIMENTO, A.COBRANCA_DATA"; 
            strSql += "   ORDER BY ";

            if(vencimento)
            {
                strSql += "  B.COBRANCA_PARCELA_VENCIMENTO,";
            }
            else
            {
                strSql += "  A.COBRANCA_DATA, ";
            }

            strSql += "C.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaContasPagasSintetico(int empCodigo, int estCodigo, string nomeCliente, string dataInicial, string dataFinal, int tipoData)
        {
            string strSql = " SELECT C.COBRANCA_VENDA_ID,"
                            + "           B.COBRANCA_PARCELA_NUM,"
                            + "           B.COBRANCA_PARCELA_VENCIMENTO,"
                            + "           A.DT_CADASTRO,"
                            + "           B.COBRANCA_PARCELA_VALOR,"
                            + "           A.COBRANCA_MOV_VL_PAGO,"
                            + "           A.COBRANCA_MOV_VL_ACRESCIMO,"
                            + "           B.COBRANCA_PARCELA_SALDO,"
                            + "           CASE"
                            + "             WHEN TO_DATE(A.DT_CADASTRO, 'DD/MM/YYYY') >"
                            + "                 TO_DATE(B.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY') THEN"
                            + "              TO_DATE(A.DT_CADASTRO, 'DD/MM/YYYY') -"
                            + "              TO_DATE(B.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY')"
                            + "             ELSE"
                            + "              0"
                            + "            END AS ATRASO,"
                            + "           D.CON_CODIGO, D.CF_NOME,  B.DT_CADASTRO AS DATA_VENDA"
                            + "     FROM COBRANCA_MOVIMENTO A"
                            + "     INNER JOIN COBRANCA_PARCELA B ON A.COBRANCA_PARCELA_ID ="
                            + "                                      B.COBRANCA_PARCELA_ID"
                            + "     INNER JOIN COBRANCA C ON A.COBRANCA_ID = C.COBRANCA_ID"
                            + "     INNER JOIN CLIFOR D ON C.COBRANCA_CF_ID = D.CF_ID"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "     AND A.EST_CODIGO = " + estCodigo
                            + "     AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "     AND A.EST_CODIGO = B.EST_CODIGO"
                            + "     AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "     AND A.EST_CODIGO = C.EST_CODIGO"
                            + "     AND D.CF_NOME = '" + nomeCliente + "'";
            if(tipoData == 0)
            {
                strSql += "     AND C.COBRANCA_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }
            else
            {
                strSql += "     AND A.DT_CADASTRO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }

            strSql += "      ORDER BY B.COBRANCA_PARCELA_VENCIMENTO, C.COBRANCA_VENDA_ID ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaContasAbertasSintetico(int empCodigo, int estCodigo, string nomeCliente, string dataInicial, string dataFinal, int tipoData)
        {
            string strSql = " SELECT C.COBRANCA_VENDA_ID,"
                            + "       B.COBRANCA_PARCELA_NUM,"
                            + "       B.COBRANCA_PARCELA_VENCIMENTO,"
                            + "       B.COBRANCA_PARCELA_VALOR,"
                            + "       B.COBRANCA_PARCELA_SALDO,"
                            + "       CASE"
                            + "          WHEN TO_DATE(SYSDATE, 'DD/MM/YYYY') >"
                            + "              TO_DATE(B.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY') THEN"
                            + "              TO_DATE(SYSDATE, 'DD/MM/YYYY') -"
                            + "              TO_DATE(B.COBRANCA_PARCELA_VENCIMENTO, 'DD/MM/YYYY')"
                            + "             ELSE"
                            + "              0"
                            + "       END AS ATRASO,"
                            + "       D.CON_CODIGO,"
                            + "       D.CF_NOME, B.DT_CADASTRO"
                            + "   FROM COBRANCA_PARCELA B"
                            + "   INNER JOIN COBRANCA C ON B.COBRANCA_ID = C.COBRANCA_ID"
                            + "   INNER JOIN CLIFOR D ON C.COBRANCA_CF_ID = D.CF_ID"
                            + "   WHERE B.EMP_CODIGO = " + empCodigo
                            + "   AND B.EST_CODIGO = " + estCodigo
                            + "   AND B.EMP_CODIGO = C.EMP_CODIGO"
                            + "   AND B.EST_CODIGO = C.EST_CODIGO"
                            + "   AND D.CF_NOME = '" + nomeCliente + "'"
                            + "   AND B.COBRANCA_PARCELA_STATUS = 'A'";
            if (tipoData == 0)
            {
                strSql += "    AND C.COBRANCA_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }
            else
            {
                strSql += "    AND B.COBRANCA_PARCELA_VENCIMENTO BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime(dataInicial + " 00:00:00")) + " AND " + Funcoes.BDataHora(Convert.ToDateTime(dataFinal + " 00:00:00"));
            }

            strSql += "   ORDER BY B.COBRANCA_PARCELA_VENCIMENTO, C.COBRANCA_VENDA_ID ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public string BuscaTituloCadastrado(string numDocto)
        {
            string sql = "SELECT COBRANCA_VENDA_ID FROM COBRANCA  WHERE COBRANCA_VENDA_ID = " + numDocto;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public string BuscaFormaPagtoCliente(int clienteID)
        {
            string sql = "SELECT B.CON_FORMA_ID"
                        + "      FROM CLIFOR A"
                        + "     INNER JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                        + "     WHERE A.CF_ID = " + clienteID;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public DataTable BuscaDados(int empCodigo, int estCodigo, string cobCodigo, string data, string cliente, out string strOrdem)
        {
            string strSql = "SELECT A.COBRANCA_ID,"
                            + "           A.COBRANCA_DATA,"
                            + "           A.COBRANCA_TOTAL,"
                            + "           A.COBRANCA_VENDA_ID,"
                            + "           B.CF_NOME,"
                            + "           B.CF_DOCTO,"
                            + "           A.COBRANCA_STATUS,"
                            + "           C.COL_NOME,"
                            + "           A.DT_ALTERACAO,"
                            + "           A.OP_ALTERACAO,"
                            + "           A.DT_CADASTRO,"
                            + "           A.OP_CADASTRO,"
                            + "           B.CF_CIDADE,"
                            + "           B.CF_UF, "
                            + "           B.CF_ID"
                            + "      FROM COBRANCA A"
                            + "     INNER JOIN CLIFOR B ON A.COBRANCA_CF_ID = B.CF_ID"
                            + "     INNER JOIN COLABORADORES C ON A.COBRANCA_COL_CODIGO = C.COL_CODIGO"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "     AND A.EST_CODIGO = " + estCodigo
                            + "     AND C.EMP_CODIGO = A.EMP_CODIGO"
                            + "     AND A.COBRANCA_ORIGEM = 'T'";

            if (cobCodigo != "")
            {
                strSql += " AND A.COBRANCA_ID = " + cobCodigo;
            }
            if (cliente != "")
            {
                strSql += " AND B.CF_NOME LIKE '%" + cliente + "%'";
            }
            if (data != "")
            {
                strSql += " AND A.COBRANCA_DATA = " + Funcoes.BData(Convert.ToDateTime(data));
            }
            strOrdem = strSql;
            strSql += " ORDER BY A.COBRANCA_ID";

            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluiParcelaCobranca(int estCodigo, int empCodigo, int idCobranca)
        {
            BancoDados.AbrirTrans();

            string strCmd = "DELETE FROM COBRANCA_PARCELA WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND COBRANCA_ID = " + idCobranca;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                strCmd = "DELETE FROM COBRANCA WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND COBRANCA_ID = " + idCobranca;
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    BancoDados.FecharTrans();
                    return 1;
                }
            }

            BancoDados.ErroTrans();
            return 0;
        }

    }
}
