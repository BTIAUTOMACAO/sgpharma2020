﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class EntradaItens
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int EntId { get; set; }
        public string CfDocto { get; set; }
        public string EntDocto { get; set; }
        public string EntSerie { get; set; }
        public int EntSequencia { get; set; }
        public string ProdCodigo { get; set; }
        public string ItemDescr { get; set; }
        public string EntAplicacao { get; set; }
        public int EntQtdeEstoque { get; set; }
        public int EntQtde { get; set; }
        public string EntUnidade { get; set; }
        public double EntUnitario { get; set; }
        public double EntDescontoItem { get; set; }
        public double EntTotalItem { get; set; }
        public double EntMargem { get; set; }
        public double EntValorVenda { get; set; }
        public string EntLote { get; set; }
        public string EntICMS { get; set; }
        public double EntAliqIcms { get; set; }
        public double EntValorBcIcms { get; set; }
        public double EntValorIcms { get; set; }
        public string ProdCST { get; set; }
        public double EntBcIcmsSt { get; set; }
        public double EntIcmsSt { get; set; }
        public double EntRedBaseIcms { get; set; }
        public double EntIcmsRet { get; set; }
        public string EntEnqIpi { get; set; }
        public string EntCstIpi { get; set; }
        public double EntBaseIpi { get; set; }
        public double EntAliqIpi { get; set; }
        public double EntValorIpi { get; set; }
        public int EntOrigem { get; set; } 
        public string ProdNCM { get; set; }
        public string ProdCFOP { get; set; }
        public string EntCstCofins { get; set; }
        public string EntCstPis { get; set; }
        public string NoCodigo { get; set; }
        public string NoComp { get; set; }
        public double EntComissao { get; set; }
        public string CompNumero { get; set; }
        public DateTime EntDataValidade { get; set; }
        public string ProdutoNovo { get; set; }
        public int EstIndice { get; set; }
        public double EntPreCompra { get; set; }
        public string EntProdCest { get; set; }
        public double EntPmc { get; set; }
        public string EntRegistroMS { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public EntradaItens() { }

        public EntradaItens(int empCodigo, int estCodigo, int entID, string cfDocto, string entDocto, string entSerie, int entSequencia, string prodCodigo,
            string itemDescr, string entAplicacao, int entQtdeEstoque, int entQtde, string entUnidade, double entUnitario, double entDescontoItem, double entTotalItem, 
            double entMargem, double entValorVenda, string entLote,string entIcms, double entAliqIcms, double entValorBcIcms, double entValorIcms, string prodCst, double entBcIcmsSt,
            double entIcmsSt, double entRedBaseIcms, double entIcmsRet, string entEnqIpi, string entCstIpi, double entBaseIpi, double entAliqIpi, double entValorIpi, int origem,
            string prodNcm, string prodCfop, string entCstCofins, string entCstPis, string noCodigo, string noComp, double entComissao, string compNumero, DateTime entDataValidade, string produtoNovo, 
            int estIndice, double entPreCompra, string entProdCest, double entPmc, string entRegistroMS, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.EntId = entID;
            this.CfDocto = cfDocto;
            this.EntDocto = entDocto;
            this.EntSerie = entSerie;
            this.EntSequencia = entSequencia;
            this.ProdCodigo = prodCodigo;
            this.ItemDescr = itemDescr;
            this.EntAplicacao = entAplicacao;
            this.EntQtdeEstoque = entQtdeEstoque;
            this.EntQtde = entQtde;
            this.EntUnidade = entUnidade;
            this.EntUnitario = entUnitario;
            this.EntDescontoItem = entDescontoItem;
            this.EntTotalItem = entTotalItem;
            this.EntMargem = entMargem;
            this.EntValorVenda = entValorVenda;
            this.EntLote = entLote;
            this.EntICMS = entIcms;
            this.EntAliqIcms = entAliqIcms;
            this.EntValorBcIcms = entValorBcIcms;
            this.EntValorIcms = entValorIcms;
            this.ProdCST = prodCst;
            this.EntBcIcmsSt = entBcIcmsSt;
            this.EntIcmsRet = entIcmsSt;
            this.EntRedBaseIcms = EntRedBaseIcms;
            this.EntIcmsRet = entIcmsRet;
            this.EntEnqIpi = entEnqIpi;
            this.EntCstIpi = entCstIpi;
            this.EntBaseIpi = entBaseIpi;
            this.EntAliqIpi = entAliqIpi;
            this.EntValorIpi = EntValorIpi;
            this.EntOrigem = EntOrigem;
            this.ProdNCM = prodNcm;
            this.ProdCFOP = prodCfop;
            this.EntCstCofins = entCstCofins;
            this.EntCstPis = EntCstPis;
            this.NoCodigo = noCodigo;
            this.NoComp = noComp;
            this.CompNumero = compNumero;
            this.EntComissao = entComissao;
            this.EntDataValidade = entDataValidade;
            this.ProdutoNovo = produtoNovo;
            this.EstIndice = estIndice;
            this.EntPreCompra = entPreCompra;
            this.EntProdCest = entProdCest;
            this.EntPmc = entPmc;
            this.EntRegistroMS = entRegistroMS;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public string BuscarIndicePorSequenciaEntradaItens(EntradaItens entItens)
        {
            string strSql = "SELECT EST_INDICE FROM ENTRADA_ITENS WHERE EST_CODIGO = " + entItens.EstCodigo
                + " AND EMP_CODIGO = " + entItens.EmpCodigo + " AND ENT_SEQUENCIA = " + entItens.EntSequencia 
                + " AND ENT_ID = " + entItens.EntId + " ORDER BY ENT_SEQUENCIA";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public int ExcluiProdutoEntradasItens(EntradaItens entItens)
        {
            string strCmd = "DELETE FROM ENTRADA_ITENS WHERE EST_CODIGO = " + entItens.EstCodigo + " AND EMP_CODIGO = " + entItens.EmpCodigo
                        + " AND ENT_ID = " + entItens.EntId + " AND PROD_CODIGO  = '" + entItens.ProdCodigo + "'";
            return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }

        public int AtualizaSequenciaEntradaItens(EntradaItens entItens)
        {
            string strCmd = "UPDATE ENTRADA_ITENS SET ENT_SEQUENCIA = " + entItens.EntSequencia + ", DTALTERACAO = " + Funcoes.BDataHora(entItens.DtAlteracao)
                          + ", OPALTERACAO = '" + entItens.OpAlteracao + "' WHERE EST_CODIGO = " + entItens.EstCodigo + " AND EMP_CODIGO = " + entItens.EmpCodigo
                          + " AND ENT_ID = " + entItens.EntId + " AND PROD_CODIGO = '" + entItens.ProdCodigo + "'";
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public bool InsereRegistrosEntradaItens(EntradaItens entradaItens)
        {
            string strCmd = "INSERT INTO ENTRADA_ITENS(EMP_CODIGO, EST_CODIGO, ENT_ID, CF_DOCTO, ENT_DOCTO, ENT_SERIE, ENT_SEQUENCIA"
                + ", PROD_CODIGO, ITEM_DESCR, ENT_APLICACAO, ENT_QTDESTOQUE, ENT_QTDE, ENT_UNIDADE, ENT_UNITARIO, ENT_DESCONTOITEM"
                + ", ENT_TOTITEM, ENT_MARGEM, ENT_VALOR_VENDA, ENT_LOTE, ENT_ICMS, ENT_ALIQICMS, ENT_VALOR_BC_ICMS"
                + ", ENT_VALORICMS, PROD_CST, ENT_BC_ICMS_ST, ENT_ICMS_ST, ENT_REDUCAOBASEICMS, ENT_ICMS_RET, ENT_ENQ_IPI, ENT_CST_IPI"
                + ", ENT_BASE_IPI, ENT_ALIQIPI, ENT_VALORIPI, ENT_ORIGEM, NCM, PROD_CFOP, ENT_CST_COFINS, ENT_CST_PIS, NO_CODIGO, NO_COMP"
                + ", COMP_NUMERO, ENT_COMISSAO, ENT_DATAVALIDADE, ENT_PRODUTO_NOVO, EST_INDICE, ENT_PRE_COMPRA, PROD_CEST, ENT_PMC, ENT_REGISTRO_MS, DTCADASTRO, OPCADASTRO) VALUES ("
                + entradaItens.EmpCodigo + ","
                + entradaItens.EstCodigo + ","
                + entradaItens.EntId + ",'"
                + entradaItens.CfDocto + "','"
                + entradaItens.EntDocto + "','"
                + entradaItens.EntSerie + "',"
                + entradaItens.EntSequencia + ",'"
                + entradaItens.ProdCodigo + "','"
                + entradaItens.ItemDescr + "','"
                + entradaItens.EntAplicacao + "',"
                + entradaItens.EntQtdeEstoque + ","
                + entradaItens.EntQtde + ",'"
                + entradaItens.EntUnidade + "',"
                + Funcoes.BFormataValor(entradaItens.EntUnitario) + ","
                + Funcoes.BFormataValor(entradaItens.EntDescontoItem) + ","
                + Funcoes.BFormataValor(entradaItens.EntTotalItem) + ","
                + Funcoes.BFormataValor(entradaItens.EntMargem) + ","
                + Funcoes.BFormataValor(entradaItens.EntValorVenda) + ",'"
                + entradaItens.EntLote + "','"
                + entradaItens.EntICMS + "',"
                + Funcoes.BFormataValor(entradaItens.EntAliqIcms) + ","
                + Funcoes.BFormataValor(entradaItens.EntValorBcIcms) + ","
                + Funcoes.BFormataValor(entradaItens.EntValorIcms) + ",'"
                + entradaItens.ProdCST + "',"
                + Funcoes.BFormataValor(entradaItens.EntBcIcmsSt) + ","
                + Funcoes.BFormataValor(entradaItens.EntIcmsSt) + ","
                + Funcoes.BFormataValor(entradaItens.EntRedBaseIcms) + ","
                + Funcoes.BFormataValor(entradaItens.EntIcmsRet) + ",'"
                + entradaItens.EntEnqIpi + "','"
                + entradaItens.EntCstIpi + "',"
                + Funcoes.BFormataValor(entradaItens.EntBaseIpi) + ","
                + Funcoes.BFormataValor(entradaItens.EntAliqIpi) + ","
                + Funcoes.BFormataValor(entradaItens.EntValorIpi) + ","
                + entradaItens.EntOrigem + ",'"
                + entradaItens.ProdNCM + "','"
                + entradaItens.ProdCFOP + "','"
                + entradaItens.EntCstCofins + "','"
                + entradaItens.EntCstPis + "','"
                + entradaItens.NoCodigo.Trim() + "','"
                + entradaItens.NoComp.Trim() + "','"
                + entradaItens.CompNumero + "',"
                + Funcoes.BFormataValor(entradaItens.EntComissao) + ",";
            if (entradaItens.EntDataValidade == Convert.ToDateTime("01/01/2001 00:00:00"))
            {
                strCmd += "null,";
            }
            else
            {
                strCmd += Funcoes.BData(entradaItens.EntDataValidade) + ",";
            }

            strCmd += "'" + entradaItens.ProdutoNovo + "',"
                + entradaItens.EstIndice + ","
                + Funcoes.BFormataValor(entradaItens.EntPreCompra) + ",'"
                + entradaItens.EntProdCest + "',"
                + Funcoes.BFormataValor(entradaItens.EntPmc) + ",'"
                + entradaItens.EntRegistroMS + "',"
                + Funcoes.BDataHora(entradaItens.DtCadastro) + ",'"
                + entradaItens.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public List<EntradaItens> CarregaEntradasItens(int estCodigo, int empCodigo, int entradaID)
        {
            List<EntradaItens> listaEntradaItens = new List<EntradaItens>();

            using (DataTable table = BancoDados.GetDataTable("SELECT * FROM ENTRADA_ITENS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND ENT_ID = " + entradaID + " ORDER BY ENT_SEQUENCIA", null))
            {
                foreach (DataRow row in table.Rows)
                {
                    EntradaItens dEntradaItens = new EntradaItens();
                    dEntradaItens.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    dEntradaItens.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEntradaItens.EntId = Convert.ToInt32(row["ENT_ID"]);
                    dEntradaItens.CfDocto = row["CF_DOCTO"].ToString();
                    dEntradaItens.EntDocto = row["ENT_DOCTO"].ToString();
                    dEntradaItens.EntSerie = row["ENT_SERIE"].ToString();
                    dEntradaItens.EntSequencia = Convert.ToInt32(row["ENT_SEQUENCIA"]);
                    dEntradaItens.ProdCodigo = row["PROD_CODIGO"].ToString();
                    dEntradaItens.ItemDescr = row["ITEM_DESCR"].ToString();
                    dEntradaItens.EntAplicacao = row["ENT_APLICACAO"].ToString();
                    dEntradaItens.EntQtdeEstoque = Convert.ToInt32(row["ENT_QTDESTOQUE"]);
                    dEntradaItens.EntQtde = Convert.ToInt32(row["ENT_QTDE"]);
                    dEntradaItens.EntUnidade = row["ENT_UNIDADE"].ToString();
                    dEntradaItens.EntUnitario = Convert.ToDouble(row["ENT_UNITARIO"]);
                    dEntradaItens.EntDescontoItem = Convert.ToDouble(row["ENT_DESCONTOITEM"]);
                    dEntradaItens.EntTotalItem = Convert.ToDouble(row["ENT_TOTITEM"]);
                    dEntradaItens.EntMargem = Convert.ToDouble(row["ENT_MARGEM"]);
                    dEntradaItens.EntValorVenda = Convert.ToDouble(row["ENT_VALOR_VENDA"]);
                    dEntradaItens.EntLote = row["ENT_LOTE"].ToString();
                    dEntradaItens.EntICMS = row["ENT_ICMS"].ToString();
                    dEntradaItens.EntAliqIcms = Convert.ToDouble(row["ENT_ALIQICMS"]);
                    dEntradaItens.EntValorBcIcms = Convert.ToDouble(row["ENT_VALOR_BC_ICMS"]);
                    dEntradaItens.EntValorIcms = Convert.ToDouble(row["ENT_VALORICMS"]);
                    dEntradaItens.ProdCST = row["PROD_CST"].ToString();
                    dEntradaItens.EntBcIcmsSt = Convert.ToDouble(row["ENT_BC_ICMS_ST"]);
                    dEntradaItens.EntIcmsSt = Convert.ToDouble(row["ENT_ICMS_ST"]);
                    dEntradaItens.EntRedBaseIcms = Convert.ToDouble(row["ENT_REDUCAOBASEICMS"]);
                    dEntradaItens.EntIcmsRet = Convert.ToDouble(row["ENT_ICMS_RET"]);
                    dEntradaItens.EntEnqIpi = row["ENT_ENQ_IPI"].ToString();
                    dEntradaItens.EntCstIpi = row["ENT_CST_IPI"].ToString();
                    dEntradaItens.EntBaseIpi = Convert.ToDouble(row["ENT_BASE_IPI"]);
                    dEntradaItens.EntAliqIpi = Convert.ToDouble(row["ENT_ALIQIPI"]);
                    dEntradaItens.EntValorIpi = Convert.ToDouble(row["ENT_VALORIPI"]);
                    dEntradaItens.EntOrigem = Convert.ToInt32(row["ENT_ORIGEM"]);
                    dEntradaItens.ProdNCM = row["NCM"].ToString();
                    dEntradaItens.ProdCFOP = row["PROD_CFOP"].ToString();
                    dEntradaItens.EntCstCofins = row["ENT_CST_COFINS"].ToString();
                    dEntradaItens.EntCstPis = row["ENT_CST_PIS"].ToString();
                    dEntradaItens.NoCodigo = row["NO_CODIGO"].ToString();
                    dEntradaItens.NoComp = row["NO_COMP"].ToString();
                    dEntradaItens.CompNumero = row["COMP_NUMERO"].ToString();
                    dEntradaItens.EntComissao = Convert.ToDouble(row["ENT_COMISSAO"]);
                    dEntradaItens.EntDataValidade = row["ENT_DATAVALIDADE"].ToString() == "" ? Convert.ToDateTime("01/01/2001 00:00:00") : Convert.ToDateTime(row["ENT_DATAVALIDADE"]);
                    dEntradaItens.ProdutoNovo = row["ENT_PRODUTO_NOVO"].ToString();
                    dEntradaItens.EstIndice = Convert.ToInt32(row["EST_INDICE"]);
                    dEntradaItens.EntPreCompra = row["ENT_PRE_COMPRA"].ToString() == "" ? Convert.ToDouble(row["ENT_UNITARIO"]) : Convert.ToDouble(row["ENT_PRE_COMPRA"]);
                    dEntradaItens.EntProdCest = row["PROD_CEST"].ToString();
                    dEntradaItens.EntPmc = row["ENT_PMC"].ToString() == "" ? 0 : Convert.ToDouble(row["ENT_PMC"]);
                    dEntradaItens.EntRegistroMS = row["ENT_REGISTRO_MS"].ToString();
                    listaEntradaItens.Add(dEntradaItens);
                }
            }

            return listaEntradaItens;
        }
    }
}
