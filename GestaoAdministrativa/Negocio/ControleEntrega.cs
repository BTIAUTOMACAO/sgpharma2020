﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    class ControleEntrega
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int EntControleID { get; set; }
        public long VendaID { get; set; }
        public int CliID { get; set; }
        public int ColCodigo { get; set; }
        public int EntControleRomaneio { get; set; }
        public int EntControleStatus { get; set; }
        public DateTime EntControleData { get; set; }
        public string EntControleEndereco { get; set; }
        public string EntControleNumero { get; set; }
        public string EntControleBairro { get; set; }
        public string EntControleCep { get; set; }
        public string EntControleCidade { get; set; }
        public string EntControleUf { get; set; }
        public string EntControleTelefone { get; set; }
        public double EntControleValorTroco { get; set; }
        public string EntControleNumEntrega { get; set; }
        public string EntControleObservacao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }

        public ControleEntrega() { }

        public ControleEntrega(int empCodigo, int estCodigo, int entControleID, long vendaID, int cliID, int colCodigo, int entControleRomaneio,
            int entControleStatus, DateTime entControleData, string entControleEndereco, string entControleNumero, string entControleBairro,
            string entControleCep, string entControleCidade, string entControleUF, string entControleTelefone, double entControleValorTroco,
            string entControleNumEnt, string entControleObservao, DateTime dtCadastro, string opCadastro, DateTime dtAlteracao, string opAlteracao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.EntControleID = entControleID;
            this.VendaID = vendaID;
            this.CliID = cliID;
            this.ColCodigo = colCodigo;
            this.EntControleRomaneio = entControleRomaneio;
            this.EntControleStatus = entControleStatus;
            this.EntControleData = entControleData;
            this.EntControleEndereco = entControleEndereco;
            this.EntControleNumero = entControleNumero;
            this.EntControleBairro = entControleBairro;
            this.EntControleCep = entControleCep;
            this.EntControleCidade = entControleCidade;
            this.EntControleUf = entControleUF;
            this.EntControleTelefone = entControleTelefone;
            this.EntControleValorTroco = entControleValorTroco;
            this.EntControleNumEntrega = entControleNumEnt;
            this.EntControleObservacao = entControleObservao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
        }

        public DataTable UltimasEntregas(int cliID, int estCodigo, int empCodigo)
        {
            string strSql;

            strSql = "SELECT A.ENT_CONTROLE_ENDER, A.ENT_CONTROLE_NUMERO, A.ENT_CONTROLE_BAIRRO, A.ENT_CONTROLE_CEP, A.ENT_CONTROLE_CIDADE, A.ENT_CONTROLE_UF "
             + " FROM CONTROLE_ENTREGA A"
             + " WHERE A.EMP_CODIGO = " + empCodigo
             + " AND A.EST_CODIGO = " + estCodigo
             + " AND A.CLI_ID = " + cliID
             + " order by a.ENT_CONTROLE_DATA desc";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(ControleEntrega dados)
        {
            string strCmd = "INSERT INTO CONTROLE_ENTREGA(EMP_CODIGO, EST_CODIGO, ENT_CONTROLE_ID,VENDA_ID, CLI_ID,"
                + "ENT_CONTROLE_STATUS,ENT_CONTROLE_DATA,ENT_CONTROLE_ENDER,ENT_CONTROLE_NUMERO, ENT_CONTROLE_BAIRRO,"
                + "ENT_CONTROLE_CEP, ENT_CONTROLE_CIDADE,ENT_CONTROLE_UF,ENT_CONTROLE_FONE,ENT_CONTROLE_VR_TROCO, ENT_CONTROLE_NUM_ENT,"
                + "ENT_CONTROLE_OBSERVACAO,DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.EntControleID + ","
                + dados.VendaID + ","
                + dados.CliID + ","
                + dados.EntControleStatus + ","
                + Funcoes.BData(dados.EntControleData) + ",'"
                + dados.EntControleEndereco + "',";
            if (dados.EntControleNumero.Equals(0))
            {
                strCmd += "null,'";
            }
            else
            {
                strCmd +=  dados.EntControleNumero + ",'";
            }

            strCmd += dados.EntControleBairro + "','"
                + dados.EntControleCep + "','"
                + dados.EntControleCidade + "','"
                + dados.EntControleUf + "','"
                + dados.EntControleTelefone + "',"
                + Funcoes.BValor(dados.EntControleValorTroco) + ",'"
                + dados.EntControleNumEntrega + "','"
                + dados.EntControleObservacao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaEntregas(int estCodigo, int empCodigo, DateTime dtInicial, DateTime dtFinal, long vendaID = 0, int romaneio = 0,
            int colVendedor = -1, int status = -1)
        {
            string strSql = "SELECT A.ENT_CONTROLE_DATA,  DECODE(A.ENT_CONTROLE_STATUS,0,'PENDENTE',1,'EM TRÂNSITO',2,'FINALIZADA',9,'CANCELADA') AS ENT_CONTROLE_STATUS,"
                    + " A.ENT_CONTROLE_ROMANEIO, C.COL_NOME, B.VENDA_ID, A.EMP_CODIGO, A.EST_CODIGO, D.COL_NOME AS VENDEDOR, E.CF_NOME, E.CF_DOCTO,"
                    + " A.ENT_CONTROLE_ENDER, A.ENT_CONTROLE_NUMERO, A.ENT_CONTROLE_BAIRRO, A.ENT_CONTROLE_CIDADE, A.ENT_CONTROLE_CEP, B.VENDA_TOTAL,"
                    + " TO_CHAR(B.Venda_Data_Hora, 'HH24:mi:ss') AS HORAS,  '' as FORMA_DESCRICAO, A.ENT_CONTROLE_ID, A.OP_ALTERACAO"
                    + " FROM CONTROLE_ENTREGA A"
                    + " INNER JOIN VENDAS B ON(A.VENDA_ID = B.VENDA_ID)"
                    + " LEFT JOIN COLABORADORES C ON(A.COL_CODIGO = C.COL_CODIGO AND A.EMP_CODIGO = C.EMP_CODIGO)"
                    + " LEFT JOIN COLABORADORES D ON(B.VENDA_COL_CODIGO = D.COL_CODIGO AND D.EMP_CODIGO = B.EMP_CODIGO)"
                    + " INNER JOIN CLIFOR E ON A.CLI_ID = E.CF_ID"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = B.EST_CODIGO"
                    + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                    + " AND A.ENT_CONTROLE_DATA BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
          

            if (!status.Equals(4))
            {
                if(status.Equals(5))
                {
                    strSql += " AND A.ENT_CONTROLE_STATUS IN (0,1)";
                }
                else if (!status.Equals(-1))
                {
                    status = status == 3 ? 9 : status;
                    strSql += " AND A.ENT_CONTROLE_STATUS IN (" + status + ")";
                }
                else
                    strSql += " AND A.ENT_CONTROLE_STATUS IN (0,1)";
            }
            strSql += " GROUP BY A.ENT_CONTROLE_DATA,B.VENDA_ID, ENT_CONTROLE_STATUS,ENT_CONTROLE_ROMANEIO,C.COL_NOME,A.EMP_CODIGO,A.EST_CODIGO,CF_NOME,"
                + " COL_NOME,CF_DOCTO,ENT_CONTROLE_ENDER, ENT_CONTROLE_NUMERO,ENT_CONTROLE_BAIRRO,ENT_CONTROLE_CIDADE,ENT_CONTROLE_CEP,VENDA_TOTAL,"
                + " Venda_Data_Hora,ENT_CONTROLE_ID,A.VENDA_ID,A.OP_ALTERACAO"
                + " ORDER BY A.ENT_CONTROLE_DATA, A.VENDA_ID";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaRomaneioStatusNumControle(int empCodigo, int estCodigo, int entregaID, long vendaID, bool retorno = false, int entregador = 0,
            int romaneio = 0, string numControle = "", int status = 0)
        {
            string strCmd = "UPDATE CONTROLE_ENTREGA SET ";
            strCmd += " ENT_CONTROLE_STATUS = " + status;

            if (status != 2)
            {
                if (entregador.Equals(0) && retorno)
                {
                    strCmd += " , COL_CODIGO = null";
                }
                else
                {
                    strCmd += " , COL_CODIGO = " + entregador;
                }

                if (romaneio.Equals(0) && retorno)
                {
                    strCmd += " , ENT_CONTROLE_ROMANEIO = null";
                }
                else
                {
                    strCmd += " , ENT_CONTROLE_ROMANEIO = " + romaneio;
                }
                if (!String.IsNullOrEmpty(numControle) && retorno)
                {
                    strCmd += " ,ENT_CONTROLE_NUM_ENT = '" + numControle;
                }
                else
                {
                    strCmd += " ,ENT_CONTROLE_NUM_ENT = null";
                }
            }
            strCmd += " , DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OP_ALTERACAO = '" + Principal.usuario + "'";
            strCmd += " WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo + " AND ENT_CONTROLE_ID = " + entregaID
                + " AND VENDA_ID = " + vendaID;
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
        
        public DataTable BuscaEntregasPendentes()
        {
            string strSql = "SELECT DISTINCT G.FORMA_DESCRICAO, A.ENT_CONTROLE_DATA,"
                           + "   DECODE(A.ENT_CONTROLE_STATUS,"
                           + "           0,"
                           + "           'PENDENTE',"
                           + "          1,"
                           + "           'EM TRÂNSITO') AS ENT_CONTROLE_STATUS,"
                           + "    B.VENDA_ID,"
                           + "    D.COL_NOME AS VENDEDOR,"
                           + "    E.CF_NOME,"
                           + "    B.VENDA_TOTAL,"
                           + "    TO_CHAR(B.Venda_Data_Hora, 'HH24:mi:ss') AS HORAS"
                           + "       FROM CONTROLE_ENTREGA A"
                           + "       INNER JOIN VENDAS B ON (A.VENDA_ID = B.VENDA_ID)"
                           + "       LEFT JOIN COLABORADORES D ON(B.VENDA_COL_CODIGO = D.COL_CODIGO AND"
                           + "                                    D.EMP_CODIGO = B.EMP_CODIGO)"
                           + "      INNER JOIN CLIFOR E ON A.CLI_ID = E.CF_ID"
                           + "      INNER JOIN VENDAS_FORMA_PAGAMENTO F ON B.VENDA_ID = F.VENDA_ID"
                           + "      INNER JOIN FORMAS_PAGAMENTO G ON F.VENDA_FORMA_ID = G.FORMA_ID"
                           + "      WHERE A.ENT_CONTROLE_STATUS IN(0, 1)"
                           + "        ORDER BY A.ENT_CONTROLE_DATA, B.VENDA_ID";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
