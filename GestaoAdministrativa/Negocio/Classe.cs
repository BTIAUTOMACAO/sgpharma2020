﻿using GestaoAdministrativa.Classes;
using System;
using System.Data;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Classe
    {
        public int EmpCodigo { get; set; }
        public int ClasCodigo { get; set; }
        public string ClasDescr { get; set; }
        public string ClasLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Classe() { }

        public Classe(int empCodigo, int clasCodigo, string clasDescr, string clasLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.ClasCodigo = clasCodigo;
            this.ClasDescr = clasDescr;
            this.ClasLiberado = clasLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        /// <summary>
        /// INSERE OS DADOS NA TABELA CLASSES
        /// </summary>
        /// <param name="dados">Objeto do Tipo Classe</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public bool InsereRegistros(Classe dados)
        {
            string strCmd = "INSERT INTO CLASSES(EMP_CODIGO, CLAS_CODIGO, CLAS_DESCR, CLAS_DESABILITADO,  DTCADASTRO, OPCADASTRO) VALUES (" +
                dados.EmpCodigo + "," +
                dados.ClasCodigo + ",'" +
                dados.ClasDescr + "','" +
                dados.ClasLiberado + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE CADASTRO DE CLASSES
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Classe com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public DataTable BuscaDados(int empCodigo, string clasCodigo, string clasDescr, string clasLiberado, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.EMP_CODIGO, A.CLAS_CODIGO, A.CLAS_DESCR, CASE WHEN A.CLAS_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS CLAS_DESABILITADO, "
                     + "  A.DAT_ALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO"
                     + " FROM CLASSES A WHERE A.EMP_CODIGO = " + empCodigo;

            if (clasCodigo != "")
            {
                strSql += " AND A.CLAS_CODIGO = " + clasCodigo;
            }
            if (clasDescr != "")
            {
                strSql += " AND A.CLAS_DESCR LIKE '%" + clasDescr + "%'";
            }
            if (clasLiberado != "TODOS")
            {
                clasLiberado = clasLiberado == "S" ? "N" : "S";
                strSql += " AND A.CLAS_DESABILITADO = '" + clasLiberado + "'";
            }
            strOrdem = strSql;
            strSql += " ORDER BY A.CLAS_CODIGO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool AtualizaDados(Classe dadosNovos, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;
            StringBuilder cmd = new StringBuilder();
            cmd.Append("UPDATE CLASSES SET ");
            if (!dadosNovos.ClasDescr.Equals(dadosOld.Rows[0]["CLAS_DESCR"]))
            {
                cmd.Append("CLAS_DESCR = '" + dadosNovos.ClasDescr + "',");
                dados[contMatriz, 0] = "CLAS_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLAS_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ClasDescr + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.ClasLiberado.Equals(dadosOld.Rows[0]["CLAS_DESABILITADO"]))
            {
                cmd.Append("CLAS_DESABILITADO = '" + dadosNovos.ClasLiberado + "',");
                dados[contMatriz, 0] = "CLAS_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLAS_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ClasLiberado + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {
                cmd.Append(" DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",");
                cmd.Append(" OPALTERACAO = '" + dadosNovos.OpAlteracao + "' ");
                cmd.Append(" WHERE CLAS_CODIGO = " + dadosNovos.ClasCodigo);
                if (BancoDados.ExecuteNoQuery(cmd.ToString(), null) != -1)
                {
                    Funcoes.GravaLogAlteracao("CLAS_CODIGO", dadosNovos.ClasCodigo.ToString(), dadosNovos.OpAlteracao, "CLASSES", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;



        }

        public bool ExcluiClasse(int classeCodigo)
        {
            string strCmd = "DELETE FROM CLASSES WHERE CLAS_CODIGO = " + classeCodigo;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}

