﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalCancelamentoNegocio
    {
        public void CancelarFuncional(ClienteCartaoFuncional dadosVenda, int numeroSequencialDoArquivo, string valorDaVenda,ConfirmaVendaResponse objConfirmaVendaResponse)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);

            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            //double totalVenda = Convert.ToDouble(listaDeProdutos.Compute("SUM(PROD_TOTAL)", "")) * 100;
            writer.WriteLine("003-000 = " + objConfirmaVendaResponse.ValorDaOperacao);
            writer.WriteLine("011-000 = 52");
            writer.WriteLine("025-000 = "+ objConfirmaVendaResponse.NSU);
            writer.WriteLine("026-000 = "+dadosVenda.DataDaTransacao);
            writer.WriteLine("027-000 = "+objConfirmaVendaResponse.NSUDoAutorizador);
            writer.WriteLine("098-000 = " + dadosVenda.Trilha2DoCartao);
            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            FuncionalUtil.RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }

    }
}
