﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalConsultaCartaoNegocio
    {
        private static int nEventsFired = 0;
        private static DateTime previousTime;
        //private static Timer aTimer;
        private static int seq = 0;
        private static bool validouArquivoStatus = false;
        

        /// <summary>
        /// Primeira etapa - Método principal para a geração do txt de envio no formato 00000000.ENV
        /// </summary>
        /// <param name="numeroCartao"></param>
        /// <param name="numeroSequencialDoArquivo"></param>
        public void GeraTxtDeEnvio(string numeroCartao, int numeroSequencialDoArquivo)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);
            //StreamWriter writer = new StreamWriter(@"C:\TVAT\ENV\" + codigoDeProcessamento.PadLeft(8, '0') + ".ENV");
            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            writer.WriteLine("011-000 = 107");
            writer.WriteLine("098-000 = " + numeroCartao);
            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }

        /// <summary>
        /// Segunda etapa - Verificação do arquivo de status por 5 segundos(default) ou parametro definido
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        /// 
        public bool ValidarArquivoStatus(int numeroSequencialDoArquivo)
        {
            Principal.ValidouArquivoDeStatus = false;
            int i = 0;
            int numeroTentativasParaBuscarArquivo = Convert.ToInt32(Funcoes.LeParametro(14, "07", false));
            while (i < numeroTentativasParaBuscarArquivo)
            {
                Thread.Sleep(1000);

                Principal.ValidouArquivoDeStatus = ExisteArquivoDeStatus(numeroSequencialDoArquivo);
                if (Principal.ValidouArquivoDeStatus)
                {
                    break;
                }
                i++;
            }

            return Principal.ValidouArquivoDeStatus;
        }

        /// <summary>
        /// Terceira etapa - Verificar se o arquivo de resposta existe por 40 segundos(default) ou parametro definido
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public bool ValidarArquivoDeResposta(int numeroSequencialDoArquivo)
        {

            Principal.ValidouArquivoDeResposta = false;
            int i = 0;
            int numeroTentativasParaBuscarArquivo = Convert.ToInt32(Funcoes.LeParametro(14, "08", false));
            while (i < numeroTentativasParaBuscarArquivo)
            {
                Thread.Sleep(1000);

                Principal.ValidouArquivoDeResposta = ExisteArquivoDeResposta(numeroSequencialDoArquivo);
                if (Principal.ValidouArquivoDeResposta)
                {
                    break;
                }
                i++;
            }

            return Principal.ValidouArquivoDeResposta;
        }
                
        /// <summary>
        /// Quarta Etapa - Valida a consistencia dos arquivos de envio e o de resposta analisando o parametro
        /// 001-00 dos arquivos e retorna obj ClienteCartaoFuncional Preenchido
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public ClienteCartaoFuncional ValidaConsistenciaArquivosEnviados(int numeroSequencialDoArquivo)
        {
            
            string arquivoEnvio = @"" + Funcoes.LeParametro(14, "05", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".log";
            string arquivoResposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
            
            var numeroDoArquivoEnvio = File.ReadAllLines(arquivoEnvio)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            var numeroDoArquivoResposta = File.ReadAllLines(arquivoResposta)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            try
            {
                Principal.ValidouConsistenciaArquivos = numeroDoArquivoEnvio[0].ToString() == numeroDoArquivoResposta[0].ToString();
            
                if (Principal.ValidouConsistenciaArquivos)
                {
                    ClienteCartaoFuncional transacaoClienteCartao;
                    transacaoClienteCartao = CarregaRetornoClienteCartao(arquivoResposta);
                    ApagarArquivoDeResposta(arquivoResposta);
                    return transacaoClienteCartao;
                }
                else return null;
            }
            catch (Exception)
            {
                throw new Exception("Os arquivos não são consistentes");
            }
        }


        #region Funções Uteis
        /// <summary>
        /// Método que extai o número sequencial + 1 do arquivo AUTORIZADOR.SEQ
        /// localizado na pasta de envio do tvat
        /// </summary>
        /// <returns>NumeroSequencia</returns>
        /// 
        public int GeraSequenciaArquivoAutorizadorSeq()
        {

            seq = 0;
            //TODO Alterar para caminho parametrizado
            string arquivo = @"" + Funcoes.LeParametro(14, "09", false) + "AUTORIZADOR.SEQ";
            int retornoSequencia = 0;
            if (File.Exists(arquivo))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(arquivo))
                    {
                        string linha = sr.ReadLine();
                        retornoSequencia = (Convert.ToInt32(linha) + 1);
                        seq = retornoSequencia;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
            Console.ReadLine();
            return retornoSequencia;
        }

        /// <summary>
        /// Método que incrementa o arquivo AUTORIZADOR.SEQ
        /// </summary>
        /// <param name="numeroAtualSequencialDoArquivo"></param>
        public void IncrementaSequenciaArquivoAutorizadorSeq(int numeroAtualSequencialDoArquivo)
        {
            //TODO Verificar se precisa ser dinâmico o caminho
            //Caminho do arquivo AUTORIZADOR.SEQ
            string arquivo = @"" + Funcoes.LeParametro(14, "09", false) + "AUTORIZADOR.SEQ";
            StreamWriter writer = new StreamWriter(arquivo);
            writer.WriteLine(numeroAtualSequencialDoArquivo.ToString().PadLeft(8, '0'));
            writer.Close();
            writer.Dispose();
        }

        /// <summary>
        /// Renomeia extensão de um arquivo passando o nome atual + nome da extensão desejada
        /// </summary>
        /// <param name="nomeAntigo"></param>
        /// <param name="extensaoDesejada"></param>
        private void RenomearExtensaoArquivo(string nomeAntigo, string extensaoDesejada)
        {
            System.IO.File.Move(@"" + nomeAntigo,
                @"" + nomeAntigo.Substring(0, nomeAntigo.Length - 4) + "." + extensaoDesejada);
        }

        /// <summary>
        /// Verifica se o arquivo de status .sta existe na pasta tvat/resp/000000.sta 
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        private static bool ExisteArquivoDeStatus(int numeroSequencialDoArquivo)
        {
            string arquivoStatus =
                @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".sta";

            return (File.Exists(arquivoStatus) ? true : false);

        }

        /// <summary>
        /// Verifica se o arquivo de resposta .rsp existe na pasta tvat/resp/000000.rsp
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        private static bool ExisteArquivoDeResposta(int numeroSequencialDoArquivo)
        {
            string arquivoStatus =
                @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";

            return (File.Exists(arquivoStatus) ? true : false);

        }

        /// <summary>
        /// Apaga arquivo de status
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public bool ApagarArquivoDeStatus(int numeroSequencialDoArquivo)
        {
            string pathArquivoStatus = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".sta";
            if (System.IO.File.Exists(pathArquivoStatus))
            {
                try
                {
                    System.IO.File.Delete(pathArquivoStatus);
                    return true;
                }
                catch (Exception )
                {
                    return false;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Apaga arquivo de resposta
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public static bool ApagarArquivoDeResposta(string arquivoDeResposta)
        {
            string pathArquivoStatus = arquivoDeResposta;
            if (System.IO.File.Exists(pathArquivoStatus))
            {
                try
                {
                    System.IO.File.Delete(pathArquivoStatus);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }
        /// <summary>
        /// Retorna a lista de parametros do arquivo de retorno quebrado por código em um List
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public static List<RetornoFuncional> CarregaParametros(string arquivo)
        {
            List<RetornoFuncional> lista = new List<RetornoFuncional>();
            string arquivoEnvio = @""+arquivo;
            TextReader tr = new StreamReader(arquivoEnvio, Encoding.Default);
            string linha = "";
            while ((linha = tr.ReadLine()) != null)
            {
                if (!linha.StartsWith("*")) //parâmetro comentado ?!
                {
                    string[] value = linha.Split('=');
                    lista.Add(new RetornoFuncional() { Id = value[0].Trim(), Valor = value[1].Trim() });
                }
            }
            tr.Close();
            tr.Dispose();
            return lista;
        }

        public static ClienteCartaoFuncional CarregaRetornoClienteCartao(string arquivo)
        {
            List<RetornoFuncional> parametros = CarregaParametros(arquivo);
            ClienteCartaoFuncional transacaoClienteCartao = new ClienteCartaoFuncional();
            
            transacaoClienteCartao.Header = parametros.SingleOrDefault(p => p.Id == "000-000").Valor;
            transacaoClienteCartao.Identificacao = parametros.SingleOrDefault(p => p.Id == "001-000").Valor;
            transacaoClienteCartao.CodigoDaResposta = parametros.SingleOrDefault(p => p.Id == "009-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "011-000") == null))
            {
                transacaoClienteCartao.CodigoDoProcessamento = parametros.SingleOrDefault(p => p.Id == "011-000").Valor;
            }    
                
            transacaoClienteCartao.NumeroDaTransacoNSU  = parametros.SingleOrDefault(p => p.Id == "012-000").Valor;
            transacaoClienteCartao.NsuDoAutorizador     = parametros.SingleOrDefault(p => p.Id == "013-000").Valor;
            transacaoClienteCartao.DataDaTransacao      = parametros.SingleOrDefault(p => p.Id == "022-000").Valor;
            transacaoClienteCartao.HoraDaTransacao      = parametros.SingleOrDefault(p => p.Id == "023-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "030-000") == null))
            {
                transacaoClienteCartao.TextoParaOperador = parametros.SingleOrDefault(p => p.Id == "030-000").Valor;
            }
            
            if (!(parametros.SingleOrDefault(p => p.Id == "098-000") == null))
            {
                transacaoClienteCartao.Trilha2DoCartao = parametros.SingleOrDefault(p => p.Id == "098-000").Valor;
            }
                 
            transacaoClienteCartao.NomeDoPortador       = parametros.SingleOrDefault(p => p.Id == "100-000").Valor;

            if (!(parametros.SingleOrDefault(p => p.Id == "307-000") == null))
            {
                transacaoClienteCartao.MapaDeAcoes = parametros.SingleOrDefault(p => p.Id == "307-000").Valor;
            }
            if (!(parametros.SingleOrDefault(p => p.Id == "308-000") == null))
            {
                transacaoClienteCartao.MensagemSobreCliente = parametros.SingleOrDefault(p => p.Id == "308-000").Valor;
            }
            
            transacaoClienteCartao.DescricaoTransacao   = parametros.SingleOrDefault(p => p.Id == "370-000").Valor;
            transacaoClienteCartao.Trailer              = parametros.SingleOrDefault(p => p.Id == "999-999").Valor;

            //string valor001 = parametros.Single(p => p.Id == "001-000").Valor;

            return transacaoClienteCartao;

        }
        #endregion
    }
}
