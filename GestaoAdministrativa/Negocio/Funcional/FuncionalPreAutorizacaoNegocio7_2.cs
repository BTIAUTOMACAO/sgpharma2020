﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalPreAutorizacaoNegocio7_2
    {
        /// <summary>
        /// Primeira etapa - Método principal para a geração do txt de envio no formato 00000000.ENV
        /// </summary>
        /// <param name="objPreAutorizacaoFuncionalrequest"></param>
        /// <param name="numeroSequencialDoArquivo"></param>
        public void SolicitaPreAutorizacao(ClienteCartaoFuncional dadosVenda, int numeroSequencialDoArquivo, DataTable listaDeProdutos)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);

            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            double totalVenda = Convert.ToDouble(listaDeProdutos.Compute("SUM(PROD_TOTAL)", "")) * 100;
            writer.WriteLine("003-000 = " + totalVenda.ToString());
            writer.WriteLine("011-000 = 109");
            writer.WriteLine("098-000 = " + dadosVenda.Trilha2DoCartao);
            writer.WriteLine("301-000 = " + (dadosVenda.CodConselho == null || dadosVenda.CodConselho == "" ? "0" : dadosVenda.CodConselho));
            writer.WriteLine("302-000 = " + (dadosVenda.UF == null || dadosVenda.UF == "" ? "0" : dadosVenda.UF));
            //todo Validar se o VALOR A PAGAR A VISTA  é sempre 0 - perguntar pra funcional se existe este tipo de caso
            writer.WriteLine("304-000 = 0");
            int qtdMedicamentos = listaDeProdutos.Rows.Count;
            writer.WriteLine("312-000 = " + qtdMedicamentos.ToString());
            for (int i = 0; i < qtdMedicamentos; i++)
            {
                writer.WriteLine("313-00" + (i + 1) + " = " + dadosVenda.DataDaTransacao);
                //todo Validar com a funcional se o param 314 é sempre 0
                writer.WriteLine("314-00" + (i + 1) + " = 0");
                writer.WriteLine("315-00" + (i + 1) + " = " + dadosVenda.CodConselho.PadLeft(10, '0'));
                writer.WriteLine("316-00" + (i + 1) + " = " + dadosVenda.UF.PadLeft(2, '0'));
                //todo Validar com a funcional se o param 317 é sempre 0
                writer.WriteLine("317-00" + (i + 1) + " = 0");
                writer.WriteLine("318-00" + (i + 1) + " = " + listaDeProdutos.Rows[i]["COD_BARRA"].ToString());
                writer.WriteLine("321-00" + (i + 1) + " = " + (Convert.ToDouble(listaDeProdutos.Rows[i]["PRECO_MAXIMO"]) * 100).ToString());
                writer.WriteLine("323-00" + (i + 1) + " = " + (Convert.ToDouble(listaDeProdutos.Rows[i]["PRE_VALOR"]) * 100).ToString());
                writer.WriteLine("326-00" + (i + 1) + " = " + listaDeProdutos.Rows[i]["PROD_QTDE"].ToString());
                writer.WriteLine("327-00" + (i + 1) + " = " + listaDeProdutos.Rows[i]["PROD_QTDE"].ToString());
                //listaDeProdutos.Rows[i][""].ToString();
            }
            //writer.WriteLine("

            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            FuncionalUtil.RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }

        /// <summary>
        /// Carrega no OBJETO de resposta os parâmetros que foram retornados no arquivo .rsp
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public PreAutorizacaoFuncionalResponse PreAutorizacaoResponse(int numeroSequencialDoArquivo, string temReceitaMedica)
        {
            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
            List<RetornoFuncional> parametros = FuncionalUtil.CarregaParametros(ArquivoTemporarioDeEnvio);

            PreAutorizacaoFuncionalResponse objPreAutorizacaoResponse = new PreAutorizacaoFuncionalResponse();
            List<ProdutoPreAutorizacao> lista = new List<ProdutoPreAutorizacao>();
            ProdutoPreAutorizacao objProdutoPreAutorizacao;


            objPreAutorizacaoResponse.Header = parametros.SingleOrDefault(p => p.Id == "000-000").Valor;
            objPreAutorizacaoResponse.Identificacao = parametros.SingleOrDefault(p => p.Id == "001-000").Valor;
            objPreAutorizacaoResponse.ValorDaOperacao = parametros.SingleOrDefault(p => p.Id == "003-000").Valor;
            objPreAutorizacaoResponse.CodigoDaResposta = parametros.SingleOrDefault(p => p.Id == "009-000").Valor;
            objPreAutorizacaoResponse.NSU = parametros.SingleOrDefault(p => p.Id == "012-000").Valor;
            objPreAutorizacaoResponse.NSUDoAutorizador = parametros.SingleOrDefault(p => p.Id == "013-000").Valor;
            objPreAutorizacaoResponse.DataTransacao = parametros.SingleOrDefault(p => p.Id == "022-000").Valor;
            objPreAutorizacaoResponse.HoraTransacao = parametros.SingleOrDefault(p => p.Id == "023-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "030-000") == null))
            {
                objPreAutorizacaoResponse.TextoParaOperador = parametros.SingleOrDefault(p => p.Id == "030-000").Valor;
            }
            objPreAutorizacaoResponse.Autenticacao = parametros.SingleOrDefault(p => p.Id == "032-000").Valor;
            objPreAutorizacaoResponse.SaldoCartao = parametros.SingleOrDefault(p => p.Id == "097-000").Valor;
            objPreAutorizacaoResponse.NomeDoPortador = parametros.SingleOrDefault(p => p.Id == "100-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "300-000") == null))
            {
                objPreAutorizacaoResponse.CodigoPreAutorizacao = parametros.SingleOrDefault(p => p.Id == "300-000").Valor;
            }
            //PREENCHE CAMPOS OPCIONAIS QUE SERÃO RETORNADOS APENAS SE A TRANSAÇÃO FOI APROVADA OU POSSUI RESTRIÇÕES 26,27 OU 28
            if (objPreAutorizacaoResponse.CodigoDaResposta == "00" || objPreAutorizacaoResponse.CodigoDaResposta == "26"
                || objPreAutorizacaoResponse.CodigoDaResposta == "27" || objPreAutorizacaoResponse.CodigoDaResposta == "28")
            {
                objPreAutorizacaoResponse.ValorPagarAVista = parametros.SingleOrDefault(p => p.Id == "304-000").Valor;
                objPreAutorizacaoResponse.QtdMedicamentos = parametros.SingleOrDefault(p => p.Id == "312-000").Valor;

                int qtdMedicamentos = Convert.ToInt32(objPreAutorizacaoResponse.QtdMedicamentos);

                if (temReceitaMedica.Equals("S"))
                    objPreAutorizacaoResponse.VendaComReceita = "S";
                else
                    objPreAutorizacaoResponse.VendaComReceita = "N";

                //PREENCHENDO OS PARAMETROS 313 A 328
                for (int i = 0; i < qtdMedicamentos; i++)
                {
                    objProdutoPreAutorizacao = new ProdutoPreAutorizacao();
                    if (temReceitaMedica.Equals("S"))
                    {
                        objPreAutorizacaoResponse.VendaComReceita = "S";
                        objProdutoPreAutorizacao.DataReceita = parametros.SingleOrDefault(p => p.Id == "313-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.TipoConselho = parametros.SingleOrDefault(p => p.Id == "314-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.CodConselho = parametros.SingleOrDefault(p => p.Id == "315-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.UFConselho = parametros.SingleOrDefault(p => p.Id == "316-00" + (i + 1)).Valor;
                    }
                    else
                    {
                        objPreAutorizacaoResponse.VendaComReceita = "N";
                        objProdutoPreAutorizacao.DataReceita = "0";
                        objProdutoPreAutorizacao.TipoConselho = "0";
                        objProdutoPreAutorizacao.CodConselho = "0";
                        objProdutoPreAutorizacao.UFConselho = "0";
                    }

                    objProdutoPreAutorizacao.TipoProduto = parametros.SingleOrDefault(p => p.Id == "317-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.CodBarras = parametros.SingleOrDefault(p => p.Id == "318-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.DescricaoMedicamento = parametros.SingleOrDefault(p => p.Id == "319-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.CondicaoVenda = parametros.SingleOrDefault(p => p.Id == "320-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoMaximo = parametros.SingleOrDefault(p => p.Id == "321-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoPBM = parametros.SingleOrDefault(p => p.Id == "322-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoVenda = parametros.SingleOrDefault(p => p.Id == "324-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.StatusItem = parametros.SingleOrDefault(p => p.Id == "325-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.QuantidadeReceitada = parametros.SingleOrDefault(p => p.Id == "326-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.QuantidadeAutorizada = parametros.SingleOrDefault(p => p.Id == "327-00" + (i + 1)).Valor;
                    if (!(parametros.SingleOrDefault(p => p.Id == "328-00" + (i + 1)) == null))
                    {
                        objProdutoPreAutorizacao.Referencia = parametros.SingleOrDefault(p => p.Id == "328-00" + (i + 1)).Valor;
                    }
                    else
                    {
                        objProdutoPreAutorizacao.Referencia = "0";
                    }

                    lista.Add(objProdutoPreAutorizacao);
                }
            }
            objPreAutorizacaoResponse.ProdutosPreAutorizacao = lista;
            objPreAutorizacaoResponse.NomeDoPortador = parametros.SingleOrDefault(p => p.Id == "370-000").Valor;

            return objPreAutorizacaoResponse;
        }

     


    }
}