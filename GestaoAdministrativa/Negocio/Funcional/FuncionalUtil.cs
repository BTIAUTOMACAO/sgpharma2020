﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public static class FuncionalUtil
    {
        public struct mensagemErro
        {
            public string codigo;
            public string mensagem;
        }

        //#region mensagens erro
        //public static mensagemErro[] mensagensErro =
        //            new mensagemErro[]{ //máximo de 36 colunas!!!
        //        /*00*/    new mensagemErro() { codigo = "01", mensagem = "Erro ao procurar o item na pré-autorização" }, //30
        //        /*01*/    new mensagemErro() { codigo = "02", mensagem = "CRM Inválido" }, //17
        //        /*02*/    new mensagemErro() { codigo = "03", mensagem = "Data da receita maior que a atual" }, //21
        //        /*03*/    new mensagemErro() { codigo = "04", mensagem = "Produto Inválido"}, // 14
        //        /*04*/    new mensagemErro() { codigo = "05", mensagem = "Preço informado maior que o da funcional." }, //23
        //        /*05*/    new mensagemErro() { codigo = "06", mensagem = "Autorização não é de débito" }, //27
        //        /*06*/    new mensagemErro() { codigo = "07", mensagem = "Produto na lista de exclusão" }, //36
        //        /*07*/    new mensagemErro() { codigo = "08", mensagem = "Produto sem Preço Máximo cadastrado" }, //33
        //        /*08*/    new mensagemErro() { codigo = "09", mensagem = "Produto não é um medicamento. A empresa compra somente medicamentos." }, //31
        //        /*09*/    new mensagemErro() { codigo = "10", mensagem = "Produto bloqueado" }, // 35
        //        /*10*/    new mensagemErro() { codigo = "11", mensagem = "Tente Novamente" }, //34
        //        /*11*/    new mensagemErro() { codigo = "12", mensagem = "CRM Inválido para a primeira compra."}, //31
        //        /*12*/    new mensagemErro() { codigo = "13", mensagem = "Data da receita vencida" }, //36
        //        /*13*/    new mensagemErro() { codigo = "14", mensagem = "Sem saldo" }, //33
        //        /*14*/    new mensagemErro() { codigo = "15", mensagem = "Produto sem preço de fábrica" }, //26
        //        /*15*/    new mensagemErro() { codigo = "16", mensagem = "Produto com preço diferente da FuncionalCard." }, //20
        //        /*16*/    new mensagemErro() { codigo = "17", mensagem = "Desconto superior a 80 %" }, //28
        //        /*17*/    new mensagemErro() { codigo = "18", mensagem = "Preço do produto não informado" }, //20
        //        /*18*/    new mensagemErro() { codigo = "19", mensagem = "Não há alíquota cadastrada para este UF" }, //21
        //        /*19*/    new mensagemErro() { codigo = "20", mensagem = "Não há preço cadastrado para essa alíquota." }, //27
        //        /*20*/    new mensagemErro() { codigo = "21", mensagem = "Sem saldo sem receita"}, //26
        //        /*22*/    new mensagemErro() { codigo = "22", mensagem = "Sem saldo com receita" }, //26
        //        /*23*/    new mensagemErro() { codigo = "23", mensagem = "Obrigatório informar a receita" }, //34
        //        /*24*/    new mensagemErro() { codigo = "24", mensagem = "Registro já existe na pre_aut_itens" }, //30
        //        /*25*/    new mensagemErro() { codigo = "25", mensagem = "Buffer de envio - Tamanho do buffer do item incorreto" }, //35
        //        /*26*/    new mensagemErro() { codigo = "26", mensagem = "Buffer de retorno - Tamanho do buffer do item incorreto" }, //34
        //        /*27*/    new mensagemErro() { codigo = "27", mensagem = "Cartão não encontrado no sistema" }, //34
        //        /*28*/    new mensagemErro() { codigo = "28", mensagem = "Produto sem preço na funcional" }, //26
        //        /*29*/    new mensagemErro() { codigo = "29", mensagem = "Obrigatório a Digitação de preço para este Produto" }, //18
        //        /*30*/    new mensagemErro() { codigo = "30", mensagem = "Consulta de preços - Tempo de espera esgotado" }, //32
        //        /*31*/    new mensagemErro() { codigo = "31", mensagem = "Data inconsistente" }, //18
        //        /*32*/    new mensagemErro() { codigo = "32", mensagem = "Estabelecimento bloqueado" }, //26
        //        /*33*/    new mensagemErro() { codigo = "33", mensagem = "Consta titular demitido" }, //24
        //        /*34*/    new mensagemErro() { codigo = "34", mensagem = "Emp. participa somente do Prog Desc" }, //36
        //        /*35*/    new mensagemErro() { codigo = "35", mensagem = "Empresa bloqueada até pagamento" }, //32
        //        /*36*/    new mensagemErro() { codigo = "36", mensagem = "Estab. bloq. para vendar para emp." }, //35
        //        /*37*/    new mensagemErro() { codigo = "37", mensagem = "Emp. bloq. para comprar no estab." }, //34
        //        /*38*/    new mensagemErro() { codigo = "38", mensagem = "Segmento não liberado pela empresa" }, //35
        //        /*39*/    new mensagemErro() { codigo = "39", mensagem = "Forma pagto não liberada" }, //18
        //        /*40*/    new mensagemErro() { codigo = "40", mensagem = "Valor incompatível com qtd parcelas" }, //36
        //        /*41*/    new mensagemErro() { codigo = "41", mensagem = "Saldo do titular insuficiente" }, //18
        //        /*42*/    new mensagemErro() { codigo = "42", mensagem = "Erro ao canc. Emp já efetuou fecham." }, //36
        //        /*43*/    new mensagemErro() { codigo = "43", mensagem = "Transação não encontrada" }, //24
        //        /*44*/    new mensagemErro() { codigo = "44", mensagem = "Forma de pagamento inválida"},
        //        /*45*/    new mensagemErro() { codigo = "45", mensagem = "Cancelamento já efetuado"},
        //        /*46*/    new mensagemErro() { codigo = "46", mensagem = "Venda não autorizada, contate o RH da empresa."}
        //};
        //#endregion

        private static int seq = 0;
        /// <summary>
        /// Método que extai o número sequencial + 1 do arquivo AUTORIZADOR.SEQ
        /// localizado na pasta de envio do tvat
        /// </summary>
        /// <returns>NumeroSequencia</returns>
        public static int GeraSequenciaArquivoAutorizadorSeq()
        {
            seq = 0;
            //TODO Alterar para caminho parametrizado
            string arquivo = @"" + Funcoes.LeParametro(14, "05", false) + "AUTORIZADOR.SEQ";

            int retornoSequencia = 0;
            if (File.Exists(arquivo))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(arquivo))
                    {
                        string linha = sr.ReadLine();
                        retornoSequencia = (Convert.ToInt32(linha) + 1);
                        seq = retornoSequencia;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
            Console.ReadLine();
            return retornoSequencia;
        }

        /// <summary>
        /// Segunda etapa - Verificação do arquivo de status por 5 segundos(default) ou parametro definido
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        /// 
        public static bool ValidarArquivoStatus(int numeroSequencialDoArquivo)
        {
            Principal.ValidouArquivoDeStatus = false;
            int i = 0;
            int numeroTentativasParaBuscarArquivo = Convert.ToInt32(Funcoes.LeParametro(14, "07", false));
            while (i < numeroTentativasParaBuscarArquivo)
            {
                Thread.Sleep(1000);

                Principal.ValidouArquivoDeStatus = ExisteArquivoDeStatus(numeroSequencialDoArquivo);
                if (Principal.ValidouArquivoDeStatus)
                {
                    break;
                }
                i++;
            }

            return Principal.ValidouArquivoDeStatus;
        }

        /// <summary>
        /// Terceira etapa - Verificar se o arquivo de resposta existe por 40 segundos(default) ou parametro definido
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public static bool ValidarArquivoDeResposta(int numeroSequencialDoArquivo)
        {

            Principal.ValidouArquivoDeResposta = false;
            int i = 0;
            int numeroTentativasParaBuscarArquivo = Convert.ToInt32(Funcoes.LeParametro(14, "08", false));
            while (i < numeroTentativasParaBuscarArquivo)
            {
                Thread.Sleep(1000);

                Principal.ValidouArquivoDeResposta = ExisteArquivoDeResposta(numeroSequencialDoArquivo);
                if (Principal.ValidouArquivoDeResposta)
                {
                    break;
                }
                i++;
            }

            return Principal.ValidouArquivoDeResposta;
        }

        /// <summary>
        /// Quarta Etapa - Valida a consistencia dos arquivos de envio e o de resposta analisando o parametro
        /// 001-00 dos arquivos
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public static bool ValidaConsistenciaArquivosEnviados(int numeroSequencialDoArquivo)
        {

            string arquivoEnvio = @"" + Funcoes.LeParametro(14, "05", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".log";
            string arquivoResposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";

            var numeroDoArquivoEnvio = File.ReadAllLines(arquivoEnvio)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            var numeroDoArquivoResposta = File.ReadAllLines(arquivoResposta)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            bool validouConsistenciaArquivos = numeroDoArquivoEnvio[0].ToString() == numeroDoArquivoResposta[0].ToString();

            return validouConsistenciaArquivos;
        }

        public static string ValidaConsistenciaMSgERetornaMSg(int numeroSequencialDoArquivo)
        {
            string arquivoEnvio = @"" + Funcoes.LeParametro(14, "05", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".log";
            string arquivoResposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";

            var numeroDoArquivoEnvio = File.ReadAllLines(arquivoEnvio)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            var numeroDoArquivoResposta = File.ReadAllLines(arquivoResposta)
                .Where(l => l.StartsWith("001-000"))
                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                .ToList();

            bool validouConsistenciaArquivos = numeroDoArquivoEnvio[0].ToString() == numeroDoArquivoResposta[0].ToString();

            if (validouConsistenciaArquivos)
            {
                List<RetornoFuncional> parametros = CarregaParametros(arquivoResposta);
                return parametros.SingleOrDefault(p => p.Id == "350-000").Valor;
            }
            else
                return "ERRO";
        }

        public static void VerificaMsg(int numeroSequencialDoArquivo, int codErro)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);

            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            writer.WriteLine("011-000 = 108");
            writer.WriteLine("325-000 = " + codErro);
            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            FuncionalUtil.RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }


        public static string IdentificaMensagemDeErro(int codErro)
        {
            int numeroSequencialDoArquivo = GeraSequenciaArquivoAutorizadorSeq();

            VerificaMsg(numeroSequencialDoArquivo, codErro);

            ValidarArquivoStatus(numeroSequencialDoArquivo);

            if (Principal.ValidouArquivoDeStatus)
            {
                if (!ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                    return "ERRO";
            }
            else
                return "ERRO";

            ValidarArquivoDeResposta(numeroSequencialDoArquivo);
            bool validouArquivoDeResposta = Principal.ValidouArquivoDeResposta;
            if (!validouArquivoDeResposta)
                return "ERRO";

            IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);

            return ValidaConsistenciaMSgERetornaMSg(numeroSequencialDoArquivo);
        }

        #region FUNÇÕES_UTEIS

        /// <summary>
        /// Retorna a lista de parametros do arquivo de retorno quebrado por código em um List
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public static List<RetornoFuncional> CarregaParametros(string arquivo)
        {
            List<RetornoFuncional> lista = new List<RetornoFuncional>();
            string arquivoEnvio = @"" + arquivo;
            TextReader tr = new StreamReader(arquivoEnvio, Encoding.Default);
            string linha = "";
            while ((linha = tr.ReadLine()) != null)
            {
                if (!linha.StartsWith("*")) //parâmetro comentado ?!
                {
                    string[] value = linha.Split('=');
                    lista.Add(new RetornoFuncional() { Id = value[0].Trim(), Valor = value[1].Trim() });
                }
            }

            tr.Close();
            tr.Dispose();
            return lista;


        }


        /// <summary>
        /// Método que incrementa o arquivo AUTORIZADOR.SEQ
        /// </summary>
        /// <param name="numeroAtualSequencialDoArquivo"></param>
        public static void IncrementaSequenciaArquivoAutorizadorSeq(int numeroAtualSequencialDoArquivo)
        {
            //TODO Verificar se precisa ser dinâmico o caminho
            //Caminho do arquivo AUTORIZADOR.SEQ
            string pathArquivoSeq = @"" + Funcoes.LeParametro(14, "09", false) + numeroAtualSequencialDoArquivo.ToString() + "AUTORIZADOR.SEQ";

            string arquivo = Funcoes.LeParametro(14,"09",true) + "AUTORIZADOR.SEQ";
            StreamWriter writer = new StreamWriter(arquivo);
            writer.WriteLine(numeroAtualSequencialDoArquivo.ToString().PadLeft(8, '0'));
            writer.Close();
            writer.Dispose();
        }

        /// <summary>
        /// Renomeia extensão de um arquivo passando o nome atual + nome da extensão desejada
        /// </summary>
        /// <param name="nomeAntigo"></param>
        /// <param name="extensaoDesejada"></param>
        public static void RenomearExtensaoArquivo(string nomeAntigo, string extensaoDesejada)
        {
            System.IO.File.Move(@"" + nomeAntigo,
                @"" + nomeAntigo.Substring(0, nomeAntigo.Length - 4) + "." + extensaoDesejada);
        }

        /// <summary>
        /// Verifica se o arquivo de status .sta existe na pasta tvat/resp/000000.sta 
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        private static bool ExisteArquivoDeStatus(int numeroSequencialDoArquivo)
        {
            string arquivoStatus =
                @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".sta";

            return (File.Exists(arquivoStatus) ? true : false);

        }

        /// <summary>
        /// Verifica se o arquivo de resposta .rsp existe na pasta tvat/resp/000000.rsp
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        private static bool ExisteArquivoDeResposta(int numeroSequencialDoArquivo)
        {
            string arquivoStatus =
                @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";

            return (File.Exists(arquivoStatus) ? true : false);

        }

        /// <summary>
        /// Apaga arquivo de status
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public static bool ApagarArquivoDeStatus(int numeroSequencialDoArquivo)
        {
            string pathArquivoStatus = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".sta";
            if (System.IO.File.Exists(pathArquivoStatus))
            {
                try
                {
                    System.IO.File.Delete(pathArquivoStatus);
                    return true;
                }
                catch (Exception )
                {
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Apaga arquivo de resposta
        /// </summary>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <returns></returns>
        public static bool ApagarArquivoDeResposta(string arquivoDeResposta)
        {
            string pathArquivoStatus = arquivoDeResposta;
            if (System.IO.File.Exists(pathArquivoStatus))
            {
                try
                {
                    System.IO.File.Delete(pathArquivoStatus);
                    return true;
                }
                catch (Exception )
                {
                    return false;
                }
            }
            return false;
        }

        #endregion
    }
}
