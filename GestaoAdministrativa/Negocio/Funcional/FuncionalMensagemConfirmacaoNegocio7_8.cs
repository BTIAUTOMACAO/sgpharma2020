﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalMensagemConfirmacaoNegocio7_8
    {
        public void EnviaMensagemConfirmacao(ClienteCartaoFuncional dadosVenda, int numeroSequencialDoArquivo, PreAutorizacaoFuncionalResponse preAutorizacaoFuncionalResponse, VendaAutorizadaRequest vendaAutorizadaRequest ,ConfirmaVendaResponse confirmaVendaResponse, DataTable listaDeProdutos)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);

            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            double totalVenda = vendaAutorizadaRequest.ValorTotal * 100;
            //double totalVenda = confirmaVendaResponse.ValorDaOperacao;
            writer.WriteLine("003-000 = " + totalVenda.ToString());
            writer.WriteLine("005-000 = 41");
            if (confirmaVendaResponse.CodigoDaResposta == "00")
            {
                writer.WriteLine("009-000 = 00");
            }
            else
            {
                writer.WriteLine("009-000 = 80");
            }

            writer.WriteLine("011-000 = 106");
            
            writer.WriteLine("025-000 = " + confirmaVendaResponse.NSU);
            writer.WriteLine("027-000 = " + confirmaVendaResponse.NSUDoAutorizador);
            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            FuncionalUtil.RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }
    }
}
