﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalComanda
    {
        public string NSU { get; set; }
        public string NSUAutorizador { get; set; }
        public string Cartao { get; set; }
        public string Conselho { get; set; }
        public string CodConselho { get; set; }
        public DateTime DtVenda { get; set; }
        public string  Hora{ get; set; }
        public double TotalAPagar { get; set; }
        public double TotalDaVenda { get; set; }
        public double ValorDebitarCartao { get; set; }
        public double ValorDebitarAVista { get; set; }
        public double Desconto { get; set; }
        public string TipoTransacao { get; set; }
        public string Nome { get; set; }

        public FuncionalComanda()
        {

        }
        
        public FuncionalComanda(string nSU, string cartao, DateTime dtVenda, double totalAPagar, double valorDebitarCartao, double valorDebitarAVista, double desconto, string tipoTransacao,string nsuAutorizador,string hora, string conselho, string codConselho, double totalDaVenda, string nome)
        {
            NSU = nSU;
            Cartao = cartao;
            DtVenda = dtVenda;
            TotalAPagar = totalAPagar;
            ValorDebitarCartao = valorDebitarCartao;
            ValorDebitarAVista = valorDebitarAVista;
            TipoTransacao = tipoTransacao;
            Desconto = desconto;
            NSUAutorizador = nsuAutorizador;
            Hora = hora;
            Conselho = conselho;
            CodConselho = codConselho;
            TotalDaVenda = totalDaVenda;
            Nome = nome;
        }

        public bool InsereRegistros(FuncionalComanda dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_FUNCIONAL_COMANDA(NSU,CARTAO, DATA, TOTAL_A_PAGAR, DESCONTO, VALOR_CARTAO, VALOR_AVISTA, TIPO_TRANSACAO, CONSELHO, COD_CONSELHO, HORA, TOTAL_VENDA, NSU_AUTORIZADOR, NOME ) VALUES ("
                + "'" +dados.NSU + "','"
                + dados.Cartao + "',"
                + Funcoes.BDataHora(dados.DtVenda) + ","
                + Funcoes.BValor(dados.TotalAPagar) + ","
                + Funcoes.BValor(dados.Desconto) + ","
                + Funcoes.BValor(dados.ValorDebitarCartao) + ","
                + Funcoes.BValor(dados.ValorDebitarAVista) + ",'"
                + dados.TipoTransacao + "','"
                + dados.Conselho + "','"
                + dados.CodConselho + "','"
                + dados.Hora + "',"
                + Funcoes.BValor(dados.TotalDaVenda) + ","
                + dados.NSUAutorizador + ",'"
                + dados.Nome + "'"
                + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDadosComandaFuncional(string nsu)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM beneficio_funcional_comanda  "
                + "WHERE NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluirDadosPorNSU(string nsu, bool transAberta = false)
        {
            string strCmd = "DELETE FROM BENEFICIO_FUNCIONAL_COMANDA WHERE NSU = '" + nsu + "'";

            if (!transAberta)
                return BancoDados.ExecuteNoQuery(strCmd, null);
            else
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }
    }
}
