﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Funcional
{
    public class FuncionalConfirmaVendaNegocio
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dadosVenda"></param>
        /// <param name="numeroSequencialDoArquivo"></param>
        /// <param name="listaDeProdutos"></param>
        /// <param name="preAutorizacaoFuncionalResponse"></param>
        public void ConfirmarVendaEnv(ClienteCartaoFuncional dadosVenda, int numeroSequencialDoArquivo, PreAutorizacaoFuncionalResponse preAutorizacaoFuncionalResponse, DataTable listaDeProdutos)
        {
            string codigoDeProcessamento = "";
            codigoDeProcessamento = numeroSequencialDoArquivo.ToString();

            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "05", false) + codigoDeProcessamento.PadLeft(8, '0') + ".tmp";

            StreamWriter writer =
                new StreamWriter(ArquivoTemporarioDeEnvio);

            writer.WriteLine("000-000 = CRT");
            writer.WriteLine("001-000 = " + codigoDeProcessamento.PadLeft(8, '0'));
            writer.WriteLine("002-000 = " + "0".PadLeft (8, '0'));
            //double totalVenda = Convert.ToDouble(preAutorizacaoFuncionalResponse.ProdutosPreAutorizacao.Sum(x => Convert.ToDecimal(x.PrecoVenda)*100));
            double totalVenda = Convert.ToDouble(listaDeProdutos.Compute("SUM(PROD_TOTAL)", "")) * 100;
            writer.WriteLine("003-000 = " + totalVenda.ToString());
            writer.WriteLine("011-000 = 41");
            writer.WriteLine("098-000 = " + dadosVenda.Trilha2DoCartao);
            writer.WriteLine("300-000 = " + preAutorizacaoFuncionalResponse.CodigoPreAutorizacao);
            writer.WriteLine("304-000 = " + preAutorizacaoFuncionalResponse.ValorPagarAVista);
            writer.WriteLine("312-000 = " + preAutorizacaoFuncionalResponse.QtdMedicamentos);
            for (int i = 0; i < Convert.ToInt32(preAutorizacaoFuncionalResponse.QtdMedicamentos); i++)
            {
                writer.WriteLine("313-00" + (i + 1) + " = " + dadosVenda.DataDaTransacao);
                //todo Validar com a funcional se o param 314 é sempre 0
                writer.WriteLine("314-00" + (i + 1) + " = 0");
                writer.WriteLine("315-00" + (i + 1) + " = " + dadosVenda.CodConselho.PadLeft(10, '0'));
                writer.WriteLine("316-00" + (i + 1) + " = " + dadosVenda.UF.PadLeft(2, '0'));
                //todo Validar com a funcional se o param 317 é sempre 0
                writer.WriteLine("317-00" + (i + 1) + " = 01");
                writer.WriteLine("318-00" + (i + 1) + " = " + listaDeProdutos.Rows[i]["COD_BARRA"].ToString());
                writer.WriteLine("321-00" + (i + 1) + " = " + (Convert.ToDouble(listaDeProdutos.Rows[i]["PRECO_MAXIMO"]) * 100).ToString());
                writer.WriteLine("323-00" + (i + 1) + " = " + (Convert.ToDouble(listaDeProdutos.Rows[i]["PRE_VALOR"]) * 100).ToString());
                writer.WriteLine("326-00" + (i + 1) + " = 1");
                writer.WriteLine("327-00" + (i + 1) + " = " + listaDeProdutos.Rows[i]["PROD_QTDE"].ToString());
            }
           
            writer.WriteLine("999-999 = ");
            writer.Close();
            writer.Dispose();

            FuncionalUtil.RenomearExtensaoArquivo(ArquivoTemporarioDeEnvio, "env");
        }

        public ConfirmaVendaResponse ConfirmaVendaRsp(int numeroSequencialDoArquivo, string temReceitaMedica)
        {
            string ArquivoTemporarioDeEnvio = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
            List<RetornoFuncional> parametros = FuncionalUtil.CarregaParametros(ArquivoTemporarioDeEnvio);

            ConfirmaVendaResponse objConfirmaVendaResponse = new ConfirmaVendaResponse();
            List<ProdutoPreAutorizacao> lista = new List<ProdutoPreAutorizacao>();
            ProdutoPreAutorizacao objProdutoPreAutorizacao;


            objConfirmaVendaResponse.Header = parametros.SingleOrDefault(p => p.Id == "000-000").Valor;
            objConfirmaVendaResponse.Identificacao = parametros.SingleOrDefault(p => p.Id == "001-000").Valor;
            objConfirmaVendaResponse.ValorDaOperacao = parametros.SingleOrDefault(p => p.Id == "003-000").Valor;
            objConfirmaVendaResponse.CodigoDaResposta = parametros.SingleOrDefault(p => p.Id == "009-000").Valor;
            if (Convert.ToInt32(objConfirmaVendaResponse.CodigoDaResposta) == 0)
            {
                objConfirmaVendaResponse.NumeroCupomFiscal = parametros.SingleOrDefault(p => p.Id == "002-000").Valor;
            }
            objConfirmaVendaResponse.NSU = parametros.SingleOrDefault(p => p.Id == "012-000").Valor;
            objConfirmaVendaResponse.NSUDoAutorizador = parametros.SingleOrDefault(p => p.Id == "013-000").Valor;
            objConfirmaVendaResponse.DataTransacao = parametros.SingleOrDefault(p => p.Id == "022-000").Valor;
            objConfirmaVendaResponse.HoraTransacao = parametros.SingleOrDefault(p => p.Id == "023-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "030-000") == null))
            {
                objConfirmaVendaResponse.TextoParaOperador = parametros.SingleOrDefault(p => p.Id == "030-000").Valor;
            }
            objConfirmaVendaResponse.Autenticacao = parametros.SingleOrDefault(p => p.Id == "032-000").Valor;
            objConfirmaVendaResponse.SaldoCartao = parametros.SingleOrDefault(p => p.Id == "097-000").Valor;
            objConfirmaVendaResponse.NomeDoPortador = parametros.SingleOrDefault(p => p.Id == "100-000").Valor;
            if (!(parametros.SingleOrDefault(p => p.Id == "300-000") == null))
            {
                objConfirmaVendaResponse.CodigoPreAutorizacao = parametros.SingleOrDefault(p => p.Id == "300-000").Valor;
            }
            //PREENCHE CAMPOS OPCIONAIS QUE SERÃO RETORNADOS APENAS SE A TRANSAÇÃO FOI APROVADA
            if (objConfirmaVendaResponse.CodigoDaResposta == "00" || objConfirmaVendaResponse.CodigoDaResposta == "26" || objConfirmaVendaResponse.CodigoDaResposta == "27"
                || objConfirmaVendaResponse.CodigoDaResposta == "28")
            {
                objConfirmaVendaResponse.ValorPagarAVista = parametros.SingleOrDefault(p => p.Id == "304-000").Valor;
                objConfirmaVendaResponse.QtdMedicamentos = parametros.SingleOrDefault(p => p.Id == "312-000").Valor;

                int qtdMedicamentos = Convert.ToInt32(objConfirmaVendaResponse.QtdMedicamentos);

                if (temReceitaMedica.Equals("S"))
                    objConfirmaVendaResponse.VendaComReceita = "S";
                else
                    objConfirmaVendaResponse.VendaComReceita = "N";

                //PREENCHENDO OS PARAMETROS 313 A 328
                for (int i = 0; i < qtdMedicamentos; i++)
                {
                    objProdutoPreAutorizacao = new ProdutoPreAutorizacao();
                    if (temReceitaMedica.Equals("S"))
                    {
                        objConfirmaVendaResponse.VendaComReceita = "S";
                        objProdutoPreAutorizacao.DataReceita = parametros.SingleOrDefault(p => p.Id == "313-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.TipoConselho = parametros.SingleOrDefault(p => p.Id == "314-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.CodConselho = parametros.SingleOrDefault(p => p.Id == "315-00" + (i + 1)).Valor;
                        objProdutoPreAutorizacao.UFConselho = parametros.SingleOrDefault(p => p.Id == "316-00" + (i + 1)).Valor;
                    }
                    else
                    {
                        objConfirmaVendaResponse.VendaComReceita = "N";
                        objProdutoPreAutorizacao.DataReceita = "0";
                        objProdutoPreAutorizacao.TipoConselho = "0";
                        objProdutoPreAutorizacao.CodConselho = "0";
                        objProdutoPreAutorizacao.UFConselho = "0";
                    }

                    objProdutoPreAutorizacao.TipoProduto = parametros.SingleOrDefault(p => p.Id == "317-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.CodBarras = parametros.SingleOrDefault(p => p.Id == "318-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.DescricaoMedicamento = parametros.SingleOrDefault(p => p.Id == "319-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.CondicaoVenda = parametros.SingleOrDefault(p => p.Id == "320-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoMaximo = parametros.SingleOrDefault(p => p.Id == "321-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoPBM = parametros.SingleOrDefault(p => p.Id == "322-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.PrecoVenda = parametros.SingleOrDefault(p => p.Id == "324-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.StatusItem = parametros.SingleOrDefault(p => p.Id == "325-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.QuantidadeReceitada = parametros.SingleOrDefault(p => p.Id == "326-00" + (i + 1)).Valor;
                    objProdutoPreAutorizacao.QuantidadeAutorizada = parametros.SingleOrDefault(p => p.Id == "327-00" + (i + 1)).Valor;
                    if (!(parametros.SingleOrDefault(p => p.Id == "328-00" + (i + 1)) == null))
                    {
                        objProdutoPreAutorizacao.Referencia = parametros.SingleOrDefault(p => p.Id == "328-00" + (i + 1)).Valor;
                    }
                    else
                    {
                        objProdutoPreAutorizacao.Referencia = "0";
                    }

                    lista.Add(objProdutoPreAutorizacao);
                }
            }
            objConfirmaVendaResponse.ProdutosPreAutorizacao = lista;
            objConfirmaVendaResponse.NomeDoPortador = parametros.SingleOrDefault(p => p.Id == "370-000").Valor;

            return objConfirmaVendaResponse;
        }

    }
}
