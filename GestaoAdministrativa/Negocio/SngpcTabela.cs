﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcTabela
    {
        public int TabSeq { get; set; }
        public int TabTipo { get; set; }
        public string TabCodigo { get; set; }
        public string TabDescr { get; set; }
        public string TabLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public SngpcTabela() { }

        public SngpcTabela(int tabSeq, int tabTipo, string tabCodigo, string tabDescr, string tabLiberado, DateTime dtAlteracao,
                           int opAlteracao, DateTime dtCadastro, int opCadastro)
        {

            this.TabSeq = tabSeq;
            this.TabTipo = tabTipo;
            this.TabCodigo = tabCodigo;
            this.TabDescr = tabDescr;
            this.TabLiberado = tabLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscarDados(SngpcTabela dados, out string strOrdem)
        {
            string sql = " SELECT TAB_TIPO, TAB_CODIGO, TAB_SEQ, TAB_DESCRICAO, DAT_ALTERACAO, DT_CADASTRO, OP_CADASTRO,"
                       + " CASE TAB_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS TAB_DESABILITADO, "
                       + " OP_ALTERACAO FROM SNGPC_TABELAS WHERE 1=1 ";

            if (dados.TabLiberado != "TODOS")
            {
                string desab = dados.TabLiberado == "SIM" ? "N" : "S";
                sql += "AND TAB_DESABILITADO = '" + desab + "'";
            }
            if (dados.TabSeq != 0)
            {

                sql += "AND TAB_SEQ = " + dados.TabSeq;
            }
            if (dados.TabTipo != 0)
            {

                sql += "AND TAB_TIPO = " + dados.TabTipo;
            }
            strOrdem = sql;
            sql += "ORDER BY TAB_SEQ ";


            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(SngpcTabela dadosNovos, DataTable dadosAntigos)
        {
            string sql = "UPDATE SNGPC_TABELAS SET "
                       + " TAB_TIPO = " + dadosNovos.TabTipo
                       + " ,TAB_CODIGO = '" + dadosNovos.TabCodigo + "'"
                       + " ,TAB_DESCRICAO = '" + dadosNovos.TabDescr + "'"
                       + " ,TAB_DESABILITADO = '" + dadosNovos.TabLiberado + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                       + " ,OP_ALTERACAO ='" + Principal.usuario.ToUpper() + "'"
                       + "WHERE TAB_SEQ = " + dadosNovos.TabSeq;


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                if (!dadosNovos.TabTipo.Equals(dadosAntigos.Rows[0]["TAB_TIPO"]))
                {
                    dados[contMatriz, 0] = "TAB_TIPO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_TIPO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabTipo + "'";
                    contMatriz = contMatriz + 1;
                }

                if (!dadosNovos.TabCodigo.Equals(dadosAntigos.Rows[0]["TAB_CODIGO"]))
                {
                    dados[contMatriz, 0] = "TAB_CODIGO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_CODIGO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TabDescr.Equals(dadosAntigos.Rows[0]["TAB_DESCRICAO"]))
                {
                    dados[contMatriz, 0] = "TAB_DESCRICAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_DESCRICAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TabLiberado.Equals(dadosAntigos.Rows[0]["TAB_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "TAB_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                Funcoes.GravaLogAlteracao("TAB_SEQ", dadosNovos.TabSeq.ToString(), Principal.usuario.ToUpper(), "SNGPC_TABELAS", dados, contMatriz, Principal.estAtual);
                return true;
            }
            {
                return false;
            }

        }

        public bool InsereDados(SngpcTabela dados)
        {
            string sql = " INSERT INTO SNGPC_TABELAS (TAB_SEQ, TAB_TIPO, TAB_CODIGO, TAB_DESCRICAO, TAB_DESABILITADO,"
                       + " DT_CADASTRO, OP_CADASTRO ) VALUES ("
                       + dados.TabSeq + ","
                       + dados.TabTipo + ",'"
                       + dados.TabCodigo + "','"
                       + dados.TabDescr + "','"
                       + dados.TabLiberado + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario.ToUpper() + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("TAB_SEQ", dados.TabSeq.ToString(), Principal.usuario.ToUpper(), "SNGPC_TABELAS", dados.TabDescr, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ExcluirDados(int tabelaSeq)
        {
            string sql = "DELETE FROM SNGPC_TABELAS WHERE TAB_SEQ = " + tabelaSeq ;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public DataTable BuscaTodasTabelas()
        {
            string sql = "SELECT TAB_SEQ, TAB_DESCRICAO FROM SNGPC_TABELAS WHERE TAB_DESABILITADO = 'N'  ORDER BY TAB_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaTabelaEntrTrans()
        {
            string sql = "SELECT TAB_SEQ , TAB_DESCRICAO FROM SNGPC_TABELAS WHERE TAB_TIPO = 3 AND TAB_CODIGO IN(1,2)";

            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BuscaTabelaPorTipo(int tipoTabela)
        {

            string sql = "SELECT TAB_SEQ, TAB_DESCRICAO, TAB_CODIGO FROM SNGPC_TABELAS WHERE TAB_TIPO = " + tipoTabela + " ORDER BY TAB_CODIGO";

            return BancoDados.GetDataTable(sql, null);
        }

        public string BuscaTabCodigo(string tabSeq)
        {
            string sql = "SELECT TAB_CODIGO FROM SNGPC_TABELAS WHERE TAB_SEQ = " + tabSeq;

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }
    }
}
