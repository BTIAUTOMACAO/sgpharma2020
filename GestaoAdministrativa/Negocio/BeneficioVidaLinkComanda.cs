﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioVidaLinkComanda
    {
        public long VendaID { get; set; }
        public string NSU { get; set; }
        public string NumSeq { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public BeneficioVidaLinkComanda() { }

        public BeneficioVidaLinkComanda(long vendaID, string nsu, string numSeq, DateTime dtCadastro, string opCadastro)
        {
            this.VendaID = vendaID;
            this.NSU = nsu;
            this.NumSeq = numSeq;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public DataTable BuscaRegistrosDeComanda()
        {
            string sql = "SELECT * FROM BENEFICIO_VIDA_LINK_COMANDA";

            return BancoDados.GetDataTable(sql, null);
        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID = 0, bool transAberta = false)
        {
            string strCmd = "DELETE FROM BENEFICIO_VIDA_LINK_COMANDA WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                strCmd += " AND VENDA_ID = " + vendaID;
            }
            if (!transAberta)
                return BancoDados.ExecuteNoQuery(strCmd, null);
            else
                return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }

        public DataTable IdentificaSeVendaEPorComanda(string nsu, long vendaID =0)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM BENEFICIO_VIDA_LINK_COMANDA "
                + "WHERE NSU = '" + nsu + "'";
            if(vendaID != 0)
            {
                strSql += " AND VENDA_ID = " + vendaID;
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(BeneficioVidaLinkComanda dados)
        {
            string strCmd = "INSERT INTO BENEFICIO_VIDA_LINK_COMANDA(VENDA_ID, NSU,NUM_SEQ, DTCADASTRO, OPCADASTRO) VALUES ("
                + dados.VendaID + ",'"
                + dados.NSU + "',"
                + dados.NumSeq + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaComandaPorVendaID(long vendaID)
        {
            string strSql;

            strSql = "SELECT * "
                + "FROM BENEFICIO_VIDA_LINK_COMANDA "
                + "WHERE  VENDA_ID = " + vendaID;
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public int ExcluirComandaPorNSU(string nsu)
        {
            string strCmd = "DELETE FROM BENEFICIO_VIDA_LINK_COMANDA WHERE NSU = '" + nsu + "'";
            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
