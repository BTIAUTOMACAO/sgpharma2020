﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class GrupoDeCliente
    {
        public int GrupoCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public string GrupoDescr { get; set; }
        public string GrupoLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public GrupoDeCliente() { }

        public GrupoDeCliente(int grupoCodigo, int empCodigo, string grupoDescr, string grupoLiberado, DateTime dtAlteracao, string opAlteracao,
            DateTime dtCadastro, string opCadastro)
        {
            this.GrupoCodigo = grupoCodigo;
            this.EmpCodigo = empCodigo;
            this.GrupoDescr = grupoDescr;
            this.GrupoLiberado = grupoLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public DataTable BuscarDados(int grupoCodigo, int empCodigo, string grupoDescr, string grupoLiberado, out string strOrdem)
        {
            string strSql = "SELECT A.EMP_CODIGO, A.GRUPO_CODIGO, A.GRUPO_DESCR, CASE WHEN A.GRUPO_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS GRUPO_DESABILITADO, A.DTALTERACAO, A.OPALTERACAO,"
                       + " A.DTCADASTRO, A.OPCADASTRO FROM GRUPO_CLIENTES A  WHERE A.EMP_CODIGO = " + empCodigo;
            if (grupoCodigo == 0 && grupoDescr == "" && grupoLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_CODIGO";
            }
            else
            {
                if (grupoCodigo != 0)
                {
                    strSql += " AND A.GRUPO_CODIGO = " + grupoCodigo;
                }
                if (grupoDescr != "")
                {
                    strSql += " AND A.GRUPO_DESCR LIKE '%" + grupoDescr + "%'";
                }
                if (grupoLiberado != "TODOS")
                {
                    grupoLiberado = grupoLiberado == "N" ? "S" : "N";
                    strSql += " AND A.GRUPO_DESABILITADO = '" + grupoLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(GrupoDeCliente dados)
        {
            string strCmd = "INSERT INTO GRUPO_CLIENTES (GRUPO_CODIGO, EMP_CODIGO, GRUPO_DESCR, GRUPO_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.GrupoCodigo + ","
                + dados.EmpCodigo + ",'"
                + dados.GrupoDescr + "','"
                + dados.GrupoLiberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(GrupoDeCliente dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE GRUPO_CLIENTES SET ";

            if (!dadosNew.GrupoDescr.Equals(dadosOld.Rows[0]["GRUPO_DESCR"]))
            {
                //VERIFICA SE GRUPO JA ESTA CADASTRADO//
                if (Util.RegistrosPorEmpresa("GRUPO_CLIENTES", "GRUPO_DESCR", dadosNew.GrupoDescr, true, true).Rows.Count != 0)
                {
                    Principal.mensagem = "Grupo de Cliente já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " GRUPO_DESCR = '" + dadosNew.GrupoDescr + "',";

                dados[contMatriz, 0] = "GRUPO_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.GrupoLiberado.Equals(dadosOld.Rows[0]["GRUPO_DESABILITADO"]))
            {
                strCmd += " GRUPO_DESABILITADO = '" + dadosNew.GrupoLiberado + "',";

                dados[contMatriz, 0] = "GRUPO_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and GRUPO_CODIGO = " + dadosNew.GrupoCodigo;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("GRUPO_CODIGO", dadosNew.GrupoCodigo.ToString(), Principal.usuario, "GRUPO_CLIENTES", dados, contMatriz, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(string grupCodigo, int empCodigo)
        {
            string strCmd = "DELETE FROM GRUPO_CLIENTES WHERE GRUPO_CODIGO = '" + grupCodigo + "' AND EMP_CODIGO = " + empCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }
    }
}
