﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    public class EntradaDetalhe
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public int EntId { get; set; }
        public string CfDocto { get; set; }
        public string EntDocto { get; set; }
        public string EntSerie { get; set; }
        public int EntParcela { get; set; }
        public double EntValParc { get; set; }
        public DateTime EntVencto { get; set; }
        public string PagDocto { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public EntradaDetalhe() { }

        public EntradaDetalhe(int estCodigo, int empCodigo, int entId, string cfDocto, string entDocto, string entSerie, int entParcela, double entValParc, DateTime entVencto, string pagDocto,
            DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.EntId = entId;
            this.CfDocto = cfDocto;
            this.EntDocto = entDocto;
            this.EntSerie = entSerie;
            this.EntParcela = entParcela;
            this.EntValParc = entValParc;
            this.EntVencto = entVencto;
            this.PagDocto = pagDocto;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }


        public List<EntradaDetalhe> CarregaEntradasDetalhe(int estCodigo, int empCodigo, int entradaID)
        {
            List<EntradaDetalhe> listaEntradaDetalhe = new List<EntradaDetalhe>();

            using (DataTable table = BancoDados.GetDataTable("SELECT * FROM ENTRADA_DETALHES WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND ENT_ID = " + entradaID, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    EntradaDetalhe dEntradaDetalhe = new EntradaDetalhe();
                    dEntradaDetalhe.EmpCodigo = Convert.ToInt32(row["EMP_CODIGO"]);
                    dEntradaDetalhe.EstCodigo = Convert.ToInt32(row["EST_CODIGO"]);
                    dEntradaDetalhe.EntId = Convert.ToInt32(row["ENT_ID"]);
                    dEntradaDetalhe.CfDocto = row["CF_DOCTO"].ToString();
                    dEntradaDetalhe.EntDocto = row["ENT_DOCTO"].ToString();
                    dEntradaDetalhe.EntSerie = row["ENT_SERIE"].ToString();
                    dEntradaDetalhe.EntParcela = Convert.ToInt32(row["ENT_PARCELA"]);
                    dEntradaDetalhe.EntValParc = Convert.ToDouble(row["ENT_VALPARC"]);
                    dEntradaDetalhe.EntVencto = Convert.ToDateTime(row["ENT_VENCTO"]);
                    dEntradaDetalhe.PagDocto = row["PAG_DOCTO"].ToString();
                    dEntradaDetalhe.DtCadastro = Convert.ToDateTime(row["DTCADASTRO"]);
                    dEntradaDetalhe.OpCadastro = row["OPCADASTRO"].ToString();
                    listaEntradaDetalhe.Add(dEntradaDetalhe);
                }
            }

            return listaEntradaDetalhe;
        }

        public DataTable GetBuscaParcelas(int estCodigo, int empCodigo, string entDocto)
        {
            var dt = new DataTable();
            string strSql = "SELECT ENT_VALPARC, ENT_VENCTO FROM ENTRADA_DETALHES WHERE EST_CODIGO = " + estCodigo
                + " AND EMP_CODIGO = " + empCodigo + " AND ENT_DOCTO = '" + entDocto + "' ORDER BY ENT_PARCELA";
            return BancoDados.GetDataTable(strSql, null);
        }


        public bool InsereRegistrosEntradaDetalhe(EntradaDetalhe entradaDetalhe)
        {
            string strCmd = "INSERT INTO ENTRADA_DETALHES (EST_CODIGO, EMP_CODIGO, ENT_ID, CF_DOCTO, ENT_DOCTO, ENT_SERIE, ENT_PARCELA, ENT_VALPARC, "
                        + "ENT_VENCTO, PAG_DOCTO, DTCADASTRO, OPCADASTRO) VALUES ("
                        + entradaDetalhe.EstCodigo + ","
                        + entradaDetalhe.EmpCodigo + ","
                        + entradaDetalhe.EntId + ",'"
                        + entradaDetalhe.CfDocto + "','"
                        + entradaDetalhe.EntDocto + "','"
                        + entradaDetalhe.EntSerie + "',"
                        + entradaDetalhe.EntParcela + ","
                        + Funcoes.BValor(entradaDetalhe.EntValParc) + ","
                        + Funcoes.BData(entradaDetalhe.EntVencto) + ",'"
                        + entradaDetalhe.PagDocto + "',"
                        + Funcoes.BDataHora(entradaDetalhe.DtCadastro) + ",'"
                        + entradaDetalhe.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }


        public string IdentificaFaturaLancada(EntradaDetalhe dadosBusca)
        {
            string strSql = "SELECT ENT_ID FROM ENTRADA_DETALHES WHERE EST_CODIGO = " + dadosBusca.EstCodigo + " AND EMP_CODIGO = " + dadosBusca.EmpCodigo
                + " AND CF_DOCTO = '" + dadosBusca.CfDocto + "' AND ENT_ID = " + dadosBusca.EntId + " AND PAG_DOCTO IS NOT NULL ";
            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public bool ExcluiParcelasEntradaNotas(int estCodigo, int empCodigo, int entId)
        {
            string strCmd = "DELETE FROM ENTRADA_DETALHES WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + "  AND ENT_ID = " + entId;
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable RelatorioBoletosFornecedores(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int tipo)
        {
            string strSql = "select ";
            if (tipo == 0)
            {
                strSql += "   a.ent_dtemis as filtro,";
            }
            else if (tipo == 1)
            {
                strSql += "   a.ent_dtlanc as filtro,";
            }
            else if (tipo == 2)
            {
                strSql += "   f.ent_vencto as filtro,";
            }

            strSql += " a.ent_dtlanc,"
                        + "   a.ent_dtemis,"
                        + "   f.ent_vencto,"
                        + "   b.cf_nome,"
                        + "   a.ent_docto,"
                        + "   a.ent_serie,"
                        + "   a.pag_docto,"
                        + "   f.ent_valparc"
                        + "   from entrada_detalhes f, colaboradores c, clifor b, entrada a"
                        + "   where a.emp_codigo = " + empCodigo
                        + "   and a.est_codigo = " + estCodigo;
            if (tipo == 0)
            {
                strSql += "   and a.ent_dtemis ";
            }
            else if (tipo == 1)
            {
                strSql += "   and a.ent_dtlanc ";
            }
            else if (tipo == 2)
            {
                strSql += "   and f.ent_vencto ";
            }

            strSql += "between " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " and "
                    + Funcoes.BData(Convert.ToDateTime(dtFinal))
                    + "   and a.cf_docto = b.cf_docto"
                    + "   and c.Emp_codigo =  a.emp_codigo"
                    + "   and a.pag_comprador = c.col_codigo"
                    + "   and a.emp_codigo = f.emp_codigo"
                    + "   and a.est_codigo = f.est_codigo"
                    + "   and a.cf_docto = f.cf_docto"
                    + "   and a.ent_docto = f.ent_docto"
                    + "   and a.ent_serie = f.ent_serie"
                    + "   group by  a.ent_dtlanc,"
                    + "       a.ent_dtemis,"
                    + "      f.ent_vencto,"
                    + "       b.cf_nome,"
                    + "       a.ent_docto,"
                    + "      a.ent_serie,"
                    + "       a.pag_docto,"
                    + "       f.ent_valparc"
                    + " order by ";

            if (tipo == 0)
            {
                strSql += "   a.ent_dtemis ";
            }
            else if (tipo == 1)
            {
                strSql += "   a.ent_dtlanc ";
            }
            else if (tipo == 2)
            {
                strSql += "   f.ent_vencto ";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable RelatorioBoletosFornecedoresSintetico(int estCodigo, int empCodigo, string dtInicial, string dtFinal)
        {
            string strSql = "select f.ent_vencto as vencimento, sum(f.ent_valparc) as total "
                        + "   from entrada_detalhes f, entrada a"
                        + "   where a.emp_codigo = " + empCodigo
                        + "   and a.est_codigo = " + estCodigo
                        + "   and f.ent_vencto ";
            strSql += "between " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " and "
                    + Funcoes.BData(Convert.ToDateTime(dtFinal))
                    + "   and a.emp_codigo = f.emp_codigo"
                    + "   and a.est_codigo = f.est_codigo"
                    + "   and a.cf_docto = f.cf_docto"
                    + "   and a.ent_docto = f.ent_docto"
                    + "   and a.ent_serie = f.ent_serie"
                    + "   group by  f.ent_vencto"
                    + " order by f.ent_vencto ";
            
            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
