﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class MovCaixaFilial
    {
        public string EspDescricao { get; set; }
        public double Valor { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
    }
}
