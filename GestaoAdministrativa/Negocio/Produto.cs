﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;
using SqlNegocio;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio
{
    public class Produto
    {
        public int ProdID { get; set; }
        public string ProdCodBarras { get; set; }
        public string ProdDescr { get; set; }
        public string ProdDescAbrev { get; set; }
        public string ProdUnidade { get; set; }
        public string ProdTipo { get; set; }
        public string ProdAbcFarma { get; set; }
        public string ProdUsoContinuo { get; set; }
        public string ProdFormula { get; set; }
        public int EspCodigo { get; set; }
        public int PrinCodigo { get; set; }
        public string ProdRegistroMS { get; set; }
        public string ProdControlado { get; set; }
        public string ProdMensagem { get; set; }
        public string ProdNCM { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string TipoReceita { get; set; }
        public string ProdClasTera { get; set; }
        public string ProdCaixaCartelado { get; set; }
        public string ProdCodigoCartelado { get; set; }
        public string ProdPortaria { get; set; }

        public Produto() { }

        public Produto(int prodId, string prodCodBarras, string prodDescr, string ProdDescrAbrev, string prodUnidade,
            string prodTipo, string prodABCFarma, string prodUsoContinuo, string prodFormula, string bloquearCompra,
            int espCodigo, int prinCodigo, string prodRegistroMs, string prodControlado, string prodMensagem,
            string prodNcm, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro, string tipoReceita,
            string prodClasTera, string prodCaixaCartelado, string prodCodigoCartelado, string prodPortaria)
        {
            this.ProdID = prodId;
            this.ProdCodBarras = prodCodBarras;
            this.ProdDescr = prodDescr;
            this.ProdDescAbrev = ProdDescrAbrev;
            this.ProdUnidade = prodUnidade;
            this.ProdTipo = prodTipo;
            this.ProdAbcFarma = prodABCFarma;
            this.ProdUsoContinuo = prodUsoContinuo;
            this.ProdFormula = prodFormula;
            this.EspCodigo = espCodigo;
            this.PrinCodigo = prinCodigo;
            this.ProdRegistroMS = prodRegistroMs;
            this.ProdControlado = prodControlado;
            this.ProdMensagem = prodMensagem;
            this.ProdNCM = prodNcm;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.TipoReceita = tipoReceita;
            this.ProdClasTera = prodClasTera;
            this.ProdCaixaCartelado = prodCaixaCartelado;
            this.ProdCodigoCartelado = prodCodigoCartelado;
            this.ProdPortaria = prodPortaria;
        }

        //BUSCA TODOS OS PRODUTOS 
        public DataTable BuscarDados(string prodDescr, string prodCodigo, int departamento, int classe, string liberado, int subClasse,
             int fabricante, int principioAtivo, string registroMS, out bool todos, out string strOrdem)
        {
            string strSql = "SELECT B.EST_CODIGO, B.EMP_CODIGO, A.PROD_ID, A.PROD_CODIGO, A.PROD_DESCR, A.PROD_ABREV, A.PROD_UNIDADE,  A.PROD_MENSAGEM, "
                          + " A.PROD_USO_CONTINUO, A.PROD_CONTROLADO, A.PROD_FORMULA, A.NCM, CASE WHEN B.PROD_SITUACAO = 'A' THEN 'S' ELSE 'N' END AS PROD_SITUACAO,"
                          + " A.DAT_ALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO "
                          + " FROM PRODUTOS A"
                          + " INNER JOIN PRODUTOS_DETALHE B ON (A.PROD_CODIGO = B.PROD_CODIGO AND A.PROD_ID = A.PROD_ID) "
                          + " WHERE B.EMP_CODIGO = " + Principal.empAtual
                          + " AND B.EST_CODIGO = " + Principal.estAtual
                          + " AND A.PROD_CODIGO = B.PROD_CODIGO";

            //BUSCA SEM NENHUM FILTRO//
            if (prodDescr == "" && prodCodigo == "" && departamento == 0 && classe == 0 && liberado == "TODOS"
                && subClasse == 0 && fabricante == 0 && principioAtivo == 0 && registroMS == "")
            {
                if (MessageBox.Show("Está solicitando uma busca sem passar por nenhum filtro\neste processo pode demorar dependendo da quantidade de\nregistros encontados.\nConfirma Solicitação?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    strOrdem = strSql;
                    strSql += " ORDER BY A.PROD_CODIGO";
                    todos = true;
                    return BancoDados.GetDataTable(strSql, null);
                }
                else
                {
                    DataTable dt = new DataTable();
                    strOrdem = "";
                    todos = false;
                    return dt;
                }

            }
            else
            {
                if (prodDescr != "")
                {
                    strSql += " AND A.PROD_DESCR LIKE '%" + prodDescr + "%'";
                }
                if (prodCodigo != "")
                {
                    strSql += " AND A.PROD_CODIGO = '" + prodCodigo + "'";
                }
                if (departamento != 0)
                {
                    strSql += " AND B.DEP_CODIGO = " + departamento;
                }
                if (classe != 0)
                {
                    strSql += " AND B.CLAS_CODIGO = " + classe;
                }
                if (subClasse != 0)
                {
                    strSql += " AND B.SUB_CODIGO = " + subClasse;
                }
                if (fabricante != 0)
                {
                    strSql += " AND B.FAB_CODIGO = " + fabricante;
                }
                if (principioAtivo != 0)
                {
                    strSql += " AND A.PRI_CODIGO = " + principioAtivo;
                }
                if (registroMS != "")
                {
                    strSql += " AND A.PROD_REGISTRO_MS = '" + registroMS + "'";
                }
                if (liberado != "TODOS")
                {
                    liberado = liberado == "S" ? "A" : "I";
                    strSql += " AND B.PROD_SITUACAO = '" + liberado + "'";
                }
                strOrdem = strSql;
                todos = true;
                strSql += " ORDER BY A.PROD_ID";

                return BancoDados.GetDataTable(strSql, null);
            }
        }

        // INSERE DADOS NOVOS TELA CADASTRO

        public bool InsereProdutosNovosCadastro(Produto dados)
        {

            string sql = " INSERT INTO PRODUTOS (PROD_ID, PROD_CODIGO, PROD_DESCR, PROD_ABREV, PROD_UNIDADE, PROD_TIPO, PROD_ABC_FARMA, "
                       + " PROD_USO_CONTINUO, PROD_FORMULA, ESP_CODIGO, PRI_CODIGO, PROD_REGISTRO_MS, PROD_CONTROLADO, "
                       + " PROD_MENSAGEM, NCM, DTCADASTRO, OPCADASTRO,  PRO_TIPO_CONTROLADO, PROD_CLASSE_TERAP, PROD_CAIXA_CARTELADO, PROD_CODIGO_CARTELADO, PROD_PORTARIA_CONTROLADO) VALUES ("
                       + dados.ProdID + ",'"
                       + dados.ProdCodBarras + "','"
                       + dados.ProdDescr + "','"
                       + dados.ProdDescAbrev + "','"
                       + dados.ProdUnidade + "','"
                       + dados.ProdTipo + "','"
                       + dados.ProdAbcFarma + "','"
                       + dados.ProdUsoContinuo + "','"
                       + dados.ProdFormula + "','"
                       + dados.EspCodigo + "','"
                       + dados.PrinCodigo + "','"
                       + dados.ProdRegistroMS + "','"
                       + dados.ProdControlado + "','"
                       + dados.ProdMensagem + "','"
                       + dados.ProdNCM + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario.ToUpper() + "','"
                       + dados.TipoReceita + "','"
                       + dados.ProdClasTera + "','"
                       + dados.ProdCaixaCartelado + "',";
            if(String.IsNullOrEmpty(dados.ProdCodigoCartelado))
            {
                sql += " null,";
            }else
            {
                sql += "'" + dados.ProdCodigoCartelado + "',";
            }

            sql += "'" + dados.ProdPortaria + "')";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("PROD_ID", dados.ProdID.ToString(), Principal.usuario, "PRODUTOS", dados.ProdDescr, Principal.empAtual, Principal.estAtual, true);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Atualiza os campos da tabela Produtos
        /// </summary>
        /// <param name="dadosNovos"></param>
        /// <param name="dadosAntigos"></param>
        /// <returns></returns>
        public bool AtualizaProdutos(Produto dadosNovos, DataTable dadosAntigos)
        {
            string sql = "  UPDATE PRODUTOS SET ";
            string[,] dados = new string[19, 3];
            int contMatriz = 0;

            if (!dadosNovos.ProdDescr.Equals(dadosAntigos.Rows[0]["PROD_DESCR"].ToString()))
            {
                sql += " PROD_DESCR = '" + dadosNovos.ProdDescr + "', ";

                dados[contMatriz, 0] = "PROD_DESCR";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_DESCR"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdDescAbrev.Equals(dadosAntigos.Rows[0]["PROD_ABREV"].ToString()))
            {
                sql += " PROD_ABREV = '" + dadosNovos.ProdDescAbrev + "', ";

                dados[contMatriz, 0] = "PROD_ABREV";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ABREV"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdDescAbrev + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdUnidade.Equals(dadosAntigos.Rows[0]["PROD_UNIDADE"].ToString()))
            {
                sql += " PROD_UNIDADE = '" + dadosNovos.ProdUnidade + "', ";

                dados[contMatriz, 0] = "PROD_UNIDADE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_UNIDADE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdUnidade + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdTipo.Equals(dadosAntigos.Rows[0]["PROD_TIPO"].ToString()))
            {
                sql += " PROD_TIPO = '" + dadosNovos.ProdTipo + "', ";

                dados[contMatriz, 0] = "PROD_TIPO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_TIPO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdTipo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdAbcFarma.Equals(dadosAntigos.Rows[0]["PROD_ABC_FARMA"].ToString()))
            {
                sql += " PROD_ABC_FARMA = '" + dadosNovos.ProdAbcFarma + "', ";

                dados[contMatriz, 0] = "PROD_ABC_FARMA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_ABC_FARMA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdAbcFarma + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdUsoContinuo.Equals(dadosAntigos.Rows[0]["PROD_USO_CONTINUO"].ToString()))
            {
                sql += " PROD_USO_CONTINUO = '" + dadosNovos.ProdUsoContinuo + "', ";

                dados[contMatriz, 0] = "PROD_USO_CONTINUO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_USO_CONTINUO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdUsoContinuo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdFormula.Equals(dadosAntigos.Rows[0]["PROD_FORMULA"].ToString()))
            {
                sql += " PROD_FORMULA = '" + dadosNovos.ProdFormula + "', ";

                dados[contMatriz, 0] = "PROD_FORMULA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_FORMULA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdFormula + "'";
                contMatriz = contMatriz + 1;
            }

            int codigoPrincipio = dadosAntigos.Rows[0]["PRI_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dadosAntigos.Rows[0]["PRI_CODIGO"]);

            if (!dadosNovos.PrinCodigo.Equals(codigoPrincipio))
            {
                sql += " PRI_CODIGO = '" + dadosNovos.PrinCodigo + "', ";

                dados[contMatriz, 0] = "PRI_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PRI_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.PrinCodigo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdRegistroMS.Equals(dadosAntigos.Rows[0]["PROD_REGISTRO_MS"].ToString()))
            {
                sql += " PROD_REGISTRO_MS ='" + dadosNovos.ProdRegistroMS + "', ";

                dados[contMatriz, 0] = "PROD_REGISTRO_MS";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_REGISTRO_MS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdRegistroMS.ToString() + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.ProdControlado.Equals(dadosAntigos.Rows[0]["PROD_CONTROLADO"].ToString()))
            {
                sql += " PROD_CONTROLADO = '" + dadosNovos.ProdControlado + "', ";

                dados[contMatriz, 0] = "PROD_CONTROLADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CONTROLADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdControlado.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdMensagem.Equals(dadosAntigos.Rows[0]["PROD_MENSAGEM"].ToString()))
            {
                sql += " PROD_MENSAGEM = '" + dadosNovos.ProdMensagem + "', ";

                dados[contMatriz, 0] = "PROD_MENSAGEM";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_MENSAGEM"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdMensagem.ToString() + "'";
                contMatriz = contMatriz + 1;
            }

            int codigoEsp = dadosAntigos.Rows[0]["ESP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dadosAntigos.Rows[0]["ESP_CODIGO"]);

            if (!dadosNovos.EspCodigo.Equals(codigoEsp))
            {
                sql += " ESP_CODIGO = '" + dadosNovos.EspCodigo + "', ";

                dados[contMatriz, 0] = "ESP_CODIGO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["ESP_CODIGO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.EspCodigo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.ProdNCM.Equals(dadosAntigos.Rows[0]["NCM"].ToString()))
            {
                sql += " NCM = '" + dadosNovos.ProdNCM + "', ";

                dados[contMatriz, 0] = "NCM";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["NCM"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdNCM.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.TipoReceita.Equals(dadosAntigos.Rows[0]["PRO_TIPO_CONTROLADO"].ToString()))
            {
                sql += " PRO_TIPO_CONTROLADO = '" + dadosNovos.TipoReceita + "', ";

                dados[contMatriz, 0] = "PRO_TIPO_CONTROLADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PRO_TIPO_CONTROLADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.TipoReceita.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdClasTera.Equals(dadosAntigos.Rows[0]["PROD_CLASSE_TERAP"].ToString()))
            {
                sql += " PROD_CLASSE_TERAP = '" + dadosNovos.ProdClasTera + "', ";
                dados[contMatriz, 0] = "PROD_CLASSE_TERAP";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CLASSE_TERAP"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdClasTera.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdCaixaCartelado.Equals(dadosAntigos.Rows[0]["PROD_CAIXA_CARTELADO"].ToString()))
            {
                sql += " PROD_CAIXA_CARTELADO = '" + dadosNovos.ProdCaixaCartelado + "', ";

                dados[contMatriz, 0] = "PROD_CAIXA_CARTELADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CAIXA_CARTELADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdCaixaCartelado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdCodigoCartelado.Equals(dadosAntigos.Rows[0]["PROD_CODIGO_CARTELADO"].ToString()))
            {
                sql += " PROD_CODIGO_CARTELADO = '" + dadosNovos.ProdCodigoCartelado + "', ";

                dados[contMatriz, 0] = "PROD_CODIGO_CARTELADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CODIGO_CARTELADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdCodigoCartelado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.ProdPortaria.Equals(dadosAntigos.Rows[0]["PROD_PORTARIA_CONTROLADO"].ToString()))
            {
                sql += " PROD_PORTARIA_CONTROLADO = '" + dadosNovos.ProdPortaria + "', ";

                dados[contMatriz, 0] = "PROD_PORTARIA_CONTROLADO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_PORTARIA_CONTROLADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.ProdPortaria + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                sql += " OPALTERACAO = '" + Principal.usuario + "', ";
                sql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now);
                sql += " WHERE PROD_ID = " + dadosNovos.ProdID;

                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("PROD_ID", dadosNovos.ProdID.ToString(), Principal.usuario, "PRODUTOS", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                    return true;
                }
                else
                    return false;
            }
            else
                return true;
        }

        // Atualiza apenas dados referente ao SNGPC
        public bool AtualizaProdutoSNGPC(Produto dadosNovos, DataTable dadosAntigos)
        {
            try
            {

                string sql = "  UPDATE PRODUTOS SET ";

                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                if (!dadosNovos.ProdRegistroMS.Equals(dadosAntigos.Rows[0]["PROD_REGISTRO_MS"].ToString()))
                {
                    sql += " PROD_REGISTRO_MS ='" + dadosNovos.ProdRegistroMS + "', ";

                    dados[contMatriz, 0] = "PROD_REGISTRO_MS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_REGISTRO_MS"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdRegistroMS.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }

                if (dadosNovos.ProdControlado != dadosAntigos.Rows[0]["PROD_CONTROLADO"].ToString() && !String.IsNullOrEmpty(dadosNovos.ProdControlado))
                {
                    sql += " PROD_CONTROLADO = '" + dadosNovos.ProdControlado + "', ";

                    dados[contMatriz, 0] = "PROD_CONTROLADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CONTROLADO"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdControlado.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }
                if (dadosNovos.ProdUnidade != dadosAntigos.Rows[0]["PROD_UNIDADE"].ToString() && !String.IsNullOrEmpty(dadosNovos.ProdUnidade))
                {
                    sql += " PROD_UNIDADE = '" + dadosNovos.ProdUnidade + "', ";

                    dados[contMatriz, 0] = "PROD_UNIDADE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_UNIDADE"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdUnidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (dadosNovos.ProdClasTera != dadosAntigos.Rows[0]["PROD_CLASSE_TERAP"].ToString() && !String.IsNullOrEmpty(dadosNovos.ProdClasTera))
                {
                    int classeTera = dadosNovos.ProdClasTera == "ANTIMICROBIANO" ? 1 : 2;

                    sql += " PROD_CLASSE_TERAP = " + classeTera + ", ";

                    dados[contMatriz, 0] = "PROD_CLASSE_TERAP";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PROD_CLASSE_TERAP"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.ProdClasTera.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }


                if (contMatriz != 0)
                {

                    sql += " OPALTERACAO = '" + Principal.usuario + "', ";
                    sql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now);
                    sql += " WHERE PROD_ID = " + dadosNovos.ProdID;

                    if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                    {
                        Funcoes.GravaLogAlteracao("PROD_ID", dadosNovos.ProdID.ToString(), Principal.usuario, "PRODUTOS", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }


        public bool ExcluiPorCodigoBarras(string codBarras)
        {
            string sql = "DELETE FROM PRODUTOS WHERE PROD_CODIGO = '" + codBarras + "'";

            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }


        public DataTable BuscaProdutosEntradaItens(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT A.PROD_DESCR, A.PROD_UNIDADE, COALESCE(B.PROD_QTDE_UN_COMP, 1) AS PROD_QTDE_UN_COMP, COALESCE(D.PRE_VALOR,0) AS PRE_VALOR, COALESCE(B.PROD_COMISSAO,0) AS PROD_COMISSAO, A.PROD_CODIGO, B.PROD_SITUACAO, B.PROD_MARGEM, COALESCE(A.NCM, E.NCM) AS NCM, "
                + " B.PROD_COMISSAO, A.PROD_CONTROLADO, COALESCE(B.PROD_ULTCUSME, 0) AS PROD_ULTCUSME,COALESCE(B.PROD_CUSME, 0) AS PROD_CUSME, B.PROD_ESTATUAL, B.PROD_PRECOMPRA, B.PROD_QTDE_UN_COMP, "
                + " B.PROD_ICMS, B.PROD_MARGEM, B.PROD_CFOP,B.PROD_CST, B.PROD_VALOR_BC_ICMS, B.PROD_VALOR_ICMS, B.PROD_BC_ICMS_ST, B.PROD_BC_ICMS_RET, B.PROD_ICMS_RET, B.PROD_ENQ_IPI, "
                + " B.PROD_CST_IPI, B.PROD_BASE_IPI, B.PROD_ALIQ_IPI, B.PROD_ORIGEM, B.PROD_CST_COFINS, B.PROD_VALOR_IPI, B.PROD_CST_PIS, B.PROD_VALOR_ICMS_ST, COALESCE(B.PROD_PRECOMPRA, 0.00) AS PROD_PRECOMPRA, B.PROD_CEST, "
                + " CASE WHEN COALESCE(F.PROD_CODIGO,'0') = 0 THEN 'N' ELSE 'S' END AS PROD_POPULAR, A.PROD_REGISTRO_MS "
                + " FROM PRODUTOS_DETALHE B"
                + " LEFT JOIN PRODUTOS A ON (A.PROD_CODIGO = B.PROD_CODIGO)"
                + " LEFT JOIN PRECOS D ON (A.PROD_CODIGO = D.PROD_CODIGO AND B.EST_CODIGO = D.EST_CODIGO AND B.EMP_CODIGO = D.EMP_CODIGO AND D.TAB_CODIGO = 1)"
                + " LEFT JOIN PRODUTOS_NCM E ON (B.PROD_CODIGO = E.PROD_CODIGO)"
                + " LEFT JOIN FP_PRODUTOS_PRECOS F ON (F.PROD_CODIGO = B.PROD_CODIGO)"
                + " WHERE B.EST_CODIGO = " + estCodigo
                + " AND B.EMP_CODIGO = " + empCodigo
                + " AND B.PROD_CODIGO = '" + prodCodigo + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosDescricao(string prodDescricao)
        {
            return BancoDados.GetDataTable("SELECT PROD_CODIGO FROM PRODUTOS WHERE PROD_DESCR = '" + prodDescricao + "'", null);
        }

        public DataTable BuscaProdutosDescricaoLike(string prodDescricao)
        {
            return BancoDados.GetDataTable("SELECT PROD_CODIGO FROM PRODUTOS WHERE PROD_DESCR LIKE '%" + prodDescricao + "%'", null);
        }

        public List<Produto> IdentificaProdutoControlado(string prodCodigo)
        {
            string strSql = "SELECT COALESCE(PROD_CONTROLADO,'N') AS PROD_CONTROLADO, PROD_REGISTRO_MS FROM PRODUTOS WHERE PROD_CODIGO = '" + prodCodigo + "'";

            List<Produto> listaProduto = new List<Produto>();

            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Produto dProduto = new Produto();
                    dProduto.ProdControlado = row["PROD_CONTROLADO"].ToString();
                    dProduto.ProdRegistroMS = row["PROD_REGISTRO_MS"].ToString();
                    listaProduto.Add(dProduto);
                }
            }

            return listaProduto;
        }


        public bool AtualizaNCMProdUnidadeEntradaNotas(Produto dadosProduto, int estCodigo, int empCodigo)
        {
            string[,] logAtualiza = new string[5, 3];
            int contMatriz = 0;
            string strCmd;
            DataTable dt = new DataTable();

            dt = BancoDados.GetDataTable("SELECT A.PROD_CODIGO, A.PROD_UNIDADE, A.NCM, A.PROD_REGISTRO_MS, A.DAT_ALTERACAO, A.OPALTERACAO "
                + " FROM PRODUTOS A "
                + " WHERE A.PROD_CODIGO = '" + dadosProduto.ProdCodBarras + "'", null);

            strCmd = "UPDATE PRODUTOS SET ";
            if (String.IsNullOrEmpty(dt.Rows[0]["PROD_UNIDADE"].ToString()))
            {
                logAtualiza[contMatriz, 0] = "PROD_UNIDADE";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_UNIDADE"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdUnidade + "'";
                strCmd += "PROD_UNIDADE = '" + dadosProduto.ProdUnidade + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosProduto.ProdNCM.Equals(dt.Rows[0]["NCM"].ToString()))
            {
                logAtualiza[contMatriz, 0] = "NCM";
                logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["NCM"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdNCM + "'";
                strCmd += "NCM = '" + dadosProduto.ProdNCM + "',";
                contMatriz = contMatriz + 1;
            }
            if (!String.IsNullOrEmpty(dadosProduto.ProdRegistroMS))
            {
                if (String.IsNullOrEmpty(dt.Rows[0]["PROD_REGISTRO_MS"].ToString()))
                {
                    logAtualiza[contMatriz, 0] = "PROD_REGISTRO_MS";
                    logAtualiza[contMatriz, 1] = "'" + dt.Rows[0]["PROD_REGISTRO_MS"] + "'";
                    logAtualiza[contMatriz, 2] = "'" + dadosProduto.ProdRegistroMS + "'";
                    strCmd += "PROD_REGISTRO_MS = '" + dadosProduto.ProdRegistroMS + "',";
                    contMatriz = contMatriz + 1;
                }
            }
            if (contMatriz != 0)
            {
                logAtualiza[contMatriz, 0] = "DAT_ALTERACAO";
                logAtualiza[contMatriz, 1] = dt.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["DAT_ALTERACAO"] + "'";
                logAtualiza[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                contMatriz = contMatriz + 1;

                logAtualiza[contMatriz, 0] = "OPALTERACAO";
                logAtualiza[contMatriz, 1] = dt.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dt.Rows[0]["OPALTERACAO"] + "'";
                logAtualiza[contMatriz, 2] = "'" + dadosProduto.OpAlteracao + "'";
                strCmd += " OPALTERACAO = '" + dadosProduto.OpAlteracao + "' ";
                contMatriz = contMatriz + 1;

                strCmd += " WHERE PROD_CODIGO = '" + dadosProduto.ProdCodBarras + "'";
                if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return false;
                }
                else
                    Funcoes.GravaLogAlteracao("PROD_CODIGO", dadosProduto.ProdCodBarras, dadosProduto.OpAlteracao, "PRODUTOS", logAtualiza, contMatriz, estCodigo, empCodigo, true);
            }

            dt = BancoDados.GetDataTable("SELECT PROD_CODIGO, NCM FROM PRODUTOS_NCM WHERE PROD_CODIGO = '" + dadosProduto.ProdCodBarras + "'", null);
            if (dt.Rows.Count > 0)
            {
                strCmd = "UPDATE PRODUTOS_NCM SET NCM = '" + dadosProduto.ProdNCM + "' WHERE PROD_CODIGO = '" + dadosProduto.ProdCodBarras + "'";
            }
            else
            {
                strCmd = "INSERT INTO PRODUTOS_NCM VALUES('" + dadosProduto.ProdCodBarras + "','" + dadosProduto.ProdNCM + "')";
            }
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }

            return true;

        }


        public bool IdentificaSeProdutoEstaCadastrado(string proCodigo)
        {
            string strSql = "SELECT PROD_CODIGO FROM PRODUTOS"
                        + " WHERE PROD_CODIGO = '" + proCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return false;
            else
                return true;
        }

        public bool AtualizaID()
        {
            var dt = new DataTable();
            string strSql = "SELECT * FROM PRODUTOS";

            dt = BancoDados.GetDataTable(strSql, null);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strSql = "UPDATE PRODUTOS SET PROD_ID = " + Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID")
                    + ", DTCADASTRO = " + Funcoes.BDataHora(DateTime.Now) + ", OPCADASTRO = '" + Principal.usuario + "' "
                    + " WHERE PROD_CODIGO = '" + dt.Rows[i]["PROD_CODIGO"] + "'";
                BancoDados.ExecuteNoQuery(strSql, null);
                Application.DoEvents();
            }

            return true;
        }

        public DataTable BuscaDadosProdutosPesquisaDetalhada(int estCodigo, int empCodigo, string descr = "", string codBarras = "", string preco = "", string prinAtivo = "", string filtro = "")
        {
            string strSql = "SELECT B.PROD_CODIGO, A.PROD_DESCR, B.PROD_ESTATUAL, H.PRE_VALOR, H.MED_PCO18, 0.00 AS DESC_MIN, 0.00 AS DESC_MAX, C.DEP_DESCR, ";
            strSql += "D.FAB_DESCRICAO, E.PRI_DESCRICAO, 'N' AS PROD_PROMO, C.DEP_CODIGO,F.CLAS_CODIGO, G.SUB_CODIGO, A.PROD_ID, COALESCE(A.PROD_CONTROLADO,'N') AS PROD_CONTROLADO ";
            strSql += "FROM PRODUTOS_DETALHE B ";
            strSql += "LEFT JOIN DEPARTAMENTOS C ON (B.DEP_CODIGO = C.DEP_CODIGO AND B.EMP_CODIGO = C.EMP_CODIGO) ";
            strSql += "LEFT JOIN FABRICANTES D ON (B.FAB_CODIGO = D.FAB_CODIGO AND B.EMP_CODIGO = D.EMP_CODIGO) ";
            strSql += "LEFT JOIN CLASSES F ON (B.CLAS_CODIGO = F.CLAS_CODIGO AND B.EMP_CODIGO = F.EMP_CODIGO) ";
            strSql += "LEFT JOIN SUBCLASSES G ON (B.SUB_CODIGO = G.SUB_CODIGO AND B.EMP_CODIGO = G.EMP_CODIGO) ";
            strSql += "INNER JOIN PRECOS H ON (B.PROD_CODIGO = H.PROD_CODIGO AND B.EMP_CODIGO = H.EMP_CODIGO AND B.EST_CODIGO = H.EST_CODIGO) ";
            strSql += "INNER JOIN PRODUTOS A ON B.PROD_CODIGO = A.PROD_CODIGO ";
            strSql += "LEFT JOIN PRINCIPIO_ATIVO E ON A.PRI_CODIGO = E.PRI_CODIGO ";
            strSql += "WHERE B.EST_CODIGO = " + estCodigo + " AND B.EMP_CODIGO = " + empCodigo;
            strSql += "AND B.PROD_SITUACAO = 'A' AND H.TAB_CODIGO = 1 ";
            if (descr != "")
            {
                strSql += "AND A.PROD_DESCR LIKE '%" + descr + "%' ";
            }
            if (preco != "")
            {
                strSql += " AND H.PRE_VALOR >= " + preco + " AND H.PRE_VALOR <= " + preco + " + 1 ";

            }
            if (codBarras != "")
            {
                strSql += "AND B.PROD_CODIGO  LIKE '%" + codBarras + "%' ";
            }
            if (prinAtivo != "")
            {
                strSql += "AND A.PRI_CODIGO = " + prinAtivo + " ";
            }
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }
            strSql += "ORDER BY A.PROD_DESCR, H.PRE_VALOR ";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DadosProduto(string prodCodigo, int prodID)
        {
            string strSql = "SELECT CASE A.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANOS' ELSE  case  A.PROD_CLASSE_TERAP WHEN '2' THEN" +
                 " 'SUJEITO A CONTROLE ESPECIAL' ELSE '' END END AS PROD_CLASSE_TERAP, A.PROD_ABC_FARMA, A.ESP_CODIGO, B.ESP_DESCRICAO, A.PRI_CODIGO," +
                 " C.PRI_DESCRICAO, A.PROD_REGISTRO_MS, A.PROD_MENSAGEM, A.PRO_TIPO_CONTROLADO, A.PROD_CAIXA_CARTELADO, A.PROD_CODIGO_CARTELADO, A.PROD_PORTARIA_CONTROLADO " +
                 " FROM PRODUTOS A" +
                 " LEFT JOIN ESPECIFICACOES B ON (A.ESP_CODIGO = B.ESP_CODIGO AND B.EMP_CODIGO = " + Principal.empAtual + ")" +
                 " LEFT JOIN PRINCIPIO_ATIVO C ON (C.PRI_CODIGO = A.PRI_CODIGO AND C.EMP_CODIGO = " + Principal.empAtual + ")" +
                 " WHERE A.PROD_CODIGO = '" + prodCodigo + "'" +
                 " AND A.PROD_ID = " + prodID;

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereProdutosNovos(Produto dados)
        {
            string strCmd = "INSERT INTO PRODUTOS (PROD_CODIGO, PROD_DESCR, PROD_UNIDADE, PROD_TIPO, DTCADASTRO, OPCADASTRO, PROD_ID) VALUES('"
                + dados.ProdCodBarras + "','"
                + Funcoes.RemoverAspas(dados.ProdDescr) + "','"
                + dados.ProdUnidade + "','"
                + dados.ProdTipo + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "'," + dados.ProdID + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InsereRegistros(Produto dados)
        {
            string strCmd = "INSERT INTO PRODUTOS (PROD_ID,PROD_CODIGO,PROD_DESCR,PROD_UNIDADE,PROD_ABREV,PROD_TIPO,PROD_CLASSE_TERAP," +
                "PROD_ABC_FARMA,PROD_USO_CONTINUO,PROD_FORMULA,ESP_CODIGO,PRI_CODIGO,PROD_REGISTRO_MS,PROD_CONTROLADO,PROD_MENSAGEM," +
                "NCM,DTCADASTRO,OPCADASTRO) VALUES(";
            strCmd += dados.ProdID + ","
            + "'" + dados.ProdCodBarras + "','"
            + dados.ProdDescr + "','"
            + dados.ProdUnidade + "',";
            if (String.IsNullOrEmpty(dados.ProdDescAbrev))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdDescAbrev + "','";

            strCmd += dados.ProdTipo + "',";

            if (String.IsNullOrEmpty(dados.ProdClasTera))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdClasTera + "',";

            if (String.IsNullOrEmpty(dados.ProdAbcFarma))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdAbcFarma + "',";

            strCmd += "'" + dados.ProdUsoContinuo + "','" + dados.ProdFormula + "',";
            strCmd += "'" + dados.EspCodigo + "','" + dados.PrinCodigo + "',";
            strCmd += "'" + dados.ProdRegistroMS + "','" + dados.ProdControlado + "',";

            strCmd += "'" + dados.ProdMensagem + "','" + dados.ProdNCM + "',";
            strCmd += Funcoes.BDataHora(DateTime.Now) + ",'" + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InsereProdutosDetalhe(Produto dados)
        {
            string strCmd = "INSERT INTO PRODUTOS_DETALHE (EMP_CODIGO,EST_CODIGO,PROD_CODIGO,DEP_CODIGO,CLAS_CODIGO,SUB_CODIGO,PROD_LOCP,PROD_LOCH,PROD_LOCV," +
                "PROD_CUSME,PROD_ESTINI,PROD_DTESTINI,PROD_ESTATUAL,PROD_ESTMIN,PROD_ULTCUSME,PROD_SITUACAO,PROD_DATAULTCOMPRA,PROD_DATAPENCOMPRA,PROD_DATAANTCOMPRA," +
                "NCM,DTCADASTRO,OPCADASTRO) VALUES(";
            strCmd += dados.ProdID + ","
            + "'" + dados.ProdCodBarras + "','"
            + dados.ProdDescr + "','"
            + dados.ProdUnidade + "',";
            if (String.IsNullOrEmpty(dados.ProdDescAbrev))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdDescAbrev + "','";

            strCmd += dados.ProdTipo + "',";

            if (String.IsNullOrEmpty(dados.ProdClasTera))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdClasTera + "',";

            if (String.IsNullOrEmpty(dados.ProdAbcFarma))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.ProdAbcFarma + "',";

            strCmd += "'" + dados.ProdUsoContinuo + "','" + dados.ProdFormula + "',";
            strCmd += "'" + dados.EspCodigo + "','" + dados.PrinCodigo + "',";
            strCmd += "'" + dados.ProdRegistroMS + "','" + dados.ProdControlado + "',";

            strCmd += "'" + dados.ProdMensagem + "','" + dados.ProdNCM + "',";
            strCmd += Funcoes.BDataHora(DateTime.Now) + ",'" + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaDadosProdutosPorCodigoBarras(int estCodigo, int empCodigo, string codBarra)
        {
            string strSql = "SELECT A.PROD_DESCR, A.PROD_ID," +
                   " A.PROD_UNIDADE," +
                   " A.PROD_CODIGO," +
                   " A.PROD_CONTROLADO," +
                   " COALESCE(C.PRE_VALOR,0) AS PRE_VALOR," +
                   " COALESCE(C.MED_PCO18,0) AS PRECO_MAXIMO," +
                   " B.PROD_ESTATUAL," +
                   " B.DEP_CODIGO," +
                   " B.CLAS_CODIGO," +
                   " B.SUB_CODIGO," +
                   " B.PROD_ECF," +
                   " COALESCE(B.PROD_ULTCUSME,0) AS PROD_ULTCUSME," +
                   " A.NCM," +
                   " COALESCE(B.PROD_CUSME,0) AS PROD_CUSME," +
                   " A.PROD_MENSAGEM," +
                   " D.ALIQ_NAC, B.PROD_ESTMIN, COALESCE(B.PROD_COMISSAO,0) AS PROD_COMISSAO, " +
                   " C.BLOQUEIA_DESCONTO " +
                   " FROM PRODUTOS A" +
                   " INNER JOIN PRODUTOS_DETALHE B ON (A.PROD_CODIGO = B.PROD_CODIGO AND A.PROD_ID = B.PROD_ID)" +
                   " INNER JOIN PRECOS C ON (A.PROD_CODIGO = C.PROD_CODIGO AND A.PROD_ID = C.PROD_ID)" +
                   " LEFT JOIN TABELA_IBPT D ON D.NCM = A.NCM" +
                   " WHERE B.EST_CODIGO = " + estCodigo +
                   " AND B.EMP_CODIGO = " + empCodigo +
                   " AND B.EMP_CODIGO = C.EMP_CODIGO" +
                   " AND B.EST_CODIGO = C.EST_CODIGO" +
                   " AND C.TAB_CODIGO = 1" +
                   " AND B.PROD_SITUACAO = 'A'" +
                   " AND A.PROD_CODIGO = '" + codBarra + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

        public string BuscaProdCodigo(string codBarra)
        {
            string strSql = "SELECT PROD_CODIGO FROM PRODUTOS  WHERE  PROD_CODIGO = '"
                + codBarra + "' AND PROD_TIPO = 'P'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public DataTable BuscaProdutosEstoqueFilial(int estCodigo, int empCodigo)
        {
            string strsq = "select a.prod_codigo, c.prod_descr, a.prod_estatual,"
                    + " case when coalesce(a.Prod_Ultcusme,0) = 0 then coalesce(a.PROD_PRECOMPRA,0) else coalesce(a.Prod_Ultcusme,0) end as Prod_Ultcusme, b.pre_valor"
                    + " from produtos_detalhe a"
                    + " inner join precos b on a.prod_codigo = b.prod_codigo"
                    + " inner join produtos c on a.prod_codigo = c.prod_codigo"
                    + " where a.emp_codigo = " + empCodigo
                    + " and a.est_codigo = " + estCodigo
                    + " and a.emp_codigo = b.emp_codigo"
                    + " and a.est_codigo = b.est_codigo"
                    + " and a.prod_situacao = 'A'"
                    + " and b.tab_codigo = 1 and prod_estatual > 0"
                    + " order by c.prod_descr";
            return BancoDados.GetDataTable(strsq, null);
        }

        public DataTable BuscaProdutos(string codigoBarras)
        {
            string sql = " SELECT  P.PROD_ID, P.PROD_CODIGO, P.PROD_DESCR, P.PROD_UNIDADE, P.PROD_REGISTRO_MS, CASE P.PROD_CLASSE_TERAP "
                       + " WHEN '1' THEN 'ANTIMICROBIANO' WHEN '2' THEN 'SUJEITO A CONTROLE ESPECIAL' ELSE ''"
                       + " END AS PROD_CLASSE_TERAP, P.PROD_CONTROLADO , PD.PROD_ULTCUSME , PRE.PRE_VALOR "
                       + " FROM PRODUTOS P "
                       + " INNER JOIN PRODUTOS_DETALHE PD ON(PD.PROD_ID = P.PROD_ID) "
                       + " INNER JOIN PRECOS PRE ON(PRE.PROD_ID = P.PROD_ID) "
                       + " WHERE PD.PROD_SITUACAO = 'A' AND P.PROD_CODIGO = '" + codigoBarras + "'"
                       + " AND PD.EMP_CODIGO = " + Principal.empAtual
                       + " AND PD.EST_CODIGO = " + Principal.estAtual
                       + " AND PRE.EMP_CODIGO = PD.EMP_CODIGO"
                       + " AND PRE.EST_CODIGO = PD.EST_CODIGO";
            return BancoDados.GetDataTable(sql, null);

        }

        public bool AtualizaEstoque(string prodCodigo, int qtde, string operacao, bool trans = true)
        {
            string strCmd = "UPDATE PRODUTOS_DETALHE SET PROD_ESTATUAL = PROD_ESTATUAL " + operacao + " " + qtde + ", DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", "
                    + " OPALTERACAO = '" + Principal.usuario + "' WHERE PROD_CODIGO = '" + prodCodigo + "' AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.empAtual;

            if (trans.Equals(true))
            {
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public bool IdentificaUsoContinuo(string proCodigo)
        {
            string strSql = "SELECT PROD_USO_CONTINUO FROM PRODUTOS"
                        + " WHERE PROD_CODIGO = '" + proCodigo + "' AND PROD_USO_CONTINUO = 'S'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return false;
            else
                return true;
        }

        public int ProdutoQtdePorEmbalagem(string codBarra)
        {
            string strSql = "SELECT PROD_QTDE_EMB FROM PRODUTOS  WHERE  PROD_CODIGO = '" + codBarra + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public DataTable DadosFiscaisSAT(int estCodigo, int empCodigo, int prodID)
        {
            string strSql = "SELECT B.PROD_CODIGO,B.PROD_DESCR, COALESCE(B.NCM,'30049099') AS NCM, COALESCE(C.PROD_CFOP, '5405') AS CFOP, COALESCE(C.PROD_CST, '060') AS PROD_CST, "
                        + " COALESCE(D.NACIONAL, 0) AS NACIONAL, COALESCE(D.ESTADUAL, 0) AS ESTADUAL,"
                        + " COALESCE(D.MUNICIPAL, 0) AS MUNICIPAL"
                        + " FROM  PRODUTOS B"
                        + " INNER JOIN PRODUTOS_DETALHE C ON C.PROD_ID = B.PROD_ID"
                        + " LEFT JOIN IBPT D ON B.NCM = D.NCM"
                        + " WHERE C.EMP_CODIGO = " + empCodigo
                        + " AND C.EST_CODIGO = " + estCodigo
                        + " AND B.PROD_ID = " + prodID;

            return BancoDados.GetDataTable(strSql, null);

        }

        public DataTable BuscaRapidaProdutos(int estCodigo, int empCodigo, string prodCodigo = "", string prodDescr = "", double preValor = 0)
        {
            string strSql = "SELECT A.PROD_CODIGO, A.PROD_ID, A.PROD_DESCR, B.PRE_VALOR, C." + Funcoes.LeParametro(9,"65",false) + " AS PROD_ULTCUSME, C.PROD_ESTATUAL "
                + " FROM PRODUTOS A"
                + " INNER JOIN PRECOS B ON A.PROD_ID = B.PROD_ID"
                + " INNER JOIN PRODUTOS_DETALHE C ON C.PROD_ID = A.PROD_ID"
                + " WHERE B.EMP_CODIGO = " + empCodigo
                + " AND B.EST_CODIGO = " + estCodigo
                + " AND B.EST_CODIGO = C.EST_CODIGO"
                + " AND B.EMP_CODIGO = C.EMP_CODIGO"
                + " AND C.PROD_SITUACAO = 'A'"
                + " AND B.TAB_CODIGO = 1";
            if (!String.IsNullOrEmpty(prodCodigo))
            {
                strSql += " AND A.PROD_CODIGO LIKE '%" + prodCodigo + "%'";
            }
            if (!String.IsNullOrEmpty(prodDescr))
            {
                strSql += " AND A.PROD_DESCR LIKE '%" + prodDescr + "%'";
            }
            if (preValor != 0)
            {
                strSql += " AND B.PRE_VALOR BETWEEN " + Funcoes.BValor(preValor - 1) + " AND " + Funcoes.BValor(preValor + 1);
            }

            strSql += " ORDER BY A.PROD_DESCR";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable UltimosProdutosPorCliente(int estCodigo, int empCodigo, DateTime dtInicial, DateTime dtFinal, int cliID)
        {
            string strSql = "SELECT A.VENDA_ID, A.VENDA_DATA, B.PROD_CODIGO, C.PROD_DESCR, B.VENDA_ITEM_QTDE, B.VENDA_ITEM_UNITARIO, B.VENDA_ITEM_TOTAL, D.PRE_VALOR"
                        + " FROM VENDAS A"
                        + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                        + " INNER JOIN PRODUTOS C ON B.PROD_ID = C.PROD_ID"
                        + " INNER JOIN PRECOS D ON B.PROD_ID = D.PROD_ID"
                        + " WHERE A.EMP_CODIGO = " + empCodigo
                        + " AND A.EST_CODIGO = " + estCodigo
                        + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                        + " AND A.EST_CODIGO = B.EST_CODIGO"
                        + " AND A.EMP_CODIGO = D.EMP_CODIGO"
                        + " AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal)
                        + " AND A.VENDA_CF_ID = " + cliID
                        + " ORDER BY A.VENDA_EMISSAO, A.VENDA_ID";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosPreco(string codigoBarras, string descricao, int departamento, int classes)
        {
            string sql = " SELECT PRO.PROD_ID, PRO.PROD_CODIGO, PRO.PROD_DESCR ,"
                       + " PROD.PROD_MARGEM,"
                       + " CASE WHEN PROD.PROD_ULTCUSME > 0 THEN PROD.PROD_ULTCUSME ELSE PROD.PROD_PRECOMPRA END AS PROD_ULTCUSME , PRE.PRE_VALOR "
                       + " FROM PRODUTOS PRO "
                       + " INNER JOIN PRECOS PRE ON(PRE.PROD_ID = PRO.PROD_ID) "
                       + " INNER JOIN PRODUTOS_DETALHE PROD ON(PROD.PROD_ID = PRO.PROD_ID) "
                       + " WHERE PRE.EST_CODIGO = " + Principal.estAtual
                       + " AND PRE.EMP_CODIGO = " + Principal.empAtual
                       + " AND PRE.EST_CODIGO = PROD.EST_CODIGO"
                       + " AND PRE.EMP_CODIGO = PROD.EST_CODIGO";

            if (!String.IsNullOrEmpty(codigoBarras))
            {
                sql += " AND PRO.PROD_CODIGO = '" + codigoBarras + "'";
            }
            if (!String.IsNullOrEmpty(descricao))
            {
                sql += " AND PRO.PROD_DESCR LIKE '%" + descricao + "%'";
            }
            if (!departamento.Equals(0))
            {
                sql += " AND PROD.DEP_CODIGO =" + departamento;
            }
            if (!classes.Equals(0))
            {
                sql += " AND PROD.CLAS_CODIGO =" + classes;
            }
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaProdutosTransferencia(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT B.PROD_DESCR, COALESCE(" + Funcoes.LeParametro(9, "66", false)
                   + " ,A.PROD_PRECOMPRA) AS PROD_PRECOMPRA,"
                   + " A.PROD_CODIGO, A.PROD_ESTATUAL, A.PROD_SITUACAO, A.PROD_ID, A.EMP_CODIGO, A.EST_CODIGO, "
                   + " COALESCE(A.PROD_ULTCUSME, 0) AS CUSTO, C.PRE_VALOR"
                   + " FROM PRODUTOS_DETALHE A"
                   + " INNER JOIN PRODUTOS B ON B.PROD_ID = A.PROD_ID"
                   + " INNER JOIN PRECOS C ON A.PROD_ID = C.PROD_ID"
                   + " WHERE A.EST_CODIGO = " + estCodigo
                   + " AND A.EMP_CODIGO = " + empCodigo
                   + " AND A.PROD_CODIGO = '" + prodCodigo + "'"
                   + " AND A.EST_CODIGO = C.EST_CODIGO"
                   + " AND A.EMP_CODIGO = C.EMP_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaQuantidadeProdutos(string codigoBarras, string descricao, int departamento, int classes, int ordem)
        {
            string sql = " SELECT PRO.PROD_ID, PRO.PROD_CODIGO, PRO.PROD_DESCR , TRUNC(PROD.PROD_ESTATUAL,0) AS PROD_ESTATUAL ,  TRUNC(PROD.PROD_ESTATUAL,0) AS PROD_ESTANTIGO, PROD.PROD_ULTCUSME, PRE.PRE_VALOR, DEP.DEP_DESCR"
                       + " FROM PRODUTOS PRO "
                       + " INNER JOIN PRECOS PRE ON(PRE.PROD_ID = PRO.PROD_ID) "
                       + " INNER JOIN PRODUTOS_DETALHE PROD ON(PROD.PROD_ID = PRO.PROD_ID) "
                       + " INNER JOIN DEPARTAMENTOS DEP ON PROD.DEP_CODIGO = DEP.DEP_CODIGO"
                       + " WHERE PRE.EST_CODIGO = " + Principal.estAtual
                       + " AND PRE.EMP_CODIGO = " + Principal.empAtual
                       + " AND PRE.EST_CODIGO = PROD.EST_CODIGO"
                       + " AND PRE.EMP_CODIGO = PROD.EST_CODIGO"
                       + " AND PROD.EMP_CODIGO = DEP.EMP_CODIGO";

            if (!String.IsNullOrEmpty(codigoBarras))
            {
                sql += " AND PRO.PROD_CODIGO = '" + codigoBarras + "'";
            }
            if (!String.IsNullOrEmpty(descricao))
            {
                sql += " AND PRO.PROD_DESCR LIKE '" + descricao + "%'";
            }
            if (!departamento.Equals(0))
            {
                sql += " AND PROD.DEP_CODIGO =" + departamento;
            }
            if (!classes.Equals(0))
            {
                sql += " AND PROD.CLAS_CODIGO =" + classes;
            }
            if(ordem.Equals(0))
            {
                sql += " ORDER BY PROD_ESTATUAL DESC";
            }
            else
            {
                sql += " ORDER BY PROD_ESTATUAL";
            }

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaNcmRegistroMSAbcFarma(Produto dados)
        {
            string strCmd = "UPDATE PRODUTOS SET ";
            
            if(!String.IsNullOrEmpty(dados.ProdNCM))
            {
                strCmd += " NCM = '" + dados.ProdNCM + "',";

                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(dados.DtAlteracao) + ", "
                    + " OPALTERACAO = '" + dados.OpAlteracao + "' WHERE PROD_CODIGO = '" + dados.ProdCodBarras + "'";
                if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return true;


        }

        public bool InsereProdutosNovosAbcFarma(Produto dados)
        {
            string strCmd = "INSERT INTO PRODUTOS (PROD_CODIGO, PROD_DESCR, PROD_UNIDADE, PROD_TIPO, NCM, DTCADASTRO, OPCADASTRO, PROD_ID,PROD_REGISTRO_MS) VALUES('"
                + dados.ProdCodBarras + "','"
                + Funcoes.RemoverAspas(dados.ProdDescr) + "','"
                + dados.ProdUnidade + "','"
                + dados.ProdTipo + "','"
                + dados.ProdNCM + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "',"
                + dados.ProdID + ",'"
                + dados.ProdRegistroMS + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaProdutosParaAbcFarma(int estCodigo, int empCodigo)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PRE_VALOR, B.MED_PCO18"
                + " FROM PRODUTOS_DETALHE A"
                + " INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                + " INNER JOIN PRODUTOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                + " WHERE A.EST_CODIGO = " + estCodigo
                + " AND A.EMP_CODIGO = " + empCodigo
                + " AND A.EST_CODIGO = B.EST_CODIGO"
                + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                + " AND B.PROD_CODIGO = C.PROD_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public bool VerificaCodigoBarras(string codigoBarras)
        {
            string sql = "SELECT PROD_CODIGO FROM PRODUTOS WHERE PROD_CODIGO = '" + codigoBarras + "'";

            var resultado = (string)BancoDados.ExecuteScalar(sql, null);
            if (String.IsNullOrEmpty(resultado))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaProdutosPortalDrogaria(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT A.PROD_DESCR, COALESCE(B.PRE_VALOR,0) AS PRE_VALOR, C.PROD_SITUACAO"
                + " FROM PRODUTOS A"
                + " LEFT JOIN PRECOS B ON (A.PROD_CODIGO = B.PROD_CODIGO AND B.TAB_CODIGO = 1)"
                + " LEFT JOIN PRODUTOS_DETALHE C ON (B.PROD_CODIGO = C.PROD_CODIGO)"
                + " WHERE B.EST_CODIGO = " + estCodigo
                + " AND B.EMP_CODIGO = " + empCodigo
                + " AND B.PROD_CODIGO = '" + prodCodigo + "'"
                + " AND B.EMP_CODIGO = C.EMP_CODIGO"
                + " AND B.EST_CODIGO = C.EST_CODIGO"
                + " AND B.PROD_CODIGO = C.PROD_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutos(string codigoBarras, string descricao, bool codigoForaDoPadrao, int codDepartamento)
        {
            string sql = " SELECT A.PROD_CODIGO, A.PROD_DESCR FROM PRODUTOS A "
                + " INNER JOIN PRODUTOS_DETALHE B ON A.PROD_CODIGO = B.PROD_CODIGO WHERE B.EMP_CODIGO = " + Principal.empAtual + " AND B.EST_CODIGO = " + Principal.estAtual;
            if (!String.IsNullOrEmpty(codigoBarras))
            {
                sql += " AND A.PROD_CODIGO LIKE '%" + codigoBarras + "%'";
            }
            if (!String.IsNullOrEmpty(descricao))
            {
                sql += " AND A.PROD_DESCR LIKE '%" + descricao + "%'";
            }
            if (codigoForaDoPadrao.Equals(true))
            {
                sql += " AND LENGTH(A.PROD_CODIGO)<>13";
            }

            if(codDepartamento != 0)
            {
                sql += " AND B.DEP_CODIGO = " + codDepartamento;
            }
            return BancoDados.GetDataTable(sql, null);

        }

        public bool InsereLogProdutos(string operacao, string codBarras, string proDescr, string campo = null, string anterior = null, string alterado = null)
        {
            string sql = "INSERT INTO LOG_PRODUTOS (LOG_OPERADOR, LOG_OPERACAO, COD_BARRAS, PROD_DESCR, LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, LOG_DATA, EST_CODIGO, EMP_CODIGO) VALUES( "
                       + "'" + Principal.usuario + "',"
                       + "'" + operacao + "',"
                       + "'" + codBarras + "',"
                       + "'" + proDescr + "',"
                       + "'" + campo + "',"
                       + "'" + anterior + "',"
                       + "'" + alterado + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ","
                       + Principal.estAtual + ","
                       + Principal.empAtual + ")";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaProdutosParaReposicao(string dtInicial, string dtFinal, string horaInicial, string horaFinal, string filtro, int empCodigo, int estCodigo, string prodCodigo = "")
        {
            string sql = " SELECT A.PROD_CODIGO, B.PROD_DESCR, B.PROD_UNIDADE, A.PROD_ESTATUAL, A.PROD_QTDE_UN_COMP, COALESCE(A.PROD_ESTMIN,1) AS PROD_ESTMIN, 0 AS ID, ";
            if (String.IsNullOrEmpty(prodCodigo))
            {
                sql += " COALESCE(SUM(C.VENDA_ITEM_QTDE), 0) AS QTDE_VENDIDA, ";
            }
            else
            {
                sql += " 0 AS QTDE_VENDIDA, ";
            }

            sql += "F.FAB_DESCRICAO, COALESCE(A.PROD_ULTCUSME, A.PROD_PRECOMPRA) AS PROD_CUSTO,"
                    + "    E.PRE_VALOR"
                    + "    FROM PRODUTOS_DETALHE A"
                    + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO";

            if (String.IsNullOrEmpty(prodCodigo))
            {
                sql += "    INNER JOIN VENDAS_ITENS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                    + "    INNER JOIN VENDAS D ON D.VENDA_ID = C.VENDA_ID";
            }
            sql += "    INNER JOIN PRECOS E ON A.PROD_CODIGO = E.PROD_CODIGO"
                + "    LEFT JOIN FABRICANTES F ON (A.FAB_CODIGO = F.FAB_CODIGO AND A.EMP_CODIGO = F.EMP_CODIGO)"
                + "    WHERE A.EMP_CODIGO = " + empCodigo
                + "    AND A.EST_CODIGO = " + estCodigo;
            if (String.IsNullOrEmpty(prodCodigo))
            {
                sql += "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                + "    AND A.EST_CODIGO = C.EST_CODIGO"
                + "    AND A.EMP_CODIGO = D.EMP_CODIGO"
                + "    AND A.EST_CODIGO = D.EST_CODIGO";
            }
            sql += "    AND A.EMP_CODIGO = E.EMP_CODIGO"
                + "    AND A.EST_CODIGO = E.EST_CODIGO"
                + "    AND A.PROD_SITUACAO = 'A'"
                + "    AND (A.PROD_BLOQ_COMPRA = 'N' or A.PROD_BLOQ_COMPRA IS NULL)";
            if (String.IsNullOrEmpty(prodCodigo))
            {
                sql += "    AND D.VENDA_DATA_HORA BETWEEN TO_DATE('" + dtInicial + " " + horaInicial + "', 'DD/MM/YYYY HH24:MI:SS') AND"
                + "            TO_DATE('" + dtFinal + " " + horaFinal + "', 'DD/MM/YYYY HH24:MI:SS')";
            }
            else
            {
                sql += " AND A.PROD_CODIGO = '" + prodCodigo + "'";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                sql += " AND " + filtro;
            }

            sql += "    GROUP BY A.PROD_CODIGO, B.PROD_DESCR, B.PROD_UNIDADE, A.PROD_ESTATUAL, F.FAB_DESCRICAO, A.PROD_ULTCUSME, A.PROD_PRECOMPRA,E.PRE_VALOR, A.PROD_QTDE_UN_COMP, A.PROD_ESTMIN "
                    + "    ORDER BY 2";
            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BuscaDescricao(string prodCodigo)
        {
            string strSql = "SELECT A.PROD_DESCR"
                + " FROM PRODUTOS A"
                + " WHERE A.PROD_CODIGO = '" + prodCodigo + "'";
            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaProdutosAjusteEstoque(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT B.PROD_DESCR, COALESCE(A.PROD_CUSME,A.PROD_ULTCUSME) AS PROD_CUSME, A.PROD_CODIGO, A.PROD_ESTATUAL, A.PROD_SITUACAO, "
                + " A.PROD_ID, A.EMP_CODIGO, A.EST_CODIGO, C.PRE_VALOR, A.PROD_ULTCUSME, A.PROD_MARGEM, A.PROD_PRECOMPRA, COALESCE(A.PROD_COMISSAO,0) AS PROD_COMISSAO "
                + " FROM PRODUTOS_DETALHE A"
                + " INNER JOIN PRODUTOS B ON B.PROD_ID = A.PROD_ID"
                + " INNER JOIN PRECOS C ON A.PROD_ID = C.PROD_ID"
                + " WHERE A.EST_CODIGO = " + estCodigo
                + " AND A.EMP_CODIGO = " + empCodigo
                + " AND A.PROD_CODIGO = '" + prodCodigo + "'"
                + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                + " AND A.EST_CODIGO = A.EST_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosFP(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT B.PROD_CODIGO, B.PROD_DESCR, COALESCE(C.PROD_PRECOMPRA,0) AS PROD_PRECOMPRA, "
                     + " COALESCE(C.PROD_ULTCUSME,0) AS PROD_ULTCUSME,  COALESCE(C.PROD_QTDE_UN_COMP_FP,0) AS PROD_QTDE_UN_COMP_FP,  COALESCE(D.PRE_VALOR,0) AS PRE_VALOR"
                     + " FROM PRODUTOS B "
                     + " INNER JOIN PRODUTOS_DETALHE C ON B.PROD_CODIGO = C.PROD_CODIGO"
                     + " INNER JOIN PRECOS D ON D.PROD_CODIGO = B.PROD_CODIGO"
                     + " WHERE C.EMP_CODIGO = " + Principal.empAtual
                     + " AND C.EST_CODIGO = " + Principal.estAtual
                     + " AND C.EST_CODIGO = D.EST_CODIGO"
                     + " AND C.EMP_CODIGO = D.EMP_CODIGO"
                     + " AND B.PROD_CODIGO = '" + prodCodigo + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaComissaoQtdeEstoqueRegistroMS(string prodCodigo, int estCodigo, int empCodigo)
        {
            string strSql = "SELECT A.PROD_ESTATUAL, COALESCE(A.PROD_COMISSAO,0) AS PROD_COMISSAO, B.PROD_REGISTRO_MS, COALESCE(A.PROD_PRECOMPRA, 0) AS PROD_PRECOMPRA" +
                 " FROM PRODUTOS_DETALHE A" +
                 " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO" +
                 " WHERE A.PROD_CODIGO = '" + prodCodigo + "'" +
                 " AND A.EMP_CODIGO = " + empCodigo +
                 " AND A.EST_CODIGO = " + estCodigo;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable RelatorioFichaDoProduto(int estCodigo, int empCodigo, string prodCodigo, string dtInicial, string dtFinal)
        {
            string strSql = " SELECT F.PROD_CODIGO,"
                            + "           F.DESCRICAO,"
                            + "           TO_DATE(F.DATA) AS DATA,"
                            + "           F.ORIGEM,"
                            + "           F.NOME,"
                            + "           F.ENTRADA,"
                            + "           F.SAIDA,"
                            + "           F.SALDO, F.ID"
                            + "      FROM(SELECT B.PROD_CODIGO,"
                            + "       C.PROD_DESCR AS DESCRICAO,"
                            + "       TO_CHAR(A.VENDA_EMISSAO, 'DD/MM/YYYY') AS DATA,"
                            + "       'VENDA' AS ORIGEM,"
                            + "       D.COL_NOME AS NOME,"
                            + "       0 AS ENTRADA,"
                            + "       SUM(B.VENDA_ITEM_QTDE)AS SAIDA,"
                            + "       0 - SUM(B.VENDA_ITEM_QTDE) AS SALDO, A.VENDA_ID AS ID "
                            + " FROM VENDAS A"
                            + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + " INNER JOIN COLABORADORES D ON A.VENDA_COL_CODIGO = D.COL_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.EST_CODIGO = B.EST_CODIGO"
                            + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + " AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + " AND B.PROD_CODIGO = '" + prodCodigo + "'"
                            + " AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal))
                            + " AND A.VENDA_STATUS = 'F'"
                            + " GROUP BY B.PROD_CODIGO, C.PROD_DESCR, A.VENDA_EMISSAO, D.COL_NOME, A.VENDA_ID"
                            + " UNION ALL"
                            + " SELECT B.PROD_CODIGO,"
                            + "       C.PROD_DESCR AS DESCRICAO,"
                            + "       TO_CHAR(A.ENT_DTLANC, 'DD/MM/YYYY') AS DATA,"
                            + "       'ENTRADA NOTAS' AS ORIGEM,"
                            + "       D.COL_NOME AS NOME,"
                            + "       B.Ent_Qtdestoque AS ENTRADA,"
                            + "       0 AS SAIDA,"
                            + "       B.Ent_Qtdestoque - 0 AS SALDO, A.ENT_ID AS ID"
                            + " FROM ENTRADA A"
                            + " INNER JOIN ENTRADA_ITENS B ON A.ENT_ID = B.ENT_ID"
                            + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + " INNER JOIN COLABORADORES D ON A.PAG_COMPRADOR = D.COL_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + " AND A.EST_CODIGO = B.EST_CODIGO"
                            + " AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + " AND A.ENT_DTLANC BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal))
                            + " AND B.PROD_CODIGO = '" + prodCodigo + "'"
                            + " UNION ALL"
                            + " SELECT A.PROD_CODIGO,"
                            + "       B.PROD_DESCR AS DESCRICAO,"
                            + "       TO_CHAR(A.DTCADASTRO, 'DD/MM/YYYY') AS DATA,"
                            + "       'AJUSTE' AS ORIGEM,"
                            + "       A.OPCADASTRO AS NOME,"
                            + "       CASE WHEN A.OPERACAO = '+' THEN A.QTDE ELSE 0 END AS ENTRADA,"
                            + "       CASE WHEN A.OPERACAO = '-' THEN A.QTDE ELSE 0 END AS SAIDA,"
                            + "       CASE WHEN A.OPERACAO = '+' THEN A.QTDE - 0 ELSE 0 - A.QTDE END AS SALDO, A.ID "
                            + " FROM ALTERACAO_ESTOQUE A"
                            + " INNER JOIN PRODUTOS B ON B.PROD_CODIGO = A.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.DTCADASTRO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                            + " AND B.PROD_CODIGO = '" + prodCodigo + "'"
                            + " UNION ALL"
                            + " SELECT A.PROD_CODIGO,"
                            + "       B.PROD_DESCR AS DESCRICAO,"
                            + "       TO_CHAR(A.DTCADASTRO, 'DD/MM/YYYY') AS DATA,"
                            + "       'TRANSFERENCIA' AS ORIGEM,"
                            + "       A.OPCADASTRO AS NOME,"
                            + "       0 AS ENTRADA,"
                            + "       A.QTDE AS SAIDA,"
                            + "       0 - A.QTDE AS SALDO, A.ID"
                            + " FROM TRANSFERENCIA_ITENS A"
                            + " INNER JOIN PRODUTOS B ON B.PROD_CODIGO = A.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + " AND A.EST_CODIGO = " + estCodigo
                            + " AND A.DTCADASTRO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                            + " AND B.PROD_CODIGO = '" + prodCodigo + "') F"
                            + " ORDER BY DATA";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosDevolucao(int estCodigo, int empCodigo, string prodCodigo)
        {
            string strSql = "SELECT B.PROD_DESCR, COALESCE(PROD_ULTCUSME,C.PRE_VALOR) AS PROD_PRECOMPRA,"
                + " A.PROD_CODIGO, A.PROD_ID, A.EMP_CODIGO, A.EST_CODIGO "
                + " FROM PRODUTOS_DETALHE A"
                + " INNER JOIN PRODUTOS B ON B.PROD_ID = A.PROD_ID"
                + " INNER JOIN PRECOS C ON A.PROD_ID = C.PROD_ID"
                + " WHERE A.EST_CODIGO = " + estCodigo
                + " AND A.EMP_CODIGO = " + empCodigo
                + " AND A.PROD_CODIGO = '" + prodCodigo + "'"
                + " AND A.EST_CODIGO = C.EST_CODIGO"
                + " AND A.EMP_CODIGO = C.EMP_CODIGO";
            return BancoDados.GetDataTable(strSql, null);
        }
        
        public DataTable BalancoSemQuebra(int estCodigo, int empCodigo, int opcaoValor, string filtro, int ordem, bool produtosZerados)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PROD_DESCR, A.PROD_ESTATUAL, C.MED_PCO18 AS PMC, ";
            if(opcaoValor.Equals(0))
            {
                strSql += " A.PROD_CUSME AS UNITARIO, A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO,";
            }
            else if (opcaoValor.Equals(1))
            {
                strSql += " A.PROD_ULTCUSME AS UNITARIO, A.PROD_ULTCUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO,";
            }
            else if (opcaoValor.Equals(2))
            {
                strSql += " C.PRE_VALOR AS UNITARIO, C.PRE_VALOR * A.PROD_ESTATUAL AS TOTAL_UNITARIO, ";
            }

            strSql += " A.PROD_DTESTINI, A.PROD_ESTINI, A.DEP_CODIGO, A.CLAS_CODIGO, A.SUB_CODIGO"; 


            strSql += " FROM PRODUTOS_DETALHE A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.PROD_SITUACAO = 'A'";
            if(!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            if (ordem.Equals(0))
            {
                strSql += " ORDER BY B.PROD_DESCR";
            }
            else
            {
                strSql += " ORDER BY A.PROD_CODIGO";
            }
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BalancoQuebraSintetico(int estCodigo, int empCodigo, int opcaoValor, string filtro, int ordem, bool produtosZerados, int quebra)
        {
            string strSql = "SELECT  X.PMC, X.PROD_DTESTINI, X.PROD_ESTINI, X.PROD_CODIGO, X.PROD_DESCR, ";
            if(quebra.Equals(0))
            {
                strSql += " D.DEP_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            else if(quebra.Equals(1))
            {
                strSql += " D.CLAS_CODIGO AS CODIGO, D.CLAS_DESCR AS DESCRICAO, ";
            }
            else if(quebra.Equals(2))
            {
                strSql += " D.SUB_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }

            strSql += " SUM(X.PROD_ESTATUAL) AS QTDE, SUM(X.UNITARIO) AS UNITARIO, SUM(X.TOTAL_UNITARIO) AS TOTAL FROM ";

            if (quebra.Equals(0))
            {
                strSql += "(SELECT A.DEP_CODIGO, A.PROD_ESTATUAL, ";
            }
            else if (quebra.Equals(1))
            {
                strSql += "(SELECT A.CLAS_CODIGO, A.PROD_ESTATUAL,";
            }
            else if (quebra.Equals(2))
            {
                strSql += "(SELECT A.SUB_CODIGO, A.PROD_ESTATUAL,";
            }

            if (opcaoValor.Equals(0))
            {
                strSql += " A.PROD_CUSME AS UNITARIO, A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO,";
            }
            else if (opcaoValor.Equals(1))
            {
                strSql += " A.PROD_ULTCUSME AS UNITARIO, A.PROD_ULTCUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO,";
            }
            else if (opcaoValor.Equals(2))
            {
                strSql += " C.PRE_VALOR AS UNITARIO, C.PRE_VALOR * A.PROD_ESTATUAL AS TOTAL_UNITARIO,";
            }
            strSql += " C.MED_PCO18 AS PMC, ";
            strSql += " A.PROD_DTESTINI, A.PROD_ESTINI, A.PROD_CODIGO, B.PROD_DESCR";

            strSql += " FROM PRODUTOS_DETALHE A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.PROD_SITUACAO = 'A'";
            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += " ) X ";

            if (quebra.Equals(0))
            { 
                strSql += " LEFT JOIN DEPARTAMENTOS D ON (D.DEP_CODIGO = X.DEP_CODIGO AND D.EMP_CODIGO = " + empCodigo 
                    + ") GROUP BY D.DEP_CODIGO, D.DEP_DESCR, X.PMC, X.PROD_DTESTINI, X.PROD_ESTINI, X.PROD_CODIGO, X.PROD_DESCR ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES D ON (D.CLAS_CODIGO = X.CLAS_CODIGO AND D.EMP_CODIGO = " + empCodigo 
                    + ") GROUP BY D.CLAS_CODIGO, D.CLAS_DESCR, X.PMC, X.PROD_DTESTINI, X.PROD_ESTINI, X.PROD_CODIGO, X.PROD_DESCR ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES D ON (D.SUB_CODIGO = X.SUB_CODIGO AND D.EMP_CODIGO = " + empCodigo 
                    + ") GROUP BY D.SUB_CODIGO, D.SUB_DESCR, X.PMC, X.PROD_DTESTINI, X.PROD_ESTINI, X.PROD_CODIGO, X.PROD_DESCR ";
            }

            strSql += " ORDER BY ";

            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BalancoQuebraAnalitico(int estCodigo, int empCodigo, int opcaoValor, string filtro, int ordem, bool produtosZerados, int quebra)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PROD_DESCR, A.PROD_ESTATUAL AS QTDE , C.MED_PCO18 AS PMC, ";
            if (quebra.Equals(0))
            {
                strSql += " D.DEP_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " D.CLAS_CODIGO AS CODIGO, D.CLAS_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " D.SUB_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            
            if (opcaoValor.Equals(0))
            {
                strSql += " A.PROD_CUSME AS UNITARIO, A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL,";
            }
            else if (opcaoValor.Equals(1))
            {
                strSql += " A.PROD_ULTCUSME AS UNITARIO, A.PROD_ULTCUSME * A.PROD_ESTATUAL AS TOTAL,";
            }
            else if (opcaoValor.Equals(2))
            {
                strSql += " C.PRE_VALOR AS UNITARIO, C.PRE_VALOR * A.PROD_ESTATUAL AS TOTAL,";
            }

            strSql += " A.PROD_DTESTINI, A.PROD_ESTINI";

            strSql += " FROM PRODUTOS_DETALHE A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO";
            
            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS D ON (D.DEP_CODIGO = A.DEP_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES D ON (D.CLAS_CODIGO = A.CLAS_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")  ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES D ON (D.SUB_CODIGO = A.SUB_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")";
            }

            strSql += " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.PROD_SITUACAO = 'A'";
            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += " ORDER BY ";

            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_DESCR, B.PROD_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_CODIGO, A.PROD_CODIGO";
                }
            }

           return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable ProdutosMaisVendidos(int estCodigo, int empCodigo, string linha, int codDepartamento, string dtInicial, string dtFinal)
        {
            string strSql = "SELECT *"
                    + " FROM(SELECT B.PROD_CODIGO, C.PROD_DESCR, SUM(B.VENDA_ITEM_QTDE) AS QTDE_VENDIDA, D.PROD_ESTATUAL, E.DEP_DESCR, G.PRE_VALOR"
                    + " FROM VENDAS A"
                    + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                    + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                    + " INNER JOIN PRODUTOS_DETALHE D ON B.PROD_CODIGO = D.PROD_CODIGO"
                    + " INNER JOIN PRECOS G ON B.PROD_CODIGO = G.PROD_CODIGO"
                    + " LEFT JOIN DEPARTAMENTOS E ON(D.DEP_CODIGO = E.DEP_CODIGO AND A.EMP_CODIGO = E.EMP_CODIGO)"
                    + " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EST_CODIGO = B.EST_CODIGO"
                    + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                    + " AND A.EST_CODIGO = D.EST_CODIGO"
                    + " AND A.EMP_CODIGO = D.EMP_CODIGO"
                    + " AND A.EST_CODIGO = G.EST_CODIGO"
                    + " AND A.EMP_CODIGO = G.EMP_CODIGO"
                    + " AND B.PROD_CODIGO NOT IN ('" + Funcoes.LeParametro(6, "307", true) + "')"
                    + " AND A.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                    + " AND A.VENDA_STATUS = 'F'";
            if (codDepartamento != 0)
            {
                strSql += " AND D.DEP_CODIGO = " + codDepartamento;
            }

            strSql += " GROUP BY B.PROD_CODIGO, C.PROD_DESCR, D.PROD_ESTATUAL, E.DEP_DESCR, G.PRE_VALOR"
                    + " ORDER BY QTDE_VENDIDA DESC) F"
                    + " WHERE ROWNUM <= " + linha;

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable ProdutosPorCliente(int estCodigo, int empCodigo,string dtInicial, string dtFinal, string numDocto, int ordem, string filtro)
        {
            string strSql = " SELECT E.VENDA_EMISSAO,"
                            + "       E.VENDA_ID,"
                            + "       B.PROD_CODIGO,"
                            + "       C.PROD_DESCR,"
                            + "       B.VENDA_ITEM_QTDE AS QTDE,"
                            + "       B.VENDA_ITEM_UNITARIO AS UNITARIO,"
                            + "       B.VENDA_ITEM_DIFERENCA AS DESCONTO,"
                            + "       B.VENDA_ITEM_TOTAL AS TOTAL"
                            + "  FROM VENDAS E"
                            + "  INNER JOIN VENDAS_ITENS B ON E.VENDA_ID = B.VENDA_ID"
                            + "  INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "  INNER JOIN CLIFOR D ON E.VENDA_CF_ID = D.CF_ID"
                            + "  INNER JOIN PRODUTOS_DETALHE A ON B.PROD_CODIGO = A.PROD_CODIGO"
                            + "  WHERE E.EMP_CODIGO = " + empCodigo
                            + "  AND E.EST_CODIGO = " + estCodigo
                            + "  AND E.EMP_CODIGO = B.EMP_CODIGO"
                            + "  AND E.EST_CODIGO = B.EST_CODIGO"
                            + "  AND E.EMP_CODIGO = A.EMP_CODIGO"
                            + "  AND E.EST_CODIGO = A.EST_CODIGO"
                            + "  AND E.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                            + "  AND E.VENDA_STATUS = 'F'"
                            + "  AND D.CF_DOCTO = '" + numDocto + "'";
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }
            strSql += "  ORDER BY E.VENDA_EMISSAO, E.VENDA_ID,";
            if(ordem.Equals(0))
            {
                strSql += " C.PROD_DESCR";
            }
            else
            {
                strSql += " B.PROD_CODIGO";
            }
      
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable ProdutosPorClienteQuebra(int estCodigo, int empCodigo, string dtInicial, string dtFinal, string numDocto, int ordem, string filtro, int quebra)
        {
            string strSql = " SELECT E.VENDA_EMISSAO,"
                            + "       E.VENDA_ID,"
                            + "       B.PROD_CODIGO,"
                            + "       C.PROD_DESCR,"
                            + "       B.VENDA_ITEM_QTDE AS QTDE,"
                            + "       B.VENDA_ITEM_UNITARIO AS UNITARIO,"
                            + "       B.VENDA_ITEM_DIFERENCA AS DESCONTO,"
                            + "       B.VENDA_ITEM_TOTAL AS TOTAL,";

            if (quebra.Equals(0))
            {
                strSql += " F.DEP_CODIGO AS CODIGO, F.DEP_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " F.CLAS_CODIGO AS CODIGO, F.CLAS_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " F.SUB_CODIGO AS CODIGO, F.DEP_DESCR AS DESCRICAO ";
            }

            strSql += "  FROM VENDAS E"
                            + "  INNER JOIN VENDAS_ITENS B ON E.VENDA_ID = B.VENDA_ID"
                            + "  INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "  INNER JOIN CLIFOR D ON E.VENDA_CF_ID = D.CF_ID"
                            + "  INNER JOIN PRODUTOS_DETALHE A ON B.PROD_CODIGO = A.PROD_CODIGO";

            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS F ON (F.DEP_CODIGO = A.DEP_CODIGO AND F.EMP_CODIGO = " + empCodigo + ") ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES F ON (F.CLAS_CODIGO = A.CLAS_CODIGO AND F.EMP_CODIGO = " + empCodigo + ")  ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES F ON (F.SUB_CODIGO = A.SUB_CODIGO AND F.EMP_CODIGO = " + empCodigo + ")";
            }

            strSql += "  WHERE E.EMP_CODIGO = " + empCodigo
                            + "  AND E.EST_CODIGO = " + estCodigo
                            + "  AND E.EMP_CODIGO = B.EMP_CODIGO"
                            + "  AND E.EST_CODIGO = B.EST_CODIGO"
                            + "  AND E.EMP_CODIGO = A.EMP_CODIGO"
                            + "  AND E.EST_CODIGO = A.EST_CODIGO"
                            + "  AND E.VENDA_STATUS = 'F'"
                            + "  AND E.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                            + "  AND D.CF_DOCTO = '" + numDocto + "'";
            
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }
            strSql += "  ORDER BY E.VENDA_EMISSAO, E.VENDA_ID,";
            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " F.DEP_DESCR, C.PROD_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " F.CLAS_DESCR, C.PROD_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " F.SUB_DESCR, C.PROD_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " F.DEP_CODIGO, B.PROD_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " F.CLAS_CODIGO, B.PROD_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " F.SUB_CODIGO, B.PROD_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable DescontosConcedidosPorDia(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int vendedor)
        {
            string strSql = " SELECT TO_CHAR(A.VENDA_EMISSAO,'DD/MM/YYYY') AS DESCRICAO,"
                            + "           TO_CHAR(A.VENDA_EMISSAO, 'MM') AS ID,"
                            + "           SUM(B.VENDA_ITEM_TOTAL)AS BRUTO,"
                            + "           SUM(A.VENDA_DIFERENCA) + SUM(B.VENDA_ITEM_DIFERENCA) AS DESCONTO,"
                            + "           SUM(B.VENDA_ITEM_TOTAL) -"
                            + "           (SUM(ABS(A.VENDA_DIFERENCA)) + SUM(ABS(B.VENDA_ITEM_DIFERENCA))) AS LIQUIDO"
                            + "      FROM VENDAS A"
                            + "      INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "      WHERE A.EMP_CODIGO = " + empCodigo
                            + "      AND A.EST_CODIGO = " + estCodigo
                            + "      AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "      AND A.EST_CODIGO = B.EST_CODIGO"
                            + "      AND A.VENDA_STATUS = 'F'"
                            + "      AND ABS(A.VENDA_DIFERENCA) > 0"
                            + "      AND ABS(B.VENDA_ITEM_DIFERENCA) > 0"
                            + "      AND A.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
            if(vendedor != 0)
            {
                strSql += "   AND B.COL_CODIGO = " + vendedor;
            }

            strSql += "     GROUP BY A.VENDA_EMISSAO"
                    + "     ORDER BY A.VENDA_EMISSAO";
            
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DescontosConcedidosPorFormaDePagamento(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int vendedor)
        {
            string strSql = " SELECT C.VENDA_FORMA_ID AS ID, "
                            + "           D.FORMA_DESCRICAO AS DESCRICAO,"
                            + "           SUM(B.VENDA_ITEM_TOTAL)AS BRUTO,"
                            + "           SUM(A.VENDA_DIFERENCA) + SUM(B.VENDA_ITEM_DIFERENCA) AS DESCONTO,"
                            + "           SUM(B.VENDA_ITEM_TOTAL) -"
                            + "           (SUM(ABS(A.VENDA_DIFERENCA)) + SUM(ABS(B.VENDA_ITEM_DIFERENCA))) AS LIQUIDO"
                            + "      FROM VENDAS A"
                            + "      INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "      INNER JOIN VENDAS_FORMA_PAGAMENTO C ON A.VENDA_ID = C.VENDA_ID"
                            + "      INNER JOIN FORMAS_PAGAMENTO D ON C.VENDA_FORMA_ID = D.FORMA_ID"
                            + "      WHERE A.EMP_CODIGO = " + empCodigo
                            + "      AND A.EST_CODIGO = " + estCodigo
                            + "      AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "      AND A.EST_CODIGO = B.EST_CODIGO"
                            + "      AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "      AND A.EST_CODIGO = C.EST_CODIGO"
                            + "      AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "      AND A.VENDA_STATUS = 'F'"
                            + "      AND ABS(A.VENDA_DIFERENCA) > 0"
                            + "      AND ABS(B.VENDA_ITEM_DIFERENCA) > 0"
                            + "      AND A.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
            if (vendedor != 0)
            {
                strSql += "   AND B.COL_CODIGO = " + vendedor;
            }

            strSql += "     GROUP BY C.VENDA_FORMA_ID,D.FORMA_DESCRICAO"
                    + "     ORDER BY C.VENDA_FORMA_ID";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DescontosConcedidosPorDepartamento(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int vendedor)
        {
            string strSql = " SELECT C.DEP_CODIGO AS ID, "
                            + "           D.DEP_DESCR AS DESCRICAO,"
                            + "           SUM(B.VENDA_ITEM_TOTAL)AS BRUTO,"
                            + "           SUM(A.VENDA_DIFERENCA) + SUM(B.VENDA_ITEM_DIFERENCA) AS DESCONTO,"
                            + "           SUM(B.VENDA_ITEM_TOTAL) -"
                            + "           (SUM(ABS(A.VENDA_DIFERENCA)) + SUM(ABS(B.VENDA_ITEM_DIFERENCA))) AS LIQUIDO"
                            + "      FROM VENDAS A"
                            + "      INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + "      INNER JOIN PRODUTOS_DETALHE C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "      LEFT JOIN DEPARTAMENTOS D ON (C.DEP_CODIGO = D.DEP_CODIGO AND C.EMP_CODIGO = D.EMP_CODIGO)"
                            + "      WHERE A.EMP_CODIGO = " + empCodigo
                            + "      AND A.EST_CODIGO = " + estCodigo
                            + "      AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "      AND A.EST_CODIGO = B.EST_CODIGO"
                            + "      AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "      AND A.EST_CODIGO = C.EST_CODIGO"
                            + "      AND A.VENDA_STATUS = 'F'"
                            + "      AND ABS(A.VENDA_DIFERENCA) > 0"
                            + "      AND ABS(B.VENDA_ITEM_DIFERENCA) > 0"
                            + "      AND A.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
            if (vendedor != 0)
            {
                strSql += "   AND B.COL_CODIGO = " + vendedor;
            }

            strSql += "     GROUP BY C.DEP_CODIGO, D.DEP_DESCR"
                    + "     ORDER BY C.DEP_CODIGO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable ProdutosXCustosSemQuebra(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int ordem, string filtro)
        {
            string strSql = " SELECT B.PROD_CODIGO,"
                            + "           C.PROD_DESCR,"
                            + "           SUM(B.VENDA_ITEM_QTDE) AS QTDE,"
                            + "           SUM(B.VENDA_ITEM_TOTAL)AS TOTAL,"
                            + "           SUM(A.PROD_CUSME * B.VENDA_ITEM_QTDE) AS CUSTO,"
                            + "          ROUND(SUM(B.VENDA_ITEM_TOTAL) / SUM(B.VENDA_ITEM_QTDE), 2) AS PRECO_MEDIO, D.VENDA_EMISSAO"
                            + "      FROM VENDAS D"
                            + "     INNER JOIN VENDAS_ITENS B ON D.VENDA_ID = B.VENDA_ID"
                            + "     INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + "     INNER JOIN PRODUTOS_DETALHE A ON B.PROD_CODIGO = A.PROD_CODIGO"
                            + "     WHERE D.EMP_CODIGO = " + empCodigo
                            + "       AND D.EST_CODIGO = " + estCodigo
                            + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = B.EST_CODIGO"
                            + "       AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = D.EMP_CODIGO"
                            + "       AND D.VENDA_STATUS = 'F'"
                            + "       AND D.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }
            strSql += " GROUP BY B.PROD_CODIGO, C.PROD_DESCR,D.VENDA_EMISSAO,D.VENDA_ID ORDER BY D.VENDA_EMISSAO, D.VENDA_ID,";
            if (ordem.Equals(0))
            {
                strSql += " C.PROD_DESCR";
            }
            else
            {
                strSql += " B.PROD_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }
        
        public DataTable ProdutosXCustosQuebra(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int ordem, string filtro, int quebra)
        {
            string strSql = " SELECT B.PROD_CODIGO,"
                            + "           C.PROD_DESCR,"
                            + "           SUM(B.VENDA_ITEM_QTDE) AS QTDE,"
                            + "           SUM(B.VENDA_ITEM_TOTAL)AS TOTAL,"
                            + "           SUM(A.PROD_CUSME * B.VENDA_ITEM_QTDE) AS CUSTO,"
                            + "          ROUND(SUM(B.VENDA_ITEM_TOTAL) / SUM(B.VENDA_ITEM_QTDE), 2) AS PRECO_MEDIO, D.VENDA_EMISSAO, ";

            if (quebra.Equals(0))
            {
                strSql += " F.DEP_CODIGO AS CODIGO, F.DEP_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " F.CLAS_CODIGO AS CODIGO, F.CLAS_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " F.SUB_CODIGO AS CODIGO, F.DEP_DESCR AS DESCRICAO ";
            }

            strSql += " FROM VENDAS D"
                        + "     INNER JOIN VENDAS_ITENS B ON D.VENDA_ID = B.VENDA_ID"
                        + "     INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                        + "     INNER JOIN PRODUTOS_DETALHE A ON B.PROD_CODIGO = A.PROD_CODIGO";

            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS F ON (F.DEP_CODIGO = A.DEP_CODIGO AND F.EMP_CODIGO = " + empCodigo + ") ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES F ON (F.CLAS_CODIGO = A.CLAS_CODIGO AND F.EMP_CODIGO = " + empCodigo + ")  ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES F ON (F.SUB_CODIGO = A.SUB_CODIGO AND F.EMP_CODIGO = " + empCodigo + ")";
            }

            strSql += " WHERE D.EMP_CODIGO = " + empCodigo
                            + "       AND D.EST_CODIGO = " + estCodigo
                            + "       AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = B.EST_CODIGO"
                            + "       AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "       AND A.EST_CODIGO = D.EMP_CODIGO"
                            + "       AND D.VENDA_STATUS = 'F'"
                            + "       AND D.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }
            strSql += " GROUP BY B.PROD_CODIGO, C.PROD_DESCR,D.VENDA_EMISSAO,D.VENDA_ID, ";

            if (quebra.Equals(0))
            {
                strSql += " F.DEP_CODIGO, F.DEP_DESCR ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " F.CLAS_CODIGO, F.CLAS_DESCR ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " F.SUB_CODIGO, F.DEP_DESCR  ";
            }

            strSql += "  ORDER BY  D.VENDA_EMISSAO,";
            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " F.DEP_DESCR, C.PROD_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " F.CLAS_DESCR, C.PROD_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " F.SUB_DESCR, C.PROD_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " F.DEP_CODIGO, B.PROD_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " F.CLAS_CODIGO, B.PROD_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " F.SUB_CODIGO, B.PROD_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable EStoqueMinMaxSemQuebra(int estCodigo, int empCodigo, int minMax, string filtro, int ordem, bool produtosZerados)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PROD_DESCR, A.PROD_ESTATUAL, B.PROD_UNIDADE, ";
            if(minMax == 0)
            {
                strSql += " A.PROD_ESTMIN AS ESTOQUE,(A.PROD_ESTATUAL - A.PROD_ESTMIN) * -1 AS REPOSICAO";
            }
            else
            {
                strSql += " A.PROD_ESTMAX AS ESTOQUE,(A.PROD_ESTATUAL - A.PROD_ESTMAX) * -1 AS REPOSICAO";
            }

            strSql += "  FROM PRODUTOS_DETALHE A"
                        + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                        + "    WHERE A.EMP_CODIGO = " + empCodigo
                        + "    AND A.EST_CODIGO = " + estCodigo;

            if(minMax == 0)
            {
                strSql += "    AND A.PROD_ESTATUAL < A.PROD_ESTMIN";
            }
            else
            {
                strSql += "    AND A.PROD_ESTATUAL < A.PROD_ESTMAX";
            }

            strSql += "    AND A.PROD_SITUACAO = 'A'";

            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL <> 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            if (ordem.Equals(0))
            {
                strSql += " ORDER BY B.PROD_DESCR";
            }
            else
            {
                strSql += " ORDER BY A.PROD_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable EStoqueMinMaxComQuebra(int estCodigo, int empCodigo, int minMax, string filtro, int ordem, bool produtosZerados, int quebra)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PROD_DESCR, A.PROD_ESTATUAL, B.PROD_UNIDADE, ";
            if (minMax == 0)
            {
                strSql += " A.PROD_ESTMIN AS ESTOQUE,(A.PROD_ESTATUAL - A.PROD_ESTMIN) * -1 AS REPOSICAO,";
            }
            else
            {
                strSql += " A.PROD_ESTMAX AS ESTOQUE,(A.PROD_ESTATUAL - A.PROD_ESTMAX) * -1 AS REPOSICAO,";
            }
            if (quebra.Equals(0))
            {
                strSql += " D.DEP_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " D.CLAS_CODIGO AS CODIGO, D.CLAS_DESCR AS DESCRICAO ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " D.SUB_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO ";
            }
            
            strSql += " FROM PRODUTOS_DETALHE A"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO";

            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS D ON (D.DEP_CODIGO = A.DEP_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES D ON (D.CLAS_CODIGO = A.CLAS_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")  ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES D ON (D.SUB_CODIGO = A.SUB_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")";
            }

            strSql += " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.PROD_SITUACAO = 'A'";
            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL <> 0";
            }

            if (minMax == 0)
            {
                strSql += "    AND A.PROD_ESTATUAL < A.PROD_ESTMIN";
            }
            else
            {
                strSql += "    AND A.PROD_ESTATUAL < A.PROD_ESTMAX";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += " ORDER BY ";

            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_DESCR, B.PROD_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_CODIGO, A.PROD_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable ProdutosSemMovimento(int estCodigo, int empCodigo, string linha, int codDepartamento, string dtInicial, string dtFinal)
        {
            string strSql = "SELECT *"
                            + "  FROM(SELECT C.PROD_CODIGO,"
                            + "               C.PROD_DESCR,"
                            + "               D.PROD_ESTATUAL,"
                            + "               E.DEP_DESCR, D.PROD_ULTCUSME, "
                            + "               G.PRE_VALOR"
                            + "         FROM PRODUTOS C"
                            + "         INNER JOIN PRODUTOS_DETALHE D ON C.PROD_CODIGO = D.PROD_CODIGO"
                            + "         INNER JOIN PRECOS G ON C.PROD_CODIGO = G.PROD_CODIGO"
                            + "          LEFT JOIN DEPARTAMENTOS E ON(D.DEP_CODIGO = E.DEP_CODIGO AND"
                            + "                                       D.EMP_CODIGO = E.EMP_CODIGO)"
                            + "         WHERE D.EMP_CODIGO = " + empCodigo
                            + "           AND D.EST_CODIGO = " + estCodigo
                            + "           AND D.EST_CODIGO = G.EST_CODIGO"
                            + "           AND D.EMP_CODIGO = G.EMP_CODIGO"
                            + "           AND D.PROD_SITUACAO = 'A'"
                            + "           AND D.PROD_ESTATUAL > 0";
            if (codDepartamento != 0)
            {
                strSql += " AND D.DEP_CODIGO = " + codDepartamento;
            }
            strSql += "          AND C.PROD_CODIGO NOT IN(SELECT PROD_CODIGO FROM VENDAS_ITENS F"
                            + "               INNER JOIN VENDAS H ON F.VENDA_ID = H.VENDA_ID"
                            + "               WHERE F.EMP_CODIGO = " + empCodigo
                            + "               AND F.EST_CODIGO = " + estCodigo
                            + "               AND F.EST_CODIGO = H.EST_CODIGO"
                            + "               AND F.EMP_CODIGO = H.EMP_CODIGO"
                            + "               AND H.VENDA_EMISSAO BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                            + "           AND H.VENDA_STATUS = 'F'"
                            + "           )"
                            + "         GROUP BY C.PROD_CODIGO,"
                            + "                  C.PROD_DESCR,"
                            + "                  D.PROD_ESTATUAL,"
                            + "                  E.DEP_DESCR,"
                            + "                  G.PRE_VALOR, D.PROD_ULTCUSME"
                            + "         ORDER BY D.PROD_ESTATUAL DESC) F";
            if(!linha.Equals("0"))
            {
                strSql += "  WHERE ROWNUM <= " + linha;
            }
            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaEstoqueAtual(int estCodigo, int empCodigo, int ordemTerapeutica, int ordem, bool produtosZerados)
        {
            string strSql = "SELECT P.PROD_CODIGO, P.PROD_DESCR, P.PROD_REGISTRO_MS,PD.PROD_ESTATUAL  FROM PRODUTOS P INNER JOIN PRODUTOS_DETALHE PD ";
            strSql += " ON P.PROD_ID = PD.PROD_ID";
            strSql += " WHERE PD.EMP_CODIGO ="+empCodigo;
            strSql += " AND PD.EST_CODIGO = "+estCodigo;
            strSql += " AND PROD_CONTROLADO = 'S'";
            if(!produtosZerados)
                strSql += " AND PD.PROD_ESTATUAL>0";
            if(ordemTerapeutica > 0)
                strSql += " AND P.PROD_CLASSE_TERAP = '"+ordemTerapeutica+"'";
            if (ordem == 0)
                strSql += " ORDER BY P.PROD_CODIGO";
            else if (ordem == 1)
                strSql += " ORDER BY P.PROD_DESCR";





            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaEstoqueSNGPC(int ordemTerapeutica, int ordem, bool produtosZerados)
        {
            string strSql = " SELECT A.PROD_CODIGO,P.PROD_DESCR,A.LOTE,A.QTDE, TO_CHAR (A.DATA , 'DD/MM/YYYY') AS DATA FROM SNGPC_CONTROLE_VENDAS A INNER JOIN PRODUTOS P ";
            strSql += " ON A.PROD_CODIGO = P.PROD_CODIGO";
            if (!produtosZerados)
                strSql += " AND A.QTDE>0";
            if (ordemTerapeutica > 0)
                strSql += " AND P.PROD_CLASSE_TERAP = '" + ordemTerapeutica + "'";
            if (ordem == 0)
                strSql += " ORDER BY P.PROD_CODIGO";
            else if (ordem == 1)
                strSql += " ORDER BY P.PROD_DESCR";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutoPorImposto(int estCodigo, int empCodigo, string prodCodigo, int codDepartamento, bool zerados)
        {
            string strSql = "SELECT A.PROD_CODIGO,"
                            + "           B.PROD_DESCR,"
                            + "           A.PROD_CFOP,"
                            + "           A.PROD_CFOP_DEVOLUCAO,"
                            + "           A.PROD_CEST,"
                            + "           A.PROD_CST,"
                            + "           B.NCM"
                            + "      FROM PRODUTOS_DETALHE A"
                            + "     INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "     WHERE A.EMP_CODIGO = " + empCodigo
                            + "     AND A.EST_CODIGO = " + estCodigo;
            if(!String.IsNullOrEmpty(prodCodigo))
            {
                strSql += " AND A.PROD_CODIGO = '" + prodCodigo + "'";
            }

            if(codDepartamento != 0)
            {
                strSql += " AND A.DEP_CODIGO = " + codDepartamento;
            }

            if(!zerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }
            strSql += " ORDER BY B.PROD_DESCR";
            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable BuscaProdutoPorComissao(int estCodigo, int empCodigo, int codDepartamento, bool zerados, bool semComissao, int comissao, int tipo)
        {
            string strSql = "SELECT A.PROD_CODIGO,"
                            + "       B.PROD_DESCR,"
                            + "       A.PROD_ULTCUSME,"
                            + "       C.PRE_VALOR,"
                            + "       A.PROD_COMISSAO,"
                            + "       D.DEP_DESCR"
                            + "  FROM PRODUTOS_DETALHE A"
                            + "  INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "  INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                            + "  INNER JOIN DEPARTAMENTOS D ON A.DEP_CODIGO = D.DEP_CODIGO"
                            + "  WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo
                            + "   AND A.EMP_CODIGO = C.EMP_CODIGO"
                            + "   AND A.EST_CODIGO = C.EST_CODIGO"
                            + "   AND A.EMP_CODIGO = D.EMP_CODIGO"
                            + "   AND A.PROD_SITUACAO = 'A'";

            if (!zerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }
            else
            {
                strSql += " AND A.PROD_ESTATUAL <= 0";
            }


            if(codDepartamento != 0)
            {
                strSql += " AND A.DEP_CODIGO = " + codDepartamento;
            }

            if(!semComissao)
            {
                strSql += " AND A.PROD_COMISSAO > 0";
            }
            else
            {
                strSql += " AND A.PROD_COMISSAO = 0";
            }

            if(comissao != 0)
            {
                if (tipo == 1)
                {
                    strSql += " AND A.PROD_COMISSAO = " + comissao;
                }
                else if (tipo == 2)
                { 
                    strSql += " AND A.PROD_COMISSAO >= " + comissao;
                }
            }

            strSql += " ORDER BY D.DEP_DESCR, A.PROD_COMISSAO";
            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable RelatorioAjustePorPeriodo(int estCodigo, int empCodigo, string dtInicial, string dtFinal, int ordem)
        {
            string strSql = " SELECT A.DTCADASTRO,"
                            + "               A.OBSERVACAO,"
                            + "               A.PROD_CODIGO,"
                            + "               B.PROD_DESCR,"
                            + "               CASE"
                            + "                 WHEN A.OPERACAO = '+' THEN"
                            + "                  A.QTDE"
                            + "                 ELSE"
                            + "                  0"
                            + "               END AS ENTRADAS,"
                            + "               CASE"
                            + "                 WHEN A.OPERACAO = '-' THEN"
                            + "                  A.QTDE"
                            + "                 ELSE"
                            + "                  0"
                            + "               END AS SAIDAS, A.OPCADASTRO AS OPERADOR"
                            + "          FROM ALTERACAO_ESTOQUE A"
                            + "         INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + "         WHERE A.EMP_CODIGO = " + empCodigo
                            + "           AND A.EST_CODIGO = " + estCodigo
                            + " AND A.DTCADASTRO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtInicial)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            if(ordem == 0)
            {
                strSql += " ORDER BY A.DTCADASTRO";
            }
            else if(ordem == 1)
            {
                strSql += " ORDER BY A.PROD_CODIGO";
            }
            else if(ordem == 2)
            {
                strSql += " ORDER BY B.PROD_DESCR";
            }
                            

            return BancoDados.GetDataTable(strSql, null);
        }

        public string BuscaDepartamento(string codBarra, int empCodigo)
        {
            string strSql = "SELECT B.DEP_DESCR"
                    + "      FROM DEPARTAMENTOS B"
                    + "      WHERE B.EMP_CODIGO = " + empCodigo;

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return Convert.ToString(r);
        }


        public int BuscaVendasProdutoBalanco(DateTime dtInicial, DateTime dtFinal, string prodCodigo, int empCodigo, int estCodigo, string status, int filtroData)
        {
            string strSql = "SELECT SUM(B.VENDA_ITEM_QTDE) AS QTDE"
                            + "  FROM VENDAS A"
                            + " INNER JOIN VENDAS_ITENS B ON A.VENDA_ID = B.VENDA_ID"
                            + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo
                            + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "   AND A.EST_CODIGO = B.EST_CODIGO"
                            + "   AND A.VENDA_STATUS = '" + status + "'";
            if(filtroData == 0)
            {
                strSql += "   AND A.VENDA_EMISSAO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
            }
            else
            {
                strSql += "   AND A.VENDA_EMISSAO >= " + Funcoes.BData(dtInicial) + " AND  A.VENDA_EMISSAO < " + Funcoes.BData(dtFinal);
            }

            strSql += "   AND C.PROD_CODIGO = '" + prodCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public int BuscaEntradaProdutoBalanco(DateTime dtInicial, DateTime dtFinal, string prodCodigo, int empCodigo, int estCodigo, int filtroData)
        {
            string strSql = "SELECT SUM(B.ENT_QTDESTOQUE) AS QTDE"
                            + "  FROM ENTRADA A"
                            + " INNER JOIN ENTRADA_ITENS B ON A.ENT_ID = B.ENT_ID"
                            + " INNER JOIN PRODUTOS C ON B.PROD_CODIGO = C.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo
                            + "   AND A.EMP_CODIGO = B.EMP_CODIGO"
                            + "   AND A.EST_CODIGO = B.EST_CODIGO";
            if (filtroData == 0)
            {
                strSql += "   AND A.ENT_DTLANC BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
            }
            else
            {
                strSql += "   AND A.ENT_DTLANC >= " + Funcoes.BData(dtInicial) + " AND A.ENT_DTLANC < " + Funcoes.BData(dtFinal);
            }

            strSql += "   AND C.PROD_CODIGO = '" + prodCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public int BuscaAjusteProdutoBalanco(DateTime dtInicial, DateTime dtFinal, string prodCodigo, int empCodigo, int estCodigo, string operacao, int filtroData)
        {
            string strSql = "SELECT SUM(A.QTDE) AS QTDE"
                            + "  FROM ALTERACAO_ESTOQUE A"
                            + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo;
            if(filtroData == 0)
            {
                strSql += "   AND A.DTCADASTRO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
            }
            else
            {
                strSql += "   AND A.DTCADASTRO >= " + Funcoes.BData(dtInicial) + " AND  A.DTCADASTRO < " + Funcoes.BData(dtFinal);
            }
                           
            strSql += "   AND B.PROD_CODIGO = '" + prodCodigo + "'"
                            + "   AND A.OPERACAO = '" + operacao + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public int BuscaTransferenciaProdutoBalanco(DateTime dtInicial, DateTime dtFinal, string prodCodigo, int empCodigo, int estCodigo, int filtroData)
        {
            string strSql = "SELECT SUM(A.QTDE) AS QTDE"
                            + "  FROM TRANSFERENCIA_ITENS A"
                            + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo;
            if(filtroData == 0)
            {
                strSql += "   AND A.DTCADASTRO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
            }
            else
            {
                strSql += "   AND A.DTCADASTRO >= " + Funcoes.BData(dtInicial) + " AND A.DTCADASTRO < " + Funcoes.BData(dtFinal);
            }
            
            strSql += "   AND B.PROD_CODIGO = '" + prodCodigo + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public int BuscaTransferenciaImpProdutoBalanco(DateTime dtInicial, DateTime dtFinal, string prodCodigo, int empCodigo, int estCodigo, int filtroData)
        {
            string strSql = "SELECT SUM(A.ENT_QTDE) AS QTDE"
                            + "  FROM MOV_ESTOQUE A"
                            + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                            + " WHERE A.EMP_CODIGO = " + empCodigo
                            + "   AND A.EST_CODIGO = " + estCodigo;
            if(filtroData == 0)
            {
                strSql += "   AND A.DAT_MOVTO BETWEEN " + Funcoes.BData(dtInicial) + " AND " + Funcoes.BData(dtFinal);
            }
            else
            {
                strSql += "   AND A.DAT_MOVTO >= " + Funcoes.BData(dtInicial) + " AND  A.DAT_MOVTO <" + Funcoes.BData(dtFinal);
            }
                            
            strSql += "   AND B.PROD_CODIGO = '" + prodCodigo + "'"
                            + "   AND A.OPE_CODIGO = 'T'"
                            + "   AND A.ENT_QTDE > 0";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return 0;
            else
                return Convert.ToInt32(r);
        }

        public DataTable BuscaSpoolBalanco(string estacao, string prodCodigo)
        {
            string strSql = " SELECT * FROM SPOOL_BALANCO WHERE PROD_CODIGO = '" + prodCodigo + "' AND ESTACAO = '" + estacao + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public int InserirSpoolBalanco(SpoolBalanco dados)
        {
            string strSql = "INSERT INTO SPOOL_BALANCO VALUES ('"
                + dados.Estacao + "','"
                + dados.ProdCodigo + "','"
                + dados.ProdDescricao + "',";
            if(!String.IsNullOrEmpty(dados.DepCodigo))
            {
                strSql += dados.DepCodigo.Replace(",", ".") + ",";
            }
            else
            {
                strSql += "null,";
            }

            if (!String.IsNullOrEmpty(dados.ClasCodigo))
            {
                strSql += dados.ClasCodigo.Replace(",", ".") + ",";
            }
            else
            {
                strSql += "null,";
            }

            if (!String.IsNullOrEmpty(dados.SubCodigo))
            {
                strSql += dados.SubCodigo.Replace(",", ".") + ",";
            }
            else
            {
                strSql += "null,";
            }

            strSql += Funcoes.BFormataValor(dados.ProdCusme) + ","
                + dados.ProdEstatual + ","
                + dados.ColCodigo + ")";

            return BancoDados.ExecuteNoQuery(strSql, null);
        }

        public int AtualizaSpoolBalanco(SpoolBalanco dados)
        {
            string strSql = "UPDATE SPOOL_BALANCO SET PROD_ESTATUAL = PROD_ESTATUAL + " + dados.ProdEstatual
                + " WHERE ESTACAO = '" + dados.Estacao + "' AND PROD_CODIGO = '" + dados.ProdCodigo + "'";

            return BancoDados.ExecuteNoQuery(strSql, null);
        }

        public DataTable BalancoSemQuebraSpool(int estCodigo, int empCodigo, string filtro, int ordem, bool produtosZerados)
        {
            string strSql = "SELECT A.PROD_CODIGO, A.PROD_DESCR, A.PROD_ESTATUAL, B.MED_PCO18 AS PMC, A.PROD_CUSME AS UNITARIO," 
                + " A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO ";
            strSql += " FROM SPOOL_BALANCO A"
                    + " INNER JOIN PRECOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " WHERE B.EMP_CODIGO = " + empCodigo
                    + " AND B.EST_CODIGO = " + estCodigo;

            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (ordem.Equals(0))
            {
                strSql += " ORDER BY A.PROD_DESCR";
            }
            else
            {
                strSql += " ORDER BY A.PROD_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BalancoSpoolQuebraSintetico(int estCodigo, int empCodigo, int opcaoValor, string filtro, int ordem, bool produtosZerados, int quebra, string estacao)
        {
            string strSql = "SELECT  X.PMC, ";
            if (quebra.Equals(0))
            {
                strSql += " D.DEP_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " D.CLAS_CODIGO AS CODIGO, D.CLAS_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " D.SUB_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }

            strSql += " SUM(X.PROD_ESTATUAL) AS QTDE, SUM(X.UNITARIO) AS UNITARIO, SUM(X.TOTAL_UNITARIO) AS TOTAL FROM ";

            if (quebra.Equals(0))
            {
                strSql += "(SELECT E.DEP_CODIGO, A.PROD_ESTATUAL, ";
            }
            else if (quebra.Equals(1))
            {
                strSql += "(SELECT E.CLAS_CODIGO, A.PROD_ESTATUAL,";
            }
            else if (quebra.Equals(2))
            {
                strSql += "(SELECT E.SUB_CODIGO, A.PROD_ESTATUAL,";
            }
            
            strSql += " A.PROD_CUSME AS UNITARIO, A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL_UNITARIO, C.MED_PCO18 AS PMC ";

            strSql += " FROM SPOOL_BALANCO A"
                    + " INNER JOIN PRODUTOS_DETALHE E ON E.PROD_CODIGO = A.PROD_CODIGO"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                    + " WHERE C.EMP_CODIGO = " + empCodigo
                    + " AND C.EST_CODIGO = " + estCodigo
                    + " AND E.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND E.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.ESTACAO = '" + estacao + "'";
            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += " ) X ";

            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS D ON (D.DEP_CODIGO = X.DEP_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") GROUP BY D.DEP_CODIGO, D.DEP_DESCR, X.PMC ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES D ON (D.CLAS_CODIGO = X.CLAS_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") GROUP BY D.CLAS_CODIGO, D.CLAS_DESCR, X.PMC ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES D ON (D.SUB_CODIGO = X.SUB_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") GROUP BY D.SUB_CODIGO, D.SUB_DESCR, X.PMC ";
            }

            strSql += " ORDER BY ";

            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BalancoSpoolQuebraAnalitico(int estCodigo, int empCodigo, int opcaoValor, string filtro, int ordem, bool produtosZerados, int quebra, string estacao)
        {
            string strSql = "SELECT A.PROD_CODIGO, B.PROD_DESCR, A.PROD_ESTATUAL AS QTDE , C.MED_PCO18 AS PMC, ";
            if (quebra.Equals(0))
            {
                strSql += " D.DEP_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " D.CLAS_CODIGO AS CODIGO, D.CLAS_DESCR AS DESCRICAO, ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " D.SUB_CODIGO AS CODIGO, D.DEP_DESCR AS DESCRICAO, ";
            }
            
            strSql += " A.PROD_CUSME AS UNITARIO, A.PROD_CUSME * A.PROD_ESTATUAL AS TOTAL";
            
            strSql += " FROM SPOOL_BALANCO E"
                    + " INNER JOIN PRODUTOS_DETALHE A ON E.PROD_CODIGO = A.PROD_CODIGO"
                    + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + " INNER JOIN PRECOS C ON A.PROD_CODIGO = C.PROD_CODIGO";

            if (quebra.Equals(0))
            {
                strSql += " LEFT JOIN DEPARTAMENTOS D ON (D.DEP_CODIGO = A.DEP_CODIGO AND D.EMP_CODIGO = " + empCodigo + ") ";
            }
            else if (quebra.Equals(1))
            {
                strSql += " LEFT JOIN CLASSES D ON (D.CLAS_CODIGO = A.CLAS_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")  ";
            }
            else if (quebra.Equals(2))
            {
                strSql += " LEFT JOIN SUBCLASSES D ON (D.SUB_CODIGO = A.SUB_CODIGO AND D.EMP_CODIGO = " + empCodigo + ")";
            }

            strSql += " WHERE A.EMP_CODIGO = " + empCodigo
                    + " AND A.EST_CODIGO = " + estCodigo
                    + " AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + " AND A.EST_CODIGO = C.EST_CODIGO"
                    + " AND A.PROD_SITUACAO = 'A'"
                    + " AND E.ESTACAO = '" + estacao + "'";
            if (!produtosZerados)
            {
                strSql += " AND A.PROD_ESTATUAL > 0";
            }

            if (!String.IsNullOrEmpty(filtro))
            {
                strSql += " AND " + filtro;
            }

            strSql += " ORDER BY ";

            if (ordem.Equals(0))
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_DESCR, B.PROD_DESCR";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_DESCR, B.PROD_DESCR";
                }
            }
            else
            {
                if (quebra.Equals(0))
                {
                    strSql += " D.DEP_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(1))
                {
                    strSql += " D.CLAS_CODIGO, A.PROD_CODIGO";
                }
                else if (quebra.Equals(2))
                {
                    strSql += " D.SUB_CODIGO, A.PROD_CODIGO";
                }
            }

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
