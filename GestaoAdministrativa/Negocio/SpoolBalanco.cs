﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SpoolBalanco
    {
        public string Estacao { get; set; }
        public string ProdCodigo { get; set; }
        public string ProdDescricao { get; set; }
        public string DepCodigo { get; set; }
        public string ClasCodigo { get; set; }
        public string SubCodigo { get; set; }
        public double ProdCusme { get; set; }
        public int ProdEstatual { get; set; }
        public int ColCodigo { get; set; }
    }
}
