﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaItenVendas
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int NumTrans { get; set; }
        public int NSU { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtd { get; set; }
        public double ValorMaximo { get; set; }
        public double ValorFinal { get; set; }
        public double ValorFabrica { get; set; }
        public double ValorAquisicaoUnitario { get; set; }
        public double ValorRepasse { get; set; }
        public DateTime dtVenda { get; set; }

        public EpharmaItenVendas() { }

        public bool InsereProdutos(EpharmaItenVendas dados)
        {
            string sql = " INSERT INTO EPHARMA_ITENS_VENDA ( EMP_CODIGO, EST_CODIGO, NUM_TRANS, NSU, PROD_CODIGO, "
                       + " QTD, VL_MAXIMO, VL_PFINAL, VL_PFABRICA, VL_AQUISICAO_UNITARIO, VL_REPASSE, DT_VENDA ) VALUES ("
                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.NumTrans + ","
                       + dados.NSU + ",'"
                       + dados.ProdCodigo + "',"
                       + dados.Qtd + ","
                       + dados.ValorMaximo.ToString().Replace(',','.') + ","
                       + dados.ValorFinal.ToString().Replace(',', '.') + ","
                       + dados.ValorFabrica.ToString().Replace(',', '.') + ","
                       + dados.ValorAquisicaoUnitario.ToString().Replace(',', '.') + ","
                       + dados.ValorRepasse.ToString().Replace(',', '.') + ","
                       + Funcoes.BDataHora(DateTime.Now) + ")";
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
