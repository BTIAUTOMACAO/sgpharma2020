﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class DadosNuvem
    {
        public static DataTable BuscaSomatorioDiario(DateTime data, int empCodigo, int estCodigo)
        {
            string strSql = " SELECT A.VENDA_EMISSAO AS DATA, SUM(A.VENDA_TOTAL) AS TOTAL, COUNT(A.VENDA_ID) AS VENDAS"
                          + " FROM VENDAS A"
                          + " WHERE A.VENDA_STATUS = 'F'"
                          + " AND A.EST_CODIGO = " + estCodigo
                          + " AND A.EMP_CODIGO = " + empCodigo
                          + " AND  A.VENDA_EMISSAO BETWEEN to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')  AND "
                          + " to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')"
                          + " GROUP BY A.VENDA_EMISSAO"
                          + " ORDER BY A.VENDA_EMISSAO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public static DataTable BuscaVendaPorFormaDePagamento(DateTime data, int empCodigo, int estCodigo)
        {
            string strSql = " SELECT C.VENDA_FORMA_ID,"
                          + "          D.FORMA_DESCRICAO,"
                          + "          SUM(C.VENDA_VALOR_PARCELA) AS VENDA"
                          + "     FROM VENDAS A"
                          + "    INNER JOIN VENDAS_FORMA_PAGAMENTO C ON(A.VENDA_ID = C.VENDA_ID)"
                          + "    INNER JOIN FORMAS_PAGAMENTO D ON(D.FORMA_ID = C.VENDA_FORMA_ID)"
                          + "    WHERE A.EMP_CODIGO = " + empCodigo
                          + "      AND A.EST_CODIGO = " + estCodigo
                          + "      AND A.EST_CODIGO = C.EST_CODIGO"
                          + "      AND A.EST_CODIGO = C.EST_CODIGO"
                          + "      AND D.EMP_CODIGO = A.EMP_CODIGO"
                          + "      AND A.VENDA_STATUS = 'F'"
                          + " AND  A.VENDA_EMISSAO BETWEEN to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')  AND "
                          + " to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')"
                          + " GROUP BY C.VENDA_FORMA_ID,D.FORMA_DESCRICAO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public static DataTable BuscaVendaPorEspecie(DateTime data, int empCodigo, int estCodigo)
        {
            string strSql = " SELECT SUM(VENDA) AS VENDA, ESP_DESCRICAO, ESP_CODIGO FROM (SELECT DISTINCT(C.ESP_CODIGO),"
                          + "          D.ESP_DESCRICAO,"
                          + "          SUM(C.VENDA_ESPECIE_VALOR) AS VENDA, A.VENDA_ID"
                          + "     FROM VENDAS A"
                          + "    INNER JOIN VENDAS_ESPECIES C ON(A.VENDA_ID = C.VENDA_ID)"
                          + "    INNER JOIN CAD_ESPECIES D ON(D.ESP_CODIGO = C.ESP_CODIGO)"
                          + "    WHERE A.EMP_CODIGO = " + empCodigo
                          + "      AND A.EST_CODIGO = " + estCodigo
                          + "      AND A.EST_CODIGO = C.EST_CODIGO"
                          + "      AND A.EST_CODIGO = C.EST_CODIGO"
                          + "      AND A.VENDA_STATUS = 'F'"
                          + " AND  A.VENDA_EMISSAO BETWEEN to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')  AND "
                          + " to_date('" + data.ToString("dd/MM/yyyy") + "','DD/MM/YYYY')"
                          + " GROUP BY C.ESP_CODIGO, D.ESP_DESCRICAO, C.VENDA_ESPECIE_ID, A.VENDA_ID) E "
                          + " GROUP BY E.ESP_CODIGO, E.ESP_DESCRICAO";
            return BancoDados.GetDataTable(strSql, null);
        }

        public static DataTable BuscaProdutosParaReposicao(DateTime data, int empCodigo, int estCodigo)
        {
            string sql = "SELECT A.PROD_CODIGO,"
                        + "   B.PROD_DESCR,"
                        + "   A.PROD_ESTATUAL,"
                        + "   COALESCE(SUM(C.VENDA_ITEM_QTDE), 0) AS QTDE_VENDIDA,"
                        + "   COALESCE(A.PROD_ULTCUSME, A.PROD_PRECOMPRA) AS PROD_CUSTO,"
                        + "   CASE"
                        + "     WHEN A.PROD_ESTATUAL <= COALESCE(SUM(C.VENDA_ITEM_QTDE), 0) THEN"
                        + "      COALESCE(SUM(C.VENDA_ITEM_QTDE), 0)"
                        + "     ELSE"
                        + "      CASE"
                        + "     WHEN((A.PROD_ESTATUAL - COALESCE(SUM(C.VENDA_ITEM_QTDE), 0)) <"
                        + "          COALESCE(A.PROD_ESTMIN, 1) AND"
                        + "          (A.PROD_ESTATUAL - COALESCE(SUM(C.VENDA_ITEM_QTDE), 0)) <"
                        + "          COALESCE(A.PROD_ESTMIN, 1)) THEN COALESCE(A.PROD_ESTMIN, 1)"
                        + "          ELSE 0 END"
                        + "   END AS SUGESTAO"
                        + "      FROM PRODUTOS_DETALHE A"
                        + "     INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                        + "     INNER JOIN VENDAS_ITENS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                        + "     INNER JOIN VENDAS D ON D.VENDA_ID = C.VENDA_ID"
                        + "     INNER JOIN PRECOS E ON A.PROD_CODIGO = E.PROD_CODIGO"
                        + "      LEFT JOIN FABRICANTES F ON A.FAB_CODIGO = F.FAB_CODIGO"
                        + "     WHERE A.EMP_CODIGO = " + empCodigo
                        + "       AND A.EST_CODIGO = " + estCodigo
                        + "       AND A.EMP_CODIGO = C.EMP_CODIGO"
                        + "       AND A.EST_CODIGO = C.EST_CODIGO"
                        + "       AND A.EMP_CODIGO = D.EMP_CODIGO"
                        + "       AND A.EST_CODIGO = D.EST_CODIGO"
                        + "       AND A.EMP_CODIGO = E.EMP_CODIGO"
                        + "       AND A.EST_CODIGO = E.EST_CODIGO"
                        + "       AND A.PROD_SITUACAO = 'A'"
                        + "       AND A.PROD_BLOQ_COMPRA = 'N'"
                        + "       AND A.EMP_CODIGO = F.EMP_CODIGO"
                        + "       AND D.VENDA_DATA_HORA BETWEEN"
                        + "   TO_DATE('" + data.ToString("dd/MM/yyyy") + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND"
                        + "       TO_DATE('" + data.ToString("dd/MM/yyyy") + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                        + " GROUP BY A.PROD_CODIGO,"
                        + "          B.PROD_DESCR,"
                        + "          A.PROD_ESTATUAL,"
                        + "          A.PROD_ULTCUSME,"
                        + "          A.PROD_PRECOMPRA,"
                        + "          A.PROD_ESTMIN"
                        + " ORDER BY 2";
            return BancoDados.GetDataTable(sql, null);
        }

        public static DataTable BuscaEntregaFilial()
        {
            string sql = " SELECT * FROM ENTREGA_FILIAL WHERE STATUS = 'P'";
                      
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
