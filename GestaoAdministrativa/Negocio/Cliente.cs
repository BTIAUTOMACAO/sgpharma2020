﻿using System;
using System.Collections.Generic;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class Cliente
    {
        public int CfId { get; set; }
        public string CfCodigo { get; set; }
        public string CfDocto { get; set; }
        public int CfTipoDocto { get; set; }
        public int CfStatus { get; set; }
        public string CfNome { get; set; }
        public string CfContato { get; set; }
        public string CfEndereco { get; set; }
        public string CfNumeroEndereco { get; set; }
        public string CfBairro { get; set; }
        public string CfCEP { get; set; }
        public string CfCidade { get; set; }
        public string CfUF { get; set; }
        public string CfTelefone { get; set; }
        public string CfTelRecado { get; set; }
        public string CfInscricaoEstadual { get; set; }
        public string CfObservacao { get; set; }
        public string CfApelido { get; set; }
        public string CfEnderecoCobranca { get; set; }
        public string CfBairroCobranca { get; set; }
        public string CfNumeroCobranca { get; set; }
        public string CfCEPCobranca { get; set; }
        public string CfCidadeCobranca { get; set; }
        public string CfUFCobranca { get; set; }
        public string CfFax { get; set; }
        public string CfCelular { get; set; }
        public string CfEmail { get; set; }
        public DateTime CfAdmissao { get; set; }
        public int CfFormaPag { get; set; }
        public string CfEnderecoEntrega { get; set; }
        public string CfBairroEntrega { get; set; }
        public string CfCEPEntrega { get; set; }
        public string CfCidadeEntrega { get; set; }
        public string CfUFEntrega { get; set; }
        public string CfNumeroEntrega { get; set; }
        public string CfPais { get; set; }
        public int GrupoCodigo { get; set; }
        public string CfSuframa { get; set; }
        public int CidadeCodigo { get; set; }
        public int CfEnquadramento { get; set; }
        public double CfLimite { get; set; }
        public string CfLiberado { get; set; }
        public int CodigoConveniada { get; set; }
        public string NumCartao { get; set; }
        public int PosNumero { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Cliente() { }

        public DataTable DadosCliente(Cliente dadosCliente, out string strOrdem)
        {

            string sql = " SELECT CF.CF_ID, CASE CF.CF_TIPO_DOCTO WHEN 0 THEN 'CPF' ELSE 'CNPJ' END CF_TIPO_DOCTO, "
                       + " CF.CF_CODIGO, CF.CF_DOCTO, CF.CF_NOME, CF.CF_APELIDO, GC.GRUPO_DESCR, CF.CF_ENDER, CF.CF_END_NUM, "
                       + " CF.CF_BAIRRO, CF.CF_CIDADE, CF.CF_CEP, CF.CF_UF, CF.CF_FONE, CF.CF_CELULAR, CF.CF_FONE_RECADO, "
                       + " CF.CF_ENDERCOB, CF.CF_NUMEROCOB, CF.CF_BAIRROCOB, CF.CF_CIDADECOB, CF.CF_UFCOB, CF.CF_CEPCOB, CF.CF_ENDERENT, "
                       + " CF.CF_NUMEROENT, CF.CF_CEPENT, CF.CF_BAIRROENT, CF.CF_CIDADEENT, CF.CF_UFENT, CF.CF_EMAIL, CF.CF_CONTATO, "
                       + " PS.POS_DESCRICAO, CF.CON_CODIGO, CF.CON_CARTAO, CF.CF_LIMITE, CF.DTCADASTRO, CF.OPCADASTRO, CF.OPALTERACAO, "
                       + " CF.DAT_ALTERACAO, CF.CF_OBSERVACAO, CF.CF_STATUS, "
                       + " CASE CF_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END CF_DESABILITADO, CF.CF_INSEST "
                       + " FROM CLIFOR CF "
                       + " LEFT JOIN POS PS ON (PS.POS_NUMERO = CF.POS_NUMERO  AND PS.EMP_CODIGO = " + Principal.empAtual + " AND PS.EST_CODIGO = " + Principal.estAtual + ") "
                       + " LEFT JOIN GRUPO_CLIENTES GC ON (GC.GRUPO_CODIGO = CF.GRU_CODIGO) "
                       + " WHERE  CF.CF_STATUS IN (4,5,2,3) ";

            if (dadosCliente.CfId != 0)
            {
                sql += " AND CF.CF_ID = " + dadosCliente.CfId;
            }
            if (dadosCliente.CfNome != "")
            {
                sql += " AND CF.CF_NOME LIKE '%" + dadosCliente.CfNome + "%'";
            }
            if (dadosCliente.CfDocto != "")
            {
                if (dadosCliente.CfDocto.Length <= 11)
                {
                    sql += " AND CF.CF_DOCTO = '" + Convert.ToUInt64(dadosCliente.CfDocto).ToString(@"000\.000\.000\-00") + "'";
                }
                else
                {
                    sql += " AND CF.CF_DOCTO = '" + Convert.ToUInt64(dadosCliente.CfDocto).ToString(@"00\.000\.000\/0000\-00") + "'";
                }

            }
            if (dadosCliente.GrupoCodigo != 0)
            {
                sql += " AND CF.GRU_CODIGO = '" + dadosCliente.GrupoCodigo + "'";
            }
            if (dadosCliente.CfCidade != "")
            {
                sql += " AND CF.CF_CIDADE LIKE '%" + dadosCliente.CfCidade + "%'";
            }
            if (dadosCliente.CfCodigo != "")
            {
                sql += " AND CF.CON_CODIGO = '" + dadosCliente.CfCodigo + "'";
            }
            if (dadosCliente.CfLiberado != "TODOS")
            {
                string desab = dadosCliente.CfLiberado == "S" ? "N" : "S";
                sql += " AND CF_DESABILITADO= '" + desab + "'";
            }
            strOrdem = sql;
            return BancoDados.GetDataTable(sql, null);

        }

        public bool InsereRegistros(Cliente dados)
        {

            string sql = " INSERT INTO CLIFOR (CF_ID, CF_TIPO_DOCTO, CF_DOCTO, CF_NOME , CF_APELIDO, CF_CODIGO, GRU_CODIGO, CF_ENDER, "
                       + " CF_END_NUM, CF_BAIRRO, CF_CIDADE, CF_CEP, CF_UF, CF_FONE, CF_CELULAR, CF_FONE_RECADO, CF_EMAIL, CF_CONTATO, CF_STATUS, "
                       + " POS_NUMERO, CF_DESABILITADO, CF_ENDERCOB, CF_BAIRROCOB, CF_NUMEROCOB, CF_CIDADECOB, CF_CEPCOB, CF_UFCOB, "
                       + " CF_ENDERENT, CF_BAIRROENT, CF_NUMEROENT, CF_CIDADEENT, CF_CEPENT,  CF_UFENT, DTCADASTRO, OPCADASTRO, "
                       + " CON_CARTAO, CF_LIMITE, CF_OBSERVACAO, CON_CODIGO, CF_INSEST ) VALUES ( "
                       + dados.CfId + ",'"
                       + dados.CfTipoDocto + "','"
                       + dados.CfDocto + "','"
                       + dados.CfNome + "','"
                       + dados.CfApelido + "','"
                       + dados.CfCodigo + "','"
                       + dados.GrupoCodigo + "','"
                       + dados.CfEndereco + "','"
                       + dados.CfNumeroEndereco + "','"
                       + dados.CfBairro + "','"
                       + dados.CfCidade + "','"
                       + dados.CfCEP + "','"
                       + dados.CfUF + "','"
                       + dados.CfTelefone + "','"
                       + dados.CfCelular + "','"
                       + dados.CfTelRecado + "','"
                       + dados.CfEmail + "','"
                       + dados.CfContato + "','"
                       + dados.CfStatus + "','"
                       + dados.CfFormaPag + "','"
                       + dados.CfLiberado + "','"
                       + dados.CfEnderecoCobranca + "','"
                       + dados.CfBairroCobranca + "','"
                       + dados.CfNumeroCobranca + "','"
                       + dados.CfCidadeCobranca + "','"
                       + dados.CfCEPCobranca + "','"
                       + dados.CfUFCobranca + "','"
                       + dados.CfEnderecoEntrega + "','"
                       + dados.CfBairroEntrega + "','"
                       + dados.CfNumeroEndereco + "','"
                       + dados.CfCidadeEntrega + "','"
                       + dados.CfCEPEntrega + "','"
                       + dados.CfUFEntrega + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario.ToUpper() + "','"
                       + dados.NumCartao + "','"
                       + dados.CfLimite + "','"
                       + dados.CfObservacao + "','"
                       + dados.CodigoConveniada + "','"
                       + dados.CfInscricaoEstadual + "')";
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("CF_ID", dados.CfId.ToString(), Principal.usuario, "CLIFOR", dados.CfNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerificaCPF(string docto)
        {
            string sql = "SELECT CF_DOCTO FROM CLIFOR WHERE ROWNUM <= 1 AND ";

            if (docto.Length <= 11)
            {
                sql += " CF_DOCTO = '" + Convert.ToUInt64(docto).ToString(@"000\.000\.000\-00") + "'";
            }
            else
            {
                sql += " CF_DOCTO = '" + Convert.ToUInt64(docto).ToString(@"00\.000\.000\/0000\-00") + "'";
            }
            
            if (String.IsNullOrEmpty((string)BancoDados.ExecuteScalar(sql, null)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaDados(Cliente dadosNovos, DataTable DadosAntigos)
        {
            string sql = " UPDATE CLIFOR SET "
                       + " CF_TIPO_DOCTO = " + dadosNovos.CfTipoDocto
                       + " ,CF_DOCTO = '" + dadosNovos.CfDocto + "'"
                       + " ,CF_NOME = '" + dadosNovos.CfNome + "'"
                       + " ,CF_APELIDO = '" + dadosNovos.CfApelido + "'"
                       + " ,CF_CODIGO = '" + dadosNovos.CfCodigo + "'"
                       + " ,GRU_CODIGO = " + dadosNovos.GrupoCodigo
                       + " ,CF_ENDER = '" + dadosNovos.CfEndereco + "'"
                       + " ,CF_END_NUM = '" + dadosNovos.CfNumeroEndereco + "'"
                       + " ,CF_BAIRRO = '" + dadosNovos.CfBairro + "'"
                       + " ,CF_CIDADE = '" + dadosNovos.CfCidade + "'"
                       + " ,CF_CEP =  '" + dadosNovos.CfCEP + "'"
                       + " ,CF_UF = '" + dadosNovos.CfUF + "'"
                       + " ,CF_FONE = '" + dadosNovos.CfTelefone + "'"
                       + " ,CF_CELULAR = '" + dadosNovos.CfCelular + "'"
                       + " ,CF_FONE_RECADO = '" + dadosNovos.CfTelRecado + "'"
                       + " ,CF_EMAIL = '" + dadosNovos.CfEmail + "'"
                       + " ,CF_CONTATO = '" + dadosNovos.CfContato + "'"
                       + " ,CF_STATUS = " + dadosNovos.CfStatus
                       + " ,POS_NUMERO = " + dadosNovos.CfFormaPag
                       + " ,CF_DESABILITADO = '" + dadosNovos.CfLiberado + "'"
                       + " ,CF_ENDERCOB = '" + dadosNovos.CfEnderecoCobranca + "'"
                       + " ,CF_BAIRROCOB = '" + dadosNovos.CfBairroCobranca + "'"
                       + " ,CF_NUMEROCOB = '" + dadosNovos.CfNumeroCobranca + "'"
                       + " ,CF_CIDADECOB = '" + dadosNovos.CfCidadeCobranca + "'"
                       + " ,CF_CEPCOB = '" + dadosNovos.CfCEPCobranca + "'"
                       + " ,CF_UFCOB = '" + dadosNovos.CfUFCobranca + "'"
                       + " ,CF_ENDERENT = '" + dadosNovos.CfEnderecoEntrega + "'"
                       + " ,CF_BAIRROENT = '" + dadosNovos.CfBairroEntrega + "'"
                       + " ,CF_NUMEROENT = '" + dadosNovos.CfNumeroEntrega + "'"
                       + " ,CF_CIDADEENT = '" + dadosNovos.CfCidadeEntrega + "'"
                       + " ,CF_CEPENT = '" + dadosNovos.CfCEPEntrega + "'"
                       + " ,CF_UFENT  = '" + dadosNovos.CfUF + "'"
                       + " ,CON_CARTAO = '" + dadosNovos.NumCartao + "'"
                       + " ,CF_LIMITE = '" + dadosNovos.CfLimite + "'"
                       + " ,CF_OBSERVACAO = '" + dadosNovos.CfObservacao + "'"
                       + " ,CON_CODIGO = " + dadosNovos.CodigoConveniada
                       + " , CF_INSEST = '" + dadosNovos.CfInscricaoEstadual + "'"
                       + " WHERE CF_ID = " + dadosNovos.CfId;


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                string[,] dados = new string[36, 3];
                int contMatriz = 0;

                if (!dadosNovos.CfNome.Equals(DadosAntigos.Rows[0]["CF_NOME"]))
                {
                    dados[contMatriz, 0] = "CF_NOME";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_NOME"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfNome + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfInscricaoEstadual.Equals(DadosAntigos.Rows[0]["CF_INSEST"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_INSEST";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_INSEST"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfInscricaoEstadual + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfApelido.Equals(DadosAntigos.Rows[0]["CF_APELIDO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_APELIDO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_APELIDO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfApelido + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCodigo.Equals(DadosAntigos.Rows[0]["CF_CODIGO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CODIGO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CODIGO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.GrupoCodigo.ToString().Equals(DadosAntigos.Rows[0]["GRU_CODIGO"].ToString() == "" ? "" : Convert.ToInt32(DadosAntigos.Rows[0]["GRU_CODIGO"]).ToString()))
                {

                    dados[contMatriz, 0] = "GRU_CODIGO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["GRU_CODIGO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.GrupoCodigo + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfEndereco.Equals(DadosAntigos.Rows[0]["CF_ENDER"]))
                {
                    dados[contMatriz, 0] = "CF_ENDER";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_ENDER"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfEndereco + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfNumeroEndereco.Equals(DadosAntigos.Rows[0]["CF_END_NUM"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_END_NUM";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_END_NUM"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfNumeroEndereco + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfBairro.Equals(DadosAntigos.Rows[0]["CF_BAIRRO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_BAIRRO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_BAIRRO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfBairro + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCidade.Equals(DadosAntigos.Rows[0]["CF_CIDADE"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CIDADE";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CIDADE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCidade + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCEP.Equals(DadosAntigos.Rows[0]["CF_CEP"].ToString().Replace(",00", "")))
                {
                    dados[contMatriz, 0] = "CF_CEP";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CEP"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCEP + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfUF.Equals(DadosAntigos.Rows[0]["CF_UF"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_UF";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_UF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfUF + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.CfTelefone).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_FONE"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_FONE";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_FONE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfTelefone + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.CfCelular).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_CELULAR"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_CELULAR";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CELULAR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCelular + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!Funcoes.RemoveCaracter(dadosNovos.CfTelRecado).Equals(Funcoes.RemoveCaracter(DadosAntigos.Rows[0]["CF_FONE_RECADO"].ToString())))
                {
                    dados[contMatriz, 0] = "CF_FONE_RECADO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_FONE_RECADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfTelRecado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfEmail.Equals(DadosAntigos.Rows[0]["CF_EMAIL"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_EMAIL";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_EMAIL"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfEmail + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfContato.Equals(DadosAntigos.Rows[0]["CF_CONTATO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CONTATO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CONTATO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfContato + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfStatus.ToString().Equals(DadosAntigos.Rows[0]["CF_STATUS"].ToString() == "" ? "" : Convert.ToInt32(DadosAntigos.Rows[0]["CF_STATUS"]).ToString()))
                {
                    dados[contMatriz, 0] = "CF_STATUS";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_STATUS"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfStatus + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.PosNumero.ToString().Equals(DadosAntigos.Rows[0]["POS_NUMERO"].ToString() == "" ? "" : Convert.ToInt32(DadosAntigos.Rows[0]["POS_NUMERO"]).ToString()))
                {
                    dados[contMatriz, 0] = "POS_NUMERO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["POS_NUMERO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.PosNumero + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfLiberado.Equals(DadosAntigos.Rows[0]["CF_DESABILITADO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_DESABILITADO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfEnderecoCobranca.Equals(DadosAntigos.Rows[0]["CF_ENDERCOB"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_ENDERCOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_ENDERCOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfEnderecoCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfBairroCobranca.Equals(DadosAntigos.Rows[0]["CF_BAIRROCOB"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_BAIRROCOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_BAIRROCOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfBairroCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfNumeroCobranca.Equals(DadosAntigos.Rows[0]["CF_NUMEROCOB"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_NUMEROCOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_NUMEROCOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfNumeroCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCidadeCobranca.Equals(DadosAntigos.Rows[0]["CF_CIDADECOB"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CIDADECOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CIDADECOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCidadeCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCEPCobranca.Equals(DadosAntigos.Rows[0]["CF_CEPCOB"].ToString().Replace(",00", "")))
                {
                    dados[contMatriz, 0] = "CF_CEPCOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CEPCOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCEPCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfUFCobranca.Equals(DadosAntigos.Rows[0]["CF_UFCOB"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_UFCOB";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_UFCOB"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfUFCobranca + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfEnderecoEntrega.Equals(DadosAntigos.Rows[0]["CF_ENDERENT"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_ENDERENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_ENDERENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfEnderecoEntrega + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfBairroEntrega.Equals(DadosAntigos.Rows[0]["CF_BAIRROENT"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_BAIRROENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_BAIRROENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfBairroEntrega + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfNumeroEntrega.Equals(DadosAntigos.Rows[0]["CF_NUMEROENT"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_NUMEROENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_NUMEROENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfNumeroEntrega + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCidadeEntrega.Equals(DadosAntigos.Rows[0]["CF_CIDADEENT"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_CIDADEENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CIDADEENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCidadeEntrega + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfCEPEntrega.Equals(DadosAntigos.Rows[0]["CF_CEPENT"].ToString().Replace(",00", "")))
                {
                    dados[contMatriz, 0] = "CF_CEPENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_CEPENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfCEPEntrega + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfUF.Equals(DadosAntigos.Rows[0]["CF_UFENT"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_UFENT";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_UFENT"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfUF + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.NumCartao.Equals(DadosAntigos.Rows[0]["CON_CARTAO"].ToString()))
                {
                    dados[contMatriz, 0] = "CON_CARTAO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CON_CARTAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.NumCartao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfLimite.ToString().Equals(DadosAntigos.Rows[0]["CF_LIMITE"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_LIMITE";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_LIMITE"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfLimite + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CfObservacao.Equals(DadosAntigos.Rows[0]["CF_OBSERVACAO"].ToString()))
                {
                    dados[contMatriz, 0] = "CF_OBSERVACAO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CF_OBSERVACAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CfObservacao + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.CodigoConveniada.ToString().Equals(DadosAntigos.Rows[0]["CON_CODIGO"].ToString()))
                {
                    dados[contMatriz, 0] = "CON_CODIGO";
                    dados[contMatriz, 1] = "'" + DadosAntigos.Rows[0]["CON_CODIGO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.CodigoConveniada + "'";
                    contMatriz = contMatriz + 1;
                }
                Funcoes.GravaLogAlteracao("CF_ID", dadosNovos.CfId.ToString(), Principal.usuario, "CLIFOR", dados, contMatriz, Principal.estAtual);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ExcluirDados(string numDoc)
        {

            string sql = "DELETE FROM CLIFOR WHERE CF_DOCTO = '" + numDoc + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Cliente> BuscaDadosFornecedor(int escolha, string campo, int tipoDocto)
        {
            string strSql = "SELECT CF_CODIGO, CF_NOME, CF_APELIDO, CF_OBSERVACAO, CF_DOCTO, CF_ENDER, CF_BAIRRO, CF_CIDADE, "
                              + "CF_UF, COALESCE(CON_CODIGO,0) AS CON_CODIGO, CF_ID, CF_STATUS, CF_FONE  FROM CLIFOR";
            if (escolha == 1)
            {
                strSql += " WHERE CF_NOME LIKE '%" + campo + "%'";
                strSql += " AND CF_TIPO_DOCTO = " + tipoDocto + " ORDER BY CF_NOME";
            }
            else if (escolha == 2)
            {
                strSql += " WHERE CF_DOCTO = '" + campo + "'";
                strSql += " AND CF_TIPO_DOCTO = " + tipoDocto + " ORDER BY CF_DOCTO";
            }

            List<Cliente> lista = new List<Cliente>();

            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Cliente dCli = new Cliente();
                    dCli.CodigoConveniada = Convert.ToInt32(row["CON_CODIGO"]);
                    dCli.CfId = Convert.ToInt32(row["CF_ID"]);
                    dCli.CfCodigo = row["CF_CODIGO"].ToString();
                    dCli.CfNome = row["CF_NOME"].ToString();
                    dCli.CfApelido = row["CF_APELIDO"].ToString();
                    dCli.CfObservacao = row["CF_OBSERVACAO"].ToString();
                    dCli.CfDocto = row["CF_DOCTO"].ToString();
                    dCli.CfEndereco = row["CF_ENDER"].ToString();
                    dCli.CfBairro = row["CF_BAIRRO"].ToString();
                    dCli.CfCidade = row["CF_CIDADE"].ToString();
                    dCli.CfUF = row["CF_UF"].ToString();
                    dCli.CfTelefone = row["CF_FONE"].ToString();
                    dCli.CfStatus = Convert.ToInt32(row["CF_STATUS"]);
                    lista.Add(dCli);
                }
            }

            return lista;
        }


        public DataTable DadosClienteFiltro(string campo, int escolha, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.CF_ID, A.CF_NOME, A.CF_APELIDO, A.CF_OBSERVACAO, A.CF_DOCTO, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_CODIGO, A.CF_STATUS,"
                + " B.CON_CODIGO,B.CON_ID, B.CON_WEB, B.CON_COD_CONV, A.CF_FONE, COALESCE(B.CON_DESCONTO, 0) AS CON_DESCONTO, B.CON_NOME, A.CF_END_NUM "
                + " FROM CLIFOR A"
                + " LEFT JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                + " WHERE a.cf_desabilitado = 'N' AND a.cf_status >= 2";

            if (escolha != -1)
            {
                if (escolha == 0)
                {
                    strSql += " AND A.CF_NOME LIKE '%" + campo + "%' ORDER BY A.CF_NOME";
                }
                else if (escolha == 1)
                {
                    strSql += " AND A.CF_DOCTO = '" + campo + "' ORDER BY A.CF_NOME";
                }
                if (escolha == 2)
                {
                    strSql += " AND A.CF_ID = " + campo + " ORDER BY A.CF_ID";
                }
                else if (escolha == 3)
                {
                    strSql += " AND A.CF_FONE = '" + campo + "' ORDER BY A.CF_FONE";
                }
                else if (escolha == 4)
                {
                    strSql += " AND A.CF_CODIGO = '" + campo + "' ORDER BY A.CF_CODIGO";
                }


                strOrdem = strSql;
            }
            else
            {
                strOrdem = strSql;
                strSql += " ORDER BY CF_ID";
            }


            return BancoDados.GetDataTable(strSql, null);
        }

        public List<Cliente> DadosClienteCfDocto(string cfDocto)
        {
            string strSql = "SELECT CF_NOME, CF_DOCTO, CF_INSEST, CF_ENDER, CF_BAIRRO, CF_CEP, CF_CIDADE, CF_UF, CF_FONE, CF_CODIGO, CF_END_NUM, CF_ID, "
                + " CF_TIPO_DOCTO,COALESCE(CF_LIMITE,0) AS CF_LIMITE, CF_STATUS, CF_EMAIL "
                + " FROM CLIFOR WHERE CF_DOCTO = '" + cfDocto + "'";

            List<Cliente> lista = new List<Cliente>();
            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    Cliente dCliente = new Cliente();
                    dCliente.CfId = Convert.ToInt32(row["CF_ID"]);
                    dCliente.CfTipoDocto = Convert.ToInt32(row["CF_TIPO_DOCTO"]);
                    dCliente.CfNome = row["CF_NOME"].ToString();
                    dCliente.CfDocto = row["CF_DOCTO"].ToString();
                    dCliente.CfInscricaoEstadual = row["CF_INSEST"].ToString();
                    dCliente.CfEndereco = row["CF_ENDER"].ToString();
                    dCliente.CfBairro = row["CF_BAIRRO"].ToString();
                    dCliente.CfCEP = row["CF_CEP"].ToString();
                    dCliente.CfCidade = row["CF_CIDADE"].ToString();
                    dCliente.CfUF = row["CF_UF"].ToString();
                    dCliente.CfTelefone = row["CF_FONE"].ToString();
                    dCliente.CfCodigo = row["CF_CODIGO"].ToString();
                    dCliente.CfNumeroEndereco = row["CF_END_NUM"].ToString();
                    dCliente.CfLimite = Convert.ToDouble(row["CF_LIMITE"]);
                    dCliente.CfStatus = Convert.ToInt32(row["CF_STATUS"]);
                    dCliente.CfEmail = row["CF_EMAIL"].ToString();
                    lista.Add(dCliente);
                }
            }
            return lista;
        }


        public DataTable SelecionaDadosDoClientePorConveniada(string cartao, string conCodigo)
        {
            string strSql = "SELECT * FROM CLIFOR WHERE CON_CARTAO = '" + cartao + "' AND CON_CODIGO = " + conCodigo;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable SelecionaDadosDoClientePorConveniadaFuncional(string cartao)
        {
            string strSql = "SELECT * FROM CLIFOR WHERE CON_CARTAO = '" + cartao + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool IncluiClienteConsultaCatrtao(Cliente dados, out int idCliente)
        {
            int id = Funcoes.IdentificaVerificaID("CLIFOR", "CF_ID");
            idCliente = id;
            string strCmd = "INSERT INTO CLIFOR (CF_ID,CF_DOCTO,CF_TIPO_DOCTO,CF_NOME,CF_ENDER,CF_END_NUM,CF_BAIRRO,CF_CEP,CF_CIDADE,CF_UF,CF_STATUS,CF_ENDERCOB,CF_NUMEROCOB,"
                + "CF_BAIRROCOB,CF_CEPCOB,CF_CIDADECOB,CF_UFCOB,CF_ENDERENT,CF_NUMEROENT,CF_BAIRROENT,CF_CEPENT,CF_CIDADEENT,CF_UFENT,CF_PAIS,CF_DESABILITADO,CON_CODIGO,"
                + "CON_CARTAO,DTCADASTRO,OPCADASTRO, CF_FONE) VALUES ("
                + id.ToString() + ",'"
                + dados.CfDocto + "',"
                + dados.CfTipoDocto + ",'"
                + dados.CfNome + "','"
                + dados.CfEndereco + "','"
                + dados.CfNumeroEndereco + "','"
                + dados.CfBairro + "',"
                + dados.CfCEP.Replace("-", "") + ",'"
                + dados.CfCidade + "','"
                + dados.CfUF + "',"
                + dados.CfStatus + ",'"
                + dados.CfEnderecoCobranca + "','"
                + dados.CfNumeroCobranca + "','"
                + dados.CfBairroCobranca + "',"
                + dados.CfCEPCobranca.Replace("-", "") + ",'"
                + dados.CfCidadeCobranca + "','"
                + dados.CfUFCobranca + "','"
                + dados.CfNumeroEntrega + "','"
                + dados.CfNumeroEntrega + "','"
                + dados.CfBairroEntrega + "',"
                + dados.CfCEPEntrega.Replace("-", "") + ",'"
                + dados.CfCidadeEntrega + "','"
                + dados.CfUFEntrega + "','"
                + dados.CfPais + "','"
                + dados.CfLiberado + "',";
            if (dados.CodigoConveniada == 0)
            {
                strCmd += "null,";
            }
            else
                strCmd += dados.CodigoConveniada + ",";

            if (String.IsNullOrEmpty(dados.NumCartao))
            {
                strCmd += "null,";
            }
            else
                strCmd += "'" + dados.NumCartao + "',";

            strCmd += Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "','" + dados.CfTelefone + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("CF_ID", id.ToString(), Principal.usuario, "CLIFOR", dados.CfNome, Principal.estAtual, Principal.empAtual);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaClienteConsultaCatrtao(Cliente dados)
        {
            try
            {
                string strCmd = "UPDATE CLIFOR SET CF_DOCTO = '" + dados.CfDocto + "',"
                    + " CF_NOME = '" + dados.CfNome + "',"
                    + " CF_ENDER = '" + dados.CfEndereco + "',"
                    + " CF_END_NUM = '" + dados.CfNumeroEndereco + "',"
                    + " CF_BAIRRO = '" + dados.CfBairro + "',"
                    + " CF_CEP = " + dados.CfCEP.Replace("-", "") + ","
                    + " CF_CIDADE = '" + dados.CfCidade + "',"
                    + " CF_UF = '" + dados.CfUF + "',"
                    + " CF_ENDERCOB = '" + dados.CfEnderecoCobranca + "',"
                    + " CF_NUMEROCOB = '" + dados.CfNumeroCobranca + "',"
                    + " CF_BAIRROCOB = '" + dados.CfBairroCobranca + "',"
                    + " CF_CEPCOB = " + dados.CfCEPCobranca.Replace("-", "") + ","
                    + " CF_CIDADECOB = '" + dados.CfCidadeCobranca + "',"
                    + " CF_UFCOB = '" + dados.CfUFCobranca + "',"
                    + " CF_ENDERENT = '" + dados.CfEnderecoEntrega + "',"
                    + " CF_NUMEROENT = '" + dados.CfNumeroEntrega + "',"
                    + " CF_BAIRROENT = '" + dados.CfBairroEntrega + "',"
                    + " CF_CEPENT = " + dados.CfCEPEntrega.Replace("-", "") + ","
                    + " CF_CIDADEENT = '" + dados.CfCidadeEntrega + "',"
                    + " CF_UFENT = '" + dados.CfUFEntrega + "',"
                    + " OPALTERACAO = '" + dados.OpAlteracao + "',"
                    + " DAT_ALTERACAO = " + Funcoes.BDataHora(dados.DtAlteracao)
                    + " WHERE CF_ID = " + dados.CfId + " AND CF_DOCTO = '" + dados.CfDocto + "'";
                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable SelecionaDadosDoClientePorDocto(string cfDocto)
        {
            string strSql = "SELECT * FROM CLIFOR WHERE CF_DOCTO = '" + cfDocto + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public string BuscaClientePorDataVenda(DateTime dtRecebimento)
        {

            string sql = " SELECT C.CF_NOME "
                       + " FROM COBRANCA_MOVIMENTO CM "
                       + " INNER JOIN COBRANCA_PARCELA CP ON(CP.COBRANCA_PARCELA_ID = CM.COBRANCA_PARCELA_ID)"
                       + " INNER JOIN COBRANCA COB ON(COB.COBRANCA_ID = CP.COBRANCA_ID) "
                       + " INNER JOIN CLIFOR C ON(C.CF_ID = COB.COBRANCA_CF_ID)"
                       + " WHERE CP.DT_RECEBIMENTO = " + Funcoes.BDataHora(dtRecebimento);

            return (string)BancoDados.ExecuteScalar(sql, null);

        }

        public DataTable SelecionaDadosDoClientePorVendaID(long vendaID, int empCodigo, int estCodigo)
        {
            string strSql = "SELECT A.* FROM CLIFOR A "
                + " INNER JOIN VENDAS B ON A.CF_ID = B.VENDA_CF_ID"
                + " WHERE B.VENDA_ID = " + vendaID
                + " AND B.EST_CODIGO = " + estCodigo
                + " AND B.EMP_CODIGO = " + empCodigo;

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DadosClientePorEmpresaConveniada(string convCodigo)
        {
            string strSql;

            strSql = "SELECT A.CF_ID, A.CF_NOME, A.CF_APELIDO, A.CF_OBSERVACAO, A.CF_DOCTO, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_CODIGO, A.CF_STATUS,"
                + " B.CON_CODIGO,B.CON_ID, B.CON_WEB, B.CON_COD_CONV, A.CF_FONE, COALESCE(B.CON_DESCONTO, 0) AS CON_DESCONTO, B.CON_NOME "
                + " FROM CLIFOR A"
                + " LEFT JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                + " WHERE a.cf_desabilitado = 'N' AND a.cf_status >= 2"
                + " AND A.CON_CODIGO = " + convCodigo + " ORDER BY A.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DadosClientePorEmpresaConveniadaFuncional(string con_cartao)
        {
            string strSql;

            strSql = "SELECT A.CF_ID, A.CF_NOME, A.CF_APELIDO, A.CF_OBSERVACAO, A.CF_DOCTO, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_CODIGO, A.CF_STATUS,"
                + " COALESCE(B.CON_CODIGO,7000) AS CON_CODIGO,COALESCE(B.CON_ID,0) as CON_ID, B.CON_WEB, B.CON_COD_CONV, A.CF_FONE, COALESCE(B.CON_DESCONTO, 0) AS CON_DESCONTO, B.CON_NOME "
                + " FROM CLIFOR A"
                + " LEFT JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                + " WHERE a.cf_desabilitado = 'N' AND a.cf_status >= 2"
                + " AND A.CON_CARTAO = " + con_cartao + " ORDER BY A.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable DadosClientePorCfCodigo(string cfCodigo)
        {
            string strSql;

            strSql = "SELECT A.CF_ID, A.CF_NOME, A.CF_APELIDO, A.CF_OBSERVACAO, A.CF_DOCTO, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_CODIGO, A.CF_STATUS,"
                + " B.CON_CODIGO,B.CON_ID, B.CON_WEB, B.CON_COD_CONV, A.CF_FONE, COALESCE(B.CON_DESCONTO, 0) AS CON_DESCONTO, B.CON_NOME "
                + " FROM CLIFOR A"
                + " LEFT JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                + " WHERE a.cf_desabilitado = 'N' AND a.cf_status >= 2"
                + " AND A.CF_CODIGO = '" + cfCodigo + "' ORDER BY A.CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }


        public DataTable DadosClienteTelaBusca(string empresa, string campo, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.CF_ID, A.CF_NOME, A.CF_APELIDO, A.CF_OBSERVACAO, A.CF_DOCTO, A.CF_ENDER, A.CF_BAIRRO, A.CF_CIDADE, A.CF_UF, A.CF_CODIGO, A.CF_STATUS,"
                + " B.CON_CODIGO,B.CON_ID, B.CON_WEB, B.CON_COD_CONV, A.CF_FONE, COALESCE(B.CON_DESCONTO, 0) AS CON_DESCONTO, B.CON_NOME "
                + " FROM CLIFOR A"
                + " LEFT JOIN CONVENIADAS B ON A.CON_CODIGO = B.CON_CODIGO"
                + " WHERE a.cf_desabilitado = 'N' AND a.cf_status >= 2"
                + " AND A.CF_NOME LIKE '" + campo + "%'";
            if(!String.IsNullOrEmpty(empresa))
            {
                strSql += " AND A.CON_CODIGO = " + empresa;
            }
                
            strOrdem = strSql;
            strSql += " ORDER BY CF_NOME";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaClientePorCPF(string cpf)
        {
            string sql = "SELECT CF_ID, CF_NOME FROM CLIFOR WHERE CF_DOCTO LIKE '" + cpf + "'";

            return BancoDados.GetDataTable(sql, null);
        }


        public string BuscaNomePorDocto(string docto)
        {
            string sql = "SELECT CF_NOME FROM CLIFOR WHERE CF_DOCTO = '" + docto + "'";

            var r = BancoDados.ExecuteScalar(sql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public DataTable BucaClientesPorEmpresaConveniada(string empresa, string nome)
        {
            string sql = " SELECT A.CF_ID,"
                        + "           A.CF_CODIGO,"
                        + "           A.CF_NOME,"
                        + "           A.CF_ENDER,"
                        + "           A.CF_END_NUM,"
                        + "           A.CF_BAIRRO,"
                        + "           A.CF_CIDADE,"
                        + "           A.CF_UF,"
                        + "           A.CF_FONE"
                        + "      FROM CLIFOR A"
                        + "      WHERE A.CON_CODIGO = " + empresa
                        + "      AND a.cf_desabilitado = 'N' AND a.cf_status >= 2";
            if (!String.IsNullOrEmpty(nome))
            {
                sql += " AND A.CF_NOME = '" + nome + "'";
            }
            sql += " ORDER BY A.CF_NOME";
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaClientePorID(int id)
        {
            string sql = "SELECT * FROM CLIFOR WHERE CF_ID = " + id;

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
