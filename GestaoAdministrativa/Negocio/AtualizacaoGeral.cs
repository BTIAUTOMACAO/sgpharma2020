﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class AtualizacaoGeral
    {
        public string Aplicativo { get; set; }
        public string Versao { get; set; }
        public string Mensagem { get; set; }
        public string EnderecoAtualizacao { get; set; }
        public string EnderecoFp { get; set; }
        public string GeradorDNA { get; set; }
        public string CodErro { get; set; }
        public string MensagemErro { get; set; }
    }
}
