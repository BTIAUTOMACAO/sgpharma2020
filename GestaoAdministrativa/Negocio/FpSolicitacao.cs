﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FpSolicitacao
    {
        public string SolicitacaoID { get; set; }
        public string DnaEstacao { get; set; }
        public DateTime DtEmissaoReceita { get; set; }
        public string Cnpj { get; set; }
        public string CpfVendedor { get; set; }
        public string CpfCliente { get; set; }
        public string NomeCliente { get; set; }
        public string CrmMedico { get; set; }
        public string UfMedico { get; set; }
        public string NumeroAutorizacao { get; set; }
        public string RetornoAutorizacao { get; set; }
        public string MensagemSolicitacao { get; set; }
        public string RetornoConfirmacao { get; set; }
        public string MensagemConfirmacao { get; set; }
        public string RetornoRecebimento { get; set; }
        public string MensagemRecebimento { get; set; }
        public string StatusEstorno { get; set; }
        public string MensagemEstorno { get; set; }
        public string Cupom { get; set; }
        public string SituacaoSolicitacao { get; set; }
        public DateTime DataVenda { get; set; }
        public DateTime DataAlteracao { get; set; }
        public long VendaID { get; set; }

        public FpSolicitacao() { }

        public string BuscaCupomPorNsu(string nsu)
        {
            string strSql = "SELECT CUPOM FROM FP_SOLICITACAO WHERE NUMEROAUTORIZACAO = '" + nsu + "'";

            return BancoDados.ExecuteScalar(strSql, null).ToString();
        }

        public bool AtualizaVendaIdPorNsu(string nsu, long vendaID)
        {
            string strCmd = "UPDATE FP_SOLICITACAO SET VENDA_ID = " + vendaID + " WHERE NUMEROAUTORIZACAO = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
