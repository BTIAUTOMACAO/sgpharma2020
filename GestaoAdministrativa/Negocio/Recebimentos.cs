﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Recebimentos
    {
        public DataTable BuscaProdutosVendas(int cobranaID)
        {

            string sql = " SELECT VEN.PROD_CODIGO,  VEN.VENDA_ITEM_QTDE , VEN.VENDA_ITEM_UNITARIO,VEN.VENDA_ITEM_DIFERENCA, VEN.VENDA_ITEM_TOTAL,"
                       + " PRO.PROD_DESCR,PRO.PROD_UNIDADE "
                       + " FROM VENDAS_ITENS VEN "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = VEN.PROD_CODIGO) "
                       + " WHERE VEN.VENDA_ID = (SELECT COBRANCA_VENDA_ID FROM COBRANCA WHERE COBRANCA_ID = " + cobranaID + ") ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaClienteComCobranca(string nome, string documento, int codigo, string telefone, string statusCobranca, out string strOrdem)
        {

            string sql = " SELECT CF_NOME, CF_APELIDO, CF_DOCTO, CF_ENDER, CF_BAIRRO, CF_CIDADE, CF_FONE, "
                       + " CF_UF, CF_OBSERVACAO, CF_ID "
                       + " FROM CLIFOR "
                       + " WHERE CF_ID IN (SELECT COBRANCA_CF_ID FROM COBRANCA WHERE 1=1 ";
            if (statusCobranca != "TODOS")
            {
                string status = statusCobranca == "SIM" ? "A" : "F";
                sql += "AND COBRANCA_STATUS = '" + status + "'";
            }
            sql += " GROUP BY COBRANCA_CF_ID )";

            if (!String.IsNullOrEmpty(nome))
            {
                sql += " AND  CF_NOME LIKE '%" + nome + "%'";
            }
            if (!String.IsNullOrEmpty(documento))
            {
                sql += " AND CF_DOCTO ='" + documento + "'";
            }
            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(telefone)))
            {
                sql += " AND CF_FONE = '" + telefone + "'";
            }
            if (!codigo.Equals(0))
            {
                sql += " AND CF_ID = " + codigo;
            }

            sql += " ORDER BY CF_NOME";
            strOrdem = sql;
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaRecebimentosPorPeriodo(DateTime dtInicial, DateTime dtFinal, int clienteID)
        {
            string sql = " SELECT SUM(CP.COBRANCA_PARCELA_VALOR) as COBRANCA_PARCELA_VALOR, "
                       + " CP.DT_RECEBIMENTO "
                       + " FROM COBRANCA_PARCELA CP "
                       + " INNER JOIN COBRANCA COB ON(COB.COBRANCA_ID = CP.COBRANCA_ID) "
                       + " WHERE(CP.COBRANCA_PARCELA_SALDO = 0 OR CP.COBRANCA_PARCELA_SALDO != CP.COBRANCA_PARCELA_VALOR) "
                       + " AND CP.DT_RECEBIMENTO BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND COB.COBRANCA_CF_ID = " + clienteID
                       + " GROUP BY CP.DT_RECEBIMENTO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaEspeciesVendaPorData(DateTime dtRecebimento)
        {
            string sql = " SELECT MCE.MOVIMENTO_CX_ESPECIE_VALOR , ES.ESP_DESCRICAO "
                       + " FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ES ON(ES.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE MCE.MOVIMENTO_CX_ESPECIE_DATA = " + Funcoes.BDataHora(dtRecebimento);

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaDataComprovantePorPerido(DateTime dtInicial, DateTime dtFinal, char status, int cfID)
        {
            string sql = " SELECT A.DT_RECEBIMENTO FROM COBRANCA_PARCELA A "
                       + " INNER JOIN COBRANCA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                       + " WHERE A.COBRANCA_PARCELA_STATUS = '" + status + "' AND "
                       + " A.DT_RECEBIMENTO BETWEEN  " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND A.EMP_CODIGO = " + Principal.empAtual
                       + " AND A.EST_CODIGO = " + Principal.estAtual
                       + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                       + " AND A.EST_CODIGO = A.EST_CODIGO"
                       + " AND B.COBRANCA_CF_ID = " + cfID
                       + " GROUP BY DT_RECEBIMENTO ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaVendaIDComprovantePorPerido(DateTime dtInicial, DateTime dtFinal, char status, int cfID)
        {
            string sql = " SELECT A.DT_RECEBIMENTO, B.COBRANCA_ID FROM COBRANCA_PARCELA A "
                       + " INNER JOIN COBRANCA B ON A.COBRANCA_ID = B.COBRANCA_ID"
                       + " WHERE A.COBRANCA_PARCELA_STATUS = '" + status + "' AND "
                       + " A.DT_RECEBIMENTO BETWEEN  " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND A.EMP_CODIGO = " + Principal.empAtual
                       + " AND A.EST_CODIGO = " + Principal.estAtual
                       + " AND A.EMP_CODIGO = B.EMP_CODIGO"
                       + " AND A.EST_CODIGO = A.EST_CODIGO"
                       + " AND B.COBRANCA_CF_ID = " + cfID
                       + " GROUP BY A.DT_RECEBIMENTO,B.COBRANCA_ID ";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaRecebimentosPorEspeciePeriodo(DateTime dtInicial, DateTime dtFinal, string usuario , int operacaoDC) {
            string sql = " SELECT SUM(MOVIMENTO_CX_ESPECIE_VALOR) AS TOTAL, ESP.ESP_DESCRICAO FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE MCE.MOVIMENTO_CX_ESPECIE_DATA IN (SELECT MOVIMENTO_CAIXA_DATA FROM MOVIMENTO_CAIXA  WHERE MOVIMENTO_CAIXA_ODC_ID = "+ operacaoDC + " AND MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) +  " AND " + Funcoes.BDataHora(dtFinal) + " ) "
                       + " AND MCE.Movimento_Cx_Especie_Usuario = '"+ usuario + "'"
                       + " GROUP BY ESP.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }
        
        public DataTable RecebimentosPorPeriodoParticular(DateTime dtInicial, DateTime dtFinal, string usuario, int operacaoDC)
        {
            string sql = " SELECT SUM(MOVIMENTO_CX_ESPECIE_VALOR) AS TOTAL, ESP.ESP_CODIGO FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE MCE.MOVIMENTO_CX_ESPECIE_DATA IN (SELECT MOVIMENTO_CAIXA_DATA FROM MOVIMENTO_CAIXA  WHERE MOVIMENTO_CAIXA_ODC_ID = " + operacaoDC + " AND MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal) + " ) "
                       + " AND MCE.Movimento_Cx_Especie_Usuario = '" + usuario + "'"
                       + " GROUP BY ESP.ESP_CODIGO";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
