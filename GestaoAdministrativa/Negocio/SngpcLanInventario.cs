﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcLanInventario
    {
        public int SngpcLacID { get; set; }
        public int SngpcLacEmp { get; set; }
        public int SngpcLacEst { get; set; }
        public string SngpcLacInvMS { get; set; }
        public string SngpcLacInvCodigo { get; set; }
        public string SngpcLacInvLote { get; set; }
        public int SngpcLacInvQtde { get; set; }

        public SngpcLanInventario() { }

        public SngpcLanInventario(int sngpcLacID, int sngpcLacEmp, int sngpcLacEst, string sngpcLacInvCodigo, string sngpcLacInvLote,
                                string sngpcLacInvMS, int sngpcLacInvQtde)
        {
            this.SngpcLacID = sngpcLacID;
            this.SngpcLacEmp = sngpcLacEmp;
            this.SngpcLacEst = sngpcLacEst;
            this.SngpcLacInvCodigo = sngpcLacInvCodigo;
            this.SngpcLacInvLote = sngpcLacInvLote;
            this.SngpcLacInvQtde = sngpcLacInvQtde;
            this.SngpcLacInvMS = sngpcLacInvMS;

        }

        public DataTable BuscaInventario(SngpcLanInventario dados, out string strOrdem)
        {
            string sql = " SELECT SN.LAC_INV_ID, SN.LAC_INV_CODIGO, SN.LAC_INV_MS, SN.LAC_INV_LOTE, SN.LAC_INV_QTDE, SN.DT_CADASTRO,SN.OP_CADASTRO,  "
                       + " SN.OP_ALTERACAO, SN.DT_ALTERACAO, "
                       + " PROD.PROD_DESCR, PROD.PROD_UNIDADE, "
                       + " CASE PROD.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' ELSE 'SUJEITO A CONTROLE ESPECIAL' END AS PROD_CLASSE_TERAP ,PROD.PROD_CONTROLADO "
                       + " FROM SNGPC_LAC_INVENTARIO SN "
                       + " INNER JOIN PRODUTOS PROD ON(SN.LAC_INV_CODIGO = PROD.PROD_CODIGO) "
                       + " WHERE EMP_CODIGO = " + Principal.empAtual;


            if (dados.SngpcLacInvMS != "")
            {
                sql += " AND COD_BARRA = '" + dados.SngpcLacInvMS + "'";
            }
            if (dados.SngpcLacInvCodigo != "")
            {
                sql += " AND LAC_INV_CODIGO = '" + dados.SngpcLacInvCodigo + "'";
            }
            if (dados.SngpcLacInvLote != "")
            {
                sql += " AND LAC_INV_LOTE = '" + dados.SngpcLacInvLote + "'";
            }
            strOrdem = sql;
            sql += " ORDER BY SN.LAC_INV_ID";

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaSNGPC(SngpcLanInventario dadosNovos, DataTable dadosAntigos)
        {
            try
            {
                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                string sql = " UPDATE SNGPC_LAC_INVENTARIO SET ";

                if (dadosNovos.SngpcLacInvMS != dadosAntigos.Rows[0]["LAC_INV_MS"].ToString())
                {
                    sql += " LAC_INV_MS ='" + dadosNovos.SngpcLacInvMS + "', ";

                    dados[contMatriz, 0] = "LAC_INV_MS";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["LAC_INV_MS"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.SngpcLacInvMS.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }

                if (dadosNovos.SngpcLacInvLote != dadosAntigos.Rows[0]["LAC_INV_LOTE"].ToString())
                {
                    sql += " LAC_INV_LOTE ='" + dadosNovos.SngpcLacInvLote + "', ";

                    dados[contMatriz, 0] = "LAC_INV_LOTE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["LAC_INV_LOTE"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.SngpcLacInvLote.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }
                if (dadosNovos.SngpcLacInvQtde != Convert.ToUInt32(dadosAntigos.Rows[0]["LAC_INV_QTDE"]))
                {
                    sql += " LAC_INV_QTDE ='" + dadosNovos.SngpcLacInvQtde + "', ";

                    dados[contMatriz, 0] = "LAC_INV_QTDE";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["LAC_INV_QTDE"].ToString() + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.SngpcLacInvQtde.ToString() + "'";
                    contMatriz = contMatriz + 1;
                }

                if (contMatriz != 0)
                {

                    sql += " OP_ALTERACAO = '" + Principal.usuario + "', ";
                    sql += " DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now);
                    sql += " WHERE LAC_INV_ID = " + dadosNovos.SngpcLacID + " AND LAC_INV_CODIGO = '" + dadosNovos.SngpcLacInvCodigo + "'";

                    if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                    {
                        Funcoes.GravaLogAlteracao("LAC_INV_ID", dadosNovos.SngpcLacID.ToString(), Principal.usuario, "SNGPC_LAC_INVENTARIO", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return true;
                }

            }
            catch (Exception)
            {
                return false;
            }


        }

        public bool Incluir(SngpcLanInventario dados)
        {
            string sql = " INSERT INTO SNGPC_LAC_INVENTARIO (LAC_INV_ID, LAC_INV_CODIGO, LAC_INV_MS, LAC_INV_LOTE, LAC_INV_QTDE, "
                       + " DT_CADASTRO, OP_CADASTRO , EMP_CODIGO, EST_CODIGO, LAC_INV_ENVIADO) VALUES ("
                       + dados.SngpcLacID + " , '"
                       + dados.SngpcLacInvCodigo.Trim() + "' , '"
                       + dados.SngpcLacInvMS + "' , '"
                       + dados.SngpcLacInvLote + "' ,"
                       + dados.SngpcLacInvQtde + " , "
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "'," 
                       + Principal.empAtual + ","
                       + Principal.estAtual + ",'N')";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("LAC_INV_ID", dados.SngpcLacID.ToString(), Principal.usuario.ToUpper(), "SNGPC_LAC_INVENTARIO", dados.SngpcLacInvCodigo, Principal.empAtual);
                return true;
            }
            else
                return false;

        }

        public string BuscaLote(string codigo, string lote) {

            string sql = " SELECT LAC_INV_LOTE FROM SNGPC_LAC_INVENTARIO WHERE  ROWNUM <= 1 AND LAC_INV_LOTE = '" + lote + "' AND LAC_INV_CODIGO = '"+ codigo + "'";

            return (string)BancoDados.ExecuteScalar(sql, null);

        }

        public bool ExcluiDados(int SngpcLacID)
        {

            string sql = "DELETE FROM SNGPC_LAC_INVENTARIO WHERE LAC_INV_ID =" + SngpcLacID;

            if (BancoDados.ExecuteNoQueryTrans (sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool ExcluirTodosRegistros()
        {
            string sql = "DELETE FROM SNGPC_LAC_INVENTARIO";
            if (!BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
