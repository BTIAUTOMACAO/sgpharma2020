﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Parametro
    {
        public int ParTabela { get; set; }
        public string ParPosicao { get; set; }
        public string ParEstacao { get; set; }
        public string ParDescricao { get; set; }
        public string ParComent { get; set; }
        public int ParSeq { get; set; }
        public string ParModulo { get; set; }
        public string ParObservacao { get; set; }

        public Parametro() { }

        public Parametro(int parTabela, string parPosicao, string parEstacao, string parDescricao, string parComent, int parSeq, string parModulo, string parObservacao)
        {
            this.ParTabela = parTabela;
            this.ParPosicao = parPosicao;
            this.ParEstacao = parEstacao;
            this.ParDescricao = parDescricao;
            this.ParComent = parComent;
            this.ParSeq = parSeq;
            this.ParModulo = parModulo;
            this.ParObservacao = parObservacao;
        }

        public DataTable BuscarParametrosPorModulo(string modulo)
        {
            string strSql = "SELECT PAR_TABELA, PAR_POSICAO, PAR_ESTACAO, PAR_DESCRICAO AS PAR_DESCR, PAR_COMENT, PAR_OBS FROM PARAMETROS WHERE PAR_MODULO = '" + modulo + "' ORDER BY PAR_POSICAO";

            return BancoDados.GetDataTable(strSql, null);
        }

        public string IdentificaEstacao(Parametro dados)
        {
            string strSql = "SELECT PAR_ESTACAO FROM PARAMETROS"
                        + " WHERE PAR_TABELA = " + dados.ParTabela  + " AND PAR_POSICAO = '" + dados.ParPosicao + "' AND PAR_ESTACAO = '" + dados.ParEstacao + "'";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return "";
            else
                return r.ToString();
        }

        public bool AtualizaParDescricao(Parametro dados)
        {
            string retorno = Funcoes.LeParametro(dados.ParTabela, dados.ParPosicao, false);

            string estacao = IdentificaEstacao(dados);

            string strCmd = "UPDATE PARAMETROS SET PAR_DESCRICAO = '" + dados.ParDescricao + "' WHERE PAR_TABELA = " + dados.ParTabela + " AND PAR_POSICAO = '" + dados.ParPosicao + "' AND PAR_MODULO = '" + dados.ParModulo + "'";

            if(!String.IsNullOrEmpty(estacao))
            {
                strCmd += " AND PAR_ESTACAO = '" + dados.ParEstacao + "'";
            }

            if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
            {
                strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA,"
                    + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO) VALUES ('PAR_TABELA','" + dados.ParTabela
                    + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PARAMETROS','PAR_DESCRICAO', ";
                strCmd += retorno == "" ? "null," : "'" + retorno + "', ";
                strCmd += dados.ParDescricao == "" ? "null)" : "'" + dados.ParDescricao + "')";
                BancoDados.ExecuteNoQuery(strCmd, null);

                return true;
            }
            else
                return false;
        }
    }
}
