﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class BeneficioNovartisProdutos
    {
        public string NSU { get; set; }
        public long VendaID { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public double PrecoBruto { get; set; }
        public double PrecoLiquido { get; set; }
        public double ValorReceberLoja { get; set; }
        public double ValorDesconto { get; set; }
        public string Status { get; set; }
        public string Cartao { get; set; }
        public DateTime Data { get; set; }
        public double ValorSubsidio { get; set; }
        public string Reposicao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public BeneficioNovartisProdutos() { }

        public BeneficioNovartisProdutos(string nsu, long vendaId, string prodCodigo, int qtde, double precoBruto, double precoLiquido, double valorReceberLoja,
            double valorDesconto, string status, string cartao, DateTime data, double valorSubsidio, string reposicao, DateTime dtCadastro, string opCadastro)
        {
            this.NSU = nsu;
            this.VendaID = vendaId;
            this.ProdCodigo = prodCodigo;
            this.Qtde = qtde;
            this.PrecoBruto = precoBruto;
            this.PrecoLiquido = precoLiquido;
            this.ValorReceberLoja = valorReceberLoja;
            this.ValorDesconto = valorDesconto;
            this.Status = status;
            this.Cartao = cartao;
            this.Data = data;
            this.ValorSubsidio = valorSubsidio;
            this.Reposicao = reposicao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
        }

        public bool InsereRegistros(BeneficioNovartisProdutos dados, bool transAberta = true)
        {
            string strCmd = "INSERT INTO BENEFICIO_NOVARTIS_PRODUTOS(NSU, PROD_CODIGO, QTD, PRE_BRUTO, PRE_LIQUIDO, VALOR_REC_LOJA, VALOR_DESCONTO,"
                + "STATUS, CARTAO, DATA, VALOR_SUBSIDIO, REPOSICAO, DTCADASTRO, OPCADASTRO) VALUES ('"
                + dados.NSU + "','"
                + dados.ProdCodigo + "',"
                + dados.Qtde + ","
                + Funcoes.BValor(dados.PrecoBruto) + ","
                + Funcoes.BValor(dados.PrecoLiquido) + ","
                + Funcoes.BValor(dados.ValorReceberLoja) + ","
                + Funcoes.BValor(dados.ValorDesconto) + ",'"
                + dados.Status + "','"
                + dados.Cartao + "',"
                + Funcoes.BData(dados.Data) + ","
                + Funcoes.BValor(dados.ValorSubsidio) + ",'"
                + dados.Reposicao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (transAberta)
            {
                if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }
        
        }

        public int ExcluirDadosPorNSU(string nsu)
        {
            string strCmd = "DELETE FROM BENEFICIO_NOVARTIS_PRODUTOS WHERE NSU = '" + nsu + "'";

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable BuscaProdutosPorNsuSomatorio(string nsu)
        {
            string strSql;

            strSql = "SELECT CASE A.STATUS WHEN 'A' THEN 'APROVADO' ELSE 'SEM DESCONTO' END AS STATUS,"
                       + " A.PROD_CODIGO, B.PROD_DESCR, A.QTD, A.PRE_BRUTO * A.QTD AS PRE_BRUTO,  A.PRE_LIQUIDO * A.QTD AS PRE_LIQUIDO,"
                       + " A.VALOR_REC_LOJA * A.QTD AS VALOR_REC_LOJA, A.VALOR_DESCONTO, A.NSU"
                       + " FROM BENEFICIO_NOVARTIS_PRODUTOS A"
                       + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                       + " WHERE NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosPorNsu(string nsu)
        {
            string strSql;

            strSql = "SELECT CASE A.STATUS WHEN 'A' THEN 'APROVADO' ELSE 'SEM DESCONTO' END AS STATUS,"
                       + " A.PROD_CODIGO, B.PROD_DESCR, A.QTD, A.PRE_BRUTO,  A.PRE_LIQUIDO,"
                       + " A.VALOR_REC_LOJA, A.VALOR_DESCONTO, A.NSU"
                       + " FROM BENEFICIO_NOVARTIS_PRODUTOS A"
                       + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                       + " WHERE NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        public DataTable BuscaProdutosPorNsuProdCodigo(string nsu, string prodCodigo)
        {
            string strSql;

            strSql = "SELECT CASE A.STATUS WHEN 'A' THEN 'APROVADO' ELSE 'SEM DESCONTO' END AS STATUS,"
                       + " A.PROD_CODIGO, B.PROD_DESCR, A.QTD, A.PRE_BRUTO,  A.PRE_LIQUIDO,"
                       + " A.VALOR_REC_LOJA, A.VALOR_DESCONTO, A.NSU"
                       + " FROM BENEFICIO_NOVARTIS_PRODUTOS A"
                       + " INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                       + " WHERE NSU = '" + nsu + "'"
                       + " AND A.PROD_CODIGO = '" + prodCodigo + "'";

            return BancoDados.GetDataTable(strSql, null);
        }


        public bool AtualizaStatusCartaoPorNsu(string nsu, string status, string cartao, long vendaID)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_PRODUTOS SET STATUS = '" + status + "', CARTAO = '" + cartao + "', VENDA_ID = " + vendaID + " WHERE NSU = '" + nsu + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }


        public bool AtualizaQtdEValoresProdutos(string nsu, int qtde, double bruto, double liquido, double valorReceber, double valorDesconto, string prodCodigo)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_PRODUTOS SET QTD = " + qtde + ", PRE_BRUTO = " + Funcoes.BValor(bruto) + ", PRE_LIQUIDO = " + Funcoes.BValor(liquido) 
                + " , VALOR_REC_LOJA = " + Funcoes.BValor(valorReceber) + ", VALOR_DESCONTO = " + Funcoes.BValor(valorDesconto)
                + " WHERE NSU = '" + nsu + "' AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDescontoProduto(string nsu, double valorDesconto, string prodCodigo)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_PRODUTOS SET VALOR_DESCONTO = " + Funcoes.BValor(valorDesconto)
                + " WHERE NSU = '" + nsu + "' AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }


        public bool AtualizaQtdeProduto(string nsu, int  qtde, string prodCodigo)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_PRODUTOS SET QTD = " + qtde
                + " WHERE NSU = '" + nsu + "' AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluiProdutoPorNSUeCodBarras(string nsu, string prodCodigo)
        {
            string strCmd = "DELETE FROM BENEFICIO_NOVARTIS_PRODUTOS WHERE NSU = '" + nsu + "' AND PROD_CODIGO = '" + prodCodigo + "'";

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public bool AtualizaSubsidioProduto(string nsu, double valorSubsidio, string prodCodigo)
        {
            string strCmd = "UPDATE BENEFICIO_NOVARTIS_PRODUTOS SET VALOR_SUBSIDIO = " + Funcoes.BValor(valorSubsidio)
                + " WHERE NSU = '" + nsu + "' AND PROD_CODIGO = '" + prodCodigo + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable BuscaValorDoSubsidio(string nsu)
        {
            string strSql;
            strSql = "SELECT A.VALOR_SUBSIDIO * A.QTD AS VALOR_SUBSIDIO"
                       + " FROM BENEFICIO_NOVARTIS_PRODUTOS A"
                       + " WHERE NSU = '" + nsu + "'";

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
