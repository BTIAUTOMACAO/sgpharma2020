﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class PedidosContador
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long PedNumero { get; set; }
        public string PedComplemento { get; set; }

        public PedidosContador() { }

        public PedidosContador(int empCodigo, int estCodigo, long pedNumero, string pedComplemento)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.PedNumero = pedNumero;
            this.PedComplemento = pedComplemento;
        }

        public bool InsereRegistros(PedidosContador dados)
        {
            string strCmd = "INSERT INTO PEDIDOS_CONTADOR(EMP_CODIGO, EST_CODIGO, PED_NUMERO, PED_COMPL) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo  + "," +
                dados.PedNumero + ",'" +
                dados.PedComplemento + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
