﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class MovimentoCaixa
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long MovimentoCaixaID { get; set; }
        public DateTime MovimentoCaixaData { get; set; }
        public int MovimentoCaixaOdcID { get; set; }
        public string MovimentoCaixaOdcClasse { get; set; }
        public double MovimentoCaixaValor { get; set; }
        public string MovimentoCaixaUsuario { get; set; }
        public long MovimentoFinanceiroID { get; set; }
        public long MovimentoFinanceiroSeq { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string MovimentoCaixaObs { get; set; }
        public long VendaID { get; set; }

        public MovimentoCaixa() { }

        public MovimentoCaixa(int empCodigo, int estCodigo, long movimentoCaixaID, DateTime movimentoCaixaData, int movimentoCaixaOdbID, string movimentoCaixaOdcClasse,
            double movimentoCaixaValor, string movimentoCaixaUsuario, long movimentoFinanceiroID, long movimentoFinanceiroSeq, DateTime dtCadastro, string opCadastro, string movimentoCaixaObs, long vendaID)
        {
            this.EmpCodigo = estCodigo;
            this.EstCodigo = estCodigo;
            this.MovimentoCaixaID = movimentoCaixaID;
            this.MovimentoCaixaData = movimentoCaixaData;
            this.MovimentoCaixaOdcID = movimentoCaixaOdbID;
            this.MovimentoCaixaOdcClasse = movimentoCaixaOdcClasse;
            this.MovimentoCaixaValor = movimentoCaixaValor;
            this.MovimentoCaixaUsuario = movimentoCaixaUsuario;
            this.MovimentoFinanceiroID = movimentoFinanceiroID;
            this.MovimentoFinanceiroSeq = movimentoFinanceiroSeq;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.MovimentoCaixaObs = movimentoCaixaObs;
            this.VendaID = vendaID;
        }

        public bool InserirDados(MovimentoCaixa dados)
        {
            string strCmd = "INSERT INTO MOVIMENTO_CAIXA(EMP_CODIGO,EST_CODIGO,MOVIMENTO_CAIXA_ID,MOVIMENTO_CAIXA_DATA,MOVIMENTO_CAIXA_ODC_ID,MOVIMENTO_CAIXA_ODC_CLASSE,MOVIMENTO_CAIXA_VALOR,"
                + "MOVIMENTO_CAIXA_USUARIO,MOVIMENTO_FINANCEIRO_ID,MOVIMENTO_FINANCEIRO_SEQ, DT_CADASTRO,OP_CADASTRO, MOVIMENTO_CAIXA_OBS, VENDA_ID) VALUES (" + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.MovimentoCaixaID + ","
                + Funcoes.BDataHora(dados.MovimentoCaixaData) + ","
                + dados.MovimentoCaixaOdcID + ",'"
                + dados.MovimentoCaixaOdcClasse + "',"
                + Funcoes.BValor(dados.MovimentoCaixaValor) + ",'"
                + dados.MovimentoCaixaUsuario + "',"
                + dados.MovimentoFinanceiroID + ","
                + dados.MovimentoFinanceiroSeq + ","
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "','"
                + dados.MovimentoCaixaObs + "',"
                + dados.VendaID + ")";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        public DataTable VerficaMovimentoCaixa(string usuario, DateTime dtInicial, DateTime dtFinal)
        {

            string sql = " SELECT * FROM MOVIMENTO_CAIXA WHERE MOVIMENTO_CAIXA_USUARIO ='" + usuario + "' AND "
                       + " MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal);

            return BancoDados.GetDataTable(sql, null);

        }

        public DataTable BuscaDados(MovimentoCaixa dados, out string strOrdem)
        {
            string sql = " SELECT MC.EST_CODIGO, MC.MOVIMENTO_CAIXA_ID, MC.MOVIMENTO_CAIXA_DATA, MC.MOVIMENTO_CAIXA_USUARIO, MOVIMENTO_FINANCEIRO_ID, "
                       + " MC.MOVIMENTO_CAIXA_ODC_ID, ODC.ODC_DESCRICAO, CASE ODC.ODC_CLASSE WHEN 0 THEN 'CREDITO' WHEN 1 THEN 'DEBITO'  WHEN 2 THEN 'RETIRADA' END AS ODC_CLASSE, "
                       + " MC.MOVIMENTO_CAIXA_VALOR, MC.DT_CADASTRO, MC.OP_CADASTRO , MOVIMENTO_CAIXA_OBS"
                       + " FROM MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " WHERE MC.EST_CODIGO = " + dados.EmpCodigo + " AND ODC.ODC_TIPO = 'C'";

            if (String.Format("{0:MM/dd/yyyy}", dados.MovimentoCaixaData) != "01/01/0001")
            {
                sql += " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(Convert.ToDateTime((dados.MovimentoCaixaData.ToString("dd/MM/yyyy") + " 00:00:00 ")))
                     + " AND " + Funcoes.BDataHora(Convert.ToDateTime((dados.MovimentoCaixaData.ToString("dd/MM/yyyy") + " 23:59:59 ")));
            }
            if (dados.MovimentoCaixaValor != 0)
            {
                sql += " AND MC.MOVIMENTO_CAIXA_VALOR = " + dados.MovimentoCaixaValor;
            }
            if (dados.MovimentoCaixaOdcID != -1)
            {
                sql += " AND MC.MOVIMENTO_CAIXA_ODC_ID = " + dados.MovimentoCaixaOdcID;
            }
            if(!String.IsNullOrEmpty(dados.MovimentoCaixaUsuario))
            {
                sql += " AND MC.MOVIMENTO_CAIXA_USUARIO = '" + dados.MovimentoCaixaUsuario + "'"; 
            }
            strOrdem = sql;

            sql += " ORDER BY MC.MOVIMENTO_CAIXA_ID DESC ";
            
            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaMovimentoCaixaPorEspecieData(DateTime dtMovimento)
        {
            string sql = " SELECT MCE.MOVIMENTO_CX_ESPECIE_CODIGO, MCE.MOVIMENTO_CX_ESPECIE_VALOR, ESP.ESP_DESCRICAO "
                       + " FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ESP ON (ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE MOVIMENTO_CX_ESPECIE_DATA = " + Funcoes.BDataHora(dtMovimento);
            return BancoDados.GetDataTable(sql, null);
        }

        public bool ExcluiMovimentacao(int idMovimento)
        {
            string sql = "DELETE FROM MOVIMENTO_CAIXA WHERE MOVIMENTO_CAIXA_ID = " + idMovimento;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExcluiMovimentoCaixaPorDataHora(DateTime dtMovimento)
        {
            string sql = "DELETE FROM MOVIMENTO_CAIXA WHERE MOVIMENTO_CAIXA_DATA = " + Funcoes.BDataHora(dtMovimento);

            try
            {
                BancoDados.ExecuteNoQueryTrans(sql, null);
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public bool ExcluirPorData(DateTime dtRecebimento)
        {
            string sql = "DELETE FROM MOVIMENTO_CAIXA WHERE MOVIMENTO_CAIXA_DATA = " + Funcoes.BDataHora(dtRecebimento);

            var retorno = BancoDados.ExecuteNoQueryTrans(sql, null);
            if (!retorno.Equals(0))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public DataTable SomaMovimentoPorOperacao(List<object> excecoes, DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string operacaoDCExcecoes = string.Join(" ,", excecoes);

            string sql = " SELECT SUM(MOVIMENTO_CAIXA_VALOR) AS VALOR, ODC.ODC_DESCRICAO from MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " WHERE MOVIMENTO_CAIXA_ODC_ID NOT IN(" + operacaoDCExcecoes + ")  "
                       + " AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' "
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND  MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' AND ODC.ODC_TIPO = 'C'"
                       + " GROUP BY  ODC.ODC_DESCRICAO "
                       + " ORDER BY  ODC.ODC_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaMovimentoPorOperacaoDetalhadoEntrada(string excecoes, DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT ME.MOVIMENTO_CX_ESPECIE_VALOR AS VALOR, ODC.ODC_DESCRICAO, MC.MOVIMENTO_CAIXA_OBS, ESP.ESP_DESCRICAO, ESP.ESP_CODIGO from MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE ME ON (MC.MOVIMENTO_CAIXA_DATA = ME.MOVIMENTO_CX_ESPECIE_DATA)"
                       + " INNER JOIN CAD_ESPECIES ESP ON(ME.MOVIMENTO_CX_ESPECIE_CODIGO = ESP.ESP_CODIGO)"
                       + " WHERE MOVIMENTO_CAIXA_ODC_ID IN(" + excecoes + ")  "
                       + " AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '+' "
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND  (MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " ORDER BY  ESP.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaMovimentoPorOperacaoDetalhado(List<object> excecoes, DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string operacaoDCExcecoes = string.Join(" ,", excecoes);

            string sql = " SELECT ME.MOVIMENTO_CX_ESPECIE_VALOR AS VALOR, ODC.ODC_DESCRICAO, MC.MOVIMENTO_CAIXA_OBS, ESP.ESP_DESCRICAO, ESP.ESP_CODIGO from MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE ME ON (MC.MOVIMENTO_CAIXA_DATA = ME.MOVIMENTO_CX_ESPECIE_DATA)"
                       + " INNER JOIN CAD_ESPECIES ESP ON(ME.MOVIMENTO_CX_ESPECIE_CODIGO = ESP.ESP_CODIGO)"
                       + " WHERE MOVIMENTO_CAIXA_ODC_ID NOT IN(" + operacaoDCExcecoes + ")  "
                       + " AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' "
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND  (MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " ORDER BY  ESP.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaMovimentoPorClassePorEspecie(DateTime dtInicial, DateTime dtFinal, string usuario, string classe)
        {
            string sql = " SELECT SUM(MCE.MOVIMENTO_CX_ESPECIE_VALOR) AS VALOR ,SUBSTR(ESP.ESP_DESCRICAO,1 ,3)AS ESP_DESCRICAO, MC.MOVIMENTO_CAIXA_OBS"
                       + " FROM MOVIMENTO_CAIXA_ESPECIE MCE "
                       + " INNER JOIN CAD_ESPECIES ESP ON (ESP.ESP_CODIGO = MCE.EST_CODIGO )"
                       + " INNER JOIN MOVIMENTO_CAIXA MC ON(MC.MOVIMENTO_CAIXA_DATA = MCE.MOVIMENTO_CX_ESPECIE_DATA) "
                       + " WHERE MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' AND MC.MOVIMENTO_CAIXA_ODC_CLASSE = '" + classe + "' AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " GROUP BY  ESP.ESP_DESCRICAO, MC.MOVIMENTO_CAIXA_OBS ";

            return BancoDados.GetDataTable(sql, null);
        }
        public DataTable BuscaMovimentosPorOperacacao(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT TO_CHAR(MC.MOVIMENTO_CAIXA_DATA, 'DD/MM/YYY') AS DATA , "
                       + " (COALESCE(SUM(CASE WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '+' THEN  MC.MOVIMENTO_CAIXA_VALOR END), 0) - COALESCE(SUM(CASE "
                       + " WHEN MC.MOVIMENTO_CAIXA_ODC_CLASSE = '-' THEN  MC.MOVIMENTO_CAIXA_VALOR END), 0)) AS VALOR, "
                       + " ODC.ODC_DESCRICAO , MC.MOVIMENTO_CAIXA_ODC_CLASSE "
                       + " FROM MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " WHERE (MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " GROUP BY ODC.ODC_DESCRICAO, TO_CHAR(MC.MOVIMENTO_CAIXA_DATA, 'DD/MM/YYY'),  MC.MOVIMENTO_CAIXA_ODC_CLASSE "
                       + " ORDER BY TO_CHAR(MC.MOVIMENTO_CAIXA_DATA, 'DD/MM/YYY') , ODC.ODC_DESCRICAO  DESC";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaMovimentosPorOperacaoEspecie(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT A.DATA, SUM(A.VALOR) AS VALOR, A.ODC_DESCRICAO, A.MOVIMENTO_CAIXA_ODC_CLASSE, A.ESP_DESCRICAO FROM ("
                       + " SELECT distinct MC.MOVIMENTO_CAIXA_ID, TO_CHAR(MC.MOVIMENTO_CAIXA_DATA, 'DD/MM/YYY') AS DATA ,  MCE.MOVIMENTO_CX_ESPECIE_VALOR AS VALOR, "
                       + " ODC.ODC_DESCRICAO , MC.MOVIMENTO_CAIXA_ODC_CLASSE , ESP.ESP_DESCRICAO "
                       + " FROM MOVIMENTO_CAIXA MC "
                       + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE MCE ON(MCE.MOVIMENTO_CX_ESPECIE_DATA = MC.MOVIMENTO_CAIXA_DATA) "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE (MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' or MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario + " ')"
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND ODC.ODC_TIPO = 'C'"
                       + " ORDER BY TO_CHAR(MC.MOVIMENTO_CAIXA_DATA, 'DD/MM/YYY')) A"
                       + "     GROUP BY A.DATA,"
                       + "               A.ODC_DESCRICAO,"
                       + "               A.MOVIMENTO_CAIXA_ODC_CLASSE,"
                       + "               A.ESP_DESCRICAO";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaMovimentoPorOperacao(DateTime dtInicial, DateTime dtFinal, string usuario, int operacao) {
            string sql = " SELECT DISTINCT MC.MOVIMENTO_CAIXA_ID ,MC.MOVIMENTO_CAIXA_DATA, MCE.MOVIMENTO_CX_ESPECIE_VALOR, ESP.ESP_DESCRICAO,MC.MOVIMENTO_CAIXA_ODC_ID "
                       + " FROM MOVIMENTO_CAIXA MC "
                       + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE MCE ON(MCE.MOVIMENTO_CX_ESPECIE_DATA = MC.MOVIMENTO_CAIXA_DATA) "
                       + " INNER JOIN CAD_ESPECIES ESP ON(ESP.ESP_CODIGO = MCE.MOVIMENTO_CX_ESPECIE_CODIGO) "
                       + " WHERE MC.MOVIMENTO_CAIXA_ODC_ID = "+operacao+" AND (MC.MOVIMENTO_CAIXA_USUARIO = '"+usuario+ "' OR MC.MOVIMENTO_CAIXA_USUARIO = '" + usuario +" ')"
                       + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN "+ Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " ORDER BY MC.MOVIMENTO_CAIXA_DATA";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable SomaTrocoPorEspeciePeriodo(DateTime dtInicial, DateTime dtFinal, string usuario, int operacaoDC)
        {
            string sql = " SELECT MC.MOVIMENTO_CAIXA_VALOR AS VALOR, ODC.ODC_DESCRICAO, MC.MOVIMENTO_CAIXA_OBS, ESP.ESP_DESCRICAO from MOVIMENTO_CAIXA MC "
                      + " INNER JOIN OPERACOES_DC ODC ON(ODC.ODC_NUM = MC.MOVIMENTO_CAIXA_ODC_ID) "
                      + " INNER JOIN MOVIMENTO_CAIXA_ESPECIE ME ON (MC.MOVIMENTO_CAIXA_DATA = ME.MOVIMENTO_CX_ESPECIE_DATA)"
                      + " INNER JOIN CAD_ESPECIES ESP ON(ME.MOVIMENTO_CX_ESPECIE_CODIGO = ESP.ESP_CODIGO)"
                      + " WHERE MOVIMENTO_CAIXA_ODC_ID = " + operacaoDC
                      + " AND MC.MOVIMENTO_CAIXA_DATA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                      + " AND  MOVIMENTO_CAIXA_USUARIO = '" + usuario + "' AND ODC.ODC_TIPO = 'C'"
                      + " ORDER BY  ESP.ESP_DESCRICAO";
            
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
