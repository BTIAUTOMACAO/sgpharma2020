﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;
using SqlNegocio;

namespace GestaoAdministrativa.Negocio
{
    public class TipoDocto
    {
        public int TipCodigo { get; set; }
        public string TipDescr { get; set; }
        public string TipoDescAbrev { get; set; }
        public string TipFatura { get; set; }
        public string TipEmiteNF { get; set; }
        public string TipLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public TipoDocto() { }

        public TipoDocto(int tipCodigo, string tipDescr, string tipoDescAbrev, string tipFatura, string tipEmiteNF,
                         string tipLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.TipCodigo = tipCodigo;
            this.TipDescr = tipDescr;
            this.TipoDescAbrev = tipoDescAbrev;
            this.TipFatura = tipFatura;
            this.TipEmiteNF = tipEmiteNF;
            this.TipLiberado = tipLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscaDados(TipoDocto dados, out string strOrdem)
        {

            string sql = " SELECT TIP_CODIGO, TIP_DESCRICAO,TIP_DESC_ABREV, TIP_FATURA, DAT_ALTERACAO, "
                       + " CASE TIP_DESABILITADO  WHEN 'N' THEN 'S' "
                       + " ELSE 'N' END AS TIP_DESABILITADO,  "
                       + " TIP_EMITE_NF, TIP_OPALTERACAO, TIP_OPCADASTRO, TIP_DTCADASTRO "
                       + " FROM TIPO_DOCTO WHERE 1=1 ";

            if (dados.TipCodigo != 0)
            {
                sql += " AND TIP_CODIGO = " + dados.TipCodigo;
            }
            if (dados.TipDescr != "")
            {
                sql += " AND TIP_DESCRICAO LIKE '%" + dados.TipDescr + "%'";
            }
            if (dados.TipLiberado != "TODOS")
            {
                string desab = dados.TipLiberado == "S" ? "N" : "S";
                sql += "AND TIP_DESABILITADO = '" + desab + "'";
            }

            strOrdem = sql;
            sql += " ORDER BY TIP_CODIGO";
            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaDados(TipoDocto dadosNovos, DataTable dadosAntigos)
        {

            string sql = " UPDATE TIPO_DOCTO SET "
                       + " TIP_DESCRICAO = '" + dadosNovos.TipDescr + "'"
                       + " ,TIP_DESC_ABREV = '" + dadosNovos.TipoDescAbrev + "'"
                       + " ,TIP_FATURA = '" + dadosNovos.TipFatura + "'"
                       + " ,TIP_DESABILITADO = '" + dadosNovos.TipLiberado + "'"
                       + " ,TIP_EMITE_NF = '" + dadosNovos.TipEmiteNF + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now)
                       + " ,TIP_OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " WHERE TIP_CODIGO = " + dadosNovos.TipCodigo;


            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[5, 3];
                int contMatriz = 0;

                if (!dadosNovos.TipDescr.Equals(dadosAntigos.Rows[0]["TIP_DESCRICAO"].ToString()))
                {

                    dados[contMatriz, 0] = "TIP_DESCRICAO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TIP_DESCRICAO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TipDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TipoDescAbrev.Equals(dadosAntigos.Rows[0]["TIP_DESC_ABREV"].ToString()))
                {
                    dados[contMatriz, 0] = "TIP_DESC_ABREV";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TIP_DESC_ABREV"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TipoDescAbrev + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TipFatura.Equals(dadosAntigos.Rows[0]["TIP_FATURA"].ToString()))
                {
                    dados[contMatriz, 0] = "TIP_FATURA";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TIP_FATURA"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TipFatura + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TipLiberado.Equals(dadosAntigos.Rows[0]["TIP_DESABILITADO"].ToString()))
                {
                    dados[contMatriz, 0] = "TIP_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TIP_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TipLiberado + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TipEmiteNF.Equals(dadosAntigos.Rows[0]["TIP_EMITE_NF"].ToString()))
                {
                    dados[contMatriz, 0] = "TIP_EMITE_NF";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TIP_EMITE_NF"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TipEmiteNF + "'";
                    contMatriz = contMatriz + 1;
                }


                Funcoes.GravaLogAlteracao("TIP_CODIGO", dadosNovos.TipCodigo.ToString(), dadosNovos.OpAlteracao, "TIPO_DOCTO", dados, contMatriz, 1);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool InserirDados(TipoDocto dadosNovos)
        {

            string sql = " INSERT INTO TIPO_DOCTO( TIP_CODIGO, TIP_DESCRICAO ,TIP_DESC_ABREV, TIP_FATURA, TIP_DESABILITADO, TIP_EMITE_NF, "
                       + " TIP_DTCADASTRO, TIP_OPCADASTRO)  VALUES ( "
                       + dadosNovos.TipCodigo + ",'"
                       + dadosNovos.TipDescr + "','"
                       + dadosNovos.TipoDescAbrev + "','"
                       + dadosNovos.TipFatura + "','"
                       + dadosNovos.TipLiberado + "','"
                       + dadosNovos.TipEmiteNF + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + dadosNovos.OpCadastro + "')";


            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("TIP_CODIGO", dadosNovos.TipCodigo.ToString(), dadosNovos.OpCadastro, "TIPO_DOCTO", dadosNovos.TipDescr, 1);
                return true;
            }
            else
                return false;
        }
        public bool ExcluirDados(string tipoCodigo) {
            
            string sql = "DELETE FROM TIPO_DOCTO WHERE TIP_CODIGO = "+  tipoCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
