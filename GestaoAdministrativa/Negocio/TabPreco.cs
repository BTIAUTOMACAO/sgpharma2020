﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class TabPreco
    {
        public int EstCodigo { get; set; }
        public int TabCodigo { get; set; }
        public string TabDescr { get; set; }
        public string TabDesabilitado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public TabPreco() { }

        public TabPreco(int estCodigo, int tabCodigo, string tabDescr, string tabDesabilitado, DateTime dtAlteracao,
                        string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EstCodigo = estCodigo;
            this.TabCodigo = tabCodigo;
            this.TabDescr = tabDescr;
            this.TabDesabilitado = tabDesabilitado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscaDados(string tabId, string tabDescricao, string tabDesabilitado, out string strOrdem)
        {
            string sql = " SELECT EMP_CODIGO, TAB_CODIGO, TAB_DESCR, CASE TAB_DESABILITADO "
                       + " WHEN 'N' THEN 'S' "
                       + " ELSE 'N' END  AS TAB_DESABILITADO , "
                       + " DAT_ALTERACAO, OPALTERACAO, DTCADASTRO, OPCADASTRO FROM TAB_PRECOS WHERE EMP_CODIGO = " + Principal.estAtual;

            if (tabId != "")
            {
                sql += " AND TAB_CODIGO = " + tabId;
            }
            if (tabDescricao != "")
            {
                sql += " AND TAB_DESCR LIKE '%" + tabDescricao + "%'";
            }
            if (tabDesabilitado != "TODOS")
            {
                string desab = tabDesabilitado == "S" ? "N" : "S";
                sql += " AND TAB_DESABILITADO = '" + desab + "'";
            }
            strOrdem = sql;
            sql += " ORDER BY TAB_CODIGO";

            return BancoDados.GetDataTable(sql, null);
        }


        public bool InserirDados(TabPreco dados)
        {
            string sql = "INSERT INTO TAB_PRECOS (EMP_CODIGO, TAB_CODIGO, TAB_DESCR, TAB_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES ("
                       + dados.EstCodigo + ","
                       + dados.TabCodigo + ",'"
                       + dados.TabDescr + "','"
                       + dados.TabDesabilitado + "',"
                       + Funcoes.BDataHora(dados.DtAlteracao) + ",'"
                       + Principal.usuario.ToUpper() + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                Funcoes.GravaLogInclusao("TAB_CODIGO", dados.TabCodigo.ToString(), Principal.usuario, "TAB_PRECOS", dados.TabDescr.ToUpper(), dados.EstCodigo);
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(TabPreco dadosNovos, DataTable dadosAntigos)
        {
            string sql = " UPDATE TAB_PRECOS SET"
                       + " TAB_DESCR = '" + dadosNovos.TabDescr + "'"
                       + " ,TAB_DESABILITADO = '" + dadosNovos.TabDesabilitado + "'"
                       + " ,DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao)
                       + " ,OPALTERACAO = '" + dadosNovos.OpAlteracao + "'"
                       + " WHERE TAB_CODIGO = " + dadosNovos.TabCodigo;

            if (BancoDados.ExecuteNoQuery(sql, null) != -1)
            {
                string[,] dados = new string[4, 3];
                int contMatriz = 0;

                if (!dadosNovos.TabDescr.Equals(dadosAntigos.Rows[0]["TAB_DESCR"]))
                {
                    dados[contMatriz, 0] = "TAB_DESCR";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_DESCR"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabDescr + "'";
                    contMatriz = contMatriz + 1;
                }
                if (!dadosNovos.TabDesabilitado.Equals(dadosAntigos.Rows[0]["TAB_DESABILITADO"]))
                {
                    dados[contMatriz, 0] = "TAB_DESABILITADO";
                    dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["TAB_DESABILITADO"] + "'";
                    dados[contMatriz, 2] = "'" + dadosNovos.TabDesabilitado + "'";
                    contMatriz = contMatriz + 1;
                }

                Funcoes.GravaLogAlteracao("TAB_CODIGO", dadosNovos.TabCodigo.ToString(), Principal.usuario, "TAB_PRECOS", dados, contMatriz, dadosNovos.EstCodigo);

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ExcluirDados(string idTabPreco)
        {

            string sql = "DELETE FROM TAB_PRECOS WHERE EMP_CODIGO= " + Principal.empAtual + " AND TAB_CODIGO = " + Math.Truncate(Convert.ToDouble(idTabPreco));

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;


        }
    }
}
