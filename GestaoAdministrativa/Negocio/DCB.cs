﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class DCB
    {
        public string NumeroDCB { get; set; }
        public string Descricao { get; set; }
        public string NumCas { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public DCB() { }

        public DCB(string numeroDCB, string descricao, string numCas)
        {
            this.NumeroDCB = numeroDCB;
            this.Descricao = descricao;
            this.NumCas = numCas;
        }
        
        public bool InsereRegistros(DCB dados)
        {
            string strCmd = "INSERT INTO DCB (NUM_DCB, DESCRICAO,CAS) VALUES('"
                + dados.NumeroDCB + "','"
                + dados.Descricao + "','"
                + dados.NumCas + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool ExcluiRegistroPorDCB(DCB dados)
        {
            string sql = "DELETE FROM DCB WHERE NUM_DCB = '" + dados.NumeroDCB + "'";
            BancoDados.ExecuteNoQuery(sql, null);
            return true;
        }
    }
}
