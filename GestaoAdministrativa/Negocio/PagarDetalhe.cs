﻿using System;
using GestaoAdministrativa.Classes;
using System.Collections.Generic;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    public class PagarDetalhe
    {
        public int EmpCodigo { get; set; }
        public int PagCodigo { get; set; }
        public string PagDocto { get; set; }
        public string CfDocto { get; set; }
        public int PdParcela { get; set; }
        public double PdValor { get; set; }
        public DateTime PdVencimento { get; set; }
        public double PdSaldo { get; set; }
        public string PdStatus { get; set; }
        public string PdTransf { get; set; }
        public string PdDocumentoCobranca { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }


        public PagarDetalhe(){}

        public bool InsereRegistroFaturaDetalhe(PagarDetalhe dadosPagarDetalhe)
        {
            string strSql = "INSERT INTO PAGAR_DETALHE(EMP_CODIGO, PAG_CODIGO, PAG_DOCTO, CF_DOCTO, PD_PARCELA, PD_VALOR, PD_VENCTO, PD_SALDO, PD_STATUS, "
                + "DTCADASTRO, OPCADASTRO) VALUES (" + dadosPagarDetalhe.EmpCodigo + ","
                + dadosPagarDetalhe.PagCodigo + ",'"
                + dadosPagarDetalhe.PagDocto + "','"
                + dadosPagarDetalhe.CfDocto + "',"
                + dadosPagarDetalhe.PdParcela + ","
                + Funcoes.BValor(dadosPagarDetalhe.PdValor) + ","
                + Funcoes.BDataHora(dadosPagarDetalhe.PdVencimento) + ","
                + Funcoes.BValor(dadosPagarDetalhe.PdSaldo) + ",'"
                + dadosPagarDetalhe.PdStatus + "',"
                + Funcoes.BDataHora(dadosPagarDetalhe.DtCadastro) + ",'"
                + dadosPagarDetalhe.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strSql, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        public int ExcluiFaturaDetalhe(int empCodigo, string identificador, bool porID)
        {
            if(porID.Equals(true))
                return BancoDados.ExecuteNoQueryTrans("DELETE FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + empCodigo + " AND PAG_CODIGO = " + identificador, null);
            else
                return BancoDados.ExecuteNoQueryTrans("DELETE FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + empCodigo + " AND PAG_DOCTO = '" + identificador + "'", null);
        }

        public List<PagarDetalhe> BuscaParcelas(int empCodigo, int pagCodigo)
        {
            string strSql = "SELECT A.PD_VALOR, A.PD_VENCTO, A.PD_STATUS, A.PD_SALDO FROM PAGAR_DETALHE A  WHERE A.EMP_CODIGO = " + empCodigo
                     + " AND A.PAG_CODIGO = " + pagCodigo + " ORDER BY A.PD_PARCELA";

            List<PagarDetalhe> lista = new List<PagarDetalhe>();

            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    PagarDetalhe dPagto = new PagarDetalhe();
                    dPagto.PdValor = Convert.ToDouble(row["PD_VALOR"]);
                    dPagto.PdVencimento = Convert.ToDateTime(row["PD_VENCTO"]);
                    dPagto.PdStatus = row["PD_STATUS"].ToString();
                    dPagto.PdSaldo = Convert.ToDouble(row["PD_SALDO"]);
                    lista.Add(dPagto);
                }
            }

            return lista;
        }

        public bool Quitacao(PagarDetalhe pgDetalhe)
        {
            string strCmd = "UPDATE PAGAR_DETALHE SET PD_SALDO = 0, PD_STATUS = 'Q', DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = '" + Principal.usuario + "'"
                 + " WHERE EMP_CODIGO = " + pgDetalhe.EmpCodigo + " AND PAG_CODIGO = " + pgDetalhe.PagCodigo
                 + " AND PD_PARCELA = " + pgDetalhe.PdParcela;

            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable VerificaParcelasPagas(PagarDetalhe dadosBusca)
        {
            string strSql = " SELECT PD_STATUS, PAG_CODIGO FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + dadosBusca.EmpCodigo
                + " AND PAG_DOCTO = '" + dadosBusca.PagDocto + "' AND CF_DOCTO = '" + dadosBusca.CfDocto + "'";

            return BancoDados.GetDataTable(strSql, null);

        }

        public bool CancelarQuitacao(PagarDetalhe pgDetalhe)
        {
            string strCmd = "UPDATE PAGAR_DETALHE SET PD_SALDO = " + Funcoes.BValor(pgDetalhe.PdValor) + ", PD_STATUS = '" + pgDetalhe.PdStatus + "',"
                + " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = '" + Principal.usuario + "' WHERE PAG_CODIGO = " + pgDetalhe.PagCodigo
                + " AND EMP_CODIGO = " + pgDetalhe.EmpCodigo + " AND PD_PARCELA = " + pgDetalhe.PdParcela;
            
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null) != -1)
            {
                return true;
            }
            else
                return false;

        }

        public bool ExcluirParcelas(int empCodigo, int pagCodigo, int parcela = 0)
        {
            string strCmd = "DELETE FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + empCodigo + " AND PAG_CODIGO = " + pagCodigo;
            if (parcela != 0)
            {
                strCmd += " AND PD_PARCELA = " + parcela;
            }

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }
    }
}
