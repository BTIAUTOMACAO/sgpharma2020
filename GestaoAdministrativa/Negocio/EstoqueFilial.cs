﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class EstoqueFilial
    {
        public string codBarras { get; set; }
        public string descricao { get; set; }
        public int quantidade { get; set; }
        public double precoCusto { get; set; }
        public double precoVenda { get; set; }
        public string codEstabelecimento { get; set; }
        public string descEstab { get; set; }
        public string telefone { get; set; }
        public string codErro { get; set; }
        public string mensagem { get; set; }


        public EstoqueFilial(string CodBarras, string Descricao, int Quantidade, double PrecoCusto, double PrecoVenda, string CodEstabelecimento,
            string DescEstab, string Telefone)
        {
            this.codBarras = CodBarras;
            this.descricao = Descricao;
            this.quantidade = Quantidade;
            this.precoCusto = PrecoCusto;
            this.precoVenda = PrecoVenda;
            this.codEstabelecimento = CodEstabelecimento;
            this.descEstab = DescEstab;
            this.telefone = Telefone;
        }
    }
}
