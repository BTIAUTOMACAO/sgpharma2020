﻿using System;
using System.Collections.Generic;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.WSFarmaciaPopular;
using System.Xml;
using System.Data;
using System.Windows.Forms;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.FPopular;
using GestaoAdministrativa.Vendas;

namespace GestaoAdministrativa.Negocio
{
    class FarmaciaPopular
    {
        ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();

        #region Passo 1 - Solicitação

        public static bool transmitirSolicitacao(DataTable dadosAcesso, DataTable dadosMedicamentos, DataTable dadosSolicitacao, out string numeroAutorizacao)
        {
            // Declaração dos objetos do Web Service
            ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();
            AutorizacaoDTO autorizacao = new AutorizacaoDTO();
            UsuarioFarmaciaDTO usuarioFP = new UsuarioFarmaciaDTO();
            SolicitacaoDTO solicitacaoFP = new SolicitacaoDTO();
            List<MedicamentoDTO> listMed = new List<MedicamentoDTO>();

            //Povoando o objeto arrMedicamentoDTO
            for (int i = 0; i < dadosMedicamentos.Rows.Count; i++)
            {
                MedicamentoDTO medicamentoFP = new MedicamentoDTO
                {
                    coCodigoBarra = dadosMedicamentos.Rows[i][0].ToString(),
                    qtSolicitada = Convert.ToDouble(dadosMedicamentos.Rows[i][2].ToString()),
                    qtPrescrita = Convert.ToDouble(dadosMedicamentos.Rows[i][3].ToString()),
                    vlPrecoVenda = Convert.ToDouble(dadosMedicamentos.Rows[i][4].ToString())
                };

                listMed.Add(medicamentoFP);
            }

            solicitacaoFP.arrMedicamentoDTO = listMed.ToArray();

            //Povoando o objeto UsuarioFarmaciaDTO
            usuarioFP.usuarioFarmacia = dadosAcesso.Rows[0][0].ToString();
            usuarioFP.senhaFarmacia = dadosAcesso.Rows[0][1].ToString();
            usuarioFP.usuarioVendedor = dadosAcesso.Rows[0][3].ToString();
            usuarioFP.senhaVendedor = dadosAcesso.Rows[0][4].ToString();

            //Preenchendo os dados da SolicitacaoDTO
            solicitacaoFP.nuCnpj = dadosAcesso.Rows[0][2].ToString();
            string cpfCliente = Funcoes.RemoveCaracter(dadosSolicitacao.Rows[0][0].ToString());
            solicitacaoFP.nuCpf = cpfCliente;

            string nomeCliente = dadosSolicitacao.Rows[1][0].ToString();

            DateTime dtReceita = Convert.ToDateTime(dadosSolicitacao.Rows[2][0].ToString());
            solicitacaoFP.dtEmissaoReceita = dtReceita;

            solicitacaoFP.nuCrm = dadosSolicitacao.Rows[3][0].ToString();
            solicitacaoFP.sgUfCrm = dadosSolicitacao.Rows[4][0].ToString();
            solicitacaoFP.coSolicitacaoFarmacia = DAL.DALFPopular.getContador();

            Principal.SolicitacaoID = solicitacaoFP.coSolicitacaoFarmacia;

            //método que incrementa o contador da Farmácia Popular
            DAL.DALFPopular.setContador();
            //Gera o DNA da máquina, através do executável 
            string dna = DAL.DALFPopular.dnaMaquina(cpfCliente, dadosAcesso.Rows[0][2].ToString(),
                                                    dadosSolicitacao.Rows[3][0].ToString(), dadosSolicitacao.Rows[4][0].ToString(),
                                                    dtReceita.ToString("dd/MM/yyyy"));
            solicitacaoFP.dnaEstacao = dna;

            frmCarregandoSolicitacao frmCarregando = new frmCarregandoSolicitacao();

            try
            {
                wsFP.Endpoint.Address = new System.ServiceModel.EndpointAddress(Principal.enderecoFp);
                if (wsFP.State == System.ServiceModel.CommunicationState.Created)
                {
                    //Abre a conexão com o Web Service da Farmácia Popular
                    wsFP.Open();
                }
                //Burla o problema com o certificado vencido da Farmácia Popular
                System.Net.ServicePointManager.CertificatePolicy = new FPPolicy();

                //Exibe o form com a mensagem de carregamento
                frmCarregando.Show();
                frmCarregando.Refresh();

                //Muda o cursor para o "loading"
                Cursor.Current = Cursors.WaitCursor;

                //Envia a solicitação para o WS com os dados estruturados (Passo 1)
                autorizacao = wsFP.executarSolicitacao(solicitacaoFP, usuarioFP);
            }
            catch (Exception e) { MessageBox.Show(e.ToString() + "\r\nErro ao tentar comunicação com o Web Service. Tente novamente em instantes!", "Farmácia Popular"); }
            finally
            {
                frmCarregando.Close();
                wsFP.Close();
            }

            if (autorizacao.inAutorizacaoSolicitacao.ToString().Equals("00S"))
            {
                try
                {
                    DAL.DALFPopular.gravaSolicitacao(solicitacaoFP, autorizacao, nomeCliente, usuarioFP.usuarioVendedor);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString() + "\r\n" + "Erro ao gravar as informações da solicitação." + "\r\n" +
                                                   "Por favor, reinicie o processo", "Solicitação não Registrada - Farmácia Popular");
                }

                frmConfirmaAutorizacao frmConfirma = new frmConfirmaAutorizacao(autorizacao, usuarioFP);
                frmConfirma.ShowDialog();
                if (Principal.venBeneficio.Equals("FALSE"))
                {
                    numeroAutorizacao = "";
                    return false;
                }
                else
                {
                    numeroAutorizacao = autorizacao.nuAutorizacao.ToString();
                    return true;
                }
            }
            else
            {
                //Exibe o retorno do WS
                string mensagem = "STATUS DA SOLICITAÇÃO" + "\r\n";
                mensagem += "==================================" + "\r\n";
                mensagem += "Cod. Autorização  ==>  " + autorizacao.inAutorizacaoSolicitacao.ToString() + "\r\n";
                mensagem += "Mensagem de Erro  ==>  " + autorizacao.descMensagemErro + "\r\n";
                mensagem += "==================================" + "\r\n";

                if (autorizacao.arrMedicamentoDTO != null)
                { 
                    for (int i = 0; i < autorizacao.arrMedicamentoDTO.Length; i++)
                    {
                        mensagem += "\r\n" + "Código de Barras => " + autorizacao.arrMedicamentoDTO[i].coCodigoBarra.ToString() + "\r\n";
                        mensagem += "Autorização do Medicamento 0" + (i + 1) + "  ==>  " + autorizacao.arrMedicamentoDTO[i].inAutorizacaoMedicamento.ToString() + "\r\n";
                        mensagem += "----------------------------------";
                    }
                }

                MessageBox.Show(mensagem, "Solicitação Recusada - Farmácia Popular");

                numeroAutorizacao = "";
                return false;
            }
        }

        #endregion

        #region Passo 2 e 3 - Confirmação e Recebimento

        public static bool enviarConfirmacao(AutorizacaoDTO aut, UsuarioFarmaciaDTO usuario)
        {
            ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();
            ConfirmacaoDTO dtConfirmacao = new ConfirmacaoDTO();
            ConfirmacaoAutorizacaoDTO dtConfirmacaoAut = new ConfirmacaoAutorizacaoDTO();
            RecebimentoDTO dtRecebimento = new RecebimentoDTO();
            ConfirmacaoRecebimentoDTO dtConfirmacaoRec = new ConfirmacaoRecebimentoDTO();
            bool retorno;

            dtConfirmacao.coSolicitacaoFarmacia = aut.coSolicitacaoFarmacia;
            dtConfirmacao.descMensagem = aut.descMensagemErro;
            dtConfirmacao.nuAutorizacao = aut.nuAutorizacao;
            dtConfirmacao.nuCupomFiscal = aut.coSolicitacaoFarmacia;

            frmCarregandoSolicitacao frmCarregando = new frmCarregandoSolicitacao();

            try
            {
                wsFP.Endpoint.Address = new System.ServiceModel.EndpointAddress(Principal.enderecoFp);
                if (wsFP.State == System.ServiceModel.CommunicationState.Created)
                {
                    //Abre a conexão com o Web Service da Farmácia Popular
                    wsFP.Open();
                }

                System.Net.ServicePointManager.CertificatePolicy = new FPPolicy();

                frmCarregando.Show();
                frmCarregando.Refresh();

                Cursor.Current = Cursors.WaitCursor;

                dtConfirmacaoAut = wsFP.confirmarAutorizacao(dtConfirmacao, usuario);

                DAL.DALFPopular.gravaConfirmacao(dtConfirmacaoAut);

                dtRecebimento.arrMedicamentoDTO = dtConfirmacaoAut.arrMedicamentoDTO;
                dtRecebimento.nuAutorizacao = dtConfirmacaoAut.nuAutorizacao;
                dtRecebimento.nuCupomFiscal = dtConfirmacaoAut.nuCupomFiscal;

                dtConfirmacaoRec = wsFP.confirmarRecebimento(dtRecebimento, usuario);

                DAL.DALFPopular.gravaRecebimento(dtConfirmacaoRec);

                retorno = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Falha na Confirmação - Farmácia Popular");
                retorno = false;
            }
            finally
            {
                frmCarregando.Close();
                wsFP.Close();
            }

            return retorno;
        }

        #endregion

        #region Estorno da Venda

        public static bool estornarSolicitacao(string autorizacao)
        {
            bool retorno = false;

            ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();
            EstornoDTO estornoFP = new EstornoDTO();
            ConfirmacaoEstornoDTO confEstornoFP = new ConfirmacaoEstornoDTO();
            UsuarioFarmaciaDTO usuarioFP = new UsuarioFarmaciaDTO();
            List<MedicamentoDTO> listMed = new List<MedicamentoDTO>();

            frmCarregandoSolicitacao frmCarregando = new frmCarregandoSolicitacao();

            try
            {
                DataTable medicamentos = DAL.DALFPopular.getMedicamentosSolicitacao(autorizacao);
                DataTable acesso = DAL.DALFPopular.getDadosAcesso(autorizacao);

                for (int i = 0; i < medicamentos.Rows.Count; i++)
                {
                    MedicamentoDTO medicamentoFP = new MedicamentoDTO
                    {
                        coCodigoBarra = medicamentos.Rows[i][0].ToString(),
                        qtDevolvida = Convert.ToDouble(medicamentos.Rows[i][1].ToString()),
                    };

                    listMed.Add(medicamentoFP);
                }

                estornoFP.arrMedicamentoDTO = listMed.ToArray();
                estornoFP.nuAutorizacao = autorizacao;
                estornoFP.nuCnpj = acesso.Rows[0][2].ToString();

                usuarioFP.usuarioFarmacia = acesso.Rows[0][0].ToString();
                usuarioFP.senhaFarmacia = acesso.Rows[0][1].ToString();
                usuarioFP.usuarioVendedor = acesso.Rows[0][3].ToString();
                usuarioFP.senhaVendedor = acesso.Rows[0][4].ToString();

                wsFP.Endpoint.Address = new System.ServiceModel.EndpointAddress(Principal.enderecoFp);
                if (wsFP.State == System.ServiceModel.CommunicationState.Created)
                {
                    //Abre a conexão com o Web Service da Farmácia Popular
                    wsFP.Open();
                }

                System.Net.ServicePointManager.CertificatePolicy = new FPPolicy();

                frmCarregando.Show();
                frmCarregando.Refresh();

                Cursor.Current = Cursors.WaitCursor;

                confEstornoFP = wsFP.executarEstorno(estornoFP, usuarioFP);

                DAL.DALFPopular.gravaEstorno(confEstornoFP, autorizacao);

                DAL.DALFPopular.gravaEstornoMedicamento(confEstornoFP, autorizacao);

                if (!confEstornoFP.inSituacaoEstorno.ToString().Equals("00E"))
                {
                    MessageBox.Show("O estorno não foi realizado, conforme informações abaixo!\r\n\r\n" + "Código de Retorno: " + confEstornoFP.inSituacaoEstorno + "\r\n" +
                                    "Mensagem de Erro:  " + confEstornoFP.descMensagemErro, "Erro - Estorno Farmácia Popular");
                }
                else
                {
                    retorno = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Estorno Farmácia Popular");
            }
            finally
            {
                frmCarregando.Close();
                wsFP.Close();
            }

            return retorno;
        }


        public static bool estornoManual(string autorizacao, List<MedicamentoDTO> medicamentos)
        {
            bool retorno = false;

            ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();
            EstornoDTO estornoFP = new EstornoDTO();
            ConfirmacaoEstornoDTO confEstornoFP = new ConfirmacaoEstornoDTO();
            UsuarioFarmaciaDTO usuarioFP = new UsuarioFarmaciaDTO();
            List<MedicamentoDTO> listMed = new List<MedicamentoDTO>();

            frmCarregandoSolicitacao frmCarregando = new frmCarregandoSolicitacao();

            try
            {
                DataTable acesso = DAL.DALFPopular.getDadosAcesso(autorizacao);

                for (int i = 0; i < medicamentos.Count; i++)
                {
                    MedicamentoDTO medicamentoFP = new MedicamentoDTO
                    {
                        coCodigoBarra = medicamentos[i].coCodigoBarra,
                        qtDevolvida = medicamentos[i].qtDevolvida
                    };

                    listMed.Add(medicamentoFP);
                }

                estornoFP.arrMedicamentoDTO = listMed.ToArray();
                estornoFP.nuAutorizacao = autorizacao;
                estornoFP.nuCnpj = acesso.Rows[0][2].ToString();

                usuarioFP.usuarioFarmacia = acesso.Rows[0][0].ToString();
                usuarioFP.senhaFarmacia = acesso.Rows[0][1].ToString();
                usuarioFP.usuarioVendedor = acesso.Rows[0][3].ToString();
                usuarioFP.senhaVendedor = acesso.Rows[0][4].ToString();

                wsFP.Endpoint.Address = new System.ServiceModel.EndpointAddress(Principal.enderecoFp);
                if (wsFP.State == System.ServiceModel.CommunicationState.Created)
                {
                    //Abre a conexão com o Web Service da Farmácia Popular
                    wsFP.Open();
                }
                System.Net.ServicePointManager.CertificatePolicy = new FPPolicy();

                frmCarregando.Show();
                frmCarregando.Refresh();

                Cursor.Current = Cursors.WaitCursor;

                confEstornoFP = wsFP.executarEstorno(estornoFP, usuarioFP);

               // DAL.DALFPopular.gravaEstorno(confEstornoFP, autorizacao);

                //DAL.DALFPopular.gravaEstornoMedicamento(confEstornoFP, autorizacao);

                if (!confEstornoFP.inSituacaoEstorno.ToString().Equals("00E"))
                {
                    MessageBox.Show("O estorno não foi realizado, conforme informações abaixo!\r\n\r\n" + "Código de Retorno: " + confEstornoFP.inSituacaoEstorno + "\r\n" +
                                    "Mensagem de Erro:  " + confEstornoFP.descMensagemErro, "Erro - Estorno Farmácia Popular");
                }
                else
                {
                    retorno = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Estorno Farmácia Popular");
            }
            finally
            {
                frmCarregando.Close();
                wsFP.Close();
            }

            return retorno;
        }
        #endregion
    }
}
