﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaProdutos
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int NumTrans { get; set; }
        public int NSU { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtd { get; set; }
        public double ValorMaximo { get; set; }
        public double ValorFinal { get; set; }
        public double ValorFabrica { get; set; }
        public double ValorAquisicaoUnitario { get; set; }
        public double ValorRepasse { get; set; }
        public DateTime dtVenda { get; set; }

        public EpharmaProdutos() { }

        public bool InsereProdutos(EpharmaProdutos dados)
        {
            string sql = " INSERT INTO EPHARMA_ITENS_VENDA ( EMP_CODIGO, EST_CODIGO, NUM_TRANS, NSU, PROD_CODIGO, "
                       + " QTD, VL_MAXIMO, VL_PFINAL, VL_PFABRICA, VL_AQUISICAO_UNITARIO, VL_REPASSE, DT_VENDA ) VALUES ("
                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.NumTrans + ","
                       + dados.NSU + ",'"
                       + dados.ProdCodigo + "',"
                       + dados.Qtd + ","
                       + dados.ValorMaximo.ToString().Replace(',', '.') + ","
                       + dados.ValorFinal.ToString().Replace(',', '.') + ","
                       + dados.ValorFabrica.ToString().Replace(',', '.') + ","
                       + dados.ValorAquisicaoUnitario.ToString().Replace(',', '.') + ","
                       + dados.ValorRepasse.ToString().Replace(',', '.') + ","
                       + Funcoes.BDataHora(DateTime.Now) + ")";
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaItensPorNsu(long nsu, int numTrans = 0)
        {
            string sql = " SELECT EIV.PROD_CODIGO, PROD.PROD_DESCR, QTD, VL_MAXIMO, VL_PFINAL, VL_PFABRICA, VL_REPASSE FROM EPHARMA_ITENS_VENDA EIV "
                       + " INNER JOIN PRODUTOS PROD ON(PROD.PROD_CODIGO = EIV.PROD_CODIGO) "
                       + " WHERE EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual + " AND NSU = " + nsu;
            if (!numTrans.Equals(0))
            {
                sql += " AND NUM_TRANS = " + numTrans;
            }

            return BancoDados.GetDataTable(sql, null);
        }

    }
}
