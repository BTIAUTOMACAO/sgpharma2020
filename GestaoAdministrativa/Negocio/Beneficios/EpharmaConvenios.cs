﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaConvenios
    {

        public void DeletaDadosConvenio()
        {
            string sql = "DELETE FROM EPHARMA_CONVENIOS";

            BancoDados.ExecuteNoQuery(sql, null);
        }

        public bool InsereRegistros(string codConvenio, string descricao)
        {
            string sql = " INSERT INTO EPHARMA_CONVENIOS (COD_CONV, NOME_DESCR) VALUES ('"
                       + codConvenio + "','"
                       + descricao + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
