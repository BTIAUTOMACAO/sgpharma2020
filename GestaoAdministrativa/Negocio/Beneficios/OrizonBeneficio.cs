﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class OrizonBeneficio
    {
        public string numContador;
        public void LimparPastas()
        {
            string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "42", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "43", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }
        }
        public void Contador()
        {
            try
            {
                numContador = "";
                string arquivo = Funcoes.LeParametro(14, "41", false) + "Orizon.SEQ";
                if (!File.Exists(arquivo))
                {
                    numContador = "0001";
                }
                else
                {
                    numContador = Util.LerArquivoTXT(arquivo);

                    if (Convert.ToInt32(numContador).ToString().Length <= 4)
                    {
                        int contador = Convert.ToInt32(numContador) + 1;
                        numContador = Funcoes.FormataZeroAEsquerda(contador.ToString(), 4);
                    }
                    else
                    {
                        numContador = "0001";
                    }
                }

                Util.CriarArquivoTXT(arquivo, numContador);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #region ConsultaAutorização 
        public string ConsultaAutorizacao(string numeroAutorizacao)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "41", false) + "Orizon.SEQ");
            string linha = sequencia.PadLeft(4, '0') + "04" + numeroAutorizacao.PadLeft(12, '0');

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "42", false) + sequencia + ".txt", linha);
            //GRAVA LOG
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "41", false) + @"LOGS\ENV_" + sequencia + ".txt", linha);

            return sequencia;
        }

        public bool LeRetornoAutorizacao(string sequencia)
        {
            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                {
                    MessageBox.Show("Erro: Não foi possível verificar a autorização ", "Retorno Autorização Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "44", false)));
            string retorno = Util.LerArquivoTXT(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "41", false) + @"\LOGS\REQ_" + sequencia + ".txt", retorno);
            if (retorno.Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retorno.Substring(8, 40).Trim(), "Retorno Autorização Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region EfetivaVenda
        public string EnviaVenda(string numeroAutorizacao, string numeroDocFiscal)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "41", false) + "Orizon.SEQ");
            string linha = sequencia.PadLeft(4, '0') + "03" + Funcoes.LeParametro(14, "45", false).PadLeft(4, '0') + numeroDocFiscal.PadLeft(6, '0') + numeroAutorizacao.PadLeft(12, '0') + Environment.NewLine;

            OrizonProdutos prod = new OrizonProdutos();
            DataTable dtProdutos = prod.BuscaItensPorAutorizacao(numeroAutorizacao);
            for (int i = 0; i < dtProdutos.Rows.Count; ++i)
            {
                linha += dtProdutos.Rows[i]["COD_BARRAS"].ToString().PadLeft(13, '0') + dtProdutos.Rows[i]["QUATIDADE"].ToString().PadLeft(2, '0') + (Convert.ToInt64(dtProdutos.Rows[i]["PRECO_CONSUMIDOR"].ToString().Replace(",", "")) * 100).ToString().PadLeft(7, '0');
            }


            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "42", false) + sequencia + ".txt", linha);
            //GRAVA LOG
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "41", false) + @"LOGS\ENV_" + sequencia + ".txt", linha);

            return sequencia;
        }

        public bool RetornoVenda(string sequencia)
        {
            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                {
                    MessageBox.Show("Erro: Não foi possível verificar a autorização ", "Retorno Autorização Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "44", false)));
            string retorno = Util.LerArquivoTXT(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "41", false) + @"\LOGS\REQ_" + sequencia + ".txt", retorno);

            if (retorno.Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retorno.Substring(8, 40).Trim(), "Retorno Autorização Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region ConfirmaVenda
        public void ConfirmaVenda(string sequencia, string status)
        {
            string linha = sequencia.PadLeft(4, '0') + "13" + status;

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "42", false) + sequencia + ".txt", linha);
            //GRAVA LOG
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "41", false) + @"LOGS\ENV_CONFIRMA_VENDA_" + sequencia + ".txt", linha);
        }
        #endregion
    }
}
