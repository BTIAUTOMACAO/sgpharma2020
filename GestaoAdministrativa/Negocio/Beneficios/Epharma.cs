﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class Epharma
    {
        public string numContador;
        public string retornoTesteDeAtividade;

        public void LimparPastas()
        {
            string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "13", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "14", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "19", false), "*.bmp", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }
        }

        public void Contador()
        {
            try
            {
                numContador = "";
                string arquivo = Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ";
                if (!File.Exists(arquivo))
                {
                    numContador = "0001";
                }
                else
                {
                    numContador = Util.LerArquivoTXT(arquivo);

                    if (Convert.ToInt32(numContador).ToString().Length <= 4)
                    {
                        int contador = Convert.ToInt32(numContador) + 1;
                        numContador = Funcoes.FormataZeroAEsquerda(contador.ToString(), 4);
                    }
                    else
                    {
                        numContador = "0001";
                    }
                }

                Util.CriarArquivoTXT(arquivo, numContador);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Teste de Atividade 97
        public bool EnvioTesteDeAtividade()
        {
            try
            {
                Contador();

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + numContador + ".txt", numContador + "97");
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\ENV_" + numContador + ".txt", numContador + "97");

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool RetornoTesteDeAtividade()
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "14", false) + numContador + ".txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                    {
                        MessageBox.Show("Erro: ao inicializar o e-Pharma", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));
                retornoTesteDeAtividade = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + numContador + ".txt");
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    return false;
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + numContador + ".txt", retornoTesteDeAtividade);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion

        #region Inicialização 10/23
        public bool EnvioInicializacao()
        {
            try
            {
                Contador();

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + numContador + ".txt", numContador + "1023");
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\ENV_" + numContador + ".txt", numContador + "1023");

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool RetornoInicializacao()
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "14", false) + numContador + ".txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                    {
                        MessageBox.Show("Erro: ao inicializar o e-Pharma", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));
                retornoTesteDeAtividade = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + numContador + ".txt");
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show("Erro: ao inicializar o e-Pharma", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                   // AtualizaTabelaConvenios(numContador);
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + numContador + ".txt", retornoTesteDeAtividade);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizaTabelaConvenios(string sequencia)
        {
            string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            EpharmaConvenios convenios = new EpharmaConvenios();
            convenios.DeletaDadosConvenio();

            for (int i = 1; i <= retorno.Count(); i++)
            {
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show("Erro: ao inicializar o e-Pharma", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    if (retorno[i].Length > 40)
                    {
                        if (convenios.InsereRegistros(retorno[i].Substring(2, 12), retorno[i].Substring(14, 30).Trim()).Equals(false))
                        {
                            MessageBox.Show("Erro: Não foi possivel atualizar a tabela de Epharma Convênios", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        };
                    }
                }
                //}
                //if (retorno[i].Substring(0, 2).Equals("02"))
                //{
                //    return true;
                //}
            }
            return true;
        }
        #endregion

        #region Cancelamento 10/21
        public bool CancelamentoAutorizacao(int nsu, int nunTrans = 0)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");

            string linha = sequencia + "1021" + nsu.ToString().PadLeft(12, '0') + "1" + "0".PadLeft(19, '0') + Environment.NewLine;

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\ENV_" + sequencia + ".txt", linha);

            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                {
                    MessageBox.Show("Erro: Não foi possível Cancelar Autorização", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

            string retorno = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            //GRAVA LOG
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + sequencia + ".txt", retorno);

            if (retorno.Substring(6, 2).Equals("OK") || retorno.Substring(8, 2).Equals("80"))
            {
                EpharmaVendas vendas = new EpharmaVendas();
                vendas.AtualizaStatusVenda(nsu, nunTrans, "D");
                return true;
            }

            return true;
        }
        public bool CancelamentoVenda(string autorizacao)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");

            EpharmaVendas vendas = new EpharmaVendas();
            DataTable dtVenda = vendas.BuscaNumeroAutorizacao(autorizacao);

            if (!dtVenda.Rows.Count.Equals(0))
            {
                string linha = sequencia + "1021" + dtVenda.Rows[0]["NSU"].ToString().PadLeft(12, '0').PadLeft(12, '0') + "1";

                if (Funcoes.LeParametro(6, "355", false, Principal.nomeEstacao).Equals("S"))
                {
                    linha += dtVenda.Rows[0]["ECF"].ToString().PadLeft(9, '0') + dtVenda.Rows[0]["NUM_DOC"].ToString().PadLeft(9, '0') + "3" + Environment.NewLine;
                }
                else
                {
                    linha += "0001".PadLeft(9, '0') + dtVenda.Rows[0]["NUM_DOC"].ToString().PadLeft(9, '0') + "1" + Environment.NewLine;
                }


                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\ENV_" + sequencia + ".txt", linha);

                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Erro: Não foi possível Cancelar Autorização", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

                string retornoCancelamento = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
                //GRAVA LOG
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + sequencia + ".txt", retornoCancelamento);
            }
            else
            {

                MessageBox.Show("Erro: Autorização não encontrada ", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        #endregion

        #region Venda 03
        public string EnviaVenda(string vendaNumeroNSU, string vendaNumeroNota, string numeroCFe)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");

            string numPDV = "";
            string numFiscal = "";
            string tipoNota = "";
            string chave = "";
            if (Funcoes.LeParametro(6, "355", false, Principal.nomeEstacao).Equals("S"))
            {
                numPDV = "0000";
                numFiscal = "000000";
                tipoNota = "3";
                chave = numeroCFe.Substring(3, 44);

            }
            else
            {
                numPDV = "0001";
                numFiscal = vendaNumeroNota.PadLeft(6, '0');
                tipoNota = "1";
                chave = "0".PadLeft(44, '0');
            }
            EpharmaProdutos pro = new EpharmaProdutos();
            DataTable dtProd = pro.BuscaItensPorNsu(Convert.ToInt64(vendaNumeroNSU));

            string linha = sequencia + "03" + numPDV + numFiscal + vendaNumeroNSU.PadLeft(12, '0') + '1' + tipoNota + chave + Environment.NewLine;
            for (int i = 0; i < dtProd.Rows.Count; ++i)
            {
                linha += dtProd.Rows[i]["PROD_CODIGO"].ToString().PadLeft(13, '0') + dtProd.Rows[i]["QTD"].ToString().PadLeft(2, '0') + (Convert.ToInt32(dtProd.Rows[i]["VL_MAXIMO"].ToString().Replace(",", "")) * 100).ToString().PadLeft(7, '0') + 1 + "0".PadLeft(15, '0') + " ".PadRight(40, ' ') + Environment.NewLine;
            }

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linha);

            return sequencia;
        }

        public bool LeRetornoVenda(string sequencia)
        {
            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                {
                    MessageBox.Show("Erro:  Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

            string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            //GRAVA LOG
            string rec = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + sequencia + ".txt", rec);
            if (retorno[0].Substring(6, 2).Equals("ER"))
            {
                EpharmaVendas vendas = new EpharmaVendas();
                vendas.AtualizaStatusVenda(retorno[0].Substring(55, 12).Trim() == "" ? 0 : Convert.ToInt32(retorno[0].Substring(55, 12)),
                    retorno[0].Substring(18, 7).Trim() == "" ? 0 : Convert.ToInt32(retorno[0].Substring(18, 7).Trim()), "E");
                MessageBox.Show(retorno[0].Substring(9, 40).Trim(), "Retorno  E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Fechamento 10/20

        public string EnviaFechamento()
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");
            string linha = sequencia + "1020" + Environment.NewLine;

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linha);
            return sequencia;
        }

        public bool RetornoFechamento(string sequencia)
        {

            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                {
                    MessageBox.Show("Erro:  Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));
            string retorno = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + numContador + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\REQ_" + sequencia + ".txt", retorno);
            if (retorno.Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retorno.Substring(9, 40).Trim(), "Retorno  E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Reimpressão 10/03
        public string SolicitaReimpressao(string autorizacao)
        {
            Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");
            string linha = sequencia + "1003" + autorizacao.PadLeft(7, '0') + "1" + Environment.NewLine;

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linha);

            return sequencia;
        }

        public string LeRetornoReimpressao(string sequencia)
        {
            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                {
                    MessageBox.Show("Erro:  Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

            string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            string ret = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\REQ_" + sequencia + ".txt", ret);
            if (retorno[0].Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retorno[0].Substring(8, retorno[0].Trim().Length), "Retorno  E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
            else
            {
                return retorno[1];
            }
        }
        #endregion
    }
}
