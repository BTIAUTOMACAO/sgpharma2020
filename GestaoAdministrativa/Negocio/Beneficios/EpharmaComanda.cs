﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaComanda
    {
        public long VendaID { get; set; }
        public string NSU { get; set; }
        public string Cartao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public bool InsereDados(EpharmaComanda dados)
        {
            string sql = " INSERT INTO EPHARMA_COMANDA(VENDA_ID, NSU, DTCADASTRO, OPCADASTRO) "
                       + " VALUES("
                       + dados.VendaID + ",'"
                       + dados.NSU + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable IdentificaSeVendaEPorComanda(string nsu, long vendaID = 0)
        {
            string sql = "SELECT * FROM EPHARMA_COMANDA WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                sql += " AND VENDA_ID = " + vendaID;
            }

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscaComandaPorVendaID(long vendaID)
        {
            string sql = "SELECT * FROM EPHARMA_COMANDA WHERE VENDA_ID = '" + vendaID + "'";
            return BancoDados.GetDataTable(sql, null);
        }

        public int ExcluirDadosPorNSU(string nsu, long vendaID)
        {
            string sql = "DELETE FROM EPHARMA_COMANDA WHERE NSU = '" + nsu + "'";
            if (vendaID != 0)
            {
                sql += " AND VENDA_ID = " + vendaID;
            }
            return BancoDados.ExecuteNoQuery(sql, null);
        }
    }
}
