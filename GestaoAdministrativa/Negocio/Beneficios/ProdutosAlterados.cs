﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class ProdutosAlterados
    {
        public string CodBarras { get; set; }
        public string Descricao { get; set; }
        public int Qtde { get; set; }
        public double ProdUnitario { get; set; }
        public double DescontoValor { get; set; }
        public double ProdTotal { get; set; }

        public ProdutosAlterados() { }
    }
}
