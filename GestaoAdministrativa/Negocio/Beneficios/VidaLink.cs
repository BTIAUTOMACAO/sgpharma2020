﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class VidaLink
    {
        public string numContador;
        public string retornoTesteDeAtividade;
        
        public void LimparPastas()
        {
            string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "23", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "24", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }
        }

        public void Contador()
        {
            try
            {
                numContador = "";
                string arquivo = Funcoes.LeParametro(14, "22", false) + "VidaLink.SEQ";
                if (!File.Exists(arquivo))
                {
                    Util.CriarArquivoTXT(arquivo, "0001");
                    numContador = "0001";
                }
                else
                {
                    numContador = Util.LerArquivoTXT(arquivo);

                    if (Convert.ToInt32(numContador).ToString().Length <= 4)
                    {
                        int contador = Convert.ToInt32(numContador) + 1;
                        numContador = Funcoes.FormataZeroAEsquerda(contador.ToString(), 4);
                    }
                    else
                        numContador = "0001";

                    Util.CriarArquivoTXT(arquivo, numContador);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool EnvioTesteDeAtividade()
        {
            try
            {
                Contador();

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "23", false) + numContador + ".txt", numContador + "11");
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\ENV_" + numContador + ".txt", numContador + "11");

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool RetornoTesteDeAtividade(out string numSeq)
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "24", false) + numContador + ".txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        numSeq = "";
                        return false;
                    }
                }

                retornoTesteDeAtividade = Util.LerArquivoTXT(Funcoes.LeParametro(14, "24", false) + numContador + ".txt");
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show("Erro ao realizar Teste de Atividade, tente novamente!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    numSeq = "";
                    return false;
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\REQ_" + numContador + ".txt", retornoTesteDeAtividade);
                numSeq = retornoTesteDeAtividade.Substring(0, 4);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                numSeq = "";
                return false;
            }
        }

        public bool EnviaConsulta(string numAutorizacao)
        {
            try
            {
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "23", false) + "Consulta.txt", retornoTesteDeAtividade.Substring(0,4) + "12" + numAutorizacao);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\ENV_Consulta_" + retornoTesteDeAtividade.Substring(0, 4) + ".txt", retornoTesteDeAtividade.Substring(0, 4) + "12" + numAutorizacao);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool RetornoConsulta(out string NSU)
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "24", false) + "Consulta.txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        NSU = string.Empty;
                        return false;
                    }
                }

                retornoTesteDeAtividade = Util.LerArquivoTXT(Funcoes.LeParametro(14, "24", false) + "Consulta.txt");
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show(retornoTesteDeAtividade.Substring(8, 40).Trim(), "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    NSU = string.Empty;
                    return false;
                }
                
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\RES_Consulta_" + retornoTesteDeAtividade.Substring(0, 4) + ".txt", retornoTesteDeAtividade);

                string[] retornoVenda = retornoTesteDeAtividade.Split('\n');

                if (retornoVenda.Length > 0)
                {
                    BancoDados.AbrirTrans();

                    NSU = Convert.ToUInt64(retornoVenda[0].Substring(48, 12)).ToString();

                    var dadosDaVenda = new BeneficioVidaLink(
                         Convert.ToUInt64(retornoVenda[0].Substring(48, 12)).ToString(),
                    0,
                         retornoVenda[0].Substring(74, 8).Trim(),
                         retornoVenda[0].Substring(82, 30).Trim(),
                         retornoVenda[0].Substring(112, 40).Trim(),
                         Convert.ToInt32(retornoVenda[0].Substring(152, 1)),
                         DateTime.Now,
                         "P",
                         Principal.nomeEstacao,
                         DateTime.Now,
                         Principal.usuario
                        );

                    if(!dadosDaVenda.InsereRegistros(dadosDaVenda))
                    {
                        MessageBox.Show("Erro ao Inserir Venda VidaLink. Contate o Suporte!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        BancoDados.ErroTrans();
                        return false;
                    }

                    for(int i = 1; i < (retornoVenda.Length - 1);i++)
                    {
                        var dadosProdutos = new BeneficioVidaLinkProdutos(
                            Convert.ToUInt64(retornoVenda[0].Substring(48, 12)).ToString(),
                            0,
                            retornoVenda[i].Substring(0, 13),
                            Convert.ToInt32(retornoVenda[i].Substring(13, 2)),
                            Convert.ToDouble(retornoVenda[i].Substring(15, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(22, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(29, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(36, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(43, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(50, 7)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(57, 5)) / 100,
                            Convert.ToDouble(retornoVenda[i].Substring(62, 5)) / 100,
                            DateTime.Now,
                            DateTime.Now,
                            Principal.usuario
                            );

                        if(!dadosProdutos.InsereRegistros(dadosProdutos))
                        {
                            MessageBox.Show("Erro ao Inserir Produto(s) VidaLink. Contate o Suporte!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }
                    }

                    BancoDados.FecharTrans();
                   
                    return true;
                }

                NSU = "";
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                NSU = "";
                return false;
            }
        }

        public bool EnviarVenda(string numAutorizacao, string numCupom)
        {
            try
            {
                Contador();

                string envio = numContador + "13" + "0001" + Funcoes.FormataZeroAEsquerda(numCupom, 6) + numAutorizacao + "\n";

                var buscaProdutos = new BeneficioVidaLinkProdutos();
                DataTable dt = buscaProdutos.BuscaProdutosPorNSU(numAutorizacao);
                for(int i=0; i < dt.Rows.Count;i++)
                {
                    envio += dt.Rows[i]["PROD_CODIGO"] + Funcoes.FormataZeroAEsquerda(dt.Rows[i]["QTD"].ToString(), 2) + "\n";
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "23", false) + "Venda.txt", envio);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\ENV_VENDA_" + numContador + ".txt", envio);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool RetonoDaVenda(out string cupom)
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "24", false) + "Venda.txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cupom = "";
                        return false;
                    }
                }

                retornoTesteDeAtividade = Util.LerArquivoTXT(Funcoes.LeParametro(14, "24", false) + "Venda.txt");
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\REQ_VENDA" + numContador + ".txt", retornoTesteDeAtividade);
                if (retornoTesteDeAtividade.Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show("Erro ao Finalizar Venda VidaLink, tente novamente!\nErro: " + retornoTesteDeAtividade, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cupom = "";
                    return false;
                }

                string[] retornoVenda = retornoTesteDeAtividade.Split('\n');

                if (retornoVenda.Length > 0)
                {
                    cupom = retornoVenda[1];
                }
                else
                    cupom = "";

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "22", false) + @"LOGS\REQ_VENDA" + numContador + ".txt", retornoTesteDeAtividade);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cupom = "";
                return false;
            }
        }
    }
}
