﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Vendas;
using GestaoAdministrativa.Vendas.PortalDaDrogaria;
using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class PortalDaDrogaria
    {
        public string numContador;
        public string retornoTesteDeAtividade;

        public bool IdentificaConexaoComInternet()
        {
            Network net = new Network();
            return net.IsAvailable;
        }

        public void LimparPastas()
        {
            string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "29", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "29", false), "*.001", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "30", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "30", false), "*.001", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }
        }

        public void Contador()
        {
            try
            {
                numContador = "";
                string arquivo = Funcoes.LeParametro(14, "28", false) + "AUTORIZADOR.SEQ";
                if (!File.Exists(arquivo))
                {
                    Util.CriarArquivoTXT(arquivo, "000001");
                    numContador = "000001";
                }
                else
                {
                    numContador = Util.LerArquivoTXT(arquivo);

                    if (Convert.ToInt32(numContador).ToString().Length <= 6)
                    {
                        int contador = Convert.ToInt32(numContador) + 1;
                        numContador = Funcoes.FormataZeroAEsquerda(contador.ToString(), 6);
                    }
                    else
                        numContador = "000001";

                    Util.CriarArquivoTXT(arquivo, numContador);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public List<RetornoPortalDrogaria> CarregaParametros(string arquivo)
        {
            List<RetornoPortalDrogaria> lista = new List<RetornoPortalDrogaria>();
            string arquivoEnvio = @"" + arquivo;
            TextReader tr = new StreamReader(arquivoEnvio, Encoding.Default);
            string linha = "";
            while ((linha = tr.ReadLine()) != null)
            {
                if (!linha.StartsWith("*"))
                {
                    string[] value = linha.Split('=');
                    lista.Add(new RetornoPortalDrogaria() { Id = value[0].Trim(), Valor = value[1].Trim() });
                }
            }

            tr.Close();
            tr.Dispose();

            return lista;
        }

        public bool EnviaAnulaVenda(string operador, string nsu)
        {
            try
            {
                LimparPastas();

                Contador();

                DateTime data = DateTime.Now;
                string arquivo = "000-000 = 0420" + "\n\n"
                    + "011-000 = 800300" + "\n\n"
                    + "001-000 = " + numContador + "\n\n"
                    + "040-000 = " + operador + "\n\n"
                    + "012-000 = " + nsu + "\n\n"
                    + "940-000 = 003" + "\n\n"
                    + "942-000 = " + Funcoes.LeParametro(14, "11", false) + "\n\n"
                    + "023-000 = " + Funcoes.FormataZeroAEsquerda(data.Hour.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Minute.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Second.ToString(), 2) + "\n\n"
                    + "022-000 = " + Funcoes.FormataZeroAEsquerda(data.Month.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Day.ToString(), 2) + "\n\n";


                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "29", false) + numContador + ".001", arquivo);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "28", false) + @"LOGS\ENV_AnulaVenda_" + numContador + ".txt", arquivo);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public List<RetornoPortalDrogaria> RetornoAnulaVenda(frmPortalDrogariaAutorizacao fAutorizacao, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    fAutorizacao.erro = codigoErro[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                return parametros;
            }
        }

        public bool EnviaRecuperacaoDePreAutorizacao(string operador, string nsu, long vendaID, string cartao)
        {
            try
            {
                LimparPastas();

                Contador();

                DateTime data = DateTime.Now;
                string arquivo = "000-000 = 0100" + "\n\n"
                    + "011-000 = 800500" + "\n\n"
                    + "001-000 = " + numContador + "\n\n"
                    + "040-000 = " + operador + "\n\n";
                if (!String.IsNullOrEmpty(nsu))
                {
                    arquivo += "012-000 = " + nsu + "\n\n";
                }
                arquivo += "940-000 = 003" + "\n\n"
                    + "942-000 = " + Funcoes.LeParametro(14,"11", false) + "\n\n"
                    + "023-000 = " + Funcoes.FormataZeroAEsquerda(data.Hour.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Minute.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Second.ToString(), 2) + "\n\n"
                    + "022-000 = " + Funcoes.FormataZeroAEsquerda(data.Month.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Day.ToString(), 2) + "\n\n";
                if (!String.IsNullOrEmpty(cartao))
                {
                    arquivo += "953-000 = " + cartao + "\n\n";
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "29", false) + numContador + ".001", arquivo);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "28", false) + @"LOGS\ENV_PreAutorizacao_" + numContador + ".txt", arquivo);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public List<RetornoPortalDrogaria> RetornoRecuperacaoDePreAutorizacao(frmPortalDrogariaAutorizacao fAutorizacao, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var mensagem = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                          .Where(l => l.StartsWith("030-000"))
                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                          .ToList();

                if (mensagem.Count > 0)
                    fAutorizacao.mensagem = mensagem[0].ToString();

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    fAutorizacao.erro = codigoErro[0].ToString();

                var produto = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                       .Where(l => l.StartsWith("902-000"))
                       .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                       .ToList();
                
                if (produto.Count > 0)
                    fAutorizacao.validou = produto[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                return parametros;
            }
        }

        public bool EnviaTransacao(string numeroDocumento, string nsu, string cartao, bool continuacao = false)
        {
            try
            {
                LimparPastas();

                Contador();

                DataTable dtRetorno = new DataTable();

                var buscaVenda = new BeneficioNovartisEfetivado();
                dtRetorno = buscaVenda.BuscaVendasPorNsu(nsu);

                DateTime data = DateTime.Now;
                string arquivo = "000-000 = 0200" + "\n\n"
                    + "011-000 = 800300" + "\n\n"
                    + "001-000 = " + numContador + "\n\n"
                    + "040-000 = " + dtRetorno.Rows[0]["OPERADORA"] + "\n\n"
                    + "012-000 = " + nsu + "\n\n"
                    + "940-000 = 003" + "\n\n"
                    + "942-000 = " + Funcoes.LeParametro(14, "11", false) + "\n\n"
                    + "023-000 = " + Funcoes.FormataZeroAEsquerda(data.Hour.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Minute.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Second.ToString(), 2) + "\n\n"
                    + "022-000 = " + Funcoes.FormataZeroAEsquerda(data.Month.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Day.ToString(), 2) + "\n\n"
                    //+ "953-000 = " + cartao + "\n\n"
                    + "002-000 = " + numeroDocumento + "\n\n";
                if(continuacao)
                {
                    arquivo += "900-000 = 1" + "\n\n";
                }
                else
                    arquivo += "900-000 = 0" + "\n\n";

                var buscaDados = new BeneficioNovartisProdutos();
                dtRetorno = buscaDados.BuscaProdutosPorNsu(nsu);

                arquivo += "901-000 = " + Funcoes.FormataZeroAEsquerda(dtRetorno.Rows.Count.ToString(), 2) + "\n\n";


                if (continuacao)
                {
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        arquivo += "902-0" + Funcoes.FormataZeroAEsquerda(i.ToString(), 2) + " = " + Funcoes.FormataZeroAEsquerda(dtRetorno.Rows[i]["PROD_CODIGO"].ToString(), 13) + "\n\n";
                        arquivo += "906-0" + Funcoes.FormataZeroAEsquerda(i.ToString(), 2) + " = U" + "\n\n";
                        arquivo += "905-0" + Funcoes.FormataZeroAEsquerda(i.ToString(), 2) + " = " + dtRetorno.Rows[i]["QTD"].ToString() + "\n\n";
                        arquivo += "910-0" + Funcoes.FormataZeroAEsquerda(i.ToString(), 2) + " = " + Funcoes.FormataZeroAEsquerda((Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * 100).ToString(), 7) + "\n\n";
                        arquivo += "911-0" + Funcoes.FormataZeroAEsquerda(i.ToString(), 2) + " = " + Funcoes.FormataZeroAEsquerda((Convert.ToDouble(dtRetorno.Rows[i]["PRE_LIQUIDO"]) * 100).ToString(), 7) + "\n\n";
                    }

                }
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "29", false) + numContador + ".001", arquivo);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "28", false) + @"LOGS\ENV_Transacao_" + numContador + ".txt", arquivo);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public List<RetornoPortalDrogaria> RetornoTransacao(frmPortalDrogariaAutorizacao fAutorizacao, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var mensagem = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                          .Where(l => l.StartsWith("030-000"))
                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                          .ToList();

                if (mensagem.Count > 0)
                    fAutorizacao.mensagem = mensagem[0].ToString();

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    fAutorizacao.erro = codigoErro[0].ToString();

                var produto = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                       .Where(l => l.StartsWith("902-000"))
                       .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                       .ToList();
                
                if (produto.Count > 0)
                    fAutorizacao.validou = produto[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                return parametros;
            }
        }


        public List<RetornoPortalDrogaria> RetornoTransacaoFinalizacao(frmPortalDrogariaFinalizaVenda fAutorizacao, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var mensagem = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                          .Where(l => l.StartsWith("030-000"))
                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                          .ToList();

                if (mensagem.Count > 0)
                    fAutorizacao.mensagem = mensagem[0].ToString();

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    fAutorizacao.erro = codigoErro[0].ToString();

                var produto = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                       .Where(l => l.StartsWith("902-000"))
                       .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                       .ToList();

                if (produto.Count > 0)
                    fAutorizacao.validou = produto[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                return parametros;
            }
        }

        public bool EnviaConfirmacaoDeTransacao(string nsu)
        {
            try
            {
                LimparPastas();

                Contador();

                DataTable dtRetorno = new DataTable();

                var buscaVenda = new BeneficioNovartisEfetivado();
                dtRetorno = buscaVenda.BuscaVendasPorNsu(nsu);

                DateTime data = DateTime.Now;
                string arquivo = "000-000 = 0202" + "\n\n"
                    + "011-000 = 800300" + "\n\n"
                    + "001-000 = " + numContador + "\n\n"
                    + "040-000 = " + dtRetorno.Rows[0]["OPERADORA"] + "\n\n"
                    + "012-000 = " + nsu + "\n\n"
                    + "940-000 = 003" + "\n\n"
                    + "942-000 = " + Funcoes.LeParametro(14, "11", false) + "\n\n"
                    + "023-000 = " + Funcoes.FormataZeroAEsquerda(data.Hour.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Minute.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Second.ToString(), 2) + "\n\n"
                    + "022-000 = " + Funcoes.FormataZeroAEsquerda(data.Month.ToString(), 2) + Funcoes.FormataZeroAEsquerda(data.Day.ToString(), 2) + "\n\n";

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "29", false) + numContador + ".001", arquivo);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "28", false) + @"LOGS\ENV_Confirmacao_" + numContador + ".txt", arquivo);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public List<RetornoPortalDrogaria> RetornoAnulaVendaCupomFiscal()
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return parametros;
            }
        }

        public List<RetornoPortalDrogaria> RetornoTransacaoVenda(frmVenda fAutorizacao, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return parametros;
                    }
                }

                var mensagem = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                          .Where(l => l.StartsWith("030-000"))
                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                          .ToList();

                if (mensagem.Count > 0)
                    fAutorizacao.mensagem = mensagem[0].ToString();

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    fAutorizacao.erro = codigoErro[0].ToString();

                //var produto = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                //       .Where(l => l.StartsWith("902-000"))
                //       .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                //       .ToList();

                //if (produto.Count > 0)
                //    fAutorizacao.validou = produto[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                return parametros;
            }
        }

        public List<RetornoPortalDrogaria> RetornoAnulaVendaErro(out string erro, out string caminho)
        {
            List<RetornoPortalDrogaria> parametros = new List<Negocio.RetornoPortalDrogaria>();

            try
            {
                erro = "0";
                var inicio = DateTime.Now;
                caminho = Funcoes.LeParametro(14, "30", false) + numContador + ".001";
                while (!File.Exists(Funcoes.LeParametro(14, "30", false) + numContador + ".001"))
                {
                    if (DateTime.Now - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Tempo de Resposta Esgotado, tente novamente!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        erro = "-1";
                        return parametros;
                    }
                }

                var codigoErro = File.ReadAllLines(Funcoes.LeParametro(14, "30", false) + numContador + ".001")
                         .Where(l => l.StartsWith("009-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                if (codigoErro.Count > 0)
                    erro = codigoErro[0].ToString();

                parametros = CarregaParametros(Funcoes.LeParametro(14, "30", false) + numContador + ".001");

                return parametros;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                caminho = "";
                erro = "-1";
                return parametros;
            }
        }

    }


}
