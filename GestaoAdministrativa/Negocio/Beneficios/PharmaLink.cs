﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestaoAdministrativa.WSPharmaLink;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    class PharmaLink
    {
        public static bool elegibilidadeCartao(string projeto, string cartao, int cpf = 0)
        {
            
            ConcentradorSoapClient wsPL = new ConcentradorSoapClient();
            wsPL.Open();

            WS_Eleg_PortadorRequestBody elegePortadorBody = new WS_Eleg_PortadorRequestBody();

            // Inicia o xml de identificação
            XmlDocument xml = new XmlDocument();
            XmlNode declaracao = xml.CreateXmlDeclaration("1.0", "windows-1252", null);
            xml.AppendChild(declaracao);
            XmlNode identifica = xml.CreateElement("identifica");

            XmlAttribute cnpj = xml.CreateAttribute("cnpj");
            cnpj.Value = "28763118000190";
            identifica.Attributes.Append(cnpj);
            XmlAttribute terminal = xml.CreateAttribute("terminal");
            terminal.Value = "000001";
            identifica.Attributes.Append(terminal);
            XmlAttribute autentica = xml.CreateAttribute("autentica");
            autentica.Value = "51131212";
            identifica.Attributes.Append(autentica);
            xml.AppendChild(identifica);

            elegePortadorBody.cIdentifica = xml.OuterXml;
            
            elegePortadorBody.cProjeto = projeto;
            elegePortadorBody.cTimestamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            elegePortadorBody.cCartao = cartao;
            elegePortadorBody.nCPF = cpf;
            elegePortadorBody.cCanal = "";

            

            WS_Eleg_PortadorRequest elegePortador = new WS_Eleg_PortadorRequest(elegePortadorBody);
            
            WS_Eleg_PortadorResponseBody respostaPortadorbody = new WS_Eleg_PortadorResponseBody();

            try
            {
                respostaPortadorbody.WS_Eleg_PortadorResult = wsPL.WS_Eleg_Portador(elegePortadorBody.cIdentifica, elegePortadorBody.cProjeto, elegePortadorBody.cTimestamp,
                                                   elegePortadorBody.cCartao, elegePortadorBody.nCPF, elegePortadorBody.cCanal);

                WS_Eleg_PortadorResponse respostaPortador = new WS_Eleg_PortadorResponse(respostaPortadorbody);

                MessageBox.Show(respostaPortador.Body.WS_Eleg_PortadorResult.ToString());

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString(), "Elegibilidade do Cartão - PharmaLink");

                return false;
            }
            finally
            {
                wsPL.Close();
            }
        }

        public static bool efetivaTransacao(string projeto, string cartao, int cpf = 0)
        {
            ConcentradorSoapClient wsPL = new ConcentradorSoapClient();
            wsPL.Open();

            WS_Eleg_PortadorRequestBody elegePortadorBody = new WS_Eleg_PortadorRequestBody();

            // Inicia o xml de identificação e o preenchimento do corpo do objeto de request

            // Cria o xml de identificação
            XmlDocument xml = new XmlDocument();
            XmlNode declaracao = xml.CreateXmlDeclaration("1.0", "windows-1252", null);
            xml.AppendChild(declaracao);
            XmlNode identifica = xml.CreateElement("identifica");

            XmlAttribute cnpj = xml.CreateAttribute("cnpj");
            cnpj.Value = "28763118000190";
            identifica.Attributes.Append(cnpj);
            XmlAttribute terminal = xml.CreateAttribute("terminal");
            terminal.Value = "000001";
            identifica.Attributes.Append(terminal);
            XmlAttribute autentica = xml.CreateAttribute("autentica");
            autentica.Value = "51131212";
            identifica.Attributes.Append(autentica);
            xml.AppendChild(identifica);

            elegePortadorBody.cIdentifica = xml.OuterXml;
            // Fim do xml de identificação

            // Preenche o restante dos itens da requisição
            elegePortadorBody.cProjeto = projeto;
            elegePortadorBody.cTimestamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            elegePortadorBody.cCartao = cartao;
            elegePortadorBody.nCPF = cpf;
            elegePortadorBody.cCanal = "";
            // Insere o corpo preenchido no objeto de request
            WS_Eleg_PortadorRequest elegePortador = new WS_Eleg_PortadorRequest(elegePortadorBody);
            
            try
            {
                // Inicia o corpo do objeto de response e faz a consulta inicial da Elegibilidade do Cartão
                WS_Eleg_PortadorResponseBody respostaPortadorBody = new WS_Eleg_PortadorResponseBody();
                wsPL.InnerChannel.OperationTimeout = new TimeSpan(0,2,0);
                respostaPortadorBody.WS_Eleg_PortadorResult = wsPL.WS_Eleg_Portador(elegePortadorBody.cIdentifica, elegePortadorBody.cProjeto, elegePortadorBody.cTimestamp,
                                                   elegePortadorBody.cCartao, elegePortadorBody.nCPF, elegePortadorBody.cCanal);

                // Inicia o objeto de response utilizando os dados retornados do web service
                WS_Eleg_PortadorResponse respostaPortador = new WS_Eleg_PortadorResponse(respostaPortadorBody);
                
                // Transforma a string retornada em XML, para facilitar a tratativa
                XmlDocument xmlRetorno = new XmlDocument();
                
                xmlRetorno.LoadXml(respostaPortador.Body.WS_Eleg_PortadorResult.ToString());

                
                
                XmlNode nodeRetorno = xmlRetorno.SelectSingleNode("/retorno");

                string nodeStatus = xmlRetorno.SelectSingleNode("/retorno/status").InnerText;

                MessageBox.Show(nodeRetorno.Attributes["status"].Value.ToString());

                if (xmlRetorno.SelectSingleNode("status").Value == "00")
                {
                    MessageBox.Show(xmlRetorno.SelectSingleNode("nsu").Value.ToString());
                }
                else
                {
                    MessageBox.Show(xmlRetorno.SelectSingleNode("status").Value);
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString(), "Elegibilidade do Cartão - PharmaLink");

                return false;
            }
            finally
            {
                wsPL.Close();
            }
        }
    }
}