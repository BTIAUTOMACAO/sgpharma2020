﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class OrizonProdutos
    {
        public int VendaID { get; set; }
        public string Autorizacao { get; set; }
        public string CodBarras { get; set; }
        public int Quantidade { get; set; }
        public double PrecoConsumidor { get; set; }
        public double PrecoVenda { get; set; }
        public double PrecoComDesconto { get; set; }
        public string Flag { get; set; }
        public double ValorReembolso { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public bool InsereItensVenda(OrizonProdutos dados)
        {
            string sql = "  INSERT INTO ORIZON_ITENS "
                       + " (VENDA_ID, AUTORIZACAO, COD_BARRAS, QUATIDADE, PRECO_CONSUMIDOR, PRECO_VENDA, PRECO_COM_DESCONTO, "
                       + " FLAG, VALOR_REEMBOLSO, DTCADASTRO, OPCADASTRO) VALUES ("
                       + dados.VendaID + ",'"
                       + dados.Autorizacao + "','"
                       + dados.CodBarras + "',"
                       + dados.Quantidade + ","
                       + dados.PrecoConsumidor.ToString().Replace(',', '.') + ","
                       + dados.PrecoVenda.ToString().Replace(',', '.') + ","
                       + dados.PrecoComDesconto.ToString().Replace(',', '.') + ",'"
                       + dados.Flag + "',"
                       + dados.ValorReembolso.ToString().Replace(',', '.') + ","
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaItensPorAutorizacao(string autorizacao)
        {
            string sql = " SELECT ITENS.COD_BARRAS, PRO.PROD_DESCR, ITENS.QUATIDADE, ITENS.PRECO_CONSUMIDOR, "
                       + " ITENS.PRECO_VENDA, ITENS.PRECO_COM_DESCONTO, ITENS.FLAG, ITENS.VALOR_REEMBOLSO "
                       + " FROM ORIZON_ITENS ITENS "
                       + " left JOIN PRODUTOS PRO ON(ITENS.COD_BARRAS = PRO.PROD_CODIGO) "
                       + " WHERE AUTORIZACAO = '" + autorizacao + "'";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
