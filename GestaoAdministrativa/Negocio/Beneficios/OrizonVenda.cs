﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class OrizonVenda
    {
        public long VendaID { get; set; }
        public string Autorizacao { get; set; }
        public string NsuTrans { get; set; }
        public string NsuHost { get; set; }
        public char Status { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public bool InsereVenda(OrizonVenda dados)
        {
            string sql = " INSERT INTO ORIZON_AUTORIZACAO "
                       + " (VENDA_ID, AUTORIZACAO, NSU_TRANS, NSU_HOST, STATUS, DTCADASTRO, OPCADASTRO) "
                       + " VALUES ("
                       + dados.VendaID + ",'"
                       + dados.Autorizacao + "','"
                       + dados.NsuTrans + "','"
                       + dados.NsuHost + "','"
                       + dados.Status + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + Principal.usuario + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaStatusVenda(OrizonVenda dados)
        {

            string sql = " UPDATE ORIZON_AUTORIZACAO SET "
                      + " VENDA_ID =" + dados.VendaID
                      + " , NSU_TRANS = '" + dados.NsuTrans + "'"
                      + " , STATUS= '" + dados.Status + "'"
                      + " WHERE AUTORIZACAO = " + dados.Autorizacao;

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }
    }
}
