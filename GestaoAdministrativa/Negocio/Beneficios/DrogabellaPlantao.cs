﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public static class DrogabellaPlantao
    {
        private static XmlDocument sXml;
        private static XmlDocument retornoXml;
        private static string retornoWS;

        private static GestaoAdministrativa.WSConvenio.wsconvenio ws = new GestaoAdministrativa.WSConvenio.wsconvenio();

        public static XmlDocument AbrirTransacao(string wsCartao, string wsEmpresa, bool vendaEntrega)
        {
            #region XML ABRIR TRANSACAO
            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("ABRIR_TRANSACAO_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;

            XmlElement cpf = sXml.CreateElement("CPF");
            raiz.AppendChild(cpf);

            XmlElement operador = sXml.CreateElement("OPERADOR");
            raiz.AppendChild(operador);
            operador.InnerText = "SGPHARMA";

            XmlElement entrega = sXml.CreateElement("ENTREGA");
            raiz.AppendChild(entrega);

            if (!vendaEntrega)
            {
                entrega.InnerText = "N";
            }
            else
            {
                entrega.InnerText = "S";
            }

            try
            {
                ws.Url = Principal.wsConexao;
                retornoWS = ws.MAbrirTransacao(sXml.InnerXml);
                retornoXml = new XmlDocument();
                retornoXml.LoadXml(retornoWS);

                return retornoXml;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return retornoXml;
            }
            #endregion
        }

        public static XmlDocument ValidarProdutosPreAutorizacao(string wsCartao, string wsTransID, DataTable tProdutos, string[,] wsRetornoProdutos)
        {
            #region XML VALIDAR PRODUTOS PRE-AUTORIZACAO
            string codBarras;

            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("VALIDAR_PRODUTOS_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;

            XmlElement cpf = sXml.CreateElement("CPF");
            raiz.AppendChild(cpf);

            XmlElement transid = sXml.CreateElement("TRANSID");
            raiz.AppendChild(transid);
            transid.InnerText = wsTransID;

            XmlElement produtos = sXml.CreateElement("PRODUTOS");
            raiz.AppendChild(produtos);

            for (int i = 0; tProdutos.Rows.Count > i; i++)
            {
                XmlElement produto = sXml.CreateElement("PRODUTO");
                produtos.AppendChild(produto);

                XmlElement codbarras = sXml.CreateElement("CODBARRAS");
                produto.AppendChild(codbarras);

                codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();

                if (codBarras.Length > 13)
                {
                    codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString().Substring(0, 13);
                }
                else
                {
                    codBarras = Funcoes.FormataZeroAEsquerda(tProdutos.Rows[i]["COD_BARRA"].ToString(), 13);
                }

                codbarras.InnerText = codBarras;

                XmlElement descricao = sXml.CreateElement("DESCRICAO");
                produto.AppendChild(descricao);
                descricao.InnerText = tProdutos.Rows[i]["PROD_DESCR"].ToString();

                wsRetornoProdutos[i, 0] = tProdutos.Rows[i]["PROD_DESCR"].ToString();

                XmlElement qtde = sXml.CreateElement("QTDE");
                produto.AppendChild(qtde);
                qtde.InnerText = (Convert.ToInt16(tProdutos.Rows[i]["PROD_QTDE"].ToString())).ToString();

                wsRetornoProdutos[i, 10] = tProdutos.Rows[i]["PROD_QTDE"].ToString();

                XmlElement precoBruto = sXml.CreateElement("PRCUNITBRU");
                produto.AppendChild(precoBruto);
                precoBruto.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                decimal desconto = Convert.ToDecimal(tProdutos.Rows[i]["PROD_DVAL"]);
                double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"])) - Math.Abs(Convert.ToDecimal((tProdutos.Rows[i]["PROD_VUNI"])) * (desconto / 100)), 2) * 100);

                XmlElement precoDesc = sXml.CreateElement("PRCUNITLIQ");
                produto.AppendChild(precoDesc);
                precoDesc.InnerText = valorDUni.ToString();

                XmlElement precoFabri = sXml.CreateElement("PRCFABRICA");
                produto.AppendChild(precoFabri);
                precoFabri.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                XmlElement grupo = sXml.CreateElement("GRUPO");
                produto.AppendChild(grupo);
                grupo.InnerText = "0";

            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MValidarProdutos(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument LerCartao(string wsCartao)
        {
            #region XML CONSULTAR CARTOES
            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("CONSULTAR_CARTOES_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("NOME_CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;


            ws.Url = Principal.wsConexao;
            retornoWS = ws.MConsultarCartoes(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument ValidarProdutosSemReceita(string wsCartao, string wsTransID, DataTable tProdutos, out string comprovante)
        {
            #region VALIDAR PRODUTOS SEM RECEITA
            string codBarras = "";
            comprovante = "";
            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("VALIDAR_PRODUTOS_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;

            XmlElement cpf = sXml.CreateElement("CPF");
            raiz.AppendChild(cpf);

            XmlElement transid = sXml.CreateElement("TRANSID");
            raiz.AppendChild(transid);
            transid.InnerText = wsTransID;

            XmlElement produtos = sXml.CreateElement("PRODUTOS");
            raiz.AppendChild(produtos);

            for (int i = 0; i < tProdutos.Rows.Count; i++)
            {
                if (tProdutos.Rows[i]["PROD_REC"].ToString() == "N")
                {
                    XmlElement produto = sXml.CreateElement("PRODUTO");
                    produtos.AppendChild(produto);

                    XmlElement codbarras = sXml.CreateElement("CODBARRAS");
                    produto.AppendChild(codbarras);

                    codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();

                    if (codBarras.Length > 13)
                    {
                        codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString().Substring(0, 13);
                    }
                    else
                    {
                        codBarras = Funcoes.FormataZeroAEsquerda(tProdutos.Rows[i]["COD_BARRA"].ToString(), 13);
                    }

                    codbarras.InnerText = codBarras;

                    XmlElement descricao = sXml.CreateElement("DESCRICAO");
                    produto.AppendChild(descricao);
                    descricao.InnerText = tProdutos.Rows[i]["PROD_DESCR"].ToString();

                    XmlElement qtde = sXml.CreateElement("QTDE");
                    produto.AppendChild(qtde);
                    qtde.InnerText = tProdutos.Rows[i]["PROD_QTDE"].ToString();

                    XmlElement precoBruto = sXml.CreateElement("PRCUNITBRU");
                    produto.AppendChild(precoBruto);
                    precoBruto.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                    decimal desconto = Convert.ToDecimal(tProdutos.Rows[i]["PROD_DVAL"]);
                    double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2) * 100);

                    XmlElement precoDesc = sXml.CreateElement("PRCUNITLIQ");
                    produto.AppendChild(precoDesc);
                    precoDesc.InnerText = valorDUni.ToString();

                    XmlElement precoFabri = sXml.CreateElement("PRCFABRICA");
                    produto.AppendChild(precoFabri);
                    precoFabri.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                    XmlElement grupo = sXml.CreateElement("GRUPO");
                    produto.AppendChild(grupo);
                    grupo.InnerText = "0";

                    comprovante += codBarras + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                    if(desconto > 0)
                    {
                        comprovante += "    R$ " + tProdutos.Rows[i]["PROD_VUNI"].ToString().Replace(",",".") + " (% " + tProdutos.Rows[i]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[i]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                    }
                    comprovante += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                }

                 


            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MValidarProdutos(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument FecharTransacaoSemReceita(string wsCartao, string wsTransID, double wsValorAutoriza, string senhaConveniado, string novaSenha = "", int parcelaDrogabella = 1)
        {
            #region FECHAR TRANSACAO SEM RECEITA
            sXml = new XmlDocument();

            XmlElement FRaiz = sXml.CreateElement("FECHAR_TRANSACAO_PARAM");
            sXml.AppendChild(FRaiz);

            XmlProcessingInstruction FPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(FPi, FRaiz);

            XmlElement Fcredenciado = sXml.CreateElement("CREDENCIADO");
            FRaiz.AppendChild(Fcredenciado);

            XmlElement Fcodacesso = sXml.CreateElement("CODACESSO");
            Fcredenciado.AppendChild(Fcodacesso);
            Fcodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement Fsenha = sXml.CreateElement("SENHA");
            Fcredenciado.AppendChild(Fsenha);
            Fsenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement Fcartao = sXml.CreateElement("CARTAO");
            FRaiz.AppendChild(Fcartao);
            Fcartao.InnerText = wsCartao;

            XmlElement Fcpf = sXml.CreateElement("CPF");
            FRaiz.AppendChild(Fcpf);

            XmlElement Ftransid = sXml.CreateElement("TRANSID");
            FRaiz.AppendChild(Ftransid);
            Ftransid.InnerText = wsTransID;

            XmlElement Fvaloraut = sXml.CreateElement("VALORAUT");
            FRaiz.AppendChild(Fvaloraut);
            Fvaloraut.InnerText = wsValorAutoriza.ToString();

            XmlElement Fformapagto = sXml.CreateElement("FORMAPAGTO");
            FRaiz.AppendChild(Fformapagto);
            Fformapagto.InnerText = parcelaDrogabella.ToString();

            XmlElement Fsenhacartao = sXml.CreateElement("SENHA_CARTAO");
            FRaiz.AppendChild(Fsenhacartao);
            Fsenhacartao.InnerText = senhaConveniado;


            XmlElement Fnovasenha = sXml.CreateElement("NOVA_SENHA_CARTAO");
            FRaiz.AppendChild(Fnovasenha);

            if (!String.IsNullOrEmpty(novaSenha))
            {
                Fnovasenha.InnerText = novaSenha;
            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MFecharTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument ValidarProdutosComReceita(string wsCartao, string wsTransID, DataTable tProdutos, out string comprovante)
        {
            #region VALIDAR PRODUTOS COM RECEITA
            string codBarras;
            comprovante = "";
            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("VALIDAR_PRODUTOS_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;

            XmlElement cpf = sXml.CreateElement("CPF");
            raiz.AppendChild(cpf);

            XmlElement transid = sXml.CreateElement("TRANSID");
            raiz.AppendChild(transid);
            transid.InnerText = wsTransID;

            XmlElement produtos = sXml.CreateElement("PRODUTOS");
            raiz.AppendChild(produtos);

            for (int i = 0; i < tProdutos.Rows.Count; i++)
            {
                if (tProdutos.Rows[i]["PROD_REC"].ToString() == "S")
                {
                    XmlElement produto = sXml.CreateElement("PRODUTO");
                    produtos.AppendChild(produto);

                    XmlElement codbarras = sXml.CreateElement("CODBARRAS");
                    produto.AppendChild(codbarras);

                    codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();

                    if (codBarras.Length > 13)
                    {
                        codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString().Substring(0, 13);
                    }
                    else
                    {
                        codBarras = Funcoes.FormataZeroAEsquerda(tProdutos.Rows[i]["COD_BARRA"].ToString(), 13);
                    }

                    codbarras.InnerText = codBarras;

                    XmlElement descricao = sXml.CreateElement("DESCRICAO");
                    produto.AppendChild(descricao);
                    descricao.InnerText = tProdutos.Rows[i]["PROD_DESCR"].ToString();

                    XmlElement qtde = sXml.CreateElement("QTDE");
                    produto.AppendChild(qtde);
                    qtde.InnerText = (Convert.ToInt16(tProdutos.Rows[i]["PROD_QTDE"].ToString())).ToString();

                    XmlElement precoBruto = sXml.CreateElement("PRCUNITBRU");
                    produto.AppendChild(precoBruto);
                    precoBruto.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                    decimal desconto = Convert.ToDecimal(tProdutos.Rows[i]["PROD_DVAL"]);
                    double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2) * 100);
                    
                    XmlElement precoDesc = sXml.CreateElement("PRCUNITLIQ");
                    produto.AppendChild(precoDesc);
                    precoDesc.InnerText = valorDUni.ToString();

                    XmlElement precoFabri = sXml.CreateElement("PRCFABRICA");
                    produto.AppendChild(precoFabri);
                    precoFabri.InnerText = ((Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])) * 100).ToString();

                    XmlElement grupo = sXml.CreateElement("GRUPO");
                    produto.AppendChild(grupo);
                    grupo.InnerText = "0";

                    comprovante += codBarras + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                    if (desconto > 0)
                    {
                        comprovante += "    R$ " + tProdutos.Rows[i]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[i]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[i]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                    }
                    comprovante += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n"; 
                }

            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MValidarProdutos(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument FecharTransacaoComReceita(string wsCartao, string wsTransID, DataTable tProdutos, string wsConselho, string wsUF, string wsCRM, string wsNumRec,
            DateTime wsData, double wsValorAutoriza, string senhaConveniado, out string comprovanteMedico, string novaSenha = "", int parcelaDrogabella = 1)
        {
            #region FECHAR TRANSACAO COM RECEITA
            string codBarras;
            sXml = new XmlDocument();

            XmlElement FRaiz = sXml.CreateElement("FECHAR_TRANSACAO_PARAM");
            sXml.AppendChild(FRaiz);

            XmlProcessingInstruction FPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(FPi, FRaiz);

            XmlElement Fcredenciado = sXml.CreateElement("CREDENCIADO");
            FRaiz.AppendChild(Fcredenciado);

            XmlElement Fcodacesso = sXml.CreateElement("CODACESSO");
            Fcredenciado.AppendChild(Fcodacesso);
            Fcodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement Fsenha = sXml.CreateElement("SENHA");
            Fcredenciado.AppendChild(Fsenha);
            Fsenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement Fcartao = sXml.CreateElement("CARTAO");
            FRaiz.AppendChild(Fcartao);
            Fcartao.InnerText = wsCartao;

            XmlElement Fcpf = sXml.CreateElement("CPF");
            FRaiz.AppendChild(Fcpf);

            XmlElement Ftransid = sXml.CreateElement("TRANSID");
            FRaiz.AppendChild(Ftransid);
            Ftransid.InnerText = wsTransID;

            XmlElement Fprodutos = sXml.CreateElement("PRODUTOS");
            FRaiz.AppendChild(Fprodutos);

            comprovanteMedico = wsConselho + ": " + wsCRM + " - UF: " + wsUF + "\n";
            comprovanteMedico += "Dt. Rec.: " + wsData.ToString("dd/MM/yyyy") + " - N. Rec.: " + wsNumRec + "\n";

            for (int i = 0; tProdutos.Rows.Count > i; i++)
            {
                if (tProdutos.Rows[i]["PROD_REC"].ToString() == "S")
                {
                    XmlElement Fproduto = sXml.CreateElement("PRODUTO");
                    Fprodutos.AppendChild(Fproduto);

                    XmlElement Fcodbarras = sXml.CreateElement("CODBARRAS");
                    Fproduto.AppendChild(Fcodbarras);

                    codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();

                    if (codBarras.Length > 13)
                    {
                        codBarras = tProdutos.Rows[i]["COD_BARRA"].ToString().Substring(0, 13);
                    }
                    else
                    {
                        codBarras = Funcoes.FormataZeroAEsquerda(tProdutos.Rows[i]["COD_BARRA"].ToString(), 13);
                    }

                    Fcodbarras.InnerText = codBarras;

                    XmlElement Fprescritor = sXml.CreateElement("PRESCRITOR");
                    Fproduto.AppendChild(Fprescritor);

                    //VERIFICA SE É CRM OU CRO//
                    if (wsConselho.Equals("CRM"))
                    {
                        wsConselho = "1";
                    }
                    else if (wsConselho.Equals("CRO"))
                    {
                        wsConselho = "2";
                    }
                    else if (wsConselho.Equals("CRMV"))
                    {
                        wsConselho = "3";
                    }

                    XmlElement Ftipoprescritor = sXml.CreateElement("TIPOPRESCRITOR");
                    Fprescritor.AppendChild(Ftipoprescritor);
                    Ftipoprescritor.InnerText = wsConselho;

                    XmlElement Fufprescritor = sXml.CreateElement("UFPRESCRITOR");
                    Fprescritor.AppendChild(Fufprescritor);
                    Fufprescritor.InnerText = wsUF;

                    XmlElement Fnumprescritor = sXml.CreateElement("NUMPRESCRITOR");
                    Fprescritor.AppendChild(Fnumprescritor);
                    Fnumprescritor.InnerText = wsCRM;

                    XmlElement Fnumreceita = sXml.CreateElement("NUMRECEITA");
                    Fprescritor.AppendChild(Fnumreceita);
                    Fnumreceita.InnerText = wsNumRec;

                    DateTime FormatarData = wsData;

                    XmlElement Fdatareceita = sXml.CreateElement("DATARECEITA");
                    Fprescritor.AppendChild(Fdatareceita);
                    Fdatareceita.InnerText = FormatarData.ToString("yyyy-MM-dd");

                }
            }
            
            XmlElement Fvaloraut = sXml.CreateElement("VALORAUT");
            FRaiz.AppendChild(Fvaloraut);
            Fvaloraut.InnerText = wsValorAutoriza.ToString();

            XmlElement Fformapagto = sXml.CreateElement("FORMAPAGTO");
            FRaiz.AppendChild(Fformapagto);
            Fformapagto.InnerText = parcelaDrogabella.ToString();

            XmlElement Fsenhacartao = sXml.CreateElement("SENHA_CARTAO");
            FRaiz.AppendChild(Fsenhacartao);
            Fsenhacartao.InnerText = senhaConveniado;


            XmlElement Fnovasenha = sXml.CreateElement("NOVA_SENHA_CARTAO");
            FRaiz.AppendChild(Fnovasenha);

            if (!String.IsNullOrEmpty(novaSenha))
            {
                Fnovasenha.InnerText = novaSenha;
            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MFecharTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument ConfirmarTransacao(string wsCartao, string wsTransID, string wsNF = "")
        {
            #region CONFIRMAR TRANSAÇÃO
            sXml = new XmlDocument();

            XmlElement CRaiz = sXml.CreateElement("CONFIRMAR_TRANSACAO_PARAM");
            sXml.AppendChild(CRaiz);

            XmlProcessingInstruction CPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(CPi, CRaiz);

            XmlElement Ccredenciado = sXml.CreateElement("CREDENCIADO");
            CRaiz.AppendChild(Ccredenciado);

            XmlElement Ccodacesso = sXml.CreateElement("CODACESSO");
            Ccredenciado.AppendChild(Ccodacesso);
            Ccodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement Csenha = sXml.CreateElement("SENHA");
            Ccredenciado.AppendChild(Csenha);
            Csenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement Ccartao = sXml.CreateElement("CARTAO");
            CRaiz.AppendChild(Ccartao);
            Ccartao.InnerText = wsCartao;

            XmlElement Ccpf = sXml.CreateElement("CPF");
            CRaiz.AppendChild(Ccpf);

            XmlElement Ctransid = sXml.CreateElement("TRANSID");
            CRaiz.AppendChild(Ctransid);
            Ctransid.InnerText = wsTransID;

            XmlElement Cdocfiscal = sXml.CreateElement("DOCFISCAL");
            CRaiz.AppendChild(Cdocfiscal);
            Cdocfiscal.InnerText = "0";

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MConfirmarTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument ConsultarTransacao(string wsTransID)
        {
            #region CONSULTAR TRANSACAO
            sXml = new XmlDocument();

            XmlElement CTRaiz = sXml.CreateElement("CONSULTAR_TRANSACAO_PARAM");
            sXml.AppendChild(CTRaiz);

            XmlProcessingInstruction CTPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(CTPi, CTRaiz);

            XmlElement CTcredenciado = sXml.CreateElement("CREDENCIADO");
            CTRaiz.AppendChild(CTcredenciado);

            XmlElement CTcodacesso = sXml.CreateElement("CODACESSO");
            CTcredenciado.AppendChild(CTcodacesso);
            CTcodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement CTsenha = sXml.CreateElement("SENHA");
            CTcredenciado.AppendChild(CTsenha);
            CTsenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement CTtransid = sXml.CreateElement("TRANSID");
            CTRaiz.AppendChild(CTtransid);
            CTtransid.InnerText = wsTransID;

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MConsultarTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument ValidarProdutosParcial(string wsCartao, string wsTransID, List<ProdutosAlterados> lProdutos, out string comprovante)
        {
            #region VALIDAR PRODUTOS COM RECEITA
            string codBarras;
            comprovante = "";
            sXml = new XmlDocument();

            XmlElement raiz = sXml.CreateElement("VALIDAR_PRODUTOS_PARAM");
            sXml.AppendChild(raiz);

            XmlProcessingInstruction pi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(pi, raiz);

            XmlElement credenciado = sXml.CreateElement("CREDENCIADO");
            raiz.AppendChild(credenciado);

            XmlElement codacesso = sXml.CreateElement("CODACESSO");
            credenciado.AppendChild(codacesso);
            codacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement senha = sXml.CreateElement("SENHA");
            credenciado.AppendChild(senha);
            senha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement cartao = sXml.CreateElement("CARTAO");
            raiz.AppendChild(cartao);
            cartao.InnerText = wsCartao;

            XmlElement cpf = sXml.CreateElement("CPF");
            raiz.AppendChild(cpf);

            XmlElement transid = sXml.CreateElement("TRANSID");
            raiz.AppendChild(transid);
            transid.InnerText = wsTransID;

            XmlElement produtos = sXml.CreateElement("PRODUTOS");
            raiz.AppendChild(produtos);

            for (int i = 0; i < lProdutos.Count; i++)
            {
                XmlElement produto = sXml.CreateElement("PRODUTO");
                produtos.AppendChild(produto);

                XmlElement codbarras = sXml.CreateElement("CODBARRAS");
                produto.AppendChild(codbarras);

                codBarras = lProdutos[i].CodBarras;

                if (codBarras.Length > 13)
                {
                    codBarras = lProdutos[i].CodBarras.Substring(0, 13);
                }
                else
                {
                    codBarras = Funcoes.FormataZeroAEsquerda(lProdutos[i].CodBarras, 13);
                }

                codbarras.InnerText = codBarras;

                XmlElement descricao = sXml.CreateElement("DESCRICAO");
                produto.AppendChild(descricao);
                descricao.InnerText = lProdutos[i].Descricao;

                XmlElement qtde = sXml.CreateElement("QTDE");
                produto.AppendChild(qtde);
                qtde.InnerText = lProdutos[i].Qtde.ToString();

                XmlElement precoBruto = sXml.CreateElement("PRCUNITBRU");
                produto.AppendChild(precoBruto);
                precoBruto.InnerText = (lProdutos[i].ProdUnitario * 100).ToString();

                decimal desconto = Convert.ToDecimal(lProdutos[i].DescontoValor);
                double valorDUni = Convert.ToDouble(Math.Round(Convert.ToDecimal(lProdutos[i].ProdTotal) / lProdutos[i].Qtde, 2) * 100);

                XmlElement precoDesc = sXml.CreateElement("PRCUNITLIQ");
                produto.AppendChild(precoDesc);
                precoDesc.InnerText = valorDUni.ToString();

                XmlElement precoFabri = sXml.CreateElement("PRCFABRICA");
                produto.AppendChild(precoFabri);
                precoFabri.InnerText = (lProdutos[i].ProdUnitario * 100).ToString();

                XmlElement grupo = sXml.CreateElement("GRUPO");
                produto.AppendChild(grupo);
                grupo.InnerText = "0";

                comprovante += codBarras + " " + (lProdutos[i].Descricao.Length > 29 ? lProdutos[i].Descricao.Substring(0, 28) : lProdutos[i].Descricao) + "\n";
                if (desconto > 0)
                {
                    comprovante += "    R$ " + lProdutos[i].ProdUnitario.ToString().Replace(",", ".") + " (% " + (lProdutos[i].DescontoValor * 100) / lProdutos[i].ProdUnitario + " Desc = R$ " + lProdutos[i].DescontoValor.ToString().Replace(",", ".") + ")\n";
                }
                comprovante += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + lProdutos[i].Qtde + " =  R$ " + ((valorDUni / 100) * lProdutos[i].Qtde).ToString().Replace(",", ".") + "\n";
            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MValidarProdutos(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument FecharTransacaoComReceitaParcial(string wsCartao, string wsTransID, List<ProdutosAlterados> lProdutos, string wsConselho, string wsUF, string wsCRM, string wsNumRec,
            DateTime wsData, double wsValorAutoriza, string senhaConveniado, out string comprovanteMedico, string novaSenha = "")
        {
            #region FECHAR TRANSACAO COM RECEITA
            string codBarras;
            sXml = new XmlDocument();

            XmlElement FRaiz = sXml.CreateElement("FECHAR_TRANSACAO_PARAM");
            sXml.AppendChild(FRaiz);

            XmlProcessingInstruction FPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(FPi, FRaiz);

            XmlElement Fcredenciado = sXml.CreateElement("CREDENCIADO");
            FRaiz.AppendChild(Fcredenciado);

            XmlElement Fcodacesso = sXml.CreateElement("CODACESSO");
            Fcredenciado.AppendChild(Fcodacesso);
            Fcodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement Fsenha = sXml.CreateElement("SENHA");
            Fcredenciado.AppendChild(Fsenha);
            Fsenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement Fcartao = sXml.CreateElement("CARTAO");
            FRaiz.AppendChild(Fcartao);
            Fcartao.InnerText = wsCartao;

            XmlElement Fcpf = sXml.CreateElement("CPF");
            FRaiz.AppendChild(Fcpf);

            XmlElement Ftransid = sXml.CreateElement("TRANSID");
            FRaiz.AppendChild(Ftransid);
            Ftransid.InnerText = wsTransID;

            XmlElement Fprodutos = sXml.CreateElement("PRODUTOS");
            FRaiz.AppendChild(Fprodutos);

            comprovanteMedico = wsConselho + ": " + wsCRM + " - UF: " + wsUF + "\n";
            comprovanteMedico += "Dt. Receita: " + wsData.ToString("dd/MM/yyyy") + " - N. Receita: " + wsNumRec + "\n";

            for (int i = 0; lProdutos.Count > i; i++)
            {
                XmlElement Fproduto = sXml.CreateElement("PRODUTO");
                Fprodutos.AppendChild(Fproduto);

                XmlElement Fcodbarras = sXml.CreateElement("CODBARRAS");
                Fproduto.AppendChild(Fcodbarras);

                codBarras = lProdutos[i].CodBarras;

                if (codBarras.Length > 13)
                {
                    codBarras = lProdutos[i].CodBarras.Substring(0, 13);
                }
                else
                {
                    codBarras = Funcoes.FormataZeroAEsquerda(lProdutos[i].CodBarras, 13);
                }

                Fcodbarras.InnerText = codBarras;

                XmlElement Fprescritor = sXml.CreateElement("PRESCRITOR");
                Fproduto.AppendChild(Fprescritor);

                //VERIFICA SE É CRM OU CRO//
                if (wsConselho.Equals("CRM"))
                {
                    wsConselho = "1";
                }
                else if (wsConselho.Equals("CRO"))
                {
                    wsConselho = "2";
                }
                else if (wsConselho.Equals("CRMV"))
                {
                    wsConselho = "3";
                }

                XmlElement Ftipoprescritor = sXml.CreateElement("TIPOPRESCRITOR");
                Fprescritor.AppendChild(Ftipoprescritor);
                Ftipoprescritor.InnerText = wsConselho;

                XmlElement Fufprescritor = sXml.CreateElement("UFPRESCRITOR");
                Fprescritor.AppendChild(Fufprescritor);
                Fufprescritor.InnerText = wsUF;

                XmlElement Fnumprescritor = sXml.CreateElement("NUMPRESCRITOR");
                Fprescritor.AppendChild(Fnumprescritor);
                Fnumprescritor.InnerText = wsCRM;

                XmlElement Fnumreceita = sXml.CreateElement("NUMRECEITA");
                Fprescritor.AppendChild(Fnumreceita);
                Fnumreceita.InnerText = wsNumRec;

                DateTime FormatarData = wsData;

                XmlElement Fdatareceita = sXml.CreateElement("DATARECEITA");
                Fprescritor.AppendChild(Fdatareceita);
                Fdatareceita.InnerText = FormatarData.ToString("yyyy-MM-dd");
            }
            
            XmlElement Fvaloraut = sXml.CreateElement("VALORAUT");
            FRaiz.AppendChild(Fvaloraut);
            Fvaloraut.InnerText = wsValorAutoriza.ToString();

            XmlElement Fformapagto = sXml.CreateElement("FORMAPAGTO");
            FRaiz.AppendChild(Fformapagto);
            Fformapagto.InnerText = "1";

            XmlElement Fsenhacartao = sXml.CreateElement("SENHA_CARTAO");
            FRaiz.AppendChild(Fsenhacartao);
            Fsenhacartao.InnerText = senhaConveniado;


            XmlElement Fnovasenha = sXml.CreateElement("NOVA_SENHA_CARTAO");
            FRaiz.AppendChild(Fnovasenha);

            if (!String.IsNullOrEmpty(novaSenha))
            {
                Fnovasenha.InnerText = novaSenha;
            }

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MFecharTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }

        public static XmlDocument CancelarTransacao(string wsTransID)
        {
            #region CANCELAR TRANSACAO
            sXml = new XmlDocument();

            XmlElement CTRaiz = sXml.CreateElement("CANCELAR_TRANSACAO_PARAM");
            sXml.AppendChild(CTRaiz);

            XmlProcessingInstruction CTPi = sXml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"iso-8859-1\"");

            sXml.InsertBefore(CTPi, CTRaiz);

            XmlElement CTcredenciado = sXml.CreateElement("CREDENCIADO");
            CTRaiz.AppendChild(CTcredenciado);

            XmlElement CTcodacesso = sXml.CreateElement("CODACESSO");
            CTcredenciado.AppendChild(CTcodacesso);
            CTcodacesso.InnerText = Funcoes.LeParametro(6, "53", false);

            XmlElement CTsenha = sXml.CreateElement("SENHA");
            CTcredenciado.AppendChild(CTsenha);
            CTsenha.InnerText = Funcoes.LeParametro(6, "54", false);

            XmlElement CTtransid = sXml.CreateElement("TRANSID");
            CTRaiz.AppendChild(CTtransid);
            CTtransid.InnerText = wsTransID;

            XmlElement operador = sXml.CreateElement("OPERADOR");
            operador.InnerText = "SG PHARMA";
            CTRaiz.AppendChild(operador);

            ws.Url = Principal.wsConexao;
            retornoWS = ws.MCancelarTransacao(sXml.InnerXml);
            retornoXml = new XmlDocument();
            retornoXml.LoadXml(retornoWS);

            return retornoXml;
            #endregion
        }


    }
}
