﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio.Beneficios
{
    public class EpharmaVendas
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int NumTrans { get; set; }
        public int NSU { get; set; }
        public int NumDoc { get; set; }
        public long VendaId { get; set; }
        public string Cartao { get; set; }
        public string Identificacao { get; set; }
        public int Tp_conv { get; set; }
        public string ECF { get; set; }
        public string PDV { get; set; }
        public string Status { get; set; }
        public DateTime DtVenda { get; set; }
        public int Autorizacao { get; set; }

        public EpharmaVendas() { }

        public bool InsereVenda(EpharmaVendas dados)
        {
            string sql = " INSERT INTO EPHARMA_VENDA( EMP_CODIGO, EST_CODIGO, NUM_TRANS, NSU, NUM_DOC, VENDA_ID, CARTAO, "
                       + " IDENTIFICACAO, TP_CONV, ECF , PDV, DT_VENDA, STATUS, AUTORIZACAO) VALUES ("
                       + Principal.empAtual + ","
                       + Principal.estAtual + ","
                       + dados.NumTrans + ","
                       + dados.NSU + ","
                       + dados.NumDoc + ","
                       + dados.VendaId + ",'"
                       + dados.Cartao + "','"
                       + dados.Identificacao + "',"
                       + dados.Tp_conv + ",'"
                       + dados.ECF + "','"
                       + dados.PDV + "',"
                       + Funcoes.BDataHora(DateTime.Now) + ",'"
                       + dados.Status + "','"
                       + dados.Autorizacao + "')";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaVenda(EpharmaVendas dadosNovos, DataTable dadosAntigos)
        {

            int contMatriz = 0;

            string sql = " UPDATE EPHARMA_VENDA SET ";

            if (!dadosNovos.NumTrans.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["NUM_TRANS"])) && !dadosNovos.NumTrans.Equals(0))
            {
                sql += "NUM_TRANS = " + dadosNovos.NumTrans + ",";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.NSU.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["NSU"])) && !dadosNovos.NSU.Equals(0))
            {
                sql += "NSU = " + dadosNovos.NSU + ",";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.Autorizacao.Equals(0))
            {
                sql += "AUTORIZACAO = " + dadosNovos.Autorizacao + ",";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.NumDoc.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["NUM_DOC"])) && !dadosNovos.NumDoc.Equals(0))
            {
                sql += "NUM_DOC = " + dadosNovos.NumDoc + ",";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.VendaId.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["VENDA_ID"])) && !dadosNovos.VendaId.Equals(0))
            {
                sql += "VENDA_ID = " + dadosNovos.VendaId + ",";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.Tp_conv.Equals(Convert.ToInt32(dadosAntigos.Rows[0]["TP_CONV"])) && !dadosNovos.Tp_conv.Equals(0))
            {
                sql += "TP_CONV = '" + dadosNovos.Tp_conv + "',";
                contMatriz = contMatriz + 1;
            }
            if (!String.IsNullOrEmpty(dadosNovos.ECF))
            {
                sql += "ECF = " + dadosNovos.ECF + ",";
                contMatriz = contMatriz + 1;
            }
            if (!String.IsNullOrEmpty(dadosNovos.PDV))
            {
                sql += "PDV = '" + dadosNovos.PDV + "',";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNovos.Status.Equals(dadosAntigos.Rows[0]["STATUS"].ToString()) && !String.IsNullOrEmpty(dadosNovos.Status))
            {
                sql += "STATUS = '" + dadosNovos.Status + "',";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                sql += "EMP_CODIGO = " + Principal.empAtual + " WHERE NSU = " + dadosAntigos.Rows[0]["NSU"].ToString() + " AND STATUS = '" + dadosAntigos.Rows[0]["STATUS"].ToString() + "'";

                if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public DataTable BuscaVendasPorNSU(int nsu)
        {
            string sql = " SELECT NUM_TRANS, NSU, NUM_DOC, VENDA_ID, CARTAO, IDENTIFICACAO, TP_CONV, ECF, PDV, STATUS, AUTORIZACAO, DT_VENDA FROM EPHARMA_VENDA"
                       + " WHERE NSU = " + nsu;

            return BancoDados.GetDataTable(sql, null);
        }

        public bool AtualizaStatusVenda(int nsu, int numTrans, string status)
        {
            string sql = " UPDATE EPHARMA_VENDA SET  STATUS = '" + status + "'"
                       + " WHERE NSU = " + nsu;
            if (!numTrans.Equals(0))
            {
                sql += "AND NUM_TRANS = " + numTrans;
            }
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable BuscaVendasPorStatus(char status)
        {
            string sql = " SELECT NUM_TRANS, NSU, NUM_DOC, VENDA_ID, CARTAO, IDENTIFICACAO, TP_CONV, ECF, PDV, STATUS, DT_VENDA, AUTORIZACAO FROM EPHARMA_VENDA WHERE STATUS = '" + status + "'";
            return BancoDados.GetDataTable(sql, null);
        }

        public string BuscaNumeroTrans(string nsu)
        {
            string sql = "SELECT NUM_TRANS FROM EPHARMA_VENDA WHERE  NSU = " + nsu;

            var retorno = Convert.ToString(BancoDados.ExecuteScalar(sql, null));
            return retorno;
        }

        public DataTable BuscaNumeroAutorizacao(string autorizacao)
        {
            string sql = " SELECT NUM_TRANS, NSU, NUM_DOC, VENDA_ID, CARTAO, IDENTIFICACAO, TP_CONV, ECF, PDV, STATUS, DT_VENDA, AUTORIZACAO FROM EPHARMA_VENDA WHERE AUTORIZACAO = '" + autorizacao + "'";

            return BancoDados.GetDataTable(sql, null);

        }
    }
}
