﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Ibpt
    {
        public string Ncm { get; set; }
        public double Nacional { get; set; }
        public double Importado { get; set; }
        public double Estadual { get; set; }
        public double Municipal { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public Ibpt() { }

        public Ibpt(string ncm, double nacional, double importado, double estadual, double municipal)
        {
            this.Ncm = ncm;
            this.Nacional = nacional;
            this.Importado = importado;
            this.Estadual = estadual;
            this.Municipal = municipal;
        }

        public bool ExcluiRegistroPorNCM(Ibpt dados)
        {
            string sql = "DELETE FROM IBPT WHERE NCM = '" + dados.Ncm + "'";
            BancoDados.ExecuteNoQuery(sql, null);
            return true;
        }

        public bool InsereRegistros(Ibpt dados)
        {
            string strCmd = "INSERT INTO IBPT (NCM, NACIONAL,IMPORTADO, ESTADUAL, MUNICIPAL) VALUES('"
                + dados.Ncm + "',"
                + Funcoes.BValor(dados.Nacional) + ","
                + Funcoes.BValor(dados.Importado) + ","
                + Funcoes.BValor(dados.Estadual) + ","
                + Funcoes.BValor(dados.Municipal) + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaImpostosPorNCM(string NCM)
        {
            string strSql = "SELECT NACIONAL, IMPORTADO, ESTADUAL, MUNICIPAL FROM IBPT WHERE NCM = '" + NCM + "'";
            return BancoDados.GetDataTable(strSql, null);
        }

    }
}
