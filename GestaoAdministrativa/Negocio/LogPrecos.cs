﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class LogPrecos
    {
        public string LogOperador { get; set; }
        public string CodBarras { get; set; }
        public double LogAnterior { get; set; }
        public double LogAlterado { get; set; }
        public DateTime LogData { get; set; }
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }

        public LogPrecos() { }

        public LogPrecos(string logOperador, string codBarras, double logAnterior, double logAlterado, DateTime logData, int empCodigo, int estCodigo)
        {
            this.LogOperador = logOperador;
            this.CodBarras = codBarras;
            this.LogAnterior = logAnterior;
            this.LogAlterado = logAlterado;
            this.LogData = logData;
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
        }


        public bool InsereRegistros(LogPrecos dados)
        {
            string strCmd = "INSERT INTO LOG_PRECOS(LOG_OPERADOR, CODBARRAS, LOG_ANTERIOR, LOG_ALTERADO,  LOG_DATA, EST_CODIGO, EMP_CODIGO) VALUES ('"
                + dados.LogOperador + "','"
                + dados.CodBarras + "',"
                + Funcoes.BFormataValor(dados.LogAnterior) + ","
                + Funcoes.BFormataValor(dados.LogAlterado) + ","
                + Funcoes.BDataHora(dados.LogData) + ","
                + dados.EstCodigo + ","
                + dados.EmpCodigo + ")";

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
