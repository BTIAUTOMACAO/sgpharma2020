﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class Reserva
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int Codigo { get; set; }
        public int ID { get; set; }
        public string Solicitante { get; set; }
        public int LojaOrigem { get; set; }
        public int LojaDestino { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtCadastro { get; set; }
        public string Status { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }
        public string NomeFilial { get; set; }

        public bool InsereRegistros(Reserva dados)
        {
            string strCmd = "INSERT INTO RESERVA(EMP_CODIGO,EST_CODIGO, CODIGO, ID, SOLICITANTE, LOJA_ORIGEM, LOJA_DESTINO, STATUS, DTCADASTRO, OPCADASTRO) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.Codigo + "," +
                dados.ID + ",'" + 
                dados.Solicitante + "'," +
                dados.LojaOrigem + "," + 
                dados.LojaDestino + ",'" +
                dados.Status + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
