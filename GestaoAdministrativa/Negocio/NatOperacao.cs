﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Negocio
{
    [System.Runtime.InteropServices.GuidAttribute("331EC3B9-D33C-400B-BEA6-9ECE243F848E")]
    public class NatOperacao
    {
        public int EmpCodigo { get; set; }
        public int NoId { get; set; }
        public int NoCodigo { get; set; }
        public int NoComp { get; set; }
        public string NoDescr { get; set; }
        public int NoTipo { get; set; }
        public int NoFatura { get; set; }
        public int NoEstoque { get; set; }
        public int NoRelatorios { get; set; }
        public string NoLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public NatOperacao(int empCodigo, int noId, int noCodigo, int noComp, string noDescr, int noTipo, int noFatura,
            int noEstoque, int noRelatorios, string noLiberado, DateTime dtAlteracao, string opAlteracao, DateTime dtCadastro, string opCadastro)
        {
            this.EmpCodigo = empCodigo;
            this.NoId = noId;
            this.NoCodigo = noCodigo;
            this.NoComp = noComp;
            this.NoDescr = noDescr;
            this.NoTipo = noTipo;
            this.NoFatura = noFatura;
            this.NoEstoque = noEstoque;
            this.NoRelatorios = noRelatorios;
            this.NoLiberado = noLiberado;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public NatOperacao() { }


        public bool VerificaNatFatura(int empCodigo, int codigo, int complemento)
        {
            DataTable dt = new DataTable();

            dt = BancoDados.selecionarRegistros("SELECT NO_FATURA FROM NAT_OPERACAO WHERE EMP_CODIGO = " + empCodigo + " AND NO_CODIGO = "
                + codigo + " AND NO_COMP = " + complemento);
            if (dt.Rows.Count != 0)
            {
                if (Equals(Convert.ToInt32(dt.Rows[0]["NO_FATURA"]), 1))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// INSERE OS DADOS NA TABELA NAT_OPERACAO
        /// </summary>
        /// <param name="dados">Objeto do Tipo NatOperacao</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public bool InsereRegistros(NatOperacao dados)
        {
            string strCmd = "INSERT INTO NAT_OPERACAO(EMP_CODIGO,NO_ID, NO_CODIGO, NO_COMP, NO_DESCR,NO_TIPO,NO_FATURA,NO_ESTOQUE, " +
            " NO_RELATORIOS, NO_DESABILITADO, DTCADASTRO, OPCADASTRO) VALUES (" +
                dados.EmpCodigo + "," +
                dados.NoId + ",'" +
                dados.NoCodigo + "','" +
                dados.NoComp + "','" +
                dados.NoDescr + "'," +
                dados.NoTipo + "," +
                dados.NoFatura + "," +
                dados.NoEstoque + "," +
                dados.NoRelatorios + ",'" +
                dados.NoLiberado + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "')";

            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// ATUALIZA OS DADOS DA TABELA NAT_OPERACAO, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo NatOperacao com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public bool AtualizaDados(NatOperacao dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[14, 3];
            int contMatriz = 0;
            string strCmd;

            strCmd = "UPDATE NAT_OPERACAO SET ";
            if (!dadosNew.NoCodigo.ToString().Equals(dadosOld.Rows[0]["NO_CODIGO"].ToString()))
            {
                if (Util.RegistrosPorEstabelecimento("NAT_OPERACAO", "NO_CODIGO", dadosNew.NoCodigo.ToString(), true, false).Rows.Count != 0)
                {
                    Principal.mensagem = "Natureza de operação já cadastrada.";
                    Funcoes.Avisa();
                    return false;
                }

                strCmd += " NO_CODIGO = " + dadosNew.NoCodigo + ",";

                dados[contMatriz, 0] = "NO_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoCodigo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoComp.ToString().Equals(dadosOld.Rows[0]["NO_COMP"].ToString()))
            {
                strCmd += " NO_COMP = " + dadosNew.NoComp + ",";

                dados[contMatriz, 0] = "NO_COMP";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_COMP"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoComp + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoDescr.Equals(dadosOld.Rows[0]["NO_DESCR"]))
            {
                strCmd += " NO_DESCR = '" + dadosNew.NoDescr + "',";

                dados[contMatriz, 0] = "NO_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoTipo.ToString().Equals(dadosOld.Rows[0]["NO_TIPO"].ToString()))
            {
                strCmd += " NO_TIPO = " + dadosNew.NoTipo + ",";

                dados[contMatriz, 0] = "NO_TIPO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_TIPO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoTipo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoFatura.ToString().Equals(dadosOld.Rows[0]["NO_FATURA"].ToString()))
            {
                strCmd += " NO_FATURA = " + dadosNew.NoFatura + ",";

                dados[contMatriz, 0] = "NO_FATURA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_FATURA"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoFatura + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoEstoque.ToString().Equals(dadosOld.Rows[0]["NO_ESTOQUE"].ToString()))
            {
                strCmd += " NO_ESTOQUE = " + dadosNew.NoEstoque + ",";

                dados[contMatriz, 0] = "NO_ESTOQUE";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_ESTOQUE"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoEstoque + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoRelatorios.ToString().Equals(dadosOld.Rows[0]["NO_RELATORIOS"].ToString()))
            {
                strCmd += " NO_RELATORIOS = " + dadosNew.NoRelatorios + ",";

                dados[contMatriz, 0] = "NO_RELATORIOS";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_RELATORIOS"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoRelatorios + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.NoLiberado.Equals(Principal.dtBusca.Rows[0]["NO_DESABILITADO"]))
            {
                strCmd += " NO_DESABILITADO = '" + dadosNew.NoLiberado + "',";

                dados[contMatriz, 0] = "NO_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["NO_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.NoLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE EMP_CODIGO = " + dadosNew.EmpCodigo + " and NO_ID = " + dadosNew.NoId;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("NO_CODIGO", dadosNew.NoCodigo.ToString(), Principal.usuario, "NAT_OPERACAO", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(int empCodigo, string noCodigo)
        {
            string strCmd = "DELETE FROM NAT_OPERACAO WHERE EMP_CODIGO = " + empCodigo + " AND NO_ID = " + noCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public DataTable BuscaDados(NatOperacao dadosBusca, out string strOrdem)
        {
            string strSql;
            strSql = "SELECT A.EMP_CODIGO, A.NO_ID, A.NO_CODIGO, A.NO_COMP, A.NO_DESCR, A.NO_TIPO, A.NO_FATURA, A.NO_ESTOQUE, A.NO_RELATORIOS,"
                  + " CASE A.NO_DESABILITADO WHEN 'N' THEN 'S' ELSE 'N' END AS NO_LIBERADO, A.DAT_ALTERACAO, A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO"
                  + " FROM NAT_OPERACAO A WHERE A.EMP_CODIGO = " + dadosBusca.EmpCodigo;
            if (dadosBusca.NoCodigo == 0 && dadosBusca.NoDescr == "" && dadosBusca.NoLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.NO_ID";
            }
            else
            {
                if (dadosBusca.NoCodigo != 0)
                {
                    strSql += " AND A.NO_CODIGO = " + dadosBusca.NoCodigo;
                }
                if (dadosBusca.NoDescr != "")
                {
                    strSql += " AND A.NO_DESCR LIKE '%" + dadosBusca.NoDescr + "%'";
                }
                if (dadosBusca.NoLiberado != "TODOS")
                {
                    dadosBusca.NoLiberado = dadosBusca.NoLiberado == "S" ? "N" : "S";
                    strSql += " AND A.NO_DESABILITADO = '" + dadosBusca.NoLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.NO_ID";
            }

            return BancoDados.GetDataTable(strSql, null);
        }
    }
}
