﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class Usuario
    {
        public int ID { get; set; }
        public string LoginID { get; set; }
        public string Senha { get; set; }
        public string Administrador { get; set; }
        public string Liberado { get; set; }
        public int GrupoID { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public Usuario() { }


        public Usuario(int iD, string loginID, string senha, string administrador, string liberado, int grupoID, DateTime dtAlteracao,
                       string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.ID = iD;
            this.LoginID = loginID;
            this.Senha = senha;
            this.Administrador = administrador;
            this.Liberado = liberado;
            this.GrupoID = grupoID;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscaPermissoesUsuario(Usuario dadosUsuario)
        {
            string sql = "SELECT M.MENU, M.NOME, P.ACESSA, G.ADMINISTRADOR"
                + " FROM MODULO_MENU M, PERMISSOES P, USUARIOS U, GRUPO_USUARIOS G WHERE M.MODULO_ID = P.MODULO_ID AND"
                + " U.LOGIN_ID = '" + dadosUsuario.LoginID + "' AND P.GRUPO_ID = U.GRUPO_ID AND G.GRUPO_USU_ID = U.GRUPO_ID ORDER BY P.MODULO_ID";

            return BancoDados.GetDataTable(sql, null);
        }

        public string BuscaNivelAcesso(string login)
        {

            string sql = "SELECT SUPERVISOR FROM USUARIOS WHERE LOGIN_ID = '" + login + "'";

            return (string)BancoDados.ExecuteScalar(sql, null);

        }
        public DataTable BuscaTodosUsuario(string status)
        {
            string sql = "SELECT LOGIN_ID FROM USUARIOS WHERE LIBERADO = '" + status + "' ORDER BY LOGIN_ID";

            return BancoDados.GetDataTable(sql, null);
        }

        public DataTable BuscarDados(Usuario dadosBusca, out string strOrdem)
        {
            string strSql = "SELECT U.ID, U.LOGIN_ID, G.ADMINISTRADOR, U.LIBERADO, U.GRUPO_ID, "
                       + "G.DESCRICAO, U.DTALTERACAO, U.OPALTERACAO, U.DTCADASTRO, "
                       + "U.OPCADASTRO, U.SENHA FROM USUARIOS U, GRUPO_USUARIOS G  WHERE U.GRUPO_ID = G.GRUPO_USU_ID";
            if (dadosBusca.ID == 0 && dadosBusca.LoginID == "" && dadosBusca.GrupoID == -1 && dadosBusca.Liberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY ID";
            }
            else
            {
                if (dadosBusca.ID != 0)
                {
                    strSql += " AND U.ID = " + dadosBusca.ID;
                }
                if (dadosBusca.LoginID != "")
                {
                    strSql += " AND U.LOGIN_ID LIKE '%" + dadosBusca.LoginID + "%'";
                }
                if (dadosBusca.GrupoID != -1)
                {
                    strSql += " AND U.GRUPO_ID = " + dadosBusca.GrupoID;
                }
                if (dadosBusca.Liberado != "TODOS")
                {
                    strSql += " AND U.LIBERADO = '" + dadosBusca.Liberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY ID";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InserirDados(Usuario dados)
        {
            string strCmd = "INSERT INTO USUARIOS (ID, LOGIN_ID, SENHA, GRUPO_ID, SUPERVISOR, LIBERADO, DTCADASTRO, OPCADASTRO) VALUES ("
                + dados.ID + ",'"
                + dados.LoginID + "','"
                + dados.Senha + "',"
                + dados.GrupoID + ",'"
                + dados.Administrador + "','"
                + dados.Liberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaDados(Usuario dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[7, 3];
            int contMatriz = 0;
            string strCmd;

            strCmd = "UPDATE USUARIOS SET ";
            if (!dadosNew.LoginID.Equals(dadosOld.Rows[0]["LOGIN_ID"]))
            {
                //VERIFICA SE USUARIO JÁ ESTA CADASTRADO//
                if (Util.RegistrosPorEstabelecimento("USUARIOS", "LOGIN_ID", dadosNew.LoginID, false, true).Rows.Count > 0)
                {
                    Principal.mensagem = "Usuário já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }
                strCmd += " LOGIN_ID = '" + dadosNew.LoginID + "',";
                dados[contMatriz, 0] = "LOGIN_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LOGIN_ID"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.LoginID + "'";
                contMatriz = contMatriz + 1;
            }

            if (!Funcoes.DescriptografaSenha(dadosNew.Senha).Equals(Funcoes.DescriptografaSenha(dadosOld.Rows[0]["SENHA"].ToString())))
            {
                strCmd += " SENHA = '" + dadosNew.Senha + "',";

                dados[contMatriz, 0] = "SENHA";
                dados[contMatriz, 1] = "'" + Funcoes.CriptografaSenha(dadosOld.Rows[0]["SENHA"].ToString()) + "'";
                dados[contMatriz, 2] = "'" + Funcoes.CriptografaSenha(dadosNew.Senha) + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Administrador.ToString().Equals(dadosOld.Rows[0]["SUPERVISOR"]))
            {
                strCmd += " SUPERVISOR = '" + dadosNew.Administrador + "',";

                dados[contMatriz, 0] = "SUPERVISOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SUPERVISOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Administrador + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Liberado.Equals(dadosOld.Rows[0]["LIBERADO"]))
            {
                strCmd += " LIBERADO = '" + dadosNew.Liberado + "',";

                dados[contMatriz, 0] = "LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Liberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.GrupoID.ToString().Equals(dadosOld.Rows[0]["GRUPO_ID"].ToString()))
            {
                strCmd += " GRUPO_ID = " + dadosNew.GrupoID + ",";

                dados[contMatriz, 0] = "GRUPO_ID";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["GRUPO_ID"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.GrupoID + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE ID = " + dadosNew.ID;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("ID", dadosNew.ID.ToString(), Principal.usuario, "USUARIOS", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public int ExcluirDados(string uCodigo)
        {
            string strCmd = "DELETE FROM USUARIOS WHERE ID = " + uCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        public List<Usuario> DadosLogin(string usuario, string senha = "")
        {
            string strSql = "SELECT A.LOGIN_ID, B.ADMINISTRADOR FROM USUARIOS A"
                + " JOIN  GRUPO_USUARIOS B ON B.GRUPO_USU_ID = A.GRUPO_ID WHERE A.LOGIN_ID = '" + usuario + "'";
            if (!String.IsNullOrEmpty(senha))
            {
                strSql += "  AND A.SENHA = '" + Funcoes.CriptografaSenha(senha) + "'";
            }
            strSql += " AND A.LIBERADO = 'S'";

            List<Usuario> lista = new List<Usuario>();
            using (DataTable table = BancoDados.GetDataTable(strSql, null))
            {
                foreach (DataRow row in table.Rows)
                {
                    var dUsuario = new Usuario();
                    dUsuario.LoginID = row["LOGIN_ID"].ToString();
                    dUsuario.Administrador = row["ADMINISTRADOR"].ToString();
                    lista.Add(dUsuario);
                }
            }

            return lista;
        }


    }
}
