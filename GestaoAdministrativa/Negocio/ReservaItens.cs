﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class ReservaItens
    {
        public int EstCodigo { get; set; }
        public int EmpCodigo { get; set; }
        public int ID { get; set; }
        public string CodBarras { get; set; }
        public int Qtde { get; set; }
        public string CodErro { get; set; }
        public string Mensagem { get; set; }

        public bool InsereRegistros(ReservaItens dados)
        {
            string strCmd = "INSERT INTO RESERVA_ITENS(EMP_CODIGO,EST_CODIGO, CODIGO, PROD_CODIGO, QTDE) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.ID + ",'" +
                dados.CodBarras + "'," +
                dados.Qtde + ")";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }
    }
}
