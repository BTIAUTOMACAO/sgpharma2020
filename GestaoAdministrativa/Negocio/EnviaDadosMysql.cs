﻿using GestaoAdministrativa.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public static class EnviaOuRecebeDadosMysql
    {
        public static async Task<int> InserePagarEPagarDetalhe(Pagar dados, List<PagarDetalhe> detalhe)
        {
            try
            {

                int id = await InserePagar(dados);

                if (id != 0)
                {
                    for (int i = 0; i < detalhe.Count; i++)
                    {
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpResponseMessage response = await client.GetAsync("Pagar/InsereContasAPagarDetalhe?id=" + id
                                            + "&vencimento=" + detalhe[i].PdVencimento.ToString("yyyy/MM/dd")
                                            + "&valor=" + detalhe[i].PdValor.ToString().Replace(",", ".")
                                            + "&saldo=" + detalhe[i].PdSaldo.ToString().Replace(",", ".")
                                            + "&status=" + detalhe[i].PdStatus + "&parcela=" + detalhe[i].PdParcela);
                            if (!response.IsSuccessStatusCode)
                            {
                                return 0;
                            }
                        }
                    }

                    BancoDados.ExecuteNoQuery("UPDATE PAGAR SET ID = " + id + " WHERE EMP_CODIGO = " + Principal.empAtual + " AND PAG_CODIGO = " + dados.PagCodigo, null);
                }
                return id;
            }
            catch
            {
                return 0;
            }
        }

        public static async Task<int> InserePagar(Pagar dados)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Pagar/InsereContasAPagar?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&pagCodigo=" + dados.PagCodigo + "&pagDocto=" + dados.PagDocto
                        + "&fornecedor=" + Funcoes.RemoveCaracter(dados.CfDocto) + "&valor=" + dados.PagValorPrincipal.ToString().Replace(",", ".")
                        + "&origem=" + dados.PagOrigem + "&status=" + dados.PagStatus + "&despCodigo=" + dados.DespCodigo
                        + "&operador=" + dados.OpCadastro);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return Convert.ToInt32(responseBody);
                }
            }
            catch
            {
                return 0;
            }
        }

        public static async Task<bool> ExcluiPagarEPagarDetalhe(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Pagar/ExcluiContasAPagarID?id=" + id);
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> AtualizaPagar(Pagar dados)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Pagar/AtualizaStatusPagamento?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&status=" + dados.PagStatus + "&id=" + dados.ID);
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> AtualizaPagarDetalhe(PagarDetalhe dados, int id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Pagar/AtualizaStatusParcela?status=" + dados.PdStatus + "&id=" + id
                        + "&parcela=" + dados.PdParcela + "&saldo=" + dados.PdValor.ToString().Replace(",","."));
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<List<EstabelecimentoFilial>> BuscaEstabelecimentos()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaEstabelecimentos?codEstab=0&grupoID=" + Funcoes.LeParametro(9, "53", false));
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<EstabelecimentoFilial> allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);
                
                return allItems;
            }
        }

        public static async Task<List<Pagar>> BuscaContasPendentes()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Pagar/BuscaContasNaoEnviadas?codEstab=" + Funcoes.LeParametro(9, "52", true)  + "&grupoID=" + Funcoes.LeParametro(9, "53", false)
                    + "&origem=W");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<Pagar> allItems = JsonConvert.DeserializeObject<List<Pagar>>(responseBody);

                return allItems;
            }
        }

        public static async Task<List<PagarDetalhe>> BuscaParcelasPorID(int ID)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Pagar/BuscaParcelasPorID?ID=" + ID);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<PagarDetalhe> allItems = JsonConvert.DeserializeObject<List<PagarDetalhe>>(responseBody);

                return allItems;
            }
        }

        public static async Task<bool> AtualizaEnvioEPagCodigo(char envio, int id, int pagCodigo)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Pagar/AtualizaEnvioPagCodigo?envio=" + envio + "&id=" + id
                        + "&pagCodigo=" + pagCodigo);
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<List<MovCaixaFilial>> BuscaMovimentoCaixa(int codLoja, DateTime dataFechamento)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("RelatorioCaixa/BuscaMovimentoCaixaEspecie?codLoja=" + codLoja  + "&codGrupo=" + Funcoes.LeParametro(9, "53", false)
                    + "&data=" + dataFechamento.ToString("yyyy/MM/dd"));
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<MovCaixaFilial> allItems = JsonConvert.DeserializeObject<List<MovCaixaFilial>>(responseBody);

                return allItems;
            }
        }

        public static async Task<List<MovCaixaFilial>> BuscaMovimento(int codLoja, DateTime dataFechamento)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("RelatorioCaixa/BuscaMovimentoCaixaPorOperacao?codLoja=" + codLoja + "&codGrupo=" + Funcoes.LeParametro(9, "53", false)
                    + "&data=" + dataFechamento.ToString("yyyy/MM/dd"));
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<MovCaixaFilial> allItems = JsonConvert.DeserializeObject<List<MovCaixaFilial>>(responseBody);

                return allItems;
            }
        }

        public static async Task<List<DespesasFilial>> BuscaDespesas(int codLoja, DateTime dataFechamento, string status, bool atrasadas)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Pagar/BuscaTodasAsDespesas?codEstab=" + codLoja + "&grupoID=" + Funcoes.LeParametro(9, "53", false)
                    + "&status=" + status + "&data=" + dataFechamento.ToString("yyyy/MM/dd") + "&atrasadas=" + atrasadas);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<DespesasFilial> allItems = JsonConvert.DeserializeObject<List<DespesasFilial>>(responseBody);

                return allItems;
            }
        }

        public static async Task<bool> AtualizacaoDePrecoFilial(List<AtualizaPrecoFilial> dados)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    for (int i = 0; i < dados.Count; i++)
                    {
                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaPrecoFilial?lojaOrigem=" + dados[i].CodLojaOrigem
                            + "&lojaDestino=" + dados[i].CodLojaDestino 
                            + "&grupoId=" + dados[i].CodGrupo
                            + "&codBarras=" + dados[i].CodDeBarras
                            + "&valor=" + dados[i].ValorDeVenda.ToString().Replace(",",".")
                            + "&atualiza=" + dados[i].Atualiza
                            + "&operador=" + dados[i].Operador
                            + "&observacao=" + dados[i].Observacao);
                        if (!response.IsSuccessStatusCode)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<List<AtualizaPrecoFilial>> BuscaAtualizacaoPreco(int codLoja, int codGrupo, char status, string codBarras = "" )
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/BuscaAtualizacaoPreco?codEstab=" + codLoja + "&grupoID=" + codGrupo
                    + "&status=" + status + "&codBarras=" + codBarras);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<AtualizaPrecoFilial> allItems = JsonConvert.DeserializeObject<List<AtualizaPrecoFilial>>(responseBody);

                return allItems;
            }
        }


        public static async Task<bool> AtualizaCampoFilial(int codEstab, int grupoID, char status, string tabela, string campo, string campoEstab)
        { 
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaStatusTabela?codEstab=" + codEstab + "&grupoID=" + grupoID
                        + "&status=" + status + "&tabela=" + tabela + "&campo=" + campo + "&campoEstab=" + campoEstab);
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<bool> AtualizacaoDeComissaoFilial(List<AtualizaComissaoFilial> dados)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    for (int i = 0; i < dados.Count; i++)
                    {
                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaComissaoFilial?lojaOrigem=" + dados[i].CodLojaOrigem
                            + "&lojaDestino=" + dados[i].CodLojaDestino
                            + "&grupoId=" + dados[i].CodGrupo
                            + "&codBarras=" + dados[i].CodDeBarras
                            + "&valor=" + dados[i].Comissao.ToString().Replace(",", ".")
                            + "&atualiza=" + dados[i].Atualiza
                            + "&operador=" + dados[i].Operador
                            + "&observacao=" + dados[i].Observacao);
                        if (!response.IsSuccessStatusCode)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static async Task<List<AtualizaComissaoFilial>> BuscaAtualizacaoComissao(int codLoja, int codGrupo, char status, string codBarras = "")
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/BuscaAtualizacaoComissao?codEstab=" + codLoja + "&grupoID=" + codGrupo
                    + "&status=" + status + "&codBarras=" + codBarras);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<AtualizaComissaoFilial> allItems = JsonConvert.DeserializeObject<List<AtualizaComissaoFilial>>(responseBody);

                return allItems;
            }
        }


    }
}
