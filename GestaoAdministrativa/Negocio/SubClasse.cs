﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    public class SubClasse
    {
        public int EmpCodigo { get; set; }
        public int SubCodigo { get; set; }
        public string SubDescr { get; set; }
        public string SubLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }

        public SubClasse() { }

        public DataTable BuscarDados(SubClasse sClasse, out string strOrdem)
        {
            string strSql = "SELECT A.EMP_CODIGO, A.SUB_CODIGO, A.SUB_DESCR, CASE WHEN A.SUB_DESABILITADO = 'N' THEN 'S' ELSE 'N' END AS SUB_LIBERADO, A.DAT_ALTERACAO, "
                      + " A.OPALTERACAO, A.DTCADASTRO, A.OPCADASTRO FROM SUBCLASSES A WHERE A.EMP_CODIGO = 1";

            if (sClasse.SubCodigo != 0)
            {
                strSql += " AND A.SUB_CODIGO = " + sClasse.SubCodigo;
            }
            if (sClasse.SubDescr != "")
            {
                strSql += " AND A.SUB_DESCR LIKE '%" + sClasse.SubDescr + "%'";
            }
            if (sClasse.SubLiberado != "TODOS")
            {
                string desabilitado = sClasse.SubLiberado == "S" ? "N" : "S";
                strSql += " AND A.SUB_DESABILITADO = '" + desabilitado + "'";
            }
            strOrdem = strSql;
            strSql += " ORDER BY A.SUB_CODIGO";


            return BancoDados.GetDataTable(strSql, null);
        }

        public bool InsereRegistros(SubClasse dados)
        {
            string desabilitado = dados.SubLiberado == "S" ? "N" : "S";
            string strCmd = "INSERT INTO SUBCLASSES(EMP_CODIGO, SUB_CODIGO, SUB_DESCR, SUB_DESABILITADO,  DTCADASTRO, OPCADASTRO) VALUES (" +
               dados.EmpCodigo + "," +
               dados.SubCodigo + ",'" +
               dados.SubDescr + "','" +
               desabilitado + "'," +
               Funcoes.BDataHora(dados.DtCadastro) + ",'" +
               dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool AtualizaDados(SubClasse dadosNovos, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE SUBCLASSES SET ";
            if (!dadosNovos.SubDescr.Equals(dadosOld.Rows[0]["SUB_DESCR"]))
            {
                strCmd = strCmd + " SUB_DESCR = '" + dadosNovos.SubDescr + "',";

                dados[contMatriz, 0] = "SUB_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SUB_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SubDescr + "'";
                contMatriz = contMatriz + 1;
            }

            if (!dadosNovos.SubLiberado.Equals(dadosOld.Rows[0]["SUB_DESABILITADO"]))
            {
                strCmd = strCmd + " SUB_DESABILITADO = '" + dadosNovos.SubLiberado + "',";

                dados[contMatriz, 0] = "SUB_DESABILITADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["SUB_DESABILITADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SubLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DAT_ALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DAT_ALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DAT_ALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;
                strCmd += " DAT_ALTERACAO = " + Funcoes.BDataHora(dadosNovos.DtAlteracao) + ",";

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.OpAlteracao + "'";
                strCmd += " OPALTERACAO = '" + dadosNovos.OpAlteracao + "' ";
                contMatriz = contMatriz + 1;

                strCmd += " WHERE SUB_CODIGO = " + dadosNovos.SubCodigo;
                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("SUB_CODIGO", dadosNovos.SubCodigo.ToString(), dadosNovos.OpAlteracao, "SUBCLASSES", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        public bool ExcluirDados(string subClasseId)
        {

            string sql = "DELETE FROM SUBCLASSES WHERE SUB_CODIGO = "+ subClasseId;
            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
                return true;
            else
            {
                return false;
            }
        }
    }
}
