﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class RelatorioDadosComprovante
    {
        public int CobrancaID { get; set; }
        public string ClienteNome { get; set; }
        public DateTime DataHora { get; set; }
        public decimal TotalVenda { get; set; }
        public string FormaDescricao { get; set; }


        public DataTable BuscaDadosClienteComprovantePorCobranca(List<object> cobrancaID)
        {
            string id = string.Join(", ", cobrancaID);
            string sql = " SELECT CO.COBRANCA_ID as CobrancaID, "
                       + " CL.CF_NOME as ClienteNome, "
                       + " CO.DT_CADASTRO as DataHora, "
                       + " FP.FORMA_DESCRICAO as FormaDescricao, "
                       + " CO.COBRANCA_TOTAL as TotalVenda"
                       + " FROM COBRANCA CO "
                       + " INNER JOIN CLIFOR CL ON(CL.CF_ID = CO.COBRANCA_CF_ID) "
                       + " INNER JOIN  FORMAS_PAGAMENTO FP ON(FP.FORMA_ID = CO.COBRANCA_FORMA_ID) "
                       + " WHERE CO.COBRANCA_ID IN (" + id + ") AND CO.COBRANCA_ORIGEM = 'C'";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
