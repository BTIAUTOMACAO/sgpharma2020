﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class AFCaixa
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public string Usuario { get; set; }
        public string CaixaAberto { get; set; }
        public DateTime DataAbertura { get; set; }
        public DateTime DataFechamento { get; set; }

        public AFCaixa() { }

        public AFCaixa(int empCodigo, int estCodigo, string usuario, string caixaAberto, DateTime dataAbertura, DateTime dataFechamento)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.Usuario = usuario;
            this.CaixaAberto = caixaAberto;
            this.DataAbertura = dataAbertura;
            this.DataFechamento = dataFechamento;

        }

        public DataTable BuscaCaixaStatus(string usuario, char caixaAberto)
        {
            string sql = " SELECT  DATA_ABERTURA, CAIXA_ABERTO, DATA_FECHAMENTO FROM CAIXA_ABERTO"
                       + " WHERE  USUARIO = '" + usuario + "' ";
            if (caixaAberto != ' ')
            {
                sql += " AND CAIXA_ABERTO = '" + caixaAberto + "'";
            }
            sql += "ORDER BY DATA_ABERTURA DESC";

            return BancoDados.GetDataTable(sql, null);

        }

        public bool AbreCaixa(AFCaixa caixa)
        {

            string sql = "INSERT INTO CAIXA_ABERTO  ( EMP_CODIGO, EST_CODIGO , USUARIO, CAIXA_ABERTO, DATA_ABERTURA) VALUES ( "
                       + caixa.EmpCodigo + ","
                       + caixa.EstCodigo + ",'"
                       + caixa.Usuario + "','"
                       + caixa.CaixaAberto + "',"
                       + Funcoes.BDataHora(caixa.DataAbertura) + ")";
            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AtualizaStatusCaixa(string usuario, char statusNovo, char statusAtual, DateTime dtDataAbertura, bool cancelamento = false)
        {
            string sql = " UPDATE CAIXA_ABERTO SET "
                          + " CAIXA_ABERTO ='" + statusNovo + "'";
            if (cancelamento)
            {
                sql += " ,DATA_FECHAMENTO = null";
            }
            else
            {
                sql += " ,DATA_FECHAMENTO = " + Funcoes.BDataHora(DateTime.Now);
            }
            sql += " WHERE USUARIO = '" + usuario + "' AND CAIXA_ABERTO = '" + statusAtual + "' AND DATA_ABERTURA = " + Funcoes.BDataHora(dtDataAbertura);

            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string IdentificaStatusDoCaixa(int empCodigo, int estCodigo, string status, string usuarioID)
        {
            string strSql = "SELECT DATA_ABERTURA FROM CAIXA_ABERTO WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo +
                 " AND CAIXA_ABERTO = '" + status + "' AND USUARIO = '" + usuarioID + "' GROUP BY DATA_ABERTURA";

            object r = BancoDados.ExecuteScalar(strSql, null);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public List<AFCaixa> DataCaixa(int empCodigo, int estCodigo, string status, string usuarioID)
        {

            string strSql = "SELECT MAX(DATA_ABERTURA) AS DATA_ABERTURA, CAIXA_ABERTO FROM CAIXA_ABERTO WHERE EMP_CODIGO  = " + empCodigo + " AND EST_CODIGO = " + estCodigo +
                 " AND CAIXA_ABERTO = '" + status + "' AND USUARIO = '" + usuarioID + "' GROUP BY CAIXA_ABERTO";

            DataTable table = BancoDados.GetDataTable(strSql, null);

            List<AFCaixa> lista = new List<AFCaixa>();
            foreach (DataRow row in table.Rows)
            {
                AFCaixa cStatus = new AFCaixa();
                cStatus.DataAbertura = Convert.ToDateTime(row["DATA_ABERTURA"]);
                cStatus.CaixaAberto = row["CAIXA_ABERTO"].ToString();
                lista.Add(cStatus);
            }

            return lista;
        }

        public string VerifcaStatusCaixa(DateTime dtInicial, DateTime dtFinal, string usuario)
        {
            string sql = " SELECT CAIXA_ABERTO FROM CAIXA_ABERTO WHERE DATA_ABERTURA BETWEEN " + Funcoes.BDataHora(dtInicial) + " AND " + Funcoes.BDataHora(dtFinal)
                       + " AND USUARIO = '" + usuario + "'";
            var retorno = BancoDados.ExecuteScalar(sql, null);

            return Convert.ToString(retorno);
        }

        public DataTable VerificaSeCaixaJaFoiFechado(string usuario)
        {
            string sql = " SELECT  DATA_ABERTURA, CAIXA_ABERTO, DATA_FECHAMENTO FROM CAIXA_ABERTO"
                       + " WHERE DATA_ABERTURA >= " + Funcoes.BData(DateTime.Now) + " AND USUARIO = '" + usuario + "' ";

            return BancoDados.GetDataTable(sql, null);

        }


        public bool CancelaAbertura(string usuario, DateTime dtDataAbertura)
        {
            string sql = "DELETE FROM CAIXA_ABERTO  WHERE USUARIO = '" + usuario + "' AND DATA_ABERTURA = " + Funcoes.BDataHora(dtDataAbertura);
            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(-1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
