﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class CriaParametros
    {
        public static bool CriaOuAtualizaParametros()
        {
            try
            {
                #region PARAMETROS VENDAS
                Funcoes.GravaParametro(6, "116", "9","GERAL", "Código da condição de pagamento ORÇAMENTO", "VENDAS", "Identifica código da condição de pagamento ORÇAMENTO", true);

                Funcoes.GravaParametro(6, "346", "N", "GERAL", "Verifica se obriga senha do colaborador na tela de Vendas", "VENDAS", "Identifica se obriga digitar a senha do colaborador para inciar a Venda", false);

                Funcoes.GravaParametro(6, "347", "N", "GERAL", "Visualiza desconto Min. e Max. na busca de produtos", "VENDAS", "Verifica se visualiza o desconto maximo e minimo na busca de produtos", false);

                Funcoes.GravaParametro(6, "348", "N", "GERAL", "Visualiza Preço PMC na busca de produtos", "VENDAS", "Verifica se visualiza o preço do PMC na busca de produtos", false);

                Funcoes.GravaParametro(6, "349", "N", Principal.nomeEstacao, "Mostra valor do ultimo custo na tela de inf. Do produto (S)Sim ou (N)Não", "VENDAS", "Identifica se obriga digitar a senha do colaborador para inciar a Venda", false);

                Funcoes.GravaParametro(6, "351", "N", "GERAL", "Controla validade por Lote", "VENDAS", "Identifica se realiza o controle de validade por Lote", false);

                Funcoes.GravaParametro(6, "352", "N", "GERAL", "Controla uso das estações", "VENDAS", "Identifica se realiza controle de estação para bloqueio de usuarios", false);

                Funcoes.GravaParametro(6, "353", "B", Principal.nomeEstacao, "Tipo da Estação: Fiscal(F) Caixa(C) Balcao(B) Estoque (E)", "VENDAS", "Identifica tipo da estação para controle de uso Fiscal(F) Caixa(X) Balcao(B) Estoque (E)", false);
                
                Funcoes.GravaParametro(6, "355", "S", Principal.nomeEstacao, "Modelo Fiscal SAT(S) ECF(E)", "VENDAS", "Identifica o modelo fiscal se é SAT ou Impressora Fiscal", false);

                Funcoes.GravaParametro(6, "356", "3", "GERAL", "Código da Condição de Paramento Entrega", "VENDAS", "Identifica a forma de pagamento para venda entrega", false);

                Funcoes.GravaParametro(6, "357", "S", "GERAL", "Bloqueia venda de Particular Limite Ultrapassado", "VENDAS", "Identifica se bloqueia venda caso cliente particular tenha ultrapassado o Limite", false);

                Funcoes.GravaParametro(6, "358", "N", "GERAL", "Obriga cadastro rapido de Cliente no Farmácia Popular", "VENDAS", "Identifica se obriga cadastro de Clientes na venda Farmácia Popular", false);

                Funcoes.GravaParametro(6, "359", "N", "GERAL", "Obriga solicitar lote na venda de Controlado", "VENDAS", "Identifica se obriga solicitar lote na venda de produto controlado", false);

                Funcoes.GravaParametro(6, "360", "N", "GERAL", "Verifica se carrega Ajuste Geral", "VENDAS", "Identifica se carrega valor do ajuste geral quando for comanda", false);

                Funcoes.GravaParametro(6, "361", "N", "GERAL", "Forca Venda Fiscal", "VENDAS", "Identifica se força Venda Fiscal na tela de Vendas", false);

                Funcoes.GravaParametro(6, "362", "N", "GERAL", "Bloqueia exclusão da Comanda no Caixa", "VENDAS", "Identifica se Bloqueia exclusão da Comanda no Caixa", false);

                Funcoes.GravaParametro(6, "363", "N", "GERAL", "Imprime desconto?", "VENDAS", "Identifica se imprime desconto dos produtos para venda SAT", false);

                Funcoes.GravaParametro(6, "364", "N", "GERAL", "Imprime Fiscal na Entrega?", "VENDAS", "Identifica se imprime Fiscal na tela de Recebimento de Entregas", false);

                Funcoes.GravaParametro(6, "365", "N", Principal.nomeEstacao, "Estação que envia Dados para Nuvem?", "VENDAS", "Identifica se é a estação que envia os dados para a nuvem.", false);

                Funcoes.GravaParametro(6, "366", "1", "GERAL", "Código do Estabelecimento", "VENDAS", "Identifica o código do estabelecimento", false);

                Funcoes.GravaParametro(6, "367", "1", "GERAL", "Código da Empresa", "VENDAS", "Identifica o código da empresa", false);

                Funcoes.GravaParametro(6, "368", "N", "GERAL", "Exibe Nome da Conveniada", "VENDAS", "Identifica se mostra o nome da empresa conveniada", false);

                Funcoes.GravaParametro(6, "369", "7891111111111", "GERAL", "Código do Produto que permite alterar Valor Unitário", "VENDAS", "Identifica o Código do Produto que permite alterar Valor Unitário", false);

                Funcoes.GravaParametro(6, "370", "Ut1ldnP-YfACGvczrT3XKoR7XsKidFMzG-_OCuc-aUIrWEyVmKwfdv_RI_vmmDLh", "GERAL", "Token para verificação de imposto", "VENDAS", "Token do site De Olho no Imposto para acessar impostos", false);

                Funcoes.GravaParametro(6, "371", "https://apidoni.ibpt.org.br/api/v1/produtos", "GERAL", "Api de Impostos", "VENDAS", "Endereço da API do De Olho no Imposto", false);

                Funcoes.GravaParametro(6, "374", "", "GERAL", "Mensagem Promocional", "VENDAS", "Mensagem Promocional para exibir na tela de vendas", false);

                Funcoes.GravaParametro(6, "375", "N", "GERAL", "Altera preço da promoção para Particular", "VENDAS", "Identifica se altera preço da promoção para vendas Particular", false);

                Funcoes.GravaParametro(6, "376", "N", "GERAL", "Muda Fiscal Benefício", "VENDAS", "Identifica se força para Fiscal quando venda é benefício (VIDALINK,EPHARMA,PORTALDROGARIA,FARMACIAPOPULAR,ORIZON)", false);

                Funcoes.GravaParametro(6, "377", "N", Principal.nomeEstacao, "Estação força fiscal", "VENDAS", "Identifica se estação força abertura no fiscal", false);

                Funcoes.GravaParametro(6, "378", "N", "GERAL", "Imprime Desconto PMC?", "VENDAS", "Identifica se imprime desconto com base no valor do PMC", false);

                Funcoes.GravaParametro(6, "379", "FF", "GERAL", "Alíquota Impressora Fiscal", "VENDAS", "Identifica a alíquota da impressora fiscal", true);

                #endregion

                #region PARAMETROS IMPRESSORA
                Funcoes.GravaParametro(2, "28", "Volte Sempre Drogabella", "GERAL", "Mensagem Promocional 1", "IMPRESSORA", "Identifica mensagem promocional 1 a ser exibida no cupom fiscal", false);

                Funcoes.GravaParametro(2, "29", "Volte Sempre Drogabella", "GERAL", "Mensagem Promocional 2", "IMPRESSORA", "Identifica mensagem promocional 2 a ser exibida no cupom fiscal", false);

                Funcoes.GravaParametro(2, "30", "S", "GERAL", "Aciona Guilhotina", "IMPRESSORA", "Identifica se aciona guilhotina na impressão do comprovante", false);

                Funcoes.GravaParametro(2, "31", "N", Principal.nomeEstacao, "Imprime entregas pendentes?", "IMPRESSORA", "Identifica se Imprime entregas pendentes", false);

                Funcoes.GravaParametro(2, "32", "28/06/2018", "GERAL", "Data da ultima impressão", "IMPRESSORA", "Identifica a data da última impressão", false);

                Funcoes.GravaParametro(2, "33", "N", "GERAL", "Imprime controle de entrega?", "IMPRESSORA", "Identifica se imprime controle de entrega.", false);
                #endregion

                #region PARAMETROS ESTOQUE
                int id = 1;
                if(!String.IsNullOrEmpty(Funcoes.LeParametro(9, "50", false)))
                {
                    id = Convert.ToInt32(Funcoes.LeParametro(9, "50", false));
                }
                if(id != Funcoes.IdentificaVerificaID("ENTRADA", "ENT_ID", Principal.estAtual, "", Principal.empAtual))
                    Funcoes.GravaParametro(9, "50", Funcoes.IdentificaVerificaID("ENTRADA","ENT_ID", Principal.estAtual,"", Principal.empAtual).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);

                Funcoes.GravaParametro(9, "51", "N", "GERAL", "Consulta estoque em outras filiais", "ESTOQUE", "Identifica se Loja possui consulta de estoque em outras filiais", false);

                Funcoes.GravaParametro(9, "52", "0", "GERAL", "Código da Loja", "ESTOQUE", "Identifica o código da loja para consulta de estoque em outras filiais", false);

                Funcoes.GravaParametro(9, "53", "0", "GERAL", "Código do Grupo das Filiais", "ESTOQUE", "Identifica o código do grupo das filiais", false);

                Funcoes.GravaParametro(9, "54", "http://ts1.drogabella.com/webApiEstoqueFilial/api/", "GERAL", "Endereço da API para consultar estoque em outras filiais", "ESTOQUE", "Identifica o endereço da API para consultar estoque em outras filiais", false);

                Funcoes.GravaParametro(9, "55", "N", "GERAL", "Atualiza Preço de Compra conforme PMC", "ESTOQUE", "Identifica se atualiza o preço de venda pelo PMC na Entrada de Notas", false);

                Funcoes.GravaParametro(9, "56", "N", "GERAL", "Avisa Estoque Minimo", "ESTOQUE", "Identifica se mostra mensagem caso produto esteja com estoque minimo", false);

                Funcoes.GravaParametro(9, "57", "201406", "GERAL", "Data da últma atualização - ABCFARMA", "ESTOQUE", "Identifica a data da ultima atualização ABCFARMA", false);

                Funcoes.GravaParametro(9, "58", "PROD_ULTCUSME", "GERAL", "Campo de Preço para Transferência", "ESTOQUE", "Identifica qual campo de preço que mostra na hora de realizar transferência", false);

                Funcoes.GravaParametro(9, "59", "N", "GERAL", "Verifica atualização de Imposto", "ESTOQUE", "Identifica se atualiza informações de imposto na hora de dar entrada na nota", false);

                Funcoes.GravaParametro(9, "60", "", "GERAL", "CNPJ da Farmácia ABCFarma", "ESTOQUE", "Identifica CNPJ cadastrado no ABCFarma para fazer atualização", false);

                Funcoes.GravaParametro(9, "61", "", "GERAL", "Senha da Farmácia ABCFarma", "ESTOQUE", "Identifica Senha cadastrada no ABCFarma para fazer atualização", false);

                Funcoes.GravaParametro(9, "62", "PROD_ULTCUSME", "GERAL", "Campo de Preço da Margem", "ESTOQUE", "Identifica o campo de preço da margem na tela de cadastro de produto", false);
                #endregion

                #region PARAMETROS BENEFICIOS
                Funcoes.GravaParametro(14, "01", "http://ts1.drogabella.com/wPegaAutor/wsconvenio.asmx?WSDL", "GERAL", "Endereço de WebService Convenio Drogabella/Plantão Card",
                    "BENEFICIOS", "Indentifica endereço de WebService Convenio Drogabella/Plantão Card", false);

                Funcoes.GravaParametro(14, "02", "200.155.3.84", "GERAL", "Endeço de IP da conexão datacenter", "BENEFICIOS",
                    "Indentifica o endereço de IP da conexão do datacenter", false);

                Funcoes.GravaParametro(14, "03", "https://200.214.130.55:9443/farmaciapopular/services/ServicoSolicitacaoWS?wsdl", "GERAL",
                    "Endereço de WS FP", "BENEFICIOS", "Endereço de HOMOLOGAÇÃO do Web Service da (WSDL) Farmacia Popular", false);

                Funcoes.GravaParametro(14, "04", "N", "GERAL", "Atende Farmacia Popular?", "BENEFICIOS", "Identifica se estabelecimento atende benefício Farmacia Popular", false);

                Funcoes.GravaParametro(14, "05", @"C:\TVAT\ENV\", "GERAL", "Caminho para gravação dos arquivos ENV TVAT Funcional", "BENEFICIOS",
                    "Caminho para gravação dos arquivos ENV TVAT Funcional", false);

                Funcoes.GravaParametro(14, "06", @"C:\TVAT\RESP\", "GERAL", "Caminho para gravação dos arquivos RESP TVAT Funcional", "BENEFICIOS",
                    "Caminho para gravação dos arquivos RESP TVAT Funcional", false);

                Funcoes.GravaParametro(14, "07", "5", "GERAL", "Tempo de espera do TVAT para o arquivo de Status STA", "BENEFICIOS",
                    "Identifica o tempo em segundos de espera do arquivo do TVAT para o arquivo de Status STA", false);

                Funcoes.GravaParametro(14, "08", "40", "GERAL", "Tempo de espera do TVAT para p arquivo de Resposta RSP", "BENEFICIOS",
                    "Identifica o tempo em segundos de espera do arquivo do TVAT para o arquivo de Resposta RSP", false);

                Funcoes.GravaParametro(14, "09", @"C:\TVAT\ENV\", "GERAL", "Caminho onde esta o arquivo AUTORIZADOR.SEQ que gera o sequencial dos arquivos", "BENEFICIOS",
                    "Caminho onde esta o arquivo AUTORIZADOR.SEQ que gera o sequencial dos arquivos", false);

                Funcoes.GravaParametro(14, "10", "S", "GERAL", "Atende e-Pharma?", "BENEFICIOS", "Identifica se estabelecimento atende benefício E-pharma", false);

                Funcoes.GravaParametro(14, "11", Funcoes.LeParametro(6,"129",false), "GERAL", "CNPJ do Estabelecimento", "BENEFICIOS",
                    "Somente Numeros do CNPJ do Estabelecimento", false);

                Funcoes.GravaParametro(14, "12", @"C:\E-PHARMA\", "GERAL", "Caminho e-Pharma", "BENEFICIOS", "Caminho dos arquivos E-Pharma", false);

                Funcoes.GravaParametro(14, "13", @"C:\E-PHARMA\ENV\", "GERAL", "Caminho de Envio e-Pharma", "BENEFICIOS", "Caminho dos arquivos de envio E-Pharma", false);

                Funcoes.GravaParametro(14, "14", @"C:\E-PHARMA\REC\", "GERAL", "Caminho de Resposta e-Pharma", "BENEFICIOS", "Caminho dos arquivos de resposta E-Pharma", false);

                Funcoes.GravaParametro(14, "15", @"C:\E-PHARMA\PBMS_PDV.exe", "GERAL", "Caminho do executavel do e-Pharma", "BENEFICIOS", "Caminho do executavel do e-Pharma", false);

                Funcoes.GravaParametro(14, "16", "10/11/2017", Principal.nomeEstacao, "Data da última atualizacao E-Pharma", "BENEFICIOS", "Indentifica a data da última atualizacao E-Pharma", false);

                Funcoes.GravaParametro(14, "17", "3000", "GERAL", "Tempo de espera em milissegundos E-Pharma", "BENEFICIOS", "Tempo de espera E-Pharma", false);

                Funcoes.GravaParametro(14, "18", "6", "GERAL", "Condição de Pagamento E-Pharma", "BENEFICIOS", "Identifica a condição de pagamento utilizada pelo E-PHARMA para desconto em folha", false);

                Funcoes.GravaParametro(14, "19", @"C:\E-PHARMA\IMG\", "GERAL", "Caminho da Pasta de Imagem e-Pharma", "BENEFICIOS", "Caminho dos arquivos de receita enviados ao E-Pharma", false);

                Funcoes.GravaParametro(14, "21", "S", "GERAL", "Atende VidaLink?", "BENEFICIOS", "Identifica se estabelecimento atende benefício VidaLink", false);

                Funcoes.GravaParametro(14, "22", @"C:\VIDALINK\", "GERAL", "Caminho VidaLink", "BENEFICIOS", "Identifica o caminho dos arquivos VidaLink", false);

                Funcoes.GravaParametro(14, "23", @"C:\VIDALINK\ENVIO\", "GERAL", "Caminho de Envio VidaLink", "BENEFICIOS", "Identifica o caminho dos arquivos de envio VidaLink", false);

                Funcoes.GravaParametro(14, "24", @"C:\VIDALINK\RESPOSTA\", "GERAL", "Caminho de Resposta VidaLink", "BENEFICIOS", "Identifica o caminho dos arquivos de resposta VidaLink", false);

                Funcoes.GravaParametro(14, "25", "5", "GERAL", "Condição de Pagamento VidaLink", "BENEFICIOS", "Identifica a condição de pagamento utilizada pelo VidaLink para desconto em folha", false);

                Funcoes.GravaParametro(14, "26","7", "GERAL", "Espécie VidaLink", "BENEFICIOS", "Identifica a espécie utilizada pelo VidaLink para desconto em folha", false);

                Funcoes.GravaParametro(14, "27", "S", "GERAL", "Atende Portal da Drogaria", "BENEFICIOS", "Identifica se estabelecimento atende benefício Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "28", @"C:\TRNCENTR\", "GERAL", "Caminho Portal da Drogaria", "BENEFICIOS", "Identifica o caminho dos arquivos Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "29", @"C:\TRNCENTR\REQ\", "GERAL", "Caminho de Envio Portal da Drogaria", "BENEFICIOS", "Identifica o caminho dos arquivos de envio Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "30", @"C:\TRNCENTR\RESP\", "GERAL", "Caminho de Resposta Portal da Drogaria", "BENEFICIOS", "Identifica o caminho dos arquivos de resposta Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "31", "7", "GERAL", "Condição de Pagamento Farmacia Popular", "BENEFICIOS", "Identifica a condição de pagamento utilizada pelo Farmacia Popular", false);

                Funcoes.GravaParametro(14, "32", "9", "GERAL", "Espécie Farmacia Popular", "BENEFICIOS", "Identifica a espécie utilizada pelo Farmacia Popular", false);

                Funcoes.GravaParametro(14, "33", "12", "GERAL", "Condição de Pagamento Portal da Drogaria", "BENEFICIOS", "Identifica a condição de pagamento utilizada pelo Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "34", "11", "GERAL", "Espécie Portal da Drogaria", "BENEFICIOS", "Identifica a espécie utilizada pelo Portal da Drogaria", false);

                Funcoes.GravaParametro(14, "35", "8", "GERAL", "Espécie E-pharma", "BENEFICIOS", "Identifica a espécie utilizada pelo E-pharma", false);

                Funcoes.GravaParametro(14, "40", @"C:\Orizon\PSC\FileExchanger.exe", "GERAL", "Caminho executavel Orizon", "BENEFICIOS", "Identifica o caminho do executavel Orizon", false);

                Funcoes.GravaParametro(14, "41", @"C:\Orizon\PSC\", "GERAL", "Caminho  Orizon", "BENEFICIOS", "Identifica o caminho dos arquivos Orizon", false);

                Funcoes.GravaParametro(14, "42", @"C:\Orizon\PSC\ENVIO\", "GERAL", "Caminho de Envio Orizon", "BENEFICIOS", "Identifica o caminho dos arquivos de envio Orizon", false);

                Funcoes.GravaParametro(14, "43", @"C:\Orizon\PSC\RECEBIMENTO\", "GERAL", "Caminho de Retorno Orizon", "BENEFICIOS", "Identifica o caminho dos arquivos de Retorno Orizon", false);

                Funcoes.GravaParametro(14, "44", "1000", "GERAL", "Tempo de espera em milissegundos Orizon", "BENEFICIOS", "Tempo de espera Orizon", false);

                Funcoes.GravaParametro(14, "45", "0001", Principal.nomeEstacao, "Identificação do PDV", "BENEFICIOS", "Identificação do Orizon", false);

                Funcoes.GravaParametro(14, "46", "8", "GERAL", "Condição de Pagamento Funcional", "BENEFICIOS", "Identifica a condição de pagamento utilizada pela Funcional para desconto em folha", false);

                Funcoes.GravaParametro(14, "47", "10", "GERAL", "Espécie Funcional", "BENEFICIOS", "Identifica a espécie utilizada pela Funcional para desconto em folha", false);

                Funcoes.GravaParametro(14, "48", @"C:\TVAT\tvat.exe", "GERAL", "Caminho do executavel do Funcional", "BENEFICIOS", "Caminho do executavel do Funcional", false);
                #endregion

                #region PARAMETROS SAT
                Funcoes.GravaParametro(15, "01", "BEMATECH", Principal.nomeEstacao, "Modelo do SAT",
                    "SAT", "Identifica o Modelo do SAT", false);

                Funcoes.GravaParametro(15, "02", "0.07", "GERAL", "Versão Layout SAT",
                    "SAT", "Identifica a versão do layout do SAT", false);

                Funcoes.GravaParametro(15, "03", Funcoes.LeParametro(6, "334", false) == "" ? "1" : Funcoes.LeParametro(6, "334", false), "GERAL", "Assinatura SAT",
                    "SAT", "Identifica a assinatura do SAT", false);

                Funcoes.GravaParametro(15, "04", "001", Principal.nomeEstacao, "Numero Caixa SAT",
                    "SAT", "Identifica o numero do caixa do SAT", false);

                Funcoes.GravaParametro(15, "05", Funcoes.LeParametro(6,"329",false) == "" ? "1" : Funcoes.LeParametro(6, "329", false), "GERAL", "Código de Ativação SAT",
                    "SAT", "Identifica o código de ativação do SAT", false);

                Funcoes.GravaParametro(15, "06", @"C:\SAT", "GERAL", "Caminho dos CFes SAT",
                    "SAT", "Identifica o caminho que são salvos os CFes emitidos pelo SAT", false);

                Funcoes.GravaParametro(15, "07", "19300727000112", "GERAL", "CNPJ da Software House",
                    "SAT", "Identifica o CNPJ da Software House", false);

                Funcoes.GravaParametro(15, "08", "012", Principal.nomeEstacao, "Código da Credenciadora",
                    "SAT", "Identifica o código da credenciadora", false);

                Funcoes.GravaParametro(15, "09", "DARUMA", "GERAL", "Fabricante Impressora Nao-Fiscal",
                    "SAT", "Identifica o fabricante da impressora usado para impressão nao-fiscal SAT", false);
                
                Funcoes.GravaParametro(15, "10", Funcoes.LeParametro(6, "333", false) == "" ? "1" : Funcoes.LeParametro(6, "333", false), "GERAL", "Número de Série SAT ",
                  "SAT", "Número de identificação do SAT", false);

                Funcoes.GravaParametro(15, "11", "2500TH", "GERAL", "Modelo Impressora Nao-Fiscal",
                    "SAT", "Identifica o modelo da impressora usado para impressão nao-fiscal SAT", false);
                #endregion

                #region PARAMETROS CAIXA
                Funcoes.GravaParametro(4, "72", "N", "GERAL", "Verifica se solicita senha para Sangria", "CAIXA", "Identifica se obriga digitar a senha do Supervisor para realizar uma Sangria", false);

                Funcoes.GravaParametro(4, "73", "N", "GERAL", "Limita valor máximo em caixa", "CAIXA", "Obriga o caixa efetuar sangria", false);

                Funcoes.GravaParametro(4, "74", "0", "GERAL", "Valor máximo permitido em caixa", "CAIXA", "Valor máximo permitido em caixa ", false);

                Funcoes.GravaParametro(4, "75", "N", "GERAL", "Imprime Recargas?", "CAIXA", "Identifica se imprime as recargas realizadas no período do fechamento de caixa ", false);

                Funcoes.GravaParametro(4, "76", "0", "GERAL", "Código do Departamento Recarga", "CAIXA", "Identifica o código do departamento da recarga de celular", false);

                Funcoes.GravaParametro(4, "78", "0", "GERAL", "Código de Operação de Troco", "CAIXA", "Identifica o código da operação de Troco", false);

                Funcoes.GravaParametro(4, "79", "0", "GERAL", "Código de Operação de Troco Entrada", "CAIXA", "Identifica o código da operação de Troco Entrada", false);
                #endregion

                #region PARAMETROS PAGAR
                Funcoes.GravaParametro(7, "01", "1", "GERAL", "Identifica código do comprador", "PAGAR", "Identifica o código do comprador cadastrado", false);

                #endregion

                #region PARAMETROS CADASTRO
                Funcoes.GravaParametro(3, "20", "1", "GERAL", "Identifica código da Empresa Conveniada de Entrega", "CADASTRO", "Identifica o código da Empresa Conveniada de Entrega", false);

                #endregion

                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
