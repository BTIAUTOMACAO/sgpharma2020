﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Negocio
{
    class PedidoItem
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long PedNumero { get; set; }
        public string PedComplemento { get; set; }
        public int PedItemSeq { get; set; }
        public string ProdCodigo { get; set; }
        public int ProdID { get; set; }
        public int PedItemQtde { get; set; }
        public double PedItemVlunit { get; set; }
        public double PedItemSubtotal { get; set; }
        public double PedItemDifer { get; set; }
        public double PedItemTotal { get; set; }
        public double PedItemBase { get; set; }
        public double PedItemAIcms { get; set; }
        public double PedItemVIcms { get; set; }
        public double PedItemAIpi { get; set; }
        public double PedItemVIpi { get; set; }
        public double PedItemAIss { get; set; }
        public double PedItemVIss { get; set; }
        public string PedItemUnidade { get; set; }
        public double PedItemComissao { get; set; }
        public double PreValor { get; set; }
        public double ProdCusme { get; set; }
        public string ProdTipo { get; set; }
        public string PedItemDescr { get; set; }
        public string ImpEcf { get; set; }
        public int ColCodigo { get; set; }
        public string PedItemPromo { get; set; }
        public string PedItemDescLib { get; set; }
        public long EstIndice { get; set; }
        public string PedItemLote { get; set; }
        public string PedItemReceita { get; set; }

        public PedidoItem() { }

        public bool ExcluirDados(int estCodigo, int empCodigo, long pedNumero, string pedComplemento)
        {
            string strCmd = "DELETE FROM PEDIDOS_ITENS WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo
                + " AND PED_NUMERO = " + pedNumero + " AND PED_COMPL = '" + pedComplemento + "'";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public bool InserirItensPreVenda(PedidoItem dados)
        {
            string strCmd = "INSERT INTO PEDIDOS_ITENS(EMP_CODIGO, EST_CODIGO, PED_NUMERO, PED_COMPL, ITEM_NUMERO, PROD_CODIGO, PROD_ID, "
                + "ITEM_QTDE, ITEM_VLUNIT, ITEM_SUBTOTAL, ITEM_DIFER, ITEM_TOTAL, ITEM_BASE, ITEM_AICMS, ITEM_VICMS, ITEM_AIPI, ITEM_VIPI, ITEM_AISS, "
                + "ITEM_VISS, ITEM_UNIDADE, ITEM_COMISSAO, PRE_VALOR, PROD_CUSME, PROD_TIPO, ITEM_DESCR, IMP_ECF, EST_INDICE, COL_CODIGO, ITEM_PROMOCAO, "
                + "ITEM_DESC_LIB, ITEM_LOTE, ITEM_RECEITA) "
                + "VALUES (" + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.PedNumero + ",'"
                + dados.PedComplemento + "',"
                + dados.PedItemSeq + ",'"
                + dados.ProdCodigo + "',"
                + dados.ProdID + ","
                + dados.PedItemQtde + ","
                + Funcoes.BValor(dados.PedItemVlunit) + ","
                + Funcoes.BValor(dados.PedItemSubtotal) + ","
                + Funcoes.BValor(dados.PedItemDifer) + ","
                + Funcoes.BValor(dados.PedItemTotal) + ","
                + Funcoes.BValor(dados.PedItemBase) + ","
                + Funcoes.BValor(dados.PedItemAIcms) + ","
                + Funcoes.BValor(dados.PedItemVIcms) + ","
                + Funcoes.BValor(dados.PedItemAIpi) + ","
                + Funcoes.BValor(dados.PedItemVIpi) + ","
                + Funcoes.BValor(dados.PedItemAIss) + ","
                + Funcoes.BValor(dados.PedItemVIss) + ",'"
                + dados.PedItemUnidade + "',"
                + Funcoes.BValor(dados.PedItemComissao) + ","
                + Funcoes.BValor(dados.PreValor) + ","
                + Funcoes.BValor(dados.ProdCusme) + ",'"
                + dados.ProdTipo + "','"
                + dados.PedItemDescr + "','"
                + dados.ImpEcf + "',";
            if (!dados.EstIndice.Equals(0))
            {
                strCmd += Funcoes.BValor(dados.EstIndice) + ",";
            }
            else
                strCmd += "null,";

            strCmd += dados.ColCodigo + ",'"
                + dados.PedItemPromo + "','"
                + dados.PedItemDescLib + "',";

            if (!String.IsNullOrEmpty(dados.PedItemLote))
            {
                strCmd +=  "'" + dados.PedItemLote + "',";
            }
            else
                strCmd += "null,";

            strCmd += "'" + dados.PedItemReceita + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

    }
}
