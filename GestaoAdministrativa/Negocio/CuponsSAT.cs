﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    class CuponsSAT
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public long VendaID { get; set; }
        public string VendaNumNota { get; set; }
        public string VendaNumCfe { get; set; }
        public double VendaTotal { get; set; }
        public string VendaEmitido { get; set; }
        public string VendaCancelado { get; set; }
        public string VendaNumeroSessao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }

        public CuponsSAT() { }

        public CuponsSAT(int empCodigo, int estCodigo, long vendaID, string vendaNumNota, string vendaNumCfe, double vendaTotal, string vendaEmitido, string vendaCancelado, string vendaNumeroSessao,
            DateTime dtCadastro, string opCadastro, DateTime dtAlteracao, string opAlteracao)
        {
            this.EmpCodigo = empCodigo;
            this.EstCodigo = estCodigo;
            this.VendaID = vendaID;
            this.VendaNumNota = vendaNumNota;
            this.VendaNumCfe = vendaNumCfe;
            this.VendaTotal = vendaTotal;
            this.VendaEmitido = vendaEmitido;
            this.VendaCancelado = vendaCancelado;
            this.VendaNumeroSessao = vendaNumeroSessao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
        }

        public bool InserirDados(CuponsSAT dados, bool transAberta = false)
        {
            string strCmd = "INSERT INTO CUPONS_SAT(EMP_CODIGO,EST_CODIGO,VENDA_ID,VENDA_NUM_NOTA,VENDA_NUM_CFE,VENDA_TOTAL,VENDA_EMITIDO,VENDA_NUMERO_SESSAO,"
                + "DT_CADASTRO,OP_CADASTRO) VALUES ("
                + dados.EmpCodigo + ","
                + dados.EstCodigo + ","
                + dados.VendaID + ",'"
                + dados.VendaNumNota + "','"
                + dados.VendaNumCfe + "',"
                + Funcoes.BValor(dados.VendaTotal) + ",'"
                + dados.VendaEmitido + "','"
                + dados.VendaNumeroSessao + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (transAberta)
            {
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public DataTable BuscaCuponsDoDia(string data)
        {
            string strSql = "SELECT To_Char(A.DT_CADASTRO,'DD/MM/YYYY') AS DATA_EMISSAO, To_Char(A.DT_CADASTRO, 'HH24:MI:SS') AS HORA,A.VENDA_NUM_CFE,"
                    + " CASE WHEN A.DT_CADASTRO >= SYSDATE - 30 / 24 / 60 THEN 'S' ELSE 'X' END AS LIBERADO, A.VENDA_EMITIDO,  A.VENDA_TOTAL"
                    + " FROM CUPONS_SAT A"
                    + " WHERE To_Char(A.DT_CADASTRO, 'DD/MM/YYYY') = TO_DATE('" + data + "', 'DD/MM/YYYY')"
                    + " AND A.VENDA_EMITIDO = 'S' ORDER BY A.DT_CADASTRO DESC";

            return BancoDados.GetDataTable(strSql, null);
        }

        public bool CancelaCupom(int estCodigo, int empCodigo, string chave, DateTime dtAlteracao, string operador, string cupom)
        {
            string strCmd = "UPDATE CUPONS_SAT SET VENDA_EMITIDO = 'C', DT_ALTERACAO = " + Funcoes.BDataHora(dtAlteracao) + ", OP_ALTERACAO = '"
                + operador + "', VENDA_CANCELADO = '" + cupom + "'  WHERE EST_CODIGO = " + estCodigo + " AND EMP_CODIGO = " + empCodigo + " AND VENDA_NUM_CFE = '" + chave + "'";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public DataTable BuscaHoraCupom(long vendaId)
        {
            string sql = "SELECT DT_CADASTRO, VENDA_NUM_CFE, VENDA_TOTAL FROM CUPONS_SAT WHERE VENDA_ID = " + vendaId;
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
