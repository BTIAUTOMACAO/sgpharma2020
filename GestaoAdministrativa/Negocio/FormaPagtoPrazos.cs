﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class FormaPagtoPrazos
    {
        public int EmpCodigo { get; set; }
        public int CprNum { get; set; }
        public int CprParcela { get; set; }
        public int CprPrazo { get; set; }
        public int CprPerc { get; set; }
        public string CprCondicao { get; set; }

        public FormaPagtoPrazos() { }

        public FormaPagtoPrazos(int empCodigo, int cprNum, int crpParcela, int cprPrazo, int cprPerc, string cprCondicao)
        {
            this.EmpCodigo = empCodigo;
            this.CprNum = cprNum;
            this.CprParcela = crpParcela;
            this.CprPrazo = cprPrazo;
            this.CprPerc = cprPerc;
            this.CprCondicao = cprCondicao;
        }

        public bool InsereRegistros(FormaPagtoPrazos dados)
        {
            string strCmd = "INSERT INTO COND_PAGTO_PRAZOS (EMP_CODIGO, CPR_NUM, CPR_PARCELA, CPR_PRAZO, CPR_PERC, CPR_CONDICAO) VALUES("
                + dados.EmpCodigo + ","
                + dados.CprNum + ","
                + dados.CprParcela + ","
                + dados.CprPrazo + ","
                + dados.CprPerc + ",'"
                + dados.CprCondicao + "')";
            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public int ExcluirDados(int empCodigo, string cprNum)
        {
            string strCmd = "DELETE FROM COND_PAGTO_PRAZOS WHERE EMP_CODIGO = " + empCodigo + " AND CPR_NUM = " + cprNum;

            return BancoDados.ExecuteNoQueryTrans(strCmd, null);
        }

        public DataTable BuscaDados(int empCodigo, int cprNum)
        {
            return BancoDados.GetDataTable("SELECT * FROM COND_PAGTO_PRAZOS A  WHERE A.EMP_CODIGO = " + empCodigo + " AND CPR_NUM = " + cprNum, null);
        }
    }
}
