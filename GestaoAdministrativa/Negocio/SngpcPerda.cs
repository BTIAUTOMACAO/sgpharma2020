﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Negocio
{
    public class SngpcPerda
    {
        public int SngpcEmpCodigo { get; set; }
        public int SngpcEstCodigo { get; set; }
        public int SngpcPerSeq { get; set; }
        public int SngpcMotivo { get; set; }
        public string SngpcProdCodigo { get; set; }
        public string SngpcMedLote { get; set; }
        public int SngpMedQtde { get; set; }
        public DateTime SngpDataPerca { get; set; }
        public string SngpMS { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }


        public SngpcPerda() { }

        public SngpcPerda(int sngpcEmpCodigo, int sngpcEstCodigo, int sngpcPerSeq, int sngpcMotivo, string sngpcProdCodigo,
                          string sngpcMedLote, int sngpMedQtde, DateTime sngpDataPerca, string sngpMS, DateTime dtAlteracao,
                          string opAlteracao, DateTime dtCadastro, string opCadastro)
        {

            this.SngpcEmpCodigo = sngpcEmpCodigo;
            this.SngpcEstCodigo = sngpcEstCodigo;
            this.SngpcPerSeq = sngpcPerSeq;
            this.SngpcMotivo = sngpcMotivo;
            this.SngpcProdCodigo = sngpcProdCodigo;
            this.SngpcMedLote = sngpcMedLote;
            this.SngpMedQtde = sngpMedQtde;
            this.SngpDataPerca = sngpDataPerca;
            this.SngpMS = sngpMS;
            this.DtAlteracao = dtAlteracao;
            this.OpAlteracao = opAlteracao;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;

        }

        public DataTable BuscarProdutos(SngpcPerda dados, out string strOrdem)
        {

            string sql = " SELECT PERD.PER_SEQ, PERD.PROD_CODIGO, PERD.PER_MED_LOTE, PERD.PER_MED_QTDE, PERD.PER_DATA, "
                       + " PERD.PER_MS, PERD.DT_ALTERACAO, PERD.DT_CADASTRO, PERD.OP_CADASTRO, PERD.OP_ALTERACAO, "
                       + " TAB.TAB_DESCRICAO AS MOTIVO, PRO.PROD_DESCR, PRO.PROD_UNIDADE, "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' ELSE 'SUJEITO A CONTROLE ESPECIAL' END AS PROD_CLASSE_TERAP "
                       + " FROM SNGPC_PERDA PERD "
                       + " INNER JOIN SNGPC_TABELAS TAB ON(TAB.TAB_SEQ = PERD.PER_MOTIVO) "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = PERD.PROD_CODIGO)"
                       + " WHERE PERD.EMP_CODIGO = " + Principal.empAtual + " AND PERD.EST_CODIGO = " + Principal.estAtual;

            if (dados.SngpcMotivo != 0)
            {
                sql += " AND PER_MOTIVO = " + dados.SngpcMotivo;
            }
            if (dados.SngpcProdCodigo != "")
            {
                sql += " AND PROD_CODIGO = '" + dados.SngpcProdCodigo + "'";
            }
            if (dados.SngpMS != "")
            {
                sql += " AND PER_MS = '" + dados.SngpMS + "'";
            }
            strOrdem = sql;

            sql += " ORDER BY PERD.PER_DATA";

            return BancoDados.GetDataTable(sql, null);

        }

        public bool AtualizaPerda(SngpcPerda dadosNovos, DataTable dadosAntigos)
        {
            string sql = "  UPDATE SNGPC_PERDA SET ";

            string[,] dados = new string[6, 3];
            int contMatriz = 0;

            if (dadosNovos.SngpcMotivo != Convert.ToInt32(dadosAntigos.Rows[0]["PER_MOTIVO"]))
            {
                sql += " PER_MOTIVO ='" + dadosNovos.SngpcMotivo + "', ";

                dados[contMatriz, 0] = "PER_MOTIVO";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PER_MOTIVO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SngpcMotivo.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.SngpcMedLote != dadosAntigos.Rows[0]["PER_MED_LOTE"].ToString())
            {
                sql += " PER_MED_LOTE ='" + dadosNovos.SngpcMedLote + "', ";

                dados[contMatriz, 0] = "PER_MED_LOTE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PER_MED_LOTE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SngpcMedLote.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.SngpMedQtde != Convert.ToInt32(dadosAntigos.Rows[0]["PER_MED_QTDE"]))
            {
                sql += " PER_MED_QTDE ='" + dadosNovos.SngpMedQtde + "', ";

                dados[contMatriz, 0] = "PER_MED_QTDE";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PER_MED_QTDE"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SngpMedQtde.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.SngpDataPerca.ToString("dd-MM-yyyy") != Convert.ToDateTime(dadosAntigos.Rows[0]["PER_DATA"]).ToString("dd-MM-yyyy"))
            {
                sql += " PER_DATA ='" + dadosNovos.SngpDataPerca.ToString("dd-MM-yyyy") + "', ";

                dados[contMatriz, 0] = "PER_DATA";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PER_DATA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SngpDataPerca.ToString() + "'";
                contMatriz = contMatriz + 1;
            }
            if (dadosNovos.SngpMS != dadosAntigos.Rows[0]["PER_MS"].ToString())
            {
                sql += " PER_MS ='" + dadosNovos.SngpMS + "', ";

                dados[contMatriz, 0] = "PER_MS";
                dados[contMatriz, 1] = "'" + dadosAntigos.Rows[0]["PER_MS"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNovos.SngpMS.ToString() + "'";
                contMatriz = contMatriz + 1;
            }

            if (contMatriz != 0)
            {

                sql += " OP_ALTERACAO = '" + Principal.usuario + "', ";
                sql += " DT_ALTERACAO = " + Funcoes.BDataHora(DateTime.Now);
                sql += " WHERE PER_SEQ = " + dadosNovos.SngpcPerSeq + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual;

                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
                {
                    Funcoes.GravaLogAlteracao("PER_SEQ", dadosNovos.SngpcPerSeq.ToString(), Principal.usuario.ToUpper(), "SNGPC_PERDA", dados, contMatriz, Principal.estAtual, Principal.empAtual, true);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public bool IncluirPerda(SngpcPerda dados)
        {

            string sql = " INSERT INTO SNGPC_PERDA ( EMP_CODIGO, EST_CODIGO, PER_SEQ, PER_MOTIVO, PROD_CODIGO, PER_MED_LOTE, "
                       + " PER_MED_QTDE, PER_DATA, PER_MS, OP_CADASTRO, DT_CADASTRO ) VALUES ("
                       + dados.SngpcEmpCodigo + " , "
                       + dados.SngpcEstCodigo + " , "
                       + dados.SngpcPerSeq + " , "
                       + dados.SngpcMotivo + ",'"
                       + dados.SngpcProdCodigo + "', '"
                       + dados.SngpcMedLote + "', "
                       + dados.SngpMedQtde + ","
                       + Funcoes.BData(dados.SngpDataPerca) + " , '"
                       + dados.SngpMS + "','"
                       + Principal.usuario + "', "
                       + Funcoes.BDataHora(DateTime.Now) + ")";

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ExcluirPerda(int codigo)
        {

            string sql = "DELETE FROM SNGPC_PERDA WHERE EST_CODIGO = " + Principal.estAtual + " AND EMP_CODIGO = " + Principal.empAtual + " AND PER_SEQ = " + codigo;

            if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(1))
            {
                return true;
            }
            else
            {

                return false;
            }
        }

        public DataTable BuscaPerdasPorPeriodo(string dtInicial, string dtFinal, char ordem)
        {
            string sql = " SELECT SP.PROD_CODIGO, PRO.PROD_DESCR, PRO.PROD_UNIDADE, SP.PER_MED_LOTE, SP.PER_MED_QTDE, SP.PER_DATA, SP.PER_MS, TAB.TAB_DESCRICAO, TAB.TAB_CODIGO, "
                       + " CASE PRO.PROD_CLASSE_TERAP WHEN '1' THEN 'ANTIMICROBIANO' ELSE 'SUJEITO A CONTROLE ESPECIAL' END AS PROD_CLASSE_TERAP"
                       + " FROM SNGPC_PERDA SP "
                       + " INNER JOIN PRODUTOS PRO ON(PRO.PROD_CODIGO = SP.PROD_CODIGO) "
                       + " INNER JOIN SNGPC_TABELAS TAB ON (TAB.TAB_SEQ = SP.PER_MOTIVO) "
                       + " WHERE SP.PER_DATA BETWEEN TO_DATE('" + dtInicial + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') "
                       + " AND TO_DATE('" + dtFinal + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')"
                       + " AND SP.EMP_CODIGO = " + Principal.empAtual + " AND SP.EST_CODIGO = " + Principal.estAtual; 
            if (ordem == 'C')
            {
                sql += " ORDER BY SP.PROD_CODIGO";
            }
            else
            {
                sql += " ORDER BY PRO.PROD_DESCR";
            }

            return BancoDados.GetDataTable(sql, null);
        }
    }
}
