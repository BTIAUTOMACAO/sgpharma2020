﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using SqlNegocio;
using System.Data;

namespace GestaoAdministrativa.Negocio
{
    class Colaboradores
    {
        public int EstCodigo { get; set; }
        public int ColCodigo { get; set; }
        public string ColNome { get; set; }
        public string ColApelido { get; set; }
        public string ColEndereco { get; set; }
        public string ColBairro { get; set; }
        public string ColCidade { get; set; }
        public string ColUf { get; set; }
        public string ColCep { get; set; }
        public string ColFone { get; set; }
        public string ColCelular { get; set; }
        public string ColEmail { get; set; }
        public string ColStatus { get; set; }
        public string ColRg { get; set; }
        public decimal ColValorHora { get; set; }
        public decimal ColComissao { get; set; }
        public string ColSenha { get; set; }
        public string ColSenhaSup { get; set; }
        public string ColLiberado { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int OpAlteracao { get; set; }
        public DateTime DtCadastro { get; set; }
        public int OpCadastro { get; set; }

        public Colaboradores() { }

        public static bool InserirDados(Colaboradores dados)
        {
            MontadorSql mont = new MontadorSql("colaboradores", MontadorType.Insert);
            mont.AddField("est_codigo", dados.EstCodigo);
            mont.AddField("col_codigo", dados.ColCodigo);
            mont.AddField("col_nome", dados.ColNome);
            mont.AddField("col_apelido", dados.ColApelido);
            mont.AddField("col_ender", dados.ColEndereco);
            mont.AddField("col_bairro", dados.ColBairro);
            mont.AddField("col_cidade", dados.ColCidade);
            mont.AddField("col_uf", dados.ColUf);
            mont.AddField("col_cep", dados.ColCep);
            mont.AddField("col_fone", dados.ColFone);

            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public static string GetDadosColaborador(int estCodigo, string colCodigo, string colStatus)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("col_codigo", colCodigo));
            ps.Add(new Fields("col_status", colStatus));

            Principal.strSql = "SELECT COL_CODIGO FROM COLABORADORES WHERE EST_CODIGO = @est_codigo AND COL_CODIGO = @col_codigo AND COL_STATUS = @col_status";


            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public static string GetSColaborador(int estCodigo, string colCodigo, string colStatus)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("col_codigo", colCodigo));
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("col_status", colStatus));

            Principal.strSql = " SELECT COL_SENHA FROM COLABORADORES WHERE COL_CODIGO = @col_codigo AND EST_CODIGO = @est_codigo AND COL_STATUS = @col_status";

            object r = BancoDados.ExecuteScalar(Principal.strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        public static string GetNomeVendedor(int estCodigo, string id)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo",estCodigo));
            ps.Add(new Fields("id", id));

            Principal.strSql = "SELECT COL_NOME FROM COLABORADORES WHERE EST_CODIGO = @est_codigo and COL_CODIGO = @id";

            return BancoDados.ExecuteScalar(Principal.strSql, ps).ToString();
        }

        public static DataTable GetDadosVendedor(int estCodigo, string colStatus)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("col_status", colStatus));

            Principal.strSql = "SELECT COL_CODIGO, COL_NOME FROM COLABORADORES WHERE EST_CODIGO = @est_codigo AND COL_STATUS = @col_status"
                + " AND COL_LIBERADO = 'S' ORDER BY COL_NOME";
            return BancoDados.GetDataTable(Principal.strSql, ps);
        }

        public static DataTable GetBuscar(Colaboradores dadosBusca, out string strOrdem)
        {
            Principal.strSql = "SELECT A.EST_CODIGO, A.COL_CODIGO, A.COL_NOME, A.COL_APELIDO, A.COL_ENDER, A.COL_BAIRRO, A.COL_CIDADE, "
            + "A.COL_UF, A.COL_CEP, A.COL_FONE, A.COL_CELULAR, A.COL_EMAIL, A.COL_STATUS, A.COL_RG, A.COL_VALOR_HORA, A.COL_COMISSAO, A.COL_SENHA, A.COL_SENHA_SUP, A.COL_LIBERADO, "
            + "A.DTALTERACAO, (SELECT NOME FROM USUARIO_SYSTEM WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO FROM COLABORADORES A, "
            + "USUARIO_SYSTEM B WHERE A.EST_CODIGO = " + Principal.estAtual + " AND A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.ColCodigo == 0 && dadosBusca.ColNome == "" && dadosBusca.ColLiberado == "TODOS")
            {
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.EST_CODIGO, A.COL_CODIGO";
            }
            else
            {
                if (dadosBusca.ColCodigo != 0)
                {
                    Principal.strSql += " AND A.COL_CODIGO = " + dadosBusca.ColCodigo;
                }
                if (dadosBusca.ColNome != "")
                {
                    Principal.strSql += " AND A.COL_NOME LIKE '%" + dadosBusca.ColNome + "%'";
                }
                if (dadosBusca.ColLiberado != "TODOS")
                {
                    Principal.strSql += " AND A.COL_LIBERADO = '" + dadosBusca.ColLiberado + "'";
                }
                strOrdem = Principal.strSql;
                Principal.strSql += " ORDER BY A.EST_CODIGO, A.COL_CODIGO";
            }

            return BancoDados.GetDataTable(Principal.strSql, null);
        }

    }
}
