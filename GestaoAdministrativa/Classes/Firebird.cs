﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirebirdSql.Data.FirebirdClient;
using System.Data;
using System.Windows.Forms;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// CLASSE QUE CONTEM OS METODOS PRESENTES NA CLASSE BANCO DE DADOS COM O USO DO FIREBIRD
    /// </summary>
    public class Firebird : IConexao 
    {   
        private string strCon;
        private FbConnection strConexao = new FbConnection();
        private FbTransaction transacao = null;
        public FbTransaction pedTrans = null;
        private FbCommand comando = new FbCommand();

        DataTable dt = new DataTable();

        public Firebird(string strConexao)
        {
            strCon = strConexao;
        }

        public bool conectar(string paramentroConexao)
        {
            try
            {
                strConexao.ConnectionString = paramentroConexao;
                strConexao.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message,"Firebird", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool fecharConexao()
        {
            try
            {
                if (strConexao.State == ConnectionState.Open)
                {
                    strConexao.Close();
                    strConexao.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable selecionarRegistros(string comandoSql)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            using (FbDataAdapter daPesq = new FbDataAdapter(comandoSql, strConexao))
            {
                DataTable dtPesq = new DataTable();
                daPesq.Fill(dtPesq);
                daPesq.Dispose();
                return dtPesq;
            }
        }

        public string selecionarRegistro(string comandoSql, string campo)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (FbDataAdapter daPesq = new FbDataAdapter(comandoSql, strConexao))
            {
                dt.Clear();
                daPesq.Fill(dt);

                if (dt.Rows.Count != 0)
                {
                    return dt.Rows[0]["" + campo + ""].ToString();
                }
                else
                    return String.Empty;
            }
        }

        public bool executarComando(string [] comando, int indice)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                transacao = strConexao.BeginTransaction();
                for (int i = 0; i < indice; i++)
                {
                    using (FbCommand Comando = new FbCommand())
                    {
                        Comando.Transaction = transacao;
                        Comando.CommandText = comando[i];
                        Comando.Connection = strConexao;
                        Comando.ExecuteNonQuery();
                        Comando.Dispose();
                    }
                }
                transacao.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool executarSql(string comandoSql)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                using (FbCommand Comando = new FbCommand())
                {
                    transacao = strConexao.BeginTransaction();
                    Comando.Transaction = transacao;
                    Comando.CommandText = comandoSql;
                    Comando.Connection = strConexao;
                    Comando.ExecuteNonQuery();
                    transacao.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public int ExecuteNoQuery(String pComando, SqlParamsList ParamsList)
        {
            int ret;
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (FbCommand Cmd = new FbCommand())
            {
                transacao = strConexao.BeginTransaction();
                Cmd.CommandText = pComando;
                Cmd.Transaction = transacao;
                Cmd.Connection = strConexao;
                SetParams(Cmd, ParamsList);
                try
                {
                    ret = Cmd.ExecuteNonQuery();
                    transacao.Commit();
                }
                catch(Exception ex)
                {
                    ret = -1;
                    transacao.Rollback();
                }
            }
            return ret;
        }

        public void AbrirTrans()
        {
            pedTrans = strConexao.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void FecharTrans()
        {
            pedTrans.Commit();
        }

        public void ErroTrans()
        {
            pedTrans.Rollback();
        }

        public int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList)
        {
            int ret;
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (FbCommand Cmd = new FbCommand())
            {
                Cmd.CommandText = pComando;
                Cmd.Transaction = pedTrans;
                Cmd.Connection = strConexao;
                SetParams(Cmd, ParamsList);
                try
                {
                    ret = Cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ret = -1;
                }
            }
            return ret;
        }

        /// <summary>
        /// Método para retornar um DataTable a partir de uma intrução sql.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna o DataTable carregado</returns>
        public DataTable GetDataTable(String pComando, SqlParamsList ParamsList)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            using (FbDataAdapter daPesq = new FbDataAdapter(pComando, strConexao))
            {
                DataTable dtPesq = new DataTable();
                daPesq.SelectCommand.CommandText = pComando;
                daPesq.SelectCommand.Connection = strConexao;
                SetParams(daPesq.SelectCommand, ParamsList);
                daPesq.Fill(dtPesq);
                daPesq.Dispose();
                return dtPesq;
            }
        }

        // <summary>
        /// Método para recuperar somente um campo do banco de dados.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna um dado do Tipo Object</returns>
        public object ExecuteScalar(String pComando, SqlParamsList ParamsList)
        {
            object r;

            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            //using ()
            //{
                comando.CommandText = pComando;
                comando.Connection = strConexao;
                SetParams(comando, ParamsList);
                r = comando.ExecuteScalar();
            //}
            return r;
        }



        /// <summary>
        /// Metodo que atribui paramentros no Cmd(parametro) passado, conforme o Banco(FireBird).
        /// </summary>
        /// <param name="Cmd">DBCommand que irá receber os paramentros</param>
        /// <param name="ParamsList">Lista dos paramentros.</param>
        /// <returns>Retorna um int, indicando a quantidade de parametros adicionados ao Cmd</returns>
        private int SetParams(IDbCommand Cmd, SqlParamsList ParamsList)
        {
            int iReturn = 0;

            if (ParamsList == null)
            return iReturn;

            char cDef = '@';
            char cNew = ' ';

            cNew = '@';

            Cmd.CommandText = Cmd.CommandText.Replace(cDef, cNew);
            Cmd.Parameters.Clear();

            foreach (Fields Param in ParamsList)
            {
                if (Param.Nome.Substring(0, 1) != "@")
                    Param.Nome = "@" + Param.Nome;

                FbParameter ParamFb = new FbParameter();
                ParamFb.ParameterName = Param.Nome;
                ParamFb.Value = Param.Valor;
                Cmd.Parameters.Add(ParamFb);
            
                iReturn++;
            }
            return iReturn;
      
        }


    }

}
