﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;
using System.Globalization;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Classes
{
    public static class Funcoes
    {
        /// <summary>
        /// OBTEM A DATA ATUAL
        /// </summary>
        /// <returns>Retorna a Data</returns>
        public static string GetDataAtual()
        {
            return DateTime.Now.ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// MUDA MENSAGEM DO STATUSSTRIP, SENDO POSSIVEL MANDAR A MENSAGEM OU CASO NAO PASSAR SERA EXIBIDO O NOME DO ESTABELECIMENTO
        /// </summary>
        /// <param name="mensagem">Mensagem a ser exibida no menu inferior</param>
        /// <returns>Mensagem</returns>
        public static string MudaMensagem(string mensagem)
        {
            string strLocal;
            DataTable dsEmpEst;
            if (mensagem.ToString().Length == 0)
            {
                strLocal = "SELECT EST_FANTASIA FROM ESTABELECIMENTOS"
                    + " WHERE EST_CODIGO = " + Principal.estAtual;
                dsEmpEst = BancoDados.selecionarRegistros(strLocal);
                if (dsEmpEst.Rows.Count == 0)
                {
                    Principal.nomeAtual = "";
                }
                else
                {
                    Principal.nomeAtual = (string)dsEmpEst.Rows[0]["EST_FANTASIA"];
                }
                return "Estab.: " + Principal.estAtual + " -> "
                    + Principal.nomeAtual + " / Usuário: " + Principal.usuario + " / Servidor: ";
            }
            else
            {
                return mensagem.ToString();
            }
        }

        /// <summary>
        /// OBTEM REGISTROS DO FORM PARA HABILITAR BOTOES DE NAVEGAÇÃO
        /// </summary>
        /// <param name="dtTabela">DataTable com a seleção de registros</param>
        /// <param name="posicao">Linha em que se encontra a navegação</param>
        /// <returns>Modelo para exibição dos menu</returns>
        public static string BotoesNavegacao(DataTable dtTabela, int posicao)
        {
            //NENHUM REGISTRO//
            if (dtTabela.Rows.Count == 0)
            {
                return "NNNN";
            }
            //SOMENTE UM REGISTRO//
            if (dtTabela.Rows.Count == 1)
            {
                return "SNNN";
            }
            //ULTIMO REGISTRO
            if (posicao == (dtTabela.Rows.Count - 1))
            {
                return "SSNN";
            }
            //PRIMEIRO REGISTRO//
            if (posicao == 0)
            {
                return "NNSS";
            }
            else
            {
                //VARIOS REGISTROS//
                return "SSSS";
            }
        }

        /// <summary>
        /// FORMATAÇÃO DO MESSABOX
        /// </summary>
        public static void Avisa()
        {
            MessageBox.Show(Principal.mensagem, Principal.msgTitulo, Principal.msgEstilo, Principal.msgIcone);
        }

        /// <summary>
        /// LIMPA CONTROLS DO FORMULARIO
        /// </summary>
        /// <param name="parent">Formulario Corrente</param>
        public static void LimpaFormularios(System.Windows.Forms.Control parent)
        {
            try
            {
                foreach (System.Windows.Forms.Control ctrControl in parent.Controls)
                {
                    if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.TextBox)))
                    {
                        ((System.Windows.Forms.TextBox)ctrControl).Text = string.Empty;
                    }
                    else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.RichTextBox)))
                    {
                        ((System.Windows.Forms.RichTextBox)ctrControl).Text = string.Empty;
                    }
                    else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.ComboBox)))
                    {
                        ((System.Windows.Forms.ComboBox)ctrControl).SelectedIndex = -1;
                    }
                    else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.CheckBox)))
                    {
                        ((System.Windows.Forms.CheckBox)ctrControl).Checked = false;
                    }
                    else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.RadioButton)))
                    {
                        ((System.Windows.Forms.RadioButton)ctrControl).Checked = false;
                    }
                    else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.MaskedTextBox)))
                    {
                        ((System.Windows.Forms.MaskedTextBox)ctrControl).Text = string.Empty;
                    }
                    if (ctrControl.Controls.Count > 0)
                    {
                        LimpaFormularios(ctrControl);
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// VERIFICA SE CAMPO ESTA VAZIO OU NÃO
        /// </summary>
        /// <param name="valor">Texto do Campo</param>
        /// <returns>Valor do Campo</returns>
        public static string ChecaCampoVazio(string valor)
        {
            if (String.IsNullOrEmpty(valor))
            {
                return "";
            }
            else
                return valor;
        }

        /// <summary>
        /// FORMATA DATA E HORA CONFORME BANCO DE DADOS(USADO PARA COMANDO SQL)
        /// </summary>
        /// <param name="data">DataHora</param>
        /// <returns>DataHora Formatada</returns>
        public static string BDataHora(DateTime data)
        {
            string dtBanco = String.Empty;
            switch (Principal.nomeBanco)
            {
                case "Oracle":
                    dtBanco = "to_date('" + data + "', 'DD/MM/YYYY HH24:MI:SS')";
                    break;
                case "MySql":
                    dtBanco = "'" + data.ToString("yyyy/MM/dd") + "'";
                    break;
                case "Firebird":
                    dtBanco = "'" + data.ToString("dd.MM.yyyy HH:mm:ss") + "'";
                    break;
                case "SqlServer":
                    dtBanco = "'" + data.ToString("dd/MM/yyyy HH:mm:ss") + "'";
                    break;
            }
            return dtBanco;
        }

        /// <summary>
        /// FORMATA DATA CONFORME BANCO DE DADOS (USADO PARA COMANDO SQL)
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Data Formatada</returns>
        public static string BData(DateTime data)
        {
            string dtBanco = String.Empty;
            switch (Principal.nomeBanco)
            {
                case "Oracle":
                    dtBanco = "to_date('" + data.ToString().Substring(0, 10) + "', 'DD/MM/YYYY')";
                    break;
                case "MySql":
                    dtBanco = "'" + data.ToString("yyyy/MM/dd") + "'";
                    break;
                case "Firebird":
                    dtBanco = "'" + data.ToString("dd.MM.yyyy") + "'";
                    break;
                case "SqlServer":
                    dtBanco = "'" + data.ToString("dd.MM.yyyy") + "'";
                    break;
            }
            return dtBanco;
        }

        /// <summary>
        /// GRAVA LOG QUANDO ATUALIZA UM CONJUNTO DE REGISTROS OU UM REGISTRO
        /// </summary>
        /// <param name="id">Campo chave da Tabela</param>
        /// <param name="idValor">Valor do campo chave</param>
        /// <param name="operador">ID do Usuário</param>
        /// <param name="tabela">Nome da Tabela alterada</param>
        /// <param name="campos">Array com os campos alterados</param>
        /// <param name="indice">Qtde de registros atualizados</param>
        /// <param name="estCodigo">Cód. do Estabecimento</param>
        /// <param name="empCodigo">Cód. da Empresa</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GravaLogAlteracao(string id, string idValor, string operador, string tabela, string[,] campos, int indice, int estCodigo, int empCodigo = 1, bool transacaoAberta = false)
        {
            string sql;
            for (int i = 0; i < indice; i++)
            {
                sql = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                    + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO)"
                    + " VALUES ('" + id + "','" + idValor + "','" + operador + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", '" + tabela + "', '"
                    + campos[i, 0] + "'," + campos[i, 1] + "," + campos[i, 2] + "," + estCodigo + "," + empCodigo + ")";
                if (transacaoAberta.Equals(true))
                {
                    if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
                        return false;
                }
                else
                    if (BancoDados.ExecuteNoQuery(sql, null).Equals(false))
                    return false;
            }

            return true;
        }

        internal static void GravaLogExclusao(string v1, string text1, string usuario, DateTime dateTime, string v2, object text2, string motivo, int estAtual)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GRAVA LOG QUANDO EXCLUI UM REGISTRO
        /// </summary>
        /// <param name="id">Campo chave da Tabela</param>
        /// <param name="idValor">Valor do campo chave</param>
        /// <param name="operador">ID do Usuário</param>
        /// <param name="data">Data da Alteração</param>
        /// <param name="tabela">Nome da Tabela alterada</param>
        /// <param name="valAlterado">Valor do campo excluido</param>
        /// <param name="motivo">Motivo da exclusão</param>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="empCodigo">Cód. da Empresa</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GravaLogExclusao(string id, string idValor, string operador, DateTime data, string tabela, string valAlterado, string motivo, int estCodigo, int empCodigo = 1, bool transacaoAberta = false)
        {
            string sql;
            sql = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, LOG_ALTERADO, LOG_MOTIVO, EST_CODIGO)"
            + " VALUES ('" + id + "','" + idValor + "','" + operador + "', 'EXCLUSÃO', " + Funcoes.BDataHora(data) + ", '" + tabela + "','"
            + valAlterado + "','" + motivo + "'," + estCodigo + ")";
            if (transacaoAberta.Equals(true))
            {
                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
                    return false;
            }
            else
                if (BancoDados.ExecuteNoQuery(sql, null).Equals(false))
                return false;

            return true;
        }

        /// <summary>
        /// GRAVA LOG QUANDO INSERE UM REGISTRO
        /// </summary>
        /// <param name="id">Campo chave da Tabela</param>
        /// <param name="idValor">Valor do campo chave</param>
        /// <param name="operador">ID do Usuário</param>
        /// <param name="tabela">Nome da Tabela alterada</param>
        /// <param name="valAlterado">Valor do campo Inserido</param>
        /// <param name="estCodigo">Cód. do Estabecimento</param>
        /// <param name="empCodigo">Cód. da Empresa</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GravaLogInclusao(string id, string idValor, string operador, string tabela, string valAlterado, int estCodigo, int empCodigo = 1, bool transacaoAberta = false)
        {
            string sql;
            sql = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO)"
            + " VALUES ('" + id + "','" + idValor + "','" + operador + "', 'INCLUSÃO', " + Funcoes.BDataHora(DateTime.Now) + ", '" + tabela + "','"
            + valAlterado + "'," + estCodigo + "," + empCodigo + ")";
            if (transacaoAberta.Equals(true))
            {
                if (BancoDados.ExecuteNoQueryTrans(sql, null).Equals(-1))
                    return false;
            }
            else
                if (BancoDados.ExecuteNoQuery(sql, null).Equals(false))
                return false;

            return true;
        }

        /// <summary>
        /// FORMATA DATA E HORA CONFORME BANCO DE DADOS
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Data Formatada</returns>
        public static string FormataData(DateTime data)
        {
            string dtBanco = String.Empty;
            switch (Principal.nomeBanco)
            {
                case "Oracle":
                    dtBanco = data.ToString("dd/MM/yyyy");
                    break;
                case "MySql":
                    dtBanco = data.ToString("yyyy/MM/dd ");
                    break;
                case "Firebird":
                    dtBanco = data.ToString("dd.MM.yyyy");
                    break;
                case "SqlServer":
                    dtBanco = data.ToString("dd.MM.yyyy");
                    break;
            }
            return dtBanco;
        }

        /// <summary>
        /// HABILITA BOTOES ATUALIZAR, INSERIR E EXCLUIR DE ACORDO COM A PERMISSAO DO USUARIO//
        /// </summary>
        /// <param name="formulario">Nome do Formulario</param>
        public static void BotoesCadastro(string formulario)
        {
            try
            {
                Principal.strSql = "SELECT P.INCLUI, P.ALTERA, P.EXCLUI FROM PERMISSOES P, MODULO_MENU M, USUARIOS U"
                           + " WHERE P.MODULO_ID = M.MODULO_ID AND M.FORMULARIO = '" + formulario + "' AND U.LOGIN_ID = '" + Principal.usuario + "'"
                           + " AND U.GRUPO_ID = P.GRUPO_ID";
                Principal.dtPesq = BancoDados.selecionarRegistros(Principal.strSql);
                if (Principal.dtPesq.Rows.Count > 0)
                {
                    if ((string)Principal.dtPesq.Rows[0]["INCLUI"] == "S")
                    {
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                    }
                    else
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;

                    if ((string)Principal.dtPesq.Rows[0]["ALTERA"] == "S")
                    {
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;
                    }
                    else
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;

                    if ((string)Principal.dtPesq.Rows[0]["EXCLUI"] == "S")
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;
                    }
                    else
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Botões de Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// CARREGA INDÍCE DO COMBO CONFORME VALOR DIGITADO
        /// </summary>
        /// <param name="combo">Nome do ComboBox</param>
        /// <param name="txtBox">Nome do TextBox</param>
        public static void AchaCombo(ComboBox combo, TextBox txtBox)
        {
            if (txtBox.Text != "")
            {
                combo.SelectedValue = txtBox.Text;

                if (combo.SelectedValue == null)
                {
                    txtBox.Text = "";
                }
            }
        }

        /// <summary>
        /// REALIZA A VALIDAÇÃO DE CNPJ
        /// </summary>
        /// <param name="cnpj">Número do CNPJ</param>
        /// <returns>CNPJ Formatado</returns>
        public static bool ValidaCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// REALIZA A VALIDACAO DE CPF
        /// </summary>
        /// <param name="cpf">Número do CPF</param>
        /// <returns>CPF Formatado</returns>
        public static bool ValidaCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// CRIPTOGRAFA SENHA PARA SALVAR NO BANCO DE DADOS
        /// </summary>
        /// <param name="senhaCriptog">Senha sem Criptografia</param>
        /// <returns>Senha Criptografada</returns>
        public static string CriptografaSenha(string senhaCriptog)
        {
            try
            {
                TripleDESCryptoServiceProvider objcriptografaSenha = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objcriptoMd5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = "BTI";

                byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objcriptografaSenha.Key = byteHash;
                objcriptografaSenha.Mode = CipherMode.ECB;

                byteBuff = ASCIIEncoding.ASCII.GetBytes(senhaCriptog);
                return Convert.ToBase64String(objcriptografaSenha.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return string.Format("Digite os valores Corretamente : {0}", ex.Message);
            }
        }

        /// <summary>
        /// DESCRIPTOGRAFA SENHA
        /// </summary>
        /// <param name="senhaDescrip">Senha Criptografada</param>
        /// <returns>Senha Descriptografada</returns>
        public static string DescriptografaSenha(string senhaDescrip)
        {
            try
            {
                TripleDESCryptoServiceProvider objdescriptografaSenha = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objcriptoMd5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = "BTI";

                byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objdescriptografaSenha.Key = byteHash;
                objdescriptografaSenha.Mode = CipherMode.ECB;

                byteBuff = Convert.FromBase64String(senhaDescrip);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objdescriptografaSenha.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objdescriptografaSenha = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return string.Format("Digite os valores Corretamente : {0}", ex.Message);
            }
        }

        /// <summary>
        /// FORMATA NUMERO COM TAMANHO DE ZEROS À ESQUEDA
        /// </summary>
        /// <param name="numero">String para inserir Zeros</param>
        /// <param name="tamanho">Tamanho da String</param>
        /// <returns>String Formatada</returns>
        public static string FormataZeroAEsquerda(string numero, int tamanho)
        {
            string sNumero;

            sNumero = numero.Trim();

            while (sNumero.Length < tamanho)
            {
                sNumero = "0" + sNumero;
            }

            return sNumero;

        }

        public static string FormataZeroADireita(string numero, int tamanho)
        {
            string sNumero;

            sNumero = numero.Trim();

            while (sNumero.Length < tamanho)
            {
                sNumero = sNumero + "0";
            }

            return sNumero;

        }

        /// <summary>
        /// RETONA CODIGO USADO NOS ARQUIVOS SNGPC
        /// </summary>
        /// <param name="codigo">Cód. da Tabela SNGPC</param>
        /// <returns>Nº do Cód, caso seja 0 valor não encontrado</returns>
        public static string AchaTabela(string codigo)
        {
            if (codigo != "")
            {
                Principal.dtBusca = BancoDados.selecionarRegistros("SELECT * FROM SNGPC_TABELAS WHERE TAB_ID = " + codigo);
                if (Principal.dtBusca.Rows.Count != 0)
                {
                    return Principal.dtBusca.Rows[0]["TAB_CODIGO"].ToString();
                }
                else
                    return "0";
            }
            else
                return "0";
        }

        /// <summary>
        /// REALIZA A LEITUA DO PARAMENTRO
        /// </summary>
        /// <param name="tabela">Cód. da Tabela do Parametro</param>
        /// <param name="posicao">Posição do Parametro</param>
        /// <param name="avisa">Exibe mensagem caso parametro esteja vazio</param>
        /// <param name="estacao">Nome da Estação (Opcional)</param>
        /// <returns>Valor do Parametro</returns>
        public static string LeParametro(int tabela, string posicao, bool avisa, string estacao = "")
        {
            try
            {
                string strSql;
                DataTable dtRetorno = new DataTable();

                strSql = "SELECT * FROM PARAMETROS WHERE PAR_TABELA = " + tabela
                    + " AND PAR_POSICAO = '" + posicao + "'";
                if (estacao != "")
                {
                    strSql += " AND PAR_ESTACAO = '" + estacao + "'";
                }
                else
                    strSql += " AND PAR_ESTACAO = 'GERAL'";

                dtRetorno = BancoDados.GetDataTable(strSql, null);
                if (dtRetorno.Rows.Count == 0)
                {
                    if (avisa == true)
                    {
                        MessageBox.Show("O parâmetro " + tabela + "/" + posicao + " não foi identificado!\nContate o administrador do sistema!", "Erro de parametrização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return "";
                    }
                    else
                    {
                        return "";
                    }
                }
                else if (avisa == true && dtRetorno.Rows[0]["PAR_DESCRICAO"].ToString() == "")
                {
                    MessageBox.Show("O parâmetro " + tabela + "/" + posicao + " não está preenchido!\nContate o administrador do sistema!", "Erro de parametrização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
                return Funcoes.ChecaCampoVazio(Convert.ToString(dtRetorno.Rows[0]["PAR_DESCRICAO"]));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lê Paramentro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }

        /// <summary>
        /// ATUALIZA OU INSERE NOVO PARAMETRO
        /// </summary>
        /// <param name="tabela">Cód. da Tabela do Parametro</param>
        /// <param name="posicao">Posição do Parametro</param>
        /// <param name="descr">Valor do Parametro</param>
        /// <param name="estacao">Nome da Estação</param>
        /// <param name="comentario">Descrição do Parametro</param>
        /// <param name="modulo">Nome do modulo que parametro é usado</param>
        /// <param name="obs">Observação do Parametro</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GravaParametro(int tabela, string posicao, string descr, string estacao = "", string comentario = "", string modulo = "", string obs = "", bool atualiza = true)
        {
            try
            {
                string[,] dados = new string[7, 3];
                string strSql;
                DataTable dtRetorno = new DataTable();
                int contMatriz = 0;

                strSql = "SELECT * FROM PARAMETROS WHERE PAR_TABELA = " + tabela
                   + " AND PAR_POSICAO = '" + posicao + "'";
                if (estacao != "")
                {
                    strSql += " AND PAR_ESTACAO = '" + estacao + "'";
                }
                else
                    strSql += " AND PAR_ESTACAO = 'GERAL'";

                dtRetorno = BancoDados.selecionarRegistros(strSql);
                if (dtRetorno.Rows.Count == 1 && atualiza)
                {
                    strSql = "UPDATE PARAMETROS SET ";
                    if (!descr.Equals(dtRetorno.Rows[0]["PAR_DESCRICAO"]))
                    {
                        dados[contMatriz, 0] = "PAR_DESCRICAO";
                        dados[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PAR_DESCRICAO"] + "'";
                        dados[contMatriz, 2] = "'" + descr + "'";
                        strSql += "PAR_DESCRICAO = ";
                        strSql += descr == "" ? "null" : "'" + descr + "'";
                        contMatriz = contMatriz + 1;
                    }
                    if (!comentario.Equals(dtRetorno.Rows[0]["PAR_COMENT"]))
                    {
                        dados[contMatriz, 0] = "PAR_COMENT";
                        dados[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PAR_COMENT"] + "'";
                        dados[contMatriz, 2] = "'" + comentario + "'";
                        if (contMatriz != 0)
                        {
                            strSql += ",";
                        }
                        strSql += "PAR_COMENT = '" + comentario + "'";
                        contMatriz = contMatriz + 1;
                    }
                    if (!modulo.Equals(dtRetorno.Rows[0]["PAR_MODULO"]))
                    {
                        dados[contMatriz, 0] = "PAR_MODULO";
                        dados[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PAR_MODULO"] + "'";
                        dados[contMatriz, 2] = "'" + modulo + "'";
                        if (contMatriz != 0)
                        {
                            strSql += ",";
                        }
                        strSql += "PAR_MODULO = '" + modulo + "'";
                        contMatriz = contMatriz + 1;
                    }
                    if (!obs.Equals(dtRetorno.Rows[0]["PAR_OBS"]))
                    {
                        dados[contMatriz, 0] = "PAR_OBS";
                        dados[contMatriz, 1] = "'" + dtRetorno.Rows[0]["PAR_OBS"] + "'";
                        dados[contMatriz, 2] = "'" + obs + "'";
                        if (contMatriz != 0)
                        {
                            strSql += ",";
                        }
                        strSql += "PAR_OBS = '" + obs + "'";
                        contMatriz = contMatriz + 1;
                    }
                    if (contMatriz != 0)
                    {
                        strSql += " WHERE PAR_TABELA = " + tabela + " AND PAR_POSICAO = '" + posicao + "'";
                        if (estacao != "")
                        {
                            strSql += " AND PAR_ESTACAO = '" + estacao + "'";
                        }
                        else
                            strSql += " AND PAR_ESTACAO = 'GERAL'";
                        if (BancoDados.executarSql(strSql).Equals(true))
                        {
                            Funcoes.GravaLogAlteracao("PAR_TABELA", tabela + " - " + posicao, Principal.usuario, "PARAMETROS", dados, contMatriz, Principal.estAtual);
                            return true;
                        }
                        else
                            return false;
                    }
                    return true;
                }
                else if (dtRetorno.Rows.Count == 0)
                {
                    strSql = "INSERT INTO PARAMETROS(PAR_TABELA, PAR_POSICAO, PAR_ESTACAO, PAR_DESCRICAO, PAR_COMENT, PAR_SEQ, PAR_MODULO, PAR_OBS) VALUES ("
                        + tabela + ",'" + posicao + "',";
                    strSql += estacao == "" ? "'GERAL'," : "'" + estacao + "',";
                    strSql += "'" + descr + "',";
                    strSql += comentario == "" ? "null," : "'" + comentario + "',";
                    strSql += posicao + ",";
                    strSql += modulo == "" ? "null," : "'" + modulo.ToUpper() + "',";
                    strSql += obs == "" ? "null)" : "'" + obs + "')";
                    BancoDados.executarSql(strSql);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grava Paramentro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        /// <summary>
        /// FORMATA VALOR PARA INSERIR NO BANCO DE DADOS
        /// </summary>
        /// <param name="valor">Valor</param>
        /// <returns>Valor Formatado</returns>
        public static string BValor(double valor)
        {
            string vBanco = String.Empty;
            string valorRetorno = Convert.ToString(valor);
            switch (Principal.nomeBanco)
            {
                case "Oracle":
                    valorRetorno = valorRetorno.Trim().Replace(".", "");
                    vBanco = valorRetorno.Trim().Replace(",", ".");
                    break;
                case "MySql":
                    break;
                case "Firebird":
                    valorRetorno = valorRetorno.Trim().Replace(".", "");
                    vBanco = valorRetorno.Trim().Replace(",", ".");
                    break;
                case "SqlServer":
                    valorRetorno = valorRetorno.Trim().Replace(".", "");
                    vBanco = valorRetorno.Trim().Replace(",", ".");
                    break;
            }
            return vBanco;
        }


        public static string BFormataValor(double valor)
        {
            string vBanco = String.Empty;
            switch (Principal.nomeBanco)
            {
                case "Oracle":
                    vBanco = Convert.ToString(valor).Replace(",", ".");
                    break;
                case "MySql":
                    break;
                case "Firebird":
                    vBanco = Convert.ToString(valor).Replace(",", ".");
                    break;
                case "SqlServer":
                    vBanco = Convert.ToString(valor).Replace(",", ".");
                    break;
            }
            return vBanco;
        }

        /// <summary>
        /// RETORNA ID DA TABELA PARA INSERIR REGISTROS
        /// </summary>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="campo">Campo ID da Tabela</param>
        /// <param name="estabelecimento">Cód. do Estabelecimento (OPCIONAL)</param>
        /// <returns>ID</returns>
        public static int IdentificaVerificaID(string tabela, string campo, int estabelecimento = 0, string condicao = "", int empresa = 0)
        {

            DataTable dt = new DataTable();
            string strSql;

            strSql = "SELECT MAX(" + campo + ") AS " + campo + " FROM " + tabela;
            if (empresa != 0 && estabelecimento != 0)
            {
                strSql += " WHERE EMP_CODIGO = " + empresa + " AND EST_CODIGO = " + estabelecimento;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (estabelecimento != 0)
            {
                strSql += " WHERE EST_CODIGO = " + estabelecimento;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (empresa != 0)
            {
                strSql += " WHERE EMP_CODIGO = " + empresa;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (!String.IsNullOrEmpty(condicao))
            {
                strSql += " WHERE " + condicao;
            }

            dt = BancoDados.selecionarRegistros(strSql);
            if (dt.Rows[0]["" + campo + ""].ToString() != "")
            {
                return Convert.ToInt32(dt.Rows[0]["" + campo + ""]) + 1;
            }
            else
            {
                return 1;
            }
        }

        public static long GeraIDLong(string tabela, string campo, int estabelecimento = 0, string condicao = "", int empresa = 0)
        {

            DataTable dt = new DataTable();
            string strSql;

            strSql = "SELECT MAX(" + campo + ") AS " + campo + " FROM " + tabela;
            if (empresa != 0 && estabelecimento != 0)
            {
                strSql += " WHERE EMP_CODIGO = " + empresa + " AND EST_CODIGO = " + estabelecimento;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (estabelecimento != 0)
            {
                strSql += " WHERE EST_CODIGO = " + estabelecimento;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (empresa != 0)
            {
                strSql += " WHERE EMP_CODIGO = " + empresa;

                if (!String.IsNullOrEmpty(condicao))
                {
                    strSql += " AND " + condicao;
                }
            }
            else if (!String.IsNullOrEmpty(condicao))
            {
                strSql += " WHERE " + condicao;
            }

            dt = BancoDados.selecionarRegistros(strSql);
            if (dt.Rows[0]["" + campo + ""].ToString() != "")
            {
                return Convert.ToInt64(dt.Rows[0]["" + campo + ""]) + 1;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// OBTEM O NOME DO MES POR EXTENSO
        /// </summary>
        /// <param name="mes">Número do Mês</param>
        /// <returns>Mês por Extenso</returns>
        public static string MesExtenso(string mes)
        {
            switch (mes)
            {
                case "01":
                    mes = "Janeiro";
                    break;
                case "02":
                    mes = "Fevereiro";
                    break;
                case "03":
                    mes = "Março";
                    break;
                case "04":
                    mes = "Abril";
                    break;
                case "05":
                    mes = "Maio";
                    break;
                case "06":
                    mes = "Junho";
                    break;
                case "07":
                    mes = "Julho";
                    break;
                case "08":
                    mes = "Agosto";
                    break;
                case "09":
                    mes = "Setembro";
                    break;
                case "10":
                    mes = "Outubro";
                    break;
                case "11":
                    mes = "Novembro";
                    break;
                case "12":
                    mes = "Dezembro";
                    break;
            }

            return mes;

        }

        /// <summary>
        /// ABRE FORM PARA ESCOLHER OPCOES DE FILTROS
        /// </summary>
        /// <param name="idDepto">ID da Tabela Departamentos</param>
        /// <param name="idClasse">ID da Tabela Classes</param>
        /// <param name="idSubClasse">ID da Tabela SubClasses</param>
        /// <param name="idFab">ID da Tabela Fabricantes</param>
        /// <returns>Retorna Filtros</returns>
        public static string FiltraProdutos(string idDepto, string idClasse, string idSubClasse, string idFab)
        {
            var filtroProd = new frmFiltroProd();
            filtroProd.idDepto = idDepto;
            filtroProd.idClasse = idClasse;
            filtroProd.idSubClasse = idSubClasse;
            filtroProd.idFab = idFab;
            filtroProd.ShowDialog();
            return filtroProd.filtro;
        }

        /// <summary>
        /// CENTRALIZA TEXTO NA IMPRESSAO NÃO-FISCAL
        /// </summary>
        /// <param name="texto">Texto a ser centralizado</param>
        /// <param name="tamanho">N° de Colunas</param>
        /// <returns>Texto Formatado</returns>
        public static string CentralizaTexto(string texto, int tamanho)
        {
            string espaco = "";
            if (texto.Length >= tamanho)
            {
                return texto;
            }
            else
            {
                espaco = new string(' ', (tamanho - texto.Length) / 2);
                return espaco + texto;
            }
        }

        /// <summary>
        /// LE O NOME DA IMPRESSORA
        /// </summary>
        /// <param name="idImp">ID do Parametro da Impressora Desejada</param>
        /// <param name="estacao">Nome da Estação</param>
        /// <returns>Nome da Impressora</returns>
        public static string LeImpressora(string idImp, string estacao)
        {
            string impressora;
            string comentario = "";
            if (Convert.ToInt32(idImp) < 10)
            {
                impressora = LeParametro(8, "0" + idImp, false, estacao);
            }
            else
            {
                impressora = LeParametro(8, idImp, false, estacao);
            }
            if (String.IsNullOrEmpty(impressora))
            {
                frmImpressoras selImpressora = new frmImpressoras();
                selImpressora.ShowDialog();
                impressora = Principal.impressora;
                switch (idImp)
                {
                    case "1":
                        comentario = "Impressora para impressão das Notas Fiscais (Impressão Texto)";
                        break;
                    case "2":
                        comentario = "Impressora para impressão dos Pedidos (Impressão Texto)";
                        break;
                    case "3":
                        comentario = "Impressora para impressão dos Boletos (Impressão Texto)";
                        break;
                    case "4":
                        comentario = "Impressora para impressão das Duplicatas (Impressão Texto)";
                        break;
                    case "5":
                        comentario = "Impressora para impressão de Operações de Caixa (Impressão Texto)";
                        break;
                    case "6":
                        comentario = "Impressoara para impressão das O.S. (Impressão Texto)";
                        break;
                    case "7":
                        comentario = "Impressora para impressão das Vendas (Impressão Texto)";
                        break;
                    case "8":
                        comentario = "Impressora para impressão das Notas Promissórias (Impressão Texto)";
                        break;
                    case "9":
                        comentario = "Impressora para impressão de Comprovante de Cancelamento (Impressão Texto)";
                        break;
                    case "10":
                        comentario = "Impressora para impressão de Comprovante de Consignação (Impressão Texto)";
                        break;
                }
                if (!String.IsNullOrEmpty(impressora))
                {
                    if (Convert.ToInt32(idImp) < 10)
                    {
                        Funcoes.GravaParametro(8, "0" + idImp, impressora, estacao, comentario, "IMPRESSORA", comentario);
                    }
                    else
                    {
                        Funcoes.GravaParametro(8, idImp, impressora, estacao, comentario, "IMPRESSORA", comentario);
                    }
                }
            }
            return impressora;
        }

        /// <summary>
        /// VERIFICA SE DATA DO COMPUTADOR ESTA CORRETA
        /// </summary>
        /// <returns>Verdadeiro caso consiga obter a data do servidor e falso caso não seja Valida</returns>
        public static bool ValidaData()
        {
            try
            {
                const string ntpServer = "pool.ntp.org";
                var ntpData = new byte[48];
                ntpData[0] = 0x1B; //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

                var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);
                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                ulong intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                ulong fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];

                var segundos = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
                var data = (new DateTime(1900, 1, 1)).AddMilliseconds((long)segundos);
                Principal.data = data.ToString("dd/MM/yyyy");
                return true;
            }
            catch (Exception ex)
            {
                string retorno = ex.Message;
                if (MessageBox.Show("Não foi possível validar a Data. Deseja continuar?", "GA", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Principal.data = DateTime.Now.ToString("dd/MM/yyyy");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// ORDENA AS COLUNAS DO DATAGRID
        /// </summary>
        /// <param name="dg">Nome do DataGrid</param>
        /// <returns>Lista dos Campos Ordenados</returns>
        public static List<DataGridViewColumn> OrdenaColunas(DataGridView dg)
        {
            List<DataGridViewColumn> columns = new List<DataGridViewColumn>();

            foreach (DataGridViewColumn column in dg.Columns)
            {
                columns.Add(column);
            }

            columns.Sort(delegate (DataGridViewColumn column1, DataGridViewColumn column2)
            {
                return column1.DisplayIndex.CompareTo(column2.DisplayIndex);
            });

            return columns;
        }

        /// <summary>
        /// METODO QUE VERIFICA SE PRODUTO ENCONTRA-SE EM ALGUM DESCONTO CADASTRADO
        /// </summary>
        /// <param name="empCodigo">Cód. do Estabelecimento</param>
        /// <param name="conID">Cód. da Empresa Conveniada</param>
        /// <param name="prodCodigo">Cód. do Produto</param>
        /// <param name="depCodigo">Cód. do Departamento</param>
        /// <param name="clasCodigo">Cód. da Classe</param>
        /// <param name="subCodigo">Cód. da SubClasse</param>
        /// <returns>Retorna o Desconto MIN. e Desconto Max.</returns>
        public static DataTable VerificaProdDesconto(int empCodigo, int conID = 0, int prodCodigo = 0, int depCodigo = 0, int clasCodigo = 0, int subCodigo = 0)
        {
            string strSql = "";
            DataTable dt = new DataTable();

            strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
            strSql += " AND CON_ID = " + conID;
            strSql += " AND PROD_ID = " + prodCodigo;
            strSql += " AND DEP_CODIGO = " + depCodigo;
            strSql += " AND CLAS_CODIGO = " + clasCodigo;
            strSql += " AND SUB_CODIGO = " + subCodigo;
            strSql += " AND DESC_DESABILITADO = 'N'";
            dt = BancoDados.selecionarRegistros(strSql);
            if (dt.Rows.Count == 0)
            {
                strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                strSql += " AND CON_ID = " + conID;
                strSql += " AND PROD_ID = 0";
                strSql += " AND DEP_CODIGO = " + depCodigo;
                strSql += " AND CLAS_CODIGO = " + clasCodigo;
                strSql += " AND SUB_CODIGO = " + subCodigo;
                strSql += " AND DESC_DESABILITADO = 'N'";
                dt = BancoDados.selecionarRegistros(strSql);
                if (dt.Rows.Count == 0)
                {
                    strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                    strSql += " AND CON_ID = " + conID;
                    strSql += " AND PROD_ID = " + prodCodigo;
                    strSql += " AND DEP_CODIGO = 0";
                    strSql += " AND CLAS_CODIGO = 0";
                    strSql += " AND SUB_CODIGO = 0";
                    strSql += " AND DESC_DESABILITADO = 'N'";
                    dt = BancoDados.selecionarRegistros(strSql);
                    if (dt.Rows.Count == 0)
                    {
                        strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                        strSql += " AND CON_ID = " + conID;
                        strSql += " AND PROD_ID = 0";
                        strSql += " AND DEP_CODIGO = " + depCodigo;
                        strSql += " AND CLAS_CODIGO = 0";
                        strSql += " AND SUB_CODIGO = 0";
                        strSql += " AND DESC_DESABILITADO = 'N'";
                        dt = BancoDados.selecionarRegistros(strSql);
                        if (dt.Rows.Count == 0)
                        {
                            strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                            strSql += " AND CON_ID = " + conID + " AND PROD_ID = 0 AND DEP_CODIGO = 0 AND CLAS_CODIGO = 0 AND SUB_CODIGO = 0 AND DESC_DESABILITADO = 'N'";
                            dt = BancoDados.selecionarRegistros(strSql);
                            if (dt.Rows.Count == 0)
                            {
                                strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                                strSql += " AND CON_ID = 0 AND PROD_ID = " + prodCodigo + " AND DEP_CODIGO = 0 AND CLAS_CODIGO = 0 AND SUB_CODIGO = 0 AND DESC_DESABILITADO = 'N'";
                                dt = BancoDados.selecionarRegistros(strSql);
                                if (dt.Rows.Count == 0)
                                {
                                    strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                                    strSql += " AND CON_ID = 0 AND PROD_ID = 0 AND DEP_CODIGO = " + depCodigo + " AND CLAS_CODIGO = 0 AND SUB_CODIGO = 0 AND DESC_DESABILITADO = 'N'";
                                    dt = BancoDados.selecionarRegistros(strSql);
                                    if (dt.Rows.Count == 0)
                                    {
                                        strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                                        strSql += " AND CON_ID = 0 AND PROD_ID = 0 AND DEP_CODIGO = 0 AND CLAS_CODIGO = " + clasCodigo + " AND SUB_CODIGO = 0 AND DESC_DESABILITADO = 'N'";
                                        dt = BancoDados.selecionarRegistros(strSql);
                                        if (dt.Rows.Count == 0)
                                        {
                                            strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                                            strSql += " AND CON_ID = 0 AND PROD_ID = 0 AND DEP_CODIGO = 0 AND CLAS_CODIGO = 0 AND SUB_CODIGO = " + subCodigo + " AND DESC_DESABILITADO = 'N'";
                                            dt = BancoDados.selecionarRegistros(strSql);
                                            if (dt.Rows.Count == 0)
                                            {
                                                strSql = "  SELECT DESC_MIN, DESC_MAX FROM DESCONTOS WHERE EMP_CODIGO = " + empCodigo;
                                                strSql += " AND CON_ID = " + conID;
                                                strSql += " AND PROD_ID = 0 AND DEP_CODIGO = " + depCodigo;
                                                strSql += " AND CLAS_CODIGO = " + clasCodigo;
                                                strSql += " AND SUB_CODIGO = " + subCodigo;
                                                strSql += " AND DESC_DESABILITADO = 'N'";
                                                dt = BancoDados.selecionarRegistros(strSql);
                                                if (dt.Rows.Count == 0)
                                                {
                                                    return dt;
                                                }
                                                else
                                                    return dt;
                                            }
                                            else
                                                return dt;
                                        }
                                        else
                                            return dt;
                                    }
                                    else
                                        return dt;
                                }
                                else
                                    return dt;
                            }
                            else
                                return dt;
                        }
                        else
                            return dt;
                    }
                    else
                        return dt;
                }
                else
                    return dt;
            }
            return dt;
        }

        /// <summary>
        /// VERIFICA SE PRODUTO ESTA EM PROMOÇÃO
        /// </summary>
        /// <param name="empCodigo">Cód. da Empresa</param>
        /// <param name="prodCodigo">Cód. do Produto</param>
        /// <param name="depCodigo">Cód. do Departamento</param>
        /// <param name="clasCodigo">Cód. da Classe</param>
        /// <param name="subCodigo">Cód. da SubClasse</param>
        /// <returns>Retorna o tipo, o periodo, preço ou porcentagem</returns>
        public static DataTable IdentificaProdutoEmPromocao(int empCodigo, int prodCodigo = 0, int depCodigo = 0, int clasCodigo = 0, int subCodigo = 0)
        {
            string strSql = "";
            string where = "";
            DataTable dt = new DataTable();

            strSql = "  SELECT CASE PROMO_TIPO WHEN 'D' THEN 'DINAMICO' ELSE 'FIXO' END AS PROMO_TIPO,";
            strSql += "    CASE PROMO_TIPO";
            strSql += "     WHEN 'F' THEN";
            strSql += "      DIA_INI";
            strSql += "     ELSE";
            strSql += "        DATA_INI END AS DATA_INI,";
            strSql += "   CASE PROMO_TIPO";
            strSql += "     WHEN 'F' THEN";
            strSql += "      DIA_FIM";
            strSql += "     ELSE";
            strSql += "     DATA_FIM END AS DATA_FIM,";
            strSql += " PROMO_PRECO, PROMO_PORC, DESCONTO_PROGRESSIVO, LEVE, PAGUE, PORCENTAGEM FROM PROMOCOES WHERE EMP_CODIGO = " + empCodigo + " AND PROMO_DESABILITADO= 'N'";

            where = " AND PROD_ID = " + prodCodigo + " AND DEP_CODIGO = " + depCodigo + " AND CLAS_CODIGO = " + clasCodigo + " AND SUB_CODIGO = " + subCodigo;
            dt = BancoDados.selecionarRegistros(strSql + where);
            if (dt.Rows.Count == 0)
            {
                where = " AND PROD_ID = " + prodCodigo + " AND DEP_CODIGO = 0 AND CLAS_CODIGO = 0 AND SUB_CODIGO = 0";
                dt = BancoDados.selecionarRegistros(strSql + where);
                if (dt.Rows.Count == 0)
                {
                    where = " AND PROD_ID = 0 AND DEP_CODIGO = " + depCodigo + " AND CLAS_CODIGO = 0 AND SUB_CODIGO = 0";
                    dt = BancoDados.selecionarRegistros(strSql + where);
                    if (dt.Rows.Count == 0)
                    {
                        where = " AND PROD_ID = 0 AND DEP_CODIGO = " + depCodigo + " AND CLAS_CODIGO = " + clasCodigo + " AND SUB_CODIGO = 0";
                        if (dt.Rows.Count == 0)
                        {
                            where = " AND PROD_ID = 0 AND DEP_CODIGO = " + depCodigo + " AND CLAS_CODIGO = " + clasCodigo + " AND SUB_CODIGO = " + subCodigo;
                            dt = BancoDados.selecionarRegistros(strSql + where);
                            if (dt.Rows.Count == 0)
                            {
                                return dt;
                            }
                            else
                                return dt;
                        }
                        else
                            return dt;
                    }
                    else
                        return dt;
                }
                else
                    return dt;
            }
            else
                return dt;
        }

        /// <summary>
        /// PRENCHE ESPACO EM BRANCO NA STRING (DIREITA)
        /// </summary>
        /// <param name="texto">String para formatar</param>
        /// <param name="tamannho">Numero de Espaços em Branco</param>
        /// <returns>String Formatada</returns>
        public static string PrencherEspacoEmBranco(string texto, int tamannho)
        {
            while (texto.Length < tamannho)
            {
                texto += " ";
            }

            return texto;
        }

        /// <summary>
        /// VERIFICA SE TEXTO POSSUI LETRA
        /// </summary>
        /// <param name="texto">String</param>
        /// <returns>Verdadeiro, caso contém letras</returns>
        public static bool IdentificaSeContemSomenteLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// VERIFICA SE TEXTO SÓ POSSUI NÚMEROS
        /// </summary>
        /// <param name="texto">String</param>
        /// <returns>Verdadeiro, caso contenha somente números</returns>
        public static bool IdentificaSeContemSomenteNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// BLOQUEIA CASO DIGITE LETRA
        /// </summary>
        /// <param name="e">Tecla prescionada</param>
        /// <returns>Verdadeiro, caso tecla seja de número</returns>
        public static bool SomenteNumeros(KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 44)
            {
                e.Handled = true;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// REMOVE CARACTERS DA STRING
        /// </summary>
        /// <param name="texto">Texto</param>
        /// <returns>String Formatada</returns>
        public static string RemoveCaracter(string texto)
        {
            string mascara;

            mascara = texto.Replace("(", "");
            mascara = mascara.Replace(")", "");
            mascara = mascara.Replace("-", "");
            mascara = mascara.Replace("/", "");
            mascara = mascara.Replace(".", "");
            mascara = mascara.Replace("'", "");
            mascara = mascara.Replace("%", "");
            mascara = mascara.Replace("&", "");
            mascara = mascara.Replace("*", "");
            mascara = mascara.Replace("$", "");
            mascara = mascara.Replace("#", "");
            mascara = mascara.Replace(",", "");
            return mascara.TrimEnd().TrimStart().Trim();

        }

        /// <summary>
        /// OBTEM O NUMERO DA SEQUENCIA
        /// </summary>
        /// <param name="nomeSeq">Nome da Sequence</param>
        /// <returns>Proxima Sequence</returns>
        public static string GeraSequence(string nomeSeq)
        {
            Principal.strSql = "SELECT NEXT VALUE FOR " + nomeSeq;
            return Convert.ToString(BancoDados.ExecuteScalar(Principal.strSql, null));
        }

        /// <summary>
        /// VALIDA EMAIL DIGITADO
        /// </summary>
        /// <param name="email">String</param>
        /// <returns></returns>
        public static bool ValidaEmail(string email)
        {
            bool validEmail = false;
            int indexArr = email.IndexOf('@');
            if (indexArr > 0)
            {
                int indexDot = email.IndexOf('.', indexArr);
                if (indexDot - 1 > indexArr)
                {
                    if (indexDot + 1 < email.Length)
                    {
                        string indexDot2 = email.Substring(indexDot + 1, 1);
                        if (indexDot2 != ".")
                        {
                            validEmail = true;
                        }
                    }
                }
            }
            return validEmail;
        }

        public static string RemoverAcentuacao(this string texto)
        {
            return new string(texto
                .Normalize(NormalizationForm.FormD)
                .Where(ch => char.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark)
                .ToArray());
        }

        public static string RemoverAspas(this string texto)
        {
            return texto.Replace("'", "");
        }


        public static string FormataTamanhoString(string texto, int tamanho)
        {
            if (texto.Length > tamanho)
                return texto.Substring(0, tamanho);
            else
                return texto;
        }

        public static string ChecaParamentos(int idTabela, string posicao, string estacao = "")
        {

            string sql = "SELECT PAR_DESCRICAO FROM PARAMETROS WHERE PAR_TABELA = " + idTabela + "   AND PAR_POSICAO = '" + posicao + "' ";

            if (!String.IsNullOrEmpty(estacao))
            {

                sql += "AND PAR_ESTACAO  = '" + estacao + "'";
            }

            return (string)BancoDados.ExecuteScalar(sql, null);

        }

        public static string ObtemUF(string codigoUF)
        {
            string retorno = String.Empty;
            switch (codigoUF)
            {
                case "11":
                    retorno = "RO";
                    break;
                case "12":
                    retorno = "AC";
                    break;
                case "13":
                    retorno = "AM";
                    break;
                case "14":
                    retorno = "RR";
                    break;
                case "15":
                    retorno = "PA";
                    break;
                case "16":
                    retorno = "AP";
                    break;
                case "17":
                    retorno = "TO";
                    break;
                case "21":
                    retorno = "MA";
                    break;
                case "22":
                    retorno = "PI";
                    break;
                case "23":
                    retorno = "CE";
                    break;
                case "24":
                    retorno = "RN";
                    break;
                case "25":
                    retorno = "PB";
                    break;
                case "26":
                    retorno = "PE";
                    break;
                case "27":
                    retorno = "AL";
                    break;
                case "28":
                    retorno = "SE";
                    break;
                case "29":
                    retorno = "BA";
                    break;
                case "31":
                    retorno = "MG";
                    break;
                case "32":
                    retorno = "ES";
                    break;
                case "33":
                    retorno = "RJ";
                    break;
                case "35":
                    retorno = "SP";
                    break;
                case "41":
                    retorno = "PR";
                    break;
                case "42":
                    retorno = "SC";
                    break;
                case "43":
                    retorno = "RS";
                    break;
                case "50":
                    retorno = "MS";
                    break;
                case "51":
                    retorno = "MT";
                    break;
                case "52":
                    retorno = "GO";
                    break;
                case "53":
                    retorno = "DF";
                    break;
            }
            return retorno;
        }

        public static string ObtemEstado(string uf)
        {
            string retorno = String.Empty;
            switch (uf)
            {
                case "AC":
                    retorno = "ACRE";
                    break;
                case "AL":
                    retorno = "ALAGOAS";
                    break;
                case "AP":
                    retorno = "AMAPÁ";
                    break;
                case "AM":
                    retorno = "AMAZONAS";
                    break;
                case "BA":
                    retorno = "BAHIA";
                    break;
                case "CE":
                    retorno = "CEARÁ";
                    break;
                case "DF":
                    retorno = "DISTRITO FEDERAL";
                    break;
                case "ES":
                    retorno = "ESPÍRITO SANTO";
                    break;
                case "GO":
                    retorno = "GOIÁS";
                    break;
                case "MA":
                    retorno = "MARANHÃO";
                    break;
                case "MT":
                    retorno = "MATO GROSSO";
                    break;
                case "MS":
                    retorno = "MATO GROSSO DO SUL";
                    break;
                case "MG":
                    retorno = "MINAS GERAIS";
                    break;
                case "PA":
                    retorno = "PARÁ";
                    break;
                case "PB":
                    retorno = "PARAÍBA";
                    break;
                case "PR":
                    retorno = "PARANÁ";
                    break;
                case "PE":
                    retorno = "PERNAMBUCO";
                    break;
                case "PI":
                    retorno = "PIUAÍ";
                    break;
                case "RJ":
                    retorno = "RIO DE JANEIRO";
                    break;
                case "RN":
                    retorno = "RIO GRANDE DO NORTE";
                    break;
                case "RS":
                    retorno = "RIO GRANDE DO SUL";
                    break;
                case "RO":
                    retorno = "RONDÔNIA";
                    break;
                case "RR":
                    retorno = "RORAIMA";
                    break;
                case "SC":
                    retorno = "SANTA CATARINA";
                    break;
                case "SP":
                    retorno = "SÃO PAULO";
                    break;
                case "SE":
                    retorno = "SERGIPE";
                    break;
                case "TO":
                    retorno = "TOCANTINS";
                    break;
            }
            return retorno;
        }
    }
}
