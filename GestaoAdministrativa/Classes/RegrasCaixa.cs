﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestaoAdministrativa.Classes
{
    public static class RegrasCaixa
    {
        /// <summary>
        /// METODO QUE CALCULA JUROS DE PAGAMENTOS EM ATRASO
        /// </summary>
        /// <param name="vencto">DIA DO VENCIMENTO</param>
        /// <param name="data">DATA DO DIA</param>
        /// <param name="saldo">SALDO DA PARCELA EM ABERTO</param>
        /// <returns></returns>
        public static double CalculaJuros(string vencto, string data, double saldo)
        {
            try
            {
                int carencia;
                int dias;
                double valor;
                double juros;

                valor = saldo;
                if (Convert.ToDateTime(vencto).CompareTo(Convert.ToDateTime(data)) == -1)
                {
                    carencia = Convert.ToInt32(Funcoes.LeParametro(4, "51", true));
                    dias = Convert.ToInt32(Convert.ToDateTime(data).Subtract(Convert.ToDateTime(vencto).AddDays(carencia)).Days);

                    if (dias > 0)
                    {
                        juros = Convert.ToDouble(Funcoes.LeParametro(4, "52", true));
                        juros = juros / 100;
                        if (Funcoes.LeParametro(4, "53", true).ToUpper() == "S")
                        {
                            valor = Math.Round(saldo + (juros * saldo * dias), 2);
                        }
                        else if (Funcoes.LeParametro(4, "53", true).ToUpper() == "C")
                        {
                            valor = Math.Round(saldo * Math.Pow((juros + 1), dias), 2);
                        }

                        return valor;
                    }
                    else
                    {
                        return valor;
                    }
                }
                return valor;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Regras Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        /// <summary>
        /// IMPRIME COMPROVANTE DE PARCELAS PAGAR NO PERIODO ESCOLHIDO
        /// </summary>
        /// <param name="impressora">NOME DA IMPRESSOA</param>
        /// <param name="dtInicial">DATA INICIAL</param>
        /// <param name="dtFinal">DATA FINAL</param>
        /// <param name="clienteID">ID DO CLIENTE</param>
        /// <returns></returns>
        public static bool ImpCompPer(string impressora, DateTime dtInicial, DateTime dtFinal, int clienteID)
        {
            try
            {
                string linha;
                string strSql;
                double jurosDes;
                double credito;
                double jurosTotal = 0;
                double total = 0;
                if (!String.IsNullOrEmpty(impressora))
                {
                    Principal.impressora = impressora;

                    linha = "";
                    linha = "========================================" + "\n" + "\r";
                    linha += Funcoes.CentralizaTexto(Principal.nomeAtual, 40) + "\n" + "\r";
                    linha += "========================================" + "\n" + "\r";
                    linha += Funcoes.CentralizaTexto("RECIBO DE PAGAMENTO", 40) + "\n" + "\r";
                    linha += "========================================" + "\n" + "\r";
                    linha += Funcoes.CentralizaTexto("PERIODO ENTRE " + dtInicial.ToString("dd/MM/yyyy") + " E " + dtFinal.ToString("dd/MM/yyyy"), 40) + "\n" + "\r";
                    linha += "CLIENTE" + "\n" + "\r";
                    linha += " - " + BancoDados.selecionarRegistro("SELECT CLI_NOME FROM CLIENTES WHERE CLI_ID = " + clienteID, "CLI_NOME") + "\n" + "\r";
                    linha += "========================================" + "\n" + "\r";
                    linha += "PARCELAS PAGAS" + "\n" + "\r";
                    linha += "TITULOS   VENCTO     PAGTO       PARCELA" + "\n" + "\r";
                    strSql = "SELECT A.EST_CODIGO, A.REC_CODIGO, A.RD_PARCELA, A.RD_VALOR, A.RD_VENCTO, A.RD_PAGO, A.RD_QUITACAO, B.RM_DATA, B.LOGIN_ID, B.MOVFIN_SEQ, C.REC_DOCTO"
                        + " FROM RECEBER_DETALHE A, RECEBER_MOVTO B, RECEBER C  WHERE A.EST_CODIGO = " + Principal.estAtual + " AND A.RD_QUITACAO BETWEEN " + Funcoes.BData(dtInicial)
                        + " AND " + Funcoes.BData(dtFinal) + " AND A.RD_PAGO = 'S' AND A.EST_CODIGO = B.EST_CODIGO AND A.REC_CODIGO = B.REC_CODIGO AND C.CLI_ID = " + clienteID;
                    strSql += " AND A.EST_CODIGO = C.EST_CODIGO AND A.REC_CODIGO = C.REC_CODIGO GROUP BY A.EST_CODIGO, A.REC_CODIGO, A.RD_PARCELA, A.RD_VALOR, A.RD_VENCTO, A.RD_PAGO,";
                    strSql += " A.RD_QUITACAO, B.RM_DATA, B.LOGIN_ID, B.MOVFIN_SEQ, C.REC_DOCTO";
                    Principal.dtRegistros = BancoDados.selecionarRegistros(strSql);
                    if (Principal.dtRegistros.Rows.Count != 0)
                    {
                        for (int i = 0; i < Principal.dtRegistros.Rows.Count; i++)
                        {
                            strSql = "SELECT * FROM RECEBER_MOVTO WHERE EST_CODIGO = " + Principal.dtRegistros.Rows[i]["EST_CODIGO"] + " AND REC_CODIGO = " + Principal.dtRegistros.Rows[i]["REC_CODIGO"]
                                + " AND RD_PARCELA = " + Principal.dtRegistros.Rows[i]["RD_PARCELA"] + " AND RM_DATA = " + Funcoes.BData(Convert.ToDateTime(Principal.dtRegistros.Rows[i]["RM_DATA"]))
                                + " AND LOGIN_ID = " + Principal.dtRegistros.Rows[i]["LOGIN_ID"] + " AND MOVFIN_SEQ = " + Principal.dtRegistros.Rows[i]["MOVFIN_SEQ"];
                            Principal.dtBusca = BancoDados.selecionarRegistros(strSql);
                            jurosDes = 0;
                            credito = 0;
                            if (Principal.dtBusca.Rows.Count != 0)
                            {
                                for (int x = 0; x < Principal.dtBusca.Rows.Count; x++)
                                {
                                    if (Convert.ToInt32(Principal.dtBusca.Rows[x]["RM_TIPO_PAGTO"]) == 0)
                                    {
                                        jurosDes = jurosDes + Convert.ToDouble(Principal.dtBusca.Rows[x]["RM_VALOR"]);
                                    }
                                    else if (Convert.ToInt32(Principal.dtBusca.Rows[x]["RM_TIPO_PAGTO"]) == 1)
                                    {
                                        jurosDes = jurosDes + Convert.ToDouble(Principal.dtBusca.Rows[x]["RM_VALOR"]);
                                        jurosTotal = jurosTotal + jurosDes;
                                    }
                                    else
                                    {
                                        credito = credito + Convert.ToDouble(Principal.dtBusca.Rows[x]["RM_VALOR"]);
                                    }
                                }

                                linha += Principal.dtRegistros.Rows[i]["REC_DOCTO"].ToString().PadRight(10) + Principal.dtRegistros.Rows[i]["RD_VENCTO"].ToString().Substring(0, 10).PadRight(11)
                                 + Principal.dtRegistros.Rows[i]["RM_DATA"].ToString().Substring(0, 10).PadLeft(10) + String.Format("{0:N}", (credito - jurosDes)).PadLeft(9) + "\n" + "\r";

                                total = total + credito;
                            }
                        }

                        linha += "\n" + "\r";
                        linha += "JUROS: " + String.Format("{0:N}", jurosTotal) + "\n" + "\r";
                        linha += ("TOTAL DO RECEBIMENTO: " + String.Format("{0:N}", total)).PadLeft(40) + "\n" + "\r";
                        linha += "========================================" + "\n" + "\r";
                        linha += Funcoes.CentralizaTexto("OBRIGADO PELA PREFERENCIA!", 40) + "\n" + "\r";
                        linha += "========================================" + "\n" + "\r";

                        Imprimir.Comprovante(impressora, linha);
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Nenhuma parcela quitada no periódo selecionado.", "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return true;
                    }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Regras Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
