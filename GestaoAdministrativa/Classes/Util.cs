﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Classes;
using System.Data;
using SqlNegocio;
using System.Windows.Forms;
using System.IO;
using System.Net.NetworkInformation;

namespace GestaoAdministrativa.Classes
{
    public static class Util
    {
        public static string MySqlConexao()
        {
            return "DRIVER={MySQL ODBC 5.1 Driver};user=root;password=allebagord;server="  + Principal.endConexao +  ";database=controle";
        }

        public static string SelecionaCampoEspecificoDaTabela(string tabela, string campo, string condicao = "", string descr = "", bool estCodigo = false, bool campoTexto = false, bool empresa = false)


        {
            string strSql = "SELECT " + campo + " FROM " + tabela;
            if (!String.IsNullOrEmpty(condicao))
            {
                strSql += " WHERE ";
                if (estCodigo)
                {
                    strSql += " EST_CODIGO = " + Principal.estAtual + " AND ";
                }
                if (empresa)
                {
                    strSql += " EMP_CODIGO = " + Principal.empAtual + " AND ";
                }
                if (campoTexto)
                {
                    strSql += condicao + " = '" + descr + "'";
                }
                else
                    strSql += condicao + " = " + descr;
            }
            return BancoDados.selecionarRegistro(strSql, campo);
        }

        public static DataTable SelecionaRegistrosTodosOuEspecifico(string tabela, string campo = "", string condicao = "", string descr = "", bool codigo = false, string ordem = "")
        {
            string strSql = "SELECT ";
            if (!String.IsNullOrEmpty(campo))
            {
                strSql += campo;
            }
            else
                strSql += " * ";

            strSql += " FROM " + tabela;
            if (!String.IsNullOrEmpty(condicao))
            {
                strSql += " WHERE ";
                if (codigo.Equals(true))
                    strSql += " EST_CODIGO = " + Principal.estAtual + " AND ";
                strSql += condicao + " = " + descr;
            }
            if (!String.IsNullOrEmpty(ordem))
            {
                strSql += " ORDER BY " + ordem;
            }
            return BancoDados.selecionarRegistros(strSql);
        }

        /// <summary>
        /// BUSCA OS DADOS PARA PREENCHER O COMBOBOX
        /// </summary>
        /// <param name="campo1">Campo da Tabela</param>
        /// <param name="campo2">Campo da Tabela</param>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="estCodigo">Cód. do Estabelecimento, se tabela tiver</param>
        /// <param name="condicao">Condição de Filtro, se desejar</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable CarregarCombosPorEstabelecimento(string campo1, string campo2, string tabela, bool estCodigo = false, string condicao = "")
        {
            string strSql = "SELECT " + campo1 + "," + campo2 + " FROM " + tabela;
            if (estCodigo.Equals(true))
                strSql += " WHERE EST_CODIGO = " + Principal.estAtual;
            if (!String.IsNullOrEmpty(condicao))
            {
                if (estCodigo.Equals(true))
                {
                    strSql += " AND " + condicao;
                }
                else
                    strSql += " WHERE " + condicao;
            }

            strSql += " ORDER BY " + campo2;

            return BancoDados.selecionarRegistros(strSql);
        }

        /// <summary>
        /// BUSCA OS DADOS PARA PREENCHER O COMBOBOX
        /// </summary>
        /// <param name="campo1">Campo da Tabela</param>
        /// <param name="campo2">Campo da Tabela</param>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="empCodigo">Cód. da Empresa, se tabela tiver</param>
        /// <param name="condicao">Condição de Filtro, se desejar</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable CarregarCombosPorEmpresa(string campo1, string campo2, string tabela, bool empCodigo = false, string condicao = "")
        {
            string strSql = "SELECT " + campo1 + "," + campo2 + " FROM " + tabela;
            if (empCodigo.Equals(true))
                strSql += " WHERE EMP_CODIGO = " + Principal.estAtual;
            if (!String.IsNullOrEmpty(condicao))
            {
                if (empCodigo.Equals(true))
                {
                    strSql += " AND " + condicao;
                }
                else
                    strSql += " WHERE " + condicao;
            }

            strSql += " ORDER BY " + campo2;

            return BancoDados.selecionarRegistros(strSql);
        }

        /// <summary>
        /// SELECIONA TODOS OS REGISTROS DA TABELA
        /// </summary>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="campo">Campo de Filtro</param>
        /// <param name="descr">Descrição do Filtro</param>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable RegistrosPorEstabelecimento(string tabela, string campo, string descr, bool estCodigo = false, bool campoString = false)
        {
            string strSql = "SELECT * FROM " + tabela + " WHERE ";
            if (estCodigo.Equals(true))
            {
                strSql += "EST_CODIGO = " + Principal.estAtual + " AND ";
            }
            strSql += campo + " = '" + descr + "'";

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// SELECIONA TODOS OS REGISTROS DA TABELA
        /// </summary>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="campo">Campo de Filtro</param>
        /// <param name="descr">Descrição do Filtro</param>
        /// <param name="empCodigo">Cód. da Empresa</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable RegistrosPorEmpresa(string tabela, string campo, string descr, bool empCodigo = false, bool campoString = false)
        {
            string strSql = "SELECT * FROM " + tabela + " WHERE ";
            if (empCodigo.Equals(true))
            {
                strSql += "EMP_CODIGO = " + Principal.empAtual + " AND ";
            }
            if (campoString.Equals(true))
            {
                strSql += campo + " = '" + descr + "'";
            }
            else
                strSql += campo + " = " + descr;

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// BUSCA DE REGISTROS CONFORME FILTRO DA TELA DE FILTRO DE PRODUTOS
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="tabela">Nome da Tabela</param>
        /// <param name="campo1">Campo para busca</param>
        /// <param name="campo2">Campo para busca</param>
        /// <param name="campo3">Campo de condição liberado</param>
        /// <param name="dg">Nome do DataGrid que ira exibir a busca</param>
        /// <param name="txtB">Condição de Filtro</param>
        /// <returns></returns>
        public static void FiltrarDados(int estCodigo, string tabela, string campo1, string campo2, string campo3, DataGridView dg, TextBox txtB)
        {
            string strSql = "SELECT " + campo1 + "," + campo2 + " FROM " + tabela + " WHERE EST_CODIGO = " + estCodigo
                   + " AND " + campo3 + " = 'S' AND " + campo2 + " LIKE '%" + txtB.Text.Trim().ToUpper() + "%' ORDER BY " + campo2;

            dg.DataSource = BancoDados.GetDataTable(strSql, null);
        }

        public static String GerarCpf()
        {
            int soma = 0, resto = 0;
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            Random rnd = new Random();
            string semente = rnd.Next(100000000, 999999999).ToString();

            for (int i = 0; i < 9; i++)
                soma += int.Parse(semente[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            semente = semente + resto;
            soma = 0;

            for (int i = 0; i < 10; i++)
                soma += int.Parse(semente[i].ToString()) * multiplicador2[i];

            resto = soma % 11;

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            semente = semente + resto;
            return semente;
        }

        public static void CriarArquivoTXT(string caminho, string texto)
        {
            using (StreamWriter escrever = new StreamWriter(caminho))
            {
                escrever.Write(texto);
            }
        }

        public static string LerArquivoTXT(string caminho)
        {
            using (StreamReader sr = new StreamReader(caminho))
            {
                String linha = sr.ReadToEnd();
                sr.Close();
                return linha;
            }
        }

        #region Escrever Decimal por Extenso
        // O método toExtenso recebe um valor do tipo decimal
        public static string toExtenso(decimal valor)
        {
            if (valor <= 0 | valor >= 1000000000000000)
                return "Valor não suportado pelo sistema.";
            else
            {
                string strValor = valor.ToString("000000000000000.00");
                string valor_por_extenso = string.Empty;

                for (int i = 0; i <= 15; i += 3)
                {
                    valor_por_extenso += escreva_parte(Convert.ToDecimal(strValor.Substring(i, 3)));
                    if (i == 0 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(0, 3)) == 1)
                            valor_por_extenso += " TRILHÃO" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(0, 3)) > 1)
                            valor_por_extenso += " TRILHÕES" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 3 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(3, 3)) == 1)
                            valor_por_extenso += " BILHÃO" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(3, 3)) > 1)
                            valor_por_extenso += " BILHÕES" + ((Convert.ToDecimal(strValor.Substring(6, 9)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 6 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(6, 3)) == 1)
                            valor_por_extenso += " MILHÃO" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(6, 3)) > 1)
                            valor_por_extenso += " MILHÕES" + ((Convert.ToDecimal(strValor.Substring(9, 6)) > 0) ? " E " : string.Empty);
                    }
                    else if (i == 9 & valor_por_extenso != string.Empty)
                        if (Convert.ToInt32(strValor.Substring(9, 3)) > 0)
                            valor_por_extenso += " MIL" + ((Convert.ToDecimal(strValor.Substring(12, 3)) > 0) ? " E " : string.Empty);

                    if (i == 12)
                    {
                        if (valor_por_extenso.Length > 8)
                            if (valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "BILHÃO" | valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "MILHÃO")
                                valor_por_extenso += " DE";
                            else
                                if (valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "BILHÕES" | valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "MILHÕES" | valor_por_extenso.Substring(valor_por_extenso.Length - 8, 7) == "TRILHÕES")
                                valor_por_extenso += " DE";
                            else
                                    if (valor_por_extenso.Substring(valor_por_extenso.Length - 8, 8) == "TRILHÕES")
                                valor_por_extenso += " DE";

                        if (Convert.ToInt64(strValor.Substring(0, 15)) == 1)
                            valor_por_extenso += " REAL";
                        else if (Convert.ToInt64(strValor.Substring(0, 15)) > 1)
                            valor_por_extenso += " REAIS";

                        if (Convert.ToInt32(strValor.Substring(16, 2)) > 0 && valor_por_extenso != string.Empty)
                            valor_por_extenso += " E ";
                    }

                    if (i == 15)
                        if (Convert.ToInt32(strValor.Substring(16, 2)) == 1)
                            valor_por_extenso += " CENTAVO";
                        else if (Convert.ToInt32(strValor.Substring(16, 2)) > 1)
                            valor_por_extenso += " CENTAVOS";
                }
                return valor_por_extenso;
            }
        }

        static string escreva_parte(decimal valor)
        {
            if (valor <= 0)
                return string.Empty;
            else
            {
                string montagem = string.Empty;
                if (valor > 0 & valor < 1)
                {
                    valor *= 100;
                }
                string strValor = valor.ToString("000");
                int a = Convert.ToInt32(strValor.Substring(0, 1));
                int b = Convert.ToInt32(strValor.Substring(1, 1));
                int c = Convert.ToInt32(strValor.Substring(2, 1));

                if (a == 1) montagem += (b + c == 0) ? "CEM" : "CENTO";
                else if (a == 2) montagem += "DUZENTOS";
                else if (a == 3) montagem += "TREZENTOS";
                else if (a == 4) montagem += "QUATROCENTOS";
                else if (a == 5) montagem += "QUINHENTOS";
                else if (a == 6) montagem += "SEISCENTOS";
                else if (a == 7) montagem += "SETECENTOS";
                else if (a == 8) montagem += "OITOCENTOS";
                else if (a == 9) montagem += "NOVECENTOS";

                if (b == 1)
                {
                    if (c == 0) montagem += ((a > 0) ? " E " : string.Empty) + "DEZ";
                    else if (c == 1) montagem += ((a > 0) ? " E " : string.Empty) + "ONZE";
                    else if (c == 2) montagem += ((a > 0) ? " E " : string.Empty) + "DOZE";
                    else if (c == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TREZE";
                    else if (c == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUATORZE";
                    else if (c == 5) montagem += ((a > 0) ? " E " : string.Empty) + "QUINZE";
                    else if (c == 6) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSEIS";
                    else if (c == 7) montagem += ((a > 0) ? " E " : string.Empty) + "DEZESSETE";
                    else if (c == 8) montagem += ((a > 0) ? " E " : string.Empty) + "DEZOITO";
                    else if (c == 9) montagem += ((a > 0) ? " E " : string.Empty) + "DEZENOVE";
                }
                else if (b == 2) montagem += ((a > 0) ? " E " : string.Empty) + "VINTE";
                else if (b == 3) montagem += ((a > 0) ? " E " : string.Empty) + "TRINTA";
                else if (b == 4) montagem += ((a > 0) ? " E " : string.Empty) + "QUARENTA";
                else if (b == 5) montagem += ((a > 0) ? " E " : string.Empty) + "CINQUENTA";
                else if (b == 6) montagem += ((a > 0) ? " E " : string.Empty) + "SESSENTA";
                else if (b == 7) montagem += ((a > 0) ? " E " : string.Empty) + "SETENTA";
                else if (b == 8) montagem += ((a > 0) ? " E " : string.Empty) + "OITENTA";
                else if (b == 9) montagem += ((a > 0) ? " E " : string.Empty) + "NOVENTA";

                if (strValor.Substring(1, 1) != "1" & c != 0 & montagem != string.Empty) montagem += " E ";

                if (strValor.Substring(1, 1) != "1")
                    if (c == 1) montagem += "UM";
                    else if (c == 2) montagem += "DOIS";
                    else if (c == 3) montagem += "TRÊS";
                    else if (c == 4) montagem += "QUATRO";
                    else if (c == 5) montagem += "CINCO";
                    else if (c == 6) montagem += "SEIS";
                    else if (c == 7) montagem += "SETE";
                    else if (c == 8) montagem += "OITO";
                    else if (c == 9) montagem += "NOVE";

                return montagem;
            }
        }
        #endregion


        public static bool TesteIPServidorNuvem()
        {
            var ping = new Ping();
            var reply = ping.Send("200.155.3.84");
            if (reply.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void BotoesGenericos()
        {
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.btnLimpar.Enabled = true;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        public static DateTime PrimeiroDiadoMes(DateTime Data)
        {
            DateTime PrimeiroDiadoMes = DateTime.Parse("01" +Data.AddMonths(-1).ToString("/MM/yyyy"));

            return PrimeiroDiadoMes;

        }

        public static DateTime UltimoDiadoMes(DateTime Data)
        {
            DateTime PrimeiroDiadoMes = DateTime.Parse("01" + Data.AddMonths(-1).ToString("/MM/yyyy"));

            DateTime PrimeiroDiadoProximoMes = PrimeiroDiadoMes.AddMonths(1);

            DateTime UltimoDiadoMes = PrimeiroDiadoProximoMes.AddDays(-1);

            return UltimoDiadoMes;
        }
    }
}
