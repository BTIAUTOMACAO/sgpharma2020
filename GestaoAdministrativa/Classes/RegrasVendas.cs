﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestaoAdministrativa.Classes
{
    public static class RegrasVendas
    {
        public static void CarregaComanda()
        {
        }

        public static void GravaEmpConveniada(string codConv, string empNome)
        {
            try
            {
                Principal.strSql = "SELECT CON_ID, CON_CODIGO, CON_NOME, CON_WEB FROM CONVENIADAS WHERE CON_CODIGO = " + codConv;
                Principal.dtBusca = BancoDados.selecionarRegistros(Principal.strSql);
                if (Principal.dtBusca.Rows.Count > 0)
                {
                    Principal.wsConWeb = Principal.dtBusca.Rows[0]["CON_WEB"].ToString();
                    Principal.wsConID = (int)Principal.dtBusca.Rows[0]["CON_ID"];
                }
                else
                {
                    Principal.strCmd = "INSERT INTO CONVENIADAS (CON_CODIGO, CON_NOME, CON_WEB, CON_LIBERADO, CON_REGRAS) VALUES("
                        + Principal.wsEmpresa + ",'" + empNome.ToUpper() + "',1,'S','" + Principal.wsRegras + "')";
                    BancoDados.executarSql(Principal.strCmd);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grava Emp. Conv.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
