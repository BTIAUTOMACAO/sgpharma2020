﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// Interface para controle de impressora
    /// </summary>
    interface IModeloImpressora
    {
        bool ImpFiscalUltCupom();

        bool ImpFiscalReducaoZ();

        bool ImpFiscalLeituraX();

        bool ImpFiscalEstado();

        bool ImpFiscalAbreCupom(string nDocto = "", string nome = "", string endereco = "");

        bool ImpFiscalVenderItem(List<VendaItem> Itens);

        bool ImpFiscalTotalizarCupom(string tipo, string desconto);

        bool ImpFiscalMeioPagamento(List<VendasEspecies> VendaEspecies);

        bool ImpFiscalFechaCupom();

        bool ImpFiscalFechaCupomMsg(string msgPromocional);

        bool ImpFiscalAbrirVinculado(string formaPgto, string parcelas, string docOrigem, string valor);

        bool ImpFiscalFechaVinculado();

        string ImpFiscalInformacoesECF(string indice, string informacao);

        string ImpFiscalNumeroCupom();

        bool ImpFiscalTextoVinculado(string mensagem);

        bool ImpFiscalAbreGerencial(string mensagem);

        bool ImpFiscalFechaGerencial();

    }
}
