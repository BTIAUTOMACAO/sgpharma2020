﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// INTERFACE PARA CONTROLE DO BANCO DE DADOS
    /// </summary>
    public interface IConexao
    {
        bool conectar(string paramentroConexao);

        bool fecharConexao();

        bool executarSql(string comandoSql);

        DataTable selecionarRegistros(string comandoSql);

        string selecionarRegistro(string comandoSql, string campo);

        bool executarComando(string[] comando, int indice);

        int ExecuteNoQuery(String pComando, SqlParamsList ParamsList);

        void AbrirTrans();

        void FecharTrans();

        void ErroTrans();

        int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList);

        DataTable GetDataTable(String pComando, SqlParamsList ParamsList);

        object ExecuteScalar(String pComando, SqlParamsList ParamsList);
    }
}
