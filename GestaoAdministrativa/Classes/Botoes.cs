﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// ASSINTATURA DOS METODOS PRESENTES NOS FORMULARIOS PARA O CONTROLE DE BOTÕES DO MENU
    /// </summary>
    public interface Botoes
    {
        /// <summary>
        /// BOTÃO DE NAVEGAÇÃO QUE RETORNA PARA O PRIMEIRO REGISTRO
        /// </summary>
        void Primeiro();

        /// <summary>
        /// BOTÃO DE NAVEGAÇÃO QUE RETORNA PARA O ULTIMO REGISTRO
        /// </summary>
        void Ultimo();

        /// <summary>
        /// BOTÃO DE NAVEGAÇÃO QUE RETORNA PARA O PRÓXIMO REGISTRO
        /// </summary>
        void Proximo();

        /// <summary>
        /// BOTÃO DE NAVEGAÇÃO QUE RETORNA PARA O REGISTRO ANTERIOR
        /// </summary>
        void Anterior();

        /// <summary>
        /// BOTÃO QUE ATUALIZA OU INSERE O REGISTRO NA TABELA DE ACORDO COM O FORMULARIO
        /// </summary>
        /// <returns></returns>
        bool Atualiza();

        /// <summary>
        /// BOTAO QUE GERA O ID REFERENTE A TABELA DO FORMULARIO PARA INSERIR NO BANCO DE DADOS
        /// </summary>
        /// <returns></returns>
        bool Incluir();

        /// <summary>
        /// BOTÃO QUE EXCLUI O REGISTRO DE ACORDO COM A TABELA DO FORMULARIO
        /// </summary>
        /// <returns></returns>
        bool Excluir();

        /// <summary>
        /// BOTÃO QUE LIMPA OS CAMPOS DO FORMULARIO
        /// </summary>
        void Limpar();

        /// <summary>
        /// BOTÃO QUE REMOVE O BOTÃO CRIADO NO MENU INFERIOR DO FORM PRINCIPAL REFERENTE AO FORMULARIO FECHADO
        /// </summary>
        void Sair();

        void ImprimirRelatorio();

        /// <summary>
        /// BOTÃO PARA TECLA DE ATALHO PARA TAB GRADE
        /// </summary>
        void AtalhoGrade();

        /// <summary>
        /// BOTÃO PARA TECLA DE ATALHO PARA TAB FICHA
        /// </summary>
        void AtalhoFicha();

        /// <summary>
        /// BOTÃO PARA TECLA DE ATALHO PARA AJUDA
        /// </summary>
        void Ajuda();

        void FormularioFoco();
    }
}
