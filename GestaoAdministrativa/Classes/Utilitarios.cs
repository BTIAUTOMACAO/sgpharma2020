﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes
{
    public class Utilitarios
    {
        public bool AtualizaCodigoBarras(string nomeTabela, string codigoBarrasNovos, string codigoBarrasAntigo)
        {
            string sql = " UPDATE " + nomeTabela + " SET PROD_CODIGO = '" + codigoBarrasNovos + "'"
                       + " WHERE PROD_CODIGO = '" + codigoBarrasAntigo + "' ";

            int retorno = BancoDados.ExecuteNoQueryTrans(sql, null);
            if (retorno >= 0)
            {
                if (retorno >= 1)
                {
                    string[,] dados = new string[1, 3];
                    {
                        dados[0, 0] = nomeTabela;
                        dados[0, 1] = codigoBarrasAntigo;
                        dados[0, 2] = codigoBarrasNovos;

                        Funcoes.GravaLogAlteracao("PROD_CODIGO", codigoBarrasAntigo, Principal.usuario, nomeTabela, dados, 1, Principal.estAtual, Principal.empAtual, true);
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
