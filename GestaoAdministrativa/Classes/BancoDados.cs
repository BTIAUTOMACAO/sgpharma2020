﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Data;
using System.Windows.Forms;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    public class BancoDados
    {
        private static IConexao bancoDados;
        private static string arquivoIni;
        private static string nomeBanco;
        public static string strCon;

        /// <summary>
        /// METODO QUE ABRE CONEXAO COM BANCO DE DADOS
        /// </summary>
        /// <returns></returns>
        public static bool conectar()
        {
            return bancoDados.conectar(strCon);
        }

        /// <summary>
        /// METODO QUE FECHA CONEXAO COM BANCO DE DADOS
        /// </summary>
        /// <returns></returns>
        public static bool fecharConexao()
        {
            return bancoDados.fecharConexao();
        }

        /// <summary>
        /// METODO QUE RETORNA A SELEÇÃO DE REGISTROS DE UMA CONSULTA
        /// </summary>
        /// <param name="comandoSql">QUERY</param>
        /// <returns>DATATABLE</returns>
        public static DataTable selecionarRegistros(string comandoSql)
        {
            return bancoDados.selecionarRegistros(comandoSql);
        }

        /// <summary>
        /// METODO QUE EXECUTA A QUERY NO BANCO DE DADOS
        /// </summary>
        /// <param name="comandoSql">QUERY</param>
        /// <returns></returns>
        public static bool executarSql(string comandoSql)
        {
            return bancoDados.executarSql(comandoSql);
        }

        /// <summary>
        /// METODO QUE SELECIONA UM REGISTRO NO BANCO DE DADOS
        /// </summary>
        /// <param name="comandoSql">QUERY</param>
        /// <param name="campo">NOME DO CAMPO</param>
        /// <returns></returns>
        public static string selecionarRegistro(string comandoSql, string campo)
        {
            return bancoDados.selecionarRegistro(comandoSql,campo);
        }

        /// <summary>
        /// METODO QUE EXECUTA VARIOS QUERY'S ONDE CASO OCORRA ERRO EM ALGUMA QUERY DA ROOLBACK NA TRANSACAO
        /// </summary>
        /// <param name="comando">MATRIZ COM AS QUERY'S</param>
        /// <param name="indice">NUMERO DE QUERY'S</param>
        /// <returns></returns>
        public static bool executarComando(string[] comando, int indice)
        {
            return bancoDados.executarComando(comando, indice);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pComando"></param>
        /// <param name="ParamsList"></param>
        /// <returns>1 operação com sucesso</returns>
        public static int ExecuteNoQuery(String pComando, SqlParamsList ParamsList)
        {
            return bancoDados.ExecuteNoQuery(pComando,ParamsList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pComando"></param>
        /// <param name="ParamsList"></param>
        /// <returns></returns>
        public static DataTable GetDataTable(String pComando, SqlParamsList ParamsList)
        {
            return bancoDados.GetDataTable(pComando, ParamsList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pComando"></param>
        /// <param name="ParamsList"></param>
        /// <returns></returns>
        public static object ExecuteScalar(String pComando, SqlParamsList ParamsList)
        {
            return bancoDados.ExecuteScalar(pComando, ParamsList);
        }

        public static void AbrirTrans()
        {
            bancoDados.AbrirTrans();
        }

        public static void FecharTrans()
        {
            bancoDados.FecharTrans();
        }

        public static void ErroTrans()
        {
            bancoDados.ErroTrans();
        }

        public static int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList)
        {
            return bancoDados.ExecuteNoQueryTrans(pComando, ParamsList);
        }

        /// <summary>
        /// METODO QUE VERIFICA QUAL BANCO SERA UTILIZADO, CONFORME ARQUIVO INI
        /// BANCO ORACLE, FIREBIRD E MYSQL
        /// </summary>
        /// <returns></returns>
        public static bool verificarBanco()
        {
            try
            {
                //ARRUMAR CAMINHO DO GADM ESTA FIXO//
                arquivoIni = Application.StartupPath + @"\ga.ini";
                nomeBanco = LeINI("Conexao", "Banco");
                Principal.nomeBanco = nomeBanco;
                strCon = LeINI("Conexao", nomeBanco);
                

                if (nomeBanco == "Oracle")
                {
                    //SENHA//
                    if (LeINI("Conexao", "OutraSenha") == "")
                    {
                        strCon = strCon + "inicial" + LeINI("Conexao", nomeBanco + "1");
                    }
                    else
                        if (LeINI("Conexao", "OutraSenha") == "JIND")
                        {
                            strCon = strCon + "bellajind!" + LeINI("Conexao", nomeBanco + "1");
                        }
                        else
                            if (LeINI("Conexao", "OutraSenha") == "ALTOPONTE")
                            {
                                strCon = strCon + "bellaponte!" + LeINI("Conexao", nomeBanco + "1");
                            }

                    //USUARIO//
                    Principal.usuario = "gadm";
                    strCon = strCon + "gadm" + LeINI("Conexao", nomeBanco + "2");
                    //DATABASE//
                    Principal.nomeServidor = LeINI("Conexao", "Nome");
                    strCon = strCon + LeINI("Conexao", "Nome") + LeINI("Conexao", nomeBanco + "3");
                    bancoDados = new Oracle(strCon);
                }
                else if (nomeBanco == "MySql")
                {
                    bancoDados = new MySql(strCon);
                }
                else if (nomeBanco == "Firebird")
                {
                    //SENHA//
                    strCon = strCon + "bella@mmiv" + LeINI("Conexao", nomeBanco + "1");
                    //USUARIO//
                    strCon = strCon + "gadm" + LeINI("Conexao", nomeBanco + "2");
                    //DATABASE//
                    strCon = strCon + "192.168.0.195:ga" + LeINI("Conexao", nomeBanco + "3");

                    bancoDados = new Firebird(strCon);
                }
                else if (nomeBanco == "SqlServer")
                {
                    //SENHA//
                    strCon = strCon + "1@allebagord" + LeINI("Conexao", nomeBanco + "1");
                    //USUARIO//
                    strCon = strCon + "sa" + LeINI("Conexao", nomeBanco + "2");
                    //DATABASE//
                    strCon = strCon + "CONNECTFARMA" + LeINI("Conexao", nomeBanco + "3");

                    bancoDados = new SqlServer(strCon);
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "arquivo:" + arquivoIni, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            

        }

        /// <summary>
        /// METODO QUE LÊ AS INFORMACOES DO ARQUIVO INI
        /// </summary>
        /// <param name="cabecalho"></param>
        /// <param name="chave"></param>
        /// <returns></returns>
        public static string LeINI(string cabecalho, string chave)
        {
            string retorno;
            StringBuilder resultado = new StringBuilder(100);

            uint funcionou = GetPrivateProfileString(cabecalho, chave, "", resultado, resultado.Capacity, arquivoIni);

            if (funcionou == 0)
            {
                retorno = "";
            }
            else
            {
                retorno = resultado.ToString().Substring(0, Convert.ToInt32(funcionou));
            }

            return retorno;
        }

        [DllImport("kernel32.dll")]
        public static extern uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
    }
}
