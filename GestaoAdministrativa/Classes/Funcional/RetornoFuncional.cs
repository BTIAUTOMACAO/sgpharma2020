﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class RetornoFuncional
    {
        public string Id { get; set; }
        public string Valor { get; set; }
    }
}
