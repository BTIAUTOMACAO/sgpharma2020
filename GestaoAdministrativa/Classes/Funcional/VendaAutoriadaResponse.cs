﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class VendaAutoriadaResponse
    {
        public string Cartao { get; set; }
        public string Data { get; set; }
        public string Hora { get; set; }
        public double ValorTotal { get; set; }
        public double Desconto { get; set; }
        public double TotalAPagar { get; set; }
        public double ValorAReceberAVista { get; set; }
        public string TipoConselhoDaVenda { get; set; }
        public string CodConselho { get; set; }
        public double ValorAReceberNoCartao { get; set; }
        public double ValorAVista { get; set; }
        public List<ProdutoConfirmaAutorizacaoResponse> lProdutosConfirmados { get; set; }

        public VendaAutoriadaResponse()
        {

        }

        public VendaAutoriadaResponse(VendaAutorizadaRequest objVendaAutorizadaRequest)
        {
            this.Cartao = objVendaAutorizadaRequest.Cartao;
            this.Data = objVendaAutorizadaRequest.Data;
            this.Hora = objVendaAutorizadaRequest.Hora;
            this.ValorTotal = objVendaAutorizadaRequest.ValorTotal;
            this.Desconto = objVendaAutorizadaRequest.Desconto;
            this.TotalAPagar = objVendaAutorizadaRequest.TotalAPagar;
            this.ValorAReceberAVista = objVendaAutorizadaRequest.ValorAReceberAVista;
            this.TipoConselhoDaVenda = objVendaAutorizadaRequest.TipoConselhoDaVenda;
            this.CodConselho = objVendaAutorizadaRequest.CodConselho;
            this.ValorAReceberNoCartao = objVendaAutorizadaRequest.ValorAReceberNoCartao;
            this.ValorAVista = objVendaAutorizadaRequest.ValorAVista;
        }
    }
}
