﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public static class Enums
    {
        public enum TipoVenda
        {
            SemReceita,
            ComReceita
        };

        public enum Conselho
        {
            CRM,
            CRO
        };

    }
}
