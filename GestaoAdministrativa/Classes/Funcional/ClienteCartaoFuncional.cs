﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{

   
    public class ClienteCartaoFuncional
    {
        
        public string Header { get; set; }
        public string Identificacao { get; set; }
        public string CodigoDaResposta { get; set; }
        public string CodigoDoProcessamento { get; set; }
        public string NumeroDaTransacoNSU { get; set; }
        public string NsuDoAutorizador { get; set; }
        public string DataDaTransacao { get; set; }
        public string HoraDaTransacao { get; set; }
        public string TextoParaOperador { get; set; }
        public string Autenticacao { get; set; }
        public string Trilha2DoCartao { get; set; }
        public string NomeDoPortador { get; set; }
        public string MapaDeAcoes { get; set; }
        public string MensagemSobreCliente { get; set; }
        public string DescricaoTransacao { get; set; }
        public string Trailer { get; set; }
        public Enum TipoDaVenda { get; set; }
        public DateTime DataReceita { get; set; }
        public int MyProperty { get; set; }
        public Enum ConselhoDaVenda { get; set; }
        public string CodConselho { get; set; }
        public string UF { get; set; }
        public bool FecharForm { get; set; }
    }
}
