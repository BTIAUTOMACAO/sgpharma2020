﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class ProdutoConfirmaAutorizacaoRequest
    {
        public string CodBarras { get; set; }
        public string DescricaoMedicamento { get; set; }
        public double PrecoMaximo { get; set; }
        public double PrecoVenda { get; set; }
        public double ProcentDescontoProduto { get; set; }
        public int Quantidade { get; set; }
        public double ValorDoDesconto { get; set; }
        public double PrecoAutorizado { get; set; }
        //produtosAutorizacao.Columns.Add("CodBarras", typeof(string));
        //    produtosAutorizacao.Columns.Add("DescricaoMedicamento", typeof(string));
        //    produtosAutorizacao.Columns.Add("PrecoMaximo", typeof(string));
        //    produtosAutorizacao.Columns.Add("PrecoVenda", typeof(string));
        //    produtosAutorizacao.Columns.Add("Prod_DPorc", typeof(string));
        //    produtosAutorizacao.Columns.Add("Qtde_Auto", typeof(string));
        //    produtosAutorizacao.Columns.Add("Retorno", typeof(string));
        //    produtosAutorizacao.Columns.Add("StatusProd", typeof(string));
    }
}
