﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class VendaAutorizadaRequest
    {
        public string Cartao { get; set; }
        public string Data { get; set; }
        public string Hora { get; set; }
        public double ValorTotal { get; set; }
        public double Desconto { get; set; }
        public double TotalAPagar { get; set; }
        public double ValorAReceberAVista { get; set; }
        public string TipoConselhoDaVenda { get; set; }
        public string CodConselho { get; set; }
        public double ValorAReceberNoCartao { get; set; }
        public double ValorAVista { get; set; }
        public string Nome { get; set; }
        public List<ProdutoConfirmaAutorizacaoResponse> lProdutosConfirmados { get; set; }
    }
}
