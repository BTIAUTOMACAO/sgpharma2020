﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class ProdutoFuncionalRequest
    {
        public string CodBarras { get; set; }
        public double PrecoDigitado { get; set; }
        public double Desconto { get; set; }
    }
}
