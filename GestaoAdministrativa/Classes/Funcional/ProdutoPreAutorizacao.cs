﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class ProdutoPreAutorizacao
    {
        public string DataReceita { get; set; }
        public string TipoConselho { get; set; }
        public string CodConselho { get; set; }
        public string UFConselho { get; set; }
        public string TipoProduto { get; set; }
        public string CodBarras { get; set; }
        public string DescricaoMedicamento { get; set; }
        public string CondicaoVenda { get; set; }
        public string PrecoMaximo { get; set; }
        public string PrecoPBM { get; set; }
        public string PrecoVenda { get; set; }
        public string StatusItem { get; set; }
        public string QuantidadeReceitada { get; set; }
        public string QuantidadeAutorizada { get; set; }
        public string Referencia { get; set; }
    }
}
