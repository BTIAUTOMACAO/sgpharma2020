﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class ProdutoConfirmaAutorizacaoResponse
    {
        public string CodBarras { get; set; }
        public double PrecoAutorizado { get; set; }
        public double PrecoDigitado { get; set; }
        public double DescontoPorcent { get; set; }
        public double DescontoValor { get; set; }
        public int Quantidade { get; set; }
        public double ProdTotalDigitado { get; set; }
        public double Pre_Valor { get; set; }
    }
}
