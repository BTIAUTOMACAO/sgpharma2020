﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.Funcional
{
    public class ConfirmaVendaResponse
    {
        public string Header { get; set; }
        public string Identificacao { get; set; }
        public string NumeroCupomFiscal { get; set; }
        public string ValorDaOperacao { get; set; }
        public string CodigoDaResposta { get; set; }
        public string NSU { get; set; }
        public string NSUDoAutorizador { get; set; }
        public string DataTransacao { get; set; }
        public string HoraTransacao { get; set; }
        public string TextoParaOperador { get; set; }
        public string Autenticacao { get; set; }
        public string SaldoCartao { get; set; }
        public string NomeDoPortador { get; set; }
        public string CodigoPreAutorizacao { get; set; }
        public string CRF { get; set; }
        public string UFCRF { get; set; }
        public string ValorPagarAVista { get; set; }
        public string QtdMedicamentos { get; set; }
        public string VendaComReceita { get; set; }
        public List<ProdutoPreAutorizacao> ProdutosPreAutorizacao { get; set; }
    }
}
