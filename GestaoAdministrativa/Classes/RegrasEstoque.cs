﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Classes
{
    public static class RegrasEstoque
    {
        private static string retorno;

        public static bool ExcluirEntradas(Entrada dadosEntrada)
        {
            var dt = new DataTable();

            if(String.IsNullOrEmpty(dadosEntrada.IdentificaEntradaCadastradaPorFornecedorDocto(dadosEntrada)))
            {
                MessageBox.Show("Entrada não cadastrada!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            dt = dadosEntrada.IdentificaEntradaCadastradaPorId(dadosEntrada);
            if (String.IsNullOrEmpty(dt.Rows[0]["PAG_DOCTO"].ToString()))
            {
                var dadosEntradaDetalhe = new EntradaDetalhe
                {
                    EstCodigo = Convert.ToInt32(dt.Rows[0]["EST_CODIGO"]),
                    EmpCodigo = Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]),
                    EntId = Convert.ToInt32(dt.Rows[0]["ENT_ID"]),
                    CfDocto = dt.Rows[0]["CF_DOCTO"].ToString(),
                };

                if (!String.IsNullOrEmpty(dadosEntradaDetalhe.IdentificaFaturaLancada(dadosEntradaDetalhe)))
                {
                    MessageBox.Show("Entrada com parcela já lançada no contas a pagar,\nexclua o título antes para poder alterar a entrada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            else
            {
                var statusPagto = new PagarDetalhe
                {
                    EmpCodigo = Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]),
                    PagDocto = dt.Rows[0]["PAG_DOCTO"].ToString(),
                    CfDocto = dt.Rows[0]["CF_DOCTO"].ToString(),
                };

                DataTable dtRetorno = statusPagto.VerificaParcelasPagas(statusPagto);
                bool pagou = false;

                for (int i = 0; i < dtRetorno.Rows.Count; i++)
                {
                    if (dtRetorno.Rows[i]["PD_STATUS"].ToString() == "Q")
                        pagou = true;
                }

                if (pagou)
                {
                    MessageBox.Show("Título no contas a pagar já possui baixa, exclua as baixas para poder alterar.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }

            BancoDados.AbrirTrans();

            if (!String.IsNullOrEmpty(dt.Rows[0]["PAG_DOCTO"].ToString()))
            {
                string idNuvem = Util.SelecionaCampoEspecificoDaTabela("PAGAR", "ID", "PAG_DOCTO", dt.Rows[0]["PAG_DOCTO"].ToString(), false, true, true);

                var faturaDetalhe = new PagarDetalhe();
                if (faturaDetalhe.ExcluiFaturaDetalhe(Principal.empAtual, dt.Rows[0]["PAG_DOCTO"].ToString(), false).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                var fatura = new Pagar();
                if (fatura.ExcluiFatura(Principal.empAtual, dt.Rows[0]["PAG_DOCTO"].ToString(), false).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                ExcluirPagaEPagarDetalheNuvem(idNuvem);
            }

            var entDetalheExcluir = new EntradaDetalhe();
            if (entDetalheExcluir.ExcluiParcelasEntradaNotas(dadosEntrada.EstCodigo, dadosEntrada.EmpCodigo, dadosEntrada.EntId).Equals(false))
            {
                BancoDados.ErroTrans();
                return false;
            }

            var entradaItens = new EntradaItens();
            var dadosProdutos = new ProdutoDetalhe();

            List<EntradaItens> listBuscaEntradaItens = entradaItens.CarregaEntradasItens(Principal.estAtual, Principal.empAtual, dadosEntrada.EntId);
            for (int i = 0; i < listBuscaEntradaItens.Count; i++)
            {
                var dadosProdutoDetalhe = new ProdutoDetalhe
                {
                    EmpCodigo = listBuscaEntradaItens[i].EmpCodigo,
                    EstCodigo = listBuscaEntradaItens[i].EstCodigo,
                    ProdCodigo = listBuscaEntradaItens[i].ProdCodigo,
                    ProdEstAtual = listBuscaEntradaItens[i].EntQtdeEstoque,
                    ProdQtdeUnComp = listBuscaEntradaItens[i].EntQtde,
                    DtAlteracao = DateTime.Now,
                    OpAlteracao = Principal.usuario,
                };

                if (dadosProdutoDetalhe.AtualizaEstoqueAtual(dadosProdutoDetalhe, "-", true).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                entradaItens.EmpCodigo = listBuscaEntradaItens[i].EmpCodigo;
                entradaItens.EstCodigo = listBuscaEntradaItens[i].EstCodigo;
                entradaItens.EntId = listBuscaEntradaItens[i].EntId;
                entradaItens.ProdCodigo = listBuscaEntradaItens[i].ProdCodigo;

                var dadosMovimento = new MovEstoque
                {
                    EstCodigo = listBuscaEntradaItens[i].EstCodigo,
                    EmpCodigo = listBuscaEntradaItens[i].EmpCodigo,
                    EstIndice = listBuscaEntradaItens[i].EstIndice,
                    ProdCodigo = listBuscaEntradaItens[i].ProdCodigo,
                };

                if (dadosMovimento.ManutencaoMovimentoEstoque("D", dadosMovimento).Equals(false))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                if (dadosProdutoDetalhe.AtualizaCusto(dadosProdutoDetalhe).Equals(false))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                if (Funcoes.LeParametro(9, "29", true).Equals("S"))
                {
                    var dadosProduto = new Produto();
                    List<Produto> listProduto = dadosProduto.IdentificaProdutoControlado(listBuscaEntradaItens[i].ProdCodigo);
                    if (listProduto[0].ProdControlado.Equals("S"))
                    {
                        var dadosSngpcEntrada = new SngpcEntrada
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EntDocto = listBuscaEntradaItens[i].EntDocto,
                            EntCNPJ = listBuscaEntradaItens[i].CfDocto,
                            ProdCodigo = listBuscaEntradaItens[i].ProdCodigo,
                        };
                        if (dadosSngpcEntrada.ExcluiProdutoViaEntradaDeNotas(dadosSngpcEntrada, true).Equals(-1))
                        {
                            BancoDados.ErroTrans();
                            return false;
                        }

                        var identificaVenda = new SngpcVenda();
                        int qtde = identificaVenda.IdentificaSeJaTemLoteEProdutoVendido(listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].EntLote, Convert.ToDateTime(dt.Rows[0]["ENT_DTLANC"]));

                        var excluiLote = new SngpcControleVendas();
                        excluiLote.ProCodigo = listBuscaEntradaItens[i].ProdCodigo;
                        excluiLote.Lote = listBuscaEntradaItens[i].EntLote;
                        excluiLote.Qtde = listBuscaEntradaItens[i].EntQtdeEstoque - qtde;
                        excluiLote.Data = DateTime.Now;

                        if (excluiLote.IdentificaSeJaTemLoteEProduto(listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].EntLote).Equals(listBuscaEntradaItens[i].EntQtdeEstoque + qtde))
                        {
                            if (!excluiLote.ExcluirPorCodigoLote(listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].EntLote))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }
                        }
                        else
                        {
                            if (!excluiLote.SubtraiQtdeMesmoLoteEProduto(excluiLote))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }
                        }
                    }
                }
                if (entradaItens.ExcluiProdutoEntradasItens(entradaItens).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    return false;
                }
            }

            if (Funcoes.LeParametro(6, "351", true).Equals("S"))
            {
                var vencimento = new ProdutoVencimento();
                if (vencimento.ExcluiProdutoViaEntradaDeNotas(dadosEntrada.EstCodigo, dadosEntrada.EmpCodigo, dadosEntrada.EntDocto).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    return false;
                }
            }

            if (dadosEntrada.ExcluiEntradaNota(dadosEntrada.EstCodigo, dadosEntrada.EmpCodigo, dadosEntrada.EntId).Equals(-1))
            {
                BancoDados.ErroTrans();
                return false;
            }

            BancoDados.FecharTrans();
            return true;
        }

        public static bool ExcluirAjustes(string codAjustes, char operacao, int qtde, string codBarra)
        {
            retorno = BancoDados.selecionarRegistro("SELECT PROD_ESTATUAL FROM PRODUTOS WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = (SELECT PROD_CODIGO FROM"
                + " PRODUTOS_BARRA WHERE EST_CODIGO = " + Principal.estAtual + " AND COD_BARRA = '" + codBarra + "'", "PROD_ESTATUAL");
            if (operacao.Equals('+'))
            {
                if (BancoDados.executarSql("UPDATE PRODUTOS SET PROD_ESTATUAL = PROD_ESTATUAL - " + qtde + ", DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = "
                    + Principal.usuario + " WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = (SELECT PROD_CODIGO FROM PRODUTOS_BARRA WHERE EST_CODIGO = " + Principal.estAtual
                    + " AND COD_BARRA = '" + codBarra + "'").Equals(false))
                    return false;
            }
            else
            {
                if (BancoDados.executarSql("UPDATE PRODUTOS SET PROD_ESTATUAL = PROD_ESTATUAL + " + qtde + ", DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = "
                    + Principal.usuario + " WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = (SELECT PROD_CODIGO FROM PRODUTOS_BARRA WHERE EST_CODIGO = " + Principal.estAtual
                    + " AND COD_BARRA = '" + codBarra + "'").Equals(false))
                    return false;
            }
            retorno = BancoDados.selecionarRegistro("SELECT ENT_CODIGO FROM AJUSTES_ESTOQUE WHERE EST_CODIGO = " + Principal.estAtual + " AND AJU_CODIGO = " + codAjustes,"ENT_CODIGO");
            return true;
        }

        public static async void ExcluirPagaEPagarDetalheNuvem(string IDNuvem)
        {
            await EnviaOuRecebeDadosMysql.ExcluiPagarEPagarDetalhe(IDNuvem);
        }




    }
}
