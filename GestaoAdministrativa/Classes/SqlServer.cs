﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    class SqlServer : IConexao
    {
        private string strCon;
        private SqlConnection strConexao = new SqlConnection();
        private SqlTransaction transacao = null;
        public SqlTransaction pedTrans = null;
        private SqlCommand comando = new SqlCommand();

        DataTable dt = new DataTable();

        public SqlServer(string strConexao)
        {
            strCon = strConexao;
        }

        public bool conectar(string paramentroConexao)
        {
            try
            {
                strConexao.ConnectionString = paramentroConexao;
                strConexao.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message,"SqlServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool fecharConexao()
        {
            try
            {
                if (strConexao.State == ConnectionState.Open)
                {
                    strConexao.Close();
                    strConexao.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SqlServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable selecionarRegistros(string comandoSql)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }

                using (SqlDataAdapter daPesq = new SqlDataAdapter(comandoSql, strConexao))
                {
                    DataTable dtPesq = new DataTable();
                    daPesq.Fill(dtPesq);
                    daPesq.Dispose();
                    return dtPesq;
                }
            }
            finally
            {
                fecharConexao();
            }
        }

        public string selecionarRegistro(string comandoSql, string campo)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                using (SqlDataAdapter daPesq = new SqlDataAdapter(comandoSql, strConexao))
                {
                    dt.Clear();
                    daPesq.Fill(dt);

                    if (dt.Rows.Count != 0)
                    {
                        return dt.Rows[0]["" + campo + ""].ToString();
                    }
                    else
                        return String.Empty;
                }
            }
            finally
            {
                fecharConexao();
            }
        }

        public bool executarComando(string [] comando, int indice)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                transacao = strConexao.BeginTransaction();
                for (int i = 0; i < indice; i++)
                {
                    using (SqlCommand Comando = new SqlCommand())
                    {
                        Comando.Transaction = transacao;
                        Comando.CommandText = comando[i];
                        Comando.Connection = strConexao;
                        Comando.ExecuteNonQuery();
                        Comando.Dispose();
                    }
                }
                transacao.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message, "SqlServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                fecharConexao();
            }
        }

        public bool executarSql(string comandoSql)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                using (SqlCommand Comando = new SqlCommand())
                {
                    transacao = strConexao.BeginTransaction();
                    Comando.Transaction = transacao;
                    Comando.CommandText = comandoSql;
                    Comando.Connection = strConexao;
                    Comando.ExecuteNonQuery();
                    transacao.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message, "SqlServer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                fecharConexao();
            }
        }


        public int ExecuteNoQuery(String pComando, SqlParamsList ParamsList)
        {
            int ret;
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (SqlCommand Cmd = new SqlCommand())
            {
                transacao = strConexao.BeginTransaction();
                Cmd.CommandText = pComando;
                Cmd.Transaction = transacao;
                Cmd.Connection = strConexao;
                SetParams(Cmd, ParamsList);
                try
                {
                    ret = Cmd.ExecuteNonQuery();
                    transacao.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message, "CONNECTFARMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ret = -1;
                    transacao.Rollback();
                }
                finally
                {
                    fecharConexao();
                }
            }
            return ret;
        }

        private SqlConnection SqlConexao()
        {
            SqlConnection SConexao = new SqlConnection(strCon);
            try
            {
                SConexao.Open();

            }
            catch (Exception E)
            {
                throw new Exception("Erro ao conectar a base de dados " + SConexao.DataSource + ":" + SConexao.Database + " Erro: " + E.Message);
            }
            return SConexao;
        }

        SqlCommand Q;
        SqlTransaction trans;

        public void AbrirTrans()
        {
            Q = SqlConexao().CreateCommand();
            trans = Q.Connection.BeginTransaction(IsolationLevel.ReadUncommitted);
            Q.Transaction = trans;
        }

        public void FecharTrans()
        {
            trans.Commit();
            Q.Connection.Close();
        }

        public void ErroTrans()
        {
            trans.Rollback();
            Q.Connection.Close();
        }

        public int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList)
        {
            //try
            //{
                int ret;
                //using (Q.CommandText = pComando)
                //{
                    Q.CommandText = pComando;
                    SetParams(Q, ParamsList);
                    try
                    {
                        ret = Q.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro: " + ex.Message,"CONNECTFARMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        ret = -1;
                    }
                //}
                return ret;
            //}
            //finally
            //{
            //    fecharConexao();
            //}
        }

        /// <summary>
        /// Método para retornar um DataTable a partir de uma intrução sql.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna o DataTable carregado</returns>
        public DataTable GetDataTable(String pComando, SqlParamsList ParamsList)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }

                using (SqlDataAdapter daPesq = new SqlDataAdapter(pComando, strConexao))
                {
                    DataTable dtPesq = new DataTable();
                    daPesq.SelectCommand.CommandText = pComando;
                    daPesq.SelectCommand.Connection = strConexao;
                    SetParams(daPesq.SelectCommand, ParamsList);
                    daPesq.Fill(dtPesq);
                    daPesq.Dispose();
                    return dtPesq;
                }
            }
            finally
            {
                fecharConexao();
            }
        }

        // <summary>
        /// Método para recuperar somente um campo do banco de dados.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna um dado do Tipo Object</returns>
        public object ExecuteScalar(String pComando, SqlParamsList ParamsList)
        {
            try
            {
                object r;

                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }

                using (SqlCommand comando = new SqlCommand())
                {
                    comando.CommandText = pComando;
                    comando.Connection = strConexao;
                    SetParams(comando, ParamsList);
                    r = comando.ExecuteScalar();
                    return r;
                }
            }
            finally
            {
                fecharConexao();
            }
        }



        /// <summary>
        /// Metodo que atribui paramentros no Cmd(parametro) passado, conforme o Banco(FireBird).
        /// </summary>
        /// <param name="Cmd">DBCommand que irá receber os paramentros</param>
        /// <param name="ParamsList">Lista dos paramentros.</param>
        /// <returns>Retorna um int, indicando a quantidade de parametros adicionados ao Cmd</returns>
        private int SetParams(IDbCommand Cmd, SqlParamsList ParamsList)
        {
            int iReturn = 0;

            if (ParamsList == null)
            return iReturn;

            char cDef = '@';
            char cNew = ' ';

            cNew = '@';

            Cmd.CommandText = Cmd.CommandText.Replace(cDef, cNew);
            Cmd.Parameters.Clear();

            foreach (Fields Param in ParamsList)
            {
                if (Param.Nome.Substring(0, 1) != "@")
                    Param.Nome = "@" + Param.Nome;

                SqlParameter ParamFb = new SqlParameter();
                ParamFb.ParameterName = Param.Nome;
                ParamFb.Value = Param.Valor;
                Cmd.Parameters.Add(ParamFb);
            
                iReturn++;
            }
            return iReturn;
      
        }

    }
}
