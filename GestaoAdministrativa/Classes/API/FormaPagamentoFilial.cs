﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.API
{
    public class FormaPagamentoFilial
    {
        public int formaID { get; set; }
        public string descricao { get; set; }
        public char venctoDiaFixo { get; set; }
        public int qtdeParcelas { get; set; }
        public int diaVenctoFixo { get; set; }
        public int qtdeVias { get; set; }
        public char operacaoCaixa { get; set; }
        public char convBeneficio { get; set; }
        public char formaLiberado { get; set; }
    }
}
