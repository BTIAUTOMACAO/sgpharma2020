﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Classes.API
{
    public class EspecieFilial
    {
        public int especieID { get; set; }
        public string descricao { get; set; }
        public char desabilitado { get; set; }
        public char cheque { get; set; }
        public char caixa { get; set; }
        public char vinculado { get; set; }
        public string sat { get; set; }
    }
}
