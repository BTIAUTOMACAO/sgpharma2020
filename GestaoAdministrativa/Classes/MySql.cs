﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.Windows.Forms;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// CLASSE QUE CONTEM OS METODOS PRESENTES NA CLASSE BANCO DE DADOS COM O USO DO MYSQL
    /// </summary>
    public class MySql : IConexao
    {
        private string strCon;
        private OdbcConnection strConexao = new OdbcConnection();
        private OdbcTransaction transacao = null;

        public MySql(string strConexao)
        {
            strCon = strConexao;
        }

        public bool conectar(string paramentroConexao)
        {
            try
            {
                strConexao.ConnectionString = paramentroConexao;
                strConexao.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool fecharConexao()
        {
            try
            {
                if (strConexao.State == ConnectionState.Open)
                {
                    strConexao.Close();
                    strConexao.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable selecionarRegistros(string comandoSql)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            OdbcDataAdapter daPesq = new OdbcDataAdapter(comandoSql, strConexao);
            DataTable dsPesq = new DataTable();
            daPesq.Fill(dsPesq);

            return dsPesq;
        }

        public string selecionarRegistro(string comandoSql, string campo)
        {
            return "";
        }

        public bool executarSql(string comandoSql)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                transacao = strConexao.BeginTransaction();
                OdbcCommand Comando = new OdbcCommand();
                Comando.CommandText = comandoSql;
                Comando.Connection = strConexao;
                Comando.ExecuteNonQuery();
                transacao.Commit();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool executarComando(string[] comando, int indice)
        {
            try
            {
                //if (strConexao.State != ConnectionState.Open)
                //{
                //    conectar(strCon);
                //}
                //FbCommand Comando = new FbCommand();
                //Comando.Transaction = transacao;
                //Comando.CommandText = comandoSql;
                //Comando.Connection = strConexao;
                //Comando.ExecuteNonQuery();
                //Comando.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public int ExecuteNoQuery(String pComando, SqlParamsList ParamsList)
        {
            int ret = 1;
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            //using (FbCommand Cmd = new FbCommand())
            //{
            //    transacao = strConexao.BeginTransaction();
            //    Cmd.CommandText = pComando;
            //    Cmd.Transaction = transacao;
            //    Cmd.Connection = strConexao;
            //    SetParams(Cmd, ParamsList);
            //    try
            //    {
            //        ret = Cmd.ExecuteNonQuery();
            //        transacao.Commit();
            //    }
            //    catch
            //    {
            //        ret = -1;
            //        transacao.Rollback();
            //    }
            //}
            return ret;
        }

        public void AbrirTrans()
        {
            //pedTrans = strConexao.BeginTransaction();
        }

        public void FecharTrans()
        {
            //pedTrans.Commit();
        }

        public void ErroTrans()
        {
            //pedTrans.Rollback();
        }

        public int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList)
        {
            int ret;
            //if (strConexao.State != ConnectionState.Open)
            //{
            //    conectar(strCon);
            //}
            //using (FbCommand Cmd = new FbCommand())
            //{
            //    Cmd.CommandText = pComando;
            //    Cmd.Transaction = pedTrans;
            //    Cmd.Connection = strConexao;
            //    SetParams(Cmd, ParamsList);
            //    try
            //    {
            //        ret = Cmd.ExecuteNonQuery();
            //    }
            //    catch (Exception ex)
            //    {
                    ret = -1;
            //    }
            //}
            return ret;
        }

        public object ExecuteScalar(String pComando, SqlParamsList ParamsList)
        {
            object r = 0;

            //if (strConexao.State != ConnectionState.Open)
            //{
            //    conectar(strCon);
            //}

            //using (FbCommand Cmd = new FbCommand())
            //{
            //    Cmd.CommandText = pComando;
            //    SetParams(Cmd, ParamsList);
            //    r = Cmd.ExecuteScalar();
            //}
            return r;
        }

        /// <summary>
        /// Metodo que atribui paramentros no Cmd(parametro) passado, conforme o Banco(FireBird, SQLServer, MySQL).
        /// </summary>
        /// <param name="Cmd">DBCommand que irá receber os paramentros</param>
        /// <param name="ParamsList">Lista dos paramentros.</param>
        /// <returns>Retorna um int, indicando a quantidade de parametros adicionados ao Cmd</returns>
        private int SetParams(IDbCommand Cmd, SqlParamsList ParamsList)
        {
            int iReturn = 0;

            if (ParamsList == null)
                return iReturn;

            char cDef = '@';
            char cNew = ' ';

            cNew = '@';

            Cmd.CommandText = Cmd.CommandText.Replace(cDef, cNew);
            Cmd.Parameters.Clear();

            foreach (Fields Param in ParamsList)
            {
                if (Param.Nome.Substring(0, 1) != "@")
                    Param.Nome = "@" + Param.Nome;

                //FbParameter ParamFb = new FbParameter();
                //ParamFb.ParameterName = Param.Nome;
                //ParamFb.Value = Param.Valor;
                //Cmd.Parameters.Add(ParamFb);

                iReturn++;
            }
            return iReturn;

        }

        /// <summary>
        /// Método para retornar um DataTable a partir de uma intrução sql.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna o DataTable carregado</returns>
        public DataTable GetDataTable(String pComando, SqlParamsList ParamsList)
        {
            DataTable dt = new DataTable();

            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            return dt;
            //using (FbDataAdapter daPesq = new FbDataAdapter())
            //{
            //    DataTable dtPesq = new DataTable();
            //    daPesq.SelectCommand.CommandText = pComando;
            //    daPesq.SelectCommand.Connection = strConexao;
            //    SetParams(daPesq.SelectCommand, ParamsList);
            //    daPesq.Fill(dtPesq);
            //    daPesq.Dispose();
            //    return dtPesq;
            //}
        }

    }
}
