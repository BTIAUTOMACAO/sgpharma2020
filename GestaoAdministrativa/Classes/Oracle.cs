﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Windows.Forms;
using SqlNegocio;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// CLASSE QUE CONTEM OS METODOS PRESENTES NA CLASSE BANCO DE DADOS COM O USO DO ORACLE
    /// </summary>
    public class Oracle : IConexao 
    {
        private string strCon;
        private OleDbConnection strConexao = new OleDbConnection();
        private OleDbTransaction transacao = null;
        DataTable dt = new DataTable();

        public Oracle(string strConexao)
        {
            strCon = strConexao;
        }

        public bool conectar(string paramentroConexao)
        {
            try
            {
                strConexao.ConnectionString = paramentroConexao;
                strConexao.Open();
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool fecharConexao()
        {
            try
            {
                if (strConexao.State == ConnectionState.Open)
                {
                    strConexao.Close();
                    strConexao.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public DataTable selecionarRegistros(string comandoSql)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }

            using (OleDbDataAdapter daPesq = new OleDbDataAdapter(comandoSql, strConexao))
            {
                DataTable dtPesq = new DataTable();
                daPesq.Fill(dtPesq);
                daPesq.Dispose();
                fecharConexao();
                return dtPesq;
            }
        }

        public string selecionarRegistro(string comandoSql, string campo)
        {
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (OleDbDataAdapter daPesq = new OleDbDataAdapter(comandoSql, strConexao))
            {
                dt.Clear();
                daPesq.Fill(dt);

                if (dt.Rows.Count != 0)
                {
                    return dt.Rows[0]["" + campo + ""].ToString();
                }
                else
                    return String.Empty;
            }
        }

        public bool executarSql(string comandoSql)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }

                using (OleDbCommand Comando = new OleDbCommand())
                {
                    transacao = strConexao.BeginTransaction();
                    Comando.Transaction = transacao;
                    Comando.CommandText = comandoSql;
                    Comando.Connection = strConexao;
                    Comando.ExecuteNonQuery();
                    transacao.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message + "\nComando: " + comandoSql, "Banco de Dados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool executarComando(string[] comando, int indice)
        {
            try
            {
                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                transacao = strConexao.BeginTransaction();
                for (int i = 0; i < indice; i++)
                {
                    using (OleDbCommand Comando = new OleDbCommand())
                    {
                        Comando.Transaction = transacao;
                        Comando.CommandText = comando[i];
                        Comando.Connection = strConexao;
                        Comando.ExecuteNonQuery();
                        Comando.Dispose();
                    }
                }
                transacao.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transacao.Rollback();
                MessageBox.Show("Erro: " + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        private OleDbConnection OracleConexao()
        {
            OleDbConnection SConexao = new OleDbConnection(strCon);
            try
            {
                SConexao.Open();

            }
            catch (Exception E)
            {
                throw new Exception("Erro ao conectar a base de dados " + SConexao.DataSource + ":" + SConexao.Database + " Erro: " + E.Message);
            }
            return SConexao;
        }

        OleDbCommand Q;
        OleDbTransaction trans;

        public void AbrirTrans()
        {
            Q = OracleConexao().CreateCommand();
            trans = Q.Connection.BeginTransaction(IsolationLevel.ReadUncommitted);
            Q.Transaction = trans;
        }

        public void FecharTrans()
        {
            trans.Commit();
            Q.Connection.Close();
        }

        public void ErroTrans()
        {
            trans.Rollback();
            Q.Connection.Close();
        }

        public int ExecuteNoQueryTrans(String pComando, SqlParamsList ParamsList)
        {
            int ret;
           
            Q.CommandText = pComando;
            SetParams(Q, ParamsList);
            try
            {
                ret = Q.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\nComando:" + pComando, "SG PHARMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ret = -1;
            }
            return ret;
        }

        public int ExecuteNoQuery(String pComando, SqlParamsList ParamsList)
        {
            int ret = 1;
            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            using (OleDbCommand Cmd = new OleDbCommand())
            {
                Cmd.CommandText = pComando;
                Cmd.Connection = strConexao;
                SetParams(Cmd, ParamsList);
                try
                {
                    ret = Cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message + "\nComando: " + pComando, "SG PHARMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ret = -1;
                    
                }
                finally
                {
                    fecharConexao();
                }
            }
            return ret;
        }


        public object ExecuteScalar(String pComando, SqlParamsList ParamsList)
        {
            try
            {
                object r;

                if (strConexao.State != ConnectionState.Open)
                {
                    conectar(strCon);
                }
                
                using (OleDbCommand comando = new OleDbCommand())
                {
                    comando.CommandText = pComando;
                    comando.Connection = strConexao;
                    SetParams(comando, ParamsList);
                    r = comando.ExecuteScalar();
                    return r;
                }
            }
            finally
            {
                fecharConexao();
            }
        }

        /// <summary>
        /// Metodo que atribui paramentros no Cmd(parametro) passado, conforme o Banco(FireBird, SQLServer, MySQL).
        /// </summary>
        /// <param name="Cmd">DBCommand que irá receber os paramentros</param>
        /// <param name="ParamsList">Lista dos paramentros.</param>
        /// <returns>Retorna um int, indicando a quantidade de parametros adicionados ao Cmd</returns>
        private int SetParams(IDbCommand Cmd, SqlParamsList ParamsList)
        {
            int iReturn = 0;

            if (ParamsList == null)
                return iReturn;

            char cDef = '@';
            char cNew = ' ';

            cNew = '@';

            Cmd.CommandText = Cmd.CommandText.Replace(cDef, cNew);
            Cmd.Parameters.Clear();

            foreach (Fields Param in ParamsList)
            {
                if (Param.Nome.Substring(0, 1) != "@")
                    Param.Nome = "@" + Param.Nome;

                OleDbParameter ParamOra = new OleDbParameter();
                ParamOra.ParameterName = Param.Nome;
                ParamOra.Value = Param.Valor;
                Cmd.Parameters.Add(ParamOra);

                iReturn++;
            }
            return iReturn;

        }

        /// <summary>
        /// Método para retornar um DataTable a partir de uma intrução sql.
        /// </summary>
        /// <param name="pComando">Comando SQL a ser executado contra o banco de dados.</param>
        /// <param name="ParamsList">Lista de Parametros contidos no comando SQL.</param>
        /// <returns>Retorna o DataTable carregado</returns>
        public DataTable GetDataTable(String pComando, SqlParamsList ParamsList)
        {
            DataTable dt = new DataTable();

            if (strConexao.State != ConnectionState.Open)
            {
                conectar(strCon);
            }
            DataTable dtPesq = new DataTable();
            try
            {
                using (OleDbDataAdapter daPesq = new OleDbDataAdapter(pComando, strConexao))
                {
                    daPesq.SelectCommand.CommandText = pComando;
                    daPesq.SelectCommand.Connection = strConexao;
                    SetParams(daPesq.SelectCommand, ParamsList);
                    daPesq.Fill(dtPesq);
                    daPesq.Dispose();
                    return dtPesq;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\nComando: " + pComando, "SG PHARMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return dtPesq;
            }
        }

    }
}
