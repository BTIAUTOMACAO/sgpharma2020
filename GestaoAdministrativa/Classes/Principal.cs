﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data;
using Microsoft.VisualBasic;
using System.Windows.Forms;

namespace GestaoAdministrativa.Classes
{
    /// <summary>
    /// CLASSE QUE CONTÉM DECLARAÇÕES DE VARIÁVEIS GLOBAIS
    /// </summary>
    public static class Principal
    {
        #region VARIÁVEIS DIVERSAS
        private static string Data = "";
        public static string data
        {
            get { return Data; }
            set { Data = value; }
        }

        private static string strpesq = "";
        public static string strPesq
        {
            get { return strpesq; }
            set { strpesq = value; }
        }

        private static int Indice = 0;
        public static int indice
        {
            get { return Indice; }
            set { Indice = value; }
        }

        private static bool BNaveg = false;
        public static bool bNaveg
        {
            get { return BNaveg; }
            set { BNaveg = value; }
        }

        public static string[] matriz;


        private static string strsql = "";
        public static string strSql
        {
            get { return strsql; }
            set { strsql = value; }
        }

        private static string strcmd = "";
        public static string strCmd
        {
            get { return strcmd; }
            set { strcmd = value; }
        }

        private static string endconexao = "";
        public static string endConexao
        {
            get { return endconexao; }
            set { endconexao = value; }
        }

        private static string nomeservidor = "";
        public static string nomeServidor
        {
            get { return nomeservidor; }
            set { nomeservidor = value; }
        }

        private static string strordem = "";
        public static string strOrdem
        {
            get { return strordem; }
            set { strordem = value; }
        }

        private static string versistema = "";
        public static string verSistema
        {
            get { return versistema; }
            set { versistema = value; }
        }

        private static DataTable dtpesq = new DataTable();
        public static DataTable dtPesq
        {
            get { return dtpesq; }
            set { dtpesq = value; }
        }

        private static DataTable dtbusca = new DataTable();
        public static DataTable dtBusca
        {
            get { return dtbusca; }
            set { dtbusca = value; }
        }

        private static DataTable dtretorno = new DataTable();
        public static DataTable dtRetorno
        {
            get { return dtretorno; }
            set { dtretorno = value; }
        }

        private static DataTable dtregistros = new DataTable();
        public static DataTable dtRegistros
        {
            get { return dtregistros; }
            set { dtregistros = value; }
        }

        private static DataTable dtrelatorio = new DataTable();
        public static DataTable dtRelatorio
        {
            get { return dtrelatorio; }
            set { dtrelatorio = value; }
        }

        private static string nomeatual;
        public static string nomeAtual
        {
            get { return nomeatual; }
            set { nomeatual = value; }
        }

        private static int estatual;
        public static int estAtual
        {
            get { return estatual; }
            set { estatual = value; }
        }

        private static int empatual;
        public static int empAtual
        {
            get { return empatual; }
            set { empatual = value; }
        }

        private static string nomeestacao;
        public static string nomeEstacao
        {
            get { return nomeestacao; }
            set { nomeestacao = value; }
        }


        private static string nUsuario;
        public static string usuario
        {
            get { return nUsuario; }
            set { nUsuario = value; }
        }

        //private static string usuarioid;
        //public static string usuarioID
        //{
        //    get { return usuarioid; }
        //    set { usuarioid = value; }
        //}

        private static string usuadm;
        public static string usuADM
        {
            get { return usuadm; }
            set { usuadm = value; }
        }

        private static string nBanco;
        public static string nomeBanco
        {
            get { return nBanco; }
            set { nBanco = value; }
        }

        private static string nSupervisor;
        public static string supervisor
        {
            get { return nSupervisor; }
            set { nSupervisor = value; }
        }

        private static string nBancoDados;
        public static string bancoDados
        {
            get { return nBancoDados; }
            set { nBancoDados = value; }
        }

        private static string nImpressora;
        public static string impressora
        {
            get { return nImpressora; }
            set { nImpressora = value; }
        }


        private static int contformularios;
        public static int contFormularios
        {
            get { return contformularios; }
            set { contformularios = value; }
        }

        private static string msg;
        public static string mensagem
        {
            get { return msg; }
            set { msg = value; }
        }

        private static string titulo;
        public static string msgTitulo
        {
            get { return titulo; }
            set { titulo = value; }
        }

        private static MessageBoxButtons estilo;
        public static MessageBoxButtons msgEstilo
        {
            get { return estilo; }
            set { estilo = value; }
        }

        private static MessageBoxIcon icone;
        public static MessageBoxIcon msgIcone
        {
            get { return icone; }
            set { icone = value; }
        }

        private static string mOcorrencia;
        public static string motivo
        {
            get { return mOcorrencia; }
            set { mOcorrencia = value; }
        }

        private static MDIPrincipal mdiprincipal;
        public static MDIPrincipal mdiPrincipal
        {
            get { return mdiprincipal; }
            set { mdiprincipal = value; }
        }

        private static string sretorno;
        public static string sRetorno
        {
            get { return sretorno; }
            set { sretorno = value; }
        }

        private static int iretorno;
        public static int iRetorno
        {
            get { return iretorno; }
            set { iretorno = value; }
        }

        private static string med;
        public static string medico
        {
            get { return med; }
            set { med = value; }
        }

        private static string uF;
        public static string uf
        {
            get { return uF; }
            set { uF = value; }
        }
        private static string crmCro;
        public static string crm
        {
            get { return crmCro; }
            set { crmCro = value; }
        }

        private static string EnderecoFp = "";
        public static string enderecoFp
        {
            get { return EnderecoFp; }
            set { EnderecoFp = value; }
        }

        private static string ExeDna = "";
        public static string exeDna
        {
            get { return ExeDna; }
            set { ExeDna = value; }
        }

        #endregion

        #region BUSCA DE PRODUTOS
        private static string proddescr;
        public static string prodDescr
        {
            get { return proddescr; }
            set { proddescr = value; }
        }

        private static string codbarra;
        public static string codBarra
        {
            get { return codbarra; }
            set { codbarra = value; }
        }

        private static string prodcodigo;
        public static string prodCodigo
        {
            get { return prodcodigo; }
            set { prodcodigo = value; }
        }
        #endregion

        #region BUSCA DE FORNECEDORES
        private static string nomeclifor;
        public static string nomeCliFor
        {
            get { return nomeclifor; }
            set { nomeclifor = value; }
        }

        private static string cnpjclifor;
        public static string cnpjCliFor
        {
            get { return cnpjclifor; }
            set { cnpjclifor = value; }
        }

        private static string cidadeclifor;
        public static string cidadeCliFor
        {
            get { return cidadeclifor; }
            set { cidadeclifor = value; }
        }

        private static string idclifor;
        public static string idCliFor
        {
            get { return idclifor; }
            set { idclifor = value; }
        }
        #endregion

        

        #region VENDAS

        private static string tipocupom;
        public static string tipoCupom
        {
            get { return tipocupom; }
            set { tipocupom = value; }
        }

        //private static bool Valida;
        //public static bool valida
        //{
        //    get { return Valida; }
        //    set { Valida = value; }
        //}


        private static string wsconexao;
        public static string wsConexao
        {
            get { return wsconexao; }
            set { wsconexao = value; }
        }

        private static string wscartao;
        public static string wsCartao
        {
            get { return wscartao; }
            set { wscartao = value; }
        }

        private static string wscredenciado;
        public static string wsCredenciado
        {
            get { return wscredenciado; }
            set { wscredenciado = value; }
        }


        private static string wssenhaacesso;
        public static string wsSenhaAcesso
        {
            get { return wssenhaacesso; }
            set { wssenhaacesso = value; }
        }

        private static string wsobrigaSenha;
        public static string wsObrigaSenha
        {
            get { return wsobrigaSenha; }
            set { wsobrigaSenha = value; }
        }

        private static string wspreAutoriza;
        public static string wsPreAutoriza
        {
            get { return wspreAutoriza; }
            set { wspreAutoriza = value; }
        }

        private static string wsempresa;
        public static string wsEmpresa
        {
            get { return wsempresa; }
            set { wsempresa = value; }
        }

        private static string wsregras;
        public static string wsRegras
        {
            get { return wsregras; }
            set { wsregras = value; }
        }

        private static int wsconid;
        public static int wsConID
        {
            get { return wsconid; }
            set { wsconid = value; }
        }

        private static string wscomsemrec;
        public static string wsComSemRec
        {
            get { return wscomsemrec; }
            set { wscomsemrec = value; }
        }

        private static bool checamedcomsemreceita;
        public static bool checaMedComSemReceita
        {
            get { return checamedcomsemreceita; }
            set { checamedcomsemreceita = value; }
        }

        private static int wsqtdeprodutosretorno;
        public static int wsQtdeProdutosRetorno
        {
            get { return wsqtdeprodutosretorno; }
            set { wsqtdeprodutosretorno = value; }
        }

        private static bool preautorizou;
        public static bool preAutorizou
        {
            get { return preautorizou; }
            set { preautorizou = value; }
        }

        private static string wstransID;
        public static string wsTransID
        {
            get { return wstransID; }
            set { wstransID = value; }
        }

        private static int tipovenda;
        public static int tipoVenda
        {
            get { return tipovenda; }
            set { tipovenda = value; }
        }

        private static bool Flag;
        public static bool flag
        {
            get { return Flag; }
            set { Flag = value; }
        }

        private static decimal descemp;
        public static decimal descEmp
        {
            get { return descemp; }
            set { descemp = value; }
        }

        private static string wsmnome;
        public static string wsMNome
        {
            get { return wsmnome; }
            set { wsmnome = value; }
        }

        private static string wscrm;
        public static string wsCRM
        {
            get { return wscrm; }
            set { wscrm = value; }
        }

        private static string wsuf;
        public static string wsUF
        {
            get { return wsuf; }
            set { wsuf = value; }
        }

        private static DateTime wsdata;
        public static DateTime wsData
        {
            get { return wsdata; }
            set { wsdata = value; }
        }

        private static string wsnumrec;
        public static string wsNumRec
        {
            get { return wsnumrec; }
            set { wsnumrec = value; }
        }

        private static string wsconselho;
        public static string wsConselho
        {
            get { return wsconselho; }
            set { wsconselho = value; }
        }

        private static string doctocliente;
        public static string doctoCliente
        {
            get { return doctocliente; }
            set { doctocliente = value; }
        }

        private static long vencodpedido;
        public static long venCodPedido
        {
            get { return vencodpedido; }
            set { vencodpedido = value; }
        }

        private static string venbeneficio;
        public static string venBeneficio
        {
            get { return venbeneficio; }
            set { venbeneficio = value; }
        }

        private static string wssenhaconv;
        public static string wsSenhaConv
        {
            get { return wssenhaconv; }
            set { wssenhaconv = value; }
        }

        private static string wsnovasenha;
        public static string wsNovaSenha
        {
            get { return wsnovasenha; }
            set { wsnovasenha = value; }
        }

        private static string wnum_aut_semreceita;
        public static string wNum_Aut_SemReceita
        {
            get { return wnum_aut_semreceita; }
            set { wnum_aut_semreceita = value; }
        }

        private static string wnum_aut_comreceita;
        public static string wNum_Aut_ComReceita
        {
            get { return wnum_aut_comreceita; }
            set { wnum_aut_comreceita = value; }
        }

        private static string wsconweb;
        public static string wsConWeb
        {
            get { return wsconweb; }
            set { wsconweb = value; }
        }

        private static string wsconcodconv;
        public static string wsConCodConv
        {
            get { return wsconcodconv; }
            set { wsconcodconv = value; }
        }

        private static bool wsabreoutratransacao;
        public static bool wsAbreOutraTransacao
        {
            get { return wsabreoutratransacao; }
            set { wsabreoutratransacao = value; }
        }

        private static string wsnumcartao;
        public static string wsNumCartao
        {
            get { return wsnumcartao; }
            set { wsnumcartao = value; }
        }

        private static string numCpfCnpj;
        public static string NumCpfCnpj
        {
            get { return numCpfCnpj; }
            set { numCpfCnpj = value; }
        }

        private static string nomeNP;
        public static string NomeNP
        {
            get { return nomeNP; }
            set { nomeNP = value; }
        }

        private static string numeroSessao;
        public static string NumeroSessao
        {
            get { return numeroSessao; }
            set { numeroSessao = value; }
        }

        private static string codigovendedor;
        public static string codigoVendedor
        {
            get { return codigovendedor; }
            set { codigovendedor = value; }
        }

        #endregion

        //#region VARIAVEIS DE ENTREGA
        //private static string ventr_end;
        //public static string vEntr_End
        //{
        //    get { return vEntr_End; }
        //    set { ventr_end = value; }
        //}

        //private static string ventr_bairro;
        //public static string vEntr_Bairro
        //{
        //    get { return ventr_bairro; }
        //    set { ventr_bairro = value; }
        //}

        //private static string ventr_cep;
        //public static string vEntr_Cep
        //{
        //    get { return ventr_cep; }
        //    set { ventr_cep = value; }
        //}

        //private static string ventr_cidade;
        //public static string vEntr_Cidade
        //{
        //    get { return ventr_cidade; }
        //    set { ventr_cidade = value; }
        //}

        //private static string ventr_uf;
        //public static string vEntr_UF
        //{
        //    get { return ventr_uf; }
        //    set { ventr_uf = value; }
        //}

        //private static string ventr_fone;
        //public static string vEntr_Fone
        //{
        //    get { return ventr_fone; }
        //    set { ventr_fone = value; }
        //}

        //private static double ventr_valor;
        //public static double vEntr_Valor
        //{
        //    get { return ventr_valor; }
        //    set { ventr_valor = value; }
        //}

        //private static string ventr_obs;
        //public static string vEntr_Obs
        //{
        //    get { return ventr_obs; }
        //    set { ventr_obs = value; }
        //}

        //private static bool ventr_canc;
        //public static bool vEntr_Canc
        //{
        //    get { return ventr_canc; }
        //    set { ventr_canc = value; }
        //}
        //#endregion

        #region METODOS

        public static String[] letras = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",

                                          "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};


        public static String GetColunaExcel(Int32 col)
        {
            String celula = "";

            if (col > 26)
            {
                Int32 idxPrimeiraLetra = Math.Abs(col / 26);

                Int32 idxSegundaLetra = col - (Math.Abs(col / 26) * 26);

                if (idxSegundaLetra == 0)

                    celula = String.Concat(letras[idxPrimeiraLetra - 2], letras[25]);

                else

                    celula = String.Concat(letras[idxPrimeiraLetra - 1], letras[idxSegundaLetra - 1]);

            }
            else
                celula = letras[col - 1];

            return celula;
        }
        #endregion

        #region TVAT
        public static bool ValidouArquivoDeStatus { get; set; }
        public static bool ValidouArquivoDeResposta { get; set; }
        public static bool ValidouConsistenciaArquivos { get; set; }
        #endregion

        #region FARMACIA POPULAR
        private static string solicitacaoID;
        public static string SolicitacaoID
        {
            get { return solicitacaoID; }
            set { solicitacaoID = value; }
        }
        #endregion

        #region IMPRESSORA
        public static bool ReimprimeGerencial { get; set; }
        #endregion

        #region ESTOQUE
        public static string AtualizaImposto { get; set; }
        #endregion
    }
}
