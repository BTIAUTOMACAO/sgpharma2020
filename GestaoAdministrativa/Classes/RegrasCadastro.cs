﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace GestaoAdministrativa.Classes
{
    public static class RegrasCadastro
    {
        static string retorno;

        public static bool ExcluirGrupos(string grupoID)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT GRUPO_ID FROM USUARIOS WHERE GRUPO_ID = " + grupoID, "GRUPO_ID");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) usúario(s) cadastrado(s) com este Grupo! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            if (BancoDados.selecionarRegistros("DELETE FROM PERMISSOES WHERE GRUPO_ID = " + grupoID).Equals(false))
            {
                return false;
            }
            return true;
        }

        public static bool ExcluirEstab(string codEstab)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM CAIXA_ABERTO WHERE EST_CODIGO = " + codEstab, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de caixa cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM MOVIMENTO_CAIXA WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de caixa cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM MOVIMENTO_FINANCEIRO WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) financeiros cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM MOVIMENTO_CAIXA_ESPECIE WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de espécie(s) cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM MOVIMENTO_LOTE WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de lote(s) cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM COBRANCA WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM COBRANCA_MOVIMENTO WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM COBRANCA_PARCELA WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM VENDAS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM VENDAS_ITENS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM VENDAS_ESPECIES WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM VENDAS_FORMA_PAGAMENTO WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM PRODUTOS_DETALHE WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM PRECOS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM ENTRADA WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM ENTRADA_DETALHES WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM ENTRADA_ITENS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM ENTREGAS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrega(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EST_CODIGO FROM ENTREGAS_STATUS WHERE EST_CODIGO = " + codEstab, "EST_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m)  entrega(s) cadastrada(s) nesse Estabelecimento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }


        public static bool ExcluirEmp(string codEmpresa)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM CAIXA_ABERTO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de caixa cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM COLABORADORES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) colaborador(es) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM FORMAS_PAGAMENTO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) forma(s) de pagamento cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM DEPARTAMENTOS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) departamento(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM MOVIMENTO_CAIXA WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de caixa cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM MOVIMENTO_FINANCEIRO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) financeiros cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM MOVIMENTO_CAIXA_ESPECIE WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de espécie(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM MOVIMENTO_LOTE WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimento(s) de lote(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM nat_operacao WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) natureza(s) de operação(ões) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM COBRANCA WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM COBRANCA_MOVIMENTO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM COBRANCA_PARCELA WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cobrança(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM PAGAR WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) pagamento(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) pagamento(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM PAGAR_MOVTO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) pagamento(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM SUBCLASSES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) subclasse(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM TAB_PRECOS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) tabela(s) de preço(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM VENDAS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM VENDAS_ITENS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM VENDAS_ESPECIES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM VENDAS_FORMA_PAGAMENTO WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM PRODUTOS_DETALHE WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM PRECOS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM CLASSES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) classe(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ENTRADA WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ENTRADA_DETALHES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ENTRADA_ITENS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ENTREGAS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrega(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ENTREGAS_STATUS WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m)  entrega(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT EMP_CODIGO FROM ESPECIFICACOES WHERE EMP_CODIGO = " + codEmpresa, "EMP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) especificações(s) cadastrada(s) nessa Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirColab(string codColab)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT PAG_COMPRADOR FROM ENTRADA WHERE EST_CODIGO = " + Principal.estAtual + " AND PAG_COMPRADOR = " 
                + codColab + " AND EMP_CODIGO = " + Principal.empAtual, "PAG_COMPRADOR");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrada(s) com este Colaborador! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT PAG_COMPRADOR FROM PAGAR WHERE EMP_CODIGO = " + Principal.empAtual + " AND PAG_COMPRADOR = " + codColab, "PAG_COMPRADOR");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) título(s) a pagar cadastrado(s) com este Colaborador! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT COBRANCA_COL_CODIGO FROM COBRANCA WHERE EMP_CODIGO = " + Principal.empAtual 
                + " AND COBRANCA_COL_CODIGO = " + codColab + " AND EST_CODIGO = " + Principal.estAtual, "COBRANCA_COL_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) título(s) no Contas a Receber cadastrado(s) com este Colaborador! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT VENDA_COL_CODIGO FROM VENDAS WHERE EMP_CODIGO = " + Principal.empAtual
                + " AND VENDA_COL_CODIGO = " + codColab + " AND EST_CODIGO = " + Principal.estAtual, "VENDA_COL_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) venda(s) cadastrado(s) com este Colaborador! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirOperDC(string codOperDC)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT MOVIMENTO_CAIXA_ID FROM MOVIMENTO_CAIXA WHERE EST_CODIGO = " + Principal.estAtual + " AND MOVIMENTO_CAIXA_ODC_ID = "
                + codOperDC + " AND EMP_CODIGO = " + Principal.empAtual, "MOVIMENTO_CAIXA_ID");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) movimentos(s) cadastrada(s) com esta Operação! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirEspecie(string codEspecie)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT MOVIMENTO_CX_ESPECIE_CODIGO FROM MOVIMENTO_CAIXA_ESPECIE WHERE EST_CODIGO = " + Principal.estAtual 
                + " AND MOVIMENTO_CX_ESPECIE_CODIGO = " + codEspecie + " AND EMP_CODIGO = " + Principal.empAtual, "MOVIMENTO_CX_ESPECIE_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) lancamento(s) para esta Espécie! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = BancoDados.selecionarRegistro("SELECT ESP_CODIGO FROM VENDAS_ESPECIES WHERE EST_CODIGO = " + Principal.estAtual
               + " AND ESP_CODIGO = " + codEspecie + " AND EMP_CODIGO = " + Principal.empAtual, "ESP_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) lancamento(s) para esta Espécie! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirDepartamentos(string codDepto)
        {
            string sql = "SELECT DEP_CODIGO FROM  PRODUTOS_DETALHE WHERE ROWNUM <= 2 AND EST_CODIGO = " + Principal.estAtual + " AND DEP_CODIGO = " + Math.Truncate(Convert.ToDouble(codDepto));
            DataTable dtprodutos = BancoDados.GetDataTable(sql, null);

            if (dtprodutos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com este Departamento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirClasses(string codClasse)
        {

            string sql = "SELECT CLAS_CODIGO FROM PRODUTOS_DETALHE WHERE ROWNUM <= 1 and EST_CODIGO = " + Principal.estAtual + " AND CLAS_CODIGO = " + codClasse;

            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com esta Classe! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirSubclasse(string codSubclasse)
        {

            string sql = "SELECT SUB_CODIGO FROM PRODUTOS_DETALHE WHERE ROWNUM <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND SUB_CODIGO = " + codSubclasse + " AND EMP_CODIGO = " + Principal.empAtual;
            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com esta Subclasse! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirFabricantes(string codFabricante)
        {

            string sql = "SELECT PROD_CODIGO FROM PRODUTOS_DETALHE WHERE ROWNUM <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND EMP_CODIGO = " + Principal.empAtual + " AND FAB_CODIGO = " + codFabricante;
            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com este Fabricante!", "Exclusão", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirPrinAtivo(string codPrinAtivo)
        {
            string sql = "SELECT PRI_CODIGO FROM PRODUTOS WHERE PRI_CODIGO = " + codPrinAtivo;

            DataTable dtprodutos = BancoDados.GetDataTable(sql, null);
            if (dtprodutos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com este principio ativo! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirTabPrecos(string codTabPrecos)
        {
            string sql = "SELECT TAB_CODIGO FROM PRECOS WHERE ROWNUM <= 2  AND EST_CODIGO = " + Principal.estAtual + " AND TAB_CODIGO = " + Math.Truncate(Convert.ToDouble(codTabPrecos));

            DataTable tabPrecos = BancoDados.GetDataTable(sql, null);
            if (tabPrecos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com este Tabela de preço! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirNatOper(string codNatOper)
        {
            string sql = "SELECT NO_CODIGO FROM ENTRADA WHERE ROWNUM <= 2  AND EST_CODIGO = " + Principal.estAtual + " AND NO_CODIGO = '" + codNatOper + "'";

            DataTable tabPrecos = BancoDados.GetDataTable(sql, null);
            if (tabPrecos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) entrada(s) com essa natureza de operação fiscal! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirTipoDocto(string codTipoDocto)
        {
            string sql = "";
            sql = "SELECT CF_DOCTO FROM ENTRADA WHERE  TIP_CODIGO = " + codTipoDocto + " AND ROWNUM <= 1";
            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) entrada(s) cadastrado(s) com este Tipo de Documento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
            {
                sql = "SELECT CF_DOCTO FROM PAGAR WHERE  TIP_CODIGO = " + codTipoDocto + " AND ROWNUM <= 1";
                if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                {

                    MessageBox.Show("Existe(m) título(s) a pagar cadastrado(s) com este Tipo de Documento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

        public static bool ExcluirTipoUni(string codTipoUni)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT ENT_UNIDADE FROM ENTRADA_ITENS WHERE EST_CODIGO = " + Principal.estAtual + " AND ENT_UNIDADE = '" + codTipoUni + "' AND EMP_CODIGO = " + Principal.empAtual, "ENT_UNIDADE");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) com este Tipo de Unidade! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT PROD_UNIDADE FROM PRODUTOS WHERE PROD_UNIDADE = '" + codTipoUni + "'", "PROD_UNIDADE");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) com este Tipo de Unidade! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT PROD_UN_COMP FROM PRODUTOS_DETALHE WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_UN_COMP = '" + codTipoUni + "' AND EMP_CODIGO = " + Principal.empAtual, "PROD_UN_COMP");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) produto(s) cadastrado(s) com este Tipo de Unidade! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirProdutos(string codProduto)
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgTitulo = "Exlusão";
            Principal.msgIcone = MessageBoxIcon.Exclamation;
            string sql = "";


            sql = "SELECT PROD_CODIGO FROM AJUSTES_ESTOQUE WHERE ROWNUM <= 1 AND  EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) lançamento(s) de estoque(s) cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = "SELECT PROD_CODIGO FROM COMPRAS_ITENS WHERE ROWNUM <= 1  AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) itens de compras cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM ENTRADA_ITENS WHERE ROWNUM <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) itens de entrada(s) cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM MOV_ESTOQUE WHERE ROWNUM <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) lançamento(s) de estoque(s) cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM SNGPC_ENTRADAS WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) entrada(s) SNGPC cadastrada(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM SNGPC_PERDA WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) lançamento(s) de perda(s) SNGPC cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM SNGPC_VENDAS WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) venda(s) SNGPC cadastrada(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }


            sql = " SELECT LAC_INV_CODIGO FROM SNGPC_LAC_INVENTARIO WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND LAC_INV_CODIGO= '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) lançamento(s) de inventário(s) SNGPC cadastrado(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }

            sql = " SELECT PROD_CODIGO FROM VENDAS_ITENS WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND EMP_CODIGO = " + Principal.empAtual 
                + " AND PROD_CODIGO = '" + codProduto + "'";
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                Principal.mensagem = "Existe(m) vendas(s) cadastrada(s) com este Produto! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }
            return true;
        }

        public static bool ExcluirFornecedores(string codFornecedor, string cnpjFornecedor)
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgTitulo = "Exlusão";
            Principal.msgIcone = MessageBoxIcon.Exclamation;

            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT CF_DOCTO FROM ENTRADA WHERE EST_CODIGO = " + Principal.estAtual + " AND CF_DOCTO = '" + cnpjFornecedor + "' AND EMP_CODIGO = " + Principal.empAtual, "CF_DOCTO");
            if (!String.IsNullOrEmpty(retorno))
            {
                Principal.mensagem = "Existe(m) entrada(s) cadastrada(s) para este Fornecedor! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT CF_DOCTO FROM PAGAR WHERE  CF_DOCTO = '" + cnpjFornecedor + "' AND EMP_CODIGO = " + Principal.empAtual, "CF_DOCTO");
            if (!String.IsNullOrEmpty(retorno))
            {
                Principal.mensagem = "Existe(m) pagamento(s) cadastrado(s) para este Fornecedor! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT COBRANCA_CF_ID FROM COBRANCA WHERE EST_CODIGO = " + Principal.estAtual + " AND COBRANCA_CF_ID = " + codFornecedor + " AND EMP_CODIGO = " + Principal.empAtual, "COBRANCA_CF_ID");
            if (!String.IsNullOrEmpty(retorno))
            {
                Principal.mensagem = "Existe(m)  título(s) a pagar cadastrado(s) para este Fornecedor! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT VENDA_CF_ID FROM VENDAS WHERE EST_CODIGO = " + Principal.estAtual + " AND VENDA_CF_ID = " + codFornecedor + " AND EMP_CODIGO = " + Principal.empAtual, "VENDA_CF_ID");
            if (!String.IsNullOrEmpty(retorno))
            {
                Principal.mensagem = "Existe(m) vendas cadastrada(s) para este Fornecedor! Exclusão não permitida!";
                Funcoes.Avisa();
                return false;
            }
            return true;
        }

        public static bool ExcluirGrupoClientes(string codCGrupo)
        {
            retorno = string.Empty;
            retorno = BancoDados.selecionarRegistro("SELECT GRU_CODIGO FROM CLIFOR WHERE GRU_CODIGO = " + codCGrupo, "GRU_CODIGO");
            if (!String.IsNullOrEmpty(retorno))
            {
                MessageBox.Show("Existe(m) cliente(s) cadastrado(s) com este Grupo! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirClientes(string id)
        {
            string sql = " SELECT VENDA_CF_ID FROM VENDAS WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND VENDA_CF_ID= " + id + " AND EMP_CODIGO = " + Principal.empAtual;
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                MessageBox.Show("Existe(m) vendas(s) cadastrada(s) com este Cliente! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            sql = " SELECT COBRANCA_CF_ID FROM COBRANCA WHERE ROWNUM  <= 1 AND EST_CODIGO = " + Principal.estAtual + " AND COBRANCA_CF_ID = " + id + " AND EMP_CODIGO = " + Principal.empAtual;
            if (BancoDados.ExecuteScalar(sql, null) != null)
            {
                MessageBox.Show("Existe(m) contas(s) a receber com este Cliente! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            
            return true;

        }

        public static bool ExcluirConveniadas(string codConveniada)
        {

            string sql = "SELECT CF_NOME, CON_CODIGO  FROM CLIFOR WHERE ROWNUM <= 1 and CON_CODIGO = " + codConveniada;

            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) cliente(s) cadastrado(s) com esta Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
                return true;
        }

        public static bool ExcluirCondPagto(string codCondPagto)
        {
            string sql = "SELECT VENDA_FORMA_ID FROM  VENDAS_FORMA_PAGAMENTO WHERE ROWNUM <= 1 AND VENDA_FORMA_ID = " + Math.Truncate(Convert.ToDouble(codCondPagto));
            DataTable dtprodutos = BancoDados.GetDataTable(sql, null);

            if (dtprodutos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) venda(s) cadastrada(s) com esta Forma de Pagamento! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        public static bool ExcluirEspecificacoes(string codEspecificacao)
        {

            string sql = "SELECT ESP_CODIGO FROM  PRODUTOS WHERE ROWNUM <= 2 AND ESP_CODIGO = " + Math.Truncate(Convert.ToDouble(codEspecificacao));
            DataTable dtprodutos = BancoDados.GetDataTable(sql, null);

            if (dtprodutos.Rows.Count != 0)
            {
                MessageBox.Show("Existe(m) produtos(s) cadastrado(s) com este Especificações! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;

        }

        //public static bool ExcluirMovimentoCaixa(int MovimentoID)
        //{

        //    string sql = "select movimento_financeiro_id from movimento_caixa  where ROWNUM <= 2 AND movimento_caixa_id= " + MovimentoID;
        //    DataTable dt = BancoDados.GetDataTable(sql, null);
        //    if ((dt.Rows[0]["movimento_financeiro_id"].ToString()).Equals("0"))
        //    {
        //        return true;

        //    }
        //    else {
        //        MessageBox.Show("Existe(m) cliente(s) cadastrado(s) com esta Empresa! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //        return false;
        //    }
               
        //}

        public static bool ExcluirMedico(string codMedico)
        {

            string sql = "SELECT VEN_PRESC_NUMERO FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_PRESC_NUMERO = '" + codMedico + "'";

            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) vendas(s) SNGPC cadastrada(s) com este Médico! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            sql = "SELECT VENDA_MEDICO FROM VENDAS WHERE ROWNUM <= 1 AND VENDA_MEDICO = '" + codMedico + "'";

            if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
            {
                MessageBox.Show("Existe(m) vendas(s) cadastrada(s) com este Médico! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        public static bool ExcluirCadDespesas(string codDespesa)
        {
            //retorno = BancoDados.selecionarRegistro("SELECT DESP_CODIGO FROM DESPESAS WHERE EST_CODIGO = " + Principal.estAtual + " AND DESP_CODIGO = " + codDespesa, "DESP_CODIGO");
            //if (!String.IsNullOrEmpty(retorno))
            //{
            //    MessageBox.Show("Existe(m) despesa(s) cadastrado(s) com este Tipo de Despesa! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}
            return true;
        }


        public static bool ExcluirCadTitulos(string codTitulo)
        {
            //retorno = BancoDados.selecionarRegistro("SELECT DESP_CODIGO FROM DESPESAS WHERE EST_CODIGO = " + Principal.estAtual + " AND DESP_CODIGO = " + codDespesa, "DESP_CODIGO");
            //if (!String.IsNullOrEmpty(retorno))
            //{
            //    MessageBox.Show("Existe(m) despesa(s) cadastrado(s) com este Tipo de Despesa! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return false;
            //}
            return true;
        }


        public static bool ExcluirDespesas(int empCodigo, string id)
        {
            bool quitou = false;
            Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM PAGAR_DETALHE WHERE EMP_CODIGO = " + Principal.estAtual + " AND PAG_CODIGO = " + id);
            for (int i = 0; i < Principal.dtPesq.Rows.Count; i++)
            {
                if (Principal.dtPesq.Rows[i]["PD_STATUS"].ToString() == "S" || Principal.dtPesq.Rows[i]["PD_STATUS"].ToString() == "P")
                {
                    quitou = true;
                }
                else
                    quitou = false;
            }
            if (quitou == true)
            {
                MessageBox.Show("Despesa já baixada no sistema! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }
        

        public static bool ExcluirTabSngpc(string codTabela, string tipo)
        {
            retorno = string.Empty;

            string sql = string.Empty;
            switch (tipo)
            {
                case "1":
                    sql = " SELECT VEN_TP_RECEITA FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND   VEN_TP_RECEITA = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Receita! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "2":
                    sql = "SELECT VEN_USO FROM SNGPC_VENDAS WHERE  ROWNUM <= 1 AND VEN_USO = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Uso de Medicamento! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "3":
                    sql = "SELECT ENT_TIPO_OPER FROM SNGPC_ENTRADAS WHERE  ROWNUM <= 1 AND ENT_TIPO_OPER = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) entrada(s) cadastrada(s) com este Tipo de Operação! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "4":
                    sql = "SELECT VEN_PRESC_CP FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_PRESC_CP = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Documento! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "5":
                    sql = "SELECT VEN_COMP_UF FROM SNGPC_VENDAS  WHERE ROWNUM <= 1 AND VEN_COMP_UF = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com esta UF! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    sql = " SELECT VEN_PRESC_UF FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_PRESC_UF = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com esta UF! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "6":
                    sql = " SELECT PER_MOTIVO FROM SNGPC_PERDA WHERE ROWNUM <= 1 AND PER_MOTIVO = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) perda(s) cadastrada(s) com este Tipo de Perda! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "7":
                    break;
                case "8":
                    break;
                case "9":
                    sql = " SELECT VEN_COMP_TD FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_COMP_TD = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Documento! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "10":
                    sql = " SELECT VEN_COMP_OE FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_COMP_OE = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Orgão Expedidor! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "11":

                    sql = " SELECT VEN_PAC_SEXO FROM SNGPC_VENDAS  WHERE ROWNUM <= 1  AND VEN_PAC_SEXO = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Sexo! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
                case "12":
                    sql = " SELECT VEN_PAC_UNIDADE FROM SNGPC_VENDAS WHERE ROWNUM <= 1 AND VEN_PAC_UNIDADE = " + codTabela;
                    if (!String.IsNullOrEmpty(BancoDados.ExecuteScalar(sql, null).ToString()))
                    {
                        MessageBox.Show("Existe(m) venda(s) cadastrada(s) com este Tipo de Unidade de Idade! Exclusão não permitida", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }
                    break;
            }
            return true;
        }
    }
}
