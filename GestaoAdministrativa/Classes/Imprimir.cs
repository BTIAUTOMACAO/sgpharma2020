﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using GestaoAdministrativa.Ecf;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Classes
{
    public class Imprimir
    {
        private static IModeloImpressora modeloImpressora;

        public static bool VerificaModeloImpressoraFiscal()
        {
            string modImpressora = Funcoes.LeParametro(6, "08", true, Funcoes.LeParametro(6, "123", true).Equals("N") ? "GERAL" : Principal.nomeEstacao).ToUpper();

            if (String.IsNullOrEmpty(modImpressora))
            {
                return false;
            }

            switch (modImpressora)
            {
                case "DARUMA":
                    modeloImpressora = new Daruma();
                    break;
                case "BEMATECH":
                    modeloImpressora = new Bematech();
                    break;
                case "ELGIN":
                    modeloImpressora = new Elgin();
                    break;
                case "EPSON":
                    modeloImpressora = new Epson();
                    break;
            }
            return true;
        }

        public static bool ImpFiscalUltCupom()
        {
            return modeloImpressora.ImpFiscalUltCupom();
        }

        public static bool ImpFiscalReducaoZ()
        {
            return modeloImpressora.ImpFiscalReducaoZ();
        }

        public static bool ImpFiscalLeituraX()
        {
            return modeloImpressora.ImpFiscalLeituraX();
        }

        public static bool ImpFiscalEstado()
        {
            return modeloImpressora.ImpFiscalEstado();
        }

        public static bool ImpFiscalAbreCupom(string nDocto = "", string nome = "", string endereco = "")
        {
            return modeloImpressora.ImpFiscalAbreCupom(nDocto, nome, endereco);
        }

        public static bool ImpFiscalVenderItem(List<VendaItem> Itens)
        {
            return modeloImpressora.ImpFiscalVenderItem(Itens);
        }

        public static bool ImpFiscalTotalizarCupom(string tipo, string desconto)
        {
            return modeloImpressora.ImpFiscalTotalizarCupom(tipo,desconto);
        }

        public static bool ImpFiscalMeioPagamento(List<VendasEspecies> VendaEspecies)
        {
            return modeloImpressora.ImpFiscalMeioPagamento(VendaEspecies);
        }

        public static bool ImpFiscalFechaCupom()
        {
            return modeloImpressora.ImpFiscalFechaCupom();
        }

        public static bool ImpFiscalFechaCupomMsg(string msgPromocional)
        {
            return modeloImpressora.ImpFiscalFechaCupomMsg(msgPromocional);
        }

        public static bool ImpFiscalAbrirVinculado(string formaPgto, string parcelas, string docOrigem, string valor)
        {
            return modeloImpressora.ImpFiscalAbrirVinculado(formaPgto, parcelas, docOrigem, valor);
        }

        public static string ImpFiscalInformacoesECF(string indice, string informacao)
        {
            return modeloImpressora.ImpFiscalInformacoesECF(indice, informacao);
        }

        public static string ImpFiscalNumeroCupom()
        {
            return modeloImpressora.ImpFiscalNumeroCupom();
        }

        public static bool ImpFiscalTextoVinculado(string mensagem)
        {
            return modeloImpressora.ImpFiscalTextoVinculado(mensagem);
        }

        public static bool ImpFiscalFechaVinculado()
        {
            return modeloImpressora.ImpFiscalFechaVinculado();
        }

        public static bool ImpFiscalAbrirGerencial(string mensagem)
        {
            return modeloImpressora.ImpFiscalAbreGerencial(mensagem);
        }

        public static bool ImpFiscalFechaGerencial()
        {
            return modeloImpressora.ImpFiscalFechaGerencial();
        }

        
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFO
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;

        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFO docInfo);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        public static bool ImprimeComprovante(string NomeImpressora, IntPtr pBytes, Int32 dwCount)
        {
            int NumError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFO docInfo = new DOCINFO();
            bool bSuccesso = false;

            docInfo.pDocName = "Comprovante";
            docInfo.pOutputFile = "";


            //ABRE IMPRESSORA//
            if (OpenPrinter(NomeImpressora.Normalize(), out hPrinter, IntPtr.Zero))
            {
                //INICIALIZA DOCUMENTO//
                if (StartDocPrinter(hPrinter, 1, docInfo))
                {
                    if (StartPagePrinter(hPrinter))
                    {
                        bSuccesso = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }


            //CASO TENHA ERRO NA IMPRESSAO//
            if (bSuccesso == false)
            {
                NumError = Marshal.GetLastWin32Error();
                MessageBox.Show("Erro ao relizar impressão. N° " + NumError, "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return bSuccesso;
        }


        public static bool Comprovante(string NomeImpressora, string Comprovante)
        {
            IntPtr pBytes;
            Int32 dwCount;

            dwCount = Comprovante.Length;
            pBytes = Marshal.StringToCoTaskMemAnsi(Comprovante);
            if (ImprimeComprovante(NomeImpressora, pBytes, dwCount).Equals(false))
            {
                Marshal.FreeCoTaskMem(pBytes);
                return false;
            }
            else
            {
                Marshal.FreeCoTaskMem(pBytes);
                return true;
            }
        }

    }
}
