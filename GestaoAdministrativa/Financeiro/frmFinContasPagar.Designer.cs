﻿namespace GestaoAdministrativa.Financeiro
{
    partial class frmFinContasPagar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFinContasPagar));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rdbWeb = new System.Windows.Forms.RadioButton();
            this.rdbOTodas = new System.Windows.Forms.RadioButton();
            this.rdbDespesas = new System.Windows.Forms.RadioButton();
            this.rdbEntrada = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rdbTodas = new System.Windows.Forms.RadioButton();
            this.rdbQuitada = new System.Windows.Forms.RadioButton();
            this.rdbAberto = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtForn = new System.Windows.Forms.TextBox();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDFinal = new System.Windows.Forms.MaskedTextBox();
            this.txtDInicial = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgContas = new System.Windows.Forms.DataGridView();
            this.PAG_ORIGEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_VENCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_PARCELA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContas)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnQuitar);
            this.panel2.Controls.Add(this.btnCancelar);
            this.panel2.Controls.Add(this.btnExcluir);
            this.panel2.Controls.Add(this.btnIncluir);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.dgContas);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 490);
            this.panel2.TabIndex = 43;
            // 
            // btnQuitar
            // 
            this.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuitar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitar.Image = ((System.Drawing.Image)(resources.GetObject("btnQuitar.Image")));
            this.btnQuitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuitar.Location = new System.Drawing.Point(726, 270);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(225, 57);
            this.btnQuitar.TabIndex = 6;
            this.btnQuitar.Text = "Quitar (F12)";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(489, 270);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(225, 57);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "     Canc. Quitação (F11) ";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluir.Image")));
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(246, 270);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(225, 57);
            this.btnExcluir.TabIndex = 4;
            this.btnExcluir.Text = "        Excluir Conta (F10) ";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIncluir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluir.Image")));
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(6, 270);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(225, 57);
            this.btnIncluir.TabIndex = 2;
            this.btnIncluir.Text = "      Incluir Despesas (F9)";
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblTotal);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnExportar);
            this.groupBox2.Controls.Add(this.btnPesquisar);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 330);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 153);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pesquisa";
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Red;
            this.lblTotal.Location = new System.Drawing.Point(762, 119);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(174, 28);
            this.lblTotal.TabIndex = 18;
            this.lblTotal.Text = "0,00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(813, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Total Selecionado";
            // 
            // btnExportar
            // 
            this.btnExportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Image = ((System.Drawing.Image)(resources.GetObject("btnExportar.Image")));
            this.btnExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportar.Location = new System.Drawing.Point(759, 63);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(180, 41);
            this.btnExportar.TabIndex = 16;
            this.btnExportar.Text = "      Exportar .xls (F2)";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(759, 21);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(180, 41);
            this.btnPesquisar.TabIndex = 15;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rdbWeb);
            this.groupBox6.Controls.Add(this.rdbOTodas);
            this.groupBox6.Controls.Add(this.rdbDespesas);
            this.groupBox6.Controls.Add(this.rdbEntrada);
            this.groupBox6.ForeColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(188, 82);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(268, 65);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Origem do Lançamento";
            // 
            // rdbWeb
            // 
            this.rdbWeb.AutoSize = true;
            this.rdbWeb.ForeColor = System.Drawing.Color.Navy;
            this.rdbWeb.Location = new System.Drawing.Point(199, 20);
            this.rdbWeb.Name = "rdbWeb";
            this.rdbWeb.Size = new System.Drawing.Size(56, 20);
            this.rdbWeb.TabIndex = 6;
            this.rdbWeb.Text = "WEB";
            this.rdbWeb.UseVisualStyleBackColor = true;
            // 
            // rdbOTodas
            // 
            this.rdbOTodas.AutoSize = true;
            this.rdbOTodas.Checked = true;
            this.rdbOTodas.ForeColor = System.Drawing.Color.Navy;
            this.rdbOTodas.Location = new System.Drawing.Point(199, 39);
            this.rdbOTodas.Name = "rdbOTodas";
            this.rdbOTodas.Size = new System.Drawing.Size(63, 20);
            this.rdbOTodas.TabIndex = 5;
            this.rdbOTodas.TabStop = true;
            this.rdbOTodas.Text = "Todas";
            this.rdbOTodas.UseVisualStyleBackColor = true;
            this.rdbOTodas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbOTodas_KeyDown);
            // 
            // rdbDespesas
            // 
            this.rdbDespesas.AutoSize = true;
            this.rdbDespesas.ForeColor = System.Drawing.Color.Navy;
            this.rdbDespesas.Location = new System.Drawing.Point(6, 39);
            this.rdbDespesas.Name = "rdbDespesas";
            this.rdbDespesas.Size = new System.Drawing.Size(188, 20);
            this.rdbDespesas.TabIndex = 4;
            this.rdbDespesas.Text = "Lançamento de Despesas";
            this.rdbDespesas.UseVisualStyleBackColor = true;
            this.rdbDespesas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbDespesas_KeyDown);
            // 
            // rdbEntrada
            // 
            this.rdbEntrada.AutoSize = true;
            this.rdbEntrada.ForeColor = System.Drawing.Color.Navy;
            this.rdbEntrada.Location = new System.Drawing.Point(6, 20);
            this.rdbEntrada.Name = "rdbEntrada";
            this.rdbEntrada.Size = new System.Drawing.Size(134, 20);
            this.rdbEntrada.TabIndex = 3;
            this.rdbEntrada.Text = "Entrada de Notas";
            this.rdbEntrada.UseVisualStyleBackColor = true;
            this.rdbEntrada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbEntrada_KeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rdbTodas);
            this.groupBox5.Controls.Add(this.rdbQuitada);
            this.groupBox5.Controls.Add(this.rdbAberto);
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(7, 82);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(175, 65);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Listar Contas";
            // 
            // rdbTodas
            // 
            this.rdbTodas.AutoSize = true;
            this.rdbTodas.ForeColor = System.Drawing.Color.Navy;
            this.rdbTodas.Location = new System.Drawing.Point(107, 20);
            this.rdbTodas.Name = "rdbTodas";
            this.rdbTodas.Size = new System.Drawing.Size(63, 20);
            this.rdbTodas.TabIndex = 2;
            this.rdbTodas.Text = "Todas";
            this.rdbTodas.UseVisualStyleBackColor = true;
            this.rdbTodas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbTodas_KeyDown);
            // 
            // rdbQuitada
            // 
            this.rdbQuitada.AutoSize = true;
            this.rdbQuitada.ForeColor = System.Drawing.Color.Navy;
            this.rdbQuitada.Location = new System.Drawing.Point(10, 41);
            this.rdbQuitada.Name = "rdbQuitada";
            this.rdbQuitada.Size = new System.Drawing.Size(82, 20);
            this.rdbQuitada.TabIndex = 1;
            this.rdbQuitada.Text = "Quitadas";
            this.rdbQuitada.UseVisualStyleBackColor = true;
            this.rdbQuitada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbQuitada_KeyDown);
            // 
            // rdbAberto
            // 
            this.rdbAberto.AutoSize = true;
            this.rdbAberto.Checked = true;
            this.rdbAberto.ForeColor = System.Drawing.Color.Navy;
            this.rdbAberto.Location = new System.Drawing.Point(10, 20);
            this.rdbAberto.Name = "rdbAberto";
            this.rdbAberto.Size = new System.Drawing.Size(91, 20);
            this.rdbAberto.TabIndex = 0;
            this.rdbAberto.TabStop = true;
            this.rdbAberto.Text = "Em aberto";
            this.rdbAberto.UseVisualStyleBackColor = true;
            this.rdbAberto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAberto_KeyDown);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtForn);
            this.groupBox4.Controls.Add(this.txtDocto);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(267, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(489, 65);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dados da Conta";
            // 
            // txtForn
            // 
            this.txtForn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtForn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtForn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForn.Location = new System.Drawing.Point(144, 37);
            this.txtForn.MaxLength = 60;
            this.txtForn.Name = "txtForn";
            this.txtForn.Size = new System.Drawing.Size(330, 22);
            this.txtForn.TabIndex = 4;
            this.txtForn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtForn_KeyDown);
            this.txtForn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtForn_KeyPress);
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(16, 37);
            this.txtDocto.MaxLength = 15;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(111, 22);
            this.txtDocto.TabIndex = 3;
            this.txtDocto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocto_KeyDown);
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(141, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nome do Fornecedor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(13, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "N° Docto.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDFinal);
            this.groupBox3.Controls.Add(this.txtDInicial);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(7, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(251, 65);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Vencimento";
            // 
            // txtDFinal
            // 
            this.txtDFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDFinal.Location = new System.Drawing.Point(137, 37);
            this.txtDFinal.Mask = "00/00/0000";
            this.txtDFinal.Name = "txtDFinal";
            this.txtDFinal.Size = new System.Drawing.Size(100, 22);
            this.txtDFinal.TabIndex = 3;
            this.txtDFinal.ValidatingType = typeof(System.DateTime);
            this.txtDFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDFinal_KeyDown);
            this.txtDFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDFinal_KeyPress);
            // 
            // txtDInicial
            // 
            this.txtDInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDInicial.Location = new System.Drawing.Point(16, 37);
            this.txtDInicial.Mask = "00/00/0000";
            this.txtDInicial.Name = "txtDInicial";
            this.txtDInicial.Size = new System.Drawing.Size(100, 22);
            this.txtDInicial.TabIndex = 2;
            this.txtDInicial.ValidatingType = typeof(System.DateTime);
            this.txtDInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDInicial_KeyDown);
            this.txtDInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDInicial_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(134, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Até";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "De";
            // 
            // dgContas
            // 
            this.dgContas.AllowUserToAddRows = false;
            this.dgContas.AllowUserToDeleteRows = false;
            this.dgContas.AllowUserToResizeColumns = false;
            this.dgContas.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgContas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgContas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgContas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgContas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAG_ORIGEM,
            this.PAG_STATUS,
            this.PD_VENCTO,
            this.PAG_DOCTO,
            this.PD_PARCELA,
            this.PD_VALOR,
            this.PD_SALDO,
            this.PAG_DATA,
            this.CF_DOCTO,
            this.CF_NOME,
            this.CF_ID,
            this.PAG_CODIGO,
            this.PD_STATUS,
            this.ID});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgContas.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgContas.GridColor = System.Drawing.Color.LightGray;
            this.dgContas.Location = new System.Drawing.Point(6, 6);
            this.dgContas.Name = "dgContas";
            this.dgContas.ReadOnly = true;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContas.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgContas.RowHeadersWidth = 30;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContas.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgContas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgContas.Size = new System.Drawing.Size(945, 258);
            this.dgContas.TabIndex = 0;
            this.dgContas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContas_CellClick);
            this.dgContas.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgContas_RowPrePaint);
            this.dgContas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgContas_KeyDown);
            // 
            // PAG_ORIGEM
            // 
            this.PAG_ORIGEM.DataPropertyName = "PAG_ORIGEM";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PAG_ORIGEM.DefaultCellStyle = dataGridViewCellStyle15;
            this.PAG_ORIGEM.HeaderText = "Origem";
            this.PAG_ORIGEM.MaxInputLength = 1;
            this.PAG_ORIGEM.Name = "PAG_ORIGEM";
            this.PAG_ORIGEM.ReadOnly = true;
            this.PAG_ORIGEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PAG_ORIGEM.Width = 65;
            // 
            // PAG_STATUS
            // 
            this.PAG_STATUS.DataPropertyName = "PAG_STATUS";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PAG_STATUS.DefaultCellStyle = dataGridViewCellStyle16;
            this.PAG_STATUS.HeaderText = "Status";
            this.PAG_STATUS.MaxInputLength = 1;
            this.PAG_STATUS.Name = "PAG_STATUS";
            this.PAG_STATUS.ReadOnly = true;
            this.PAG_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PAG_STATUS.Width = 60;
            // 
            // PD_VENCTO
            // 
            this.PD_VENCTO.DataPropertyName = "PD_VENCTO";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PD_VENCTO.DefaultCellStyle = dataGridViewCellStyle17;
            this.PD_VENCTO.HeaderText = "Vencimento";
            this.PD_VENCTO.MaxInputLength = 10;
            this.PD_VENCTO.Name = "PD_VENCTO";
            this.PD_VENCTO.ReadOnly = true;
            this.PD_VENCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PAG_DOCTO
            // 
            this.PAG_DOCTO.DataPropertyName = "PAG_DOCTO";
            this.PAG_DOCTO.HeaderText = "N° Docto.";
            this.PAG_DOCTO.MaxInputLength = 15;
            this.PAG_DOCTO.Name = "PAG_DOCTO";
            this.PAG_DOCTO.ReadOnly = true;
            this.PAG_DOCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PD_PARCELA
            // 
            this.PD_PARCELA.DataPropertyName = "PD_PARCELA";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PD_PARCELA.DefaultCellStyle = dataGridViewCellStyle18;
            this.PD_PARCELA.HeaderText = "Parcela";
            this.PD_PARCELA.MaxInputLength = 18;
            this.PD_PARCELA.Name = "PD_PARCELA";
            this.PD_PARCELA.ReadOnly = true;
            this.PD_PARCELA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PD_PARCELA.Width = 70;
            // 
            // PD_VALOR
            // 
            this.PD_VALOR.DataPropertyName = "PD_VALOR";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.Format = "N2";
            dataGridViewCellStyle19.NullValue = null;
            this.PD_VALOR.DefaultCellStyle = dataGridViewCellStyle19;
            this.PD_VALOR.HeaderText = "Valor Total";
            this.PD_VALOR.MaxInputLength = 18;
            this.PD_VALOR.Name = "PD_VALOR";
            this.PD_VALOR.ReadOnly = true;
            this.PD_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PD_SALDO
            // 
            this.PD_SALDO.DataPropertyName = "PD_SALDO";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.PD_SALDO.DefaultCellStyle = dataGridViewCellStyle20;
            this.PD_SALDO.HeaderText = "Saldo";
            this.PD_SALDO.MaxInputLength = 18;
            this.PD_SALDO.Name = "PD_SALDO";
            this.PD_SALDO.ReadOnly = true;
            this.PD_SALDO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PAG_DATA
            // 
            this.PAG_DATA.DataPropertyName = "PAG_DATA";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.PAG_DATA.DefaultCellStyle = dataGridViewCellStyle21;
            this.PAG_DATA.HeaderText = "Emissão";
            this.PAG_DATA.MaxInputLength = 18;
            this.PAG_DATA.Name = "PAG_DATA";
            this.PAG_DATA.ReadOnly = true;
            this.PAG_DATA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CNPJ";
            this.CF_DOCTO.MaxInputLength = 20;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_DOCTO.Width = 130;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Fornecedor";
            this.CF_NOME.MaxInputLength = 60;
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_NOME.Width = 300;
            // 
            // CF_ID
            // 
            this.CF_ID.DataPropertyName = "CF_ID";
            this.CF_ID.HeaderText = "Forn. Codigo";
            this.CF_ID.MaxInputLength = 18;
            this.CF_ID.Name = "CF_ID";
            this.CF_ID.ReadOnly = true;
            this.CF_ID.Visible = false;
            // 
            // PAG_CODIGO
            // 
            this.PAG_CODIGO.DataPropertyName = "PAG_CODIGO";
            this.PAG_CODIGO.HeaderText = "Pg. Codigo";
            this.PAG_CODIGO.MaxInputLength = 18;
            this.PAG_CODIGO.Name = "PAG_CODIGO";
            this.PAG_CODIGO.ReadOnly = true;
            this.PAG_CODIGO.Visible = false;
            // 
            // PD_STATUS
            // 
            this.PD_STATUS.DataPropertyName = "PD_STATUS";
            this.PD_STATUS.HeaderText = "Pago";
            this.PD_STATUS.Name = "PD_STATUS";
            this.PD_STATUS.ReadOnly = true;
            this.PD_STATUS.Visible = false;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Contas à Pagar";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmFinContasPagar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmFinContasPagar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmFinContasPagar";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmFinContasPagar_Load);
            this.Shown += new System.EventHandler(this.frmFinContasPagar_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgContas;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rdbOTodas;
        private System.Windows.Forms.RadioButton rdbDespesas;
        private System.Windows.Forms.RadioButton rdbEntrada;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rdbTodas;
        private System.Windows.Forms.RadioButton rdbQuitada;
        private System.Windows.Forms.RadioButton rdbAberto;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtForn;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtDFinal;
        private System.Windows.Forms.MaskedTextBox txtDInicial;
        private System.Windows.Forms.RadioButton rdbWeb;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_ORIGEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_VENCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_PARCELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_SALDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}