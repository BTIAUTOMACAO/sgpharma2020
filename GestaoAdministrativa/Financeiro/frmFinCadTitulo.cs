﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Geral;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinCadTitulo : Form, Botoes
    {
        private DataTable dtTitulos = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTitulos = new ToolStripButton("Cad. Títulos");
        private ToolStripSeparator tssTitulos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmFinCadTitulo(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinCadTitulo_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Primeiro()
        {
            if (dtTitulos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTitulos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTitulos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbTitulos.AutoSize = false;
                this.tsbTitulos.Image = Image.FromFile(Application.StartupPath + @"\Imagens\receber.png");
                this.tsbTitulos.Size = new System.Drawing.Size(135, 20);
                this.tsbTitulos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTitulos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTitulos);
                tsbTitulos.Click += delegate
                {
                    var cadDespesas = Application.OpenForms.OfType<frmFinCadDesp>().FirstOrDefault();
                    BotoesHabilitados();
                    cadDespesas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmFinCadTitulo");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
            if (tcTitulos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTitulos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtDespID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtTitulos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTitulos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTitulos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTitulos.Clear();
            dgTitulos.DataSource = dtTitulos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtDespID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTitulos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtDespID.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["TIL_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["TIL_DESCR"].ToString());
                if (dtTitulos.Rows[linha]["TIL_LIBERADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadClasses_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtDespID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do tipo título?", "Exclusão tabela Títulos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirCadTitulos(txtDespID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            if (!DAL.DALTitulo.GetExcluirDados(txtDespID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("TIL_CODIGO", txtDespID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "TITULOS", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtTitulos.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTitulos.DataSource = dtTitulos;
                                tcTitulos.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Limpar();
                txtDespID.Text = Funcoes.GeraSequence("STIL_CODIGO");
                chkLiberado.Checked = true;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDespID.Text.Trim()))
            {
                Principal.mensagem = "Título ID não pode ser branco, precione o botão\nInserir para gerar código do título.";
                Funcoes.Avisa();
                txtDespID.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTitulos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTitulos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTitulos.RowCount;
                    for (i = 0; i <= dgTitulos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTitulos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTitulos.ColumnCount; k++)
                    {
                        if (dgTitulos.Columns[k].Visible == true)
                        {
                            switch (dgTitulos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTitulos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTitulos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTitulos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTitulos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTitulos.ColumnCount : indice) + Convert.ToString(dgTitulos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        public bool Atualiza()
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Titulo cadTil = new Titulo
                    {
                        TilCodigo = int.Parse(txtDespID.Text),
                        TilDescr = txtDescr.Text.Trim().ToUpper(),
                        TilLiberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = int.Parse(Principal.usuario)
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("TITULOS", "TIL_CODIGO", txtDespID.Text);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        if (Util.RegistrosPorEstabelecimento("TITULOS", "TIL_DESCR", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Título já cadastrado.";
                            Funcoes.Avisa();
                            txtDescr.Text = "";
                            txtDescr.Focus();
                            return false;
                        }

                        if (DAL.DALTitulo.GetInserirDados(cadTil).Equals(true))
                        {
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("TIL_CODIGO", txtDespID.Text, Principal.usuario, "TITULOS", txtDescr.Text, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtTitulos.Clear();
                            dgTitulos.DataSource = dtTitulos;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtDescr.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        if (DAL.DALTitulo.GetAtualizaDados(cadTil, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Cad. Título");
                        }

                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTitulos.Clear();
                        dgTitulos.DataSource = dtTitulos;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string liberado = "";
                if (cmbLiberado.Text == "" || cmbLiberado.Text == "TODOS")
                {
                    liberado = "TODOS";
                }
                else
                    if (cmbLiberado.Text == "SIM")
                    {
                        liberado = "S";
                    }
                    else
                        liberado = "N";


                Titulo cadTitulo = new Titulo
                {
                    TilCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    TilDescr = txtBDescr.Text.Trim().ToUpper(),
                    TilLiberado = liberado
                };

                dtTitulos = DAL.DALTitulo.GetBuscar(cadTitulo, out strOrdem);
                if (dtTitulos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTitulos.Rows.Count;
                    dgTitulos.DataSource = dtTitulos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, 0));
                    dgTitulos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTitulos.DataSource = dtTitulos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcTitulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTitulos.SelectedTab == tpGrade)
            {
                dgTitulos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTitulos.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmFinCadTitulo");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                        chkLiberado.Checked = false;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void dgTitulos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
            emGrade = true;
        }

        private void dgTitulos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTitulos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTitulos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTitulos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTitulos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTitulos.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgTitulos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTitulos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtTitulos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTitulos.Columns[e.ColumnIndex].Name + " DESC");
                        dgTitulos.DataSource = dtTitulos;
                        decrescente = true;
                    }
                    else
                    {
                        dtTitulos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTitulos.Columns[e.ColumnIndex].Name + " ASC");
                        dgTitulos.DataSource = dtTitulos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgTitulos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTitulos.Rows[e.RowIndex].Cells[2].Value.ToString() == "N")
            {
                dgTitulos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTitulos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void txtDespID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDescr.Focus();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtDespID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    if (dgTitulos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTitulos.ColumnCount];
                string[] coluna = new string[dgTitulos.ColumnCount];

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    grid[i] = dgTitulos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    if (dgTitulos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTitulos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTitulos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTitulos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTitulos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTitulos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTitulos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTitulos.ColumnCount; i++)
                        {
                            dgTitulos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTitulos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTitulos.SelectedTab = tpFicha;
        }





        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
