﻿using System;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GestaoAdministrativa.Geral;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinTitulos : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private decimal valorTotal;
        private decimal valorParcelas;
        private decimal valorSaldo;
        private decimal valorQuitado;
        private DataTable dtTitulos = new DataTable();
        private int contRegistros;
        public bool emGrade;
        private ToolStripButton tsbTitulos = new ToolStripButton("Lanc. de Títulos");
        private ToolStripSeparator tssTitulos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private bool permiteAtu;
        private int idCliente;
        #endregion

        public frmFinTitulos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinTitulos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dtPVencimento.Value = DateTime.Now;
                dtEmissao.Value = DateTime.Now;
                cmbCBusca.SelectedIndex = 1;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var dados = new Cobranca();
                dtTitulos = dados.BuscaDados(Principal.empAtual, Principal.estAtual, txtBID.Text, Funcoes.RemoveCaracter(txtData.Text), txtBCliente.Text, out strOrdem);
                if (dtTitulos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTitulos.Rows.Count;
                    dgTitulos.DataSource = dtTitulos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, 0));
                    dgTitulos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTitulos.DataSource = dtTitulos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    dgParcelas.Rows.Clear();
                    Funcoes.LimpaFormularios(this);
                    cmbCBusca.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmFinTitulos_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        public bool CarregaCombos()
        {
            try
            {
                //CARREGA TIPO DE DOCUMENTO//
                Principal.dtPesq = Util.CarregarCombosPorEmpresa("COL_NOME", "COL_CODIGO", "COLABORADORES", false, "COL_DESABILITADO = 'N' AND COL_STATUS = 'V'");
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    cmbVendedor.DataSource = Principal.dtPesq;
                    cmbVendedor.DisplayMember = "COL_NOME";
                    cmbVendedor.ValueMember = "COL_CODIGO";
                    cmbVendedor.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Vendedor,\npara lançar um título.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                return true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Título", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void tcTitulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTitulos.SelectedTab == tpGrade)
            {
                dgTitulos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
               if (tcTitulos.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmFinTitulos");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    dgParcelas.AllowUserToDeleteRows = true;
                    permiteAtu = true;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtData.Focus();
                }
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                dgParcelas.Rows.Clear();
                valorTotal = 0;
                valorSaldo = 0;
                valorParcelas = 0;
                permiteAtu = false;

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtReceberID.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["COBRANCA_ID"].ToString());
                dtEmissao.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["COBRANCA_DATA"].ToString());
                txtTValor.Text = String.Format("{0:N}", dtTitulos.Rows[linha]["COBRANCA_TOTAL"]);
                txtDocto.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["COBRANCA_VENDA_ID"].ToString());
                cmbVendedor.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["COL_NOME"].ToString());
                if (dtTitulos.Rows[linha]["CF_DOCTO"].Equals("J"))
                {
                    txtCCnpj.Text = String.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(Funcoes.RemoveCaracter(dtTitulos.Rows[linha]["CF_DOCTO"].ToString())));
                }
                else
                {
                    txtCCnpj.Text = String.Format(@"{0:000\.000\.000\-00}", Convert.ToInt64(Funcoes.RemoveCaracter(dtTitulos.Rows[linha]["CF_DOCTO"].ToString())));
                }
                txtCNome.Text = dtTitulos.Rows[linha]["CF_NOME"].ToString();
                txtCCidade.Text = dtTitulos.Rows[linha]["CF_CIDADE"].ToString() + "/" + dtTitulos.Rows[linha]["CF_UF"].ToString();

                txtData.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTitulos.Rows[linha]["OP_ALTERACAO"].ToString());

                if (dtTitulos.Rows[linha]["COBRANCA_STATUS"].ToString().Equals("F"))
                {
                    dgParcelas.AllowUserToDeleteRows = false;
                    permiteAtu = false;
                }
                else
                {
                    dgParcelas.AllowUserToDeleteRows = true;
                    permiteAtu = true;
                }

                idCliente = Convert.ToInt32(dtTitulos.Rows[linha]["CF_ID"]);
                cmbCBusca.SelectedIndex = 1;

                var dadosParcelas = new CobrancaParcela();
                DataTable dt = dadosParcelas.BuscaCobrancaParcelaPorID(Convert.ToInt32(dtTitulos.Rows[linha]["COBRANCA_ID"]), Principal.empAtual, Principal.estAtual);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { dt.Rows[i]["COBRANCA_PARCELA_VALOR"],
                        Convert.ToDateTime(dt.Rows[i]["COBRANCA_PARCELA_VENCIMENTO"]).ToString("dd/MM/yyyy"),dt.Rows[i]["COBRANCA_PARCELA_SALDO"], dt.Rows[i]["COBRANCA_PARCELA_STATUS"] });

                    valorTotal += Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_VALOR"]);

                    if (Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_SALDO"]) < Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_VALOR"]))
                    {
                        valorSaldo += Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_VALOR"]) - Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_SALDO"]);
                    }

                    if (dt.Rows[i]["COBRANCA_PARCELA_STATUS"].ToString().Equals("F"))
                    {
                        valorQuitado += Convert.ToDecimal(dt.Rows[i]["COBRANCA_PARCELA_VALOR"]);
                        dgParcelas.AllowUserToDeleteRows = false;
                        permiteAtu = false;
                    }
                }

                txtPTotal.Text = "0,00";

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, linha));
                dtEmissao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtTitulos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTitulos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTitulos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTitulos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTitulos.CurrentCell = dgTitulos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbTitulos.AutoSize = false;
                this.tsbTitulos.Image = Properties.Resources.financeiro;
                this.tsbTitulos.Size = new System.Drawing.Size(140, 20);
                this.tsbTitulos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTitulos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTitulos);
                tsbTitulos.Click += delegate
                {
                    var cadTitulos = Application.OpenForms.OfType<frmFinTitulos>().FirstOrDefault();
                    Funcoes.BotoesCadastro(cadTitulos.Name);
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                    if (tcTitulos.SelectedTab == tpGrade)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    }
                    cadTitulos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Funcoes.BotoesCadastro("frmFinTitulos");
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                if (tcTitulos.SelectedTab == tpGrade)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtTitulos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTitulos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTitulos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            #region LIMPAR CAMPOS
            idCliente = 0;
            dtTitulos.Clear();
            dgTitulos.DataSource = dtTitulos;
            cmbCBusca.SelectedIndex = 1;
            cmbVendedor.SelectedIndex = -1;
            dgParcelas.Rows.Clear();
            Funcoes.LimpaFormularios(this);
            txtTValor.Text = "0,00";
            txtPTotal.Text = "0,00";
            dtPVencimento.Value = DateTime.Now;
            contRegistros = 0;
            valorParcelas = 0;
            valorSaldo = 0;
            valorTotal = 0;
            valorQuitado = 0;
            permiteAtu = false;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            #endregion
        }

        public void LimparFicha()
        {
            #region LIMPAR CAMPOS
            txtReceberID.Text = "";
            dtEmissao.Value = DateTime.Now;
            txtDocto.Text = "";
            txtTValor.Text = "0,00";
            cmbCBusca.SelectedIndex = 1;
            cmbVendedor.SelectedIndex = -1;
            txtCBusca.Text = "";
            txtCNome.Text = "";
            txtCCnpj.Text = "";
            txtCCidade.Text = "";
            txtPTotal.Text = "0,00";
            dtPVencimento.Value = DateTime.Now;
            dgParcelas.Rows.Clear();
            txtData.Text = "";
            txtUsuario.Text = "";
            valorParcelas = 0;
            valorSaldo = 0;
            valorTotal = 0;
            valorQuitado = 0;
            permiteAtu = true;
            idCliente = 0;
            dtEmissao.Focus();
            #endregion
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmFinTitulos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
            if (tcTitulos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTitulos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtReceberID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTitulos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void ImprimirRelatorio()
        {
        }

        private void dgTitulos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
            emGrade = true;
        }

        private void dgTitulos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTitulos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTitulos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTitulos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtTitulos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTitulos.Columns[e.ColumnIndex].Name + " DESC");
                        dgTitulos.DataSource = dtTitulos;
                        decrescente = true;
                    }
                    else
                    {
                        dtTitulos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTitulos.Columns[e.ColumnIndex].Name + " ASC");
                        dgTitulos.DataSource = dtTitulos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgTitulos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTitulos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTitulos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTitulos.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgTitulos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        public void AtalhoGrade()
        {
            tcTitulos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTitulos.SelectedTab = tpFicha;
        }

        public bool Excluir()
        {
            try
            {
                if ((txtReceberID.Text.Trim() != "") && (txtDocto.Text.Trim() != ""))
                {
                    if (permiteAtu)
                    {
                        if (MessageBox.Show("Confirma a exclusão do título e suas parcelas?", "Exclusão tabela Receber", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var excluirDados = new Cobranca();
                            if (excluirDados.ExcluiParcelaCobranca(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtReceberID.Text)).Equals(1))
                            {
                                Funcoes.GravaLogExclusao("COBRANCA_ID", txtReceberID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "COBRANCA", txtDocto.Text.ToUpper(), Principal.motivo, Principal.estAtual, Principal.empAtual);
                                dtTitulos.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTitulos.DataSource = dtTitulos;
                                tcTitulos.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                return true;
                            }
                        }
                        else
                            return false;
                    }
                    else
                    {
                        MessageBox.Show("Título consta com pagamento total/parcial, exclusão não liberada!", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";


            if (permiteAtu.Equals(false))
            {
                Principal.mensagem = "Título já possui pagamentos efetuados. Alteração não Permitida!";
                Funcoes.Avisa();
                CarregarDados(contRegistros);
                return false;
            }
            if (Convert.ToDouble(txtTValor.Text) <= 0)
            {
                Principal.mensagem = "Valor não pode ser zero.";
                Funcoes.Avisa();
                txtTValor.Focus();
                return false;
            }
            if (cmbVendedor.SelectedIndex == -1)
            {
                Principal.mensagem = "Selecione um vendedor.";
                Funcoes.Avisa();
                cmbVendedor.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDocto.Text.Trim()))
            {
                Principal.mensagem = "Número do documento não pode ser em branco.";
                Funcoes.Avisa();
                txtDocto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCCnpj.Text))
            {
                Principal.mensagem = "Cliente não pode ser em branco.";
                Funcoes.Avisa();
                txtCBusca.Focus();
                return false;
            }
            if (dgParcelas.RowCount == 0)
            {
                Principal.mensagem = "Parcela(s) não pode(m) ser em branco.";
                Funcoes.Avisa();
                txtPTotal.Focus();
                return false;
            }
            valorParcelas = 0;
            for (int i = 0; i < dgParcelas.RowCount; i++)
            {
                valorParcelas = valorParcelas + Convert.ToDecimal(dgParcelas.Rows[i].Cells[0].Value);
            }
            if (valorParcelas != Convert.ToDecimal(txtTValor.Text))
            {
                Principal.mensagem = "O valor das parcelas não está igual ao valor total.";
                Funcoes.Avisa();
                txtPTotal.Focus();
                return false;
            }
            return true;
        }

        public bool Atualiza()
        {
            try
            {
                int id = 0;
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    if (permiteAtu)
                    {
                        var dadosCobranca = new Cobranca();
                        #region EXCLUI O TITULO E INSERE NOVO REGISTRO
                        if (dadosCobranca.ExcluiParcelaCobranca(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtReceberID.Text)).Equals(1))
                        {
                            if (String.IsNullOrEmpty(dadosCobranca.BuscaTituloCadastrado(txtDocto.Text)))
                            {
                                string formaPagto = dadosCobranca.BuscaFormaPagtoCliente(idCliente);

                                if (!String.IsNullOrEmpty(formaPagto))
                                {
                                    BancoDados.AbrirTrans();
                                    id = Convert.ToInt32(txtReceberID.Text);

                                    dadosCobranca.EmpCodigo = Principal.empAtual;
                                    dadosCobranca.EstCodigo = Principal.estAtual;
                                    dadosCobranca.CobrancaID = id;
                                    dadosCobranca.CobrancaCfID = idCliente;
                                    dadosCobranca.CobrancaVendaID = Convert.ToInt64(txtDocto.Text);
                                    dadosCobranca.CobrancaData = Convert.ToDateTime(dtEmissao.Text);
                                    dadosCobranca.CobrancaTotal = Convert.ToDouble(txtTValor.Text);
                                    dadosCobranca.CobrancaFormaID = Convert.ToInt32(formaPagto);
                                    dadosCobranca.CobrancaColCodigo = Convert.ToInt32(cmbVendedor.SelectedValue);
                                    dadosCobranca.CobrancaOrigem = "T";
                                    dadosCobranca.CobrancaStatus = "A";
                                    dadosCobranca.CobrancaUsuario = Principal.usuario;
                                    dadosCobranca.DtCadastro = DateTime.Now;
                                    dadosCobranca.OpCadastro = Principal.usuario;
                                    dadosCobranca.DtAlteracao = DateTime.Now;
                                    dadosCobranca.OpAlteracao = Principal.usuario;

                                    if (!dadosCobranca.InserirDados(dadosCobranca))
                                        return false;

                                    int idParcela = Funcoes.IdentificaVerificaID("COBRANCA_PARCELA", "COBRANCA_PARCELA_ID", Principal.estAtual, "", Principal.empAtual);
                                    for (int y = 0; y < dgParcelas.RowCount; y++)
                                    {
                                        var dadosParcelas = new CobrancaParcela
                                            (
                                                Principal.empAtual,
                                                Principal.estAtual,
                                                idParcela + y,
                                                id,
                                                y + 1,
                                                Convert.ToDouble(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VALOR"].Value),
                                                Convert.ToDouble(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VALOR"].Value),
                                                "A",
                                                Convert.ToDateTime(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VENCIMENTO"].Value),
                                                DateTime.Now,
                                                DateTime.Now,
                                                Principal.usuario,
                                                DateTime.Now,
                                                Principal.usuario
                                            );

                                        if (!dadosParcelas.InserirDados(dadosParcelas))
                                            return false;

                                    }

                                    //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                                    Funcoes.GravaLogInclusao("COBRANCA_ID", Convert.ToString(id), Principal.usuario, "COBRANCA", txtDocto.Text.Trim().ToUpper(), Principal.estAtual, Principal.empAtual, true);
                                    for (int i = 0; i < dgParcelas.RowCount; i++)
                                    {
                                        Funcoes.GravaLogInclusao("COBRANCA_ID", Convert.ToString(id), Principal.usuario, "COBRANCA_PARCELA", Funcoes.BValor(Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value)),
                                            Principal.estAtual, Principal.empAtual, true);
                                    }

                                    BancoDados.FecharTrans();

                                    MessageBox.Show("Atualização realizada com sucesso!", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                    dtTitulos.Clear();
                                    dgTitulos.DataSource = dtTitulos;
                                    emGrade = false;
                                    tslRegistros.Text = "";
                                    Limpar();
                                    
                                    return true;
                                }
                                else
                                {
                                    MessageBox.Show("Cliente não vinculado em uma empresa. Verifique o cadastro primeiro", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtData.Focus();
                                }

                            }
                            else
                            {
                                MessageBox.Show("Número de Documento já cadastrado", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtDocto.Focus();
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        MessageBox.Show("Título consta com pagamento total/parcial, alteração não permitida", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtEmissao.Focus();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Incluir()
        {
            try
            {
                int id = 0;
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var dadosCobranca = new Cobranca();

                    if (String.IsNullOrEmpty(dadosCobranca.BuscaTituloCadastrado(txtDocto.Text)))
                    {
                        #region INSERE NOVO REGISTRO
                        string formaPagto = dadosCobranca.BuscaFormaPagtoCliente(idCliente);
                        if (!String.IsNullOrEmpty(formaPagto))
                        {
                            BancoDados.AbrirTrans();

                            id = Funcoes.IdentificaVerificaID("COBRANCA", "COBRANCA_ID", Principal.estAtual, "", Principal.empAtual);

                            dadosCobranca.EmpCodigo = Principal.empAtual;
                            dadosCobranca.EstCodigo = Principal.estAtual;
                            dadosCobranca.CobrancaID = id;
                            dadosCobranca.CobrancaCfID = idCliente;
                            dadosCobranca.CobrancaVendaID = Convert.ToInt64(txtDocto.Text);
                            dadosCobranca.CobrancaData = Convert.ToDateTime(dtEmissao.Text);
                            dadosCobranca.CobrancaTotal = Convert.ToDouble(txtTValor.Text);
                            dadosCobranca.CobrancaFormaID = Convert.ToInt32(formaPagto);
                            dadosCobranca.CobrancaColCodigo = Convert.ToInt32(cmbVendedor.SelectedValue);
                            dadosCobranca.CobrancaOrigem = "T";
                            dadosCobranca.CobrancaStatus = "A";
                            dadosCobranca.CobrancaUsuario = Principal.usuario;
                            dadosCobranca.DtCadastro = DateTime.Now;
                            dadosCobranca.OpCadastro = Principal.usuario;
                            dadosCobranca.DtAlteracao = DateTime.Now;
                            dadosCobranca.OpAlteracao = Principal.usuario;

                            if (!dadosCobranca.InserirDados(dadosCobranca))
                                return false;

                            int idParcela = Funcoes.IdentificaVerificaID("COBRANCA_PARCELA", "COBRANCA_PARCELA_ID", Principal.estAtual, "", Principal.empAtual);
                            for (int y = 0; y < dgParcelas.RowCount; y++)
                            {
                                var dadosParcelas = new CobrancaParcela
                                    (
                                        Principal.empAtual,
                                        Principal.estAtual,
                                        idParcela + y,
                                        id,
                                        y + 1,
                                        Convert.ToDouble(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VALOR"].Value),
                                        Convert.ToDouble(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VALOR"].Value),
                                        "A",
                                        Convert.ToDateTime(dgParcelas.Rows[y].Cells["COBRANCA_PARCELA_VENCIMENTO"].Value),
                                        DateTime.Now,
                                        DateTime.Now,
                                        Principal.usuario,
                                        DateTime.Now,
                                        Principal.usuario
                                    );

                                if (!dadosParcelas.InserirDados(dadosParcelas))
                                    return false;

                            }

                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("COBRANCA_ID", Convert.ToString(id), Principal.usuario, "COBRANCA", txtDocto.Text.Trim().ToUpper(), Principal.estAtual, Principal.empAtual, true);
                            for (int i = 0; i < dgParcelas.RowCount; i++)
                            {
                                Funcoes.GravaLogInclusao("COBRANCA_ID", Convert.ToString(id), Principal.usuario, "COBRANCA_PARCELA", Funcoes.BValor(Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value)),
                                    Principal.estAtual, Principal.empAtual, true);
                            }

                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtTitulos.Clear();
                            dgTitulos.DataSource = dtTitulos;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            BancoDados.FecharTrans();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Cliente não vinculado em uma empresa. Verifique o cadastro primeiro", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtData.Focus();
                        }
                        #endregion
                    }
                    else
                    {
                        MessageBox.Show("Número de Documento já cadastrado", "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDocto.Focus();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dtEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTValor.Focus();
        }

        private void txtTValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbVendedor.Focus();
        }

        private void txtTValor_Validated(object sender, EventArgs e)
        {
            if (txtTValor.Text.Trim() != "")
            {
                txtTValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTValor.Text));
            }
            else
                txtTValor.Text = "0,00";

            if (valorTotal > 0)
            {
                if (Convert.ToDecimal(txtTValor.Text) < valorQuitado + valorSaldo)
                {
                    MessageBox.Show("Valor Total não pode ser menor que o valor pago " + String.Format("{0:C}", valorQuitado + valorSaldo), "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTValor.Text = String.Format("{0:N}", valorQuitado + valorSaldo);
                    txtTValor.Focus();
                }
            }
        }

        private void cmbDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCBusca.Focus();
        }

        public void IncluirTitulos()
        {
            Funcoes.BotoesCadastro("frmFinTitulos");
            lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTitulos, contRegistros));
            cmbCBusca.SelectedIndex = 1;
            dtEmissao.Focus();
            emGrade = false;

        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBEmissao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBEmissao.Text.Replace("/", "").Trim()))
                {
                    txtBCliente.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBDespesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void cmbTitulo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCBusca.Focus();
        }

        private void cmbCBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCBusca.Focus();
        }

        private void txtCBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (cmbCBusca.SelectedIndex != -1 && txtCBusca.Text.Trim() != "")
                    {
                        var dCliente = new Cliente();
                        DataTable cliente = new DataTable();
                        string filtro;
                        if (cmbCBusca.SelectedIndex == 0)
                        {
                            cliente = dCliente.DadosClienteFiltro(txtCBusca.Text.Trim().ToUpper(), 4, out filtro);
                        }
                        else if (cmbCBusca.SelectedIndex == 1)
                        {
                            cliente = dCliente.DadosClienteFiltro(txtCBusca.Text.Trim().ToUpper(), 0, out filtro);
                        }
                        if (cliente.Rows.Count.Equals(1))
                        {
                            idCliente = Convert.ToInt32(cliente.Rows[0]["CF_ID"]);
                            txtCNome.Text = cliente.Rows[0]["CF_NOME"].ToString();
                            txtCCnpj.Text = cliente.Rows[0]["CF_DOCTO"].ToString(); ;
                            txtCCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString() + "/" + cliente.Rows[0]["CF_UF"].ToString();
                            cmbCBusca.SelectedIndex = 1;
                            txtPTotal.Focus();
                        }
                        else if (cliente.Rows.Count.Equals(0))
                        {
                            MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cmbCBusca.SelectedIndex = 0;
                            txtCBusca.Text = "";
                            txtCBusca.Focus();
                        }
                        else
                        {
                            using (var t = new frmBuscaForn("cliente"))
                            {
                                t.dgCliFor.DataSource = cliente;
                                t.ShowDialog();

                                if (!String.IsNullOrEmpty(t.cfNome))
                                {
                                    idCliente = t.cfID;
                                    txtCNome.Text = t.cfNome;
                                    txtCCnpj.Text = t.cfDocto;
                                    txtCCidade.Text = t.cfCidade;
                                    cmbCBusca.SelectedIndex = 1;
                                    txtCBusca.Text = "";
                                    txtPTotal.Focus();
                                }
                                else
                                {
                                    cmbCBusca.SelectedIndex = 1;
                                    txtCBusca.Text = "";
                                    txtCBusca.Focus();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtPVencimento.Focus();
        }

        private void txtPTotal_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPTotal.Text.Trim() != "")
                {
                    txtPTotal.Text = String.Format("{0:N}", Convert.ToDecimal(txtPTotal.Text));
                }
                else
                    txtPTotal.Text = "0,00";

                if (valorTotal > 0)
                {
                    if (Convert.ToDecimal(txtTValor.Text) < valorQuitado + valorSaldo)
                    {
                        MessageBox.Show("Valor Total não pode ser menor que o valor pago " + String.Format("{0:C}", valorQuitado + valorSaldo), "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTValor.Text = String.Format("{0:N}", valorQuitado + valorSaldo);
                        txtTValor.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtPVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (permiteAtu.Equals(false))
                    {
                        MessageBox.Show("Despesa já possui pagamentos efetuados. Alteração não Permitida!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CarregarDados(contRegistros);
                        return;
                    }
                    if ((Convert.ToDecimal(txtPTotal.Text) <= 0))
                    {
                        MessageBox.Show("Valor total da nota não pode ser zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTValor.Focus();
                        return;
                    }
                    if (dtPVencimento.Value <= DateTime.Now)
                    {
                        MessageBox.Show("Vencimento não pode ser igual ou inferior a data de hoje!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtPVencimento.Focus();
                        return;
                    }
                    else
                    {
                        valorTotal = 0;
                        foreach (DataGridViewRow col in dgParcelas.Rows)
                        {
                            valorTotal = valorTotal + Convert.ToDecimal(col.Cells[0].Value);
                        }

                        if ((Convert.ToDecimal(txtTValor.Text) - valorTotal) >= Convert.ToDecimal(txtPTotal.Text))
                        {
                            dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { Convert.ToDecimal(txtPTotal.Text).ToString("##0.00"), dtPVencimento.Value.ToString("dd/MM/yyyy"), "N" });
                            txtPTotal.Text = "0,00";
                            txtPTotal.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Valor das parcelas está diferente do valor total da nota", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtPTotal.Focus();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    if (dgTitulos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTitulos.ColumnCount];
                string[] coluna = new string[dgTitulos.ColumnCount];

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    grid[i] = dgTitulos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTitulos.ColumnCount; i++)
                {
                    if (dgTitulos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTitulos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTitulos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTitulos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTitulos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTitulos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTitulos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTitulos.ColumnCount; i++)
                        {
                            dgTitulos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTitulos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTitulos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTitulos.RowCount;
                    for (i = 0; i <= dgTitulos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTitulos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTitulos.ColumnCount; k++)
                    {
                        if (dgTitulos.Columns[k].Visible == true)
                        {
                            switch (dgTitulos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTitulos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTitulos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTitulos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTitulos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Valor Total":
                                    numCel = Principal.GetColunaExcel(dgTitulos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTitulos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTitulos.ColumnCount : indice) + Convert.ToString(dgTitulos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtReceberID") && (control.Name != "txtCNome") && (control.Name != "txtCCnpj") && (control.Name != "txtCCidade") &&
                        (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtBCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }
    }
}
