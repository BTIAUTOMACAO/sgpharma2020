﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinCadDesp : Form, Botoes
    {
        private DataTable dtDespesas = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbDespesas = new ToolStripButton("Cad. Despesas");
        private ToolStripSeparator tssDespesas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmFinCadDesp(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinCadDesp_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtDespesas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcDespesas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtDespesas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbDespesas.AutoSize = false;
                this.tsbDespesas.Image = Properties.Resources.financeiro;
                this.tsbDespesas.Size = new System.Drawing.Size(135, 20);
                this.tsbDespesas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDespesas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDespesas);
                tsbDespesas.Click += delegate
                {
                    var cadDespesas = Application.OpenForms.OfType<frmFinCadDesp>().FirstOrDefault();
                    BotoesHabilitados();
                    cadDespesas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmFinCadDesp");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
            if (tcDespesas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcDespesas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtDespID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtDespesas.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDespesas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDespesas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtDespesas.Clear();
            dgDespesas.DataSource = dtDespesas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtDespID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcDespesas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtDespID.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["DESP_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["DESP_DESCRICAO"].ToString());
                if (dtDespesas.Rows[linha]["DESP_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadClasses_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtDespID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da despesa?", "Exclusão tabela Cad. Despesas", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirCadDespesas(txtDespID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var excluir = new Despesa();
                            if (!excluir.ExcluirDados(txtDespID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("DESP_CODIGO", txtDespID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "DESPESAS", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtDespesas.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgDespesas.DataSource = dtDespesas;
                                tcDespesas.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;
                    txtDespID.Text = Funcoes.IdentificaVerificaID("CAD_DESPESAS", "DESP_CODIGO").ToString();
                    chkLiberado.Checked = true;
                    Despesa cadDesp = new Despesa
                    {
                        DespCodigo = Convert.ToInt32(txtDespID.Text),
                        DespDescr = txtDescr.Text.Trim().ToUpper(),
                        DespLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    //VERIFICA SE CLASSE JA ESTA CADASTRADA//
                    if (Util.RegistrosPorEstabelecimento("CAD_DESPESAS", "DESP_DESCRICAO", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Despesa já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    if (cadDesp.InsereRegistros(cadDesp).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("DESP_CODIGO", txtDespID.Text, Principal.usuario, "CAD_DESPESAS", txtDescr.Text, Principal.estAtual, Principal.empAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtDespesas.Clear();
                        dgDespesas.DataSource = dtDespesas;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgDespesas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgDespesas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgDespesas.RowCount;
                    for (i = 0; i <= dgDespesas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgDespesas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgDespesas.ColumnCount; k++)
                    {
                        if (dgDespesas.Columns[k].Visible == true)
                        {
                            switch (dgDespesas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgDespesas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDespesas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgDespesas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDespesas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgDespesas.ColumnCount : indice) + Convert.ToString(dgDespesas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Despesa atuDesp = new Despesa
                    {
                        DespCodigo = Convert.ToInt32(Convert.ToDouble(txtDespID.Text)),
                        DespDescr = txtDescr.Text.Trim().ToUpper(),
                        DespLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("CAD_DESPESAS", "DESP_CODIGO", txtDespID.Text);
                    
                    if (atuDesp.AtualizaDados(atuDesp, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Cad. Despesas");
                    }

                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtDespesas.Clear();
                    dgDespesas.DataSource = dtDespesas;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Despesa cadDesp = new Despesa
                {
                    DespCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    DespDescr = txtBDescr.Text.Trim().ToUpper(),
                    DespLiberado = cmbLiberado.Text == "S" ? "N" : cmbLiberado.Text == "TODOS" ? "TODOS" : "S"
                };

                dtDespesas = cadDesp.BuscarDados(cadDesp, out strOrdem);
                if (dtDespesas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtDespesas.Rows.Count;
                    dgDespesas.DataSource = dtDespesas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, 0));
                    dgDespesas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgDespesas.DataSource = dtDespesas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcDespesas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcDespesas.SelectedTab == tpGrade)
            {
                dgDespesas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcDespesas.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmFinCadDesp");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
                    chkLiberado.Checked = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                    txtDescr.Focus();
                }
            }
        }

        private void dgDespesas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
            emGrade = true;
        }

        private void dgDespesas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcDespesas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgDespesas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtDespesas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtDespesas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtDespesas.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgDespesas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgDespesas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtDespesas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDespesas.Columns[e.ColumnIndex].Name + " DESC");
                        dgDespesas.DataSource = dtDespesas;
                        decrescente = true;
                    }
                    else
                    {
                        dtDespesas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDespesas.Columns[e.ColumnIndex].Name + " ASC");
                        dgDespesas.DataSource = dtDespesas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgDespesas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgDespesas.Rows[e.RowIndex].Cells[2].Value.ToString() == "N")
            {
                dgDespesas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void txtDespID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDescr.Focus();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtDespID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    if (dgDespesas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgDespesas.ColumnCount];
                string[] coluna = new string[dgDespesas.ColumnCount];

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    grid[i] = dgDespesas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    if (dgDespesas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgDespesas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgDespesas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgDespesas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgDespesas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgDespesas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgDespesas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgDespesas.ColumnCount; i++)
                        {
                            dgDespesas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cad. Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcDespesas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcDespesas.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
