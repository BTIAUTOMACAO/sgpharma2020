﻿namespace GestaoAdministrativa.Financeiro
{
    partial class frmFinTitulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFinTitulos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tcTitulos = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBCliente = new System.Windows.Forms.TextBox();
            this.txtBEmissao = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgTitulos = new System.Windows.Forms.DataGridView();
            this.cmsTitulos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbVendedor = new System.Windows.Forms.ComboBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.dgParcelas = new System.Windows.Forms.DataGridView();
            this.dtPVencimento = new System.Windows.Forms.DateTimePicker();
            this.txtPTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtCCidade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCCnpj = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCNome = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCBusca = new System.Windows.Forms.TextBox();
            this.cmbCBusca = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.txtTValor = new System.Windows.Forms.TextBox();
            this.dtEmissao = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtReceberID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.COBRANCA_PARCELA_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_VENCIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COL_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.tcTitulos.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTitulos)).BeginInit();
            this.cmsTitulos.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tcTitulos);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // tcTitulos
            // 
            this.tcTitulos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcTitulos.Controls.Add(this.tpGrade);
            this.tcTitulos.Controls.Add(this.tpFicha);
            this.tcTitulos.Location = new System.Drawing.Point(6, 33);
            this.tcTitulos.Name = "tcTitulos";
            this.tcTitulos.SelectedIndex = 0;
            this.tcTitulos.Size = new System.Drawing.Size(965, 499);
            this.tcTitulos.TabIndex = 43;
            this.tcTitulos.SelectedIndexChanged += new System.EventHandler(this.tcTitulos_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBCliente);
            this.tpGrade.Controls.Add(this.txtBEmissao);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label13);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.dgTitulos);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(595, 419);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 91;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBCliente
            // 
            this.txtBCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCliente.Location = new System.Drawing.Point(248, 435);
            this.txtBCliente.MaxLength = 15;
            this.txtBCliente.Name = "txtBCliente";
            this.txtBCliente.Size = new System.Drawing.Size(341, 22);
            this.txtBCliente.TabIndex = 89;
            this.txtBCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCliente_KeyPress);
            // 
            // txtBEmissao
            // 
            this.txtBEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBEmissao.Location = new System.Drawing.Point(119, 435);
            this.txtBEmissao.Mask = "00/00/0000";
            this.txtBEmissao.Name = "txtBEmissao";
            this.txtBEmissao.Size = new System.Drawing.Size(110, 22);
            this.txtBEmissao.TabIndex = 88;
            this.txtBEmissao.ValidatingType = typeof(System.DateTime);
            this.txtBEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBEmissao_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(245, 415);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 84;
            this.label1.Text = "Cliente";
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(9, 435);
            this.txtBID.MaxLength = 18;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(96, 22);
            this.txtBID.TabIndex = 63;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(116, 415);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 16);
            this.label13.TabIndex = 62;
            this.label13.Text = "Data de Emissão";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 416);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "ID";
            // 
            // dgTitulos
            // 
            this.dgTitulos.AllowUserToAddRows = false;
            this.dgTitulos.AllowUserToDeleteRows = false;
            this.dgTitulos.AllowUserToOrderColumns = true;
            this.dgTitulos.AllowUserToResizeColumns = false;
            this.dgTitulos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgTitulos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgTitulos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTitulos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTitulos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgTitulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTitulos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COBRANCA_ID,
            this.COBRANCA_DATA,
            this.COBRANCA_TOTAL,
            this.COBRANCA_VENDA_ID,
            this.CF_DOCTO,
            this.CF_NOME,
            this.COBRANCA_STATUS,
            this.COL_NOME,
            this.DT_ALTERACAO,
            this.OP_ALTERACAO,
            this.DT_CADASTRO,
            this.OP_CADASTRO,
            this.CF_CIDADE,
            this.CF_UF,
            this.CF_ID});
            this.dgTitulos.ContextMenuStrip = this.cmsTitulos;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTitulos.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgTitulos.GridColor = System.Drawing.Color.LightGray;
            this.dgTitulos.Location = new System.Drawing.Point(6, 6);
            this.dgTitulos.MultiSelect = false;
            this.dgTitulos.Name = "dgTitulos";
            this.dgTitulos.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTitulos.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgTitulos.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgTitulos.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgTitulos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTitulos.Size = new System.Drawing.Size(945, 394);
            this.dgTitulos.TabIndex = 1;
            this.dgTitulos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgTitulos_CellMouseClick);
            this.dgTitulos.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgTitulos_CellMouseDoubleClick);
            this.dgTitulos.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgTitulos_ColumnHeaderMouseClick);
            this.dgTitulos.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgTitulos_RowPrePaint);
            this.dgTitulos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgTitulos_KeyDown);
            // 
            // cmsTitulos
            // 
            this.cmsTitulos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsTitulos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsTitulos.Name = "cmsTitulos";
            this.cmsTitulos.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 92);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(574, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 193;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 21);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.cmbVendedor);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.txtDocto);
            this.groupBox2.Controls.Add(this.txtTValor);
            this.groupBox2.Controls.Add(this.dtEmissao);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.txtReceberID);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 360);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(454, 58);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 285;
            this.pictureBox11.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(379, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 16);
            this.label20.TabIndex = 284;
            this.label20.Text = "Vendedor";
            // 
            // cmbVendedor
            // 
            this.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVendedor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVendedor.FormattingEnabled = true;
            this.cmbVendedor.Location = new System.Drawing.Point(382, 74);
            this.cmbVendedor.Name = "cmbVendedor";
            this.cmbVendedor.Size = new System.Drawing.Size(185, 24);
            this.cmbVendedor.TabIndex = 119;
            this.cmbVendedor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDocto_KeyPress);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(641, 58);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 249;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(344, 59);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 248;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(209, 58);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 247;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.pictureBox10);
            this.groupBox7.Controls.Add(this.pictureBox4);
            this.groupBox7.Controls.Add(this.dgParcelas);
            this.groupBox7.Controls.Add(this.dtPVencimento);
            this.groupBox7.Controls.Add(this.txtPTotal);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.ForeColor = System.Drawing.Color.Navy;
            this.groupBox7.Location = new System.Drawing.Point(16, 226);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(439, 128);
            this.groupBox7.TabIndex = 244;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Parcelas";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(189, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 288;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(83, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 287;
            this.pictureBox4.TabStop = false;
            // 
            // dgParcelas
            // 
            this.dgParcelas.AllowUserToAddRows = false;
            this.dgParcelas.AllowUserToResizeColumns = false;
            this.dgParcelas.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgParcelas.BackgroundColor = System.Drawing.Color.White;
            this.dgParcelas.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgParcelas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParcelas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COBRANCA_PARCELA_VALOR,
            this.COBRANCA_PARCELA_VENCIMENTO,
            this.COBRANCA_PARCELA_STATUS,
            this.COBRANCA_PARCELA_SALDO});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgParcelas.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgParcelas.GridColor = System.Drawing.Color.White;
            this.dgParcelas.Location = new System.Drawing.Point(227, 11);
            this.dgParcelas.Name = "dgParcelas";
            this.dgParcelas.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgParcelas.RowHeadersWidth = 35;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgParcelas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgParcelas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgParcelas.Size = new System.Drawing.Size(206, 111);
            this.dgParcelas.TabIndex = 195;
            // 
            // dtPVencimento
            // 
            this.dtPVencimento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPVencimento.Location = new System.Drawing.Point(110, 40);
            this.dtPVencimento.Name = "dtPVencimento";
            this.dtPVencimento.Size = new System.Drawing.Size(106, 22);
            this.dtPVencimento.TabIndex = 194;
            this.dtPVencimento.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtPVencimento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtPVencimento_KeyPress);
            // 
            // txtPTotal
            // 
            this.txtPTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPTotal.Location = new System.Drawing.Point(9, 40);
            this.txtPTotal.MaxLength = 10;
            this.txtPTotal.Name = "txtPTotal";
            this.txtPTotal.Size = new System.Drawing.Size(95, 22);
            this.txtPTotal.TabIndex = 193;
            this.txtPTotal.Text = "0,00";
            this.txtPTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPTotal_KeyPress);
            this.txtPTotal.Validated += new System.EventHandler(this.txtPTotal_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(107, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 16);
            this.label16.TabIndex = 157;
            this.label16.Text = "Vencimento";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 156;
            this.label14.Text = "Valor Total";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.txtCBusca);
            this.groupBox5.Controls.Add(this.cmbCBusca);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(16, 100);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(908, 120);
            this.groupBox5.TabIndex = 159;
            this.groupBox5.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox6);
            this.groupBox6.Controls.Add(this.txtCCidade);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.txtCCnpj);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtCNome);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.ForeColor = System.Drawing.Color.Navy;
            this.groupBox6.Location = new System.Drawing.Point(9, 44);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(893, 68);
            this.groupBox6.TabIndex = 166;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Cliente";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(497, 18);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 172;
            this.pictureBox6.TabStop = false;
            // 
            // txtCCidade
            // 
            this.txtCCidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCidade.Location = new System.Drawing.Point(649, 36);
            this.txtCCidade.MaxLength = 50;
            this.txtCCidade.Name = "txtCCidade";
            this.txtCCidade.ReadOnly = true;
            this.txtCCidade.Size = new System.Drawing.Size(228, 22);
            this.txtCCidade.TabIndex = 156;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(646, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 16);
            this.label19.TabIndex = 155;
            this.label19.Text = "Cidade/UF";
            // 
            // txtCCnpj
            // 
            this.txtCCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCCnpj.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCnpj.Location = new System.Drawing.Point(449, 36);
            this.txtCCnpj.MaxLength = 18;
            this.txtCCnpj.Name = "txtCCnpj";
            this.txtCCnpj.ReadOnly = true;
            this.txtCCnpj.Size = new System.Drawing.Size(183, 22);
            this.txtCCnpj.TabIndex = 154;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(446, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 16);
            this.label18.TabIndex = 153;
            this.label18.Text = "Docto.";
            // 
            // txtCNome
            // 
            this.txtCNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNome.Location = new System.Drawing.Point(10, 36);
            this.txtCNome.MaxLength = 50;
            this.txtCNome.Name = "txtCNome";
            this.txtCNome.ReadOnly = true;
            this.txtCNome.Size = new System.Drawing.Size(420, 22);
            this.txtCNome.TabIndex = 152;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(7, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 16);
            this.label17.TabIndex = 151;
            this.label17.Text = "Nome";
            // 
            // txtCBusca
            // 
            this.txtCBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCBusca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCBusca.Location = new System.Drawing.Point(255, 15);
            this.txtCBusca.MaxLength = 50;
            this.txtCBusca.Name = "txtCBusca";
            this.txtCBusca.Size = new System.Drawing.Size(440, 22);
            this.txtCBusca.TabIndex = 165;
            this.txtCBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCBusca_KeyPress);
            // 
            // cmbCBusca
            // 
            this.cmbCBusca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCBusca.FormattingEnabled = true;
            this.cmbCBusca.Items.AddRange(new object[] {
            "CODIGO",
            "NOME"});
            this.cmbCBusca.Location = new System.Drawing.Point(111, 13);
            this.cmbCBusca.Name = "cmbCBusca";
            this.cmbCBusca.Size = new System.Drawing.Size(131, 24);
            this.cmbCBusca.TabIndex = 153;
            this.cmbCBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCBusca_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 16);
            this.label7.TabIndex = 152;
            this.label7.Text = "Pesquisar por:";
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(576, 76);
            this.txtDocto.MaxLength = 15;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(135, 22);
            this.txtDocto.TabIndex = 120;
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // txtTValor
            // 
            this.txtTValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTValor.Location = new System.Drawing.Point(265, 77);
            this.txtTValor.MaxLength = 18;
            this.txtTValor.Name = "txtTValor";
            this.txtTValor.Size = new System.Drawing.Size(102, 22);
            this.txtTValor.TabIndex = 118;
            this.txtTValor.Text = "0,00";
            this.txtTValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTValor_KeyPress);
            this.txtTValor.Validated += new System.EventHandler(this.txtTValor_Validated);
            // 
            // dtEmissao
            // 
            this.dtEmissao.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtEmissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtEmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissao.Location = new System.Drawing.Point(133, 77);
            this.dtEmissao.Name = "dtEmissao";
            this.dtEmissao.Size = new System.Drawing.Size(116, 22);
            this.dtEmissao.TabIndex = 117;
            this.dtEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtEmissao_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(573, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 115;
            this.label5.Text = "N° Docto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(262, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 114;
            this.label4.Text = "Valor Total";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(130, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 16);
            this.label3.TabIndex = 113;
            this.label3.Text = "Dt. Emissão";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(40, 59);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 112;
            this.pictureBox2.TabStop = false;
            // 
            // txtReceberID
            // 
            this.txtReceberID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReceberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReceberID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceberID.Location = new System.Drawing.Point(16, 77);
            this.txtReceberID.MaxLength = 18;
            this.txtReceberID.Name = "txtReceberID";
            this.txtReceberID.ReadOnly = true;
            this.txtReceberID.Size = new System.Drawing.Size(105, 22);
            this.txtReceberID.TabIndex = 111;
            this.txtReceberID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 16);
            this.label2.TabIndex = 109;
            this.label2.Text = "ID";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            this.lblEstab.Tag = "";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(169, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Lançamento de Títulos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // COBRANCA_PARCELA_VALOR
            // 
            this.COBRANCA_PARCELA_VALOR.DataPropertyName = "COBRANCA_PARCELA_VALOR";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this.COBRANCA_PARCELA_VALOR.DefaultCellStyle = dataGridViewCellStyle11;
            this.COBRANCA_PARCELA_VALOR.HeaderText = "Valor";
            this.COBRANCA_PARCELA_VALOR.MaxInputLength = 18;
            this.COBRANCA_PARCELA_VALOR.Name = "COBRANCA_PARCELA_VALOR";
            this.COBRANCA_PARCELA_VALOR.ReadOnly = true;
            this.COBRANCA_PARCELA_VALOR.Width = 70;
            // 
            // COBRANCA_PARCELA_VENCIMENTO
            // 
            this.COBRANCA_PARCELA_VENCIMENTO.DataPropertyName = "COBRANCA_PARCELA_VENCIMENTO";
            this.COBRANCA_PARCELA_VENCIMENTO.HeaderText = "Vencimento";
            this.COBRANCA_PARCELA_VENCIMENTO.MaxInputLength = 10;
            this.COBRANCA_PARCELA_VENCIMENTO.Name = "COBRANCA_PARCELA_VENCIMENTO";
            this.COBRANCA_PARCELA_VENCIMENTO.ReadOnly = true;
            this.COBRANCA_PARCELA_VENCIMENTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COBRANCA_PARCELA_VENCIMENTO.Width = 258;
            // 
            // COBRANCA_PARCELA_STATUS
            // 
            this.COBRANCA_PARCELA_STATUS.DataPropertyName = "COBRANCA_PARCELA_STATUS";
            this.COBRANCA_PARCELA_STATUS.HeaderText = "Status";
            this.COBRANCA_PARCELA_STATUS.Name = "COBRANCA_PARCELA_STATUS";
            this.COBRANCA_PARCELA_STATUS.ReadOnly = true;
            this.COBRANCA_PARCELA_STATUS.Visible = false;
            // 
            // COBRANCA_PARCELA_SALDO
            // 
            this.COBRANCA_PARCELA_SALDO.DataPropertyName = "COBRANCA_PARCELA_SALDO";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.COBRANCA_PARCELA_SALDO.DefaultCellStyle = dataGridViewCellStyle12;
            this.COBRANCA_PARCELA_SALDO.HeaderText = "Saldo";
            this.COBRANCA_PARCELA_SALDO.Name = "COBRANCA_PARCELA_SALDO";
            this.COBRANCA_PARCELA_SALDO.ReadOnly = true;
            // 
            // COBRANCA_ID
            // 
            this.COBRANCA_ID.DataPropertyName = "COBRANCA_ID";
            this.COBRANCA_ID.HeaderText = "ID";
            this.COBRANCA_ID.MaxInputLength = 18;
            this.COBRANCA_ID.Name = "COBRANCA_ID";
            this.COBRANCA_ID.ReadOnly = true;
            this.COBRANCA_ID.Width = 65;
            // 
            // COBRANCA_DATA
            // 
            this.COBRANCA_DATA.DataPropertyName = "COBRANCA_DATA";
            this.COBRANCA_DATA.HeaderText = "Data de Emissão";
            this.COBRANCA_DATA.MaxInputLength = 10;
            this.COBRANCA_DATA.Name = "COBRANCA_DATA";
            this.COBRANCA_DATA.ReadOnly = true;
            this.COBRANCA_DATA.Width = 150;
            // 
            // COBRANCA_TOTAL
            // 
            this.COBRANCA_TOTAL.DataPropertyName = "COBRANCA_TOTAL";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.COBRANCA_TOTAL.DefaultCellStyle = dataGridViewCellStyle3;
            this.COBRANCA_TOTAL.HeaderText = "Valor Total";
            this.COBRANCA_TOTAL.MaxInputLength = 18;
            this.COBRANCA_TOTAL.Name = "COBRANCA_TOTAL";
            this.COBRANCA_TOTAL.ReadOnly = true;
            // 
            // COBRANCA_VENDA_ID
            // 
            this.COBRANCA_VENDA_ID.DataPropertyName = "COBRANCA_VENDA_ID";
            this.COBRANCA_VENDA_ID.HeaderText = "N° Docto.";
            this.COBRANCA_VENDA_ID.MaxInputLength = 15;
            this.COBRANCA_VENDA_ID.Name = "COBRANCA_VENDA_ID";
            this.COBRANCA_VENDA_ID.ReadOnly = true;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CPF/CNPJ";
            this.CF_DOCTO.MaxInputLength = 20;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.Width = 130;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome";
            this.CF_NOME.MaxInputLength = 100;
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.Width = 250;
            // 
            // COBRANCA_STATUS
            // 
            this.COBRANCA_STATUS.DataPropertyName = "COBRANCA_STATUS";
            this.COBRANCA_STATUS.HeaderText = "Status";
            this.COBRANCA_STATUS.Name = "COBRANCA_STATUS";
            this.COBRANCA_STATUS.ReadOnly = true;
            this.COBRANCA_STATUS.Width = 80;
            // 
            // COL_NOME
            // 
            this.COL_NOME.DataPropertyName = "COL_NOME";
            this.COL_NOME.HeaderText = "Vendedor";
            this.COL_NOME.Name = "COL_NOME";
            this.COL_NOME.ReadOnly = true;
            // 
            // DT_ALTERACAO
            // 
            this.DT_ALTERACAO.DataPropertyName = "DT_ALTERACAO";
            dataGridViewCellStyle4.Format = "G";
            this.DT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DT_ALTERACAO.HeaderText = "Data Alteração";
            this.DT_ALTERACAO.MaxInputLength = 20;
            this.DT_ALTERACAO.Name = "DT_ALTERACAO";
            this.DT_ALTERACAO.ReadOnly = true;
            this.DT_ALTERACAO.Width = 150;
            // 
            // OP_ALTERACAO
            // 
            this.OP_ALTERACAO.DataPropertyName = "OP_ALTERACAO";
            this.OP_ALTERACAO.HeaderText = "Usuário Alteração";
            this.OP_ALTERACAO.MaxInputLength = 25;
            this.OP_ALTERACAO.Name = "OP_ALTERACAO";
            this.OP_ALTERACAO.ReadOnly = true;
            this.OP_ALTERACAO.Width = 150;
            // 
            // DT_CADASTRO
            // 
            this.DT_CADASTRO.DataPropertyName = "DT_CADASTRO";
            dataGridViewCellStyle5.Format = "G";
            this.DT_CADASTRO.DefaultCellStyle = dataGridViewCellStyle5;
            this.DT_CADASTRO.HeaderText = "Data do Cadastro";
            this.DT_CADASTRO.MaxInputLength = 20;
            this.DT_CADASTRO.Name = "DT_CADASTRO";
            this.DT_CADASTRO.ReadOnly = true;
            this.DT_CADASTRO.Width = 150;
            // 
            // OP_CADASTRO
            // 
            this.OP_CADASTRO.DataPropertyName = "OP_CADASTRO";
            this.OP_CADASTRO.HeaderText = "Usuário Cadastro";
            this.OP_CADASTRO.MaxInputLength = 25;
            this.OP_CADASTRO.Name = "OP_CADASTRO";
            this.OP_CADASTRO.ReadOnly = true;
            this.OP_CADASTRO.Width = 150;
            // 
            // CF_CIDADE
            // 
            this.CF_CIDADE.DataPropertyName = "CF_CIDADE";
            this.CF_CIDADE.HeaderText = "Cidade";
            this.CF_CIDADE.Name = "CF_CIDADE";
            this.CF_CIDADE.ReadOnly = true;
            this.CF_CIDADE.Visible = false;
            // 
            // CF_UF
            // 
            this.CF_UF.DataPropertyName = "CF_UF";
            this.CF_UF.HeaderText = "CF_UF";
            this.CF_UF.Name = "CF_UF";
            this.CF_UF.ReadOnly = true;
            this.CF_UF.Visible = false;
            // 
            // CF_ID
            // 
            this.CF_ID.DataPropertyName = "CF_ID";
            this.CF_ID.HeaderText = "CF_ID";
            this.CF_ID.Name = "CF_ID";
            this.CF_ID.ReadOnly = true;
            this.CF_ID.Visible = false;
            // 
            // frmFinTitulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmFinTitulos";
            this.Text = "frmFinTitulos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmFinTitulos_Load);
            this.Shown += new System.EventHandler(this.frmFinTitulos_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcTitulos.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTitulos)).EndInit();
            this.cmsTitulos.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TabControl tcTitulos;
        public System.Windows.Forms.TabPage tpGrade;
        public System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBCliente;
        private System.Windows.Forms.MaskedTextBox txtBEmissao;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgTitulos;
        public System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.DataGridView dgParcelas;
        private System.Windows.Forms.DateTimePicker dtPVencimento;
        private System.Windows.Forms.TextBox txtPTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.TextBox txtCCidade;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtCCnpj;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtCNome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCBusca;
        private System.Windows.Forms.ComboBox cmbCBusca;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.TextBox txtTValor;
        private System.Windows.Forms.DateTimePicker dtEmissao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtReceberID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.ContextMenuStrip cmsTitulos;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbVendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_VENCIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_SALDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ID;
    }
}