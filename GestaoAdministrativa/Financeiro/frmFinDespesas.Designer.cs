﻿namespace GestaoAdministrativa.Financeiro
{
    partial class frmFinDespesas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFinDespesas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tcDespesas = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbBDespesa = new System.Windows.Forms.ComboBox();
            this.txtBDocto = new System.Windows.Forms.TextBox();
            this.txtBEmissao = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgDespesas = new System.Windows.Forms.DataGridView();
            this.PAG_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_VALPRIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIP_DESC_ABREV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESP_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORN_CGC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HISTORICO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsDespesas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbDocto = new System.Windows.Forms.ComboBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtHistorico = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.dgParcelas = new System.Windows.Forms.DataGridView();
            this.ENT_VALPARC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VENCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PD_SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtPVencimento = new System.Windows.Forms.DateTimePicker();
            this.txtPTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtFCidade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFCnpj = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFNome = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFBusca = new System.Windows.Forms.TextBox();
            this.cmbFBusca = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbTDespesa = new System.Windows.Forms.ComboBox();
            this.txtTDesp = new System.Windows.Forms.TextBox();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.txtTValor = new System.Windows.Forms.TextBox();
            this.dtEmissao = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtPagarID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.tcDespesas.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDespesas)).BeginInit();
            this.cmsDespesas.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tcDespesas);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // tcDespesas
            // 
            this.tcDespesas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcDespesas.Controls.Add(this.tpGrade);
            this.tcDespesas.Controls.Add(this.tpFicha);
            this.tcDespesas.Location = new System.Drawing.Point(6, 33);
            this.tcDespesas.Name = "tcDespesas";
            this.tcDespesas.SelectedIndex = 0;
            this.tcDespesas.Size = new System.Drawing.Size(965, 499);
            this.tcDespesas.TabIndex = 43;
            this.tcDespesas.SelectedIndexChanged += new System.EventHandler(this.tcDespesas_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbBDespesa);
            this.tpGrade.Controls.Add(this.txtBDocto);
            this.tpGrade.Controls.Add(this.txtBEmissao);
            this.tpGrade.Controls.Add(this.label9);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label13);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.dgDespesas);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(628, 418);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 91;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbBDespesa
            // 
            this.cmbBDespesa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBDespesa.FormattingEnabled = true;
            this.cmbBDespesa.Location = new System.Drawing.Point(382, 432);
            this.cmbBDespesa.Name = "cmbBDespesa";
            this.cmbBDespesa.Size = new System.Drawing.Size(226, 24);
            this.cmbBDespesa.TabIndex = 90;
            this.cmbBDespesa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBDespesa_KeyPress);
            // 
            // txtBDocto
            // 
            this.txtBDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBDocto.Location = new System.Drawing.Point(248, 434);
            this.txtBDocto.MaxLength = 15;
            this.txtBDocto.Name = "txtBDocto";
            this.txtBDocto.Size = new System.Drawing.Size(114, 22);
            this.txtBDocto.TabIndex = 89;
            this.txtBDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBDocto_KeyPress);
            // 
            // txtBEmissao
            // 
            this.txtBEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBEmissao.Location = new System.Drawing.Point(119, 434);
            this.txtBEmissao.Mask = "00/00/0000";
            this.txtBEmissao.Name = "txtBEmissao";
            this.txtBEmissao.Size = new System.Drawing.Size(110, 22);
            this.txtBEmissao.TabIndex = 88;
            this.txtBEmissao.ValidatingType = typeof(System.DateTime);
            this.txtBEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBEmissao_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(379, 415);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 16);
            this.label9.TabIndex = 87;
            this.label9.Text = "Tipo de Despesa";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(245, 415);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 84;
            this.label1.Text = "N° Docto.";
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(9, 435);
            this.txtBID.MaxLength = 18;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(96, 22);
            this.txtBID.TabIndex = 63;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(116, 415);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 16);
            this.label13.TabIndex = 62;
            this.label13.Text = "Data de Emissão";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 416);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "ID";
            // 
            // dgDespesas
            // 
            this.dgDespesas.AllowUserToAddRows = false;
            this.dgDespesas.AllowUserToDeleteRows = false;
            this.dgDespesas.AllowUserToOrderColumns = true;
            this.dgDespesas.AllowUserToResizeColumns = false;
            this.dgDespesas.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.dgDespesas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgDespesas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDespesas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDespesas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgDespesas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDespesas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAG_CODIGO,
            this.PAG_DATA,
            this.PAG_VALPRIN,
            this.TIP_DESC_ABREV,
            this.PAG_DOCTO,
            this.DESP_DESCRICAO,
            this.FORN_CGC,
            this.CF_NOME,
            this.HISTORICO,
            this.PAG_STATUS,
            this.DAT_ALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO});
            this.dgDespesas.ContextMenuStrip = this.cmsDespesas;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDespesas.DefaultCellStyle = dataGridViewCellStyle21;
            this.dgDespesas.GridColor = System.Drawing.Color.LightGray;
            this.dgDespesas.Location = new System.Drawing.Point(6, 6);
            this.dgDespesas.MultiSelect = false;
            this.dgDespesas.Name = "dgDespesas";
            this.dgDespesas.ReadOnly = true;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDespesas.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgDespesas.RowHeadersVisible = false;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dgDespesas.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dgDespesas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDespesas.Size = new System.Drawing.Size(945, 394);
            this.dgDespesas.TabIndex = 1;
            this.dgDespesas.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgDespesas_CellMouseClick);
            this.dgDespesas.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgDespesas_CellMouseDoubleClick);
            this.dgDespesas.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgDespesas_ColumnHeaderMouseClick);
            this.dgDespesas.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgDespesas_RowPrePaint);
            this.dgDespesas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgDespesas_KeyDown);
            // 
            // PAG_CODIGO
            // 
            this.PAG_CODIGO.DataPropertyName = "PAG_CODIGO";
            this.PAG_CODIGO.HeaderText = "ID";
            this.PAG_CODIGO.MaxInputLength = 18;
            this.PAG_CODIGO.Name = "PAG_CODIGO";
            this.PAG_CODIGO.ReadOnly = true;
            this.PAG_CODIGO.Width = 65;
            // 
            // PAG_DATA
            // 
            this.PAG_DATA.DataPropertyName = "PAG_DATA";
            this.PAG_DATA.HeaderText = "Data de Emissão";
            this.PAG_DATA.MaxInputLength = 10;
            this.PAG_DATA.Name = "PAG_DATA";
            this.PAG_DATA.ReadOnly = true;
            this.PAG_DATA.Width = 150;
            // 
            // PAG_VALPRIN
            // 
            this.PAG_VALPRIN.DataPropertyName = "PAG_VALPRIN";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.Format = "N2";
            dataGridViewCellStyle18.NullValue = null;
            this.PAG_VALPRIN.DefaultCellStyle = dataGridViewCellStyle18;
            this.PAG_VALPRIN.HeaderText = "Valor Total";
            this.PAG_VALPRIN.MaxInputLength = 18;
            this.PAG_VALPRIN.Name = "PAG_VALPRIN";
            this.PAG_VALPRIN.ReadOnly = true;
            // 
            // TIP_DESC_ABREV
            // 
            this.TIP_DESC_ABREV.DataPropertyName = "TIP_DESC_ABREV";
            this.TIP_DESC_ABREV.HeaderText = "Tipo Docto.";
            this.TIP_DESC_ABREV.Name = "TIP_DESC_ABREV";
            this.TIP_DESC_ABREV.ReadOnly = true;
            // 
            // PAG_DOCTO
            // 
            this.PAG_DOCTO.DataPropertyName = "PAG_DOCTO";
            this.PAG_DOCTO.HeaderText = "N° Docto.";
            this.PAG_DOCTO.MaxInputLength = 15;
            this.PAG_DOCTO.Name = "PAG_DOCTO";
            this.PAG_DOCTO.ReadOnly = true;
            // 
            // DESP_DESCRICAO
            // 
            this.DESP_DESCRICAO.DataPropertyName = "DESP_DESCRICAO";
            this.DESP_DESCRICAO.HeaderText = "Tipo de Despesa";
            this.DESP_DESCRICAO.MaxInputLength = 50;
            this.DESP_DESCRICAO.Name = "DESP_DESCRICAO";
            this.DESP_DESCRICAO.ReadOnly = true;
            this.DESP_DESCRICAO.Width = 140;
            // 
            // FORN_CGC
            // 
            this.FORN_CGC.DataPropertyName = "CF_DOCTO";
            this.FORN_CGC.HeaderText = "CNPJ";
            this.FORN_CGC.MaxInputLength = 20;
            this.FORN_CGC.Name = "FORN_CGC";
            this.FORN_CGC.ReadOnly = true;
            this.FORN_CGC.Width = 130;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome";
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.Width = 200;
            // 
            // HISTORICO
            // 
            this.HISTORICO.DataPropertyName = "HISTORICO";
            this.HISTORICO.HeaderText = "Histórico";
            this.HISTORICO.MaxInputLength = 100;
            this.HISTORICO.Name = "HISTORICO";
            this.HISTORICO.ReadOnly = true;
            this.HISTORICO.Width = 300;
            // 
            // PAG_STATUS
            // 
            this.PAG_STATUS.DataPropertyName = "PAG_STATUS";
            this.PAG_STATUS.HeaderText = "Status";
            this.PAG_STATUS.Name = "PAG_STATUS";
            this.PAG_STATUS.ReadOnly = true;
            // 
            // DAT_ALTERACAO
            // 
            this.DAT_ALTERACAO.DataPropertyName = "DAT_ALTERACAO";
            dataGridViewCellStyle19.Format = "G";
            this.DAT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle19;
            this.DAT_ALTERACAO.HeaderText = "Data Alteração";
            this.DAT_ALTERACAO.MaxInputLength = 20;
            this.DAT_ALTERACAO.Name = "DAT_ALTERACAO";
            this.DAT_ALTERACAO.ReadOnly = true;
            this.DAT_ALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle20.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle20;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 20;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // cmsDespesas
            // 
            this.cmsDespesas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsDespesas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsDespesas.Name = "cmsDespesas";
            this.cmsDespesas.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 92);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(574, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 193;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 21);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.cmbDocto);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.txtHistorico);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.cmbTDespesa);
            this.groupBox2.Controls.Add(this.txtTDesp);
            this.groupBox2.Controls.Add(this.txtDocto);
            this.groupBox2.Controls.Add(this.txtTValor);
            this.groupBox2.Controls.Add(this.dtEmissao);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.txtPagarID);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 360);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(479, 57);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 285;
            this.pictureBox11.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(379, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 16);
            this.label20.TabIndex = 284;
            this.label20.Text = "Tipo de Docto.";
            // 
            // cmbDocto
            // 
            this.cmbDocto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocto.FormattingEnabled = true;
            this.cmbDocto.Location = new System.Drawing.Point(382, 74);
            this.cmbDocto.Name = "cmbDocto";
            this.cmbDocto.Size = new System.Drawing.Size(126, 24);
            this.cmbDocto.TabIndex = 119;
            this.cmbDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDocto_KeyPress);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(784, 58);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 250;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(588, 58);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 249;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(344, 59);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 248;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(209, 58);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 247;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // txtHistorico
            // 
            this.txtHistorico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHistorico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHistorico.Location = new System.Drawing.Point(464, 242);
            this.txtHistorico.MaxLength = 50;
            this.txtHistorico.Name = "txtHistorico";
            this.txtHistorico.Size = new System.Drawing.Size(460, 22);
            this.txtHistorico.TabIndex = 246;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(461, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 16);
            this.label8.TabIndex = 245;
            this.label8.Text = "Histórico";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.pictureBox10);
            this.groupBox7.Controls.Add(this.pictureBox4);
            this.groupBox7.Controls.Add(this.dgParcelas);
            this.groupBox7.Controls.Add(this.dtPVencimento);
            this.groupBox7.Controls.Add(this.txtPTotal);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.ForeColor = System.Drawing.Color.Navy;
            this.groupBox7.Location = new System.Drawing.Point(16, 226);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(439, 128);
            this.groupBox7.TabIndex = 244;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Parcelas";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(189, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 288;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(83, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 287;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // dgParcelas
            // 
            this.dgParcelas.AllowUserToAddRows = false;
            this.dgParcelas.AllowUserToDeleteRows = false;
            this.dgParcelas.AllowUserToResizeColumns = false;
            this.dgParcelas.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgParcelas.BackgroundColor = System.Drawing.Color.White;
            this.dgParcelas.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgParcelas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParcelas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ENT_VALPARC,
            this.ENT_VENCTO,
            this.PD_STATUS,
            this.PD_SALDO});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgParcelas.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgParcelas.GridColor = System.Drawing.Color.White;
            this.dgParcelas.Location = new System.Drawing.Point(227, 11);
            this.dgParcelas.Name = "dgParcelas";
            this.dgParcelas.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgParcelas.RowHeadersWidth = 35;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgParcelas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgParcelas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgParcelas.Size = new System.Drawing.Size(206, 111);
            this.dgParcelas.TabIndex = 195;
            // 
            // ENT_VALPARC
            // 
            this.ENT_VALPARC.DataPropertyName = "ENT_VALPARC";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.ENT_VALPARC.DefaultCellStyle = dataGridViewCellStyle6;
            this.ENT_VALPARC.HeaderText = "Valor";
            this.ENT_VALPARC.MaxInputLength = 18;
            this.ENT_VALPARC.Name = "ENT_VALPARC";
            this.ENT_VALPARC.ReadOnly = true;
            this.ENT_VALPARC.Width = 70;
            // 
            // ENT_VENCTO
            // 
            this.ENT_VENCTO.DataPropertyName = "ENT_VENCTO";
            this.ENT_VENCTO.HeaderText = "Vencimento";
            this.ENT_VENCTO.MaxInputLength = 10;
            this.ENT_VENCTO.Name = "ENT_VENCTO";
            this.ENT_VENCTO.ReadOnly = true;
            this.ENT_VENCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_VENCTO.Width = 258;
            // 
            // PD_STATUS
            // 
            this.PD_STATUS.DataPropertyName = "PD_STATUS";
            this.PD_STATUS.HeaderText = "Status";
            this.PD_STATUS.Name = "PD_STATUS";
            this.PD_STATUS.ReadOnly = true;
            this.PD_STATUS.Visible = false;
            // 
            // PD_SALDO
            // 
            this.PD_SALDO.DataPropertyName = "PD_SALDO";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.PD_SALDO.DefaultCellStyle = dataGridViewCellStyle7;
            this.PD_SALDO.HeaderText = "Saldo";
            this.PD_SALDO.Name = "PD_SALDO";
            this.PD_SALDO.ReadOnly = true;
            // 
            // dtPVencimento
            // 
            this.dtPVencimento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPVencimento.Location = new System.Drawing.Point(110, 40);
            this.dtPVencimento.Name = "dtPVencimento";
            this.dtPVencimento.Size = new System.Drawing.Size(106, 22);
            this.dtPVencimento.TabIndex = 194;
            this.dtPVencimento.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtPVencimento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtPVencimento_KeyPress);
            // 
            // txtPTotal
            // 
            this.txtPTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPTotal.Location = new System.Drawing.Point(9, 40);
            this.txtPTotal.MaxLength = 10;
            this.txtPTotal.Name = "txtPTotal";
            this.txtPTotal.Size = new System.Drawing.Size(95, 22);
            this.txtPTotal.TabIndex = 193;
            this.txtPTotal.Text = "0,00";
            this.txtPTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPTotal_KeyPress);
            this.txtPTotal.Validated += new System.EventHandler(this.txtPTotal_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(107, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 16);
            this.label16.TabIndex = 157;
            this.label16.Text = "Vencimento";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 156;
            this.label14.Text = "Valor Total";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.txtFBusca);
            this.groupBox5.Controls.Add(this.cmbFBusca);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(16, 100);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(908, 120);
            this.groupBox5.TabIndex = 159;
            this.groupBox5.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox6);
            this.groupBox6.Controls.Add(this.txtFCidade);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.txtFCnpj);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtFNome);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.ForeColor = System.Drawing.Color.Navy;
            this.groupBox6.Location = new System.Drawing.Point(9, 44);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(893, 68);
            this.groupBox6.TabIndex = 166;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fornecedor";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(497, 18);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 172;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // txtFCidade
            // 
            this.txtFCidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFCidade.Location = new System.Drawing.Point(649, 36);
            this.txtFCidade.MaxLength = 50;
            this.txtFCidade.Name = "txtFCidade";
            this.txtFCidade.ReadOnly = true;
            this.txtFCidade.Size = new System.Drawing.Size(228, 22);
            this.txtFCidade.TabIndex = 156;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(646, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 16);
            this.label19.TabIndex = 155;
            this.label19.Text = "Cidade/UF";
            // 
            // txtFCnpj
            // 
            this.txtFCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFCnpj.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFCnpj.Location = new System.Drawing.Point(449, 36);
            this.txtFCnpj.MaxLength = 18;
            this.txtFCnpj.Name = "txtFCnpj";
            this.txtFCnpj.ReadOnly = true;
            this.txtFCnpj.Size = new System.Drawing.Size(183, 22);
            this.txtFCnpj.TabIndex = 154;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(446, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 16);
            this.label18.TabIndex = 153;
            this.label18.Text = "Docto.";
            // 
            // txtFNome
            // 
            this.txtFNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFNome.Location = new System.Drawing.Point(10, 36);
            this.txtFNome.MaxLength = 50;
            this.txtFNome.Name = "txtFNome";
            this.txtFNome.ReadOnly = true;
            this.txtFNome.Size = new System.Drawing.Size(420, 22);
            this.txtFNome.TabIndex = 152;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(7, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 16);
            this.label17.TabIndex = 151;
            this.label17.Text = "Nome";
            // 
            // txtFBusca
            // 
            this.txtFBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFBusca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFBusca.Location = new System.Drawing.Point(255, 15);
            this.txtFBusca.MaxLength = 50;
            this.txtFBusca.Name = "txtFBusca";
            this.txtFBusca.Size = new System.Drawing.Size(440, 22);
            this.txtFBusca.TabIndex = 165;
            this.txtFBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFBusca_KeyPress);
            // 
            // cmbFBusca
            // 
            this.cmbFBusca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFBusca.FormattingEnabled = true;
            this.cmbFBusca.Items.AddRange(new object[] {
            "NOME",
            "CNPJ"});
            this.cmbFBusca.Location = new System.Drawing.Point(111, 13);
            this.cmbFBusca.Name = "cmbFBusca";
            this.cmbFBusca.Size = new System.Drawing.Size(131, 24);
            this.cmbFBusca.TabIndex = 153;
            this.cmbFBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFBusca_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 16);
            this.label7.TabIndex = 152;
            this.label7.Text = "Pesquisar por:";
            // 
            // cmbTDespesa
            // 
            this.cmbTDespesa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTDespesa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTDespesa.FormattingEnabled = true;
            this.cmbTDespesa.Location = new System.Drawing.Point(747, 75);
            this.cmbTDespesa.Name = "cmbTDespesa";
            this.cmbTDespesa.Size = new System.Drawing.Size(177, 24);
            this.cmbTDespesa.TabIndex = 122;
            this.cmbTDespesa.SelectedIndexChanged += new System.EventHandler(this.cmbTDespesa_SelectedIndexChanged);
            this.cmbTDespesa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTDespesa_KeyPress);
            // 
            // txtTDesp
            // 
            this.txtTDesp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTDesp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTDesp.Location = new System.Drawing.Point(668, 76);
            this.txtTDesp.MaxLength = 18;
            this.txtTDesp.Name = "txtTDesp";
            this.txtTDesp.Size = new System.Drawing.Size(69, 22);
            this.txtTDesp.TabIndex = 121;
            this.txtTDesp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTDesp_KeyPress);
            this.txtTDesp.Validated += new System.EventHandler(this.txtTDesp_Validated);
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(523, 76);
            this.txtDocto.MaxLength = 15;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(135, 22);
            this.txtDocto.TabIndex = 120;
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // txtTValor
            // 
            this.txtTValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTValor.Location = new System.Drawing.Point(265, 77);
            this.txtTValor.MaxLength = 18;
            this.txtTValor.Name = "txtTValor";
            this.txtTValor.Size = new System.Drawing.Size(102, 22);
            this.txtTValor.TabIndex = 118;
            this.txtTValor.Text = "0,00";
            this.txtTValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTValor_KeyPress);
            this.txtTValor.Validated += new System.EventHandler(this.txtTValor_Validated);
            // 
            // dtEmissao
            // 
            this.dtEmissao.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtEmissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtEmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissao.Location = new System.Drawing.Point(133, 77);
            this.dtEmissao.Name = "dtEmissao";
            this.dtEmissao.Size = new System.Drawing.Size(116, 22);
            this.dtEmissao.TabIndex = 117;
            this.dtEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtEmissao_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(665, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 16);
            this.label6.TabIndex = 116;
            this.label6.Text = "Tipo de Despesa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(520, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 115;
            this.label5.Text = "N° Docto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(262, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 114;
            this.label4.Text = "Valor Total";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(130, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 16);
            this.label3.TabIndex = 113;
            this.label3.Text = "Dt. Emissão";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(40, 59);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 112;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // txtPagarID
            // 
            this.txtPagarID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPagarID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPagarID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPagarID.Location = new System.Drawing.Point(16, 77);
            this.txtPagarID.MaxLength = 18;
            this.txtPagarID.Name = "txtPagarID";
            this.txtPagarID.ReadOnly = true;
            this.txtPagarID.Size = new System.Drawing.Size(105, 22);
            this.txtPagarID.TabIndex = 111;
            this.txtPagarID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 16);
            this.label2.TabIndex = 109;
            this.label2.Text = "ID";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            this.lblEstab.Tag = "";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(190, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Lançamento de Despesas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmFinDespesas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmFinDespesas";
            this.Text = "frmFinDespesas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmFinDespesas_Load);
            this.Shown += new System.EventHandler(this.frmFinDespesas_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tcDespesas.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDespesas)).EndInit();
            this.cmsDespesas.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgDespesas;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.TextBox txtFCidade;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtFCnpj;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtFNome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFBusca;
        private System.Windows.Forms.ComboBox cmbFBusca;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbTDespesa;
        private System.Windows.Forms.TextBox txtTDesp;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.TextBox txtTValor;
        private System.Windows.Forms.DateTimePicker dtEmissao;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.DataGridView dgParcelas;
        private System.Windows.Forms.DateTimePicker dtPVencimento;
        private System.Windows.Forms.TextBox txtPTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtHistorico;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbBDespesa;
        private System.Windows.Forms.TextBox txtBDocto;
        private System.Windows.Forms.MaskedTextBox txtBEmissao;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip cmsDespesas;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        public System.Windows.Forms.TextBox txtBID;
        public System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtPagarID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbDocto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VALPARC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VENCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PD_SALDO;
        public System.Windows.Forms.TabControl tcDespesas;
        public System.Windows.Forms.TabPage tpGrade;
        public System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_VALPRIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIP_DESC_ABREV;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESP_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FORN_CGC;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn HISTORICO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
    }
}