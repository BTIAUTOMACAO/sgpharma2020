﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinContasPagar : Form, Botoes
    {
        private DataTable dtContas = new DataTable();
        private double total;
        private ToolStripButton tsbContas = new ToolStripButton("Contas à Pagar");
        private ToolStripSeparator tssContas = new ToolStripSeparator();
        private int indiceGrid;
        private int qtdParc;

        public frmFinContasPagar(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinContasPagar_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                indiceGrid = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool CarregarContas()
        {
            try
            {
                indiceGrid = 0;
                Principal.strPesq = "";
                Cursor = Cursors.WaitCursor;

                Pagar pg = new Pagar()
                {
                    PagDocto = txtDocto.Text.ToUpper(),
                    CfDocto = txtForn.Text.ToUpper(),
                    PagStatus = rdbAberto.Checked == true ? "N" : rdbQuitada.Checked == true ? "Q" : "T"
                };

                dtContas = pg.ListarContasPagar(pg, rdbOTodas.Checked ? "T" : rdbEntrada.Checked ? "E" : rdbDespesas.Checked ? "D" : "W",txtDInicial.Text, txtDFinal.Text, Principal.estAtual);
                if (dtContas.Rows.Count == 0)
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContas.DataSource = dtContas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    tslRegistros.Text = "";
                    txtDInicial.Focus();
                    Funcoes.LimpaFormularios(this);
                    lblTotal.Text = "0,00";
                    rdbAberto.Checked = true;
                    rdbOTodas.Checked = true;
                    return false;
                }
                else
                {
                    lblTotal.Text = "0,00";
                    total = 0;
                    for (int i = 0; i < dtContas.Rows.Count; i++)
                    {
                        total = total + Convert.ToDouble(dtContas.Rows[i]["PD_SALDO"]);
                    }
                    lblTotal.Text = String.Format("{0:N}", total);

                    tslRegistros.Text = "Registros encontrados: " + dtContas.Rows.Count;
                    dgContas.DataSource = dtContas;
                    dgContas.Focus();
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmFinContasPagar_Shown(object sender, EventArgs e)
        {
            CarregarContas();
        }

        private void txtDInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDFinal.Focus();
        }

        private void txtDFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtForn.Focus();
        }

        private void txtForn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnPesquisar.PerformClick();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            CarregarContas();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgContas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 1; k < (dgContas.Columns.Count - 2) + 1; k++)
                    {
                        planilha.Cells[1, k] = dgContas.Columns[k - 1].HeaderText;
                        planilha.Cells.NumberFormat = "@";
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgContas.RowCount;
                    for (i = 0; i <= dgContas.RowCount - 1; i++)
                    {
                        for (j = 0; j <= (dgContas.ColumnCount - 2) - 1; j++)
                        {
                            DataGridViewCell coluna = dgContas[j, i];
                            planilha.Cells[k + 1, j + 1] = coluna.Value;
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }
                    Excel.Range celulas;
                    //FORMATA VALOR//
                    celulas = planilha.get_Range("F2", "F" + Convert.ToString(dgContas.RowCount + 1));
                    celulas.NumberFormat = "#,##0.00";

                    celulas = planilha.get_Range("G2", "G" + Convert.ToString(dgContas.RowCount + 1));
                    celulas.NumberFormat = "#,##0.00";

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", "K" + Convert.ToString(dgContas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
                else
                {
                    MessageBox.Show("Não há registros a serem Exportados!", "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btnPesquisar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbContas.AutoSize = false;
                this.tsbContas.Image = Properties.Resources.financeiro;
                this.tsbContas.Size = new System.Drawing.Size(135, 20);
                this.tsbContas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbContas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssContas);
                tsbContas.Click += delegate
                {
                    var finContas = Application.OpenForms.OfType<frmFinContasPagar>().FirstOrDefault();
                    Util.BotoesGenericos();
                    finContas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtContas.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbContas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssContas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Excluir()
        {
            try
            {
                bool parcelas = false;

                if (dgContas.RowCount != 0)
                {
                    for (int i = 0; i < dgContas.RowCount; i++)
                    {
                        if (dgContas.Rows[i].Selected == true)
                        {
                            if (Convert.ToString(dgContas.Rows[i].Cells["PAG_STATUS"].Value) == "Q")
                            {
                                MessageBox.Show("ATENÇÃO: Esta conta já está quitada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            if (Convert.ToString(dgContas.Rows[i].Cells["PAG_ORIGEM"].Value) == "E")
                            {
                                MessageBox.Show("ATENÇÃO: Esta conta tem origem na ENTRADA E NOTAS. Será excluída somente as informações do Conta à Pagar.\nExclusão da nota completa deve ser feita diretamente na ENTRADA DE DOCUMENTOS.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            if (MessageBox.Show("Não será possível a recuperação dessa conta. Confirma a Exlusão?", "Exlusão Contas à Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                //VERIFICA SE POSSUI MAIS DE UMA PARCELA//
                                Pagar pg = new Pagar()
                                {
                                    EmpCodigo = Principal.empAtual,
                                    PagCodigo = Convert.ToInt32(dgContas.Rows[i].Cells["PAG_CODIGO"].Value),
                                };

                                qtdParc = pg.QtdeParcelas(pg, true);

                                if (qtdParc > 1)
                                {
                                    if (MessageBox.Show("Deseja apagar todas as parcelas referente a esta conta?", "Exlusão Conta à Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        parcelas = true;
                                    }
                                }
                                else
                                {
                                    parcelas = true;
                                }

                                using (frmOcorrencia ocorrencia = new frmOcorrencia())
                                {
                                    ocorrencia.ShowDialog();
                                }

                                BancoDados.AbrirTrans();

                                var detalhe = new PagarDetalhe();

                                if (detalhe.ExcluirParcelas(Principal.empAtual, Convert.ToInt32(dgContas.Rows[indiceGrid].Cells["PAG_CODIGO"].Value),
                                    parcelas == false ? Convert.ToInt32(dgContas.Rows[i].Cells["PD_PARCELA"].Value) : 0).Equals(false))
                                    return false;
                                
                                if (parcelas.Equals(true))
                                {
                                    //EXCLUI DA TABELA PAGAR//
                                    if (pg.ExcluiFatura(Principal.empAtual, dgContas.Rows[i].Cells["PAG_CODIGO"].Value.ToString(),false).Equals(false))
                                        return false;
                                }

                                Funcoes.GravaLogExclusao("PAG_CODIGO", Convert.ToString(dgContas.Rows[i].Cells["PAG_CODIGO"].Value), Principal.usuario, 
                                    DateTime.Now, "PAGAR_DETALHE", Convert.ToString(dgContas.Rows[i].Cells["PAG_CODIGO"].Value), Principal.motivo, Principal.estAtual, Principal.empAtual, true);
                                if (qtdParc == 1)
                                {
                                    Funcoes.GravaLogExclusao("PAG_CODIGO", Convert.ToString(dgContas.Rows[i].Cells["PAG_CODIGO"].Value), 
                                        Principal.usuario, DateTime.Now, "PAGAR", Convert.ToString(dgContas.Rows[i].Cells["PAG_CODIGO"].Value), Principal.motivo, Principal.estAtual,Principal.empAtual, true);
                                }
                               
                                BancoDados.FecharTrans();

                                ExcluirPagaEPagarDetalheNuvem(dgContas.Rows[i].Cells["ID"].Value.ToString());

                                MessageBox.Show("Conta excluída com sucesso!", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }
                        }
                    }

                    CarregarContas();

                    return true;
                }
                else
                {
                    MessageBox.Show("Necessário realizar uma busca para Excluir uma Conta.", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnExcluir.Focus();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static async void ExcluirPagaEPagarDetalheNuvem(string IDNuvem)
        {
            await EnviaOuRecebeDadosMysql.ExcluiPagarEPagarDetalhe(IDNuvem);
        }

        private void dgContas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (Excluir().Equals(false))
                BancoDados.ErroTrans();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgContas.RowCount != 0)
                {
                    for (int i = 0; i < dgContas.RowCount; i++)
                    {
                        if (dgContas.Rows[i].Selected == true)
                        {
                            if (Convert.ToString(dgContas.Rows[i].Cells["PAG_STATUS"].Value) != "Q")
                            {
                                if (Convert.ToString(dgContas.Rows[i].Cells["PD_STATUS"].Value) == "N")
                                {
                                    MessageBox.Show("ATENÇÃO: Esta conta ainda não foi quitada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }

                            if (MessageBox.Show("Confirma o estorno da quitação?", "Contas à Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                Cursor = Cursors.WaitCursor;

                                Pagar pg = new Pagar();
                                pg.EmpCodigo = Principal.empAtual;
                                pg.PagCodigo = Convert.ToInt32(dgContas.Rows[i].Cells["PAG_CODIGO"].Value);
                                pg.PagStatus = "N";
                                pg.ID = Convert.ToInt32(dgContas.Rows[i].Cells["ID"].Value);

                                try
                                {
                                    BancoDados.AbrirTrans();

                                    if (pg.AtualizaStatusPagto(Principal.empAtual, Convert.ToInt32(dgContas.Rows[i].Cells["PAG_CODIGO"].Value),
                                          "N", dgContas.Rows[i].Cells["PAG_STATUS"].Value.ToString()).Equals(false))
                                    {
                                        BancoDados.ErroTrans();
                                        return;
                                    }

                                    PagarDetalhe pgDetalhe = new PagarDetalhe()
                                    {
                                        EmpCodigo = Principal.empAtual,
                                        PagCodigo = Convert.ToInt32(dgContas.Rows[i].Cells["PAG_CODIGO"].Value),
                                        PdParcela = Convert.ToInt32(dgContas.Rows[i].Cells["PD_PARCELA"].Value),
                                        PdValor = Convert.ToDouble(dgContas.Rows[i].Cells["PD_VALOR"].Value),
                                        PdStatus = "N"
                                    };

                                    if (pgDetalhe.CancelarQuitacao(pgDetalhe))
                                    {
                                        BancoDados.FecharTrans();
                                    }
                                    else
                                        BancoDados.ErroTrans();

                                    if (pg.ID != 0)
                                    {
                                        AtualizaStatusPagamento(pg);
                                        
                                        AtualizaStatusPagamentoDetlahe(pgDetalhe, pg.ID);
                                    }
                                }
                                catch (Exception)
                                {
                                    BancoDados.ErroTrans();
                                }
                            }
                            else
                                return;
                        }
                    }

                    MessageBox.Show("Conta(s) estornada(s) com sucesso!", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    CarregarContas();
                }
                else
                {
                    MessageBox.Show("Necessário realizar uma busca para Cancelar uma Quitação.", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnCancelar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if (dgContas.RowCount != 0)
            {
                for (int i = 0; i < dgContas.RowCount; i++)
                {
                    if (dgContas.Rows[i].Selected == true)
                    {
                        if (dgContas.Rows[i].Cells[1].Value.ToString().Equals("Q"))
                        {
                            MessageBox.Show("ATENÇÃO: Esta conta já está quitada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (dgContas.Rows[i].Cells["PD_STATUS"].Value.ToString().Equals("S"))
                        {
                            MessageBox.Show("ATENÇÃO: Esta parcela já está quitada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            if (!Quitar(true, i))
                                return;
                        }
                    }
                }

                MessageBox.Show("Conta(s) quitada(s) com sucesso!", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                CarregarContas();

            }
            else
            {
                MessageBox.Show("Necessário realizar uma busca para realizar uma Quitação.", "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnQuitar.Focus();
            }
        }


        public async void AtualizaStatusPagamento(Pagar dados)
        {
            await EnviaOuRecebeDadosMysql.AtualizaPagar(dados);
        }

        public async void AtualizaStatusPagamentoDetlahe(PagarDetalhe dados, int id)
        {
            await EnviaOuRecebeDadosMysql.AtualizaPagarDetalhe(dados, id);
        }

        public bool Quitar(bool total, int indice)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //VERIFICA SE POSSUI MAIS DE UMA PARCELA PARA ATUALIZAR O STATUS NA TABELA PAGAR//
                Pagar pg = new Pagar();
               
                pg.EmpCodigo = Principal.empAtual;
                pg.PagCodigo = Convert.ToInt32(dgContas.Rows[indice].Cells["PAG_CODIGO"].Value);
                pg.ID = Convert.ToInt32(dgContas.Rows[indice].Cells["ID"].Value);
                pg.PagStatus = "N";

                qtdParc = pg.QtdeParcelas(pg, true);

                pg.PagStatus = qtdParc == 1 ? "Q" : "N";

                BancoDados.AbrirTrans();

                if (pg.AtualizaStatusPagto(Principal.empAtual, Convert.ToInt32(dgContas.Rows[indice].Cells["PAG_CODIGO"].Value),
                                    qtdParc == 1 ? "Q" : "N",
                                    dgContas.Rows[indice].Cells["PAG_STATUS"].Value.ToString()).Equals(false))
                {
                    BancoDados.ErroTrans();
                    return false;
                }

                PagarDetalhe pDetalhe = new PagarDetalhe()
                {
                    EmpCodigo = Principal.empAtual,
                    PagCodigo = Convert.ToInt32(dgContas.Rows[indice].Cells["PAG_CODIGO"].Value),
                    PdParcela = Convert.ToInt32(dgContas.Rows[indice].Cells["PD_PARCELA"].Value),
                    PdSaldo = 0, 
                    PdStatus = "Q"
                };

                if (pDetalhe.Quitacao(pDetalhe).Equals(false))
                {
                    BancoDados.ErroTrans();
                    return false;
                }
                
                BancoDados.FecharTrans();

                if(pg.ID !=0)
                {
                    if(qtdParc == 1)
                    {
                        AtualizaStatusPagamento(pg);
                    }

                    AtualizaStatusPagamentoDetlahe(pDetalhe, pg.ID);
                }
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        
        public void Limpar()
        {
            dtContas.Rows.Clear();
            dgContas.DataSource = dtContas;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            tslRegistros.Text = "";
            txtDInicial.Focus();
            Funcoes.LimpaFormularios(this);
            rdbAberto.Checked = true;
            rdbOTodas.Checked = true;
            lblTotal.Text = "0,00";
        }

        public void Primeiro()
        {

            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;

            CarregarContas();
        }

        public void Proximo() { }

        public void Ultimo() { }

        public void Anterior() { }

        public void ImprimirRelatorio() { }

        public bool Atualiza()
        {
            return false;
        }

        public bool Incluir()
        {
            return true;
        }

        public void AtalhoGrade()
        {
            btnPesquisar.PerformClick();
        }

        public void AtalhoFicha() 
        {
            btnExportar.PerformClick();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var finDespesas = Application.OpenForms.OfType<frmFinDespesas>().FirstOrDefault();
            if (finDespesas == null)
            {
                if (Principal.contFormularios <= 16)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    frmFinDespesas despesas = new frmFinDespesas(Principal.mdiPrincipal);
                    despesas.MdiParent = Principal.mdiPrincipal;
                    despesas.Botao();
                    despesas.Show();
                    despesas.tcDespesas.SelectedTab = despesas.tpFicha;
                    despesas.IncluirDespesas();

                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                finDespesas.Sair();
                Principal.contFormularios = Principal.contFormularios + 1;
                frmFinDespesas despesas = new frmFinDespesas(Principal.mdiPrincipal);
                despesas.MdiParent = Principal.mdiPrincipal;
                despesas.Botao();
                despesas.Show();
                despesas.tcDespesas.SelectedTab = despesas.tpFicha;
                despesas.IncluirDespesas();
            }
        }
        
        private void dgContas_KeyDown(object sender, KeyEventArgs e)
        {
           
            int indice = dgContas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && indiceGrid < indice)
            {
                indiceGrid = indiceGrid + 1;
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && indiceGrid < indice)
                {
                    indiceGrid = indiceGrid - 1;
                }
                else
                    if (e.KeyCode == Keys.Enter && indiceGrid < indice)
                    {
                        indiceGrid = 0;
                    }
                    else
                    {
                        teclaAtalho(e);
                    }
            
        }

        private void dgContas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if ((DateTime.Compare(Convert.ToDateTime(dgContas.Rows[e.RowIndex].Cells[2].Value), Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy 00:00:00"))) < 0) 
                && dgContas.Rows[e.RowIndex].Cells["PD_STATUS"].Value.ToString().Equals("N"))
            {
                dgContas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.Red;
            }
            else if (dgContas.Rows[e.RowIndex].Cells["PD_STATUS"].Value.ToString().Equals("Q"))
            {
                dgContas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Navy;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.Navy;
            }
            else if (dgContas.Rows[e.RowIndex].Cells["PD_STATUS"].Value.ToString().Equals("P"))
            {
                dgContas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Coral;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.Coral;
            }
            else
            {
                dgContas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
                dgContas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.DarkGreen;
            }
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    btnIncluir.PerformClick();
                    break;
                case Keys.F10:
                    btnExcluir.PerformClick();
                    break;
                case Keys.F11:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F12:
                    btnQuitar.PerformClick();
                    break;
            }
        }

        private void txtDInicial_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtDFinal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtDocto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtForn_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbAberto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbQuitada_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbTodas_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbEntrada_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbDespesas_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbOTodas_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
