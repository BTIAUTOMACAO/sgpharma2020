﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro.Relatorios
{
    public partial class frmRelCartaCobranca : Form
    {
        private frmFinCartaCobranca relatorio;
        private DataTable dtRelatorio;
        
        public frmRelCartaCobranca(frmFinCartaCobranca frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
            this.WindowState = FormWindowState.Maximized;
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        private void frmRelCartaCobranca_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[7];
            var estabelecimento = new Estabelecimento();
            List<Estabelecimento> dados = estabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
            parametro[0] = new ReportParameter("cidade", dados[0].EstCidade);
            parametro[1] = new ReportParameter("nomeEmpresa", dados[0].EstFantasia);
            parametro[2] = new ReportParameter("endereco", dados[0].EstEndereco);
            parametro[3] = new ReportParameter("cidadeEmpresa", dados[0].EstCidade);
            parametro[4] = new ReportParameter("ufEmpresa", dados[0].EstUf);
            parametro[5] = new ReportParameter("foneEmpresa", dados[0].EstFone);
            parametro[6] = new ReportParameter("juros", Funcoes.LeParametro(4, "52", false));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var relatorio = new ReportDataSource("dsCarta", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(relatorio);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var parcelas = new CobrancaParcela();
            DataTable dtParcelas = parcelas.BuscaCobrancaParcelaPorCliente(e.Parameters["ID"].Values.First(), relatorio.dtInicial.Value, relatorio.dtFinal.Value,
                Principal.empAtual, Principal.estAtual);

            var itens = new ReportDataSource("dsParcelas", dtParcelas);
            e.DataSources.Add(itens);
        }
    }
}
