﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro.Relatorios
{
    public partial class frmRelatorioQuitadoAna : Form
    {
        private frmRelTitlosPagos relatorio;
        private DataTable dtRelatorio;

        public frmRelatorioQuitadoAna(frmRelTitlosPagos frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
            this.WindowState = FormWindowState.Maximized;
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        private void frmRelatorioQuitadoAna_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Text);
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Text);
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var relatorio = new ReportDataSource("dsQuitadoAna", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(relatorio);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new VendasItens();
            DataTable dtItens = tItens.BuscaProdutosPorVendaId(Principal.estAtual, Principal.empAtual, e.Parameters["ID"].Values.First());

            var itens = new ReportDataSource("dsItens", dtItens);
            e.DataSources.Add(itens);
        }
    }
}