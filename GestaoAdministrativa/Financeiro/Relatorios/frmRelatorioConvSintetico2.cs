﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro.Relatorios
{
    public partial class frmRelatorioConvSintetico2 : Form
    {
        private frmRelEmpresaConvenio relatorio;
        private DataTable dtRelatorio;

        public frmRelatorioConvSintetico2(frmRelEmpresaConvenio frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmRelatorioConvSintetico2_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;

        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var relatorio = new ReportDataSource("dsSinteticoEmissao", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(relatorio);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var vendas = new VendasItens();
            DataTable dtVendas = vendas.BuscaVendaConveniadPorEmissao(e.Parameters["ID"].Values.First(), Principal.estAtual, Principal.empAtual,
                relatorio.dtInicial.Value.ToString("dd/MM/yyyy"), relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));

            var itens = new ReportDataSource("dsVendas", dtVendas);
            e.DataSources.Add(itens);
        }
    }
}
