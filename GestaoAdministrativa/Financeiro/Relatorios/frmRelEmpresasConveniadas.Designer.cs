﻿namespace GestaoAdministrativa.Financeiro.Relatorios
{
    partial class frmRelEmpresasConveniadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelEmpresasConveniadas));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkQuitados = new System.Windows.Forms.CheckBox();
            this.cmbEmpresa = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbSintetico = new System.Windows.Forms.RadioButton();
            this.rdbAnalitico = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbEmissao = new System.Windows.Forms.RadioButton();
            this.rdbVencimento = new System.Windows.Forms.RadioButton();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnBuscar);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkQuitados);
            this.groupBox4.Controls.Add(this.cmbEmpresa);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(442, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(375, 117);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Empresa Conveniada";
            // 
            // chkQuitados
            // 
            this.chkQuitados.AutoSize = true;
            this.chkQuitados.ForeColor = System.Drawing.Color.Navy;
            this.chkQuitados.Location = new System.Drawing.Point(7, 73);
            this.chkQuitados.Name = "chkQuitados";
            this.chkQuitados.Size = new System.Drawing.Size(162, 20);
            this.chkQuitados.TabIndex = 234;
            this.chkQuitados.Text = "Listar títulos quitados";
            this.chkQuitados.UseVisualStyleBackColor = true;
            // 
            // cmbEmpresa
            // 
            this.cmbEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpresa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmpresa.ForeColor = System.Drawing.Color.Black;
            this.cmbEmpresa.FormattingEnabled = true;
            this.cmbEmpresa.Location = new System.Drawing.Point(7, 31);
            this.cmbEmpresa.Name = "cmbEmpresa";
            this.cmbEmpresa.Size = new System.Drawing.Size(357, 24);
            this.cmbEmpresa.TabIndex = 233;
            this.cmbEmpresa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEmpresa_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbSintetico);
            this.groupBox2.Controls.Add(this.rdbAnalitico);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(311, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(125, 117);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo";
            // 
            // rdbSintetico
            // 
            this.rdbSintetico.AutoSize = true;
            this.rdbSintetico.ForeColor = System.Drawing.Color.Navy;
            this.rdbSintetico.Location = new System.Drawing.Point(23, 73);
            this.rdbSintetico.Name = "rdbSintetico";
            this.rdbSintetico.Size = new System.Drawing.Size(82, 20);
            this.rdbSintetico.TabIndex = 1;
            this.rdbSintetico.TabStop = true;
            this.rdbSintetico.Text = "Sintético";
            this.rdbSintetico.UseVisualStyleBackColor = true;
            this.rdbSintetico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSintetico_KeyDown);
            // 
            // rdbAnalitico
            // 
            this.rdbAnalitico.AutoSize = true;
            this.rdbAnalitico.ForeColor = System.Drawing.Color.Navy;
            this.rdbAnalitico.Location = new System.Drawing.Point(23, 29);
            this.rdbAnalitico.Name = "rdbAnalitico";
            this.rdbAnalitico.Size = new System.Drawing.Size(82, 20);
            this.rdbAnalitico.TabIndex = 0;
            this.rdbAnalitico.TabStop = true;
            this.rdbAnalitico.Text = "Analítico";
            this.rdbAnalitico.UseVisualStyleBackColor = true;
            this.rdbAnalitico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAnalitico_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdbEmissao);
            this.groupBox3.Controls.Add(this.rdbVencimento);
            this.groupBox3.Controls.Add(this.dtFinal);
            this.groupBox3.Controls.Add(this.dtInicial);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(8, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(297, 117);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Período Data";
            // 
            // rdbEmissao
            // 
            this.rdbEmissao.AutoSize = true;
            this.rdbEmissao.ForeColor = System.Drawing.Color.Navy;
            this.rdbEmissao.Location = new System.Drawing.Point(9, 88);
            this.rdbEmissao.Name = "rdbEmissao";
            this.rdbEmissao.Size = new System.Drawing.Size(78, 20);
            this.rdbEmissao.TabIndex = 8;
            this.rdbEmissao.TabStop = true;
            this.rdbEmissao.Text = "Emissão";
            this.rdbEmissao.UseVisualStyleBackColor = true;
            this.rdbEmissao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbEmissao_KeyDown);
            // 
            // rdbVencimento
            // 
            this.rdbVencimento.AutoSize = true;
            this.rdbVencimento.ForeColor = System.Drawing.Color.Navy;
            this.rdbVencimento.Location = new System.Drawing.Point(9, 62);
            this.rdbVencimento.Name = "rdbVencimento";
            this.rdbVencimento.Size = new System.Drawing.Size(101, 20);
            this.rdbVencimento.TabIndex = 7;
            this.rdbVencimento.TabStop = true;
            this.rdbVencimento.Text = "Vencimento";
            this.rdbVencimento.UseVisualStyleBackColor = true;
            this.rdbVencimento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbVencimento_KeyDown);
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(197, 25);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(86, 22);
            this.dtFinal.TabIndex = 6;
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(59, 25);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(86, 22);
            this.dtInicial.TabIndex = 5;
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(151, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inicial";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(388, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Contas à Receber Empresas Conveniadas (Particular)";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(827, 11);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(88, 110);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "F1 -Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            this.btnBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBuscar_KeyDown);
            // 
            // frmRelEmpresasConveniadas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmRelEmpresasConveniadas";
            this.Text = "frmRelEmpresasConveniadas";
            this.Load += new System.EventHandler(this.frmRelEmpresasConveniadas_Load);
            this.Shown += new System.EventHandler(this.frmRelEmpresasConveniadas_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRelEmpresasConveniadas_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.DateTimePicker dtFinal;
        public System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbSintetico;
        private System.Windows.Forms.RadioButton rdbAnalitico;
        public System.Windows.Forms.ComboBox cmbEmpresa;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.CheckBox chkQuitados;
        public System.Windows.Forms.RadioButton rdbEmissao;
        public System.Windows.Forms.RadioButton rdbVencimento;
    }
}