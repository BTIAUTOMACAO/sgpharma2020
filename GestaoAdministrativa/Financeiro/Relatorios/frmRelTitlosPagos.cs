﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro.Relatorios
{
    public partial class frmRelTitlosPagos : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Extrato por Cliente");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmRelTitlosPagos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmRelTitlosPagos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Extrato por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Extrato por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Extrato por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmRelTitlosPagos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Extrato por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtFBusca.Text = "";
            cmbFBusca.SelectedIndex = 0;
            txtFCidade.Text = "";
            txtFNome.Text = "";
            txtFCnpj.Text = "";
            rdbSintetico.Checked = true;
            rdbQuitados.Checked = true;
            rdbEmissao.Checked = true;
            txtFBusca.Focus();
        }

        private void txtFBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbFBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAberto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbQuitados_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                {
                    var dCliente = new Cliente();
                    DataTable cliente = new DataTable();
                    string filtro;
                    if (cmbFBusca.SelectedIndex == 0)
                    {
                        cliente = dCliente.DadosClienteFiltro(txtFBusca.Text.Trim().ToUpper(), 0, out filtro);
                    }
                    else if (cmbFBusca.SelectedIndex == 1)
                    {
                        cliente = dCliente.DadosClienteFiltro(txtFBusca.Text.Trim().ToUpper(), 1, out filtro);
                    }
                    if (cliente.Rows.Count.Equals(1))
                    {
                        txtFNome.Text = cliente.Rows[0]["CF_NOME"].ToString();
                        txtFCnpj.Text = cliente.Rows[0]["CF_DOCTO"].ToString(); ;
                        txtFCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString() + "/" + cliente.Rows[0]["CF_UF"].ToString();
                        cmbFBusca.SelectedIndex = 0;
                        dtInicial.Focus();
                    }
                    else if (cliente.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbFBusca.SelectedIndex = 0;
                        txtFBusca.Text = "";
                        txtFBusca.Focus();
                    }
                    else
                    {
                        using (var t = new frmBuscaForn("cliente"))
                        {
                            t.dgCliFor.DataSource = cliente;
                            t.ShowDialog();
                       
                            if (!String.IsNullOrEmpty(t.cfNome))
                            {
                                txtFNome.Text = t.cfNome;
                                txtFCnpj.Text = t.cfDocto;
                                txtFCidade.Text = t.cfCidade;
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                dtInicial.Focus();
                            }
                            else
                            {
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                txtFBusca.Focus();
                            }
                        }
                    }
                }
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAberto.Focus();
        }

        private void rdbAnalitico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSintetico.Focus();
        }

        private void rdbSintetico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAberto.Focus();
        }

        private void rdbAberto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.Focus();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var busca = new Cobranca();
                DataTable dtRetorno = new DataTable();
                if (rdbSintetico.Checked)
                {
                    if (rdbQuitados.Checked)
                    {
                        dtRetorno = busca.BuscaContasPagasSintetico(Principal.estAtual, Principal.empAtual, txtFNome.Text.ToUpper(),
                            dtInicial.Text, dtFinal.Text, rdbEmissao.Checked ? 0 : 1);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmRelatorioQuitadoSint relatorio = new frmRelatorioQuitadoSint(this, dtRetorno);
                            relatorio.Text = "Extrato por Cliente Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Relatório de Empresas Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaContasAbertasSintetico(Principal.estAtual, Principal.empAtual, txtFNome.Text.ToUpper(),
                           dtInicial.Text, dtFinal.Text, rdbEmissao.Checked ? 0 : 1);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmRelatorioAbertoSint relatorio = new frmRelatorioAbertoSint(this, dtRetorno);
                            relatorio.Text = "Extrato por Cliente Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Relatório de Empresas Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                        }
                    }


                }
                else
                {
                    if (rdbQuitados.Checked)
                    {
                        dtRetorno = busca.BuscaContasPagasSintetico(Principal.estAtual, Principal.empAtual, txtFNome.Text.ToUpper(),
                            dtInicial.Text, dtFinal.Text, rdbEmissao.Checked ? 0 : 1);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmRelatorioQuitadoAna relatorio = new frmRelatorioQuitadoAna(this, dtRetorno);
                            relatorio.Text = "Extrato por Cliente Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Relatório de Empresas Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaContasAbertasSintetico(Principal.estAtual, Principal.empAtual, txtFNome.Text.ToUpper(),
                           dtInicial.Text, dtFinal.Text, rdbEmissao.Checked ? 0 : 1);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmRelatorioAbertoAnal relatorio = new frmRelatorioAbertoAnal(this, dtRetorno);
                            relatorio.Text = "Extrato por Cliente Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Relatório de Empresas Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Extrato por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
