﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinDespesas : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private double valorTotal;
        private double valorParcelas;
        private double valorSaldo;
        private double valorQuitado;
        private DataTable dtDespesas = new DataTable();
        private int contRegistros;
        public bool emGrade;
        private ToolStripButton tsbDespesas = new ToolStripButton("Lanc. de Despesas");
        private ToolStripSeparator tssDespesas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private bool permiteAtu;
        private string idNuvem;
        #endregion

        public frmFinDespesas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinDespesas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dtPVencimento.Value = DateTime.Now;
                dtEmissao.Value = DateTime.Now;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTValor.Focus();
        }

        private void txtTValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTDesp.Focus();
        }

        private void txtTDesp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbTDespesa.Focus();
        }

        private void cmbTDespesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFBusca.Focus();
        }

        private void cmbFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFBusca.Focus();
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                    {
                        var buscaFornecedor = new Cliente();
                        List<Cliente> cliente = new List<Cliente>();
                        if (cmbFBusca.SelectedIndex == 0)
                        {
                            cliente = buscaFornecedor.BuscaDadosFornecedor(1, txtFBusca.Text.Trim().ToUpper(), 1);
                        }
                        else if (cmbFBusca.SelectedIndex == 1)
                        {
                            long cnpj = Convert.ToInt64(txtFBusca.Text);
                            cliente = buscaFornecedor.BuscaDadosFornecedor(2, String.Format(@"{0:00\.000\.000\/0000\-00}", cnpj), 1);
                        }
                        if (cliente.Count.Equals(1))
                        {
                            txtFNome.Text = cliente[0].CfNome;
                            txtFCnpj.Text = cliente[0].CfDocto;
                            txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                            cmbFBusca.SelectedIndex = 0;
                            txtPTotal.Focus();
                        }
                        else if (cliente.Count.Equals(0))
                        {
                            MessageBox.Show("Fornecedor não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtFBusca.Focus();
                        }
                        else
                        {
                            using (var t = new frmBuscaForn("fornecedor"))
                            {
                                for (int i = 0; i < cliente.Count; i++)
                                {
                                    t.dgCliFor.Rows.Insert(t.dgCliFor.RowCount, new Object[] { cliente[i].CodigoConveniada, cliente[i].CfCodigo, cliente[i].CfNome, cliente[i].CfApelido, cliente[i].CfObservacao,
                                    cliente[i].CfDocto, cliente[i].CfTelefone, cliente[i].CfEndereco, cliente[i].CfBairro, cliente[i].CfCidade, cliente[i].CfUF, cliente[i].CfId, "0", "0", "0", cliente[i].CfStatus });
                                }
                                t.ShowDialog();
                            }

                            if (!String.IsNullOrEmpty(Principal.nomeCliFor))
                            {
                                txtFNome.Text = Principal.nomeCliFor;
                                txtFCnpj.Text = Principal.cnpjCliFor;
                                txtFCidade.Text = Principal.cidadeCliFor;
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                txtPTotal.Focus();
                            }
                            else
                            {
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                txtFBusca.Focus();
                            }

                        }

                    }
                    else
                        txtPTotal.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtPVencimento.Focus();
        }

        private void dtPVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (permiteAtu.Equals(false))
                    {
                        MessageBox.Show("Despesa já possui pagamentos efetuados. Alteração não Permitida!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CarregarDados(contRegistros);
                        return;
                    }
                    if ((Convert.ToDecimal(txtPTotal.Text) <= 0))
                    {
                        MessageBox.Show("Valor total da nota não pode ser zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTValor.Focus();
                        return;
                    }
                    if (dtPVencimento.Value <= DateTime.Now)
                    {
                        MessageBox.Show("Vencimento não pode ser igual ou inferior a data de hoje!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtPVencimento.Focus();
                        return;
                    }
                    else
                    {
                        valorTotal = 0;
                        foreach (DataGridViewRow col in dgParcelas.Rows)
                        {
                            valorTotal = valorTotal + Convert.ToDouble(col.Cells[0].Value);
                        }

                        if ((Convert.ToDouble(txtTValor.Text) - valorTotal) >= Convert.ToDouble(txtPTotal.Text))
                        {
                            dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { Convert.ToDecimal(txtPTotal.Text).ToString("##0.00"), dtPVencimento.Value.ToString("dd/MM/yyyy"), "N" });
                            if((Convert.ToDouble(txtTValor.Text) - valorTotal) == Convert.ToDouble(txtPTotal.Text))
                            {
                                txtPTotal.Text = "0,00";
                                txtHistorico.Focus();
                            }
                            else
                            {
                                txtPTotal.Text = "0,00";
                                txtPTotal.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Valor das parcelas está diferente do valor total da nota", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtHistorico.Focus();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtTDesp_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbTDespesa, txtTDesp);
            if (cmbTDespesa.SelectedIndex != -1)
                txtFBusca.Focus();
        }

        private void cmbTDespesa_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtTDesp.Text = Convert.ToString(cmbTDespesa.SelectedValue);
        }

        public bool CarregaCombos()
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("DESP_CODIGO", "DESP_DESCRICAO", "CAD_DESPESAS", false, "DESP_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    cmbBDespesa.DataSource = Principal.dtPesq;
                    cmbBDespesa.DisplayMember = "DESP_DESCRICAO";
                    cmbBDespesa.ValueMember = "DESP_CODIGO";
                    cmbBDespesa.SelectedIndex = -1;

                    Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("DESP_CODIGO", "DESP_DESCRICAO", "CAD_DESPESAS", false, "DESP_DESABILITADO = 'N'");

                    cmbTDespesa.DataSource = Principal.dtPesq;
                    cmbTDespesa.DisplayMember = "DESP_DESCRICAO";
                    cmbTDespesa.ValueMember = "DESP_CODIGO";
                    cmbTDespesa.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de despesa,\npara lançar uma despesa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }

                //CARREGA TIPO DE DOCUMENTO//
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("TIP_CODIGO", "TIP_DESC_ABREV", "TIPO_DOCTO", false, "TIP_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    cmbDocto.DataSource = Principal.dtPesq;
                    cmbDocto.DisplayMember = "TIP_DESC_ABREV";
                    cmbDocto.ValueMember = "TIP_CODIGO";
                    cmbDocto.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de documento,\npara lançar uma despesa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                return true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmFinDespesas_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        public void Primeiro()
        {
            if (dtDespesas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcDespesas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtDespesas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDespesas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDespesas.CurrentCell = dgDespesas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmFinDespesas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
            if (tcDespesas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcDespesas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtPagarID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbDespesas.AutoSize = false;
                this.tsbDespesas.Image = Properties.Resources.financeiro;
                this.tsbDespesas.Size = new System.Drawing.Size(140, 20);
                this.tsbDespesas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDespesas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDespesas);
                tsbDespesas.Click += delegate
                {
                    var cadDespesas = Application.OpenForms.OfType<frmFinDespesas>().FirstOrDefault();
                    BotoesHabilitados();
                    cadDespesas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtDespesas.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDespesas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDespesas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            #region LIMPAR CAMPOS
            dtDespesas.Clear();
            dgDespesas.DataSource = dtDespesas;
            cmbTDespesa.SelectedIndex = -1;
            cmbFBusca.SelectedIndex = 1;
            cmbDocto.SelectedIndex = -1;
            dgParcelas.Rows.Clear();
            Funcoes.LimpaFormularios(this);
            txtTValor.Text = "0,00";
            txtPTotal.Text = "0,00";
            dtPVencimento.Value = DateTime.Now;
            contRegistros = 0;
            valorParcelas = 0;
            valorSaldo = 0;
            valorTotal = 0;
            valorQuitado = 0;
            permiteAtu = false;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            #endregion
        }

        public void LimparFicha()
        {
            #region LIMPAR CAMPOS
            txtPagarID.Text = "";
            dtEmissao.Value = DateTime.Now;
            txtDocto.Text = "";
            txtTValor.Text = "0,00";
            txtTDesp.Text = "";
            cmbTDespesa.SelectedIndex = -1;
            cmbFBusca.SelectedIndex = 0;
            cmbDocto.SelectedIndex = -1;
            txtFBusca.Text = "";
            txtFNome.Text = "";
            txtFCnpj.Text = "";
            txtFCidade.Text = "";
            txtPTotal.Text = "0,00";
            dtPVencimento.Value = DateTime.Now;
            dgParcelas.Rows.Clear();
            txtHistorico.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            valorParcelas = 0;
            valorSaldo = 0;
            valorTotal = 0;
            valorQuitado = 0;
            permiteAtu = true;
            dtEmissao.Focus();
            #endregion
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcDespesas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                dgParcelas.Rows.Clear();
                valorTotal = 0;
                valorSaldo = 0;
                valorParcelas = 0;

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtPagarID.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["PAG_CODIGO"].ToString());
                dtEmissao.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["PAG_DATA"].ToString());
                txtTValor.Text = String.Format("{0:N}", dtDespesas.Rows[linha]["PAG_VALPRIN"]);
                cmbDocto.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["TIP_DESC_ABREV"].ToString());
                txtDocto.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["PAG_DOCTO"].ToString());
                cmbTDespesa.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["DESP_DESCRICAO"].ToString());
                txtTDesp.Text = Convert.ToString(cmbTDespesa.SelectedValue);
                txtFCnpj.Text = String.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(Funcoes.RemoveCaracter(dtDespesas.Rows[linha]["CF_DOCTO"].ToString())));

                if(dtDespesas.Rows[linha]["PAG_STATUS"].ToString().Equals("QUITADO") || dtDespesas.Rows[linha]["PAG_STATUS"].ToString().Equals("PARCIAL"))
                {
                    permiteAtu = false;
                }
                else
                {
                    permiteAtu = true;
                }

                var buscaFornecedor = new Cliente();
                List<Cliente> cliente = buscaFornecedor.BuscaDadosFornecedor(2, txtFCnpj.Text, 1);
                if (!cliente.Count.Equals(0))
                {
                    txtFNome.Text = cliente[0].CfNome;
                    txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                }

                txtHistorico.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["HISTORICO"].ToString());

                txtData.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtDespesas.Rows[linha]["OPALTERACAO"].ToString());

                cmbFBusca.SelectedIndex = 1;

                var dadosParcela = new PagarDetalhe();
                List<PagarDetalhe> dPagto = dadosParcela.BuscaParcelas(Principal.empAtual, int.Parse(txtPagarID.Text));
                for (int i = 0; i < dPagto.Count; i++)
                {
                    dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { dPagto[i].PdValor, Convert.ToDateTime(dPagto[i].PdVencimento).ToString("dd/MM/yyyy"), dPagto[i].PdStatus, dPagto[i].PdSaldo });

                    valorTotal += dPagto[i].PdValor;

                    if (dPagto[i].PdSaldo < dPagto[i].PdValor)
                    {
                        valorSaldo += dPagto[i].PdValor - dPagto[i].PdSaldo;
                    }

                    if (dPagto[i].PdStatus.Equals("S"))
                    {
                        valorQuitado += dPagto[i].PdValor;
                    }

                }

                txtPTotal.Text = "0,00";

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, linha));
                dtEmissao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcDespesas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcDespesas.SelectedTab == tpGrade)
            {
                dgDespesas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcDespesas.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmFinDespesas");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnLimpar.Enabled = true;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        cmbFBusca.SelectedIndex = 0;
                        permiteAtu = true;
                        dtEmissao.Focus();
                        emGrade = false;
                    }
                }
        }

        public void IncluirDespesas()
        {
            Funcoes.BotoesCadastro("frmFinDespesas");
            lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
            cmbFBusca.SelectedIndex = 1;
            dtEmissao.Focus();
            emGrade = false;
            
        }

        public bool Excluir()
        {
            try
            {
                if ((txtPagarID.Text.Trim() != "") && (txtDocto.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da despesa e suas parcelas?", "Exclusão tabela Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirDespesas(Principal.empAtual, txtPagarID.Text))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var excluir = new Pagar();

                            idNuvem = Util.SelecionaCampoEspecificoDaTabela("PAGAR", "ID", "PAG_CODIGO", txtPagarID.Text, false, true, true);
                          
                            if (excluir.ExcluiParcPagar(Principal.empAtual,txtPagarID.Text.Trim()).Equals(1))
                            {
                                ExcluirPagaEPagarDetalheNuvem(idNuvem);

                                Funcoes.GravaLogExclusao("PAGAR_CODIGO", txtPagarID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PAGAR", txtDocto.Text.ToUpper(), Principal.motivo, Principal.estAtual, Principal.empAtual);
                                dtDespesas.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgDespesas.DataSource = dtDespesas;
                                tcDespesas.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (permiteAtu.Equals(false))
            {
                Principal.mensagem = "Despesa já possui pagamentos efetuados. Alteração não Permitida!";
                Funcoes.Avisa();
                CarregarDados(contRegistros);
                return false;
            }
            if (Convert.ToDouble(txtTValor.Text) <= 0)
            {
                Principal.mensagem = "Valor não pode ser zero.";
                Funcoes.Avisa();
                txtTValor.Focus();
                return false;
            }
            if (cmbDocto.SelectedIndex == -1)
            {
                Principal.mensagem = "Selecione um tipo de docto.";
                Funcoes.Avisa();
                cmbDocto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDocto.Text.Trim()))
            {
                Principal.mensagem = "Número do documento não pode ser em branco.";
                Funcoes.Avisa();
                txtDocto.Focus();
                return false;
            }
            if (cmbTDespesa.SelectedIndex == -1)
            {
                Principal.mensagem = "Selecione um tipo de despesa.";
                Funcoes.Avisa();
                cmbTDespesa.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFCnpj.Text))
            {
                Principal.mensagem = "Forncedor não pode ser em branco.";
                Funcoes.Avisa();
                txtFBusca.Focus();
                return false;
            }
            if (dgParcelas.RowCount == 0)
            {
                Principal.mensagem = "Parcela(s) não pode(m) ser em branco.";
                Funcoes.Avisa();
                txtPTotal.Focus();
                return false;
            }
            valorParcelas = 0;
            for (int i = 0; i < dgParcelas.RowCount; i++)
            {
                valorParcelas = valorParcelas + Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);
            }
            if (valorParcelas != Convert.ToDouble(txtTValor.Text))
            {
                Principal.mensagem = "O valor das parcelas não está igual ao valor total.";
                Funcoes.Avisa();
                txtPTotal.Focus();
                return false;
            }
            return true;
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos())
                {
                    BancoDados.AbrirTrans();

                    Pagar pgto = new Pagar
                    {
                        EmpCodigo = Principal.empAtual,
                        PagCodigo = int.Parse(txtPagarID.Text),
                        CfDocto = txtFCnpj.Text,
                        PagDocto = txtDocto.Text.Trim().ToUpper(),
                        PagData = dtEmissao.Value,
                        PagValorPrincipal = Convert.ToDouble(txtTValor.Text),
                        TipoCodigo = int.Parse(Convert.ToDouble(cmbDocto.SelectedValue).ToString()),
                        PagComprador = Convert.ToInt32(Funcoes.LeParametro(7, "01", true)),
                        DespCodigo = int.Parse(Convert.ToDouble(cmbTDespesa.SelectedValue).ToString()),
                        Historico = txtHistorico.Text.Trim().ToUpper(),
                        PagStatus = "N",
                        PagOrigem = "D",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    PagarDetalhe pgtoDt = new PagarDetalhe();

                    if(pgtoDt.ExcluiFaturaDetalhe(Principal.empAtual, txtPagarID.Text, true) == -1)
                    {
                        BancoDados.ErroTrans();
                        return false;
                    }

                    if(pgto.ExcluiFatura(Principal.empAtual, txtPagarID.Text, true) == -1)
                    {
                        BancoDados.ErroTrans();
                        return false;
                    }

                    idNuvem = Util.SelecionaCampoEspecificoDaTabela("PAGAR", "ID", "PAG_CODIGO", txtPagarID.Text, false, true, true);
                    ExcluirPagaEPagarDetalheNuvem(idNuvem);

                    Funcoes.GravaLogExclusao("PAGAR_CODIGO", txtPagarID.Text, Principal.usuario, DateTime.Now, "PAGAR", txtDocto.Text.ToUpper(), Principal.motivo, Principal.estAtual, Principal.empAtual, true);

                    if (!pgto.InsereRegistroFatura(pgto))
                    {
                        BancoDados.ErroTrans();
                        return false;
                    }

                    List<PagarDetalhe> detalhe = new List<PagarDetalhe>();

                    for (int i = 0; i < dgParcelas.RowCount; i++)
                    {
                        pgtoDt.EmpCodigo = Principal.empAtual;
                        pgtoDt.PagCodigo = int.Parse(txtPagarID.Text);
                        pgtoDt.PagDocto = txtDocto.Text;
                        pgtoDt.CfDocto = txtFCnpj.Text;
                        pgtoDt.PdParcela = (i + 1);
                        pgtoDt.PdValor = Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);
                        pgtoDt.PdVencimento = Convert.ToDateTime(dgParcelas.Rows[i].Cells[1].Value);
                        pgtoDt.PdSaldo = Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);
                        pgtoDt.PdStatus = dgParcelas.Rows[i].Cells[2].Value.ToString();
                        pgtoDt.DtCadastro = DateTime.Now;
                        pgtoDt.OpCadastro = Principal.usuario;

                        detalhe.Add(pgtoDt);

                        if (!pgtoDt.InsereRegistroFaturaDetalhe(pgtoDt))
                        {
                            BancoDados.ErroTrans();
                            return false;
                        }
                    }

                    Funcoes.GravaLogInclusao("PAG_CODIGO", txtPagarID.Text, Principal.usuario, "PAGAR", txtDocto.Text.Trim().ToUpper(), Principal.estAtual, Principal.empAtual, true);

                    //ENVIA DADOS PRA NUVEM//
                    IncluiPagaEPagarDetalheNuvem(pgto, detalhe);

                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtDespesas.Clear();
                    dgDespesas.DataSource = dtDespesas;
                    emGrade = false;
                    tslRegistros.Text = "";
                    Limpar();
                    BancoDados.FecharTrans();

                    MessageBox.Show("Atualização realizada com Sucesso", "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos())
                {
                    BancoDados.AbrirTrans();
                    txtPagarID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("PAGAR", "PAG_CODIGO", 0, "", Principal.empAtual));

                    Pagar pgto = new Pagar
                    {
                        EmpCodigo = Principal.empAtual,
                        PagCodigo = int.Parse(txtPagarID.Text),
                        CfDocto = txtFCnpj.Text,
                        PagDocto = txtDocto.Text.Trim().ToUpper(),
                        PagData = dtEmissao.Value,
                        PagValorPrincipal = Convert.ToDouble(txtTValor.Text),
                        TipoCodigo = int.Parse(Convert.ToDouble(cmbDocto.SelectedValue).ToString()),
                        PagComprador = Convert.ToInt32(Funcoes.LeParametro(7, "01", true)),
                        DespCodigo = int.Parse(Convert.ToDouble(cmbTDespesa.SelectedValue).ToString()),
                        Historico = txtHistorico.Text.Trim().ToUpper(),
                        PagStatus = "N",
                        PagOrigem = "D",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    Principal.dtBusca = Util.RegistrosPorEmpresa("PAGAR", "PAG_DOCTO", txtDocto.Text,true, true);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        if (!pgto.InsereRegistroFatura(pgto))
                        {
                            BancoDados.ErroTrans();
                            return false;
                        }

                        List<PagarDetalhe> pagDetalhe = new List<PagarDetalhe>();
                        //INCLUI NOVA LINHA NA TABELA PAGAR_DETALHE//
                        for (int i = 0; i < dgParcelas.RowCount; i++)
                        {
                            PagarDetalhe pgtoDt = new PagarDetalhe
                            {
                                EmpCodigo = Principal.empAtual,
                                PagCodigo = int.Parse(txtPagarID.Text),
                                PagDocto = txtDocto.Text,
                                CfDocto = txtFCnpj.Text,
                                PdParcela = (i + 1),
                                PdValor = Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value),
                                PdVencimento = Convert.ToDateTime(dgParcelas.Rows[i].Cells[1].Value),
                                PdSaldo = Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value),
                                PdStatus = dgParcelas.Rows[i].Cells[2].Value.ToString(),
                                DtCadastro = DateTime.Now,
                                OpCadastro = Principal.usuario
                            };

                            pagDetalhe.Add(pgtoDt);

                            if (!pgtoDt.InsereRegistroFaturaDetalhe(pgtoDt))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }

                        }
                        
                        Funcoes.GravaLogInclusao("PAG_CODIGO", txtPagarID.Text, Principal.usuario, "PAGAR", txtDocto.Text.Trim().ToUpper(), Principal.estAtual,Principal.empAtual, true);

                        //ENVIA DADOS PRA NUVEM//
                        IncluiPagaEPagarDetalheNuvem(pgto, pagDetalhe);
                        
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtDespesas.Clear();
                        dgDespesas.DataSource = dtDespesas;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        BancoDados.FecharTrans();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Documento já cadastrado", "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDocto.Focus();
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }


        public async void IncluiPagaEPagarDetalheNuvem(Pagar dados, List<PagarDetalhe> detalhePagto)
        {
            await EnviaOuRecebeDadosMysql.InserePagarEPagarDetalhe(dados, detalhePagto);
        }
        
        public async void ExcluirPagaEPagarDetalheNuvem(string IDNuvem)
        {
            await EnviaOuRecebeDadosMysql.ExcluiPagarEPagarDetalhe(IDNuvem);
        }
        
        public void ImprimirRelatorio()
        {
        }

        private void txtTValor_Validated(object sender, EventArgs e)
        {
            if (txtTValor.Text.Trim() != "")
            {
                txtTValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTValor.Text));
            }
            else
                txtTValor.Text = "0,00";

            if (valorTotal > 0)
            {
                if (Convert.ToDouble(txtTValor.Text) < valorQuitado + valorSaldo)
                {
                    MessageBox.Show("Valor Total não pode ser menor que o valor pago " + String.Format("{0:C}", valorQuitado + valorSaldo), "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTValor.Text = String.Format("{0:N}", valorQuitado + valorSaldo);
                    txtTValor.Focus();
                }
            }
        }

        private void txtPTotal_Validated(object sender, EventArgs e)
        {
            if (txtPTotal.Text.Trim() != "")
            {
                txtPTotal.Text = String.Format("{0:N}", Convert.ToDecimal(txtPTotal.Text));
            }
            else
                txtPTotal.Text = "0,00";

            if (valorTotal > 0)
            {
                if (Convert.ToDouble(txtTValor.Text) < valorQuitado + valorSaldo)
                {
                    MessageBox.Show("Valor Total não pode ser menor que o valor pago " + String.Format("{0:C}", valorQuitado + valorSaldo), "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTValor.Text = String.Format("{0:N}", valorQuitado + valorSaldo);
                    txtTValor.Focus();
                }
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBEmissao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtBEmissao.Text)))
                {
                    txtBDocto.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDocto.Text.Trim()))
                {
                    cmbBDespesa.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBDespesa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Pagar pgto = new Pagar
                {
                    EmpCodigo = Principal.estAtual,
                    PagCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    PagData = Funcoes.RemoveCaracter(txtBEmissao.Text) == "" ? Convert.ToDateTime("01/01/2001") : Convert.ToDateTime(txtBEmissao.Text),
                    PagDocto = txtBDocto.Text.Trim(),
                    DespCodigo = cmbBDespesa.SelectedIndex == -1 ? 0 : int.Parse(Convert.ToDouble(cmbBDespesa.SelectedValue).ToString())
                };

                dtDespesas = pgto.BuscarDados(pgto, out strOrdem);
                if (dtDespesas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtDespesas.Rows.Count;
                    dgDespesas.DataSource = dtDespesas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, 0));
                    dgDespesas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgDespesas.DataSource = dtDespesas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    dgParcelas.Rows.Clear();
                    Funcoes.LimpaFormularios(this);
                    cmbBDespesa.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtDespID") && (control.Name != "txtFNome") && (control.Name != "txtFCnpj") && (control.Name != "txtFCidade") &&
                        (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void dgDespesas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
            emGrade = true;
        }

        private void dgDespesas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcDespesas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgDespesas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtDespesas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtDespesas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtDespesas.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgDespesas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgDespesas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgDespesas.RowCount;
                    for (i = 0; i <= dgDespesas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgDespesas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgDespesas.ColumnCount; k++)
                    {
                        if (dgDespesas.Columns[k].Visible == true)
                        {
                            switch (dgDespesas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgDespesas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDespesas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgDespesas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDespesas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Valor Total":
                                    numCel = Principal.GetColunaExcel(dgDespesas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDespesas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgDespesas.ColumnCount : indice) + Convert.ToString(dgDespesas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgDespesas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgDespesas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtDespesas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDespesas.Columns[e.ColumnIndex].Name + " DESC");
                        dgDespesas.DataSource = dtDespesas;
                        decrescente = true;
                    }
                    else
                    {
                        dtDespesas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDespesas.Columns[e.ColumnIndex].Name + " ASC");
                        dgDespesas.DataSource = dtDespesas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDespesas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox10, "Campo Obrigatório");
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    if (dgDespesas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgDespesas.ColumnCount];
                string[] coluna = new string[dgDespesas.ColumnCount];

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    grid[i] = dgDespesas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgDespesas.ColumnCount; i++)
                {
                    if (dgDespesas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgDespesas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgDespesas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgDespesas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgDespesas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgDespesas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgDespesas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgDespesas.ColumnCount; i++)
                        {
                            dgDespesas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. de Despesas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void AtalhoGrade()
        {
            tcDespesas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcDespesas.SelectedTab = tpFicha;
        }

        private void cmbDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }



        private void dgDespesas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //if (dgDespesas.Rows[e.RowIndex].Cells[8].Value.ToString() == "PARCIAL")
            //{
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.Beige;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Beige;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
            //}
            //else if (dgDespesas.Rows[e.RowIndex].Cells[8].Value.ToString() == "QUITADO")
            //{
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.LightBlue;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
            //}
            //else if (dgDespesas.Rows[e.RowIndex].Cells[8].Value.ToString() == "ABERTO")
            //{
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.White;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor = Color.LightCoral;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightCoral;
            //    dgDespesas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
            //}
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        
    }
}
