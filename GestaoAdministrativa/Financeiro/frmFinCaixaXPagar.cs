﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinCaixaXPagar : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Caixa X Pagar");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();
        private double caixa;
        private double boleto;
        private double cheque;
        private double credito;
        private double debito;
        private double dinheiro;
        private double extra;

        public frmFinCaixaXPagar(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinCaixaXPagar_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtData.Value = DateTime.Now;
            rdbSangria.Checked = true;
            rdbDia.Checked = true;
            extra = 0;
            lblExtra.Text = "";
            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
            {
                BuscaFilial();
            }
            else
            {
                cmbGrupos.Enabled = false;
            }
        }

        public async void BuscaFilial()
        {
            cmbGrupos.Enabled = true;
            cmbGrupos.Enabled = true;
            List<EstabelecimentoFilial> dadosEstb = await EnviaOuRecebeDadosMysql.BuscaEstabelecimentos();

            if (dadosEstb.Count > 0)
            {
                cmbGrupos.Enabled = true;
                var todos = new EstabelecimentoFilial();
                todos.NomeEstabelecimento = "TODAS AS LOJAS";
                todos.CodEstabelecimento = 0;
                dadosEstb.Add(todos);
                cmbGrupos.DataSource = dadosEstb;
                cmbGrupos.ValueMember = "codEstabelecimento";
                cmbGrupos.DisplayMember = "nomeEstabelecimento";
                cmbGrupos.SelectedIndex = -1;
            }
            else
                cmbGrupos.Enabled = false;
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.financeiro;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmFinCaixaXPagar>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                bool novos = true;
                extra = 0;
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Caixa X Contas à Pagar";
                DataTable retorno = new DataTable();
                var buscaCaixa = new CaixaStatus();

                if (dgDespesas.RowCount > 0)
                {
                    novos = false;
                }

                if (dtData.Text == DateTime.Now.ToString("dd/MM/yyyy") || dtData.Value > DateTime.Now)
                {
                    Principal.mensagem = "A Data não pode igual ou maior que a Data Atual!";
                    Funcoes.Avisa();
                    dtData.Focus();
                    return;
                }

                List<MovCaixaFilial> movEspecies = new List<Negocio.MovCaixaFilial>();

                Cursor = Cursors.WaitCursor;

                if (rdbSangria.Checked)
                {
                    if (cmbGrupos.SelectedIndex < 0)
                    {
                        List<Object> excecoes = new List<object>();
                        excecoes.Add(Funcoes.LeParametro(6, "75", false));
                        excecoes.Add(Funcoes.LeParametro(4, "13", false));
                        retorno = buscaCaixa.SomaMovimentoPorOperacaoCaixa(excecoes, dtData.Value, dtData.Value);
                    }
                    else
                    {
                        if (cmbGrupos.Text == "TODAS AS LOJAS")
                        {
                            movEspecies = await EnviaOuRecebeDadosMysql.BuscaMovimento(0, dtData.Value);
                        }
                        else
                        {
                            movEspecies = await EnviaOuRecebeDadosMysql.BuscaMovimento(Convert.ToInt32(cmbGrupos.SelectedValue), dtData.Value);
                        }
                    }
                }
                else
                {
                    if(cmbGrupos.SelectedIndex < 0)
                    {
                        retorno = buscaCaixa.MovimentoPorEspecieDoDia(dtData.Value, dtData.Value);
                    }
                    else
                    {
                        if (cmbGrupos.Text == "TODAS AS LOJAS")
                        {
                            movEspecies = await EnviaOuRecebeDadosMysql.BuscaMovimentoCaixa(0, dtData.Value);
                        }
                        else
                        {
                            movEspecies = await EnviaOuRecebeDadosMysql.BuscaMovimentoCaixa(Convert.ToInt32(cmbGrupos.SelectedValue), dtData.Value);
                        }
                    }

                }

                if (cmbGrupos.SelectedIndex < 0)
                {
                    dgCaixa.DataSource = retorno;
                }
                else
                {
                    dgCaixa.Rows.Clear();
                    if (movEspecies[0].CodErro == "00")
                    {
                        for (int i = 0; i < movEspecies.Count; i++)
                        {
                            dgCaixa.Rows.Insert(dgCaixa.RowCount, new Object[] { movEspecies[i].EspDescricao, movEspecies[i].Valor });
                        }
                    }
                }

                List<DespesasFilial> despesaFilial = new List<DespesasFilial>();

                if (cmbGrupos.SelectedIndex < 0)
                {
                    //BUSCA VENCIMENTOS//
                    retorno = buscaCaixa.BuscaDespesas(dtData.Value, "N", Principal.empAtual, rdbDia.Checked ? false : true);
                    dgDespesas.DataSource = retorno;
                }
                else
                {
                    despesaFilial = await EnviaOuRecebeDadosMysql.BuscaDespesas(cmbGrupos.Text == "TODAS AS LOJAS" ? 0 : Convert.ToInt32(cmbGrupos.SelectedValue),
                        dtData.Value, "N", rdbDia.Checked ? false : true);

                    dgDespesas.Rows.Clear();
                    if (despesaFilial[0].CodErro == "00")
                    {
                        for (int i = 0; i < movEspecies.Count; i++)
                        {
                            dgDespesas.Rows.Insert(dgDespesas.RowCount, new Object[] { despesaFilial[i].PagOrigem, despesaFilial[i].PagDocto, despesaFilial[i].CfDocto,
                                despesaFilial[i].PdSaldo, despesaFilial[i].PdVencimento, despesaFilial[i].PdParcela, despesaFilial[i].Loja, despesaFilial[i].PdStatus});
                        }
                    }

                }

                if (retorno.Rows.Count > 0 || despesaFilial.Count > 0)
                {
                    if (novos)
                    {
                        DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                        combo.HeaderText = "Espécie";
                        combo.Name = "Especie";
                        ArrayList row = new ArrayList();

                        row.Add("CHEQUE");
                        row.Add("CREDITO");
                        row.Add("DEBITO");
                        row.Add("DINHEIRO");

                        combo.Items.AddRange(row.ToArray());
                        dgDespesas.Columns.Add(combo);
                    }
                }

                RecalculaValores();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public void RecalculaValores()
        {
            try
            {
                caixa = 0;
                boleto = 0;
                dinheiro = 0;
                debito = 0;
                credito = 0;
                cheque = 0;

                for (int i = 0; i < dgCaixa.RowCount; i++)
                {
                    caixa += Convert.ToDouble(dgCaixa.Rows[i].Cells["TOTAL"].Value);

                    if (dgCaixa.Rows[i].Cells["ESP_DESCRICAO"].Value.ToString().Equals("CHEQUE"))
                    {
                        cheque = Convert.ToDouble(dgCaixa.Rows[i].Cells["TOTAL"].Value);
                    }
                    else if (dgCaixa.Rows[i].Cells["ESP_DESCRICAO"].Value.ToString().Equals("CARTAO CREDITO"))
                    {
                        credito = Convert.ToDouble(dgCaixa.Rows[i].Cells["TOTAL"].Value);
                    }
                    else if (dgCaixa.Rows[i].Cells["ESP_DESCRICAO"].Value.ToString().Equals("CARTAO DEBITO"))
                    {
                        debito = Convert.ToDouble(dgCaixa.Rows[i].Cells["TOTAL"].Value);
                    }
                    else if (dgCaixa.Rows[i].Cells["ESP_DESCRICAO"].Value.ToString().Equals("DINHEIRO"))
                    {
                        dinheiro = Convert.ToDouble(dgCaixa.Rows[i].Cells["TOTAL"].Value);
                    }
                }
                for (int x = 0; x < dgDespesas.RowCount; x++)
                {
                    boleto += Convert.ToDouble(dgDespesas.Rows[x].Cells["PD_SALDO"].Value);

                    if (dgDespesas.Rows[x].Cells["ESPECIE"].Value != null)
                    {
                        if (dgDespesas.Rows[x].Cells["ESPECIE"].Value.ToString() == "CHEQUE")
                        {
                            cheque = cheque - Convert.ToDouble(dgDespesas.Rows[x].Cells["PD_SALDO"].Value);
                        }
                        else if (dgDespesas.Rows[x].Cells["ESPECIE"].Value.ToString() == "CREDITO")
                        {
                            credito = credito - Convert.ToDouble(dgDespesas.Rows[x].Cells["PD_SALDO"].Value);
                        }
                        else if (dgDespesas.Rows[x].Cells["ESPECIE"].Value.ToString() == "DEBITO")
                        {
                            debito = debito - Convert.ToDouble(dgDespesas.Rows[x].Cells["PD_SALDO"].Value);
                        }
                        else if (dgDespesas.Rows[x].Cells["ESPECIE"].Value.ToString() == "DINHEIRO")
                        {
                            dinheiro = dinheiro - Convert.ToDouble(dgDespesas.Rows[x].Cells["PD_SALDO"].Value);
                        }
                    }
                }

                if (cheque < 0)
                    lblCheque.ForeColor = Color.Red;
                else
                    lblCheque.ForeColor = Color.DarkGreen;

                if (debito < 0)
                    lblDebito.ForeColor = Color.Red;
                else
                    lblDebito.ForeColor = Color.DarkGreen;

                if (credito < 0)
                    lblCredito.ForeColor = Color.Red;
                else
                    lblCredito.ForeColor = Color.DarkGreen;

                if (dinheiro < 0)
                    lblDinheiro.ForeColor = Color.Red;
                else
                    lblDinheiro.ForeColor = Color.DarkGreen;

                lblCaixa.Text = String.Format("{0:N}", caixa);
                lblDespesas.Text = String.Format("{0:N}", boleto);
                lblSaldo.Text = String.Format("{0:N}", caixa - boleto);

                if (Convert.ToDouble(lblSaldo.Text) < 0)
                {
                    lblSaldo.ForeColor = Color.Red;
                }
                else
                    lblSaldo.ForeColor = Color.DarkGreen;

                lblCheque.Text = String.Format("{0:N}", cheque);
                lblDebito.Text = String.Format("{0:N}", debito);
                lblCredito.Text = String.Format("{0:N}", credito);
                lblDinheiro.Text = String.Format("{0:N}", dinheiro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCaixa_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            RecalculaValores();
        }

        private void dgDespesas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            RecalculaValores();
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Limpa Lançamento de Despesa");
        }

        private void btnOk_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnOk,"Lançamento de Despesa");
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtValor.Text))
                {
                    if (Convert.ToDouble(txtValor.Text) != 0)
                    {
                        cmbEspecie.Focus();
                        return;
                    }
                    else
                        MessageBox.Show("Valor não pode ser em Branco", "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Valor não pode ser em Branco", "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                txtValor.Focus();
            }
        }

        private void cmbEspecie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbEspecie.SelectedIndex != -1)
                {
                    btnOk.PerformClick();
                }
                else
                {
                    MessageBox.Show("Necessário selecionar uma Espécie", "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbEspecie.Focus();
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(lblExtra.Text))
                {
                    if (cmbEspecie.Text == "CHEQUE")
                    {
                        cheque = cheque - Convert.ToDouble(txtValor.Text);
                    }
                    else if (cmbEspecie.Text == "CREDITO")
                    {
                        credito = credito - Convert.ToDouble(txtValor.Text);
                    }
                    else if (cmbEspecie.Text == "DEBITO")
                    {
                        debito = debito - Convert.ToDouble(txtValor.Text);
                    }
                    else if (cmbEspecie.Text == "DINHEIRO")
                    {
                        dinheiro = dinheiro - Convert.ToDouble(txtValor.Text);
                    }

                    if (cheque < 0)
                        lblCheque.ForeColor = Color.Red;
                    else
                        lblCheque.ForeColor = Color.DarkGreen;

                    if (debito < 0)
                        lblDebito.ForeColor = Color.Red;
                    else
                        lblDebito.ForeColor = Color.DarkGreen;

                    if (credito < 0)
                        lblCredito.ForeColor = Color.Red;
                    else
                        lblCredito.ForeColor = Color.DarkGreen;

                    if (dinheiro < 0)
                        lblDinheiro.ForeColor = Color.Red;
                    else
                        lblDinheiro.ForeColor = Color.DarkGreen;

                    lblCheque.Text = String.Format("{0:N}", cheque);
                    lblDebito.Text = String.Format("{0:N}", debito);
                    lblCredito.Text = String.Format("{0:N}", credito);
                    lblDinheiro.Text = String.Format("{0:N}", dinheiro);

                    lblExtra.Text = "Extra: " + String.Format("{0:N}", extra + Convert.ToDouble(txtValor.Text));

                    txtValor.Text = "0,00";

                    cmbEspecie.SelectedIndex = -1;
                }
                else
                {
                    if (MessageBox.Show("Despesa Extra já Lançada! Deseja limpar despesa e Lançar outro Valor?", "Caixa X Contas à Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                        DialogResult.Yes)
                    {
                        lblExtra.Text = "";
                        extra = 0;
                        RecalculaValores();
                        txtValor.Focus();
                    }
                    else
                        dgDespesas.Focus();
                       
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Caixa X Contas à Pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Limpar as Despesas?", "Caixa X Contas à Pagar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) ==
                DialogResult.Yes)
            {
                lblExtra.Text = "";
                extra = 0;
                RecalculaValores();
                txtValor.Focus();
            }
            else
                dgDespesas.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
