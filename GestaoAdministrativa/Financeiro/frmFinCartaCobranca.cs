﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Financeiro.Relatorios;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Financeiro
{
    public partial class frmFinCartaCobranca : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Carta de Cobrança");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmFinCartaCobranca(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmFinCartaCobranca_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.carta;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmFinCartaCobranca>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now.AddDays(-30);
            dtFinal.Value = DateTime.Now;
            txtNome.Text = "";
            dtInicial.Focus();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {

                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                var buscaCobranca = new Cobranca();
                DataTable dt = buscaCobranca.BuscaDadosCobrancaCarta(dtInicial.Value, dtFinal.Value, Principal.empAtual, Principal.estAtual, txtNome.Text);

                if(dt.Rows.Count > 0)
                {
                    dgContas.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dtInicial.Focus();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmFinCartaCobranca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtCarencia_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtIndice_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSimples_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbComposto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dgContas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnMarcar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnDesmarcar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnCartas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.F2:
                    btnMarcar.PerformClick();
                    break;
                case Keys.F3:
                    btnDesmarcar.PerformClick();
                    break;
                case Keys.F4:
                    btnCartas.PerformClick();
                    break;
            }
        }

        private void btnMarcar_Click(object sender, EventArgs e)
        {
            for(int i=0; i < dgContas.RowCount; i++)
            {
                dgContas.Rows[i].Selected = true;
            }
        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgContas.RowCount; i++)
            {
                dgContas.Rows[i].Selected = false;
            }
        }

        private void btnCartas_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgContas.RowCount > 0)
                {
                    string id = "";
                    for(int i=0; i< dgContas.RowCount;i++)
                    {
                        if(dgContas.Rows[i].Selected == true)
                        {
                            id = id + dgContas.Rows[i].Cells["CF_ID"].Value + ",";
                        }
                    }

                    if(!String.IsNullOrEmpty(id))
                    {
                        var cobranca = new Cobranca();
                        DataTable dt = cobranca.BuscaDadosCartaClienteSelecionados(id);

                        frmRelCartaCobranca relatorio = new frmRelCartaCobranca(this, dt);
                        relatorio.Text = "Carta de Cobrança";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Necessário selecionar ao menos um Cliente!", "Cartas de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgContas.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário realizar a busca!", "Cartas de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnBuscar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Carta de Cobrança", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }
        
        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }
    }
}
