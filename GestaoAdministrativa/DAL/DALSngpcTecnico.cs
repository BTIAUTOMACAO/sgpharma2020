﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.DAL
{
    public class DALSngpcTecnico
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA SNPGC_TECNICOS
        /// </summary>
        /// <param name="dados">Objeto do Tipo SngpcTecnico</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(SngpcTecnico dados)
        {
            MontadorSql mont = new MontadorSql("sngpc_tecnicos", MontadorType.Insert);
            mont.AddField("tec_codigo", dados.TecCodigo);
            mont.AddField("tec_cpf", dados.TecCpf);
            mont.AddField("tec_nome", dados.TecNome);
            mont.AddField("tec_login", dados.TecLogin);
            mont.AddField("tec_senha", dados.TecSenha);
            mont.AddField("tec_liberado", dados.TecLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE CADASTRO DE TECNICOS SNGPC
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo SngpcTecnico com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(SngpcTecnico dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.TEC_CODIGO, A.TEC_CPF, A.TEC_NOME, A.TEC_LOGIN, A.TEC_LIBERADO, A.DTALTERACAO, "
                + "(SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO, A.TEC_SENHA FROM SNGPC_TECNICOS A,"
                + " USUARIOS B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.TecCodigo == 0 && dadosBusca.TecCpf == ""  && dadosBusca.TecLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.TEC_CODIGO";
            }
            else
            {
                if (dadosBusca.TecCodigo != 0)
                {
                    strSql += " AND A.TEC_CODIGO = " + dadosBusca.TecCodigo;
                }
                if (dadosBusca.TecCpf != "")
                {
                    strSql += " AND A.TEC_CPF = '" + dadosBusca.TecCpf + "'";
                }
                if (dadosBusca.TecLiberado != "TODOS")
                {
                    strSql += " AND A.TEC_LIBERADO = '" + dadosBusca.TecLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.TEC_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// EXCLUI OS DADOS DA TABELA SNPGC_TECNICOS
        /// </summary>
        /// <param name="tecCodigo">Cód. do Tecnico</param>
        /// <returns>-1, erro</returns>
        public static int GetExcluirDados(string tecCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("tec_codigo", tecCodigo));

            string strCmd = "DELETE FROM SNGPC_TECNICOS WHERE TEC_CODIGO = @tec_codigo";

            return BancoDados.ExecuteNoQuery(strCmd, ps);
        }


        /// <summary>
        /// ATUALIZA OS DADOS DA TABELA SNPGC_TECNICOS, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo SngpcTecnico com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(SngpcTecnico dadosNew, DataTable dadosOld)
        {
            #region ATUALIZAÇÃO
            string[,] dados = new string[7, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("sngpc_tecnicos", MontadorType.Update);
            if (!dadosNew.TecCpf.Equals(dadosOld.Rows[0]["TEC_CPF"]))
            {
                //VERIFICA SE TECNICO JÁ ESTA CADASTRADO//
                if (Util.RegistrosPorEstabelecimento("SNGPC_TECNICOS", "TEC_CPF", dadosNew.TecCpf, true).Rows.Count > 1)
                {
                    Principal.mensagem = "Usuário já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                mont.AddField("tec_cpf", dadosNew.TecCpf);

                dados[contMatriz, 0] = "TEC_CPF";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TEC_CPF"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TecCpf + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TecNome.Equals(dadosOld.Rows[0]["TEC_NOME"]))
            {
                mont.AddField("tec_nome", dadosNew.TecNome);

                dados[contMatriz, 0] = "TEC_NOME";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TEC_NOME"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TecNome + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TecLogin.Equals(dadosOld.Rows[0]["TEC_LOGIN"].ToString()))
            {
                mont.AddField("TEC_LOGIN", dadosNew.TecLogin);

                dados[contMatriz, 0] = "TEC_LOGIN";
                dados[contMatriz, 1] = dadosOld.Rows[0]["TEC_LOGIN"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["TEC_LOGIN"] + "'";
                dados[contMatriz, 2] = dadosNew.TecLogin == "" ? "null" : "'" + dadosNew.TecLogin + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TecSenha.Equals(dadosOld.Rows[0]["TEC_SENHA"].ToString()))
            {
                mont.AddField("TEC_SENHA", dadosNew.TecSenha);

                dados[contMatriz, 0] = "TEC_SENHA";
                dados[contMatriz, 1] = dadosOld.Rows[0]["TEC_SENHA"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["TEC_SENHA"] + "'";
                dados[contMatriz, 2] = dadosNew.TecSenha == "" ? "null" : "'" + Funcoes.CriptografaSenha(dadosNew.TecSenha) + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TecLiberado.Equals(dadosOld.Rows[0]["TEC_LIBERADO"]))
            {
                mont.AddField("TEC_LIBERADO", dadosNew.TecLiberado);

                dados[contMatriz, 0] = "TEC_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TEC_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TecLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                mont.SetWhere("WHERE TEC_CODIGO = " + dadosNew.TecCodigo, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    Funcoes.GravaLogAlteracao("TEC_CODIGO", dadosNew.TecCodigo.ToString(), Principal.usuario, "SNGPC_TECNICOS", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;

            #endregion
        }

    }
}
