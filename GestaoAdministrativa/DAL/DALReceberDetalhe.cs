﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.DAL
{
    public class DALReceberDetalhe
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA RECEBER_DETALHE
        /// </summary>
        /// <param name="dados">Objeto do Tipo ReceberDetalhe</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(ReceberDetalhe dados)
        {
            MontadorSql mont = new MontadorSql("receber_detalhe", MontadorType.Insert);
            mont.AddField("est_codigo", dados.EmpCodigo);
            //mont.AddField("rec_codigo", dados.RecCodigo);
            //mont.AddField("rd_parcela", dados.RdParcela);
            mont.AddField("rd_valor", dados.RdValor);
            mont.AddField("rd_vencto", dados.RdVencimento);
            mont.AddField("rd_saldo", dados.RdSaldo);
            mont.AddField("rd_status", dados.RdStatus);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// BUSCA AS PARCELAS DE UM TITULO
        /// </summary>
        /// <param name="estCodigo">Cód do Estabelecimento</param>
        /// <param name="recCodigo">Cód da Título</param>
        /// <returns>Retorna um list do tipo ReceberDetalhe</returns>
        public static List<ReceberDetalhe> GetPacelasPagamento(int estCodigo, int recCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));

            string strSql = "SELECT A.RD_VALOR, A.RD_VENCTO, A.RD_STATUS, A.RD_SALDO FROM RECEBER_DETALHE A  WHERE A.EST_CODIGO = @est_codigo"
                     + " AND A.REC_CODIGO = @rec_codigo ORDER BY A.RD_PARCELA";

            List<ReceberDetalhe> lista = new List<ReceberDetalhe>();

            using (DataTable table = BancoDados.GetDataTable(strSql, ps))
            {
                foreach (DataRow row in table.Rows)
                {
                    ReceberDetalhe dPagto = new ReceberDetalhe();
                    //dPagto.RdValor = Convert.ToDecimal(row["RD_VALOR"]);
                    //dPagto.RdVencimento = row["RD_VENCTO"].ToString();
                    //dPagto.RdStatus = row["RD_STATUS"].ToString();
                    //dPagto.RdSaldo = Convert.ToDecimal(row["RD_SALDO"]);
                    lista.Add(dPagto);
                }
            }

            return lista;
        }

        /// <summary>
        /// BUSCA FATURAS PARA EXIBICAO TELA DE MOVIMENTO DE PARCELAS, TÍTULOS COM REC_STATUS DIFERENTE DE QUITADO
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Receber com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(Receber dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.REC_CODIGO, A.REC_DATA, A.REC_VALPRIN, A.REC_DOCTO, B.TIL_DESCR, C.CLI_DOCTO, "
                + "CASE A.REC_STATUS WHEN 'A' THEN 'ABERTO' ELSE 'PARCIAL' END AS REC_STATUS, COUNT(E.RD_PARCELA) AS RD_PARCELA,"
                + "A.DTALTERACAO,(SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, G.NOME AS OPCADASTRO, "
                + "C.CLI_ID, C.CLI_NOME, C.CLI_TIPO_DOCTO "
                + "FROM RECEBER A "
                + "LEFT JOIN TITULOS B ON B.TIL_CODIGO = A.TIL_CODIGO "
                + "INNER JOIN CLIENTES C ON C.CLI_ID = A.CLI_ID "
                + "INNER JOIN TIPO_DOCTO D ON D.TIP_CODIGO = A.TIP_CODIGO "
                + "INNER JOIN RECEBER_DETALHE E ON (A.REC_CODIGO = E.REC_CODIGO AND A.EST_CODIGO = E.EST_CODIGO) "
                + "LEFT JOIN USUARIOS G ON A.OPCADASTRO = G.LOGIN_ID "
                + "WHERE A.REC_STATUS <> 'Q' AND A.EST_CODIGO = " + dadosBusca.EmpCodigo;

            //if (dadosBusca.RecCodigo == 0 && Funcoes.RemoveCaracter(dadosBusca.RecData) == "" && dadosBusca.TilCodigo == 0 && dadosBusca.RecDocto == "" && dadosBusca.CliId == "")
            //{
            //    strSql += "GROUP BY A.REC_CODIGO, A.REC_DATA, A.REC_VALPRIN, D.TIP_DESC_ABREV, A.REC_DOCTO, B.TIL_DESCR, "
            //            + "C.CLI_DOCTO, A.REC_STATUS,A.DTALTERACAO, A.OPALTERACAO,A.DTCADASTRO,G.NOME, C.CLI_ID, C.CLI_NOME, C.CLI_TIPO_DOCTO ";
            //    strOrdem = strSql;
            //    strSql += " ORDER BY A.REC_CODIGO, A.REC_DATA";
            //}
            //else
            //{
            //    if (dadosBusca.RecCodigo != 0)
            //    {
            //        strSql += " AND A.REC_CODIGO = " + dadosBusca.RecCodigo;
            //    }
            //    if (dadosBusca.TilCodigo != 0)
            //    {
            //        strSql += " AND A.TIL_CODIGO = " + dadosBusca.TilCodigo;
            //    }
            //    if (Funcoes.RemoveCaracter(dadosBusca.RecData) != "")
            //    {
            //        strSql += " AND A.REC_DATA = '" + dadosBusca.RecData + "'";
            //    }
            //    if (dadosBusca.RecDocto != "")
            //    {
            //        strSql += " AND A.REC_DOCTO = '" + dadosBusca.RecDocto + "'";
            //    }
            //    if (dadosBusca.CliId != "")
            //    {
            //        strSql += " AND C.CLI_DOCTO = '" + dadosBusca.CliId + "'";
            //    }
            //    strSql += " GROUP BY A.REC_CODIGO, A.REC_DATA, A.REC_VALPRIN, D.TIP_DESC_ABREV, A.REC_DOCTO, B.TIL_DESCR,"
            //            + " C.CLI_DOCTO, A.REC_STATUS,A.DTALTERACAO, A.OPALTERACAO,A.DTCADASTRO,G.NOME, C.CLI_ID, C.CLI_NOME, C.CLI_TIPO_DOCTO ";
                strOrdem = strSql;
            //    strSql += " ORDER BY A.REC_CODIGO, A.REC_DATA";
            //}

            return BancoDados.GetDataTable(strSql, null);
        }


        /// <summary>
        /// EXCLUI PARCELA(S) DA TABELA RECEBER_DETALHE
        /// </summary>
        /// <param name="estCodigo">Cod. Estabelecimento</param>
        /// <param name="depCodigo">Cod. do Título</param>
        /// <param name="parcela">0 exclui todas as parcelas, caso contrario informar o numero da parcelar para excluir</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetExcluirParcelas(int estCodigo, int recCodigo, int parcela = 0)
        {
            string strCmd = "DELETE FROM RECEBER_DETALHE WHERE EST_CODIGO = " + estCodigo + " AND REC_CODIGO = " + recCodigo;
            if (parcela != 0)
            {
                strCmd += " AND RD_PARCELA = " + parcela;
            }

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// ATUALIZA O SALDO DA PARCELA
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="recCodigo">Cód. do Título</param>
        /// <param name="valor">Valor Recebido</param>
        /// <param name="status">Status do Pagamento</param>
        /// <param name="dadosOld">Objeto do Tipo ReceberDetalhe com os dados antigos para gravar Log</param>
        /// <param name="operacao">Soma ou Subtração</param>
        /// <param name="parcela">N° da Parcela</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaSaldo(int estCodigo, int recCodigo, decimal valor, string status, ReceberDetalhe dadosOld, char operacao, int parcela = 0)
        {
            int contMatriz = 0;
            string[,] logAtualiza = new string[4, 3];

            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));
            ps.Add(new Fields("rd_saldo", valor));
            ps.Add(new Fields("rd_parcela", parcela));
            ps.Add(new Fields("rd_status", status));
            ps.Add(new Fields("dtalteracao", DateTime.Now));
            ps.Add(new Fields("opalteracao", Principal.usuario));

            if (!dadosOld.RdStatus.Equals("N"))
            {
                logAtualiza[contMatriz, 0] = "RD_STATUS";
                logAtualiza[contMatriz, 1] = "'" + dadosOld.RdStatus + "'";
                logAtualiza[contMatriz, 2] = "'" + status + "'";
                contMatriz = contMatriz + 1;
            }


            logAtualiza[contMatriz, 0] = "RD_SALDO";
            logAtualiza[contMatriz, 1] = "'" + dadosOld.RdSaldo + "'";
            //logAtualiza[contMatriz, 2] = "'" + (dadosOld.RdSaldo - valor).ToString() + "'";
            contMatriz = contMatriz + 1;

            logAtualiza[contMatriz, 0] = "DTALTERACAO";
            logAtualiza[contMatriz, 1] = dadosOld.DtAlteracao.ToString("dd/MM/yyyy") == "01/01/2001" ? "null" : "'" + dadosOld.DtAlteracao + "'";
            logAtualiza[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
            contMatriz = contMatriz + 1;

            logAtualiza[contMatriz, 0] = "OPALTERACAO";
            //logAtualiza[contMatriz, 1] = dadosOld.OpAlteracao == 0 ? "null" : "'" + dadosOld.OpAlteracao.ToString() + "'";
            logAtualiza[contMatriz, 2] = Principal.usuario;
            contMatriz = contMatriz + 1;


            string strCmd = "UPDATE RECEBER_DETALHE SET RD_SALDO = ROUND(RD_SALDO " + operacao + " @rd_saldo,2)"
                                       + ", RD_STATUS = @rd_status"
                                       + ", DTALTERACAO = @dtalteracao, OPALTERACAO = @opalteracao WHERE EST_CODIGO = @est_codigo"
                                       + " AND REC_CODIGO = @rec_codigo AND RD_PARCELA = @rd_parcela";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                return false;
            }
            else
            {
                Funcoes.GravaLogAlteracao("REC_CODIGO", recCodigo.ToString(), Principal.usuario, "RECEBER_DETALHE", logAtualiza, contMatriz, estCodigo);
                return true;
            }
        }

        /// <summary>
        /// VERIFICA NUMERO DE PARCELAS DO TITULO
        /// </summary>
        /// <param name="dadosBusca">Objeto do tipo Receber</param>
        /// <param name="status">Filtro por Status do Pagamento</param>
        /// <returns>N° de parcelas</returns>
        public static int GetQtdeParcelas(Receber dadosBusca, bool status)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", dadosBusca.EmpCodigo));
            //ps.Add(new Fields("pag_codigo", dadosBusca.RecCodigo));

            string strSql = "SELECT COUNT(*) AS QTDE FROM RECEBER_DETALHE WHERE REC_CODIGO = @pag_codigo"
                         + " AND EST_CODIGO = @est_codigo";

            if (status == true)
            {
                ps.Add(new Fields("status", dadosBusca.RecStatus));
                strSql += " AND RD_STATUS = @status";
            }

            return Convert.ToInt32(BancoDados.ExecuteScalar(strSql, ps));
        }


        /// <summary>
        /// CANCELA A QUITAÇÃO DE UMA CONTA QUE JÁ FOI PAGA
        /// </summary>
        /// <param name="recDetalhe">Objeto do Tipo ReceberDetalhe</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetCancelarQuitacao(ReceberDetalhe recDetalhe)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            DataTable dtBusca = new DataTable();

            //dtBusca = BancoDados.selecionarRegistros("SELECT RD_SALDO, RD_VALOR, RD_STATUS, DTALTERACAO, OPALTERACAO FROM RECEBER_DETALHE WHERE EST_CODIGO = " + recDetalhe.EmpCodigo
            //                + " AND REC_CODIGO = " + recDetalhe.RecCodigo + " AND RD_PARCELA = " + recDetalhe.RdParcela);

            dados[contMatriz, 0] = "RD_SALDO";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["RD_SALDO"] + "'";
            dados[contMatriz, 2] = "'" + dtBusca.Rows[0]["RD_VALOR"] + "'";
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "RD_STATUS";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["RD_STATUS"] + "'";
            dados[contMatriz, 2] = "'N'";
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "DTALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["DTALTERACAO"] + "'";
            dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "OPALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["OPALTERACAO"] + "'";
            dados[contMatriz, 2] = "'" + Principal.usuario + "'";
            contMatriz = contMatriz + 1;


            MontadorSql mont = new MontadorSql("receber_detalhe", MontadorType.Update);
            mont.AddField("RD_SALDO", recDetalhe.RdValor);
            mont.AddField("RD_STATUS", "N");
            mont.AddField("DTALTERACAO", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("OPALTERACAO", Principal.usuario);
            //mont.SetWhere("WHERE REC_CODIGO = " + recDetalhe.RecCodigo + " AND EST_CODIGO = " + recDetalhe.EmpCodigo + " AND RD_PARCELA = " + recDetalhe.RdParcela, null);

            if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()) != -1)
            {
                //ReceberMovto pMovto = new ReceberMovto()
                //{
                //    EstCodigo = recDetalhe.EmpCodigo,
                //    RecCodigo = recDetalhe.RecCodigo,
                //    RdParcela = recDetalhe.RdParcela
                //};

                //if (DAL.DALReceberMovto.GetExcluirMovimento(pMovto).Equals(false))
                //{
                    return false;
                //}
                //else
                //{
                //    Funcoes.GravaLogAlteracao("REC_CODIGO", recDetalhe.RecCodigo.ToString(), Principal.usuario, "RECEBER_DETALHE", dados, contMatriz, Principal.estAtual);
                //    Funcoes.GravaLogExclusao("REC_CODIGO", recDetalhe.RecCodigo.ToString(), Principal.usuario, DateTime.Now, "RECEBER_MOVTO", recDetalhe.RecCodigo.ToString(), Principal.motivo, Principal.estAtual);
                //    return true;
                //}
            }
            else
                return false;

        }
    }
}
