﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.DAL
{
    public class DALGrupoDeUsuario
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA GRUPO_USUARIOS
        /// </summary>
        /// <param name="dados">Objeto do Tipo GrupoDeUsuario</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(GrupoDeUsuario dados)
        {
            string strCmd = "INSERT INTO GRUPO_USUARIOS (GRUPO_USU_ID, DESCRICAO, ADMINISTRADOR, LIBERADO, DTCADASTRO, OPCADASTRO) VALUES("
                + dados.GrupoUsuId + ",'"
                + dados.Descricao + "','"
                + dados.Administrador + "','"
                + dados.Liberado + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// ATUALIZA OS DADOS DA TABELA GRUPO_USUARIOS, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo GrupoDeUsuario com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(GrupoDeUsuario dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[5, 3];
            int contMatriz = 0;

            string strCmd = "UPDATE GRUPO_USUARIOS SET ";
            if (!dadosNew.Descricao.Equals(dadosOld.Rows[0]["DESCRICAO"]))
            {
                if (Util.RegistrosPorEstabelecimento("GRUPO_USUARIOS", "DESCRICAO", dadosNew.Descricao,false, true).Rows.Count > 1)
                {
                    Principal.mensagem = "Grupo já cadastrado.";
                    Funcoes.GetAvisa();
                    return false;
                }

                strCmd += " DESCRICAO = '" + dadosNew.Descricao + "',";

                dados[contMatriz, 0] = "DESCRICAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESCRICAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Descricao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Administrador.Equals(dadosOld.Rows[0]["ADMINISTRADOR"]))
            {
                strCmd += " ADMINISTRADOR = '" + dadosNew.Administrador + "',";

                dados[contMatriz, 0] = "ADMINISTRADOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ADMINISTRADOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Administrador + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.Liberado.Equals(dadosOld.Rows[0]["LIBERADO"]))
            {
                strCmd += " LIBERADO = '" + dadosNew.Liberado + "',";

                dados[contMatriz, 0] = "LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.Liberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuarioID + "'";
                contMatriz = contMatriz + 1;


                strCmd += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                strCmd += " OPALTERACAO = '" + Principal.usuario + "'";
                strCmd += " WHERE GRUPO_USU_ID = " + dadosNew.GrupoUsuId;

                if (BancoDados.ExecuteNoQuery(strCmd, null) != -1)
                {
                    Funcoes.GravaLogAlteracao("GRUPO_USU_ID", dadosNew.GrupoUsuId.ToString(), Principal.usuario, "GRUPO_USUARIOS", dados, contMatriz, Principal.estAtual, Principal.empAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// EXCLUI OS DADOS DA TABELA GRUPO_USUARIOS
        /// </summary>
        /// <param name="gCodigo">Cód. do Grupo de Usuário</param>
        /// <returns>-1, erro</returns>
        public static int GetExcluirDados(string gCodigo)
        {
            string strCmd = "DELETE FROM GRUPO_USUARIOS WHERE GRUPO_USU_ID = " + gCodigo;

            return BancoDados.ExecuteNoQuery(strCmd, null);
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE CADASTRO DE GRUPO DE USUARIOS
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo GrupoDeUsuario com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(GrupoDeUsuario dadosBusca, out string strOrdem)
        {
            string strSql = "SELECT A.GRUPO_USU_ID, A.DESCRICAO, A.ADMINISTRADOR, A.LIBERADO, A.DTALTERACAO, A.OPALTERACAO, "
                    + " A.DTCADASTRO, A.OPCADASTRO FROM GRUPO_USUARIOS A ";
            if (dadosBusca.GrupoUsuId == 0 && dadosBusca.Descricao == "" && dadosBusca.Liberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_USU_ID";
            }
            else
            {
                if (dadosBusca.GrupoUsuId != 0)
                {
                    strSql += " WHERE A.GRUPO_USU_ID = " + dadosBusca.GrupoUsuId;
                }
                if (dadosBusca.Descricao != "")
                {
                    if (dadosBusca.GrupoUsuId == 0)
                    {
                        strSql += " WHERE A.DESCRICAO LIKE '%" + dadosBusca.Descricao + "%'";
                    }
                    else
                        strSql += " AND A.DESCRICAO LIKE '%" + dadosBusca.Descricao + "%'";
                }
                if (dadosBusca.Liberado != "TODOS")
                {
                    if ((dadosBusca.GrupoUsuId == 0) && (dadosBusca.Descricao == ""))
                    {
                        strSql += " WHERE A.LIBERADO = '" + dadosBusca.Liberado + "'";
                    }
                    else
                        strSql += " AND A.LIBERADO = '" + dadosBusca.Liberado + "'";
                }

                strOrdem = strSql;
                strSql += " ORDER BY A.GRUPO_USU_ID";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

    }
}
