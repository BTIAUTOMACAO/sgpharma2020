﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;
using System.Windows.Forms;

namespace GestaoAdministrativa.DAL
{
    class DALPermissao
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA PERMISSOES
        /// </summary>
        /// <param name="dados">Objeto do Tipo Permissao</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool InserirDados(Permissao dados)
        {
            string strCMD = "INSERT INTO PERMISSOES (PERMISSOES_ID, GRUPO_ID, MODULO_ID, ACESSA, INCLUI, ALTERA, EXCLUI, DTCADASTRO, OPCADASTRO) VALUES ("
                + dados.PermissoesID + ","
                + dados.GrupoID + ","
                + dados.ModuloID + ",'"
                + dados.Acessa + "','"
                + dados.Inclui + "','"
                + dados.Altera + "','"
                + dados.Exclui + "',"
                + Funcoes.BDataHora(dados.DtCadastro) + ",'"
                + dados.OpCadastro + "')";
            if (BancoDados.ExecuteNoQuery(strCMD, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// BUSCA AS PERMISSOES DO GRUPO
        /// </summary>
        /// <param name="grupoID">Id do Grupo</param>
        /// <returns>DataTable com os registros</returns>
        public static DataTable GetCarregaPermissoes(int grupoID)
        {
            string strSql = "SELECT P.MODULO_ID, M.DESCRICAO, P.ACESSA, P.INCLUI, P.ALTERA, P.EXCLUI, "
                                + "P.GRUPO_ID, P.PERMISSOES_ID FROM PERMISSOES P, MODULO_MENU M WHERE P.MODULO_ID = M.MODULO_ID AND P.GRUPO_ID = " + grupoID
                                + " ORDER BY M.MODULO_ID";

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// ATUALIZA AS PERMISSOES DE ACESSO
        /// </summary>
        /// <param name="colNome">Nome da Coluna</param>
        /// <param name="valor">S ou N</param>
        /// <param name="grupoID">Código do Grupo de Usuário</param>
        /// <param name="moduloID">Código do Modulo do Menu</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetUpPermissoes(string colNome, string valor, int grupoID, int moduloID)
        {
            string strCmd = "UPDATE PERMISSOES SET " + colNome + " = '" + valor
                + "' WHERE GRUPO_ID = " + grupoID + " AND MODULO_ID = " + moduloID;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// INSERE LOG DAS PERMISSOES ALTERADAS
        /// </summary>
        /// <param name="id">Id da Permissão</param>
        /// <param name="usuarioID">Nome do Usuário</param>
        /// <param name="logCampo">Campo da Tabela</param>
        /// <param name="logAlterado">Valor da Alteração</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetLogPermissoes(int id, int usuarioID, string logCampo, string logAlterado)
        {
            string strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA,"
                        + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO) VALUES ('PERMISSOES_ID','" + id
                        + "'," + usuarioID + ", 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PERMISSOES','" + logCampo + "',";
            if (logAlterado == "S")
            {
                strCmd += "'N','";
            }
            else
            {
                strCmd += "'S','";
            }
            strCmd += logAlterado + "'," + Principal.estAtual + "," + Principal.empAtual + ")";
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// INSERE AS PERMISSOES DO GRUPO DE USUÁRIOS
        /// </summary>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool InserirPermisssoes()
        {
            try
            {
                Permissao permissoes = new Permissao();

                Principal.dtBusca = Util.SelecionaRegistrosTodosOuEspecifico("GRUPO_USUARIOS", "GRUPO_USU_ID, ADMINISTRADOR");

                for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                {
                    Principal.dtRetorno = BancoDados.selecionarRegistros("SELECT MODULO_ID FROM MODULO_MENU ORDER BY MODULO_ID");
                    for (int x = 0; x < Principal.dtRetorno.Rows.Count; x++)
                    {
                        Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM PERMISSOES WHERE MODULO_ID = " + Principal.dtRetorno.Rows[x]["MODULO_ID"] + " AND GRUPO_ID = " + Principal.dtBusca.Rows[i]["GRUPO_USU_ID"]);
                        if (Principal.dtPesq.Rows.Count == 0)
                        {
                            permissoes.PermissoesID = Convert.ToInt32(Funcoes.GetVerificaID("PERMISSOES","PERMISSOES_ID"));
                            permissoes.GrupoID = Convert.ToInt32(Principal.dtBusca.Rows[i]["GRUPO_USU_ID"]);
                            permissoes.ModuloID = Convert.ToInt32(Principal.dtRetorno.Rows[x]["MODULO_ID"]);
                            if (Principal.dtBusca.Rows[i]["ADMINISTRADOR"].ToString() == "S")
                            {
                                permissoes.Acessa = 'S';
                                permissoes.Inclui = 'S';
                                permissoes.Altera = 'S';
                                permissoes.Exclui = 'S';
                            }
                            else
                            {
                                permissoes.Acessa = 'N';
                                permissoes.Inclui = 'N';
                                permissoes.Altera = 'N';
                                permissoes.Exclui = 'N';
                            }
                            permissoes.DtCadastro = DateTime.Now;
                            permissoes.OpCadastro = Principal.usuario;

                            if (InserirDados(permissoes).Equals(false))
                            {
                                return false;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "BTI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
