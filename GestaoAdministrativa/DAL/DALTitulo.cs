﻿using System;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Data;

namespace GestaoAdministrativa.DAL
{
    public class DALTitulo
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA TITULOS
        /// </summary>
        /// <param name="dados">Objeto do Tipo Titulo</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(Titulo dados)
        {
            MontadorSql mont = new MontadorSql("titulos", MontadorType.Insert);
            mont.AddField("til_codigo", dados.TilCodigo);
            mont.AddField("til_descr", dados.TilDescr);
            mont.AddField("til_liberado", dados.TilLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        ///  BUSCA OS DADOS DA TELA DE CADASTRO DE TITULOS
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Titulo com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(Titulo dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.TIL_CODIGO, A.TIL_DESCR, A.TIL_LIBERADO, A.DTALTERACAO,"
                       + " (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO"
                       + " FROM TITULOS A, USUARIOS B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.TilCodigo == 0 && dadosBusca.TilDescr == "" && dadosBusca.TilLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY TIL_CODIGO";
            }
            else
            {
                if (dadosBusca.TilCodigo != 0)
                {
                    strSql += " AND A.TIL_CODIGO = " + dadosBusca.TilCodigo;
                }
                if (dadosBusca.TilDescr != "")
                {
                    strSql += " AND A.TIL_DESCR LIKE '%" + dadosBusca.TilDescr + "%'";
                }
                if (dadosBusca.TilLiberado != "TODOS")
                {
                    strSql += " AND A.TIL_LIBERADO = '" + dadosBusca.TilLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.TIL_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        ///  ATUALIZA OS DADOS DA TABELA TITULOS, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo Titulo com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(Titulo dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("titulos", MontadorType.Update);
            if (!dadosNew.TilDescr.Equals(dadosOld.Rows[0]["TIL_DESCR"]))
            {
                //VERIFICA SE CLASSE JA ESTA CADASTRADA//
                if (Util.RegistrosPorEstabelecimento("TITULOS", "TIL_DESCR", dadosNew.TilDescr).Rows.Count != 0)
                {
                    Principal.mensagem = "Título já cadastrado.";
                    Funcoes.Avisa();
                    return false;
                }

                mont.AddField("TIL_DESCR", dadosNew.TilDescr);

                dados[contMatriz, 0] = "TIL_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TIL_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TilDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TilLiberado.Equals(dadosOld.Rows[0]["TIL_LIBERADO"]))
            {
                mont.AddField("TIL_LIBERADO", dadosNew.TilLiberado);

                dados[contMatriz, 0] = "TIL_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TIL_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TilLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                mont.SetWhere("WHERE til_codigo = " + dadosNew.TilCodigo, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    Funcoes.GravaLogAlteracao("TIL_CODIGO", dadosNew.TilCodigo.ToString(), Principal.usuario, "TITULOS", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// EXCLUI OS DADOS DA TABELA TITULOS
        /// </summary>
        /// <param name="tilCodigo">Cód. do Titulo</param>
        /// <returns>-1, erro</returns>
        public static int GetExcluirDados(string tilCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("til_codigo", tilCodigo));

            string strCmd = "DELETE FROM TITULOS WHERE TIL_CODIGO = @til_codigo";

            return BancoDados.ExecuteNoQuery(strCmd, ps);
        }
    }
}
