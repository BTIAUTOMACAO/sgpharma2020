﻿using GestaoAdministrativa.Negocio;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Collections.Generic;
using System.Data;
using System;

namespace GestaoAdministrativa.DAL
{
    public class DALPagarDetalhe
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA PAGAR_DETALHE
        /// </summary>
        /// <param name="dados">Objeto do Tipo PagarDetalhe</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(PagarDetalhe dados)
        {
            MontadorSql mont = new MontadorSql("pagar_detalhe", MontadorType.Insert);
            //mont.AddField("est_codigo", dados.EstCodigo);
            mont.AddField("pag_codigo", dados.PagCodigo);
            //mont.AddField("forn_codigo", dados.FornCodigo);
            //mont.AddField("pd_parcela", dados.PdParcela);
            mont.AddField("pd_valor", dados.PdValor);
            mont.AddField("pd_vencto", dados.PdVencimento);
            mont.AddField("pd_saldo", dados.PdSaldo);
            mont.AddField("pd_status", dados.PdStatus);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// BUSCA AS PARCELAS DE UMA FATURA
        /// </summary>
        /// <param name="estCodigo">Cód do Estabelecimento</param>
        /// <param name="pagCodigo">Cód da Fatura</param>
        /// <returns>Retorna um list do tipo PagarDetalhe</returns>
        public static List<PagarDetalhe> GetPacelasPagamento(int estCodigo, int pagCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_codigo", pagCodigo));

            string strSql = "SELECT A.PD_VALOR, A.PD_VENCTO, A.PD_STATUS, A.PD_SALDO FROM PAGAR_DETALHE A  WHERE A.EST_CODIGO = @est_codigo"
                     + " AND A.PAG_CODIGO = @pag_codigo ORDER BY A.PD_PARCELA";

            List<PagarDetalhe> lista = new List<PagarDetalhe>();

            using (DataTable table = BancoDados.GetDataTable(strSql, ps))
            {
                foreach (DataRow row in table.Rows)
                {
                    PagarDetalhe dPagto = new PagarDetalhe();
                    //dPagto.PdValor = Convert.ToDecimal(row["PD_VALOR"]);
                    //dPagto.PdVencimento = row["PD_VENCTO"].ToString();
                    //dPagto.PdStatus = row["PD_STATUS"].ToString();
                    //dPagto.PdSaldo = Convert.ToDecimal(row["PD_SALDO"]);
                    lista.Add(dPagto);
                }
            }

            return lista;
        }

        /// <summary>
        /// BUSCA FATURAS PARA EXIBICAO TELA DE MOVIMENTO DE PARCELAS, FATURAS COM PAG_STATUS DIFERENTE DE QUITADO
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Pagar com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(Pagar dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.PAG_CODIGO, A.PAG_DATA, A.PAG_VALPRIN, A.PAG_DOCTO, B.DESP_DESCR, C.FORN_CGC, "
                + "CASE A.PAG_STATUS WHEN 'A' THEN 'ABERTO' ELSE 'PARCIAL' END AS PAG_STATUS, COUNT(E.PD_PARCELA) AS PD_PARCELA,"
                + "A.DTALTERACAO,(SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, G.NOME AS OPCADASTRO, "
                + "C.FORN_CODIGO, C.FORN_RAZAO "
                + "FROM PAGAR A "
                + "LEFT JOIN DESPESAS B ON B.DESP_CODIGO = A.DESP_CODIGO "
                + "INNER JOIN FORNECEDORES C ON C.FORN_CODIGO = A.FORN_CODIGO "
                + "INNER JOIN TIPO_DOCTO D ON D.TIP_CODIGO = A.TIP_CODIGO "
                + "INNER JOIN PAGAR_DETALHE E ON (A.PAG_CODIGO = E.PAG_CODIGO AND A.EST_CODIGO = E.EST_CODIGO) "
                + "LEFT JOIN USUARIOS G ON A.OPCADASTRO = G.LOGIN_ID "
                + "WHERE A.PAG_STATUS <> 'Q' AND A.EST_CODIGO = " + dadosBusca.EmpCodigo;

            //if (dadosBusca.PagCodigo == 0 && Funcoes.RemoveCaracter(dadosBusca.PagData) == "" && dadosBusca.DespCodigo == 0 && dadosBusca.PagDocto == "" && dadosBusca.FornCodigo == "")
            //{
            //    strSql += "GROUP BY A.PAG_CODIGO, A.PAG_DATA, A.PAG_VALPRIN, D.TIP_DESC_ABREV, A.PAG_DOCTO, B.DESP_DESCR, "
            //            + "C.FORN_CGC, A.PAG_STATUS,A.DTALTERACAO, A.OPALTERACAO,A.DTCADASTRO,G.NOME, C.FORN_CODIGO, C.FORN_RAZAO ";
            //    strOrdem = strSql;
            //    strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            //}
            //else
            //{
            //    if (dadosBusca.PagCodigo != 0)
            //    {
            //        strSql += " AND A.PAG_CODIGO = " + dadosBusca.PagCodigo;
            //    }
            //    if (dadosBusca.DespCodigo != 0)
            //    {
            //        strSql += " AND A.DESP_CODIGO = " + dadosBusca.DespCodigo;
            //    }
            //    if (Funcoes.RemoveCaracter(dadosBusca.PagData) != "")
            //    {
            //        strSql += " AND A.PAG_DATA = '" + dadosBusca.PagData + "'";
            //    }
            //    if (dadosBusca.PagDocto != "")
            //    {
            //        strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
            //    }
            //    if (dadosBusca.FornCodigo != "")
            //    {
            //        strSql += " AND C.FORN_CGC = '" + dadosBusca.FornCodigo + "'";
            //    }
            //    strSql += " GROUP BY A.PAG_CODIGO, A.PAG_DATA, A.PAG_VALPRIN, D.TIP_DESC_ABREV, A.PAG_DOCTO, B.DESP_DESCR,"
            //            + " C.FORN_CGC, A.PAG_STATUS,A.DTALTERACAO, A.OPALTERACAO,A.DTCADASTRO,G.NOME, C.FORN_CODIGO, C.FORN_RAZAO ";
                strOrdem = strSql;
                strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            //}

            return BancoDados.GetDataTable(strSql, null);
        }

        
        /// <summary>
        /// EXCLUI PARCELA(S) DA TABELA PAGAR_DETALHE
        /// </summary>
        /// <param name="estCodigo">Cod. Estabelecimento</param>
        /// <param name="pagCodigo">Cod. da Fatura</param>
        /// <param name="parcela">0 exclui todas as parcelas, caso contrario informar o numero da parcelar para excluir</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetExcluirParcelas(int estCodigo, int pagCodigo, int parcela = 0)
        {
            string strCmd = "DELETE FROM PAGAR_DETALHE WHERE EST_CODIGO = " + estCodigo + " AND PAG_CODIGO = " + pagCodigo;
            if (parcela != 0)
            {
                strCmd += " AND PD_PARCELA = " + parcela;
            }

            if (BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// VERIFICA NUMERO DE PARCELAS DA FATURA
        /// </summary>
        /// <param name="dadosBusca">Objeto do tipo Pagar</param>
        /// <param name="status">Filtro por Status do Pagamento</param>
        /// <returns>N° de parcelas</returns>
        public static int GetQtdeParcelas(Pagar dadosBusca, bool status)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", dadosBusca.EmpCodigo));
            ps.Add(new Fields("pag_codigo", dadosBusca.PagCodigo));

            string strSql = "SELECT COUNT(*) AS QTDE FROM PAGAR_DETALHE WHERE PAG_CODIGO = @pag_codigo"
                         + " AND EST_CODIGO = @est_codigo";

            //if (status == true)
            //{
            //    ps.Add(new Fields("status", dadosBusca.PagStatus));
            //    strSql += " AND PD_STATUS = @status";
            //}

            return Convert.ToInt32(BancoDados.ExecuteScalar(strSql, ps));
        }

        /// <summary>
        /// CANCELA A QUITAÇÃO DE UMA CONTA QUE JÁ FOI PAGA
        /// </summary>
        /// <param name="pgDetalhe">Objeto do Tipo PagarDetalhe</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetCancelarQuitacao(PagarDetalhe pgDetalhe)
        {
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            DataTable dtBusca = new DataTable();

            //dtBusca = BancoDados.selecionarRegistros("SELECT PD_SALDO, PD_VALOR, PD_STATUS, DTALTERACAO, OPALTERACAO FROM PAGAR_DETALHE WHERE EST_CODIGO = " + pgDetalhe.EstCodigo
            //                + " AND PAG_CODIGO = " + pgDetalhe.PagCodigo + " AND PD_PARCELA = " + pgDetalhe.PdParcela);

            dados[contMatriz, 0] = "PD_SALDO";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["PD_SALDO"] + "'";
            dados[contMatriz, 2] = "'" + dtBusca.Rows[0]["PD_VALOR"] + "'";
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "PD_STATUS";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["PD_STATUS"] + "'";
            dados[contMatriz, 2] = "'N'";
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "DTALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["DTALTERACAO"] + "'";
            dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "OPALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["OPALTERACAO"] + "'";
            dados[contMatriz, 2] = "'" + Principal.usuario + "'";
            contMatriz = contMatriz + 1;


            MontadorSql mont = new MontadorSql("pagar_detalhe", MontadorType.Update);
            mont.AddField("PD_SALDO", pgDetalhe.PdValor);
            mont.AddField("PD_STATUS", "N");
            mont.AddField("DTALTERACAO", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("OPALTERACAO", Principal.usuario);
            //mont.SetWhere("WHERE PAG_CODIGO = " + pgDetalhe.PagCodigo + " AND EST_CODIGO = " + pgDetalhe.EstCodigo + " AND PD_PARCELA = " + pgDetalhe.PdParcela, null);

            //if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()) != -1)
            //{
            //    PagarMovto pMovto = new PagarMovto()
            //    {
            //        //EstCodigo = pgDetalhe.EstCodigo,
            //        PagCodigo = pgDetalhe.PagCodigo,
            //        PdParcela = pgDetalhe.PdParcela
            //    };

            //    if (DAL.DALPagarMovto.GetExcluirMovimento(pMovto).Equals(false))
            //    {
            //        return false;
            //    }
            //    else
            //    {
            //        Funcoes.GravaLogAlteracao("PAG_CODIGO", pgDetalhe.PagCodigo.ToString(), Principal.usuario, "PAGAR_DETALHE", dados, contMatriz, Principal.estAtual);
            //        Funcoes.GravaLogExclusao("PAG_CODIGO", pgDetalhe.PagCodigo.ToString(), Principal.usuario, DateTime.Now, "PAGAR_MOVTO", pgDetalhe.PagCodigo.ToString(), Principal.motivo, Principal.estAtual);
            //        return true;
            //    }
            //}
            //else
                return false;
         
        }

        /// <summary>
        /// REALIZA O PAGAMENTO DE UMA FATURA DE ACORDO COM A PARCELA
        /// </summary>
        /// <param name="pgDetalhe">Objeto do Tipo PagarDetalhe</param>
        /// <param name="pgTotal">Identifica se pagamento e Total ou Parcial</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetQuitacao(PagarDetalhe pgDetalhe, bool pgTotal)
        {
            DataTable dtBusca = new DataTable();
            string[,] dados = new string[4, 3];
            int contMatriz = 0;

            //dtBusca = BancoDados.selecionarRegistros("SELECT PD_SALDO, PD_STATUS, DTALTERACAO, OPALTERACAO FROM PAGAR_DETALHE WHERE EST_CODIGO = " + pgDetalhe.EstCodigo
                 //      + " AND PAG_CODIGO = " + pgDetalhe.PagCodigo + " AND PD_PARCELA = " + pgDetalhe.PdParcela);

            #region DADOS PARA ATUALIZAÇÃO DE LOG
            dados[contMatriz, 0] = "PD_SALDO";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["PD_SALDO"] + "'";
            if (pgTotal.Equals(true))
            {
                dados[contMatriz, 2] = "'0,00'";
            }
            //else
            //{
            //    dados[contMatriz, 2] = "'" + Convert.ToString(Convert.ToDecimal(dtBusca.Rows[0]["PD_SALDO"]) - pgDetalhe.PdSaldo) + "'";
            //}
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "PD_STATUS";
            dados[contMatriz, 1] = "'" + dtBusca.Rows[0]["PD_STATUS"] + "'";
            if (pgTotal.Equals(true))
            {
                dados[contMatriz, 2] = "'S'";
            }
            else
            {
                dados[contMatriz, 2] = "'P'";
            }
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "DTALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["DTALTERACAO"] + "'";
            dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
            contMatriz = contMatriz + 1;

            dados[contMatriz, 0] = "OPALTERACAO";
            dados[contMatriz, 1] = dtBusca.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dtBusca.Rows[0]["OPALTERACAO"] + "'";
            dados[contMatriz, 2] = "'" + Principal.usuario + "'";
            contMatriz = contMatriz + 1;
            #endregion

            string strCmd = "UPDATE PAGAR_DETALHE SET ";
            if (pgTotal.Equals(true))
            {
                strCmd += "PD_SALDO = 0, PD_STATUS = 'S',";
            }
            //else
            //{
            //    strCmd += "PD_SALDO = PD_SALDO - " + Funcoes.BValor(Convert.ToString(pgDetalhe.PdSaldo)) + ", PD_STATUS = 'P',";
            //}

            //strCmd += "DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ", OPALTERACAO = " + Principal.usuario
            //     + " WHERE EST_CODIGO = " + pgDetalhe.EstCodigo + " AND PAG_CODIGO = " + pgDetalhe.PagCodigo
            //     + " AND PD_PARCELA = " + pgDetalhe.PdParcela;

            if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
            {
                Funcoes.GravaLogAlteracao("PAG_CODIGO", pgDetalhe.PagCodigo.ToString(), Principal.usuario, "PAGAR_DETALHE", dados, contMatriz, Principal.estAtual);
                return true;
            }
            else
                return false;
           
        }

    }
}
