﻿using System.Data;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Classes;
using SqlNegocio;
using System;

namespace GestaoAdministrativa.DAL
{
    public class DALReceber
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA RECEBER
        /// </summary>
        /// <param name="dados">Objeto do Tipo Receber</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(Receber dados)
        {
            MontadorSql mont = new MontadorSql("receber", MontadorType.Insert);
            mont.AddField("est_codigo", dados.EmpCodigo);
            //mont.AddField("rec_codigo", dados.RecCodigo);
            //mont.AddField("cli_id", dados.CliId);
            //mont.AddField("rec_docto", dados.RecDocto);
            //mont.AddField("rec_data", dados.RecData);
            //mont.AddField("rec_valprin", dados.RecValPrin);
            //mont.AddField("col_codigo", dados.ColCodigo);
            //mont.AddField("tip_codigo", dados.TipCodigo);
            //mont.AddField("til_codigo", dados.TilCodigo);
            //mont.AddField("historico", dados.Historico);
            //mont.AddField("rec_status", dados.RecStatus);
            mont.AddField("rec_origem", dados.RecOrigem);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE LANCAMENTO DE TITULOS
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Receber com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(Receber dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.REC_CODIGO, A.REC_DATA, A.REC_VALPRIN, D.TIP_DESC_ABREV, A.REC_DOCTO, B.TIL_DESCR, C.CLI_DOCTO, A.HISTORICO, "
                + "CASE A.REC_STATUS WHEN 'A' THEN 'ABERTO' ELSE CASE A.REC_STATUS WHEN 'P' THEN 'PARCIAL' ELSE 'QUITADO' END END AS REC_STATUS, "
                + "A.DTALTERACAO,(SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, E.NOME AS OPCADASTRO, C.CLI_TIPO_DOCTO "
                + "FROM RECEBER A "
                + "INNER JOIN TITULOS B ON B.TIL_CODIGO = A.TIL_CODIGO "
                + "INNER JOIN CLIENTES C ON C.CLI_ID = A.CLI_ID "
                + "INNER JOIN TIPO_DOCTO D ON D.TIP_CODIGO = A.TIP_CODIGO "
                + "LEFT JOIN USUARIOS E ON A.OPCADASTRO = E.LOGIN_ID "
                + "WHERE A.REC_ORIGEM = 'T' AND A.EST_CODIGO = " + dadosBusca.EmpCodigo;

            //if (dadosBusca.RecCodigo == 0 && Funcoes.RemoveCaracter(dadosBusca.RecData) == "" && dadosBusca.TilCodigo == 0 && dadosBusca.RecDocto == "")
            //{
            //    strOrdem = strSql;
            //    strSql += " ORDER BY A.REC_CODIGO, A.REC_DATA";
            //}
            //else
            //{
            //    if (dadosBusca.RecCodigo != 0)
            //    {
            //        strSql += " AND A.REC_CODIGO = " + dadosBusca.RecCodigo;
            //    }
            //    if (dadosBusca.TilCodigo != 0)
            //    {
            //        strSql += " AND A.TIL_CODIGO = " + dadosBusca.TilCodigo;
            //    }
            //    if (Funcoes.RemoveCaracter(dadosBusca.RecData) != "")
            //    {
            //        strSql += " AND A.REC_DATA = '" + dadosBusca.RecData + "'";
            //    }
            //    if (dadosBusca.RecDocto != "")
            //    {
            //        strSql += " AND A.REC_DOCTO = '" + dadosBusca.RecDocto + "'";
            //    }
                strOrdem = strSql;
            //    strSql += " ORDER BY A.REC_CODIGO, A.REC_DATA";
            //}

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// EXCLUI TITULO TANTO DA TABELA RECEBER_DETALHE E RECEBER
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="recCodigo">Cód. dO Título</param>
        /// <returns>1, executado com sucesso</returns>
        public static int GetExcluiParcReceber(int estCodigo, string recCodigo)
        {
            BancoDados.AbrirTrans();

            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));

            string strCmd = "DELETE FROM RECEBER_DETALHE WHERE EST_CODIGO = @est_codigo AND REC_CODIGO = @pag_codigo";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                strCmd = "DELETE FROM RECEBER WHERE EST_CODIGO = @est_codigo AND REC_CODIGO = @pag_codigo";
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
                {
                    BancoDados.FecharTrans();
                    return 1;
                }
            }

            BancoDados.ErroTrans();
            return 0;
        }

        /// <summary>
        /// OBTEM O CODIGO DO TITULO (REC_CODIGO)
        /// </summary>
        /// <param name="estCodigo">Cód do Estabelecimento</param>
        /// <param name="recDocto">N° do Docto</param>
        /// <param name="cliId">Cód. do Cliente</param>
        /// <returns>Retorno da Busca</returns>
        public static string GetVerificaDocto(int estCodigo, string recDocto, string cliId)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_docto", recDocto));
            ps.Add(new Fields("cli_id", cliId));

            string strSql = "SELECT REC_CODIGO FROM RECEBER WHERE EST_CODIGO = @est_codigo AND REC_DOCTO = @rec_docto"
                           + " AND CLI_ID = @cli_id";

            object r = BancoDados.ExecuteScalar(strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        /// <summary>
        /// METODO QUE ATUALIZA OS DADOS DA TABELA RECEBER, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo Receber com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(Receber dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[8, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("receber", MontadorType.Update);

            //if (!dadosNew.CliId.ToString().Equals(dadosOld.Rows[0]["CLI_ID"].ToString()))
            //{
            //    mont.AddField("cli_id", dadosNew.CliId);

            //    dados[contMatriz, 0] = "CLI_ID";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["CLI_ID"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.CliId + "'";
            //    contMatriz = contMatriz + 1;
            //}
            if (!dadosNew.RecDocto.Equals(dadosOld.Rows[0]["REC_DOCTO"].ToString()))
            {
                //if (!String.IsNullOrEmpty(DAL.DALReceber.GetVerificaDocto(dadosNew.EmpCodigo, dadosNew.RecDocto, dadosNew.CliId)))
                //{
                //    Principal.mensagem = "Já existe um registro com o mesmo número de documento para esse Cliente. Não será permitido esta inclusão.";
                //    Funcoes.Avisa();
                //    return false;
                //}

                mont.AddField("rec_docto", dadosNew.RecDocto);

                dados[contMatriz, 0] = "REC_DOCTO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["REC_DOCTO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.RecDocto + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.RecData.Equals(Principal.dtBusca.Rows[0]["REC_DATA"].ToString().Substring(0, 10)))
            {
                mont.AddField("rec_data", dadosNew.RecData);

                dados[contMatriz, 0] = "REC_DATA";
                dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["REC_DATA"].ToString().Substring(0, 10) + "'";
                dados[contMatriz, 2] = "'" + dadosNew.RecData + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.RecValPrin).Equals(String.Format("{0:N}", Principal.dtBusca.Rows[0]["REC_VALPRIN"])))
            {
                mont.AddField("rec_valprin", dadosNew.RecValPrin);

                dados[contMatriz, 0] = "REC_VALPRIN";
                dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["REC_VALPRIN"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.RecValPrin + "'";
                contMatriz = contMatriz + 1;
            }
            //if (!dadosNew.TipCodigo.ToString().Equals(dadosOld.Rows[0]["TIP_CODIGO"].ToString()))
            //{
            //    mont.AddField("tip_codigo", dadosNew.TipCodigo);

            //    dados[contMatriz, 0] = "TIP_CODIGO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TIP_CODIGO"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.TipCodigo + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.TilCodigo.ToString().Equals(dadosOld.Rows[0]["TIL_CODIGO"].ToString()))
            //{
            //    mont.AddField("til_codigo", dadosNew.TilCodigo);

            //    dados[contMatriz, 0] = "TIL_CODIGO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TIL_CODIGO"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.TilCodigo + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.Historico.Equals(dadosOld.Rows[0]["HISTORICO"].ToString()))
            //{
            //    mont.AddField("historico", dadosNew.Historico);

            //    dados[contMatriz, 0] = "HISTORICO";
            //    dados[contMatriz, 1] = dadosOld.Rows[0]["HISTORICO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["HISTORICO"] + "'";
            //    dados[contMatriz, 2] = dadosNew.Historico == "" ? "null" : "'" + dadosNew.Historico + "'";
            //    contMatriz = contMatriz + 1;
            //}
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                //mont.SetWhere("WHERE REC_CODIGO = " + dadosNew.RecCodigo + " AND EST_CODIGO = " + dadosNew.EmpCodigo, null);

                if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// ATUALIZA O VALOR TOTAL DO TÍTULO, USADO NA TELA DE MANUTENÇÃO DE TÍTULOS
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="recCodigo">Cód. do Título</param>
        /// <param name="valorTotal">Novo valor do Titulo</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaValorTotal(int estCodigo, int recCodigo, decimal valorTotal, decimal valorAnt)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));
            ps.Add(new Fields("valor", valorTotal));
            ps.Add(new Fields("dtalteracao", DateTime.Now));
            ps.Add(new Fields("opalteracao", Principal.usuario));

            string strCmd = "UPDATE RECEBER SET REC_VALPRIN = @valor, DTALTERACAO = @dtalteracao, OPALTERACAO = @opalteracao WHERE EST_CODIGO = @est_codigo and REC_CODIGO = @rec_codigo";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) != -1)
            {
                //strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                //       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO)"
                //       + " VALUES ('REC_VALPRIN','" + recCodigo + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'RECEBER', 'REC_VALPRIN','"
                //       + Funcoes.BValor(valorAnt.ToString()) + "','" + Funcoes.BValor(valorTotal.ToString()) + "'," + estCodigo + ")";
                //if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) == -1)
                //{
                //    return false;
                //}
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA CONTAS A RECEBER
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Receber com os filtros</param>
        /// <param name="todos">Sem filtro de origem do lançamento</param>
        /// <param name="entrada">Filtro de titulos de venda</param>
        /// <param name="titulo">Filtro de titulos do lançamento de titulos</param>
        /// <param name="dtIni">Filtro de período Data Inicial</param>
        /// <param name="dtFinal">Filtro de período Data Final</param>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetListarContasReceber(Receber dadosBusca, bool todos, bool venda, bool titulo, string dtIni, string dtFinal, int estCodigo)
        {
            string strSql = "";

            strSql += " SELECT A.REC_ORIGEM, A.REC_STATUS, B.RD_VENCTO, A.REC_DOCTO, B.RD_PARCELA, B.RD_VALOR, B.RD_SALDO,"
                + " A.REC_DATA, D.CLI_DOCTO, D.CLI_NOME, D.CLI_ID, A.REC_CODIGO, B.RD_STATUS, C.RM_DATA"
                + " FROM RECEBER A"
                + " INNER JOIN RECEBER_DETALHE B ON (A.EST_CODIGO = B.EST_CODIGO AND A.REC_CODIGO = B.REC_CODIGO)"
                + " INNER JOIN CLIENTES D ON (A.CLI_ID = D.CLI_ID)"
                + " LEFT JOIN RECEBER_MOVTO C ON (A.EST_CODIGO = C.EST_CODIGO AND A.REC_CODIGO = C.REC_CODIGO)"
                + " WHERE A.EST_CODIGO = " + estCodigo;

            if (!String.IsNullOrEmpty(dadosBusca.RecDocto))
            {
                strSql += " AND A.REC_DOCTO = '" + dadosBusca.RecDocto + "'";
            }

            //if (!String.IsNullOrEmpty(dadosBusca.CliId))
            //{
            //    strSql += " AND D.CLI_NOME LIKE '" + dadosBusca.CliId + "%'";
            //}

            if (!dadosBusca.RecStatus.Equals("T"))
            {
                if (dadosBusca.RecStatus.Equals("A"))
                {
                    strSql += " AND B.RD_STATUS <> 'S'";
                }
                else if (dadosBusca.RecStatus.Equals("Q"))
                {
                    strSql += " AND B.RD_STATUS = 'S'";
                }
            }

            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)) && !String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
            {
                strSql += " AND B.RD_VENCTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtIni)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            }
            else if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)))
            {
                strSql += " AND B.RD_VENCTO = " + Funcoes.BData(Convert.ToDateTime(dtIni));
            }
            else if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
            {
                strSql += " AND B.RD_VENCTO = " + Funcoes.BData(Convert.ToDateTime(dtFinal));
            }

            if (todos == false)
            {
                if (venda == true)
                {
                    strSql += " AND A.REC_ORIGEM = 'V'";
                }
                else if (titulo == true)
                {
                    strSql += " AND A.REC_ORIGEM = 'T'";
                }
            }
            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// BUSCA AS PARCELAS DOS CLIENTES TELA DE RECEBIMENTO DE CAIXA
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="cliID">Cód. do Cliente</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscaParcelasClientes(int estCodigo, int cliID)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("cli_id", cliID));

            string strSql = "SELECT A.EST_CODIGO, A.REC_CODIGO, C.CLI_DOCTO, A.REC_DOCTO, B.RD_PARCELA, B.RD_VALOR, B.RD_VENCTO, B.RD_SALDO,  0.00 AS ATUAL, A.REC_DATA,"
                  + " 0.00 AS ACRESCIMO, B.DTALTERACAO, (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = B.OPALTERACAO) AS OPALTERACAO, C.CLI_ID, B.RD_STATUS,"
                  + " A.REC_STATUS, B.OPALTERACAO AS OP, A.REC_ORIGEM "
                  + " FROM RECEBER A"
                  + " INNER JOIN RECEBER_DETALHE B ON (A.REC_CODIGO = B.REC_CODIGO AND A.EST_CODIGO = B.EST_CODIGO)"
                  + " INNER JOIN CLIENTES C ON (C.CLI_ID = A.CLI_ID)"
                  + " WHERE A.EST_CODIGO = @est_codigo AND "
                  + " A.CLI_ID = @cli_id AND B.RD_STATUS <> 'S' AND A.REC_STATUS <> 'Q'"
                  + " ORDER BY  A.REC_CODIGO, B.RD_VENCTO";

            return BancoDados.GetDataTable(strSql, ps);
        }

        /// <summary>
        /// EXCLUI REGISTRO DA TABELA RECEBER
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="recCodigo">Cód. do Título</param>
        /// <returns>1, executado com sucesso</returns>
        public static int GetExcluiReceber(int estCodigo, int recCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));

            string strCmd = "DELETE FROM RECEBER WHERE EST_CODIGO = @est_codigo AND REC_CODIGO = @rec_codigo";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// ATUALIZA O STATUS DO TITULO A-ABERTO,P-PARCIAL-Q-QUITADO
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="pagCodigo">Cód. do Título</param>
        /// <param name="status">Status do Pagamento</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaStatusPagto(int estCodigo, int recCodigo, string status, string statusAnt)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("rec_codigo", recCodigo));
            ps.Add(new Fields("rec_status", status));
            ps.Add(new Fields("dtalteracao", DateTime.Now));
            ps.Add(new Fields("opalteracao", Principal.usuario));

            string strCmd = "UPDATE RECEBER SET REC_STATUS = @rec_status, DTALTERACAO = @dtalteracao, OPALTERACAO = @opalteracao WHERE EST_CODIGO = @est_codigo and REC_CODIGO = @rec_codigo";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) != -1)
            {
                strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO)"
                       + " VALUES ('REC_STATUS','" + recCodigo + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'RECEBER', 'REC_STATUS','"
                       + statusAnt + "','" + status + "'," + estCodigo + ")";
                if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) == -1)
                {
                    return false;
                }

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// VERIFICA SALDO PAGO DO TITULO, CASO SEJA 0 E QUE NÃO HOUVE PAGAMENTO
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabecimento</param>
        /// <param name="recCodigo">Cód. do Título</param>
        /// <returns>Valor do Saldo, -1 erro</returns>
        public static decimal GetVerificaSaldo(int estCodigo, int recCodigo)
        {
            string strSql = "SELECT SUM(A.REC_VALPRIN) - SUM(B.RD_SALDO) AS SALDO"
                + " FROM RECEBER A"
                + " INNER JOIN RECEBER_DETALHE B ON (B.EST_CODIGO = A.EST_CODIGO AND A.REC_CODIGO = B.REC_CODIGO)"
                + " WHERE A.EST_CODIGO = " + estCodigo + " AND A.REC_CODIGO = " + recCodigo;

            try
            {
                object r = BancoDados.ExecuteScalar(strSql, null);
                return Convert.ToDecimal(r);
                
            }
            catch
            {
                return -1;
            }
        }

    }
}
