﻿using System;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Data;

namespace GestaoAdministrativa.DAL
{
    public class DALPagar
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA PAGAR
        /// </summary>
        /// <param name="dados">Objeto do Tipo Pagar</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(Pagar dados)
        {
            MontadorSql mont = new MontadorSql("pagar", MontadorType.Insert);
            //mont.AddField("est_codigo", dados.EmpCodigo);
            //mont.AddField("pag_codigo", dados.PagCodigo);
            //mont.AddField("forn_codigo", dados.FornCodigo);
            //mont.AddField("pag_docto", dados.PagDocto);
            //mont.AddField("pag_data", dados.PagData);
            //mont.AddField("pag_valprin", dados.PagValPrin);
            //mont.AddField("pag_comprador", dados.PagComprador);
            //mont.AddField("tip_codigo", dados.TipCodigo);
            //mont.AddField("desp_codigo", dados.DespCodigo);
            //mont.AddField("historico", dados.Historico);
            //mont.AddField("pag_status", dados.PagStatus);
            //mont.AddField("pag_origem", dados.PagOrigem);
            //mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            //mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE LANCAMENTO DE DESPESAS
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Pagar com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(Pagar dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.PAG_CODIGO, A.PAG_DATA, A.PAG_VALPRIN, D.TIP_DESC_ABREV, A.PAG_DOCTO, B.DESP_DESCR, C.FORN_CGC, A.HISTORICO, "
                + "CASE A.PAG_STATUS WHEN 'A' THEN 'ABERTO' ELSE CASE A.PAG_STATUS WHEN 'P' THEN 'PARCIAL' ELSE 'QUITADO' END END AS PAG_STATUS, "
                + "A.DTALTERACAO,(SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, E.NOME AS OPCADASTRO "
                + "FROM PAGAR A "
                + "INNER JOIN DESPESAS B ON B.DESP_CODIGO = A.DESP_CODIGO "
                + "INNER JOIN FORNECEDORES C ON C.FORN_CODIGO = A.FORN_CODIGO "
                + "INNER JOIN TIPO_DOCTO D ON D.TIP_CODIGO = A.TIP_CODIGO "
                + "LEFT JOIN USUARIOS E ON A.OPCADASTRO = E.LOGIN_ID "
                + "WHERE A.PAG_ORIGEM = 'D' AND A.EST_CODIGO = " + dadosBusca.EmpCodigo;

            //if (dadosBusca.PagCodigo == 0 && Funcoes.RemoveCaracter(dadosBusca.PagData) == "" && dadosBusca.DespCodigo == 0 && dadosBusca.PagDocto == "")
            //{
            //    strOrdem = strSql;
            //    strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            //}
            //else
            //{
            //    if (dadosBusca.PagCodigo != 0)
            //    {
            //        strSql += " AND A.PAG_CODIGO = " + dadosBusca.PagCodigo;
            //    }
            //    if (dadosBusca.DespCodigo != 0)
            //    {
            //        strSql += " AND A.DESP_CODIGO = " + dadosBusca.DespCodigo;
            //    }
            //    if (Funcoes.RemoveCaracter(dadosBusca.PagData) != "")
            //    {
            //        strSql += " AND A.PAG_DATA = '" + dadosBusca.PagData + "'";
            //    }
            //    if (dadosBusca.PagDocto != "")
            //    {
            //        strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
            //    }
                strOrdem = strSql;
                strSql += " ORDER BY A.PAG_CODIGO, A.PAG_DATA";
            //}

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// OBTEM O CODIGO DA FATURA (PAG_CODIGO)
        /// </summary>
        /// <param name="estCodigo">Cód do Estabelecimento</param>
        /// <param name="pagDocto">N° do Docto</param>
        /// <param name="fornCodigo">Cód. do Fornecedor</param>
        /// <returns>Retorno da Busca</returns>
        public static string GetVerificaDocto(int estCodigo, string pagDocto, string fornCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_docto", pagDocto));
            ps.Add(new Fields("forn_codigo", fornCodigo));

            string strSql = "SELECT PAG_CODIGO FROM PAGAR WHERE EST_CODIGO = @est_codigo AND PAG_DOCTO = @pag_docto"
                           + " AND FORN_CODIGO = @forn_codigo";

            object r = BancoDados.ExecuteScalar(strSql, ps);
            if (r == null || r == System.DBNull.Value)
                return string.Empty;
            else
                return Convert.ToString(r);
        }

        /// <summary>
        /// METODO QUE ATUALIZA OS DADOS DA TABELA PAGAR, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo Pagar com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(Pagar dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[8, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("pagar", MontadorType.Update);

            //if (!dadosNew.FornCodigo.ToString().Equals(dadosOld.Rows[0]["FORN_CODIGO"].ToString()))
            //{
            //    mont.AddField("forn_codigo", dadosNew.FornCodigo);

            //    dados[contMatriz, 0] = "FORN_CODIGO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["FORN_CODIGO"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.FornCodigo + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.PagDocto.Equals(dadosOld.Rows[0]["PAG_DOCTO"].ToString()))
            //{
            //    if (!String.IsNullOrEmpty(DAL.DALPagar.GetVerificaDocto(dadosNew.EmpCodigo, dadosNew.PagDocto, dadosNew.FornCodigo)))
            //    {
            //        Principal.mensagem = "Já existe um registro com o mesmo número de documento para esse Fornecedor. Não será permitido esta inclusão.";
            //        Funcoes.GetAvisa();
            //        return false;
            //    }

            //    mont.AddField("pag_docto", dadosNew.PagDocto);

            //    dados[contMatriz, 0] = "PAG_DOCTO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["PAG_DOCTO"].ToString() + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.PagDocto + "'";
            //    contMatriz = contMatriz + 1;
            //}
            if (!dadosNew.PagData.Equals(Principal.dtBusca.Rows[0]["PAG_DATA"].ToString().Substring(0, 10)))
            {
                mont.AddField("pag_data", dadosNew.PagData);

                dados[contMatriz, 0] = "PAG_DATA";
                dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["PAG_DATA"].ToString().Substring(0, 10) + "'";
                dados[contMatriz, 2] = "'" + dadosNew.PagData + "'";
                contMatriz = contMatriz + 1;
            }
            //if (!String.Format("{0:N}", dadosNew.PagValPrin).Equals(String.Format("{0:N}", Principal.dtBusca.Rows[0]["PAG_VALPRIN"])))
            //{
            //    mont.AddField("pag_valprin", dadosNew.PagValPrin);

            //    dados[contMatriz, 0] = "PAG_VALPRIN";
            //    dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["PAG_VALPRIN"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.PagValPrin + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.TipCodigo.ToString().Equals(dadosOld.Rows[0]["TIP_CODIGO"].ToString()))
            //{
            //    mont.AddField("tip_codigo", dadosNew.TipCodigo);

            //    dados[contMatriz, 0] = "TIP_CODIGO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TIP_CODIGO"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.TipCodigo + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.DespCodigo.ToString().Equals(dadosOld.Rows[0]["DESP_CODIGO"].ToString()))
            //{
            //    mont.AddField("desp_codigo", dadosNew.DespCodigo);

            //    dados[contMatriz, 0] = "DESP_CODIGO";
            //    dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["DESP_CODIGO"] + "'";
            //    dados[contMatriz, 2] = "'" + dadosNew.DespCodigo + "'";
            //    contMatriz = contMatriz + 1;
            //}
            //if (!dadosNew.Historico.Equals(dadosOld.Rows[0]["HISTORICO"].ToString()))
            //{
            //    mont.AddField("historico", dadosNew.Historico);

            //    dados[contMatriz, 0] = "HISTORICO";
            //    dados[contMatriz, 1] = dadosOld.Rows[0]["HISTORICO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["HISTORICO"] + "'";
            //    dados[contMatriz, 2] = dadosNew.Historico == "" ? "null" : "'" + dadosNew.Historico + "'";
            //    contMatriz = contMatriz + 1;
            //}
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                mont.SetWhere("WHERE PAG_CODIGO = " + dadosNew.PagCodigo + " AND EST_CODIGO = " + dadosNew.EmpCodigo, null);

                if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// ATUALIZA O VALOR TOTAL DA FATURA, USADO NA TELA DE MANUTENÇÃO DE FATURAS
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="pagCodigo">Cód. da Fatura</param>
        /// <param name="valorTotal">Novo valor da Fatura</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaValorTotal(int estCodigo, int pagCodigo, decimal valorTotal, decimal valorAnt)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_codigo", pagCodigo));
            ps.Add(new Fields("valor", valorTotal));
            ps.Add(new Fields("dtalteracao", DateTime.Now));
            ps.Add(new Fields("opalteracao", Principal.usuario));

            string strCmd = "UPDATE PAGAR SET PAG_VALPRIN = @valor, DTALTERACAO = @dtalteracao, OPALTERACAO = @opalteracao WHERE EST_CODIGO = @est_codigo and PAG_CODIGO = @pag_codigo";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) != -1)
            {
                //strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                //       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO)"
                //       + " VALUES ('PAG_VALPRIN','" + pagCodigo + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PAGAR', 'PAG_VALPRIN','"
                //       + Funcoes.BValor(valorAnt) + "','" + Funcoes.BValor(valorTotal) + "'," + estCodigo + ")";
                //if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) == -1)
                //{
                //    return false;
                //}
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// EXCLUI FATURA TANTO DA TABELA PAGAR_DETALHE E PAGAR
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="pagCodigo">Cód. da Fatura</param>
        /// <returns>1, executado com sucesso</returns>
        public static int GetExcluiParcPagar(int estCodigo, string pagCodigo)
        {
            BancoDados.AbrirTrans();

            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_codigo", pagCodigo));

            string strCmd = "DELETE FROM PAGAR_DETALHE WHERE EST_CODIGO = @est_codigo AND PAG_CODIGO = @pag_codigo";
            if (!BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                strCmd = "DELETE FROM PAGAR WHERE EST_CODIGO = @est_codigo AND PAG_CODIGO = @pag_codigo";
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
                {
                    BancoDados.FecharTrans();
                    return 1;
                }
            }

            BancoDados.ErroTrans();
            return 0;
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA CONTAS A PAGAR
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo Pagar com os filtros</param>
        /// <param name="todos">Sem filtro de origem do lançamento</param>
        /// <param name="entrada">Filtro de faturas da entrada de notas</param>
        /// <param name="despesas">Filtro de faturas do lançamento de despesas</param>
        /// <param name="dtIni">Filtro de período Data Inicial</param>
        /// <param name="dtFinal">Filtro de período Data Final</param>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetListarContasPagar(Pagar dadosBusca, bool todos, bool entrada, bool despesas, string dtIni, string dtFinal, int estCodigo)
        {
            string strSql = "";

            if (entrada == true || todos == true)
            {
                strSql = "SELECT B.PAG_ORIGEM, B.PAG_STATUS, A.PD_VENCTO, B.PAG_DOCTO, A.PD_PARCELA, A.PD_VALOR, A.PD_SALDO, D.ENT_DTEMIS AS PAG_DATA,"
                    + " C.FORN_CGC, C.FORN_RAZAO, A.FORN_CODIGO, A.PAG_CODIGO, A.PD_STATUS"
                    + " FROM PAGAR_DETALHE A"
                    + " INNER JOIN PAGAR B ON (A.PAG_CODIGO = B.PAG_CODIGO AND A.EST_CODIGO = B.EST_CODIGO)"
                    + " INNER JOIN FORNECEDORES C ON A.FORN_CODIGO = C.FORN_CODIGO"
                    + " INNER JOIN ENTRADA D ON (A.PAG_CODIGO = D.PAG_CODIGO AND A.EST_CODIGO = D.EST_CODIGO AND A.FORN_CODIGO = D.FORN_CODIGO)"
                    + " WHERE A.EST_CODIGO = " + estCodigo
                    + " AND B.FORN_CODIGO = C.FORN_CODIGO AND A.FORN_CODIGO = D.FORN_CODIGO AND B.PAG_ORIGEM = 'E'";

                //if (!String.IsNullOrEmpty(dadosBusca.PagDocto))
                //{
                //    strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
                //}

                //if (!String.IsNullOrEmpty(dadosBusca.FornCodigo))
                //{
                //    strSql += " AND C.FORN_RAZAO LIKE '" + dadosBusca.FornCodigo + "%'";
                //}

                //if (!dadosBusca.PagStatus.Equals("T"))
                //{
                //    if (dadosBusca.PagStatus.Equals("A"))
                //    {
                //        strSql += " AND A.PD_STATUS <> 'S'";
                //    }
                //    else if (dadosBusca.PagStatus.Equals("Q"))
                //    {
                //        strSql += " AND A.PD_STATUS = 'S'";
                //    }
                //}
                //if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)) && !String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
                //{
                //    Principal.strPesq += " AND A.PD_VENCTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtIni)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
                //}
            }
            if (despesas == true || todos == true)
            {
                if (todos == true)
                {
                    strSql += " UNION ALL";
                }
                strSql += " SELECT A.PAG_ORIGEM, A.PAG_STATUS, B.PD_VENCTO, A.PAG_DOCTO, B.PD_PARCELA, B.PD_VALOR, B.PD_SALDO,"
                    + " A.PAG_DATA, D.FORN_CGC, D.FORN_RAZAO, D.FORN_CODIGO, A.PAG_CODIGO, B.PD_STATUS"
                    + " FROM PAGAR A"
                    + " INNER JOIN PAGAR_DETALHE B ON (A.EST_CODIGO = B.EST_CODIGO AND A.PAG_CODIGO = B.PAG_CODIGO)"
                    + " INNER JOIN DESPESAS C ON (A.DESP_CODIGO = C.DESP_CODIGO)"
                    + " INNER JOIN FORNECEDORES D ON (A.FORN_CODIGO = D.FORN_CODIGO)"
                    + " WHERE A.EST_CODIGO = " + estCodigo
                    + " AND A.FORN_CODIGO = B.FORN_CODIGO  AND A.PAG_ORIGEM = 'D'";

                if (!String.IsNullOrEmpty(dadosBusca.PagDocto))
                {
                    strSql += " AND A.PAG_DOCTO = '" + dadosBusca.PagDocto + "'";
                }

                //if (!String.IsNullOrEmpty(dadosBusca.FornCodigo))
                //{
                //    strSql += " AND D.FORN_RAZAO LIKE '" + dadosBusca.FornCodigo + "%'";
                //}

                //if (!dadosBusca.PagStatus.Equals("T"))
                //{
                //    if (dadosBusca.PagStatus.Equals("A"))
                //    {
                //        strSql += " AND B.PD_STATUS <> 'S'";
                //    }
                //    else if (dadosBusca.PagStatus.Equals("Q"))
                //    {
                //        strSql += " AND B.PD_STATUS = 'S'";
                //    }
                //}

                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)) && !String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
                {
                    strSql += " AND B.PD_VENCTO BETWEEN " + Funcoes.BData(Convert.ToDateTime(dtIni)) + " AND " + Funcoes.BData(Convert.ToDateTime(dtFinal));
                }
                else if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtIni)))
                {
                    strSql += " AND B.PD_VENCTO = " + Funcoes.BData(Convert.ToDateTime(dtIni));
                }
                else if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinal)))
                {
                    strSql += " AND B.PD_VENCTO = " + Funcoes.BData(Convert.ToDateTime(dtFinal));
                }
            }

            if (!String.IsNullOrEmpty(strSql))
            {
                strSql += " ORDER BY PAG_ORIGEM, PAG_STATUS, PAG_DOCTO, PD_PARCELA ";
            }
            return BancoDados.GetDataTable(strSql, null);

        }

        /// <summary>
        /// EXCLUI REGISTRO DA TABELA PAGAR
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="pagCodigo">Cód. da Fatura</param>
        /// <returns>1, executado com sucesso</returns>
        public static int GetExcluiPagar(int estCodigo, int pagCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_codigo", pagCodigo));

            string strCmd = "DELETE FROM PAGAR WHERE EST_CODIGO = @est_codigo AND PAG_CODIGO = @pag_codigo";
            
            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// ATUALIZA O STATUS DA FATURA A-ABERTO,P-PARCIAL-Q-QUITADO
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="pagCodigo">Cód. da Fatura</param>
        /// <param name="status">Status do Pagamento</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaStatusPagto(int estCodigo, int pagCodigo, string status, string statusAnt)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("pag_codigo", pagCodigo));
            ps.Add(new Fields("pag_status", status));
            ps.Add(new Fields("dtalteracao", DateTime.Now));
            ps.Add(new Fields("opalteracao", Principal.usuario));

            string strCmd = "UPDATE PAGAR SET PAG_STATUS = @pag_status, DTALTERACAO = @dtalteracao, OPALTERACAO = @opalteracao WHERE EST_CODIGO = @est_codigo and PAG_CODIGO = @pag_codigo";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) != -1)
            {
                strCmd = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO)"
                       + " VALUES ('PAG_STATUS','" + pagCodigo + "','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PAGAR', 'PAG_STATUS','"
                       + statusAnt + "','" + status + "'," + estCodigo + ")";
                if (BancoDados.ExecuteNoQueryTrans(strCmd, ps) == -1)
                {
                    return false;
                }

                return true;
            }
            else
                return false;
        }
    }
}
