﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using System.Data;

namespace GestaoAdministrativa.DAL
{
    public class DALReceberMovto
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA RECEBER_MOVTO
        /// </summary>
        /// <param name="dados">>Objeto do Tipo ReceberMovto</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(ReceberMovto dados)
        {
            MontadorSql mont = new MontadorSql("receber_movto", MontadorType.Insert);
            mont.AddField("est_codigo", dados.EstCodigo);
            mont.AddField("rm_codigo", dados.RmCodigo);
            mont.AddField("rec_codigo", dados.RecCodigo);
            mont.AddField("rd_parcela", dados.RdParcela);
            mont.AddField("rm_operacao", dados.RmOperacao);
            mont.AddField("rm_data", dados.RmData.ToString("dd/MM/yyyy"));
            mont.AddField("rm_valor", dados.RmValor);
            mont.AddField("rm_historico", dados.RmHistorico);
            mont.AddField("rm_tipo_pagto", dados.RmTipoPagto);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            mont.AddField("movfin_seq", dados.MovfinSeq);
            if (BancoDados.ExecuteNoQueryTrans(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }

        /// <summary>
        ///  EXCLUI REGISTRO DA TABELE RECEBER_MOVTO
        /// </summary>
        /// <param name="recMovto">Objeto do Tipo ReceberMovto</param>
        /// <param name="tParcelas">Indica se exclui todas as parcelas</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetExcluirMovimento(ReceberMovto recMovto, bool tParcelas = true)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", recMovto.EstCodigo));
            ps.Add(new Fields("rec_codigo", recMovto.RecCodigo));
           
            string strCmd = "DELETE FROM RECEBER_MOVTO WHERE EST_CODIGO = @est_codigo AND REC_CODIGO = @rec_codigo ";
            
            if(tParcelas.Equals(false))
            {
                ps.Add(new Fields("rd_parcela", recMovto.RdParcela));
                strCmd += " AND RD_PARCELA = @rd_parcela";
            }

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }


        public static DataTable GetBuscar(Receber rec)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", rec.EmpCodigo));
            //ps.Add(new Fields("cli_id", rec.CliId));
            ps.Add(new Fields("data", rec.RecData));
            ps.Add(new Fields("login", rec.OpCadastro));

            string strSql = "SELECT A.EST_CODIGO,"
                    + " A.REC_CODIGO,"
                    + " C.CLI_DOCTO,"
                    + " A.REC_DOCTO,"
                    + " B.RD_PARCELA,"
                    + " D.RM_VALOR,"
                    + " B.RD_VENCTO,"
                    + " B.RD_SALDO,"
                    + " 0.00 AS ATUAL,"
                    + " A.REC_DATA,"
                    + " 0.00 AS ACRESCIMO,"
                    + " B.DTALTERACAO,"
                    + " (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = B.OPALTERACAO) AS OPALTERACAO,"
                    + " C.CLI_ID,"
                    + " B.RD_STATUS,"
                    + " A.REC_STATUS,"
                    + " B.OPALTERACAO AS OP,"
                    + " A.REC_ORIGEM,"
                    + " D.MOVFIN_SEQ, D.RM_DATA, D.RM_OPERACAO"
                    + " FROM RECEBER_MOVTO D"
                    + " INNER JOIN RECEBER  A ON (D.REC_CODIGO = A.REC_CODIGO AND"
                    + " D.EST_CODIGO = A.EST_CODIGO)"
                    + " INNER JOIN RECEBER_DETALHE B ON (A.REC_CODIGO = B.REC_CODIGO AND"
                    + " A.EST_CODIGO = B.EST_CODIGO)"
                    + " INNER JOIN CLIENTES C ON (C.CLI_ID = A.CLI_ID)"
                    + " WHERE A.EST_CODIGO = @est_codigo"
                    + " AND A.CLI_ID = @cli_id"
                    + " AND D.RM_DATA = @data"
                    + " AND D.OPCADASTRO = @login AND B.RD_STATUS = 'S'"
                    + " GROUP BY A.EST_CODIGO, B.RD_PARCELA, A.REC_CODIGO, C.CLI_DOCTO, A.REC_DOCTO,"
                    + " D.RM_VALOR,"
                    + " B.RD_VENCTO,"
                    + " B.RD_SALDO, A.REC_DATA,  B.DTALTERACAO, B.OPALTERACAO, C.CLI_ID,"
                    + " B.RD_STATUS,"
                    + " A.REC_STATUS,"
                    + " A.REC_ORIGEM, D.MOVFIN_SEQ,  D.RM_DATA, D.RM_OPERACAO"
                    + " ORDER BY A.REC_CODIGO, B.RD_VENCTO";

            return BancoDados.GetDataTable(strSql, ps);
        }

        /// <summary>
        /// EXCLUI OS MOVIMENTOS FEITOS NO RECEBIMENTO DE TÍTULOS
        /// </summary>
        /// <param name="recMovto">Objeto do Tipo ReceberMovto</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetExcluirPorDataESeq(ReceberMovto recMovto)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", recMovto.EstCodigo));
            ps.Add(new Fields("data", recMovto.RmData.ToString("dd/MM/yyyy")));
            ps.Add(new Fields("movfin_seq", recMovto.MovfinSeq));
            ps.Add(new Fields("login", recMovto.OpCadastro));

            string strCmd = "DELETE FROM RECEBER_MOVTO WHERE EST_CODIGO = @est_codigo AND RM_DATA = @data AND MOVFIN_SEQ = @movfin_seq AND OPCADASTRO = @login";

            if (BancoDados.ExecuteNoQueryTrans(strCmd, ps).Equals(-1))
            {
                return false;
            }
            else
                return true;
        }
    }
}
