﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestaoAdministrativa.Negocio;
using System.Data;
using GestaoAdministrativa.Classes;
using SqlNegocio;

namespace GestaoAdministrativa.DAL
{
    public class DALSngpcTabela
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA SNGPC_TABELAS
        /// </summary>
        /// <param name="dados">Objeto do Tipo SngpcTabela</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(SngpcTabela dados)
        {
            MontadorSql mont = new MontadorSql("sngpc_tabelas", MontadorType.Insert);
            mont.AddField("tab_id", dados.TabSeq);
            mont.AddField("tab_tipo", dados.TabTipo);
            mont.AddField("tab_codigo", dados.TabCodigo);
            mont.AddField("tab_descr", dados.TabDescr);
            mont.AddField("tab_liberado", dados.TabLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE CADASTRO DE TABELAS SNGPC
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo SngpcTabela com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(SngpcTabela dadosBusca, out string strOrdem)
        {
            string strSql;

            strSql = "SELECT A.TAB_ID, A.TAB_TIPO, A.TAB_CODIGO, A.TAB_DESCR, A.TAB_LIBERADO, A.DTALTERACAO, (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO)"
                  + " AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO FROM SNGPC_TABELAS A, USUARIOS B WHERE A.OPCADASTRO = B.LOGIN_ID";
            if (dadosBusca.TabSeq == 0 && dadosBusca.TabTipo == 0 && dadosBusca.TabLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.TAB_ID, A.TAB_TIPO, A.TAB_CODIGO";
            }
            else
            {
                if (dadosBusca.TabSeq != 0)
                {
                    strSql += " AND A.TAB_ID = " + dadosBusca.TabSeq;
                }
                if (dadosBusca.TabTipo != 0)
                {
                    strSql += " AND A.TAB_TIPO = " + (dadosBusca.TabTipo + 1);
                }
                if (dadosBusca.TabLiberado != "TODOS")
                {
                    strSql += " AND A.TAB_LIBERADO = '" + dadosBusca.TabLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.TAB_ID, A.TAB_TIPO, A.TAB_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }


        /// <summary>
        /// EXCLUI OS DADOS DA TABELA SNGPC_TABELAS
        /// </summary>
        /// <param name="tabCodigo">Cód. da Tabela</param>
        /// <returns>-1, erro</returns>
        public static int GetExcluirDados(string tabCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("tab_id", tabCodigo));

            string strCmd = "DELETE FROM SNGPC_TABELAS WHERE TAB_ID = @tab_id";

            return BancoDados.ExecuteNoQuery(strCmd, ps);
        }

        /// <summary>
        /// VERIFICA SE CODIGO E TIPO DE TABELA SNGPC ESTA CADASTRADA
        /// </summary>
        /// <param name="tabCodigo">Código da Tabela</param>
        /// <param name="tabTipo">Tipo da Tabela</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetVerificarCodigoTipoExistente(string tabCodigo, int tabTipo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("tab_codigo", tabCodigo));
            ps.Add(new Fields("tab_tipo", tabTipo));

            string strSql = "SELECT * FROM SNGPC_TABELAS WHERE TAB_CODIGO = @tab_codigo AND TAB_TIPO = @tab_tipo";

            return BancoDados.GetDataTable(strSql, ps);
        }


        /// <summary>
        /// ATUALIZA OS DADOS DA TABELA SNGPC_TABELAS, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo SngpcTabela com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(SngpcTabela dadosNew, DataTable dadosOld)
        {
            #region ATUALIZAÇÃO
            string[,] dados = new string[6, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("sngpc_tabelas", MontadorType.Update);
            if (!dadosNew.TabTipo.ToString().Equals(dadosOld.Rows[0]["TAB_TIPO"].ToString()))
            {
                //VERIFICA SE TABELA JÁ ESTA CADASTRADO//
                if (DAL.DALSngpcTabela.GetVerificarCodigoTipoExistente(dadosNew.TabCodigo, dadosNew.TabTipo).Rows.Count != 0)
                {
                    Principal.mensagem = "Tabela já cadastrada.";
                    Funcoes.Avisa();
                    return false;
                }

                mont.AddField("TAB_TIPO", dadosNew.TabTipo);

                dados[contMatriz, 0] = "TAB_TIPO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TAB_TIPO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TabTipo + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TabDescr.Equals(dadosOld.Rows[0]["TAB_CODIGO"].ToString()))
            {
                mont.AddField("TAB_CODIGO", dadosNew.TabCodigo);

                dados[contMatriz, 0] = "TAB_CODIGO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TAB_CODIGO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TabDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TabDescr.Equals(dadosOld.Rows[0]["TAB_DESCR"]))
            {
                mont.AddField("TAB_DESCR", dadosNew.TabDescr);

                dados[contMatriz, 0] = "TAB_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TAB_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TabDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.TabLiberado.Equals(dadosOld.Rows[0]["TAB_LIBERADO"]))
            {
                mont.AddField("TAB_LIBERADO", dadosNew.TabLiberado);

                dados[contMatriz, 0] = "TAB_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["TAB_LIBERADO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.TabLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                mont.SetWhere("WHERE TAB_ID = " + dadosNew.TabSeq, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    Funcoes.GravaLogAlteracao("TAB_ID", dadosNew.TabSeq.ToString(), Principal.usuario, "SNGPC_TABELAS", dados, contMatriz, Principal.estAtual);
                    return true;
                }
                else
                    return false;
            }
            return true;
            #endregion
        }
    }
}
