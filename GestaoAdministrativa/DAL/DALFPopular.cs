﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using GestaoAdministrativa.WSFarmaciaPopular;

namespace GestaoAdministrativa.DAL
{
    class DALFPopular
    {
        private static string retorno;

        // Retorna os dados de acesso da farmácia e do vendedor
        public static DataTable dadosAcessoFP(string idVendedor)
        {
            string sql = "SELECT PAR_DESCRICAO AS LOGIN_FARMACIA,"
                         + " (SELECT PAR_DESCRICAO FROM PARAMETROS WHERE PAR_POSICAO = 176) AS SENHA_FARMACIA,"
                         + " (SELECT PAR_DESCRICAO FROM PARAMETROS WHERE PAR_POSICAO = 129) AS CNPJ_FARMACIA,"
                         + " (SELECT USUARIO_DATASUS FROM FP_VENDEDORES WHERE COL_CODIGO = '" + idVendedor + "' AND EMP_CODIGO = " + Principal.empAtual
                         + " AND EST_CODIGO = " + Principal.estAtual + ") AS LOGIN_VENDEDOR,"
                         + " (SELECT SENHA_DATASUS FROM FP_VENDEDORES WHERE COL_CODIGO = '"+ idVendedor + "' AND EMP_CODIGO = " + Principal.empAtual
                         + " AND EST_CODIGO = " + Principal.estAtual + ") AS SENHA_VENDEDOR"
                         + " FROM PARAMETROS WHERE PAR_POSICAO = 177 ";
            
            return BancoDados.GetDataTable(sql, null);
        }

        public static string getContador()
        {
            string sql = "SELECT * FROM FP_CONTADOR";

            string aux = BancoDados.ExecuteScalar(sql, null).ToString();

            return aux.Remove(aux.IndexOf(','));
        }

        public static bool setContador()
        {
            string sql = "UPDATE FP_CONTADOR SET NUM_SEQ = (NUM_SEQ + 1)";

            if (!BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        #region Geração de DNA e Identidade *gbasmsb.exe*
        // Gera o código da estação, utilizado para cadastrar o computador no portal da Farmácia Popular
        public static string identificaEstacao()
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.Arguments = "/C C:\\FPopular\\gbasmsb.exe --identificacao > C:\\Fpopular\\DNA\\identificacao.txt";
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit();
            string[] lines = File.ReadAllLines(@"C:\FPopular\DNA\identificacao.txt");
            foreach (string line in lines)
            {
                retorno = line;
            }

            return retorno.Trim();
        }

        // Gera o dna da máquina, utilizado na solicitação de compra da farmácia popular
        public static string dnaMaquina(string cpf, string cnpj, string crm, string uf, string dataReceita)
        {
            string retorno = "";

            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.Arguments = "/C C:\\FPopular\\" + Principal.exeDna + " --solicitacao " + cpf + " " + cnpj + " " +
                                       crm + " " + uf + " " + dataReceita + " > C:\\Fpopular\\DNA\\dna.txt";
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Start();
            proc.WaitForExit();

            string[] lines = File.ReadAllLines(@"C:\FPopular\DNA\dna.txt");
            retorno = "";
            foreach (string line in lines)
            {
                if (!String.IsNullOrEmpty(line))
                    retorno = line;
            }

            return retorno.Trim();
        }
        #endregion

        public static bool gravaSolicitacao(SolicitacaoDTO sol, AutorizacaoDTO aut, string nomeCliente, string cpfVendedor)
        {
            string sql = "INSERT INTO FP_SOLICITACAO (SOLICITACAO_ID, DNAESTACAO, DTEMISSAORECEITA, CNPJ, CPFVENDEDOR, CPFCLIENTE, " +
                         "NOMECLIENTE, CRMMEDICO, UFCRM, NUMEROAUTORIZACAO, RETORNOSOLICITACAO, MENSAGEMSOLICITACAO, " +
                         "SITUACAOSOLICITACAO, DATAVENDA, DATAALTERACAO) VALUES(" + sol.coSolicitacaoFarmacia + ",'" + sol.dnaEstacao + "', " +
                         "TO_DATE('" + sol.dtEmissaoReceita.ToString() + "', 'DD/MM/YYYY HH24:MI:SS'),'" +
                         sol.nuCnpj + "','" + cpfVendedor + "','" + sol.nuCpf + "','" + nomeCliente + "','" + sol.nuCrm + "','" + sol.sgUfCrm +
                         "','" + aut.nuAutorizacao.ToString().Replace(".","") + "','" + aut.inAutorizacaoSolicitacao +
                         "','" + aut.descMensagemErro + "', 'P',TO_DATE('" + DateTime.Now + "', 'DD/MM/YYYY HH24:MI:SS'), " +
                         "TO_DATE('" + DateTime.Now.ToString() + "', 'DD/MM/YYYY HH24:MI:SS'))"; 

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                for (int i = 0; i < aut.arrMedicamentoDTO.Length; i++)
                {
                    double qtde = 0;
                        
                    if(aut.arrMedicamentoDTO[i].qtAutorizada != aut.arrMedicamentoDTO[i].qtSolicitada)
                    {
                        qtde = (aut.arrMedicamentoDTO[i].qtAutorizada * aut.arrMedicamentoDTO[i].qtPrescrita) / aut.arrMedicamentoDTO[i].qtSolicitada;
                    }
                    else
                    {
                        qtde = aut.arrMedicamentoDTO[i].qtPrescrita;
                    }

                    sql = "INSERT INTO FP_MEDICAMENTOS_SOLICITACAO (AUTORIZACAOSOLICITACAO, CODBARRAS, UNIDAPRESENTACAO, " +
                          "AUTORIZACAOMEDICAMENTO, QTDAUTORIZADA, QTDPRESCRITA, QTDSOLICITADA, SUBSIDIADOMS, " +
                          "SUBSIDIADOCLIENTE, PRECOVENDA) VALUES ('" + aut.nuAutorizacao.ToString().Replace(".", "") + "','" +
                          aut.arrMedicamentoDTO[i].coCodigoBarra + "','" + aut.arrMedicamentoDTO[i].dsUnidApresentacao +
                          "','" + aut.arrMedicamentoDTO[i].inAutorizacaoMedicamento + "','" +
                          aut.arrMedicamentoDTO[i].qtAutorizada + "','" + qtde +
                          "','" + aut.arrMedicamentoDTO[i].qtSolicitada + "','" + aut.arrMedicamentoDTO[i].vlPrecoSubsidiadoMS +
                          "','" + aut.arrMedicamentoDTO[i].vlPrecoSubsidiadoPaciente + "','" +
                          aut.arrMedicamentoDTO[i].vlPrecoVenda + "')"; 

                    BancoDados.ExecuteNoQuery(sql, null);
                }

                return true;
            }
            else
                return false;
        }

        public static bool gravaConfirmacao(ConfirmacaoAutorizacaoDTO confAut)
        {
            string sql = "UPDATE FP_SOLICITACAO SET RETORNOCONFIRMACAO = '" + confAut.inAutorizacaoSolicitacao + 
                         "', MENSAGEMCONFIRMACAO = '" + confAut.descMensagemErro + "', SITUACAOSOLICITACAO = 'A' " +
                         "WHERE NUMEROAUTORIZACAO LIKE '" + confAut.nuAutorizacao.ToString().Replace(".","") + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public static bool gravaRecebimento(ConfirmacaoRecebimentoDTO confRec)
        {
            string sql = "UPDATE FP_SOLICITACAO SET RETORNORECEBIMENTO = '" + confRec.codigoRetorno +
                         "', MENSAGEMRECEBIMENTO = '" + confRec.mensagemRetorno + "', SITUACAOSOLICITACAO = 'R', " +
                         "CUPOM = '" + confRec.cupomVinculado + "' WHERE NUMEROAUTORIZACAO LIKE '" +
                         confRec.nuAutorizacao.ToString().Replace(".", "") + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public static bool gravaEstorno(ConfirmacaoEstornoDTO confEstorno, string autorizacao)
        {
            string sql = "UPDATE FP_SOLICITACAO SET STATUSESTORNO = '" + confEstorno.inSituacaoEstorno +
                         "', MENSAGEMESTORNO = '" + confEstorno.descMensagemErro + "', SITUACAOSOLICITACAO = 'E' WHERE NUMEROAUTORIZACAO LIKE '" +
                         autorizacao + "'";

            if (BancoDados.ExecuteNoQuery(sql, null).Equals(1))
            {
                return true;
            }
            else
                return false;
        }

        public static bool gravaEstornoMedicamento(ConfirmacaoEstornoDTO confEstorno, string autorizacao)
        {
            
            for(int i = 0; i < confEstorno.arrMedicamentoDTO.Length; i++)
            {
                string sql = "UPDATE FP_MEDICAMENTOS_SOLICITACAO SET AUTORIZACAOESTORNO = '" + confEstorno.arrMedicamentoDTO[i].inAutorizacaoEstorno.ToString().Substring(0,4) +
                         "', QTDDEVOLVIDA = '" + confEstorno.arrMedicamentoDTO[i].qtDevolvida + "', QTDESTORNADA = '" + confEstorno.arrMedicamentoDTO[i].qtEstornada +
                         "', SUBSIDIADOMSPOSESTORNO = " + confEstorno.arrMedicamentoDTO[i].vlrSubsidiadoMSPosEstorno +
                         ", SUBSIDIADOCLIENTEPOSESTORNO = " + confEstorno.arrMedicamentoDTO[i].vlPrecoSubsidiadoPacientePosEstorno +
                         ", TOTALVENDAPOSESTORNO = " + confEstorno.arrMedicamentoDTO[i].vlrTotalVendaPosEstorno +
                         " WHERE AUTORIZACAOSOLICITACAO = '" + autorizacao + "' AND CODBARRAS = '" + confEstorno.arrMedicamentoDTO[i].coCodigoBarra.ToString() + "'";

                if (!BancoDados.ExecuteNoQuery(sql, null).Equals(1))
                {
                    return false;
                }
            }
            return true;
        }

        public static double getPrecoMedicamentoFP(string codBarras)
        {
            string sql = "SELECT PRE_COM_DESC FROM FP_PRODUTOS_PRECOS WHERE PROD_CODIGO LIKE '" + codBarras + "'";

            object preco = BancoDados.ExecuteScalar(sql, null);

            if (preco == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(BancoDados.ExecuteScalar(sql, null));
            }
        }

        public static double getQtdMedicamentoFP(string codBarras)
        {
            string sql = "SELECT COALESCE(PROD_QTDE_UN_COMP_FP,PROD_QTDE_UN_COMP) AS PROD_QTDE_UN_COMP_FP FROM PRODUTOS_DETALHE WHERE PROD_CODIGO LIKE '" + codBarras + "'";

            object qtd = BancoDados.ExecuteScalar(sql, null);

            if ((qtd == null) || (qtd.ToString() == ""))
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(BancoDados.ExecuteScalar(sql, null));
            }
        }

        public static DataTable getVendasFP(DateTime dtInicial, DateTime dtFinal, string cpf = null, string autorizacao = null)
        {
            
            string sql = "SELECT NUMEROAUTORIZACAO, VENDA_ID, CPFCLIENTE, DATAVENDA, " +
                         "(SELECT SUM(SUBSIDIADOMS) FROM FP_MEDICAMENTOS_SOLICITACAO WHERE AUTORIZACAOSOLICITACAO = FP_SOLICITACAO.NUMEROAUTORIZACAO) AS TOTALMS, " +
                         "(SELECT SUM(SUBSIDIADOCLIENTE) FROM FP_MEDICAMENTOS_SOLICITACAO WHERE AUTORIZACAOSOLICITACAO = FP_SOLICITACAO.NUMEROAUTORIZACAO) AS TOTALCLIENTE, " +
                         "(SELECT SUM(PRECOVENDA) FROM FP_MEDICAMENTOS_SOLICITACAO WHERE AUTORIZACAOSOLICITACAO = FP_SOLICITACAO.NUMEROAUTORIZACAO) AS TOTALVENDA " +
                         "FROM FP_SOLICITACAO WHERE DATAVENDA BETWEEN TRUNC(TO_DATE('" + dtInicial.Date.ToString() + "','DD/MM/YYYY HH24:MI:SS')) " +
                         "AND TO_DATE('" + dtFinal.Date.AddHours(23).AddMinutes(59).AddSeconds(59).ToString() + "','DD/MM/YYYY HH24:MI:SS') AND SITUACAOSOLICITACAO = 'R' ";

            if(!string.IsNullOrEmpty(cpf))
            {
                sql += "AND CPFCLIENTE LIKE '" + cpf + "' ";
            }

            if (!string.IsNullOrEmpty(autorizacao))
            {
                sql += "AND NUMEROAUTORIZACAO LIKE '" + autorizacao + "' ";
            }

            sql += "ORDER BY DATAVENDA DESC";

            return BancoDados.GetDataTable(sql,null);
        }

        public static DataTable getMedicamentosSolicitacao(string autorizacao)
        {
            string sql = "SELECT CODBARRAS, QTDAUTORIZADA FROM FP_MEDICAMENTOS_SOLICITACAO WHERE AUTORIZACAOSOLICITACAO = '" + autorizacao + "'";
            
            return BancoDados.GetDataTable(sql, null);
        }

        public static DataTable getMedicamentosConfirmados(string autorizacao)
        {
            string sql = "SELECT MED.CODBARRAS, MED.QTDAUTORIZADA,MED.PRECOVENDA FROM FP_MEDICAMENTOS_SOLICITACAO MED "
                       + " INNER JOIN FP_SOLICITACAO SOL ON (MED.AUTORIZACAOSOLICITACAO = SOL.NUMEROAUTORIZACAO) "
                       + " WHERE SOL.SOLICITACAO_ID = '" + autorizacao + "' AND MED.AUTORIZACAOESTORNO IS NULL ";

            return BancoDados.GetDataTable(sql, null);
        }

        public static DataTable getDadosAcesso(string autorizacao)
        {
            string sql = "SELECT CPFVENDEDOR FROM FP_SOLICITACAO WHERE NUMEROAUTORIZACAO LIKE '" + autorizacao + "'";

            string cpfVendedor = BancoDados.ExecuteScalar(sql,null).ToString();

            sql = "SELECT PAR_DESCRICAO AS LOGIN_FARMACIA,"
                         + " (SELECT PAR_DESCRICAO FROM PARAMETROS WHERE PAR_POSICAO = 176) AS SENHA_FARMACIA,"
                         + " (SELECT PAR_DESCRICAO FROM PARAMETROS WHERE PAR_POSICAO = 129) AS CNPJ_FARMACIA,"
                         + "'" + cpfVendedor + "'  AS LOGIN_VENDEDOR,"
                         + " (SELECT SENHA_DATASUS FROM FP_VENDEDORES WHERE USUARIO_DATASUS = '" + cpfVendedor + "' AND EST_CODIGO = " + Principal.estAtual 
                         +" AND EMP_CODIGO = " + Principal.empAtual + ") AS SENHA_VENDEDOR"
                         + " FROM PARAMETROS WHERE PAR_POSICAO = 177";

            return BancoDados.GetDataTable(sql, null);
        }
    }
}