﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SqlNegocio;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.DAL
{
    class DALTaxaEntrega
    {
        /// <summary>
        /// INSERE OS DADOS NA TABELA TAXA_ENTREGA
        /// </summary>
        /// <param name="dados">Objeto do Tipo TaxaEntrega</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetInserirDados(TaxaEntrega dados)
        {
            MontadorSql mont = new MontadorSql("taxa_entrega", MontadorType.Insert);
            mont.AddField("est_codigo", dados.EstCodigo);
            mont.AddField("entr_codigo", dados.EntrCodigo);
            mont.AddField("entr_descr", dados.EntrDescr);
            mont.AddField("entr_valor", dados.EntrValor);
            mont.AddField("entr_isencao", dados.EntrIsencao);
            mont.AddField("entr_regra", dados.EntrRegra);
            mont.AddField("entr_empres", dados.EntrEmpres);
            mont.AddField("entr_liberado", dados.EntrLiberado);
            mont.AddField("dtcadastro", dados.DtCadastro.ToString("dd/MM/yyyy HH:mm:ss"));
            mont.AddField("opcadastro", dados.OpCadastro);
            if (!BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()).Equals(-1))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// BUSCA TAXA DE ENTREGA CADASTRADAS
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetTaxaEntrega(int estCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));

            string strSql = "SELECT ENTR_VALOR, ENTR_ISENCAO, ENTR_REGRA, ENTR_EMPRES FROM TAXA_ENTREGA WHERE EST_CODIGO = @est_codigo AND ENTR_LIBERADO = 'S'";

            return BancoDados.GetDataTable(strSql, ps);
        }

        /// <summary>
        /// BUSCA OS DADOS DA TELA DE CADASTRO DE TAXA DE ENTREGA
        /// </summary>
        /// <param name="dadosBusca">Objeto do Tipo TaxaEntrega com os filtros</param>
        /// <param name="strOrdem">Variável que retorna com a string de busca</param>
        /// <returns>Retorno da Busca</returns>
        public static DataTable GetBuscar(TaxaEntrega dadosBusca, out string strOrdem)
        {
            string strSql = "  SELECT A.EST_CODIGO, A.ENTR_CODIGO, A.ENTR_DESCR, A.ENTR_VALOR, A.ENTR_ISENCAO, A.ENTR_REGRA, A.ENTR_EMPRES,";
            strSql += " A.ENTR_LIBERADO, A.DTALTERACAO,";
            strSql += " (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO";
            strSql += " FROM TAXA_ENTREGA A";
            strSql += " LEFT JOIN USUARIOS B ON (A.OPCADASTRO = B.LOGIN_ID)";
            strSql += " WHERE A.EST_CODIGO = " + dadosBusca.EstCodigo;

            //BUSCA SEM NENHUM FILTRO//
            if (dadosBusca.EntrDescr == "" && dadosBusca.EntrCodigo == 0 && dadosBusca.EntrLiberado == "TODOS")
            {
                strOrdem = strSql;
                strSql += " ORDER BY A.ENTR_CODIGO";
            }
            else
            {
                if (dadosBusca.EntrCodigo != 0)
                {
                    strSql += " AND A.ENTR_CODIGO = " + dadosBusca.EntrCodigo;
                }
                if (dadosBusca.EntrDescr != "")
                {
                    strSql += " AND A.ENTR_DESCR LIKE '%" + dadosBusca.EntrDescr + "%'";
                }
                if (dadosBusca.EntrLiberado != "TODOS")
                {
                    strSql += " AND A.ENTR_LIBERADO = '" + dadosBusca.EntrLiberado + "'";
                }
                strOrdem = strSql;
                strSql += " ORDER BY A.ENTR_CODIGO";
            }

            return BancoDados.GetDataTable(strSql, null);
        }

        /// <summary>
        /// EXCLUI OS DADOS DA TABELA TAXA_ENTREGA
        /// </summary>
        /// <param name="estCodigo">Cód. do Estabelecimento</param>
        /// <param name="tCodigo">Cód. da Entrega</param>
        /// <returns>-1, erro</returns>
        public static int GetExcluirDados(int estCodigo,string tCodigo)
        {
            SqlParamsList ps = new SqlParamsList();
            ps.Add(new Fields("est_codigo", estCodigo));
            ps.Add(new Fields("entr_codigo", tCodigo));

            string strCmd = "DELETE FROM TAXA_ENTREGA WHERE EST_CODIGO = @est_codigo AND ENTR_CODIGO = @entr_codigo";

            return BancoDados.ExecuteNoQuery(strCmd, ps);
        }

        /// <summary>
        /// ATUALIZA OS DADOS DA TABELA TAXA_ENTREGA, E GRAVA LOG DE ALTERAÇÃO
        /// </summary>
        /// <param name="dadosNew">Objeto do Tipo TaxaEntrega com os novos dados</param>
        /// <param name="dadosOld">DataTable com os dados antigos</param>
        /// <returns>Verdadeiro, executado com sucesso</returns>
        public static bool GetAtualizaDados(TaxaEntrega dadosNew, DataTable dadosOld)
        {
            string[,] dados = new string[8, 3];
            int contMatriz = 0;

            MontadorSql mont = new MontadorSql("taxa_entrega", MontadorType.Update);
            if (!dadosNew.EntrDescr.Equals(dadosOld.Rows[0]["ENTR_DESCR"]))
            {
                mont.AddField("entr_descr", dadosNew.EntrDescr);

                dados[contMatriz, 0] = "ENTR_DESCR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ENTR_DESCR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EntrDescr + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.EntrValor).Equals(String.Format("{0:N}", dadosOld.Rows[0]["ENTR_VALOR"])))
            {
                mont.AddField("entr_valor", dadosNew.EntrValor);

                dados[contMatriz, 0] = "ENTR_VALOR";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ENTR_VALOR"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EntrValor + "'";
                contMatriz = contMatriz + 1;
            }
            if (!String.Format("{0:N}", dadosNew.EntrIsencao).Equals(String.Format("{0:N}", dadosOld.Rows[0]["ENTR_ISENCAO"])))
            {
                mont.AddField("entr_isencao", dadosNew.EntrIsencao);

                dados[contMatriz, 0] = "ENTR_ISENCAO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ENTR_ISENCAO"] + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EntrIsencao + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EntrRegra.Equals(dadosOld.Rows[0]["ENTR_REGRA"]))
            {
                mont.AddField("entr_regra", dadosNew.EntrRegra);

                dados[contMatriz, 0] = "ENTR_REGRA";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ENTR_REGRA"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EntrRegra + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EntrEmpres.Equals(dadosOld.Rows[0]["ENTR_EMPRES"].ToString()))
            {
                mont.AddField("entr_empres", dadosNew.EntrEmpres);

                dados[contMatriz, 0] = "ENTR_EMPRES";
                dados[contMatriz, 1] = dadosOld.Rows[0]["ENTR_EMPRES"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["ENTR_EMPRES"].ToString() + "'";
                dados[contMatriz, 2] = dadosNew.EntrEmpres == "" ? "null" : "'" + dadosNew.EntrEmpres + "'";
                contMatriz = contMatriz + 1;
            }
            if (!dadosNew.EntrLiberado.Equals(dadosOld.Rows[0]["ENTR_LIBERADO"]))
            {
                mont.AddField("entr_liberado", dadosNew.EntrLiberado);

                dados[contMatriz, 0] = "ENTR_LIBERADO";
                dados[contMatriz, 1] = "'" + dadosOld.Rows[0]["ENTR_LIBERADO"].ToString() + "'";
                dados[contMatriz, 2] = "'" + dadosNew.EntrLiberado + "'";
                contMatriz = contMatriz + 1;
            }
            if (contMatriz != 0)
            {
                dados[contMatriz, 0] = "DTALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["DTALTERACAO"] + "'";
                dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                contMatriz = contMatriz + 1;

                dados[contMatriz, 0] = "OPALTERACAO";
                dados[contMatriz, 1] = dadosOld.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + dadosOld.Rows[0]["OPALTERACAO"] + "'";
                dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                contMatriz = contMatriz + 1;

                mont.AddField("dtalteracao", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                mont.AddField("opalteracao", Principal.usuario);
                mont.SetWhere("WHERE est_codigo = " + dadosNew.EstCodigo + " and entr_codigo = " + dadosNew.EntrCodigo, null);

                if (BancoDados.ExecuteNoQuery(mont.GetSqlString(), mont.GetParams()) != -1)
                {
                    Funcoes.GravaLogAlteracao("ENTR_CODIGO", dadosNew.EntrCodigo.ToString(), Principal.usuario, "TAXA_ENTREGA", dados, contMatriz, dadosNew.EstCodigo);
                    return true;
                }
                else
                    return false;
            }
            return true;
        }

    }
}
