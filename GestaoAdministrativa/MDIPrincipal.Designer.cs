﻿namespace GestaoAdministrativa
{
    partial class MDIPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIPrincipal));
            this.msPrincipal = new System.Windows.Forms.MenuStrip();
            this.MGeral = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmParametros = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCadGrupos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCadUsuario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAlterarSenha = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmLogoff = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSuporte = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSenha = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSair = new System.Windows.Forms.ToolStripMenuItem();
            this.MCadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmClasses = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmColaboradores = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDepartamento = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEmpresas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmConveniadas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEspecies = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEspecificacoes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstabelecimentos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFabricantes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCondicoes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFornecedores = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGrupos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoCpfCnpj = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMedicos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmNatOper = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOperacoes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPrinAtivo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProdutos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSubClasse = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTabPrecos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTEntregas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTipoDocto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUnidade = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCVendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDescontos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPromocoes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmComandas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCodNCM = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFPVendedor = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFarmaciaPopular = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeVendedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCadPrecosFP = new System.Windows.Forms.ToolStripMenuItem();
            this.MCaixa = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAberFecha = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCanAberFecha = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmMovCaixa = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRecebimentos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRecebConveniadas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmCRelatorios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCFechaDiario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMovimentoEspecie = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMovimentacaoOperaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumoDiárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.recebimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MContPagar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFinDespesas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFinContasPagar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutParcelas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCadDespesas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmFinRelatorio = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelPagFornecedores = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmCaixaXContas = new System.Windows.Forms.ToolStripMenuItem();
            this.MContReceber = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLancamentoTitulos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFinContasReceber = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRecManutParcelas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCadTitulo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmCartaCobranca = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRecRelatorio = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelEmpresaConveniada = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelEmpresasConvenio = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelRecebimentoTitulo = new System.Windows.Forms.ToolStripMenuItem();
            this.MEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstAjuste = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstAbcFarma = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAtuPreProd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTabelaDCB = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAtuTabIBPT = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAtualizacaoLinearDePreco = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstAtuLinear = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmComissaoPorDepartamento = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstEntradas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstoqueFiliais = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEstManutencaoCodBarras = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoDeProdutos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoCusto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmReposicaoProdutos = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmERelatorios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelAjustes = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelBalanco = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelComissaoProduto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelEntDocumento = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelAbaixoEst = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelFichaProduto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelImpostos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelPrecosAlterados = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelValidade = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoDeFilial = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManuAtualizacaoPreco = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManuAtualizacaoComissao = new System.Windows.Forms.ToolStripMenuItem();
            this.MSngpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSngpcTecnico = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSngpcTabelas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEntrTrans = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmInventario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLanInventario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmGerarInventario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPerdas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmTransferencia = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVendaMed = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmRelatoriosSNGPC = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBalancoSngpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmComprasDeMedicamentos = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeEstoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelInventario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHistorioDeEnvios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPerdasDeMedicamentos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVendasDeMedicamentos = new System.Windows.Forms.ToolStripMenuItem();
            this.MVendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenComanda = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCancelamentoVendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmControleEntrega = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDevolucaoDeProdutos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenImportarTransferencia = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLancamentoDevolucao = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmManutencaoComanda = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenTransferencia = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmVenNFe = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAutorizacoesFarmaciaPopular = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenEstornoManual = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenUtilitariosEcf = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmControleEntregaFilial = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLancamentoEntrega = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmVenRelatorios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenRelComissaoProd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenRelComissaoVen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelVenDescontoConcedido = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmVendaEntregaFilial = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenRelPeriodo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenRelEspecie = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmVenRelFormaPagto = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmRelProdXCusto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProdutoPorCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelProdutoPorEspecie = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProdutoPorPeriodo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProdutosMaisVendidos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProdutosMenosVendidos = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmRelCurvaAbc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelLucroXCusto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelFormaXEspecie = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelListaPosNeg = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelVendasCanceladas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelVendaFiscal = new System.Windows.Forms.ToolStripMenuItem();
            this.MRelatorios = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelEmpresaParticular = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelComandasAberto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelEntregasAberto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEntregaPeriodo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLogAlteracao = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRelTransferencia = new System.Windows.Forms.ToolStripMenuItem();
            this.MAberturaChamado = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tslCaps = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslNum = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslInsert = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslData = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMensagem = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbMenu = new System.Windows.Forms.ToolStripProgressBar();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAtualiza = new System.Windows.Forms.ToolStripButton();
            this.btnExcluir = new System.Windows.Forms.ToolStripButton();
            this.btnIncluir = new System.Windows.Forms.ToolStripButton();
            this.btnLimpar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.btnAnterior = new System.Windows.Forms.ToolStripButton();
            this.btnProximo = new System.Windows.Forms.ToolStripButton();
            this.btnUltimo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnConPrecos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.btnImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLogoff = new System.Windows.Forms.ToolStripButton();
            this.btnAjuda = new System.Windows.Forms.ToolStripButton();
            this.btnInternet = new System.Windows.Forms.ToolStripButton();
            this.btnSuporte = new System.Windows.Forms.ToolStripButton();
            this.btnAcesso = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.btnEstab = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.btnBackup = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.btnMensagem = new System.Windows.Forms.ToolStripButton();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.stsBotoes = new System.Windows.Forms.StatusStrip();
            this.timerReserva = new System.Windows.Forms.Timer(this.components);
            this.timerEntrega = new System.Windows.Forms.Timer(this.components);
            this.timerImpressao = new System.Windows.Forms.Timer(this.components);
            this.timerAvisos = new System.Windows.Forms.Timer(this.components);
            this.timerPreco = new System.Windows.Forms.Timer(this.components);
            this.timerPromocaoFilial = new System.Windows.Forms.Timer(this.components);
            this.msPrincipal.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // msPrincipal
            // 
            this.msPrincipal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.msPrincipal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MGeral,
            this.MCadastro,
            this.MCaixa,
            this.MContPagar,
            this.MContReceber,
            this.MEstoque,
            this.MSngpc,
            this.MVendas,
            this.MRelatorios,
            this.MAberturaChamado});
            this.msPrincipal.Location = new System.Drawing.Point(0, 0);
            this.msPrincipal.Name = "msPrincipal";
            this.msPrincipal.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.msPrincipal.Size = new System.Drawing.Size(994, 24);
            this.msPrincipal.TabIndex = 0;
            this.msPrincipal.Text = "MenuStrip";
            // 
            // MGeral
            // 
            this.MGeral.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmParametros,
            this.tsmUsuarios,
            this.tsmAlterarSenha,
            this.toolStripSeparator4,
            this.tsmLogoff,
            this.tsmSuporte,
            this.tsmSair});
            this.MGeral.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.MGeral.Name = "MGeral";
            this.MGeral.Size = new System.Drawing.Size(49, 20);
            this.MGeral.Tag = "1";
            this.MGeral.Text = "&Geral";
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Image = ((System.Drawing.Image)(resources.GetObject("tsmConfigurar.Image")));
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(199, 22);
            this.tsmConfigurar.Text = "Configurar Impressora";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmParametros
            // 
            this.tsmParametros.Name = "tsmParametros";
            this.tsmParametros.Size = new System.Drawing.Size(199, 22);
            this.tsmParametros.Text = "&Parâmetros";
            this.tsmParametros.Click += new System.EventHandler(this.tsmParametros_Click);
            // 
            // tsmUsuarios
            // 
            this.tsmUsuarios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCadGrupos,
            this.tsmCadUsuario});
            this.tsmUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("tsmUsuarios.Image")));
            this.tsmUsuarios.Name = "tsmUsuarios";
            this.tsmUsuarios.Size = new System.Drawing.Size(199, 22);
            this.tsmUsuarios.Text = "&Usuários";
            // 
            // tsmCadGrupos
            // 
            this.tsmCadGrupos.Name = "tsmCadGrupos";
            this.tsmCadGrupos.Size = new System.Drawing.Size(186, 22);
            this.tsmCadGrupos.Text = "Grupos de Usuários";
            this.tsmCadGrupos.Click += new System.EventHandler(this.tsmCadGrupos_Click);
            // 
            // tsmCadUsuario
            // 
            this.tsmCadUsuario.Name = "tsmCadUsuario";
            this.tsmCadUsuario.Size = new System.Drawing.Size(186, 22);
            this.tsmCadUsuario.Text = "Usuários";
            this.tsmCadUsuario.Click += new System.EventHandler(this.tsmCadUsuario_Click);
            // 
            // tsmAlterarSenha
            // 
            this.tsmAlterarSenha.Name = "tsmAlterarSenha";
            this.tsmAlterarSenha.Size = new System.Drawing.Size(199, 22);
            this.tsmAlterarSenha.Text = "&Alteração de Senha";
            this.tsmAlterarSenha.Click += new System.EventHandler(this.tsmAlterarSenha_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(196, 6);
            // 
            // tsmLogoff
            // 
            this.tsmLogoff.Image = ((System.Drawing.Image)(resources.GetObject("tsmLogoff.Image")));
            this.tsmLogoff.Name = "tsmLogoff";
            this.tsmLogoff.Size = new System.Drawing.Size(199, 22);
            this.tsmLogoff.Text = "&Logoff";
            this.tsmLogoff.Click += new System.EventHandler(this.tsmLogoff_Click);
            // 
            // tsmSuporte
            // 
            this.tsmSuporte.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSenha});
            this.tsmSuporte.Name = "tsmSuporte";
            this.tsmSuporte.Size = new System.Drawing.Size(199, 22);
            this.tsmSuporte.Text = "Suporte";
            // 
            // tsmSenha
            // 
            this.tsmSenha.Name = "tsmSenha";
            this.tsmSenha.Size = new System.Drawing.Size(197, 22);
            this.tsmSenha.Text = "Manutenção de Senha";
            this.tsmSenha.Click += new System.EventHandler(this.tsmSenha_Click);
            // 
            // tsmSair
            // 
            this.tsmSair.Image = ((System.Drawing.Image)(resources.GetObject("tsmSair.Image")));
            this.tsmSair.Name = "tsmSair";
            this.tsmSair.Size = new System.Drawing.Size(199, 22);
            this.tsmSair.Text = "&Encerrar Sistema";
            this.tsmSair.Click += new System.EventHandler(this.tsmSair_Click);
            // 
            // MCadastro
            // 
            this.MCadastro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmClasses,
            this.tsmClientes,
            this.tsmColaboradores,
            this.tsmDepartamento,
            this.tsmEmpresas,
            this.tsmConveniadas,
            this.tsmEspecies,
            this.tsmEspecificacoes,
            this.tsmEstabelecimentos,
            this.tsmFabricantes,
            this.tsmCondicoes,
            this.tsmFornecedores,
            this.tsmGrupos,
            this.tsmManutencaoCpfCnpj,
            this.tsmMedicos,
            this.tsmNatOper,
            this.tsmOperacoes,
            this.tsmPrinAtivo,
            this.tsmProdutos,
            this.tsmSubClasse,
            this.tsmTabPrecos,
            this.tsmTEntregas,
            this.tsmTipoDocto,
            this.tsmUnidade,
            this.tsmCVendas,
            this.tsmFarmaciaPopular});
            this.MCadastro.Name = "MCadastro";
            this.MCadastro.Size = new System.Drawing.Size(77, 20);
            this.MCadastro.Text = "&Cadastros";
            // 
            // tsmClasses
            // 
            this.tsmClasses.Name = "tsmClasses";
            this.tsmClasses.Size = new System.Drawing.Size(227, 22);
            this.tsmClasses.Text = "Classes";
            this.tsmClasses.Click += new System.EventHandler(this.tsmClasses_Click);
            // 
            // tsmClientes
            // 
            this.tsmClientes.Name = "tsmClientes";
            this.tsmClientes.Size = new System.Drawing.Size(227, 22);
            this.tsmClientes.Text = "Clientes";
            this.tsmClientes.Click += new System.EventHandler(this.tsmClientes_Click);
            // 
            // tsmColaboradores
            // 
            this.tsmColaboradores.Name = "tsmColaboradores";
            this.tsmColaboradores.Size = new System.Drawing.Size(227, 22);
            this.tsmColaboradores.Text = "&Colaboradores";
            this.tsmColaboradores.Click += new System.EventHandler(this.tsmColaboradores_Click);
            // 
            // tsmDepartamento
            // 
            this.tsmDepartamento.Name = "tsmDepartamento";
            this.tsmDepartamento.Size = new System.Drawing.Size(227, 22);
            this.tsmDepartamento.Text = "Departamentos";
            this.tsmDepartamento.Click += new System.EventHandler(this.tsmDepartamento_Click);
            // 
            // tsmEmpresas
            // 
            this.tsmEmpresas.Name = "tsmEmpresas";
            this.tsmEmpresas.Size = new System.Drawing.Size(227, 22);
            this.tsmEmpresas.Text = "Empresas";
            this.tsmEmpresas.Click += new System.EventHandler(this.tsmEmpresas_Click);
            // 
            // tsmConveniadas
            // 
            this.tsmConveniadas.Name = "tsmConveniadas";
            this.tsmConveniadas.Size = new System.Drawing.Size(227, 22);
            this.tsmConveniadas.Text = "Empresas Conveniadas";
            this.tsmConveniadas.Click += new System.EventHandler(this.tsmConveniadas_Click);
            // 
            // tsmEspecies
            // 
            this.tsmEspecies.Name = "tsmEspecies";
            this.tsmEspecies.Size = new System.Drawing.Size(227, 22);
            this.tsmEspecies.Text = "Espécies";
            this.tsmEspecies.Click += new System.EventHandler(this.tsmEspecies_Click);
            // 
            // tsmEspecificacoes
            // 
            this.tsmEspecificacoes.Name = "tsmEspecificacoes";
            this.tsmEspecificacoes.Size = new System.Drawing.Size(227, 22);
            this.tsmEspecificacoes.Text = "Especificações";
            this.tsmEspecificacoes.Click += new System.EventHandler(this.tsmEspecificacoes_Click);
            // 
            // tsmEstabelecimentos
            // 
            this.tsmEstabelecimentos.Name = "tsmEstabelecimentos";
            this.tsmEstabelecimentos.Size = new System.Drawing.Size(227, 22);
            this.tsmEstabelecimentos.Text = "E&stabelecimentos";
            this.tsmEstabelecimentos.Click += new System.EventHandler(this.tsmEstabelecimentos_Click);
            // 
            // tsmFabricantes
            // 
            this.tsmFabricantes.Name = "tsmFabricantes";
            this.tsmFabricantes.Size = new System.Drawing.Size(227, 22);
            this.tsmFabricantes.Text = "Fabricantes";
            this.tsmFabricantes.Click += new System.EventHandler(this.tsmFabricantes_Click);
            // 
            // tsmCondicoes
            // 
            this.tsmCondicoes.Name = "tsmCondicoes";
            this.tsmCondicoes.Size = new System.Drawing.Size(227, 22);
            this.tsmCondicoes.Text = "Formas de Pagamento";
            this.tsmCondicoes.Click += new System.EventHandler(this.tsmCondicoes_Click);
            // 
            // tsmFornecedores
            // 
            this.tsmFornecedores.Name = "tsmFornecedores";
            this.tsmFornecedores.Size = new System.Drawing.Size(227, 22);
            this.tsmFornecedores.Text = "Fornecedores";
            this.tsmFornecedores.Click += new System.EventHandler(this.tsmFornecedores_Click);
            // 
            // tsmGrupos
            // 
            this.tsmGrupos.Name = "tsmGrupos";
            this.tsmGrupos.Size = new System.Drawing.Size(227, 22);
            this.tsmGrupos.Text = "Grupo de Clientes";
            this.tsmGrupos.Click += new System.EventHandler(this.tsmGrupos_Click);
            // 
            // tsmManutencaoCpfCnpj
            // 
            this.tsmManutencaoCpfCnpj.Name = "tsmManutencaoCpfCnpj";
            this.tsmManutencaoCpfCnpj.Size = new System.Drawing.Size(227, 22);
            this.tsmManutencaoCpfCnpj.Text = "Manutenção de CPF/CNPJ";
            this.tsmManutencaoCpfCnpj.Click += new System.EventHandler(this.tsmManutencaoCpfCnpj_Click);
            // 
            // tsmMedicos
            // 
            this.tsmMedicos.Name = "tsmMedicos";
            this.tsmMedicos.Size = new System.Drawing.Size(227, 22);
            this.tsmMedicos.Text = "Médicos";
            this.tsmMedicos.Click += new System.EventHandler(this.tsmMedicos_Click);
            // 
            // tsmNatOper
            // 
            this.tsmNatOper.Name = "tsmNatOper";
            this.tsmNatOper.Size = new System.Drawing.Size(227, 22);
            this.tsmNatOper.Text = "Natureza de Operação";
            this.tsmNatOper.Click += new System.EventHandler(this.tsmNatOper_Click);
            // 
            // tsmOperacoes
            // 
            this.tsmOperacoes.Name = "tsmOperacoes";
            this.tsmOperacoes.Size = new System.Drawing.Size(227, 22);
            this.tsmOperacoes.Text = "Operações Débito e Crédito";
            this.tsmOperacoes.Click += new System.EventHandler(this.tsmOperacoes_Click);
            // 
            // tsmPrinAtivo
            // 
            this.tsmPrinAtivo.Name = "tsmPrinAtivo";
            this.tsmPrinAtivo.Size = new System.Drawing.Size(227, 22);
            this.tsmPrinAtivo.Text = "Princípio Ativo";
            this.tsmPrinAtivo.Click += new System.EventHandler(this.tsmPrinAtivo_Click);
            // 
            // tsmProdutos
            // 
            this.tsmProdutos.Name = "tsmProdutos";
            this.tsmProdutos.Size = new System.Drawing.Size(227, 22);
            this.tsmProdutos.Text = "Produtos";
            this.tsmProdutos.Click += new System.EventHandler(this.tsmProdutos_Click);
            // 
            // tsmSubClasse
            // 
            this.tsmSubClasse.Name = "tsmSubClasse";
            this.tsmSubClasse.Size = new System.Drawing.Size(227, 22);
            this.tsmSubClasse.Text = "Subclasses";
            this.tsmSubClasse.Click += new System.EventHandler(this.tsmSubClasse_Click);
            // 
            // tsmTabPrecos
            // 
            this.tsmTabPrecos.Name = "tsmTabPrecos";
            this.tsmTabPrecos.Size = new System.Drawing.Size(227, 22);
            this.tsmTabPrecos.Text = "Tabela de Preços";
            this.tsmTabPrecos.Click += new System.EventHandler(this.tsmTabPrecos_Click);
            // 
            // tsmTEntregas
            // 
            this.tsmTEntregas.Name = "tsmTEntregas";
            this.tsmTEntregas.Size = new System.Drawing.Size(227, 22);
            this.tsmTEntregas.Text = "Taxa de Entrega";
            this.tsmTEntregas.Visible = false;
            this.tsmTEntregas.Click += new System.EventHandler(this.tsmTEntregas_Click);
            // 
            // tsmTipoDocto
            // 
            this.tsmTipoDocto.Name = "tsmTipoDocto";
            this.tsmTipoDocto.Size = new System.Drawing.Size(227, 22);
            this.tsmTipoDocto.Text = "Tipo de Documento";
            this.tsmTipoDocto.Click += new System.EventHandler(this.tsmTipoDocto_Click);
            // 
            // tsmUnidade
            // 
            this.tsmUnidade.Name = "tsmUnidade";
            this.tsmUnidade.Size = new System.Drawing.Size(227, 22);
            this.tsmUnidade.Text = "Tipo de Unidade";
            this.tsmUnidade.Click += new System.EventHandler(this.tsmUnidade_Click);
            // 
            // tsmCVendas
            // 
            this.tsmCVendas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmDescontos,
            this.tsmPromocoes,
            this.tsmComandas,
            this.tsmCodNCM,
            this.tsmFPVendedor});
            this.tsmCVendas.Name = "tsmCVendas";
            this.tsmCVendas.Size = new System.Drawing.Size(227, 22);
            this.tsmCVendas.Text = "Vendas";
            // 
            // tsmDescontos
            // 
            this.tsmDescontos.Name = "tsmDescontos";
            this.tsmDescontos.Size = new System.Drawing.Size(227, 22);
            this.tsmDescontos.Text = "Descontos";
            this.tsmDescontos.Click += new System.EventHandler(this.tsmDescontos_Click);
            // 
            // tsmPromocoes
            // 
            this.tsmPromocoes.Name = "tsmPromocoes";
            this.tsmPromocoes.Size = new System.Drawing.Size(227, 22);
            this.tsmPromocoes.Text = "Promoções";
            this.tsmPromocoes.Click += new System.EventHandler(this.tsmPromocoes_Click);
            // 
            // tsmComandas
            // 
            this.tsmComandas.Name = "tsmComandas";
            this.tsmComandas.Size = new System.Drawing.Size(227, 22);
            this.tsmComandas.Text = "Comandas";
            this.tsmComandas.Click += new System.EventHandler(this.tsmComandas_Click);
            // 
            // tsmCodNCM
            // 
            this.tsmCodNCM.Name = "tsmCodNCM";
            this.tsmCodNCM.Size = new System.Drawing.Size(227, 22);
            this.tsmCodNCM.Text = "Código NCM";
            this.tsmCodNCM.Visible = false;
            this.tsmCodNCM.Click += new System.EventHandler(this.tsmCodNCM_Click);
            // 
            // tsmFPVendedor
            // 
            this.tsmFPVendedor.Name = "tsmFPVendedor";
            this.tsmFPVendedor.Size = new System.Drawing.Size(227, 22);
            this.tsmFPVendedor.Text = "Vendedor Farmácia Popular";
            this.tsmFPVendedor.Visible = false;
            this.tsmFPVendedor.Click += new System.EventHandler(this.tsmFPVendedor_Click);
            // 
            // tsmFarmaciaPopular
            // 
            this.tsmFarmaciaPopular.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeVendedoresToolStripMenuItem,
            this.tsmCadPrecosFP});
            this.tsmFarmaciaPopular.Name = "tsmFarmaciaPopular";
            this.tsmFarmaciaPopular.Size = new System.Drawing.Size(227, 22);
            this.tsmFarmaciaPopular.Text = "Farmácia Popular";
            // 
            // cadastroDeVendedoresToolStripMenuItem
            // 
            this.cadastroDeVendedoresToolStripMenuItem.Name = "cadastroDeVendedoresToolStripMenuItem";
            this.cadastroDeVendedoresToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.cadastroDeVendedoresToolStripMenuItem.Text = "Cadastro de Vendedores";
            this.cadastroDeVendedoresToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeVendedoresToolStripMenuItem_Click);
            // 
            // tsmCadPrecosFP
            // 
            this.tsmCadPrecosFP.Name = "tsmCadPrecosFP";
            this.tsmCadPrecosFP.Size = new System.Drawing.Size(214, 22);
            this.tsmCadPrecosFP.Text = "Preços Farmácia Popular";
            this.tsmCadPrecosFP.Click += new System.EventHandler(this.tsmCadPrecosFP_Click);
            // 
            // MCaixa
            // 
            this.MCaixa.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmAberFecha,
            this.tsmCanAberFecha,
            this.toolStripSeparator6,
            this.tsmMovCaixa,
            this.tsmRecebimentos,
            this.tsmRecebConveniadas,
            this.toolStripSeparator14,
            this.tsmCRelatorios});
            this.MCaixa.Name = "MCaixa";
            this.MCaixa.Size = new System.Drawing.Size(50, 20);
            this.MCaixa.Text = "C&aixa";
            // 
            // tsmAberFecha
            // 
            this.tsmAberFecha.Name = "tsmAberFecha";
            this.tsmAberFecha.Size = new System.Drawing.Size(302, 22);
            this.tsmAberFecha.Text = "Abertura/Fechamento";
            this.tsmAberFecha.Click += new System.EventHandler(this.tsmAberFecha_Click);
            // 
            // tsmCanAberFecha
            // 
            this.tsmCanAberFecha.Name = "tsmCanAberFecha";
            this.tsmCanAberFecha.Size = new System.Drawing.Size(302, 22);
            this.tsmCanAberFecha.Text = "Cancela Abertura/Fechamento";
            this.tsmCanAberFecha.Click += new System.EventHandler(this.tsmCanAberFecha_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(299, 6);
            // 
            // tsmMovCaixa
            // 
            this.tsmMovCaixa.Name = "tsmMovCaixa";
            this.tsmMovCaixa.Size = new System.Drawing.Size(302, 22);
            this.tsmMovCaixa.Text = "Movimentação de Caixa";
            this.tsmMovCaixa.Click += new System.EventHandler(this.tsmMovCaixa_Click);
            // 
            // tsmRecebimentos
            // 
            this.tsmRecebimentos.Name = "tsmRecebimentos";
            this.tsmRecebimentos.Size = new System.Drawing.Size(302, 22);
            this.tsmRecebimentos.Text = "Recebimentos de Títulos";
            this.tsmRecebimentos.Click += new System.EventHandler(this.tsmRecebimentos_Click);
            // 
            // tsmRecebConveniadas
            // 
            this.tsmRecebConveniadas.Name = "tsmRecebConveniadas";
            this.tsmRecebConveniadas.Size = new System.Drawing.Size(302, 22);
            this.tsmRecebConveniadas.Text = "Recebimento de Empresas Conveniadas";
            this.tsmRecebConveniadas.Click += new System.EventHandler(this.tsmRecebConveniadas_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(299, 6);
            // 
            // tsmCRelatorios
            // 
            this.tsmCRelatorios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCFechaDiario,
            this.tsmMovimentoEspecie,
            this.tsmMovimentacaoOperaçãoToolStripMenuItem,
            this.resumoDiárioToolStripMenuItem,
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem,
            this.toolStripSeparator23,
            this.recebimentosToolStripMenuItem});
            this.tsmCRelatorios.Name = "tsmCRelatorios";
            this.tsmCRelatorios.Size = new System.Drawing.Size(302, 22);
            this.tsmCRelatorios.Text = "Relatórios";
            // 
            // tsmCFechaDiario
            // 
            this.tsmCFechaDiario.Name = "tsmCFechaDiario";
            this.tsmCFechaDiario.Size = new System.Drawing.Size(283, 22);
            this.tsmCFechaDiario.Text = "Fechamento Diário";
            this.tsmCFechaDiario.Click += new System.EventHandler(this.tsmCFechaDiario_Click);
            // 
            // tsmMovimentoEspecie
            // 
            this.tsmMovimentoEspecie.Name = "tsmMovimentoEspecie";
            this.tsmMovimentoEspecie.Size = new System.Drawing.Size(283, 22);
            this.tsmMovimentoEspecie.Text = "Movimentação por Espécie";
            this.tsmMovimentoEspecie.Click += new System.EventHandler(this.tsmMovimentoEspecie_Click);
            // 
            // tsmMovimentacaoOperaçãoToolStripMenuItem
            // 
            this.tsmMovimentacaoOperaçãoToolStripMenuItem.Name = "tsmMovimentacaoOperaçãoToolStripMenuItem";
            this.tsmMovimentacaoOperaçãoToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.tsmMovimentacaoOperaçãoToolStripMenuItem.Text = "Movimentação por Operação";
            this.tsmMovimentacaoOperaçãoToolStripMenuItem.Click += new System.EventHandler(this.tsmMovimentacaoOperaçãoToolStripMenuItem_Click);
            // 
            // resumoDiárioToolStripMenuItem
            // 
            this.resumoDiárioToolStripMenuItem.Name = "resumoDiárioToolStripMenuItem";
            this.resumoDiárioToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.resumoDiárioToolStripMenuItem.Text = "Resumo Diário";
            this.resumoDiárioToolStripMenuItem.Click += new System.EventHandler(this.resumoDiárioToolStripMenuItem_Click);
            // 
            // resumoGeralDeVendasPorPerídoToolStripMenuItem
            // 
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem.Name = "resumoGeralDeVendasPorPerídoToolStripMenuItem";
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem.Text = "Resumo Geral de Vendas por Período";
            this.resumoGeralDeVendasPorPerídoToolStripMenuItem.Click += new System.EventHandler(this.resumoGeralDeVendasPorPerídoToolStripMenuItem_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(280, 6);
            // 
            // recebimentosToolStripMenuItem
            // 
            this.recebimentosToolStripMenuItem.Name = "recebimentosToolStripMenuItem";
            this.recebimentosToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.recebimentosToolStripMenuItem.Text = "Recebimentos de Particular";
            this.recebimentosToolStripMenuItem.Click += new System.EventHandler(this.recebimentosToolStripMenuItem_Click);
            // 
            // MContPagar
            // 
            this.MContPagar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFinDespesas,
            this.tsmFinContasPagar,
            this.tsmManutParcelas,
            this.tsmCadDespesas,
            this.toolStripSeparator7,
            this.tsmFinRelatorio,
            this.toolStripSeparator21,
            this.tsmCaixaXContas});
            this.MContPagar.Name = "MContPagar";
            this.MContPagar.Size = new System.Drawing.Size(105, 20);
            this.MContPagar.Text = "Contas a &Pagar";
            this.MContPagar.Click += new System.EventHandler(this.MContPagar_Click);
            // 
            // tsmFinDespesas
            // 
            this.tsmFinDespesas.Name = "tsmFinDespesas";
            this.tsmFinDespesas.Size = new System.Drawing.Size(279, 22);
            this.tsmFinDespesas.Text = "Lançamento de Despesas";
            this.tsmFinDespesas.Click += new System.EventHandler(this.tsmFinDespesas_Click);
            // 
            // tsmFinContasPagar
            // 
            this.tsmFinContasPagar.Name = "tsmFinContasPagar";
            this.tsmFinContasPagar.Size = new System.Drawing.Size(279, 22);
            this.tsmFinContasPagar.Text = "Manutenção de Despesas";
            this.tsmFinContasPagar.Click += new System.EventHandler(this.tsmFinContasPagar_Click);
            // 
            // tsmManutParcelas
            // 
            this.tsmManutParcelas.Name = "tsmManutParcelas";
            this.tsmManutParcelas.Size = new System.Drawing.Size(279, 22);
            this.tsmManutParcelas.Text = "Manutenção de Parcelas (Despesas)";
            this.tsmManutParcelas.Visible = false;
            this.tsmManutParcelas.Click += new System.EventHandler(this.tsmManutParcelas_Click);
            // 
            // tsmCadDespesas
            // 
            this.tsmCadDespesas.Name = "tsmCadDespesas";
            this.tsmCadDespesas.Size = new System.Drawing.Size(279, 22);
            this.tsmCadDespesas.Text = "Tipo de Despesas";
            this.tsmCadDespesas.Click += new System.EventHandler(this.tsmCadDespesas_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(276, 6);
            this.toolStripSeparator7.Visible = false;
            // 
            // tsmFinRelatorio
            // 
            this.tsmFinRelatorio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRelPagFornecedores});
            this.tsmFinRelatorio.Name = "tsmFinRelatorio";
            this.tsmFinRelatorio.Size = new System.Drawing.Size(279, 22);
            this.tsmFinRelatorio.Text = "Relatórios";
            this.tsmFinRelatorio.Click += new System.EventHandler(this.tsmFinRelatorio_Click);
            // 
            // tsmRelPagFornecedores
            // 
            this.tsmRelPagFornecedores.Name = "tsmRelPagFornecedores";
            this.tsmRelPagFornecedores.Size = new System.Drawing.Size(196, 22);
            this.tsmRelPagFornecedores.Text = "Boletos Fornecedores";
            this.tsmRelPagFornecedores.Click += new System.EventHandler(this.tsmRelPagFornecedores_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(276, 6);
            this.toolStripSeparator21.Visible = false;
            // 
            // tsmCaixaXContas
            // 
            this.tsmCaixaXContas.Name = "tsmCaixaXContas";
            this.tsmCaixaXContas.Size = new System.Drawing.Size(279, 22);
            this.tsmCaixaXContas.Text = "Posição de Caixa X Contas à Pagar";
            this.tsmCaixaXContas.Click += new System.EventHandler(this.tsmCaixaXContas_Click);
            // 
            // MContReceber
            // 
            this.MContReceber.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmLancamentoTitulos,
            this.tsmFinContasReceber,
            this.tsmRecManutParcelas,
            this.tsmCadTitulo,
            this.toolStripSeparator16,
            this.tsmCartaCobranca,
            this.tsmRecRelatorio});
            this.MContReceber.Name = "MContReceber";
            this.MContReceber.Size = new System.Drawing.Size(119, 20);
            this.MContReceber.Text = "Contas a &Receber";
            // 
            // tsmLancamentoTitulos
            // 
            this.tsmLancamentoTitulos.Name = "tsmLancamentoTitulos";
            this.tsmLancamentoTitulos.Size = new System.Drawing.Size(261, 22);
            this.tsmLancamentoTitulos.Text = "Lançamento de Títulos";
            this.tsmLancamentoTitulos.Click += new System.EventHandler(this.tsmLancamentoTitulos_Click);
            // 
            // tsmFinContasReceber
            // 
            this.tsmFinContasReceber.Name = "tsmFinContasReceber";
            this.tsmFinContasReceber.Size = new System.Drawing.Size(261, 22);
            this.tsmFinContasReceber.Text = "Lançamento/Manutenção (Títulos)";
            this.tsmFinContasReceber.Visible = false;
            this.tsmFinContasReceber.Click += new System.EventHandler(this.tsmFinContasReceber_Click);
            // 
            // tsmRecManutParcelas
            // 
            this.tsmRecManutParcelas.Name = "tsmRecManutParcelas";
            this.tsmRecManutParcelas.Size = new System.Drawing.Size(261, 22);
            this.tsmRecManutParcelas.Text = "Manutenção Parcelas (Títulos)";
            this.tsmRecManutParcelas.Visible = false;
            this.tsmRecManutParcelas.Click += new System.EventHandler(this.tsmRecManutParcelas_Click);
            // 
            // tsmCadTitulo
            // 
            this.tsmCadTitulo.Name = "tsmCadTitulo";
            this.tsmCadTitulo.Size = new System.Drawing.Size(261, 22);
            this.tsmCadTitulo.Text = "Tipo de Títulos";
            this.tsmCadTitulo.Visible = false;
            this.tsmCadTitulo.Click += new System.EventHandler(this.tsmCadTitulo_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(258, 6);
            this.toolStripSeparator16.Visible = false;
            // 
            // tsmCartaCobranca
            // 
            this.tsmCartaCobranca.Name = "tsmCartaCobranca";
            this.tsmCartaCobranca.Size = new System.Drawing.Size(261, 22);
            this.tsmCartaCobranca.Text = "Carta de Cobrança";
            this.tsmCartaCobranca.Click += new System.EventHandler(this.tsmCartaCobranca_Click);
            // 
            // tsmRecRelatorio
            // 
            this.tsmRecRelatorio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRelEmpresaConveniada,
            this.tsmRelEmpresasConvenio,
            this.tsmRelRecebimentoTitulo});
            this.tsmRecRelatorio.Name = "tsmRecRelatorio";
            this.tsmRecRelatorio.Size = new System.Drawing.Size(261, 22);
            this.tsmRecRelatorio.Text = "Relatórios";
            // 
            // tsmRelEmpresaConveniada
            // 
            this.tsmRelEmpresaConveniada.Name = "tsmRelEmpresaConveniada";
            this.tsmRelEmpresaConveniada.Size = new System.Drawing.Size(271, 22);
            this.tsmRelEmpresaConveniada.Text = "Empresas Conveniadas (Particular)";
            this.tsmRelEmpresaConveniada.Click += new System.EventHandler(this.tsmRelEmpresaConveniada_Click);
            // 
            // tsmRelEmpresasConvenio
            // 
            this.tsmRelEmpresasConvenio.Name = "tsmRelEmpresasConvenio";
            this.tsmRelEmpresasConvenio.Size = new System.Drawing.Size(271, 22);
            this.tsmRelEmpresasConvenio.Text = "Empresas Conveniadas (Convênio)";
            this.tsmRelEmpresasConvenio.Click += new System.EventHandler(this.tsmRelEmpresasConvenio_Click);
            // 
            // tsmRelRecebimentoTitulo
            // 
            this.tsmRelRecebimentoTitulo.Name = "tsmRelRecebimentoTitulo";
            this.tsmRelRecebimentoTitulo.Size = new System.Drawing.Size(271, 22);
            this.tsmRelRecebimentoTitulo.Text = "Extrato por Cliente";
            this.tsmRelRecebimentoTitulo.Click += new System.EventHandler(this.tsmRelRecebimentoTitulo_Click);
            // 
            // MEstoque
            // 
            this.MEstoque.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmEstAjuste,
            this.tsmEstAbcFarma,
            this.tsmAtuPreProd,
            this.tsmTabelaDCB,
            this.tsmAtuTabIBPT,
            this.tsmAtualizacaoLinearDePreco,
            this.tsmEstAtuLinear,
            this.tsmComissaoPorDepartamento,
            this.tsmEstEntradas,
            this.tsmEstoqueFiliais,
            this.tsmEstManutencaoCodBarras,
            this.tsmManutencaoDeProdutos,
            this.tsmManutencaoEstoque,
            this.tsmManutencaoCusto,
            this.tsmReposicaoProdutos,
            this.toolStripSeparator5,
            this.tsmERelatorios,
            this.tsmManutencaoDeFilial});
            this.MEstoque.Name = "MEstoque";
            this.MEstoque.Size = new System.Drawing.Size(65, 20);
            this.MEstoque.Text = "&Estoque";
            // 
            // tsmEstAjuste
            // 
            this.tsmEstAjuste.Name = "tsmEstAjuste";
            this.tsmEstAjuste.Size = new System.Drawing.Size(301, 22);
            this.tsmEstAjuste.Text = "Ajuste de Estoque";
            this.tsmEstAjuste.Click += new System.EventHandler(this.tsmEstAjuste_Click);
            // 
            // tsmEstAbcFarma
            // 
            this.tsmEstAbcFarma.Name = "tsmEstAbcFarma";
            this.tsmEstAbcFarma.Size = new System.Drawing.Size(301, 22);
            this.tsmEstAbcFarma.Text = "Atualiza Preços ABCFarma";
            this.tsmEstAbcFarma.Click += new System.EventHandler(this.tsmEstAbcFarma_Click);
            // 
            // tsmAtuPreProd
            // 
            this.tsmAtuPreProd.Name = "tsmAtuPreProd";
            this.tsmAtuPreProd.Size = new System.Drawing.Size(301, 22);
            this.tsmAtuPreProd.Text = "Atualiza Preços Tabela Anvisa";
            this.tsmAtuPreProd.Click += new System.EventHandler(this.tsmAtuPreProd_Click);
            // 
            // tsmTabelaDCB
            // 
            this.tsmTabelaDCB.Name = "tsmTabelaDCB";
            this.tsmTabelaDCB.Size = new System.Drawing.Size(301, 22);
            this.tsmTabelaDCB.Text = "Atualiza Tabela DCB";
            this.tsmTabelaDCB.Click += new System.EventHandler(this.tsmTabelaDCB_Click);
            // 
            // tsmAtuTabIBPT
            // 
            this.tsmAtuTabIBPT.Name = "tsmAtuTabIBPT";
            this.tsmAtuTabIBPT.Size = new System.Drawing.Size(301, 22);
            this.tsmAtuTabIBPT.Text = "Atualiza Tabela IBPT";
            this.tsmAtuTabIBPT.Click += new System.EventHandler(this.tsmAtuTabIBPT_Click);
            // 
            // tsmAtualizacaoLinearDePreco
            // 
            this.tsmAtualizacaoLinearDePreco.Name = "tsmAtualizacaoLinearDePreco";
            this.tsmAtualizacaoLinearDePreco.Size = new System.Drawing.Size(301, 22);
            this.tsmAtualizacaoLinearDePreco.Text = "Atualização Linear de Preço";
            this.tsmAtualizacaoLinearDePreco.Click += new System.EventHandler(this.atualizaçãoLinearDePreçoToolStripMenuItem_Click);
            // 
            // tsmEstAtuLinear
            // 
            this.tsmEstAtuLinear.Name = "tsmEstAtuLinear";
            this.tsmEstAtuLinear.Size = new System.Drawing.Size(301, 22);
            this.tsmEstAtuLinear.Text = "Atualização Linear de Produtos";
            this.tsmEstAtuLinear.Click += new System.EventHandler(this.tsmEstAtuLinear_Click);
            // 
            // tsmComissaoPorDepartamento
            // 
            this.tsmComissaoPorDepartamento.Name = "tsmComissaoPorDepartamento";
            this.tsmComissaoPorDepartamento.Size = new System.Drawing.Size(301, 22);
            this.tsmComissaoPorDepartamento.Text = "Comissão por Departamento";
            this.tsmComissaoPorDepartamento.Click += new System.EventHandler(this.tsmComissaoPorDepartamento_Click);
            // 
            // tsmEstEntradas
            // 
            this.tsmEstEntradas.Name = "tsmEstEntradas";
            this.tsmEstEntradas.Size = new System.Drawing.Size(301, 22);
            this.tsmEstEntradas.Text = "Entrada de Notas";
            this.tsmEstEntradas.Click += new System.EventHandler(this.tsmEstEntradas_Click);
            // 
            // tsmEstoqueFiliais
            // 
            this.tsmEstoqueFiliais.Name = "tsmEstoqueFiliais";
            this.tsmEstoqueFiliais.Size = new System.Drawing.Size(301, 22);
            this.tsmEstoqueFiliais.Text = "Estoque de Produto Filiais";
            this.tsmEstoqueFiliais.Click += new System.EventHandler(this.tsmEstoqueFiliais_Click);
            // 
            // tsmEstManutencaoCodBarras
            // 
            this.tsmEstManutencaoCodBarras.Name = "tsmEstManutencaoCodBarras";
            this.tsmEstManutencaoCodBarras.Size = new System.Drawing.Size(301, 22);
            this.tsmEstManutencaoCodBarras.Text = "Manutenção de Código de Barras";
            this.tsmEstManutencaoCodBarras.Click += new System.EventHandler(this.tsmEstManutencaoCodBarras_Click);
            // 
            // tsmManutencaoDeProdutos
            // 
            this.tsmManutencaoDeProdutos.Name = "tsmManutencaoDeProdutos";
            this.tsmManutencaoDeProdutos.Size = new System.Drawing.Size(301, 22);
            this.tsmManutencaoDeProdutos.Text = "Manutenção de Produtos (Exclusão)";
            this.tsmManutencaoDeProdutos.Click += new System.EventHandler(this.manutencaoDeProdutosToolStripMenuItem_Click);
            // 
            // tsmManutencaoEstoque
            // 
            this.tsmManutencaoEstoque.Name = "tsmManutencaoEstoque";
            this.tsmManutencaoEstoque.Size = new System.Drawing.Size(301, 22);
            this.tsmManutencaoEstoque.Text = "Manutenção da Quantidade do Estoque";
            this.tsmManutencaoEstoque.Click += new System.EventHandler(this.tsmManutencaoEstoque_Click);
            // 
            // tsmManutencaoCusto
            // 
            this.tsmManutencaoCusto.Name = "tsmManutencaoCusto";
            this.tsmManutencaoCusto.Size = new System.Drawing.Size(301, 22);
            this.tsmManutencaoCusto.Text = "Manutenção do Último Custo";
            this.tsmManutencaoCusto.Click += new System.EventHandler(this.tsmManutencaoCusto_Click);
            // 
            // tsmReposicaoProdutos
            // 
            this.tsmReposicaoProdutos.Name = "tsmReposicaoProdutos";
            this.tsmReposicaoProdutos.Size = new System.Drawing.Size(301, 22);
            this.tsmReposicaoProdutos.Text = "Reposição de Produtos Vendidos / Faltas";
            this.tsmReposicaoProdutos.Click += new System.EventHandler(this.tsmReposicaoProdutos_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(298, 6);
            // 
            // tsmERelatorios
            // 
            this.tsmERelatorios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRelAjustes,
            this.tsmRelBalanco,
            this.tsmRelComissaoProduto,
            this.tsmRelEntDocumento,
            this.tsmRelAbaixoEst,
            this.tsmRelFichaProduto,
            this.tsmRelImpostos,
            this.tsmRelPrecosAlterados,
            this.tsmRelValidade});
            this.tsmERelatorios.Name = "tsmERelatorios";
            this.tsmERelatorios.Size = new System.Drawing.Size(301, 22);
            this.tsmERelatorios.Text = "Relatórios";
            // 
            // tsmRelAjustes
            // 
            this.tsmRelAjustes.Name = "tsmRelAjustes";
            this.tsmRelAjustes.Size = new System.Drawing.Size(225, 22);
            this.tsmRelAjustes.Text = "Ajustes por Período";
            this.tsmRelAjustes.Click += new System.EventHandler(this.tsmRelAjustes_Click);
            // 
            // tsmRelBalanco
            // 
            this.tsmRelBalanco.Name = "tsmRelBalanco";
            this.tsmRelBalanco.Size = new System.Drawing.Size(225, 22);
            this.tsmRelBalanco.Text = "Balanço";
            this.tsmRelBalanco.Click += new System.EventHandler(this.tsmRelBalanco_Click);
            // 
            // tsmRelComissaoProduto
            // 
            this.tsmRelComissaoProduto.Name = "tsmRelComissaoProduto";
            this.tsmRelComissaoProduto.Size = new System.Drawing.Size(225, 22);
            this.tsmRelComissaoProduto.Text = "Comissão de Produtos";
            this.tsmRelComissaoProduto.Click += new System.EventHandler(this.tsmRelComissaoProduto_Click);
            // 
            // tsmRelEntDocumento
            // 
            this.tsmRelEntDocumento.Name = "tsmRelEntDocumento";
            this.tsmRelEntDocumento.Size = new System.Drawing.Size(225, 22);
            this.tsmRelEntDocumento.Text = "Documentos de Entrada";
            this.tsmRelEntDocumento.Click += new System.EventHandler(this.tsmRelEntDocumento_Click);
            // 
            // tsmRelAbaixoEst
            // 
            this.tsmRelAbaixoEst.Name = "tsmRelAbaixoEst";
            this.tsmRelAbaixoEst.Size = new System.Drawing.Size(225, 22);
            this.tsmRelAbaixoEst.Text = "Estoque Máximo/Mínimo";
            this.tsmRelAbaixoEst.Click += new System.EventHandler(this.tsmRelAbaixoEst_Click);
            // 
            // tsmRelFichaProduto
            // 
            this.tsmRelFichaProduto.Name = "tsmRelFichaProduto";
            this.tsmRelFichaProduto.Size = new System.Drawing.Size(225, 22);
            this.tsmRelFichaProduto.Text = "Ficha do Produto";
            this.tsmRelFichaProduto.Click += new System.EventHandler(this.tsmRelFichaProduto_Click);
            // 
            // tsmRelImpostos
            // 
            this.tsmRelImpostos.Name = "tsmRelImpostos";
            this.tsmRelImpostos.Size = new System.Drawing.Size(225, 22);
            this.tsmRelImpostos.Text = "Impostos";
            this.tsmRelImpostos.Click += new System.EventHandler(this.tsmRelImpostos_Click);
            // 
            // tsmRelPrecosAlterados
            // 
            this.tsmRelPrecosAlterados.Name = "tsmRelPrecosAlterados";
            this.tsmRelPrecosAlterados.Size = new System.Drawing.Size(225, 22);
            this.tsmRelPrecosAlterados.Text = "Preços Alterados AbcFarma";
            this.tsmRelPrecosAlterados.Click += new System.EventHandler(this.tsmRelPrecosAlterados_Click);
            // 
            // tsmRelValidade
            // 
            this.tsmRelValidade.Name = "tsmRelValidade";
            this.tsmRelValidade.Size = new System.Drawing.Size(225, 22);
            this.tsmRelValidade.Text = "Validade dos Produtos";
            this.tsmRelValidade.Visible = false;
            // 
            // tsmManutencaoDeFilial
            // 
            this.tsmManutencaoDeFilial.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmManuAtualizacaoPreco,
            this.tsmManuAtualizacaoComissao});
            this.tsmManutencaoDeFilial.Name = "tsmManutencaoDeFilial";
            this.tsmManutencaoDeFilial.Size = new System.Drawing.Size(301, 22);
            this.tsmManutencaoDeFilial.Text = "Manutenção Filiais";
            // 
            // tsmManuAtualizacaoPreco
            // 
            this.tsmManuAtualizacaoPreco.Name = "tsmManuAtualizacaoPreco";
            this.tsmManuAtualizacaoPreco.Size = new System.Drawing.Size(214, 22);
            this.tsmManuAtualizacaoPreco.Text = "Atualização de Preço";
            this.tsmManuAtualizacaoPreco.Click += new System.EventHandler(this.tsmManuAtualizacaoPreco_Click);
            // 
            // tsmManuAtualizacaoComissao
            // 
            this.tsmManuAtualizacaoComissao.Name = "tsmManuAtualizacaoComissao";
            this.tsmManuAtualizacaoComissao.Size = new System.Drawing.Size(214, 22);
            this.tsmManuAtualizacaoComissao.Text = "Atualização de Comissão";
            this.tsmManuAtualizacaoComissao.Click += new System.EventHandler(this.tsmManuAtualizacaoComissao_Click);
            // 
            // MSngpc
            // 
            this.MSngpc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmSngpcTecnico,
            this.tsmSngpcTabelas,
            this.tsmEntrTrans,
            this.tsmInventario,
            this.tsmPerdas,
            this.tsmTransferencia,
            this.tsmVendaMed,
            this.toolStripSeparator20,
            this.tsmRelatoriosSNGPC});
            this.MSngpc.Name = "MSngpc";
            this.MSngpc.Size = new System.Drawing.Size(62, 20);
            this.MSngpc.Text = "&SNGPC";
            // 
            // tsmSngpcTecnico
            // 
            this.tsmSngpcTecnico.Name = "tsmSngpcTecnico";
            this.tsmSngpcTecnico.Size = new System.Drawing.Size(309, 22);
            this.tsmSngpcTecnico.Text = "Cadastro de Téc. Farmacêutico";
            this.tsmSngpcTecnico.Click += new System.EventHandler(this.tsmSngpcTecnico_Click);
            // 
            // tsmSngpcTabelas
            // 
            this.tsmSngpcTabelas.Name = "tsmSngpcTabelas";
            this.tsmSngpcTabelas.Size = new System.Drawing.Size(309, 22);
            this.tsmSngpcTabelas.Text = "Cadastro de Tabelas";
            this.tsmSngpcTabelas.Click += new System.EventHandler(this.tsmSngpcTabelas_Click);
            // 
            // tsmEntrTrans
            // 
            this.tsmEntrTrans.Name = "tsmEntrTrans";
            this.tsmEntrTrans.Size = new System.Drawing.Size(309, 22);
            this.tsmEntrTrans.Text = "Compra e Transferência de Medicamentos";
            this.tsmEntrTrans.Click += new System.EventHandler(this.tsmEntrTrans_Click);
            // 
            // tsmInventario
            // 
            this.tsmInventario.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmLanInventario,
            this.tsmGerarInventario});
            this.tsmInventario.Name = "tsmInventario";
            this.tsmInventario.Size = new System.Drawing.Size(309, 22);
            this.tsmInventario.Text = "Inventário";
            // 
            // tsmLanInventario
            // 
            this.tsmLanInventario.Name = "tsmLanInventario";
            this.tsmLanInventario.Size = new System.Drawing.Size(216, 22);
            this.tsmLanInventario.Text = "Lançamento de Inventário";
            this.tsmLanInventario.Click += new System.EventHandler(this.tsmLanInventario2_Click);
            // 
            // tsmGerarInventario
            // 
            this.tsmGerarInventario.Name = "tsmGerarInventario";
            this.tsmGerarInventario.Size = new System.Drawing.Size(216, 22);
            this.tsmGerarInventario.Text = "Gerar Inventário (XML)";
            this.tsmGerarInventario.Click += new System.EventHandler(this.dsToolStripMenuItem_Click);
            // 
            // tsmPerdas
            // 
            this.tsmPerdas.Name = "tsmPerdas";
            this.tsmPerdas.Size = new System.Drawing.Size(309, 22);
            this.tsmPerdas.Text = "Perda de Medicamentos";
            this.tsmPerdas.Click += new System.EventHandler(this.tsmPerdas_Click);
            // 
            // tsmTransferencia
            // 
            this.tsmTransferencia.Name = "tsmTransferencia";
            this.tsmTransferencia.Size = new System.Drawing.Size(309, 22);
            this.tsmTransferencia.Text = "Transferência SNGPC";
            this.tsmTransferencia.Click += new System.EventHandler(this.tsmTransferencia_Click);
            // 
            // tsmVendaMed
            // 
            this.tsmVendaMed.Name = "tsmVendaMed";
            this.tsmVendaMed.Size = new System.Drawing.Size(309, 22);
            this.tsmVendaMed.Text = "Venda de Medicamentos";
            this.tsmVendaMed.Click += new System.EventHandler(this.tsmVendaMed_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(306, 6);
            // 
            // tsmRelatoriosSNGPC
            // 
            this.tsmRelatoriosSNGPC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmBalancoSngpc,
            this.tsmComprasDeMedicamentos,
            this.relatórioDeEstoqueToolStripMenuItem,
            this.tsmRelInventario,
            this.tsmHistorioDeEnvios,
            this.tsmPerdasDeMedicamentos,
            this.tsmVendasDeMedicamentos});
            this.tsmRelatoriosSNGPC.Name = "tsmRelatoriosSNGPC";
            this.tsmRelatoriosSNGPC.Size = new System.Drawing.Size(309, 22);
            this.tsmRelatoriosSNGPC.Text = "Relatórios SNGPC";
            // 
            // tsmBalancoSngpc
            // 
            this.tsmBalancoSngpc.Name = "tsmBalancoSngpc";
            this.tsmBalancoSngpc.Size = new System.Drawing.Size(227, 22);
            this.tsmBalancoSngpc.Text = "Balanço";
            this.tsmBalancoSngpc.Click += new System.EventHandler(this.tsmBalancoSngpc_Click);
            // 
            // tsmComprasDeMedicamentos
            // 
            this.tsmComprasDeMedicamentos.Name = "tsmComprasDeMedicamentos";
            this.tsmComprasDeMedicamentos.Size = new System.Drawing.Size(227, 22);
            this.tsmComprasDeMedicamentos.Text = "Compras de Medicamentos";
            this.tsmComprasDeMedicamentos.Click += new System.EventHandler(this.comprasDeMedicamentosToolStripMenuItem_Click);
            // 
            // relatórioDeEstoqueToolStripMenuItem
            // 
            this.relatórioDeEstoqueToolStripMenuItem.Name = "relatórioDeEstoqueToolStripMenuItem";
            this.relatórioDeEstoqueToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.relatórioDeEstoqueToolStripMenuItem.Text = "Estoque";
            this.relatórioDeEstoqueToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeEstoqueToolStripMenuItem_Click);
            // 
            // tsmRelInventario
            // 
            this.tsmRelInventario.Name = "tsmRelInventario";
            this.tsmRelInventario.Size = new System.Drawing.Size(227, 22);
            this.tsmRelInventario.Text = "Inventário";
            this.tsmRelInventario.Click += new System.EventHandler(this.tsmRelInventario_Click);
            // 
            // tsmHistorioDeEnvios
            // 
            this.tsmHistorioDeEnvios.Name = "tsmHistorioDeEnvios";
            this.tsmHistorioDeEnvios.Size = new System.Drawing.Size(227, 22);
            this.tsmHistorioDeEnvios.Text = "Histórico de Envios";
            this.tsmHistorioDeEnvios.Click += new System.EventHandler(this.tsmHistorioDeEnvios_Click);
            // 
            // tsmPerdasDeMedicamentos
            // 
            this.tsmPerdasDeMedicamentos.Name = "tsmPerdasDeMedicamentos";
            this.tsmPerdasDeMedicamentos.Size = new System.Drawing.Size(227, 22);
            this.tsmPerdasDeMedicamentos.Text = "Perdas de Medicamentos";
            this.tsmPerdasDeMedicamentos.Click += new System.EventHandler(this.perdasDeMedicamentosToolStripMenuItem_Click);
            // 
            // tsmVendasDeMedicamentos
            // 
            this.tsmVendasDeMedicamentos.Name = "tsmVendasDeMedicamentos";
            this.tsmVendasDeMedicamentos.Size = new System.Drawing.Size(227, 22);
            this.tsmVendasDeMedicamentos.Text = "Vendas de Medicamentos";
            this.tsmVendasDeMedicamentos.Click += new System.EventHandler(this.vendasDeMedicamentosToolStripMenuItem_Click);
            // 
            // MVendas
            // 
            this.MVendas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmVenComanda,
            this.tsmCancelamentoVendas,
            this.tsmControleEntrega,
            this.tsmDevolucaoDeProdutos,
            this.tsmVenImportarTransferencia,
            this.tsmLancamentoDevolucao,
            this.tsmManutencaoComanda,
            this.tsmVenTransferencia,
            this.toolStripSeparator15,
            this.tsmVenNFe,
            this.tsmAutorizacoesFarmaciaPopular,
            this.tsmVenEstornoManual,
            this.tsmVenUtilitariosEcf,
            this.toolStripSeparator3,
            this.tsmControleEntregaFilial,
            this.tsmLancamentoEntrega,
            this.toolStripSeparator22,
            this.tsmVenRelatorios});
            this.MVendas.Name = "MVendas";
            this.MVendas.Size = new System.Drawing.Size(60, 20);
            this.MVendas.Text = "&Vendas";
            // 
            // tsmVenComanda
            // 
            this.tsmVenComanda.Name = "tsmVenComanda";
            this.tsmVenComanda.Size = new System.Drawing.Size(291, 22);
            this.tsmVenComanda.Text = "Vendas";
            this.tsmVenComanda.Click += new System.EventHandler(this.tsmVenComanda_Click);
            // 
            // tsmCancelamentoVendas
            // 
            this.tsmCancelamentoVendas.Name = "tsmCancelamentoVendas";
            this.tsmCancelamentoVendas.Size = new System.Drawing.Size(291, 22);
            this.tsmCancelamentoVendas.Text = "Cancelamento de Vendas";
            this.tsmCancelamentoVendas.Click += new System.EventHandler(this.tsmCancelamentoVendas_Click);
            // 
            // tsmControleEntrega
            // 
            this.tsmControleEntrega.Name = "tsmControleEntrega";
            this.tsmControleEntrega.Size = new System.Drawing.Size(291, 22);
            this.tsmControleEntrega.Text = "Controle de Entregas";
            this.tsmControleEntrega.Click += new System.EventHandler(this.tsmControleEntrega_Click);
            // 
            // tsmDevolucaoDeProdutos
            // 
            this.tsmDevolucaoDeProdutos.Name = "tsmDevolucaoDeProdutos";
            this.tsmDevolucaoDeProdutos.Size = new System.Drawing.Size(291, 22);
            this.tsmDevolucaoDeProdutos.Text = "Devolução de Produtos";
            this.tsmDevolucaoDeProdutos.Click += new System.EventHandler(this.tsmDevolucaoDeProdutos_Click);
            // 
            // tsmVenImportarTransferencia
            // 
            this.tsmVenImportarTransferencia.Name = "tsmVenImportarTransferencia";
            this.tsmVenImportarTransferencia.Size = new System.Drawing.Size(291, 22);
            this.tsmVenImportarTransferencia.Text = "Importar Transferência de Produtos";
            this.tsmVenImportarTransferencia.Click += new System.EventHandler(this.tsmVenImportarTransferencia_Click);
            // 
            // tsmLancamentoDevolucao
            // 
            this.tsmLancamentoDevolucao.Name = "tsmLancamentoDevolucao";
            this.tsmLancamentoDevolucao.Size = new System.Drawing.Size(291, 22);
            this.tsmLancamentoDevolucao.Text = "Lançamento de Devolução de Produtos";
            this.tsmLancamentoDevolucao.Click += new System.EventHandler(this.tsmLancamentoDevolucao_Click);
            // 
            // tsmManutencaoComanda
            // 
            this.tsmManutencaoComanda.Name = "tsmManutencaoComanda";
            this.tsmManutencaoComanda.Size = new System.Drawing.Size(291, 22);
            this.tsmManutencaoComanda.Text = "Manutenção de Comandas em Aberto";
            this.tsmManutencaoComanda.Click += new System.EventHandler(this.tsmManutencaoComanda_Click);
            // 
            // tsmVenTransferencia
            // 
            this.tsmVenTransferencia.Name = "tsmVenTransferencia";
            this.tsmVenTransferencia.Size = new System.Drawing.Size(291, 22);
            this.tsmVenTransferencia.Text = "Transferência de Produtos";
            this.tsmVenTransferencia.Click += new System.EventHandler(this.tsmVenTransferencia_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(288, 6);
            // 
            // tsmVenNFe
            // 
            this.tsmVenNFe.Name = "tsmVenNFe";
            this.tsmVenNFe.Size = new System.Drawing.Size(291, 22);
            this.tsmVenNFe.Text = "Emissão de NF-e";
            this.tsmVenNFe.Click += new System.EventHandler(this.tsmVenNFe_Click);
            // 
            // tsmAutorizacoesFarmaciaPopular
            // 
            this.tsmAutorizacoesFarmaciaPopular.Name = "tsmAutorizacoesFarmaciaPopular";
            this.tsmAutorizacoesFarmaciaPopular.Size = new System.Drawing.Size(291, 22);
            this.tsmAutorizacoesFarmaciaPopular.Text = "Estorno Farmácia Popular";
            this.tsmAutorizacoesFarmaciaPopular.Click += new System.EventHandler(this.autorizaçõesFarmáciaPopularToolStripMenuItem_Click);
            // 
            // tsmVenEstornoManual
            // 
            this.tsmVenEstornoManual.Name = "tsmVenEstornoManual";
            this.tsmVenEstornoManual.Size = new System.Drawing.Size(291, 22);
            this.tsmVenEstornoManual.Text = "Estorno Manual Farmácia Popular";
            this.tsmVenEstornoManual.Click += new System.EventHandler(this.tsmVenEstornoManual_Click);
            // 
            // tsmVenUtilitariosEcf
            // 
            this.tsmVenUtilitariosEcf.Name = "tsmVenUtilitariosEcf";
            this.tsmVenUtilitariosEcf.Size = new System.Drawing.Size(291, 22);
            this.tsmVenUtilitariosEcf.Text = "Utilitários ECF/SAT";
            this.tsmVenUtilitariosEcf.Click += new System.EventHandler(this.tsmVenUtilitariosEcf_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(288, 6);
            // 
            // tsmControleEntregaFilial
            // 
            this.tsmControleEntregaFilial.Name = "tsmControleEntregaFilial";
            this.tsmControleEntregaFilial.Size = new System.Drawing.Size(291, 22);
            this.tsmControleEntregaFilial.Text = "Controle de Entrega Filial";
            this.tsmControleEntregaFilial.Click += new System.EventHandler(this.tsmControleEntregaFilial_Click);
            // 
            // tsmLancamentoEntrega
            // 
            this.tsmLancamentoEntrega.Name = "tsmLancamentoEntrega";
            this.tsmLancamentoEntrega.Size = new System.Drawing.Size(291, 22);
            this.tsmLancamentoEntrega.Text = "Lançamento Entrega Filial";
            this.tsmLancamentoEntrega.Click += new System.EventHandler(this.tsmLancamentoEntrega_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(288, 6);
            // 
            // tsmVenRelatorios
            // 
            this.tsmVenRelatorios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmVenRelComissaoProd,
            this.tsmVenRelComissaoVen,
            this.tsmRelVenDescontoConcedido,
            this.toolStripSeparator18,
            this.tsmVendaEntregaFilial,
            this.tsmVenRelPeriodo,
            this.tsmVenRelEspecie,
            this.tsmVenRelFormaPagto,
            this.toolStripSeparator17,
            this.tsmRelProdXCusto,
            this.tsmProdutoPorCliente,
            this.tsmRelProdutoPorEspecie,
            this.tsmProdutoPorPeriodo,
            this.tsmProdutosMaisVendidos,
            this.tsmProdutosMenosVendidos,
            this.toolStripSeparator19,
            this.tsmRelCurvaAbc,
            this.tsmRelLucroXCusto,
            this.tsmRelFormaXEspecie,
            this.tsmRelListaPosNeg,
            this.tsmRelVendasCanceladas,
            this.tsmRelVendaFiscal});
            this.tsmVenRelatorios.Name = "tsmVenRelatorios";
            this.tsmVenRelatorios.Size = new System.Drawing.Size(291, 22);
            this.tsmVenRelatorios.Text = "Relatórios";
            // 
            // tsmVenRelComissaoProd
            // 
            this.tsmVenRelComissaoProd.Name = "tsmVenRelComissaoProd";
            this.tsmVenRelComissaoProd.Size = new System.Drawing.Size(252, 22);
            this.tsmVenRelComissaoProd.Text = "Comissão por Produtos";
            this.tsmVenRelComissaoProd.Click += new System.EventHandler(this.tsmVenRelComissaoProd_Click);
            // 
            // tsmVenRelComissaoVen
            // 
            this.tsmVenRelComissaoVen.Name = "tsmVenRelComissaoVen";
            this.tsmVenRelComissaoVen.Size = new System.Drawing.Size(252, 22);
            this.tsmVenRelComissaoVen.Text = "Comissão por Vendedor";
            this.tsmVenRelComissaoVen.Click += new System.EventHandler(this.tsmVenRelComissaoVen_Click);
            // 
            // tsmRelVenDescontoConcedido
            // 
            this.tsmRelVenDescontoConcedido.Name = "tsmRelVenDescontoConcedido";
            this.tsmRelVenDescontoConcedido.Size = new System.Drawing.Size(252, 22);
            this.tsmRelVenDescontoConcedido.Text = "Descontos Concedidos";
            this.tsmRelVenDescontoConcedido.Click += new System.EventHandler(this.tsmRelVenDescontoConcedido_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(249, 6);
            // 
            // tsmVendaEntregaFilial
            // 
            this.tsmVendaEntregaFilial.Name = "tsmVendaEntregaFilial";
            this.tsmVendaEntregaFilial.Size = new System.Drawing.Size(252, 22);
            this.tsmVendaEntregaFilial.Text = "Venda Entrega Filial";
            this.tsmVendaEntregaFilial.Click += new System.EventHandler(this.tsmVendaEntregaFilial_Click);
            // 
            // tsmVenRelPeriodo
            // 
            this.tsmVenRelPeriodo.Name = "tsmVenRelPeriodo";
            this.tsmVenRelPeriodo.Size = new System.Drawing.Size(252, 22);
            this.tsmVenRelPeriodo.Text = "Venda por Período";
            this.tsmVenRelPeriodo.Click += new System.EventHandler(this.tsmVenRelPeriodo_Click);
            // 
            // tsmVenRelEspecie
            // 
            this.tsmVenRelEspecie.Name = "tsmVenRelEspecie";
            this.tsmVenRelEspecie.Size = new System.Drawing.Size(252, 22);
            this.tsmVenRelEspecie.Text = "Venda por Espécie";
            this.tsmVenRelEspecie.Click += new System.EventHandler(this.tsmVenRelEspecie_Click);
            // 
            // tsmVenRelFormaPagto
            // 
            this.tsmVenRelFormaPagto.Name = "tsmVenRelFormaPagto";
            this.tsmVenRelFormaPagto.Size = new System.Drawing.Size(252, 22);
            this.tsmVenRelFormaPagto.Text = "Venda por Forma de Pagamento";
            this.tsmVenRelFormaPagto.Click += new System.EventHandler(this.tsmVenRelFormaPagto_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(249, 6);
            // 
            // tsmRelProdXCusto
            // 
            this.tsmRelProdXCusto.Name = "tsmRelProdXCusto";
            this.tsmRelProdXCusto.Size = new System.Drawing.Size(252, 22);
            this.tsmRelProdXCusto.Text = "Produtos X Custo";
            this.tsmRelProdXCusto.Click += new System.EventHandler(this.tsmRelProdXCusto_Click);
            // 
            // tsmProdutoPorCliente
            // 
            this.tsmProdutoPorCliente.BackColor = System.Drawing.SystemColors.Control;
            this.tsmProdutoPorCliente.Enabled = false;
            this.tsmProdutoPorCliente.ForeColor = System.Drawing.Color.Black;
            this.tsmProdutoPorCliente.Name = "tsmProdutoPorCliente";
            this.tsmProdutoPorCliente.Size = new System.Drawing.Size(252, 22);
            this.tsmProdutoPorCliente.Text = "Produtos por Cliente";
            this.tsmProdutoPorCliente.Click += new System.EventHandler(this.tsmProdutoPorCliente_Click);
            // 
            // tsmRelProdutoPorEspecie
            // 
            this.tsmRelProdutoPorEspecie.Name = "tsmRelProdutoPorEspecie";
            this.tsmRelProdutoPorEspecie.Size = new System.Drawing.Size(252, 22);
            this.tsmRelProdutoPorEspecie.Text = "Produtos por Espécie";
            this.tsmRelProdutoPorEspecie.Click += new System.EventHandler(this.tsmRelProdutoPorEspecie_Click);
            // 
            // tsmProdutoPorPeriodo
            // 
            this.tsmProdutoPorPeriodo.Name = "tsmProdutoPorPeriodo";
            this.tsmProdutoPorPeriodo.Size = new System.Drawing.Size(252, 22);
            this.tsmProdutoPorPeriodo.Text = "Produtos por Período";
            this.tsmProdutoPorPeriodo.Click += new System.EventHandler(this.tsmProdutoPorPeriodo_Click);
            // 
            // tsmProdutosMaisVendidos
            // 
            this.tsmProdutosMaisVendidos.Name = "tsmProdutosMaisVendidos";
            this.tsmProdutosMaisVendidos.Size = new System.Drawing.Size(252, 22);
            this.tsmProdutosMaisVendidos.Text = "Produtos mais Vendidos";
            this.tsmProdutosMaisVendidos.Click += new System.EventHandler(this.tsmProdutosMaisVendidos_Click);
            // 
            // tsmProdutosMenosVendidos
            // 
            this.tsmProdutosMenosVendidos.Name = "tsmProdutosMenosVendidos";
            this.tsmProdutosMenosVendidos.Size = new System.Drawing.Size(252, 22);
            this.tsmProdutosMenosVendidos.Text = "Produtos sem Movimento";
            this.tsmProdutosMenosVendidos.Click += new System.EventHandler(this.tsmProdutosMenosVendidos_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(249, 6);
            // 
            // tsmRelCurvaAbc
            // 
            this.tsmRelCurvaAbc.Name = "tsmRelCurvaAbc";
            this.tsmRelCurvaAbc.Size = new System.Drawing.Size(252, 22);
            this.tsmRelCurvaAbc.Text = "Curva ABC";
            this.tsmRelCurvaAbc.Click += new System.EventHandler(this.tsmRelCurvaAbc_Click);
            // 
            // tsmRelLucroXCusto
            // 
            this.tsmRelLucroXCusto.Name = "tsmRelLucroXCusto";
            this.tsmRelLucroXCusto.Size = new System.Drawing.Size(252, 22);
            this.tsmRelLucroXCusto.Text = "Custo X Lucro";
            this.tsmRelLucroXCusto.Click += new System.EventHandler(this.tsmRelLucroXCusto_Click);
            // 
            // tsmRelFormaXEspecie
            // 
            this.tsmRelFormaXEspecie.Name = "tsmRelFormaXEspecie";
            this.tsmRelFormaXEspecie.Size = new System.Drawing.Size(252, 22);
            this.tsmRelFormaXEspecie.Text = "Forma Pagamento X Espécie";
            this.tsmRelFormaXEspecie.Click += new System.EventHandler(this.tsmRelFormaXEspecie_Click);
            // 
            // tsmRelListaPosNeg
            // 
            this.tsmRelListaPosNeg.Name = "tsmRelListaPosNeg";
            this.tsmRelListaPosNeg.Size = new System.Drawing.Size(252, 22);
            this.tsmRelListaPosNeg.Text = "Lista Positiva/Negativa";
            this.tsmRelListaPosNeg.Click += new System.EventHandler(this.tsmRelListaPosNeg_Click);
            // 
            // tsmRelVendasCanceladas
            // 
            this.tsmRelVendasCanceladas.Name = "tsmRelVendasCanceladas";
            this.tsmRelVendasCanceladas.Size = new System.Drawing.Size(252, 22);
            this.tsmRelVendasCanceladas.Text = "Vendas Canceladas";
            this.tsmRelVendasCanceladas.Click += new System.EventHandler(this.tsmRelVendasCanceladas_Click);
            // 
            // tsmRelVendaFiscal
            // 
            this.tsmRelVendaFiscal.Name = "tsmRelVendaFiscal";
            this.tsmRelVendaFiscal.Size = new System.Drawing.Size(252, 22);
            this.tsmRelVendaFiscal.Text = "Venda Fiscal";
            this.tsmRelVendaFiscal.Click += new System.EventHandler(this.tsmRelVendaFiscal_Click);
            // 
            // MRelatorios
            // 
            this.MRelatorios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmRelEmpresaParticular,
            this.tsmRelComandasAberto,
            this.tsmRelEntregasAberto,
            this.tsmEntregaPeriodo,
            this.tsmLogAlteracao,
            this.tsmRelTransferencia});
            this.MRelatorios.Name = "MRelatorios";
            this.MRelatorios.Size = new System.Drawing.Size(76, 20);
            this.MRelatorios.Text = "Re&latórios";
            // 
            // tsmRelEmpresaParticular
            // 
            this.tsmRelEmpresaParticular.Name = "tsmRelEmpresaParticular";
            this.tsmRelEmpresaParticular.Size = new System.Drawing.Size(250, 22);
            this.tsmRelEmpresaParticular.Text = "Clientes por Empresa Particular";
            this.tsmRelEmpresaParticular.Click += new System.EventHandler(this.tsmRelEmpresaParticular_Click);
            // 
            // tsmRelComandasAberto
            // 
            this.tsmRelComandasAberto.Name = "tsmRelComandasAberto";
            this.tsmRelComandasAberto.Size = new System.Drawing.Size(250, 22);
            this.tsmRelComandasAberto.Text = "Comandas em Aberto";
            this.tsmRelComandasAberto.Click += new System.EventHandler(this.tsmRelComandasAberto_Click);
            // 
            // tsmRelEntregasAberto
            // 
            this.tsmRelEntregasAberto.Name = "tsmRelEntregasAberto";
            this.tsmRelEntregasAberto.Size = new System.Drawing.Size(250, 22);
            this.tsmRelEntregasAberto.Text = "Entregas em Aberto";
            this.tsmRelEntregasAberto.Click += new System.EventHandler(this.tsmRelEntregasAberto_Click);
            // 
            // tsmEntregaPeriodo
            // 
            this.tsmEntregaPeriodo.Name = "tsmEntregaPeriodo";
            this.tsmEntregaPeriodo.Size = new System.Drawing.Size(250, 22);
            this.tsmEntregaPeriodo.Text = "Entregas por Período";
            this.tsmEntregaPeriodo.Click += new System.EventHandler(this.tsmEntregaPeriodo_Click);
            // 
            // tsmLogAlteracao
            // 
            this.tsmLogAlteracao.Name = "tsmLogAlteracao";
            this.tsmLogAlteracao.Size = new System.Drawing.Size(250, 22);
            this.tsmLogAlteracao.Text = "Logs de Alteração";
            this.tsmLogAlteracao.Click += new System.EventHandler(this.tsmLogAlteracao_Click);
            // 
            // tsmRelTransferencia
            // 
            this.tsmRelTransferencia.Name = "tsmRelTransferencia";
            this.tsmRelTransferencia.Size = new System.Drawing.Size(250, 22);
            this.tsmRelTransferencia.Text = "Transferência de Produtos";
            this.tsmRelTransferencia.Click += new System.EventHandler(this.tsmRelTransferencia_Click);
            // 
            // MAberturaChamado
            // 
            this.MAberturaChamado.Name = "MAberturaChamado";
            this.MAberturaChamado.Size = new System.Drawing.Size(140, 20);
            this.MAberturaChamado.Text = "Abertura de Chamado";
            this.MAberturaChamado.Click += new System.EventHandler(this.chamadosToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.Color.WhiteSmoke;
            this.statusStrip.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslCaps,
            this.tslNum,
            this.tslInsert,
            this.tslData,
            this.tslMensagem,
            this.pbMenu});
            this.statusStrip.Location = new System.Drawing.Point(0, 554);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(994, 24);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // tslCaps
            // 
            this.tslCaps.AutoSize = false;
            this.tslCaps.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslCaps.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tslCaps.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tslCaps.DoubleClickEnabled = true;
            this.tslCaps.Name = "tslCaps";
            this.tslCaps.Size = new System.Drawing.Size(40, 19);
            this.tslCaps.DoubleClick += new System.EventHandler(this.tslCaps_DoubleClick);
            // 
            // tslNum
            // 
            this.tslNum.AutoSize = false;
            this.tslNum.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslNum.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tslNum.DoubleClickEnabled = true;
            this.tslNum.Name = "tslNum";
            this.tslNum.Size = new System.Drawing.Size(40, 19);
            this.tslNum.Text = "NUM";
            this.tslNum.DoubleClick += new System.EventHandler(this.tslNum_DoubleClick);
            // 
            // tslInsert
            // 
            this.tslInsert.AutoSize = false;
            this.tslInsert.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslInsert.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tslInsert.DoubleClickEnabled = true;
            this.tslInsert.Name = "tslInsert";
            this.tslInsert.Size = new System.Drawing.Size(40, 19);
            this.tslInsert.DoubleClick += new System.EventHandler(this.tslInsert_DoubleClick);
            // 
            // tslData
            // 
            this.tslData.AutoSize = false;
            this.tslData.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslData.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tslData.Name = "tslData";
            this.tslData.Size = new System.Drawing.Size(75, 19);
            // 
            // tslMensagem
            // 
            this.tslMensagem.AutoSize = false;
            this.tslMensagem.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslMensagem.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tslMensagem.Name = "tslMensagem";
            this.tslMensagem.Size = new System.Drawing.Size(650, 19);
            this.tslMensagem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbMenu
            // 
            this.pbMenu.Name = "pbMenu";
            this.pbMenu.Size = new System.Drawing.Size(100, 18);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator11,
            this.btnAtualiza,
            this.btnExcluir,
            this.btnIncluir,
            this.btnLimpar,
            this.toolStripSeparator1,
            this.btnPrimeiro,
            this.btnAnterior,
            this.btnProximo,
            this.btnUltimo,
            this.toolStripSeparator2,
            this.btnConPrecos,
            this.toolStripSeparator9,
            this.btnImprimir,
            this.toolStripSeparator10,
            this.btnLogoff,
            this.btnAjuda,
            this.btnInternet,
            this.btnSuporte,
            this.btnAcesso,
            this.toolStripSeparator12,
            this.toolStripSeparator8,
            this.btnSair,
            this.toolStripSeparator13,
            this.btnEstab,
            this.toolStripSeparator24,
            this.btnBackup,
            this.toolStripSeparator25,
            this.btnMensagem});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(994, 39);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 39);
            // 
            // btnAtualiza
            // 
            this.btnAtualiza.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtualiza.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualiza.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualiza.Image")));
            this.btnAtualiza.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAtualiza.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtualiza.Name = "btnAtualiza";
            this.btnAtualiza.Size = new System.Drawing.Size(36, 36);
            this.btnAtualiza.Text = "Atualiza - [F3]";
            this.btnAtualiza.Click += new System.EventHandler(this.btnAtualiza_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExcluir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluir.Image")));
            this.btnExcluir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(36, 36);
            this.btnExcluir.Text = "Excluir - [F4]";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnIncluir
            // 
            this.btnIncluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnIncluir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluir.Image")));
            this.btnIncluir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnIncluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(36, 36);
            this.btnIncluir.Text = "Incluir - [INS]";
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLimpar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnLimpar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(36, 36);
            this.btnLimpar.Text = "Limpar - [F6]";
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // btnPrimeiro
            // 
            this.btnPrimeiro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrimeiro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrimeiro.Image = ((System.Drawing.Image)(resources.GetObject("btnPrimeiro.Image")));
            this.btnPrimeiro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPrimeiro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrimeiro.Name = "btnPrimeiro";
            this.btnPrimeiro.Size = new System.Drawing.Size(36, 36);
            this.btnPrimeiro.Text = "Primeiro Registro - [F9]";
            this.btnPrimeiro.Click += new System.EventHandler(this.btnPrimeiro_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnterior.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.Image = ((System.Drawing.Image)(resources.GetObject("btnAnterior.Image")));
            this.btnAnterior.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAnterior.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(36, 36);
            this.btnAnterior.Text = "Registro Anterior - [F10]";
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnProximo
            // 
            this.btnProximo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnProximo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.Image = ((System.Drawing.Image)(resources.GetObject("btnProximo.Image")));
            this.btnProximo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnProximo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnProximo.Name = "btnProximo";
            this.btnProximo.Size = new System.Drawing.Size(36, 36);
            this.btnProximo.Text = "Próximo Registro - [F11] ";
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // btnUltimo
            // 
            this.btnUltimo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnUltimo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUltimo.Image = ((System.Drawing.Image)(resources.GetObject("btnUltimo.Image")));
            this.btnUltimo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnUltimo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUltimo.Name = "btnUltimo";
            this.btnUltimo.Size = new System.Drawing.Size(36, 36);
            this.btnUltimo.Text = "Último Registro - [F12]";
            this.btnUltimo.Click += new System.EventHandler(this.btnUltimo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // btnConPrecos
            // 
            this.btnConPrecos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConPrecos.Enabled = false;
            this.btnConPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConPrecos.Image = ((System.Drawing.Image)(resources.GetObject("btnConPrecos.Image")));
            this.btnConPrecos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnConPrecos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConPrecos.Name = "btnConPrecos";
            this.btnConPrecos.Size = new System.Drawing.Size(36, 36);
            this.btnConPrecos.Text = "Consulta Rápida de Preços";
            this.btnConPrecos.Click += new System.EventHandler(this.btnConPrecos_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 39);
            // 
            // btnImprimir
            // 
            this.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnImprimir.Enabled = false;
            this.btnImprimir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(36, 36);
            this.btnImprimir.Text = "Imprimir - [F2]";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 39);
            // 
            // btnLogoff
            // 
            this.btnLogoff.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLogoff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoff.Image = ((System.Drawing.Image)(resources.GetObject("btnLogoff.Image")));
            this.btnLogoff.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnLogoff.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLogoff.Name = "btnLogoff";
            this.btnLogoff.Size = new System.Drawing.Size(36, 36);
            this.btnLogoff.Text = "Logoff do sistema";
            this.btnLogoff.Click += new System.EventHandler(this.btnLogoff_Click);
            // 
            // btnAjuda
            // 
            this.btnAjuda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAjuda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjuda.Image = ((System.Drawing.Image)(resources.GetObject("btnAjuda.Image")));
            this.btnAjuda.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAjuda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAjuda.Name = "btnAjuda";
            this.btnAjuda.Size = new System.Drawing.Size(36, 36);
            this.btnAjuda.Text = "Ajuda";
            this.btnAjuda.ToolTipText = "Ajuda - [F7] ";
            this.btnAjuda.Click += new System.EventHandler(this.btnAjuda_Click);
            // 
            // btnInternet
            // 
            this.btnInternet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnInternet.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInternet.Image = ((System.Drawing.Image)(resources.GetObject("btnInternet.Image")));
            this.btnInternet.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnInternet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInternet.Name = "btnInternet";
            this.btnInternet.Size = new System.Drawing.Size(36, 36);
            this.btnInternet.Text = "Painel Administrativo";
            this.btnInternet.Click += new System.EventHandler(this.btnInternet_Click);
            // 
            // btnSuporte
            // 
            this.btnSuporte.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSuporte.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuporte.Image = ((System.Drawing.Image)(resources.GetObject("btnSuporte.Image")));
            this.btnSuporte.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSuporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSuporte.Name = "btnSuporte";
            this.btnSuporte.Size = new System.Drawing.Size(36, 36);
            this.btnSuporte.Text = "Suporte SGPharma";
            this.btnSuporte.Click += new System.EventHandler(this.btnSuporte_Click);
            // 
            // btnAcesso
            // 
            this.btnAcesso.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAcesso.Image = ((System.Drawing.Image)(resources.GetObject("btnAcesso.Image")));
            this.btnAcesso.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAcesso.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAcesso.Name = "btnAcesso";
            this.btnAcesso.Size = new System.Drawing.Size(32, 36);
            this.btnAcesso.Text = "Download TeamViewer";
            this.btnAcesso.Click += new System.EventHandler(this.btnAcesso_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
            // 
            // btnSair
            // 
            this.btnSair.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSair.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(36, 36);
            this.btnSair.Tag = "ToolStripButton";
            this.btnSair.Text = "Sair do sistema";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 39);
            // 
            // btnEstab
            // 
            this.btnEstab.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstab.Image = ((System.Drawing.Image)(resources.GetObject("btnEstab.Image")));
            this.btnEstab.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnEstab.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEstab.Name = "btnEstab";
            this.btnEstab.Size = new System.Drawing.Size(36, 36);
            this.btnEstab.Text = "Troca de Estabelecimento";
            this.btnEstab.Click += new System.EventHandler(this.btnEstab_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(6, 39);
            // 
            // btnBackup
            // 
            this.btnBackup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnBackup.Image = ((System.Drawing.Image)(resources.GetObject("btnBackup.Image")));
            this.btnBackup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnBackup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(36, 36);
            this.btnBackup.Text = "Backup do Banco de Dados";
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(6, 39);
            // 
            // btnMensagem
            // 
            this.btnMensagem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMensagem.Image = ((System.Drawing.Image)(resources.GetObject("btnMensagem.Image")));
            this.btnMensagem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnMensagem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMensagem.Name = "btnMensagem";
            this.btnMensagem.Size = new System.Drawing.Size(36, 36);
            this.btnMensagem.Text = "toolStripButton1";
            this.btnMensagem.ToolTipText = "Mensagens!";
            this.btnMensagem.Click += new System.EventHandler(this.btnMensagem_Click);
            // 
            // stsBotoes
            // 
            this.stsBotoes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stsBotoes.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.stsBotoes.Location = new System.Drawing.Point(0, 554);
            this.stsBotoes.Name = "stsBotoes";
            this.stsBotoes.Size = new System.Drawing.Size(994, 0);
            this.stsBotoes.TabIndex = 6;
            this.stsBotoes.Tag = "";
            // 
            // timerReserva
            // 
            this.timerReserva.Interval = 30000;
            this.timerReserva.Tick += new System.EventHandler(this.timerReserva_Tick);
            // 
            // timerEntrega
            // 
            this.timerEntrega.Interval = 30000;
            this.timerEntrega.Tick += new System.EventHandler(this.timerEntrega_Tick);
            // 
            // timerImpressao
            // 
            this.timerImpressao.Interval = 10000;
            this.timerImpressao.Tick += new System.EventHandler(this.timerImpressao_Tick);
            // 
            // timerAvisos
            // 
            this.timerAvisos.Interval = 300000;
            this.timerAvisos.Tick += new System.EventHandler(this.timerAvisos_Tick);
            // 
            // timerPreco
            // 
            this.timerPreco.Interval = 120000;
            this.timerPreco.Tick += new System.EventHandler(this.timerPreco_Tick);
            // 
            // timerPromocaoFilial
            // 
            this.timerPromocaoFilial.Interval = 120000;
            this.timerPromocaoFilial.Tick += new System.EventHandler(this.timerPromocaoFilial_Tick);
            // 
            // MDIPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.stsBotoes);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.msPrincipal);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.msPrincipal;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MDIPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SGPharma";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.MDICadastro_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDIPrincipal_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MDIPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.MDICadastro_Load);
            this.Shown += new System.EventHandler(this.MDIPrincipal_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MDICadastro_KeyUp);
            this.msPrincipal.ResumeLayout(false);
            this.msPrincipal.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tslCaps;
        private System.Windows.Forms.ToolStripMenuItem MContReceber;
        private System.Windows.Forms.ToolStripMenuItem MEstoque;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem MSngpc;
        private System.Windows.Forms.ToolStripMenuItem MVendas;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem MCaixa;
        private System.Windows.Forms.ToolStripStatusLabel tslNum;
        private System.Windows.Forms.ToolStripStatusLabel tslInsert;
        private System.Windows.Forms.ToolStripStatusLabel tslData;
        private System.Windows.Forms.ToolStripMenuItem tsmEstabelecimentos;
        private System.Windows.Forms.ToolStripMenuItem tsmColaboradores;
        private System.Drawing.Printing.PrintDocument printDocument1;
        public System.Windows.Forms.ToolStripButton btnPrimeiro;
        public System.Windows.Forms.ToolStripButton btnAnterior;
        public System.Windows.Forms.ToolStripButton btnProximo;
        public System.Windows.Forms.ToolStripButton btnUltimo;
        public System.Windows.Forms.ToolStripStatusLabel tslMensagem;
        public System.Windows.Forms.ToolStripMenuItem MGeral;
        public System.Windows.Forms.ToolStripMenuItem MCadastro;
        public System.Windows.Forms.ToolStripButton btnExcluir;
        public System.Windows.Forms.ToolStripButton btnIncluir;
        public System.Windows.Forms.ToolStripButton btnLimpar;
        public System.Windows.Forms.ToolStripButton btnConPrecos;
        public System.Windows.Forms.ToolStripButton btnImprimir;
        public System.Windows.Forms.ToolStripButton btnLogoff;
        public System.Windows.Forms.ToolStripButton btnSair;
        public System.Windows.Forms.ToolStripMenuItem tsmParametros;
        public System.Windows.Forms.ToolStripMenuItem tsmUsuarios;
        public System.Windows.Forms.ToolStripMenuItem tsmAlterarSenha;
        public System.Windows.Forms.ToolStripMenuItem tsmLogoff;
        public System.Windows.Forms.ToolStripMenuItem tsmSair;
        public System.Windows.Forms.ToolStripButton btnAtualiza;
        private System.Windows.Forms.ToolStripMenuItem tsmCadGrupos;
        private System.Windows.Forms.ToolStripMenuItem tsmCadUsuario;
        public System.Windows.Forms.MenuStrip msPrincipal;
        public System.Windows.Forms.StatusStrip stsBotoes;
        private System.Windows.Forms.ToolStripMenuItem MContPagar;
        private System.Windows.Forms.ToolStripMenuItem MRelatorios;
        private System.Windows.Forms.ToolStripButton btnAjuda;
        private System.Windows.Forms.ToolStripButton btnInternet;
        private System.Windows.Forms.ToolStripButton btnSuporte;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem tsmOperacoes;
        public System.Windows.Forms.ToolStripButton btnEstab;
        private System.Windows.Forms.ToolStripMenuItem tsmEspecies;
        private System.Windows.Forms.ToolStripMenuItem tsmDepartamento;
        private System.Windows.Forms.ToolStripMenuItem tsmClasses;
        private System.Windows.Forms.ToolStripMenuItem tsmSubClasse;
        private System.Windows.Forms.ToolStripMenuItem tsmFabricantes;
        private System.Windows.Forms.ToolStripMenuItem tsmPrinAtivo;
        private System.Windows.Forms.ToolStripMenuItem tsmTabPrecos;
        private System.Windows.Forms.ToolStripMenuItem tsmNatOper;
        private System.Windows.Forms.ToolStripMenuItem tsmTipoDocto;
        private System.Windows.Forms.ToolStripMenuItem tsmUnidade;
        private System.Windows.Forms.ToolStripMenuItem tsmProdutos;
        private System.Windows.Forms.ToolStripMenuItem tsmFornecedores;
        private System.Windows.Forms.ToolStripMenuItem tsmGrupos;
        private System.Windows.Forms.ToolStripMenuItem tsmCondicoes;
        private System.Windows.Forms.ToolStripMenuItem tsmClientes;
        private System.Windows.Forms.ToolStripMenuItem tsmConveniadas;
        private System.Windows.Forms.ToolStripMenuItem tsmEspecificacoes;
        private System.Windows.Forms.ToolStripMenuItem tsmMedicos;
        private System.Windows.Forms.ToolStripMenuItem tsmSngpcTecnico;
        private System.Windows.Forms.ToolStripMenuItem tsmSngpcTabelas;
        private System.Windows.Forms.ToolStripMenuItem tsmPerdas;
        private System.Windows.Forms.ToolStripMenuItem tsmEntrTrans;
        private System.Windows.Forms.ToolStripMenuItem tsmVendaMed;
        private System.Windows.Forms.ToolStripMenuItem tsmTransferencia;
        private System.Windows.Forms.ToolStripMenuItem tsmEstEntradas;
        private System.Windows.Forms.ToolStripMenuItem tsmAtuPreProd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.ToolStripProgressBar pbMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmEstAjuste;
        private System.Windows.Forms.ToolStripMenuItem tsmEstAtuLinear;
        private System.Windows.Forms.ToolStripMenuItem tsmAberFecha;
        private System.Windows.Forms.ToolStripMenuItem tsmCanAberFecha;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem tsmMovCaixa;
        private System.Windows.Forms.ToolStripMenuItem tsmRecebimentos;
        private System.Windows.Forms.ToolStripMenuItem tsmRecebConveniadas;
        private System.Windows.Forms.ToolStripMenuItem tsmCadDespesas;
        private System.Windows.Forms.ToolStripMenuItem tsmFinContasPagar;
        private System.Windows.Forms.ToolStripMenuItem tsmFinDespesas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem tsmFinRelatorio;
        private System.Windows.Forms.ToolStripMenuItem tsmVenComanda;
        private System.Windows.Forms.ToolStripMenuItem tsmCVendas;
        private System.Windows.Forms.ToolStripMenuItem tsmPromocoes;
        private System.Windows.Forms.ToolStripMenuItem tsmDescontos;
        private System.Windows.Forms.ToolStripMenuItem tsmTEntregas;
        private System.Windows.Forms.ToolStripMenuItem tsmComandas;
        private System.Windows.Forms.ToolStripMenuItem tsmAtuTabIBPT;
        private System.Windows.Forms.ToolStripMenuItem tsmCodNCM;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem tsmCRelatorios;
        private System.Windows.Forms.ToolStripMenuItem tsmCFechaDiario;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem tsmFPVendedor;
        private System.Windows.Forms.ToolStripMenuItem tsmVenUtilitariosEcf;
        private System.Windows.Forms.ToolStripMenuItem tsmCadTitulo;
        private System.Windows.Forms.ToolStripMenuItem tsmManutParcelas;
        private System.Windows.Forms.ToolStripMenuItem tsmLancamentoTitulos;
        private System.Windows.Forms.ToolStripMenuItem tsmRecManutParcelas;
        private System.Windows.Forms.ToolStripMenuItem tsmFinContasReceber;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem tsmRecRelatorio;
        private System.Windows.Forms.ToolStripMenuItem tsmEmpresas;
        private System.Windows.Forms.ToolStripMenuItem tsmEstoqueFiliais;
        private System.Windows.Forms.ToolStripMenuItem tsmRelatoriosSNGPC;
        private System.Windows.Forms.ToolStripMenuItem tsmHistorioDeEnvios;
        private System.Windows.Forms.ToolStripMenuItem tsmCancelamentoVendas;
        private System.Windows.Forms.ToolStripMenuItem tsmControleEntrega;
        private System.Windows.Forms.ToolStripMenuItem tsmMovimentoEspecie;
        private System.Windows.Forms.ToolStripMenuItem tsmMovimentacaoOperaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumoDiárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumoGeralDeVendasPorPerídoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recebimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmPerdasDeMedicamentos;
        private System.Windows.Forms.ToolStripMenuItem tsmComprasDeMedicamentos;
        private System.Windows.Forms.ToolStripMenuItem tsmVendasDeMedicamentos;
        private System.Windows.Forms.ToolStripMenuItem tsmAtualizacaoLinearDePreco;
        private System.Windows.Forms.ToolStripMenuItem tsmVenTransferencia;
        private System.Windows.Forms.ToolStripMenuItem tsmDevolucaoDeProdutos;
        private System.Windows.Forms.ToolStripMenuItem tsmAutorizacoesFarmaciaPopular;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoDeProdutos;
        private System.Windows.Forms.ToolStripMenuItem tsmFarmaciaPopular;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeVendedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmReposicaoProdutos;
        private System.Windows.Forms.ToolStripMenuItem tsmInventario;
        private System.Windows.Forms.ToolStripMenuItem tsmGerarInventario;
        private System.Windows.Forms.ToolStripMenuItem tsmLanInventario;
        private System.Windows.Forms.ToolStripMenuItem tsmSuporte;
        private System.Windows.Forms.ToolStripMenuItem tsmSenha;
        private System.Windows.Forms.ToolStripMenuItem tsmEstManutencaoCodBarras;
        private System.Windows.Forms.ToolStripMenuItem tsmVenImportarTransferencia;
        private System.Windows.Forms.ToolStripButton btnAcesso;
        private System.Windows.Forms.ToolStripMenuItem tsmERelatorios;
        private System.Windows.Forms.ToolStripMenuItem tsmRelEntDocumento;
        private System.Windows.Forms.ToolStripMenuItem tsmRelBalanco;
        private System.Windows.Forms.ToolStripMenuItem tsmRelAbaixoEst;
        private System.Windows.Forms.ToolStripMenuItem tsmRelValidade;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelatorios;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelPeriodo;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelFormaPagto;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelComissaoVen;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelEspecie;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.ToolStripMenuItem tsmLancamentoDevolucao;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoComanda;
        private System.Windows.Forms.ToolStripMenuItem tsmTabelaDCB;
        private System.Windows.Forms.ToolStripMenuItem tsmCadPrecosFP;
        private System.Windows.Forms.ToolStripMenuItem tsmRelComandasAberto;
        private System.Windows.Forms.ToolStripMenuItem tsmRelPagFornecedores;
        private System.Windows.Forms.ToolStripMenuItem tsmRelFichaProduto;
        private System.Windows.Forms.ToolStripMenuItem tsmRelTransferencia;
        private System.Windows.Forms.ToolStripMenuItem tsmVenNFe;
        private System.Windows.Forms.ToolStripMenuItem tsmRelEntregasAberto;
        private System.Windows.Forms.ToolStripMenuItem tsmCartaCobranca;
        private System.Windows.Forms.ToolStripMenuItem tsmProdutoPorPeriodo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripMenuItem tsmRelInventario;
        private System.Windows.Forms.ToolStripMenuItem tsmProdutosMaisVendidos;
        private System.Windows.Forms.ToolStripMenuItem tsmProdutoPorCliente;
        private System.Windows.Forms.ToolStripMenuItem tsmRelVenDescontoConcedido;
        private System.Windows.Forms.Timer timerReserva;
        private System.Windows.Forms.ToolStripMenuItem tsmRelEmpresaConveniada;
        private System.Windows.Forms.ToolStripMenuItem tsmVenRelComissaoProd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem tsmRelVendaFiscal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripMenuItem tsmCaixaXContas;
        private System.Windows.Forms.ToolStripMenuItem tsmBalancoSngpc;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoEstoque;
        private System.Windows.Forms.ToolStripMenuItem tsmRelCurvaAbc;
        private System.Windows.Forms.ToolStripMenuItem tsmRelListaPosNeg;
        private System.Windows.Forms.ToolStripMenuItem tsmRelRecebimentoTitulo;
        private System.Windows.Forms.ToolStripMenuItem tsmRelProdXCusto;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoCpfCnpj;
        private System.Windows.Forms.ToolStripMenuItem tsmRelVendasCanceladas;
        private System.Windows.Forms.ToolStripMenuItem tsmRelProdutoPorEspecie;
        private System.Windows.Forms.ToolStripMenuItem tsmLancamentoEntrega;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.Timer timerEntrega;
        private System.Windows.Forms.ToolStripMenuItem tsmControleEntregaFilial;
        private System.Windows.Forms.ToolStripMenuItem tsmVendaEntregaFilial;
        private System.Windows.Forms.ToolStripMenuItem tsmProdutosMenosVendidos;
        private System.Windows.Forms.ToolStripMenuItem tsmRelFormaXEspecie;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeEstoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem tsmRelImpostos;
        private System.Windows.Forms.ToolStripMenuItem tsmRelComissaoProduto;
        private System.Windows.Forms.ToolStripMenuItem tsmVenEstornoManual;
        private System.Windows.Forms.ToolStripMenuItem tsmRelLucroXCusto;
        private System.Windows.Forms.ToolStripMenuItem tsmRelAjustes;
        private System.Windows.Forms.ToolStripMenuItem tsmRelEmpresaParticular;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoCusto;
        private System.Windows.Forms.ToolStripMenuItem tsmEstAbcFarma;
        private System.Windows.Forms.ToolStripMenuItem tsmRelPrecosAlterados;
        private System.Windows.Forms.ToolStripMenuItem tsmLogAlteracao;
        private System.Windows.Forms.ToolStripMenuItem tsmEntregaPeriodo;
        private System.Windows.Forms.ToolStripMenuItem tsmRelEmpresasConvenio;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripButton btnBackup;
        private System.Windows.Forms.Timer timerImpressao;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripButton btnMensagem;
        private System.Windows.Forms.Timer timerAvisos;
        private System.Windows.Forms.ToolStripMenuItem tsmComissaoPorDepartamento;
        private System.Windows.Forms.ToolStripMenuItem MAberturaChamado;
        private System.Windows.Forms.ToolStripMenuItem tsmManutencaoDeFilial;
        private System.Windows.Forms.ToolStripMenuItem tsmManuAtualizacaoPreco;
        private System.Windows.Forms.Timer timerPreco;
        private System.Windows.Forms.ToolStripMenuItem tsmManuAtualizacaoComissao;
        private System.Windows.Forms.Timer timerPromocaoFilial;
    }
}



