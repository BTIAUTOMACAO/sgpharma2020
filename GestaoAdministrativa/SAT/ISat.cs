﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.SAT
{
    interface ISat
    {
        string SatEnviarDadosDaVenda(string numeroSessao, string codAtivacao, string xml);

        string SatConsultarStatus(string numeroSessao);

        string SatCancelarUltimaVenda(string numeroSessao, string codigoDeAtivacao, string chave, string dadosCancelamento);

        string SatConsultarStatusOperacional(string numeroSessao, string codigoDeAtivacao);

        string SatConsultarNumeroSessao(string numeroSessao, string codigoDeAtivacao, string cNumeroDeSessao);

        string SatAtualizarSoftware(string numeroSessao, string codigoDeAtivacao);

    }
}
