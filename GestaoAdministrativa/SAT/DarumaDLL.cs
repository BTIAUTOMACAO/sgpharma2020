﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.SAT
{
    class DarumaDLL : ISat
    {
        public string SatAtualizarSoftware(string numeroSessao, string codigoDeAtivacao)
        {
            return Marshal.PtrToStringAnsi(AtualizarSoftwareSAT(Convert.ToInt32(numeroSessao), codigoDeAtivacao));
        }

        public string SatCancelarUltimaVenda(string numeroSessao, string codigoDeAtivacao, string chave, string dadosCancelamento)
        {
            int iRetorno = tCFeCancelar_SAT_Daruma();
            return TrataRetorno(iRetorno);
        }

        public string SatConsultarNumeroSessao(string numeroSessao, string codigoDeAtivacao, string cNumeroDeSessao)
        {
            throw new NotImplementedException();
        }

        public string SatConsultarStatus(string numeroSessao)
        {
            int iRetorno = rVerificarComunicacao_SAT_Daruma();
            return TrataRetorno(iRetorno);
        }

        public string SatConsultarStatusOperacional(string numeroSessao, string codigoDeAtivacao)
        {
            StringBuilder str_RetornoStatus = new StringBuilder(400);
            int iRetorno = rConsultarStatus_SAT_Daruma(str_RetornoStatus);
            return TrataRetorno(iRetorno);
        }

        public string SatEnviarDadosDaVenda(string numeroSessao, string codAtivacao, string xml)
        {
            throw new NotImplementedException();
        }

        public string DefinirProduto(string produto)
        {
            int iRetorno = eDefinirProduto_Daruma(produto);
            return TrataRetorno(iRetorno);
        }
        
        [DllImport("DarumaFrameWork.dll")]
        public static extern int rVerificarComunicacao_SAT_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int rConsultarStatus_SAT_Daruma(StringBuilder strRetornoSAT);

        [DllImport("DLLSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr AtualizarSoftwareSAT(int numeroSessao, string codigoDeAtivacao);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int tCFeCancelar_SAT_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int eDefinirProduto_Daruma(string pszProduto);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int aCFeAbrir_SAT_Daruma(string strTagsSAT);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int aCFeVender_SAT_Daruma(string strTagsSAT);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int aCFeTotalizar_SAT_Daruma(string strTagsSAT);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int aCFeEfetuarPagamento_SAT_Daruma(string strTagsSAT);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int tCFeEncerrar_SAT_Daruma(string strTagsSAT);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int rInfoEstendida_SAT_Daruma(string strIndice, StringBuilder strRetorno);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iImprimirTexto_DUAL_DarumaFramework(string stTexto, int iTam);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iConfigurarGuilhotina_DUAL_DarumaFramework(string iHabilitar, string iQtdeLinha);

        public static string TrataRetorno(int iRetMetodo)
        {
            switch (iRetMetodo)
            {
                case 1:
                    return "[1] - Comando executado com sucesso!";
                case 0:
                    return "[0] - Método não executado/ Tag inválida/ Não foi possível comunicar com impressora";
                case -6:
                    return "[-6] - TimeOut, erro de comunicação com o SAT";
                case -7:
                    return "[-7] - Erro ao abrir comunicação com o SAT";
                case -40:
                    return "[-40] - Tag XML inválida";
                case -50:
                    return "[-50] - Impressora off-Line";
                case -51:
                    return "[-51] - Impressora sem papel";
                case -99:
                    return "[-99] - Parâmetro inválido ou ponteiro nulo de parâmetro";
                case -120:
                    return "[-120] - Encontrada tag inválida";
                case -121:
                    return "[-121] - Estrutura Invalida";
                case -122:
                    return "[-122] - Tag obrigatória não foi informada";
                case -123:
                    return "[-123] - Tag obrigatória não tem valor preenchido";
                case -130:
                    return "[-130] - CFe já aberto";
                case -131:
                    return "[-131] - CFe não aberto";
                case -132:
                    return "[-132] - CFe não em fase de venda";
                case -133:
                    return "[-133] - CFe não em fase de totalização";
                case -134:
                    return "[-134] - CFe não em fase de pagamento";
                case -135:
                    return "[-135] - CFe não em fase de encerramento";
                case -136:
                    return "[-136] - CFe em estado inválido para operação";
                case -140:
                    return "[-140] - Biblioteca auxiliar SAT.dll não foi encontrada/carregada";
                case -141:
                    return "[-141] - Impressora inválida (modelo deve ser DR700 ou versão incompativel)";
                case -142:
                    return "[-142] - Resposta Incompleta do SAT";
                case 1084:
                    return "[1084] - Formato do Certificado Inválido";
                case 1085:
                    return "[1085] - Assinatura do Aplicativo Comercial não confere";
                case 1218:
                    return "[1218] - CF-e-SAT Já está cancelado";
                case 1412:
                    return "[1412] - CFe de cancelamento não corresponde a um CFe emitido nos 30 minutos anteriores ao pedido de cancelamento";
                case 1999:
                    return "[1999] - Erro desconhecido";
                case 6001:
                    return "[6001] - Código de ativação inválido";
                case 6002:
                    return "[6002] - SAT ainda não ativado";
                case 6003:
                    return "[6003] - SAT não vinculado ao AC";
                case 6004:
                    return "[6004] - Vinculação do AC não confere";
                case 6005:
                    return "[6005] - Tamanho do CFe superior a 1500KB";
                case 6006:
                    return "[6006] - SAT bloqueado pelo contribuinte";
                case 6007:
                    return "[6007] - SAT bloqueado pela SEFAZ";
                case 6008:
                    return "[6008] - SAT bloqueado por falta de comunicação";
                case 6009:
                    return "[6009] - SAT bloqueado, código de ativação incorreto";
                case 6010:
                    return "[6010] - Erro de validação do conteúdo";
                case 6098:
                    return "[6098] - SAT em processamento. Tente novamente";
                case 6099:
                    return "[6099] - Erro desconhecido";
                case 7001:
                    return "[7001] - Código de ativação inválido";
                case 7002:
                    return "[7002] - Cupom Inválido";
                case 7003:
                    return "[7003] - SAT bloqueado pelo contribuinte";
                case 7004:
                    return "[7004] - SAT bloqueado pela SEFAZ";
                case 7005:
                    return "[7005] - SAT bloqueado por falta de comunicação";
                case 7006:
                    return "[7006] - SAT bloqueado, código de ativação incorreto";
                case 7007:
                    return "[7007] - Erro de validação do conteúdo";
                case 7098:
                    return "[7098] - SAT em processamento. Tente novamente";
                case 7099:
                    return "[7099] - Erro desconhecido";
                case 8098:
                    return "[8098] - SAT em processamento. Tente novamente";
                case 8099:
                    return "[8099] - Erro desconhecido";
                case 10001:
                    return "[10001] - Código de ativação inválido";
                case 10098:
                    return "[10098] - SAT em processamento. Tente novamente";
                case 10099:
                    return "[10099] - Erro desconhecido";
                case 13001:
                    return "[13001] - Código de ativação inválido";
                case 13002:
                    return "[13002] - Erro de comunicação com a SEFAZ";
                case 13003:
                    return "[13003] - Assinatura fora do padrão informado";
                case 13098:
                    return "[13098] - SAT em processamento. Tente novamente";
                case 13099:
                    return "[13099] - Erro desconhecido";
                default:
                    return "[" + iRetMetodo.ToString() + "] - Número de Erro não identificado... Consulte a Especificação de Requisitos SAT vigente.";
            }
        }
    }
}
