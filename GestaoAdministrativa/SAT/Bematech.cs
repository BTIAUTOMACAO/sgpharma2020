﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.SAT
{
    class Bematech : ISat
    {
        public Bematech() { }

        public string SatEnviarDadosDaVenda(string numeroSessao, string codAtivacao, string xml)
        {
            return Marshal.PtrToStringAnsi(EnviarDadosVenda(Convert.ToInt32(numeroSessao), codAtivacao, xml));
        }

        public string SatConsultarStatus(string numeroSessao)
        {
            return Marshal.PtrToStringAnsi(ConsultarSAT(Convert.ToInt32(numeroSessao)));
        }

        public string SatCancelarUltimaVenda(string numeroSessao, string codAtivacao, string chave, string dadosCancelamento)
        {
            return Marshal.PtrToStringAnsi(CancelarUltimaVenda(Convert.ToInt32(numeroSessao), codAtivacao, chave, dadosCancelamento));
        }

        public string SatConsultarStatusOperacional(string numeroSessao, string codigoDeAtivacao)
        {
            return Marshal.PtrToStringAnsi(ConsultarStatusOperacional(Convert.ToInt32(numeroSessao), codigoDeAtivacao));
        }

        public string SatConsultarNumeroSessao(string numeroSessao, string codigoDeAtivacao, string cNumeroDeSessao)
        {
            return Marshal.PtrToStringAnsi(ConsultarNumeroSessao(Convert.ToInt32(numeroSessao), codigoDeAtivacao, Convert.ToInt32(cNumeroDeSessao)));
        }

        public string SatAtualizarSoftware(string numeroSessao, string codigoDeAtivacao)
        {
            return Marshal.PtrToStringAnsi(AtualizarSoftwareSAT(Convert.ToInt32(numeroSessao), codigoDeAtivacao));
        }

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr EnviarDadosVenda(int sessao, [MarshalAs(UnmanagedType.LPStr)] string cod, [MarshalAs(UnmanagedType.LPStr)] string dados);

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr ConsultarStatusOperacional(int sessao, [MarshalAs(UnmanagedType.LPStr)] string codAtivacao);

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr ConsultarSAT(int sessao);

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr ConsultarNumeroSessao(int sessao, [MarshalAs(UnmanagedType.LPStr)] string cod, int sessao_a_ser_consultada);

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr AtualizarSoftwareSAT(int sessao, [MarshalAs(UnmanagedType.LPStr)] string cod_ativacao);

        [DllImport("BemaSAT.DLL", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr CancelarUltimaVenda(int sessao, [MarshalAs(UnmanagedType.LPStr)] string cod, [MarshalAs(UnmanagedType.LPStr)] string chave, [MarshalAs(UnmanagedType.LPStr)] string dadoscancel);
    }
}
