﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.SAT
{
    class Epson : ISat
    {
        public string SatEnviarDadosDaVenda(string numeroSessao, string codAtivacao, string xml)
        {
            return Marshal.PtrToStringAnsi(EnviarDadosVenda(Convert.ToInt32(numeroSessao), codAtivacao, xml));
        }

        public string SatConsultarStatus(string numeroSessao)
        {
            return Marshal.PtrToStringAnsi(ConsultarSAT(Convert.ToInt32(numeroSessao)));
        }

        public string SatCancelarUltimaVenda(string numeroSessao, string codAtivacao, string chave, string dadosCancelamento)
        {
            return Marshal.PtrToStringAnsi(CancelarUltimaVenda(Convert.ToInt32(numeroSessao), codAtivacao, chave, dadosCancelamento));
        }

        public string SatConsultarStatusOperacional(string numeroSessao, string codigoDeAtivacao)
        {
            return Marshal.PtrToStringAnsi(ConsultarStatusOperacional(Convert.ToInt32(numeroSessao), codigoDeAtivacao));
        }

        public string SatConsultarNumeroSessao(string numeroSessao, string codigoDeAtivacao, string cNumeroDeSessao)
        {
            return Marshal.PtrToStringAnsi(ConsultarNumeroSessao(Convert.ToInt32(numeroSessao), codigoDeAtivacao, Convert.ToInt32(cNumeroDeSessao)));
        }

        public string SatAtualizarSoftware(string numeroSessao, string codigoDeAtivacao)
        {
            return Marshal.PtrToStringAnsi(AtualizarSoftwareSAT(Convert.ToInt32(numeroSessao), codigoDeAtivacao));
        }

        [DllImport("GERSAT.dll", EntryPoint = "EnviarDadosVenda", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr EnviarDadosVenda(int numeroSessao, string codigoDeAtivacao, string dadosVenda);

        [DllImport("GERSAT.dll", EntryPoint = "AtualizarSoftwareSAT", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr AtualizarSoftwareSAT(int numeroSessao, string codigoDeAtivacao);

        [DllImport("GERSAT.dll", EntryPoint = "ConsultarNumeroSessao", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ConsultarNumeroSessao(int numeroSessao, string codigoDeAtivacao, int cNumeroDeSessao);

        [DllImport("GERSAT.dll", EntryPoint = "ConsultarStatusOperacional", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ConsultarStatusOperacional(int numeroSessao, string codigoDeAtivacao);

        [DllImport("GERSAT.dll", EntryPoint = "CancelarUltimaVenda", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CancelarUltimaVenda(int numeroSessao, string codigoDeAtivacao, string chave, string dadosCancelamento);

        [DllImport("GERSAT.dll", EntryPoint = "ConsultarSAT", ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ConsultarSAT(int numeroSessao);
    }
}
