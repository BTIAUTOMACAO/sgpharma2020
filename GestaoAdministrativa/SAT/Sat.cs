﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MessagingToolkit.QRCode.Codec;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.SAT
{
    class Sat
    {
        private static ISat modeloSAT;

        public static bool VerificaModeloSAT()
        {
            string modSAT = Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).ToUpper();

            if (String.IsNullOrEmpty(modSAT))
            {
                return false;
            }

            switch (modSAT)
            {
                case "DIMEP":
                    modeloSAT = new Dimep();
                    break;
                case "BEMATECH":
                    modeloSAT = new Bematech();
                    break;
                case "SWEDA":
                    modeloSAT = new Sweda();
                    break;
                case "ELGIN":
                    modeloSAT = new Elgin();
                    break;
                case "EPSON":
                    modeloSAT = new Epson();
                    break;
                case "DARUMA":
                    modeloSAT = new Daruma();
                    break;
                case "DARUMADLL":
                    modeloSAT = new DarumaDLL();
                    break;
                case "TANCA":
                    modeloSAT = new Tanca();
                    break;
                case "NITERE":
                    modeloSAT = new Nitere();
                    break;

            }
            return true;
        }

        public static string GeraNumeroSessao()
        {
            Random rdn = new Random();
            Principal.NumeroSessao = Convert.ToString(rdn.Next(999999));
            return Principal.NumeroSessao;
        }

        public static string ConverterToUTF8(string dados)
        {
            byte[] utf16Bytes = Encoding.Unicode.GetBytes(dados);
            byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf16Bytes);

            return Encoding.Default.GetString(utf8Bytes);
        }

        public static bool TrataRetornoSATVenda(string Retorno, out string arquivoXML, out string numCFe)
        {
            string[] retornoSplit = Retorno.Split('|');
            var CodRetornoSAT = retornoSplit[1];
            if (CodRetornoSAT != "06000" && CodRetornoSAT != "07000")
            {
                var RetornoSAT = retornoSplit[3];
                var chars = Encoding.GetEncoding("UTF-8").GetChars(Encoding.Default.GetBytes(RetornoSAT));
                RetornoSAT = new string(chars);

                var CodAlerta = retornoSplit[2];

                var CodMsgSefaz = retornoSplit[4];
                var MsgSefaz = retornoSplit[5];
                var chars2 = Encoding.GetEncoding("UTF-8").GetChars(Encoding.Default.GetBytes(MsgSefaz));
                MsgSefaz = new string(chars2);

                MessageBox.Show(String.Format("Retorno SAT: {0}{1} \nAlerta: {2} \nRetorno Sefaz: {3} {4} ",
                CodRetornoSAT + " - ", RetornoSAT, CodAlerta, CodMsgSefaz + " - ", MsgSefaz),
                "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                arquivoXML = "";
                numCFe = "";
                return false;
            }
            else
            {
                arquivoXML = retornoSplit[6];
                numCFe = retornoSplit[8];
                return true;
            }
        }

        public static bool TrataRetornoSAT(string Retorno)
        {
            MessageBox.Show(String.Format("Retorno SAT: " + Retorno), "D -SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return true;
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string SatEnviarDadosDaVenda(string xml)
        {
            return modeloSAT.SatEnviarDadosDaVenda(GeraNumeroSessao(), Funcoes.LeParametro(15, "05", true,Principal.nomeEstacao), ConverterToUTF8(xml));
        }

        public static string SatConsultarStatus()
        {
            return modeloSAT.SatConsultarStatus(GeraNumeroSessao());
        }

        public static string SatCancelarUltimaVenda(string chave, string xml)
        {
            return modeloSAT.SatCancelarUltimaVenda(GeraNumeroSessao(), Funcoes.LeParametro(15, "05", true, Principal.nomeEstacao), chave, ConverterToUTF8(xml));
        }

        public static string SatConsultarStatusOperacional()
        {
            return modeloSAT.SatConsultarStatusOperacional(GeraNumeroSessao(), Funcoes.LeParametro(15, "05", true, Principal.nomeEstacao));
        }

        public static string SatConsultarNumeroSessao(string cNumeroDeSessao)
        {
            return modeloSAT.SatConsultarNumeroSessao(GeraNumeroSessao(), Funcoes.LeParametro(15, "05", true, Principal.nomeEstacao), cNumeroDeSessao);
        }

        public static string SatAtualizarSoftware()
        {
            return modeloSAT.SatAtualizarSoftware(GeraNumeroSessao(), Funcoes.LeParametro(15, "05", true, Principal.nomeEstacao));
        }

        protected static void teste()
        {
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += PrintPage;
            pd.Print();
        }

        public static bool SatImpressaoComprovante(string xml, double impFederal, double impEstadual, double impMunicipal, string numCFe, out string numeroNota, string vendedor, long vendaID, string endereco = "", string nome = "", string mensagemPMC = "", string especie = "")
        {
            try
            {
                string comprovante = "";
                string comprovante1 = "";
                string comprovante2 = "";
                string tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);

                xnResul = xmlSAT.GetElementsByTagName("emit");
                if (xnResul.Count > 0)
                {
                    tamanho = xnResul[0]["xNome"].InnerText;
                    comprovante = Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                    tamanho = "";
                    tamanho = Util.SelecionaCampoEspecificoDaTabela("EMPRESA", "EMP_FANTASIA", "EMP_CODIGO", Principal.empAtual.ToString());
                    if(!String.IsNullOrEmpty(tamanho))
                    {
                        comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                    }
                    tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                    comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("CEP:" + xnResul[0]["enderEmit"]["CEP"].InnerText + " - " + xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                    tamanho = "";
                    tamanho = Util.SelecionaCampoEspecificoDaTabela("EMPRESA", "EMP_FONE", "EMP_CODIGO", Principal.empAtual.ToString());
                    if (!String.IsNullOrEmpty(tamanho))
                    {
                        comprovante += Funcoes.CentralizaTexto("TELEFONE: " + tamanho, 43) + "\n";
                    }
                    comprovante += "_________________________________________" + "\n";
                }

                xnResul = xmlSAT.GetElementsByTagName("ide");
                if (xnResul.Count > 0)
                {
                    comprovante += Funcoes.CentralizaTexto("Extrato N " + xnResul[0]["nCFe"].InnerText, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("CUPOM FISCAL ELETRONICO - SAT", 43) + "\n";
                    comprovante += "_________________________________________" + "\n";
                }

                numeroNota = xnResul[0]["nCFe"].InnerText;

                xnResul = xmlSAT.GetElementsByTagName("dest");
                if (xnResul.Count > 0 && (xnResul[0]["CPF"] != null || xnResul[0]["CNPJ"] != null))
                {
                    comprovante += "CPF/CNPJ do Consumidor: " + (xnResul[0]["CNPJ"] != null ? xnResul[0]["CNPJ"].InnerText :
                        xnResul[0]["CPF"].InnerText) + "\n";
                    comprovante += "_________________________________________" + "\n";
                }

                comprovante += "#|COD|DESC|QTD|UN|VL UN R$|(VL TR R$)*|VL ITEM R$" + "\n";
                comprovante += "_________________________________________" + "\n";

                xnResul = xmlSAT.GetElementsByTagName("det");
                for (int i = 0; i < xnResul.Count; i++)
                {
                    comprovante1 += Funcoes.FormataZeroAEsquerda((i + 1).ToString(), 3) + " " + xnResul[i]["prod"]["cProd"].InnerText + " " +
                        (xnResul[i]["prod"]["xProd"].InnerText.Length < 7 ? xnResul[i]["prod"]["xProd"].InnerText : xnResul[i]["prod"]["xProd"].InnerText.Substring(0, 7))
                        + " " + Convert.ToDouble(xnResul[i]["prod"]["qCom"].InnerText) / 10000
                        + xnResul[i]["prod"]["uCom"].InnerText + " X " + String.Format("{0:N}", (Convert.ToDouble(xnResul[i]["prod"]["vUnCom"].InnerText) / 1000)).Replace(",", ".")
                        + " " + String.Format("{0:N}", xnResul[i]["prod"]["vProd"].InnerText) + "\n";

                    if (Convert.ToDouble(xnResul[i]["prod"]["vProd"].InnerText) != Convert.ToDouble(xnResul[i]["prod"]["vItem"].InnerText))
                    {
                        comprovante1 += "rateio de desconto sobre subtotal" + String.Format("{0:N}", xnResul[i]["prod"]["vRatDesc"].InnerText).PadLeft(13 - xnResul[i]["prod"]["vRatDesc"].InnerText.Length, ' ') + "\n";
                    }
                }

                //comprovante1 += "\n";
                xnResul = xmlSAT.GetElementsByTagName("total");
                if (xnResul.Count > 0)
                {
                    comprovante2 += "Total bruto de itens R$ " + String.Format("{0:N}", xnResul[0]["ICMSTot"]["vProd"].InnerText).PadLeft(21 - xnResul[0]["ICMSTot"]["vProd"].InnerText.Length, ' ') + "\n";
                    if (xnResul[0]["DescAcrEntr"] != null)
                    {
                        comprovante2 += "Desconto sobre subtotal R$ " + String.Format("{0:N}", xnResul[0]["DescAcrEntr"]["vDescSubtot"].InnerText).PadLeft(17 - xnResul[0]["DescAcrEntr"]["vDescSubtot"].InnerText.Length, ' ') + "\n";
                    }
                    comprovante2 += "TOTAL R$" + String.Format("{0:N}", xnResul[0]["vCFe"].InnerText).PadLeft(37 - xnResul[0]["vCFe"].InnerText.Length, ' ') + "\n";
                }

                double desconto = Convert.ToDouble(xnResul[0]["ICMSTot"]["vProd"].InnerText) - Convert.ToDouble(xnResul[0]["vCFe"].InnerText);

                if (Funcoes.LeParametro(6, "387", false).Equals("S"))
                {
                    comprovante2 += especie;
                }
                else
                {
                    xnResul = xmlSAT.GetElementsByTagName("MP");
                    for (int i = 0; i < xnResul.Count; i++)
                    {
                        switch (xnResul[i]["cMP"].InnerText)
                        {
                            case "01":
                                comprovante2 += "DINHEIRO" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(37 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                            case "02":
                                comprovante2 += "CHEQUE" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(39 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                            case "03":
                                comprovante2 += "CARTAO DE CREDITO" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(28 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                            case "04":
                                comprovante2 += "CARTAO DE DEBITO" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(29 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                            case "05":
                                comprovante2 += "CREDITO LOJA" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(33 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                            case "99":
                                comprovante2 += "OUTROS" + String.Format("{0:N}", xnResul[i]["vMP"].InnerText).PadLeft(39 - xnResul[i]["vMP"].InnerText.Length, ' ') + "\n";
                                break;
                        }
                    }
                }

                xnResul = xmlSAT.GetElementsByTagName("pgto");
                if (xnResul.Count > 0)
                {
                    if (Convert.ToDouble(xnResul[0]["vTroco"].InnerText) > 0)
                    {
                        comprovante2 += "TROCO R$" + String.Format("{0:N}", xnResul[0]["vTroco"].InnerText).PadLeft(37 - xnResul[0]["vTroco"].InnerText.Length, ' ') + "\n";
                    }
                }

                if (!String.IsNullOrEmpty(endereco.Trim()))
                {
                    comprovante2 += "_________________________________________" + "\n";
                    comprovante2 += "DADOS PARA ENTREGA" + "\n";
                    comprovante2 += "Endereco: " + (endereco.Length > 32 ? endereco.Substring(0, 31) : endereco) + "\n";
                    comprovante2 += "Destinatario: " + (nome.Length > 32 ? nome.Substring(0, 31) : nome) + "\n";
                }

                comprovante2 += "_________________________________________" + "\n";
                comprovante2 += "OBSERVACOES DO CONTRIBUINTE" + "\n";
                comprovante2 += Funcoes.LeParametro(2, "28", true) + "\n";
                comprovante2 += "Vendedor: " + vendedor + "\n";
                comprovante2 += "Venda ID: " + vendaID + "\n";
                if(desconto > 0)
                {
                    comprovante2 += "Voce Economizou: " + String.Format("{0:N}", desconto / 100) + "\n";
                }

                if(Funcoes.LeParametro(6,"378", false).Equals("S"))
                {
                    comprovante2 += mensagemPMC;
                }
                else
                {
                    comprovante2 += "\n";
                }
                
                comprovante2 += "Valor aproximado dos tributos deste cupom" + "\n";
                comprovante2 += "(conforme a Lei Fed. 12.741/2012)" + "\n";
                comprovante2 += String.Format("{0:N}", impFederal).Replace(",", ".") + " Federal " + String.Format("{0:N}", impEstadual).Replace(",", ".") + " Estadual " + String.Format("{0:N}", impMunicipal).Replace(",", ".") + " Municipal Fonte:IBPT" + "\n";
                comprovante2 += "_________________________________________" + "\n";

                xnResul = xmlSAT.GetElementsByTagName("ide");
                if (xnResul.Count > 0)
                {
                    comprovante2 += Funcoes.CentralizaTexto("SAT No." + xnResul[0]["nserieSAT"].InnerText.Substring(0, 3) + "."
                        + xnResul[0]["nserieSAT"].InnerText.Substring(3, 3) + "." + xnResul[0]["nserieSAT"].InnerText.Substring(6, 3), 43) + "\n";
                    comprovante2 += Funcoes.CentralizaTexto(xnResul[0]["dEmi"].InnerText.Substring(6, 2) + "/" + xnResul[0]["dEmi"].InnerText.Substring(4, 2)
                        + "/" + xnResul[0]["dEmi"].InnerText.Substring(0, 4) + " - " + xnResul[0]["hEmi"].InnerText.Substring(0, 2)
                        + ":" + xnResul[0]["hEmi"].InnerText.Substring(2, 2) + ":" + xnResul[0]["hEmi"].InnerText.Substring(4, 2), 43) + "\n\n";

                }

                comprovante2 += numCFe.Substring(3, 4) + " " + numCFe.Substring(7, 4) + " " + numCFe.Substring(11, 4) + " " + numCFe.Substring(15, 4) + " " +
                    numCFe.Substring(19, 4) + " " + numCFe.Substring(23, 4) + " " + numCFe.Substring(27, 4) + " " + numCFe.Substring(31, 4) + "   \n" +
                    numCFe.Substring(35, 4) + " " + numCFe.Substring(39, 4) + " " + numCFe.Substring(43, 4) + "\n";

                int iRetorno;
                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON"))
                {
                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante1);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante2);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ConfiguraCodigoBarras(60, 0, 0, 0, 150);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    //IMPRIME CODIGO DE BARRAS
                    iRetorno = InterfaceEpsonNF.ImprimeCodigoBarrasCODE128(numCFe.Substring(3, 22));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeCodigoBarrasCODE128(numCFe.Substring(25));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    //IMPRIME QRCODE
                    iRetorno = InterfaceEpsonNF.ImprimeCodigoQRCODE(3, 3, 1, 1, 1, Sat.Base64Decode(xnResul[0]["assinaturaQRCODE"].InnerText));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.FechaPorta();
                    if (iRetorno != 1)
                    {
                        return false;
                    }
                }
                else if(Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("BEMATECH"))
                {
                   
                    string s_cmdTX = "\n";
                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                    {
                        //IMPRIME COMPROVANTE
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);

                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante1);

                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante2);

                        //IMPRIME CODIGO DE BARRAS
                        Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(3, 22), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(25), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                        //IMPRIME QRCODE
                        Impressao.Impressao.ImpressaoQRCode(xnResul[0]["assinaturaQRCODE"].InnerText, Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                        if (Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("BEMATECH"))
                        {
                            BematechImpressora.AcionaGuilhotina(0);
                        }
                        else
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }

                        System.Threading.Thread.Sleep(7 * 1000);
                    }
                    else
                    {
                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("4200TH"))
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                        }
                        else
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(5);
                        }

                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(comprovante, 1, 0, 0, 0, 0);

                        iRetorno = BematechImpressora.FormataTX(comprovante1, 1, 0, 0, 0, 0);

                        iRetorno = BematechImpressora.FormataTX(comprovante2, 1, 0, 0, 0, 0);

                        iRetorno = BematechImpressora.ConfiguraCodigoBarras(60, 0, 0, 0, 150);

                        iRetorno = BematechImpressora.ImprimeCodigoBarrasCODE128(numCFe.Substring(3, 22));
                    
                        s_cmdTX = "\r\n";

                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.ImprimeCodigoBarrasCODE128(numCFe.Substring(25));

                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);
                        
                        iRetorno = BematechImpressora.ImprimeCodigoQRCODE(0, 6, 0, 10, 1, Sat.Base64Decode(xnResul[0]["assinaturaQRCODE"].InnerText));

                        s_cmdTX = "\r\n\r\n\r\n";

                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.AcionaGuilhotina(0);

                        iRetorno = BematechImpressora.FechaPorta();
                    }
                }
                else 
                {
                    //IMPRIME COMPROVANTE
                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);

                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante1);

                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante2);

                    //IMPRIME CODIGO DE BARRAS
                    Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(3, 22), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                    Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(25), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                    //IMPRIME QRCODE
                    Impressao.Impressao.ImpressaoQRCode(xnResul[0]["assinaturaQRCODE"].InnerText, Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH"))
                    {
                        BematechImpressora.AcionaGuilhotina(0);
                    }
                    else
                    {
                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                    }

                    System.Threading.Thread.Sleep(7 * 1000);
                }

                return true;
            }
            catch (Exception ex)
            {
                numeroNota = "";
                MessageBox.Show("Erro: " + ex.Message, "SAT Impressão", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private static void PrintPage(object o, PrintPageEventArgs e)
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile(@"C:\BTI\teste.bmp");
            Point loc = new Point(50, 50);
            e.Graphics.DrawImage(img, loc);

        }
        public static bool SatImpressaoComprovanteCancelamento(string xml, string numCFe, double total, out string numeroNota)
        {
            try
            {
                string comprovante = "";
                string tamanho;
                XmlDocument xmlSAT = new XmlDocument();
                XmlNodeList xnResul;
                xmlSAT.Load(xml);
                numeroNota = "";

                xnResul = xmlSAT.GetElementsByTagName("emit");
                if (xnResul.Count > 0)
                {
                    comprovante = Funcoes.CentralizaTexto(xnResul[0]["xFant"].InnerText, 43) + "\n";
                    tamanho = xnResul[0]["xNome"].InnerText;
                    comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                    tamanho = xnResul[0]["enderEmit"]["xLgr"].InnerText + " " + xnResul[0]["enderEmit"]["nro"].InnerText + " " + xnResul[0]["enderEmit"]["xBairro"].InnerText;
                    comprovante += Funcoes.CentralizaTexto(tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(xnResul[0]["enderEmit"]["xMun"].InnerText + " - SP", 43) + "\n\n";
                    comprovante += Funcoes.CentralizaTexto("CNPJ:" + xnResul[0]["CNPJ"].InnerText, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("IE:" + xnResul[0]["IE"].InnerText, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("IM:" + xnResul[0]["IM"].InnerText, 43) + "\n";
                    comprovante += "__________________________________________";
                }

                xnResul = xmlSAT.GetElementsByTagName("ide");
                if (xnResul.Count > 0)
                {
                    numeroNota = xnResul[0]["nCFe"].InnerText;
                    comprovante += Funcoes.CentralizaTexto("Extrato N " + xnResul[0]["nCFe"].InnerText, 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("CUPOM FISCAL ELETRONICO - SAT", 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto("CANCELAMENTO", 43) + "\n";
                    comprovante += "__________________________________________" + "\n";
                }

                comprovante += Funcoes.CentralizaTexto("DADOS DO CUPOM FISCAL ELETRONICO CANCELADO", 43) + "\n";

                xnResul = xmlSAT.GetElementsByTagName("dest");
                if (xnResul[0]["CPF"] == null)
                {
                    comprovante += "CPF/CNPJ do Consumidor: " + "\n";
                }
                else
                {
                    comprovante += "CPF/CNPJ do Consumidor: " + xnResul[0]["CPF"].InnerText + "\n";
                }
                comprovante += "TOTAL R$: " + String.Format("{0:N}", total) + "\n";
                comprovante += "__________________________________________" + "\n";

                xnResul = xmlSAT.GetElementsByTagName("ide");
                if (xnResul.Count > 0)
                {
                    comprovante += Funcoes.CentralizaTexto("SAT No." + xnResul[0]["nserieSAT"].InnerText.Substring(0, 3) + "."
                        + xnResul[0]["nserieSAT"].InnerText.Substring(3, 3) + "." + xnResul[0]["nserieSAT"].InnerText.Substring(6, 3), 43) + "\n";
                    comprovante += Funcoes.CentralizaTexto(xnResul[0]["dEmi"].InnerText.Substring(6, 2) + "/" + xnResul[0]["dEmi"].InnerText.Substring(4, 2)
                        + "/" + xnResul[0]["dEmi"].InnerText.Substring(0, 4) + " - " + xnResul[0]["hEmi"].InnerText.Substring(0, 2)
                        + ":" + xnResul[0]["hEmi"].InnerText.Substring(2, 2) + ":" + xnResul[0]["hEmi"].InnerText.Substring(4, 2), 43) + "\n\n";

                }

                comprovante += numCFe.Substring(3, 4) + " " + numCFe.Substring(7, 4) + " " + numCFe.Substring(11, 4) + " " + numCFe.Substring(15, 4) + " " +
                    numCFe.Substring(19, 4) + " " + numCFe.Substring(23, 4) + " " + numCFe.Substring(27, 4) + " " + numCFe.Substring(31, 4) + "   " +
                    numCFe.Substring(35, 4) + " " + numCFe.Substring(39, 4) + " " + numCFe.Substring(43, 4) + "\n\n";

                if (Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("EPSON"))
                {
                    int iRetorno;

                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ConfiguraCodigoBarras(60, 0, 0, 0, 150);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    //IMPRIME CODIGO DE BARRAS
                    iRetorno = InterfaceEpsonNF.ImprimeCodigoBarrasCODE128(numCFe.Substring(3, 22));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeCodigoBarrasCODE128(numCFe.Substring(25));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    //IMPRIME QRCODE
                    iRetorno = InterfaceEpsonNF.ImprimeCodigoQRCODE(3, 3, 1, 1, 1, Sat.Base64Decode(xnResul[0]["assinaturaQRCODE"].InnerText));
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                    if (iRetorno != 1)
                    {
                        return false;
                    }

                    iRetorno = InterfaceEpsonNF.FechaPorta();
                    if (iRetorno != 1)
                    {
                        return false;
                    }
                }
                else if (Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("BEMATECH"))
                {
                    int iRetorno;
                    string s_cmdTX = "\n";
                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                    {
                        //IMPRIME COMPROVANTE
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);

                        //IMPRIME CODIGO DE BARRAS

                        Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(3, 22), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(25), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                        //IMPRIME QRCODE
                        Impressao.Impressao.ImpressaoQRCode(xnResul[0]["assinaturaQRCODE"].InnerText, Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                        if (Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("BEMATECH"))
                        {
                            BematechImpressora.AcionaGuilhotina(0);
                        }
                        else
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }

                    }
                    else
                    {
                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                        
                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                        iRetorno = BematechImpressora.ConfiguraCodigoBarras(60, 1, 0, 1, 1);

                        iRetorno = BematechImpressora.ImprimeCodigoBarrasCODE128(numCFe.Substring(3, 22));

                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.ImprimeCodigoBarrasCODE128(numCFe.Substring(25));

                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        // iRetorno = BematechImpressora.ComandoTX("\x1B\x61\x1", 7);

                        iRetorno = BematechImpressora.ImprimeCodigoQRCODE(1, 5, 0, 2, 1, Sat.Base64Decode(xnResul[0]["assinaturaQRCODE"].InnerText));

                        s_cmdTX = "\r\n";
                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.AcionaGuilhotina(0);

                        iRetorno = BematechImpressora.FechaPorta();
                    }

                }
                else
                {
                    //IMPRIME COMPROVANTE
                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);

                    //IMPRIME CODIGO DE BARRAS

                    Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(3, 22), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                    Impressao.Impressao.ImpressaoCodBarra(numCFe.Substring(25), Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                    //IMPRIME QRCODE
                    Impressao.Impressao.ImpressaoQRCode(xnResul[0]["assinaturaQRCODE"].InnerText, Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));

                    if (Funcoes.LeParametro(15, "09", true,Principal.nomeEstacao).Equals("BEMATECH"))
                    {
                        BematechImpressora.AcionaGuilhotina(0);
                    }
                    else
                    {
                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                numeroNota = "";
                MessageBox.Show("Erro: " + ex.Message, "SAT Impressão", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        #region CANCELAMENTO
        public static string MontaXmlCancelamento(string numeroChave)
        {
            try
            {
                string xmlSAT;
                xmlSAT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xmlSAT += "<CFeCanc>";
                xmlSAT += " <infCFe chCanc=\"" + numeroChave + "\">";
                xmlSAT += "<ide><CNPJ>" + Funcoes.LeParametro(15, "07", true) + "</CNPJ>";
                xmlSAT += "<signAC>" + Funcoes.LeParametro(15, "03", true,Principal.nomeEstacao) + "</signAC>";
                xmlSAT += "<numeroCaixa>" + Funcoes.LeParametro(15, "04", true, Principal.nomeEstacao) + "</numeroCaixa>";
                xmlSAT += "</ide>";
                xmlSAT += "<emit/>";
                xmlSAT += "<dest/> ";
                xmlSAT += "<total/>";
                xmlSAT += "</infCFe>";
                xmlSAT += "</CFeCanc>";

                return xmlSAT;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }
        #endregion
    }
}
