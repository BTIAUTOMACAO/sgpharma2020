﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace GestaoAdministrativa.Cadastros
{
    /// <summary>
    /// Cadastro de Colaboradores (Vendedor, Caixa, Comprador, Entregador, etc)
    /// </summary>
    public partial class frmCadColaboradores : Form, Botoes
    {
        private DataTable dtColab = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbColab = new ToolStripButton("Colaboradores");
        private ToolStripSeparator tssColab = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private string enderecoWebService = Funcoes.LeParametro(9, "54", true);
        private string grupoID = Funcoes.LeParametro(9, "53", true);
        private string codEstab = Funcoes.LeParametro(9, "52", true);

        /// <summary>
        /// Inicialização do Formulário
        /// </summary>
        /// <param name="menu">Formulário Principal</param>
        public frmCadColaboradores(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadColaboradores_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Função que Habilita ou Desabilita Botões de Cadastro
        /// </summary>
        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadColaboradores");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
            if (tcColab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcColab.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtColabID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Cria botão do formulário na barra inferior do menu principal
        /// </summary>
        public void Botao()
        {
            try
            {
                this.tsbColab.AutoSize = false;
                this.tsbColab.Image = Properties.Resources.cadastro;
                this.tsbColab.Size = new System.Drawing.Size(125, 20);
                this.tsbColab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbColab);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssColab);
                tsbColab.Click += delegate
                {
                    var cadColab = Application.OpenForms.OfType<frmCadColaboradores>().FirstOrDefault();
                    BotoesHabilitados();
                    cadColab.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Abre formulário se o mesmo já estiver na barra de botões
        /// </summary>
        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgColab_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
            emGrade = true;
        }

        private void dgColab_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcColab.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Carrega os dados do registro selecionado, quando muda da aba em grade para a aba em ficha
        /// </summary>
        /// <param name="linha">ID da linha do Grid dos Registros a ser carregado</param>
        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Empresa: " + Principal.empAtual + " - " + Principal.nomeAtual;
                txtColabID.Text = Math.Round(Convert.ToDecimal(dtColab.Rows[linha]["COL_CODIGO"]), 0).ToString();
                txtNome.Text = dtColab.Rows[linha]["COL_NOME"].ToString();
                txtApelido.Text = dtColab.Rows[linha]["COL_APELIDO"].ToString();
                txtRg.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_RG"].ToString());
                cmbStatus.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_STATUS"].ToString());
                if (cmbStatus.Text.Equals("SUPERVISOR"))
                {
                    lblSuper.Visible = true;
                    txtSenhaSuper.Visible = true;
                    picSuper.Visible = true;
                    txtSenhaSuper.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_SENHA_SUPERVISOR"].ToString()) == "" ? "" : Funcoes.DescriptografaSenha(dtColab.Rows[linha]["COL_SENHA_SUPERVISOR"].ToString());
                }
                else
                {
                    lblSuper.Visible = false;
                    txtSenhaSuper.Visible = false;
                    picSuper.Visible = false;
                }
                txtEndereco.Text = dtColab.Rows[linha]["COL_ENDER"].ToString();
                txtBairro.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_BAIRRO"].ToString());
                txtCidade.Text = dtColab.Rows[linha]["COL_CIDADE"].ToString();
                txtCep.Text = Math.Round(Convert.ToDecimal(dtColab.Rows[linha]["COL_CEP"]), 2).ToString();
                cmbUF.SelectedItem = dtColab.Rows[linha]["COL_UF"].ToString();
                txtTelefone.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_FONE"].ToString());
                txtCelular.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_CELULAR"].ToString());
                txtEmail.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_EMAIL"].ToString());
                txtHora.Text = String.Format("{0:N}", dtColab.Rows[linha]["COL_VALOR_HORA"]);
                txtComissao.Text = String.Format("{0:N}", dtColab.Rows[linha]["COL_COMISSAO"]);
                txtSenha.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_SENHA"].ToString()) == "" ? "" : Funcoes.DescriptografaSenha(dtColab.Rows[linha]["COL_SENHA"].ToString());
                if (dtColab.Rows[linha]["COL_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["COL_OPALTERACAO"].ToString());

                if (String.IsNullOrEmpty(txtComissao.Text))
                {
                    txtComissao.Text = "0,00";
                }

                if (String.IsNullOrEmpty(txtHora.Text))
                {
                    txtHora.Text = "0,00";
                }
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, linha));
                txtNome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();

            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void tcColab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcColab.SelectedTab == tpGrade)
            {
                dgColab.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcColab.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadColaboradores");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;

                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
                    chkLiberado.Checked = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                    txtNome.Focus();
                }
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Primeiro()
        {
            if (dtColab.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcColab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Ultimo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = dtColab.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Proximo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão Atualizar do Menu Principal, atualiza um registro
        /// </summary>
        /// <returns></returns>
        public bool Atualiza()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;
                    string status = "";
                    switch (cmbStatus.Text)
                    {
                        case "CAIXA":
                            status = "X";
                            break;
                        case "COMPRADOR":
                            status = "C";
                            break;
                        case "EMPREGADO":
                            status = "E";
                            break;
                        case "ENTREGADOR":
                            status = "T";
                            break;
                        case "VENDEDOR":
                            status = "V";
                            break;
                        case "SUPERVISOR":
                            status = "S";
                            break;
                    }

                    var colaborador = new Colaborador(
                            Principal.empAtual,
                            Convert.ToInt32(txtColabID.Text),
                            txtNome.Text.Trim().ToUpper(),
                            txtApelido.Text.Trim().ToUpper(),
                            txtEndereco.Text.ToUpper().Trim(),
                            txtBairro.Text.Trim() == "" ? "" : txtBairro.Text.ToUpper().Trim(),
                            txtCidade.Text.ToUpper().Trim(),
                            cmbUF.Text,
                            Funcoes.RemoveCaracter(txtCep.Text),
                            Funcoes.RemoveCaracter(txtTelefone.Text.Replace(" ", "")),
                            Funcoes.RemoveCaracter(txtCelular.Text.Replace(" ", "")),
                            txtEmail.Text.Trim() == "" ? "" : txtEmail.Text,
                            status,
                            Funcoes.RemoveCaracter(txtRg.Text) == " " ? "" : Funcoes.RemoveCaracter(txtRg.Text),
                            Convert.ToDecimal(txtHora.Text),
                            Convert.ToDecimal(txtComissao.Text),
                            Funcoes.CriptografaSenha(txtSenha.Text),
                            txtSenhaSuper.Visible.Equals(true) ? Funcoes.CriptografaSenha(txtSenhaSuper.Text.Trim()) : "",
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario

                        );

                    Principal.dtBusca = Util.RegistrosPorEmpresa("COLABORADORES", "COL_CODIGO", txtColabID.Text, true);

                    if (colaborador.AtualizaDados(colaborador, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Colaboradores");

                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtColab.Clear();
                        dgColab.DataSource = dtColab;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Botão Incluir do Menu Principal, insere um registro
        /// </summary>
        /// <returns></returns>
        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos())
                {
                    Cursor = Cursors.WaitCursor;
                    string status = "";
                    chkLiberado.Checked = true;
                    switch (cmbStatus.Text)
                    {
                        case "CAIXA":
                            status = "X";
                            break;
                        case "COMPRADOR":
                            status = "C";
                            break;
                        case "EMPREGADO":
                            status = "E";
                            break;
                        case "ENTREGADOR":
                            status = "T";
                            break;
                        case "VENDEDOR":
                            status = "V";
                            break;
                        case "SUPERVISOR":
                            status = "S";
                            break;
                    }
                    var colaborador = new Colaborador(
                                Principal.empAtual,
                                Funcoes.IdentificaVerificaID("COLABORADORES", "COL_CODIGO", 0, string.Empty),
                                txtNome.Text.Trim().ToUpper(),
                                txtApelido.Text.Trim().ToUpper(),
                                txtEndereco.Text.ToUpper().Trim(),
                                txtBairro.Text.Trim() == "" ? "" : txtBairro.Text.ToUpper().Trim(),
                                txtCidade.Text.ToUpper().Trim(),
                                cmbUF.Text,
                                Funcoes.RemoveCaracter(txtCep.Text),
                                Funcoes.RemoveCaracter(txtTelefone.Text.Replace(" ", "")),
                                Funcoes.RemoveCaracter(txtCelular.Text.Replace(" ", "")),
                                txtEmail.Text.Trim() == "" ? "" : txtEmail.Text,
                                status,
                                Funcoes.RemoveCaracter(txtRg.Text) == " " ? "" : Funcoes.RemoveCaracter(txtRg.Text),
                                Convert.ToDecimal(txtHora.Text),
                                Convert.ToDecimal(txtComissao.Text),
                                Funcoes.CriptografaSenha(txtSenha.Text),
                                txtSenhaSuper.Visible.Equals(true) ? Funcoes.CriptografaSenha(txtSenhaSuper.Text.Trim()) : "",
                                chkLiberado.Checked == true ? "N" : "S",
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario

                            );

                    Principal.dtPesq = Util.RegistrosPorEmpresa("COLABORADORES", "COL_NOME", txtNome.Text.Trim().ToUpper(), true, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Principal.mensagem = "Colaboradores já cadastrada.";
                        Funcoes.Avisa();
                        return false;
                    }
                    else
                    {
                        if (colaborador.InsereDados(colaborador).Equals(true))
                        {

                            //INsere dados api//
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtColab.Clear();
                            dgColab.DataSource = dtColab;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtNome.Focus();
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Botão Excluir do Menu Principal, remove o registro selecionado
        /// </summary>
        /// <returns></returns>
        public bool Excluir()
        {
            try
            {
                if ((txtColabID.Text.Trim() != "") && (txtNome.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do colaborador?", "Exclusão tabela Colaborador", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirColab(txtColabID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            var colaborador = new Colaborador();
                            if (!colaborador.ExcluirDados(Convert.ToInt32(txtColabID.Text)).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("COL_CODIGO", txtColabID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "COLABORADORES", txtNome.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                                MessageBox.Show("Caladorador excluido com sucesso!", "Colaboradores");
                                dtColab.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgColab.DataSource = dtColab;
                                emGrade = false;
                                tcColab.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Fecha o formulário e remove o botão do mesmo
        /// </summary>
        public void Sair()
        {
            try
            {
                dtColab.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbColab);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssColab);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Limpa os campos da Aba em Grade
        /// </summary>
        public void LimparGrade()
        {
            dtColab.Clear();
            dgColab.DataSource = dtColab;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            lblSuper.Visible = false;
            picSuper.Visible = false;
            txtSenhaSuper.Visible = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        /// <summary>
        /// Limpa os campos da Aba em Ficha
        /// </summary>
        public void LimparFicha()
        {
            txtColabID.Text = "";
            txtNome.Text = "";
            txtApelido.Text = "";
            txtRg.Text = "";
            txtEndereco.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtEmail.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            chkLiberado.Checked = false;
            cmbStatus.SelectedIndex = 0;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtHora.Text = "0,00";
            txtComissao.Text = "0,00";
            txtSenha.Text = "";
            lblSuper.Visible = false;
            picSuper.Visible = false;
            txtSenhaSuper.Visible = false;
            txtNome.Focus();
        }

        /// <summary>
        /// Botão Limpar do Menu Principal, limpa os dados do Formulário
        /// </summary>
        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcColab.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        /// <summary>
        /// Botão Impressão de Relatório do Menu Principal
        /// </summary>
        public void ImprimirRelatorio()
        {
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtApelido.Focus();
            }
        }

        private void txtApelido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtRg.Focus();
            }
        }

        private void txtRg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                cmbStatus.Focus();
            }
        }

        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    txtCep.Focus();
                }
                else
                    txtEndereco.Focus();
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtBairro.Focus();
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCidade.Focus();
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCep.Focus();
            }
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {
                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.ToUpper();
                        txtCidade.Text = end.localidade.ToUpper();
                        cmbUF.Text = end.uf;
                        txtTelefone.Focus();
                    }
                    else
                        txtEndereco.Focus();

                    Cursor = Cursors.Default;
                }
            }
        }

        /// <summary>
        /// Consulta endereço via CEP utilizando a API Via CEP
        /// </summary>
        /// <param name="cep">Número do CEP</param>
        /// <returns></returns>
        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtTelefone.Focus();
            }
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtCelular.Focus();
            }
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtEmail.Focus();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtHora.Focus();
            }
        }

        private void txtHora_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtComissao.Focus();
            }
        }

        private void txtComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtSenha.Focus();
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtApelido.Text.Trim()))
            {
                Principal.mensagem = "Apelido não pode ser em branco.";
                Funcoes.Avisa();
                txtApelido.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtBairro.Text.Trim()))
            {
                Principal.mensagem = "Bairro não pode ser em branco.";
                Funcoes.Avisa();
                txtBairro.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
            {
                Principal.mensagem = "Cep não pode ser em branco.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtSenha.Text.Trim()))
            {
                Principal.mensagem = "Senha não pode ser em branco.";
                Funcoes.Avisa();
                txtSenha.Focus();
                return false;
            }
            if (Equals(cmbStatus.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um Status.";
                Funcoes.Avisa();
                cmbStatus.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Exporta os dados do Grid para Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgColab.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgColab);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }
                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgColab.RowCount;
                    for (i = 0; i <= dgColab.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgColab[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgColab.ColumnCount; k++)
                    {
                        if (dgColab.Columns[k].Visible == true)
                        {
                            switch (dgColab.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Valor da Hora":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Comissao":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgColab.ColumnCount : indice) + Convert.ToString(dgColab.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var colaborador = new Colaborador();

                int colCodigo = txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text);
                dtColab = colaborador.BuscaDados(Convert.ToInt32(Principal.empAtual), colCodigo, txtBNome.Text.ToUpper().Trim(), cmbLiberado.Text, out strOrdem);
                if (dtColab.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtColab.Rows.Count;
                    for (int i = 0; i < dtColab.Rows.Count; i++)
                    {
                        switch (dtColab.Rows[i]["COL_STATUS"].ToString())
                        {
                            case "X":
                                dtColab.Rows[i]["COL_STATUS"] = "CAIXA";
                                break;
                            case "C":
                                dtColab.Rows[i]["COL_STATUS"] = "COMPRADOR";
                                break;
                            case "E":
                                dtColab.Rows[i]["COL_STATUS"] = "EMPREGADO";
                                break;
                            case "T":
                                dtColab.Rows[i]["COL_STATUS"] = "ENTREGADOR";
                                break;
                            case "V":
                                dtColab.Rows[i]["COL_STATUS"] = "VENDEDOR";
                                break;
                            case "S":
                                dtColab.Rows[i]["COL_STATUS"] = "SUPERVISOR";
                                break;
                        }

                        dtColab.AcceptChanges();
                    }

                    dgColab.DataSource = dtColab;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, 0));
                    dgColab.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgColab.DataSource = dtColab;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgColab_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgColab.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtColab = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgColab.Columns[e.ColumnIndex].Name + " DESC");
                        dgColab.DataSource = dtColab;
                        decrescente = true;
                    }
                    else
                    {
                        dtColab = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgColab.Columns[e.ColumnIndex].Name + " ASC");
                        dgColab.DataSource = dtColab;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void dgColab_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtColab.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtColab.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtColab.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgColab_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgColab.Rows[e.RowIndex].Cells[15].Value.ToString() == "N")
            {
                dgColab.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgColab.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void txtHora_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtHora.Text.Trim()))
            {
                txtHora.Text = String.Format("{0:N}", Convert.ToDecimal(txtHora.Text));
            }
            else
                txtHora.Text = "0,00";
        }

        private void txtComissao_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtComissao.Text.Trim()))
            {
                txtComissao.Text = String.Format("{0:N}", Convert.ToDecimal(txtComissao.Text));
            }
            else
                txtComissao.Text = "0,00";
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtProdID") && (control.Name != "txtEstAtu") && (control.Name != "txtCusUlt") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    if (dgColab.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgColab.ColumnCount];
                string[] coluna = new string[dgColab.ColumnCount];

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    grid[i] = dgColab.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    if (dgColab.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgColab.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgColab.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgColab.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgColab.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgColab.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgColab.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgColab.ColumnCount; i++)
                        {
                            dgColab.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Colaboradores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSenhaSuper.Focus();
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox10, "Campo Obrigatório");
        }

        /// <summary>
        /// Atalho F1 para ir para aba em Grade
        /// </summary>
        public void AtalhoGrade()
        {
            tcColab.SelectedTab = tpGrade;
        }

        /// <summary>
        /// Atalho F2 para ir para aba em Ficha
        /// </summary>
        public void AtalhoFicha()
        {
            tcColab.SelectedTab = tpFicha;
        }

        private void picSuper_MouseDown(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(this.picSuper, "Campo Obrigatório");
        }

        private void cmbStatus_Validated(object sender, EventArgs e)
        {
            if (cmbStatus.Text.Equals("SUPERVISOR"))
            {
                lblSuper.Visible = true;
                txtSenhaSuper.Visible = true;
                picSuper.Visible = true;
            }
        }

        /// <summary>
        /// Botão Ajuda do Menu Principal
        /// </summary>
        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void cmbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbStatus.Text == "SUPERVISOR")
            {
                picSuper.Visible = true;
                lblSuper.Visible = true;
                txtSenhaSuper.Visible = true;
            }
            else
            {
                picSuper.Visible = false;
                lblSuper.Visible = false;
                txtSenhaSuper.Visible = false;
            }
        }
    }
}
