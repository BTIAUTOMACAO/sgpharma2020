﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadEspecies : Form, Botoes
    {
        private DataTable dtEspecie = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbEspecie = new ToolStripButton("Espécies");
        private ToolStripSeparator tssEspecie = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadEspecies(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadEspecies_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void dgEspecie_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, contRegistros));
            emGrade = true;
        }

        private void dgEspecie_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEspecie.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEspecie.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEspecie.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEspecie.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtEspID.Text = Funcoes.ChecaCampoVazio(Math.Round(Convert.ToDecimal(dtEspecie.Rows[linha]["ESP_CODIGO"]), 0).ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtEspecie.Rows[linha]["ESP_DESCRICAO"].ToString());
                if (dtEspecie.Rows[linha]["ESP_CHEQUE"].ToString() == "N")
                {
                    chkCheque.Checked = false;
                }
                else
                    chkCheque.Checked = true;

                if (dtEspecie.Rows[linha]["ESP_CAIXA"].ToString().Equals("SIM"))
                {
                    chkConEspecie.Checked = true;
                }
                else
                    chkConEspecie.Checked = false;

                if (dtEspecie.Rows[linha]["ESP_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                if (dtEspecie.Rows[linha]["ESP_VINCULADO"].ToString() == "N")
                {
                    chkVinculado.Checked = false;
                }
                else
                    chkVinculado.Checked = true;

                txtEspSAT.Text = Funcoes.ChecaCampoVazio(dtEspecie.Rows[linha]["ESP_SAT"].ToString());
                txtEfc.Text = Funcoes.ChecaCampoVazio(dtEspecie.Rows[linha]["ESP_ECF"].ToString());
                txtData.Text = Funcoes.ChecaCampoVazio(dtEspecie.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEspecie.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtEspecie.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEspecie.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEspecie.CurrentCell = dgEspecie.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEspecie.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEspecie.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEspecie.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEspecie.CurrentCell = dgEspecie.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEspecie.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEspecie.CurrentCell = dgEspecie.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEspecie.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEspecie.CurrentCell = dgEspecie.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadEspecies");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, contRegistros));
            if (tcEspecie.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEspecie.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEspID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEspecie.AutoSize = false;
                this.tsbEspecie.Image = Properties.Resources.cadastro;
                this.tsbEspecie.Size = new System.Drawing.Size(125, 20);
                this.tsbEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEspecie);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEspecie);
                tsbEspecie.Click += delegate
                {
                    var cadEspecie = Application.OpenForms.OfType<frmCadEspecies>().FirstOrDefault();
                    BotoesHabilitados();
                    cadEspecie.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtEspecie.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEspecie);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEspecie);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtEspecie.Clear();
            dgEspecie.DataSource = dtEspecie;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtEspID.Text = "";
            txtDescr.Text = "";
            chkCheque.Checked = false;
            chkLiberado.Checked = false;
            chkConEspecie.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtEspSAT.Text = "";
            txtEfc.Text = "";
            chkVinculado.Checked = false;
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcEspecie.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
            {
                LimparFicha();
            }

            BotoesHabilitados();
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Excluir()
        {
            try
            {
                if ((txtEspID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da espécie?", "Exclusão tabela Espécies", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirEspecie(txtEspID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var especie = new Especie();
                        if (!especie.ExcluirDados(txtEspID.Text).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("ESP_CODIGO", txtEspID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CAD_ESPECIES", txtDescr.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                            dtEspecie.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgEspecie.DataSource = dtEspecie;
                            tcEspecie.SelectedTab = tpGrade;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                    }
                    return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("CAD_ESPECIES", "ESP_CODIGO", txtEspID.Text);

                    var especie = new Especie(
                       Convert.ToInt32(txtEspID.Text),
                       txtDescr.Text.Trim(),
                       chkCheque.Checked ? "S" : "N",
                       chkConEspecie.Checked ? "S" : "N",
                       chkLiberado.Checked ? "N" : "S",
                       chkVinculado.Checked ? "S" : "N",
                       txtEspSAT.Text,
                       txtEfc.Text,
                       DateTime.Now,
                       Principal.usuario,
                       DateTime.Now,
                       Principal.usuario
                    );

                    if (especie.AtualizaDados(especie, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Espécies");
                    }

                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtEspecie.Clear();
                    dgEspecie.DataSource = dtEspecie;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos())
                {
                    txtEspID.Text = Funcoes.IdentificaVerificaID("CAD_ESPECIES", "ESP_CODIGO").ToString();
                    chkLiberado.Checked = true;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");

                    var especie = new Especie(
                        Convert.ToInt32(txtEspID.Text),
                        txtDescr.Text.Trim(),
                        chkCheque.Checked ? "S" : "N",
                        chkConEspecie.Checked ? "S" : "N",
                        chkLiberado.Checked ? "N" : "S",
                        chkVinculado.Checked ? "S" : "N",
                        txtEspSAT.Text,
                        txtEfc.Text,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );

                    if (Util.RegistrosPorEstabelecimento("CAD_ESPECIES", "ESP_DESCRICAO", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Espécie já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    if (especie.InserirDados(especie).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("ESP_CODIGO", txtEspID.Text, Principal.usuario, "CAD_ESPECIES", txtDescr.Text, Principal.estAtual, Principal.empAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtEspecie.Clear();
                        dgEspecie.DataSource = dtEspecie;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void tcEspecie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEspecie.SelectedTab == tpGrade)
            {
                dgEspecie.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEspecie.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadEspecies");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, contRegistros));
                    chkLiberado.Checked = false;
                    chkCheque.Checked = false;
                    chkConEspecie.Checked = false;
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtDescr.Focus();
                }
            }
        }


        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEspSAT.Text.Trim()))
            {
                Principal.mensagem = "Espécie SAT não pode ser em branco.";
                Funcoes.Avisa();
                txtEspSAT.Focus();
                return false;
            }

            return true;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var especie = new Especie();

                dtEspecie = especie.BuscarDados(txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text), txtBDescr.Text, cmbLiberado.Text, out strOrdem);
                if (dtEspecie.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtEspecie.Rows.Count;
                    dgEspecie.DataSource = dtEspecie;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, 0));
                    dgEspecie.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEspecie.DataSource = dtEspecie;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEspecie.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEspecie);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEspecie.RowCount;
                    for (i = 0; i <= dgEspecie.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEspecie[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEspecie.ColumnCount; k++)
                    {
                        if (dgEspecie.Columns[k].Visible == true)
                        {
                            switch (dgEspecie.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEspecie.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEspecie.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEspecie.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEspecie.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEspecie.ColumnCount : indice) + Convert.ToString(dgEspecie.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgEspecie_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEspecie.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEspecie = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEspecie.Columns[e.ColumnIndex].Name + " DESC");
                        dgEspecie.DataSource = dtEspecie;
                        decrescente = true;
                    }
                    else
                    {
                        dtEspecie = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEspecie.Columns[e.ColumnIndex].Name + " ASC");
                        dgEspecie.DataSource = dtEspecie;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEspecie, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void dgEspecie_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgEspecie.Rows[e.RowIndex].Cells[4].Value.ToString() == "N")
            {
                dgEspecie.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgEspecie.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtConID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEspecie.ColumnCount; i++)
                {
                    if (dgEspecie.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEspecie.ColumnCount];
                string[] coluna = new string[dgEspecie.ColumnCount];

                for (int i = 0; i < dgEspecie.ColumnCount; i++)
                {
                    grid[i] = dgEspecie.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEspecie.ColumnCount; i++)
                {
                    if (dgEspecie.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgEspecie.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEspecie.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEspecie.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEspecie.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEspecie.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEspecie.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEspecie.ColumnCount; i++)
                        {
                            dgEspecie.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEspSAT.Focus();
        }

        private void chkCheque_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkConEspecie.Focus();
        }

        public void AtalhoGrade()
        {
            tcEspecie.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEspecie.SelectedTab = tpFicha;
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtEspSAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEfc.Focus();
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void txtEfc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkCheque.Focus();
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void chkCheque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkConEspecie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void chkLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkVinculado.Focus();
        }
        
    }
}
