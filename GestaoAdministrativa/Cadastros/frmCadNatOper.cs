﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadNatOper : Form, Botoes
    {
        private DataTable dtNatOper = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbNatOper = new ToolStripButton("Natureza de Operação");
        private ToolStripSeparator tssNatOper = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadNatOper(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmNatOper_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcNatOper_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcNatOper.SelectedTab == tpGrade)
            {
                dgNatOper.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;

            }
            else
                if (tcNatOper.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadNatOper");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = true;
                        emGrade = false;
                        txtNoCFiscal.Focus();
                    }
                }
        }

        private void dgNatOper_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, contRegistros));
            emGrade = true;
        }

        private void dgNatOper_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcNatOper.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgNatOper_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtNatOper.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtNatOper.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtNatOper.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public void Primeiro()
        {
            if (dtNatOper.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgNatOper.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgNatOper.CurrentCell = dgNatOper.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcNatOper.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtNatOper.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgNatOper.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgNatOper.CurrentCell = dgNatOper.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgNatOper.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgNatOper.CurrentCell = dgNatOper.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgNatOper.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgNatOper.CurrentCell = dgNatOper.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadNatOper");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, contRegistros));
            if (tcNatOper.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcNatOper.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtNoID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbNatOper.AutoSize = false;
                this.tsbNatOper.Image = Properties.Resources.cadastro;
                this.tsbNatOper.Size = new System.Drawing.Size(160, 20);
                this.tsbNatOper.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbNatOper);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssNatOper);
                tsbNatOper.Click += delegate
                {
                    var cadNatOper = Application.OpenForms.OfType<frmCadNatOper>().FirstOrDefault();
                    BotoesHabilitados();
                    cadNatOper.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtNatOper.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbNatOper);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssNatOper);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtNatOper.Clear();
            dgNatOper.DataSource = dtNatOper;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtNoID.Text = "";
            txtNoCFiscal.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            rdbEntrada.Checked = false;
            rdbSaida.Checked = false;
            chkEstoque.Checked = false;
            chkFatura.Checked = false;
            chkRelatorios.Checked = false;
            txtComp.Text = "";
            txtNoCFiscal.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcNatOper.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                lblEstab.Text = "Empresa: " + Principal.empAtual + " - " + Principal.nomeAtual;
                txtNoID.Text = Funcoes.ChecaCampoVazio(Math.Round(Convert.ToDecimal(dtNatOper.Rows[linha]["NO_ID"]), 0).ToString());
                txtNoCFiscal.Text = Funcoes.ChecaCampoVazio(dtNatOper.Rows[linha]["NO_CODIGO"].ToString());
                txtComp.Text = Funcoes.ChecaCampoVazio(dtNatOper.Rows[linha]["NO_COMP"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtNatOper.Rows[linha]["NO_DESCR"].ToString());
                if (Math.Round(Convert.ToDecimal(dtNatOper.Rows[linha]["NO_TIPO"]), 0).ToString() == "0")
                {
                    rdbEntrada.Checked = true;
                }
                else
                {
                    rdbSaida.Checked = true;
                }

                if (dtNatOper.Rows[linha]["NO_FATURA"].ToString() == "0")
                {
                    chkFatura.Checked = false;
                }
                else
                {
                    chkFatura.Checked = true;
                }

                if (dtNatOper.Rows[linha]["NO_ESTOQUE"].ToString() == "0")
                {
                    chkEstoque.Checked = false;
                }
                else
                {
                    chkEstoque.Checked = true;
                }

                if (dtNatOper.Rows[linha]["NO_RELATORIOS"].ToString() == "0")
                {
                    chkRelatorios.Checked = false;
                }
                else
                {
                    chkRelatorios.Checked = true;
                }

                if (dtNatOper.Rows[linha]["NO_LIBERADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                {
                    chkLiberado.Checked = true;
                }

                txtData.Text = Funcoes.ChecaCampoVazio(dtNatOper.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtNatOper.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, linha));
                txtNoCFiscal.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgNatOper.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgNatOper);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgNatOper.RowCount;
                    for (i = 0; i <= dgNatOper.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgNatOper[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgNatOper.ColumnCount; k++)
                    {
                        if (dgNatOper.Columns[k].Visible == true)
                        {
                            switch (dgNatOper.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgNatOper.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgNatOper.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgNatOper.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgNatOper.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgNatOper.ColumnCount : indice) + Convert.ToString(dgNatOper.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtNoCFiscal.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da natureza de operação?", "Exclusão tabela Natureza de Operação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirNatOper(txtNoCFiscal.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var dados = new NatOperacao();
                            if (!dados.ExcluirDados(Principal.estAtual, txtNoID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("NO_ID", txtNoID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "NAT_OPERACAO", txtDescr.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                                dtNatOper.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgNatOper.DataSource = dtNatOper;
                                tcNatOper.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    chkLiberado.Checked = true;
                    int noId = Funcoes.IdentificaVerificaID("NAT_OPERACAO", "NO_ID", 0, string.Empty, 0);
                    var natOperacao = new NatOperacao(
                        Principal.empAtual,
                        noId,
                        int.Parse(txtNoCFiscal.Text),
                        int.Parse(txtComp.Text),
                        txtDescr.Text,
                        rdbEntrada.Checked ? 0 : 1,
                        chkFatura.Checked == true ? 1 : 0,
                        chkEstoque.Checked == true ? 1 : 0,
                        chkRelatorios.Checked == true ? 1 : 0,
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                    if (natOperacao.InsereRegistros(natOperacao).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("NO_ID", natOperacao.NoId.ToString(), Principal.usuario, "NAT_OPERACAO", natOperacao.NoDescr, Principal.estAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtNatOper.Clear();
                        dgNatOper.DataSource = dtNatOper;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            //if (String.IsNullOrEmpty(txtNoID.Text.Trim()))
            //{
            //    Principal.mensagem = "Nat. Oper. ID não pode ser branco, precione o botão\nInserir para gerar código da natureza de operação.";
            //    Funcoes.Avisa();
            //    txtNoID.Focus();
            //    return false;
            //}
            if (String.IsNullOrEmpty(txtNoCFiscal.Text.Trim()))
            {
                Principal.mensagem = "Código fiscal não pode ser branco.";
                Funcoes.Avisa();
                txtNoCFiscal.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtComp.Text.Trim()))
            {
                Principal.mensagem = "Complemento não pode ser em branco.";
                Funcoes.Avisa();
                txtComp.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    NatOperacao natOper = new NatOperacao
                    {
                        EmpCodigo = Principal.empAtual,
                        NoId = int.Parse(txtNoID.Text),
                        NoCodigo = int.Parse(txtNoCFiscal.Text.Trim()),
                        NoComp = int.Parse(txtComp.Text.Trim()),
                        NoDescr = txtDescr.Text.Trim().ToUpper(),
                        NoTipo = rdbEntrada.Checked == true ? 0 : 1,
                        NoFatura = chkFatura.Checked == true ? 1 : 0,
                        NoEstoque = chkEstoque.Checked == true ? 1 : 0,
                        NoRelatorios = chkRelatorios.Checked == true ? 1 : 0,
                        NoLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    Principal.dtBusca = Util.RegistrosPorEmpresa("NAT_OPERACAO", "NO_ID", txtNoID.Text, true);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        //VERIFICA SE NATUREZA DE OPERAÇÃO JÁ ESTA CADASTRADO//
                        if (Util.RegistrosPorEmpresa("NAT_OPERACAO", "NO_CODIGO", txtNoCFiscal.Text.Trim(), true, true).Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Natureza de operação já cadastrada.";
                            Funcoes.Avisa();
                            txtNoCFiscal.Text = "";
                            txtNoCFiscal.Focus();
                            return false;
                        }

                        if (natOper.InsereRegistros(natOper).Equals(true))
                        {
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("NO_ID", txtNoID.Text, Principal.usuario, "NAT_OPERACAO", txtNoCFiscal.Text, Principal.estAtual, Principal.empAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtNatOper.Clear();
                            dgNatOper.DataSource = dtNatOper;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtDescr.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        if (natOper.AtualizaDados(natOper, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Natureza de Operação");
                        }
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtNatOper.Clear();
                        dgNatOper.DataSource = dtNatOper;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
         
                NatOperacao natOper = new NatOperacao
                {
                    EmpCodigo = Principal.empAtual,
                    NoCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    NoDescr = txtBDescr.Text.Trim().ToUpper(),
                    NoLiberado = cmbLiberado.Text
                };

                dtNatOper = natOper.BuscaDados(natOper, out strOrdem);
                if (dtNatOper.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtNatOper.Rows.Count;
                    dgNatOper.DataSource = dtNatOper;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, 0));
                    dgNatOper.Focus();
                    contRegistros = 0;
                    cmbLiberado.SelectedIndex = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgNatOper.DataSource = dtNatOper;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex =0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtComp_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtDescr.Focus();
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                rdbSaida.Focus();
            }
        }

        private void rdbEntrada_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbEntrada.Checked == true)
            {
                chkFatura.Focus();
            }
        }

        private void rdbSaida_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbSaida.Checked == true)
            {
                chkFatura.Focus();
            }
        }

        private void chkFatura_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFatura.Checked == true)
            {
                chkEstoque.Focus();
            }
        }

        private void chkEstoque_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEstoque.Checked == true)
            {
                chkRelatorios.Focus();
            }
        }

        private void chkRelatorios_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRelatorios.Checked == true)
            {
                chkLiberado.Focus();
            }
        }

        private void dgNatOper_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgNatOper.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtNatOper = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgNatOper.Columns[e.ColumnIndex].Name + " DESC");
                        dgNatOper.DataSource = dtNatOper;
                        decrescente = true;
                    }
                    else
                    {
                        dtNatOper = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgNatOper.Columns[e.ColumnIndex].Name + " ASC");
                        dgNatOper.DataSource = dtNatOper;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtNatOper, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void dgNatOper_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgNatOper.Rows[e.RowIndex].Cells[9].Value.ToString() == "N")
            {
                dgNatOper.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgNatOper.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void txtNoCFiscal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtComp.Focus();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtNoID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgNatOper.ColumnCount; i++)
                {
                    if (dgNatOper.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgNatOper.ColumnCount];
                string[] coluna = new string[dgNatOper.ColumnCount];

                for (int i = 0; i < dgNatOper.ColumnCount; i++)
                {
                    grid[i] = dgNatOper.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgNatOper.ColumnCount; i++)
                {
                    if (dgNatOper.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgNatOper.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgNatOper.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgNatOper.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgNatOper.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgNatOper.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgNatOper.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgNatOper.ColumnCount; i++)
                        {
                            dgNatOper.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Natureza de Operação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcNatOper.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcNatOper.SelectedTab = tpFicha;
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
