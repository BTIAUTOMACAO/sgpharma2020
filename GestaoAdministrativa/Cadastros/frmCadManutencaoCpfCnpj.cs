﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadManutencaoCpfCnpj : Form, Botoes
    {
        private ToolStripButton tsbManutencao = new ToolStripButton("Manutenção Documento");
        private ToolStripSeparator tssManutencao = new ToolStripSeparator();

        public frmCadManutencaoCpfCnpj(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void rdbFisica_CheckedChanged(object sender, EventArgs e)
        {
            if(rdbFisica.Checked)
            {
                txtDocumento.Mask = "000,000,000-00";
                txtDocumento.Focus();
            }
        }

        private void rdbJuridica_CheckedChanged(object sender, EventArgs e)
        {
            if(rdbJuridica.Checked)
            {
                txtDocumento.Mask = "00,000,000/0000-00";
                txtDocumento.Focus();
            }
        }

        private void frmCadManutencaoCpfCnpj_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Limpar()
        {
            rdbFisica.Checked = true;
            txtNome.Text = "";
            txtDocumento.Text = "";
            txtNovoDocumento.Text = "";
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.btnLimpar.Enabled = true;
            rdbFisica.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbManutencao.AutoSize = false;
                this.tsbManutencao.Image = Properties.Resources.cadastro;
                this.tsbManutencao.Size = new System.Drawing.Size(170, 20);
                this.tsbManutencao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssManutencao);
                tsbManutencao.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmCadManutencaoCpfCnpj>().FirstOrDefault();
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnLimpar.Enabled = true;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssManutencao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbFisicaPara_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbFisicaPara.Checked)
            {
                txtNovoDocumento.Mask = "000,000,000-00";
                txtNovoDocumento.Focus();
            }
        }

        private void rdbJuridicaPara_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbJuridicaPara.Checked)
            {
                txtNovoDocumento.Mask = "00,000,000/0000-00";
                txtNovoDocumento.Focus();
            }
        }

        private void txtDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtDocumento.Text)))
                    {
                        MessageBox.Show("Número do Documento não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDocumento.Focus();
                    }
                    else
                    {
                        var cliente = new Negocio.Cliente();
                        string nome = cliente.BuscaNomePorDocto(txtDocumento.Text);
                        if (String.IsNullOrEmpty(nome))
                        {
                            MessageBox.Show("Documento não Cadastrado!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDocumento.Focus();
                        }
                        else
                        {
                            txtNome.Text = nome;
                            rdbFisicaPara.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNovoDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtNovoDocumento.Text)))
                {
                    MessageBox.Show("Número do Documento não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNovoDocumento.Focus();
                }
                else
                    btnAlterar.PerformClick();
            }
        }

        private void rdbFisica_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void rdbJuridica_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDocumento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void rdbFisicaPara_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void rdbJuridicaPara_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtNovoDocumento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAlterar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;
            }
        }

        private void frmCadManutencaoCpfCnpj_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAlterar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                bool existe = false;
                var cliente = new Negocio.Cliente();
                if(!String.IsNullOrEmpty(cliente.BuscaNomePorDocto(txtNovoDocumento.Text)))
                {
                    if (MessageBox.Show("Número do Documento para Alteração já Existe!\nDeseja transferir todo Histórico do Cliente: " + txtNome.Text + "\nPara o Cliente: " +
                        cliente.BuscaNomePorDocto(txtNovoDocumento.Text) + "?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        txtNovoDocumento.Focus();
                        return;
                    }
                    else
                        existe = true;
                }

                BancoDados.AbrirTrans();
                if(!existe)
                {
                    if(BancoDados.ExecuteNoQuery("UPDATE CLIFOR SET CF_DOCTO = '" + txtNovoDocumento.Text + "', CF_TIPO_DOCTO = " + (rdbFisicaPara.Checked ? 0 : 1) + " WHERE CF_DOCTO = '" + txtDocumento.Text + "'",null) == -1)
                    {
                        BancoDados.ErroTrans();
                        return;
                    }
                }
                
                if (BancoDados.ExecuteNoQuery("UPDATE ENTRADA SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE ENTRADA_DETALHES SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE ENTRADA_ITENS SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }
                
                if (BancoDados.ExecuteNoQuery("UPDATE PAGAR SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE PAGAR_DETALHE SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE PAGAR_MOVTO SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE POS SET CF_DOCTO = '" + txtNovoDocumento.Text + "' WHERE CF_DOCTO = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                if (BancoDados.ExecuteNoQuery("UPDATE FP_SOLICITACAO SET CPFCLIENTE = '" + txtNovoDocumento.Text + "' WHERE CPFCLIENTE = '" + txtDocumento.Text + "'", null) == -1)
                {
                    BancoDados.ErroTrans();
                    return;
                }

                BancoDados.FecharTrans();

                MessageBox.Show("Aleração Concluída", "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Limpar();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
