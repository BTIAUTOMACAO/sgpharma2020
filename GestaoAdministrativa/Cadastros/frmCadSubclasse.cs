﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadSubclasse : Form, Botoes
    {
        private DataTable dtSubclasse = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbSubclasse = new ToolStripButton("Subclasses");
        private ToolStripSeparator tssSubclasse = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadSubclasse(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void frmCadSubclasse_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void dgSubclasse_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, contRegistros));
            emGrade = true;
        }

        private void dgSubclasse_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcSubclasse.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgSubclasse_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtSubclasse.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtSubclasse.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtSubclasse.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public void Primeiro()
        {
            if (dtSubclasse.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgSubclasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgSubclasse.CurrentCell = dgSubclasse.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcSubclasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtSubclasse.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgSubclasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgSubclasse.CurrentCell = dgSubclasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgSubclasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgSubclasse.CurrentCell = dgSubclasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgSubclasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgSubclasse.CurrentCell = dgSubclasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadSubClasse");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, contRegistros));
            if (tcSubclasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcSubclasse.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtSubID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbSubclasse.AutoSize = false;
                this.tsbSubclasse.Image = Properties.Resources.cadastro;
                this.tsbSubclasse.Size = new System.Drawing.Size(125, 20);
                this.tsbSubclasse.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbSubclasse);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssSubclasse);
                tsbSubclasse.Click += delegate
                {
                    var cadSubclasse = Application.OpenForms.OfType<frmCadSubclasse>().FirstOrDefault();
                    BotoesHabilitados();
                    cadSubclasse.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtSubclasse.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbSubclasse);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssSubclasse);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtSubclasse.Clear();
            dgSubclasse.DataSource = dtSubclasse;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtSubID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcSubclasse.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtSubID.Text = Funcoes.ChecaCampoVazio(Convert.ToInt32(dtSubclasse.Rows[linha]["SUB_CODIGO"]).ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtSubclasse.Rows[linha]["SUB_DESCR"].ToString());
                if (dtSubclasse.Rows[linha]["SUB_LIBERADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtSubclasse.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtSubclasse.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasse", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcSubclasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcSubclasse.SelectedTab == tpGrade)
            {
                dgSubclasse.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcSubclasse.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadSubclasse");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = true;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgSubclasse.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgSubclasse);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgSubclasse.RowCount;
                    for (i = 0; i <= dgSubclasse.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgSubclasse[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgSubclasse.ColumnCount; k++)
                    {
                        if (dgSubclasse.Columns[k].Visible == true)
                        {
                            switch (dgSubclasse.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgSubclasse.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgSubclasse.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgSubclasse.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgSubclasse.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgSubclasse.ColumnCount : indice) + Convert.ToString(dgSubclasse.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        public bool Excluir()
        {
            try
            {
                if ((txtSubID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da subclasse?", "Exclusão tabela Subclasses", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirSubclasse(txtSubID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            SubClasse sub = new SubClasse();
                            if (sub.ExcluirDados(txtSubID.Text) == true)
                            {
                                Funcoes.GravaLogExclusao("SUB_CODIGO", txtSubID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "SUBCLASSES", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Subclasse excluido com sucesso!", "Subclasse");
                                dtSubclasse.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgSubclasse.DataSource = dtSubclasse;
                                tcSubclasse.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    SubClasse sClasse = new SubClasse();
                    sClasse.EmpCodigo = Principal.empAtual;
                    sClasse.SubCodigo = Funcoes.IdentificaVerificaID("SUBCLASSES", "SUB_CODIGO", 0);
                    sClasse.SubDescr = txtDescr.Text.ToUpper().Trim();
                    sClasse.SubLiberado = chkLiberado.Checked == true ? "S" : "N";
                    sClasse.DtCadastro = DateTime.Now;
                    sClasse.OpCadastro = Principal.usuario.ToString();
                    sClasse.DtAlteracao = DateTime.Now;
                    sClasse.OpAlteracao = Principal.usuario.ToString();


                    Principal.dtPesq = Util.RegistrosPorEmpresa("SUBCLASSES", "SUB_DESCR", txtDescr.Text.ToUpper(), true, true);
                    if (Principal.dtPesq.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Subclasse já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    else if (sClasse.InsereRegistros(sClasse).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("SUB_CODIGO", sClasse.SubCodigo.ToString(), Principal.usuario, "SUBCLASSES", txtDescr.Text, Principal.estAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtSubclasse.Clear();
                        dgSubclasse.DataSource = dtSubclasse;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: ao efetuar o cadastro", "Subclasse", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;
                    }
                }

                chkLiberado.Checked = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    SubClasse sClasse = new SubClasse();

                    sClasse.EmpCodigo = Principal.empAtual;
                    sClasse.SubCodigo = (int)(Convert.ToDouble(txtSubID.Text));
                    sClasse.SubDescr = txtDescr.Text.ToUpper().Trim();
                    sClasse.SubLiberado = chkLiberado.Checked == true ? "N" : "S";
                    sClasse.DtCadastro = DateTime.Now;
                    sClasse.OpCadastro = Principal.usuario.ToString();
                    sClasse.DtAlteracao = DateTime.Now;
                    sClasse.OpAlteracao = Principal.usuario.ToString();

                    Principal.dtBusca = Util.RegistrosPorEmpresa("SUBCLASSES", "SUB_CODIGO", sClasse.SubCodigo.ToString(), true);

                    if (sClasse.AtualizaDados(sClasse, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Subclasses");

                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtSubclasse.Clear();
                        dgSubclasse.DataSource = dtSubclasse;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao tentar atualizar", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;

                    }
                }
                Cursor = Cursors.Default;
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var subClasse = new SubClasse();

                SubClasse sClasse = new SubClasse
                {
                    EmpCodigo = Principal.estAtual,
                    SubCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    SubDescr = txtBDescr.Text.Trim().ToUpper(),
                    SubLiberado = cmbLiberado.Text
                };

                dtSubclasse = subClasse.BuscarDados(sClasse, out strOrdem);
                if (dtSubclasse.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtSubclasse.Rows.Count;
                    dgSubclasse.DataSource = dtSubclasse;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, 0));
                    dgSubclasse.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSubclasse.DataSource = dtSubclasse;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    Funcoes.LimpaFormularios(this);
                    tslRegistros.Text = "";
                    emGrade = false;
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasses", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgSubclasse_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgSubclasse.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtSubclasse = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgSubclasse.Columns[e.ColumnIndex].Name + " DESC");
                        dgSubclasse.DataSource = dtSubclasse;
                        decrescente = true;
                    }
                    else
                    {
                        dtSubclasse = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgSubclasse.Columns[e.ColumnIndex].Name + " ASC");
                        dgSubclasse.DataSource = dtSubclasse;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtSubclasse, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasse", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void dgSubclasse_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgSubclasse.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgSubclasse.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgSubclasse.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtSubID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgSubclasse.ColumnCount; i++)
                {
                    if (dgSubclasse.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgSubclasse.ColumnCount];
                string[] coluna = new string[dgSubclasse.ColumnCount];

                for (int i = 0; i < dgSubclasse.ColumnCount; i++)
                {
                    grid[i] = dgSubclasse.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgSubclasse.ColumnCount; i++)
                {
                    if (dgSubclasse.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgSubclasse.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgSubclasse.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgSubclasse.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgSubclasse.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgSubclasse.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgSubclasse.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgSubclasse.ColumnCount; i++)
                        {
                            dgSubclasse.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Subclasse", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcSubclasse.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcSubclasse.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
