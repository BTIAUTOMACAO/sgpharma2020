﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Geral;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadEntregas : Form, Botoes
    {

        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbEntregas = new ToolStripButton("Taxa de Entrega");
        private ToolStripSeparator tssEntregas = new ToolStripSeparator();
        private DataTable dtEntregas = new DataTable();
        private int contRegistros;
        private string strOrdem;
        private bool emGrade;
        private bool decrescente;
        private string empresa;
        #endregion


        public frmCadEntregas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadEntregas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadEntregas_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        public bool CarregaCombos()
        {
            try
            {
                //CARREGA EMPRESAS CONVENIADAS//
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("CONCAT(CON_CODIGO,' - ',CON_NOME) AS CON_NOME", "CON_CODIGO", "CONVENIADAS", false, "CON_LIBERADO = 'S'");
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    dgEmpresas.DataSource = Principal.dtPesq;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void chkRegra_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRegra.Checked.Equals(true))
            {
                lblEmpresa.Visible = true;
                dgEmpresas.Visible = true;
            }
            else
            {
                lblEmpresa.Visible = false;
                dgEmpresas.Visible = false;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPreco.Focus();
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtIsencao.Focus();
        }

        private void txtIsencao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                chkRegra.Focus();
        }

        private void txtPreco_Validated(object sender, EventArgs e)
        {
            if (txtPreco.Text.Trim() != "")
            {
                txtPreco.Text = String.Format("{0:N}", Convert.ToDecimal(txtPreco.Text));
            }
            else
                txtPreco.Text = "0,00";
        }

        private void txtIsencao_Validated(object sender, EventArgs e)
        {
            if (txtIsencao.Text.Trim() != "")
            {
                txtIsencao.Text = String.Format("{0:N}", Convert.ToDecimal(txtIsencao.Text));
            }
            else
                txtIsencao.Text = "0,00";
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string liberado = "";
                Cursor = Cursors.WaitCursor;

                if (cmbLiberado.Text == "" || cmbLiberado.Text == "TODOS")
                {
                    liberado = "TODOS";
                }
                else
                    if (cmbLiberado.Text == "SIM")
                    {
                        liberado = "S";
                    }
                    else
                        liberado = "N";

                TaxaEntrega tEntrega = new TaxaEntrega
                {
                    EstCodigo = Principal.estAtual,
                    EntrCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    EntrDescr = txtBDescr.Text.Trim().ToUpper(),
                    EntrLiberado = liberado,
                };

                dtEntregas = DAL.DALTaxaEntrega.GetBuscar(tEntrega, out strOrdem);
                if (dtEntregas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtEntregas.Rows.Count;
                    dgEntregas.DataSource = dtEntregas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, 0));
                    dgEntregas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Taza de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEntregas.DataSource = dtEntregas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = -1;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadEntregas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, contRegistros));
            if (tcEntregas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEntregas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEntrID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEntregas.AutoSize = false;
                this.tsbEntregas.Image = Properties.Resources.cadastro;
                this.tsbEntregas.Size = new System.Drawing.Size(125, 20);
                this.tsbEntregas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEntregas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEntregas);
                tsbEntregas.Click += delegate
                {
                    var cadEntregas = Application.OpenForms.OfType<frmCadEntregas>().FirstOrDefault();
                    BotoesHabilitados();
                    cadEntregas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtEntregas.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEntregas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEntregas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcEntregas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEntregas.SelectedTab == tpGrade)
            {
                dgEntregas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEntregas.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadEntregas");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, contRegistros));
                        chkLiberado.Checked = false;
                        emGrade = false;
                        txtPreco.Text = "0,00";
                        txtIsencao.Text = "0,00";
                        txtDescr.Focus();
                    }
                }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                string[] split;
                empresa = "";

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtEntrID.Text = dtEntregas.Rows[linha]["ENTR_CODIGO"].ToString();
                txtDescr.Text = dtEntregas.Rows[linha]["ENTR_DESCR"].ToString();

                txtPreco.Text = String.Format("{0:N}", dtEntregas.Rows[linha]["ENTR_VALOR"]);
                txtIsencao.Text = String.Format("{0:N}", dtEntregas.Rows[linha]["ENTR_ISENCAO"]);

                for (int i = 0; i < dgEmpresas.RowCount; i++)
                {
                    dgEmpresas.Rows[i].Cells[0].Value = false;
                    dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                }

                if (dtEntregas.Rows[linha]["ENTR_REGRA"].Equals("S"))
                {
                    chkRegra.Checked = true;
                    lblEmpresa.Visible = true;
                    dgEmpresas.Visible = true;

                    split = dtEntregas.Rows[linha]["ENTR_EMPRES"].ToString().Split(';');

                    empresa = Funcoes.ChecaCampoVazio(dtEntregas.Rows[linha]["ENTR_EMPRES"].ToString());

                    for (int x = 0; x < split.Length; x++)
                    {
                        if (dgEmpresas.RowCount > 0)
                        {
                            for (int i = 0; i < dgEmpresas.RowCount; i++)
                            {
                                if (Convert.ToString(dgEmpresas.Rows[i].Cells[1].Value).Equals(split[x]))
                                {
                                    dgEmpresas.Rows[i].Cells[0].Value = true;
                                    dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                                }
                            }
                        }
                    }
                }
                else
                {
                    chkRegra.Checked = false;
                    lblEmpresa.Visible = false;
                    dgEmpresas.Visible = false;
                }

                if (dtEntregas.Rows[linha]["ENTR_LIBERADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtEntregas.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEntregas.Rows[linha]["OPALTERACAO"].ToString());
                txtDescr.Focus();

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtEntregas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEntregas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEntregas.CurrentCell = dgEntregas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEntregas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEntregas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntregas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntregas.CurrentCell = dgEntregas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntregas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntregas.CurrentCell = dgEntregas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntregas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntregas.CurrentCell = dgEntregas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void ImprimirRelatorio() { }

        public bool Excluir()
        {
            try
            {
                if ((txtEntrID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da taxa de entrega?", "Exclusão tabela Taxa Entrega", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        if (!DAL.DALTaxaEntrega.GetExcluirDados(Principal.estAtual, txtEntrID.Text).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("ENTR_CODIGO", txtEntrID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "TAXA ENTREGA", txtDescr.Text, Principal.motivo, Principal.estAtual);
                            dtEntregas.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgEntregas.DataSource = dtEntregas;
                            tcEntregas.SelectedTab = tpGrade;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            cmbLiberado.SelectedIndex = -1;
                            txtBID.Focus();
                            return true;
                        }
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Limpar();
                txtEntrID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("TAXA_ENTREGA", "ENTR_CODIGO", Principal.estAtual));
                chkLiberado.Checked = true;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtEntrID.Text.Trim()))
            {
                Principal.mensagem = "Entrega ID não pode ser branco, precione o botão\nInserir para gerar código da entrega.";
                Funcoes.Avisa();
                txtEntrID.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtPreco.Text.Trim()))
            {
                Principal.mensagem = "Necessário informar o Valor.";
                Funcoes.Avisa();
                txtPreco.Focus();
                return false;
            }
            if (Convert.ToDouble(txtPreco.Text) <= 0 )
            {
                Principal.mensagem = "Valor não pode ser zero.";
                Funcoes.Avisa();
                txtPreco.Focus();
                return false;
            }
            if (chkRegra.Checked.Equals(true) && String.IsNullOrEmpty(empresa))
            {
                Principal.mensagem = "Necessário selecionar ao menos uma empresa, para cadastrar a regra.";
                Funcoes.Avisa();
                txtPreco.Focus();
                return false;
            }

            return true;
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    TaxaEntrega tEntrega = new TaxaEntrega
                    {
                        EstCodigo = Principal.estAtual,
                        EntrCodigo = int.Parse(txtEntrID.Text),
                        EntrDescr = txtDescr.Text.ToUpper().Trim(),
                        EntrValor = Convert.ToDecimal(txtPreco.Text),
                        EntrIsencao = Convert.ToDecimal(txtIsencao.Text),
                        EntrRegra = chkRegra.Checked == true ? "S" : "N",
                        EntrEmpres = chkRegra.Checked == true ? empresa : "",
                        EntrLiberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = int.Parse(Principal.usuario)   
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("TAXA_ENTREGA", "ENTR_CODIGO", txtEntrID.Text, true);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        #region INSERE NOVO REGISTRO
                        //VERIFICA SE TAXA DE ENTREGA JA ESTA CADASTRADA//
                        if (Util.RegistrosPorEstabelecimento("TAXA_ENTREGA","ENTR_DESCR",txtDescr.Text.ToUpper().Trim(),true).Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Taxa de Entrega já cadastrada.";
                            Funcoes.Avisa();
                            txtDescr.Text = "";
                            txtDescr.Focus();
                            return false;
                        }

                        if (DAL.DALTaxaEntrega.GetInserirDados(tEntrega).Equals(true))
                        {
                            Funcoes.GravaLogInclusao("ENTR_CODIGO", txtEntrID.Text, Principal.usuario, "TAXA_ENTREGA", txtDescr.Text, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtEntregas.Clear();
                            dgEntregas.DataSource = dtEntregas;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtDescr.Focus();
                            return false;
                        }
                        #endregion
                    }
                    else
                    {
                        #region ATUALIZA INFORMAÇÕES DO REGISTRO
                        if (DAL.DALTaxaEntrega.GetAtualizaDados(tEntrega, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Taxa de Entrega");
                        }
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtEntregas.Clear();
                        dgEntregas.DataSource = dtEntregas;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;

                        #endregion
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public void LimparGrade()
        {
            dtEntregas.Clear();
            dgEntregas.DataSource = dtEntregas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtPreco.Text = "0,00";
            txtIsencao.Text = "0,00";
            lblEmpresa.Visible = false;
            dgEmpresas.Visible = false;
            chkRegra.Checked = false;
            empresa = "";
            CarregaCombos();
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtEntrID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtPreco.Text = "0,00";
            txtIsencao.Text = "0,00";
            lblEmpresa.Visible = false;
            dgEmpresas.Visible = false;
            chkRegra.Checked = false;
            empresa = "";
            CarregaCombos();
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcEntregas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        private void dgEntregas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, contRegistros));
            emGrade = true;
        }

        private void dgEntregas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEntregas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEntregas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEntregas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEntregas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntregas.Columns[e.ColumnIndex].Name + " DESC");
                        dgEntregas.DataSource = dtEntregas;
                        decrescente = true;
                    }
                    else
                    {
                        dtEntregas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntregas.Columns[e.ColumnIndex].Name + " ASC");
                        dgEntregas.DataSource = dtEntregas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntregas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgEntregas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEntregas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEntregas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEntregas.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgEntregas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgEntregas.Rows[e.RowIndex].Cells[7].Value.ToString() == "N")
            {
                dgEntregas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgEntregas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void dgEmpresas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                empresa = "";
                if (dgEmpresas.RowCount > 0)
                {
                    for (int i = 0; i < dgEmpresas.RowCount; i++)
                    {
                        if (Convert.ToString(dgEmpresas.Rows[i].Cells[0].Value).Equals("True"))
                        {
                            empresa += dgEmpresas.Rows[i].Cells[1].Value + ";";
                            dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                        }
                        else
                        {
                            dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEmpresas_BackgroundColorChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgEmpresas.RowCount; i++)
            {
                if (Convert.ToString(dgEmpresas.Rows[i].Cells[0].Value).Equals("True"))
                {
                    dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                }
                else
                {
                    dgEmpresas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                }
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtEntrID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEntregas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEntregas.RowCount;
                    for (i = 0; i <= dgEntregas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEntregas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEntregas.ColumnCount; k++)
                    {
                        if (dgEntregas.Columns[k].Visible == true)
                        {
                            switch (dgEntregas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEntregas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntregas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEntregas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntregas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Valor":
                                    numCel = Principal.GetColunaExcel(dgEntregas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntregas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor para Isenção":
                                     numCel = Principal.GetColunaExcel(dgEntregas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntregas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEntregas.ColumnCount : indice) + Convert.ToString(dgEntregas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEntregas.ColumnCount; i++)
                {
                    if (dgEntregas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEntregas.ColumnCount];
                string[] coluna = new string[dgEntregas.ColumnCount];

                for (int i = 0; i < dgEntregas.ColumnCount; i++)
                {
                    grid[i] = dgEntregas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEntregas.ColumnCount; i++)
                {
                    if (dgEntregas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgEntregas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEntregas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEntregas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEntregas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEntregas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEntregas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEntregas.ColumnCount; i++)
                        {
                            dgEntregas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        public void AtalhoGrade()
        {
            tcEntregas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEntregas.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
