﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadFabricantes : Form, Botoes
    {
        private DataTable dtFabricantes = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbFabricantes = new ToolStripButton("Fabricantes");
        private ToolStripSeparator tssFabricantes = new ToolStripSeparator();
        private string strOrdem;
        private bool decrescente;

        public frmCadFabricantes(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadFabricantes_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtFabricantes.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgFabricantes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgFabricantes.CurrentCell = dgFabricantes.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcFabricantes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtFabricantes.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFabricantes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFabricantes.CurrentCell = dgFabricantes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFabricantes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFabricantes.CurrentCell = dgFabricantes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFabricantes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFabricantes.CurrentCell = dgFabricantes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadFabricantes");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, contRegistros));
            if (tcFabricantes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcFabricantes.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtFabID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbFabricantes.AutoSize = false;
                this.tsbFabricantes.Image = Properties.Resources.cadastro;
                this.tsbFabricantes.Size = new System.Drawing.Size(125, 20);
                this.tsbFabricantes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbFabricantes);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssFabricantes);
                tsbFabricantes.Click += delegate
                {
                    var cadFabricantes = Application.OpenForms.OfType<frmCadFabricantes>().FirstOrDefault();
                    BotoesHabilitados();
                    cadFabricantes.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtFabricantes.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbFabricantes);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssFabricantes);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtFabricantes.Clear();
            dgFabricantes.DataSource = dtFabricantes;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtFabID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcFabricantes.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtFabID.Text = Funcoes.ChecaCampoVazio(dtFabricantes.Rows[linha]["FAB_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtFabricantes.Rows[linha]["FAB_DESCRICAO"].ToString());
                if (dtFabricantes.Rows[linha]["FAB_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtFabricantes.Rows[linha]["FAB_DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtFabricantes.Rows[linha]["FAB_OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcFabricantes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcFabricantes.SelectedTab == tpGrade)
            {
                dgFabricantes.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcFabricantes.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadFabricantes");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = true;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void dgFabricantes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, contRegistros));
            emGrade = true;
        }

        private void dgFabricantes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcFabricantes.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgFabricantes_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtFabricantes.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtFabricantes.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtFabricantes.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgFabricantes.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgFabricantes);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgFabricantes.RowCount;
                    for (i = 0; i <= dgFabricantes.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgFabricantes[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }


                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgFabricantes.ColumnCount; k++)
                    {
                        if (dgFabricantes.Columns[k].Visible == true)
                        {
                            switch (dgFabricantes.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgFabricantes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgFabricantes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgFabricantes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgFabricantes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgFabricantes.ColumnCount : indice) + Convert.ToString(dgFabricantes.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtFabID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do fabricante?", "Exclusão tabela Fabricantes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirFabricantes(txtFabID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            Fabricante fab = new Fabricante();

                            if (fab.ExcluirDados(txtFabID.Text).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("FAB_CODIGO", txtFabID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "FABRICANTES", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Fabricante excluido com sucesso!", "Fabricante");
                                dtFabricantes.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgFabricantes.DataSource = dtFabricantes;
                                tcFabricantes.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    chkLiberado.Checked = true;
                    Fabricante fab = new Fabricante
                    {
                        EmpCodigo = Principal.estAtual,
                        FabCodigo = Funcoes.IdentificaVerificaID("FABRICANTES", "FAB_CODIGO"),
                        FabDescricao = txtDescr.Text.Trim().ToUpper(),
                        FabLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    if (Util.RegistrosPorEstabelecimento("FABRICANTES", "FAB_DESCRICAO", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Fabricante já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else if (fab.InserirDados(fab).Equals(true)) {

                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtFabricantes.Clear();
                        dgFabricantes.DataSource = dtFabricantes;
                        emGrade = false;
                        chkLiberado.Checked = true;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro: ao efetuar o cadastro", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;
                    }
                }
                chkLiberado.Checked = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Fabricante fab = new Fabricante
                    {
                        EmpCodigo = Principal.empAtual,
                        FabCodigo = int.Parse(txtFabID.Text),
                        FabDescricao = txtDescr.Text.Trim().ToUpper(),
                        FabLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("FABRICANTES", "FAB_CODIGO", txtFabID.Text);

                    if (fab.AtualizaDados(fab, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Fabricantes");

                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtFabricantes.Clear();
                        dgFabricantes.DataSource = dtFabricantes;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao tentar atualizar", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Fabricante fab = new Fabricante
                {
                    EmpCodigo = Principal.estAtual,
                    FabCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    FabDescricao = txtBDescr.Text.Trim().ToUpper(),
                    FabLiberado = cmbLiberado.Text
                };

                dtFabricantes = fab.GetBuscar(fab, out strOrdem);
                if (dtFabricantes.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtFabricantes.Rows.Count;
                    dgFabricantes.DataSource = dtFabricantes;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, 0));
                    dgFabricantes.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgFabricantes.DataSource = dtFabricantes;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgFabricantes_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgFabricantes.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtFabricantes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgFabricantes.Columns[e.ColumnIndex].Name + " DESC");
                        dgFabricantes.DataSource = dtFabricantes;
                        decrescente = true;
                    }
                    else
                    {
                        dtFabricantes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgFabricantes.Columns[e.ColumnIndex].Name + " ASC");
                        dgFabricantes.DataSource = dtFabricantes;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFabricantes, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void dgFabricantes_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgFabricantes.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgFabricantes.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgFabricantes.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtFabID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgFabricantes.ColumnCount; i++)
                {
                    if (dgFabricantes.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgFabricantes.ColumnCount];
                string[] coluna = new string[dgFabricantes.ColumnCount];

                for (int i = 0; i < dgFabricantes.ColumnCount; i++)
                {
                    grid[i] = dgFabricantes.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgFabricantes.ColumnCount; i++)
                {
                    if (dgFabricantes.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgFabricantes.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgFabricantes.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgFabricantes.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgFabricantes.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgFabricantes.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgFabricantes.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgFabricantes.ColumnCount; i++)
                        {
                            dgFabricantes.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcFabricantes.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcFabricantes.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
