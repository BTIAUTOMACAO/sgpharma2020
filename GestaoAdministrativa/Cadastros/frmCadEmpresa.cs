﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Geral;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadEmpresa : Form, Botoes
    {
        private DataTable dtEmpresa = new DataTable();
        private string strOrdem;
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbEstab = new ToolStripButton("Empresas");
        private ToolStripSeparator tssEstab = new ToolStripSeparator();
        private bool decrescente;

        public frmCadEmpresa(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadEmpresa_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var empresa = new Empresa();

                int codEmpresa = txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text);
                dtEmpresa = empresa.BuscaDados(Convert.ToInt32(Principal.empAtual), codEmpresa, txtBNome.Text.ToUpper().Trim(), cmbLiberado.Text, out strOrdem);
                if (dtEmpresa.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtEmpresa.Rows.Count;
                    dgEmp.DataSource = dtEmpresa;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, 0));
                    dgEmp.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEmp.DataSource = dtEmpresa;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEmp.SelectedTab == tpGrade)
            {
                dgEmp.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEmp.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadEstabelecimentos");
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;

                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, contRegistros));
                        chkLiberado.Checked = false;
                        emGrade = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        txtCnpj.Focus();
                    }
                }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                txtEmpID.Text = Funcoes.ChecaCampoVazio(Math.Round(Convert.ToDecimal(dtEmpresa.Rows[linha]["EMP_CODIGO"]), 0).ToString());
                txtCnpj.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_CGC"].ToString());
                txtInsEst.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_INSEST"].ToString());
                txtRazao.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_RAZAO"].ToString());
                txtFantasia.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_FANTASIA"].ToString());
                txtEndereco.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_ENDERECO"].ToString());
                txtNumero.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_NUMERO"].ToString());
                txtCidade.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_CIDADE"].ToString());
                cmbUF.SelectedItem = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_UF"].ToString());
                txtTelefone.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_FONE"].ToString());
                if (dtEmpresa.Rows[linha]["EMP_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEmpresa.Rows[linha]["EMP_OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, linha));
                txtCnpj.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtEmpresa.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEmp.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEmp.CurrentCell = dgEmp.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEmp.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEmpresa.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEmp.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEmp.CurrentCell = dgEmp.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEmp.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEmp.CurrentCell = dgEmp.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEmp.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEmp.CurrentCell = dgEmp.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    chkLiberado.Checked = true;

                    var emp = new Empresa(

                       Funcoes.IdentificaVerificaID("EMPRESA", "EMP_CODIGO", 0, string.Empty),
                       txtRazao.Text.Trim().ToUpper(),
                       txtFantasia.Text.Trim().ToUpper(),
                       Funcoes.RemoveCaracter(txtCnpj.Text),
                       txtEndereco.Text.ToUpper(),
                       txtNumero.Text.Trim(),
                       txtCidade.Text.Trim().ToUpper(),
                       cmbUF.Text,
                       Funcoes.RemoveCaracter(txtTelefone.Text.Replace(" ", "")),
                       Funcoes.RemoveCaracter(txtInsEst.Text.Trim()),
                       chkLiberado.Checked == true ? "N" : "S",
                       DateTime.Now,
                       Principal.usuario,
                       DateTime.Now,
                       Principal.usuario

                        );

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("EMPRESA", "EMP_CODIGO", txtEmpID.Text);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        if (Util.RegistrosPorEstabelecimento("EMPRESA", "EMP_CGC", emp.EmpCgc, false, true).Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Empresa já cadastrada.";
                            Funcoes.Avisa();
                            txtCnpj.Text = "";
                            txtCnpj.Focus();
                            return false;
                        }
                        else
                        {

                            if (emp.InserirDados(emp).Equals(true))
                            {
                                //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                                Funcoes.GravaLogInclusao("EMP_CODIGO", Convert.ToString(emp.EmpCodigo), Principal.usuario, "EMPRESA", txtCnpj.Text, Principal.estAtual, Principal.empAtual);
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dtEmpresa.Clear();
                                dgEmp.DataSource = dtEmpresa;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Limpar();
                                return true;
                            }
                        }
                    }
                    else
                    {

                        Cursor = Cursors.Default;
                        Principal.mensagem = "Empresa já cadastrada.";
                        Funcoes.Avisa();
                        txtCnpj.Text = "";
                        txtCnpj.Focus();
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var emp = new Empresa(

                       Convert.ToInt32(txtEmpID.Text),
                       txtRazao.Text.Trim().ToUpper(),
                       txtFantasia.Text.Trim().ToUpper(),
                       Funcoes.RemoveCaracter(txtCnpj.Text),
                       txtEndereco.Text.ToUpper(),
                       txtNumero.Text.Trim(),
                       txtCidade.Text.Trim().ToUpper(),
                       cmbUF.Text,
                       Funcoes.RemoveCaracter(txtTelefone.Text),
                       Funcoes.RemoveCaracter(txtInsEst.Text.Trim()),
                       chkLiberado.Checked == true ? "N" : "S",
                       DateTime.Now,
                       Principal.usuario,
                       DateTime.Now,
                       Principal.usuario

                        );
                    Principal.dtBusca = Util.RegistrosPorEmpresa("EMPRESA", "EMP_CODIGO", txtEmpID.Text, false);
                    if (emp.AtualizaDados(emp, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Empresas");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtEmpresa.Clear();
                        dgEmp.DataSource = dtEmpresa;
                        emGrade = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpj.Text)))
            {
                Principal.mensagem = "CNPJ não pode ser zero.";
                Funcoes.Avisa();
                txtCnpj.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFantasia.Text.Trim()))
            {
                Principal.mensagem = "Razão social não pode ser em branco.";
                Funcoes.Avisa();
                txtRazao.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFantasia.Text.Trim()))
            {
                Principal.mensagem = "Nome fantasia não pode ser em branco.";
                Funcoes.Avisa();
                txtFantasia.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNumero.Text.Trim()))
            {
                Principal.mensagem = "Número não pode ser em branco.";
                Funcoes.Avisa();
                txtNumero.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }

            return true;
        }

        public bool Excluir()
        {
            try
            {

                if ((txtEmpID.Text.Trim() != "") && (Funcoes.RemoveCaracter(txtCnpj.Text) != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da empresa?", "Exclusão tabela Empresas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirEmp(txtEmpID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            var emp = new Empresa();
                            if (!emp.ExcluirDados(txtEmpID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("EMP_CODIGO", txtEmpID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "EMPRESA", txtCnpj.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                                dtEmpresa.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgEmp.DataSource = dtEmpresa;
                                emGrade = false;
                                tcEmp.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;

                            }
                        }
                        else
                        {

                            MessageBox.Show("Existe(m) entrada(s) cadastrada(s) com esta empresa ! Exclusão não permitida!", "Exclusão", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public void LimparGrade()
        {
            dtEmpresa.Clear();
            dgEmp.DataSource = dtEmpresa;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtEmpID.Text = "";
            txtRazao.Text = "";
            txtFantasia.Text = "";
            txtCnpj.Text = "";
            txtEndereco.Text = "";
            txtInsEst.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            cmbUF.SelectedIndex = -1;
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtNumero.Text = "";
            txtCnpj.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcEmp.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
            {
                LimparFicha();
            }

            BotoesHabilitados();
        }

        public void Sair()
        {
            try
            {
                dtEmpresa.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEstab);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEstab);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEmp.ColumnCount; i++)
                {
                    if (dgEmp.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEmp.ColumnCount];
                string[] coluna = new string[dgEmp.ColumnCount];

                for (int i = 0; i < dgEmp.ColumnCount; i++)
                {
                    grid[i] = dgEmp.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEmp.ColumnCount; i++)
                {
                    if (dgEmp.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgEmp.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEmp.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEmp.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEmp.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEmp.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEmp.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEmp.ColumnCount; i++)
                        {
                            dgEmp.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEmp.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEmp);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEmp.RowCount;
                    for (i = 0; i <= dgEmp.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEmp[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEmp.ColumnCount; k++)
                    {
                        if (dgEmp.Columns[k].Visible == true)
                        {
                            switch (dgEmp.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEmp.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEmp.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEmp.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEmp.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEmp.ColumnCount : indice) + Convert.ToString(dgEmp.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public void AtalhoGrade()
        {
            tcEmp.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEmp.SelectedTab = tpFicha;
        }

        private void dgEmp_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, contRegistros));
            emGrade = true;
        }

        private void dgEmp_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEmp.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEmp_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEmp.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEmpresa = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEmp.Columns[e.ColumnIndex].Name + " DESC");
                        dgEmp.DataSource = dtEmpresa;
                        decrescente = true;
                    }
                    else
                    {
                        dtEmpresa = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEmp.Columns[e.ColumnIndex].Name + " ASC");
                        dgEmp.DataSource = dtEmpresa;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgEmp_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEmpresa.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEmpresa.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEmpresa.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgEmp_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgEmp.Rows[e.RowIndex].Cells[10].Value.ToString() == "N")
            {
                dgEmp.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgEmp.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadEmpresa");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEmpresa, contRegistros));
            if (tcEmp.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEmp.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEmpID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEstab.AutoSize = false;
                this.tsbEstab.Image = Properties.Resources.cadastro;
                this.tsbEstab.Size = new System.Drawing.Size(130, 20);
                this.tsbEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEstab);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEstab);
                tsbEstab.Click += delegate
                {
                    var cadEmp = Application.OpenForms.OfType<frmCadEmpresa>().FirstOrDefault();
                    BotoesHabilitados();
                    cadEmp.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void txtCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtInsEst.Focus();
            }
        }

        private void txtCnpj_Validated(object sender, EventArgs e)
        {
            if (!Funcoes.RemoveCaracter(txtCnpj.Text).Equals(""))
            {
                if (Funcoes.ValidaCnpj(txtCnpj.Text).Equals(false))
                {
                    MessageBox.Show("CNPJ incorreto! Digite novamente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpj.Focus();
                }
            }
        }

        private void txtInsEst_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtRazao.Focus();
            }
        }

        private void txtRazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtFantasia.Focus();
            }
        }

        private void txtFantasia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtEndereco.Focus();
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtNumero.Focus();
            }
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtCidade.Focus();
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbUF.Focus();
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtTelefone.Focus();
            }
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtEmpID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
