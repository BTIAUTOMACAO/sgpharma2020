﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadEstabelecimentos : Form, Botoes
    {
        private DataTable dtEstab = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbEstab = new ToolStripButton("Estabelecimentos");
        private ToolStripSeparator tssEstab = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadEstabelecimentos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadEstabelecimentos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
                CarregaCombos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtEstab.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEstab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEstab.CurrentCell = dgEstab.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEstab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEstab.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEstab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEstab.CurrentCell = dgEstab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEstab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEstab.CurrentCell = dgEstab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEstab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEstab.CurrentCell = dgEstab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    txtEstID.Text = Funcoes.IdentificaVerificaID("ESTABELECIMENTOS", "EST_CODIGO").ToString();
                    chkLiberado.Checked = true;
                    var estabelecimentos = new Estabelecimento(
                            Convert.ToInt32(txtEstID.Text),
                            Convert.ToInt32(cmbEmpresa.SelectedValue),
                            txtCnpj.Text,
                            txtInsEst.Text,
                            txtRazao.Text,
                            txtFantasia.Text,
                            txtEndereco.Text,
                            Convert.ToInt32(txtNumero.Text),
                            txtBairro.Text,
                            txtCidade.Text,
                            txtCep.Text,
                            cmbUF.Text,
                            txtTelefone.Text,
                            txtContato.Text,
                            txtInscMunicipal.Text,
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario
                        );

                    if (Util.RegistrosPorEmpresa("ESTABELECIMENTOS", "EST_CGC", txtCnpj.Text, true, true).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Estabelecimento já cadastrado.";
                        Funcoes.Avisa();
                        txtCnpj.Text = "";
                        txtCnpj.Focus();
                        return false;
                    }

                    if (estabelecimentos.InsereRegistros(estabelecimentos).Equals(true))
                    {
                        Funcoes.GravaLogInclusao("EST_CODIGO", txtEstID.Text, Principal.usuario, "ESTABELECIMENTOS", txtCnpj.Text, Convert.ToInt32(txtEstID.Text), Convert.ToInt32(cmbEmpresa.SelectedValue));
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtEstab.Clear();
                        dgEstab.DataSource = dtEstab;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtCnpj.Focus();
                        return false;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var estabelecimentos = new Estabelecimento(
                            Convert.ToInt32(txtEstID.Text),
                            Convert.ToInt32(cmbEmpresa.SelectedValue),
                            txtCnpj.Text,
                            txtInsEst.Text,
                            txtRazao.Text,
                            txtFantasia.Text,
                            txtEndereco.Text,
                            Convert.ToInt32(txtNumero.Text),
                            txtBairro.Text,
                            txtCidade.Text,
                            Funcoes.RemoveCaracter(txtCep.Text),
                            cmbUF.Text,
                            Funcoes.RemoveCaracter(txtTelefone.Text),
                            txtContato.Text,
                            txtInscMunicipal.Text,
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario
                        );


                    Principal.dtBusca = Util.RegistrosPorEmpresa("ESTABELECIMENTOS", "EST_CODIGO", txtEstID.Text, true);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Estabelecimento não cadastrado.";
                        Funcoes.Avisa();
                        txtCnpj.Text = "";
                        txtCnpj.Focus();
                        return false;
                    }
                    else
                    {
                        if (estabelecimentos.AtualizaDados(estabelecimentos, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Estabelecimentos");
                        }
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtEstab.Clear();
                        dgEstab.DataSource = dtEstab;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {

                if ((txtEstID.Text.Trim() != "") && (Funcoes.RemoveCaracter(txtCnpj.Text) != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do estabelecimento?", "Exclusão tabela Estabelecimentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirEstab(txtEstID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var estabelecimento = new Estabelecimento();
                        if (!estabelecimento.ExcluirDados(txtEstID.Text, cmbEmpresa.SelectedValue.ToString()).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("EST_CODIGO", txtEstID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "ESTABELECIMENTOS", txtCnpj.Text, Principal.motivo, Convert.ToInt32(txtEstID.Text), Convert.ToInt32(cmbEmpresa.SelectedValue));
                            dtEstab.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgEstab.DataSource = dtEstab;
                            emGrade = false;
                            tcEstab.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                            txtBID.Focus();
                            return true;

                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public void LimparGrade()
        {
            dtEstab.Clear();
            dgEstab.DataSource = dtEstab;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtEstID.Text = "";
            txtRazao.Text = "";
            txtFantasia.Text = "";
            txtCnpj.Text = "";
            txtEndereco.Text = "";
            txtInsEst.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            txtContato.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtNumero.Text = "";
            cmbEmpresa.SelectedIndex = -1;
            txtInscMunicipal.Text = "";
            cmbLiberado.SelectedIndex = 0;
            cmbEmpresa.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcEstab.SelectedTab == tpGrade)
            { 
                LimparGrade();
            }
            else
            {
                LimparFicha();
            }

            BotoesHabilitados();

        }

        public void Sair()
        {
            try
            {
                dtEstab.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEstab);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEstab);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEstab_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, contRegistros));
            emGrade = true;
        }

        private void dgEstab_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEstab.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEstab_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEstab.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEstab.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEstab.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                txtEstID.Text = Funcoes.ChecaCampoVazio(Math.Round(Convert.ToDecimal(dtEstab.Rows[linha]["EST_CODIGO"]), 0).ToString());
                cmbEmpresa.SelectedValue = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EMP_CODIGO"].ToString());
                txtCnpj.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_CGC"].ToString());
                txtInsEst.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_INSEST"].ToString());
                txtRazao.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_RAZAO"].ToString());
                txtFantasia.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_FANTASIA"].ToString());
                txtEndereco.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_ENDERECO"].ToString());
                txtNumero.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_END_NUM"].ToString());
                txtBairro.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_BAIRRO"].ToString());
                txtCidade.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_CIDADE"].ToString());
                txtCep.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_CEP"].ToString());
                cmbUF.SelectedItem = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_UF"].ToString());
                txtTelefone.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_FONE"].ToString());
                txtContato.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_CONTATO"].ToString());
                txtInscMunicipal.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["EST_INSMUN"].ToString());
                if (dtEstab.Rows[linha]["EST_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEstab.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, linha));
                cmbEmpresa.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtEstID.Focus();
            }
        }

        private void txtEstID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCnpj.Focus();
            }
        }

        private void txtCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtInsEst.Focus();
            }
        }

        private void txtInsEst_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtRazao.Focus();
            }
        }

        private void txtRazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtFantasia.Focus();
            }
        }

        private void txtFantasia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    txtCep.Focus();
                }
                else
                    txtEndereco.Focus();
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtNumero.Focus();
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCidade.Focus();
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCep.Focus();
            }
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {
                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.ToUpper();
                        txtCidade.Text = end.localidade.ToUpper();
                        cmbUF.Text = end.uf;
                        txtNumero.Focus();
                    }

                    Cursor = Cursors.Default;
                }
                else
                {
                    cmbUF.Focus();
                }
            }
        }


        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {
                throw;
            }
        }
        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtTelefone.Focus();
            }
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtContato.Focus();
            }
        }

        private void txtContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtInscMunicipal.Focus();
            }
        }

        private void tcEstab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEstab.SelectedTab == tpGrade)
            {
                dgEstab.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEstab.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadEstabelecimentos");
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, contRegistros));
                        chkLiberado.Checked = false;
                        emGrade = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        cmbEmpresa.Focus();
                    }
                }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadEstabelecimentos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, contRegistros));
            if (tcEstab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEstab.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEstID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEstab.AutoSize = false;
                this.tsbEstab.Image = Properties.Resources.cadastro;
                this.tsbEstab.Size = new System.Drawing.Size(130, 20);
                this.tsbEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEstab);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEstab);
                tsbEstab.Click += delegate
                {
                    var cadEstab = Application.OpenForms.OfType<frmCadEstabelecimentos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadEstab.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (cmbEmpresa.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar uma Empresa.";
                Funcoes.Avisa();
                cmbEmpresa.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpj.Text)))
            {
                Principal.mensagem = "CNPJ não pode ser zero.";
                Funcoes.Avisa();
                txtCnpj.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFantasia.Text.Trim()))
            {
                Principal.mensagem = "Nome fantasia não pode ser em branco.";
                Funcoes.Avisa();
                txtFantasia.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFantasia.Text.Trim()))
            {
                Principal.mensagem = "Razão social não pode ser em branco.";
                Funcoes.Avisa();
                txtRazao.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCep.Text.Replace("-", "").Trim()))
            {
                Principal.mensagem = "Cep não pode ser em branco.";
                Funcoes.Avisa();
                txtCep.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }

            return true;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEstab.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEstab);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEstab.RowCount;
                    for (i = 0; i <= dgEstab.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEstab[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEstab.ColumnCount; k++)
                    {
                        if (dgEstab.Columns[k].Visible == true)
                        {
                            switch (dgEstab.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEstab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEstab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEstab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEstab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEstab.ColumnCount : indice) + Convert.ToString(dgEstab.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var estabelecimento = new Estabelecimento();

                dtEstab = estabelecimento.BuscaDados(Principal.empAtual, txtBID.Text, txtBNome.Text.ToUpper().Trim(), cmbLiberado.Text, out strOrdem);
                if (dtEstab.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtEstab.Rows.Count;
                    dgEstab.DataSource = dtEstab;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, 0));
                    dgEstab.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEstab.DataSource = dtEstab;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgEstab_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEstab.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEstab = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEstab.Columns[e.ColumnIndex].Name + " DESC");
                        dgEstab.DataSource = dtEstab;
                        decrescente = true;
                    }
                    else
                    {
                        dtEstab = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEstab.Columns[e.ColumnIndex].Name + " ASC");
                        dgEstab.DataSource = dtEstab;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEstab, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void txtCnpj_Validated(object sender, EventArgs e)
        {
            //if (!Funcoes.GetRemoverCaracter(txtCnpj.Text).Equals(""))
            //{
            //    if (Funcoes.GetValidaCnpj(txtCnpj.Text).Equals(false))
            //    {
            //        MessageBox.Show("CNPJ incorreto! Digite novamente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        txtCnpj.Focus();
            //    }
            //}
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void dgEstab_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgEstab.Rows[e.RowIndex].Cells[13].Value.ToString() == "N")
            {
                dgEstab.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgEstab.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtEstID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEstab.ColumnCount; i++)
                {
                    if (dgEstab.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEstab.ColumnCount];
                string[] coluna = new string[dgEstab.ColumnCount];

                for (int i = 0; i < dgEstab.ColumnCount; i++)
                {
                    grid[i] = dgEstab.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEstab.ColumnCount; i++)
                {
                    if (dgEstab.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgEstab.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEstab.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEstab.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEstab.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEstab.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEstab.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEstab.ColumnCount; i++)
                        {
                            dgEstab.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcEstab.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEstab.SelectedTab = tpFicha;
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox11, "Campo Obrigatório");
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    txtBairro.Focus();
                }
                else
                {
                    cmbUF.Focus();
                }
            }
        }

        public bool CarregaCombos()
        {
            try
            {

                DataTable dtPesq = new DataTable();

                //CARREGA UNIDADES//
                dtPesq = Util.CarregarCombosPorEstabelecimento("EMP_CODIGO || ' - ' || EMP_RAZAO AS EMP_RAZAO", "EMP_CODIGO", "EMPRESA", false, "EMP_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma empresa,\npara cadastrar um estabelecimento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbEmpresa.DataSource = dtPesq;
                    cmbEmpresa.DisplayMember = "EMP_RAZAO";
                    cmbEmpresa.ValueMember = "EMP_CODIGO";
                    cmbEmpresa.SelectedIndex = -1;
                }

                return true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void cmbEmpresa_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCnpj.Focus();
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtInscMunicipal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox13, "Campo Obrigatório");
        }
    }
}
