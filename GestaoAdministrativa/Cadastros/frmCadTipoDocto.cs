﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadTipoDocto : Form, Botoes
    {
        private DataTable dtTipoDocto = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTipoDocto = new ToolStripButton("Tipo de Documento");
        private ToolStripSeparator tssTipoDocto = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadTipoDocto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadTipoDocto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcTipoDocto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTipoDocto.SelectedTab == tpGrade)
            {
                dgTipoDocto.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTipoDocto.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadTipoDocto");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, contRegistros));
                        chkLiberado.Checked = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void dgTipoDocto_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, contRegistros));
            emGrade = true;
        }

        private void dgTipoDocto_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTipoDocto.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTipoDocto_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTipoDocto.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTipoDocto.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTipoDocto.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public void Primeiro()
        {
            if (dtTipoDocto.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTipoDocto.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTipoDocto.CurrentCell = dgTipoDocto.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTipoDocto.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTipoDocto.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoDocto.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoDocto.CurrentCell = dgTipoDocto.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoDocto.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoDocto.CurrentCell = dgTipoDocto.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoDocto.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoDocto.CurrentCell = dgTipoDocto.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadTipoDocto");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, contRegistros));
            if (tcTipoDocto.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTipoDocto.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtTipID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbTipoDocto.AutoSize = false;
                this.tsbTipoDocto.Image = Properties.Resources.cadastro;
                this.tsbTipoDocto.Size = new System.Drawing.Size(160, 20);
                this.tsbTipoDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTipoDocto);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTipoDocto);
                tsbTipoDocto.Click += delegate
                {
                    var cadTipoDocto = Application.OpenForms.OfType<frmCadTipoDocto>().FirstOrDefault();
                    BotoesHabilitados();
                    cadTipoDocto.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtTipoDocto.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTipoDocto);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTipoDocto);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTipoDocto.Clear();
            dgTipoDocto.DataSource = dtTipoDocto;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtTipID.Text = "";
            txtDescrAbr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            chkFatura.Checked = false;
            txtDescr.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTipoDocto.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtDescrAbr.Focus();
            }
        }

        private void txtDescrAbr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkFatura.Focus();
            }
        }

        private void chkFatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkEmiteNF.Focus();
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtTipID.Text = Funcoes.ChecaCampoVazio(Math.Round(Convert.ToDecimal(dtTipoDocto.Rows[linha]["TIP_CODIGO"]), 0).ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtTipoDocto.Rows[linha]["TIP_DESCRICAO"].ToString());
                txtDescrAbr.Text = Funcoes.ChecaCampoVazio(dtTipoDocto.Rows[linha]["TIP_DESC_ABREV"].ToString());

                if (dtTipoDocto.Rows[linha]["TIP_FATURA"].ToString() == "N")
                {
                    chkFatura.Checked = false;
                }
                else
                {
                    chkFatura.Checked = true;
                }

                if (dtTipoDocto.Rows[linha]["TIP_EMITE_NF"].ToString() == "N")
                {
                    chkEmiteNF.Checked = false;
                }
                else
                {
                    chkEmiteNF.Checked = true;
                }

                if (dtTipoDocto.Rows[linha]["TIP_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                {
                    chkLiberado.Checked = true;
                }

                txtData.Text = Funcoes.ChecaCampoVazio(dtTipoDocto.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTipoDocto.Rows[linha]["TIP_OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTipoDocto.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTipoDocto);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTipoDocto.RowCount;
                    for (i = 0; i <= dgTipoDocto.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTipoDocto[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTipoDocto.ColumnCount; k++)
                    {
                        if (dgTipoDocto.Columns[k].Visible == true)
                        {
                            switch (dgTipoDocto.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTipoDocto.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTipoDocto.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTipoDocto.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTipoDocto.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTipoDocto.ColumnCount : indice) + Convert.ToString(dgTipoDocto.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtTipID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do tipo de documento?", "Exclusão tabela Tipo de Documento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirTipoDocto(txtTipID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var tipDoc = new TipoDocto();
                            if (tipDoc.ExcluirDados(txtTipID.Text).Equals(true))
                            //if (!DAL.DALTipoDocto.GetExcluirDados(txtTipID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("TIP_CODIGO", txtTipID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "TIPO_DOCTO", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Tipo Documento excluido com sucesso!", "Tipo Documento");
                                dtTipoDocto.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTipoDocto.DataSource = dtTipoDocto;
                                tcTipoDocto.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    
                    if (Util.RegistrosPorEstabelecimento("TIPO_DOCTO", "TIP_DESCRICAO", txtDescr.Text.Trim()).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Tipo Documento já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }else{

                        TipoDocto tDocto = new TipoDocto
                        {
                            TipCodigo = Funcoes.IdentificaVerificaID("TIPO_DOCTO", "TIP_CODIGO"),
                            TipDescr = txtDescr.Text.ToUpper().Trim(),
                            TipoDescAbrev = txtDescrAbr.Text.ToUpper().Trim(),
                            TipFatura = chkFatura.Checked == true ? "S" : "N",
                            TipEmiteNF = chkEmiteNF.Checked == true ? "S" : "N",
                            TipLiberado = chkLiberado.Checked == true ? "N" : "S",
                            OpCadastro = Principal.usuario.ToUpper(),
                        };

                        if (tDocto.InserirDados(tDocto).Equals(true))
                        {
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtTipoDocto.Clear();
                            dgTipoDocto.DataSource = dtTipoDocto;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;

                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Erro ao efetuar o cadastro", "Tipo Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Funcoes.Avisa();
                            txtDescr.Focus();
                            return false;

                        }

                    }

                }



                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescrAbr.Text.Trim()))
            {
                Principal.mensagem = "Descr. abrev. não pode ser em branco.";
                Funcoes.Avisa();
                txtDescrAbr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    TipoDocto tDocto = new TipoDocto
                    {
                        TipCodigo = int.Parse(txtTipID.Text),
                        TipDescr = txtDescr.Text.ToUpper().Trim(),
                        TipoDescAbrev = txtDescrAbr.Text.ToUpper().Trim(),
                        TipFatura = chkFatura.Checked == true ? "S" : "N",
                        TipEmiteNF = chkEmiteNF.Checked == true ? "S" : "N",
                        TipLiberado = chkLiberado.Checked == true ? "N" : "S",
                        OpAlteracao = Principal.usuario.ToUpper(),

                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("TIPO_DOCTO", "TIP_CODIGO", txtTipID.Text);
                    if (tDocto.AtualizaDados(tDocto, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Tipo de Documento");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTipoDocto.Clear();
                        dgTipoDocto.DataSource = dtTipoDocto;
                        emGrade = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro ao tentar afetuar a atualização", "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                
                TipoDocto tDocto = new TipoDocto
                {
                    TipCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    TipDescr = txtBDescr.Text.Trim(),
                    TipLiberado = cmbLiberado.Text
                };
                dtTipoDocto = tDocto.BuscaDados(tDocto, out strOrdem);
                if (dtTipoDocto.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTipoDocto.Rows.Count;
                    dgTipoDocto.DataSource = dtTipoDocto;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, 0));
                    dgTipoDocto.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTipoDocto.DataSource = dtTipoDocto;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgTipoDocto_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTipoDocto.Rows.Count > 0)
                {
                    Cursor = Cursors.Default;
                    if (decrescente == false)
                    {
                        dtTipoDocto = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTipoDocto.Columns[e.ColumnIndex].Name + " DESC");
                        dgTipoDocto.DataSource = dtTipoDocto;
                        decrescente = true;
                    }
                    else
                    {
                        dtTipoDocto = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTipoDocto.Columns[e.ColumnIndex].Name + " ASC");
                        dgTipoDocto.DataSource = dtTipoDocto;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoDocto, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void dgTipoDocto_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTipoDocto.Rows[e.RowIndex].Cells[5].Value.ToString() == "N")
            {
                dgTipoDocto.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTipoDocto.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtTipID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTipoDocto.ColumnCount; i++)
                {
                    if (dgTipoDocto.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTipoDocto.ColumnCount];
                string[] coluna = new string[dgTipoDocto.ColumnCount];

                for (int i = 0; i < dgTipoDocto.ColumnCount; i++)
                {
                    grid[i] = dgTipoDocto.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTipoDocto.ColumnCount; i++)
                {
                    if (dgTipoDocto.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTipoDocto.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTipoDocto.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTipoDocto.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTipoDocto.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTipoDocto.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTipoDocto.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTipoDocto.ColumnCount; i++)
                        {
                            dgTipoDocto.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTipoDocto.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTipoDocto.SelectedTab = tpFicha;
        }

        private void chkEmiteNF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
