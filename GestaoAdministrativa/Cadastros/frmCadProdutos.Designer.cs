﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadProdutos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcProdutos = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBReg = new System.Windows.Forms.TextBox();
            this.txtBCod = new System.Windows.Forms.TextBox();
            this.txtBDescr = new System.Windows.Forms.TextBox();
            this.cmbBPrinc = new System.Windows.Forms.ComboBox();
            this.cmbBFab = new System.Windows.Forms.ComboBox();
            this.cmbBSub = new System.Windows.Forms.ComboBox();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cmbBClasse = new System.Windows.Forms.ComboBox();
            this.cmbBDepto = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_ABREV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_UNIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_USO_CONTINUO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_FORMULA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CONTROLADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NCM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_SITUACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_MENSAGEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsProdutos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDescricaoCartelado = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.lblCartelado = new System.Windows.Forms.Label();
            this.txtCodCartelado = new System.Windows.Forms.TextBox();
            this.chkCaixaCartelado = new System.Windows.Forms.CheckBox();
            this.txtCodABC = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cmbTabPrecos = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtNCM = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.cmbPrinAtivo = new System.Windows.Forms.ComboBox();
            this.txtPrincID = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbEspec = new System.Windows.Forms.ComboBox();
            this.txtEspcID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.cmbFabricante = new System.Windows.Forms.ComboBox();
            this.txtFabID = new System.Windows.Forms.TextBox();
            this.cmbSubClas = new System.Windows.Forms.ComboBox();
            this.txtSubID = new System.Windows.Forms.TextBox();
            this.cmbClasse = new System.Windows.Forms.ComboBox();
            this.txtClasID = new System.Windows.Forms.TextBox();
            this.cmbDepto = new System.Windows.Forms.ComboBox();
            this.txtDeptoID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbUnidade = new System.Windows.Forms.ComboBox();
            this.txtDescrAbr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodBarra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProdID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpComplemento = new System.Windows.Forms.TabPage();
            this.tcComplemento = new System.Windows.Forms.TabControl();
            this.tpGeral = new System.Windows.Forms.TabPage();
            this.cmbPortaria = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txtCfopDev = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.cmbCst = new System.Windows.Forms.ComboBox();
            this.label89 = new System.Windows.Forms.Label();
            this.txtEnqIpi = new System.Windows.Forms.TextBox();
            this.txtCstPis = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtCstCofins = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtCstIpi = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtCsosn = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.cmbIcms = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtCest = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtCfop = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.cmbTipoReceita = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chkManipulado = new System.Windows.Forms.CheckBox();
            this.chkControlado = new System.Windows.Forms.CheckBox();
            this.ckbUsoContinuo = new System.Windows.Forms.CheckBox();
            this.chkBCompra = new System.Windows.Forms.CheckBox();
            this.txtRegMS = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.cmbClasTera = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.tpPrecos = new System.Windows.Forms.TabPage();
            this.lblAlterado = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.dgUltimasCompras = new System.Windows.Forms.DataGridView();
            this.ENT_DTLANC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtDcb = new System.Windows.Forms.TextBox();
            this.txtQtdUniFP = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.cmbLista = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtEcf = new System.Windows.Forms.TextBox();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbUComp = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.chkBloqDesconto = new System.Windows.Forms.CheckBox();
            this.dtCInicial = new System.Windows.Forms.DateTimePicker();
            this.label61 = new System.Windows.Forms.Label();
            this.txtDtVenda = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtPVenda = new System.Windows.Forms.TextBox();
            this.txtComissao = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCompra = new System.Windows.Forms.TextBox();
            this.txtCusUlt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPmc = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCusIni = new System.Windows.Forms.TextBox();
            this.txtCusMed = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtEstMin = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtEstIni = new System.Windows.Forms.TextBox();
            this.txtEstAtu = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEstMax = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcProdutos.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.cmsProdutos.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.tpComplemento.SuspendLayout();
            this.tcComplemento.SuspendLayout();
            this.tpGeral.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tpPrecos.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUltimasCompras)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcProdutos);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(163, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Produtos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcProdutos
            // 
            this.tcProdutos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcProdutos.Controls.Add(this.tpGrade);
            this.tcProdutos.Controls.Add(this.tpFicha);
            this.tcProdutos.Controls.Add(this.tpComplemento);
            this.tcProdutos.Location = new System.Drawing.Point(6, 33);
            this.tcProdutos.Name = "tcProdutos";
            this.tcProdutos.SelectedIndex = 0;
            this.tcProdutos.Size = new System.Drawing.Size(965, 499);
            this.tcProdutos.TabIndex = 40;
            this.tcProdutos.SelectedIndexChanged += new System.EventHandler(this.tcProdutos_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBReg);
            this.tpGrade.Controls.Add(this.txtBCod);
            this.tpGrade.Controls.Add(this.txtBDescr);
            this.tpGrade.Controls.Add(this.cmbBPrinc);
            this.tpGrade.Controls.Add(this.cmbBFab);
            this.tpGrade.Controls.Add(this.cmbBSub);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.label43);
            this.tpGrade.Controls.Add(this.cmbBClasse);
            this.tpGrade.Controls.Add(this.cmbBDepto);
            this.tpGrade.Controls.Add(this.label42);
            this.tpGrade.Controls.Add(this.label41);
            this.tpGrade.Controls.Add(this.label40);
            this.tpGrade.Controls.Add(this.label39);
            this.tpGrade.Controls.Add(this.label38);
            this.tpGrade.Controls.Add(this.label37);
            this.tpGrade.Controls.Add(this.label36);
            this.tpGrade.Controls.Add(this.label35);
            this.tpGrade.Controls.Add(this.dgProdutos);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(848, 425);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(103, 38);
            this.btnBuscar.TabIndex = 109;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBReg
            // 
            this.txtBReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBReg.Location = new System.Drawing.Point(641, 441);
            this.txtBReg.MaxLength = 20;
            this.txtBReg.Name = "txtBReg";
            this.txtBReg.Size = new System.Drawing.Size(201, 22);
            this.txtBReg.TabIndex = 108;
            this.txtBReg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBReg_KeyPress);
            // 
            // txtBCod
            // 
            this.txtBCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCod.Location = new System.Drawing.Point(250, 394);
            this.txtBCod.MaxLength = 20;
            this.txtBCod.Name = "txtBCod";
            this.txtBCod.Size = new System.Drawing.Size(164, 22);
            this.txtBCod.TabIndex = 96;
            this.txtBCod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCod_KeyPress);
            // 
            // txtBDescr
            // 
            this.txtBDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBDescr.Location = new System.Drawing.Point(6, 394);
            this.txtBDescr.MaxLength = 50;
            this.txtBDescr.Name = "txtBDescr";
            this.txtBDescr.Size = new System.Drawing.Size(233, 22);
            this.txtBDescr.TabIndex = 95;
            this.txtBDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBDescr_KeyPress);
            // 
            // cmbBPrinc
            // 
            this.cmbBPrinc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBPrinc.FormattingEnabled = true;
            this.cmbBPrinc.Items.AddRange(new object[] {
            "SIM",
            "NAO",
            "TODOS"});
            this.cmbBPrinc.Location = new System.Drawing.Point(425, 439);
            this.cmbBPrinc.Name = "cmbBPrinc";
            this.cmbBPrinc.Size = new System.Drawing.Size(205, 24);
            this.cmbBPrinc.TabIndex = 107;
            this.cmbBPrinc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBPrinc_KeyPress);
            // 
            // cmbBFab
            // 
            this.cmbBFab.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBFab.FormattingEnabled = true;
            this.cmbBFab.Items.AddRange(new object[] {
            "SIM",
            "NAO",
            "TODOS"});
            this.cmbBFab.Location = new System.Drawing.Point(188, 439);
            this.cmbBFab.Name = "cmbBFab";
            this.cmbBFab.Size = new System.Drawing.Size(226, 24);
            this.cmbBFab.TabIndex = 106;
            this.cmbBFab.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBFab_KeyPress);
            // 
            // cmbBSub
            // 
            this.cmbBSub.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBSub.FormattingEnabled = true;
            this.cmbBSub.Items.AddRange(new object[] {
            "SIM",
            "NAO",
            "TODOS"});
            this.cmbBSub.Location = new System.Drawing.Point(6, 439);
            this.cmbBSub.Name = "cmbBSub";
            this.cmbBSub.Size = new System.Drawing.Size(167, 24);
            this.cmbBSub.TabIndex = 105;
            this.cmbBSub.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBSub_KeyPress);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(848, 392);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(103, 24);
            this.cmbLiberado.TabIndex = 104;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(845, 373);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 16);
            this.label43.TabIndex = 103;
            this.label43.Text = "Liberado";
            // 
            // cmbBClasse
            // 
            this.cmbBClasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBClasse.FormattingEnabled = true;
            this.cmbBClasse.Items.AddRange(new object[] {
            "SIM",
            "NAO",
            "TODOS"});
            this.cmbBClasse.Location = new System.Drawing.Point(641, 392);
            this.cmbBClasse.Name = "cmbBClasse";
            this.cmbBClasse.Size = new System.Drawing.Size(201, 24);
            this.cmbBClasse.TabIndex = 98;
            this.cmbBClasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBClasse_KeyPress);
            // 
            // cmbBDepto
            // 
            this.cmbBDepto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBDepto.FormattingEnabled = true;
            this.cmbBDepto.Location = new System.Drawing.Point(425, 392);
            this.cmbBDepto.Name = "cmbBDepto";
            this.cmbBDepto.Size = new System.Drawing.Size(205, 24);
            this.cmbBDepto.TabIndex = 97;
            this.cmbBDepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBDepto_KeyPress);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(638, 419);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(188, 16);
            this.label42.TabIndex = 94;
            this.label42.Text = "Reg. no Ministério da Saúde";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(422, 419);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(101, 16);
            this.label41.TabIndex = 93;
            this.label41.Text = "Princípio Ativo";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(185, 418);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 16);
            this.label40.TabIndex = 92;
            this.label40.Text = "Fabricante";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(3, 418);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(72, 16);
            this.label39.TabIndex = 91;
            this.label39.Text = "Subclasse";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(638, 373);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(49, 16);
            this.label38.TabIndex = 90;
            this.label38.Text = "Classe";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(422, 376);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(98, 16);
            this.label37.TabIndex = 89;
            this.label37.Text = "Departamento";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(247, 376);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(76, 16);
            this.label36.TabIndex = 88;
            this.label36.Text = "Cód. Barra";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(3, 376);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(70, 16);
            this.label35.TabIndex = 87;
            this.label35.Text = "Descrição";
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            this.dgProdutos.AllowUserToOrderColumns = true;
            this.dgProdutos.AllowUserToResizeColumns = false;
            this.dgProdutos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProdutos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EST_CODIGO,
            this.EMP_CODIGO,
            this.PROD_ID,
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.PROD_ABREV,
            this.PROD_UNIDADE,
            this.PROD_USO_CONTINUO,
            this.PROD_FORMULA,
            this.PROD_CONTROLADO,
            this.NCM,
            this.PROD_SITUACAO,
            this.PROD_MENSAGEM,
            this.DAT_ALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO});
            this.dgProdutos.ContextMenuStrip = this.cmsProdutos;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgProdutos.GridColor = System.Drawing.Color.LightGray;
            this.dgProdutos.Location = new System.Drawing.Point(6, 0);
            this.dgProdutos.MultiSelect = false;
            this.dgProdutos.Name = "dgProdutos";
            this.dgProdutos.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgProdutos.RowHeadersVisible = false;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutos.Size = new System.Drawing.Size(945, 364);
            this.dgProdutos.TabIndex = 1;
            this.dgProdutos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProdutos_CellMouseClick);
            this.dgProdutos.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProdutos_CellMouseDoubleClick);
            this.dgProdutos.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProdutos_ColumnHeaderMouseClick);
            this.dgProdutos.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgProdutos_RowPrePaint);
            this.dgProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProdutos_KeyDown);
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.EST_CODIGO.DefaultCellStyle = dataGridViewCellStyle3;
            this.EST_CODIGO.HeaderText = "Estab. ID";
            this.EST_CODIGO.MaxInputLength = 18;
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EST_CODIGO.Width = 80;
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.EMP_CODIGO.DefaultCellStyle = dataGridViewCellStyle4;
            this.EMP_CODIGO.HeaderText = "Emp. ID";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            this.EMP_CODIGO.Width = 80;
            // 
            // PROD_ID
            // 
            this.PROD_ID.DataPropertyName = "PROD_ID";
            this.PROD_ID.HeaderText = "ID";
            this.PROD_ID.Name = "PROD_ID";
            this.PROD_ID.ReadOnly = true;
            this.PROD_ID.Width = 80;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Código de Barras";
            this.PROD_CODIGO.MaxInputLength = 18;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 150;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.MaxInputLength = 50;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.Width = 400;
            // 
            // PROD_ABREV
            // 
            this.PROD_ABREV.DataPropertyName = "PROD_ABREV";
            this.PROD_ABREV.HeaderText = "Descr. Abrev.";
            this.PROD_ABREV.MaxInputLength = 30;
            this.PROD_ABREV.Name = "PROD_ABREV";
            this.PROD_ABREV.ReadOnly = true;
            this.PROD_ABREV.Width = 150;
            // 
            // PROD_UNIDADE
            // 
            this.PROD_UNIDADE.DataPropertyName = "PROD_UNIDADE";
            this.PROD_UNIDADE.HeaderText = "Unidade";
            this.PROD_UNIDADE.MaxInputLength = 2;
            this.PROD_UNIDADE.Name = "PROD_UNIDADE";
            this.PROD_UNIDADE.ReadOnly = true;
            this.PROD_UNIDADE.Width = 70;
            // 
            // PROD_USO_CONTINUO
            // 
            this.PROD_USO_CONTINUO.DataPropertyName = "PROD_USO_CONTINUO";
            this.PROD_USO_CONTINUO.HeaderText = "Contínuo";
            this.PROD_USO_CONTINUO.MaxInputLength = 1;
            this.PROD_USO_CONTINUO.Name = "PROD_USO_CONTINUO";
            this.PROD_USO_CONTINUO.ReadOnly = true;
            this.PROD_USO_CONTINUO.Width = 75;
            // 
            // PROD_FORMULA
            // 
            this.PROD_FORMULA.DataPropertyName = "PROD_FORMULA";
            this.PROD_FORMULA.HeaderText = "Manipulado";
            this.PROD_FORMULA.MaxInputLength = 1;
            this.PROD_FORMULA.Name = "PROD_FORMULA";
            this.PROD_FORMULA.ReadOnly = true;
            this.PROD_FORMULA.Width = 75;
            // 
            // PROD_CONTROLADO
            // 
            this.PROD_CONTROLADO.DataPropertyName = "PROD_CONTROLADO";
            this.PROD_CONTROLADO.HeaderText = "Controlado";
            this.PROD_CONTROLADO.MaxInputLength = 1;
            this.PROD_CONTROLADO.Name = "PROD_CONTROLADO";
            this.PROD_CONTROLADO.ReadOnly = true;
            this.PROD_CONTROLADO.Width = 75;
            // 
            // NCM
            // 
            this.NCM.DataPropertyName = "NCM";
            this.NCM.HeaderText = "NCM";
            this.NCM.MaxInputLength = 20;
            this.NCM.Name = "NCM";
            this.NCM.ReadOnly = true;
            // 
            // PROD_SITUACAO
            // 
            this.PROD_SITUACAO.DataPropertyName = "PROD_SITUACAO";
            this.PROD_SITUACAO.HeaderText = "Liberado";
            this.PROD_SITUACAO.MaxInputLength = 1;
            this.PROD_SITUACAO.Name = "PROD_SITUACAO";
            this.PROD_SITUACAO.ReadOnly = true;
            this.PROD_SITUACAO.Width = 75;
            // 
            // PROD_MENSAGEM
            // 
            this.PROD_MENSAGEM.DataPropertyName = "PROD_MENSAGEM";
            this.PROD_MENSAGEM.HeaderText = "Mensagem";
            this.PROD_MENSAGEM.MaxInputLength = 50;
            this.PROD_MENSAGEM.Name = "PROD_MENSAGEM";
            this.PROD_MENSAGEM.ReadOnly = true;
            this.PROD_MENSAGEM.Width = 200;
            // 
            // DAT_ALTERACAO
            // 
            this.DAT_ALTERACAO.DataPropertyName = "DAT_ALTERACAO";
            dataGridViewCellStyle5.Format = "G";
            this.DAT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle5;
            this.DAT_ALTERACAO.HeaderText = "Data Alteração";
            this.DAT_ALTERACAO.MaxInputLength = 30;
            this.DAT_ALTERACAO.Name = "DAT_ALTERACAO";
            this.DAT_ALTERACAO.ReadOnly = true;
            this.DAT_ALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle6.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle6;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 30;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // cmsProdutos
            // 
            this.cmsProdutos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsProdutos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsProdutos.Name = "cmsProdutos";
            this.cmsProdutos.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 394);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 70);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(574, 21);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(362, 36);
            this.groupBox4.TabIndex = 192;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 14);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 14);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 14);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 14);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 36);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 37);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtDescricaoCartelado);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.lblCartelado);
            this.groupBox2.Controls.Add(this.txtCodCartelado);
            this.groupBox2.Controls.Add(this.chkCaixaCartelado);
            this.groupBox2.Controls.Add(this.txtCodABC);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.cmbTabPrecos);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.txtNCM);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.cmbPrinAtivo);
            this.groupBox2.Controls.Add(this.txtPrincID);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.cmbEspec);
            this.groupBox2.Controls.Add(this.txtEspcID);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.cmbFabricante);
            this.groupBox2.Controls.Add(this.txtFabID);
            this.groupBox2.Controls.Add(this.cmbSubClas);
            this.groupBox2.Controls.Add(this.txtSubID);
            this.groupBox2.Controls.Add(this.cmbClasse);
            this.groupBox2.Controls.Add(this.txtClasID);
            this.groupBox2.Controls.Add(this.cmbDepto);
            this.groupBox2.Controls.Add(this.txtDeptoID);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbUnidade);
            this.groupBox2.Controls.Add(this.txtDescrAbr);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtCodBarra);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.txtDescr);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtProdID);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 302);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // txtDescricaoCartelado
            // 
            this.txtDescricaoCartelado.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescricaoCartelado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescricaoCartelado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescricaoCartelado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescricaoCartelado.Location = new System.Drawing.Point(346, 249);
            this.txtDescricaoCartelado.MaxLength = 13;
            this.txtDescricaoCartelado.Name = "txtDescricaoCartelado";
            this.txtDescricaoCartelado.Size = new System.Drawing.Size(418, 22);
            this.txtDescricaoCartelado.TabIndex = 331;
            this.txtDescricaoCartelado.Visible = false;
            this.txtDescricaoCartelado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescricaoCartelado_KeyPress);
            this.txtDescricaoCartelado.Validated += new System.EventHandler(this.txtDescricaoCartelado_Validated);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(343, 231);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(156, 16);
            this.label52.TabIndex = 330;
            this.label52.Text = "Descrição do Cartelado";
            this.label52.Visible = false;
            // 
            // lblCartelado
            // 
            this.lblCartelado.AutoSize = true;
            this.lblCartelado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartelado.ForeColor = System.Drawing.Color.Navy;
            this.lblCartelado.Location = new System.Drawing.Point(162, 230);
            this.lblCartelado.Name = "lblCartelado";
            this.lblCartelado.Size = new System.Drawing.Size(168, 16);
            this.lblCartelado.TabIndex = 329;
            this.lblCartelado.Text = "Cód. de Barras Cartelado";
            this.lblCartelado.Visible = false;
            // 
            // txtCodCartelado
            // 
            this.txtCodCartelado.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodCartelado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodCartelado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCartelado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodCartelado.Location = new System.Drawing.Point(165, 249);
            this.txtCodCartelado.MaxLength = 13;
            this.txtCodCartelado.Name = "txtCodCartelado";
            this.txtCodCartelado.Size = new System.Drawing.Size(163, 22);
            this.txtCodCartelado.TabIndex = 328;
            this.txtCodCartelado.Visible = false;
            this.txtCodCartelado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCartelado_KeyPress);
            // 
            // chkCaixaCartelado
            // 
            this.chkCaixaCartelado.AutoSize = true;
            this.chkCaixaCartelado.ForeColor = System.Drawing.Color.Navy;
            this.chkCaixaCartelado.Location = new System.Drawing.Point(13, 253);
            this.chkCaixaCartelado.Name = "chkCaixaCartelado";
            this.chkCaixaCartelado.Size = new System.Drawing.Size(150, 20);
            this.chkCaixaCartelado.TabIndex = 327;
            this.chkCaixaCartelado.Text = "Caixa de Cartelado";
            this.chkCaixaCartelado.UseVisualStyleBackColor = true;
            this.chkCaixaCartelado.CheckedChanged += new System.EventHandler(this.chkCaixaCartelado_CheckedChanged);
            // 
            // txtCodABC
            // 
            this.txtCodABC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCodABC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodABC.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodABC.Location = new System.Drawing.Point(644, 154);
            this.txtCodABC.MaxLength = 15;
            this.txtCodABC.Name = "txtCodABC";
            this.txtCodABC.ReadOnly = true;
            this.txtCodABC.Size = new System.Drawing.Size(292, 22);
            this.txtCodABC.TabIndex = 326;
            this.txtCodABC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodABC_KeyPress);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(641, 136);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(109, 16);
            this.label31.TabIndex = 325;
            this.label31.Text = "Cód. ABCFarma";
            // 
            // cmbTabPrecos
            // 
            this.cmbTabPrecos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTabPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTabPrecos.FormattingEnabled = true;
            this.cmbTabPrecos.Location = new System.Drawing.Point(782, 196);
            this.cmbTabPrecos.Name = "cmbTabPrecos";
            this.cmbTabPrecos.Size = new System.Drawing.Size(154, 24);
            this.cmbTabPrecos.TabIndex = 322;
            this.cmbTabPrecos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTabPrecos_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(779, 180);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 16);
            this.label34.TabIndex = 321;
            this.label34.Text = "Tab. de Preços";
            // 
            // txtNCM
            // 
            this.txtNCM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNCM.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNCM.Location = new System.Drawing.Point(644, 198);
            this.txtNCM.MaxLength = 15;
            this.txtNCM.Name = "txtNCM";
            this.txtNCM.Size = new System.Drawing.Size(120, 22);
            this.txtNCM.TabIndex = 320;
            this.txtNCM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNCM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNCM_KeyPress);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(641, 180);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(37, 16);
            this.label46.TabIndex = 319;
            this.label46.Text = "NCM";
            // 
            // cmbPrinAtivo
            // 
            this.cmbPrinAtivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinAtivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPrinAtivo.FormattingEnabled = true;
            this.cmbPrinAtivo.Location = new System.Drawing.Point(410, 198);
            this.cmbPrinAtivo.Name = "cmbPrinAtivo";
            this.cmbPrinAtivo.Size = new System.Drawing.Size(219, 24);
            this.cmbPrinAtivo.TabIndex = 262;
            this.cmbPrinAtivo.SelectedIndexChanged += new System.EventHandler(this.cmbPrinAtivo_SelectedIndexChanged);
            this.cmbPrinAtivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPrinAtivo_KeyPress);
            // 
            // txtPrincID
            // 
            this.txtPrincID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrincID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrincID.Location = new System.Drawing.Point(346, 199);
            this.txtPrincID.MaxLength = 6;
            this.txtPrincID.Name = "txtPrincID";
            this.txtPrincID.Size = new System.Drawing.Size(58, 22);
            this.txtPrincID.TabIndex = 261;
            this.txtPrincID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrincID_KeyPress);
            this.txtPrincID.Validated += new System.EventHandler(this.txtPrincID_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(343, 180);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(134, 16);
            this.label16.TabIndex = 260;
            this.label16.Text = "Cód. Princípio Ativo";
            // 
            // cmbEspec
            // 
            this.cmbEspec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEspec.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEspec.FormattingEnabled = true;
            this.cmbEspec.Location = new System.Drawing.Point(81, 199);
            this.cmbEspec.Name = "cmbEspec";
            this.cmbEspec.Size = new System.Drawing.Size(247, 24);
            this.cmbEspec.TabIndex = 259;
            this.cmbEspec.SelectedIndexChanged += new System.EventHandler(this.cmbEspec_SelectedIndexChanged);
            this.cmbEspec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEspec_KeyPress);
            // 
            // txtEspcID
            // 
            this.txtEspcID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEspcID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEspcID.Location = new System.Drawing.Point(13, 199);
            this.txtEspcID.MaxLength = 6;
            this.txtEspcID.Name = "txtEspcID";
            this.txtEspcID.Size = new System.Drawing.Size(58, 22);
            this.txtEspcID.TabIndex = 258;
            this.txtEspcID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEspcID_KeyPress);
            this.txtEspcID.Validated += new System.EventHandler(this.txtEspcID_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(10, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 16);
            this.label15.TabIndex = 257;
            this.label15.Text = "Cód. Especificação";
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(13, 227);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 256;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(445, 86);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 193;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(293, 86);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 192;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(851, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 191;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(229, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 190;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(93, 39);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 189;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // cmbFabricante
            // 
            this.cmbFabricante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFabricante.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFabricante.FormattingEnabled = true;
            this.cmbFabricante.Location = new System.Drawing.Point(410, 153);
            this.cmbFabricante.Name = "cmbFabricante";
            this.cmbFabricante.Size = new System.Drawing.Size(219, 24);
            this.cmbFabricante.TabIndex = 127;
            this.cmbFabricante.SelectedIndexChanged += new System.EventHandler(this.cmbFabricante_SelectedIndexChanged);
            this.cmbFabricante.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFabricante_KeyPress);
            // 
            // txtFabID
            // 
            this.txtFabID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFabID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabID.Location = new System.Drawing.Point(346, 154);
            this.txtFabID.MaxLength = 6;
            this.txtFabID.Name = "txtFabID";
            this.txtFabID.Size = new System.Drawing.Size(58, 22);
            this.txtFabID.TabIndex = 126;
            this.txtFabID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFabID_KeyPress);
            this.txtFabID.Validated += new System.EventHandler(this.txtFabID_Validated);
            // 
            // cmbSubClas
            // 
            this.cmbSubClas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubClas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSubClas.FormattingEnabled = true;
            this.cmbSubClas.Location = new System.Drawing.Point(81, 153);
            this.cmbSubClas.Name = "cmbSubClas";
            this.cmbSubClas.Size = new System.Drawing.Size(247, 24);
            this.cmbSubClas.TabIndex = 125;
            this.cmbSubClas.SelectedIndexChanged += new System.EventHandler(this.cmbSubClas_SelectedIndexChanged);
            this.cmbSubClas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbSubClas_KeyPress);
            // 
            // txtSubID
            // 
            this.txtSubID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubID.Location = new System.Drawing.Point(13, 154);
            this.txtSubID.MaxLength = 6;
            this.txtSubID.Name = "txtSubID";
            this.txtSubID.Size = new System.Drawing.Size(58, 22);
            this.txtSubID.TabIndex = 124;
            this.txtSubID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubID_KeyPress);
            this.txtSubID.Validated += new System.EventHandler(this.txtSubID_Validated);
            // 
            // cmbClasse
            // 
            this.cmbClasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClasse.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClasse.FormattingEnabled = true;
            this.cmbClasse.Location = new System.Drawing.Point(708, 102);
            this.cmbClasse.Name = "cmbClasse";
            this.cmbClasse.Size = new System.Drawing.Size(228, 24);
            this.cmbClasse.TabIndex = 123;
            this.cmbClasse.SelectedIndexChanged += new System.EventHandler(this.cmbClasse_SelectedIndexChanged);
            this.cmbClasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbClasse_KeyPress);
            // 
            // txtClasID
            // 
            this.txtClasID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClasID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClasID.Location = new System.Drawing.Point(644, 104);
            this.txtClasID.MaxLength = 6;
            this.txtClasID.Name = "txtClasID";
            this.txtClasID.Size = new System.Drawing.Size(58, 22);
            this.txtClasID.TabIndex = 122;
            this.txtClasID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClasID_KeyPress);
            this.txtClasID.Validated += new System.EventHandler(this.txtClasID_Validated);
            // 
            // cmbDepto
            // 
            this.cmbDepto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDepto.FormattingEnabled = true;
            this.cmbDepto.Location = new System.Drawing.Point(410, 102);
            this.cmbDepto.Name = "cmbDepto";
            this.cmbDepto.Size = new System.Drawing.Size(219, 24);
            this.cmbDepto.TabIndex = 121;
            this.cmbDepto.SelectedIndexChanged += new System.EventHandler(this.cmbDepto_SelectedIndexChanged);
            this.cmbDepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDepto_KeyPress);
            // 
            // txtDeptoID
            // 
            this.txtDeptoID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeptoID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeptoID.Location = new System.Drawing.Point(346, 104);
            this.txtDeptoID.MaxLength = 6;
            this.txtDeptoID.Name = "txtDeptoID";
            this.txtDeptoID.Size = new System.Drawing.Size(58, 22);
            this.txtDeptoID.TabIndex = 120;
            this.txtDeptoID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeptoID_KeyPress);
            this.txtDeptoID.Validated += new System.EventHandler(this.txtDeptoID_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(343, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 16);
            this.label9.TabIndex = 119;
            this.label9.Text = "Fabricante";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(10, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 118;
            this.label7.Text = "Subclasse";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(641, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 16);
            this.label6.TabIndex = 117;
            this.label6.Text = "Classe";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(343, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 16);
            this.label5.TabIndex = 116;
            this.label5.Text = "Departamento";
            // 
            // cmbUnidade
            // 
            this.cmbUnidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnidade.FormattingEnabled = true;
            this.cmbUnidade.Location = new System.Drawing.Point(233, 104);
            this.cmbUnidade.Name = "cmbUnidade";
            this.cmbUnidade.Size = new System.Drawing.Size(95, 24);
            this.cmbUnidade.TabIndex = 115;
            this.cmbUnidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUnidade_KeyPress);
            // 
            // txtDescrAbr
            // 
            this.txtDescrAbr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescrAbr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescrAbr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescrAbr.Location = new System.Drawing.Point(13, 104);
            this.txtDescrAbr.MaxLength = 30;
            this.txtDescrAbr.Name = "txtDescrAbr";
            this.txtDescrAbr.Size = new System.Drawing.Size(203, 22);
            this.txtDescrAbr.TabIndex = 114;
            this.txtDescrAbr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescrAbr_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(10, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.TabIndex = 113;
            this.label4.Text = "Descr. Abreviada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(727, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 16);
            this.label1.TabIndex = 111;
            this.label1.Text = "Código de Barras";
            // 
            // txtCodBarra
            // 
            this.txtCodBarra.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodBarra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodBarra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarra.Location = new System.Drawing.Point(730, 57);
            this.txtCodBarra.MaxLength = 13;
            this.txtCodBarra.Name = "txtCodBarra";
            this.txtCodBarra.Size = new System.Drawing.Size(206, 22);
            this.txtCodBarra.TabIndex = 110;
            this.txtCodBarra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarra_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(230, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.TabIndex = 109;
            this.label3.Text = "Unidade";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 17);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(156, 57);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(568, 22);
            this.txtDescr.TabIndex = 105;
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(153, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 85;
            this.label2.Text = "Descrição";
            // 
            // txtProdID
            // 
            this.txtProdID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtProdID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdID.Location = new System.Drawing.Point(13, 57);
            this.txtProdID.MaxLength = 18;
            this.txtProdID.Name = "txtProdID";
            this.txtProdID.ReadOnly = true;
            this.txtProdID.Size = new System.Drawing.Size(128, 22);
            this.txtProdID.TabIndex = 81;
            this.txtProdID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(10, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Produto ID";
            // 
            // tpComplemento
            // 
            this.tpComplemento.Controls.Add(this.tcComplemento);
            this.tpComplemento.Location = new System.Drawing.Point(4, 25);
            this.tpComplemento.Name = "tpComplemento";
            this.tpComplemento.Padding = new System.Windows.Forms.Padding(3);
            this.tpComplemento.Size = new System.Drawing.Size(957, 470);
            this.tpComplemento.TabIndex = 2;
            this.tpComplemento.Text = "Complemento";
            this.tpComplemento.UseVisualStyleBackColor = true;
            // 
            // tcComplemento
            // 
            this.tcComplemento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcComplemento.Controls.Add(this.tpGeral);
            this.tcComplemento.Controls.Add(this.tpPrecos);
            this.tcComplemento.Location = new System.Drawing.Point(6, 6);
            this.tcComplemento.Name = "tcComplemento";
            this.tcComplemento.SelectedIndex = 0;
            this.tcComplemento.Size = new System.Drawing.Size(945, 458);
            this.tcComplemento.TabIndex = 326;
            this.tcComplemento.SelectedIndexChanged += new System.EventHandler(this.tcComplemento_SelectedIndexChanged);
            // 
            // tpGeral
            // 
            this.tpGeral.Controls.Add(this.cmbPortaria);
            this.tpGeral.Controls.Add(this.label54);
            this.tpGeral.Controls.Add(this.groupBox13);
            this.tpGeral.Controls.Add(this.cmbTipoReceita);
            this.tpGeral.Controls.Add(this.label53);
            this.tpGeral.Controls.Add(this.groupBox6);
            this.tpGeral.Controls.Add(this.txtRegMS);
            this.tpGeral.Controls.Add(this.label17);
            this.tpGeral.Controls.Add(this.label48);
            this.tpGeral.Controls.Add(this.cmbClasTera);
            this.tpGeral.Controls.Add(this.label13);
            this.tpGeral.Controls.Add(this.txtMsg);
            this.tpGeral.Location = new System.Drawing.Point(4, 25);
            this.tpGeral.Name = "tpGeral";
            this.tpGeral.Size = new System.Drawing.Size(937, 429);
            this.tpGeral.TabIndex = 1;
            this.tpGeral.Text = "Geral";
            this.tpGeral.UseVisualStyleBackColor = true;
            // 
            // cmbPortaria
            // 
            this.cmbPortaria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortaria.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPortaria.FormattingEnabled = true;
            this.cmbPortaria.Items.AddRange(new object[] {
            "",
            "A1",
            "A2",
            "A3",
            "B1",
            "B2",
            "C1",
            "C2",
            "C3",
            "C5",
            "D1",
            "D2"});
            this.cmbPortaria.Location = new System.Drawing.Point(630, 83);
            this.cmbPortaria.Name = "cmbPortaria";
            this.cmbPortaria.Size = new System.Drawing.Size(120, 24);
            this.cmbPortaria.TabIndex = 361;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(627, 64);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(59, 16);
            this.label54.TabIndex = 335;
            this.label54.Text = "Portaria";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.txtCfopDev);
            this.groupBox13.Controls.Add(this.label51);
            this.groupBox13.Controls.Add(this.cmbCst);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this.txtEnqIpi);
            this.groupBox13.Controls.Add(this.txtCstPis);
            this.groupBox13.Controls.Add(this.label65);
            this.groupBox13.Controls.Add(this.txtCstCofins);
            this.groupBox13.Controls.Add(this.label64);
            this.groupBox13.Controls.Add(this.txtCstIpi);
            this.groupBox13.Controls.Add(this.label63);
            this.groupBox13.Controls.Add(this.txtCsosn);
            this.groupBox13.Controls.Add(this.label62);
            this.groupBox13.Controls.Add(this.cmbIcms);
            this.groupBox13.Controls.Add(this.label60);
            this.groupBox13.Controls.Add(this.txtCest);
            this.groupBox13.Controls.Add(this.label59);
            this.groupBox13.Controls.Add(this.label58);
            this.groupBox13.Controls.Add(this.txtCfop);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(188, 111);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(733, 116);
            this.groupBox13.TabIndex = 334;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Impostos";
            // 
            // txtCfopDev
            // 
            this.txtCfopDev.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCfopDev.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCfopDev.Location = new System.Drawing.Point(302, 79);
            this.txtCfopDev.MaxLength = 10;
            this.txtCfopDev.Name = "txtCfopDev";
            this.txtCfopDev.Size = new System.Drawing.Size(112, 22);
            this.txtCfopDev.TabIndex = 362;
            this.txtCfopDev.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(299, 62);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(115, 16);
            this.label51.TabIndex = 361;
            this.label51.Text = "CFOP Devolução";
            // 
            // cmbCst
            // 
            this.cmbCst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCst.FormattingEnabled = true;
            this.cmbCst.Items.AddRange(new object[] {
            "",
            "000",
            "020",
            "010",
            "070",
            "030",
            "060",
            "040",
            "041",
            "050",
            "051",
            "090"});
            this.cmbCst.Location = new System.Drawing.Point(111, 35);
            this.cmbCst.Name = "cmbCst";
            this.cmbCst.Size = new System.Drawing.Size(84, 24);
            this.cmbCst.TabIndex = 360;
            this.cmbCst.SelectedIndexChanged += new System.EventHandler(this.cmbCst_SelectedIndexChanged);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Navy;
            this.label89.Location = new System.Drawing.Point(205, 62);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(57, 16);
            this.label89.TabIndex = 359;
            this.label89.Text = "Enq. IPI";
            // 
            // txtEnqIpi
            // 
            this.txtEnqIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEnqIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnqIpi.Location = new System.Drawing.Point(208, 79);
            this.txtEnqIpi.MaxLength = 10;
            this.txtEnqIpi.Name = "txtEnqIpi";
            this.txtEnqIpi.Size = new System.Drawing.Size(84, 22);
            this.txtEnqIpi.TabIndex = 358;
            this.txtEnqIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEnqIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnqIpi_KeyPress);
            // 
            // txtCstPis
            // 
            this.txtCstPis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstPis.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstPis.Location = new System.Drawing.Point(111, 79);
            this.txtCstPis.MaxLength = 10;
            this.txtCstPis.Name = "txtCstPis";
            this.txtCstPis.Size = new System.Drawing.Size(84, 22);
            this.txtCstPis.TabIndex = 357;
            this.txtCstPis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstPis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstPis_KeyPress);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(108, 62);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(60, 16);
            this.label65.TabIndex = 356;
            this.label65.Text = "CST PIS";
            // 
            // txtCstCofins
            // 
            this.txtCstCofins.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstCofins.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstCofins.Location = new System.Drawing.Point(14, 79);
            this.txtCstCofins.MaxLength = 10;
            this.txtCstCofins.Name = "txtCstCofins";
            this.txtCstCofins.Size = new System.Drawing.Size(84, 22);
            this.txtCstCofins.TabIndex = 355;
            this.txtCstCofins.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstCofins.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstCofins_KeyPress);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Navy;
            this.label64.Location = new System.Drawing.Point(11, 62);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(87, 16);
            this.label64.TabIndex = 354;
            this.label64.Text = "CST COFINS";
            // 
            // txtCstIpi
            // 
            this.txtCstIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstIpi.Location = new System.Drawing.Point(631, 37);
            this.txtCstIpi.MaxLength = 10;
            this.txtCstIpi.Name = "txtCstIpi";
            this.txtCstIpi.Size = new System.Drawing.Size(84, 22);
            this.txtCstIpi.TabIndex = 353;
            this.txtCstIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstIpi_KeyPress);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(628, 20);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(55, 16);
            this.label63.TabIndex = 352;
            this.label63.Text = "CST IPI";
            // 
            // txtCsosn
            // 
            this.txtCsosn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCsosn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCsosn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCsosn.ForeColor = System.Drawing.Color.Black;
            this.txtCsosn.Location = new System.Drawing.Point(541, 37);
            this.txtCsosn.MaxLength = 10;
            this.txtCsosn.Name = "txtCsosn";
            this.txtCsosn.ReadOnly = true;
            this.txtCsosn.Size = new System.Drawing.Size(84, 22);
            this.txtCsosn.TabIndex = 351;
            this.txtCsosn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(538, 20);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(54, 16);
            this.label62.TabIndex = 350;
            this.label62.Text = "CSOSN";
            // 
            // cmbIcms
            // 
            this.cmbIcms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIcms.FormattingEnabled = true;
            this.cmbIcms.Items.AddRange(new object[] {
            "",
            "TRIBUTADA INTEGRALMENTE",
            "TRIB. E COM COBRANÇA ICMS ST",
            "RED. BASE DE CÁLCULO",
            "ISENTA/NÃO TRIB. COM COB. DE ICMS ST",
            "ISENTA",
            "NÃO TRIBUTADA",
            "COM SUSPENSÃO",
            "COM DIFERIMENTO",
            "ICMS COBRADO ANTERIORMENTE POR ST",
            "RED. BASE DE CÁLC. E COB. DO ICMS ST",
            "OUTRAS"});
            this.cmbIcms.Location = new System.Drawing.Point(302, 35);
            this.cmbIcms.Name = "cmbIcms";
            this.cmbIcms.Size = new System.Drawing.Size(229, 24);
            this.cmbIcms.TabIndex = 349;
            this.cmbIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIcms_KeyPress);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Navy;
            this.label60.Location = new System.Drawing.Point(299, 19);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(41, 16);
            this.label60.TabIndex = 348;
            this.label60.Text = "ICMS";
            // 
            // txtCest
            // 
            this.txtCest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCest.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCest.Location = new System.Drawing.Point(208, 37);
            this.txtCest.MaxLength = 10;
            this.txtCest.Name = "txtCest";
            this.txtCest.Size = new System.Drawing.Size(84, 22);
            this.txtCest.TabIndex = 347;
            this.txtCest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCest_KeyPress);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Navy;
            this.label59.Location = new System.Drawing.Point(205, 20);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(42, 16);
            this.label59.TabIndex = 346;
            this.label59.Text = "CEST";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(108, 20);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 16);
            this.label58.TabIndex = 344;
            this.label58.Text = "CST";
            // 
            // txtCfop
            // 
            this.txtCfop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCfop.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCfop.Location = new System.Drawing.Point(14, 37);
            this.txtCfop.MaxLength = 10;
            this.txtCfop.Name = "txtCfop";
            this.txtCfop.Size = new System.Drawing.Size(84, 22);
            this.txtCfop.TabIndex = 343;
            this.txtCfop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCfop.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCfop_KeyPress);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(11, 20);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(44, 16);
            this.label57.TabIndex = 342;
            this.label57.Text = "CFOP";
            // 
            // cmbTipoReceita
            // 
            this.cmbTipoReceita.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoReceita.FormattingEnabled = true;
            this.cmbTipoReceita.Items.AddRange(new object[] {
            "",
            "BRANCA",
            "AZUL"});
            this.cmbTipoReceita.Location = new System.Drawing.Point(630, 37);
            this.cmbTipoReceita.Name = "cmbTipoReceita";
            this.cmbTipoReceita.Size = new System.Drawing.Size(291, 24);
            this.cmbTipoReceita.TabIndex = 333;
            this.cmbTipoReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTipoReceita_KeyPress);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Navy;
            this.label53.Location = new System.Drawing.Point(627, 18);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(88, 16);
            this.label53.TabIndex = 332;
            this.label53.Text = "Tipo Receita";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.chkManipulado);
            this.groupBox6.Controls.Add(this.chkControlado);
            this.groupBox6.Controls.Add(this.ckbUsoContinuo);
            this.groupBox6.Controls.Add(this.chkBCompra);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(14, 111);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(168, 116);
            this.groupBox6.TabIndex = 331;
            this.groupBox6.TabStop = false;
            // 
            // chkManipulado
            // 
            this.chkManipulado.AutoSize = true;
            this.chkManipulado.ForeColor = System.Drawing.Color.Navy;
            this.chkManipulado.Location = new System.Drawing.Point(10, 90);
            this.chkManipulado.Name = "chkManipulado";
            this.chkManipulado.Size = new System.Drawing.Size(102, 20);
            this.chkManipulado.TabIndex = 326;
            this.chkManipulado.Text = "Manipulado";
            this.chkManipulado.UseVisualStyleBackColor = true;
            this.chkManipulado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkManipulado_KeyPress);
            // 
            // chkControlado
            // 
            this.chkControlado.AutoSize = true;
            this.chkControlado.ForeColor = System.Drawing.Color.Navy;
            this.chkControlado.Location = new System.Drawing.Point(10, 64);
            this.chkControlado.Name = "chkControlado";
            this.chkControlado.Size = new System.Drawing.Size(97, 20);
            this.chkControlado.TabIndex = 325;
            this.chkControlado.Text = "Controlado";
            this.chkControlado.UseVisualStyleBackColor = true;
            this.chkControlado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkControlado_KeyPress);
            // 
            // ckbUsoContinuo
            // 
            this.ckbUsoContinuo.AutoSize = true;
            this.ckbUsoContinuo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbUsoContinuo.ForeColor = System.Drawing.Color.Navy;
            this.ckbUsoContinuo.Location = new System.Drawing.Point(10, 37);
            this.ckbUsoContinuo.Name = "ckbUsoContinuo";
            this.ckbUsoContinuo.Size = new System.Drawing.Size(111, 20);
            this.ckbUsoContinuo.TabIndex = 324;
            this.ckbUsoContinuo.Text = "Uso Contínuo";
            this.ckbUsoContinuo.UseVisualStyleBackColor = true;
            this.ckbUsoContinuo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkContinuo_KeyPress);
            // 
            // chkBCompra
            // 
            this.chkBCompra.AutoSize = true;
            this.chkBCompra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBCompra.ForeColor = System.Drawing.Color.Navy;
            this.chkBCompra.Location = new System.Drawing.Point(10, 11);
            this.chkBCompra.Name = "chkBCompra";
            this.chkBCompra.Size = new System.Drawing.Size(153, 20);
            this.chkBCompra.TabIndex = 323;
            this.chkBCompra.Text = "Bloquear p/ compra";
            this.chkBCompra.UseVisualStyleBackColor = true;
            this.chkBCompra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkBCompra_KeyPress);
            // 
            // txtRegMS
            // 
            this.txtRegMS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegMS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegMS.Location = new System.Drawing.Point(321, 39);
            this.txtRegMS.MaxLength = 13;
            this.txtRegMS.Name = "txtRegMS";
            this.txtRegMS.Size = new System.Drawing.Size(292, 22);
            this.txtRegMS.TabIndex = 330;
            this.txtRegMS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegMS_KeyPress);
            this.txtRegMS.Validated += new System.EventHandler(this.txtRegMS_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(318, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(188, 16);
            this.label17.TabIndex = 329;
            this.label17.Text = "Reg. no Ministério da Saúde";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(11, 64);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(272, 16);
            this.label48.TabIndex = 327;
            this.label48.Text = "Mensagem para exibir na Tela de Vendas";
            // 
            // cmbClasTera
            // 
            this.cmbClasTera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClasTera.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClasTera.FormattingEnabled = true;
            this.cmbClasTera.Items.AddRange(new object[] {
            "",
            "ANTIMICROBIANOS",
            "SUJEITO A CONTROLE ESPECIAL"});
            this.cmbClasTera.Location = new System.Drawing.Point(14, 37);
            this.cmbClasTera.Name = "cmbClasTera";
            this.cmbClasTera.Size = new System.Drawing.Size(291, 24);
            this.cmbClasTera.TabIndex = 326;
            this.cmbClasTera.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbClasTera_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(11, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 16);
            this.label13.TabIndex = 325;
            this.label13.Text = "Classe Terapêutica";
            // 
            // txtMsg
            // 
            this.txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMsg.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMsg.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsg.Location = new System.Drawing.Point(14, 83);
            this.txtMsg.MaxLength = 60;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(599, 22);
            this.txtMsg.TabIndex = 328;
            this.txtMsg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMsg_KeyPress);
            // 
            // tpPrecos
            // 
            this.tpPrecos.Controls.Add(this.lblAlterado);
            this.tpPrecos.Controls.Add(this.groupBox12);
            this.tpPrecos.Controls.Add(this.groupBox9);
            this.tpPrecos.Controls.Add(this.groupBox8);
            this.tpPrecos.Controls.Add(this.groupBox7);
            this.tpPrecos.Location = new System.Drawing.Point(4, 25);
            this.tpPrecos.Name = "tpPrecos";
            this.tpPrecos.Padding = new System.Windows.Forms.Padding(3);
            this.tpPrecos.Size = new System.Drawing.Size(937, 429);
            this.tpPrecos.TabIndex = 0;
            this.tpPrecos.Text = "Preços";
            this.tpPrecos.UseVisualStyleBackColor = true;
            // 
            // lblAlterado
            // 
            this.lblAlterado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlterado.ForeColor = System.Drawing.Color.Navy;
            this.lblAlterado.Location = new System.Drawing.Point(353, 153);
            this.lblAlterado.Name = "lblAlterado";
            this.lblAlterado.Size = new System.Drawing.Size(568, 19);
            this.lblAlterado.TabIndex = 363;
            this.lblAlterado.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.dgUltimasCompras);
            this.groupBox12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(16, 261);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(436, 162);
            this.groupBox12.TabIndex = 352;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Últimas Compras";
            // 
            // dgUltimasCompras
            // 
            this.dgUltimasCompras.AllowUserToAddRows = false;
            this.dgUltimasCompras.AllowUserToDeleteRows = false;
            this.dgUltimasCompras.AllowUserToResizeColumns = false;
            this.dgUltimasCompras.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dgUltimasCompras.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgUltimasCompras.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgUltimasCompras.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgUltimasCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUltimasCompras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ENT_DTLANC,
            this.VALOR,
            this.CF_NOME});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgUltimasCompras.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgUltimasCompras.GridColor = System.Drawing.Color.Gainsboro;
            this.dgUltimasCompras.Location = new System.Drawing.Point(15, 21);
            this.dgUltimasCompras.MultiSelect = false;
            this.dgUltimasCompras.Name = "dgUltimasCompras";
            this.dgUltimasCompras.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgUltimasCompras.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgUltimasCompras.RowHeadersVisible = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dgUltimasCompras.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgUltimasCompras.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUltimasCompras.Size = new System.Drawing.Size(407, 135);
            this.dgUltimasCompras.TabIndex = 0;
            // 
            // ENT_DTLANC
            // 
            this.ENT_DTLANC.DataPropertyName = "ENT_DTLANC";
            this.ENT_DTLANC.HeaderText = "Data";
            this.ENT_DTLANC.Name = "ENT_DTLANC";
            this.ENT_DTLANC.ReadOnly = true;
            this.ENT_DTLANC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VALOR
            // 
            this.VALOR.DataPropertyName = "VALOR";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.VALOR.DefaultCellStyle = dataGridViewCellStyle12;
            this.VALOR.HeaderText = "Valor";
            this.VALOR.Name = "VALOR";
            this.VALOR.ReadOnly = true;
            this.VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VALOR.Width = 80;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Fornecedor";
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_NOME.Width = 220;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtDcb);
            this.groupBox9.Controls.Add(this.txtQtdUniFP);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this.cmbLista);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.txtEcf);
            this.groupBox9.Controls.Add(this.txtQtde);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.cmbUComp);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(16, 139);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(325, 116);
            this.groupBox9.TabIndex = 349;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Informações Adicionais";
            // 
            // txtDcb
            // 
            this.txtDcb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDcb.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDcb.Location = new System.Drawing.Point(214, 36);
            this.txtDcb.MaxLength = 20;
            this.txtDcb.Name = "txtDcb";
            this.txtDcb.Size = new System.Drawing.Size(103, 22);
            this.txtDcb.TabIndex = 346;
            this.txtDcb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDcb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDcb_KeyPress);
            // 
            // txtQtdUniFP
            // 
            this.txtQtdUniFP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtdUniFP.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdUniFP.Location = new System.Drawing.Point(214, 76);
            this.txtQtdUniFP.MaxLength = 20;
            this.txtQtdUniFP.Name = "txtQtdUniFP";
            this.txtQtdUniFP.Size = new System.Drawing.Size(103, 22);
            this.txtQtdUniFP.TabIndex = 347;
            this.txtQtdUniFP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtdUniFP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtdUniFP_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(211, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 16);
            this.label30.TabIndex = 344;
            this.label30.Text = "DCB";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(212, 59);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(105, 16);
            this.label50.TabIndex = 345;
            this.label50.Text = "Qt por Unid. FP";
            // 
            // cmbLista
            // 
            this.cmbLista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLista.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLista.FormattingEnabled = true;
            this.cmbLista.Items.AddRange(new object[] {
            "POSITIVA",
            "NEGATIVA",
            "NEUTRA"});
            this.cmbLista.Location = new System.Drawing.Point(15, 34);
            this.cmbLista.Name = "cmbLista";
            this.cmbLista.Size = new System.Drawing.Size(84, 24);
            this.cmbLista.TabIndex = 336;
            this.cmbLista.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTabPrecos_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(12, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 16);
            this.label25.TabIndex = 328;
            this.label25.Text = "Lista";
            // 
            // txtEcf
            // 
            this.txtEcf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEcf.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEcf.Location = new System.Drawing.Point(115, 36);
            this.txtEcf.MaxLength = 3;
            this.txtEcf.Name = "txtEcf";
            this.txtEcf.Size = new System.Drawing.Size(84, 22);
            this.txtEcf.TabIndex = 341;
            this.txtEcf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEcf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEcf_KeyPress);
            // 
            // txtQtde
            // 
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(115, 76);
            this.txtQtde.MaxLength = 20;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(84, 22);
            this.txtQtde.TabIndex = 343;
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(112, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 16);
            this.label26.TabIndex = 337;
            this.label26.Text = "ECF";
            // 
            // cmbUComp
            // 
            this.cmbUComp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUComp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUComp.FormattingEnabled = true;
            this.cmbUComp.Location = new System.Drawing.Point(15, 76);
            this.cmbUComp.Name = "cmbUComp";
            this.cmbUComp.Size = new System.Drawing.Size(84, 24);
            this.cmbUComp.TabIndex = 342;
            this.cmbUComp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUComp_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(113, 59);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 16);
            this.label28.TabIndex = 339;
            this.label28.Text = "Qt por Unid.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(12, 59);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(95, 16);
            this.label27.TabIndex = 338;
            this.label27.Text = "Unid. Compra";
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.chkBloqDesconto);
            this.groupBox8.Controls.Add(this.dtCInicial);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this.txtDtVenda);
            this.groupBox8.Controls.Add(this.label49);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.txtPVenda);
            this.groupBox8.Controls.Add(this.txtComissao);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.txtMargem);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this.txtCompra);
            this.groupBox8.Controls.Add(this.txtCusUlt);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.txtPmc);
            this.groupBox8.Controls.Add(this.label47);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.txtCusIni);
            this.groupBox8.Controls.Add(this.txtCusMed);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(244, 17);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(677, 116);
            this.groupBox8.TabIndex = 348;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Valores";
            // 
            // chkBloqDesconto
            // 
            this.chkBloqDesconto.AutoSize = true;
            this.chkBloqDesconto.ForeColor = System.Drawing.Color.Navy;
            this.chkBloqDesconto.Location = new System.Drawing.Point(302, 82);
            this.chkBloqDesconto.Name = "chkBloqDesconto";
            this.chkBloqDesconto.Size = new System.Drawing.Size(163, 20);
            this.chkBloqDesconto.TabIndex = 362;
            this.chkBloqDesconto.Text = "Bloquear p/ Desconto";
            this.chkBloqDesconto.UseVisualStyleBackColor = true;
            this.chkBloqDesconto.CheckedChanged += new System.EventHandler(this.chkBloqDesconto_CheckedChanged);
            // 
            // dtCInicial
            // 
            this.dtCInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtCInicial.Location = new System.Drawing.Point(204, 81);
            this.dtCInicial.Name = "dtCInicial";
            this.dtCInicial.Size = new System.Drawing.Size(84, 22);
            this.dtCInicial.TabIndex = 361;
            this.dtCInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtCInicial_KeyPress);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Navy;
            this.label61.Location = new System.Drawing.Point(202, 65);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 16);
            this.label61.TabIndex = 355;
            this.label61.Text = "Dt. Inicial";
            // 
            // txtDtVenda
            // 
            this.txtDtVenda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDtVenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDtVenda.Location = new System.Drawing.Point(109, 81);
            this.txtDtVenda.Mask = "00/00/0000";
            this.txtDtVenda.Name = "txtDtVenda";
            this.txtDtVenda.ReadOnly = true;
            this.txtDtVenda.Size = new System.Drawing.Size(84, 22);
            this.txtDtVenda.TabIndex = 354;
            this.txtDtVenda.ValidatingType = typeof(System.DateTime);
            this.txtDtVenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDtVenda_KeyPress);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(106, 65);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(94, 16);
            this.label49.TabIndex = 353;
            this.label49.Text = "Dt. Ult. Venda";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(585, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 16);
            this.label33.TabIndex = 351;
            this.label33.Text = "Venda";
            // 
            // txtPVenda
            // 
            this.txtPVenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVenda.Location = new System.Drawing.Point(587, 40);
            this.txtPVenda.MaxLength = 20;
            this.txtPVenda.Name = "txtPVenda";
            this.txtPVenda.Size = new System.Drawing.Size(84, 22);
            this.txtPVenda.TabIndex = 352;
            this.txtPVenda.Text = "0,00";
            this.txtPVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVenda_KeyPress);
            // 
            // txtComissao
            // 
            this.txtComissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComissao.Location = new System.Drawing.Point(13, 81);
            this.txtComissao.MaxLength = 20;
            this.txtComissao.Name = "txtComissao";
            this.txtComissao.Size = new System.Drawing.Size(84, 22);
            this.txtComissao.TabIndex = 344;
            this.txtComissao.Text = "0,00";
            this.txtComissao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComissao_KeyPress);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(491, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 16);
            this.label32.TabIndex = 349;
            this.label32.Text = "Margem";
            // 
            // txtMargem
            // 
            this.txtMargem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMargem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMargem.Location = new System.Drawing.Point(493, 40);
            this.txtMargem.MaxLength = 20;
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(84, 22);
            this.txtMargem.TabIndex = 350;
            this.txtMargem.Text = "0,00";
            this.txtMargem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMargem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMargem_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(395, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 16);
            this.label14.TabIndex = 347;
            this.label14.Text = "Compra";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(14, 64);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 16);
            this.label29.TabIndex = 340;
            this.label29.Text = "Comissão";
            // 
            // txtCompra
            // 
            this.txtCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCompra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompra.Location = new System.Drawing.Point(397, 40);
            this.txtCompra.MaxLength = 20;
            this.txtCompra.Name = "txtCompra";
            this.txtCompra.Size = new System.Drawing.Size(84, 22);
            this.txtCompra.TabIndex = 348;
            this.txtCompra.Text = "0,00";
            this.txtCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCompra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCompra_KeyPress);
            // 
            // txtCusUlt
            // 
            this.txtCusUlt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCusUlt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCusUlt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusUlt.ForeColor = System.Drawing.Color.Black;
            this.txtCusUlt.Location = new System.Drawing.Point(204, 40);
            this.txtCusUlt.MaxLength = 18;
            this.txtCusUlt.Name = "txtCusUlt";
            this.txtCusUlt.ReadOnly = true;
            this.txtCusUlt.Size = new System.Drawing.Size(84, 22);
            this.txtCusUlt.TabIndex = 335;
            this.txtCusUlt.Text = "0,00";
            this.txtCusUlt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(11, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 16);
            this.label22.TabIndex = 325;
            this.label22.Text = "Custo Inicial";
            // 
            // txtPmc
            // 
            this.txtPmc.BackColor = System.Drawing.Color.White;
            this.txtPmc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPmc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmc.ForeColor = System.Drawing.Color.Black;
            this.txtPmc.Location = new System.Drawing.Point(302, 40);
            this.txtPmc.MaxLength = 20;
            this.txtPmc.Name = "txtPmc";
            this.txtPmc.Size = new System.Drawing.Size(84, 22);
            this.txtPmc.TabIndex = 346;
            this.txtPmc.Text = "0,00";
            this.txtPmc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPmc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPmc_KeyPress_1);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(299, 21);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(78, 16);
            this.label47.TabIndex = 345;
            this.label47.Text = "Preço PMC";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(106, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(86, 16);
            this.label23.TabIndex = 326;
            this.label23.Text = "Custo Médio";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(201, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 16);
            this.label24.TabIndex = 327;
            this.label24.Text = "Último Custo";
            // 
            // txtCusIni
            // 
            this.txtCusIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCusIni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusIni.Location = new System.Drawing.Point(13, 40);
            this.txtCusIni.MaxLength = 20;
            this.txtCusIni.Name = "txtCusIni";
            this.txtCusIni.Size = new System.Drawing.Size(84, 22);
            this.txtCusIni.TabIndex = 333;
            this.txtCusIni.Text = "0,00";
            this.txtCusIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCusIni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCusIni_KeyPress);
            // 
            // txtCusMed
            // 
            this.txtCusMed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCusMed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusMed.Location = new System.Drawing.Point(109, 40);
            this.txtCusMed.MaxLength = 20;
            this.txtCusMed.Name = "txtCusMed";
            this.txtCusMed.Size = new System.Drawing.Size(84, 22);
            this.txtCusMed.TabIndex = 334;
            this.txtCusMed.Text = "0,00";
            this.txtCusMed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCusMed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCusMed_KeyPress);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtEstMin);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.txtEstIni);
            this.groupBox7.Controls.Add(this.txtEstAtu);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.txtEstMax);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(16, 17);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(212, 116);
            this.groupBox7.TabIndex = 347;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Estoque";
            // 
            // txtEstMin
            // 
            this.txtEstMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstMin.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstMin.Location = new System.Drawing.Point(115, 40);
            this.txtEstMin.MaxLength = 20;
            this.txtEstMin.Name = "txtEstMin";
            this.txtEstMin.Size = new System.Drawing.Size(84, 22);
            this.txtEstMin.TabIndex = 330;
            this.txtEstMin.Text = "0";
            this.txtEstMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEstMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstMin_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(10, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 16);
            this.label18.TabIndex = 321;
            this.label18.Text = "Est. Inicial";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(112, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 16);
            this.label19.TabIndex = 322;
            this.label19.Text = "Est. Mínimo";
            // 
            // txtEstIni
            // 
            this.txtEstIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstIni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstIni.Location = new System.Drawing.Point(13, 40);
            this.txtEstIni.MaxLength = 20;
            this.txtEstIni.Name = "txtEstIni";
            this.txtEstIni.Size = new System.Drawing.Size(84, 22);
            this.txtEstIni.TabIndex = 329;
            this.txtEstIni.Text = "0";
            this.txtEstIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEstIni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstIni_KeyPress);
            // 
            // txtEstAtu
            // 
            this.txtEstAtu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEstAtu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstAtu.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtEstAtu.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstAtu.Location = new System.Drawing.Point(115, 81);
            this.txtEstAtu.MaxLength = 18;
            this.txtEstAtu.Name = "txtEstAtu";
            this.txtEstAtu.ReadOnly = true;
            this.txtEstAtu.Size = new System.Drawing.Size(84, 22);
            this.txtEstAtu.TabIndex = 332;
            this.txtEstAtu.Text = "0";
            this.txtEstAtu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(12, 64);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 16);
            this.label20.TabIndex = 323;
            this.label20.Text = "Est. Máximo";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(112, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 16);
            this.label21.TabIndex = 324;
            this.label21.Text = "Est. Atual";
            // 
            // txtEstMax
            // 
            this.txtEstMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstMax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstMax.Location = new System.Drawing.Point(15, 81);
            this.txtEstMax.MaxLength = 20;
            this.txtEstMax.Name = "txtEstMax";
            this.txtEstMax.Size = new System.Drawing.Size(84, 22);
            this.txtEstMax.TabIndex = 331;
            this.txtEstMax.Text = "0";
            this.txtEstMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEstMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstMax_KeyPress);
            // 
            // frmCadProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadProdutos";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadProdutos_Load);
            this.Shown += new System.EventHandler(this.frmCadProdutos_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcProdutos.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.cmsProdutos.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.tpComplemento.ResumeLayout(false);
            this.tcComplemento.ResumeLayout(false);
            this.tpGeral.ResumeLayout(false);
            this.tpGeral.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tpPrecos.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUltimasCompras)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.ContextMenuStrip cmsProdutos;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.TabControl tcProdutos;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBReg;
        private System.Windows.Forms.TextBox txtBCod;
        private System.Windows.Forms.TextBox txtBDescr;
        private System.Windows.Forms.ComboBox cmbBPrinc;
        private System.Windows.Forms.ComboBox cmbBFab;
        private System.Windows.Forms.ComboBox cmbBSub;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbBClasse;
        private System.Windows.Forms.ComboBox cmbBDepto;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbFabricante;
        private System.Windows.Forms.TextBox txtFabID;
        private System.Windows.Forms.ComboBox cmbSubClas;
        private System.Windows.Forms.TextBox txtSubID;
        private System.Windows.Forms.ComboBox cmbClasse;
        private System.Windows.Forms.TextBox txtClasID;
        private System.Windows.Forms.ComboBox cmbDepto;
        private System.Windows.Forms.TextBox txtDeptoID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbUnidade;
        private System.Windows.Forms.TextBox txtDescrAbr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodBarra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProdID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.TabPage tpComplemento;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.TextBox txtCodABC;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cmbTabPrecos;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtNCM;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cmbPrinAtivo;
        private System.Windows.Forms.TextBox txtPrincID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbEspec;
        private System.Windows.Forms.TextBox txtEspcID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabControl tcComplemento;
        private System.Windows.Forms.TabPage tpGeral;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chkControlado;
        private System.Windows.Forms.CheckBox ckbUsoContinuo;
        private System.Windows.Forms.CheckBox chkBCompra;
        private System.Windows.Forms.TextBox txtRegMS;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cmbClasTera;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.TabPage tpPrecos;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtCusUlt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtPmc;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCusIni;
        private System.Windows.Forms.TextBox txtCusMed;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtEstMin;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtEstIni;
        private System.Windows.Forms.TextBox txtEstAtu;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtEstMax;
        private System.Windows.Forms.TextBox txtComissao;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.ComboBox cmbUComp;
        private System.Windows.Forms.TextBox txtEcf;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbLista;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.MaskedTextBox txtDtVenda;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtPVenda;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCompra;
        private System.Windows.Forms.ComboBox cmbTipoReceita;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox txtCest;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtCfop;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dgUltimasCompras;
        private System.Windows.Forms.CheckBox chkManipulado;
        private System.Windows.Forms.DateTimePicker dtCInicial;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtCstIpi;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtCsosn;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ComboBox cmbIcms;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtCstPis;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtCstCofins;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txtEnqIpi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DTLANC;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ABREV;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_USO_CONTINUO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_FORMULA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CONTROLADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NCM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_SITUACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_MENSAGEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.ComboBox cmbCst;
        private System.Windows.Forms.TextBox txtDcb;
        private System.Windows.Forms.TextBox txtQtdUniFP;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtCfopDev;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.CheckBox chkBloqDesconto;
        private System.Windows.Forms.CheckBox chkCaixaCartelado;
        private System.Windows.Forms.Label lblCartelado;
        private System.Windows.Forms.TextBox txtCodCartelado;
        private System.Windows.Forms.Label lblAlterado;
        private System.Windows.Forms.TextBox txtDescricaoCartelado;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cmbPortaria;
        private System.Windows.Forms.Label label54;
    }
}