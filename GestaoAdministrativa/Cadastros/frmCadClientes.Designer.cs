﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadClientes));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcClientes = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.txtBConv = new System.Windows.Forms.TextBox();
            this.txtBCidade = new System.Windows.Forms.TextBox();
            this.cmbBGrupo = new System.Windows.Forms.ComboBox();
            this.txtBCpf = new System.Windows.Forms.MaskedTextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBNome = new System.Windows.Forms.TextBox();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.dgClientes = new System.Windows.Forms.DataGridView();
            this.CF_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_TIPO_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_APELIDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRUPO_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ENDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_END_NUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_BAIRRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CEP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_FONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CELULAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_FONE_RECADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ENDERCOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NUMEROCOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_BAIRROCOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADECOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CEPCOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UFCOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ENDERENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NUMEROENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_BAIRROENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADEENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CEPENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UFENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CONTATO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POS_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CARTAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_LIMITE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_OBSERVACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DESABILITADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_INSEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsClientes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDocumento = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.label42 = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.rdbJuridica = new System.Windows.Forms.RadioButton();
            this.rdbFisica = new System.Windows.Forms.RadioButton();
            this.label34 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.cmbCondicao = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtContato = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtENum = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.cmbEUF = new System.Windows.Forms.ComboBox();
            this.txtECep = new System.Windows.Forms.MaskedTextBox();
            this.txtECidade = new System.Windows.Forms.TextBox();
            this.txtEBairro = new System.Windows.Forms.TextBox();
            this.txtEEndereco = new System.Windows.Forms.TextBox();
            this.btnEMesmo = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.txtCNum = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.cmbCUF = new System.Windows.Forms.ComboBox();
            this.txtCCep = new System.Windows.Forms.MaskedTextBox();
            this.txtCCidade = new System.Windows.Forms.TextBox();
            this.txtCBairro = new System.Windows.Forms.TextBox();
            this.txtCEndereco = new System.Windows.Forms.TextBox();
            this.btnCMesmo = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.txtRecado = new System.Windows.Forms.MaskedTextBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.cmbUF = new System.Windows.Forms.ComboBox();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.cmbGrupo = new System.Windows.Forms.ComboBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtApelido = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.txtCliID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpComplemento = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.txtLimite = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtCodCartao = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtConveniada = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcClientes.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).BeginInit();
            this.cmsClientes.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tpComplemento.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcClientes);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 21);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(5, 2);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(157, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Clientes";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(2, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 11, 0);
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcClientes
            // 
            this.tcClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcClientes.Controls.Add(this.tpGrade);
            this.tcClientes.Controls.Add(this.tpFicha);
            this.tcClientes.Controls.Add(this.tpComplemento);
            this.tcClientes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcClientes.Location = new System.Drawing.Point(6, 33);
            this.tcClientes.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tcClientes.Name = "tcClientes";
            this.tcClientes.SelectedIndex = 0;
            this.tcClientes.Size = new System.Drawing.Size(965, 499);
            this.tcClientes.TabIndex = 38;
            this.tcClientes.SelectedIndexChanged += new System.EventHandler(this.tcClientes_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.txtBConv);
            this.tpGrade.Controls.Add(this.txtBCidade);
            this.tpGrade.Controls.Add(this.cmbBGrupo);
            this.tpGrade.Controls.Add(this.txtBCpf);
            this.tpGrade.Controls.Add(this.label32);
            this.tpGrade.Controls.Add(this.label20);
            this.tpGrade.Controls.Add(this.txtBNome);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label43);
            this.tpGrade.Controls.Add(this.label38);
            this.tpGrade.Controls.Add(this.label37);
            this.tpGrade.Controls.Add(this.label36);
            this.tpGrade.Controls.Add(this.label35);
            this.tpGrade.Controls.Add(this.dgClientes);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(571, 423);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(103, 38);
            this.btnBuscar.TabIndex = 130;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(435, 436);
            this.cmbLiberado.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(125, 24);
            this.cmbLiberado.TabIndex = 129;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // txtBConv
            // 
            this.txtBConv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBConv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBConv.Location = new System.Drawing.Point(303, 438);
            this.txtBConv.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBConv.MaxLength = 18;
            this.txtBConv.Name = "txtBConv";
            this.txtBConv.Size = new System.Drawing.Size(114, 22);
            this.txtBConv.TabIndex = 128;
            this.txtBConv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBConv_KeyPress);
            // 
            // txtBCidade
            // 
            this.txtBCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBCidade.Location = new System.Drawing.Point(6, 438);
            this.txtBCidade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBCidade.MaxLength = 30;
            this.txtBCidade.Name = "txtBCidade";
            this.txtBCidade.Size = new System.Drawing.Size(282, 22);
            this.txtBCidade.TabIndex = 127;
            this.txtBCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCidade_KeyPress);
            // 
            // cmbBGrupo
            // 
            this.cmbBGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBGrupo.FormattingEnabled = true;
            this.cmbBGrupo.Location = new System.Drawing.Point(744, 393);
            this.cmbBGrupo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbBGrupo.Name = "cmbBGrupo";
            this.cmbBGrupo.Size = new System.Drawing.Size(207, 24);
            this.cmbBGrupo.TabIndex = 126;
            this.cmbBGrupo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBGrupo_KeyPress);
            // 
            // txtBCpf
            // 
            this.txtBCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCpf.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCpf.Location = new System.Drawing.Point(571, 395);
            this.txtBCpf.Name = "txtBCpf";
            this.txtBCpf.Size = new System.Drawing.Size(153, 22);
            this.txtBCpf.TabIndex = 125;
            this.txtBCpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCpf_KeyPress);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(741, 376);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 16);
            this.label32.TabIndex = 114;
            this.label32.Text = "Grupo";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(4, 420);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 16);
            this.label20.TabIndex = 112;
            this.label20.Text = "Cidade";
            // 
            // txtBNome
            // 
            this.txtBNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBNome.Location = new System.Drawing.Point(128, 395);
            this.txtBNome.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBNome.MaxLength = 60;
            this.txtBNome.Name = "txtBNome";
            this.txtBNome.Size = new System.Drawing.Size(432, 22);
            this.txtBNome.TabIndex = 96;
            this.txtBNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNome_KeyPress);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(5, 395);
            this.txtBID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBID.MaxLength = 18;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(114, 22);
            this.txtBID.TabIndex = 95;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(432, 418);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 16);
            this.label43.TabIndex = 103;
            this.label43.Text = "Liberado";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(300, 420);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(117, 16);
            this.label38.TabIndex = 90;
            this.label38.Text = "Cód. Conveniada";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(568, 376);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(72, 16);
            this.label37.TabIndex = 89;
            this.label37.Text = "CPF/CNPJ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(125, 376);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(45, 16);
            this.label36.TabIndex = 88;
            this.label36.Text = "Nome";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(2, 377);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(70, 16);
            this.label35.TabIndex = 87;
            this.label35.Text = "Cliente ID";
            // 
            // dgClientes
            // 
            this.dgClientes.AllowUserToAddRows = false;
            this.dgClientes.AllowUserToDeleteRows = false;
            this.dgClientes.AllowUserToOrderColumns = true;
            this.dgClientes.AllowUserToResizeColumns = false;
            this.dgClientes.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            this.dgClientes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgClientes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CF_ID,
            this.CF_CODIGO,
            this.CF_TIPO_DOCTO,
            this.CF_DOCTO,
            this.CF_NOME,
            this.CF_APELIDO,
            this.GRUPO_DESCR,
            this.CF_ENDER,
            this.CF_END_NUM,
            this.CF_BAIRRO,
            this.CF_CIDADE,
            this.CF_CEP,
            this.CF_UF,
            this.CF_FONE,
            this.CF_CELULAR,
            this.CF_FONE_RECADO,
            this.CF_ENDERCOB,
            this.CF_NUMEROCOB,
            this.CF_BAIRROCOB,
            this.CF_CIDADECOB,
            this.CF_CEPCOB,
            this.CF_UFCOB,
            this.CF_ENDERENT,
            this.CF_NUMEROENT,
            this.CF_BAIRROENT,
            this.CF_CIDADEENT,
            this.CF_CEPENT,
            this.CF_UFENT,
            this.CF_EMAIL,
            this.CF_CONTATO,
            this.POS_DESCRICAO,
            this.CON_CODIGO,
            this.CON_CARTAO,
            this.CF_LIMITE,
            this.CF_OBSERVACAO,
            this.CF_DESABILITADO,
            this.DAT_ALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO,
            this.CF_STATUS,
            this.CF_INSEST});
            this.dgClientes.ContextMenuStrip = this.cmsClientes;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgClientes.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgClientes.GridColor = System.Drawing.Color.LightGray;
            this.dgClientes.Location = new System.Drawing.Point(6, 6);
            this.dgClientes.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgClientes.MultiSelect = false;
            this.dgClientes.Name = "dgClientes";
            this.dgClientes.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgClientes.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgClientes.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgClientes.Size = new System.Drawing.Size(945, 364);
            this.dgClientes.TabIndex = 1;
            this.dgClientes.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgClientes_CellMouseClick);
            this.dgClientes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgClientes_CellMouseDoubleClick);
            this.dgClientes.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgClientes_ColumnHeaderMouseClick);
            this.dgClientes.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgClientes_RowPrePaint);
            this.dgClientes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgClientes_KeyDown);
            // 
            // CF_ID
            // 
            this.CF_ID.DataPropertyName = "CF_ID";
            this.CF_ID.HeaderText = "Cliente ID";
            this.CF_ID.MaxInputLength = 18;
            this.CF_ID.Name = "CF_ID";
            this.CF_ID.ReadOnly = true;
            // 
            // CF_CODIGO
            // 
            this.CF_CODIGO.DataPropertyName = "CF_CODIGO";
            this.CF_CODIGO.HeaderText = "Cod. Cliente";
            this.CF_CODIGO.Name = "CF_CODIGO";
            this.CF_CODIGO.ReadOnly = true;
            this.CF_CODIGO.Width = 130;
            // 
            // CF_TIPO_DOCTO
            // 
            this.CF_TIPO_DOCTO.DataPropertyName = "CF_TIPO_DOCTO";
            this.CF_TIPO_DOCTO.HeaderText = "Tipo Docto.";
            this.CF_TIPO_DOCTO.Name = "CF_TIPO_DOCTO";
            this.CF_TIPO_DOCTO.ReadOnly = true;
            this.CF_TIPO_DOCTO.Width = 150;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "N° Docto";
            this.CF_DOCTO.MaxInputLength = 14;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.Width = 150;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome";
            this.CF_NOME.MaxInputLength = 60;
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.Width = 350;
            // 
            // CF_APELIDO
            // 
            this.CF_APELIDO.DataPropertyName = "CF_APELIDO";
            this.CF_APELIDO.HeaderText = "Apelido";
            this.CF_APELIDO.MaxInputLength = 30;
            this.CF_APELIDO.Name = "CF_APELIDO";
            this.CF_APELIDO.ReadOnly = true;
            this.CF_APELIDO.Width = 200;
            // 
            // GRUPO_DESCR
            // 
            this.GRUPO_DESCR.DataPropertyName = "GRUPO_DESCR";
            this.GRUPO_DESCR.HeaderText = "Grupo";
            this.GRUPO_DESCR.MaxInputLength = 30;
            this.GRUPO_DESCR.Name = "GRUPO_DESCR";
            this.GRUPO_DESCR.ReadOnly = true;
            this.GRUPO_DESCR.Width = 130;
            // 
            // CF_ENDER
            // 
            this.CF_ENDER.DataPropertyName = "CF_ENDER";
            this.CF_ENDER.HeaderText = "Endereço";
            this.CF_ENDER.MaxInputLength = 60;
            this.CF_ENDER.Name = "CF_ENDER";
            this.CF_ENDER.ReadOnly = true;
            this.CF_ENDER.Width = 250;
            // 
            // CF_END_NUM
            // 
            this.CF_END_NUM.DataPropertyName = "CF_END_NUM";
            this.CF_END_NUM.HeaderText = "Número";
            this.CF_END_NUM.Name = "CF_END_NUM";
            this.CF_END_NUM.ReadOnly = true;
            // 
            // CF_BAIRRO
            // 
            this.CF_BAIRRO.DataPropertyName = "CF_BAIRRO";
            this.CF_BAIRRO.HeaderText = "Bairro";
            this.CF_BAIRRO.MaxInputLength = 20;
            this.CF_BAIRRO.Name = "CF_BAIRRO";
            this.CF_BAIRRO.ReadOnly = true;
            this.CF_BAIRRO.Width = 150;
            // 
            // CF_CIDADE
            // 
            this.CF_CIDADE.DataPropertyName = "CF_CIDADE";
            this.CF_CIDADE.HeaderText = "Cidade";
            this.CF_CIDADE.MaxInputLength = 30;
            this.CF_CIDADE.Name = "CF_CIDADE";
            this.CF_CIDADE.ReadOnly = true;
            this.CF_CIDADE.Width = 200;
            // 
            // CF_CEP
            // 
            this.CF_CEP.DataPropertyName = "CF_CEP";
            this.CF_CEP.HeaderText = "CEP";
            this.CF_CEP.MaxInputLength = 9;
            this.CF_CEP.Name = "CF_CEP";
            this.CF_CEP.ReadOnly = true;
            // 
            // CF_UF
            // 
            this.CF_UF.DataPropertyName = "CF_UF";
            this.CF_UF.HeaderText = "UF";
            this.CF_UF.MaxInputLength = 2;
            this.CF_UF.Name = "CF_UF";
            this.CF_UF.ReadOnly = true;
            this.CF_UF.Width = 50;
            // 
            // CF_FONE
            // 
            this.CF_FONE.DataPropertyName = "CF_FONE";
            this.CF_FONE.HeaderText = "Telefone";
            this.CF_FONE.MaxInputLength = 14;
            this.CF_FONE.Name = "CF_FONE";
            this.CF_FONE.ReadOnly = true;
            // 
            // CF_CELULAR
            // 
            this.CF_CELULAR.DataPropertyName = "CF_CELULAR";
            this.CF_CELULAR.HeaderText = "Celular";
            this.CF_CELULAR.MaxInputLength = 15;
            this.CF_CELULAR.Name = "CF_CELULAR";
            this.CF_CELULAR.ReadOnly = true;
            // 
            // CF_FONE_RECADO
            // 
            this.CF_FONE_RECADO.DataPropertyName = "CF_FONE_RECADO";
            this.CF_FONE_RECADO.HeaderText = "Recado";
            this.CF_FONE_RECADO.MaxInputLength = 14;
            this.CF_FONE_RECADO.Name = "CF_FONE_RECADO";
            this.CF_FONE_RECADO.ReadOnly = true;
            // 
            // CF_ENDERCOB
            // 
            this.CF_ENDERCOB.DataPropertyName = "CF_ENDERCOB";
            this.CF_ENDERCOB.HeaderText = "End. de Cobrança";
            this.CF_ENDERCOB.MaxInputLength = 60;
            this.CF_ENDERCOB.Name = "CF_ENDERCOB";
            this.CF_ENDERCOB.ReadOnly = true;
            this.CF_ENDERCOB.Width = 250;
            // 
            // CF_NUMEROCOB
            // 
            this.CF_NUMEROCOB.DataPropertyName = "CF_NUMEROCOB";
            this.CF_NUMEROCOB.HeaderText = "N° Cobrança";
            this.CF_NUMEROCOB.Name = "CF_NUMEROCOB";
            this.CF_NUMEROCOB.ReadOnly = true;
            this.CF_NUMEROCOB.Width = 110;
            // 
            // CF_BAIRROCOB
            // 
            this.CF_BAIRROCOB.DataPropertyName = "CF_BAIRROCOB";
            this.CF_BAIRROCOB.HeaderText = "Bairro Cobr.";
            this.CF_BAIRROCOB.MaxInputLength = 20;
            this.CF_BAIRROCOB.Name = "CF_BAIRROCOB";
            this.CF_BAIRROCOB.ReadOnly = true;
            this.CF_BAIRROCOB.Width = 150;
            // 
            // CF_CIDADECOB
            // 
            this.CF_CIDADECOB.DataPropertyName = "CF_CIDADECOB";
            this.CF_CIDADECOB.HeaderText = "Cidade Cobr.";
            this.CF_CIDADECOB.MaxInputLength = 30;
            this.CF_CIDADECOB.Name = "CF_CIDADECOB";
            this.CF_CIDADECOB.ReadOnly = true;
            this.CF_CIDADECOB.Width = 200;
            // 
            // CF_CEPCOB
            // 
            this.CF_CEPCOB.DataPropertyName = "CF_CEPCOB";
            this.CF_CEPCOB.HeaderText = "CEP Cobr.";
            this.CF_CEPCOB.MaxInputLength = 9;
            this.CF_CEPCOB.Name = "CF_CEPCOB";
            this.CF_CEPCOB.ReadOnly = true;
            // 
            // CF_UFCOB
            // 
            this.CF_UFCOB.DataPropertyName = "CF_UFCOB";
            this.CF_UFCOB.HeaderText = "UF Cobr.";
            this.CF_UFCOB.MaxInputLength = 2;
            this.CF_UFCOB.Name = "CF_UFCOB";
            this.CF_UFCOB.ReadOnly = true;
            this.CF_UFCOB.Width = 85;
            // 
            // CF_ENDERENT
            // 
            this.CF_ENDERENT.DataPropertyName = "CF_ENDERENT";
            this.CF_ENDERENT.HeaderText = "End. de Entrega";
            this.CF_ENDERENT.MaxInputLength = 60;
            this.CF_ENDERENT.Name = "CF_ENDERENT";
            this.CF_ENDERENT.ReadOnly = true;
            this.CF_ENDERENT.Width = 250;
            // 
            // CF_NUMEROENT
            // 
            this.CF_NUMEROENT.DataPropertyName = "CF_NUMEROENT";
            this.CF_NUMEROENT.HeaderText = "N° Entrega";
            this.CF_NUMEROENT.Name = "CF_NUMEROENT";
            this.CF_NUMEROENT.ReadOnly = true;
            // 
            // CF_BAIRROENT
            // 
            this.CF_BAIRROENT.DataPropertyName = "CF_BAIRROENT";
            this.CF_BAIRROENT.HeaderText = "Bairro Entr.";
            this.CF_BAIRROENT.MaxInputLength = 20;
            this.CF_BAIRROENT.Name = "CF_BAIRROENT";
            this.CF_BAIRROENT.ReadOnly = true;
            this.CF_BAIRROENT.Width = 150;
            // 
            // CF_CIDADEENT
            // 
            this.CF_CIDADEENT.DataPropertyName = "CF_CIDADEENT";
            this.CF_CIDADEENT.HeaderText = "Cidade  Entr.";
            this.CF_CIDADEENT.MaxInputLength = 30;
            this.CF_CIDADEENT.Name = "CF_CIDADEENT";
            this.CF_CIDADEENT.ReadOnly = true;
            this.CF_CIDADEENT.Width = 200;
            // 
            // CF_CEPENT
            // 
            this.CF_CEPENT.DataPropertyName = "CF_CEPENT";
            this.CF_CEPENT.HeaderText = "CEP  Entr.";
            this.CF_CEPENT.MaxInputLength = 9;
            this.CF_CEPENT.Name = "CF_CEPENT";
            this.CF_CEPENT.ReadOnly = true;
            // 
            // CF_UFENT
            // 
            this.CF_UFENT.DataPropertyName = "CF_UFENT";
            this.CF_UFENT.HeaderText = "UF  Entr.";
            this.CF_UFENT.MaxInputLength = 2;
            this.CF_UFENT.Name = "CF_UFENT";
            this.CF_UFENT.ReadOnly = true;
            this.CF_UFENT.Width = 85;
            // 
            // CF_EMAIL
            // 
            this.CF_EMAIL.DataPropertyName = "CF_EMAIL";
            this.CF_EMAIL.HeaderText = "E-mail";
            this.CF_EMAIL.MaxInputLength = 30;
            this.CF_EMAIL.Name = "CF_EMAIL";
            this.CF_EMAIL.ReadOnly = true;
            this.CF_EMAIL.Width = 200;
            // 
            // CF_CONTATO
            // 
            this.CF_CONTATO.DataPropertyName = "CF_CONTATO";
            this.CF_CONTATO.HeaderText = "Contato";
            this.CF_CONTATO.MaxInputLength = 30;
            this.CF_CONTATO.Name = "CF_CONTATO";
            this.CF_CONTATO.ReadOnly = true;
            this.CF_CONTATO.Width = 200;
            // 
            // POS_DESCRICAO
            // 
            this.POS_DESCRICAO.DataPropertyName = "POS_DESCRICAO";
            this.POS_DESCRICAO.HeaderText = "Condição de Pagamento";
            this.POS_DESCRICAO.MaxInputLength = 30;
            this.POS_DESCRICAO.Name = "POS_DESCRICAO";
            this.POS_DESCRICAO.ReadOnly = true;
            this.POS_DESCRICAO.Width = 200;
            // 
            // CON_CODIGO
            // 
            this.CON_CODIGO.DataPropertyName = "CON_CODIGO";
            this.CON_CODIGO.HeaderText = "Cód. Conveniada";
            this.CON_CODIGO.MaxInputLength = 18;
            this.CON_CODIGO.Name = "CON_CODIGO";
            this.CON_CODIGO.ReadOnly = true;
            this.CON_CODIGO.Width = 135;
            // 
            // CON_CARTAO
            // 
            this.CON_CARTAO.DataPropertyName = "CON_CARTAO";
            this.CON_CARTAO.HeaderText = "Cód. Cartão";
            this.CON_CARTAO.MaxInputLength = 18;
            this.CON_CARTAO.Name = "CON_CARTAO";
            this.CON_CARTAO.ReadOnly = true;
            this.CON_CARTAO.Width = 125;
            // 
            // CF_LIMITE
            // 
            this.CF_LIMITE.DataPropertyName = "CF_LIMITE";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.CF_LIMITE.DefaultCellStyle = dataGridViewCellStyle3;
            this.CF_LIMITE.HeaderText = "Limite";
            this.CF_LIMITE.MaxInputLength = 10;
            this.CF_LIMITE.Name = "CF_LIMITE";
            this.CF_LIMITE.ReadOnly = true;
            // 
            // CF_OBSERVACAO
            // 
            this.CF_OBSERVACAO.DataPropertyName = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.HeaderText = "Observação";
            this.CF_OBSERVACAO.MaxInputLength = 300;
            this.CF_OBSERVACAO.Name = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.ReadOnly = true;
            this.CF_OBSERVACAO.Width = 500;
            // 
            // CF_DESABILITADO
            // 
            this.CF_DESABILITADO.DataPropertyName = "CF_DESABILITADO";
            this.CF_DESABILITADO.HeaderText = "Liberado";
            this.CF_DESABILITADO.MaxInputLength = 1;
            this.CF_DESABILITADO.Name = "CF_DESABILITADO";
            this.CF_DESABILITADO.ReadOnly = true;
            this.CF_DESABILITADO.Width = 75;
            // 
            // DAT_ALTERACAO
            // 
            this.DAT_ALTERACAO.DataPropertyName = "DAT_ALTERACAO";
            dataGridViewCellStyle4.Format = "G";
            this.DAT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DAT_ALTERACAO.HeaderText = "Data Alteração";
            this.DAT_ALTERACAO.MaxInputLength = 30;
            this.DAT_ALTERACAO.Name = "DAT_ALTERACAO";
            this.DAT_ALTERACAO.ReadOnly = true;
            this.DAT_ALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle5.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle5;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 30;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // CF_STATUS
            // 
            this.CF_STATUS.DataPropertyName = "CF_STATUS";
            this.CF_STATUS.HeaderText = "Status";
            this.CF_STATUS.Name = "CF_STATUS";
            this.CF_STATUS.ReadOnly = true;
            this.CF_STATUS.Visible = false;
            // 
            // CF_INSEST
            // 
            this.CF_INSEST.DataPropertyName = "CF_INSEST";
            this.CF_INSEST.HeaderText = "Insc. Estadual/R.G.";
            this.CF_INSEST.Name = "CF_INSEST";
            this.CF_INSEST.ReadOnly = true;
            this.CF_INSEST.Width = 150;
            // 
            // cmsClientes
            // 
            this.cmsClientes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsClientes.Name = "cmsClientes";
            this.cmsClientes.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox7);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(7, 385);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Size = new System.Drawing.Size(945, 77);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.pictureBox1);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this.pictureBox13);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(574, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(365, 46);
            this.groupBox7.TabIndex = 193;
            this.groupBox7.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(173, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 195;
            this.pictureBox1.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(16, 21);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(16, 16);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox13.TabIndex = 191;
            this.pictureBox13.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(232, 44);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(129, 22);
            this.txtUsuario.TabIndex = 37;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(13, 44);
            this.txtData.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 36;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(10, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 200;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(229, 25);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 200;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtDocumento);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.cmbStatus);
            this.groupBox2.Controls.Add(this.pictureBox18);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.pictureBox17);
            this.groupBox2.Controls.Add(this.txtNumero);
            this.groupBox2.Controls.Add(this.pictureBox15);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Controls.Add(this.rdbJuridica);
            this.groupBox2.Controls.Add(this.rdbFisica);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.pictureBox14);
            this.groupBox2.Controls.Add(this.cmbCondicao);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.txtContato);
            this.groupBox2.Controls.Add(this.txtEmail);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.txtRecado);
            this.groupBox2.Controls.Add(this.txtCelular);
            this.groupBox2.Controls.Add(this.txtTelefone);
            this.groupBox2.Controls.Add(this.cmbUF);
            this.groupBox2.Controls.Add(this.txtCep);
            this.groupBox2.Controls.Add(this.txtCidade);
            this.groupBox2.Controls.Add(this.txtBairro);
            this.groupBox2.Controls.Add(this.txtEndereco);
            this.groupBox2.Controls.Add(this.cmbGrupo);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtApelido);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtCpf);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.txtCliID);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(5, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Size = new System.Drawing.Size(947, 378);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // txtDocumento
            // 
            this.txtDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocumento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocumento.Location = new System.Drawing.Point(428, 103);
            this.txtDocumento.MaxLength = 30;
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.Size = new System.Drawing.Size(123, 22);
            this.txtDocumento.TabIndex = 9;
            this.txtDocumento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocumento_KeyPress);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(425, 86);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(126, 16);
            this.label46.TabIndex = 242;
            this.label46.Text = "Insc. Estadual/R.G.";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(250, 131);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 240;
            this.pictureBox5.TabStop = false;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "CLIENTE ATIVO",
            "CLIENTE BLOQUEADO ",
            "CLIENTE / FORNECEDOR ATIVO",
            "CLIENTE / FORNECEDOR BLOQUEADO"});
            this.cmbStatus.Location = new System.Drawing.Point(16, 104);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(250, 24);
            this.cmbStatus.TabIndex = 7;
            this.cmbStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbStatus_KeyPress);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(57, 87);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(16, 16);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox18.TabIndex = 239;
            this.pictureBox18.TabStop = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(13, 87);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 16);
            this.label42.TabIndex = 238;
            this.label42.Text = "Status";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(535, 132);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(16, 16);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox17.TabIndex = 234;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.MouseHover += new System.EventHandler(this.pictureBox17_MouseHover);
            // 
            // txtNumero
            // 
            this.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumero.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.Location = new System.Drawing.Point(481, 150);
            this.txtNumero.MaxLength = 6;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(89, 22);
            this.txtNumero.TabIndex = 14;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(61, 131);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(16, 16);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox15.TabIndex = 233;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(478, 134);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 16);
            this.label39.TabIndex = 232;
            this.label39.Text = "Número";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(374, 43);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(16, 16);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox12.TabIndex = 231;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // rdbJuridica
            // 
            this.rdbJuridica.AutoSize = true;
            this.rdbJuridica.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbJuridica.ForeColor = System.Drawing.Color.Black;
            this.rdbJuridica.Location = new System.Drawing.Point(230, 64);
            this.rdbJuridica.Name = "rdbJuridica";
            this.rdbJuridica.Size = new System.Drawing.Size(77, 20);
            this.rdbJuridica.TabIndex = 3;
            this.rdbJuridica.TabStop = true;
            this.rdbJuridica.Text = "Jurídica";
            this.rdbJuridica.UseVisualStyleBackColor = true;
            this.rdbJuridica.CheckedChanged += new System.EventHandler(this.rdbJuridica_CheckedChanged);
            // 
            // rdbFisica
            // 
            this.rdbFisica.AutoSize = true;
            this.rdbFisica.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbFisica.ForeColor = System.Drawing.Color.Black;
            this.rdbFisica.Location = new System.Drawing.Point(163, 64);
            this.rdbFisica.Name = "rdbFisica";
            this.rdbFisica.Size = new System.Drawing.Size(63, 20);
            this.rdbFisica.TabIndex = 2;
            this.rdbFisica.TabStop = true;
            this.rdbFisica.Text = "Fisica";
            this.rdbFisica.UseVisualStyleBackColor = true;
            this.rdbFisica.CheckedChanged += new System.EventHandler(this.rdbFisica_CheckedChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(159, 43);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(80, 16);
            this.label34.TabIndex = 228;
            this.label34.Text = "Tipo Docto.";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(322, 87);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(16, 16);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox14.TabIndex = 227;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // cmbCondicao
            // 
            this.cmbCondicao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCondicao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCondicao.FormattingEnabled = true;
            this.cmbCondicao.Items.AddRange(new object[] {
            ""});
            this.cmbCondicao.Location = new System.Drawing.Point(621, 320);
            this.cmbCondicao.Name = "cmbCondicao";
            this.cmbCondicao.Size = new System.Drawing.Size(309, 24);
            this.cmbCondicao.TabIndex = 34;
            this.cmbCondicao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCondicao_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(618, 303);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(165, 16);
            this.label30.TabIndex = 225;
            this.label30.Text = "Condição de Pagamento";
            // 
            // txtContato
            // 
            this.txtContato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContato.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContato.Location = new System.Drawing.Point(342, 322);
            this.txtContato.MaxLength = 28;
            this.txtContato.Name = "txtContato";
            this.txtContato.Size = new System.Drawing.Size(270, 22);
            this.txtContato.TabIndex = 33;
            this.txtContato.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContato_KeyPress);
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEmail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(16, 322);
            this.txtEmail.MaxLength = 30;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(313, 22);
            this.txtEmail.TabIndex = 32;
            this.txtEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail_KeyPress);
            this.txtEmail.Validating += new System.ComponentModel.CancelEventHandler(this.txtEmail_Validating);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtENum);
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.cmbEUF);
            this.groupBox5.Controls.Add(this.txtECep);
            this.groupBox5.Controls.Add(this.txtECidade);
            this.groupBox5.Controls.Add(this.txtEBairro);
            this.groupBox5.Controls.Add(this.txtEEndereco);
            this.groupBox5.Controls.Add(this.btnEMesmo);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.ForeColor = System.Drawing.Color.Black;
            this.groupBox5.Location = new System.Drawing.Point(481, 178);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(449, 122);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Endereço de Entrega/Retirada";
            // 
            // txtENum
            // 
            this.txtENum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtENum.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtENum.Location = new System.Drawing.Point(367, 42);
            this.txtENum.MaxLength = 6;
            this.txtENum.Name = "txtENum";
            this.txtENum.Size = new System.Drawing.Size(75, 22);
            this.txtENum.TabIndex = 28;
            this.txtENum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtENum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtENum_KeyPress);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(364, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(22, 16);
            this.label41.TabIndex = 236;
            this.label41.Text = "N°";
            // 
            // cmbEUF
            // 
            this.cmbEUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEUF.FormattingEnabled = true;
            this.cmbEUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbEUF.Location = new System.Drawing.Point(367, 87);
            this.cmbEUF.Name = "cmbEUF";
            this.cmbEUF.Size = new System.Drawing.Size(75, 24);
            this.cmbEUF.TabIndex = 31;
            this.cmbEUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEUF_KeyPress);
            // 
            // txtECep
            // 
            this.txtECep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtECep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtECep.Location = new System.Drawing.Point(6, 42);
            this.txtECep.Mask = "00000-000";
            this.txtECep.Name = "txtECep";
            this.txtECep.Size = new System.Drawing.Size(100, 22);
            this.txtECep.TabIndex = 26;
            this.txtECep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtECep_KeyPress);
            // 
            // txtECidade
            // 
            this.txtECidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtECidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtECidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtECidade.Location = new System.Drawing.Point(6, 89);
            this.txtECidade.MaxLength = 40;
            this.txtECidade.Name = "txtECidade";
            this.txtECidade.Size = new System.Drawing.Size(178, 22);
            this.txtECidade.TabIndex = 29;
            this.txtECidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtECidade_KeyPress);
            // 
            // txtEBairro
            // 
            this.txtEBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEBairro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEBairro.Location = new System.Drawing.Point(190, 89);
            this.txtEBairro.MaxLength = 20;
            this.txtEBairro.Name = "txtEBairro";
            this.txtEBairro.Size = new System.Drawing.Size(171, 22);
            this.txtEBairro.TabIndex = 30;
            this.txtEBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEBairro_KeyPress);
            // 
            // txtEEndereco
            // 
            this.txtEEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEEndereco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEEndereco.Location = new System.Drawing.Point(111, 42);
            this.txtEEndereco.MaxLength = 40;
            this.txtEEndereco.Name = "txtEEndereco";
            this.txtEEndereco.Size = new System.Drawing.Size(250, 22);
            this.txtEEndereco.TabIndex = 27;
            this.txtEEndereco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEEndereco_KeyPress);
            // 
            // btnEMesmo
            // 
            this.btnEMesmo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEMesmo.Location = new System.Drawing.Point(367, 0);
            this.btnEMesmo.Name = "btnEMesmo";
            this.btnEMesmo.Size = new System.Drawing.Size(75, 23);
            this.btnEMesmo.TabIndex = 25;
            this.btnEMesmo.Text = "O Mesmo";
            this.btnEMesmo.UseVisualStyleBackColor = true;
            this.btnEMesmo.Click += new System.EventHandler(this.btnEMesmo_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(3, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 16);
            this.label25.TabIndex = 184;
            this.label25.Text = "CEP";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(108, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 16);
            this.label26.TabIndex = 180;
            this.label26.Text = "Endereço";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(187, 68);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 16);
            this.label27.TabIndex = 183;
            this.label27.Text = "Bairro";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(368, 68);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 16);
            this.label28.TabIndex = 182;
            this.label28.Text = "UF";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(3, 70);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 16);
            this.label29.TabIndex = 181;
            this.label29.Text = "Cidade";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pictureBox16);
            this.groupBox4.Controls.Add(this.txtCNum);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.pictureBox11);
            this.groupBox4.Controls.Add(this.pictureBox10);
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Controls.Add(this.cmbCUF);
            this.groupBox4.Controls.Add(this.txtCCep);
            this.groupBox4.Controls.Add(this.txtCCidade);
            this.groupBox4.Controls.Add(this.txtCBairro);
            this.groupBox4.Controls.Add(this.txtCEndereco);
            this.groupBox4.Controls.Add(this.btnCMesmo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(16, 178);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(449, 122);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Endereço para Cobrança/Pagamento";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(390, 23);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(16, 16);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox16.TabIndex = 235;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseHover += new System.EventHandler(this.pictureBox16_MouseHover);
            // 
            // txtCNum
            // 
            this.txtCNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCNum.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNum.Location = new System.Drawing.Point(369, 42);
            this.txtCNum.MaxLength = 6;
            this.txtCNum.Name = "txtCNum";
            this.txtCNum.Size = new System.Drawing.Size(74, 22);
            this.txtCNum.TabIndex = 21;
            this.txtCNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCNum_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(366, 23);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(22, 16);
            this.label40.TabIndex = 234;
            this.label40.Text = "N°";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(390, 70);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 194;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(41, 23);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 193;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(66, 70);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 192;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(184, 23);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // cmbCUF
            // 
            this.cmbCUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCUF.FormattingEnabled = true;
            this.cmbCUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbCUF.Location = new System.Drawing.Point(369, 87);
            this.cmbCUF.Name = "cmbCUF";
            this.cmbCUF.Size = new System.Drawing.Size(74, 24);
            this.cmbCUF.TabIndex = 24;
            this.cmbCUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCUF_KeyPress);
            // 
            // txtCCep
            // 
            this.txtCCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCCep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCep.Location = new System.Drawing.Point(11, 42);
            this.txtCCep.Mask = "00000-000";
            this.txtCCep.Name = "txtCCep";
            this.txtCCep.Size = new System.Drawing.Size(100, 22);
            this.txtCCep.TabIndex = 19;
            this.txtCCep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCCep_KeyPress);
            // 
            // txtCCidade
            // 
            this.txtCCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCidade.Location = new System.Drawing.Point(11, 89);
            this.txtCCidade.MaxLength = 40;
            this.txtCCidade.Name = "txtCCidade";
            this.txtCCidade.Size = new System.Drawing.Size(178, 22);
            this.txtCCidade.TabIndex = 22;
            this.txtCCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCCidade_KeyPress);
            // 
            // txtCBairro
            // 
            this.txtCBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCBairro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCBairro.Location = new System.Drawing.Point(195, 89);
            this.txtCBairro.MaxLength = 20;
            this.txtCBairro.Name = "txtCBairro";
            this.txtCBairro.Size = new System.Drawing.Size(165, 22);
            this.txtCBairro.TabIndex = 23;
            this.txtCBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCBairro_KeyPress);
            // 
            // txtCEndereco
            // 
            this.txtCEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCEndereco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEndereco.Location = new System.Drawing.Point(117, 42);
            this.txtCEndereco.MaxLength = 40;
            this.txtCEndereco.Name = "txtCEndereco";
            this.txtCEndereco.Size = new System.Drawing.Size(243, 22);
            this.txtCEndereco.TabIndex = 20;
            this.txtCEndereco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCEndereco_KeyPress);
            // 
            // btnCMesmo
            // 
            this.btnCMesmo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCMesmo.Location = new System.Drawing.Point(368, 1);
            this.btnCMesmo.Name = "btnCMesmo";
            this.btnCMesmo.Size = new System.Drawing.Size(75, 23);
            this.btnCMesmo.TabIndex = 18;
            this.btnCMesmo.Text = "O Mesmo";
            this.btnCMesmo.UseVisualStyleBackColor = true;
            this.btnCMesmo.Click += new System.EventHandler(this.btnCMesmo_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(8, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 16);
            this.label15.TabIndex = 184;
            this.label15.Text = "CEP";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(114, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 16);
            this.label24.TabIndex = 180;
            this.label24.Text = "Endereço";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(192, 70);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 183;
            this.label21.Text = "Bairro";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(363, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(25, 16);
            this.label22.TabIndex = 182;
            this.label22.Text = "UF";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(12, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 16);
            this.label23.TabIndex = 181;
            this.label23.Text = "Cidade";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(339, 303);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 16);
            this.label19.TabIndex = 219;
            this.label19.Text = "Contato";
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(16, 350);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 35;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // txtRecado
            // 
            this.txtRecado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecado.Location = new System.Drawing.Point(812, 150);
            this.txtRecado.Mask = "(99) 0000-0000";
            this.txtRecado.Name = "txtRecado";
            this.txtRecado.Size = new System.Drawing.Size(111, 22);
            this.txtRecado.TabIndex = 17;
            this.txtRecado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRecado_KeyPress);
            // 
            // txtCelular
            // 
            this.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCelular.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.Location = new System.Drawing.Point(695, 150);
            this.txtCelular.Mask = "(99) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(111, 22);
            this.txtCelular.TabIndex = 16;
            this.txtCelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCelular_KeyPress);
            // 
            // txtTelefone
            // 
            this.txtTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.Location = new System.Drawing.Point(576, 150);
            this.txtTelefone.Mask = "(99) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(113, 22);
            this.txtTelefone.TabIndex = 15;
            this.txtTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone_KeyPress);
            // 
            // cmbUF
            // 
            this.cmbUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUF.FormattingEnabled = true;
            this.cmbUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbUF.Location = new System.Drawing.Point(385, 150);
            this.cmbUF.Name = "cmbUF";
            this.cmbUF.Size = new System.Drawing.Size(74, 24);
            this.cmbUF.TabIndex = 13;
            this.cmbUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUF_KeyPress);
            // 
            // txtCep
            // 
            this.txtCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCep.Location = new System.Drawing.Point(557, 103);
            this.txtCep.Mask = "00000-000";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(66, 22);
            this.txtCep.TabIndex = 10;
            this.txtCep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCep_KeyPress);
            // 
            // txtCidade
            // 
            this.txtCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.Location = new System.Drawing.Point(196, 150);
            this.txtCidade.MaxLength = 30;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(180, 22);
            this.txtCidade.TabIndex = 12;
            this.txtCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCidade_KeyPress);
            // 
            // txtBairro
            // 
            this.txtBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBairro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairro.Location = new System.Drawing.Point(16, 150);
            this.txtBairro.MaxLength = 20;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(174, 22);
            this.txtBairro.TabIndex = 11;
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBairro_KeyPress);
            // 
            // txtEndereco
            // 
            this.txtEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndereco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.Location = new System.Drawing.Point(628, 104);
            this.txtEndereco.MaxLength = 60;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(302, 22);
            this.txtEndereco.TabIndex = 10;
            this.txtEndereco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEndereco_KeyPress);
            // 
            // cmbGrupo
            // 
            this.cmbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrupo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGrupo.FormattingEnabled = true;
            this.cmbGrupo.Location = new System.Drawing.Point(272, 103);
            this.cmbGrupo.Name = "cmbGrupo";
            this.cmbGrupo.Size = new System.Drawing.Size(150, 24);
            this.cmbGrupo.TabIndex = 8;
            this.cmbGrupo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbGrupo_KeyPress);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(406, 131);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 207;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(589, 84);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 206;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(693, 87);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 204;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(492, 43);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 203;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(84, 44);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 201;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(13, 303);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 203;
            this.label14.Text = "E-mail";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(809, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 177;
            this.label2.Text = "Recado";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(269, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 174;
            this.label1.Text = "Grupo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(692, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 16);
            this.label13.TabIndex = 166;
            this.label13.Text = "Celular";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(554, 85);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 16);
            this.label18.TabIndex = 153;
            this.label18.Text = "CEP";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(13, 134);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 16);
            this.label17.TabIndex = 152;
            this.label17.Text = "Bairro";
            // 
            // txtApelido
            // 
            this.txtApelido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApelido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApelido.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApelido.Location = new System.Drawing.Point(773, 62);
            this.txtApelido.MaxLength = 30;
            this.txtApelido.Name = "txtApelido";
            this.txtApelido.Size = new System.Drawing.Size(158, 22);
            this.txtApelido.TabIndex = 6;
            this.txtApelido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApelido_KeyPress);
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.Color.White;
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(452, 62);
            this.txtNome.MaxLength = 60;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(315, 22);
            this.txtNome.TabIndex = 5;
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(449, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 149;
            this.label4.Text = "Nome";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(770, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 148;
            this.label3.Text = "Apelido";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(573, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 147;
            this.label9.Text = "Fone";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(384, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 16);
            this.label7.TabIndex = 146;
            this.label7.Text = "UF";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(197, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 145;
            this.label6.Text = "Cidade";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(626, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 144;
            this.label5.Text = "Endereço";
            // 
            // txtCpf
            // 
            this.txtCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCpf.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCpf.Location = new System.Drawing.Point(313, 62);
            this.txtCpf.Mask = "000,000,000-00";
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(132, 22);
            this.txtCpf.TabIndex = 4;
            this.txtCpf.DoubleClick += new System.EventHandler(this.txtCpf_DoubleClick);
            this.txtCpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCpf_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(310, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 16);
            this.label16.TabIndex = 114;
            this.label16.Text = "N° Docto";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 20);
            this.lblEstab.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            // 
            // txtCliID
            // 
            this.txtCliID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCliID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCliID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCliID.Location = new System.Drawing.Point(16, 62);
            this.txtCliID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCliID.MaxLength = 18;
            this.txtCliID.Name = "txtCliID";
            this.txtCliID.ReadOnly = true;
            this.txtCliID.Size = new System.Drawing.Size(135, 22);
            this.txtCliID.TabIndex = 1;
            this.txtCliID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 44);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Cliente ID";
            // 
            // tpComplemento
            // 
            this.tpComplemento.Controls.Add(this.groupBox6);
            this.tpComplemento.Location = new System.Drawing.Point(4, 25);
            this.tpComplemento.Name = "tpComplemento";
            this.tpComplemento.Padding = new System.Windows.Forms.Padding(3);
            this.tpComplemento.Size = new System.Drawing.Size(957, 470);
            this.tpComplemento.TabIndex = 2;
            this.tpComplemento.Text = "Complemento";
            this.tpComplemento.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.txtCodigo);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.txtObs);
            this.groupBox6.Controls.Add(this.txtLimite);
            this.groupBox6.Controls.Add(this.label55);
            this.groupBox6.Controls.Add(this.txtCodCartao);
            this.groupBox6.Controls.Add(this.label54);
            this.groupBox6.Controls.Add(this.txtConveniada);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(5, 3);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox6.Size = new System.Drawing.Size(947, 207);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            // 
            // txtCodigo
            // 
            this.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(13, 34);
            this.txtCodigo.MaxLength = 6;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(102, 22);
            this.txtCodigo.TabIndex = 39;
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(10, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 16);
            this.label33.TabIndex = 210;
            this.label33.Text = "Código Cliente";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(13, 59);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 16);
            this.label31.TabIndex = 197;
            this.label31.Text = "Observações";
            // 
            // txtObs
            // 
            this.txtObs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObs.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtObs.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObs.Location = new System.Drawing.Point(16, 79);
            this.txtObs.MaxLength = 500;
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(446, 120);
            this.txtObs.TabIndex = 44;
            // 
            // txtLimite
            // 
            this.txtLimite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLimite.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLimite.Location = new System.Drawing.Point(417, 34);
            this.txtLimite.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtLimite.MaxLength = 18;
            this.txtLimite.Name = "txtLimite";
            this.txtLimite.Size = new System.Drawing.Size(102, 22);
            this.txtLimite.TabIndex = 43;
            this.txtLimite.Text = "0,00";
            this.txtLimite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLimite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLimite_KeyPress);
            this.txtLimite.Validated += new System.EventHandler(this.txtLimite_Validated);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Navy;
            this.label55.Location = new System.Drawing.Point(414, 15);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(48, 16);
            this.label55.TabIndex = 194;
            this.label55.Text = "Limite";
            // 
            // txtCodCartao
            // 
            this.txtCodCartao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodCartao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodCartao.Location = new System.Drawing.Point(235, 34);
            this.txtCodCartao.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCodCartao.MaxLength = 18;
            this.txtCodCartao.Name = "txtCodCartao";
            this.txtCodCartao.Size = new System.Drawing.Size(176, 22);
            this.txtCodCartao.TabIndex = 42;
            this.txtCodCartao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCartao_KeyPress);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(234, 15);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(83, 16);
            this.label54.TabIndex = 192;
            this.label54.Text = "Cód. Cartão";
            // 
            // txtConveniada
            // 
            this.txtConveniada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConveniada.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConveniada.Location = new System.Drawing.Point(125, 34);
            this.txtConveniada.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtConveniada.MaxLength = 18;
            this.txtConveniada.Name = "txtConveniada";
            this.txtConveniada.Size = new System.Drawing.Size(102, 22);
            this.txtConveniada.TabIndex = 40;
            this.txtConveniada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConveniada_KeyPress);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(122, 15);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(84, 16);
            this.label62.TabIndex = 61;
            this.label62.Text = "Conveniada";
            // 
            // frmCadClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadClientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadClientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadClientes_Load);
            this.Shown += new System.EventHandler(this.frmCadClientes_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcClientes.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).EndInit();
            this.cmsClientes.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tpComplemento.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcClientes;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtApelido;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox txtCpf;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.TextBox txtCliID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tpComplemento;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtObs;
        private System.Windows.Forms.TextBox txtLimite;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtCodCartao;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtConveniada;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ContextMenuStrip cmsClientes;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.TextBox txtBConv;
        private System.Windows.Forms.TextBox txtBCidade;
        private System.Windows.Forms.ComboBox cmbBGrupo;
        private System.Windows.Forms.MaskedTextBox txtBCpf;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBNome;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridView dgClientes;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbCondicao;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtContato;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cmbEUF;
        private System.Windows.Forms.MaskedTextBox txtECep;
        private System.Windows.Forms.TextBox txtECidade;
        private System.Windows.Forms.TextBox txtEBairro;
        private System.Windows.Forms.TextBox txtEEndereco;
        private System.Windows.Forms.Button btnEMesmo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.ComboBox cmbCUF;
        private System.Windows.Forms.MaskedTextBox txtCCep;
        private System.Windows.Forms.TextBox txtCCidade;
        private System.Windows.Forms.TextBox txtCBairro;
        private System.Windows.Forms.TextBox txtCEndereco;
        private System.Windows.Forms.Button btnCMesmo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.MaskedTextBox txtRecado;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.ComboBox cmbUF;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.ComboBox cmbGrupo;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.RadioButton rdbJuridica;
        private System.Windows.Forms.RadioButton rdbFisica;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtENum;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.TextBox txtCNum;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TextBox txtDocumento;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_TIPO_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_APELIDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRUPO_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ENDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_END_NUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_BAIRRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CEP;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_FONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CELULAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_FONE_RECADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ENDERCOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NUMEROCOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_BAIRROCOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADECOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CEPCOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UFCOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ENDERENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NUMEROENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_BAIRROENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADEENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CEPENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UFENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_EMAIL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CONTATO;
        private System.Windows.Forms.DataGridViewTextBoxColumn POS_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CARTAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_LIMITE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_OBSERVACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DESABILITADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_INSEST;
    }
}