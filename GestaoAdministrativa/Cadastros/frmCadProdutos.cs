﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadProdutos : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private string classeTera;
        private string lista;
        private DataTable dtProdutos = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbProdutos = new ToolStripButton("Produtos");
        private ToolStripSeparator tssProdutos = new ToolStripSeparator();
        private int indice;
        private bool decrescente;
        private string strOrdem;
        private double wCusto;
        private double wLucro;
        private double wMargem;
        
        #endregion

        public frmCadProdutos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Ajuda()
        { }

        public void Primeiro()
        {
            if (dtProdutos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgProdutos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgProdutos.CurrentCell = dgProdutos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcProdutos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtProdutos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdutos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdutos.CurrentCell = dgProdutos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
            //  CarregarCodBarra(txtProdID.Text);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdutos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdutos.CurrentCell = dgProdutos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
            //  CarregarCodBarra(txtProdID.Text);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdutos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdutos.CurrentCell = dgProdutos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadProdutos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, contRegistros));
            if (tcProdutos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcProdutos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtProdID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbProdutos.AutoSize = false;
                this.tsbProdutos.Image = Properties.Resources.cadastro;
                this.tsbProdutos.Size = new System.Drawing.Size(125, 20);
                this.tsbProdutos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbProdutos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssProdutos);
                tsbProdutos.Click += delegate
                {
                    var cadProdutos = Application.OpenForms.OfType<frmCadProdutos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadProdutos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                Application.DoEvents();
                var produtos = new Produto();
                var produtosDetalhe = new ProdutoDetalhe();
                var precos = new Preco();
                var pedidos = new Pedido();
                var entrada = new Entrada();

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtProdID.Text = dtProdutos.Rows[linha]["PROD_ID"].ToString();
                txtDescr.Text = dtProdutos.Rows[linha]["PROD_DESCR"].ToString();
                txtCodBarra.Text = Funcoes.ChecaCampoVazio(dtProdutos.Rows[linha]["PROD_CODIGO"].ToString());
                txtDescrAbr.Text = Funcoes.ChecaCampoVazio(dtProdutos.Rows[linha]["PROD_ABREV"].ToString());
                cmbUnidade.Text = dtProdutos.Rows[linha]["PROD_UNIDADE"].ToString();

                if (dtProdutos.Rows[linha]["PROD_USO_CONTINUO"].ToString() == "S")
                {
                    ckbUsoContinuo.Checked = true;
                }
                else
                    ckbUsoContinuo.Checked = false;

                if (dtProdutos.Rows[linha]["PROD_FORMULA"].ToString() == "S")
                {
                    chkManipulado.Checked = true;
                }
                else
                    chkManipulado.Checked = false;

                if (dtProdutos.Rows[linha]["PROD_CONTROLADO"].ToString() == "S")
                {
                    chkControlado.Checked = true;
                }
                else
                    chkControlado.Checked = false;

                txtNCM.Text = Funcoes.ChecaCampoVazio(dtProdutos.Rows[linha]["NCM"].ToString());

                if (dtProdutos.Rows[linha]["PROD_SITUACAO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtProdutos.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtProdutos.Rows[linha]["OPALTERACAO"].ToString());

                Principal.dtBusca = produtos.DadosProduto(txtCodBarra.Text, Convert.ToInt32(txtProdID.Text));

                if (Principal.dtBusca.Rows.Count != 0)
                {
                    cmbClasTera.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CLASSE_TERAP"].ToString());
                    txtCodABC.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_ABC_FARMA"].ToString());
                    txtRegMS.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_REGISTRO_MS"].ToString());
                    txtMsg.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_MENSAGEM"].ToString());
                    cmbTipoReceita.SelectedIndex = Principal.dtBusca.Rows[0]["PRO_TIPO_CONTROLADO"].ToString() == "" ? 0 : Convert.ToInt32(Principal.dtBusca.Rows[0]["PRO_TIPO_CONTROLADO"]);
                    cmbPortaria.Text = Principal.dtBusca.Rows[0]["PROD_PORTARIA_CONTROLADO"].ToString();
                    
                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["ESP_DESCRICAO"].ToString()) == "")
                    {
                        cmbEspec.SelectedIndex = -1;
                        txtEspcID.Text = "";
                    }
                    else
                    {
                        cmbEspec.Text = Principal.dtBusca.Rows[0]["ESP_DESCRICAO"].ToString();
                        txtEspcID.Text = Convert.ToString(cmbEspec.SelectedValue);
                    }

                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PRI_DESCRICAO"].ToString()) == "")
                    {
                        cmbPrinAtivo.SelectedIndex = -1;
                        txtPrincID.Text = "";
                    }
                    else
                    {
                        cmbPrinAtivo.Text = Principal.dtBusca.Rows[0]["PRI_DESCRICAO"].ToString();
                        txtPrincID.Text = Convert.ToString(cmbPrinAtivo.SelectedValue);
                    }

                    if(Principal.dtBusca.Rows[0]["PROD_CAIXA_CARTELADO"].ToString().Equals("N"))
                    {
                        chkCaixaCartelado.Checked = false;
                        txtCodCartelado.Text = "";
                        txtCodCartelado.Visible = false;
                        lblCartelado.Visible = false;
                    }
                    else
                    {
                        chkCaixaCartelado.Checked = true;
                        lblCartelado.Visible = true;
                        txtCodCartelado.Visible = true;
                        txtCodCartelado.Text = Principal.dtBusca.Rows[0]["PROD_CODIGO_CARTELADO"].ToString();
                    }
                }

                Principal.dtBusca = produtosDetalhe.DadosProdutoDetalhe(txtCodBarra.Text, Convert.ToInt32(txtProdID.Text));
                if (Principal.dtBusca.Rows.Count != 0)
                {
                    cmbDepto.SelectedValue = Principal.dtBusca.Rows[0]["DEP_CODIGO"];
                    txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);

                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["FAB_CODIGO"].ToString()) == "")
                    {
                        cmbFabricante.SelectedIndex = -1;
                        txtFabID.Text = "";
                    }
                    else
                    {
                        cmbFabricante.SelectedValue = Principal.dtBusca.Rows[0]["FAB_CODIGO"];
                        txtFabID.Text = Convert.ToString(cmbFabricante.SelectedValue);
                    }

                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["CLAS_CODIGO"].ToString()) == "")
                    {
                        cmbClasse.SelectedIndex = -1;
                        txtClasID.Text = "";
                    }
                    else
                    {
                        cmbClasse.SelectedValue = Principal.dtBusca.Rows[0]["CLAS_CODIGO"];
                        txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
                    }


                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["SUB_CODIGO"].ToString()) == "")
                    {
                        cmbSubClas.SelectedIndex = -1;
                        txtSubID.Text = "";
                    }
                    else
                    {
                        cmbSubClas.SelectedValue = Principal.dtBusca.Rows[0]["SUB_CODIGO"];
                        txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
                    }

                    if (Principal.dtBusca.Rows[0]["PROD_BLOQ_COMPRA"].ToString() == "S")
                    {
                        chkBCompra.Checked = true;
                    }
                    else
                        chkBCompra.Checked = false;

                    txtEstIni.Text = Principal.dtBusca.Rows[0]["PROD_ESTINI"].ToString() == "" ? "0" : Math.Round(Convert.ToDecimal(Principal.dtBusca.Rows[0]["PROD_ESTINI"])).ToString();
                    txtEstMin.Text = Principal.dtBusca.Rows[0]["PROD_ESTMIN"].ToString() == "" ? "0" : Math.Round(Convert.ToDecimal(Principal.dtBusca.Rows[0]["PROD_ESTMIN"])).ToString();
                    txtEstMax.Text = Principal.dtBusca.Rows[0]["PROD_ESTMAX"].ToString() == "" ? "0" : Math.Round(Convert.ToDecimal(Principal.dtBusca.Rows[0]["PROD_ESTMAX"])).ToString();
                    txtEstAtu.Text = Principal.dtBusca.Rows[0]["PROD_ESTATUAL"].ToString() == "" ? "0" : Math.Round(Convert.ToDecimal(Principal.dtBusca.Rows[0]["PROD_ESTATUAL"])).ToString();
                    txtCusIni.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_CUSINI"]);
                    txtCusMed.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_CUSME"]);
                    txtCusUlt.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_ULTCUSME"]);
                    
                    lista = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_LISTA"].ToString());
                    if (lista == "P")
                    {
                        cmbLista.Text = "POSITIVA";
                    }
                    else
                        if (lista == "N")
                        {
                            cmbLista.Text = "NEGATIVA";
                        }
                        else
                            if (lista == "U")
                            {
                                cmbLista.Text = "NEUTRA";
                            }
                            else
                                cmbLista.SelectedIndex = -1;

                    txtEcf.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_ECF"].ToString());
                    if (Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_UN_COMP"].ToString()) == "")
                    {
                        cmbUComp.SelectedIndex = -1;
                    }
                    else
                        cmbUComp.Text = Principal.dtBusca.Rows[0]["PROD_UN_COMP"].ToString();

                    txtQtde.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_QTDE_UN_COMP"].ToString());
                    txtComissao.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_COMISSAO"].ToString() == "" ? 0 : Principal.dtBusca.Rows[0]["PROD_COMISSAO"]);
                    txtCompra.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_PRECOMPRA"].ToString() == "" ? 0 : Principal.dtBusca.Rows[0]["PROD_PRECOMPRA"]);
                    txtMargem.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PROD_MARGEM"].ToString() == "" ? 0 : Principal.dtBusca.Rows[0]["PROD_MARGEM"]);
                    
                    if ((Convert.ToInt32(txtEstIni.Text) < Convert.ToInt32(txtEstMax.Text)) && (Convert.ToInt32(txtEstAtu.Text) < Convert.ToInt32(txtEstMax.Text)))
                    {
                        txtEstAtu.ForeColor = Color.Red;
                    }
                    else
                    {
                        txtEstAtu.ForeColor = Color.Black;
                    }

                    if (!String.IsNullOrEmpty(txtMargem.Text.Trim()))
                    {
                        if (Convert.ToDouble(txtMargem.Text) < 0)
                        {
                            txtEstAtu.ForeColor = Color.Red;
                        }
                        else
                        {
                            txtEstAtu.ForeColor = Color.Black;
                        }
                    }

                    dtCInicial.Value = Convert.ToDateTime(Principal.dtBusca.Rows[0]["PROD_DTESTINI"]);

                    txtCfop.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CFOP"].ToString());

                    cmbCst.SelectedItem = Principal.dtBusca.Rows[0]["PROD_CST"].ToString();
                    txtCest.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CEST"].ToString());
                    cmbCst.Text = Funcoes.FormataZeroAEsquerda(Principal.dtBusca.Rows[0]["PROD_CST"].ToString(),3);
                    cmbIcms.SelectedIndex = MostraTributoICMS(Principal.dtBusca.Rows[0]["PROD_ICMS"].ToString());
                    txtCstIpi.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CST_IPI"].ToString());
                    txtCstCofins.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CST_COFINS"].ToString());
                    txtCstPis.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CST_PIS"].ToString());
                    txtEnqIpi.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_ENQ_IPI"].ToString());
                    txtDcb.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_DCB"].ToString());
                    txtQtdUniFP.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_QTDE_UN_COMP_FP"].ToString());
                    txtCfopDev.Text = Funcoes.ChecaCampoVazio(Principal.dtBusca.Rows[0]["PROD_CFOP_DEVOLUCAO"].ToString());
                }

                Principal.dtBusca = precos.DadosPrecos(txtCodBarra.Text, Convert.ToInt32(txtProdID.Text));
                if (Principal.dtBusca.Rows.Count != 0)
                {
                    txtPVenda.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["PRE_VALOR"]);
                    cmbTabPrecos.SelectedValue = Convert.ToInt32(Principal.dtBusca.Rows[0]["TAB_CODIGO"]);
                    txtPmc.Text = String.Format("{0:N}", Principal.dtBusca.Rows[0]["MED_PCO18"]);
                    
                    //CHECK BLOQUEIA DESCONTO
                    if (Principal.dtBusca.Rows[0]["BLOQUEIA_DESCONTO"].ToString() == "S")
                    {
                        chkBloqDesconto.Checked = true;
                    }
                    else
                        chkBloqDesconto.Checked = false;

                    if(Principal.dtBusca.Rows[0]["ATUALIZADO_ABCFARMA"].ToString().Equals("N"))
                    {
                        lblAlterado.Text = "Preço não alterado na última Atualização ABCFARMA/ANVISA (MEDICAMENTO)";
                    }
                    else
                    {
                        lblAlterado.Text = "";
                    }
                }

                txtDtVenda.Text = Funcoes.ChecaCampoVazio(pedidos.IdentificaUltimaVendaProduto(txtCodBarra.Text));

                Principal.dtBusca = entrada.IdentificaUltimasCompras(txtCodBarra.Text);
                dgUltimasCompras.Rows.Clear();
                if (Principal.dtBusca.Rows.Count != 0)
                {
                    // verifca se o numero de registro de comprar é maior que 3 
                    int num = Principal.dtBusca.Rows.Count >= 3 ? 3 : Principal.dtBusca.Rows.Count;
                    for (int i = 0; i < num; i++)
                    {
                        double total = Math.Round((Convert.ToDouble(Principal.dtBusca.Rows[i]["ent_totitem"]) + Convert.ToDouble(Principal.dtBusca.Rows[i]["ent_valoripi"])) / Convert.ToDouble(Principal.dtBusca.Rows[i]["ent_qtdestoque"]), 2);

                        dgUltimasCompras.Rows.Insert(dgUltimasCompras.RowCount, new Object[] {
                       Convert.ToDateTime(Principal.dtBusca.Rows[i]["ENT_DTLANC"]).ToString("dd/MM/yyyy"), total,Principal.dtBusca.Rows[i]["cf_nome"]});
                    }
                }

                var codAbcFarma = new ProdutosAbcFarma();
                txtCodABC.Text = Funcoes.ChecaCampoVazio(codAbcFarma.BuscaCodigoAbcFarma(txtCodBarra.Text));
                
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private string IdentificaTributoICMS()
        {
            if (cmbIcms.SelectedIndex == 0)
            {
                return "";
            }
            else if (cmbIcms.SelectedIndex == 1)
            {
                return "00";
            }
            else if (cmbIcms.SelectedIndex == 2)
            {
                return "10";
            }
            else if (cmbIcms.SelectedIndex == 3)
            {
                return "20";
            }
            else if (cmbIcms.SelectedIndex == 4)
            {
                return "30";
            }
            else if (cmbIcms.SelectedIndex == 5)
            {
                return "40";
            }
            else if (cmbIcms.SelectedIndex == 6)
            {
                return "41";
            }
            else if (cmbIcms.SelectedIndex == 7)
            {
                return "50";
            }
            else if (cmbIcms.SelectedIndex == 8)
            {
                return "51";
            }
            else if (cmbIcms.SelectedIndex == 9)
            {
                return "60";
            }
            else if (cmbIcms.SelectedIndex == 10)
            {
                return "70";
            }
            else if (cmbIcms.SelectedIndex == 11)
            {
                return "90";
            }


            return string.Empty;
        }

        private int MostraTributoICMS(string icms)
        {
            if (icms == "00" || icms == "0")
            {
                return 1;
            }
            else if (icms == "10")
            {
                return 2;
            }
            else if (icms == "20")
            {
                return 3;
            }
            else if (icms == "30")
            {
                return 4;
            }
            else if (icms == "40")
            {
                return 5;
            }
            else if (icms == "41")
            {
                return 6;
            }
            else if (icms == "50")
            {
                return 7;
            }
            else if (icms == "51")
            {
                return 8;
            }
            else if (icms == "60")
            {
                return 9;
            }
            else if (icms == "70")
            {
                return 10;
            }
            else if (icms == "90")
            {
                return 11;
            }
            else
            {
                return 0;
            }

        }
        public string IdentificaCsosn(string cst)
        {
            if (cst.Equals("000") || cst.Equals("020") || cst.Equals("00") || cst.Equals("20"))
            {
                return "101";
            }
            else if (cst.Equals("010") || cst.Equals("070") || cst.Equals("10") || cst.Equals("70"))
            {
                return "201";
            }
            else if (cst.Equals("030") || cst.Equals("30"))
            {
                return "202";
            }
            else if (cst.Equals("060") || cst.Equals("60"))
            {
                return "500";
            }
            else if (cst.Equals("040") || cst.Equals("041") || cst.Equals("40") || cst.Equals("41"))
            {
                return "102";
            }
            else if (cst.Equals("050") || cst.Equals("051") || cst.Equals("50") || cst.Equals("51"))
            {
                return "300";
            }
            else
                return "";
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtProdutos.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbProdutos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssProdutos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtProdutos.Clear();
            dgProdutos.DataSource = dtProdutos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            //// Complemento Preco

            txtEstIni.Text = "0";
            txtEstMin.Text = "0";
            txtEstMax.Text = "0";
            cmbLista.SelectedIndex = 0;
            cmbUComp.SelectedIndex = -1;
            txtEcf.Text = "0";
            txtQtde.Text = "0";
            txtCusIni.Text = "0,00";
            txtComissao.Text = "0,00";
            txtCusMed.Text = "0,00";
            txtCusUlt.Text = "0,00";
            txtPmc.Text = "0,00";
            txtCompra.Text = "0,00";
            txtMargem.Text = "0,00";
            txtPVenda.Text = "0,00";
            txtData.Text = "";
            txtUsuario.Text = "";

            txtBDescr.Focus();
        }

        public void LimparFicha()
        {
            dtProdutos.Clear();
            dgProdutos.DataSource = dtProdutos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            dgUltimasCompras.Rows.Clear();

            //tcProdutos.SelectedTab = tpFicha;
            txtProdID.Clear();
            txtDescr.Clear();
            txtCodBarra.Clear();
            txtDescrAbr.Clear();
            cmbUnidade.SelectedIndex = -1;
            txtDeptoID.Clear();
            cmbDepto.SelectedIndex = -1;
            txtClasID.Clear();
            cmbClasse.SelectedIndex = -1;
            txtSubID.Clear();
            cmbSubClas.SelectedIndex = -1;
            txtFabID.Clear();
            cmbFabricante.SelectedIndex = -1;
            txtCodABC.Clear();
            txtEspcID.Clear();
            cmbEspec.SelectedIndex = -1;
            txtPrincID.Clear();
            cmbPrinAtivo.SelectedIndex = -1;
            txtNCM.Clear();
            cmbTabPrecos.SelectedIndex = -1;
            cmbLista.SelectedIndex = -1;
            chkLiberado.Checked = true;

            // Complemento Geral

            cmbClasTera.SelectedIndex = -1;
            txtRegMS.Clear();
            cmbTipoReceita.SelectedIndex = 0;
            txtMsg.Clear();
            chkBCompra.Checked = false;
            ckbUsoContinuo.Checked = false;
            chkControlado.Checked = false;
            chkManipulado.Checked = false;
            txtCfop.Clear();
            cmbCst.SelectedIndex = -1;
            txtCest.Clear();
            txtCstCofins.Clear();
            txtCstPis.Clear();
            txtEnqIpi.Clear();
            cmbIcms.SelectedIndex = -1;
            txtCsosn.Clear();
            txtCstIpi.Clear();
            txtRegMS.Text = "";
            txtCfopDev.Text = "";

            //// Complemento Preco
            txtQtdUniFP.Text = "";
            txtDcb.Text = "";
            txtEstIni.Text = "0";
            txtEstMin.Text = "0";
            txtEstMax.Text = "0";
            cmbLista.SelectedIndex = 0;
            cmbUComp.SelectedIndex = -1;
            txtEcf.Text = "0";
            txtQtde.Text = "0";
            txtCusIni.Text = "0,00";
            txtComissao.Text = "0,00";
            txtCusMed.Text = "0,00";
            txtCusUlt.Text = "0,00";
            txtPmc.Text = "0,00";
            txtCompra.Text = "0,00";
            txtMargem.Text = "0,00";
            txtPVenda.Text = "0,00";
            cmbTabPrecos.SelectedIndex = 0;
            txtDescr.Focus();
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            lblCartelado.Visible = false;
            chkCaixaCartelado.Checked = false;
            txtCodCartelado.Text = "";
            txtCodCartelado.Visible = false;
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcProdutos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
            {
                LimparFicha();
            }

            BotoesHabilitados();
        }


        public void ImprimirRelatorio()
        {
        }

        public bool Excluir()
        {
            try
            {
                if ((txtProdID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do produto?", "Exclusão tabela Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Cursor = Cursors.WaitCursor;
                        if (RegrasCadastro.ExcluirProdutos(txtCodBarra.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            Produto prod = new Produto();
                            ProdutoDetalhe prodDetalhe = new ProdutoDetalhe();
                            Preco preco = new Preco();

                            BancoDados.AbrirTrans();
                            if (preco.ExcluirPorCodigoBarras(txtCodBarra.Text).Equals(true))
                            {
                                if (prodDetalhe.ExcluirPorCodigoBarras(txtCodBarra.Text).Equals(true))
                                {
                                    if (prod.ExcluiPorCodigoBarras(txtCodBarra.Text).Equals(true))
                                    {
                                        if(Funcoes.LeParametro(9,"51", false).Equals("S"))
                                        {
                                            ExcluiProdutoEstoqueFilial(txtCodBarra.Text);
                                        }

                                        Funcoes.GravaLogExclusao("PROD_CODIGO", txtProdID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PRODUTOS", txtCodBarra.Text, Principal.motivo, Principal.estAtual);
                                        Funcoes.GravaLogExclusao("PROD_CODIGO", txtProdID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PRODUTOS_DETALHE", txtCodBarra.Text, Principal.motivo, Principal.estAtual);
                                        Funcoes.GravaLogExclusao("PROD_CODIGO", txtProdID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PRECOS", txtCodBarra.Text, Principal.motivo, Principal.estAtual);
                                        dtProdutos.Clear();
                                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                        dgProdutos.DataSource = dtProdutos;
                                        emGrade = false;
                                        tcProdutos.SelectedTab = tpGrade;
                                        tslRegistros.Text = "";
                                        Funcoes.LimpaFormularios(this);
                                        txtBDescr.Focus();
                                        cmbLiberado.SelectedIndex = 0;
                                        BancoDados.FecharTrans();
                                        return true;

                                    }
                                    else {
                                        BancoDados.ErroTrans();
                                        MessageBox.Show("103 Erro ao excluir", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    
                                    }
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 Erro ao excluir", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("101 Erro ao excluir", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    classeTera = "";
                    if (cmbClasTera.SelectedIndex > 0)
                    {
                        if (cmbClasTera.SelectedIndex == 1)
                        {
                            classeTera = "1";
                        }
                        else
                            if (cmbClasTera.SelectedIndex == 2)
                            {
                                classeTera = "2";
                            }
                    }


                    lista = "";
                    if (cmbLista.Text == "POSITIVA")
                    {
                        lista = "P";
                    }
                    else
                        if (cmbLista.Text == "NEGATIVA")
                        {
                            lista = "N";
                        }
                        else
                            if (cmbLista.Text == "NEUTRA")
                            {
                                lista = "U";
                            }
                    Produto prod = new Produto
                    {
                        ProdID = Convert.ToInt32(txtProdID.Text),
                        ProdCodBarras = txtCodBarra.Text,
                        ProdDescr = txtDescr.Text,
                        ProdDescAbrev = txtDescrAbr.Text,
                        ProdUnidade = cmbUnidade.Text,
                        ProdTipo = "P",
                        ProdAbcFarma = txtCodABC.Text.Trim() == "" ? "" : txtCodABC.Text.Trim(),
                        ProdUsoContinuo = ckbUsoContinuo.Checked == true ? "S" : "N",
                        ProdFormula = chkManipulado.Checked == true ? "S" : "N",
                        EspCodigo = cmbEspec.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbEspec.SelectedValue),
                        PrinCodigo = cmbPrinAtivo.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbPrinAtivo.SelectedValue),
                        ProdRegistroMS = txtRegMS.Text.Trim(),
                        ProdControlado = chkControlado.Checked == true ? "S" : "N",
                        ProdMensagem = txtMsg.Text.Trim(),
                        ProdNCM = txtNCM.Text.Trim(),
                        TipoReceita = cmbTipoReceita.SelectedIndex.ToString(),
                        ProdClasTera = classeTera,
                        ProdCaixaCartelado = chkCaixaCartelado.Checked ? "S" : "N",
                        ProdCodigoCartelado = chkCaixaCartelado.Checked ? txtCodCartelado.Text : "",
                        ProdPortaria = cmbPortaria.Text
                    };

                    Principal.dtBusca = Util.RegistrosPorEmpresa("PRODUTOS", "PROD_ID", txtProdID.Text, false);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Erro ao efetuar atualização produto não encontrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        return false;
                    }
                    else
                    {

                        BancoDados.AbrirTrans();

                        if (prod.AtualizaProdutos(prod, Principal.dtBusca).Equals(true))
                        {
                            ProdutoDetalhe prodDetalhes = new ProdutoDetalhe
                            {
                                ProdID = Convert.ToInt32(txtProdID.Text),
                                ProdCodigo = txtCodBarra.Text,
                                DepCodigo = txtDeptoID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtDeptoID.Text.Replace(",00", "")),
                                ClasCodigo = txtClasID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtClasID.Text.Replace(",00", "")),
                                SubCodigo = txtSubID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtSubID.Text.Replace(",00", "")),
                                FabCodigo = txtFabID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtFabID.Text.Replace(",00", "")),
                                
                                ProdCfop = txtCfop.Text.Trim(),
                                ProdCst = cmbCst.Text,
                                ProdCest = txtCest.Text.Trim(),
                                ProdIcms = IdentificaTributoICMS(),
                                ProdCsosn = txtCsosn.Text.Trim(),
                                ProdCstIpi = txtCstIpi.Text.Trim(),
                                ProdCstPis = txtCstPis.Text.Trim(),
                                ProdCstCofins = txtCstCofins.Text.Trim(),
                                ProdEnqIpi = txtEnqIpi.Text.Trim(),
                                ProdEstIni = txtEstIni.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstIni.Text),
                                ProdEstAtual = txtEstAtu.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstAtu.Text),
                                ProdEstMin = txtEstMin.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstMin.Text),
                                ProdEstMax = txtEstMax.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstMax.Text),
                                ProdLista = lista,
                                ProdCusIni = txtCusIni.Text.Trim() == "" ? 0 : Convert.ToDouble(txtCusIni.Text.Trim()),
                                ProdCusme = txtCusMed.Text.Trim() == "" ? 0 : Convert.ToDouble(txtCusMed.Text.Trim()),
                                ProdUltCusme = txtCusUlt.Text.Trim() == "" ? 0 : Convert.ToDouble(txtCusUlt.Text.Trim()),
                                ProdPreCompra = txtCompra.Text.Trim() == "" ? 0 : Convert.ToDouble(txtCompra.Text.Trim()),
                                ProdMargem = txtMargem.Text.Trim() == "" ? 0 : Convert.ToDouble(txtMargem.Text.Trim()),
                                ProdComissao = txtComissao.Text.Trim() == "" ? 0 : Convert.ToDouble(txtComissao.Text.Trim()),
                                ProdBloqCompra = chkBCompra.Checked == true ? "S" : "N",
                                ProdLiberado = chkLiberado.Checked == true ? "A" : "I",
                                ProdEcf = txtEcf.Text,
                                ProdUnComp = cmbUComp.Text,
                                ProdQtdeUnComp = txtQtde.Text == "" ? 0 : Convert.ToDouble(txtQtde.Text.Replace(",00", "")),
                                ProdQtdeUnCompFP = txtQtdUniFP.Text.Trim() == "" ? 0 : Convert.ToDouble(txtQtdUniFP.Text),
                                ProdDCB = txtDcb.Text,
                                ProdCfopDevolucao = txtCfopDev.Text
                            };
                            Principal.dtBusca = Util.RegistrosPorEmpresa("PRODUTOS_DETALHE", "PROD_ID", txtProdID.Text, true);
                            if (prodDetalhes.AtualizaCadastro(prodDetalhes, Principal.dtBusca).Equals(true))
                            {
                                if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                {
                                    IncluiOuAtualizaProdutoFilial("menos", Convert.ToInt32(Principal.dtBusca.Rows[0]["PROD_ESTATUAL"]));

                                    IncluiOuAtualizaProdutoFilial("mais", Convert.ToInt32(txtEstAtu.Text));
                                }

                                Preco preco = new Preco
                                {
                                    ProdID = Convert.ToInt32(txtProdID.Text),
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    ProdCodigo = txtCodBarra.Text,
                                    TabCodigo = cmbTabPrecos.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbTabPrecos.SelectedValue),
                                    PreValor = txtPVenda.Text == "" ? 0 : Convert.ToDouble(txtPVenda.Text),
                                    BloqDesconto = chkBloqDesconto.Checked == true ? "S" : "N",
                                    AtualizadoABCFarma = "S",
                                    ProdPMC = txtPmc.Text == "" ? 0 : Convert.ToDouble(txtPmc.Text),
                                };
                                Principal.dtBusca = Util.RegistrosPorEmpresa("PRECOS", "PROD_ID", txtProdID.Text, true);
                                if (preco.AtualizaPreco(preco, Principal.dtBusca).Equals(true))
                                {
                                    BancoDados.FecharTrans();
                                    MessageBox.Show("Atualização efetuada com sucesso!", "Produtos", MessageBoxButtons.OK);
                                    tcProdutos.SelectedTab = tpFicha;
                                    txtDescr.Focus();
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("103 - Erro ao efetuar atualização  ", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuar atualização  ", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar atualização  ", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }


                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                BancoDados.ErroTrans();
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            if (txtCodBarra.TextLength < 5)
            {
                Principal.mensagem = "Código de barras tem que ter no mínimo 5 caracteres";
                Funcoes.Avisa();
                txtCodBarra.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "A Descrição não pode estar em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarra.Text.Trim()))
            {
                Principal.mensagem = "O Código de Barras não pode estar em branco.";
                Funcoes.Avisa();
                txtCodBarra.Focus();
                return false;
            }
            if (Equals(cmbUnidade.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione uma Unidade.";
                Funcoes.Avisa();
                cmbUnidade.Focus();
                return false;
            }
            if (Equals(cmbDepto.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um Departamento.";
                Funcoes.Avisa();
                cmbDepto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtPVenda.Text.Trim()))
            {
                Principal.mensagem = "O Preço de Venda não pode estar em branco.";
                Funcoes.Avisa();
                txtPVenda.Focus();
                return false;
            }
            if (Convert.ToDouble(txtPVenda.Text) <= 0)
            {
                Principal.mensagem = "O Preço de Venda não pode ser zero.";
                Funcoes.Avisa();
                txtPVenda.Focus();
                return false;
            }
            if(chkCaixaCartelado.Checked && String.IsNullOrEmpty(txtCodCartelado.Text))
            {
                Principal.mensagem = "Cód. do Cartelado não pode ser em branco!";
                Funcoes.Avisa();
                txtCodCartelado.Focus();
                return false;
            }
            if (Convert.ToDouble(txtPmc.Text) <= 0)
            {
                Principal.mensagem = "Preço PMC não pode ser em branco!";
                Funcoes.Avisa();
                txtPmc.Focus();
                return false;
            }
            return true;
        }

        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();

                //CARREGA UNIDADES//
                dtPesq = Util.CarregarCombosPorEstabelecimento("UNI_CODIGO", "UNI_DESC_ABREV", "TIPO_UNIDADE", false, "UNI_LIBERADO = 'S'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Unidade,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUnidade.DataSource = dtPesq;
                    cmbUnidade.DisplayMember = "UNI_DESC_ABREV";
                    cmbUnidade.ValueMember = "UNI_CODIGO";

                    cmbUnidade.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEstabelecimento("UNI_CODIGO", "UNI_DESC_ABREV", "TIPO_UNIDADE", false, "UNI_LIBERADO = 'S'");
                    cmbUComp.DataSource = dtPesq;
                    cmbUComp.DisplayMember = "UNI_DESC_ABREV";
                    cmbUComp.ValueMember = "UNI_CODIGO";
                    cmbUComp.SelectedIndex = -1;

                }

                //CARREGA DEPARTAMENTOS//
                dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Departameto,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbDepto.DataSource = dtPesq;
                    cmbDepto.DisplayMember = "DEP_DESCR";
                    cmbDepto.ValueMember = "DEP_CODIGO";
                    cmbDepto.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
                    cmbBDepto.DataSource = dtPesq;
                    cmbBDepto.DisplayMember = "DEP_DESCR";
                    cmbBDepto.ValueMember = "DEP_CODIGO";
                    cmbBDepto.SelectedIndex = -1;
                }

                //CARREGA CLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbClasse.DataSource = dtPesq;
                    cmbClasse.DisplayMember = "CLAS_DESCR";
                    cmbClasse.ValueMember = "CLAS_CODIGO";
                    cmbClasse.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'N'");
                    cmbBClasse.DataSource = dtPesq;
                    cmbBClasse.DisplayMember = "CLAS_DESCR";
                    cmbBClasse.ValueMember = "CLAS_CODIGO";
                    cmbBClasse.SelectedIndex = -1;
                }

                //CARREGA SUBCLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("SUB_CODIGO", "SUB_DESCR", "SUBCLASSES", true, "SUB_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbSubClas.DataSource = dtPesq;
                    cmbSubClas.DisplayMember = "SUB_DESCR";
                    cmbSubClas.ValueMember = "SUB_CODIGO";
                    cmbSubClas.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEmpresa("SUB_CODIGO", "SUB_DESCR", "SUBCLASSES", true, "SUB_DESABILITADO = 'N'");
                    cmbBSub.DataSource = dtPesq;
                    cmbBSub.DisplayMember = "SUB_DESCR";
                    cmbBSub.ValueMember = "SUB_CODIGO";
                    cmbBSub.SelectedIndex = -1;
                }

                //CARREGA FABRICANTES//
                dtPesq = Util.CarregarCombosPorEmpresa("FAB_CODIGO", "FAB_DESCRICAO", "FABRICANTES", true, "FAB_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbFabricante.DataSource = dtPesq;
                    cmbFabricante.DisplayMember = "FAB_DESCRICAO";
                    cmbFabricante.ValueMember = "FAB_CODIGO";
                    cmbFabricante.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEmpresa("FAB_CODIGO", "FAB_DESCRICAO", "FABRICANTES", true, "FAB_DESABILITADO = 'N'");
                    cmbBFab.DataSource = dtPesq;
                    cmbBFab.DisplayMember = "FAB_DESCRICAO";
                    cmbBFab.ValueMember = "FAB_CODIGO";
                    cmbBFab.SelectedIndex = -1;
                }

                //CARREGA ESPECIFICACOES//
                dtPesq = Util.CarregarCombosPorEmpresa("ESP_CODIGO", "ESP_DESCRICAO", "ESPECIFICACOES", true, "ESP_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbEspec.DataSource = dtPesq;
                    cmbEspec.DisplayMember = "ESP_DESCRICAO";
                    cmbEspec.ValueMember = "ESP_CODIGO";
                    cmbEspec.SelectedIndex = -1;
                }

                //CARREGA PRINCIPIO ATIVO//
                dtPesq = Util.CarregarCombosPorEmpresa("PRI_CODIGO", "PRI_DESCRICAO", "PRINCIPIO_ATIVO", true, "PRI_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbPrinAtivo.DataSource = dtPesq;
                    cmbPrinAtivo.DisplayMember = "PRI_DESCRICAO";
                    cmbPrinAtivo.ValueMember = "PRI_CODIGO";
                    cmbPrinAtivo.SelectedIndex = -1;

                    dtPesq = Util.CarregarCombosPorEmpresa("PRI_CODIGO", "PRI_DESCRICAO", "PRINCIPIO_ATIVO", true, "PRI_DESABILITADO = 'N'");
                    cmbBPrinc.DataSource = dtPesq;
                    cmbBPrinc.DisplayMember = "PRI_DESCRICAO";
                    cmbBPrinc.ValueMember = "PRI_CODIGO";
                    cmbBPrinc.SelectedIndex = -1;
                }

                //CARREGA TABELA DE PREÇOS//
                dtPesq = Util.CarregarCombosPorEmpresa("TAB_CODIGO", "TAB_DESCR", "TAB_PRECOS", true, "TAB_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Tabela de Preços,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbTabPrecos.DataSource = dtPesq;
                    cmbTabPrecos.DisplayMember = "TAB_DESCR";
                    cmbTabPrecos.ValueMember = "TAB_CODIGO";
                    cmbTabPrecos.SelectedIndex = -1;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    classeTera = "";
                    if (cmbClasTera.SelectedIndex > 0)
                    {
                        classeTera = cmbClasTera.SelectedIndex.ToString();
                    }

                    lista = "";
                    if (cmbLista.Text == "POSITIVA")
                    {
                        lista = "P";
                    }
                    else if (cmbLista.Text == "NEGATIVA")
                    {
                        lista = "N";
                    }
                    else if (cmbLista.Text == "NEUTRA")
                    {
                        lista = "U";
                    }

                    chkLiberado.Checked = true;
                    Produto prod = new Produto
                    {
                        ProdID = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID"),
                        ProdCodBarras = txtCodBarra.Text,
                        ProdDescr = txtDescr.Text,
                        ProdDescAbrev = txtDescrAbr.Text,
                        ProdUnidade = cmbUnidade.Text,
                        ProdTipo = "P",
                        ProdAbcFarma = txtCodABC.Text.Trim() == "" ? "" : txtCodABC.Text.Trim(),
                        ProdUsoContinuo = ckbUsoContinuo.Checked == true ? "S" : "N",
                        ProdFormula = chkManipulado.Checked == true ? "S" : "N",
                        EspCodigo = cmbEspec.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbEspec.SelectedValue),
                        PrinCodigo = cmbPrinAtivo.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbPrinAtivo.SelectedValue),
                        ProdRegistroMS = txtRegMS.Text.Trim(),
                        ProdControlado = chkControlado.Checked == true ? "S" : "N",
                        ProdMensagem = txtMsg.Text.Trim(),
                        ProdNCM = txtNCM.Text.Trim(),
                        TipoReceita = cmbTipoReceita.SelectedIndex.ToString(),
                        ProdClasTera = classeTera,
                        ProdCaixaCartelado = chkCaixaCartelado.Checked ? "S" : "N",
                        ProdCodigoCartelado = chkCaixaCartelado.Checked ? txtCodCartelado.Text : "",
                        ProdPortaria = cmbPortaria.Text
                    };

                    DataTable dtProdutos = prod.BuscaProdutosDescricao(txtDescr.Text);
                    string codbarras = prod.BuscaProdCodigo(txtCodBarra.Text);
                    if (dtProdutos.Rows.Count == 0 && codbarras == "")
                    {
                        BancoDados.AbrirTrans();

                        if (prod.InsereProdutosNovosCadastro(prod).Equals(true))
                        {
                            ProdutoDetalhe prodDetalhes = new ProdutoDetalhe
                            {
                                ProdID = prod.ProdID,
                                ProdCodigo = txtCodBarra.Text.Trim(),
                                DepCodigo = Convert.ToInt32(txtDeptoID.Text.Replace(",00", "")),
                                ClasCodigo = txtClasID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtClasID.Text.Replace(",00", "")),
                                SubCodigo = txtSubID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtSubID.Text.Replace(",00", "")),
                                FabCodigo = txtFabID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtFabID.Text.Replace(",00", "")),
                                ProdCfop = txtCfop.Text.Trim(),
                                ProdCst = cmbCst.SelectedText.Trim(),
                                ProdCest = txtCest.Text.Trim(),
                                ProdIcms = IdentificaTributoICMS(),
                                ProdCsosn = txtCsosn.Text.Trim(),
                                ProdCstIpi = txtCstIpi.Text.Trim(),
                                ProdCstCofins = txtCstCofins.Text.Trim(),
                                ProdEnqIpi = txtEnqIpi.Text.Trim(),
                                ProdEstIni = txtEstIni.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstIni.Text),
                                ProdEstAtual = txtEstAtu.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstAtu.Text),
                                ProdEstMin = txtEstMin.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstMin.Text),
                                ProdEstMax = txtEstMax.Text.Trim() == "" ? 0 : Convert.ToInt32(txtEstMax.Text),
                                ProdLista = lista,
                                ProdCusIni = Convert.ToDouble(txtCusIni.Text),
                                ProdCusme = Convert.ToDouble(txtCusMed.Text),
                                ProdUltCusme = Convert.ToDouble(txtCusUlt.Text),
                                ProdPreCompra = Convert.ToDouble(txtCompra.Text),
                                ProdMargem = Convert.ToDouble(txtMargem.Text),
                                ProdComissao = Convert.ToDouble(txtComissao.Text),
                                ProdBloqCompra = chkBCompra.Checked == true ? "S" : "N",
                                ProdLiberado = chkLiberado.Checked == true ? "A" : "I",
                                ProdEcf = txtEcf.Text,
                                ProdUnComp = cmbUComp.SelectedText,
                                ProdQtdeUnComp = txtQtde.Text.Trim() == "" ? 0 : Convert.ToDouble(txtQtde.Text),
                                ProdQtdeUnCompFP = txtQtdUniFP.Text.Trim() == "" ? 0 : Convert.ToDouble(txtQtdUniFP.Text),
                                ProdDCB = txtDcb.Text,
                                ProdCfopDevolucao = txtCfopDev.Text
                            };

                            if (prodDetalhes.InsereProdutosDetalhe(prodDetalhes).Equals(true))
                            {

                                Preco preco = new Preco
                                {
                                    ProdID = prod.ProdID,
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    ProdCodigo = txtCodBarra.Text,
                                    TabCodigo = 1,
                                    PreValor = txtPVenda.Text == "" ? 0 : Convert.ToDouble(txtPVenda.Text),
                                    BloqDesconto = chkBloqDesconto.Checked == true ? "S" : "N",
                                    AtualizadoABCFarma = "S", 
                                    ProdPMC = txtPmc.Text == "" ? 0 : Convert.ToDouble(txtPmc.Text),
                                }; 

                                if (preco.InsereProdutos(preco).Equals(true))
                                {
                                    BancoDados.FecharTrans();

                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        IncluiOuAtualizaProdutoFilial("mais", txtEstAtu.Text == "" ? 0 : Convert.ToInt32(txtEstAtu.Text));
                                    }

                                    MessageBox.Show("Cadastro efetuado com sucesso!", "Produtos", MessageBoxButtons.OK);
                                    Limpar();
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("103 - Erro ao efetuar cadastro", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuar cadastro", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar cadastro ", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Descrição já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                }
                Cursor = Cursors.Default;
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async void IncluiOuAtualizaProdutoFilial(string operacao, int quantidade)
        {
            await InsereEstoqueFiliais(operacao, Funcoes.LeParametro(9, "52", true), quantidade);
        }
        
        public async void ExcluiProdutoEstoqueFilial(string codBarras)
        {
            await ExcluiEstoqueFiliais(codBarras);
        }

        private async Task ExcluiEstoqueFiliais(string codBarras)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/ExcluiProdutosPorCodBarras?codBarras=" + codBarras + "&codEstabelecimento=" + Funcoes.LeParametro(9, "52", true));
                    response.EnsureSuccessStatusCode();
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao excluir registros!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private async Task InsereEstoqueFiliais(string operacao, string filial, int quantidade)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoque?codBarra=" + txtCodBarra.Text
                                    + "&descricao=" + txtDescr.Text 
                                    + "&qtde=" + quantidade
                                    + "&precoCusto=" + txtCompra.Text.Replace(",", ".")
                                    + "&precoVenda=" + txtPVenda.Text.Replace(",", ".") + "&codEstabelecimento=" + filial + "&operacao=" + operacao);
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\CadastroProduto.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarra.Text + " / QTDE: " + txtEstAtu.Text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarra.Focus();
        }

        private void txtCodBarra_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
                txtDescrAbr.Focus();
        }

        private void txtDescrAbr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUnidade.Focus();
        }

        private void cmbUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDeptoID.Focus();
        }

        private void txtDeptoID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbDepto.Focus();
        }

        private void cmbDepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtClasID.Focus();
        }

        private void txtClasID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbClasse.Focus();
        }

        private void cmbClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSubID.Focus();
        }

        private void txtSubID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbSubClas.Focus();
        }

        private void cmbSubClas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFabID.Focus();
        }


        private void txtFabID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbFabricante.Focus();
        }

        private void cmbFabricante_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEspcID.Focus();
        }

        private void cmbClasTera_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtRegMS.Focus();
        }

        private void txtCodABC_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e); Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtEspcID.Focus();
        }

        private void txtEspcID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbEspec.Focus();
        }

        private void cmbEspec_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPrincID.Focus();
        }

        private void txtPrincID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbPrinAtivo.Focus();
        }

        private void cmbPrinAtivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNCM.Focus();
        }

        private void txtRegMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if(txtRegMS.Text.Length == 0)
                if (e.KeyChar == 55)
                    e.Handled = true; 
            if (e.KeyChar == 13)
                cmbTipoReceita.Focus();
        }

        private void chkContinuo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkControlado.Focus();
        }

        private void chkManipulado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCfop.Focus();
        }

        private void chkControlado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkManipulado.Focus();
        }

        private void txtEstIni_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtEstMin.Focus();
        }

        private void txtEstMin_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtEstMax.Focus();
        }

        private void txtEstMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCusIni.Focus();
        }

        private void txtEstAtu_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCusIni.Focus();
        }

        private void txtCusIni_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCusMed.Focus();
        }

        private void txtCusMed_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPmc.Focus();
        }

        private void txtCusUlt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCompra.Focus();
        }

        private void cmbLista_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEcf.Focus();
        }

        private void txtEcf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUComp.Focus();
        }

        private void cmbUComp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDcb.Focus();
        }

        private void txtComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
                cmbLista.Focus();
        }

        private void txtMargem_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (Funcoes.LeParametro(9, "62", false).Equals("PROD_ULTCUSME"))
                {
                    if (Convert.ToDouble(txtCusUlt.Text) > 0)
                    {
                        txtPVenda.Text = String.Format("{0:N}", Convert.ToDouble(txtCusUlt.Text) + ((Convert.ToDouble(txtMargem.Text) / 100) * Convert.ToDouble(txtCusUlt.Text)));
                    }
                    else
                    {
                        txtPVenda.Text = String.Format("{0:N}", Convert.ToDouble(txtCompra.Text) + ((Convert.ToDouble(txtMargem.Text) / 100) * Convert.ToDouble(txtCompra.Text)));
                    }
                }
                else if (Funcoes.LeParametro(9, "62", false).Equals("PROD_PRECOMPRA"))
                {
                    txtPVenda.Text = String.Format("{0:N}", Convert.ToDouble(txtCompra.Text) + ((Convert.ToDouble(txtMargem.Text) / 100) * Convert.ToDouble(txtCompra.Text)));
                }

                txtPVenda.Focus();
            }
        }

        private void txtPVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (Funcoes.LeParametro(9, "62", false).Equals("PROD_ULTCUSME"))
                {
                    if (Convert.ToDouble(txtCusUlt.Text) > 0)
                    {
                        txtMargem.Text = String.Format("{0:N}", Convert.ToDouble(String.Format("{0:0.00}", ((Convert.ToDouble(txtPVenda.Text) / Convert.ToDouble(txtCusUlt.Text)) - 1) * 100)));
                    }
                    else if (Convert.ToDouble(txtCompra.Text) > 0)
                    {
                        txtMargem.Text = String.Format("{0:N}", Convert.ToDouble(String.Format("{0:0.00}", ((Convert.ToDouble(txtPVenda.Text) / Convert.ToDouble(txtCompra.Text)) - 1) * 100)));
                    }
                    else
                    {
                        txtMargem.Text = "0,00";
                    }
                }
                else if (Funcoes.LeParametro(9, "62", false).Equals("PROD_PRECOMPRA"))
                {
                    if (Convert.ToDouble(txtCompra.Text) > 0)
                    {
                        txtMargem.Text = String.Format("{0:N}", Convert.ToDouble(String.Format("{0:0.00}", ((Convert.ToDouble(txtPVenda.Text) / Convert.ToDouble(txtCompra.Text)) - 1) * 100)));
                    }
                    else
                    {
                        txtMargem.Text = "0,00";
                    }
                }

                if(Convert.ToDouble(txtMargem.Text) < 0)
                {
                    MessageBox.Show("Margem Negativa. Atenção ao Preço de Venda!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPVenda.Focus();
                }
                else
                    txtComissao.Focus();
            }
        }

        private void cmbTabPrecos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEcf.Focus();
        }

        private void txtDeptoID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbDepto, txtDeptoID);
            if (cmbDepto.SelectedIndex != -1)
                txtClasID.Focus();
        }

        private void cmbDepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);
        }

        private void txtClasID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbClasse, txtClasID);
            if (cmbClasse.SelectedIndex != -1)
                txtSubID.Focus();
        }

        private void cmbClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
        }

        private void txtSubID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbSubClas, txtSubID);
            if (cmbSubClas.SelectedIndex != -1)
                txtFabID.Focus();
        }

        private void cmbSubClas_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
        }

        private void txtFabID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbFabricante, txtFabID);
            if (cmbFabricante.SelectedIndex != -1)
                txtRegMS.Focus();
        }

        private void cmbFabricante_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFabID.Text = Convert.ToString(cmbFabricante.SelectedValue);
        }

        private void txtEspcID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbEspec, txtEspcID);
            if (cmbEspec.SelectedIndex != -1)
                txtPrincID.Focus();
        }

        private void cmbEspec_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEspcID.Text = Convert.ToString(cmbEspec.SelectedValue);
        }

        private void txtPrincID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbPrinAtivo, txtPrincID);
            if (cmbPrinAtivo.SelectedIndex != -1)
                txtMsg.Focus();
        }

        private void cmbPrinAtivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPrincID.Text = Convert.ToString(cmbPrinAtivo.SelectedValue);
        }

        private void dgProdutos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, contRegistros));
            emGrade = true;

        }

        private void dgProdutos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcProdutos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtProdutos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtProdutos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtProdutos.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                bool todos = true;
                Cursor = Cursors.WaitCursor;
                var produto = new Produto();

                Application.DoEvents();
                dtProdutos = produto.BuscarDados(txtBDescr.Text.ToUpper().Trim(), txtBCod.Text.Trim(),
                    cmbBDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBDepto.SelectedValue),
                    cmbBClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBClasse.SelectedValue),
                    cmbLiberado.Text, cmbBSub.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBSub.SelectedValue),
                    cmbBFab.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBFab.SelectedValue),
                    cmbBPrinc.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBPrinc.SelectedValue),
                    txtBReg.Text.ToUpper().Trim(), out todos, out strOrdem);
                if (todos.Equals(true))
                {
                    if (dtProdutos.Rows.Count > 0)
                    {
                        tslRegistros.Text = "Registros encontrados: " + dtProdutos.Rows.Count;
                        dgProdutos.DataSource = dtProdutos;
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, 0));
                        dgProdutos.Focus();
                        contRegistros = 0;
                        emGrade = true;
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgProdutos.DataSource = dtProdutos;
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        txtBDescr.Focus();
                        tslRegistros.Text = "";
                        emGrade = false;
                        Funcoes.LimpaFormularios(this);
                        Limpar();
                        cmbLiberado.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void tcProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (tcProdutos.SelectedTab == tpGrade)
            {
                dgProdutos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcProdutos.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadProdutos");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, contRegistros));
                        cmbTabPrecos.SelectedIndex = 0;
                        cmbTipoReceita.SelectedIndex = 0;
                        cmbClasTera.SelectedIndex = 0;
                        cmbLista.SelectedIndex = 0;
                        chkLiberado.Checked = false;
                        emGrade = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        txtDescr.Focus();
                    }
                }
                else if (tcProdutos.SelectedTab == tpComplemento)
                {
                    cmbClasTera.Focus();
                }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    txtBCod.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCod_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCod.Text.Trim()))
                {
                    cmbBDepto.Focus();
                }
                else
                    btnBuscar.PerformClick();

            }
        }

        private void cmbBDepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBDepto.SelectedIndex == -1)
                {
                    cmbBClasse.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBClasse.SelectedIndex == -1)
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbLiberado.SelectedIndex == -1)
                {
                    cmbBSub.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBSub_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBSub.SelectedIndex == -1)
                {
                    cmbBFab.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBFab_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFabricante.SelectedIndex == -1)
                {
                    cmbBPrinc.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBPrinc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBPrinc.SelectedIndex == -1)
                {
                    txtBReg.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgProdutos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgProdutos.RowCount;
                    for (i = 0; i <= dgProdutos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgProdutos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgProdutos.ColumnCount; k++)
                    {
                        if (dgProdutos.Columns[k].Visible == true)
                        {
                            switch (dgProdutos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Código de Barra":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Reg. no Ministerio da Saúde":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Custo Inicial":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Custo Médio":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Último Custo":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Comissão":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Preço Compra":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Margem":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Preço Venda":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Preço PMC":
                                    numCel = Principal.GetColunaExcel(dgProdutos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdutos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgProdutos.ColumnCount : indice) + Convert.ToString(dgProdutos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void frmCadProdutos_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }
        
        private void dgProdutos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgProdutos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtProdutos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgProdutos.Columns[e.ColumnIndex].Name + " DESC");
                        dgProdutos.DataSource = dtProdutos;
                        decrescente = true;
                    }
                    else
                    {
                        dtProdutos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgProdutos.Columns[e.ColumnIndex].Name + " ASC");
                        dgProdutos.DataSource = dtProdutos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdutos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            //toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void dgProdutos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgProdutos.Rows[e.RowIndex].Cells[12].Value.ToString() == "N")
            {
                dgProdutos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgProdutos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void txtCusIni_Validated(object sender, EventArgs e)
        {
            if (txtCusIni.Text.Trim() != "")
            {
                txtCusIni.Text = String.Format("{0:N}", Convert.ToDecimal(txtCusIni.Text));
            }
            else
                txtCusIni.Text = "0,00";
        }

        private void txtCusMed_Validated(object sender, EventArgs e)
        {
            if (txtCusMed.Text.Trim() != "")
            {
                txtCusMed.Text = String.Format("{0:N}", Convert.ToDecimal(txtCusMed.Text));
            }
            else
                txtCusMed.Text = "0,00";
        }

        private void txtCusUlt_Validated(object sender, EventArgs e)
        {
            if (txtCusUlt.Text.Trim() != "")
            {
                txtCusUlt.Text = String.Format("{0:N}", Convert.ToDecimal(txtCusUlt.Text));
            }
            else
                txtCusUlt.Text = "0,00";
        }

        private void txtComissao_Validated(object sender, EventArgs e)
        {
            if (txtComissao.Text.Trim() != "")
            {
                txtComissao.Text = String.Format("{0:N}", Convert.ToDecimal(txtComissao.Text));
            }
            else
                txtComissao.Text = "0,00";
        }

        private void txtPCompra_Validated(object sender, EventArgs e)
        {
            //if (txtPCompra.Text.Trim() != "")
            //{
            //    txtPCompra.Text = String.Format("{0:N}", Convert.ToDecimal(txtPCompra.Text));
            //}
            //else
            //    txtPCompra.Text = "0,00";
        }

        private void txtMargem_Validated(object sender, EventArgs e)
        {
            try
            {
                //if (txtMargem.Text.Trim() != "")
                //{
                //    wCusto = Convert.ToDouble(txtPCompra.Text);
                //    txtPVenda.Text = String.Format("{0:N}", wCusto + ((Convert.ToDouble(txtMargem.Text) / 100) * wCusto));
                //    txtMargem.Text = String.Format("{0:N}", Convert.ToDecimal(txtMargem.Text));

                //    if (Convert.ToDouble(txtMargem.Text) < 0)
                //    {
                //        txtEstAtu.ForeColor = Color.Red;
                //    }
                //    else
                //    {
                //        txtEstAtu.ForeColor = Color.Black;
                //    }
                //}
                //else
                //    txtMargem.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPVenda_Validated(object sender, EventArgs e)
        {
            try
            {
                //if (txtPVenda.Text.Trim() != "")
                //{
                //    if (Convert.ToDouble(txtPVenda.Text) < 0)
                //    {
                //        MessageBox.Show("Preço de Venda do produto não pode ser menor que zero", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        txtPVenda.Text = "0,00";
                //        txtPVenda.Focus();
                //    }
                //    else
                //    {
                //        wCusto = Convert.ToDouble(txtPCompra.Text);
                //        wLucro = Convert.ToDouble(txtPVenda.Text) - wCusto;
                //        if (wCusto != 0)
                //        {
                //            wMargem = (wLucro / wCusto) * 100;
                //            txtMargem.Text = String.Format("{0:N}", wMargem);
                //        }
                //        txtPVenda.Text = String.Format("{0:N}", Convert.ToDecimal(txtPVenda.Text));

                //        if (Convert.ToDouble(txtMargem.Text) < 0)
                //        {
                //            txtEstAtu.ForeColor = Color.Red;
                //        }
                //        else
                //        {
                //            txtEstAtu.ForeColor = Color.Black;
                //        }
                //    }
                //}
                //else
                //    txtPVenda.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEstIni_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtEstIni.Text.Trim() != "")
                {
                    if (Convert.ToInt32(txtEstIni.Text) < Convert.ToInt32(txtEstMax.Text))
                    {
                        txtEstAtu.ForeColor = Color.Red;
                    }
                    else
                    {
                        txtEstAtu.ForeColor = Color.Black;
                    }
                }
                else
                    txtEstIni.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEstMin_Validated(object sender, EventArgs e)
        {
            if (txtEstMin.Text.Trim() == "")
            {
                txtEstMin.Text = "0";
            }
        }

        private void txtEstMax_Validated(object sender, EventArgs e)
        {
            if (txtEstMax.Text.Trim() != "")
            {
                if (Convert.ToInt32(txtEstIni.Text) < Convert.ToInt32(txtEstMax.Text))
                {
                    txtEstAtu.ForeColor = Color.Red;
                }
                else
                {
                    txtEstAtu.ForeColor = Color.Black;
                }
            }
            else
                txtEstMax.Text = "0";
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtProdID") && (control.Name != "txtEstAtu") && (control.Name != "txtCusUlt") && (control.Name != "txtData") && (control.Name != "txtUsuario")
                    &&  (control.Name != "txtCsosn") && (control.Name != "txtCodABC") && (control.Name != "txtDtVenda"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgProdutos.ColumnCount; i++)
                {
                    if (dgProdutos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgProdutos.ColumnCount];
                string[] coluna = new string[dgProdutos.ColumnCount];

                for (int i = 0; i < dgProdutos.ColumnCount; i++)
                {
                    grid[i] = dgProdutos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgProdutos.ColumnCount; i++)
                {
                    if (dgProdutos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgProdutos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgProdutos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgProdutos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgProdutos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgProdutos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgProdutos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgProdutos.ColumnCount; i++)
                        {
                            dgProdutos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPmc_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtPmc.Text.Trim()))
            {
                txtPmc.Text = String.Format("{0:N}", txtPmc.Text);
            }
            else
            {
                txtPmc.Text = "0,00";
            }
        }

        private void txtNCM_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbTabPrecos.Focus();
        }

        private void chkLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                tcProdutos.SelectedTab = tpComplemento;
        }

        private void txtPmc_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtMsg.Focus();
        }

        public void AtalhoGrade()
        {
            tcProdutos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcProdutos.SelectedTab = tpFicha;
        }

        private void txtMsg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkBCompra.Focus();
        }

        private void cmbTipoReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtMsg.Focus();
        }

        private void chkBCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                ckbUsoContinuo.Focus();
        }

        private void txtCfop_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbCst.Focus();
        }

        private void txtCst_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCest.Focus();
        }

        private void txtCest_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbIcms.Focus();
        }

        private void tcComplemento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcComplemento.SelectedTab == tpGeral)
            {
                cmbClasTera.Focus();
            }
            else if (tcComplemento.SelectedTab == tpPrecos)
                txtEstIni.Focus();
        }

        private void txtCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtMargem.Focus();
        }

        private void txtDtVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                dtCInicial.Focus();
        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }

        private void txtValorDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }

        private void dtCInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbLista.Focus();
        }

        private void cmbIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCstIpi.Focus();
        }

        private void txtCstIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCstCofins.Focus();
        }

        private void txtCstCofins_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCstPis.Focus();
        }

        private void txtCstPis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEnqIpi.Focus();
        }

        private void cmbCst_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCst.Text == "000" || cmbCst.Text == "020")
            {
                txtCsosn.Text = "101";
            }
            else if (cmbCst.Text == "010" || cmbCst.Text == "070")
            {
                txtCsosn.Text = "201";
            }
            else if (cmbCst.Text == "030")
            {
                txtCsosn.Text = "202";
            }
            else if (cmbCst.Text == "060")
            {
                txtCsosn.Text = "500";
            }
            else if (cmbCst.Text == "040" || cmbCst.Text == "041")
            {
                txtCsosn.Text = "102";
            }
            else if (cmbCst.Text == "050" || cmbCst.Text == "051")
            {
                txtCsosn.Text = "300";
            }
            else if (cmbCst.Text == "090")
            {
                txtCsosn.Text = "900";
            }
            else
            {
                txtCsosn.Text = "";
            }
        }

        private void txtDcb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtdUniFP.Focus();
        }

        private void txtQtdUniFP_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                dtCInicial.Focus();
        }

        private void txtEnqIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCfopDev.Focus();
        }

        private void chkBloqDesconto_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkCaixaCartelado_CheckedChanged(object sender, EventArgs e)
        {
            if(chkCaixaCartelado.Checked)
            {
                txtCodCartelado.Text = "";
                txtCodCartelado.Visible = true;
                lblCartelado.Visible = true;
                label52.Visible = true;
                txtDescricaoCartelado.Visible = true;
                txtCodCartelado.Focus();
            }
            else
            {
                txtCodCartelado.Text = "";
                txtCodCartelado.Visible = false;
                lblCartelado.Visible = false;
                label52.Visible = false;
                txtDescricaoCartelado.Visible = false;
            }
        }

        private void txtPmc_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCompra.Focus();
        }

        private void txtDescricaoCartelado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodCartelado.Focus();
        }

        private void txtDescricaoCartelado_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescricaoCartelado.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodCartelado.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescricaoCartelado.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodCartelado.Text = Principal.codBarra;
                                txtDescricaoCartelado.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro de Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, txtCodCartelado.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescricaoCartelado.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescricaoCartelado.Text = "Produto não cadastrado";
                    txtCodCartelado.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro de Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtCodCartelado_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 13)
            {
                if (txtCodCartelado.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodCartelado.Focus();
                    }
                }
                else
                    txtDescricaoCartelado.Focus();
            }
        }

        private void txtRegMS_Validated(object sender, EventArgs e)
        {
            if (txtRegMS.Text.Length != 0)
                if (txtRegMS.Text[0].Equals('7'))
                    txtRegMS.Text = "";
        }
    }
}
