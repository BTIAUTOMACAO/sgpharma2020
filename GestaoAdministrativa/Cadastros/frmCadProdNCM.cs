﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Geral;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadProdNCM : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        private DataTable dtProdNCM = new DataTable();
        private int contRegistros;
        private string[,] dados = new string[4, 3];
        private bool emGrade;
        private ToolStripButton tsbProdNCM = new ToolStripButton("Cód. NCM");
        private ToolStripSeparator tssProdNCM = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private string id;
        #endregion

        public frmCadProdNCM(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmCadProdNCM_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtNcm.Focus();
        }

        public void Primeiro()
        {
            if (dtProdNCM.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgProdNCM.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgProdNCM.CurrentCell = dgProdNCM.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcCodNCM.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtProdNCM.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdNCM.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdNCM.CurrentCell = dgProdNCM.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdNCM.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdNCM.CurrentCell = dgProdNCM.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgProdNCM.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgProdNCM.CurrentCell = dgProdNCM.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadProdNcm");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, contRegistros));
            if (tcCodNCM.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcCodNCM.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtNcm.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbProdNCM.AutoSize = false;
                this.tsbProdNCM.Image = Properties.Resources.cadastro;
                this.tsbProdNCM.Size = new System.Drawing.Size(125, 20);
                this.tsbProdNCM.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbProdNCM);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssProdNCM);
                tsbProdNCM.Click += delegate
                {
                    var cadProdNCM = Application.OpenForms.OfType<frmCadProdNCM>().FirstOrDefault();
                    BotoesHabilitados();
                    cadProdNCM.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtProdNCM.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbProdNCM);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssProdNCM);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtProdNCM.Clear();
            dgProdNCM.DataSource = dtProdNCM;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            Array.Clear(dados, 0, 12);
            txtBCodBarras.Focus();
        }

        public void LimparFicha()
        {
            id = "";
            txtCodBarras.Text = "";
            txtNcm.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            Array.Clear(dados, 0, 12);
            txtCodBarras.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcCodNCM.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                id = dtProdNCM.Rows[linha]["NCM_ID"].ToString();
                txtCodBarras.Text = Funcoes.ChecaCampoVazio(dtProdNCM.Rows[linha]["COD_BARRA"].ToString());
                txtNcm.Text = Funcoes.ChecaCampoVazio(dtProdNCM.Rows[linha]["NCM"].ToString());
               
                txtData.Text = Funcoes.ChecaCampoVazio(dtProdNCM.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtProdNCM.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, linha));
                txtCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cod. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcCodNCM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcCodNCM.SelectedTab == tpGrade)
            {
                dgProdNCM.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcCodNCM.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadProdNCM");
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, contRegistros));
                        emGrade = false;
                        txtCodBarras.Focus();
                    }
                }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Principal.strSql = "SELECT A.NCM_ID, A.COD_BARRA, A.NCM, A.DTALTERACAO,"
                    + " (SELECT NOME FROM USUARIOS WHERE LOGIN_ID = A.OPALTERACAO) AS OPALTERACAO, A.DTCADASTRO, B.NOME AS OPCADASTRO"
                    + " FROM PRODUTOS_NCM A LEFT JOIN USUARIOS B ON A.OPCADASTRO = B.LOGIN_ID ";
                if (txtBCodBarras.Text.Trim() == "" && txtBNCM.Text.Trim() == "")
                {
                    if (MessageBox.Show("Está solicitando uma busca sem passar por nenhum filtro\neste processo pode demorar dependendo da quantidade de\nregistros encontados.\nConfirma Solicitação?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        strOrdem = Principal.strSql;
                        Principal.strSql += " ORDER BY A.COD_BARRA";
                    }
                    else
                    {
                        txtBCodBarras.Focus();
                        return;
                    }
                }
                else
                    {
                        if (txtBCodBarras.Text.Trim() != "")
                        {
                            Principal.strSql += " WHERE A.COD_BARRA = '" + txtBCodBarras.Text.Trim() + "'";
                        }
                        if (txtBNCM.Text.Trim() != "")
                        {
                            if (txtBCodBarras.Text.Trim() != "")
                            {
                                Principal.strSql += " AND A.NCM = '" + txtBNCM.Text.Trim() + "'";
                            }
                            else
                                Principal.strSql += " WHERE A.NCM = '" + txtBNCM.Text.Trim() + "'";
                           
                        }
                    }

                dtProdNCM = BancoDados.selecionarRegistros(Principal.strSql);
                if (dtProdNCM.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtProdNCM.Rows.Count;
                    dgProdNCM.DataSource = dtProdNCM;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, 0));
                    dgProdNCM.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProdNCM.DataSource = dtProdNCM;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBCodBarras.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgProdNCM_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, contRegistros));
            emGrade = true;
        }

        private void dgProdNCM_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcCodNCM.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgProdNCM_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgProdNCM.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtProdNCM = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgProdNCM.Columns[e.ColumnIndex].Name + " DESC");
                        dgProdNCM.DataSource = dtProdNCM;
                        decrescente = true;
                    }
                    else
                    {
                        dtProdNCM = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgProdNCM.Columns[e.ColumnIndex].Name + " ASC");
                        dgProdNCM.DataSource = dtProdNCM;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtProdNCM, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgProdNCM_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtProdNCM.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtProdNCM.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtProdNCM.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtCodBarras.Text.Trim() != "") && (txtNcm.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do Cód. NCM?", "Exclusão tabela Produtos NCM", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirClasses(id).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            Principal.strSql = "DELETE FROM CLASSES WHERE NCM_ID = " + id;
                            if (BancoDados.executarSql(Principal.strSql).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("NCM_ID", id, Principal.usuario, Convert.ToDateTime(Principal.data), "PRODUTOS_NCM", txtCodBarras.Text, Principal.motivo, Principal.estAtual);
                                dtProdNCM.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgProdNCM.DataSource = dtProdNCM;
                                tcCodNCM.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBCodBarras.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            LimparFicha();
            ///id = Funcoes.GeraID("GEN_NCM");
            return true;
        }

        public bool Atualiza()
        {
            try
            {
                int contMatriz = 0;
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Principal.strSql = "SELECT * FROM PRODUTOS_NCM WHERE NCM_ID = " + id;
                    Principal.dtBusca = BancoDados.selecionarRegistros(Principal.strSql);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        #region INSERE UM NOVO REGISTRO
                        Principal.strCmd = "INSERT INTO PRODUTOS_NCM(NCM_ID, COD_BARRA, NCM, DTCADASTRO, OPCADASTRO) VALUES(" + id + "," + txtCodBarras.Text.Trim()
                            + "','" + txtNcm.Text.Trim() + "'," + Funcoes.BDataHora(DateTime.Now) + "," + Principal.usuario + ")";
                        if (BancoDados.executarSql(Principal.strCmd).Equals(true))
                        {
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtProdNCM.Clear();
                            dgProdNCM.DataSource = dtProdNCM;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtCodBarras.Focus();
                            return false;
                        }
                        #endregion
                    }
                    else
                    {
                        #region ATUALIZA REGISTRO NA TABELA
                        Principal.strSql = "UPDATE PRODUTOS_NCM SET ";
                        if (!txtCodBarras.Text.ToUpper().Equals(Principal.dtBusca.Rows[0]["COD_BARRA"]))
                        {
                            dados[contMatriz, 0] = "COD_BARRA";
                            dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["COD_BARRA"] + "'";
                            dados[contMatriz, 2] = "'" + txtCodBarras.Text.ToUpper() + "'";
                            Principal.strSql += "COD_BARRA = '" + txtCodBarras.Text.ToUpper() + "',";
                            contMatriz = contMatriz + 1;
                        }
                        if (!txtNcm.Text.ToUpper().Equals(Principal.dtBusca.Rows[0]["NCM"]))
                        {
                            dados[contMatriz, 0] = "NCM";
                            dados[contMatriz, 1] = "'" + Principal.dtBusca.Rows[0]["NCM"] + "'";
                            dados[contMatriz, 2] = "'" + txtNcm.Text.ToUpper() + "'";
                            Principal.strSql += "NCM = '" + txtNcm.Text.ToUpper() + "',";
                            contMatriz = contMatriz + 1;
                        }
                        if (contMatriz != 0)
                        {
                            dados[contMatriz, 0] = "DTALTERACAO";
                            dados[contMatriz, 1] = Principal.dtBusca.Rows[0]["DTALTERACAO"].ToString() == "" ? "null" : "'" + Principal.dtBusca.Rows[0]["DTALTERACAO"] + "'";
                            dados[contMatriz, 2] = Funcoes.BDataHora(DateTime.Now);
                            Principal.strSql += " DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + ",";
                            contMatriz = contMatriz + 1;

                            dados[contMatriz, 0] = "OPALTERACAO";
                            dados[contMatriz, 1] = Principal.dtBusca.Rows[0]["OPALTERACAO"].ToString() == "" ? "null" : "'" + Principal.dtBusca.Rows[0]["OPALTERACAO"] + "'";
                            dados[contMatriz, 2] = "'" + Principal.usuario + "'";
                            Principal.strSql += " OPALTERACAO = " + Principal.usuario + " ";
                            contMatriz = contMatriz + 1;

                            Principal.strSql += "WHERE NCM_ID = " + id;

                            //ATUALIZA O BANCO DE DADOS//
                            if (BancoDados.executarSql(Principal.strSql).Equals(true))
                            {
                                if (Funcoes.GravaLogAlteracao("NCM_ID", id, Principal.usuario, "PRODUTOS_NCM", dados, contMatriz, Principal.estAtual).Equals(true))
                                {
                                    MessageBox.Show("Atualização realizada com sucesso!", "Cód. NCM");
                                }
                            }
                        }
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtProdNCM.Clear();
                        dgProdNCM.DataSource = dtProdNCM;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        #endregion
                    }
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio() { }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(id))
            {
                Principal.mensagem = "NCM ID não pode ser branco, precione o botão\nInserir para gerar código do NCM.";
                Funcoes.Avisa();
                Principal.mdiPrincipal.btnIncluir.Select();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNcm.Text.Trim()))
            {
                Principal.mensagem = "NCM não pode ser em branco.";
                Funcoes.Avisa();
                txtNcm.Focus();
                return false;
            }
           

            return true;
        }

        private void txtBCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCodBarras.Text.Trim()))
                {
                    txtBNCM.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNCM_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgProdNCM.ColumnCount; i++)
                {
                    if (dgProdNCM.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgProdNCM.ColumnCount];
                string[] coluna = new string[dgProdNCM.ColumnCount];

                for (int i = 0; i < dgProdNCM.ColumnCount; i++)
                {
                    grid[i] = dgProdNCM.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgProdNCM.ColumnCount; i++)
                {
                    if (dgProdNCM.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgProdNCM.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgProdNCM.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgProdNCM.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgProdNCM.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgProdNCM.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgProdNCM.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgProdNCM.ColumnCount; i++)
                        {
                            dgProdNCM.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cód. NCM", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdNCM.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgProdNCM);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgProdNCM.RowCount;
                    for (i = 0; i <= dgProdNCM.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgProdNCM[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgProdNCM.ColumnCount; k++)
                    {
                        if (dgProdNCM.Columns[k].Visible == true)
                        {
                            switch (dgProdNCM.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgProdNCM.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdNCM.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgProdNCM.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgProdNCM.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgProdNCM.ColumnCount : indice) + Convert.ToString(dgProdNCM.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public void AtalhoGrade()
        {
            tcCodNCM.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcCodNCM.SelectedTab = tpFicha;
        }

        private void txtNcm_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
