﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Cadastros
{
    /// <summary>
    /// Cadastro de Clientes
    /// </summary>
    public partial class frmCadClientes : Form, Botoes
    {
        private DataTable dtClientes = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbClientes = new ToolStripButton("Clientes");
        private ToolStripSeparator tssClientes = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        /// <summary>
        /// Inicialização do Formulário
        /// </summary>
        /// <param name="menu">Formulário Principal</param>
        public frmCadClientes(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadClientes_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Função que Habilita ou Desabilita Botões de Cadastro
        /// </summary>
        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadClientes");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
            if (tcClientes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcClientes.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtCliID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Primeiro()
        {
            if (dtClientes.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgClientes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcClientes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                cmbLiberado.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Ultimo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = dtClientes.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClientes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClientes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Anterior()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClientes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Cria botão do formulário na barra inferior do menu principal
        /// </summary>
        public void Botao()
        {
            try
            {
                this.tsbClientes.AutoSize = false;
                this.tsbClientes.Image = Properties.Resources.cadastro;
                this.tsbClientes.Size = new System.Drawing.Size(125, 20);
                this.tsbClientes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbClientes);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssClientes);
                tsbClientes.Click += delegate
                {
                    var cadClientes = Application.OpenForms.OfType<frmCadClientes>().FirstOrDefault();
                    BotoesHabilitados();
                    cadClientes.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Carrega os dados do registro selecionado, quando muda da aba em grade para a aba em ficha
        /// </summary>
        /// <param name="linha">ID da linha do Grid dos Registros a ser carregado</param>
        public void CarregarDados(int linha)
        {
            try
            {
                if (String.IsNullOrEmpty(txtCliID.Text))
                {
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (dtClientes.Rows[linha]["CF_TIPO_DOCTO"].ToString().Equals("CPF"))
                    {
                        rdbFisica.Checked = true;
                    }
                    else
                        rdbJuridica.Checked = true;

                    txtCliID.Text = dtClientes.Rows[linha]["CF_ID"].ToString();
                    txtCpf.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_DOCTO"].ToString());
                    txtNome.Text = dtClientes.Rows[linha]["CF_NOME"].ToString();
                    txtApelido.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_APELIDO"].ToString());
                    txtCodigo.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_CODIGO"].ToString());
                    if (Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["GRUPO_DESCR"].ToString()) != "")
                    {
                        cmbGrupo.Text = dtClientes.Rows[linha]["GRUPO_DESCR"].ToString();
                    }
                    else
                        cmbGrupo.SelectedIndex = -1;

                    if (Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_STATUS"].ToString()) != "")
                    {
                        int status = Convert.ToInt32(dtClientes.Rows[linha]["CF_STATUS"].ToString().Replace(",00", ""));

                        if (status == 4)
                        {
                            cmbStatus.SelectedIndex = 0;
                        }
                        else if (status == 5)
                        {
                            cmbStatus.SelectedIndex = 1;
                        }
                        else if (status == 2)
                        {
                            cmbStatus.SelectedIndex = 2;
                        }
                        else if (status == 3)
                        {
                            cmbStatus.SelectedIndex = 3;
                        }

                    }
                    else
                        cmbStatus.SelectedIndex = -1;

                    if (Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["POS_DESCRICAO"].ToString()) != "")
                    {
                        cmbCondicao.Text = dtClientes.Rows[linha]["POS_DESCRICAO"].ToString();
                    }
                    else
                        cmbCondicao.SelectedIndex = -1;

                    txtDocumento.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_INSEST"].ToString());
                    txtEndereco.Text = dtClientes.Rows[linha]["CF_ENDER"].ToString();
                    txtNumero.Text = dtClientes.Rows[linha]["CF_END_NUM"].ToString();
                    txtBairro.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_BAIRRO"].ToString());
                    txtCidade.Text = dtClientes.Rows[linha]["CF_CIDADE"].ToString();
                    txtCep.Text = dtClientes.Rows[linha]["CF_CEP"].ToString();
                    cmbUF.Text = dtClientes.Rows[linha]["CF_UF"].ToString();
                    txtTelefone.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_FONE"].ToString());
                    txtCelular.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_CELULAR"].ToString());
                    txtRecado.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_FONE_RECADO"].ToString());
                    txtCEndereco.Text = dtClientes.Rows[linha]["CF_ENDERCOB"].ToString();
                    txtCNum.Text = dtClientes.Rows[linha]["CF_NUMEROCOB"].ToString();
                    txtCBairro.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_BAIRROCOB"].ToString());
                    txtCCidade.Text = dtClientes.Rows[linha]["CF_CIDADECOB"].ToString();
                    txtCCep.Text = dtClientes.Rows[linha]["CF_CEPCOB"].ToString();
                    cmbCUF.Text = dtClientes.Rows[linha]["CF_UFCOB"].ToString();
                    txtEEndereco.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_ENDERENT"].ToString());
                    txtENum.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_NUMEROENT"].ToString());
                    txtEBairro.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_BAIRROENT"].ToString());
                    txtECidade.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_CIDADEENT"].ToString());
                    txtECep.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_CEPENT"].ToString());
                    if (Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_UFENT"].ToString()) == "")
                    {
                        cmbEUF.SelectedIndex = -1;
                    }
                    else
                        cmbEUF.Text = dtClientes.Rows[linha]["CF_UFENT"].ToString();

                    txtEmail.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_EMAIL"].ToString());
                    txtContato.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_CONTATO"].ToString());
                    if (Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["POS_DESCRICAO"].ToString()) == "")
                    {
                        cmbCondicao.SelectedIndex = -1;
                    }
                    else
                        cmbCondicao.Text = dtClientes.Rows[linha]["POS_DESCRICAO"].ToString();

                    if (dtClientes.Rows[linha]["CON_CODIGO"].ToString() != "")
                    {
                        txtConveniada.Text = dtClientes.Rows[linha]["CON_CODIGO"].ToString();
                    }
                    else
                    {
                        txtConveniada.Text = "";
                    }


                    txtCodCartao.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CON_CARTAO"].ToString());
                    txtLimite.Text = String.Format("{0:N}", dtClientes.Rows[linha]["CF_LIMITE"]);
                    txtObs.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["CF_OBSERVACAO"].ToString());
                    if (dtClientes.Rows[linha]["CF_DESABILITADO"].ToString() == "N")
                    {
                        chkLiberado.Checked = false;
                    }
                    else
                        chkLiberado.Checked = true;

                    txtData.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["DAT_ALTERACAO"].ToString());
                    txtUsuario.Text = Funcoes.ChecaCampoVazio(dtClientes.Rows[linha]["OPALTERACAO"].ToString());
                    txtCpf.Focus();

                    //HABILITA BOTOES DE NAVEGACAO//
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, linha));
                    txtCpf.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Abre formulário se o mesmo já estiver na barra de botões
        /// </summary>
        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Fecha o formulário e remove o botão do mesmo
        /// </summary>
        public void Sair()
        {
            try
            {
                dtClientes.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbClientes);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssClientes);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Limpa os campos da Aba em Grade
        /// </summary>
        public void LimparGrade()
        {
            dtClientes.Clear();
            dgClientes.DataSource = dtClientes;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            cmbLiberado.SelectedIndex = 0;
            tslRegistros.Text = "";
            cmbGrupo.SelectedIndex = -1;
            emGrade = false;
            txtBID.Focus();
        }

        /// <summary>
        /// Limpa os campos da Aba em Ficha
        /// </summary>
        public void LimparFicha()
        {
            rdbFisica.Checked = true;
            txtCliID.Text = "";
            txtCpf.Text = "";
            txtNome.Text = "";
            txtApelido.Text = "";
            cmbGrupo.SelectedIndex = -1;
            txtEndereco.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtTelefone.Text = "";
            txtRecado.Text = "";
            txtCEndereco.Text = "";
            txtCBairro.Text = "";
            txtCCidade.Text = "";
            txtCCep.Text = "";
            cmbCUF.SelectedIndex = -1;
            txtEEndereco.Text = "";
            txtEBairro.Text = "";
            txtECidade.Text = "";
            txtECep.Text = "";
            cmbEUF.SelectedIndex = -1;
            txtEmail.Text = "";
            txtContato.Text = "";
            cmbCondicao.SelectedIndex = -1;
            chkLiberado.Checked = false;
            txtConveniada.Text = "";
            txtCodCartao.Text = "";
            txtLimite.Text = "0.00";
            txtObs.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            txtCpf.Focus();
            txtCodigo.Text = "";
            txtNumero.Text = "";
            txtENum.Text = "";
            txtCNum.Text = "";
            cmbGrupo.SelectedIndex = -1;
            rdbFisica.Focus();
        }

        /// <summary>
        /// Botão Limpar do Menu Principal, limpa os dados do Formulário
        /// </summary>
        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcClientes.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        /// <summary>
        /// Botão Impressão de Relatório do Menu Principal
        /// </summary>
        public void ImprimirRelatorio()
        {
        }

        /// <summary>
        /// Botão Excluir do Menu Principal, remove o registro selecionado
        /// </summary>
        /// <returns></returns>
        public bool Excluir()
        {
            try
            {
                if ((txtCliID.Text.Trim() != "") && (txtNome.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do cliente?", "Exclusão tabela Clientes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirClientes(txtCliID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            Cliente cli = new Cliente();
                            if (cli.ExcluirDados(txtCpf.Text).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("CLI_ID", txtCliID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CLIENTES", txtCpf.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Cliente excluído com sucesso!", "Clientes", MessageBoxButtons.OK);
                                dtClientes.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgClientes.DataSource = dtClientes;
                                emGrade = false;
                                tcClientes.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                            else {

                                MessageBox.Show("Erro ao excluir caso o erro persista contate a administradora", "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Botão Atualizar do Menu Principal, atualiza um registro
        /// </summary>
        /// <returns></returns>
        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {

                    int status = 0;
                    if (cmbStatus.SelectedIndex == 0)
                    {
                        status = 4;
                    }
                    else if (cmbStatus.SelectedIndex == 1)
                    {
                        status = 5;
                    }
                    else if (cmbStatus.SelectedIndex == 2)
                    {
                        status = 2;
                    }
                    else if (cmbStatus.SelectedIndex == 3)
                    {
                        status = 3;
                    }


                    Cliente cli = new Cliente
                    {
                        CfId = Convert.ToInt32(txtCliID.Text),
                        CfTipoDocto = rdbFisica.Checked == true ? 0 : 1,
                        CfDocto = txtCpf.Text,
                        CfNome = txtNome.Text,
                        CfApelido = txtApelido.Text,
                        CfCodigo = txtCodigo.Text,
                        GrupoCodigo = cmbGrupo.SelectedIndex == 0 ? 0 : Convert.ToInt32(cmbGrupo.SelectedValue),
                        CfInscricaoEstadual = txtDocumento.Text,
                        CfStatus = status,
                        CfEndereco = txtEndereco.Text,
                        CfNumeroEndereco = txtNumero.Text,
                        CfBairro = txtBairro.Text,
                        CfCidade = txtCidade.Text,
                        CfCEP = Funcoes.RemoveCaracter(txtCep.Text),
                        CfUF = cmbUF.Text,
                        CfTelefone = Funcoes.RemoveCaracter(txtTelefone.Text).Replace(" ", "").Trim(),
                        CfCelular = Funcoes.RemoveCaracter(txtCelular.Text).Replace(" ", "").Trim(),
                        CfTelRecado = Funcoes.RemoveCaracter(txtRecado.Text).Replace(" ", "").Trim(),
                        CfEmail = txtEmail.Text,
                        CfContato = txtContato.Text,
                        CfFormaPag = Convert.ToInt32(cmbCondicao.SelectedValue),
                        CfLiberado = chkLiberado.Checked == true ? "N" : "S",
                        NumCartao = txtCodCartao.Text,
                        CfLimite = txtLimite.Text == "" ? 0 : Convert.ToDouble(txtLimite.Text),
                        CfObservacao = txtObs.Text,
                        CodigoConveniada = txtConveniada.Text == "" ? 0 : Convert.ToInt32(txtConveniada.Text),
                        // endereco Cobranca
                        CfEnderecoCobranca = txtCEndereco.Text,
                        CfBairroCobranca = txtCBairro.Text,
                        CfNumeroCobranca = txtCNum.Text,
                        CfCidadeCobranca = txtCCidade.Text,
                        CfCEPCobranca = Funcoes.RemoveCaracter(txtCCep.Text),
                        CfUFCobranca = cmbCUF.Text,
                        // endereco entrega
                        CfEnderecoEntrega = txtEEndereco.Text,
                        CfBairroEntrega = txtEBairro.Text,
                        CfNumeroEntrega = txtENum.Text,
                        CfCidadeEntrega = txtECidade.Text,
                        CfCEPEntrega = Funcoes.RemoveCaracter(txtECep.Text),
                        CfUFEntrega = cmbEUF.Text,

                    };

                    Principal.dtBusca = Util.RegistrosPorEmpresa("CLIFOR", "CF_ID", txtCliID.Text, false);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Erro ao efetuar atualização, cliente não encontrado.";
                        Funcoes.Avisa();
                        txtNome.Text = "";
                        return false;
                    }
                    else if (cli.AtualizaDados(cli, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Cliente");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtClientes.Clear();
                        dgClientes.DataSource = dtClientes;
                        emGrade = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;

                    }
                    else
                    {

                        MessageBox.Show("Erro ao tentar afetuar a atualização", "Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCpf.Text)))
            {
                Principal.mensagem = "N° do Docto não pode ser em branco.";
                Funcoes.Avisa();
                txtCpf.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (Equals(cmbStatus.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um status.";
                Funcoes.Avisa();
                cmbStatus.Focus();
                return false;
            }
            if (Equals(cmbGrupo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um grupo.";
                Funcoes.Avisa();
                cmbGrupo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtENum.Text.Trim()))
            {
                Principal.mensagem = "Numero de Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtENum.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
            {
                Principal.mensagem = "Cep não pode ser em branco.";
                Funcoes.Avisa();
                txtCep.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço de cobrança não pode ser em branco.";
                Funcoes.Avisa();
                txtCEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade de cobrança não pode ser em branco.";
                Funcoes.Avisa();
                txtCCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCCep.Text)))
            {
                Principal.mensagem = "Cep de cobrança não pode ser em branco.";
                Funcoes.Avisa();
                txtCCep.Focus();
                return false;
            }
            if (Equals(cmbCUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbCUF.Focus();
                return false;
            }

            return true;
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtApelido.Focus();
        }

        private void txtApelido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbStatus.Focus();
        }

        private void cmbGrupo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocumento.Focus();
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBairro.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUF.Focus();
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtEndereco.Focus();

                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {
                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.Length > 20 ? Funcoes.RemoverAcentuacao(end.bairro.Substring(0,19).ToUpper()) : Funcoes.RemoverAcentuacao(end.bairro.ToUpper());
                        txtCidade.Text = Funcoes.RemoverAcentuacao(end.localidade.ToUpper());
                        cmbUF.Text = end.uf;
                        txtNumero.Focus();
                    }
                }

                Cursor = Cursors.Default;
            }

        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCelular.Focus();
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtRecado.Focus();
        }

        private void txtRecado_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCCep.Focus();
        }

        private void txtCEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCNum.Focus();
        }

        private void txtCBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbCUF.Focus();
        }

        private void txtCCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCCep.Focus();
        }

        private async void txtCCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtCEndereco.Focus();

                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCCep.Text);
                    if (end.bairro != null)
                    {

                        txtCEndereco.Text = end.logradouro.ToUpper();
                        txtCBairro.Text = end.bairro.ToUpper();
                        txtCCidade.Text = end.localidade.ToUpper();
                        cmbCUF.Text = end.uf;

                    }
                }
                Cursor = Cursors.Default;
            }
        }

        private void cmbCUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtECep.Focus();
        }

        private void txtEEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEBairro.Focus();
        }

        private void txtEBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbEUF.Focus();
        }

        private void txtECidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtECep.Focus();
        }

        private async void txtECep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtEBairro.Focus();
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtECep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCCep.Text);
                    if (end.bairro != null)
                    {
                        txtEEndereco.Text = end.logradouro.ToUpper();
                        txtEBairro.Text = end.bairro.ToUpper();
                        txtECidade.Text = end.localidade.ToUpper();
                        cmbEUF.Text = end.uf;
                    }
                }
                Cursor = Cursors.Default;
            }

        }

        private void cmbEUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEmail.Focus();
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtContato.Focus();
        }

        private void txtContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbCondicao.Focus();
        }

        private void cmbCondicao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void btnCMesmo_Click(object sender, EventArgs e)
        {
            txtCEndereco.Text = txtEndereco.Text;
            txtCNum.Text = txtNumero.Text;
            txtCBairro.Text = txtBairro.Text;
            txtCCidade.Text = txtCidade.Text;
            txtCCep.Text = txtCep.Text;
            cmbCUF.Text = cmbUF.Text;
        }

        private void btnEMesmo_Click(object sender, EventArgs e)
        {
            txtEEndereco.Text = txtEndereco.Text;
            txtENum.Text = txtNumero.Text;
            txtEBairro.Text = txtBairro.Text;
            txtECidade.Text = txtCidade.Text;
            txtECep.Text = txtCep.Text;
            cmbEUF.Text = cmbUF.Text;
            txtEmail.Focus();
        }

        private void txtConveniada_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtCodCartao.Focus();
            }
        }

        private void cmbConveniada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodCartao.Focus();
        }

        private void txtCodCartao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtLimite.Focus();
        }

        private void txtLimite_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void tcClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcClientes.SelectedTab == tpGrade)
            {
                dgClientes.Focus();
                cmbBGrupo.SelectedIndex = -1;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcClientes.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadClientes");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
                        emGrade = false;
                        rdbFisica.Checked = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        txtCpf.Focus();
                    }
                }
                else
                    if (tcClientes.SelectedTab == tpComplemento)
                    {
                        txtConveniada.Focus();
                    }
        }

        /// <summary>
        /// Botão Incluir do Menu Principal, insere um registro
        /// </summary>
        /// <returns></returns>
        public bool Incluir()
        {
            try
            {

                if (ConsisteCampos().Equals(true))
                {
                    int status = 0;
                    if (cmbStatus.SelectedIndex == 0)
                    {
                        status = 4;
                    }
                    else if (cmbStatus.SelectedIndex == 1)
                    {
                        status = 5;
                    }
                    else if (cmbStatus.SelectedIndex == 2)
                    {
                        status = 2;
                    }
                    else if (cmbStatus.SelectedIndex == 3)
                    {
                        status = 3;
                    }

                    chkLiberado.Checked = true;

                    Cliente cli = new Cliente
                    {
                        CfId = Funcoes.IdentificaVerificaID("CLIFOR", "CF_ID", 0, string.Empty),
                        CfTipoDocto = rdbFisica.Checked == true ? 0 : 1,
                        CfStatus = status,
                        CfDocto = txtCpf.Text,
                        CfNome = txtNome.Text,
                        CfInscricaoEstadual = txtDocumento.Text,
                        CfApelido = txtApelido.Text,
                        CfCodigo = txtCodigo.Text,
                        GrupoCodigo = cmbGrupo.SelectedIndex == 0 ? 0 : Convert.ToInt32(cmbGrupo.SelectedValue),
                        CfEndereco = txtEndereco.Text,
                        CfNumeroEndereco = txtNumero.Text,
                        CfBairro = txtBairro.Text.Length > 20 ? txtBairro.Text.Substring(0,19) : txtBairro.Text,
                        CfCidade = txtCidade.Text,
                        CfCEP = Funcoes.RemoveCaracter(txtCep.Text),
                        CfUF = cmbUF.Text,
                        CfTelefone = Funcoes.RemoveCaracter(txtTelefone.Text).Replace(" ","").Trim(),
                        CfCelular = Funcoes.RemoveCaracter(txtCelular.Text).Replace(" ", "").Trim(),
                        CfTelRecado = Funcoes.RemoveCaracter(txtRecado.Text).Replace(" ", "").Trim(),
                        CfEmail = txtEmail.Text,
                        CfContato = txtContato.Text,
                        CfFormaPag = cmbCondicao.SelectedIndex == 0 ? 0 : Convert.ToInt32(cmbCondicao.SelectedValue),
                        CfLiberado = chkLiberado.Checked == true ? "N" : "S",
                        NumCartao = txtCodCartao.Text,
                        CfLimite = txtLimite.Text == "" ? 0 : Convert.ToDouble(txtLimite.Text),
                        CfObservacao = txtObs.Text,
                        CodigoConveniada = txtConveniada.Text == "" ? 0 : Convert.ToInt32(txtConveniada.Text),
                        // endereco Cobranca
                        CfEnderecoCobranca = txtCEndereco.Text,
                        CfBairroCobranca = txtCBairro.Text.Length > 20 ? txtCBairro.Text.Substring(0, 19) : txtCBairro.Text,
                        CfNumeroCobranca = txtCNum.Text,
                        CfCidadeCobranca = txtCCidade.Text,
                        CfCEPCobranca = Funcoes.RemoveCaracter(txtCCep.Text),
                        CfUFCobranca = cmbCUF.Text,
                        // endereco entrega
                        CfEnderecoEntrega = txtEEndereco.Text,
                        CfBairroEntrega = txtEBairro.Text.Length > 20 ? txtEBairro.Text.Substring(0, 19) : txtEBairro.Text,
                        CfNumeroEntrega = txtENum.Text,
                        CfCidadeEntrega = txtECidade.Text,
                        CfCEPEntrega = Funcoes.RemoveCaracter(txtECep.Text),
                        CfUFEntrega = cmbEUF.Text
                    };


                    if (cli.VerificaCPF(Funcoes.RemoveCaracter(txtCpf.Text)).Equals(true))
                    {
                        if (cli.InsereRegistros(cli).Equals(true))
                        {
                            dtClientes.Clear();
                            dgClientes.DataSource = dtClientes;
                            emGrade = false;
                            Limpar();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Erro: ao tentar cadastrar", "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtCpf.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("CPF ja cadastrado ", "Clientes");
                        txtCpf.Focus();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Carrega os combobox presentes no formulário
        /// </summary>
        /// <returns></returns>
        public bool CarregaCombos()
        {
            try
            {
                //CARREGA GRUPO DE CLIENTES//
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("GRUPO_CODIGO", "GRUPO_DESCR", "GRUPO_CLIENTES", false, "GRUPO_DESABILITADO= 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Grupo de Clientes,\npara cadastrar um cliente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    DataRow row = Principal.dtPesq.NewRow();
                    Principal.dtPesq.Rows.InsertAt(row, 0);
                    cmbGrupo.DataSource = Principal.dtPesq;
                    cmbGrupo.DisplayMember = "GRUPO_DESCR";
                    cmbGrupo.ValueMember = "GRUPO_CODIGO";
                    cmbGrupo.SelectedIndex = 0;


                    cmbBGrupo.DataSource = Principal.dtPesq;
                    cmbBGrupo.DisplayMember = "GRUPO_DESCR";
                    cmbBGrupo.ValueMember = "GRUPO_CODIGO";
                    cmbBGrupo.SelectedIndex = -1;

                }

                //CARREGA CONDICOES DE PAGAMENTO//
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("POS_NUMERO", "POS_DESCRICAO", "POS", true, "POS_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário pelo menos uma Condição de Pagamento,\npara cadastrar um cliente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    DataRow row = Principal.dtPesq.NewRow();
                    Principal.dtPesq.Rows.InsertAt(row, 0);
                    cmbCondicao.DataSource = Principal.dtPesq;
                    cmbCondicao.DisplayMember = "POS_DESCRICAO";
                    cmbCondicao.ValueMember = "POS_NUMERO";
                    cmbCondicao.SelectedIndex = 0;
                }
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmCadClientes_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    txtBCpf.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        /// <summary>
        /// Exporta os dados do Grid para Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgClientes.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;

                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgClientes);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }
                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgClientes.RowCount;
                    for (i = 0; i <= dgClientes.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgClientes[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;

                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgClientes.ColumnCount; k++)
                    {
                        if (dgClientes.Columns[k].Visible == true)
                        {
                            switch (dgClientes.Columns[k].HeaderText)
                            {
                                case "Cód. Cartão":
                                    numCel = Principal.GetColunaExcel(dgClientes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgClientes.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000000";
                                    break;
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgClientes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgClientes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgClientes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgClientes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgClientes.ColumnCount : indice) + Convert.ToString(dgClientes.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void dgClientes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtCliID.Text = "";
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
            emGrade = true;
        }

        private void dgClientes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                txtCliID.Text = "";
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcClientes.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgClientes_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgClientes.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtClientes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgClientes.Columns[e.ColumnIndex].Name + " DESC");
                        dgClientes.DataSource = dtClientes;
                        decrescente = true;
                    }
                    else
                    {
                        dtClientes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgClientes.Columns[e.ColumnIndex].Name + " ASC");
                        dgClientes.DataSource = dtClientes;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void dgClientes_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtClientes.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtClientes.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtClientes.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }
        
        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox9_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox9, "Campo Obrigatório");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox10, "Campo Obrigatório");
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox11, "Campo Obrigatório");
        }
        
        private void dgClientes_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgClientes.Rows[e.RowIndex].Cells[40].Value.ToString() == "N")
            {
                dgClientes.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgClientes.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Cliente cliente = new Cliente
                {
                    CfId = txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text),
                    CfNome = txtBNome.Text.Trim(),
                    CfDocto = Funcoes.RemoveCaracter(txtBCpf.Text.Trim()),
                    GrupoCodigo = cmbBGrupo.SelectedIndex == -1 ? 0 : cmbBGrupo.SelectedIndex == 0 ? 0 : Convert.ToInt32(cmbBGrupo.SelectedValue),
                    CfCidade = txtBCidade.Text.Trim(),
                    CfCodigo = txtBConv.Text.Trim(),
                    CfLiberado = cmbLiberado.Text

                };

                dtClientes = cliente.DadosCliente(cliente, out strOrdem);

                if (dtClientes.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtClientes.Rows.Count;
                    dgClientes.DataSource = dtClientes;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, 0));
                    dgClientes.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgClientes.DataSource = dtClientes;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                    cmbBGrupo.SelectedIndex = -1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtBCpf.Text)))
                {
                    cmbBGrupo.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBGrupo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBGrupo.SelectedIndex == -1)
                {
                    txtBCidade.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCidade.Text.Trim()))
                {
                    txtBConv.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBConv_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBConv.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void txtLimite_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtLimite.Text.Trim()))
            {
                txtLimite.Text = String.Format("{0:N}", Convert.ToDecimal(txtLimite.Text));
            }
            else
                txtLimite.Text = "0,00";
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtCliID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbGrupo.Focus();
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgClientes.ColumnCount; i++)
                {
                    if (dgClientes.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgClientes.ColumnCount];
                string[] coluna = new string[dgClientes.ColumnCount];

                for (int i = 0; i < dgClientes.ColumnCount; i++)
                {
                    grid[i] = dgClientes.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgClientes.ColumnCount; i++)
                {
                    if (dgClientes.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgClientes.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgClientes.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgClientes.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgClientes.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgClientes.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgClientes.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgClientes.ColumnCount; i++)
                        {
                            dgClientes.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Atalho F1 para ir para aba em Grade
        /// </summary>
        public void AtalhoGrade()
        {
            tcClientes.SelectedTab = tpGrade;
        }

        /// <summary>
        /// Atalho F2 para ir para aba em Ficha
        /// </summary>
        public void AtalhoFicha()
        {
            tcClientes.SelectedTab = tpFicha;
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox14, "Campo Obrigatório");
        }

        private void rdbFisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbFisica.Checked.Equals(true))
            {
                txtCpf.Mask = "000,000,000-00";
                txtCpf.Focus();
            }
        }

        private void rdbJuridica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbJuridica.Checked.Equals(true))
            {
                txtCpf.Mask = "00,000,000/0000-00";
                txtCpf.Focus();
            }
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox12, "Campo Obrigatório");
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtCNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCCidade.Focus();
        }

        private void txtENum_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtECidade.Focus();
        }

        private void txtEmail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtEmail.Text.Trim()))
            {
                if (Funcoes.ValidaEmail(txtEmail.Text.Trim()).Equals(false))
                {
                    e.Cancel = true;
                    MessageBox.Show("Informe um email válido.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox15, "Campo Obrigatório");
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox16, "Campo Obrigatório");
        }

        /// <summary>
        /// Botão Ajuda do Menu Principal
        /// </summary>
        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Consulta endereço via CEP utilizando a API Via CEP
        /// </summary>
        /// <param name="cep">Número do CEP</param>
        /// <returns></returns>
        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 13)
                cmbGrupo.Focus();

        }

        private void txtDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox17, "Campo Obrigatório");
        }

        private void txtCpf_DoubleClick(object sender, EventArgs e)
        {
            txtCpf.Text = Util.GerarCpf();
        }
        
    }
}
