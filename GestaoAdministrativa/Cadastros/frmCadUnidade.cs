﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadUnidade : Form, Botoes
    {
        private DataTable dtTipoUni = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTipoUni = new ToolStripButton("Tipo de Unidade");
        private ToolStripSeparator tssTipoUni = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadUnidade(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadUnidade_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtTipoUni.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTipoUni.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTipoUni.CurrentCell = dgTipoUni.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTipoUni.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTipoUni.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoUni.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoUni.CurrentCell = dgTipoUni.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoUni.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoUni.CurrentCell = dgTipoUni.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTipoUni.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTipoUni.CurrentCell = dgTipoUni.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadUnidade");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, contRegistros));
            if (tcTipoUni.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTipoUni.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtUniID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTipoUni.AutoSize = false;
                this.tsbTipoUni.Image = Properties.Resources.cadastro;
                this.tsbTipoUni.Size = new System.Drawing.Size(160, 20);
                this.tsbTipoUni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTipoUni);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTipoUni);
                tsbTipoUni.Click += delegate
                {
                    var cadTipoUni = Application.OpenForms.OfType<frmCadUnidade>().FirstOrDefault();
                    BotoesHabilitados();
                    cadTipoUni.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtTipoUni.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTipoUni);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTipoUni);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTipoUni.Clear();
            dgTipoUni.DataSource = dtTipoUni;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtUniID.Text = "";
            txtDescrAbr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTipoUni.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        private void tcTipoUni_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTipoUni.SelectedTab == tpGrade)
            {
                dgTipoUni.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTipoUni.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadUnidade");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, contRegistros));
                    chkLiberado.Checked = true;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                    txtDescr.Focus();
                }
            }
        }

        private void dgTipoUni_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, contRegistros));
            emGrade = true;
        }

        private void dgTipoUni_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTipoUni.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTipoUni_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTipoUni.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTipoUni.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTipoUni.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }


        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtUniID.Text = Funcoes.ChecaCampoVazio(dtTipoUni.Rows[linha]["UNI_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtTipoUni.Rows[linha]["UNI_DESCRICAO"].ToString());
                txtDescrAbr.Text = Funcoes.ChecaCampoVazio(dtTipoUni.Rows[linha]["UNI_DESC_ABREV"].ToString());
                if (dtTipoUni.Rows[linha]["UNI_LIBERADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                {
                    chkLiberado.Checked = true;
                }

                txtData.Text = Funcoes.ChecaCampoVazio(dtTipoUni.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTipoUni.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTipoUni.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTipoUni);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTipoUni.RowCount;
                    for (i = 0; i <= dgTipoUni.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTipoUni[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTipoUni.ColumnCount; k++)
                    {
                        if (dgTipoUni.Columns[k].Visible == true)
                        {
                            switch (dgTipoUni.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTipoUni.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTipoUni.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTipoUni.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTipoUni.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTipoUni.ColumnCount : indice) + Convert.ToString(dgTipoUni.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }


        public bool Excluir()
        {
            try
            {
                if ((txtUniID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do tipo de unidade?", "Exclusão tabela Tipo de Unidade", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirTipoUni(txtDescrAbr.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var unidade = new TipoUnidade();
                            if (!unidade.ExcluiTipoUnidade(Convert.ToInt32(txtUniID.Text)).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("UNI_CODIGO", txtUniID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "TIPO_UNIDADE", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtTipoUni.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTipoUni.DataSource = dtTipoUni;
                                tcTipoUni.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                            else
                                return false;
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    var tUnidade = new TipoUnidade(
                            Funcoes.IdentificaVerificaID("TIPO_UNIDADE", "UNI_CODIGO", 0, ""),
                            txtDescr.Text.ToUpper(),
                            txtDescrAbr.Text.Trim().ToUpper(),
                            chkLiberado.Checked == true ? "S" : "N",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario
                    );

                    //VERIFICA SE UNIDADE JA ESTA CADASTRADO//
                    if (Util.RegistrosPorEstabelecimento("TIPO_UNIDADE", "UNI_DESCRICAO", txtDescr.Text.ToUpper()).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Tipo de unidade já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else if (tUnidade.InsereRegistros(tUnidade).Equals(true))
                    {
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTipoUni.Clear();
                        dgTipoUni.DataSource = dtTipoUni;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {

                        Cursor = Cursors.Default;
                        MessageBox.Show("Erro ao efetuar o cadastro", "Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;

                    }
                }

                chkLiberado.Checked = true;
                txtDescr.Focus();
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescrAbr.Text.Trim()))
            {
                Principal.mensagem = "Descr. abrev. não pode ser em branco.";
                Funcoes.Avisa();
                txtDescrAbr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var tUnidade = new TipoUnidade(
                        Convert.ToInt32(txtUniID.Text),
                        txtDescr.Text.ToUpper(),
                        txtDescrAbr.Text.Trim().ToUpper(),
                        chkLiberado.Checked == true ? "S" : "N",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("TIPO_UNIDADE", "UNI_CODIGO", txtUniID.Text);

                    if (tUnidade.AtualizaDados(tUnidade, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Tipo de Unidade");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTipoUni.Clear();
                        dgTipoUni.DataSource = dtTipoUni;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao tentar atualizar", "Fabricantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;

                    }
                }
                Cursor = Cursors.Default;
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var unidade = new TipoUnidade();

                dtTipoUni = unidade.BuscarDados(txtBID.Text, txtBDescr.Text.Trim(), cmbLiberado.Text, out strOrdem);
                if (dtTipoUni.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTipoUni.Rows.Count;
                    dgTipoUni.DataSource = dtTipoUni;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, 0));
                    dgTipoUni.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTipoUni.DataSource = dtTipoUni;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtDescrAbr.Focus();
            }
        }

        private void txtDescrAbr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void dgTipoUni_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTipoUni.Rows.Count > 0)
                {
                    Cursor = Cursors.Default;
                    if (decrescente == false)
                    {
                        dtTipoUni = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTipoUni.Columns[e.ColumnIndex].Name + " DESC");
                        dgTipoUni.DataSource = dtTipoUni;
                        decrescente = true;
                    }
                    else
                    {
                        dtTipoUni = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTipoUni.Columns[e.ColumnIndex].Name + " ASC");
                        dgTipoUni.DataSource = dtTipoUni;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTipoUni, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void dgTipoUni_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTipoUni.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgTipoUni.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTipoUni.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtUniID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTipoUni.ColumnCount; i++)
                {
                    if (dgTipoUni.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTipoUni.ColumnCount];
                string[] coluna = new string[dgTipoUni.ColumnCount];

                for (int i = 0; i < dgTipoUni.ColumnCount; i++)
                {
                    grid[i] = dgTipoUni.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTipoUni.ColumnCount; i++)
                {
                    if (dgTipoUni.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTipoUni.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTipoUni.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTipoUni.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTipoUni.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTipoUni.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTipoUni.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTipoUni.ColumnCount; i++)
                        {
                            dgTipoUni.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tipo de Unidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTipoUni.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTipoUni.SelectedTab = tpGrade;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
