﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadMedicos : Form, Botoes
    {
        private DataTable dtMedicos = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbMedicos = new ToolStripButton("Médicos");
        private ToolStripSeparator tssMedicos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadMedicos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadMedicos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBCrm.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadMedicos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, contRegistros));
            if (tcMedicos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcMedicos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtMedID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbMedicos.AutoSize = false;
                this.tsbMedicos.Image = Properties.Resources.cadastro;
                this.tsbMedicos.Size = new System.Drawing.Size(125, 20);
                this.tsbMedicos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbMedicos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssMedicos);
                tsbMedicos.Click += delegate
                {
                    var cadMedicos = Application.OpenForms.OfType<frmCadMedicos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadMedicos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtMedicos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgMedicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgMedicos.CurrentCell = dgMedicos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcMedicos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtMedicos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMedicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMedicos.CurrentCell = dgMedicos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMedicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMedicos.CurrentCell = dgMedicos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMedicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMedicos.CurrentCell = dgMedicos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Medico med = new Medico()
                    {
                        MedID = Convert.ToInt32(txtMedID.Text),
                        MedCodigo = txtCrm.Text,
                        MedNome = txtNome.Text,
                        MedCpf = txtCpf.Text,
                        MedRg = txtRg.Text,
                        MedCrm = txtCrm.Text,
                        MedNascimento = Funcoes.RemoveCaracter(txtDtNasc.Text) == "" ? "" : txtDtNasc.Text,
                        MedEnder = txtEndereco.Text,
                        MedBairro = txtBairro.Text,
                        MedCep = Funcoes.RemoveCaracter(txtCep.Text),
                        MedCidade = txtCidade.Text,
                        MedUf = cmbUF.Text,
                        MedFone = Funcoes.RemoveCaracter(txtTelefone.Text),
                        MedObs = txtObs.Text,
                        MedLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario

                    };
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("MEDICOS", "MED_ID", txtMedID.Text);
                    if (med.AtualizaDados(med, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Médicos");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        LimparGrade();
                        return true;

                    }
                    else
                    {
                        MessageBox.Show("Erro ao efetuar atualização", "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;

                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCrm.Text.Trim()))
            {
                Principal.mensagem = "CRM não pode ser em branco.";
                Funcoes.Avisa();
                txtCrm.Focus();
                return false;
            }
            if (cmbUF.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar a UF.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }

            return true;
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtMedID.Text = dtMedicos.Rows[linha]["MED_ID"].ToString();
                txtNome.Text = dtMedicos.Rows[linha]["MED_NOME"].ToString();
                txtCpf.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_CPF"].ToString());
                txtRg.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_RG"].ToString());
                txtCrm.Text = dtMedicos.Rows[linha]["MED_CRM"].ToString();
                txtDtNasc.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_NASCIMENTO"].ToString());
                txtEndereco.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_ENDERECO"].ToString());
                txtBairro.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_BAIRRO"].ToString());
                txtCidade.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_CIDADE"].ToString());
                txtCep.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_CEP"].ToString());
                if (Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_UF"].ToString()) == "")
                {
                    cmbUF.SelectedIndex = -1;
                }
                else
                    cmbUF.Text = dtMedicos.Rows[linha]["MED_UF"].ToString();

                txtTelefone.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_FONE"].ToString());
                txtObs.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["MED_OBS"].ToString());
                if (dtMedicos.Rows[linha]["MED_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtMedicos.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, linha));
                txtNome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    chkLiberado.Checked = true;

                    Medico med = new Medico()
                    {
                        MedID = Funcoes.IdentificaVerificaID("MEDICOS", "MED_ID", 0, string.Empty),
                        MedCodigo = txtCrm.Text,
                        MedNome = txtNome.Text,
                        MedCpf = txtCpf.Text,
                        MedRg = txtRg.Text,
                        MedCrm = txtCrm.Text,
                        MedNascimento = txtDtNasc.Text,
                        MedEnder = txtEndereco.Text,
                        MedBairro = txtBairro.Text,
                        MedCep = Funcoes.RemoveCaracter(txtCep.Text),
                        MedCidade = txtCidade.Text,
                        MedUf = cmbUF.Text,
                        MedFone = txtTelefone.Text,
                        MedObs = txtObs.Text,
                        MedLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario

                    };

                    if (Util.RegistrosPorEstabelecimento("MEDICOS", "MED_CRM", med.MedCrm).Rows.Count != 0)
                    {

                        Principal.mensagem = "CRM já cadastrado.";
                        Funcoes.Avisa();
                        txtCrm.Text = "";
                        txtCrm.Focus();
                        return false;
                    }
                    else if (med.IncluirDados(med).Equals(true))
                    {
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        LimparGrade();
                        cmbLiberado.SelectedIndex = 0;
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro ao efetuar o Cadastro", "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {

                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtMedID.Text.Trim() != "") && (txtNome.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do médico?", "Exclusão tabela Médicos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirMedico(txtCrm.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            Medico med = new Medico();

                            if (med.ExcluirDados(txtCrm.Text).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("MED_CODIGO", txtMedID.Text, Principal.usuario.ToUpper(), Convert.ToDateTime(Principal.data), "MEDICOS", txtNome.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Médico excluído com sucesso", "Médicos", MessageBoxButtons.OK);
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                LimparFicha();
                                LimparGrade();
                                emGrade = false;
                                tcMedicos.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBCrm.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Sair()
        {
            try
            {
                dtMedicos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbMedicos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssMedicos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtMedicos.Clear();
            dgMedicos.DataSource = dtMedicos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBCrm.Focus();
        }

        public void LimparFicha()
        {
            txtMedID.Text = "";
            txtNome.Text = "";
            txtCpf.Text = "";
            txtRg.Text = "";
            txtCrm.Text = "";
            txtEndereco.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            txtDtNasc.Text = "";
            txtObs.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            chkLiberado.Checked = false; txtData.Text = "";
            txtUsuario.Text = "";
            cmbLiberado.SelectedIndex = 0;
            txtNome.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcMedicos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void ImprimirRelatorio()
        {
        }

        private void tcMedicos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcMedicos.SelectedTab == tpGrade)
            {
                dgMedicos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcMedicos.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadMedicos");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, contRegistros));
                    chkLiberado.Checked = false;
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtNome.Focus();
                }
            }
        }

        private void dgMedicos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, contRegistros));
            emGrade = true;
        }

        private void dgMedicos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcMedicos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgMedicos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgMedicos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtMedicos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgMedicos.Columns[e.ColumnIndex].Name + " DESC");
                        dgMedicos.DataSource = dtMedicos;
                        decrescente = true;
                    }
                    else
                    {
                        dtMedicos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgMedicos.Columns[e.ColumnIndex].Name + " ASC");
                        dgMedicos.DataSource = dtMedicos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, contRegistros));
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgMedicos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtMedicos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtMedicos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtMedicos.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox4_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCpf.Focus();
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtRg.Focus();
        }

        private void txtRg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCrm.Focus();
        }

        private void txtCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDtNasc.Focus();
        }

        private void txtDtNasc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtEndereco.Text))
                {
                    txtCep.Focus();
                }
                else
                    txtEndereco.Focus();
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBairro.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {

                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.ToUpper();
                        txtCidade.Text = end.localidade.ToUpper();
                        cmbUF.Text = end.uf;
                        txtTelefone.Focus();
                    }
                }
                Cursor = Cursors.Default;
            }
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void txtBCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCrm.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Medico med = new Medico
                {
                    MedCrm = txtBCrm.Text.Trim(),
                    MedNome = txtBNome.Text.Trim().ToUpper(),
                    MedLiberado = cmbLiberado.Text
                };
                dtMedicos = med.BuscaTodosMedicos(med, out strOrdem);
                if (dtMedicos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtMedicos.Rows.Count;
                    dgMedicos.DataSource = dtMedicos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMedicos, 0));
                    dgMedicos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                    cmbLiberado.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgMedicos.DataSource = dtMedicos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBCrm.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgMedicos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgMedicos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgMedicos.RowCount;
                    for (i = 0; i <= dgMedicos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgMedicos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgMedicos.ColumnCount; k++)
                    {
                        if (dgMedicos.Columns[k].Visible == true)
                        {
                            switch (dgMedicos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgMedicos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgMedicos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgMedicos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgMedicos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgMedicos.ColumnCount : indice) + Convert.ToString(dgMedicos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgMedicos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgMedicos.Rows[e.RowIndex].Cells[12].Value.ToString() == "N")
            {
                dgMedicos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgMedicos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtMedID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgMedicos.ColumnCount; i++)
                {
                    if (dgMedicos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgMedicos.ColumnCount];
                string[] coluna = new string[dgMedicos.ColumnCount];

                for (int i = 0; i < dgMedicos.ColumnCount; i++)
                {
                    grid[i] = dgMedicos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgMedicos.ColumnCount; i++)
                {
                    if (dgMedicos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgMedicos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgMedicos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgMedicos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgMedicos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgMedicos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgMedicos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgMedicos.ColumnCount; i++)
                        {
                            dgMedicos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Médicos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        public void AtalhoGrade()
        {
            tcMedicos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcMedicos.SelectedTab = tpGrade;
        }

        private void frmCadMedicos_KeyPress(object sender, KeyPressEventArgs e)
        {
            //this.ProcessTabKey(true);
            //e.Handled = true;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
