﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadConveniadas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadConveniadas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcConveniadas = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbBConvenio = new System.Windows.Forms.ComboBox();
            this.txtBCidade = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBNome = new System.Windows.Forms.TextBox();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.dgConveniadas = new System.Windows.Forms.DataGridView();
            this.cmsConveniadas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkParcelado = new System.Windows.Forms.CheckBox();
            this.cmbForma = new System.Windows.Forms.ComboBox();
            this.lblForma = new System.Windows.Forms.Label();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.cmbConvenio = new System.Windows.Forms.ComboBox();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVencimento = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFechamento = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.cmbUF = new System.Windows.Forms.ComboBox();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtConCod = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.txtConID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblEstab = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chkObrigaDescontoDaRegra = new System.Windows.Forms.CheckBox();
            this.CON_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_ENDERECO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_BAIRRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CEP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_FECHAMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_VENCIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_WEB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_REGRAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_DESABILITADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_DT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_OP_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_DT_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_OP_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_FORMA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_PARCELA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_OBRIGA_REGRA_DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcConveniadas.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConveniadas)).BeginInit();
            this.cmsConveniadas.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcConveniadas);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 21);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(5, 2);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(265, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Empresas Conveniadas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(2, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 11, 0);
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcConveniadas
            // 
            this.tcConveniadas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcConveniadas.Controls.Add(this.tpGrade);
            this.tcConveniadas.Controls.Add(this.tpFicha);
            this.tcConveniadas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcConveniadas.Location = new System.Drawing.Point(6, 33);
            this.tcConveniadas.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tcConveniadas.Name = "tcConveniadas";
            this.tcConveniadas.SelectedIndex = 0;
            this.tcConveniadas.Size = new System.Drawing.Size(965, 499);
            this.tcConveniadas.TabIndex = 40;
            this.tcConveniadas.SelectedIndexChanged += new System.EventHandler(this.tcConveniadas_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.label43);
            this.tpGrade.Controls.Add(this.label9);
            this.tpGrade.Controls.Add(this.cmbBConvenio);
            this.tpGrade.Controls.Add(this.txtBCidade);
            this.tpGrade.Controls.Add(this.label20);
            this.tpGrade.Controls.Add(this.txtBNome);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label36);
            this.tpGrade.Controls.Add(this.label35);
            this.tpGrade.Controls.Add(this.dgConveniadas);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(848, 417);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(103, 38);
            this.btnBuscar.TabIndex = 212;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(746, 431);
            this.cmbLiberado.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(88, 24);
            this.cmbLiberado.TabIndex = 211;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(743, 413);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 16);
            this.label43.TabIndex = 210;
            this.label43.Text = "Liberado";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(562, 413);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 16);
            this.label9.TabIndex = 209;
            this.label9.Text = "Sistema de Convênios";
            // 
            // cmbBConvenio
            // 
            this.cmbBConvenio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBConvenio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBConvenio.FormattingEnabled = true;
            this.cmbBConvenio.Items.AddRange(new object[] {
            "TODOS",
            "PARTICULAR",
            "CONV. DROGABELLA/REDE PLANTAO"});
            this.cmbBConvenio.Location = new System.Drawing.Point(565, 431);
            this.cmbBConvenio.Name = "cmbBConvenio";
            this.cmbBConvenio.Size = new System.Drawing.Size(173, 24);
            this.cmbBConvenio.TabIndex = 208;
            this.cmbBConvenio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBConvenio_KeyPress);
            // 
            // txtBCidade
            // 
            this.txtBCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBCidade.Location = new System.Drawing.Point(364, 433);
            this.txtBCidade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBCidade.MaxLength = 30;
            this.txtBCidade.Name = "txtBCidade";
            this.txtBCidade.Size = new System.Drawing.Size(191, 22);
            this.txtBCidade.TabIndex = 113;
            this.txtBCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCidade_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(361, 413);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 16);
            this.label20.TabIndex = 112;
            this.label20.Text = "Cidade";
            // 
            // txtBNome
            // 
            this.txtBNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBNome.Location = new System.Drawing.Point(91, 433);
            this.txtBNome.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBNome.MaxLength = 50;
            this.txtBNome.Name = "txtBNome";
            this.txtBNome.Size = new System.Drawing.Size(269, 22);
            this.txtBNome.TabIndex = 96;
            this.txtBNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNome_KeyPress);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(4, 433);
            this.txtBID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBID.MaxLength = 6;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(82, 22);
            this.txtBID.TabIndex = 95;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(88, 413);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(45, 16);
            this.label36.TabIndex = 88;
            this.label36.Text = "Nome";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(2, 414);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 16);
            this.label35.TabIndex = 87;
            this.label35.Text = "Código";
            // 
            // dgConveniadas
            // 
            this.dgConveniadas.AllowUserToAddRows = false;
            this.dgConveniadas.AllowUserToDeleteRows = false;
            this.dgConveniadas.AllowUserToOrderColumns = true;
            this.dgConveniadas.AllowUserToResizeColumns = false;
            this.dgConveniadas.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgConveniadas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgConveniadas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgConveniadas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConveniadas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgConveniadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgConveniadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CON_ID,
            this.CON_CODIGO,
            this.CON_NOME,
            this.CON_ENDERECO,
            this.CON_BAIRRO,
            this.CON_CIDADE,
            this.CON_CEP,
            this.CON_UF,
            this.CON_FECHAMENTO,
            this.CON_VENCIMENTO,
            this.CON_DESCONTO,
            this.CON_WEB,
            this.CON_REGRAS,
            this.CON_DESABILITADO,
            this.CON_DT_ALTERACAO,
            this.CON_OP_ALTERACAO,
            this.CON_DT_CADASTRO,
            this.CON_OP_CADASTRO,
            this.CON_FORMA_ID,
            this.CON_PARCELA,
            this.CON_OBRIGA_REGRA_DESCONTO});
            this.dgConveniadas.ContextMenuStrip = this.cmsConveniadas;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgConveniadas.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgConveniadas.GridColor = System.Drawing.Color.LightGray;
            this.dgConveniadas.Location = new System.Drawing.Point(6, 6);
            this.dgConveniadas.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgConveniadas.MultiSelect = false;
            this.dgConveniadas.Name = "dgConveniadas";
            this.dgConveniadas.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConveniadas.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgConveniadas.RowHeadersVisible = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dgConveniadas.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgConveniadas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConveniadas.Size = new System.Drawing.Size(945, 394);
            this.dgConveniadas.TabIndex = 1;
            this.dgConveniadas.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgConveniadas_CellMouseClick);
            this.dgConveniadas.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgConveniadas_CellMouseDoubleClick);
            this.dgConveniadas.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgConveniadas_ColumnHeaderMouseClick);
            this.dgConveniadas.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgConveniadas_RowPrePaint);
            this.dgConveniadas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgConveniadas_KeyDown);
            // 
            // cmsConveniadas
            // 
            this.cmsConveniadas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsConveniadas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsConveniadas.Name = "cmsConveniadas";
            this.cmsConveniadas.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(7, 372);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Size = new System.Drawing.Size(945, 92);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox10);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(573, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 193;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(16, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 191;
            this.pictureBox10.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(223, 47);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(129, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 28);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkObrigaDescontoDaRegra);
            this.groupBox2.Controls.Add(this.chkParcelado);
            this.groupBox2.Controls.Add(this.cmbForma);
            this.groupBox2.Controls.Add(this.lblForma);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.cmbConvenio);
            this.groupBox2.Controls.Add(this.txtDesconto);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtVencimento);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtFechamento);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.cmbUF);
            this.groupBox2.Controls.Add(this.txtCep);
            this.groupBox2.Controls.Add(this.txtCidade);
            this.groupBox2.Controls.Add(this.txtBairro);
            this.groupBox2.Controls.Add(this.txtEndereco);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.txtConCod);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.txtConID);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(5, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Size = new System.Drawing.Size(947, 325);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // chkParcelado
            // 
            this.chkParcelado.AutoSize = true;
            this.chkParcelado.ForeColor = System.Drawing.Color.Navy;
            this.chkParcelado.Location = new System.Drawing.Point(197, 270);
            this.chkParcelado.Name = "chkParcelado";
            this.chkParcelado.Size = new System.Drawing.Size(170, 20);
            this.chkParcelado.TabIndex = 251;
            this.chkParcelado.Text = "Permite Parcelamento";
            this.chkParcelado.UseVisualStyleBackColor = true;
            this.chkParcelado.Visible = false;
            // 
            // cmbForma
            // 
            this.cmbForma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbForma.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbForma.FormattingEnabled = true;
            this.cmbForma.Location = new System.Drawing.Point(386, 234);
            this.cmbForma.Name = "cmbForma";
            this.cmbForma.Size = new System.Drawing.Size(325, 24);
            this.cmbForma.TabIndex = 250;
            this.cmbForma.Visible = false;
            this.cmbForma.SelectedIndexChanged += new System.EventHandler(this.cmbForma_SelectedIndexChanged);
            // 
            // lblForma
            // 
            this.lblForma.AutoSize = true;
            this.lblForma.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForma.ForeColor = System.Drawing.Color.Navy;
            this.lblForma.Location = new System.Drawing.Point(383, 215);
            this.lblForma.Name = "lblForma";
            this.lblForma.Size = new System.Drawing.Size(146, 16);
            this.lblForma.TabIndex = 249;
            this.lblForma.Text = "Forma de Pagamento";
            this.lblForma.Visible = false;
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(16, 270);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 248;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // cmbConvenio
            // 
            this.cmbConvenio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConvenio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConvenio.FormattingEnabled = true;
            this.cmbConvenio.Items.AddRange(new object[] {
            "CONVENIO DROGABELLA/REDE PLANTAO",
            "NORMAL",
            "PARTICULAR"});
            this.cmbConvenio.Location = new System.Drawing.Point(17, 234);
            this.cmbConvenio.Name = "cmbConvenio";
            this.cmbConvenio.Size = new System.Drawing.Size(350, 24);
            this.cmbConvenio.TabIndex = 247;
            this.cmbConvenio.SelectedIndexChanged += new System.EventHandler(this.cmbConvenio_SelectedIndexChanged);
            this.cmbConvenio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbConvenio_KeyPress);
            // 
            // txtDesconto
            // 
            this.txtDesconto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesconto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto.Location = new System.Drawing.Point(842, 180);
            this.txtDesconto.MaxLength = 10;
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(98, 22);
            this.txtDesconto.TabIndex = 246;
            this.txtDesconto.Text = "0,00";
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesconto_KeyPress);
            this.txtDesconto.Validated += new System.EventHandler(this.txtDesconto_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(837, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 16);
            this.label3.TabIndex = 245;
            this.label3.Text = "% Desconto";
            // 
            // txtVencimento
            // 
            this.txtVencimento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVencimento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVencimento.Location = new System.Drawing.Point(726, 181);
            this.txtVencimento.MaxLength = 2;
            this.txtVencimento.Name = "txtVencimento";
            this.txtVencimento.Size = new System.Drawing.Size(98, 22);
            this.txtVencimento.TabIndex = 244;
            this.txtVencimento.Text = "0";
            this.txtVencimento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVencimento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVencimento_KeyPress);
            this.txtVencimento.Validated += new System.EventHandler(this.txtVencimento_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(723, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 16);
            this.label2.TabIndex = 243;
            this.label2.Text = "Vencimento";
            // 
            // txtFechamento
            // 
            this.txtFechamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFechamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechamento.Location = new System.Drawing.Point(613, 181);
            this.txtFechamento.MaxLength = 2;
            this.txtFechamento.Name = "txtFechamento";
            this.txtFechamento.Size = new System.Drawing.Size(98, 22);
            this.txtFechamento.TabIndex = 242;
            this.txtFechamento.Text = "0";
            this.txtFechamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFechamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFechamento_KeyPress);
            this.txtFechamento.Validated += new System.EventHandler(this.txtFechamento_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(610, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 16);
            this.label14.TabIndex = 241;
            this.label14.Text = "Fechamento";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(169, 215);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 240;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 16);
            this.label8.TabIndex = 238;
            this.label8.Text = "Sistema de Convênios";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(531, 163);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 232;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(421, 163);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 231;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(371, 59);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 230;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // cmbUF
            // 
            this.cmbUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUF.FormattingEnabled = true;
            this.cmbUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbUF.Location = new System.Drawing.Point(503, 180);
            this.cmbUF.Name = "cmbUF";
            this.cmbUF.Size = new System.Drawing.Size(100, 24);
            this.cmbUF.TabIndex = 229;
            this.cmbUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUF_KeyPress);
            // 
            // txtCep
            // 
            this.txtCep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCep.Location = new System.Drawing.Point(386, 181);
            this.txtCep.Mask = "00000-000";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(100, 22);
            this.txtCep.TabIndex = 228;
            this.txtCep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCep_KeyPress);
            // 
            // txtCidade
            // 
            this.txtCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.Location = new System.Drawing.Point(17, 181);
            this.txtCidade.MaxLength = 30;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(350, 22);
            this.txtCidade.TabIndex = 227;
            this.txtCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCidade_KeyPress);
            // 
            // txtBairro
            // 
            this.txtBairro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBairro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBairro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairro.Location = new System.Drawing.Point(503, 128);
            this.txtBairro.MaxLength = 20;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(437, 22);
            this.txtBairro.TabIndex = 226;
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBairro_KeyPress);
            // 
            // txtEndereco
            // 
            this.txtEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndereco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.Location = new System.Drawing.Point(16, 130);
            this.txtEndereco.MaxLength = 80;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(468, 22);
            this.txtEndereco.TabIndex = 225;
            this.txtEndereco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEndereco_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(384, 163);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 16);
            this.label18.TabIndex = 224;
            this.label18.Text = "CEP";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(500, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 16);
            this.label17.TabIndex = 223;
            this.label17.Text = "Bairro";
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(323, 77);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(429, 22);
            this.txtNome.TabIndex = 222;
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(320, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 221;
            this.label4.Text = "Nome";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(500, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 16);
            this.label7.TabIndex = 220;
            this.label7.Text = "UF";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 219;
            this.label6.Text = "Cidade";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(13, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 218;
            this.label5.Text = "Endereço";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(221, 59);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 217;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // txtConCod
            // 
            this.txtConCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConCod.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConCod.Location = new System.Drawing.Point(166, 77);
            this.txtConCod.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtConCod.MaxLength = 6;
            this.txtConCod.Name = "txtConCod";
            this.txtConCod.Size = new System.Drawing.Size(135, 22);
            this.txtConCod.TabIndex = 216;
            this.txtConCod.DoubleClick += new System.EventHandler(this.txtConCod_DoubleClick);
            this.txtConCod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConCod_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(163, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 215;
            this.label1.Text = "Código";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(119, 59);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 212;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // txtConID
            // 
            this.txtConID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtConID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConID.Location = new System.Drawing.Point(16, 77);
            this.txtConID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtConID.MaxLength = 18;
            this.txtConID.Name = "txtConID";
            this.txtConID.ReadOnly = true;
            this.txtConID.Size = new System.Drawing.Size(135, 22);
            this.txtConID.TabIndex = 211;
            this.txtConID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(13, 59);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 16);
            this.label13.TabIndex = 210;
            this.label13.Text = "Conveniada ID";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(70, 162);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 199;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(84, 111);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 198;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            // 
            // chkObrigaDescontoDaRegra
            // 
            this.chkObrigaDescontoDaRegra.AutoSize = true;
            this.chkObrigaDescontoDaRegra.ForeColor = System.Drawing.Color.Navy;
            this.chkObrigaDescontoDaRegra.Location = new System.Drawing.Point(16, 296);
            this.chkObrigaDescontoDaRegra.Name = "chkObrigaDescontoDaRegra";
            this.chkObrigaDescontoDaRegra.Size = new System.Drawing.Size(249, 20);
            this.chkObrigaDescontoDaRegra.TabIndex = 252;
            this.chkObrigaDescontoDaRegra.Text = "Obriga Regra de Desconto Máximo";
            this.chkObrigaDescontoDaRegra.UseVisualStyleBackColor = true;
            // 
            // CON_ID
            // 
            this.CON_ID.DataPropertyName = "CON_ID";
            this.CON_ID.HeaderText = "Conveniada ID";
            this.CON_ID.MaxInputLength = 18;
            this.CON_ID.Name = "CON_ID";
            this.CON_ID.ReadOnly = true;
            this.CON_ID.Width = 125;
            // 
            // CON_CODIGO
            // 
            this.CON_CODIGO.DataPropertyName = "CON_CODIGO";
            this.CON_CODIGO.HeaderText = "Código Conveniada";
            this.CON_CODIGO.MaxInputLength = 18;
            this.CON_CODIGO.Name = "CON_CODIGO";
            this.CON_CODIGO.ReadOnly = true;
            this.CON_CODIGO.Width = 150;
            // 
            // CON_NOME
            // 
            this.CON_NOME.DataPropertyName = "CON_NOME";
            this.CON_NOME.HeaderText = "Nome";
            this.CON_NOME.MaxInputLength = 60;
            this.CON_NOME.Name = "CON_NOME";
            this.CON_NOME.ReadOnly = true;
            this.CON_NOME.Width = 350;
            // 
            // CON_ENDERECO
            // 
            this.CON_ENDERECO.DataPropertyName = "CON_ENDERECO";
            this.CON_ENDERECO.HeaderText = "Endereço";
            this.CON_ENDERECO.MaxInputLength = 60;
            this.CON_ENDERECO.Name = "CON_ENDERECO";
            this.CON_ENDERECO.ReadOnly = true;
            this.CON_ENDERECO.Width = 250;
            // 
            // CON_BAIRRO
            // 
            this.CON_BAIRRO.DataPropertyName = "CON_BAIRRO";
            this.CON_BAIRRO.HeaderText = "Bairro";
            this.CON_BAIRRO.MaxInputLength = 20;
            this.CON_BAIRRO.Name = "CON_BAIRRO";
            this.CON_BAIRRO.ReadOnly = true;
            this.CON_BAIRRO.Width = 150;
            // 
            // CON_CIDADE
            // 
            this.CON_CIDADE.DataPropertyName = "CON_CIDADE";
            this.CON_CIDADE.HeaderText = "Cidade";
            this.CON_CIDADE.MaxInputLength = 30;
            this.CON_CIDADE.Name = "CON_CIDADE";
            this.CON_CIDADE.ReadOnly = true;
            this.CON_CIDADE.Width = 200;
            // 
            // CON_CEP
            // 
            this.CON_CEP.DataPropertyName = "CON_CEP";
            this.CON_CEP.HeaderText = "CEP";
            this.CON_CEP.MaxInputLength = 9;
            this.CON_CEP.Name = "CON_CEP";
            this.CON_CEP.ReadOnly = true;
            // 
            // CON_UF
            // 
            this.CON_UF.DataPropertyName = "CON_UF";
            this.CON_UF.HeaderText = "UF";
            this.CON_UF.MaxInputLength = 2;
            this.CON_UF.Name = "CON_UF";
            this.CON_UF.ReadOnly = true;
            this.CON_UF.Width = 50;
            // 
            // CON_FECHAMENTO
            // 
            this.CON_FECHAMENTO.DataPropertyName = "CON_FECHAMENTO";
            this.CON_FECHAMENTO.HeaderText = "Fechamento";
            this.CON_FECHAMENTO.MaxInputLength = 2;
            this.CON_FECHAMENTO.Name = "CON_FECHAMENTO";
            this.CON_FECHAMENTO.ReadOnly = true;
            this.CON_FECHAMENTO.Width = 125;
            // 
            // CON_VENCIMENTO
            // 
            this.CON_VENCIMENTO.DataPropertyName = "CON_VENCIMENTO";
            this.CON_VENCIMENTO.HeaderText = "Vencimento";
            this.CON_VENCIMENTO.MaxInputLength = 2;
            this.CON_VENCIMENTO.Name = "CON_VENCIMENTO";
            this.CON_VENCIMENTO.ReadOnly = true;
            this.CON_VENCIMENTO.Width = 125;
            // 
            // CON_DESCONTO
            // 
            this.CON_DESCONTO.DataPropertyName = "CON_DESCONTO";
            dataGridViewCellStyle6.Format = "C2";
            dataGridViewCellStyle6.NullValue = null;
            this.CON_DESCONTO.DefaultCellStyle = dataGridViewCellStyle6;
            this.CON_DESCONTO.HeaderText = "% Desconto";
            this.CON_DESCONTO.MaxInputLength = 10;
            this.CON_DESCONTO.Name = "CON_DESCONTO";
            this.CON_DESCONTO.ReadOnly = true;
            this.CON_DESCONTO.Width = 125;
            // 
            // CON_WEB
            // 
            this.CON_WEB.DataPropertyName = "CON_WEB";
            this.CON_WEB.HeaderText = "Sis. de Convênio";
            this.CON_WEB.MaxInputLength = 35;
            this.CON_WEB.Name = "CON_WEB";
            this.CON_WEB.ReadOnly = true;
            this.CON_WEB.Width = 200;
            // 
            // CON_REGRAS
            // 
            this.CON_REGRAS.DataPropertyName = "CON_REGRAS";
            this.CON_REGRAS.HeaderText = "Regras";
            this.CON_REGRAS.MaxInputLength = 1;
            this.CON_REGRAS.Name = "CON_REGRAS";
            this.CON_REGRAS.ReadOnly = true;
            this.CON_REGRAS.Width = 75;
            // 
            // CON_DESABILITADO
            // 
            this.CON_DESABILITADO.DataPropertyName = "CON_DESABILITADO";
            this.CON_DESABILITADO.HeaderText = "Liberado";
            this.CON_DESABILITADO.MaxInputLength = 1;
            this.CON_DESABILITADO.Name = "CON_DESABILITADO";
            this.CON_DESABILITADO.ReadOnly = true;
            this.CON_DESABILITADO.Width = 75;
            // 
            // CON_DT_ALTERACAO
            // 
            this.CON_DT_ALTERACAO.DataPropertyName = "CON_DT_ALTERACAO";
            dataGridViewCellStyle7.Format = "G";
            this.CON_DT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle7;
            this.CON_DT_ALTERACAO.HeaderText = "Data Alteração";
            this.CON_DT_ALTERACAO.MaxInputLength = 30;
            this.CON_DT_ALTERACAO.Name = "CON_DT_ALTERACAO";
            this.CON_DT_ALTERACAO.ReadOnly = true;
            this.CON_DT_ALTERACAO.Width = 150;
            // 
            // CON_OP_ALTERACAO
            // 
            this.CON_OP_ALTERACAO.DataPropertyName = "CON_OP_ALTERACAO";
            this.CON_OP_ALTERACAO.HeaderText = "Usuário Alteração";
            this.CON_OP_ALTERACAO.MaxInputLength = 25;
            this.CON_OP_ALTERACAO.Name = "CON_OP_ALTERACAO";
            this.CON_OP_ALTERACAO.ReadOnly = true;
            this.CON_OP_ALTERACAO.Width = 150;
            // 
            // CON_DT_CADASTRO
            // 
            this.CON_DT_CADASTRO.DataPropertyName = "CON_DT_CADASTRO";
            dataGridViewCellStyle8.Format = "G";
            this.CON_DT_CADASTRO.DefaultCellStyle = dataGridViewCellStyle8;
            this.CON_DT_CADASTRO.HeaderText = "Data do Cadastro";
            this.CON_DT_CADASTRO.MaxInputLength = 30;
            this.CON_DT_CADASTRO.Name = "CON_DT_CADASTRO";
            this.CON_DT_CADASTRO.ReadOnly = true;
            this.CON_DT_CADASTRO.Width = 150;
            // 
            // CON_OP_CADASTRO
            // 
            this.CON_OP_CADASTRO.DataPropertyName = "CON_OP_CADASTRO";
            this.CON_OP_CADASTRO.HeaderText = "Usuário Cadastro";
            this.CON_OP_CADASTRO.MaxInputLength = 25;
            this.CON_OP_CADASTRO.Name = "CON_OP_CADASTRO";
            this.CON_OP_CADASTRO.ReadOnly = true;
            this.CON_OP_CADASTRO.Width = 150;
            // 
            // CON_FORMA_ID
            // 
            this.CON_FORMA_ID.DataPropertyName = "CON_FORMA_ID";
            this.CON_FORMA_ID.HeaderText = "CON_FORMA_ID";
            this.CON_FORMA_ID.Name = "CON_FORMA_ID";
            this.CON_FORMA_ID.ReadOnly = true;
            this.CON_FORMA_ID.Visible = false;
            // 
            // CON_PARCELA
            // 
            this.CON_PARCELA.DataPropertyName = "CON_PARCELA";
            this.CON_PARCELA.HeaderText = "CON_PARCELA";
            this.CON_PARCELA.Name = "CON_PARCELA";
            this.CON_PARCELA.ReadOnly = true;
            this.CON_PARCELA.Visible = false;
            // 
            // CON_OBRIGA_REGRA_DESCONTO
            // 
            this.CON_OBRIGA_REGRA_DESCONTO.DataPropertyName = "CON_OBRIGA_REGRA_DESCONTO";
            this.CON_OBRIGA_REGRA_DESCONTO.HeaderText = "CON_OBRIGA_REGRA_DESCONTO";
            this.CON_OBRIGA_REGRA_DESCONTO.Name = "CON_OBRIGA_REGRA_DESCONTO";
            this.CON_OBRIGA_REGRA_DESCONTO.ReadOnly = true;
            this.CON_OBRIGA_REGRA_DESCONTO.Visible = false;
            // 
            // frmCadConveniadas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadConveniadas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadConveniadas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadConveniadas_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcConveniadas.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConveniadas)).EndInit();
            this.cmsConveniadas.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcConveniadas;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.TextBox txtBCidade;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBNome;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridView dgConveniadas;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbBConvenio;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox cmbUF;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtConCod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.TextBox txtConID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.ComboBox cmbConvenio;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVencimento;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFechamento;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ContextMenuStrip cmsConveniadas;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.ComboBox cmbForma;
        private System.Windows.Forms.Label lblForma;
        private System.Windows.Forms.CheckBox chkParcelado;
        private System.Windows.Forms.CheckBox chkObrigaDescontoDaRegra;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_ENDERECO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_BAIRRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CEP;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_FECHAMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_VENCIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_DESCONTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_WEB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_REGRAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_DESABILITADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_DT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_OP_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_DT_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_OP_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_FORMA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_PARCELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_OBRIGA_REGRA_DESCONTO;
    }
}