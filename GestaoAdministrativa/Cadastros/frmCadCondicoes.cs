﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Geral;

namespace GestaoAdministrativa.Cadastros
{
    /// <summary>
    /// Cadastro de Formas de Pagamento
    /// </summary>
    public partial class frmCadCondicoes : Form, Botoes
    {
        private DataTable dtCondicoes = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbCondicoes = new ToolStripButton("Formas de Pagto");
        private ToolStripSeparator tssCondicoes = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        /// <summary>
        /// Inicialização do Formulário
        /// </summary>
        /// <param name="menu">Formulário Principal</param>
        public frmCadCondicoes(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadCondicoes_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Primeiro()
        {
            if (dtCondicoes.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgCondicoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgCondicoes.CurrentCell = dgCondicoes.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcCondicoes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadCondicoes");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, contRegistros));
            if (tcCondicoes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcCondicoes.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtCondID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtCondicoes.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgCondicoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgCondicoes.CurrentCell = dgCondicoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgCondicoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgCondicoes.CurrentCell = dgCondicoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgCondicoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgCondicoes.CurrentCell = dgCondicoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Cria botão do formulário na barra inferior do menu principal
        /// </summary>
        public void Botao()
        {
            this.tsbCondicoes.AutoSize = false;
            this.tsbCondicoes.Image = Properties.Resources.cadastro;
            this.tsbCondicoes.Size = new System.Drawing.Size(135, 20);
            this.tsbCondicoes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            Principal.mdiPrincipal.stsBotoes.Items.Add(tsbCondicoes);
            Principal.mdiPrincipal.stsBotoes.Items.Add(tssCondicoes);
            tsbCondicoes.Click += delegate
            {
                var cadConveniadas = Application.OpenForms.OfType<frmCadCondicoes>().FirstOrDefault();
                BotoesHabilitados();
                cadConveniadas.Focus();
            };
        }

        /// <summary>
        /// Carrega os dados do registro selecionado, quando muda da aba em grade para a aba em ficha
        /// </summary>
        /// <param name="linha">ID da linha do Grid dos Registros a ser carregado</param>
        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtCondID.Text = Funcoes.ChecaCampoVazio(Convert.ToInt32(dtCondicoes.Rows[linha]["FORMA_ID"]).ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["FORMA_DESCRICAO"].ToString());
                txtQtdeVias.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["QTDE_VIAS"].ToString());
                chkVencimento.Checked = dtCondicoes.Rows[linha]["VENCTO_DIA_FIXO"].ToString() == "S" ? true : false;
                txtDiaVencimento.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["DIA_VENCTO_FIXO"].ToString());
                chkBeneficio.Checked = dtCondicoes.Rows[linha]["CONV_BENEFICIO"].ToString() == "S" ? true : false;
                chkOperacao.Checked = dtCondicoes.Rows[linha]["OPERACAO_CAIXA"].ToString() == "S" ? true : false;
                txtParcelas.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["QTDE_PARCELAS"].ToString());

                if (dtCondicoes.Rows[linha]["FORMA_LIBERADO"].ToString() == "S")
                {
                    chkLiberado.Checked = true;
                }
                else
                    chkLiberado.Checked = false;

                txtData.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtCondicoes.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Abre formulário se o mesmo já estiver na barra de botões
        /// </summary>
        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Fecha o formulário e remove o botão do mesmo
        /// </summary>
        public void Sair()
        {
            try
            {
                dtCondicoes.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbCondicoes);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssCondicoes);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Limpa os campos da Aba em Grade
        /// </summary>
        public void LimparGrade()
        {
            dtCondicoes.Clear();
            dgCondicoes.DataSource = dtCondicoes;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        /// <summary>
        /// Limpa os campos da Aba em Ficha
        /// </summary>
        public void LimparFicha()
        {
            txtCondID.Text = "";
            txtDescr.Text = "";
            txtParcelas.Text = "1";
            chkVencimento.Checked = false;
            txtDiaVencimento.Text = "0";
            txtQtdeVias.Text = "1";
            chkOperacao.Checked = false;
            chkBeneficio.Checked = false;
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        /// <summary>
        /// Botão Limpar do Menu Principal, limpa os dados do Formulário
        /// </summary>
        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcCondicoes.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        /// <summary>
        /// Botão Impressão de Relatório do Menu Principal
        /// </summary>
        public void ImprimirRelatorio()
        {
        }

        /// <summary>
        /// Botão Excluir do Menu Principal, remove o registro selecionado
        /// </summary>
        /// <returns></returns>
        public bool Excluir()
        {
            try
            {
                if ((txtCondID.Text != "") && (txtDescr.Text != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da condição de pagamento?", "Exclusão tabela Formas de Pagto", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirCondPagto(txtCondID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var formaPagto = new FormasPagamento();
                            if (!formaPagto.ExcluirDados(Principal.estAtual, txtCondID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("FORMA_ID", txtCondID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "FORMAS_PAGAMENTO", txtDescr.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                                dtCondicoes.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgCondicoes.DataSource = dtCondicoes;
                                emGrade = false;
                                tcCondicoes.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                {
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Botão Incluir do Menu Principal, insere um registro
        /// </summary>
        /// <returns></returns>
        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    txtCondID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("FORMAS_PAGAMENTO", "FORMA_ID", 0, "", Principal.empAtual));
                    chkLiberado.Checked = true;

                    //VERIFICA SE CODIGO DA CONDICAO JÁ ESTA CADASTRADO//
                    if (Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", txtDescr.Text.Trim().ToUpper(), true, true).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Forma de Pagamento já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    var formaPagto = new FormasPagamento(
                        Principal.empAtual,
                        Convert.ToInt32(txtCondID.Text),
                        txtDescr.Text,
                        chkVencimento.Checked == true ? "S" : "N",
                        Convert.ToInt32(txtParcelas.Text),
                        Convert.ToInt32(txtDiaVencimento.Text),
                        Convert.ToInt32(txtQtdeVias.Text),
                        chkOperacao.Checked == true ? "S" : "N",
                        chkBeneficio.Checked == true ? "S" : "N",
                        chkLiberado.Checked == true ? "S" : "N",
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now
                    );

                    if (formaPagto.InsereRegistros(formaPagto))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("FORMA_ID", txtCondID.Text, Principal.usuario, "FORMAS_PAGAMENTO", txtDescr.Text, Principal.estAtual, Principal.empAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtCondicoes.Clear();
                        dgCondicoes.DataSource = dtCondicoes;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;

                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        txtDescr.Focus();
                        return false;
                    }


                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Botão Atualizar do Menu Principal, atualiza um registro
        /// </summary>
        /// <returns></returns>
        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var formaPagto = new FormasPagamento(
                        Principal.empAtual,
                        Convert.ToInt32(txtCondID.Text),
                        txtDescr.Text,
                        chkVencimento.Checked == true ? "S" : "N",
                        Convert.ToInt32(txtParcelas.Text),
                        Convert.ToInt32(txtDiaVencimento.Text),
                        Convert.ToInt32(txtQtdeVias.Text),
                        chkOperacao.Checked == true ? "S" : "N",
                        chkBeneficio.Checked == true ? "S" : "N",
                        chkLiberado.Checked == true ? "S" : "N",
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now
                    );

                    Principal.dtBusca = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_ID", txtCondID.Text);

                    if (formaPagto.AtualizaDados(formaPagto, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Formas de Pagamento");
                        dtCondicoes.Clear();
                        dgCondicoes.DataSource = dtCondicoes;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (txtDescr.Text.Trim().Length.Equals(0))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (chkVencimento.Checked.Equals(true) && txtDiaVencimento.Text == "0")
            {
                Principal.mensagem = "Necessário informar o Dia do Vencimento, maior que 0.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            return true;
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }

        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var formPag = new FormasPagamento();
                dtCondicoes = formPag.BuscaDados(Principal.empAtual, txtCondID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtCondID.Text),
                    txtDescr.Text, cmbLiberado.Text, out strOrdem);
                if (dtCondicoes.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtCondicoes.Rows.Count;
                    dgCondicoes.DataSource = dtCondicoes;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, 0));
                    dgCondicoes.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Condições", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCondicoes.DataSource = dtCondicoes;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = -1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcCondicoes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcCondicoes.SelectedTab == tpGrade)
            {
                dgCondicoes.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcCondicoes.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadCondicoes");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    CarregarDados(contRegistros);
                }
                else
                {
                    LimparFicha();
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, contRegistros));
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                    txtDescr.Focus();
                }
            }
        }

        /// <summary>
        /// Exporta os dados do Grid para Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgCondicoes.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgCondicoes);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgCondicoes.RowCount;
                    for (i = 0; i <= dgCondicoes.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgCondicoes[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgCondicoes.ColumnCount; k++)
                    {
                        if (dgCondicoes.Columns[k].Visible == true)
                        {
                            switch (dgCondicoes.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgCondicoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgCondicoes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgCondicoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgCondicoes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgCondicoes.ColumnCount : indice) + Convert.ToString(dgCondicoes.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgCondicoes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null && e.Value.Equals("N") && dgCondicoes.Columns[e.ColumnIndex].Name == "CPR_LIBERADO")
            {
                dgCondicoes.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgCondicoes.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void dgCondicoes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, contRegistros));
            emGrade = true;
        }

        private void dgCondicoes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcCondicoes.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgCondicoes_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgCondicoes.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtCondicoes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgCondicoes.Columns[e.ColumnIndex].Name + " DESC");
                        decrescente = true;
                    }
                    else
                    {
                        dtCondicoes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgCondicoes.Columns[e.ColumnIndex].Name + " ASC");
                        decrescente = false;
                    }

                    for (int i = 0; i < dtCondicoes.Rows.Count; i++)
                    {
                        switch (dtCondicoes.Rows[i]["CPR_APARTIR"].ToString())
                        {
                            case "P":
                                dtCondicoes.Rows[i]["CPR_APARTIR"] = "DO PEDIDO";
                                break;
                            case "D":
                                dtCondicoes.Rows[i]["CPR_APARTIR"] = "DA ENTREGA";
                                break;
                        }

                        switch (dtCondicoes.Rows[i]["CPR_CARENCIA"].ToString())
                        {
                            case "T":
                                dtCondicoes.Rows[i]["CPR_CARENCIA"] = "DA DATA";
                                break;
                            case "S":
                                dtCondicoes.Rows[i]["CPR_CARENCIA"] = "DA SEMANA";
                                break;
                            case "D":
                                dtCondicoes.Rows[i]["CPR_CARENCIA"] = "DA DEZENA";
                                break;
                            case "Q":
                                dtCondicoes.Rows[i]["CPR_CARENCIA"] = "DA QUINZENA";
                                break;
                            case "M":
                                dtCondicoes.Rows[i]["CPR_CARENCIA"] = "DO MÊS";
                                break;
                        }

                        dtCondicoes.AcceptChanges();
                    }
                    dgCondicoes.DataSource = dtCondicoes;
                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtCondicoes, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgCondicoes_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtCondicoes.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtCondicoes.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtCondicoes.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        /// <summary>
        /// Atalho F1 para ir para aba em Grade
        /// </summary>
        public void AtalhoGrade() {
            tcCondicoes.SelectedTab = tpGrade;
        }

        /// <summary>
        /// Atalho F2 para ir para aba em Ficha
        /// </summary>
        public void AtalhoFicha() {
            tcCondicoes.SelectedTab = tpFicha;
        }

        private void dgCondicoes_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgCondicoes.Rows[e.RowIndex].Cells[9].Value.ToString() == "N")
            {
                dgCondicoes.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgCondicoes.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtCondID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgCondicoes.ColumnCount; i++)
                {
                    if (dgCondicoes.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgCondicoes.ColumnCount];
                string[] coluna = new string[dgCondicoes.ColumnCount];

                for (int i = 0; i < dgCondicoes.ColumnCount; i++)
                {
                    grid[i] = dgCondicoes.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgCondicoes.ColumnCount; i++)
                {
                    if (dgCondicoes.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgCondicoes.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgCondicoes.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgCondicoes.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgCondicoes.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgCondicoes.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgCondicoes.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgCondicoes.ColumnCount; i++)
                        {
                            dgCondicoes.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Botão Ajuda do Menu Principal
        /// </summary>
        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtParcelas.Focus();
        }

        private void txtParcelas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkVencimento.Focus();
        }

        private void txtParcelas_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtParcelas.Text))
            {
                if (txtParcelas.Text.Equals("0"))
                {
                    MessageBox.Show("Parcela não pode ser igual a 0.", "Formas de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtParcelas.Text = "1";
                }
            }
            else
                txtParcelas.Text = "1";
        }

        private void chkVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDiaVencimento.Focus();
        }

        private void txtDiaVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtdeVias.Focus();
        }

        private void txtDiaVencimento_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtDiaVencimento.Text))
            {
                txtDiaVencimento.Text = "0";
            }
        }

        private void txtQtdeVias_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkOperacao.Focus();
        }

        private void txtQtdeVias_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtQtdeVias.Text))
            {
                txtQtdeVias.Text = "1";
            }
        }

        private void chkOperacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkBeneficio.Focus();
        }

        private void chkBeneficio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }
    }
}
