﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadComanda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadComanda));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcComanda = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dgComanda = new System.Windows.Forms.DataGridView();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COM_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.csmComanda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblEstab = new System.Windows.Forms.Label();
            this.txtComanda = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcComanda.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgComanda)).BeginInit();
            this.csmComanda.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcComanda);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Comandas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcComanda
            // 
            this.tcComanda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcComanda.Controls.Add(this.tpGrade);
            this.tcComanda.Controls.Add(this.tpFicha);
            this.tcComanda.Location = new System.Drawing.Point(6, 33);
            this.tcComanda.Name = "tcComanda";
            this.tcComanda.SelectedIndex = 0;
            this.tcComanda.Size = new System.Drawing.Size(965, 499);
            this.tcComanda.TabIndex = 40;
            this.tcComanda.SelectedIndexChanged += new System.EventHandler(this.tcComanda_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.dgComanda);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(143, 419);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 86;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBID.Location = new System.Drawing.Point(6, 435);
            this.txtBID.MaxLength = 9;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(122, 22);
            this.txtBID.TabIndex = 63;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 416);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "N° Comanda";
            // 
            // dgComanda
            // 
            this.dgComanda.AllowUserToAddRows = false;
            this.dgComanda.AllowUserToDeleteRows = false;
            this.dgComanda.AllowUserToOrderColumns = true;
            this.dgComanda.AllowUserToResizeColumns = false;
            this.dgComanda.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgComanda.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgComanda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgComanda.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgComanda.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgComanda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgComanda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EST_CODIGO,
            this.EMP_CODIGO,
            this.COM_NUMERO,
            this.DTCADASTRO,
            this.OPCADASTRO});
            this.dgComanda.ContextMenuStrip = this.csmComanda;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgComanda.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgComanda.GridColor = System.Drawing.Color.LightGray;
            this.dgComanda.Location = new System.Drawing.Point(6, 6);
            this.dgComanda.MultiSelect = false;
            this.dgComanda.Name = "dgComanda";
            this.dgComanda.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgComanda.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgComanda.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgComanda.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgComanda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgComanda.Size = new System.Drawing.Size(945, 394);
            this.dgComanda.TabIndex = 1;
            this.dgComanda.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgComanda_CellMouseClick);
            this.dgComanda.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgComanda_CellMouseDoubleClick);
            this.dgComanda.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgComanda_ColumnHeaderMouseClick);
            this.dgComanda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgComanda_KeyDown);
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "Estab. ID";
            this.EST_CODIGO.MaxInputLength = 3;
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EST_CODIGO.Width = 70;
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            this.EMP_CODIGO.HeaderText = "Emp. ID";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            // 
            // COM_NUMERO
            // 
            this.COM_NUMERO.DataPropertyName = "COM_NUMERO";
            this.COM_NUMERO.HeaderText = "Número da Comanda";
            this.COM_NUMERO.MaxInputLength = 18;
            this.COM_NUMERO.Name = "COM_NUMERO";
            this.COM_NUMERO.ReadOnly = true;
            this.COM_NUMERO.Width = 300;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle3.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle3;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 20;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // csmComanda
            // 
            this.csmComanda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.csmComanda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.csmComanda.Name = "csmComanda";
            this.csmComanda.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 92);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(574, 23);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 193;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 21);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.txtComanda);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 116);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(162, 59);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 110;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            this.lblEstab.Tag = "";
            // 
            // txtComanda
            // 
            this.txtComanda.BackColor = System.Drawing.Color.White;
            this.txtComanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComanda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComanda.Location = new System.Drawing.Point(16, 77);
            this.txtComanda.MaxLength = 9;
            this.txtComanda.Name = "txtComanda";
            this.txtComanda.Size = new System.Drawing.Size(237, 22);
            this.txtComanda.TabIndex = 81;
            this.txtComanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComanda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComanda_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Número da Comanda";
            // 
            // frmCadComanda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadComanda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadComanda";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadComanda_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcComanda.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgComanda)).EndInit();
            this.csmComanda.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcComanda;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.DataGridView dgComanda;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.TextBox txtComanda;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ContextMenuStrip csmComanda;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COM_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
    }
}