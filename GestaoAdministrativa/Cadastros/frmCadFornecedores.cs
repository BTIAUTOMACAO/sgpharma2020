﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadFornecedores : Form, Botoes
    {
        private DataTable dtFornecedores = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbFornecedores = new ToolStripButton("Fornecedores");
        private ToolStripSeparator tssFornecedores = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private string mascara;

        public frmCadFornecedores(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadFornecedores_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtFornecedores.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgFornecedores.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgFornecedores.CurrentCell = dgFornecedores.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcFornecedores.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtFornecedores.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFornecedores.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFornecedores.CurrentCell = dgFornecedores.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFornecedores.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFornecedores.CurrentCell = dgFornecedores.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgFornecedores.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgFornecedores.CurrentCell = dgFornecedores.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadFornecedores");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, contRegistros));
            if (tcFornecedores.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcFornecedores.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtFornID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbFornecedores.AutoSize = false;
                this.tsbFornecedores.Image = Properties.Resources.cadastro;
                this.tsbFornecedores.Size = new System.Drawing.Size(125, 20);
                this.tsbFornecedores.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbFornecedores);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssFornecedores);
                tsbFornecedores.Click += delegate
                {
                    var cadFornecedores = Application.OpenForms.OfType<frmCadFornecedores>().FirstOrDefault();
                    BotoesHabilitados();
                    cadFornecedores.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtFornID.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_ID"].ToString());
                txtCnpj.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_DOCTO"].ToString());
                txtInsEst.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_INSEST"].ToString());
                if (Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_ENQUADRA"].ToString()) == "0")
                {
                    cmbEnquadra.SelectedIndex = 1;
                }
                else
                    cmbEnquadra.SelectedIndex = 0;

                txtRazao.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_NOME"].ToString());
                txtFantasia.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_APELIDO"].ToString());
                txtEndereco.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_ENDER"].ToString());
                txtBairro.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_BAIRRO"].ToString());
                txtCidade.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_CIDADE"].ToString());
                txtCep.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_CEP"].ToString());
                cmbUF.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_UF"].ToString());
                txtPais.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_PAIS"].ToString());
                txtTelefone.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_FONE"].ToString()).Replace(" ","");
                txtCelular.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_CELULAR"].ToString()).Replace(" ", "");
                txtEmail.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_EMAIL"].ToString());
                txtContato.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_CNOME"].ToString());
                txtNumero.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["CF_END_NUM"].ToString());
                if (dtFornecedores.Rows[linha]["CF_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtFornecedores.Rows[linha]["OPALTERACAO"].ToString());
                txtCnpj.Focus();

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, linha));
                txtCnpj.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtFornecedores.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbFornecedores);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssFornecedores);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtFornecedores.Clear();
            dgFornecedores.DataSource = dtFornecedores;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            cmbLiberado.SelectedIndex = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtFornID.Text = "";
            txtCnpj.Text = "";
            txtInsEst.Text = "";
            cmbEnquadra.SelectedIndex = 1;
            txtRazao.Text = "";
            txtFantasia.Text = "";
            txtEndereco.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            txtPais.Text = "";
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtEmail.Text = "";
            txtContato.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtCnpj.Focus();

        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcFornecedores.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }


        public void ImprimirRelatorio()
        {
        }

        public bool Excluir()
        {
            try
            {
                if ((txtFornID.Text.Trim() != "") && (Funcoes.RemoveCaracter(txtCnpj.Text) != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do fornecedor?", "Exclusão tabela Fornecedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirFornecedores(txtFornID.Text, txtCnpj.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var eFornecedor = new Fornecedor();
                            if (!eFornecedor.ExcluirDados(txtFornID.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("CF_ID", txtFornID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CLIFOR", txtCnpj.Text, Principal.motivo, Principal.estAtual);
                                dtFornecedores.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgFornecedores.DataSource = dtFornecedores;
                                emGrade = false;
                                tcFornecedores.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    if(String.IsNullOrEmpty(txtFornID.Text))
                    {
                        Principal.mensagem = "ID em branco Fornecedor não Cadastrado.";
                        Funcoes.Avisa();
                        txtFornID.Focus();
                        return false;
                    }
                    Fornecedor forn = new Fornecedor
                    {
                        FornCodigo = int.Parse(txtFornID.Text),
                        FornCgc = txtCnpj.Text,
                        FornInsest = txtInsEst.Text,
                        FornEnquadra = cmbEnquadra.SelectedIndex == 0 ? 1 : 0,
                        FornRazao = txtRazao.Text.Trim(),
                        FornFantasia = txtFantasia.Text.Trim(),
                        FornEndereco = txtEndereco.Text.Trim(),
                        FornBairro = txtBairro.Text.Trim() == "" ? "" : txtBairro.Text.Trim(),
                        FornCidade = txtCidade.Text.Trim(),
                        FornCep = Funcoes.RemoveCaracter(txtCep.Text),
                        FornUf = cmbUF.Text,
                        FornPais = txtPais.Text.Trim() == "" ? "" : txtPais.Text.Trim(),
                        FornFone = Funcoes.RemoveCaracter(txtTelefone.Text) == "" ? "" : Funcoes.RemoveCaracter(txtTelefone.Text),
                        FornCelular = Funcoes.RemoveCaracter(txtCelular.Text) == "" ? "" : Funcoes.RemoveCaracter(txtCelular.Text),
                        FornEmail = txtEmail.Text.Trim() == "" ? "" : txtEmail.Text,
                        FornContato = txtContato.Text.Trim() == "" ? "" : txtContato.Text.Trim(),
                        FornLiberado = chkLiberado.Checked == true ? "N" : "S",
                        FornNumero = txtNumero.Text,
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("CLIFOR", "CF_ID", txtFornID.Text);

                    if (forn.AtualizaDados(forn, Principal.dtBusca))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Fornecedores");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtFornecedores.Clear();
                        dgFornecedores.DataSource = dtFornecedores;
                        emGrade = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro ao tentar afetuar a atualização", "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;

                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpj.Text)))
            {
                Principal.mensagem = "CNPJ não pode ser zero.";
                Funcoes.Avisa();
                txtCnpj.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtRazao.Text.Trim()))
            {
                Principal.mensagem = "Razão Social não pode ser em branco.";
                Funcoes.Avisa();
                txtRazao.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFantasia.Text.Trim()))
            {
                Principal.mensagem = "Nome Fantasia não pode ser em branco.";
                Funcoes.Avisa();
                txtFantasia.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNumero.Text.Trim()))
            {
                Principal.mensagem = "Número não pode ser em branco.";
                Funcoes.Avisa();
                txtNumero.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCep.Text.Replace("-", "").Trim()))
            {
                Principal.mensagem = "Cep não pode ser em branco.";
                Funcoes.Avisa();
                txtCep.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }

            return true;
        }

        private void txtCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtInsEst.Focus();
        }

        private void txtInsEst_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbEnquadra.Focus();
        }

        private void cmbEnquadra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtRazao.Focus();
        }

        private void txtRazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFantasia.Focus();
        }

        private void txtFantasia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if(String.IsNullOrEmpty(txtEndereco.Text))
                {
                    txtCep.Focus();
                }
                else
                    txtEndereco.Focus();
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtEndereco.Focus();

                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {

                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.Length > 20 ? Funcoes.RemoverAcentuacao(end.bairro.Substring(0, 19).ToUpper()) : Funcoes.RemoverAcentuacao(end.bairro.ToUpper());
                        txtCidade.Text = Funcoes.RemoverAcentuacao(end.localidade.ToUpper());
                        cmbUF.Text = end.uf;
                        txtNumero.Focus();
                    }
                }
                Cursor = Cursors.Default;
            }
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPais.Focus();
        }

        private void txtPais_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCelular.Focus();
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtEmail.Focus();
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtContato.Focus();
        }

        private void txtContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }


        private void tcFornecedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcFornecedores.SelectedTab == tpGrade)
            {
                dgFornecedores.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcFornecedores.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadFornecedores");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, contRegistros));
                        cmbEnquadra.SelectedIndex = 1;
                        chkLiberado.Checked = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        emGrade = false;
                        txtCnpj.Focus();
                    }
                }
        }


        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;
                    Principal.dtPesq = Util.RegistrosPorEstabelecimento("CLIFOR", "CF_DOCTO", txtCnpj.Text);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "CNPJ já cadastrada.";
                        Funcoes.Avisa();
                        txtCnpj.Focus();
                        return false;
                    }
                    else
                    {
                        txtFornID.Text = Funcoes.IdentificaVerificaID("CLIFOR", "CF_ID").ToString();
                        chkLiberado.Checked = true;

                        Fornecedor forn = new Fornecedor
                        {
                            FornCodigo = Convert.ToInt32(txtFornID.Text),
                            FornCgc = txtCnpj.Text,
                            FornInsest = txtInsEst.Text,
                            FornEnquadra = cmbEnquadra.SelectedIndex == 0 ? 1 : 0,
                            FornRazao = txtRazao.Text.Trim(),
                            FornFantasia = txtFantasia.Text.Trim(),
                            FornEndereco = txtEndereco.Text.Trim(),
                            FornBairro = txtBairro.Text.Trim() == "" ? "" : txtBairro.Text.Length > 20 ? Funcoes.RemoverAcentuacao(txtBairro.Text.Substring(0,19)) : Funcoes.RemoverAcentuacao(txtBairro.Text),
                            FornCidade = txtCidade.Text.Trim(),
                            FornCep = Funcoes.RemoveCaracter(txtCep.Text),
                            FornUf = cmbUF.Text,
                            FornPais = txtPais.Text.Trim() == "" ? "" : txtPais.Text.Trim(),
                            FornFone = Funcoes.RemoveCaracter(txtTelefone.Text) == "" ? "" : Funcoes.RemoveCaracter(txtTelefone.Text),
                            FornCelular = Funcoes.RemoveCaracter(txtCelular.Text) == "" ? "" : Funcoes.RemoveCaracter(txtCelular.Text),
                            FornEmail = txtEmail.Text.Trim() == "" ? "" : txtEmail.Text,
                            FornContato = txtContato.Text.Trim() == "" ? "" : txtContato.Text.Trim(),
                            FornLiberado = chkLiberado.Checked == true ? "N" : "S",
                            FornNumero = txtNumero.Text,
                        };
                        if (forn.InsereDados(forn).Equals(true))
                        {
                            dtFornecedores.Clear();
                            dgFornecedores.DataSource = dtFornecedores;
                            emGrade = false;
                            Limpar();
                            return true;
                        }
                        else {
                            MessageBox.Show("Erro: ao tentar cadastrar", "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtCnpj.Focus();
                            return false;
                        
                        }

                    }
                }
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBRazao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBRazao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBRazao.Text.Trim()))
                {
                    txtBFantasia.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBFantasia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBFantasia.Text.Trim()))
                {
                    txtBCnpj.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgFornecedores.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgFornecedores);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }
                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgFornecedores.RowCount;
                    for (i = 0; i <= dgFornecedores.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgFornecedores[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgFornecedores.ColumnCount; k++)
                    {
                        if (dgFornecedores.Columns[k].Visible == true)
                        {
                            switch (dgFornecedores.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgFornecedores.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgFornecedores.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgFornecedores.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgFornecedores.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Inscrição Estadual":
                                    numCel = Principal.GetColunaExcel(dgFornecedores.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgFornecedores.RowCount + 1));
                                    celulas.NumberFormat = "000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgFornecedores.ColumnCount : indice) + Convert.ToString(dgFornecedores.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void dgFornecedores_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, contRegistros));
            emGrade = true;
        }

        private void dgFornecedores_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcFornecedores.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgFornecedores_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtFornecedores.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtFornecedores.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtFornecedores.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgFornecedores_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgFornecedores.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtFornecedores = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgFornecedores.Columns[e.ColumnIndex].Name + " DESC");
                        dgFornecedores.DataSource = dtFornecedores;
                        decrescente = true;
                    }
                    else
                    {
                        dtFornecedores = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgFornecedores.Columns[e.ColumnIndex].Name + " ASC");
                        dgFornecedores.DataSource = dtFornecedores;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtCnpj_Validated(object sender, EventArgs e)
        {
            mascara = txtCnpj.Text;
            mascara = mascara.Replace("/", "");
            mascara = mascara.Replace(".", "");
            mascara = mascara.Replace("-", "");
            if (!mascara.Trim().Equals(""))
            {
                if (Funcoes.ValidaCnpj(txtCnpj.Text).Equals(false))
                {
                    MessageBox.Show("CNPJ incorreto! Digite novamente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpj.Focus();
                }
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void dgFornecedores_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgFornecedores.Rows[e.RowIndex].Cells[16].Value.ToString() == "N")
            {
                dgFornecedores.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgFornecedores.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Fornecedor forn = new Fornecedor
                {
                    FornCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text),
                    FornRazao = txtBRazao.Text.Trim(),
                    FornFantasia = txtBFantasia.Text.Trim(),
                    FornCgc = txtBCnpj.Text,
                    FornCidade = txtBCidade.Text.Trim(),
                    FornLiberado = cmbLiberado.Text
                };

                dtFornecedores = forn.BuscaDados(forn, out strOrdem);
                if (dtFornecedores.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtFornecedores.Rows.Count;
                    dgFornecedores.DataSource = dtFornecedores;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFornecedores, 0));
                    dgFornecedores.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgFornecedores.DataSource = dtFornecedores;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(((txtBCnpj.Text.Replace(".", "").Trim()).Replace("-", "")).Replace("/", "").Trim()))
                {
                    txtBCidade.Focus();
                }
                else
                    btnBuscar.PerformClick();

            }
        }

        private void txtBCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCidade.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }

        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtFornID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgFornecedores.ColumnCount; i++)
                {
                    if (dgFornecedores.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgFornecedores.ColumnCount];
                string[] coluna = new string[dgFornecedores.ColumnCount];

                for (int i = 0; i < dgFornecedores.ColumnCount; i++)
                {
                    grid[i] = dgFornecedores.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgFornecedores.ColumnCount; i++)
                {
                    if (dgFornecedores.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgFornecedores.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgFornecedores.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgFornecedores.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgFornecedores.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgFornecedores.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgFornecedores.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgFornecedores.ColumnCount; i++)
                        {
                            dgFornecedores.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Fornecedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcFornecedores.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcFornecedores.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox17, "Campo Obrigatório");
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    txtBairro.Focus();
                }
                else
                    txtPais.Focus();
            }
        }
        
    }
}
