﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadEmpresa));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcEmp = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtBNome = new System.Windows.Forms.TextBox();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgEmp = new System.Windows.Forms.DataGridView();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CGC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_INSEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_RAZAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_FANTASIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_ENDERECO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_FONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_DESABILITADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsEmpresa = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.cmbUF = new System.Windows.Forms.ComboBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtFantasia = new System.Windows.Forms.TextBox();
            this.txtRazao = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtInsEst = new System.Windows.Forms.TextBox();
            this.txtCnpj = new System.Windows.Forms.MaskedTextBox();
            this.txtEmpID = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcEmp.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmp)).BeginInit();
            this.cmsEmpresa.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcEmp);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(970, 560);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(970, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(8, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Empresas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(964, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcEmp
            // 
            this.tcEmp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcEmp.Controls.Add(this.tpGrade);
            this.tcEmp.Controls.Add(this.tpFicha);
            this.tcEmp.Location = new System.Drawing.Point(7, 33);
            this.tcEmp.Name = "tcEmp";
            this.tcEmp.SelectedIndex = 0;
            this.tcEmp.Size = new System.Drawing.Size(964, 499);
            this.tcEmp.TabIndex = 40;
            this.tcEmp.SelectedIndexChanged += new System.EventHandler(this.tcEmp_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.label21);
            this.tpGrade.Controls.Add(this.txtBNome);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label14);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.dgEmp);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(956, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(756, 417);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(110, 38);
            this.btnBuscar.TabIndex = 92;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(568, 431);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(151, 24);
            this.cmbLiberado.TabIndex = 91;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(565, 414);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 16);
            this.label21.TabIndex = 90;
            this.label21.Text = "Liberado";
            // 
            // txtBNome
            // 
            this.txtBNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNome.Location = new System.Drawing.Point(178, 433);
            this.txtBNome.Name = "txtBNome";
            this.txtBNome.Size = new System.Drawing.Size(365, 22);
            this.txtBNome.TabIndex = 64;
            this.txtBNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNome_KeyPress);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(10, 433);
            this.txtBID.MaxLength = 18;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(148, 22);
            this.txtBID.TabIndex = 63;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(175, 414);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 16);
            this.label14.TabIndex = 61;
            this.label14.Text = "Nome Fantasia";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(7, 414);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "Empresa ID";
            // 
            // dgEmp
            // 
            this.dgEmp.AllowUserToAddRows = false;
            this.dgEmp.AllowUserToDeleteRows = false;
            this.dgEmp.AllowUserToOrderColumns = true;
            this.dgEmp.AllowUserToResizeColumns = false;
            this.dgEmp.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEmp.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgEmp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEmp.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEmp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMP_CODIGO,
            this.EMP_CGC,
            this.EMP_INSEST,
            this.EMP_RAZAO,
            this.EMP_FANTASIA,
            this.EMP_ENDERECO,
            this.EMP_NUMERO,
            this.EMP_CIDADE,
            this.EMP_UF,
            this.EMP_FONE,
            this.EMP_DESABILITADO,
            this.DAT_ALTERACAO,
            this.EMP_OPALTERACAO,
            this.EMP_DTCADASTRO,
            this.EMP_OPCADASTRO});
            this.dgEmp.ContextMenuStrip = this.cmsEmpresa;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEmp.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgEmp.GridColor = System.Drawing.Color.LightGray;
            this.dgEmp.Location = new System.Drawing.Point(7, 6);
            this.dgEmp.MultiSelect = false;
            this.dgEmp.Name = "dgEmp";
            this.dgEmp.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEmp.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgEmp.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEmp.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEmp.Size = new System.Drawing.Size(941, 394);
            this.dgEmp.TabIndex = 1;
            this.dgEmp.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEmp_CellMouseClick);
            this.dgEmp.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEmp_CellMouseDoubleClick);
            this.dgEmp.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEmp_ColumnHeaderMouseDoubleClick);
            this.dgEmp.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgEmp_RowPrePaint);
            this.dgEmp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEmp_KeyDown);
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.EMP_CODIGO.DefaultCellStyle = dataGridViewCellStyle3;
            this.EMP_CODIGO.HeaderText = "Emp. ID";
            this.EMP_CODIGO.MaxInputLength = 18;
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            this.EMP_CODIGO.Width = 90;
            // 
            // EMP_CGC
            // 
            this.EMP_CGC.DataPropertyName = "EMP_CGC";
            this.EMP_CGC.HeaderText = "CNPJ";
            this.EMP_CGC.MaxInputLength = 18;
            this.EMP_CGC.Name = "EMP_CGC";
            this.EMP_CGC.ReadOnly = true;
            this.EMP_CGC.Width = 150;
            // 
            // EMP_INSEST
            // 
            this.EMP_INSEST.DataPropertyName = "EMP_INSEST";
            this.EMP_INSEST.HeaderText = "Inscrição Estadual";
            this.EMP_INSEST.MaxInputLength = 18;
            this.EMP_INSEST.Name = "EMP_INSEST";
            this.EMP_INSEST.ReadOnly = true;
            this.EMP_INSEST.Width = 150;
            // 
            // EMP_RAZAO
            // 
            this.EMP_RAZAO.DataPropertyName = "EMP_RAZAO";
            this.EMP_RAZAO.HeaderText = "Razão Social";
            this.EMP_RAZAO.MaxInputLength = 60;
            this.EMP_RAZAO.Name = "EMP_RAZAO";
            this.EMP_RAZAO.ReadOnly = true;
            this.EMP_RAZAO.Width = 300;
            // 
            // EMP_FANTASIA
            // 
            this.EMP_FANTASIA.DataPropertyName = "EMP_FANTASIA";
            this.EMP_FANTASIA.HeaderText = "Fantasia";
            this.EMP_FANTASIA.MaxInputLength = 30;
            this.EMP_FANTASIA.Name = "EMP_FANTASIA";
            this.EMP_FANTASIA.ReadOnly = true;
            this.EMP_FANTASIA.Width = 250;
            // 
            // EMP_ENDERECO
            // 
            this.EMP_ENDERECO.DataPropertyName = "EMP_ENDERECO";
            this.EMP_ENDERECO.HeaderText = "Endereço";
            this.EMP_ENDERECO.MaxInputLength = 60;
            this.EMP_ENDERECO.Name = "EMP_ENDERECO";
            this.EMP_ENDERECO.ReadOnly = true;
            this.EMP_ENDERECO.Width = 300;
            // 
            // EMP_NUMERO
            // 
            this.EMP_NUMERO.DataPropertyName = "EMP_NUMERO";
            this.EMP_NUMERO.HeaderText = "Número";
            this.EMP_NUMERO.MaxInputLength = 20;
            this.EMP_NUMERO.Name = "EMP_NUMERO";
            this.EMP_NUMERO.ReadOnly = true;
            // 
            // EMP_CIDADE
            // 
            this.EMP_CIDADE.DataPropertyName = "EMP_CIDADE";
            this.EMP_CIDADE.HeaderText = "Cidade";
            this.EMP_CIDADE.MaxInputLength = 30;
            this.EMP_CIDADE.Name = "EMP_CIDADE";
            this.EMP_CIDADE.ReadOnly = true;
            this.EMP_CIDADE.Width = 200;
            // 
            // EMP_UF
            // 
            this.EMP_UF.DataPropertyName = "EMP_UF";
            this.EMP_UF.HeaderText = "UF";
            this.EMP_UF.MaxInputLength = 2;
            this.EMP_UF.Name = "EMP_UF";
            this.EMP_UF.ReadOnly = true;
            this.EMP_UF.Width = 50;
            // 
            // EMP_FONE
            // 
            this.EMP_FONE.DataPropertyName = "EMP_FONE";
            this.EMP_FONE.HeaderText = "Telefone";
            this.EMP_FONE.MaxInputLength = 14;
            this.EMP_FONE.Name = "EMP_FONE";
            this.EMP_FONE.ReadOnly = true;
            this.EMP_FONE.Width = 150;
            // 
            // EMP_DESABILITADO
            // 
            this.EMP_DESABILITADO.DataPropertyName = "EMP_DESABILITADO";
            this.EMP_DESABILITADO.HeaderText = "Liberado";
            this.EMP_DESABILITADO.MaxInputLength = 1;
            this.EMP_DESABILITADO.Name = "EMP_DESABILITADO";
            this.EMP_DESABILITADO.ReadOnly = true;
            this.EMP_DESABILITADO.Width = 75;
            // 
            // DAT_ALTERACAO
            // 
            this.DAT_ALTERACAO.DataPropertyName = "DAT_ALTERACAO";
            dataGridViewCellStyle4.Format = "G";
            this.DAT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DAT_ALTERACAO.HeaderText = "Data Alteração";
            this.DAT_ALTERACAO.MaxInputLength = 20;
            this.DAT_ALTERACAO.Name = "DAT_ALTERACAO";
            this.DAT_ALTERACAO.ReadOnly = true;
            this.DAT_ALTERACAO.Width = 150;
            // 
            // EMP_OPALTERACAO
            // 
            this.EMP_OPALTERACAO.DataPropertyName = "EMP_OPALTERACAO";
            this.EMP_OPALTERACAO.HeaderText = "Usuário Alteração";
            this.EMP_OPALTERACAO.MaxInputLength = 25;
            this.EMP_OPALTERACAO.Name = "EMP_OPALTERACAO";
            this.EMP_OPALTERACAO.ReadOnly = true;
            this.EMP_OPALTERACAO.Width = 150;
            // 
            // EMP_DTCADASTRO
            // 
            this.EMP_DTCADASTRO.DataPropertyName = "EMP_DTCADASTRO";
            dataGridViewCellStyle5.Format = "G";
            this.EMP_DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle5;
            this.EMP_DTCADASTRO.HeaderText = "Data do Cadastro";
            this.EMP_DTCADASTRO.MaxInputLength = 20;
            this.EMP_DTCADASTRO.Name = "EMP_DTCADASTRO";
            this.EMP_DTCADASTRO.ReadOnly = true;
            this.EMP_DTCADASTRO.Width = 150;
            // 
            // EMP_OPCADASTRO
            // 
            this.EMP_OPCADASTRO.DataPropertyName = "EMP_OPCADASTRO";
            this.EMP_OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.EMP_OPCADASTRO.MaxInputLength = 25;
            this.EMP_OPCADASTRO.Name = "EMP_OPCADASTRO";
            this.EMP_OPCADASTRO.ReadOnly = true;
            this.EMP_OPCADASTRO.Width = 150;
            // 
            // cmsEmpresa
            // 
            this.cmsEmpresa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsEmpresa.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsEmpresa.Name = "cmsEmpresa";
            this.cmsEmpresa.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(956, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNumero);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.txtTelefone);
            this.groupBox2.Controls.Add(this.cmbUF);
            this.groupBox2.Controls.Add(this.txtCidade);
            this.groupBox2.Controls.Add(this.txtEndereco);
            this.groupBox2.Controls.Add(this.txtFantasia);
            this.groupBox2.Controls.Add(this.txtRazao);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtInsEst);
            this.groupBox2.Controls.Add(this.txtCnpj);
            this.groupBox2.Controls.Add(this.txtEmpID);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(7, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(941, 272);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(651, 137);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 131;
            this.pictureBox11.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(587, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 130;
            this.label2.Text = "Número";
            // 
            // txtNumero
            // 
            this.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumero.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.Location = new System.Drawing.Point(590, 156);
            this.txtNumero.MaxLength = 20;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(126, 22);
            this.txtNumero.TabIndex = 89;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(482, 187);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 129;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(80, 187);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 127;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(98, 137);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 126;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(591, 84);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 125;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(113, 83);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(265, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 123;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(101, 31);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 122;
            this.pictureBox7.TabStop = false;
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(18, 244);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 96;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // txtTelefone
            // 
            this.txtTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.Location = new System.Drawing.Point(590, 206);
            this.txtTelefone.Mask = "(99) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(180, 22);
            this.txtTelefone.TabIndex = 93;
            this.txtTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone_KeyPress);
            // 
            // cmbUF
            // 
            this.cmbUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUF.FormattingEnabled = true;
            this.cmbUF.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbUF.Location = new System.Drawing.Point(449, 204);
            this.cmbUF.Name = "cmbUF";
            this.cmbUF.Size = new System.Drawing.Size(116, 24);
            this.cmbUF.TabIndex = 92;
            this.cmbUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUF_KeyPress);
            // 
            // txtCidade
            // 
            this.txtCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.Location = new System.Drawing.Point(19, 206);
            this.txtCidade.MaxLength = 30;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(408, 22);
            this.txtCidade.TabIndex = 90;
            this.txtCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCidade_KeyPress);
            // 
            // txtEndereco
            // 
            this.txtEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEndereco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.Location = new System.Drawing.Point(19, 156);
            this.txtEndereco.MaxLength = 60;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(546, 22);
            this.txtEndereco.TabIndex = 88;
            this.txtEndereco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEndereco_KeyPress);
            // 
            // txtFantasia
            // 
            this.txtFantasia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFantasia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFantasia.Location = new System.Drawing.Point(485, 102);
            this.txtFantasia.MaxLength = 30;
            this.txtFantasia.Name = "txtFantasia";
            this.txtFantasia.Size = new System.Drawing.Size(438, 22);
            this.txtFantasia.TabIndex = 85;
            this.txtFantasia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFantasia_KeyPress);
            // 
            // txtRazao
            // 
            this.txtRazao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRazao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRazao.Location = new System.Drawing.Point(19, 102);
            this.txtRazao.MaxLength = 60;
            this.txtRazao.Name = "txtRazao";
            this.txtRazao.Size = new System.Drawing.Size(439, 22);
            this.txtRazao.TabIndex = 84;
            this.txtRazao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRazao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(15, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 16);
            this.label4.TabIndex = 83;
            this.label4.Text = "Razão Social";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(482, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 82;
            this.label3.Text = "Nome Fantasia";
            // 
            // txtInsEst
            // 
            this.txtInsEst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsEst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsEst.Location = new System.Drawing.Point(485, 49);
            this.txtInsEst.MaxLength = 20;
            this.txtInsEst.Name = "txtInsEst";
            this.txtInsEst.Size = new System.Drawing.Size(292, 22);
            this.txtInsEst.TabIndex = 81;
            this.txtInsEst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInsEst_KeyPress);
            // 
            // txtCnpj
            // 
            this.txtCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCnpj.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCnpj.Location = new System.Drawing.Point(223, 49);
            this.txtCnpj.Mask = "99,999,999/9999-99";
            this.txtCnpj.Name = "txtCnpj";
            this.txtCnpj.Size = new System.Drawing.Size(235, 22);
            this.txtCnpj.TabIndex = 80;
            this.txtCnpj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCnpj_KeyPress);
            this.txtCnpj.Validated += new System.EventHandler(this.txtCnpj_Validated);
            // 
            // txtEmpID
            // 
            this.txtEmpID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEmpID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmpID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmpID.Location = new System.Drawing.Point(19, 49);
            this.txtEmpID.MaxLength = 18;
            this.txtEmpID.Name = "txtEmpID";
            this.txtEmpID.ReadOnly = true;
            this.txtEmpID.Size = new System.Drawing.Size(180, 22);
            this.txtEmpID.TabIndex = 79;
            this.txtEmpID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(219, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 16);
            this.label16.TabIndex = 78;
            this.label16.Text = "CNPJ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(586, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 16);
            this.label9.TabIndex = 62;
            this.label9.Text = "Telefone";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(482, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Inscrição Estadual";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(446, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 16);
            this.label7.TabIndex = 60;
            this.label7.Text = "UF";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(15, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 59;
            this.label6.Text = "Cidade";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(15, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 58;
            this.label5.Text = "Endereço";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(15, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 39;
            this.label1.Text = "Empresa ID";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(7, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(941, 92);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox10);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(558, 28);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 194;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(187, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(18, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(211, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(40, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(19, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 191;
            this.pictureBox10.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(266, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(192, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(10, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(227, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(7, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(262, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // frmCadEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadEmpresa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadEmpresa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadEmpresa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcEmp.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmp)).EndInit();
            this.cmsEmpresa.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcEmp;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBNome;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgEmp;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.ComboBox cmbUF;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.TextBox txtFantasia;
        private System.Windows.Forms.TextBox txtRazao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtInsEst;
        private System.Windows.Forms.MaskedTextBox txtCnpj;
        private System.Windows.Forms.TextBox txtEmpID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ContextMenuStrip cmsEmpresa;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CGC;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_INSEST;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_RAZAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_FANTASIA;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_ENDERECO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_FONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_DESABILITADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_OPCADASTRO;
    }
}