﻿namespace GestaoAdministrativa.Cadastros
{
    partial class frmCadPromocoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadPromocoes));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcPromocoes = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBPeriodo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBDescr = new System.Windows.Forms.TextBox();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgPromocoes = new System.Windows.Forms.DataGridView();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_INI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_FIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEP_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLAS_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUB_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_PRECO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_PORC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROMO_DESABILITADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCONTO_PROGRESSIVO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LEVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAGUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PORCENTAGEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPromocoes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmbDepto = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDeptoID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClasID = new System.Windows.Forms.TextBox();
            this.cmbClasse = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSubID = new System.Windows.Forms.TextBox();
            this.cmbSubClas = new System.Windows.Forms.ComboBox();
            this.lblPrecoPromocao = new System.Windows.Forms.Label();
            this.chkDescontoProgressivo = new System.Windows.Forms.CheckBox();
            this.gbDesconto = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPorcentagem = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPague = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtLeve = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtValorPorcentagem = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPreVenda = new System.Windows.Forms.TextBox();
            this.lblPreVenda = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.txtPorc = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPDescr = new System.Windows.Forms.TextBox();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.lblDataFim = new System.Windows.Forms.Label();
            this.lblDataIni = new System.Windows.Forms.Label();
            this.cmbPeriodo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.txtPromoID = new System.Windows.Forms.TextBox();
            this.txtDtIni = new System.Windows.Forms.TextBox();
            this.txtDtFim = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcPromocoes.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPromocoes)).BeginInit();
            this.cmsPromocoes.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gbDesconto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcPromocoes);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(178, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cadastro de Promoções";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcPromocoes
            // 
            this.tcPromocoes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcPromocoes.Controls.Add(this.tpGrade);
            this.tcPromocoes.Controls.Add(this.tpFicha);
            this.tcPromocoes.Location = new System.Drawing.Point(6, 33);
            this.tcPromocoes.Name = "tcPromocoes";
            this.tcPromocoes.SelectedIndex = 0;
            this.tcPromocoes.Size = new System.Drawing.Size(965, 499);
            this.tcPromocoes.TabIndex = 40;
            this.tcPromocoes.SelectedIndexChanged += new System.EventHandler(this.tcPromocoes_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.cmbBPeriodo);
            this.tpGrade.Controls.Add(this.label14);
            this.tpGrade.Controls.Add(this.txtBDescr);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label13);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.dgPromocoes);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(733, 418);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 219;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(597, 432);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(130, 24);
            this.cmbLiberado.TabIndex = 218;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(594, 413);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 217;
            this.label1.Text = "Liberado";
            // 
            // cmbBPeriodo
            // 
            this.cmbBPeriodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBPeriodo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBPeriodo.FormattingEnabled = true;
            this.cmbBPeriodo.Items.AddRange(new object[] {
            "DINAMICO",
            "FIXO"});
            this.cmbBPeriodo.Location = new System.Drawing.Point(452, 432);
            this.cmbBPeriodo.Name = "cmbBPeriodo";
            this.cmbBPeriodo.Size = new System.Drawing.Size(139, 24);
            this.cmbBPeriodo.TabIndex = 216;
            this.cmbBPeriodo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBPeriodo_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(449, 416);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 16);
            this.label14.TabIndex = 87;
            this.label14.Text = "Período";
            // 
            // txtBDescr
            // 
            this.txtBDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBDescr.Location = new System.Drawing.Point(133, 434);
            this.txtBDescr.MaxLength = 30;
            this.txtBDescr.Name = "txtBDescr";
            this.txtBDescr.Size = new System.Drawing.Size(313, 22);
            this.txtBDescr.TabIndex = 82;
            this.txtBDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBDescr_KeyPress);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(6, 434);
            this.txtBID.MaxLength = 18;
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(121, 22);
            this.txtBID.TabIndex = 63;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBID_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(130, 416);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 16);
            this.label13.TabIndex = 62;
            this.label13.Text = "Descrição";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 416);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "Cód. Barras";
            // 
            // dgPromocoes
            // 
            this.dgPromocoes.AllowUserToAddRows = false;
            this.dgPromocoes.AllowUserToDeleteRows = false;
            this.dgPromocoes.AllowUserToOrderColumns = true;
            this.dgPromocoes.AllowUserToResizeColumns = false;
            this.dgPromocoes.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPromocoes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgPromocoes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPromocoes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPromocoes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgPromocoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPromocoes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMP_CODIGO,
            this.PROMO_CODIGO,
            this.PROMO_DESCR,
            this.PROMO_TIPO,
            this.DATA_INI,
            this.DATA_FIM,
            this.PROD_CODIGO,
            this.DEP_DESCR,
            this.CLAS_DESCR,
            this.SUB_DESCR,
            this.PROMO_PRECO,
            this.PROMO_PORC,
            this.PROMO_DESABILITADO,
            this.DTALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO,
            this.DESCONTO_PROGRESSIVO,
            this.LEVE,
            this.PAGUE,
            this.PORCENTAGEM});
            this.dgPromocoes.ContextMenuStrip = this.cmsPromocoes;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPromocoes.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgPromocoes.GridColor = System.Drawing.Color.LightGray;
            this.dgPromocoes.Location = new System.Drawing.Point(6, 6);
            this.dgPromocoes.MultiSelect = false;
            this.dgPromocoes.Name = "dgPromocoes";
            this.dgPromocoes.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPromocoes.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgPromocoes.RowHeadersVisible = false;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPromocoes.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgPromocoes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPromocoes.Size = new System.Drawing.Size(945, 394);
            this.dgPromocoes.TabIndex = 1;
            this.dgPromocoes.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPromocoes_CellMouseClick);
            this.dgPromocoes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPromocoes_CellMouseDoubleClick);
            this.dgPromocoes.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPromocoes_ColumnHeaderMouseClick);
            this.dgPromocoes.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgPromocoes_RowPrePaint);
            this.dgPromocoes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPromocoes_KeyDown);
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            this.EMP_CODIGO.HeaderText = "Emp. ID";
            this.EMP_CODIGO.MaxInputLength = 18;
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            this.EMP_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EMP_CODIGO.Width = 70;
            // 
            // PROMO_CODIGO
            // 
            this.PROMO_CODIGO.DataPropertyName = "PROMO_CODIGO";
            this.PROMO_CODIGO.HeaderText = "Promoção ID";
            this.PROMO_CODIGO.MaxInputLength = 18;
            this.PROMO_CODIGO.Name = "PROMO_CODIGO";
            this.PROMO_CODIGO.ReadOnly = true;
            this.PROMO_CODIGO.Width = 125;
            // 
            // PROMO_DESCR
            // 
            this.PROMO_DESCR.DataPropertyName = "PROMO_DESCR";
            this.PROMO_DESCR.HeaderText = "Descrição";
            this.PROMO_DESCR.MaxInputLength = 50;
            this.PROMO_DESCR.Name = "PROMO_DESCR";
            this.PROMO_DESCR.ReadOnly = true;
            this.PROMO_DESCR.Width = 350;
            // 
            // PROMO_TIPO
            // 
            this.PROMO_TIPO.DataPropertyName = "PROMO_TIPO";
            this.PROMO_TIPO.HeaderText = "Período";
            this.PROMO_TIPO.MaxInputLength = 15;
            this.PROMO_TIPO.Name = "PROMO_TIPO";
            this.PROMO_TIPO.ReadOnly = true;
            // 
            // DATA_INI
            // 
            this.DATA_INI.DataPropertyName = "DATA_INI";
            this.DATA_INI.HeaderText = "Dia/Data Inicial";
            this.DATA_INI.MaxInputLength = 15;
            this.DATA_INI.Name = "DATA_INI";
            this.DATA_INI.ReadOnly = true;
            this.DATA_INI.Width = 120;
            // 
            // DATA_FIM
            // 
            this.DATA_FIM.DataPropertyName = "DATA_FIM";
            this.DATA_FIM.HeaderText = "Dia/Data Final";
            this.DATA_FIM.MaxInputLength = 15;
            this.DATA_FIM.Name = "DATA_FIM";
            this.DATA_FIM.ReadOnly = true;
            this.DATA_FIM.Width = 120;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. de Barras";
            this.PROD_CODIGO.MaxInputLength = 20;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 120;
            // 
            // DEP_DESCR
            // 
            this.DEP_DESCR.DataPropertyName = "DEP_DESCR";
            this.DEP_DESCR.HeaderText = "Departamento";
            this.DEP_DESCR.MaxInputLength = 50;
            this.DEP_DESCR.Name = "DEP_DESCR";
            this.DEP_DESCR.ReadOnly = true;
            this.DEP_DESCR.Width = 150;
            // 
            // CLAS_DESCR
            // 
            this.CLAS_DESCR.DataPropertyName = "CLAS_DESCR";
            this.CLAS_DESCR.HeaderText = "Classe";
            this.CLAS_DESCR.MaxInputLength = 50;
            this.CLAS_DESCR.Name = "CLAS_DESCR";
            this.CLAS_DESCR.ReadOnly = true;
            this.CLAS_DESCR.Width = 150;
            // 
            // SUB_DESCR
            // 
            this.SUB_DESCR.DataPropertyName = "SUB_DESCR";
            this.SUB_DESCR.HeaderText = "Subclasse";
            this.SUB_DESCR.MaxInputLength = 50;
            this.SUB_DESCR.Name = "SUB_DESCR";
            this.SUB_DESCR.ReadOnly = true;
            this.SUB_DESCR.Width = 150;
            // 
            // PROMO_PRECO
            // 
            this.PROMO_PRECO.DataPropertyName = "PROMO_PRECO";
            dataGridViewCellStyle13.Format = "N2";
            dataGridViewCellStyle13.NullValue = null;
            this.PROMO_PRECO.DefaultCellStyle = dataGridViewCellStyle13;
            this.PROMO_PRECO.HeaderText = "Preço";
            this.PROMO_PRECO.MaxInputLength = 18;
            this.PROMO_PRECO.Name = "PROMO_PRECO";
            this.PROMO_PRECO.ReadOnly = true;
            this.PROMO_PRECO.Width = 80;
            // 
            // PROMO_PORC
            // 
            this.PROMO_PORC.DataPropertyName = "PROMO_PORC";
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = null;
            this.PROMO_PORC.DefaultCellStyle = dataGridViewCellStyle14;
            this.PROMO_PORC.HeaderText = "Porcentagem %";
            this.PROMO_PORC.MaxInputLength = 18;
            this.PROMO_PORC.Name = "PROMO_PORC";
            this.PROMO_PORC.ReadOnly = true;
            this.PROMO_PORC.Width = 130;
            // 
            // PROMO_DESABILITADO
            // 
            this.PROMO_DESABILITADO.DataPropertyName = "PROMO_DESABILITADO";
            this.PROMO_DESABILITADO.HeaderText = "Liberado";
            this.PROMO_DESABILITADO.MaxInputLength = 3;
            this.PROMO_DESABILITADO.Name = "PROMO_DESABILITADO";
            this.PROMO_DESABILITADO.ReadOnly = true;
            this.PROMO_DESABILITADO.Width = 75;
            // 
            // DTALTERACAO
            // 
            this.DTALTERACAO.DataPropertyName = "DTALTERACAO";
            dataGridViewCellStyle15.Format = "G";
            this.DTALTERACAO.DefaultCellStyle = dataGridViewCellStyle15;
            this.DTALTERACAO.HeaderText = "Data Alteração";
            this.DTALTERACAO.MaxInputLength = 20;
            this.DTALTERACAO.Name = "DTALTERACAO";
            this.DTALTERACAO.ReadOnly = true;
            this.DTALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle16.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle16;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 20;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // DESCONTO_PROGRESSIVO
            // 
            this.DESCONTO_PROGRESSIVO.DataPropertyName = "DESCONTO_PROGRESSIVO";
            this.DESCONTO_PROGRESSIVO.HeaderText = "Desc. Progressivo";
            this.DESCONTO_PROGRESSIVO.Name = "DESCONTO_PROGRESSIVO";
            this.DESCONTO_PROGRESSIVO.ReadOnly = true;
            this.DESCONTO_PROGRESSIVO.Width = 140;
            // 
            // LEVE
            // 
            this.LEVE.DataPropertyName = "LEVE";
            this.LEVE.HeaderText = "Leve";
            this.LEVE.Name = "LEVE";
            this.LEVE.ReadOnly = true;
            // 
            // PAGUE
            // 
            this.PAGUE.DataPropertyName = "PAGUE";
            this.PAGUE.HeaderText = "Pague";
            this.PAGUE.Name = "PAGUE";
            this.PAGUE.ReadOnly = true;
            // 
            // PORCENTAGEM
            // 
            this.PORCENTAGEM.DataPropertyName = "PORCENTAGEM";
            dataGridViewCellStyle17.Format = "N2";
            dataGridViewCellStyle17.NullValue = null;
            this.PORCENTAGEM.DefaultCellStyle = dataGridViewCellStyle17;
            this.PORCENTAGEM.HeaderText = "Porcentagem";
            this.PORCENTAGEM.Name = "PORCENTAGEM";
            this.PORCENTAGEM.ReadOnly = true;
            this.PORCENTAGEM.Width = 120;
            // 
            // cmsPromocoes
            // 
            this.cmsPromocoes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsPromocoes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsPromocoes.Name = "cmsPromocoes";
            this.cmsPromocoes.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 398);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 66);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pictureBox9);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.pictureBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(574, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 46);
            this.groupBox4.TabIndex = 193;
            this.groupBox4.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 21);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 191;
            this.pictureBox8.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 38);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 38);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.lblPrecoPromocao);
            this.groupBox2.Controls.Add(this.chkDescontoProgressivo);
            this.groupBox2.Controls.Add(this.gbDesconto);
            this.groupBox2.Controls.Add(this.txtValorPorcentagem);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtPreVenda);
            this.groupBox2.Controls.Add(this.lblPreVenda);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.txtPorc);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtPreco);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.dtFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.lblDataFim);
            this.groupBox2.Controls.Add(this.lblDataIni);
            this.groupBox2.Controls.Add(this.cmbPeriodo);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.txtPromoID);
            this.groupBox2.Controls.Add(this.txtDtIni);
            this.groupBox2.Controls.Add(this.txtDtFim);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 386);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cmbDepto);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.txtDeptoID);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.txtClasID);
            this.groupBox5.Controls.Add(this.cmbClasse);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.txtSubID);
            this.groupBox5.Controls.Add(this.cmbSubClas);
            this.groupBox5.Location = new System.Drawing.Point(9, 190);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(914, 65);
            this.groupBox5.TabIndex = 251;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Desconto por Departamento";
            // 
            // cmbDepto
            // 
            this.cmbDepto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDepto.FormattingEnabled = true;
            this.cmbDepto.Location = new System.Drawing.Point(82, 35);
            this.cmbDepto.Name = "cmbDepto";
            this.cmbDepto.Size = new System.Drawing.Size(294, 24);
            this.cmbDepto.TabIndex = 229;
            this.cmbDepto.SelectedIndexChanged += new System.EventHandler(this.cmbDepto_SelectedIndexChanged);
            this.cmbDepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDepto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(8, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 16);
            this.label5.TabIndex = 227;
            this.label5.Text = "Departamento";
            // 
            // txtDeptoID
            // 
            this.txtDeptoID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeptoID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeptoID.Location = new System.Drawing.Point(11, 36);
            this.txtDeptoID.MaxLength = 2;
            this.txtDeptoID.Name = "txtDeptoID";
            this.txtDeptoID.Size = new System.Drawing.Size(58, 22);
            this.txtDeptoID.TabIndex = 228;
            this.txtDeptoID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeptoID_KeyPress);
            this.txtDeptoID.Validated += new System.EventHandler(this.txtDeptoID_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(382, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 16);
            this.label6.TabIndex = 230;
            this.label6.Text = "Classe";
            // 
            // txtClasID
            // 
            this.txtClasID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClasID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClasID.Location = new System.Drawing.Point(385, 36);
            this.txtClasID.MaxLength = 2;
            this.txtClasID.Name = "txtClasID";
            this.txtClasID.Size = new System.Drawing.Size(58, 22);
            this.txtClasID.TabIndex = 231;
            this.txtClasID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClasID_KeyPress);
            this.txtClasID.Validated += new System.EventHandler(this.txtClasID_Validated);
            // 
            // cmbClasse
            // 
            this.cmbClasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClasse.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClasse.FormattingEnabled = true;
            this.cmbClasse.Location = new System.Drawing.Point(449, 35);
            this.cmbClasse.Name = "cmbClasse";
            this.cmbClasse.Size = new System.Drawing.Size(228, 24);
            this.cmbClasse.TabIndex = 232;
            this.cmbClasse.SelectedIndexChanged += new System.EventHandler(this.cmbClasse_SelectedIndexChanged);
            this.cmbClasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbClasse_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(680, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 16);
            this.label7.TabIndex = 233;
            this.label7.Text = "Subclasse";
            // 
            // txtSubID
            // 
            this.txtSubID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubID.Location = new System.Drawing.Point(683, 36);
            this.txtSubID.MaxLength = 2;
            this.txtSubID.Name = "txtSubID";
            this.txtSubID.Size = new System.Drawing.Size(58, 22);
            this.txtSubID.TabIndex = 234;
            this.txtSubID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubID_KeyPress);
            this.txtSubID.Validated += new System.EventHandler(this.txtSubID_Validated);
            // 
            // cmbSubClas
            // 
            this.cmbSubClas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubClas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSubClas.FormattingEnabled = true;
            this.cmbSubClas.Location = new System.Drawing.Point(747, 34);
            this.cmbSubClas.Name = "cmbSubClas";
            this.cmbSubClas.Size = new System.Drawing.Size(157, 24);
            this.cmbSubClas.TabIndex = 235;
            this.cmbSubClas.SelectedIndexChanged += new System.EventHandler(this.cmbSubClas_SelectedIndexChanged);
            this.cmbSubClas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbSubClas_KeyPress);
            // 
            // lblPrecoPromocao
            // 
            this.lblPrecoPromocao.BackColor = System.Drawing.Color.Transparent;
            this.lblPrecoPromocao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecoPromocao.ForeColor = System.Drawing.Color.Red;
            this.lblPrecoPromocao.Location = new System.Drawing.Point(317, 167);
            this.lblPrecoPromocao.Name = "lblPrecoPromocao";
            this.lblPrecoPromocao.Size = new System.Drawing.Size(369, 22);
            this.lblPrecoPromocao.TabIndex = 250;
            this.lblPrecoPromocao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkDescontoProgressivo
            // 
            this.chkDescontoProgressivo.AutoSize = true;
            this.chkDescontoProgressivo.ForeColor = System.Drawing.Color.Navy;
            this.chkDescontoProgressivo.Location = new System.Drawing.Point(9, 283);
            this.chkDescontoProgressivo.Name = "chkDescontoProgressivo";
            this.chkDescontoProgressivo.Size = new System.Drawing.Size(163, 20);
            this.chkDescontoProgressivo.TabIndex = 249;
            this.chkDescontoProgressivo.Text = "Desconto Progressivo";
            this.chkDescontoProgressivo.UseVisualStyleBackColor = true;
            this.chkDescontoProgressivo.CheckedChanged += new System.EventHandler(this.chkDescontoProgressivo_CheckedChanged);
            // 
            // gbDesconto
            // 
            this.gbDesconto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDesconto.Controls.Add(this.label22);
            this.gbDesconto.Controls.Add(this.txtPorcentagem);
            this.gbDesconto.Controls.Add(this.label21);
            this.gbDesconto.Controls.Add(this.txtPague);
            this.gbDesconto.Controls.Add(this.label20);
            this.gbDesconto.Controls.Add(this.txtLeve);
            this.gbDesconto.Controls.Add(this.label19);
            this.gbDesconto.Location = new System.Drawing.Point(9, 311);
            this.gbDesconto.Name = "gbDesconto";
            this.gbDesconto.Size = new System.Drawing.Size(914, 69);
            this.gbDesconto.TabIndex = 248;
            this.gbDesconto.TabStop = false;
            this.gbDesconto.Text = "Desconto Progressivo";
            this.gbDesconto.Visible = false;
            // 
            // label22
            // 
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(381, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(527, 41);
            this.label22.TabIndex = 252;
            this.label22.Text = "Aviso: Desconto Progressivo é uma regra por produto se inserir mais de uma regra " +
    "de desconto para o mesmo produto ele ira acatar o primeiro.";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPorcentagem
            // 
            this.txtPorcentagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPorcentagem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentagem.Location = new System.Drawing.Point(234, 40);
            this.txtPorcentagem.MaxLength = 20;
            this.txtPorcentagem.Name = "txtPorcentagem";
            this.txtPorcentagem.ReadOnly = true;
            this.txtPorcentagem.Size = new System.Drawing.Size(141, 22);
            this.txtPorcentagem.TabIndex = 251;
            this.txtPorcentagem.Text = "0,00";
            this.txtPorcentagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(231, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 16);
            this.label21.TabIndex = 250;
            this.label21.Text = "Porcentagem %";
            // 
            // txtPague
            // 
            this.txtPague.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPague.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPague.Location = new System.Drawing.Point(122, 40);
            this.txtPague.MaxLength = 20;
            this.txtPague.Name = "txtPague";
            this.txtPague.Size = new System.Drawing.Size(101, 22);
            this.txtPague.TabIndex = 249;
            this.txtPague.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPague.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPague_KeyPress);
            this.txtPague.Validated += new System.EventHandler(this.txtPague_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(119, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 16);
            this.label20.TabIndex = 248;
            this.label20.Text = "Pague";
            // 
            // txtLeve
            // 
            this.txtLeve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLeve.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeve.ForeColor = System.Drawing.Color.Black;
            this.txtLeve.Location = new System.Drawing.Point(8, 40);
            this.txtLeve.MaxLength = 20;
            this.txtLeve.Name = "txtLeve";
            this.txtLeve.Size = new System.Drawing.Size(101, 22);
            this.txtLeve.TabIndex = 247;
            this.txtLeve.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLeve.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLeve_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(5, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 16);
            this.label19.TabIndex = 246;
            this.label19.Text = "Leve";
            // 
            // txtValorPorcentagem
            // 
            this.txtValorPorcentagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorPorcentagem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorPorcentagem.Location = new System.Drawing.Point(781, 283);
            this.txtValorPorcentagem.MaxLength = 20;
            this.txtValorPorcentagem.Name = "txtValorPorcentagem";
            this.txtValorPorcentagem.Size = new System.Drawing.Size(141, 22);
            this.txtValorPorcentagem.TabIndex = 247;
            this.txtValorPorcentagem.Text = "0,00";
            this.txtValorPorcentagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorPorcentagem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorPorcentagem_KeyPress);
            this.txtValorPorcentagem.Validated += new System.EventHandler(this.txtValorPorcentagem_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(778, 264);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(124, 16);
            this.label16.TabIndex = 246;
            this.label16.Text = "Valor Promocao %";
            // 
            // txtPreVenda
            // 
            this.txtPreVenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreVenda.Enabled = false;
            this.txtPreVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreVenda.Location = new System.Drawing.Point(631, 283);
            this.txtPreVenda.MaxLength = 20;
            this.txtPreVenda.Name = "txtPreVenda";
            this.txtPreVenda.Size = new System.Drawing.Size(141, 22);
            this.txtPreVenda.TabIndex = 245;
            this.txtPreVenda.Text = "0,00";
            this.txtPreVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPreVenda.Visible = false;
            // 
            // lblPreVenda
            // 
            this.lblPreVenda.AutoSize = true;
            this.lblPreVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreVenda.ForeColor = System.Drawing.Color.Navy;
            this.lblPreVenda.Location = new System.Drawing.Point(628, 264);
            this.lblPreVenda.Name = "lblPreVenda";
            this.lblPreVenda.Size = new System.Drawing.Size(109, 16);
            this.lblPreVenda.TabIndex = 244;
            this.lblPreVenda.Text = "Preço de Venda";
            this.lblPreVenda.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(192, 42);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 243;
            this.pictureBox2.TabStop = false;
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(9, 261);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 242;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // txtPorc
            // 
            this.txtPorc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPorc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorc.Location = new System.Drawing.Point(481, 283);
            this.txtPorc.MaxLength = 20;
            this.txtPorc.Name = "txtPorc";
            this.txtPorc.Size = new System.Drawing.Size(141, 22);
            this.txtPorc.TabIndex = 241;
            this.txtPorc.Text = "0,00";
            this.txtPorc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPorc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPorc_KeyPress);
            this.txtPorc.Validated += new System.EventHandler(this.txtPorc_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(478, 264);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 16);
            this.label18.TabIndex = 240;
            this.label18.Text = "Porcentagem %";
            // 
            // txtPreco
            // 
            this.txtPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreco.Location = new System.Drawing.Point(330, 283);
            this.txtPreco.MaxLength = 20;
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(141, 22);
            this.txtPreco.TabIndex = 239;
            this.txtPreco.Text = "0,00";
            this.txtPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPreco_KeyPress);
            this.txtPreco.Validated += new System.EventHandler(this.txtPreco_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(328, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 16);
            this.label9.TabIndex = 238;
            this.label9.Text = "Valor do Desconto R$";
            // 
            // txtPDescr
            // 
            this.txtPDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPDescr.Location = new System.Drawing.Point(151, 36);
            this.txtPDescr.MaxLength = 50;
            this.txtPDescr.Name = "txtPDescr";
            this.txtPDescr.Size = new System.Drawing.Size(279, 22);
            this.txtPDescr.TabIndex = 226;
            this.txtPDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPDescr_KeyPress);
            this.txtPDescr.Validated += new System.EventHandler(this.txtPDescr_Validated);
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(10, 37);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(132, 22);
            this.txtCodBarras.TabIndex = 225;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(148, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 16);
            this.label3.TabIndex = 224;
            this.label3.Text = "Descrição do Produto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(7, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 16);
            this.label4.TabIndex = 223;
            this.label4.Text = "Código de Barras";
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(391, 61);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(102, 22);
            this.dtFinal.TabIndex = 219;
            this.dtFinal.Visible = false;
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            this.dtFinal.Validated += new System.EventHandler(this.dtFinal_Validated);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(282, 61);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(103, 22);
            this.dtInicial.TabIndex = 218;
            this.dtInicial.Visible = false;
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // lblDataFim
            // 
            this.lblDataFim.AutoSize = true;
            this.lblDataFim.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataFim.ForeColor = System.Drawing.Color.Navy;
            this.lblDataFim.Location = new System.Drawing.Point(391, 42);
            this.lblDataFim.Name = "lblDataFim";
            this.lblDataFim.Size = new System.Drawing.Size(73, 16);
            this.lblDataFim.TabIndex = 217;
            this.lblDataFim.Text = "Data Final";
            this.lblDataFim.Visible = false;
            // 
            // lblDataIni
            // 
            this.lblDataIni.AutoSize = true;
            this.lblDataIni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataIni.ForeColor = System.Drawing.Color.Navy;
            this.lblDataIni.Location = new System.Drawing.Point(282, 42);
            this.lblDataIni.Name = "lblDataIni";
            this.lblDataIni.Size = new System.Drawing.Size(80, 16);
            this.lblDataIni.TabIndex = 216;
            this.lblDataIni.Text = "Data Inicial";
            this.lblDataIni.Visible = false;
            // 
            // cmbPeriodo
            // 
            this.cmbPeriodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPeriodo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPeriodo.FormattingEnabled = true;
            this.cmbPeriodo.Items.AddRange(new object[] {
            "DINAMICO",
            "FIXO"});
            this.cmbPeriodo.Location = new System.Drawing.Point(137, 59);
            this.cmbPeriodo.Name = "cmbPeriodo";
            this.cmbPeriodo.Size = new System.Drawing.Size(139, 24);
            this.cmbPeriodo.TabIndex = 215;
            this.cmbPeriodo.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodo_SelectedIndexChanged);
            this.cmbPeriodo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPeriodo_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(134, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 16);
            this.label17.TabIndex = 214;
            this.label17.Text = "Período";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(109, 43);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 191;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 16);
            this.label8.TabIndex = 190;
            this.label8.Text = "Promoção ID";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 108;
            this.lblEstab.Tag = "";
            // 
            // txtPromoID
            // 
            this.txtPromoID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPromoID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPromoID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPromoID.Location = new System.Drawing.Point(16, 61);
            this.txtPromoID.MaxLength = 18;
            this.txtPromoID.Name = "txtPromoID";
            this.txtPromoID.ReadOnly = true;
            this.txtPromoID.Size = new System.Drawing.Size(109, 22);
            this.txtPromoID.TabIndex = 81;
            this.txtPromoID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDtIni
            // 
            this.txtDtIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDtIni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtIni.Location = new System.Drawing.Point(282, 61);
            this.txtDtIni.MaxLength = 2;
            this.txtDtIni.Name = "txtDtIni";
            this.txtDtIni.Size = new System.Drawing.Size(103, 22);
            this.txtDtIni.TabIndex = 221;
            this.txtDtIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDtIni.Visible = false;
            this.txtDtIni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDtIni_KeyPress);
            this.txtDtIni.Validated += new System.EventHandler(this.txtDtIni_Validated);
            // 
            // txtDtFim
            // 
            this.txtDtFim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDtFim.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtFim.Location = new System.Drawing.Point(391, 61);
            this.txtDtFim.MaxLength = 2;
            this.txtDtFim.Name = "txtDtFim";
            this.txtDtFim.Size = new System.Drawing.Size(102, 22);
            this.txtDtFim.TabIndex = 222;
            this.txtDtFim.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDtFim.Visible = false;
            this.txtDtFim.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDtFim_KeyPress);
            this.txtDtFim.Validated += new System.EventHandler(this.txtDtFim_Validated);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtPDescr);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtCodBarras);
            this.groupBox6.Location = new System.Drawing.Point(8, 89);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(914, 73);
            this.groupBox6.TabIndex = 252;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Desconto por Produto";
            // 
            // frmCadPromocoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCadPromocoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCadPromocoes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCadPromocoes_Load);
            this.Shown += new System.EventHandler(this.frmCadPromocoes_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcPromocoes.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPromocoes)).EndInit();
            this.cmsPromocoes.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.gbDesconto.ResumeLayout(false);
            this.gbDesconto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcPromocoes;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.TextBox txtBDescr;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.DataGridView dgPromocoes;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.TextBox txtPromoID;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbPeriodo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDtFim;
        private System.Windows.Forms.TextBox txtDtIni;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label lblDataFim;
        private System.Windows.Forms.Label lblDataIni;
        private System.Windows.Forms.TextBox txtPDescr;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbDepto;
        private System.Windows.Forms.TextBox txtDeptoID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbSubClas;
        private System.Windows.Forms.TextBox txtSubID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbClasse;
        private System.Windows.Forms.TextBox txtClasID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.TextBox txtPorc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbBPeriodo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ContextMenuStrip cmsPromocoes;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.TextBox txtPreVenda;
        private System.Windows.Forms.Label lblPreVenda;
        private System.Windows.Forms.TextBox txtValorPorcentagem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkDescontoProgressivo;
        private System.Windows.Forms.GroupBox gbDesconto;
        private System.Windows.Forms.TextBox txtPorcentagem;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPague;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtLeve;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_INI;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_FIM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEP_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLAS_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUB_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_PRECO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_PORC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROMO_DESABILITADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCONTO_PROGRESSIVO;
        private System.Windows.Forms.DataGridViewTextBoxColumn LEVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAGUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PORCENTAGEM;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblPrecoPromocao;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
    }
}