﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadComanda : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        private DataTable dtComanda = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbComanda = new ToolStripButton("Comanda");
        private ToolStripSeparator tssComanda = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        #endregion

        public frmCadComanda(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmCadComanda_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var comanda = new Comanda();

                dtComanda = comanda.BuscarDados(txtBID.Text.Trim(), Principal.estAtual, Principal.empAtual, out strOrdem);
                if (dtComanda.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtComanda.Rows.Count;
                    dgComanda.DataSource = dtComanda;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, 0));
                    dgComanda.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgComanda.DataSource = dtComanda;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            if (dtComanda.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgComanda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgComanda.CurrentCell = dgComanda.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcComanda.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtComanda.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgComanda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgComanda.CurrentCell = dgComanda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgComanda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgComanda.CurrentCell = dgComanda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgComanda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgComanda.CurrentCell = dgComanda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbComanda.AutoSize = false;
                this.tsbComanda.Image = Properties.Resources.cadastro;
                this.tsbComanda.Size = new System.Drawing.Size(125, 20);
                this.tsbComanda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbComanda);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssComanda);
                tsbComanda.Click += delegate
                {
                    var cadComanda = Application.OpenForms.OfType<frmCadComanda>().FirstOrDefault();
                    Funcoes.BotoesCadastro(cadComanda.Name);
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, contRegistros));
                    if (tcComanda.SelectedTab == tpGrade)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    }
                    else if (tcComanda.SelectedTab == tpFicha && contRegistros == 0)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    }
                    cadComanda.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Funcoes.BotoesCadastro("frmCadComanda");
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, contRegistros));
                if (tcComanda.SelectedTab == tpGrade)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
                else if (tcComanda.SelectedTab == tpFicha && contRegistros == 0)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtComanda.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbComanda);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssComanda);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtComanda.Clear();
            dgComanda.DataSource = dtComanda;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtComanda.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            Funcoes.BotoesCadastro("frmCadComanda");
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            txtComanda.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcComanda.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtComanda.Text = Funcoes.ChecaCampoVazio(dtComanda.Rows[linha]["COM_NUMERO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, linha));
                txtComanda.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgComanda_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, contRegistros));
            emGrade = true;
        }

        private void dgComanda_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcComanda.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgComanda_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgComanda.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtComanda = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgComanda.Columns[e.ColumnIndex].Name + " DESC");
                        dgComanda.DataSource = dtComanda;
                        decrescente = true;
                    }
                    else
                    {
                        dtComanda = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgComanda.Columns[e.ColumnIndex].Name + " ASC");
                        dgComanda.DataSource = dtComanda;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgComanda_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtComanda.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtComanda.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtComanda.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void tcComanda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcComanda.SelectedTab == tpGrade)
            {
                dgComanda.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcComanda.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadComanda");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComanda, contRegistros));
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtComanda.Focus();
                }
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtPesq = Util.RegistrosPorEmpresa("CAD_COMANDA", "COM_NUMERO", txtComanda.Text.ToUpper().Trim(), true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Comanda já cadastrada.";
                        Funcoes.Avisa();
                        txtComanda.Text = "";
                        txtComanda.Focus();
                        return false;
                    }

                    var comanda = new Comanda(
                        Principal.empAtual,
                        Principal.estAtual,
                        int.Parse(txtComanda.Text.Trim()),
                        DateTime.Now,
                        Principal.usuario
                    );

                    if (comanda.InsereRegistros(comanda).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("COM_NUMERO", txtComanda.Text, Principal.usuario, "CAD_COMANDA", txtComanda.Text, Principal.estAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtComanda.Clear();
                        dgComanda.DataSource = dtComanda;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtComanda.Focus();
                        return false;
                    }

                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtComanda.Text.Trim()))
            {
                Principal.mensagem = "Número da comanda não pode ser em branco.";
                Funcoes.Avisa();
                txtComanda.Focus();
                return false;
            }

            return true;
        }

        public bool Atualiza()
        {
            return true;
        }

        public bool Excluir()
        {
            try
            {
                if (txtComanda.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma a exclusão da comanda?", "Exclusão tabela Cad. Comanda", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var comanda = new Comanda();
                        if (!comanda.ExcluirDados(Principal.estAtual, Principal.empAtual, txtComanda.Text.Trim()).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("COM_NUMERO", txtComanda.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CAD_COMANDA", txtComanda.Text, Principal.motivo, Principal.estAtual);
                            dtComanda.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgComanda.DataSource = dtComanda;
                            tcComanda.SelectedTab = tpGrade;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgComanda.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgComanda);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgComanda.RowCount;
                    for (i = 0; i <= dgComanda.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgComanda[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }
                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgComanda.ColumnCount; k++)
                    {
                        if (dgComanda.Columns[k].Visible == true)
                        {
                            switch (dgComanda.Columns[k].HeaderText)
                            {
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgComanda.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgComanda.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgComanda.ColumnCount : indice) + Convert.ToString(dgComanda.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgComanda.ColumnCount; i++)
                {
                    if (dgComanda.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgComanda.ColumnCount];
                string[] coluna = new string[dgComanda.ColumnCount];

                for (int i = 0; i < dgComanda.ColumnCount; i++)
                {
                    grid[i] = dgComanda.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgComanda.ColumnCount; i++)
                {
                    if (dgComanda.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgComanda.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgComanda.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgComanda.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgComanda.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgComanda.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgComanda.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgComanda.ColumnCount; i++)
                        {
                            dgComanda.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comandas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        public void AtalhoGrade()
        {
            tcComanda.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcComanda.SelectedTab = tpFicha;
        }

        private void txtComanda_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
