﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadGrupos : Form, Botoes
    {
        private DataTable dtGrupo = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbCGrupos = new ToolStripButton("Grupo de Clientes");
        private ToolStripSeparator tssCGrupos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadGrupos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadGrupos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        public void Primeiro()
        {
            if (dtGrupo.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgGrupo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgGrupo.CurrentCell = dgGrupo.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcGrupo.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtGrupo.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupo.CurrentCell = dgGrupo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupo.CurrentCell = dgGrupo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadGrupos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, contRegistros));
            if (tcGrupo.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcGrupo.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtGrupoID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupo.CurrentCell = dgGrupo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbCGrupos.AutoSize = false;
                this.tsbCGrupos.Image = Properties.Resources.cadastro;
                this.tsbCGrupos.Size = new System.Drawing.Size(135, 20);
                this.tsbCGrupos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbCGrupos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssCGrupos);
                tsbCGrupos.Click += delegate
                {
                    var cadGrupo = Application.OpenForms.OfType<frmCadGrupos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadGrupo.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtGrupo.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbCGrupos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssCGrupos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtGrupo.Clear();
            dgGrupo.DataSource = dtGrupo;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtGrupoID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcGrupo.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtGrupoID.Text = Funcoes.ChecaCampoVazio(dtGrupo.Rows[linha]["GRUPO_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtGrupo.Rows[linha]["GRUPO_DESCR"].ToString());
                if (dtGrupo.Rows[linha]["GRUPO_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtGrupo.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtGrupo.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool Excluir()
        {
            try
            {
                if ((txtGrupoID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do grupo de clientes?", "Exclusão tabela Grupos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirGrupoClientes(txtGrupoID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var grupoClientes = new GrupoDeCliente();
                            if (!grupoClientes.ExcluirDados(txtGrupoID.Text,Principal.empAtual).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("GRUPO_CODIGO", txtGrupoID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "GRUPO_CLIENTES", txtDescr.Text, Principal.motivo, Principal.empAtual);
                                dtGrupo.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgGrupo.DataSource = dtGrupo;
                                tcGrupo.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    txtGrupoID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("GRUPO_CLIENTES", "GRUPO_CODIGO", 0, "", Principal.empAtual));
                    chkLiberado.Checked = true;
                    var grupoCliente = new GrupoDeCliente(
                        Convert.ToInt32(txtGrupoID.Text),
                        Principal.empAtual,
                        txtDescr.Text.Trim(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );

                    //VERIFICA SE GRUPO JA ESTA CADASTRADO//
                    Principal.dtPesq = Util.RegistrosPorEmpresa("GRUPO_CLIENTES", "GRUPO_DESCR", txtDescr.Text.Trim(), true, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Grupo de clientes já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    if (grupoCliente.InsereRegistros(grupoCliente).Equals(true))
                    {
                        //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                        Funcoes.GravaLogInclusao("GRUPO_CODIGO", txtGrupoID.Text, Principal.usuario, "GRUPO_CLIENTES", txtDescr.Text, Principal.empAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtGrupo.Clear();
                        dgGrupo.DataSource = dtGrupo;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var grupoCliente = new GrupoDeCliente(
                        Convert.ToInt32(txtGrupoID.Text),
                        Principal.empAtual,
                        txtDescr.Text.Trim(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );

                    Principal.dtBusca = Util.RegistrosPorEmpresa("GRUPO_CLIENTES", "GRUPO_CODIGO", txtGrupoID.Text, true);
                    if (grupoCliente.AtualizaDados(grupoCliente, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Grupo de Clientes");
                    }
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtGrupo.Clear();
                    dgGrupo.DataSource = dtGrupo;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void dgGrupo_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, contRegistros));
            emGrade = true;
        }

        private void dgGrupo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcGrupo.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgGrupo_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtGrupo.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtGrupo.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtGrupo.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgGrupo_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgGrupo.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtGrupo = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgGrupo.Columns[e.ColumnIndex].Name + " DESC");
                        dgGrupo.DataSource = dtGrupo;
                        decrescente = true;
                    }
                    else
                    {
                        dtGrupo = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgGrupo.Columns[e.ColumnIndex].Name + " ASC");
                        dgGrupo.DataSource = dtGrupo;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgGrupo.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgGrupo);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgGrupo.RowCount;
                    for (i = 0; i <= dgGrupo.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgGrupo[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;

                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgGrupo.ColumnCount; k++)
                    {
                        if (dgGrupo.Columns[k].Visible == true)
                        {
                            switch (dgGrupo.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgGrupo.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgGrupo.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgGrupo.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgGrupo.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgGrupo.ColumnCount : indice) + Convert.ToString(dgGrupo.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var grupoClientes = new GrupoDeCliente();
                dtGrupo = grupoClientes.BuscarDados(txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text), Principal.empAtual, txtBDescr.Text.Trim(), cmbLiberado.Text, out strOrdem);
                if (dtGrupo.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtGrupo.Rows.Count;
                    dgGrupo.DataSource = dtGrupo;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, 0));
                    dgGrupo.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgGrupo.DataSource = dtGrupo;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcGrupo.SelectedTab == tpGrade)
            {
                dgGrupo.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcGrupo.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadGrupos");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupo, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = false;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void dgGrupo_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgGrupo.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgGrupo.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgGrupo.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtGrupoID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgGrupo.ColumnCount; i++)
                {
                    if (dgGrupo.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgGrupo.ColumnCount];
                string[] coluna = new string[dgGrupo.ColumnCount];

                for (int i = 0; i < dgGrupo.ColumnCount; i++)
                {
                    grid[i] = dgGrupo.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgGrupo.ColumnCount; i++)
                {
                    if (dgGrupo.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgGrupo.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgGrupo.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgGrupo.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgGrupo.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgGrupo.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgGrupo.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgGrupo.ColumnCount; i++)
                        {
                            dgGrupo.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcGrupo.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcGrupo.SelectedTab = tpFicha;
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
