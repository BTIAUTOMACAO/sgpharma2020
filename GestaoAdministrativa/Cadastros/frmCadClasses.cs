﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    /// <summary>
    /// Cadastro de Classes utilizada para classificar o Produto
    /// </summary>
    public partial class frmCadClasses : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        private DataTable dtClasse = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbClasse = new ToolStripButton("Classes");
        private ToolStripSeparator tssClasse = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        #endregion

        /// <summary>
        /// Inicialização do Formulário
        /// </summary>
        /// <param name="menu">Formulário Principal</param>
        public frmCadClasses(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();

            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void dgClasse_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, contRegistros));
            emGrade = true;
        }

        private void dgClasse_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcClasse.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgClasse_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtClasse.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtClasse.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtClasse.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Primeiro()
        {
            if (dtClasse.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgClasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgClasse.CurrentCell = dgClasse.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcClasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtClasse.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClasse.CurrentCell = dgClasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClasse.CurrentCell = dgClasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Botão de Navegação do Menu Principal
        /// </summary>
        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgClasse.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgClasse.CurrentCell = dgClasse.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        /// <summary>
        /// Função que Habilita ou Desabilita Botões de Cadastro
        /// </summary>
        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadClasses");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, contRegistros));
            if (tcClasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcClasse.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtClasID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                    
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Cria botão do formulário na barra inferior do menu principal
        /// </summary>
        public void Botao()
        {
            try
            {
                this.tsbClasse.AutoSize = false;
                this.tsbClasse.Image = Properties.Resources.cadastro;
                this.tsbClasse.Size = new System.Drawing.Size(125, 20);
                this.tsbClasse.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbClasse);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssClasse);
                tsbClasse.Click += delegate
                {
                    var cadClasse = Application.OpenForms.OfType<frmCadClasses>().FirstOrDefault();
                    BotoesHabilitados();
                    cadClasse.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Abre formulário se o mesmo já estiver na barra de botões
        /// </summary>
        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Fecha o formulário e remove o botão do mesmo
        /// </summary>
        public void Sair()
        {
            try
            {
                dtClasse.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbClasse);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssClasse);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Limpa os campos da Aba em Grade
        /// </summary>
        public void LimparGrade()
        {
            dtClasse.Clear();
            dgClasse.DataSource = dtClasse;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        /// <summary>
        /// Limpa os campos da Aba em Ficha
        /// </summary>
        public void LimparFicha()
        {
            txtClasID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        /// <summary>
        /// Botão Limpar do Menu Principal, limpa os dados do Formulário
        /// </summary>
        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcClasse.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        /// <summary>
        /// Carrega os dados do registro selecionado, quando muda da aba em grade para a aba em ficha
        /// </summary>
        /// <param name="linha">ID da linha do Grid dos Registros a ser carregado</param>
        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Empresa: " + Principal.empAtual + " - " + Principal.nomeAtual;
                txtClasID.Text = Convert.ToInt32(dtClasse.Rows[linha]["CLAS_CODIGO"]).ToString();
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtClasse.Rows[linha]["CLAS_DESCR"].ToString());
                if (dtClasse.Rows[linha]["CLAS_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtClasse.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtClasse.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadClasses_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        /// <summary>
        /// Botão Excluir do Menu Principal, remove o registro selecionado
        /// </summary>
        /// <returns></returns>
        public bool Excluir()
        {
            try
            {
                if ((txtClasID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da classe?", "Exclusão tabela Classes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirClasses(txtClasID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var classe = new Classe();
                            if (!classe.ExcluiClasse(Convert.ToInt32(txtClasID.Text)).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("CLAS_CODIGO", txtClasID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CLASSES", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtClasse.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgClasse.DataSource = dtClasse;
                                tcClasse.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Classes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Botão Incluir do Menu Principal, insere um registro
        /// </summary>
        /// <returns></returns>
        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos())
                {
                    Cursor = Cursors.WaitCursor;
                    chkLiberado.Checked = true;

                    var classe = new Classe(
                        Principal.empAtual,
                        Funcoes.IdentificaVerificaID("CLASSES", "CLAS_CODIGO", 0, string.Empty,Principal.empAtual),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                    Principal.dtPesq = Util.RegistrosPorEmpresa("CLASSES", "CLAS_DESCR", txtDescr.Text.ToUpper(), true, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Classe já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else
                    {
                        if (classe.InsereRegistros(classe).Equals(true))
                        {
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("CLAS_CODIGO", classe.ClasCodigo.ToString(), Principal.usuario, "CLASSES", classe.ClasDescr, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtClasse.Clear();
                            dgClasse.DataSource = dtClasse;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Erro: ao efetuar o cadastro", "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Funcoes.Avisa();
                            txtDescr.Focus();
                            return false;
                        }
                    }
                }
                chkLiberado.Checked = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Exporta os dados do Grid para Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgClasse.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgClasse);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgClasse.RowCount;
                    for (i = 0; i <= dgClasse.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgClasse[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgClasse.ColumnCount; k++)
                    {
                        if (dgClasse.Columns[k].Visible == true)
                        {
                            switch (dgClasse.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgClasse.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgClasse.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgClasse.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgClasse.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgClasse.ColumnCount : indice) + Convert.ToString(dgClasse.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    planilha.Columns.AutoFit();

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Botão Atualizar do Menu Principal, atualiza um registro
        /// </summary>
        /// <returns></returns>
        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var classe = new Classe(
                        Principal.empAtual,
                        int.Parse(txtClasID.Text),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );



                    Principal.dtBusca = Util.RegistrosPorEmpresa("CLASSES", "CLAS_CODIGO", txtClasID.Text, true);

                    if (classe.AtualizaDados(classe, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Classes");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtClasse.Clear();
                        dgClasse.DataSource = dtClasse;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Botão Impressão de Relatório do Menu Principal
        /// </summary>
        public void ImprimirRelatorio()
        {
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var classe = new Classe();

                dtClasse = classe.BuscaDados(Principal.empAtual, txtBID.Text, txtBDescr.Text, cmbLiberado.Text, out strOrdem);
                if (dtClasse.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtClasse.Rows.Count;
                    dgClasse.DataSource = dtClasse;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, 0));
                    dgClasse.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Classes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgClasse.DataSource = dtClasse;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcClasse.SelectedTab == tpGrade)
            {
                dgClasse.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcClasse.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadClasses");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, contRegistros));
                        chkLiberado.Checked = false;
                        emGrade = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void dgClasse_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgClasse.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtClasse = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgClasse.Columns[e.ColumnIndex].Name + " DESC");
                        dgClasse.DataSource = dtClasse;
                        decrescente = true;
                    }
                    else
                    {
                        dtClasse = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgClasse.Columns[e.ColumnIndex].Name + " ASC");
                        dgClasse.DataSource = dtClasse;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClasse, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void dgClasse_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgClasse.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgClasse.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgClasse.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtClasID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgClasse.ColumnCount; i++)
                {
                    if (dgClasse.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgClasse.ColumnCount];
                string[] coluna = new string[dgClasse.ColumnCount];

                for (int i = 0; i < dgClasse.ColumnCount; i++)
                {
                    grid[i] = dgClasse.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgClasse.ColumnCount; i++)
                {
                    if (dgClasse.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgClasse.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgClasse.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgClasse.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgClasse.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgClasse.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgClasse.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgClasse.ColumnCount; i++)
                        {
                            dgClasse.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Classes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Atalho F1 para ir para aba em Grade
        /// </summary>
        public void AtalhoGrade()
        {
            tcClasse.SelectedTab = tpGrade;
        }

        /// <summary>
        /// Atalho F2 para ir para aba em Ficha
        /// </summary>
        public void AtalhoFicha()
        {
            tcClasse.SelectedTab = tpFicha;
        }

        /// <summary>
        /// Botão Ajuda do Menu Principal
        /// </summary>
        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        
    }
}
