﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadTabPrecos : Form, Botoes
    {
        private DataTable dtTabPrecos = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTabPrecos = new ToolStripButton("Tabela de Preços");
        private ToolStripSeparator tssTabPrecos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadTabPrecos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadTabPrecos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcTabPrecos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTabPrecos.SelectedTab == tpGrade)
            {
                dgTabPrecos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTabPrecos.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadTabPrecos");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, contRegistros));
                        chkLiberado.Checked = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private void dgTabPrecos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, contRegistros));
            emGrade = true;
        }

        private void dgTabPrecos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTabPrecos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTabPrecos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTabPrecos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTabPrecos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTabPrecos.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        public void Primeiro()
        {
            if (dtTabPrecos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTabPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTabPrecos.CurrentCell = dgTabPrecos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTabPrecos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTabPrecos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabPrecos.CurrentCell = dgTabPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabPrecos.CurrentCell = dgTabPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabPrecos.CurrentCell = dgTabPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadTabPrecos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, contRegistros));
            if (tcTabPrecos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTabPrecos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtTabID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTabPrecos.AutoSize = false;
                this.tsbTabPrecos.Image = Properties.Resources.cadastro;
                this.tsbTabPrecos.Size = new System.Drawing.Size(125, 20);
                this.tsbTabPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTabPrecos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTabPrecos);
                tsbTabPrecos.Click += delegate
                {
                    var cadTabPrecos = Application.OpenForms.OfType<frmCadTabPrecos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadTabPrecos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtTabPrecos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTabPrecos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTabPrecos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTabPrecos.Clear();
            dgTabPrecos.DataSource = dtTabPrecos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtTabID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTabPrecos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtTabID.Text = Funcoes.ChecaCampoVazio(Convert.ToInt32(dtTabPrecos.Rows[linha]["TAB_CODIGO"]).ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtTabPrecos.Rows[linha]["TAB_DESCR"].ToString());
                if (dtTabPrecos.Rows[linha]["TAB_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtTabPrecos.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTabPrecos.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTabPrecos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTabPrecos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTabPrecos.RowCount;
                    for (i = 0; i <= dgTabPrecos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTabPrecos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTabPrecos.ColumnCount; k++)
                    {
                        if (dgTabPrecos.Columns[k].Visible == true)
                        {
                            switch (dgTabPrecos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTabPrecos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTabPrecos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTabPrecos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTabPrecos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTabPrecos.ColumnCount : indice) + Convert.ToString(dgTabPrecos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtTabID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da tabela de preços?", "Exclusão tabela Tabela de Preços", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirTabPrecos(txtTabID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var tabpreco = new TabPreco();

                            if (tabpreco.ExcluirDados(txtTabID.Text).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("TAB_CODIGO", txtTabID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "TAB_PRECOS", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Exclusão realizada com sucesso", "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtTabPrecos.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTabPrecos.DataSource = dtTabPrecos;
                                tcTabPrecos.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtPesq = Util.RegistrosPorEstabelecimento("TAB_PRECOS", "TAB_DESCR", txtDescr.Text.ToUpper());
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Tabela de preços já cadastrada.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else
                    {
                        chkLiberado.Checked = true;
                        var tPreco = new TabPreco(
                           Principal.estAtual,
                           Funcoes.IdentificaVerificaID("TAB_PRECOS", "TAB_CODIGO"),
                           txtDescr.Text.ToUpper(),
                           chkLiberado.Checked == true ? "N" : "S",
                           DateTime.Now,
                           Principal.usuario.ToUpper(),
                           DateTime.Now,
                           txtUsuario.Text.ToUpper()
                           );

                        if (tPreco.InserirDados(tPreco).Equals(true))
                        {
                            dtTabPrecos.Clear();
                            dgTabPrecos.DataSource = dtTabPrecos;
                            emGrade = false;
                            Limpar();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Erro: ao tentar cadastrar", "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtDescr.Focus();
                            return false;
                        }
                    }
                }

                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var tPreco = new TabPreco(
                            Principal.estAtual,
                            Convert.ToInt32(Math.Truncate(Convert.ToDouble(txtTabID.Text))),
                            txtDescr.Text.ToUpper(),
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario.ToUpper(),
                            DateTime.Now,
                            txtUsuario.Text.ToUpper()
                        );

                        Principal.dtBusca = Util.RegistrosPorEstabelecimento("TAB_PRECOS", "TAB_CODIGO", txtTabID.Text);
                        if (tPreco.AtualizaDados(tPreco, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Tabela de Preços");
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtTabPrecos.Clear();
                            dgTabPrecos.DataSource = dtTabPrecos;
                            emGrade = false;
                            Principal.mdiPrincipal.btnExcluir.Enabled = false;
                            Principal.mdiPrincipal.btnIncluir.Enabled = true;
                            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                            tslRegistros.Text = "";
                            LimparFicha();
                            return true;
                        }
                        else {
                            MessageBox.Show("Erro ao tentar afetuar a atualização", "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        
                        }

                    }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var tPreco = new TabPreco();

                dtTabPrecos = tPreco.BuscaDados(txtBID.Text, txtBDescr.Text.Trim(), cmbLiberado.Text, out strOrdem);

                if (dtTabPrecos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTabPrecos.Rows.Count;
                    dgTabPrecos.DataSource = dtTabPrecos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, 0));
                    dgTabPrecos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTabPrecos.DataSource = dtTabPrecos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        private void dgTabPrecos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTabPrecos.Rows.Count > 0)
                {
                    Cursor = Cursors.Default;
                    if (decrescente == false)
                    {
                        dtTabPrecos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTabPrecos.Columns[e.ColumnIndex].Name + " DESC");
                        dgTabPrecos.DataSource = dtTabPrecos;
                        decrescente = true;
                    }
                    else
                    {
                        dtTabPrecos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTabPrecos.Columns[e.ColumnIndex].Name + " ASC");
                        dgTabPrecos.DataSource = dtTabPrecos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabPrecos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void dgTabPrecos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTabPrecos.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgTabPrecos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTabPrecos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtTabID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTabPrecos.ColumnCount; i++)
                {
                    if (dgTabPrecos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTabPrecos.ColumnCount];
                string[] coluna = new string[dgTabPrecos.ColumnCount];

                for (int i = 0; i < dgTabPrecos.ColumnCount; i++)
                {
                    grid[i] = dgTabPrecos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTabPrecos.ColumnCount; i++)
                {
                    if (dgTabPrecos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTabPrecos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTabPrecos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTabPrecos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTabPrecos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTabPrecos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTabPrecos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTabPrecos.ColumnCount; i++)
                        {
                            dgTabPrecos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabela de Preços", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTabPrecos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTabPrecos.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
