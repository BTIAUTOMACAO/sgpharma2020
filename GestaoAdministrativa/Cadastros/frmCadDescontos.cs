﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadDescontos : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private string strOrdem;
        private DataTable dtDescontos = new DataTable();
        private bool emGrade;
        private int contRegistros;
        private bool decrescente;
        private ToolStripButton tsbDescontos = new ToolStripButton("Descontos");
        private ToolStripSeparator tssDescontos = new ToolStripSeparator();
        #endregion

        public frmCadDescontos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadDescontos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEmpres.Focus();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtPDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                DataTable dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                Principal.strSql = "SELECT PROD_DESCR, B.DEP_CODIGO FROM PRODUTOS A "
                    + " INNER JOIN PRODUTOS_DETALHE B ON (A.PROD_CODIGO = B.PROD_CODIGO AND A.PROD_ID = B.PROD_ID)"
                    + " INNER JOIN PRECOS C ON (A.PROD_CODIGO = C.PROD_CODIGO  AND A.PROD_ID = C.PROD_ID)"
                    + " WHERE B.EST_CODIGO = " + Principal.estAtual + " AND B.EMP_CODIGO = " + Principal.empAtual
                    + " AND C.EMP_CODIGO = B.EMP_CODIGO AND C.EST_CODIGO = B.EST_CODIGO"
                    + " AND C.TAB_CODIGO = 1 AND PROD_SITUACAO = 'A'"
                    + " AND A.PROD_CODIGO = '" + txtCodBarras.Text + "'";
                dtLePrdutos = BancoDados.selecionarRegistros(Principal.strSql);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtPDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                    txtDeptoID.Text = dtLePrdutos.Rows[0]["DEP_CODIGO"].ToString();
                    Funcoes.AchaCombo(cmbDepto, txtDeptoID);
                    txtClasID.Text = "";
                    cmbClasse.SelectedIndex = -1;
                    txtSubID.Text = "";
                    cmbSubClas.SelectedIndex = -1;
                    txtDescMin.Focus();
                }
                else
                {
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtPDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDeptoID.Focus();
        }

        private void txtPDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPDescr.Text.Trim() != "")
                {
                    var produto = new Produto();
                    Principal.dtRetorno = produto.BuscaProdutosDescricaoLike(txtPDescr.Text.ToUpper());
                    if (Principal.dtRetorno.Rows.Count == 1)
                    {
                        txtCodBarras.Text = Principal.dtRetorno.Rows[0]["PROD_CODIGO"].ToString();
                        LeProdutosCod();
                    }
                    else if (Principal.dtRetorno.Rows.Count == 0)
                    {
                        MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtPDescr.Text = "";
                        txtPDescr.Focus();
                    }
                    else
                    {
                        //FORM DE BUSCA DE PRODUTOS//
                        using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                        {
                            buscaProd.BuscaProd(txtPDescr.Text.ToUpper());
                            buscaProd.ShowDialog();
                        }
                        if (!String.IsNullOrEmpty(Principal.codBarra))
                        {
                            txtCodBarras.Text = Principal.codBarra;
                            txtPDescr.Text = Principal.prodDescr;
                            LeProdutosCod();
                        }
                        else
                        {
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDeptoID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbDepto.Focus();
        }

        private void txtDeptoID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbDepto, txtDeptoID);
            if (cmbDepto.SelectedIndex != -1)
                txtClasID.Focus();
        }

        private void cmbDepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtClasID.Focus();
        }

        private void cmbDepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);
        }

        private void txtClasID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbClasse, txtClasID);
            if (cmbClasse.SelectedIndex != -1)
                txtSubID.Focus();
        }

        private void txtClasID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbClasse.Focus();
        }

        private void cmbClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
        }

        private void cmbClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSubID.Focus();
        }

        private void txtSubID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbSubClas.Focus();
        }

        private void txtSubID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbSubClas, txtSubID);
            if (cmbSubClas.SelectedIndex != -1)
                txtDescMin.Focus();
        }

        private void cmbSubClas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDescMin.Focus();
        }

        private void cmbSubClas_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
        }

        private void txtDescMin_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDescMax.Focus();
        }

        private void txtDescMin_Validated(object sender, EventArgs e)
        {
            if (txtDescMin.Text.Trim() != "")
            {
                txtDescMin.Text = String.Format("{0:N}", Convert.ToDecimal(txtDescMin.Text));
            }
            else
                txtDescMin.Text = "0,00";
        }

        private void txtDescMax_Validated(object sender, EventArgs e)
        {
            if (txtDescMax.Text.Trim() != "")
            {
                txtDescMax.Text = String.Format("{0:N}", Convert.ToDecimal(txtDescMax.Text));
            }
            else
                txtDescMax.Text = "0,00";
        }

        private void txtDescMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void txtEmpres_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbEmpres.Focus();
        }

        private void txtEmpres_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbEmpres, txtEmpres);
            if (cmbEmpres.SelectedIndex != -1)
                txtCodBarras.Focus();
        }

        private void cmbEmpres_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void cmbEmpres_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEmpres.Text = Convert.ToString(cmbEmpres.SelectedValue);
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var desconto = new Desconto();

                dtDescontos = desconto.BuscaDados(Principal.empAtual, txtBID.Text, txtBDescr.Text, cmbLiberado.Text, out strOrdem);
                if (dtDescontos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtDescontos.Rows.Count;
                    dgDescontos.DataSource = dtDescontos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, 0));
                    dgDescontos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgDescontos.DataSource = dtDescontos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgDescontos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, contRegistros));
            emGrade = true;
        }

        private void dgDescontos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcDescontos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }

        }

        private void dgDescontos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgDescontos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtDescontos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDescontos.Columns[e.ColumnIndex].Name + " DESC");
                        dgDescontos.DataSource = dtDescontos;
                        decrescente = true;
                    }
                    else
                    {
                        dtDescontos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDescontos.Columns[e.ColumnIndex].Name + " ASC");
                        dgDescontos.DataSource = dtDescontos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgDescontos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtDescontos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtDescontos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtDescontos.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgDescontos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgDescontos.Rows[e.RowIndex].Cells[10].Value.ToString() == "N")
            {
                dgDescontos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgDescontos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtDescID.Text = dtDescontos.Rows[linha]["DESC_CODIGO"].ToString();
                txtDescr.Text = dtDescontos.Rows[linha]["DESC_DESCR"].ToString();

                if (Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["CON_NOME"].ToString()) == "")
                {
                    cmbEmpres.SelectedIndex = -1;
                    txtEmpres.Text = "";
                }
                else
                {
                    cmbEmpres.Text = dtDescontos.Rows[linha]["CON_NOME"].ToString();
                    txtEmpres.Text = Convert.ToString(cmbEmpres.SelectedValue);
                }

                if (!String.IsNullOrEmpty(dtDescontos.Rows[linha]["PROD_CODIGO"].ToString()))
                {
                    txtCodBarras.Text = dtDescontos.Rows[linha]["PROD_CODIGO"].ToString();
                    LeProdutosCod();
                }
                else
                {
                    txtCodBarras.Text = "";
                    txtPDescr.Text = "";
                }

                if (Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["DEP_DESCR"].ToString()) == "")
                {
                    cmbDepto.SelectedIndex = -1;
                    txtDeptoID.Text = "";
                }
                else
                {
                    cmbDepto.Text = dtDescontos.Rows[linha]["DEP_DESCR"].ToString();
                    txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);
                }

                if (Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["CLAS_DESCR"].ToString()) == "")
                {
                    cmbClasse.SelectedIndex = -1;
                    txtClasID.Text = "";
                }
                else
                {
                    cmbClasse.Text = dtDescontos.Rows[linha]["CLAS_DESCR"].ToString();
                    txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
                }

                if (Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["SUB_DESCR"].ToString()) == "")
                {
                    cmbSubClas.SelectedIndex = -1;
                    txtSubID.Text = "";
                }
                else
                {
                    cmbSubClas.Text = dtDescontos.Rows[linha]["SUB_DESCR"].ToString();
                    txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
                }


                txtDescMin.Text = String.Format("{0:N}", dtDescontos.Rows[linha]["DESC_MIN"]);
                txtDescMax.Text = String.Format("{0:N}", dtDescontos.Rows[linha]["DESC_MAX"]);
                if (dtDescontos.Rows[linha]["DESC_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtDescontos.Rows[linha]["OPALTERACAO"].ToString());
                txtDescr.Focus();

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    #region INSERE NOVO REGISTRO
                    txtDescID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("DESCONTOS", "DESC_CODIGO", 0, "", Principal.empAtual));
                    chkLiberado.Checked = true;
                    var desc = new Desconto
                    (
                        Principal.empAtual,
                        Convert.ToInt32(txtDescID.Text),
                        txtDescr.Text,
                        cmbEmpres.SelectedIndex == -1 ? 0 : int.Parse(Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_ID", "CON_CODIGO", cmbEmpres.SelectedValue.ToString())),
                        txtCodBarras.Text.Trim() == "" ? 0 : Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true)),
                        cmbDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepto.SelectedValue),
                        cmbClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbClasse.SelectedValue),
                        cmbSubClas.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbSubClas.SelectedValue),
                        Convert.ToDouble(txtDescMin.Text),
                        Convert.ToDouble(txtDescMax.Text),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                    //VERIFICA SE DESCONTO JA ESTA CADASTRADO//
                    if (Util.RegistrosPorEmpresa("DESCONTOS", "DESC_DESCR", txtDescr.Text.ToUpper().Trim(), true, true).Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Desconto já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }

                    if (desc.InsereRegistros(desc).Equals(true))
                    {
                        Funcoes.GravaLogInclusao("DESC_CODIGO", txtDescID.Text, Principal.usuario, "DESCONTOS", txtDescr.Text, Principal.empAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtDescontos.Clear();
                        dgDescontos.DataSource = dtDescontos;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtDescr.Focus();
                        return false;
                    }
                    #endregion
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            if (dtDescontos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgDescontos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgDescontos.CurrentCell = dgDescontos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcDescontos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtDescontos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDescontos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDescontos.CurrentCell = dgDescontos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDescontos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDescontos.CurrentCell = dgDescontos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDescontos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDescontos.CurrentCell = dgDescontos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio() { }

        public void Sair()
        {
            try
            {
                dtDescontos.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDescontos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDescontos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadDescontos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, contRegistros));
            if (tcDescontos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcDescontos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtDescID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbDescontos.AutoSize = false;
                this.tsbDescontos.Image = Properties.Resources.cadastro;
                this.tsbDescontos.Size = new System.Drawing.Size(125, 20);
                this.tsbDescontos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDescontos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDescontos);
                tsbDescontos.Click += delegate
                {
                    var cadDescontos = Application.OpenForms.OfType<frmCadDescontos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadDescontos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtDescontos.Clear();
            dgDescontos.DataSource = dtDescontos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtDescMin.Text = "0,00";
            txtDescMax.Text = "0,00";
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtDescID.Text = "";
            txtDescr.Text = "";
            txtDeptoID.Text = "";
            cmbDepto.SelectedIndex = -1;
            txtClasID.Text = "";
            cmbClasse.SelectedIndex = -1;
            txtSubID.Text = "";
            cmbSubClas.SelectedIndex = -1;
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescMin.Text = "0,00";
            txtDescMax.Text = "0,00";
            txtCodBarras.Text = "";
            txtPDescr.Text = "";
            cmbEmpres.SelectedIndex = -1;
            txtEmpres.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcDescontos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public bool Excluir()
        {
            try
            {
                if ((txtDescID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do desconto?", "Exclusão tabela Descontos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var desconto = new Desconto();
                        if (!desconto.ExcluirDados(Principal.estAtual, txtDescID.Text).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("DESC_CODIGO", txtDescID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "DESCONTOS", txtDescr.Text, Principal.motivo, Principal.empAtual);
                            dtDescontos.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgDescontos.DataSource = dtDescontos;
                            tcDescontos.SelectedTab = tpGrade;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void tcDescontos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcDescontos.SelectedTab == tpGrade)
            {
                dgDescontos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcDescontos.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadDescontos");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDescontos, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = false;
                        emGrade = false;
                        txtDescMin.Text = "0,00";
                        txtDescMax.Text = "0,00";
                        txtDescr.Focus();
                    }
                }
        }

        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();
                //CARREGA EMPRESAS CONVENIADAS//
                dtPesq = Util.CarregarCombosPorEstabelecimento("CON_CODIGO", "CON_NOME", "CONVENIADAS", false, "CON_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbEmpres.DataSource = dtPesq;
                    cmbEmpres.DisplayMember = "CON_NOME";
                    cmbEmpres.ValueMember = "CON_CODIGO";
                    cmbEmpres.SelectedIndex = -1;
                }

                //CARREGA DEPARTAMENTOS//
                dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbDepto.DataSource = dtPesq;
                    cmbDepto.DisplayMember = "DEP_DESCR";
                    cmbDepto.ValueMember = "DEP_CODIGO";
                    cmbDepto.SelectedIndex = -1;
                }

                //CARREGA CLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbClasse.DataSource = dtPesq;
                    cmbClasse.DisplayMember = "CLAS_DESCR";
                    cmbClasse.ValueMember = "CLAS_CODIGO";
                    cmbClasse.SelectedIndex = -1;
                }

                //CARREGA SUBCLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("SUB_CODIGO", "SUB_DESCR", "SUBCLASSES", true, "SUB_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbSubClas.DataSource = dtPesq;
                    cmbSubClas.DisplayMember = "SUB_DESCR";
                    cmbSubClas.ValueMember = "SUB_CODIGO";
                    cmbSubClas.SelectedIndex = -1;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmCadDescontos_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    #region ATUALIZA INFORMAÇÕES DO REGISTRO
                    var desc = new Desconto
                   (
                       Principal.empAtual,
                       Convert.ToInt32(txtDescID.Text),
                       txtDescr.Text,
                       cmbEmpres.SelectedIndex == -1 ? 0 : int.Parse(Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_ID", "CON_CODIGO", cmbEmpres.SelectedValue.ToString())),
                       txtCodBarras.Text.Trim() == "" ? 0 : Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true)),
                       cmbDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepto.SelectedValue),
                       cmbClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbClasse.SelectedValue),
                       cmbSubClas.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbSubClas.SelectedValue),
                       Convert.ToDouble(txtDescMin.Text),
                       Convert.ToDouble(txtDescMax.Text),
                       chkLiberado.Checked == true ? "N" : "S",
                       DateTime.Now,
                       Principal.usuario,
                       DateTime.Now,
                       Principal.usuario
                   );

                    Principal.dtBusca = Util.RegistrosPorEmpresa("DESCONTOS", "DESC_CODIGO", txtDescID.Text, true);
                    if (desc.AtualizaDados(desc, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Descontos");
                    }
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtDescontos.Clear();
                    dgDescontos.DataSource = dtDescontos;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;

                    #endregion
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()) && Equals(cmbDepto.SelectedIndex, -1) && Equals(cmbEmpres.SelectedIndex, -1)
                && Equals(cmbClasse.SelectedIndex, -1) && Equals(cmbSubClas.SelectedIndex, -1))
            {
                Principal.mensagem = "Necessário informar ao menos um campo para a regra de desconto.";
                Funcoes.Avisa();
                cmbEmpres.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescMax.Text.Trim()))
            {
                Principal.mensagem = "Necessário informar Desconto Máximo.";
                Funcoes.Avisa();
                txtDescMax.Focus();
                return false;
            }
            if (Convert.ToDouble(txtDescMax.Text) <= 0)
            {
                Principal.mensagem = "Desconto Máximo não pode ser zero.";
                Funcoes.Avisa();
                txtDescMax.Focus();
                return false;
            }

            return true;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgDescontos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgDescontos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgDescontos.RowCount;
                    for (i = 0; i <= dgDescontos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgDescontos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgDescontos.ColumnCount; k++)
                    {
                        if (dgDescontos.Columns[k].Visible == true)
                        {
                            switch (dgDescontos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgDescontos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDescontos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgDescontos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDescontos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Desconto MIN.":
                                    numCel = Principal.GetColunaExcel(dgDescontos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDescontos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Desconto MAX.":
                                    numCel = Principal.GetColunaExcel(dgDescontos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDescontos.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Cód. de Barras":
                                    numCel = Principal.GetColunaExcel(dgDescontos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDescontos.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgDescontos.ColumnCount : indice) + Convert.ToString(dgDescontos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgDescontos.ColumnCount; i++)
                {
                    if (dgDescontos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgDescontos.ColumnCount];
                string[] coluna = new string[dgDescontos.ColumnCount];

                for (int i = 0; i < dgDescontos.ColumnCount; i++)
                {
                    grid[i] = dgDescontos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgDescontos.ColumnCount; i++)
                {
                    if (dgDescontos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgDescontos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgDescontos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgDescontos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgDescontos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgDescontos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgDescontos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgDescontos.ColumnCount; i++)
                        {
                            dgDescontos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtDescID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void AtalhoGrade()
        {
            tcDescontos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcDescontos.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
