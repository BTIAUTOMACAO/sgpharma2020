﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadPrinAtivo : Form, Botoes
    {
        private DataTable dtPrinAtivo = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbPrinAtivo = new ToolStripButton("Princípio Ativo");
        private ToolStripSeparator tssPrinAtivo = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadPrinAtivo(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadPrinAtivo_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void dgPrinAtivo_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, contRegistros));
            emGrade = true;
        }

        private void dgPrinAtivo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcPrinAtivo.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgPrinAtivo_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtPrinAtivo.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtPrinAtivo.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtPrinAtivo.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void tcPrinAtivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcPrinAtivo.SelectedTab == tpGrade)
            {
                dgPrinAtivo.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcPrinAtivo.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadPrinAtivo");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, contRegistros));
                    chkLiberado.Checked = false;
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtDescr.Focus();
                }
            }
        }


        public void Primeiro()
        {
            if (dtPrinAtivo.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgPrinAtivo.CurrentCell = dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcPrinAtivo.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtPrinAtivo.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrinAtivo.CurrentCell = dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrinAtivo.CurrentCell = dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrinAtivo.CurrentCell = dgPrinAtivo.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadPrinAtivo");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, contRegistros));
            if (tcPrinAtivo.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcPrinAtivo.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtPriID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbPrinAtivo.AutoSize = false;
                this.tsbPrinAtivo.Image = Properties.Resources.cadastro;
                this.tsbPrinAtivo.Size = new System.Drawing.Size(125, 20);
                this.tsbPrinAtivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbPrinAtivo);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssPrinAtivo);
                tsbPrinAtivo.Click += delegate
                {
                    var cadPrinAtivo = Application.OpenForms.OfType<frmCadPrinAtivo>().FirstOrDefault();
                    BotoesHabilitados();
                    cadPrinAtivo.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtPrinAtivo.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbPrinAtivo);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssPrinAtivo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtPrinAtivo.Clear();
            dgPrinAtivo.DataSource = dtPrinAtivo;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtPriID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbLiberado.SelectedIndex = 0;
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcPrinAtivo.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtPriID.Text = Funcoes.ChecaCampoVazio(dtPrinAtivo.Rows[linha]["PRI_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtPrinAtivo.Rows[linha]["PRI_DESCRICAO"].ToString());
                if (dtPrinAtivo.Rows[linha]["PRI_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtPrinAtivo.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtPrinAtivo.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPrinAtivo.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgPrinAtivo);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgPrinAtivo.RowCount;
                    for (i = 0; i <= dgPrinAtivo.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgPrinAtivo[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgPrinAtivo.ColumnCount; k++)
                    {
                        if (dgPrinAtivo.Columns[k].Visible == true)
                        {
                            switch (dgPrinAtivo.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgPrinAtivo.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPrinAtivo.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgPrinAtivo.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPrinAtivo.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgPrinAtivo.ColumnCount : indice) + Convert.ToString(dgPrinAtivo.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtPriID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do princípio ativo?", "Exclusão tabela Princípio Ativo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirPrinAtivo(txtPriID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }

                            PrincipioAtivo prinAtivo = new PrincipioAtivo();

                            if (prinAtivo.ExcluirDados(Convert.ToInt32(txtPriID.Text)))
                            {
                                Funcoes.GravaLogExclusao("PRI_CODIGO", txtPriID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PRINCIPIO_ATIVO", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtPrinAtivo.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgPrinAtivo.DataSource = dtPrinAtivo;
                                emGrade = false;
                                tcPrinAtivo.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    txtPriID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("PRINCIPIO_ATIVO", "PRI_CODIGO"));
                    chkLiberado.Checked = true;
                    DateTime dtUltimaAtualizacao = txtData.Text == "" ? DateTime.Now : Convert.ToDateTime(txtData.Text);
                    var tPrincAtivo = new PrincipioAtivo(

                        Principal.estAtual,
                        Convert.ToInt32(Math.Truncate(Convert.ToDouble(txtPriID.Text))),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        dtUltimaAtualizacao,
                        Principal.usuario.ToUpper(),
                        DateTime.Now,
                        txtUsuario.Text.ToUpper()
                    );

                    //VERIFICA SE PRINCIPIO_ATIVO JA ESTA CADASTRADO POR DESCRICAO//
                    Principal.dtPesq = Util.RegistrosPorEstabelecimento("PRINCIPIO_ATIVO", "PRI_DESCRICAO", txtDescr.Text.ToUpper());
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Princípio ativo já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else
                    {
                        if (tPrincAtivo.InserirDados(tPrincAtivo).Equals(true))
                        {
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("PRI_CODIGO", txtPriID.Text, Principal.usuario, "PRINCIPIO_ATIVO", txtDescr.Text, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtPrinAtivo.Clear();
                            dgPrinAtivo.DataSource = dtPrinAtivo;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    DateTime dtUltimaAtualizacao = txtData.Text == "" ? DateTime.Now : Convert.ToDateTime(txtData.Text);
                    var tPrincAtivo = new PrincipioAtivo(

                        Principal.estAtual,
                        Convert.ToInt32(Math.Truncate(Convert.ToDouble(txtPriID.Text))),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        dtUltimaAtualizacao,
                        Principal.usuario.ToUpper(),
                        DateTime.Now,
                        txtUsuario.Text.ToUpper()
                    );

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("PRINCIPIO_ATIVO", "PRI_CODIGO", txtPriID.Text.ToUpper());
                    //ATUALIZA PRINCIPIO ATIVO
                    if (tPrincAtivo.AtualizaDados(tPrincAtivo, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Princípio Ativo");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtPrinAtivo.Clear();
                        dgPrinAtivo.DataSource = dtPrinAtivo;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var principioAtivo = new PrincipioAtivo();
                Cursor = Cursors.WaitCursor;

                dtPrinAtivo = principioAtivo.BuscarDados(Principal.estAtual, txtBID.Text, txtBDescr.Text.Trim(), cmbLiberado.Text, out strOrdem);

                if (dtPrinAtivo.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtPrinAtivo.Rows.Count;
                    dgPrinAtivo.DataSource = dtPrinAtivo;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, 0));
                    dgPrinAtivo.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPrinAtivo.DataSource = dtPrinAtivo;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = -1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgPrinAtivo_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgPrinAtivo.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtPrinAtivo = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPrinAtivo.Columns[e.ColumnIndex].Name + " DESC");
                        dgPrinAtivo.DataSource = dtPrinAtivo;
                        decrescente = true;
                    }
                    else
                    {
                        dtPrinAtivo = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPrinAtivo.Columns[e.ColumnIndex].Name + " ASC");
                        dgPrinAtivo.DataSource = dtPrinAtivo;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPrinAtivo, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void dgPrinAtivo_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgPrinAtivo.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgPrinAtivo.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgPrinAtivo.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtPriID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgPrinAtivo.ColumnCount; i++)
                {
                    if (dgPrinAtivo.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgPrinAtivo.ColumnCount];
                string[] coluna = new string[dgPrinAtivo.ColumnCount];

                for (int i = 0; i < dgPrinAtivo.ColumnCount; i++)
                {
                    grid[i] = dgPrinAtivo.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgPrinAtivo.ColumnCount; i++)
                {
                    if (dgPrinAtivo.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgPrinAtivo.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgPrinAtivo.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgPrinAtivo.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgPrinAtivo.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgPrinAtivo.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgPrinAtivo.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgPrinAtivo.ColumnCount; i++)
                        {
                            dgPrinAtivo.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Princípio Ativo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcPrinAtivo.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcPrinAtivo.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
