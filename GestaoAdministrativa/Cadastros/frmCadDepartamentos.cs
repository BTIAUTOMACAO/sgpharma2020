﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadDepartamentos : Form, Botoes
    {
        private DataTable dtDep = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbDep = new ToolStripButton("Departamentos");
        private ToolStripSeparator tssDep = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadDepartamentos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadDepartamentos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, contRegistros));
            if (tcDep.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcDep.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtDepID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void dgDep_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, contRegistros));
            emGrade = true;
        }

        private void dgDep_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcDep.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgDep_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtDep.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtDep.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtDep.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var departamento = new Departamento();

                int codDepartamento = txtBID.Text == "" ? 0 : Convert.ToInt32(txtBID.Text);
                dtDep = departamento.BuscaDados(Convert.ToInt32(Principal.empAtual), codDepartamento, txtBDescr.Text.ToUpper().Trim(), cmbLiberado.Text, out strOrdem);

                if (dtDep.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtDep.Rows.Count;
                    dgDep.DataSource = dtDep;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, 0));
                    dgDep.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgDep.DataSource = dtDep;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtDepID.Text = Funcoes.ChecaCampoVazio(dtDep.Rows[linha]["DEP_CODIGO"].ToString());
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtDep.Rows[linha]["DEP_DESCR"].ToString());
                if (dtDep.Rows[linha]["DEP_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtDep.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtDep.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadDepartamentos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        public void Primeiro()
        {
            if (dtDep.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgDep.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgDep.CurrentCell = dgDep.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcDep.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;

            contRegistros = dtDep.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDep.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDep.CurrentCell = dgDep.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDep.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDep.CurrentCell = dgDep.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgDep.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgDep.CurrentCell = dgDep.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbDep.AutoSize = false;
                this.tsbDep.Image = Properties.Resources.cadastro;
                this.tsbDep.Size = new System.Drawing.Size(125, 20);
                this.tsbDep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDep);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDep);
                tsbDep.Click += delegate
                {
                    var cadDep = Application.OpenForms.OfType<frmCadDepartamentos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadDep.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtDep.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDep);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDep);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtDep.Clear();
            dgDep.DataSource = dtDep;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtDepID.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtDescr.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcDep.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void ImprimirRelatorio()
        {
        }

        public bool Excluir()
        {
            try
            {
                if ((txtDepID.Text.Trim() != "") && (txtDescr.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do departamento?", "Exclusão tabela Departamentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {

                        if (RegrasCadastro.ExcluirDepartamentos(txtDepID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var tdept = new Departamento();

                            if (tdept.ExcluirDados(txtDepID.Text).Equals(true))
                            {
                                MessageBox.Show("Departamentos excluido com sucesso!", "Departamentos");
                                dtDep.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgDep.DataSource = dtDep;
                                tcDep.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true)) {

                    var tdept = new Departamento(
                        Principal.estAtual,
                        Funcoes.IdentificaVerificaID("DEPARTAMENTOS", "DEP_CODIGO", 0, string.Empty),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario.ToUpper(),
                        DateTime.Now,
                        txtUsuario.Text.ToUpper()
                        );

                    if (Util.RegistrosPorEstabelecimento("DEPARTAMENTOS", "DEP_DESCR", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                    {
                        Principal.mensagem = "Departamento já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;
                    }
                    else if (tdept.InsereRegistros(tdept).Equals(true))
                    {
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtDep.Clear();
                        dgDep.DataSource = dtDep;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;

                    }
                    else {

                        MessageBox.Show("Erro: ao efetuar o cadastro", "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;
                    }
                }
                Cursor = Cursors.Default;
                return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void tcDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcDep.SelectedTab == tpGrade)
            {
                dgDep.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcDep.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadDepartamentos");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = true;
                        emGrade = false;
                        txtDescr.Focus();
                    }
                }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgDep.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgDep);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgDep.RowCount;
                    for (i = 0; i <= dgDep.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgDep[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgDep.ColumnCount; k++)
                    {
                        if (dgDep.Columns[k].Visible == true)
                        {
                            switch (dgDep.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgDep.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDep.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgDep.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgDep.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgDep.ColumnCount : indice) + Convert.ToString(dgDep.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    
                    var tdept = new Departamento(
                        Principal.estAtual,
                        Convert.ToInt32(Math.Truncate(Convert.ToDouble(txtDepID.Text))),
                        txtDescr.Text.ToUpper(),
                        chkLiberado.Checked == true ? "N" : "S",
                        DateTime.Now,
                        Principal.usuario.ToUpper(),
                        DateTime.Now,
                        txtUsuario.Text.ToUpper()
                        );

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("DEPARTAMENTOS", "DEP_CODIGO", txtDepID.Text);

                        //VERIFICA SE DEPARTAMENTO JA ESTA CADASTRADO//

                        if (tdept.AtualizaDados(tdept, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Departamentos");

                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtDep.Clear();
                            dgDep.DataSource = dtDep;
                            emGrade = false;
                            tslRegistros.Text = "";
                            LimparFicha();
                            Principal.mdiPrincipal.btnExcluir.Enabled = false;
                            Principal.mdiPrincipal.btnIncluir.Enabled = true;
                            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                            return true;
                        }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgDep_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgDep.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtDep = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDep.Columns[e.ColumnIndex].Name + " DESC");
                        dgDep.DataSource = dtDep;
                        decrescente = true;
                    }
                    else
                    {
                        dtDep = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgDep.Columns[e.ColumnIndex].Name + " ASC");
                        dgDep.DataSource = dtDep;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtDep, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void dgDep_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgDep.Rows[e.RowIndex].Cells[4].Value.ToString() == "N")
            {
                dgDep.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgDep.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtDepID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgDep.ColumnCount; i++)
                {
                    if (dgDep.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgDep.ColumnCount];
                string[] coluna = new string[dgDep.ColumnCount];

                for (int i = 0; i < dgDep.ColumnCount; i++)
                {
                    grid[i] = dgDep.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgDep.ColumnCount; i++)
                {
                    if (dgDep.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgDep.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgDep.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgDep.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgDep.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgDep.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgDep.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgDep.ColumnCount; i++)
                        {
                            dgDep.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcDep.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcDep.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
