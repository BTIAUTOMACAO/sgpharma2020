﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadOperacaoDC : Form, Botoes
    {
        private DataTable dtOperDC = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbOperDC = new ToolStripButton("Operações DC");
        private ToolStripSeparator tssOperDC = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadOperacaoDC(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadOperacaoDC_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                cmbBTipo.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtOperDC.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgOperDC.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgOperDC.CurrentCell = dgOperDC.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcOperDC.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtOperDC.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgOperDC.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgOperDC.CurrentCell = dgOperDC.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgOperDC.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgOperDC.CurrentCell = dgOperDC.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgOperDC.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgOperDC.CurrentCell = dgOperDC.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        private void dgOperDC_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, contRegistros));
            emGrade = true;
        }

        private void dgOperDC_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcOperDC.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgOperDC_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtOperDC.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtOperDC.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtOperDC.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadOperacaoDC");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, contRegistros));
            if (tcOperDC.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcOperDC.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtOperID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbOperDC.AutoSize = false;
                this.tsbOperDC.Image = Properties.Resources.cadastro;
                this.tsbOperDC.Size = new System.Drawing.Size(125, 20);
                this.tsbOperDC.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbOperDC);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssOperDC);
                tsbOperDC.Click += delegate
                {
                    var cadOperDC = Application.OpenForms.OfType<frmCadOperacaoDC>().FirstOrDefault();
                    BotoesHabilitados();
                    cadOperDC.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtOperDC.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbOperDC);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssOperDC);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtOperID.Text = Funcoes.ChecaCampoVazio(dtOperDC.Rows[linha]["ODC_NUM"].ToString().Replace(",00", ""));
                cmbTipo.Text = dtOperDC.Rows[linha]["ODC_TIPO"].ToString();
                if (dtOperDC.Rows[linha]["ODC_CLASSE"].ToString() == "CREDITO")
                {
                    chkCredito.Checked = true;
                }
                else
                    if (dtOperDC.Rows[linha]["ODC_CLASSE"].ToString() == "DEBITO")
                {
                    chkDebito.Checked = true;
                }
                else
                {
                    chkNulo.Checked = true;
                }
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtOperDC.Rows[linha]["ODC_DESCRICAO"].ToString());
                if (dtOperDC.Rows[linha]["ODC_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtOperDC.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtOperDC.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, linha));
                cmbTipo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtOperDC.Clear();
            dgOperDC.DataSource = dtOperDC;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            cmbBTipo.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtOperID.Text = "";
            cmbTipo.SelectedIndex = -1;
            chkCredito.Checked = false;
            chkDebito.Checked = false;
            chkNulo.Checked = false;
            txtDescr.Text = "";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbTipo.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcOperDC.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    chkLiberado.Checked = true;
                    OperacaoDC op = new OperacaoDC()
                    {
                        OdcID = Funcoes.IdentificaVerificaID("OPERACOES_DC", "ODC_NUM", 0, string.Empty),
                        OdcTipo = cmbTipo.Text == "BANCOS" ? "B" : "C",
                        OdcDescricao = txtDescr.Text,
                        OdcClasse = chkCredito.Checked == true ? 0 : chkDebito.Checked == true ? 1 : 2,
                        OdcLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario.ToUpper()

                    };
                    if (Util.RegistrosPorEstabelecimento("OPERACOES_DC", "ODC_DESCRICAO", txtDescr.Text.ToUpper().Trim()).Rows.Count != 0)
                    {
                        Principal.mensagem = "Operação já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Focus();
                        return false;
                    }
                    else if (op.InsereDados(op).Equals(true))
                    {
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        emGrade = false;
                        dtOperDC.Clear();
                        dgOperDC.DataSource = dtOperDC;
                        tslRegistros.Text = "";
                        Limpar();
                        cmbLiberado.SelectedIndex = 0;
                        cmbBTipo.SelectedIndex = 0;
                        return true;

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtOperID.Text.Trim() != "") && (cmbTipo.SelectedIndex != -1))
                {
                    if (MessageBox.Show("Confirma a exclusão da operação DC?", "Exclusão tabela Operação DC", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirOperDC(txtOperID.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            OperacaoDC op = new OperacaoDC();
                            if (op.ExcluiDados(Convert.ToInt32(txtOperID.Text)).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("ODC_ID", txtOperID.Text, Principal.usuario.ToUpper(), Convert.ToDateTime(Principal.data), "OPERACOES_DC", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                emGrade = false;
                                tcOperDC.SelectedTab = tpGrade;
                                LimparFicha();
                                LimparGrade();
                                cmbBTipo.SelectedIndex = 0;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;

                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {

                    OperacaoDC op = new OperacaoDC()
                    {
                        OdcID = Convert.ToInt32(txtOperID.Text),
                        OdcTipo = cmbTipo.Text == "BANCOS" ? "B" : "C",
                        OdcDescricao = txtDescr.Text,
                        OdcClasse = chkCredito.Checked == true ? 0 : chkDebito.Checked == true ? 1 : 2,
                        OdcLiberado = chkLiberado.Checked == true ? "N" : "S",
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario.ToUpper()
                    };
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("OPERACOES_DC", "ODC_NUM", txtOperID.Text);
                    if (op.AtualizaDados(op, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Operações DC");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        LimparGrade();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        cmbLiberado.SelectedIndex = 0;
                        cmbBTipo.SelectedIndex = 0;
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro ao efetuar a atualização ", "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    cmbBTipo.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBTipo.SelectedIndex == -1)
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void tcOperDC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcOperDC.SelectedTab == tpGrade)
            {
                dgOperDC.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcOperDC.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadOperacaoDC");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, contRegistros));
                    chkLiberado.Checked = false;
                    chkNulo.Checked = false;
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    cmbTipo.Focus();
                }
            }
        }

        private void cmbTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkNulo.Focus();
            }
        }

        private void chkNulo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNulo.Checked == true)
            {
                txtDescr.Focus();
                chkCredito.Checked = false;
                chkDebito.Checked = false;
            }
        }

        private void chkDebito_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDebito.Checked == true)
            {
                txtDescr.Focus();
                chkCredito.Checked = false;
                chkNulo.Checked = false;
            }
        }

        private void chkCredito_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCredito.Checked == true)
            {
                txtDescr.Focus();
                chkNulo.Checked = false;
                chkDebito.Checked = false;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgOperDC.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgOperDC);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgOperDC.RowCount;
                    for (i = 0; i <= dgOperDC.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgOperDC[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;

                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgOperDC.ColumnCount; k++)
                    {
                        if (dgOperDC.Columns[k].Visible == true)
                        {
                            switch (dgOperDC.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgOperDC.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgOperDC.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgOperDC.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgOperDC.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgOperDC.ColumnCount : indice) + Convert.ToString(dgOperDC.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbTipo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de operação.";
                Funcoes.Avisa();
                cmbTipo.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }

            return true;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                OperacaoDC op = new OperacaoDC()
                {
                    OdcID = txtBID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtBID.Text),
                    OdcTipo = cmbBTipo.Text,
                    OdcDescricao = txtBDescr.Text.Trim(),
                    OdcLiberado = cmbLiberado.Text
                };

                dtOperDC = op.BuscaDados(op);
                if (dtOperDC.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtOperDC.Rows.Count;
                    dgOperDC.DataSource = dtOperDC;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, 0));
                    dgOperDC.Focus();
                    contRegistros = 0;
                    cmbLiberado.SelectedIndex = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Operações DC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgOperDC.DataSource = dtOperDC;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbBTipo.SelectedIndex = -1;
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void dgOperDC_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgOperDC.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtOperDC = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgOperDC.Columns[e.ColumnIndex].Name + " DESC");
                        dgOperDC.DataSource = dtOperDC;
                        decrescente = true;
                    }
                    else
                    {
                        dtOperDC = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgOperDC.Columns[e.ColumnIndex].Name + " ASC");
                        dgOperDC.DataSource = dtOperDC;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtOperDC, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void dgOperDC_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgOperDC.Rows[e.RowIndex].Cells[4].Value.ToString() == "N")
            {
                dgOperDC.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgOperDC.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtOperID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgOperDC.ColumnCount; i++)
                {
                    if (dgOperDC.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgOperDC.ColumnCount];
                string[] coluna = new string[dgOperDC.ColumnCount];

                for (int i = 0; i < dgOperDC.ColumnCount; i++)
                {
                    grid[i] = dgOperDC.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgOperDC.ColumnCount; i++)
                {
                    if (dgOperDC.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgOperDC.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgOperDC.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgOperDC.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgOperDC.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgOperDC.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgOperDC.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgOperDC.ColumnCount; i++)
                        {
                            dgOperDC.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Operações", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcOperDC.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcOperDC.SelectedTab = tpFicha;
        }




        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
