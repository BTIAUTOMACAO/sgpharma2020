﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Cadastros
{
    public partial class frmCadPromocoes : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbPromocoes = new ToolStripButton("Promoções");
        private ToolStripSeparator tssPromocoes = new ToolStripSeparator();
        private DataTable dtPromocoes = new DataTable();
        private int contRegistros;
        private string strOrdem;
        private bool emGrade;
        private bool decrescente;
        #endregion

        public frmCadPromocoes(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadPromocoes_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbPeriodo.Focus();
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDeptoID.Focus();
        }

        private void txtDeptoID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbDepto.Focus();
        }

        private void cmbDepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtClasID.Focus();
        }

        private void txtClasID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbClasse.Focus();
        }

        private void cmbClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSubID.Focus();
        }

        private void txtSubID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbSubClas.Focus();
        }

        private void cmbSubClas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPreco.Focus();
        }

        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();

                //CARREGA DEPARTAMENTOS//
                dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbDepto.DataSource = dtPesq;
                    cmbDepto.DisplayMember = "DEP_DESCR";
                    cmbDepto.ValueMember = "DEP_CODIGO";
                    cmbDepto.SelectedIndex = -1;
                }

                //CARREGA CLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbClasse.DataSource = dtPesq;
                    cmbClasse.DisplayMember = "CLAS_DESCR";
                    cmbClasse.ValueMember = "CLAS_CODIGO";
                    cmbClasse.SelectedIndex = -1;
                }

                //CARREGA SUBCLASSES//
                dtPesq = Util.CarregarCombosPorEmpresa("SUB_CODIGO", "SUB_DESCR", "SUBCLASSES", true, "SUB_DESABILITADO = 'N'");
                if (dtPesq.Rows.Count != 0)
                {
                    cmbSubClas.DataSource = dtPesq;
                    cmbSubClas.DisplayMember = "SUB_DESCR";
                    cmbSubClas.ValueMember = "SUB_CODIGO";
                    cmbSubClas.SelectedIndex = -1;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmCadPromocoes_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void txtDeptoID_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Funcoes.AchaCombo(cmbDepto, txtDeptoID);
                if (cmbDepto.SelectedIndex != -1)
                    txtClasID.Focus();
            }
            else
            {
                txtDeptoID.Text = "";
                cmbDepto.SelectedIndex = -1;
            }
        }

        private void txtClasID_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                if (cmbDepto.SelectedIndex != -1)
                {
                    Funcoes.AchaCombo(cmbClasse, txtClasID);
                    if (cmbClasse.SelectedIndex != -1)
                        txtSubID.Focus();
                }
                else
                {
                    txtClasID.Text = "";
                    cmbClasse.SelectedIndex = -1;
                }
            }
            else
            {
                txtClasID.Text = "";
                cmbClasse.SelectedIndex = -1;
            }
        }

        private void txtSubID_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                if (cmbClasse.SelectedIndex != -1)
                {
                    Funcoes.AchaCombo(cmbSubClas, txtSubID);
                    if (cmbSubClas.SelectedIndex != -1)
                        txtPreco.Focus();
                }
                else
                {
                    txtSubID.Text = "";
                    cmbSubClas.SelectedIndex = -1;
                }
            }
            else
            {
                txtSubID.Text = "";
                cmbSubClas.SelectedIndex = -1;
            }
        }

        private void cmbDepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);
            }
            else
            {
                cmbDepto.SelectedIndex = -1;
            }
        }

        private void cmbClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                if (cmbDepto.SelectedIndex != -1)
                {
                    txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
                }
                else
                    cmbClasse.SelectedIndex = -1;
            }
            else
            {
                cmbClasse.SelectedIndex = -1;
            }
        }

        private void cmbSubClas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                if (cmbClasse.SelectedIndex != -1)
                {
                    txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
                }
                else
                    cmbSubClas.SelectedIndex = -1;
            }
            else
            {
                cmbSubClas.SelectedIndex = -1;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadPromocoes");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
            if (tcPromocoes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcPromocoes.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtPromoID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbPromocoes.AutoSize = false;
                this.tsbPromocoes.Image = Properties.Resources.cadastro;
                this.tsbPromocoes.Size = new System.Drawing.Size(125, 20);
                this.tsbPromocoes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbPromocoes);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssPromocoes);
                tsbPromocoes.Click += delegate
                {
                    var cadPromocoes = Application.OpenForms.OfType<frmCadPromocoes>().FirstOrDefault();
                    Funcoes.BotoesCadastro(cadPromocoes.Name);
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
                    if (tcPromocoes.SelectedTab == tpGrade)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    }
                    cadPromocoes.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtPromocoes.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbPromocoes);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssPromocoes);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPreco_Validated(object sender, EventArgs e)
        {
            if (txtPreco.Text.Trim() != "")
            {
                if (Convert.ToDouble(txtPreco.Text) != 0)
                {
                    txtPreco.Text = String.Format("{0:N}", Convert.ToDecimal(txtPreco.Text));
                    lblPrecoPromocao.Text = "Valor da Promoção: R$ " + String.Format("{0:N}", Math.Round(Convert.ToDouble(txtPreVenda.Text) - Convert.ToDouble(txtPreco.Text), 2));
                    lblPrecoPromocao.Refresh();
                    txtPorc.Text = "0,00";
                }
                else
                {
                    lblPrecoPromocao.Text = "";
                    lblPrecoPromocao.Refresh();
                    txtPreco.Text = "0,00";
                }

            }
            else
                txtPreco.Text = "0,00";
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPorc.Focus();
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbBPeriodo.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtPromoID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var promocao = new Promocao();
                dtPromocoes = promocao.BuscarDados(txtBID.Text.Trim(), Principal.empAtual, txtBDescr.Text, cmbBPeriodo.Text, cmbLiberado.Text, out strOrdem);
                if (dtPromocoes.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtPromocoes.Rows.Count;
                    dgPromocoes.DataSource = dtPromocoes;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, 0));
                    dgPromocoes.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPromocoes.DataSource = dtPromocoes;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public void CarregarDados(int linha)
        {
            try
            {

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtPromoID.Text = dtPromocoes.Rows[linha]["PROMO_CODIGO"].ToString();
                cmbPeriodo.Text = dtPromocoes.Rows[linha]["PROMO_TIPO"].ToString();
                if (cmbPeriodo.Text.Equals("DINAMICO"))
                {
                    dtInicial.Visible = true;
                    dtFinal.Visible = true;
                    txtDtIni.Visible = false;
                    txtDtFim.Visible = false;
                    txtDtIni.Text = "";
                    txtDtFim.Text = "";
                    dtInicial.Value = Convert.ToDateTime(dtPromocoes.Rows[linha]["DATA_INI"]);
                    dtFinal.Value = Convert.ToDateTime(dtPromocoes.Rows[linha]["DATA_FIM"]);
                }
                else
                {
                    dtInicial.Visible = false;
                    dtFinal.Visible = false;
                    txtDtIni.Visible = true;
                    txtDtFim.Visible = true;
                    dtInicial.Value = DateTime.Now;
                    dtFinal.Value = DateTime.Now;
                    txtDtIni.Text = dtPromocoes.Rows[linha]["DATA_INI"].ToString();
                    txtDtFim.Text = dtPromocoes.Rows[linha]["DATA_FIM"].ToString();
                }
                if (!String.IsNullOrEmpty(dtPromocoes.Rows[linha]["PROD_CODIGO"].ToString()))
                {
                    txtCodBarras.Text = dtPromocoes.Rows[linha]["PROD_CODIGO"].ToString();
                    LeProdutosCod();
                }

                if (Funcoes.ChecaCampoVazio(dtPromocoes.Rows[linha]["DEP_DESCR"].ToString()) == "")
                {
                    cmbDepto.SelectedIndex = -1;
                    txtDeptoID.Text = "";
                }
                else
                {
                    cmbDepto.Text = dtPromocoes.Rows[linha]["DEP_DESCR"].ToString();
                    txtDeptoID.Text = Convert.ToString(cmbDepto.SelectedValue);
                }

                if (Funcoes.ChecaCampoVazio(dtPromocoes.Rows[linha]["CLAS_DESCR"].ToString()) == "")
                {
                    cmbClasse.SelectedIndex = -1;
                    txtClasID.Text = "";
                }
                else
                {
                    cmbClasse.Text = dtPromocoes.Rows[linha]["CLAS_DESCR"].ToString();
                    txtClasID.Text = Convert.ToString(cmbClasse.SelectedValue);
                }

                if (Funcoes.ChecaCampoVazio(dtPromocoes.Rows[linha]["SUB_DESCR"].ToString()) == "")
                {
                    cmbSubClas.SelectedIndex = -1;
                    txtSubID.Text = "";
                }
                else
                {
                    cmbSubClas.Text = dtPromocoes.Rows[linha]["SUB_DESCR"].ToString();
                    txtSubID.Text = Convert.ToString(cmbSubClas.SelectedValue);
                }


                txtPreco.Text = String.Format("{0:N}", dtPromocoes.Rows[linha]["PROMO_PRECO"]);
                txtPorc.Text = String.Format("{0:N}", dtPromocoes.Rows[linha]["PROMO_PORC"]);

                if(Convert.ToDouble(dtPromocoes.Rows[linha]["PROMO_PRECO"]) != 0)
                {
                    lblPrecoPromocao.Text = "Valor da Promoção: R$ " + String.Format("{0:N}", Math.Round(Convert.ToDouble(txtPreVenda.Text) - Convert.ToDouble(dtPromocoes.Rows[linha]["PROMO_PRECO"]),2));
                    lblPrecoPromocao.Refresh();
                }
                else if(Convert.ToDouble(dtPromocoes.Rows[linha]["PROMO_PORC"]) != 0)
                {
                    lblPrecoPromocao.Text = "Valor da Promoção: R$ " + String.Format("{0:N}", Math.Round(Convert.ToDouble(txtPreVenda.Text) - ((Convert.ToDouble(txtPreVenda.Text) * Convert.ToDouble(dtPromocoes.Rows[linha]["PROMO_PORC"])) / 100),2));
                    lblPrecoPromocao.Refresh();
                }

                if (dtPromocoes.Rows[linha]["PROMO_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtData.Text = Funcoes.ChecaCampoVazio(dtPromocoes.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtPromocoes.Rows[linha]["OPALTERACAO"].ToString());

                if (dtPromocoes.Rows[linha]["DESCONTO_PROGRESSIVO"].ToString() == "N")
                {
                    chkDescontoProgressivo.Checked = false;
                    gbDesconto.Visible = false;
                    txtLeve.Text = "0";
                    txtPague.Text = "0";
                    txtPorcentagem.Text = "0,00";
                }
                else
                {
                    chkDescontoProgressivo.Checked = true;
                    gbDesconto.Visible = true;
                    txtLeve.Text = dtPromocoes.Rows[linha]["LEVE"].ToString();
                    txtPague.Text = dtPromocoes.Rows[linha]["PAGUE"].ToString();
                    txtPorcentagem.Text = dtPromocoes.Rows[linha]["PORCENTAGEM"].ToString();
                    lblPrecoPromocao.Text = "";
                }
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, linha));
                txtCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcPromocoes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcPromocoes.SelectedTab == tpGrade)
            {
                dgPromocoes.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcPromocoes.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadPromocoes");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        CarregarDados(contRegistros);
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
                        chkLiberado.Checked = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnLimpar.Enabled = true;
                        emGrade = false;
                        dtInicial.Value = DateTime.Now;
                        dtFinal.Value = DateTime.Now;
                        txtPreco.Text = "0,00";
                        txtCodBarras.Focus();
                    }
                }
        }

        public void ImprimirRelatorio()
        {
        }

        public void LimparGrade()
        {
            dtPromocoes.Clear();
            dgPromocoes.DataSource = dtPromocoes;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtPreco.Text = "0,00";
            txtPorc.Text = "0,00";
            txtDtFim.Visible = false;
            txtDtIni.Visible = false;
            dtInicial.Visible = false;
            chkDescontoProgressivo.Checked = false;
            txtLeve.Text = "0";
            txtPague.Text = "0";
            txtPorcentagem.Text = "0,00";
            dtFinal.Visible = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtPromoID.Text = "";
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtDeptoID.Text = "";
            cmbDepto.SelectedIndex = -1;
            txtClasID.Text = "";
            cmbClasse.SelectedIndex = -1;
            txtSubID.Text = "";
            cmbSubClas.SelectedIndex = -1;
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtPreco.Text = "0,00";
            txtPorc.Text = "0,00";
            txtDtFim.Visible = false;
            txtDtIni.Visible = false;
            dtInicial.Visible = false;
            dtFinal.Visible = false;
            txtDtIni.Text = "";
            txtDtFim.Text = "";
            lblDataFim.Visible = false;
            lblDataIni.Visible = false;
            txtCodBarras.Text = "";
            txtPDescr.Text = "";
            cmbPeriodo.SelectedIndex = -1;
            txtPreVenda.Visible = false;
            lblPreVenda.Visible = false;
            chkDescontoProgressivo.Checked = false;
            txtLeve.Text = "0";
            txtPague.Text = "0";
            txtPorcentagem.Text = "0,00";
            dtFinal.Visible = false;
            lblPrecoPromocao.Text = "";
            txtCodBarras.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcPromocoes.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();
        }

        public bool Excluir()
        {
            try
            {
                if (txtPromoID.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma a exclusão da promoção?", "Exclusão tabela Promoções", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var promocoes = new Promocao();
                        if (!promocoes.ExcluirDados(Principal.estAtual, txtPromoID.Text).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("PROMO_CODIGO", txtPromoID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "PROMOCOES", txtCodBarras.Text, Principal.motivo, Principal.empAtual);
                            dtPromocoes.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgPromocoes.DataSource = dtPromocoes;
                            tcPromocoes.SelectedTab = tpGrade;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    txtPromoID.Text = Convert.ToString(Funcoes.IdentificaVerificaID("PROMOCOES", "PROMO_CODIGO", 0, "", Principal.empAtual));
                    chkLiberado.Checked = true;

                    var promo = new Promocao(
                            Principal.empAtual,
                            Convert.ToInt32(txtPromoID.Text),
                            "PROMOCAO - " + txtPromoID.Text,
                            cmbPeriodo.SelectedIndex == 0 ? "D" : "F",
                            cmbPeriodo.SelectedIndex == 0 ? dtInicial.Value.ToString("dd/MM/yyyy") : "",
                            cmbPeriodo.SelectedIndex == 0 ? dtFinal.Value.ToString("dd/MM/yyyy") : "",
                            cmbPeriodo.SelectedIndex == 1 ? txtDtIni.Text.Trim() : "",
                            cmbPeriodo.SelectedIndex == 1 ? txtDtFim.Text.Trim() : "",
                            txtCodBarras.Text.Trim() == "" ? 0 : Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true)),
                            cmbDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepto.SelectedValue),
                            cmbClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbClasse.SelectedValue),
                            cmbSubClas.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbSubClas.SelectedValue),
                            Convert.ToDouble(txtPreco.Text),
                            Convert.ToDouble(txtPorc.Text),
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario, 
                            chkDescontoProgressivo.Checked ? "S" : "N",
                            txtLeve.Text == "" ? 0 : Convert.ToInt32(txtLeve.Text),
                            txtPague.Text == "" ? 0 : Convert.ToInt32(txtPague.Text),
                            txtPorcentagem.Text == "" ? 0 : Convert.ToDouble(txtPorcentagem.Text)
                        );

                    #region INSERE NOVO REGISTRO
                    if (promo.InsereRegistros(promo).Equals(true))
                    {
                        Funcoes.GravaLogInclusao("PROD_CODIGO", txtPromoID.Text, Principal.usuario, "PROMOCOES", txtPromoID.Text, Principal.estAtual);
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtPromocoes.Clear();
                        dgPromocoes.DataSource = dtPromocoes;
                        emGrade = false;
                        tslRegistros.Text = "";
                        Limpar();
                        return true;
                    }
                    else
                    {
                        txtCodBarras.Focus();
                        return false;
                    }
                    #endregion

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            if (dtPromocoes.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgPromocoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgPromocoes.CurrentCell = dgPromocoes.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcPromocoes.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtPromocoes.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPromocoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPromocoes.CurrentCell = dgPromocoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPromocoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPromocoes.CurrentCell = dgPromocoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPromocoes.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPromocoes.CurrentCell = dgPromocoes.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void FormularioFoco()
        {
            try
            {
                Funcoes.BotoesCadastro("frmCadPromocoes");
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
                if (tcPromocoes.SelectedTab == tpGrade)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
                else if (tcPromocoes.SelectedTab == tpFicha && contRegistros == 0)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    #region ATUALIZA INFORMAÇÕES DO REGISTRO
                    var promo = new Promocao(
                            Principal.empAtual,
                            Convert.ToInt32(txtPromoID.Text),
                            "PROMOCAO - " + txtPromoID.Text,
                            cmbPeriodo.SelectedIndex == 0 ? "D" : "F",
                            cmbPeriodo.SelectedIndex == 0 ? dtInicial.Value.ToString("dd/MM/yyyy") : "",
                            cmbPeriodo.SelectedIndex == 0 ? dtFinal.Value.ToString("dd/MM/yyyy") : "",
                            cmbPeriodo.SelectedIndex == 1 ? txtDtIni.Text.Trim() : "",
                            cmbPeriodo.SelectedIndex == 1 ? txtDtFim.Text.Trim() : "",
                            txtCodBarras.Text.Trim() == "" ? 0 : Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true)),
                            cmbDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepto.SelectedValue),
                            cmbClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbClasse.SelectedValue),
                            cmbSubClas.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbSubClas.SelectedValue),
                            Convert.ToDouble(txtPreco.Text),
                            Convert.ToDouble(txtPorc.Text),
                            chkLiberado.Checked == true ? "N" : "S",
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario,
                            chkDescontoProgressivo.Checked ? "S" : "N",
                            txtLeve.Text == "" ? 0 : Convert.ToInt32(txtLeve.Text),
                            txtPague.Text == "" ? 0 : Convert.ToInt32(txtPague.Text),
                            txtPorcentagem.Text == "" ? 0 : Convert.ToDouble(txtPorcentagem.Text)
                        );

                    Principal.dtBusca = Util.RegistrosPorEmpresa("PROMOCOES", "PROMO_CODIGO", txtPromoID.Text, true, true);
                    if (promo.AtualizaDados(promo, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Promoções");
                    }
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtPromocoes.Clear();
                    dgPromocoes.DataSource = dtPromocoes;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;
                    #endregion

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            
            if (cmbPeriodo.SelectedIndex == -1)
            {
                Principal.mensagem = "Período não pode ser em branco.";
                Funcoes.Avisa();
                cmbPeriodo.Focus();
                return false;
            }
            if (cmbPeriodo.Text.Equals("FIXO"))
            {
                if (String.IsNullOrEmpty(txtDtIni.Text.Trim()))
                {
                    Principal.mensagem = "Dia Inicial não pode ser em branco.";
                    Funcoes.Avisa();
                    txtDtIni.Focus();
                    return false;
                }
                if (String.IsNullOrEmpty(txtDtFim.Text.Trim()))
                {
                    Principal.mensagem = "Dia Final não pode ser em branco.";
                    Funcoes.Avisa();
                    txtDtFim.Focus();
                    return false;
                }
            }
            if (cmbPeriodo.Text.Equals("DINAMICO"))
            {
                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A data final deve ser maior que a data inicial";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return false;
                }
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()) && Equals(cmbDepto.SelectedIndex, -1))
            {
                Principal.mensagem = "Necessário informar um produto ou um departamento.";
                Funcoes.Avisa();
                cmbDepto.Focus();
                return false;
            }
            if (chkDescontoProgressivo.Checked)
            {
                if(String.IsNullOrEmpty(txtLeve.Text))
                {
                    Principal.mensagem = "Leve não pode ser em Branco.";
                    Funcoes.Avisa();
                    txtLeve.Focus();
                    return false;
                }
                else if (String.IsNullOrEmpty(txtPague.Text))
                {
                    Principal.mensagem = "Pague não pode ser em Branco.";
                    Funcoes.Avisa();
                    txtPague.Focus();
                    return false;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtPreco.Text.Trim()) && String.IsNullOrEmpty(txtPorc.Text.Trim()))
                {
                    Principal.mensagem = "Necessário informar Preço ou Porcentagem.";
                    Funcoes.Avisa();
                    txtPreco.Focus();
                    return false;
                }
                if (Convert.ToDouble(txtPreco.Text) <= 0 && Convert.ToDouble(txtPorc.Text) <= 0)
                {
                    Principal.mensagem = "Preço ou Porcentagem não pode ser zero.";
                    Funcoes.Avisa();
                    txtPreco.Focus();
                    return false;
                }
            }

            return true;
        }

        private void cmbPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPeriodo.SelectedIndex != -1)
            {
                if (cmbPeriodo.SelectedIndex.Equals(1))
                {
                    lblDataFim.Visible = true;
                    lblDataIni.Visible = true;
                    lblDataIni.Text = "Dia Inicial";
                    lblDataFim.Text = "Dia Final";
                    txtDtFim.Visible = true;
                    txtDtIni.Visible = true;
                    dtFinal.Visible = false;
                    dtInicial.Visible = false;
                }
                else if (cmbPeriodo.SelectedIndex.Equals(0))
                {
                    lblDataFim.Visible = true;
                    lblDataIni.Visible = true;
                    lblDataIni.Text = "Data Inicial";
                    lblDataFim.Text = "Data Final";
                    txtDtFim.Visible = false;
                    txtDtIni.Visible = false;
                    dtFinal.Visible = true;
                    dtInicial.Visible = true;
                }
            }
        }

        private void cmbPeriodo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbPeriodo.SelectedIndex.Equals(1))
                {
                    txtDtIni.Focus();
                }
                else if (cmbPeriodo.SelectedIndex.Equals(0))
                {
                    dtInicial.Focus();
                }
            }
        }

        private void txtDtIni_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDtFim.Focus();
        }

        private void txtDtFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtPDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                DataTable dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                Principal.strSql = "SELECT PROD_DESCR, C.PRE_VALOR, B.DEP_CODIGO FROM PRODUTOS A "
                    + " INNER JOIN PRODUTOS_DETALHE B ON (A.PROD_CODIGO = B.PROD_CODIGO AND A.PROD_ID = B.PROD_ID)"
                    + " INNER JOIN PRECOS C ON (A.PROD_CODIGO = C.PROD_CODIGO  AND A.PROD_ID = C.PROD_ID)"
                    + " WHERE B.EST_CODIGO = " + Principal.estAtual + " AND B.EMP_CODIGO = " + Principal.empAtual
                    + " AND C.EMP_CODIGO = B.EMP_CODIGO AND C.EST_CODIGO = B.EST_CODIGO"
                    + " AND C.TAB_CODIGO = 1 AND PROD_SITUACAO = 'A'"
                    + " AND A.PROD_CODIGO = '" + txtCodBarras.Text + "'";
                dtLePrdutos = BancoDados.selecionarRegistros(Principal.strSql);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtPDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                    txtDeptoID.Text = "";
                    cmbDepto.SelectedIndex = -1;
                    txtClasID.Text = "";
                    cmbClasse.SelectedIndex = -1;
                    txtSubID.Text = "";
                    cmbSubClas.SelectedIndex = -1;
                    txtPreVenda.Visible = true;
                    lblPreVenda.Visible = true;
                    txtPreVenda.Text = dtLePrdutos.Rows[0]["PRE_VALOR"].ToString();
                    cmbPeriodo.Focus();
                }
                else
                {
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPDescr.Text = "Produto não cadastrado";
                    txtPreVenda.Visible = false;
                    lblPreVenda.Visible = false;
                    txtCodBarras.Focus();
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtPDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDeptoID.Focus();
        }

        private void txtPDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPDescr.Text.Trim() != "")
                {
                    var produto = new Produto();
                    Principal.dtRetorno = produto.BuscaProdutosDescricaoLike(txtPDescr.Text.ToUpper());
                    if (Principal.dtRetorno.Rows.Count == 1)
                    {
                        txtCodBarras.Text = Principal.dtRetorno.Rows[0]["PROD_CODIGO"].ToString();
                        LeProdutosCod();
                    }
                    else if (Principal.dtRetorno.Rows.Count == 0)
                    {
                        MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtPDescr.Text = "";
                        txtPDescr.Focus();
                    }
                    else
                    {
                        //FORM DE BUSCA DE PRODUTOS//
                        using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                        {
                            buscaProd.BuscaProd(txtPDescr.Text.ToUpper());
                            buscaProd.ShowDialog();
                        }
                        if (!String.IsNullOrEmpty(Principal.codBarra))
                        {
                            txtCodBarras.Text = Principal.codBarra;
                            txtPDescr.Text = Principal.prodDescr;
                            LeProdutosCod();
                        }
                        else
                        {
                            txtCodBarras.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Descontos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDtIni_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtDtIni.Text.Trim()))
            {
                if (Convert.ToInt32(txtDtIni.Text.Trim()) > 31)
                {
                    MessageBox.Show("Dia não pode ser maior que 31", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    txtDtIni.Focus();
                }
            }
        }

        private void txtDtFim_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtDtFim.Text.Trim()))
            {
                if (Convert.ToInt32(txtDtFim.Text.Trim()) > 31)
                {
                    MessageBox.Show("Dia não pode ser maior que 31", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDtFim.Text = "";
                    txtDtFim.Focus();
                }
                else if (Convert.ToInt32(txtDtIni.Text.Trim()) > Convert.ToInt32(txtDtFim.Text.Trim()))
                {
                    MessageBox.Show("Dia Final não pode ser menor que Dia Inicial", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDtFim.Text = "";
                    txtDtFim.Focus();
                }
            }
        }

        private void dtFinal_Validated(object sender, EventArgs e)
        {
            if (dtInicial.Value > dtFinal.Value)
            {
                MessageBox.Show("A data final deve ser maior que a data inicial", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtFinal.Focus();
            }
        }

        private void txtPorc_Validated(object sender, EventArgs e)
        {
            if (txtPorc.Text.Trim() != "")
            {
                if (Convert.ToDouble(txtPorc.Text) != 0)
                {
                    txtPorc.Text = String.Format("{0:N}", Convert.ToDecimal(txtPorc.Text));
                    lblPrecoPromocao.Text = "Valor da Promoção: R$ " + String.Format("{0:N}", Math.Round((Convert.ToDouble(txtPreVenda.Text) * Convert.ToDouble(txtPorc.Text)) / 100, 2));
                    lblPrecoPromocao.Refresh();
                    txtPreco.Text = "0,00";
                }
                else
                {
                    lblPrecoPromocao.Text = "";
                    lblPrecoPromocao.Refresh();
                    txtPorc.Text = "0,00";
                }
            }
            else
                txtPorc.Text = "0,00";
        }

        private void txtPorc_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void cmbBPeriodo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbLiberado.Focus();
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPromocoes.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgPromocoes);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgPromocoes.RowCount;
                    for (i = 0; i <= dgPromocoes.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgPromocoes[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgPromocoes.ColumnCount; k++)
                    {
                        if (dgPromocoes.Columns[k].Visible == true)
                        {
                            switch (dgPromocoes.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgPromocoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPromocoes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgPromocoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPromocoes.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Cód. de Barras":
                                    numCel = Principal.GetColunaExcel(dgPromocoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPromocoes.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Preço":
                                    numCel = Principal.GetColunaExcel(dgPromocoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPromocoes.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Porcentagem %":
                                    numCel = Principal.GetColunaExcel(dgPromocoes.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPromocoes.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgPromocoes.ColumnCount : indice) + Convert.ToString(dgPromocoes.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgPromocoes.ColumnCount; i++)
                {
                    if (dgPromocoes.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgPromocoes.ColumnCount];
                string[] coluna = new string[dgPromocoes.ColumnCount];

                for (int i = 0; i < dgPromocoes.ColumnCount; i++)
                {
                    grid[i] = dgPromocoes.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgPromocoes.ColumnCount; i++)
                {
                    if (dgPromocoes.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgPromocoes.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgPromocoes.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgPromocoes.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgPromocoes.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgPromocoes.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgPromocoes.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgPromocoes.ColumnCount; i++)
                        {
                            dgPromocoes.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgPromocoes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
            emGrade = true;
        }

        private void dgPromocoes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcPromocoes.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgPromocoes_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgPromocoes.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtPromocoes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPromocoes.Columns[e.ColumnIndex].Name + " DESC");
                        dgPromocoes.DataSource = dtPromocoes;
                        decrescente = true;
                    }
                    else
                    {
                        dtPromocoes = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPromocoes.Columns[e.ColumnIndex].Name + " ASC");
                        dgPromocoes.DataSource = dtPromocoes;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPromocoes, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Promoções", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgPromocoes_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtPromocoes.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtPromocoes.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtPromocoes.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void dgPromocoes_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgPromocoes.Rows[e.RowIndex].Cells[12].Value.ToString() == "N")
            {
                dgPromocoes.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgPromocoes.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        public void AtalhoGrade()
        {
            tcPromocoes.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcPromocoes.SelectedTab = tpFicha;
        }




        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtValorPorcentagem_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPorc.Focus();
        }

        private void txtValorPorcentagem_Validated(object sender, EventArgs e)
        {
            if (txtValorPorcentagem.Text.Trim() != "")
            {
                if (Convert.ToDouble(txtValorPorcentagem.Text) != 0)
                {
                    txtPorc.Text = String.Format("{0:N}", 100 - ((Convert.ToDecimal(txtValorPorcentagem.Text) * 100) / Convert.ToDecimal(txtPreVenda.Text)));
                    lblPrecoPromocao.Text = "Valor da Promoção: R$ " + String.Format("{0:N}", Convert.ToDecimal(txtValorPorcentagem.Text));
                    lblPrecoPromocao.Refresh();
                    txtValorPorcentagem.Text = "0,00";
                }
                else
                {
                    lblPrecoPromocao.Text = "";
                    lblPrecoPromocao.Refresh();
                    txtPorc.Text = "0,00";
                }
            }
            else
                txtPorc.Text = "0,00";
        }

        private void chkDescontoProgressivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDescontoProgressivo.Checked)
            {
                if (String.IsNullOrEmpty(txtCodBarras.Text))
                {
                    MessageBox.Show("Informe o produto para o Desconto Progressivo", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chkDescontoProgressivo.Checked = false;
                    txtCodBarras.Focus();
                }
                else
                {
                    txtPreco.Text = "0,00";
                    txtPreco.Enabled = false;
                    txtPorc.Enabled = false;
                    txtPorc.Text = "0,00";
                    gbDesconto.Visible = true;
                    txtLeve.Focus();
                }
            }
            else
            {
                txtPreco.Enabled = true;
                txtPorc.Enabled = true;
                txtLeve.Text = "0";
                txtPague.Text = "0";
                txtPorcentagem.Text = "0,00";
                gbDesconto.Visible = false;
            }
            
        }

        private void txtLeve_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if(String.IsNullOrEmpty(txtLeve.Text))
                {
                    MessageBox.Show("Leve não pode ser em Branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLeve.Focus();
                }else if(Convert.ToInt32(txtLeve.Text) == 0)
                {
                    MessageBox.Show("Leve não pode ser Zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLeve.Focus();
                }
                else
                    txtPague.Focus();
            }
        }

        private void txtPague_Validated(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtPague.Text))
            {
                double valorVenda = Convert.ToDouble(txtPreVenda.Text) * Convert.ToInt32(txtLeve.Text);
                double valoDesconto = Convert.ToDouble(txtPreVenda.Text) * Convert.ToInt32(txtPague.Text);
                double desconto = valorVenda - valoDesconto;
                double procentagem = (desconto * 100) / valorVenda;
                txtPorcentagem.Text = String.Format("{0:N}", procentagem);
            }
        }

        private void txtPague_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtPague.Text))
                {
                    MessageBox.Show("Pague não pode ser em Branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPague.Focus();
                }
                else if (Convert.ToInt32(txtPague.Text) == 0)
                {
                    MessageBox.Show("Pague não pode ser Zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPague.Focus();
                }
                else
                    txtPorcentagem.Focus();
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
