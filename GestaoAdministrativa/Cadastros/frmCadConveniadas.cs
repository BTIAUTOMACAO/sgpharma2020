﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Cadastros
{
    /// <summary>
    /// Cadastro de Empresas Conveniadas
    /// </summary>
    public partial class frmCadConveniadas : Form, Botoes
    {
        private DataTable dtConveniadas = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbConveniadas = new ToolStripButton("Conveniadas");
        private ToolStripSeparator tssConveniadas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmCadConveniadas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCadConveniadas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                cmbBConvenio.SelectedIndex = 0;

                Principal.dtPesq = Util.CarregarCombosPorEmpresa("FORMA_ID", "FORMA_DESCRICAO", "FORMAS_PAGAMENTO", true, "FORMA_LIBERADO = 'S'");
                if (Principal.dtPesq.Rows.Count > 0)
                {
                    cmbForma.DataSource = Principal.dtPesq;
                    cmbForma.DisplayMember = "FORMA_DESCRICAO";
                    cmbForma.ValueMember = "FORMA_ID";
                    cmbForma.SelectedIndex = -1;
                }

                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtConCod_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if(String.IsNullOrEmpty(txtEndereco.Text))
                {
                    txtCep.Focus();
                }
                else 
                    txtEndereco.Focus();
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadConveniadas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, contRegistros));
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcConveniadas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtConID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBairro.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {
                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = end.bairro.ToUpper();
                        txtCidade.Text = end.localidade.ToUpper();
                        cmbUF.Text = end.uf;
                        Cursor = Cursors.Default;
                        txtFechamento.Focus();
                    }
                    else
                        txtEndereco.Focus();

                    Cursor = Cursors.Default;
                }
            }
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFechamento.Focus();
        }

        private void txtFechamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVencimento.Focus();
        }

        private void txtVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDesconto.Focus();
        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbConvenio.Focus();
        }

        private void cmbConvenio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbConvenio.SelectedIndex != -1)
                {
                    if (cmbConvenio.Text == "PARTICULAR")
                    {
                        lblForma.Visible = true;
                        cmbForma.Visible = true;
                    }
                }
                else
                {
                    lblForma.Visible = false;
                    cmbForma.Visible = false;
                    chkLiberado.Focus();
                }
            }
        }

        private void dgConveniadas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, contRegistros));
            emGrade = true;
        }

        private void dgConveniadas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcConveniadas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgConveniadas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgConveniadas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtConveniadas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgConveniadas.Columns[e.ColumnIndex].Name + " DESC");
                        dgConveniadas.DataSource = dtConveniadas;
                        decrescente = true;
                    }
                    else
                    {
                        dtConveniadas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgConveniadas.Columns[e.ColumnIndex].Name + " ASC");
                        dgConveniadas.DataSource = dtConveniadas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgConveniadas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtConveniadas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtConveniadas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtConveniadas.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }


        public void Primeiro()
        {
            if (dtConveniadas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgConveniadas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgConveniadas.CurrentCell = dgConveniadas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = dtConveniadas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgConveniadas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgConveniadas.CurrentCell = dgConveniadas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgConveniadas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgConveniadas.CurrentCell = dgConveniadas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgConveniadas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgConveniadas.CurrentCell = dgConveniadas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbConveniadas.AutoSize = false;
                this.tsbConveniadas.Image = Properties.Resources.cadastro;
                this.tsbConveniadas.Size = new System.Drawing.Size(125, 20);
                this.tsbConveniadas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbConveniadas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssConveniadas);
                tsbConveniadas.Click += delegate
                {
                    var cadConveniadas = Application.OpenForms.OfType<frmCadConveniadas>().FirstOrDefault();
                    BotoesHabilitados();
                    cadConveniadas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtConID.Text = dtConveniadas.Rows[linha]["CON_ID"].ToString();
                txtConCod.Text = dtConveniadas.Rows[linha]["CON_CODIGO"].ToString();
                txtNome.Text = dtConveniadas.Rows[linha]["CON_NOME"].ToString();
                txtEndereco.Text = dtConveniadas.Rows[linha]["CON_ENDERECO"].ToString();
                txtBairro.Text = Funcoes.ChecaCampoVazio(dtConveniadas.Rows[linha]["CON_BAIRRO"].ToString());
                txtCidade.Text = dtConveniadas.Rows[linha]["CON_CIDADE"].ToString();
                txtCep.Text = dtConveniadas.Rows[linha]["CON_CEP"].ToString();
                cmbUF.Text = dtConveniadas.Rows[linha]["CON_UF"].ToString();
                txtFechamento.Text = Funcoes.ChecaCampoVazio(dtConveniadas.Rows[linha]["CON_FECHAMENTO"].ToString());
                txtVencimento.Text = Funcoes.ChecaCampoVazio(dtConveniadas.Rows[linha]["CON_VENCIMENTO"].ToString());
                txtDesconto.Text = String.Format("{0:N}", dtConveniadas.Rows[linha]["CON_DESCONTO"]);
                cmbConvenio.Text = Convert.ToInt32(dtConveniadas.Rows[linha]["CON_WEB"]) == 1 ? "CONVENIO DROGABELLA/REDE PLANTAO" :
                    Convert.ToInt32(dtConveniadas.Rows[linha]["CON_WEB"]) == 0 ? "NORMAL" : "PARTICULAR";
                if (dtConveniadas.Rows[linha]["CON_DESABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;


                if (cmbConvenio.Text.Equals("PARTICULAR"))
                {
                    lblForma.Visible = true;
                    cmbForma.Visible = true;
                    if (dtConveniadas.Rows[linha]["CON_FORMA_ID"].ToString() == "")
                    {
                        cmbForma.SelectedIndex = -1;
                    }
                    else
                    {
                        cmbForma.SelectedValue = dtConveniadas.Rows[linha]["CON_FORMA_ID"];
                    }
                }
                else if (cmbConvenio.Text.Equals("CONVENIO DROGABELLA/REDE PLANTAO"))
                {
                    lblForma.Visible = false;
                    cmbForma.Visible = false;
                    chkParcelado.Visible = true;
                    chkParcelado.Checked = dtConveniadas.Rows[linha]["CON_PARCELA"].ToString() == "S" ? true : false;
                }
                else
                {
                    chkParcelado.Visible = false;
                    lblForma.Visible = false;
                    cmbForma.Visible = false;
                }

                if (dtConveniadas.Rows[linha]["CON_OBRIGA_REGRA_DESCONTO"].ToString() == "N")
                {
                    chkObrigaDescontoDaRegra.Checked = false;
                }
                else
                    chkObrigaDescontoDaRegra.Checked = true;


                txtData.Text = Funcoes.ChecaCampoVazio(dtConveniadas.Rows[linha]["CON_DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtConveniadas.Rows[linha]["CON_OP_CADASTRO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, linha));
                txtConCod.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtConveniadas.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbConveniadas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssConveniadas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtConveniadas.Clear();
            dgConveniadas.DataSource = dtConveniadas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            cmbLiberado.SelectedIndex = 0;
            cmbBConvenio.SelectedIndex = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtConID.Text = "";
            txtConCod.Text = "";
            txtNome.Text = "";
            txtEndereco.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtCep.Text = "";
            cmbUF.SelectedIndex = -1;
            txtFechamento.Text = "0";
            txtVencimento.Text = "0";
            txtDesconto.Text = "0";
            cmbConvenio.SelectedIndex = -1;
            lblForma.Visible = false;
            cmbForma.Visible = false;
            chkParcelado.Visible = false;
            chkLiberado.Checked = true;
            chkObrigaDescontoDaRegra.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtConCod.Focus();

        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }


        public void ImprimirRelatorio()
        {
        }

        public bool Excluir()
        {
            try
            {
                if ((txtConID.Text.Trim() != "") && (txtConCod.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão da empresa conveniada?", "Exclusão tabela Convenidas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirConveniadas(txtConCod.Text).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var conve = new Conveniada();
                            if (conve.ExcluiDados(Convert.ToInt32(txtConID.Text)).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("CON_ID", txtConID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "CONVENIADAS", txtConCod.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Convenidas excluido com sucesso!", "Convenidas");
                                dtConveniadas.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgConveniadas.DataSource = dtConveniadas;
                                emGrade = false;
                                tcConveniadas.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBID.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                cmbBConvenio.SelectedIndex = 0;
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Convenidas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var empConv = new Conveniada();
                    Conveniada empConveniadas = new Conveniada
                    {
                        ConID = int.Parse(txtConID.Text),
                        ConCodigo = int.Parse(txtConCod.Text),
                        ConNome = txtNome.Text.ToUpper().Trim(),
                        ConEndereco = txtEndereco.Text.ToUpper().Trim(),
                        ConBairro = txtBairro.Text.Trim(),
                        ConCidade = txtCidade.Text.Trim(),
                        ConCep = Funcoes.RemoveCaracter(txtCep.Text),
                        ConUf = cmbUF.Text,
                        ConFechamento = int.Parse(txtFechamento.Text.Trim()),
                        ConVencimento = int.Parse(txtVencimento.Text.Trim()),
                        ConDesconto = Convert.ToDecimal(txtDesconto.Text),
                        ConRegras = "N",
                        ConLiberado = chkLiberado.Checked ? "N" : "S",
                        ConWeb = cmbConvenio.Text == "CONVENIO DROGABELLA/REDE PLANTAO" ? 1 : cmbConvenio.Text == "NORMAL" ? 0 : 4,
                        OpAlteracao = Principal.usuario,
                        ConFormaID = cmbConvenio.Text == "PARTICULAR" ? Convert.ToInt32(cmbForma.SelectedValue) : 0,
                        DtAlteracao = DateTime.Now,
                        ConParcela = chkParcelado.Checked ? "S" : "N",
                        ConObrigaRegraDesconto = chkObrigaDescontoDaRegra.Checked ? "S" : "N"
                    };
                    
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("CONVENIADAS", "CON_NOME", txtNome.Text);

                    if (empConv.AtualizaDados(empConveniadas, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Conveniadas");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtConveniadas.Clear();
                        dgConveniadas.DataSource = dtConveniadas;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro ao tentar afetuar a atualização", "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";


            if (String.IsNullOrEmpty(txtConCod.Text.Trim()))
            {
                Principal.mensagem = "Código não pode ser zero.";
                Funcoes.Avisa();
                txtConCod.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCep.Text)))
            {
                Principal.mensagem = "Cep não pode ser em branco.";
                Funcoes.Avisa();
                txtCep.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (Equals(cmbConvenio.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um sistema de convênio.";
                Funcoes.Avisa();
                cmbConvenio.Focus();
                return false;
            }

            return true;
        }

        private void tcConveniadas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                dgConveniadas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcConveniadas.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadConveniadas");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;

                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, contRegistros));
                        chkLiberado.Checked = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        emGrade = false;
                        txtConCod.Focus();
                    }
                }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtPesq = Util.RegistrosPorEstabelecimento("CONVENIADAS", "CON_NOME", txtNome.Text);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Nome já cadastrado.";
                        Funcoes.Avisa();
                        txtNome.Focus();
                        return false;
                    }
                    else
                    {
                        Principal.dtPesq = Util.RegistrosPorEstabelecimento("CONVENIADAS", "CON_CODIGO", txtConCod.Text);
                        if (Principal.dtPesq.Rows.Count == 0)
                        {
                            chkLiberado.Checked = true;
                            var conv = new Conveniada();
                            Conveniada empConveniadas = new Conveniada
                            {
                                ConID = Funcoes.IdentificaVerificaID("CONVENIADAS", "CON_ID"),
                                ConCodigo = int.Parse(txtConCod.Text),
                                ConNome = txtNome.Text.ToUpper().Trim(),
                                ConEndereco = txtEndereco.Text.ToUpper().Trim(),
                                ConBairro = txtBairro.Text.Trim(),
                                ConCidade = txtCidade.Text.Trim(),
                                ConCep = Funcoes.RemoveCaracter(txtCep.Text),
                                ConUf = cmbUF.Text,
                                ConFechamento = txtFechamento.Text == "" ? 0 : int.Parse(txtFechamento.Text.Trim()),
                                ConVencimento = txtVencimento.Text == "" ? 0 : int.Parse(txtVencimento.Text.Trim()),
                                ConDesconto = txtDesconto.Text == "" ? 0 : Convert.ToDecimal(txtDesconto.Text),
                                ConRegras = "N",
                                ConLiberado = chkLiberado.Checked ? "N" : "S",
                                ConWeb = cmbConvenio.Text == "CONVENIO DROGABELLA/REDE PLANTAO" ? 1 : cmbConvenio.Text == "NORMAL" ? 0 : 4,
                                ConFormaID = cmbConvenio.Text == "PARTICULAR" ? Convert.ToInt32(cmbForma.SelectedValue) : 0,
                                ConParcela = chkParcelado.Checked ? "S" : "N",
                                ConObrigaRegraDesconto = chkObrigaDescontoDaRegra.Checked ? "S" : "N",
                                DtCadastro = DateTime.Now,
                                OpCadastro = Principal.usuario,
                            };
                            if (conv.InsereDados(empConveniadas).Equals(true))
                            {
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dtConveniadas.Clear();
                                dgConveniadas.DataSource = dtConveniadas;
                                emGrade = false;
                                tslRegistros.Text = "";
                                LimparFicha();
                                return true;

                            }
                            else
                            {

                                MessageBox.Show("Erro ao tentar afetuar o Cadastro", "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;

                            }
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Código já cadastrada.";
                            Funcoes.Avisa();
                            txtNome.Focus();
                            return false;

                        }

                    }
                }
                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    txtBCidade.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCidade.Text.Trim()))
                {
                    cmbBConvenio.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBConvenio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBConvenio.SelectedIndex == -1)
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }

        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var empConv = new Conveniada();
                int conWeb = 0;

                if (cmbBConvenio.Text != "TODOS")
                {
                    if (cmbBConvenio.Text == "CONV. DROGABELLA/REDE PLANTAO")
                    {
                        conWeb = 1;
                    }
                    else
                        conWeb = 4;
                }

                Conveniada empConveniadas = new Conveniada
                {
                    ConCodigo = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    ConNome = txtBNome.Text.Trim(),
                    ConCidade = txtBCidade.Text.Trim(),
                    ConLiberado = cmbLiberado.Text,
                    ConWeb = conWeb
                };

                dtConveniadas = empConv.BuscaDados(empConveniadas, out strOrdem);
                if (dtConveniadas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtConveniadas.Rows.Count;
                    dgConveniadas.DataSource = dtConveniadas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtConveniadas, 0));
                    dgConveniadas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgConveniadas.DataSource = dtConveniadas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    emGrade = false;
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                    cmbBConvenio.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgConveniadas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgConveniadas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }
                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgConveniadas.RowCount;
                    for (i = 0; i <= dgConveniadas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgConveniadas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgConveniadas.ColumnCount; k++)
                    {
                        if (dgConveniadas.Columns[k].Visible == true)
                        {
                            switch (dgConveniadas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgConveniadas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgConveniadas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgConveniadas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgConveniadas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "% Desconto":
                                    numCel = Principal.GetColunaExcel(dgConveniadas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgConveniadas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgConveniadas.ColumnCount : indice) + Convert.ToString(dgConveniadas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void dgConveniadas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgConveniadas.Rows[e.RowIndex].Cells[7].Value.ToString() == "N")
            {
                dgConveniadas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgConveniadas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void txtDesconto_Validated(object sender, EventArgs e)
        {
            if (txtDesconto.Text.Trim() != "")
            {
                txtDesconto.Text = String.Format("{0:N}", Convert.ToDecimal(txtDesconto.Text));
            }
            else
                txtDesconto.Text = "0,00";
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtConID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgConveniadas.ColumnCount; i++)
                {
                    if (dgConveniadas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgConveniadas.ColumnCount];
                string[] coluna = new string[dgConveniadas.ColumnCount];

                for (int i = 0; i < dgConveniadas.ColumnCount; i++)
                {
                    grid[i] = dgConveniadas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgConveniadas.ColumnCount; i++)
                {
                    if (dgConveniadas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgConveniadas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgConveniadas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgConveniadas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgConveniadas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgConveniadas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgConveniadas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgConveniadas.ColumnCount; i++)
                        {
                            dgConveniadas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcConveniadas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcConveniadas.SelectedTab = tpFicha;
        }

        private void txtFechamento_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFechamento.Text.Trim()))
            {
                txtFechamento.Text = "0";
            }
        }

        private void txtVencimento_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtVencimento.Text.Trim()))
            {
                txtVencimento.Text = "0";
            }
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        
        private void txtConCod_DoubleClick(object sender, EventArgs e)
        {
            txtConCod.Text = Funcoes.IdentificaVerificaID("CONVENIADAS", "CON_CODIGO").ToString();
        }

        private void cmbForma_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void cmbConvenio_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbConvenio.SelectedIndex != -1)
            {
                if (cmbConvenio.Text == "PARTICULAR")
                {
                    chkParcelado.Visible = false;
                    lblForma.Visible = true;
                    cmbForma.Visible = true;
                }
                else if (cmbConvenio.Text == "CONVENIO DROGABELLA/REDE PLANTAO")
                {
                    lblForma.Visible = false;
                    cmbForma.Visible = false;
                    chkParcelado.Visible = true;
                }
                else
                {
                    lblForma.Visible = false;
                    cmbForma.Visible = false;
                    chkParcelado.Visible = false;
                }
            }
        }
    }
}
