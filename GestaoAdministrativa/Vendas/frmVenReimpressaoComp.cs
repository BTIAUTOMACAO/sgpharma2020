﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using System.IO;
using GestaoAdministrativa.Impressao;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenReimpressaoComp : Form
    {
        private string comprovanteVenda;
        private int indice;
        private string xmlSAT;
        private double totalImpFederal;
        private double totalImpEstadual;
        private double totalImpMunicipal;
        private string vendaNumeroNota;
        DataTable tProdutos = new DataTable();
        DataTable dadosVenda = new DataTable();

        public frmVenReimpressaoComp()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenReimpressaoComp_Load(object sender, EventArgs e)
        {
            try
            {
                indice = 0;
                dtDataVenda.Value = DateTime.Now;
                rdbComprovante.Checked = true;
                lblRegistros.Text = "";
                BuscaVendas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpr. Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {

            Funcoes.LimpaFormularios(this);
            dtDataVenda.Value = DateTime.Now;
            rdbComprovante.Checked = true;
            lblRegistros.Text = "";
        }


        private void frmVenReimpressaoComp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscaVendas();
        }

        public void BuscaVendas()
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                Comprovante comp = new Comprovante();

                DateTime dtInicial = Convert.ToDateTime(dtDataVenda.Text + " 00:00:00");
                DateTime dtFinal = Convert.ToDateTime(dtDataVenda.Text + " 23:59:59");
                DataTable dtComp = comp.BuscaComprovantePeriodo(dtInicial, dtFinal, txtNome.Text);
                if (dtComp.Rows.Count > 0)
                {
                    lblRegistros.Text = "Registros encontrados: " + dtComp.Rows.Count;
                    dgReimpressao.DataSource = dtComp;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtComp, 0));
                    dgReimpressao.Focus();

                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado!", "Reimpr. Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReimpressao.DataSource = dtComp;
                    lblRegistros.Text = "Registros encontrados: " + dtComp.Rows.Count;
                    dtDataVenda.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpr. Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void dgReimpressao_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void dgReimpressao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                frmVenComprovante venComp = new frmVenComprovante(Convert.ToInt64(dgReimpressao["VENDA_ID", e.RowIndex].Value));
                venComp.ShowDialog();
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Confirma a Reimpressão do Comprovante?", "Reimpr.Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (rdbComprovante.Checked)
                    {
                        ComprovanteVenda(Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value), Principal.empAtual, Principal.estAtual, Convert.ToDateTime(dgReimpressao.Rows[indice].Cells[2].Value));
                    }
                    else if (rdbFiscal.Checked)
                    {
                        if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_EMITIDO", "VENDA_ID", dgReimpressao.Rows[indice].Cells[1].Value.ToString(), false, true).Equals("S"))
                        {
                            if (MessageBox.Show("Cupom Fiscal já emitido! Deseja emitir novamente?", "Reimpr.Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                            {
                                return;
                            }
                        }
                        var dadosDaVenda = new VendasDados();

                        if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            if(SATCupomFiscal(Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value)))
                            {
                                dadosDaVenda.AtualizarInformacoesFiscais(Principal.estAtual, Principal.empAtual, Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value), vendaNumeroNota);
                            }
                        }
                        else if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
                        {
                            if (!ImprimirCupomFiscal(Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value)))
                            {
                                Imprimir.ImpFiscalUltCupom();
                            }
                            else
                            {
                                dadosDaVenda.AtualizarInformacoesFiscais(Principal.estAtual, Principal.empAtual, Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value), vendaNumeroNota);
                            }
                        }
                    }
                }
                else
                    dgReimpressao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpr. Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ComprovanteVenda(long vendaID, int empCodigo, int estCodigo, DateTime dataVenda)
        {
            try
            {
                double somaTotal = 0;
                double desconto = 0;
                Cursor = Cursors.WaitCursor;
                string tamanho;
                var dadosEstabelecimento = new Estabelecimento();
                List<Cliente> dadosVendaCliente = new List<Cliente>();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(estCodigo, empCodigo);
                if (retornoEstab.Count > 0)
                {
                    var vendaFormaPagto = new VendasFormaPagamento();
                    DataTable dtRetorno = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(empCodigo, estCodigo, vendaID);

                    int qtde = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtRetorno.Rows[0]["QTDE_VIAS"]);

                    for (int y = 1; y <= qtde; y++)
                    {
                        comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                        comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                        comprovanteVenda += "N. VENDA: " + vendaID + "\n";
                        comprovanteVenda += "Data: " + Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dataVenda).ToString("HH:mm:ss") + "\n";
                        comprovanteVenda += "_________________________________________" + "\n";
                        comprovanteVenda += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VLITEM R$" + "\n";
                        comprovanteVenda += "_________________________________________" + "\n";

                        var itensVendas = new VendasItens();

                        tProdutos = itensVendas.BuscaItensDaVendaPorID(vendaID, estCodigo, empCodigo);
                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]), 2) * 100);
                            comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[i]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                comprovanteVenda += "    R$ " + tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";

                            double pmc = 0;

                            string valorPmC = BancoDados.selecionarRegistro("SELECT COALESCE(med_pco18,0) AS med_pco18 FROM PRECOS WHERE PROD_CODIGO = '" + tProdutos.Rows[i]["PROD_CODIGO"].ToString() + "'"
                                  + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual, "med_pco18");

                            if(!String.IsNullOrEmpty(valorPmC))
                            {
                                pmc = Convert.ToDouble(valorPmC);
                            }

                            double diferenca = Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) - pmc;

                            if (diferenca < 0)
                            {
                                somaTotal = somaTotal + Math.Abs(diferenca);
                            }
                        }

                        desconto = Math.Abs(Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")));
                        comprovanteVenda += "=========================================" + "\n";

                        dadosVenda = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", vendaID.ToString(), true);

                        var colaborador = new Colaborador();
                        comprovanteVenda += "VENDEDOR.: " + colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), estCodigo) + "\n";
                        comprovanteVenda += "SUBTOTAL.: R$ " + String.Format("{0:N}", dadosVenda.Rows[0]["VENDA_SUBTOTAL"]) + "\n";

                        if (Math.Abs(Convert.ToDouble(desconto)) > 0 || Math.Abs(Convert.ToDouble(dadosVenda.Rows[0]["VENDA_DIFERENCA"])) != 0)
                        {
                            comprovanteVenda += "DESCONTO.: R$ " + String.Format("{0:N}", Math.Abs(desconto)) + "   AJUSTE.: R$ " + String.Format("{0:N}", Math.Abs(Convert.ToDouble(dadosVenda.Rows[0]["VENDA_DIFERENCA"]))) + "\n";
                        }
                        comprovanteVenda += "TOTAL....: R$ " + String.Format("{0:N}", dadosVenda.Rows[0]["VENDA_TOTAL"]) + "\n";

                        desconto = 0;
                        desconto = Convert.ToDouble(dadosVenda.Rows[0]["VENDA_SUBTOTAL"]) - Convert.ToDouble(dadosVenda.Rows[0]["VENDA_TOTAL"]);
                    
                        var descricaoForma = new FormasPagamento();
                        comprovanteVenda += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO", 43) + "\n";
                        dtRetorno = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(empCodigo, estCodigo, vendaID);
                        for (int x = 0; x < dtRetorno.Rows.Count; x++)
                        {
                            comprovanteVenda += descricaoForma.IdentificaFormaDePagamento(Convert.ToInt32(dtRetorno.Rows[x]["VENDA_FORMA_ID"]), Convert.ToInt32(dtRetorno.Rows[x]["EMP_CODIGO"]))
                                + ".: R$ " + Funcoes.BValor(Convert.ToDouble(dtRetorno.Rows[x]["VENDA_VALOR_PARCELA"])) + "\n";
                        }

                        var vendaEspecie = new VendasEspecies();
                        dtRetorno = vendaEspecie.BuscaEspeciesPorVendaID(empCodigo, estCodigo, vendaID);
                        
                        comprovanteVenda += Funcoes.CentralizaTexto("ESPECIE(S)", 43) + "\n";

                        for (int a = 0; a < dtRetorno.Rows.Count; a++)
                        {
                            comprovanteVenda += Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_DESCRICAO", "ESP_CODIGO", dtRetorno.Rows[a]["ESP_CODIGO"].ToString()) + ".: R$ " + String.Format("{0:N}", dtRetorno.Rows[a]["VENDA_ESPECIE_VALOR"]) + "\n";
                        }
                        comprovanteVenda += "_________________________________________" + "\n";

                        var dCliente = new Cliente();
                        dadosVendaCliente = dCliente.DadosClienteCfDocto(Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_DOCTO", "CF_ID", dadosVenda.Rows[0]["VENDA_CF_ID"].ToString()));
                        if (dadosVendaCliente.Count > 0)
                        {
                            comprovanteVenda += dadosVendaCliente[0].CfCodigo == "" ? "00" : dadosVendaCliente[0].CfCodigo;
                            comprovanteVenda += " - " + dadosVendaCliente[0].CfNome + "\n";
                            comprovanteVenda += dadosVendaCliente[0].CfEndereco + "," + dadosVendaCliente[0].CfNumeroEndereco + "\n";
                            comprovanteVenda += dadosVendaCliente[0].CfCidade + "/" + dadosVendaCliente[0].CfUF + "\n";
                            comprovanteVenda += "CPF.:" + dadosVendaCliente[0].CfDocto + "\n";
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString()) && dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() != "0")
                            {
                                comprovanteVenda += "EMPRESA: " + dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - "
                                    + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString()) + "\n";
                            }
                            comprovanteVenda += "_________________________________________" + "\n";
                        }

                        if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO"))
                        {
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().PadLeft(38 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().Length, ' ') + "\n";
                            }
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao com Receita:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().PadLeft(26 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().Length, ' ') + "\n";
                            }
                            comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                            comprovanteVenda += "Cartao:                  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().PadLeft(33 - dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length, ' ')) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).PadLeft(32 - (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length, ' ')) + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_CONVENIO_PARCELA"].ToString()))
                            {
                                if (!Convert.ToInt32(dadosVenda.Rows[0]["VENDA_CONVENIO_PARCELA"]).Equals("1"))
                                    comprovanteVenda += "N° Parcelas: " + dadosVenda.Rows[0]["VENDA_CONVENIO_PARCELA"] + "\n";
                            }
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_MEDICO"].ToString()))
                            {
                                comprovanteVenda += "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"];
                            }

                            comprovanteVenda += "\n";

                            comprovanteVenda += "_________________________________________" + "\n";
                        }
                        else if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("FUNCIONAL"))
                        {
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString() + "\n";
                            }
                            comprovanteVenda += "Administradora:  FUNCIONAL CARD" + "\n";
                            comprovanteVenda += "Cartao:  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString()) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).ToString()) + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_MEDICO"].ToString()))
                            {
                                comprovanteVenda += "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"];
                            }

                            comprovanteVenda += "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                        }

                        comprovanteVenda += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                        if (desconto > 0)
                        {
                            comprovanteVenda += "VOCE ECONOMIZOU R$ " + String.Format("{0:N}", desconto) + "\n";
                        }

                        if (Funcoes.LeParametro(6, "378", false).Equals("S"))
                        {
                            if (somaTotal > 0)
                            {
                                comprovanteVenda += "ECONONIA DE R$ " + String.Format("{0:N}", somaTotal) + " COM BASE NO PMC UNIT.\n";
                            }
                        }
                        comprovanteVenda += "_________________________________________" + "\n\n\n\n\n";

                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    BematechImpressora.AcionaGuilhotina(0);
                                }

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                            {
                                using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                                {
                                    writer.WriteLine(comprovanteVenda);
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovanteVenda += "\n\n\n\n\n";
                                Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                }
                            }
                        }
                        else
                        {
                            comprovanteVenda += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }

                    string wsObrigaReceita = dadosVenda.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString();

                    if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && wsObrigaReceita.Equals("S"))
                    {
                        for (int i = 1; i <= qtde; i++)
                        {
                            comprovanteVenda = Funcoes.CentralizaTexto("COMPROVANTE", 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n\n";
                            comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                            comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                            comprovanteVenda += "N. VENDA: " + vendaID + "\n";
                            comprovanteVenda += "Data: " + Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dataVenda).ToString("HH:mm:ss") + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().PadLeft(38 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().Length, ' ') + "\n";
                            }
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao com Receita:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().PadLeft(26 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().Length, ' ') + "\n";
                            }
                            comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                            comprovanteVenda += "Cartao:                  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().PadLeft(33 - dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length, ' ')) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).PadLeft(32 - (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length, ' ')) + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS SEM RECEITA", 43) + "\n";
                                for (int x = 0; x < tProdutos.Rows.Count; x++)
                                {
                                    if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                                    {
                                        double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                        comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                        if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                        {
                                            comprovanteVenda += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                        }
                                        comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                                    }
                                }
                                comprovanteVenda += "_________________________________________" + "\n";
                            }

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS COM RECEITA", 43) + "\n";
                                for (int y = 0; y < tProdutos.Rows.Count; y++)
                                {
                                    if (tProdutos.Rows[y]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                                    {
                                        double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[y]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[y]["VENDA_ITEM_QTDE"]), 2) * 100);
                                        comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[y]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[y]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[y]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[y]["PROD_DESCR"].ToString()) + "\n";
                                        if (Math.Abs(Convert.ToDouble(tProdutos.Rows[y]["VENDA_ITEM_DIFERENCA"])) > 0)
                                        {
                                            comprovanteVenda += "    R$ " + tProdutos.Rows[y]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[y]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                        }
                                        comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[y]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[y]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                                    }
                                }
                                comprovanteVenda += "_________________________________________" + "\n";
                            }

                            comprovanteVenda += "Declaro ter verificado a classificacao" + "\n";
                            comprovanteVenda += "das compras com e sem receita para fins" + "\n";
                            comprovanteVenda += "de desconto." + "\n\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += Funcoes.LeParametro(2, "28", true) + "\n";
                            comprovanteVenda += "_________________________________________" + "\n\n\n\n\n";

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                     && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    string s_cmdTX = "\n";
                                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                    }
                                    else
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                    }

                                    iRetorno = BematechImpressora.IniciaPorta("USB");

                                    iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                    s_cmdTX = "\r\n";
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                        BematechImpressora.AcionaGuilhotina(0);
                                    }

                                    iRetorno = BematechImpressora.FechaPorta();
                                }
                                else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                    iRetorno = InterfaceEpsonNF.FechaPorta();
                                }
                                else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                                {
                                    using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\Drogabella" + vendaID + ".txt", true))
                                    {
                                        writer.WriteLine(comprovanteVenda);
                                    }
                                }
                                else
                                {
                                    //IMPRIME COMPROVANTE
                                    comprovanteVenda += "\n\n\n\n\n";
                                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                    }
                                }
                            }
                            else
                            {
                                comprovanteVenda += "\n\n\n\n";
                                //IMPRIME COMPROVANTE
                                int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }
                        }
                    }

                    if (Funcoes.LeParametro(6, "97", false).Equals("S"))
                    {
                        ProtocoloEntrega(vendaID, empCodigo, estCodigo, dataVenda, dadosVendaCliente[0].CfNome, Convert.ToDouble(dadosVenda.Rows[0]["VENDA_TOTAL"]));
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Reimpr. Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Orcamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgReimpressao_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        public void ProtocoloEntrega(long vendaID, int empCodigo, int estCodigo, DateTime dataVenda, string nome, double total)
        {
            try
            {
                string protocoloDeEntrega;
                Cursor = Cursors.WaitCursor;

                DataTable dadosEntrega = Util.SelecionaRegistrosTodosOuEspecifico("CONTROLE_ENTREGA", "", "VENDA_ID", vendaID.ToString(), true);

                if (dadosEntrega.Rows.Count > 0)
                {
                    protocoloDeEntrega = "_________________________________________" + "\n";
                    protocoloDeEntrega += Funcoes.CentralizaTexto("PROTOCOLO DE ENTREGA", 43) + "\n";
                    protocoloDeEntrega += "_________________________________________" + "\n";
                    protocoloDeEntrega += "VENDA.........: " + vendaID + "\n";
                    protocoloDeEntrega += "DATA..........: " + dataVenda.ToString("dd/MM/yyyy") + "\n";
                    protocoloDeEntrega += "CLIENTE.......: " + nome + "\n";
                    protocoloDeEntrega += "ENDERECO......: " + dadosEntrega.Rows[0]["ENT_CONTROLE_ENDER"] + "\n";
                    protocoloDeEntrega += "NUMERO........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_NUM_ENT"] + "\n";
                    protocoloDeEntrega += "BAIRRO........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_BAIRRO"] + "\n";
                    protocoloDeEntrega += "CIDADE........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_CIDADE"] + "\n";
                    protocoloDeEntrega += "CEP...........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_CEP"] + "\n";
                    protocoloDeEntrega += "TELEFONE......: " + dadosEntrega.Rows[0]["ENT_CONTROLE_FONE"] + "\n";
                    protocoloDeEntrega += "TOTAL DA NOTA.: " + String.Format("{0:N}", total) + "\n";
                    if (Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]) != 0)
                    {
                        protocoloDeEntrega += "TROCO PARA....: " + String.Format("{0:N}", Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]) + total) + "\n";
                        protocoloDeEntrega += "TROCO.........: " + String.Format("{0:N}", (Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]))) + "\n";
                    }
                    else
                    {
                        protocoloDeEntrega += "TROCO.........: 0,00\n";
                    }
                    protocoloDeEntrega += "OBSERVACAO....: " + dadosEntrega.Rows[0]["ENT_CONTROLE_OBSERVACAO"] + "\n\n\n";
                    protocoloDeEntrega += "_________________________________________" + "\n";
                    protocoloDeEntrega += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                    protocoloDeEntrega += "_________________________________________" + "\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(protocoloDeEntrega, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                BematechImpressora.AcionaGuilhotina(0);
                            }

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(protocoloDeEntrega);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                            {
                                writer.WriteLine(protocoloDeEntrega);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            protocoloDeEntrega += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocoloDeEntrega);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        comprovanteVenda += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocoloDeEntrega, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Protocolo de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgReimpressao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void btnImprimir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void dtDataVenda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void rdbComprovante_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void rdbFiscal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        public bool MontarXmlSAT(string numeroDocumento, string nomeNP, long idVenda)
        {
            #region MONTA XML SAT
            try
            {
                totalImpFederal = 0;
                totalImpEstadual = 0;
                totalImpMunicipal = 0;

                xmlSAT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xmlSAT += "<CFe>";
                xmlSAT += "<infCFe versaoDadosEnt=\"" + Funcoes.LeParametro(15, "02", true) + "\">";
                xmlSAT += "<ide>";
                xmlSAT += "<CNPJ>" + Funcoes.LeParametro(15, "07", true) + "</CNPJ>";
                xmlSAT += "<signAC>" + Funcoes.LeParametro(15, "03", true, Principal.nomeEstacao) + "</signAC>";
                xmlSAT += "<numeroCaixa>" + Funcoes.LeParametro(15, "04", true, Principal.nomeEstacao) + "</numeroCaixa>";
                xmlSAT += "</ide>";

                var estabelecimentos = new Estabelecimento();
                List<Estabelecimento> dadosFiscais = estabelecimentos.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                if (dadosFiscais.Count > 0)
                {
                    xmlSAT += "<emit>";
                    xmlSAT += "<CNPJ>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstCNPJ) + "</CNPJ>";
                    xmlSAT += "<IE>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoEstadual) + "</IE>";
                    xmlSAT += "<IM>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoMunicipal) + "</IM>";
                    xmlSAT += "<cRegTribISSQN>1</cRegTribISSQN>";
                    xmlSAT += "<indRatISSQN>N</indRatISSQN>";
                    xmlSAT += "</emit>";
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (!String.IsNullOrEmpty(numeroDocumento))
                {
                    xmlSAT += "<dest><" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(numeroDocumento) + "</" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">"
                        + "<xNome>" + nomeNP + "</xNome></dest>";
                }
                else
                    xmlSAT += "<dest/>";


                var dadosProdutos = new Produto();

                double impostoFederal;
                double impostoEstadual;
                double impostoMunicipal;

                var dadosItens = new VendasItens();
                tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(Convert.ToInt32(tProdutos.Rows[i]["EST_CODIGO"]), Convert.ToInt32(tProdutos.Rows[i]["EMP_CODIGO"]),
                        Convert.ToInt32(tProdutos.Rows[i]["PROD_ID"]));

                    xmlSAT += "<det nItem=\"" + (i + 1) + "\">";
                    xmlSAT += "<prod>";
                    xmlSAT += "<cProd>" + tProdutos.Rows[i]["PROD_CODIGO"] + "</cProd>";
                    xmlSAT += "<xProd>" + Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString()) + "</xProd>";
                    xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"] + "</NCM>";
                    xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                    xmlSAT += "<uCom>" + tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"] + "</uCom>";
                    xmlSAT += "<qCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]).ToString("#0.0000").Replace(",", ".") + "</qCom>";
                    xmlSAT += "<vUnCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]).ToString("#0.000").Replace(",", ".") + "</vUnCom>";
                    xmlSAT += "<indRegra>A</indRegra>";
                    xmlSAT += "</prod>";
                    xmlSAT += "<imposto>";

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"]) > 0)
                        impostoFederal = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"]) > 0)
                        impostoEstadual = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"]) > 0)
                        impostoMunicipal = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"])) / 100;

                    totalImpFederal += impostoFederal;
                    totalImpEstadual += impostoEstadual;
                    totalImpMunicipal += impostoMunicipal;

                    xmlSAT += "<vItem12741>" + String.Format("{0:N}", impostoFederal + impostoEstadual + impostoMunicipal).Replace(",", ".") + "</vItem12741>";
                    xmlSAT += "<ICMS>";

                    if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>201</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>202</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>500</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>300</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else
                    {
                        xmlSAT += "<ICMSSN900>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>900</CSOSN>";
                        xmlSAT += "<pICMS>0.00</pICMS>";
                        xmlSAT += "</ICMSSN900>";
                    }
                    xmlSAT += "</ICMS>";
                    xmlSAT += "<PIS>";
                    xmlSAT += "<PISNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false) + "</CST>";
                    xmlSAT += "</PISNT>";
                    xmlSAT += "</PIS>";
                    xmlSAT += "<COFINS>";
                    xmlSAT += "<COFINSNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false) + "</CST>";
                    xmlSAT += "</COFINSNT>";
                    xmlSAT += "</COFINS>";
                    xmlSAT += "</imposto>";
                    xmlSAT += "</det>";
                }

                double descontoTotal = Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")) + Math.Abs(Convert.ToDouble(
                    Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));
                if (Math.Abs(descontoTotal) > 0)
                {
                    xmlSAT += "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(descontoTotal)).Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "</total>";
                }
                else
                {
                    xmlSAT += "<total/>";
                }

                xmlSAT += "<pgto>";

                var dadosEspecies = new VendasEspecies();
                DataTable dtRetorno = dadosEspecies.BuscaEspeciesESatPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                for (int i = 0; i < dtRetorno.Rows.Count; i++)
                {
                    xmlSAT += "<MP><cMP>" + dtRetorno.Rows[i]["ESP_SAT"] + "</cMP>";
                    xmlSAT += "<vMP>" + String.Format("{0:N}", dtRetorno.Rows[i]["VENDA_ESPECIE_VALOR"]).ToString().Replace(",", ".") + "</vMP>";
                    if (dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("03") || dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("04"))
                    {
                        xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                    }
                    xmlSAT += "</MP>";
                }

                xmlSAT += "</pgto>";
                xmlSAT += "</infCFe>";
                xmlSAT += "</CFe>";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "XML SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion
        }

        private bool SATCupomFiscal(long idVenda)
        {
            try
            {
                string numeroCFe;
                if (!Sat.VerificaModeloSAT())
                {
                    return false;
                }

                Principal.NumCpfCnpj = "";
                Principal.NomeNP = "";
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    var dadosCliente = new Cliente();
                    DataTable dt = dadosCliente.SelecionaDadosDoClientePorVendaID(Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value), Principal.empAtual, Principal.estAtual);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dt.Rows[0]["CF_TIPO_DOCTO"]).Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }

                Cursor = Cursors.WaitCursor;
                var colaborador = new Colaborador();
                string arquivoSATResposta, mes;
                mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                dadosVenda = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", idVenda.ToString(), true);
                var dadosEspecies = new VendasEspecies();

                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (!MontarXmlSAT(Principal.NumCpfCnpj, Principal.NomeNP, idVenda))
                        return false;

                    string retorno = Sat.SatEnviarDadosDaVenda(xmlSAT);

                    FileStream fs;
                    StreamWriter sw;
                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCFe))
                    {
                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesComErro\" + mes.ToUpper() + @"\" + idVenda + ".xml", FileMode.Create);
                        sw = new StreamWriter(fs);
                        sw.WriteLine(xmlSAT);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                        return false;
                    }

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    //CONVERTE BASE64 PARA STRING//
                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    Sat.SatImpressaoComprovante(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml",
                        totalImpFederal, totalImpEstadual, totalImpMunicipal, numeroCFe, out vendaNumeroNota, colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), idVenda,
                        "", "");

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);
                }
                else
                {
                    int iRetorno;

                    iRetorno = DarumaDLL.iConfigurarGuilhotina_DUAL_DarumaFramework("1", "2");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    iRetorno = DarumaDLL.eDefinirProduto_Daruma("SAT");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    if (!String.IsNullOrEmpty(Principal.NumCpfCnpj))
                    {
                        xmlSAT = "<dest><" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(Principal.NumCpfCnpj) + "</" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">"
                            + "<xNome>" + Principal.NomeNP + "</xNome></dest>";
                    }
                    else
                        xmlSAT = "";

                    iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        if (iRetorno == -130)
                        {
                            iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                            else
                            {
                                iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }

                        }
                        else
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    var dadosProdutos = new Produto();

                    var dadosItens = new VendasItens();
                    tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                    for (int i = 0; i < tProdutos.Rows.Count; i++)
                    {
                        DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(Convert.ToInt32(tProdutos.Rows[i]["EST_CODIGO"]), Convert.ToInt32(tProdutos.Rows[i]["EMP_CODIGO"]),
                            Convert.ToInt32(tProdutos.Rows[i]["PROD_ID"]));

                        xmlSAT = "<prod>";
                        xmlSAT += "<cProd>" + tProdutos.Rows[i]["PROD_CODIGO"] + "</cProd>";
                        xmlSAT += "<xProd>" + Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString()) + "</xProd>";
                        xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"] + "</NCM>";
                        xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                        xmlSAT += "<uCom>" + tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"] + "</uCom>";
                        xmlSAT += "<qCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]).ToString("#0.0000").Replace(",", ".") + "</qCom>";
                        xmlSAT += "<vUnCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]).ToString("#0.000").Replace(",", ".") + "</vUnCom>";
                        xmlSAT += "<indRegra>A</indRegra>";
                        xmlSAT += "</prod>";
                        xmlSAT += "<imposto>";

                        xmlSAT += "<ICMS>";

                        if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                            dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>101</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>201</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>202</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>500</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>102</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>300</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else
                        {
                            xmlSAT += "<ICMSSN900>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>900</CSOSN>";
                            xmlSAT += "<pICMS>0.00</pICMS>";
                            xmlSAT += "</ICMSSN900>";
                        }
                        xmlSAT += "</ICMS>";
                        xmlSAT += "<PIS>";
                        xmlSAT += "<PISNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false);
                        xmlSAT += "</CST>";
                        xmlSAT += "</PISNT>";
                        xmlSAT += "</PIS>";
                        xmlSAT += "<COFINS>";
                        xmlSAT += "<COFINSNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false);
                        xmlSAT += "</CST>";
                        xmlSAT += "</COFINSNT>";
                        xmlSAT += "</COFINS>";
                        xmlSAT += "</imposto>";

                        iRetorno = DarumaDLL.aCFeVender_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    double descontoTotal = Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")) + Math.Abs(Convert.ToDouble(
                    Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));

                    xmlSAT = "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(descontoTotal)).Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "<vCFeLei12741>0.00</vCFeLei12741>";
                    xmlSAT += "</total>";

                    iRetorno = DarumaDLL.aCFeTotalizar_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    DataTable dtRetorno = dadosEspecies.BuscaEspeciesESatPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        xmlSAT = "<MP><cMP>" + dtRetorno.Rows[i]["ESP_SAT"] + "</cMP>";
                        xmlSAT += "<vMP>" + String.Format("{0:N}", dtRetorno.Rows[i]["VENDA_ESPECIE_VALOR"]).ToString().Replace(",", ".") + "</vMP>";
                        if (dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("03") || dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("04"))
                        {
                            xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                        }
                        xmlSAT += "</MP>";

                        iRetorno = DarumaDLL.aCFeEfetuarPagamento_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    iRetorno = DarumaDLL.tCFeEncerrar_SAT_Daruma("<infAdic><infCpl>" + Funcoes.LeParametro(2, "28", true) + "</infCpl></infAdic>");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    StringBuilder str_RetornoStatus = new StringBuilder(400);
                    iRetorno = DarumaDLL.rConsultarStatus_SAT_Daruma(str_RetornoStatus);

                    string[] split = str_RetornoStatus.ToString().Split('|');

                    System.IO.File.Move(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml", @"" + Funcoes.LeParametro(15, "06", true)
                        + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\CFe" + split[17] + ".xml");

                    System.IO.File.Delete(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml");

                    numeroCFe = "CFe" + split[17];

                    iRetorno = DarumaDLL.rInfoEstendida_SAT_Daruma("1", str_RetornoStatus);
                    vendaNumeroNota = str_RetornoStatus.ToString();

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);
                }

                List<VendasEspecies> vendaEspecie = dadosEspecies.DadosEspeciesPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                var vendaFormaPagto = new VendasFormaPagamento();
                DataTable dtForma = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtForma.Rows[0]["QTDE_VIAS"]);

                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                            vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        string wsProdutosCupomSemReceita = "";
                        string wsProdutosCupomReceita = "";
                        double valorDUni;

                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;
                            if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomSemReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomSemReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomSemReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                            else if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                        }

                        ComprovantesVinculados.VinculadoDrogabellaPlantaoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                         vendaEspecie[0].Valor, vendaEspecie[0].EspDescricao, dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString(), dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(),
                         idVenda, wsProdutosCupomSemReceita, wsProdutosCupomReceita, Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true),
                         dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), dadosVenda.Rows[0]["VENDA_CARTAO"].ToString(),
                         dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString(), dadosVenda.Rows[0]["VENDA_ENTREGA"].ToString() == "S" ? true : false,
                         dadosVenda.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString(), dadosVenda.Rows[0]["VENDA_MEDICO"].ToString() == "" ? "" :
                         "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"],
                         colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), Convert.ToInt32(dadosVenda.Rows[0]["VENDA_CONVENIO_PARCELA"]));

                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_WEB", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true).Equals("4"))
                    {
                        string produtosComprov = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                            produtosComprov += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                produtosComprov += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        var dadosPagamento = new VendasFormaPagamento();
                        List<VendasFormaPagamento> vendaPedFormaPagto = dadosPagamento.DadosFormaPagamentoPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                        ComprovantesVinculados.VinculadoParticular(qtdeVias, vendaNumeroNota,
                           @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                           vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), produtosComprov, vendaPedFormaPagto,
                           Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_NOME", "CF_ID", dadosVenda.Rows[0]["VENDA_CF_ID"].ToString(), false, true), dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString());
                    }
                }


                var dadosFiscais = new CuponsSAT
                    (
                        Principal.empAtual,
                        Principal.estAtual,
                        idVenda,
                        vendaNumeroNota,
                        numeroCFe,
                        Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                        "S",
                        "N",
                        Principal.NumeroSessao,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                dadosFiscais.InserirDados(dadosFiscais);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ImprimirCupomFiscal(long idVenda)
        {
            try
            {
                if (Imprimir.VerificaModeloImpressoraFiscal().Equals(false))
                {
                    return false;
                }

                #region CPF/CNPJ
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    var dadosCliente = new Cliente();
                    DataTable dt = dadosCliente.SelecionaDadosDoClientePorVendaID(Convert.ToInt64(dgReimpressao.Rows[indice].Cells[1].Value), Principal.empAtual, Principal.estAtual);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dt.Rows[0]["CF_TIPO_DOCTO"]).Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }
                #endregion

                string modeloImpressora = Funcoes.LeParametro(6, "08", true, Funcoes.LeParametro(6, "123", true).Equals("N") ? "GERAL" : Principal.nomeEstacao).ToUpper();
                
                Cursor = Cursors.WaitCursor;
                string vendaNumeroCaixa = "";

                string retorno = Imprimir.ImpFiscalInformacoesECF("107", vendaNumeroCaixa);
                if (String.IsNullOrEmpty(retorno))
                    return false;

                vendaNumeroNota = Imprimir.ImpFiscalNumeroCupom();

                if (modeloImpressora.Equals("EPSON"))
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt64(vendaNumeroNota.Substring(58, 6)) + 1);
                }
                else
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt32(vendaNumeroNota) + 1);
                }

                if (!Imprimir.ImpFiscalAbreCupom(Principal.NumCpfCnpj, Principal.NomeNP))
                    return false;

                List<VendaItem> venItens = new List<VendaItem>();

                var dadosItens = new VendasItens();
                tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    venItens.Add(new VendaItem
                    {
                        ItemECF = tProdutos.Rows[i]["PROD_ECF"].ToString(),
                        ItemQtde = tProdutos.Rows[i]["VENDA_ITEM_QTDE"].ToString(),
                        ItemVlUnit = tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"].ToString(),
                        ItemTipoDesc = "D$",
                        ItemVlDesc = Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString(),
                        ItemCodBarra = tProdutos.Rows[i]["PROD_CODIGO"].ToString(),
                        ItemUniade = tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"].ToString(),
                        ItemDescr = tProdutos.Rows[i]["PROD_DESCR"].ToString(),
                        ItemNcm = tProdutos.Rows[i]["NCM"].ToString(),
                        ItemTipo = "0"
                    });
                }

                if (!Imprimir.ImpFiscalVenderItem(venItens))
                    return false;

                double descontoTotal = Math.Abs(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));

                if (!Imprimir.ImpFiscalTotalizarCupom("D", descontoTotal.ToString()))
                    return false;

                List<VendasEspecies> vendaEspecie = new List<VendasEspecies>();
                var dadosEspecies = new VendasEspecies();
                vendaEspecie = dadosEspecies.DadosEspeciesPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                if (!Imprimir.ImpFiscalMeioPagamento(vendaEspecie))
                    return false;

                var colaborador = new Colaborador();
                dadosVenda = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", idVenda.ToString(), true);

                string mensagem = "";
                string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);
                if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual)
                        + "    " + "Num.Pedido:" + idVenda, 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString(), 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao com Receita:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(), 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    mensagem += Funcoes.PrencherEspacoEmBranco("Administradora: DROGABELLA/PLANTAO CARD", 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Cartao: " + dadosVenda.Rows[0]["VENDA_CARTAO"].ToString(), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ?
                        dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32) : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("EMPRESA: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " +
                        wsNomeEmpresa).Length > 40 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 40)
                        : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa)), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }
                else if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("FUNCIONAL"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString(), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Administradora:  FUNCIONAL CARD", 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Cartao:  " + dadosVenda.Rows[0]["VENDA_CARTAO"], 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                        : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";

                    wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                    mensagem += Funcoes.PrencherEspacoEmBranco("Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                        : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";

                    if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_MEDICO"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"], 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                }
                else if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("PARTICULAR"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual)
                      + "    " + "Num.Pedido:" + idVenda, 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " 
                        + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString())).Length > 32 ?
                        (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString())).Substring(0, 32)
                       : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString())).ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Nome: " + Util.SelecionaCampoEspecificoDaTabela("CLIFOR","CF_NOME","CF_ID", dadosVenda.Rows[0]["VENDA_CF_ID"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }

                #region LEI DE IMPOSTO
                    double impostoFederal, impostoEstadual, impostoMunicipal;
                var buscaImpostos = new Ibpt();

                for (int i = 0; i < venItens.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    DataTable dtRetorno = buscaImpostos.BuscaImpostosPorNCM(venItens[i].ItemNcm);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"]) > 0)
                            impostoFederal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"]) > 0)
                            impostoEstadual = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"]) > 0)
                            impostoMunicipal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"])) / 100;

                        totalImpFederal += impostoFederal;
                        totalImpEstadual += impostoEstadual;
                        totalImpMunicipal += impostoMunicipal;
                    }
                }

                mensagem += Funcoes.PrencherEspacoEmBranco("Trib aprox R$:" + String.Format("{0:N}", totalImpFederal).Replace(",", ".") + " Fed, " + String.Format("{0:N}", totalImpEstadual).Replace(",", ".") + " Est, " + String.Format("{0:N}", totalImpMunicipal).Replace(",", ".") + " Mun", 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco("Fonte:IBPT   5oi7eW", 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco(Funcoes.LeParametro(2, "28", false), 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                #endregion

                if (!Imprimir.ImpFiscalFechaCupomMsg(mensagem))
                    return false;

                #region CUPONS VINCULADOS
                double valorDUni;

                var vendaFormaPagto = new VendasFormaPagamento();
                DataTable dtForma = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);
                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtForma.Rows[0]["QTDE_VIAS"]);
                
                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoFiscal(qtdeVias, vendaNumeroNota, Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                            vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        string wsProdutosCupomSemReceita = "";
                        string wsProdutosCupomReceita = "";
                        
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;
                            if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomSemReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomSemReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomSemReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                            else if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                        }

                        ComprovantesVinculados.VinculadoDrogabellaPlantaoFiscal(qtdeVias, vendaNumeroNota,
                            Convert.ToDateTime(dadosVenda.Rows[0]["VENDA_DATA_HORA"]), Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                            vendaEspecie[0].Valor, vendaEspecie[0].EspDescricao, dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString(), dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(),
                            idVenda, wsProdutosCupomSemReceita, wsProdutosCupomReceita, wsNomeEmpresa,
                            dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), dadosVenda.Rows[0]["VENDA_CARTAO"].ToString(),
                            dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString(), dadosVenda.Rows[0]["VENDA_ENTREGA"].ToString() == "S" ? true : false,
                            dadosVenda.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString(), dadosVenda.Rows[0]["VENDA_MEDICO"].ToString() == "" ? "" :
                            "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"],
                            colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), Convert.ToInt32(dadosVenda.Rows[0]["VENDA_CONVENIO_PARCELA"]));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_WEB", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true).Equals("4"))
                    {
                        var dadosPagamento = new VendasFormaPagamento();
                        List<VendasFormaPagamento> vendaPedFormaPagto = dadosPagamento.DadosFormaPagamentoPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                        string produtos = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;

                            valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                            produtos += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                produtos += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            produtos += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";

                        }

                        ComprovantesVinculados.VinculadoParticularFiscal(qtdeVias, vendaNumeroNota,
                           idVenda, Convert.ToDouble(dgReimpressao.Rows[indice].Cells[4].Value),
                           vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), vendaPedFormaPagto,
                           Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_NOME", "CF_ID", dadosVenda.Rows[0]["VENDA_CF_ID"].ToString(), false, true), dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), produtos);
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão Cupom Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void dtDataVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }
    }

}
