﻿namespace GestaoAdministrativa.Vendas.NFE
{
    partial class frmVenNFE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenNFE));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabNFe = new System.Windows.Forms.TabControl();
            this.tpConsultaCadastro = new System.Windows.Forms.TabPage();
            this.lblCadastro = new System.Windows.Forms.Label();
            this.btnConsultarCadastro = new System.Windows.Forms.Button();
            this.tpDevolucao = new System.Windows.Forms.TabPage();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.TotCOFINS = new System.Windows.Forms.TextBox();
            this.TotPIS = new System.Windows.Forms.TextBox();
            this.BcPIS = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.AliqPIS = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.AliqCOFINS = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.BcCOFINS = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtValidade = new System.Windows.Forms.MaskedTextBox();
            this.txtFabricacao = new System.Windows.Forms.MaskedTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtLote = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtVlIcmsST = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtVlBcRT = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtVlIcms = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtVlBaseIcms = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cmbIcms = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtNaturezaDeOperacao = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbDevOperacao = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCnpj = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnGerarDevolucao = new System.Windows.Forms.Button();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.txtChave = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblQtde = new System.Windows.Forms.Label();
            this.dgDevolucao = new System.Windows.Forms.DataGridView();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tpEmitir = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPrincipio = new System.Windows.Forms.CheckBox();
            this.cmbDocumento = new System.Windows.Forms.ComboBox();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtErros = new System.Windows.Forms.TextBox();
            this.txtComplementar = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGerarNFE = new System.Windows.Forms.Button();
            this.cmbOperacao = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbEmissao = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgBusca = new System.Windows.Forms.DataGridView();
            this.VENDA_EMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRODUTOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTDE_PRODUTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_DATA_HORA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtIdCliente = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.chkUnificar = new System.Windows.Forms.CheckBox();
            this.dtFinicial = new System.Windows.Forms.MaskedTextBox();
            this.lblFinal = new System.Windows.Forms.Label();
            this.dtInicial = new System.Windows.Forms.MaskedTextBox();
            this.lblInicial = new System.Windows.Forms.Label();
            this.dtData = new System.Windows.Forms.MaskedTextBox();
            this.btnBuscarVenda = new System.Windows.Forms.Button();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.txtVendaID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tpGeradas = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtMotivoCancelamento = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnImpressaoCCe = new System.Windows.Forms.Button();
            this.btnGerarCCe = new System.Windows.Forms.Button();
            this.txtMotivoCCe = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnReimpressao = new System.Windows.Forms.Button();
            this.dgEmitidos = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHAVE_NFE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NFE_DATA_CANCELAMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NFE_DEVOLUCAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUM_PROTOCOLO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtBData = new System.Windows.Forms.MaskedTextBox();
            this.btnEmitidas = new System.Windows.Forms.Button();
            this.txtBChave = new System.Windows.Forms.TextBox();
            this.txtBVendaId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tpInutilizacao = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRetorno = new System.Windows.Forms.TextBox();
            this.btnInutilizar = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtJustificativa = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFinal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInicial = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.TextBox();
            this.tpManifestacao = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRetornoManif = new System.Windows.Forms.TextBox();
            this.txtManifestacaoJus = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.cbEvento = new System.Windows.Forms.ComboBox();
            this.btnManifestacao = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.chNfeDest = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tpStatus = new System.Windows.Forms.TabPage();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnStatus = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.PROD_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_PRECOMPRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vBC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vBCSTRet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vICMSST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataFabricacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataValidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vBcCOFINS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vBcPIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pAliqCOFINS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pAliqPIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor_cofins = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor_pis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabNFe.SuspendLayout();
            this.tpConsultaCadastro.SuspendLayout();
            this.tpDevolucao.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDevolucao)).BeginInit();
            this.tpEmitir.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBusca)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tpGeradas.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmitidos)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tpInutilizacao.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tpManifestacao.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tpStatus.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(960, 560);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tabNFe);
            this.panel2.Location = new System.Drawing.Point(9, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(945, 495);
            this.panel2.TabIndex = 43;
            // 
            // tabNFe
            // 
            this.tabNFe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabNFe.Controls.Add(this.tpConsultaCadastro);
            this.tabNFe.Controls.Add(this.tpDevolucao);
            this.tabNFe.Controls.Add(this.tpEmitir);
            this.tabNFe.Controls.Add(this.tpGeradas);
            this.tabNFe.Controls.Add(this.tpInutilizacao);
            this.tabNFe.Controls.Add(this.tpManifestacao);
            this.tabNFe.Controls.Add(this.tpStatus);
            this.tabNFe.Location = new System.Drawing.Point(3, 3);
            this.tabNFe.Name = "tabNFe";
            this.tabNFe.SelectedIndex = 0;
            this.tabNFe.Size = new System.Drawing.Size(937, 487);
            this.tabNFe.TabIndex = 0;
            this.tabNFe.SelectedIndexChanged += new System.EventHandler(this.tabNFe_SelectedIndexChanged);
            // 
            // tpConsultaCadastro
            // 
            this.tpConsultaCadastro.Controls.Add(this.lblCadastro);
            this.tpConsultaCadastro.Controls.Add(this.btnConsultarCadastro);
            this.tpConsultaCadastro.Location = new System.Drawing.Point(4, 25);
            this.tpConsultaCadastro.Name = "tpConsultaCadastro";
            this.tpConsultaCadastro.Size = new System.Drawing.Size(929, 458);
            this.tpConsultaCadastro.TabIndex = 4;
            this.tpConsultaCadastro.Text = "Consultar Cadastro";
            this.tpConsultaCadastro.UseVisualStyleBackColor = true;
            // 
            // lblCadastro
            // 
            this.lblCadastro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCadastro.Location = new System.Drawing.Point(203, 22);
            this.lblCadastro.Name = "lblCadastro";
            this.lblCadastro.Size = new System.Drawing.Size(709, 352);
            this.lblCadastro.TabIndex = 3;
            // 
            // btnConsultarCadastro
            // 
            this.btnConsultarCadastro.BackColor = System.Drawing.Color.White;
            this.btnConsultarCadastro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarCadastro.ForeColor = System.Drawing.Color.Black;
            this.btnConsultarCadastro.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultarCadastro.Image")));
            this.btnConsultarCadastro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultarCadastro.Location = new System.Drawing.Point(16, 22);
            this.btnConsultarCadastro.Name = "btnConsultarCadastro";
            this.btnConsultarCadastro.Size = new System.Drawing.Size(181, 65);
            this.btnConsultarCadastro.TabIndex = 2;
            this.btnConsultarCadastro.Text = "Consultar Cadastro";
            this.btnConsultarCadastro.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsultarCadastro.UseVisualStyleBackColor = false;
            this.btnConsultarCadastro.Click += new System.EventHandler(this.btnConsultarCadastro_Click);
            // 
            // tpDevolucao
            // 
            this.tpDevolucao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpDevolucao.Controls.Add(this.label49);
            this.tpDevolucao.Controls.Add(this.label48);
            this.tpDevolucao.Controls.Add(this.TotCOFINS);
            this.tpDevolucao.Controls.Add(this.TotPIS);
            this.tpDevolucao.Controls.Add(this.BcPIS);
            this.tpDevolucao.Controls.Add(this.label43);
            this.tpDevolucao.Controls.Add(this.AliqPIS);
            this.tpDevolucao.Controls.Add(this.label42);
            this.tpDevolucao.Controls.Add(this.AliqCOFINS);
            this.tpDevolucao.Controls.Add(this.label41);
            this.tpDevolucao.Controls.Add(this.BcCOFINS);
            this.tpDevolucao.Controls.Add(this.label40);
            this.tpDevolucao.Controls.Add(this.txtValidade);
            this.tpDevolucao.Controls.Add(this.txtFabricacao);
            this.tpDevolucao.Controls.Add(this.label38);
            this.tpDevolucao.Controls.Add(this.label37);
            this.tpDevolucao.Controls.Add(this.txtLote);
            this.tpDevolucao.Controls.Add(this.label36);
            this.tpDevolucao.Controls.Add(this.txtVlIcmsST);
            this.tpDevolucao.Controls.Add(this.label35);
            this.tpDevolucao.Controls.Add(this.txtVlBcRT);
            this.tpDevolucao.Controls.Add(this.label34);
            this.tpDevolucao.Controls.Add(this.txtVlIcms);
            this.tpDevolucao.Controls.Add(this.label33);
            this.tpDevolucao.Controls.Add(this.txtVlBaseIcms);
            this.tpDevolucao.Controls.Add(this.label32);
            this.tpDevolucao.Controls.Add(this.txtDesconto);
            this.tpDevolucao.Controls.Add(this.label31);
            this.tpDevolucao.Controls.Add(this.groupBox10);
            this.tpDevolucao.Controls.Add(this.lblTotal);
            this.tpDevolucao.Controls.Add(this.lblQtde);
            this.tpDevolucao.Controls.Add(this.dgDevolucao);
            this.tpDevolucao.Controls.Add(this.btnAdicionar);
            this.tpDevolucao.Controls.Add(this.txtQtde);
            this.tpDevolucao.Controls.Add(this.label21);
            this.tpDevolucao.Controls.Add(this.txtPreco);
            this.tpDevolucao.Controls.Add(this.label22);
            this.tpDevolucao.Controls.Add(this.txtDescr);
            this.tpDevolucao.Controls.Add(this.txtCodBarras);
            this.tpDevolucao.Controls.Add(this.label20);
            this.tpDevolucao.Controls.Add(this.label6);
            this.tpDevolucao.Location = new System.Drawing.Point(4, 25);
            this.tpDevolucao.Name = "tpDevolucao";
            this.tpDevolucao.Padding = new System.Windows.Forms.Padding(3);
            this.tpDevolucao.Size = new System.Drawing.Size(929, 458);
            this.tpDevolucao.TabIndex = 11;
            this.tpDevolucao.Text = "Devolução de Mercadoria";
            this.tpDevolucao.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(274, 100);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(109, 16);
            this.label49.TabIndex = 278;
            this.label49.Text = "Vlr. Tot. COFINS";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(638, 100);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 16);
            this.label48.TabIndex = 277;
            this.label48.Text = "Vlr. Tot. PIS";
            // 
            // TotCOFINS
            // 
            this.TotCOFINS.BackColor = System.Drawing.Color.White;
            this.TotCOFINS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotCOFINS.Enabled = false;
            this.TotCOFINS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotCOFINS.ForeColor = System.Drawing.Color.Black;
            this.TotCOFINS.Location = new System.Drawing.Point(277, 118);
            this.TotCOFINS.MaxLength = 13;
            this.TotCOFINS.Name = "TotCOFINS";
            this.TotCOFINS.Size = new System.Drawing.Size(127, 22);
            this.TotCOFINS.TabIndex = 276;
            this.TotCOFINS.Text = "0";
            this.TotCOFINS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TotPIS
            // 
            this.TotPIS.BackColor = System.Drawing.Color.White;
            this.TotPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TotPIS.Enabled = false;
            this.TotPIS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotPIS.ForeColor = System.Drawing.Color.Black;
            this.TotPIS.Location = new System.Drawing.Point(641, 118);
            this.TotPIS.MaxLength = 13;
            this.TotPIS.Name = "TotPIS";
            this.TotPIS.Size = new System.Drawing.Size(73, 22);
            this.TotPIS.TabIndex = 275;
            this.TotPIS.Text = "0";
            this.TotPIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BcPIS
            // 
            this.BcPIS.BackColor = System.Drawing.Color.White;
            this.BcPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BcPIS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BcPIS.ForeColor = System.Drawing.Color.Black;
            this.BcPIS.Location = new System.Drawing.Point(410, 118);
            this.BcPIS.MaxLength = 13;
            this.BcPIS.Name = "BcPIS";
            this.BcPIS.Size = new System.Drawing.Size(79, 22);
            this.BcPIS.TabIndex = 274;
            this.BcPIS.Text = "0";
            this.BcPIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BcPIS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BcPIS_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(407, 100);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 16);
            this.label43.TabIndex = 273;
            this.label43.Text = "Vl. BC PIS";
            // 
            // AliqPIS
            // 
            this.AliqPIS.BackColor = System.Drawing.Color.White;
            this.AliqPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AliqPIS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AliqPIS.ForeColor = System.Drawing.Color.Black;
            this.AliqPIS.Location = new System.Drawing.Point(495, 118);
            this.AliqPIS.MaxLength = 13;
            this.AliqPIS.Name = "AliqPIS";
            this.AliqPIS.Size = new System.Drawing.Size(137, 22);
            this.AliqPIS.TabIndex = 272;
            this.AliqPIS.Text = "0";
            this.AliqPIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AliqPIS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AliqPIS_KeyPress);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(494, 100);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(122, 16);
            this.label42.TabIndex = 271;
            this.label42.Text = "Vl. Aliquota PIS %";
            // 
            // AliqCOFINS
            // 
            this.AliqCOFINS.BackColor = System.Drawing.Color.White;
            this.AliqCOFINS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AliqCOFINS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AliqCOFINS.ForeColor = System.Drawing.Color.Black;
            this.AliqCOFINS.Location = new System.Drawing.Point(120, 118);
            this.AliqCOFINS.MaxLength = 13;
            this.AliqCOFINS.Name = "AliqCOFINS";
            this.AliqCOFINS.Size = new System.Drawing.Size(146, 22);
            this.AliqCOFINS.TabIndex = 270;
            this.AliqCOFINS.Text = "0,00";
            this.AliqCOFINS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.AliqCOFINS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AliqCOFINS_KeyPress);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(117, 100);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(149, 16);
            this.label41.TabIndex = 269;
            this.label41.Text = "Vl. Aliquota COFINS %";
            // 
            // BcCOFINS
            // 
            this.BcCOFINS.BackColor = System.Drawing.Color.White;
            this.BcCOFINS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BcCOFINS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BcCOFINS.ForeColor = System.Drawing.Color.Black;
            this.BcCOFINS.Location = new System.Drawing.Point(12, 118);
            this.BcCOFINS.MaxLength = 13;
            this.BcCOFINS.Name = "BcCOFINS";
            this.BcCOFINS.Size = new System.Drawing.Size(97, 22);
            this.BcCOFINS.TabIndex = 268;
            this.BcCOFINS.Text = "0,00";
            this.BcCOFINS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BcCOFINS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BcCOFINS_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(9, 100);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(100, 16);
            this.label40.TabIndex = 267;
            this.label40.Text = "Vl. BC COFINS";
            // 
            // txtValidade
            // 
            this.txtValidade.Location = new System.Drawing.Point(641, 73);
            this.txtValidade.Mask = "00/00/0000";
            this.txtValidade.Name = "txtValidade";
            this.txtValidade.Size = new System.Drawing.Size(137, 22);
            this.txtValidade.TabIndex = 258;
            this.txtValidade.ValidatingType = typeof(System.DateTime);
            this.txtValidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValidade_KeyPress);
            // 
            // txtFabricacao
            // 
            this.txtFabricacao.Location = new System.Drawing.Point(495, 72);
            this.txtFabricacao.Mask = "00/00/0000";
            this.txtFabricacao.Name = "txtFabricacao";
            this.txtFabricacao.Size = new System.Drawing.Size(137, 22);
            this.txtFabricacao.TabIndex = 257;
            this.txtFabricacao.ValidatingType = typeof(System.DateTime);
            this.txtFabricacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFabricacao_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(638, 55);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(97, 16);
            this.label38.TabIndex = 256;
            this.label38.Text = "Data Validade";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(492, 55);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(112, 16);
            this.label37.TabIndex = 255;
            this.label37.Text = "Data Fabricação";
            // 
            // txtLote
            // 
            this.txtLote.BackColor = System.Drawing.Color.White;
            this.txtLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLote.ForeColor = System.Drawing.Color.Black;
            this.txtLote.Location = new System.Drawing.Point(410, 72);
            this.txtLote.MaxLength = 13;
            this.txtLote.Name = "txtLote";
            this.txtLote.Size = new System.Drawing.Size(79, 22);
            this.txtLote.TabIndex = 254;
            this.txtLote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLote_KeyPress);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(407, 55);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(36, 16);
            this.label36.TabIndex = 253;
            this.label36.Text = "Lote";
            // 
            // txtVlIcmsST
            // 
            this.txtVlIcmsST.BackColor = System.Drawing.Color.White;
            this.txtVlIcmsST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVlIcmsST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlIcmsST.ForeColor = System.Drawing.Color.Black;
            this.txtVlIcmsST.Location = new System.Drawing.Point(277, 72);
            this.txtVlIcmsST.MaxLength = 13;
            this.txtVlIcmsST.Name = "txtVlIcmsST";
            this.txtVlIcmsST.Size = new System.Drawing.Size(127, 22);
            this.txtVlIcmsST.TabIndex = 252;
            this.txtVlIcmsST.Text = "0,00";
            this.txtVlIcmsST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVlIcmsST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlIcmsST_KeyPress);
            this.txtVlIcmsST.Validated += new System.EventHandler(this.txtVlIcmsST_Validated);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(274, 55);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(119, 16);
            this.label35.TabIndex = 251;
            this.label35.Text = "Valor do ICMS ST";
            // 
            // txtVlBcRT
            // 
            this.txtVlBcRT.BackColor = System.Drawing.Color.White;
            this.txtVlBcRT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVlBcRT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlBcRT.ForeColor = System.Drawing.Color.Black;
            this.txtVlBcRT.Location = new System.Drawing.Point(120, 72);
            this.txtVlBcRT.MaxLength = 13;
            this.txtVlBcRT.Name = "txtVlBcRT";
            this.txtVlBcRT.Size = new System.Drawing.Size(151, 22);
            this.txtVlBcRT.TabIndex = 250;
            this.txtVlBcRT.Text = "0,00";
            this.txtVlBcRT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVlBcRT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlBcRT_KeyPress);
            this.txtVlBcRT.Validated += new System.EventHandler(this.txtVlBcRT_Validated);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(117, 55);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(154, 16);
            this.label34.TabIndex = 249;
            this.label34.Text = "Vl. da BC do ICMS R.A.";
            // 
            // txtVlIcms
            // 
            this.txtVlIcms.BackColor = System.Drawing.Color.White;
            this.txtVlIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVlIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlIcms.ForeColor = System.Drawing.Color.Black;
            this.txtVlIcms.Location = new System.Drawing.Point(11, 73);
            this.txtVlIcms.MaxLength = 13;
            this.txtVlIcms.Name = "txtVlIcms";
            this.txtVlIcms.Size = new System.Drawing.Size(99, 22);
            this.txtVlIcms.TabIndex = 248;
            this.txtVlIcms.Text = "0,00";
            this.txtVlIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVlIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlIcms_KeyPress);
            this.txtVlIcms.Validated += new System.EventHandler(this.txtVlIcms_Validated);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(8, 55);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(82, 16);
            this.label33.TabIndex = 247;
            this.label33.Text = "Vl. do ICMS";
            // 
            // txtVlBaseIcms
            // 
            this.txtVlBaseIcms.BackColor = System.Drawing.Color.White;
            this.txtVlBaseIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVlBaseIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlBaseIcms.ForeColor = System.Drawing.Color.Black;
            this.txtVlBaseIcms.Location = new System.Drawing.Point(729, 30);
            this.txtVlBaseIcms.MaxLength = 13;
            this.txtVlBaseIcms.Name = "txtVlBaseIcms";
            this.txtVlBaseIcms.Size = new System.Drawing.Size(121, 22);
            this.txtVlBaseIcms.TabIndex = 246;
            this.txtVlBaseIcms.Text = "0,00";
            this.txtVlBaseIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVlBaseIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlBaseIcms_KeyPress);
            this.txtVlBaseIcms.Validated += new System.EventHandler(this.txtVlBaseIcms_Validated);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(726, 11);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(124, 16);
            this.label32.TabIndex = 245;
            this.label32.Text = "Vl. da BC do ICMS";
            // 
            // txtDesconto
            // 
            this.txtDesconto.BackColor = System.Drawing.Color.White;
            this.txtDesconto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesconto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto.ForeColor = System.Drawing.Color.Black;
            this.txtDesconto.Location = new System.Drawing.Point(641, 30);
            this.txtDesconto.MaxLength = 13;
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(79, 22);
            this.txtDesconto.TabIndex = 244;
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesconto_KeyPress);
            this.txtDesconto.Validated += new System.EventHandler(this.txtDesconto_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(638, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(66, 16);
            this.label31.TabIndex = 243;
            this.label31.Text = "Desconto";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.cmbIcms);
            this.groupBox10.Controls.Add(this.label39);
            this.groupBox10.Controls.Add(this.txtNaturezaDeOperacao);
            this.groupBox10.Controls.Add(this.label30);
            this.groupBox10.Controls.Add(this.cmbDevOperacao);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.txtCnpj);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.btnGerarDevolucao);
            this.groupBox10.Controls.Add(this.txtComplemento);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.txtMotivo);
            this.groupBox10.Controls.Add(this.txtChave);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.Color.Navy;
            this.groupBox10.Location = new System.Drawing.Point(11, 293);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(903, 157);
            this.groupBox10.TabIndex = 242;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Gerar Nota Devolução";
            // 
            // cmbIcms
            // 
            this.cmbIcms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIcms.ForeColor = System.Drawing.Color.Black;
            this.cmbIcms.FormattingEnabled = true;
            this.cmbIcms.Items.AddRange(new object[] {
            "1 - Contribuinte ICMS",
            "2 - Contribuinte isento de Inscrição no cadastro de Contribuintes do ICMS",
            "9 - Não Contribuinte"});
            this.cmbIcms.Location = new System.Drawing.Point(730, 86);
            this.cmbIcms.Name = "cmbIcms";
            this.cmbIcms.Size = new System.Drawing.Size(166, 24);
            this.cmbIcms.TabIndex = 15;
            this.cmbIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIcms_KeyPress);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(727, 71);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(145, 16);
            this.label39.TabIndex = 14;
            this.label39.Text = "Identificador de ICMS";
            // 
            // txtNaturezaDeOperacao
            // 
            this.txtNaturezaDeOperacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNaturezaDeOperacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNaturezaDeOperacao.ForeColor = System.Drawing.Color.Black;
            this.txtNaturezaDeOperacao.Location = new System.Drawing.Point(399, 129);
            this.txtNaturezaDeOperacao.Name = "txtNaturezaDeOperacao";
            this.txtNaturezaDeOperacao.Size = new System.Drawing.Size(352, 22);
            this.txtNaturezaDeOperacao.TabIndex = 13;
            this.txtNaturezaDeOperacao.Text = "DEVOLUCAO DE MERCADORIA";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(399, 113);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(151, 16);
            this.label30.TabIndex = 12;
            this.label30.Text = "Natureza de Operação";
            // 
            // cmbDevOperacao
            // 
            this.cmbDevOperacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDevOperacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDevOperacao.ForeColor = System.Drawing.Color.Black;
            this.cmbDevOperacao.FormattingEnabled = true;
            this.cmbDevOperacao.Items.AddRange(new object[] {
            "1 - Operação Interna",
            "2 - Operação Interestadual",
            "3 - Operação com Exterior"});
            this.cmbDevOperacao.Location = new System.Drawing.Point(544, 86);
            this.cmbDevOperacao.Name = "cmbDevOperacao";
            this.cmbDevOperacao.Size = new System.Drawing.Size(180, 24);
            this.cmbDevOperacao.TabIndex = 11;
            this.cmbDevOperacao.SelectedIndexChanged += new System.EventHandler(this.cmbDevOperacao_SelectedIndexChanged);
            this.cmbDevOperacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDevOperacao_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(541, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(174, 16);
            this.label28.TabIndex = 10;
            this.label28.Text = "Identificador de Operação";
            // 
            // txtCnpj
            // 
            this.txtCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCnpj.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCnpj.ForeColor = System.Drawing.Color.Black;
            this.txtCnpj.Location = new System.Drawing.Point(399, 88);
            this.txtCnpj.Mask = "00,000,000/0000-00";
            this.txtCnpj.Name = "txtCnpj";
            this.txtCnpj.Size = new System.Drawing.Size(138, 22);
            this.txtCnpj.TabIndex = 9;
            this.txtCnpj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCnpj_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(396, 69);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(122, 16);
            this.label26.TabIndex = 8;
            this.label26.Text = "CNPJ Destinatário";
            // 
            // btnGerarDevolucao
            // 
            this.btnGerarDevolucao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarDevolucao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerarDevolucao.ForeColor = System.Drawing.Color.Black;
            this.btnGerarDevolucao.Image = ((System.Drawing.Image)(resources.GetObject("btnGerarDevolucao.Image")));
            this.btnGerarDevolucao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerarDevolucao.Location = new System.Drawing.Point(772, 113);
            this.btnGerarDevolucao.Name = "btnGerarDevolucao";
            this.btnGerarDevolucao.Size = new System.Drawing.Size(124, 38);
            this.btnGerarDevolucao.TabIndex = 7;
            this.btnGerarDevolucao.Text = "Gerar NF-e";
            this.btnGerarDevolucao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGerarDevolucao.UseVisualStyleBackColor = true;
            this.btnGerarDevolucao.Click += new System.EventHandler(this.btnGerarDevolucao_Click);
            // 
            // txtComplemento
            // 
            this.txtComplemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComplemento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComplemento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(13, 88);
            this.txtComplemento.Multiline = true;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(380, 63);
            this.txtComplemento.TabIndex = 6;
            this.txtComplemento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComplemento_KeyPress);
            this.txtComplemento.Validated += new System.EventHandler(this.txtComplemento_Validated);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(10, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(178, 16);
            this.label25.TabIndex = 5;
            this.label25.Text = "Informação Complementar";
            // 
            // txtMotivo
            // 
            this.txtMotivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivo.ForeColor = System.Drawing.Color.Black;
            this.txtMotivo.Location = new System.Drawing.Point(399, 46);
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(497, 22);
            this.txtMotivo.TabIndex = 4;
            this.txtMotivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMotivo_KeyPress);
            // 
            // txtChave
            // 
            this.txtChave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChave.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtChave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChave.ForeColor = System.Drawing.Color.Black;
            this.txtChave.Location = new System.Drawing.Point(13, 46);
            this.txtChave.Name = "txtChave";
            this.txtChave.Size = new System.Drawing.Size(380, 22);
            this.txtChave.TabIndex = 3;
            this.txtChave.TextChanged += new System.EventHandler(this.txtChave_TextChanged);
            this.txtChave.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtChave_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(396, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(141, 16);
            this.label24.TabIndex = 2;
            this.label24.Text = "Motivo da Devolução";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(10, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(209, 16);
            this.label23.TabIndex = 1;
            this.label23.Text = "Chave de Acesso de Referência";
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Navy;
            this.lblTotal.Location = new System.Drawing.Point(663, 276);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(244, 21);
            this.lblTotal.TabIndex = 241;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblQtde
            // 
            this.lblQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtde.ForeColor = System.Drawing.Color.Navy;
            this.lblQtde.Location = new System.Drawing.Point(684, 255);
            this.lblQtde.Name = "lblQtde";
            this.lblQtde.Size = new System.Drawing.Size(223, 21);
            this.lblQtde.TabIndex = 240;
            this.lblQtde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgDevolucao
            // 
            this.dgDevolucao.AllowUserToAddRows = false;
            this.dgDevolucao.AllowUserToResizeColumns = false;
            this.dgDevolucao.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgDevolucao.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDevolucao.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDevolucao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgDevolucao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgDevolucao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PROD_ID,
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.PROD_PRECOMPRA,
            this.PROD_QTDE,
            this.DESCONTO,
            this.vBC,
            this.vICMS,
            this.vBCSTRet,
            this.vICMSST,
            this.Lote,
            this.DataFabricacao,
            this.DataValidade,
            this.vBcCOFINS,
            this.vBcPIS,
            this.pAliqCOFINS,
            this.pAliqPIS,
            this.valor_cofins,
            this.valor_pis});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDevolucao.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgDevolucao.GridColor = System.Drawing.Color.Black;
            this.dgDevolucao.Location = new System.Drawing.Point(11, 146);
            this.dgDevolucao.Name = "dgDevolucao";
            this.dgDevolucao.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDevolucao.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgDevolucao.RowHeadersWidth = 20;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgDevolucao.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgDevolucao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDevolucao.Size = new System.Drawing.Size(903, 106);
            this.dgDevolucao.TabIndex = 239;
            this.dgDevolucao.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgDevolucao_RowsRemoved);
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionar.Image")));
            this.btnAdicionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdicionar.Location = new System.Drawing.Point(729, 107);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(121, 33);
            this.btnAdicionar.TabIndex = 238;
            this.btnAdicionar.Text = "Adicionar";
            this.btnAdicionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // txtQtde
            // 
            this.txtQtde.BackColor = System.Drawing.Color.White;
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.ForeColor = System.Drawing.Color.Black;
            this.txtQtde.Location = new System.Drawing.Point(577, 30);
            this.txtQtde.MaxLength = 13;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(55, 22);
            this.txtQtde.TabIndex = 237;
            this.txtQtde.Text = "0";
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(574, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 16);
            this.label21.TabIndex = 236;
            this.label21.Text = "Qtde.";
            // 
            // txtPreco
            // 
            this.txtPreco.BackColor = System.Drawing.Color.White;
            this.txtPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreco.ForeColor = System.Drawing.Color.Black;
            this.txtPreco.Location = new System.Drawing.Point(495, 30);
            this.txtPreco.MaxLength = 13;
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(73, 22);
            this.txtPreco.TabIndex = 235;
            this.txtPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPreco_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(492, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 16);
            this.label22.TabIndex = 234;
            this.label22.Text = "Preço";
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.ForeColor = System.Drawing.Color.Black;
            this.txtDescr.Location = new System.Drawing.Point(172, 30);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(317, 22);
            this.txtDescr.TabIndex = 228;
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            this.txtDescr.Validated += new System.EventHandler(this.txtDescr_Validated);
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.ForeColor = System.Drawing.Color.Black;
            this.txtCodBarras.Location = new System.Drawing.Point(11, 30);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(155, 22);
            this.txtCodBarras.TabIndex = 227;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(169, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 16);
            this.label20.TabIndex = 1;
            this.label20.Text = "Descrição";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(8, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Cód. de Barras";
            // 
            // tpEmitir
            // 
            this.tpEmitir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpEmitir.Controls.Add(this.groupBox3);
            this.tpEmitir.Controls.Add(this.dgBusca);
            this.tpEmitir.Controls.Add(this.groupBox2);
            this.tpEmitir.Location = new System.Drawing.Point(4, 25);
            this.tpEmitir.Name = "tpEmitir";
            this.tpEmitir.Padding = new System.Windows.Forms.Padding(3);
            this.tpEmitir.Size = new System.Drawing.Size(929, 458);
            this.tpEmitir.TabIndex = 0;
            this.tpEmitir.Text = "Gerar NFe";
            this.tpEmitir.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.chkPrincipio);
            this.groupBox3.Controls.Add(this.cmbDocumento);
            this.groupBox3.Controls.Add(this.txtObservacao);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.txtErros);
            this.groupBox3.Controls.Add(this.txtComplementar);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtPedido);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnGerarNFE);
            this.groupBox3.Controls.Add(this.cmbOperacao);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cmbEmissao);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(8, 197);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(910, 253);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Opções de Emissão";
            // 
            // chkPrincipio
            // 
            this.chkPrincipio.AutoSize = true;
            this.chkPrincipio.ForeColor = System.Drawing.Color.Navy;
            this.chkPrincipio.Location = new System.Drawing.Point(605, 107);
            this.chkPrincipio.Name = "chkPrincipio";
            this.chkPrincipio.Size = new System.Drawing.Size(158, 20);
            this.chkPrincipio.TabIndex = 26;
            this.chkPrincipio.Text = "Desc. Principío Ativo";
            this.chkPrincipio.UseVisualStyleBackColor = true;
            // 
            // cmbDocumento
            // 
            this.cmbDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocumento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocumento.ForeColor = System.Drawing.Color.Black;
            this.cmbDocumento.FormattingEnabled = true;
            this.cmbDocumento.Items.AddRange(new object[] {
            "0 - Entrada",
            "1 - Saida",
            "2 - Remessa para Conserto"});
            this.cmbDocumento.Location = new System.Drawing.Point(800, 20);
            this.cmbDocumento.Name = "cmbDocumento";
            this.cmbDocumento.Size = new System.Drawing.Size(98, 24);
            this.cmbDocumento.TabIndex = 25;
            // 
            // txtObservacao
            // 
            this.txtObservacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObservacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtObservacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObservacao.Location = new System.Drawing.Point(12, 107);
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(587, 22);
            this.txtObservacao.TabIndex = 23;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(685, 28);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(112, 16);
            this.label29.TabIndex = 24;
            this.label29.Text = "Tipo Documento";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(9, 90);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(191, 16);
            this.label27.TabIndex = 22;
            this.label27.Text = "Observação para uso Interno";
            // 
            // txtErros
            // 
            this.txtErros.BackColor = System.Drawing.Color.White;
            this.txtErros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtErros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtErros.ForeColor = System.Drawing.Color.Black;
            this.txtErros.Location = new System.Drawing.Point(12, 133);
            this.txtErros.Multiline = true;
            this.txtErros.Name = "txtErros";
            this.txtErros.ReadOnly = true;
            this.txtErros.Size = new System.Drawing.Size(751, 114);
            this.txtErros.TabIndex = 21;
            // 
            // txtComplementar
            // 
            this.txtComplementar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComplementar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComplementar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplementar.Location = new System.Drawing.Point(173, 66);
            this.txtComplementar.Name = "txtComplementar";
            this.txtComplementar.Size = new System.Drawing.Size(725, 22);
            this.txtComplementar.TabIndex = 20;
            this.txtComplementar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComplementar_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(170, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(178, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Informação Complementar";
            // 
            // txtPedido
            // 
            this.txtPedido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPedido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPedido.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPedido.Location = new System.Drawing.Point(12, 66);
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.Size = new System.Drawing.Size(155, 22);
            this.txtPedido.TabIndex = 18;
            this.txtPedido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPedido_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(9, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 16);
            this.label7.TabIndex = 17;
            this.label7.Text = "Número do Pedido";
            // 
            // btnGerarNFE
            // 
            this.btnGerarNFE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarNFE.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerarNFE.Image = ((System.Drawing.Image)(resources.GetObject("btnGerarNFE.Image")));
            this.btnGerarNFE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerarNFE.Location = new System.Drawing.Point(777, 94);
            this.btnGerarNFE.Name = "btnGerarNFE";
            this.btnGerarNFE.Size = new System.Drawing.Size(121, 56);
            this.btnGerarNFE.TabIndex = 4;
            this.btnGerarNFE.Text = "Gerar NF-e";
            this.btnGerarNFE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGerarNFE.UseVisualStyleBackColor = true;
            this.btnGerarNFE.Click += new System.EventHandler(this.btnGerarNFE_Click);
            // 
            // cmbOperacao
            // 
            this.cmbOperacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOperacao.ForeColor = System.Drawing.Color.Black;
            this.cmbOperacao.FormattingEnabled = true;
            this.cmbOperacao.Items.AddRange(new object[] {
            "1 - Operação Interna",
            "2 - Operação Interestadual",
            "3 - Operação com Exterior"});
            this.cmbOperacao.Location = new System.Drawing.Point(499, 20);
            this.cmbOperacao.Name = "cmbOperacao";
            this.cmbOperacao.Size = new System.Drawing.Size(180, 24);
            this.cmbOperacao.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(319, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Identificador de Operação";
            // 
            // cmbEmissao
            // 
            this.cmbEmissao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmissao.ForeColor = System.Drawing.Color.Black;
            this.cmbEmissao.FormattingEnabled = true;
            this.cmbEmissao.Items.AddRange(new object[] {
            "1-Emissão normal"});
            this.cmbEmissao.Location = new System.Drawing.Point(127, 20);
            this.cmbEmissao.Name = "cmbEmissao";
            this.cmbEmissao.Size = new System.Drawing.Size(180, 24);
            this.cmbEmissao.TabIndex = 1;
            this.cmbEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEmissao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(9, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tipo de Emissão";
            // 
            // dgBusca
            // 
            this.dgBusca.AllowUserToAddRows = false;
            this.dgBusca.AllowUserToDeleteRows = false;
            this.dgBusca.AllowUserToResizeColumns = false;
            this.dgBusca.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBusca.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgBusca.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgBusca.BackgroundColor = System.Drawing.Color.White;
            this.dgBusca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBusca.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VENDA_EMISSAO,
            this.VENDA_ID,
            this.CF_NOME,
            this.PRODUTOS,
            this.QTDE_PRODUTO,
            this.VENDA_DATA_HORA,
            this.CF_DOCTO,
            this.VENDA_TOTAL});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBusca.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgBusca.GridColor = System.Drawing.Color.Black;
            this.dgBusca.Location = new System.Drawing.Point(8, 78);
            this.dgBusca.Name = "dgBusca";
            this.dgBusca.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBusca.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgBusca.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBusca.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgBusca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBusca.Size = new System.Drawing.Size(910, 119);
            this.dgBusca.TabIndex = 2;
            // 
            // VENDA_EMISSAO
            // 
            this.VENDA_EMISSAO.DataPropertyName = "VENDA_EMISSAO";
            this.VENDA_EMISSAO.HeaderText = "Data Emissão";
            this.VENDA_EMISSAO.Name = "VENDA_EMISSAO";
            this.VENDA_EMISSAO.ReadOnly = true;
            this.VENDA_EMISSAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_EMISSAO.Width = 110;
            // 
            // VENDA_ID
            // 
            this.VENDA_ID.DataPropertyName = "VENDA_ID";
            this.VENDA_ID.HeaderText = "ID Venda";
            this.VENDA_ID.Name = "VENDA_ID";
            this.VENDA_ID.ReadOnly = true;
            this.VENDA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_ID.Width = 80;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome Cliente";
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_NOME.Width = 350;
            // 
            // PRODUTOS
            // 
            this.PRODUTOS.DataPropertyName = "PRODUTOS";
            this.PRODUTOS.HeaderText = "Qtde Itens";
            this.PRODUTOS.Name = "PRODUTOS";
            this.PRODUTOS.ReadOnly = true;
            this.PRODUTOS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRODUTOS.Width = 110;
            // 
            // QTDE_PRODUTO
            // 
            this.QTDE_PRODUTO.DataPropertyName = "QTDE_PRODUTO";
            this.QTDE_PRODUTO.HeaderText = "Qtde. Produtos";
            this.QTDE_PRODUTO.Name = "QTDE_PRODUTO";
            this.QTDE_PRODUTO.ReadOnly = true;
            this.QTDE_PRODUTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QTDE_PRODUTO.Width = 120;
            // 
            // VENDA_DATA_HORA
            // 
            this.VENDA_DATA_HORA.DataPropertyName = "VENDA_DATA_HORA";
            this.VENDA_DATA_HORA.HeaderText = "Data/Hora";
            this.VENDA_DATA_HORA.Name = "VENDA_DATA_HORA";
            this.VENDA_DATA_HORA.ReadOnly = true;
            this.VENDA_DATA_HORA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_DATA_HORA.Visible = false;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CF_DOCTO";
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_DOCTO.Visible = false;
            // 
            // VENDA_TOTAL
            // 
            this.VENDA_TOTAL.DataPropertyName = "VENDA_TOTAL";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.VENDA_TOTAL.DefaultCellStyle = dataGridViewCellStyle8;
            this.VENDA_TOTAL.HeaderText = "Total da Nota";
            this.VENDA_TOTAL.Name = "VENDA_TOTAL";
            this.VENDA_TOTAL.ReadOnly = true;
            this.VENDA_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_TOTAL.Width = 120;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtIdCliente);
            this.groupBox2.Controls.Add(this.lblID);
            this.groupBox2.Controls.Add(this.chkUnificar);
            this.groupBox2.Controls.Add(this.dtFinicial);
            this.groupBox2.Controls.Add(this.lblFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.lblInicial);
            this.groupBox2.Controls.Add(this.dtData);
            this.groupBox2.Controls.Add(this.btnBuscarVenda);
            this.groupBox2.Controls.Add(this.txtCliente);
            this.groupBox2.Controls.Add(this.txtVendaID);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(910, 71);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtro";
            // 
            // txtIdCliente
            // 
            this.txtIdCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIdCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCliente.Location = new System.Drawing.Point(734, 40);
            this.txtIdCliente.Name = "txtIdCliente";
            this.txtIdCliente.Size = new System.Drawing.Size(70, 22);
            this.txtIdCliente.TabIndex = 14;
            this.txtIdCliente.Visible = false;
            this.txtIdCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdCliente_KeyPress);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.ForeColor = System.Drawing.Color.Navy;
            this.lblID.Location = new System.Drawing.Point(736, 22);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(21, 16);
            this.lblID.TabIndex = 13;
            this.lblID.Text = "ID";
            this.lblID.Visible = false;
            // 
            // chkUnificar
            // 
            this.chkUnificar.AutoSize = true;
            this.chkUnificar.ForeColor = System.Drawing.Color.Navy;
            this.chkUnificar.Location = new System.Drawing.Point(430, 40);
            this.chkUnificar.Name = "chkUnificar";
            this.chkUnificar.Size = new System.Drawing.Size(126, 20);
            this.chkUnificar.TabIndex = 12;
            this.chkUnificar.Text = "Unificar Vendas";
            this.chkUnificar.UseVisualStyleBackColor = true;
            this.chkUnificar.CheckedChanged += new System.EventHandler(this.chkUnificar_CheckedChanged);
            // 
            // dtFinicial
            // 
            this.dtFinicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinicial.Location = new System.Drawing.Point(649, 40);
            this.dtFinicial.Mask = "00/00/0000";
            this.dtFinicial.Name = "dtFinicial";
            this.dtFinicial.Size = new System.Drawing.Size(81, 22);
            this.dtFinicial.TabIndex = 11;
            this.dtFinicial.ValidatingType = typeof(System.DateTime);
            this.dtFinicial.Visible = false;
            this.dtFinicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinicial_KeyPress);
            // 
            // lblFinal
            // 
            this.lblFinal.AutoSize = true;
            this.lblFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinal.ForeColor = System.Drawing.Color.Navy;
            this.lblFinal.Location = new System.Drawing.Point(646, 22);
            this.lblFinal.Name = "lblFinal";
            this.lblFinal.Size = new System.Drawing.Size(73, 16);
            this.lblFinal.TabIndex = 10;
            this.lblFinal.Text = "Data Final";
            this.lblFinal.Visible = false;
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Location = new System.Drawing.Point(557, 40);
            this.dtInicial.Mask = "00/00/0000";
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(81, 22);
            this.dtInicial.TabIndex = 9;
            this.dtInicial.ValidatingType = typeof(System.DateTime);
            this.dtInicial.Visible = false;
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // lblInicial
            // 
            this.lblInicial.AutoSize = true;
            this.lblInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInicial.ForeColor = System.Drawing.Color.Navy;
            this.lblInicial.Location = new System.Drawing.Point(554, 22);
            this.lblInicial.Name = "lblInicial";
            this.lblInicial.Size = new System.Drawing.Size(80, 16);
            this.lblInicial.TabIndex = 8;
            this.lblInicial.Text = "Data Inicial";
            this.lblInicial.Visible = false;
            // 
            // dtData
            // 
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Location = new System.Drawing.Point(12, 41);
            this.dtData.Mask = "00/00/0000";
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(81, 22);
            this.dtData.TabIndex = 7;
            this.dtData.ValidatingType = typeof(System.DateTime);
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // btnBuscarVenda
            // 
            this.btnBuscarVenda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarVenda.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarVenda.Image")));
            this.btnBuscarVenda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscarVenda.Location = new System.Drawing.Point(809, 21);
            this.btnBuscarVenda.Name = "btnBuscarVenda";
            this.btnBuscarVenda.Size = new System.Drawing.Size(89, 41);
            this.btnBuscarVenda.TabIndex = 6;
            this.btnBuscarVenda.Text = "Buscar";
            this.btnBuscarVenda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscarVenda.UseVisualStyleBackColor = true;
            this.btnBuscarVenda.Click += new System.EventHandler(this.btnBuscarVenda_Click);
            // 
            // txtCliente
            // 
            this.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCliente.Location = new System.Drawing.Point(184, 41);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(234, 22);
            this.txtCliente.TabIndex = 5;
            this.txtCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCliente_KeyPress);
            // 
            // txtVendaID
            // 
            this.txtVendaID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendaID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendaID.Location = new System.Drawing.Point(100, 41);
            this.txtVendaID.Name = "txtVendaID";
            this.txtVendaID.Size = new System.Drawing.Size(76, 22);
            this.txtVendaID.TabIndex = 4;
            this.txtVendaID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVendaID_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(181, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cliente/Fornecedor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(97, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Venda ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data";
            // 
            // tpGeradas
            // 
            this.tpGeradas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpGeradas.Controls.Add(this.groupBox5);
            this.tpGeradas.Controls.Add(this.dgEmitidos);
            this.tpGeradas.Controls.Add(this.groupBox4);
            this.tpGeradas.Location = new System.Drawing.Point(4, 25);
            this.tpGeradas.Name = "tpGeradas";
            this.tpGeradas.Size = new System.Drawing.Size(929, 458);
            this.tpGeradas.TabIndex = 10;
            this.tpGeradas.Text = "Notas Emitidas";
            this.tpGeradas.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.btnReimpressao);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(8, 276);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(910, 177);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Opcões ";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.btnCancelar);
            this.groupBox7.Controls.Add(this.txtMotivoCancelamento);
            this.groupBox7.Location = new System.Drawing.Point(12, 109);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(892, 62);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Cancelamento";
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(728, 12);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(158, 44);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar NF-e";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtMotivoCancelamento
            // 
            this.txtMotivoCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMotivoCancelamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivoCancelamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoCancelamento.ForeColor = System.Drawing.Color.Black;
            this.txtMotivoCancelamento.Location = new System.Drawing.Point(6, 21);
            this.txtMotivoCancelamento.Multiline = true;
            this.txtMotivoCancelamento.Name = "txtMotivoCancelamento";
            this.txtMotivoCancelamento.Size = new System.Drawing.Size(716, 35);
            this.txtMotivoCancelamento.TabIndex = 2;
            this.txtMotivoCancelamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMotivoCancelamento_KeyPress);
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.btnImpressaoCCe);
            this.groupBox6.Controls.Add(this.btnGerarCCe);
            this.groupBox6.Controls.Add(this.txtMotivoCCe);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Location = new System.Drawing.Point(177, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(727, 99);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "CC-e";
            // 
            // btnImpressaoCCe
            // 
            this.btnImpressaoCCe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImpressaoCCe.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImpressaoCCe.Image = ((System.Drawing.Image)(resources.GetObject("btnImpressaoCCe.Image")));
            this.btnImpressaoCCe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImpressaoCCe.Location = new System.Drawing.Point(563, 56);
            this.btnImpressaoCCe.Name = "btnImpressaoCCe";
            this.btnImpressaoCCe.Size = new System.Drawing.Size(158, 36);
            this.btnImpressaoCCe.TabIndex = 3;
            this.btnImpressaoCCe.Text = "Reimpressão CC-e";
            this.btnImpressaoCCe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImpressaoCCe.UseVisualStyleBackColor = true;
            this.btnImpressaoCCe.Click += new System.EventHandler(this.btnImpressaoCCe_Click);
            // 
            // btnGerarCCe
            // 
            this.btnGerarCCe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarCCe.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerarCCe.Image = ((System.Drawing.Image)(resources.GetObject("btnGerarCCe.Image")));
            this.btnGerarCCe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerarCCe.Location = new System.Drawing.Point(563, 16);
            this.btnGerarCCe.Name = "btnGerarCCe";
            this.btnGerarCCe.Size = new System.Drawing.Size(158, 36);
            this.btnGerarCCe.TabIndex = 2;
            this.btnGerarCCe.Text = "Gerar CC-e";
            this.btnGerarCCe.UseVisualStyleBackColor = true;
            this.btnGerarCCe.Click += new System.EventHandler(this.btnGerarCCe_Click);
            // 
            // txtMotivoCCe
            // 
            this.txtMotivoCCe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMotivoCCe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivoCCe.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotivoCCe.ForeColor = System.Drawing.Color.Black;
            this.txtMotivoCCe.Location = new System.Drawing.Point(11, 40);
            this.txtMotivoCCe.Multiline = true;
            this.txtMotivoCCe.Name = "txtMotivoCCe";
            this.txtMotivoCCe.Size = new System.Drawing.Size(546, 52);
            this.txtMotivoCCe.TabIndex = 1;
            this.txtMotivoCCe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMotivoCCe_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "Motivo da Correção";
            // 
            // btnReimpressao
            // 
            this.btnReimpressao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReimpressao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimpressao.Image = ((System.Drawing.Image)(resources.GetObject("btnReimpressao.Image")));
            this.btnReimpressao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReimpressao.Location = new System.Drawing.Point(12, 21);
            this.btnReimpressao.Name = "btnReimpressao";
            this.btnReimpressao.Size = new System.Drawing.Size(158, 82);
            this.btnReimpressao.TabIndex = 1;
            this.btnReimpressao.Text = "Reimpressão NF-e";
            this.btnReimpressao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReimpressao.UseVisualStyleBackColor = true;
            this.btnReimpressao.Click += new System.EventHandler(this.btnReimpressao_Click);
            // 
            // dgEmitidos
            // 
            this.dgEmitidos.AllowUserToAddRows = false;
            this.dgEmitidos.AllowUserToDeleteRows = false;
            this.dgEmitidos.AllowUserToResizeColumns = false;
            this.dgEmitidos.AllowUserToResizeRows = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEmitidos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgEmitidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEmitidos.BackgroundColor = System.Drawing.Color.White;
            this.dgEmitidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEmitidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.VENDA,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.CHAVE_NFE,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.NFE_DATA_CANCELAMENTO,
            this.NFE_DEVOLUCAO,
            this.OPCADASTRO,
            this.PATH,
            this.NUM_PROTOCOLO});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEmitidos.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgEmitidos.GridColor = System.Drawing.Color.Black;
            this.dgEmitidos.Location = new System.Drawing.Point(8, 62);
            this.dgEmitidos.Name = "dgEmitidos";
            this.dgEmitidos.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEmitidos.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgEmitidos.RowHeadersVisible = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEmitidos.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgEmitidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEmitidos.Size = new System.Drawing.Size(910, 214);
            this.dgEmitidos.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NFE_DATA";
            this.dataGridViewTextBoxColumn1.HeaderText = "Data Emissão";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDA
            // 
            this.VENDA.DataPropertyName = "VENDA_ID";
            this.VENDA.HeaderText = "ID Venda";
            this.VENDA.Name = "VENDA";
            this.VENDA.ReadOnly = true;
            this.VENDA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA.Visible = false;
            this.VENDA.Width = 80;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CF_NOME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Nome Cliente";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 250;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "STATUS";
            this.dataGridViewTextBoxColumn4.HeaderText = "Status";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 60;
            // 
            // CHAVE_NFE
            // 
            this.CHAVE_NFE.DataPropertyName = "CHAVE_NFE";
            this.CHAVE_NFE.HeaderText = "Chave de Acesso";
            this.CHAVE_NFE.Name = "CHAVE_NFE";
            this.CHAVE_NFE.ReadOnly = true;
            this.CHAVE_NFE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CHAVE_NFE.Width = 280;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "NUM_LOTE";
            this.dataGridViewTextBoxColumn6.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NUM_RECIBO";
            this.dataGridViewTextBoxColumn7.HeaderText = "Num. Recibo";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "NFE_CANCELADA";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn8.HeaderText = "Cancelada";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NFE_DATA_CANCELAMENTO
            // 
            this.NFE_DATA_CANCELAMENTO.DataPropertyName = "NFE_DATA_CANCELAMENTO";
            this.NFE_DATA_CANCELAMENTO.HeaderText = "Data Can.";
            this.NFE_DATA_CANCELAMENTO.Name = "NFE_DATA_CANCELAMENTO";
            this.NFE_DATA_CANCELAMENTO.ReadOnly = true;
            this.NFE_DATA_CANCELAMENTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NFE_DEVOLUCAO
            // 
            this.NFE_DEVOLUCAO.DataPropertyName = "NFE_DEVOLUCAO";
            this.NFE_DEVOLUCAO.HeaderText = "Devolução";
            this.NFE_DEVOLUCAO.Name = "NFE_DEVOLUCAO";
            this.NFE_DEVOLUCAO.ReadOnly = true;
            this.NFE_DEVOLUCAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Operador";
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PATH
            // 
            this.PATH.DataPropertyName = "PATH";
            this.PATH.HeaderText = "PAHT";
            this.PATH.Name = "PATH";
            this.PATH.ReadOnly = true;
            this.PATH.Visible = false;
            // 
            // NUM_PROTOCOLO
            // 
            this.NUM_PROTOCOLO.DataPropertyName = "NUM_PROTOCOLO";
            this.NUM_PROTOCOLO.HeaderText = "Protocolo";
            this.NUM_PROTOCOLO.Name = "NUM_PROTOCOLO";
            this.NUM_PROTOCOLO.ReadOnly = true;
            this.NUM_PROTOCOLO.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.txtBData);
            this.groupBox4.Controls.Add(this.btnEmitidas);
            this.groupBox4.Controls.Add(this.txtBChave);
            this.groupBox4.Controls.Add(this.txtBVendaId);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(8, 1);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(910, 59);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtro";
            // 
            // txtBData
            // 
            this.txtBData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBData.Location = new System.Drawing.Point(12, 32);
            this.txtBData.Mask = "00/00/0000";
            this.txtBData.Name = "txtBData";
            this.txtBData.Size = new System.Drawing.Size(122, 22);
            this.txtBData.TabIndex = 7;
            this.txtBData.ValidatingType = typeof(System.DateTime);
            this.txtBData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBData_KeyPress);
            // 
            // btnEmitidas
            // 
            this.btnEmitidas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmitidas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmitidas.Image = ((System.Drawing.Image)(resources.GetObject("btnEmitidas.Image")));
            this.btnEmitidas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmitidas.Location = new System.Drawing.Point(586, 13);
            this.btnEmitidas.Name = "btnEmitidas";
            this.btnEmitidas.Size = new System.Drawing.Size(104, 41);
            this.btnEmitidas.TabIndex = 6;
            this.btnEmitidas.Text = "Buscar";
            this.btnEmitidas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEmitidas.UseVisualStyleBackColor = true;
            this.btnEmitidas.Click += new System.EventHandler(this.btnEmitidas_Click);
            // 
            // txtBChave
            // 
            this.txtBChave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBChave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBChave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBChave.Location = new System.Drawing.Point(257, 32);
            this.txtBChave.Name = "txtBChave";
            this.txtBChave.Size = new System.Drawing.Size(318, 22);
            this.txtBChave.TabIndex = 5;
            this.txtBChave.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBChave_KeyPress);
            // 
            // txtBVendaId
            // 
            this.txtBVendaId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBVendaId.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBVendaId.Location = new System.Drawing.Point(141, 32);
            this.txtBVendaId.Name = "txtBVendaId";
            this.txtBVendaId.Size = new System.Drawing.Size(103, 22);
            this.txtBVendaId.TabIndex = 4;
            this.txtBVendaId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBVendaId_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(254, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Chave de Acesso";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(138, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 16);
            this.label11.TabIndex = 1;
            this.label11.Text = "Venda ID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(9, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Data";
            // 
            // tpInutilizacao
            // 
            this.tpInutilizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpInutilizacao.Controls.Add(this.groupBox8);
            this.tpInutilizacao.Location = new System.Drawing.Point(4, 25);
            this.tpInutilizacao.Name = "tpInutilizacao";
            this.tpInutilizacao.Size = new System.Drawing.Size(929, 458);
            this.tpInutilizacao.TabIndex = 2;
            this.tpInutilizacao.Text = "Inutilização";
            this.tpInutilizacao.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.txtRetorno);
            this.groupBox8.Controls.Add(this.btnInutilizar);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.txtJustificativa);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.txtFinal);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.txtInicial);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.txtAno);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(8, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(910, 446);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Dados da Inutilização";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(12, 296);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 16);
            this.label18.TabIndex = 10;
            this.label18.Text = "Retorno";
            // 
            // txtRetorno
            // 
            this.txtRetorno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRetorno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRetorno.ForeColor = System.Drawing.Color.Black;
            this.txtRetorno.Location = new System.Drawing.Point(15, 315);
            this.txtRetorno.Multiline = true;
            this.txtRetorno.Name = "txtRetorno";
            this.txtRetorno.Size = new System.Drawing.Size(351, 84);
            this.txtRetorno.TabIndex = 9;
            // 
            // btnInutilizar
            // 
            this.btnInutilizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInutilizar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInutilizar.Image = ((System.Drawing.Image)(resources.GetObject("btnInutilizar.Image")));
            this.btnInutilizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInutilizar.Location = new System.Drawing.Point(223, 222);
            this.btnInutilizar.Name = "btnInutilizar";
            this.btnInutilizar.Size = new System.Drawing.Size(143, 59);
            this.btnInutilizar.TabIndex = 8;
            this.btnInutilizar.Text = "Inutilização";
            this.btnInutilizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInutilizar.UseVisualStyleBackColor = true;
            this.btnInutilizar.Click += new System.EventHandler(this.btnInutilizar_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(74, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ano:";
            // 
            // txtJustificativa
            // 
            this.txtJustificativa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtJustificativa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJustificativa.ForeColor = System.Drawing.Color.Black;
            this.txtJustificativa.Location = new System.Drawing.Point(117, 119);
            this.txtJustificativa.Multiline = true;
            this.txtJustificativa.Name = "txtJustificativa";
            this.txtJustificativa.Size = new System.Drawing.Size(249, 97);
            this.txtJustificativa.TabIndex = 7;
            this.txtJustificativa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJustificativa_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 16);
            this.label15.TabIndex = 1;
            this.label15.Text = "Número Inicial:";
            // 
            // txtFinal
            // 
            this.txtFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinal.ForeColor = System.Drawing.Color.Black;
            this.txtFinal.Location = new System.Drawing.Point(117, 86);
            this.txtFinal.Name = "txtFinal";
            this.txtFinal.Size = new System.Drawing.Size(100, 22);
            this.txtFinal.TabIndex = 6;
            this.txtFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFinal_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(12, 92);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 16);
            this.label16.TabIndex = 2;
            this.label16.Text = "Número Final:";
            // 
            // txtInicial
            // 
            this.txtInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicial.ForeColor = System.Drawing.Color.Black;
            this.txtInicial.Location = new System.Drawing.Point(117, 52);
            this.txtInicial.Name = "txtInicial";
            this.txtInicial.Size = new System.Drawing.Size(100, 22);
            this.txtInicial.TabIndex = 5;
            this.txtInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInicial_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(23, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 16);
            this.label17.TabIndex = 3;
            this.label17.Text = "Justificativa:";
            // 
            // txtAno
            // 
            this.txtAno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAno.ForeColor = System.Drawing.Color.Black;
            this.txtAno.Location = new System.Drawing.Point(117, 21);
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(100, 22);
            this.txtAno.TabIndex = 4;
            this.txtAno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAno_KeyPress);
            // 
            // tpManifestacao
            // 
            this.tpManifestacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpManifestacao.Controls.Add(this.groupBox9);
            this.tpManifestacao.Location = new System.Drawing.Point(4, 25);
            this.tpManifestacao.Name = "tpManifestacao";
            this.tpManifestacao.Size = new System.Drawing.Size(929, 458);
            this.tpManifestacao.TabIndex = 8;
            this.tpManifestacao.Text = "Manifestação do Destinatário";
            this.tpManifestacao.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.txtRetornoManif);
            this.groupBox9.Controls.Add(this.txtManifestacaoJus);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this.cbEvento);
            this.groupBox9.Controls.Add(this.btnManifestacao);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this.chNfeDest);
            this.groupBox9.Controls.Add(this.label45);
            this.groupBox9.Controls.Add(this.label44);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(8, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(910, 446);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Dados da Manifestação";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(9, 286);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 16);
            this.label19.TabIndex = 18;
            this.label19.Text = "Retorno";
            // 
            // txtRetornoManif
            // 
            this.txtRetornoManif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRetornoManif.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRetornoManif.ForeColor = System.Drawing.Color.Black;
            this.txtRetornoManif.Location = new System.Drawing.Point(12, 305);
            this.txtRetornoManif.Multiline = true;
            this.txtRetornoManif.Name = "txtRetornoManif";
            this.txtRetornoManif.Size = new System.Drawing.Size(452, 74);
            this.txtRetornoManif.TabIndex = 17;
            // 
            // txtManifestacaoJus
            // 
            this.txtManifestacaoJus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtManifestacaoJus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtManifestacaoJus.ForeColor = System.Drawing.Color.Black;
            this.txtManifestacaoJus.Location = new System.Drawing.Point(99, 95);
            this.txtManifestacaoJus.Multiline = true;
            this.txtManifestacaoJus.Name = "txtManifestacaoJus";
            this.txtManifestacaoJus.Size = new System.Drawing.Size(365, 74);
            this.txtManifestacaoJus.TabIndex = 16;
            this.txtManifestacaoJus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtManifestacaoJus_KeyPress);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(38, 35);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(55, 16);
            this.label47.TabIndex = 15;
            this.label47.Text = "Evento:";
            // 
            // cbEvento
            // 
            this.cbEvento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEvento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEvento.ForeColor = System.Drawing.Color.Black;
            this.cbEvento.FormattingEnabled = true;
            this.cbEvento.Items.AddRange(new object[] {
            "Confirmação da Operação",
            "Ciência da Operação",
            "Desconhecimento da Operação",
            "Operação não Realizada"});
            this.cbEvento.Location = new System.Drawing.Point(99, 27);
            this.cbEvento.Name = "cbEvento";
            this.cbEvento.Size = new System.Drawing.Size(365, 24);
            this.cbEvento.TabIndex = 14;
            this.cbEvento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbEvento_KeyPress);
            // 
            // btnManifestacao
            // 
            this.btnManifestacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManifestacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManifestacao.Image = ((System.Drawing.Image)(resources.GetObject("btnManifestacao.Image")));
            this.btnManifestacao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManifestacao.Location = new System.Drawing.Point(284, 213);
            this.btnManifestacao.Name = "btnManifestacao";
            this.btnManifestacao.Size = new System.Drawing.Size(180, 62);
            this.btnManifestacao.TabIndex = 13;
            this.btnManifestacao.Text = "Gerar Manifestação";
            this.btnManifestacao.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnManifestacao.UseVisualStyleBackColor = true;
            this.btnManifestacao.Click += new System.EventHandler(this.btnManifestacao_Click);
            // 
            // label46
            // 
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(9, 172);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(455, 48);
            this.label46.TabIndex = 12;
            this.label46.Text = "Obs: este campo deve ser informado somente no evento de Operação não realizada.";
            // 
            // chNfeDest
            // 
            this.chNfeDest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chNfeDest.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chNfeDest.ForeColor = System.Drawing.Color.Black;
            this.chNfeDest.Location = new System.Drawing.Point(99, 64);
            this.chNfeDest.Name = "chNfeDest";
            this.chNfeDest.Size = new System.Drawing.Size(365, 22);
            this.chNfeDest.TabIndex = 10;
            this.chNfeDest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chNfeDest_KeyPress);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(6, 101);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(87, 16);
            this.label45.TabIndex = 9;
            this.label45.Text = "Justificativa:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(8, 70);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(85, 16);
            this.label44.TabIndex = 8;
            this.label44.Text = "Chave NF-e:";
            // 
            // tpStatus
            // 
            this.tpStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tpStatus.Controls.Add(this.lblStatus);
            this.tpStatus.Controls.Add(this.btnStatus);
            this.tpStatus.Location = new System.Drawing.Point(4, 25);
            this.tpStatus.Name = "tpStatus";
            this.tpStatus.Size = new System.Drawing.Size(929, 458);
            this.tpStatus.TabIndex = 3;
            this.tpStatus.Text = "Status Serviço";
            this.tpStatus.UseVisualStyleBackColor = true;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(182, 22);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(724, 65);
            this.lblStatus.TabIndex = 1;
            // 
            // btnStatus
            // 
            this.btnStatus.BackColor = System.Drawing.Color.White;
            this.btnStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStatus.Image = ((System.Drawing.Image)(resources.GetObject("btnStatus.Image")));
            this.btnStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatus.Location = new System.Drawing.Point(16, 22);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(160, 65);
            this.btnStatus.TabIndex = 0;
            this.btnStatus.Text = "Status Serviço";
            this.btnStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStatus.UseVisualStyleBackColor = false;
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(247, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Emissão de Nota Fiscal Eletrônica";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(954, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // PROD_ID
            // 
            this.PROD_ID.DataPropertyName = "PROD_ID";
            this.PROD_ID.HeaderText = "ID";
            this.PROD_ID.Name = "PROD_ID";
            this.PROD_ID.ReadOnly = true;
            this.PROD_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PROD_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_ID.Visible = false;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. de Barras";
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_CODIGO.Width = 120;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESCR.Width = 400;
            // 
            // PROD_PRECOMPRA
            // 
            this.PROD_PRECOMPRA.DataPropertyName = "PROD_PRECOMPRA";
            this.PROD_PRECOMPRA.HeaderText = "Preço de Custo";
            this.PROD_PRECOMPRA.Name = "PROD_PRECOMPRA";
            this.PROD_PRECOMPRA.ReadOnly = true;
            this.PROD_PRECOMPRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_PRECOMPRA.Width = 150;
            // 
            // PROD_QTDE
            // 
            this.PROD_QTDE.DataPropertyName = "PROD_QTDE";
            this.PROD_QTDE.HeaderText = "Qtde";
            this.PROD_QTDE.Name = "PROD_QTDE";
            this.PROD_QTDE.ReadOnly = true;
            this.PROD_QTDE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DESCONTO
            // 
            this.DESCONTO.DataPropertyName = "DESCONTO";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.DESCONTO.DefaultCellStyle = dataGridViewCellStyle3;
            this.DESCONTO.HeaderText = "Desconto";
            this.DESCONTO.Name = "DESCONTO";
            this.DESCONTO.ReadOnly = true;
            this.DESCONTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // vBC
            // 
            this.vBC.DataPropertyName = "vBC";
            this.vBC.HeaderText = "Valor Base ICMS";
            this.vBC.Name = "vBC";
            this.vBC.ReadOnly = true;
            // 
            // vICMS
            // 
            this.vICMS.DataPropertyName = "vICMS";
            this.vICMS.HeaderText = "Vl. Icms";
            this.vICMS.Name = "vICMS";
            this.vICMS.ReadOnly = true;
            // 
            // vBCSTRet
            // 
            this.vBCSTRet.DataPropertyName = "vBCSTRet";
            this.vBCSTRet.HeaderText = "Vl. BC Icms R.A";
            this.vBCSTRet.Name = "vBCSTRet";
            this.vBCSTRet.ReadOnly = true;
            // 
            // vICMSST
            // 
            this.vICMSST.DataPropertyName = "vICMSST";
            this.vICMSST.HeaderText = "Vl. ICMS ST";
            this.vICMSST.Name = "vICMSST";
            this.vICMSST.ReadOnly = true;
            // 
            // Lote
            // 
            this.Lote.DataPropertyName = "Lote";
            this.Lote.HeaderText = "Lote";
            this.Lote.Name = "Lote";
            this.Lote.ReadOnly = true;
            // 
            // DataFabricacao
            // 
            this.DataFabricacao.DataPropertyName = "DataFabricacao";
            this.DataFabricacao.HeaderText = "Data Fabricação";
            this.DataFabricacao.Name = "DataFabricacao";
            this.DataFabricacao.ReadOnly = true;
            // 
            // DataValidade
            // 
            this.DataValidade.DataPropertyName = "DataValidade";
            this.DataValidade.HeaderText = "Data Validade";
            this.DataValidade.Name = "DataValidade";
            this.DataValidade.ReadOnly = true;
            // 
            // vBcCOFINS
            // 
            this.vBcCOFINS.DataPropertyName = "VBCCOFINS";
            this.vBcCOFINS.HeaderText = "BC COFINS";
            this.vBcCOFINS.Name = "vBcCOFINS";
            this.vBcCOFINS.ReadOnly = true;
            // 
            // vBcPIS
            // 
            this.vBcPIS.DataPropertyName = "VBCPIS";
            this.vBcPIS.HeaderText = "BC PIS";
            this.vBcPIS.Name = "vBcPIS";
            this.vBcPIS.ReadOnly = true;
            // 
            // pAliqCOFINS
            // 
            this.pAliqCOFINS.DataPropertyName = "PALIQCOFINS";
            this.pAliqCOFINS.HeaderText = "Aliquota COFINS";
            this.pAliqCOFINS.Name = "pAliqCOFINS";
            this.pAliqCOFINS.ReadOnly = true;
            this.pAliqCOFINS.Width = 150;
            // 
            // pAliqPIS
            // 
            this.pAliqPIS.DataPropertyName = "PALIQPIS";
            this.pAliqPIS.HeaderText = "Aliquota PIS";
            this.pAliqPIS.Name = "pAliqPIS";
            this.pAliqPIS.ReadOnly = true;
            // 
            // valor_cofins
            // 
            this.valor_cofins.DataPropertyName = "valor_cofins";
            this.valor_cofins.HeaderText = "Valor Cofins";
            this.valor_cofins.Name = "valor_cofins";
            this.valor_cofins.ReadOnly = true;
            // 
            // valor_pis
            // 
            this.valor_pis.DataPropertyName = "valor_pis";
            this.valor_pis.HeaderText = "Valor PIS";
            this.valor_pis.Name = "valor_pis";
            this.valor_pis.ReadOnly = true;
            // 
            // frmVenNFE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenNFE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmVenNFE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmVenNFE_Load);
            this.Shown += new System.EventHandler(this.frmVenNFE_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabNFe.ResumeLayout(false);
            this.tpConsultaCadastro.ResumeLayout(false);
            this.tpDevolucao.ResumeLayout(false);
            this.tpDevolucao.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDevolucao)).EndInit();
            this.tpEmitir.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBusca)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tpGeradas.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEmitidos)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tpInutilizacao.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tpManifestacao.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tpStatus.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tabNFe;
        private System.Windows.Forms.TabPage tpConsultaCadastro;
        private System.Windows.Forms.TabPage tpEmitir;
        private System.Windows.Forms.TabPage tpGeradas;
        private System.Windows.Forms.TabPage tpInutilizacao;
        private System.Windows.Forms.TabPage tpManifestacao;
        private System.Windows.Forms.TabPage tpStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.Label lblCadastro;
        private System.Windows.Forms.Button btnConsultarCadastro;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscarVenda;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.TextBox txtVendaID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox dtData;
        private System.Windows.Forms.DataGridView dgBusca;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnGerarNFE;
        private System.Windows.Forms.ComboBox cmbOperacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbEmissao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtComplementar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtErros;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_EMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRODUTOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTDE_PRODUTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_DATA_HORA;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TOTAL;
        private System.Windows.Forms.DataGridView dgEmitidos;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox txtBData;
        private System.Windows.Forms.Button btnEmitidas;
        private System.Windows.Forms.TextBox txtBChave;
        private System.Windows.Forms.TextBox txtBVendaId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnReimpressao;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnImpressaoCCe;
        private System.Windows.Forms.Button btnGerarCCe;
        private System.Windows.Forms.TextBox txtMotivoCCe;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtMotivoCancelamento;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnInutilizar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtJustificativa;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtFinal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInicial;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtAno;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRetorno;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox txtManifestacaoJus;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cbEvento;
        private System.Windows.Forms.Button btnManifestacao;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox chNfeDest;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtRetornoManif;
        private System.Windows.Forms.CheckBox chkUnificar;
        private System.Windows.Forms.MaskedTextBox dtFinicial;
        private System.Windows.Forms.Label lblFinal;
        private System.Windows.Forms.MaskedTextBox dtInicial;
        private System.Windows.Forms.Label lblInicial;
        private System.Windows.Forms.TextBox txtIdCliente;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TabPage tpDevolucao;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblQtde;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.TextBox txtChave;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnGerarDevolucao;
        private System.Windows.Forms.MaskedTextBox txtCnpj;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmbDevOperacao;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHAVE_NFE;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn NFE_DATA_CANCELAMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NFE_DEVOLUCAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PATH;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUM_PROTOCOLO;
        private System.Windows.Forms.ComboBox cmbDocumento;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox chkPrincipio;
        private System.Windows.Forms.TextBox txtNaturezaDeOperacao;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtVlBaseIcms;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtVlIcms;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtVlBcRT;
        private System.Windows.Forms.TextBox txtVlIcmsST;
        private System.Windows.Forms.TextBox txtLote;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DataGridView dgDevolucao;
        private System.Windows.Forms.MaskedTextBox txtValidade;
        private System.Windows.Forms.MaskedTextBox txtFabricacao;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cmbIcms;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox BcPIS;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox AliqPIS;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox AliqCOFINS;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox BcCOFINS;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox TotCOFINS;
        private System.Windows.Forms.TextBox TotPIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_PRECOMPRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCONTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn vBC;
        private System.Windows.Forms.DataGridViewTextBoxColumn vICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn vBCSTRet;
        private System.Windows.Forms.DataGridViewTextBoxColumn vICMSST;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lote;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataFabricacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataValidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn vBcCOFINS;
        private System.Windows.Forms.DataGridViewTextBoxColumn vBcPIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn pAliqCOFINS;
        private System.Windows.Forms.DataGridViewTextBoxColumn pAliqPIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor_cofins;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor_pis;
    }
}