﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.Vendas.NFE
{
    public partial class frmVenNFE : Form, Botoes
    {
        private ToolStripButton tsbEmissao = new ToolStripButton("Emissão NF-e");
        private ToolStripSeparator tssEmissao = new ToolStripSeparator();
        private DataTable dtRetorno = new DataTable();
        VendasDados dados = new VendasDados();
        private string camposFaltantes;
        private NotasFiscaisEmitidas dadosDaNota = new NotasFiscaisEmitidas();
        private string caminhoXml;
        private string retornoDll;
        private string numeroRecibo;
        XmlNodeList xnResul;
        XmlDocument xmlNFe = new XmlDocument();
        private string caminhoDownload;
        private int tipo;
        private string[] split;
        nfec.nfecsharp nfe = new nfec.nfecsharp();
        private int totalQtde;
        private double valorTotal;
        private string ID;
        private int IdCliente;
        public string[] ide, emit, dest, total, transp, cobr, pag, infAdic, autXML;
        public string[,] prod;
        public double totalNota;
        public string numeroRandomico;
        public double descontoDevolucao;
        public double TotalvBC;
        public double TotalvICMS;
        public double TotalvBCSTRet;
        public double TotalvICMSST;
        public double ValorCOFINS;
        public double ValorPIS;
        public double TotalCOFINS;
        public double TotalPIS; 

        public frmVenNFE(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void frmVenNFE_Load(object sender, EventArgs e)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF - e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEmissao.AutoSize = false;
                this.tsbEmissao.Image = Properties.Resources.vendas;
                this.tsbEmissao.Size = new System.Drawing.Size(140, 20);
                this.tsbEmissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEmissao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEmissao);
                tsbEmissao.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenNFE>().FirstOrDefault();
                    Funcoes.BotoesCadastro(relatorio.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEmissao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEmissao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    if ((control.Name != "txtErro"))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenNFE_Shown(object sender, EventArgs e)
        {
            tabNFe.SelectedTab = tpGeradas;
            RegistryKey key = Registry.CurrentUser.OpenSubKey("nfeapp", true);

            try
            {
                if (key == null)
                    key = Registry.CurrentUser.CreateSubKey("nfeapp");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                key.Close();
            }
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.AppStarting;
            lblStatus.Text = "";
            tslRegistros.Text = "Aguarde...";
            btnStatus.Enabled = false;

            nfec.nfecsharp nfe = new nfec.nfecsharp();
            lblStatus.Text = nfe.NfeStatusServico();

            btnStatus.Enabled = true;
            tslRegistros.Text = "";
            Cursor = Cursors.Default;
        }

        private void btnConsultarCadastro_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.AppStarting;
            tslRegistros.Text = "Aguarde, consulta de cadastro...";
            btnConsultarCadastro.Enabled = false;
            Application.DoEvents();

            nfec.nfecsharp nfe = new nfec.nfecsharp();
            lblCadastro.Text = nfe.NfeConsultaCadastro(Funcoes.ObtemUF(Funcoes.LeParametro(6, "318", true)), Funcoes.LeParametro(6, "129", true));

            btnConsultarCadastro.Enabled = true;

            tslRegistros.Text = "";
            Cursor = Cursors.Default;
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtData.Text)))
                {
                    txtVendaID.Focus();
                }
                else
                    btnBuscarVenda.PerformClick();
            }
        }

        private void txtVendaID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtVendaID.Text))
                {
                    txtCliente.Focus();
                }
                else
                    btnBuscarVenda.PerformClick();
            }

        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscarVenda.PerformClick();
        }

        private void btnBuscarVenda_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtData.Text)) && String.IsNullOrEmpty(txtVendaID.Text) && String.IsNullOrEmpty(txtCliente.Text) && !chkUnificar.Checked)
                {
                    MessageBox.Show("Informe ao menos um critério de busca.", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtData.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;

                    if (!chkUnificar.Checked)
                    {
                        dtRetorno = dados.BuscaEmissaoNFE(Principal.empAtual, Principal.estAtual, dtData.Text, txtVendaID.Text, txtCliente.Text);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            dgBusca.DataSource = dtRetorno;
                            dgBusca.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Nenhum registro encontrado!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtData.Focus();
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtInicial.Text)))
                        {
                            MessageBox.Show("Informe a Data Inicial!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                            return;
                        }
                        if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dtFinicial.Text)))
                        {
                            MessageBox.Show("Informe a Data Final!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                            return;
                        }
                        if (Convert.ToDateTime(dtInicial.Text) > Convert.ToDateTime(dtFinicial.Text))
                        {
                            MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                            return;
                        }

                        if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtIdCliente.Text)))
                        {
                            MessageBox.Show("Informe o ID do Cliente!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtIdCliente.Focus();
                            return;
                        }

                        dtRetorno = dados.BuscaEmissaoNFETotal(Principal.empAtual, Principal.estAtual, dtInicial.Text, dtFinicial.Text, txtIdCliente.Text);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            dgBusca.DataSource = dtRetorno;
                            dgBusca.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Nenhum registro encontrado!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtInicial.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void LimparEmitir()
        {
            txtCliente.Text = "";
            dtData.Text = "";
            txtVendaID.Text = "";
            dtRetorno = dados.BuscaEmissaoNFE(Principal.empAtual, Principal.estAtual, "", "0", "");
            dgBusca.DataSource = dtRetorno;
            cmbEmissao.SelectedIndex = 0;
            cmbOperacao.SelectedIndex = 0;
            camposFaltantes = "";
            txtErros.Text = "";
            txtPedido.Text = "";
            txtComplementar.Text = "";
            tslRegistros.Text = "";
            dtInicial.Text = "";
            dtFinicial.Text = "";
            txtIdCliente.Text = "";
            lblInicial.Visible = false;
            lblFinal.Visible = false;
            dtInicial.Visible = false;
            dtFinicial.Visible = false;
            lblID.Visible = false;
            txtIdCliente.Visible = false;
            txtObservacao.Text = "";
            cmbDocumento.SelectedIndex = 1;
            chkPrincipio.Checked = false;
            numeroRandomico = "";
            dtData.Focus();
        }

        public void LimparDevolucao()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtPreco.Text = "";
            txtQtde.Text = "0";
            lblQtde.Text = "";
            lblTotal.Text = "";
            totalQtde = 0;
            valorTotal = 0;
            txtChave.Text = "";
            txtMotivo.Text = "";
            txtComplemento.Text = "";
            cmbDevOperacao.SelectedIndex = -1;
            cmbIcms.SelectedIndex = -1;
            txtNaturezaDeOperacao.Text = "DEVOLUCAO DE MERCADORIA";
            txtCnpj.Text = "";
            txtVlBaseIcms.Text = "0,00";
            txtVlIcms.Text = "0,00";
            txtVlBcRT.Text = "0,00";
            txtVlIcmsST.Text = "0,00";
            txtLote.Text = string.Empty;
            txtValidade.Text = string.Empty;
            txtFabricacao.Text = string.Empty;
            dgDevolucao.Rows.Clear();
            txtCodBarras.Focus();
            BcCOFINS.Text = "0.00";
            AliqCOFINS.Text = "0";
            BcPIS.Text = "0.00";
            AliqPIS.Text = "0";  
        }

        public void LimparGeradas()
        {
            dtRetorno = dadosDaNota.BuscaNfeEmitidas(Principal.estAtual, Principal.empAtual, "0", "", "");
            dgEmitidos.DataSource = dtRetorno;
            txtBChave.Text = "";
            txtBData.Text = "";
            txtBVendaId.Text = "";
            txtMotivoCCe.Text = "";
            txtMotivoCancelamento.Text = "";
            tslRegistros.Text = "";
            txtBData.Focus();
        }

        public void LimparInutilizacao()
        {
            txtAno.Text = "";
            txtInicial.Text = "";
            txtFinal.Text = "";
            txtJustificativa.Text = "";
            txtRetorno.Text = "";
            tslRegistros.Text = "";
            txtAno.Focus();
        }

        public void LimparManifestacao()
        {
            cbEvento.SelectedIndex = -1;
            chNfeDest.Text = "";
            txtManifestacaoJus.Text = "";
            txtRetornoManif.Text = "";
            tslRegistros.Text = "";
            cbEvento.Focus();
        }

        private void tabNFe_SelectedIndexChanged(object sender, EventArgs e)
        {
            Limpar();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        private void cmbEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbOperacao.Focus();
        }


        private void txtMotivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtComplemento.Focus();
        }

        private void txtPedido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtComplementar.Focus();
        }

        private void txtComplementar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnGerarNFE.PerformClick();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            if (tabNFe.SelectedTab == tpEmitir)
            {
                LimparEmitir();
            }
            else if (tabNFe.SelectedTab == tpGeradas)
            {
                LimparGeradas();
            }
            else if (tabNFe.SelectedTab == tpInutilizacao)
            {
                LimparInutilizacao();
            }
            else if (tabNFe.SelectedTab == tpManifestacao)
            {
                LimparManifestacao();
            }
            else if (tabNFe.SelectedTab == tpDevolucao)
            {
                LimparDevolucao();
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private async void btnGerarNFE_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgBusca.RowCount == 0)
                {
                    MessageBox.Show("Necessário ao menos uma Venda para Emitir a NFe", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtData.Focus();
                    return;
                }

                if (!chkUnificar.Checked)
                {
                    for (int i = 0; i < dgBusca.RowCount; i++)
                    {
                        if (dgBusca.Rows[i].Selected == true)
                        {
                            await GerarNFE(Convert.ToDateTime(dgBusca.Rows[i].Cells["VENDA_DATA_HORA"].Value), dgBusca.Rows[i].Cells["CF_DOCTO"].Value.ToString(), dgBusca.Rows[i].Cells["VENDA_ID"].Value.ToString());
                        }
                    }
                }
                else
                {
                    await GerarNFE(DateTime.Now, dgBusca.Rows[0].Cells["CF_DOCTO"].Value.ToString(), "0");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<bool> GerarNFE(DateTime dataHora, string numDocto, string vendaID)
        {
            try
            {
                btnGerarNFE.Enabled = false;
                Cursor = Cursors.WaitCursor;
                txtErros.Focus();
                txtErros.Text = "";
                caminhoXml = "";

                bool validou;

                tslRegistros.Text = "Validando Dados. Aguarde...";
                if (!DadosDeIdentificacao(dataHora))
                    return false;

                if (!DadosDoEmitente())
                    return false;

                validou = await DadosDoDestinatario(numDocto, false);
                if (!validou)
                    return false;

                if (!DadosDoProduto(vendaID))
                    return false;

                if (!DadosTotalizadores(vendaID))
                    return false;

                DadosDoTransporte();

                DadosDaCobranca();

                if (!DadosDoPagamento(false))
                    return false;

                if (!DadosAdicionais(false))
                    return false;

                autXML = new string[200];
                autXML[0] = "";

                tslRegistros.Text = "Gerando Nota. Aguarde...";

                retornoDll = nfe.GeraNFe(ide, emit, dest, prod, total, transp, cobr, pag, infAdic, autXML, false);

                if (!String.IsNullOrEmpty(retornoDll))
                {
                    txtErros.Text = "Nota Gerada: " + retornoDll;

                    Random r = new Random(6);
                    int nNF = (r.Next(6) + DateTime.Now.Millisecond);

                    dadosDaNota.EmpCodigo = Principal.empAtual;
                    dadosDaNota.EstCodigo = Principal.estAtual;
                    dadosDaNota.VendaId = chkUnificar.Checked == false ? Convert.ToInt64(vendaID) : nNF;
                    dadosDaNota.ChaveNfe = retornoDll.Replace("NFe", "");
                    dadosDaNota.Devolucao = "N";
                    dadosDaNota.OpCadastro = Principal.usuario;
                    dadosDaNota.DtCadastro = DateTime.Now;
                    dadosDaNota.DataEmisssao = DateTime.Now;
                    dadosDaNota.Observacao = txtObservacao.Text;
                    dadosDaNota.CfID = IdCliente;

                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);

                    if (!dadosDaNota.InserePreNota(dadosDaNota))
                        return false;

                    caminhoXml = Funcoes.LeParametro(6, "322", false) + @"arquivos\" + retornoDll + ".xml";

                    if (!AssinarArquivo())
                        return false;

                    if (!ValidarArquivo())
                        return false;

                    if (!GerarLote())
                        return false;

                    if (!AutorizarNFe(false))
                        return false;

                    Application.DoEvents();
                    System.Threading.Thread.Sleep(10000);

                    if (!ConsultarProcessamentoNFe(false))
                        return false;

                    if (!ConsultarNFe(dataHora, false))
                        return false;

                    if (!ImprimirNFe(dataHora, false))
                        return false;

                    Funcoes.GravaParametro(6, "345", (Convert.ToInt32(Funcoes.LeParametro(6, "345", true)) + 1).ToString(), "GERAL", "Sequencia NFe", "VENDAS", "Identifica o numero de serie de uma NFe", true);
                }
                else
                {
                    MessageBox.Show("Falha ao Gerar NFe", "Gerar NFE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Gerar NFE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                btnGerarNFE.Enabled = true;
                tslRegistros.Text = "";
                Cursor = Cursors.Default;

            }
        }

        public bool AssinarArquivo()
        {
            try
            {
                tslRegistros.Text = "Assinando Arquivo. Aguarde...";

                retornoDll = nfe.AssinarArquivoXML(caminhoXml, "infNFe");
                txtErros.Text = retornoDll;

                if (retornoDll.Substring(0, 4).Equals("Erro"))
                {
                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                    return false;
                }
                else
                {
                    caminhoXml = Funcoes.LeParametro(6, "322", false) + @"arquivos\assinado\NFe" + dadosDaNota.ChaveNfe + "-assinado.xml";
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Assinar Arquivo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ValidarArquivo()
        {
            try
            {
                tslRegistros.Text = "Validando Arquivo. Aguarde...";

                if (nfe.ValidarArquivoXML(caminhoXml, "nfe_v4.00.xsd", true) == "OK")
                {
                    txtErros.Text = "Validação concluída, nenhum erro identificado.";
                    return true;
                }
                else
                {
                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                    txtErros.Text = "Problemas identificados na validação";
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Assinar Arquivo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                return false;
            }
        }

        public bool GerarLote()
        {
            try
            {
                tslRegistros.Text = "Gerando Lote. Aguarde...";

                Random r = new Random(5);
                int lote = Convert.ToInt32((r.Next() + DateTime.Now.Millisecond));
                retornoDll = nfe.GerarLote(caminhoXml, lote);

                dadosDaNota.AtualizaLote(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo, Funcoes.FormataZeroAEsquerda(lote.ToString(), 12));

                txtErros.Text = "Lote gerado com sucesso!";

                caminhoXml = Funcoes.LeParametro(6, "322", false) + @"lotes\" + Funcoes.FormataZeroAEsquerda(lote.ToString(), 12) + "-env-lot.xml";
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Gerar Lote", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public bool AutorizarNFe(bool devolucao)
        {
            try
            {
                numeroRecibo = "";
                tslRegistros.Text = "Autorizando NFe. Aguarde...";

                retornoDll = nfe.NfeAutorizacao(caminhoXml);
                txtErros.Text = retornoDll;

                if (retornoDll.Substring(0, 4).Equals("Erro"))
                {
                    if (devolucao)
                    {
                        MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);

                    return false;
                }
                else
                {
                    split = retornoDll.ToString().Split('#');
                    numeroRecibo = split[1].Substring(0, 15);
                    dadosDaNota.AtualizaNumeroRecibo(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo, numeroRecibo);
                    return true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Autorizar NFe", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                return false;
            }
        }

        public bool ConsultarProcessamentoNFe(bool devolucao)
        {
            try
            {
                tslRegistros.Text = "Consultando Processamento NFe. Aguarde...";

                retornoDll = nfe.NfeRetAutorizacao(numeroRecibo);

                caminhoXml = Funcoes.LeParametro(6, "322", false) + @"retornos\" + numeroRecibo + "-pro-rec.xml";

                if (System.IO.File.Exists(caminhoXml))
                {
                    xmlNFe.Load(caminhoXml);

                    xnResul = xmlNFe.GetElementsByTagName("infProt");

                    if (xnResul.Count > 0)
                    {
                        if (!xnResul[0]["cStat"].InnerText.Equals("100"))
                        {
                            if (devolucao)
                            {
                                MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                                txtErros.Text = retornoDll;

                            dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                            return false;
                        }
                        else
                            return true;
                    }
                    else
                        return false;
                }
                else
                {
                    if (devolucao)
                    {
                        MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        txtErros.Text = retornoDll;

                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Autorizar NFe", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                return false;
            }
        }

        public bool ConsultarNFe(DateTime dataVenda, bool devolucao)
        {
            try
            {
                tslRegistros.Text = "Consultando NFe. Aguarde...";

                retornoDll = nfe.NfeConsulta(dadosDaNota.ChaveNfe);

                caminhoXml = Funcoes.LeParametro(6, "322", false) + @"retornos\" + dadosDaNota.ChaveNfe + "-sit.xml";

                if (System.IO.File.Exists(caminhoXml))
                {
                    xmlNFe.Load(caminhoXml);

                    xnResul = xmlNFe.GetElementsByTagName("infProt");

                    if (xnResul.Count > 0)
                    {
                        if (!xnResul[0]["cStat"].InnerText.Equals("100"))
                        {
                            if (devolucao)
                            {
                                MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                                txtErros.Text = retornoDll;

                            dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                            return false;
                        }
                        else
                        {
                            dadosDaNota.AtualizaProtocolo(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo, xnResul[0]["nProt"].InnerText);
                            caminhoXml = Funcoes.LeParametro(6, "322", false) + @"arquivos\procNFe\" + dataVenda.ToString("yyyyMM") + @"\" + dadosDaNota.ChaveNfe + "-procNFe.xml";
                            return true;
                        }
                    }
                    else
                        return false;
                }
                else
                {
                    if (devolucao)
                    {
                        MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        txtErros.Text = retornoDll;

                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Autorizar NFe", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);
                return false;
            }
        }

        public bool ImprimirNFe(DateTime dataVenda, bool devolucao)
        {
            try
            {
                tslRegistros.Text = "Gerando NFe. Aguarde...";
                tipo = 3;
                string caminhoPDF = "";

                if (MessageBox.Show("Deseja Salvar em PDF (SIM), ou somente Visualizar (Não)?", "Impressão da DANFE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    caminhoPDF = Funcoes.LeParametro(6, "322", false) + @"arquivos\procNFe\" + dataVenda.ToString("yyyyMM") + @"\" + dadosDaNota.ChaveNfe + ".pdf";
                    tipo = 2;
                }

                if (nfe.NFeDanfe(caminhoXml, caminhoPDF, tipo, false))
                {
                    dadosDaNota.AtualizaCaminho(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo, caminhoXml);
                    txtErros.Text = "DANFE Emitido com sucesso!";

                    if (tipo == 2)
                    {
                        System.Diagnostics.Process.Start(caminhoPDF);
                    }
                    return true;
                }
                else
                {
                    if (devolucao)
                    {
                        MessageBox.Show(retornoDll, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        txtErros.Text = "Falha na emissão do DANFE.";

                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Autorizar NFe", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }


        private void btnEmitidas_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                dtRetorno = dadosDaNota.BuscaNfeEmitidas(Principal.estAtual, Principal.empAtual, txtBVendaId.Text, txtBData.Text, txtBChave.Text);
                if (dtRetorno.Rows.Count > 0)
                {
                    tslRegistros.Text = "Notas Emitidas: " + dtRetorno.Rows.Count;
                    dgEmitidos.DataSource = dtRetorno;
                    dgEmitidos.Focus();
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado!", "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtData.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Emissão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnReimpressao_Click(object sender, EventArgs e)
        {
            try
            {

                tipo = 3;
                caminhoDownload = "";
                if (dgEmitidos.RowCount > 0)
                {
                    for (int i = 0; i < dgEmitidos.RowCount; i++)
                    {
                        if (dgEmitidos.Rows[i].Selected == true)
                        {
                            if (MessageBox.Show("Deseja Salvar em PDF (SIM), ou somente Visualizar (Não)?", "Impressão da DANFE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    caminhoDownload = saveFileDialog1.FileName + ".pdf";
                                    tipo = 2;
                                }
                            }

                            if (nfe.NFeDanfe(dgEmitidos.Rows[i].Cells["PATH"].Value.ToString(), caminhoDownload, tipo, false))
                            {
                                tslRegistros.Text = "Reimpressão da NF-e gerada com Sucesso.";
                                if (tipo == 2)
                                {
                                    System.Diagnostics.Process.Start(caminhoDownload);
                                }
                            }
                            else
                            {
                                tslRegistros.Text = "Falha na Reimpressão da NF-e.";
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ao menos uma Nota para realizar a Reimpressão", "Reimpressão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBData.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpressão NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }
        }

        private void btnGerarCCe_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEmitidos.RowCount > 0)
                {
                    for (int i = 0; i < dgEmitidos.RowCount; i++)
                    {
                        if (dgEmitidos.Rows[i].Selected == true)
                        {
                            if (String.IsNullOrEmpty(txtMotivoCCe.Text))
                            {
                                MessageBox.Show("Informe o Motivo da Correção", "CC-e NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtMotivoCCe.Focus();
                            }
                            else if (txtMotivoCCe.Text.Length <= 15)
                            {
                                MessageBox.Show("Motivo da Correção não pode ser menor que 15 caracteres", "CC-e NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtMotivoCCe.Focus();
                            }
                            else
                            {
                                int correcao = dadosDaNota.BuscaNumeroCorrecao(Principal.empAtual, Principal.estAtual);
                                retornoDll = nfe.GeraCCe(dgEmitidos.Rows[i].Cells["CHAVE_NFE"].Value.ToString(),             //chave NFE (apenas numeros)
                                           correcao.ToString(),                //Seqüencial do evento para o mesmo tipo de evento. Para maioria dos eventos será 1, nos casos em que possa existir mais de um evento, como é o caso da carta de correção, o autor do evento deve numerar de forma seqüencial.
                                           "110110",                           //Código do de evento, conforme NT2011.03, default = 110110
                                           DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("HH:mm:ss") + "-03:00",   //Data e hora do evento no formato AAAA-MMDDThh:mm:ssTZD
                                           Funcoes.RemoverAcentuacao(txtMotivoCCe.Text));

                                if (retornoDll.Substring(0, 5).Equals("Falha"))
                                {
                                    MessageBox.Show(retornoDll, "CC-e NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    txtMotivoCCe.Focus();
                                }
                                else
                                {
                                    tipo = 2;
                                    caminhoDownload = "";
                                    split = retornoDll.ToString().Split('\n');
                                    numeroRecibo = split[2].Substring(split[2].Length - 15, 15);

                                    dadosDaNota.AtualizaNumeroCorrecaoEProtocolo(Convert.ToInt64(dgEmitidos.Rows[i].Cells["VENDA"].Value), Principal.empAtual, Principal.estAtual, correcao, numeroRecibo);

                                    if (MessageBox.Show("Deseja Salvar em PDF (SIM), ou somente Visualizar (Não)?", "Impressão da DANFE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                                    {
                                        if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                                        {
                                            caminhoDownload = saveFileDialog1.FileName + ".pdf";
                                        }
                                    }
                                    else
                                    {
                                        caminhoDownload = @"C:\Nota" + dgEmitidos.Rows[i].Cells["VENDA"].Value.ToString() + ".pdf";
                                    }

                                    if (nfe.ImpCCe("", caminhoDownload, tipo))
                                    {
                                        tslRegistros.Text = "CCe impresso com sucesso!";
                                        txtMotivoCCe.Text = "";
                                        dgEmitidos.Focus();
                                    }
                                    else
                                    {
                                        tslRegistros.Text = "Falha na impressão da CCe.";
                                        btnGerarCCe.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ao menos uma Nota para realizar a CC-e", "CC-e NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBData.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "CC-e NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEmitidos.RowCount > 0)
                {
                    for (int i = 0; i < dgEmitidos.RowCount; i++)
                    {
                        if (dgEmitidos.Rows[i].Selected == true)
                        {
                            if (String.IsNullOrEmpty(txtMotivoCancelamento.Text))
                            {
                                MessageBox.Show("Informe o Motivo do Cancelamento", "Cancelar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtMotivoCancelamento.Focus();
                            }
                            else if (txtMotivoCancelamento.Text.Length <= 15)
                            {
                                MessageBox.Show("Motivo do Cancelamento não pode ser menor que 15 caracteres", "Cancelar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtMotivoCancelamento.Focus();
                            }
                            else
                            {
                                retornoDll = nfe.NfeCancelamentoEvento(
                                                   "0000121",
                                                   dgEmitidos.Rows[i].Cells["CHAVE_NFE"].Value.ToString(),
                                                   "1",
                                                   dgEmitidos.Rows[i].Cells["NUM_PROTOCOLO"].Value.ToString(),
                                                   Funcoes.RemoverAcentuacao(txtMotivoCancelamento.Text));

                                if (retornoDll.Substring(0, 5).Equals("Falha"))
                                {
                                    MessageBox.Show(retornoDll, "Cancelar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    txtMotivoCancelamento.Focus();
                                }
                                else
                                {
                                    split = retornoDll.ToString().Split('#');
                                    numeroRecibo = split[3].Substring(split[3].Length - 17, 15);

                                    dadosDaNota.AtualizaDadosCancelamento(Convert.ToInt64(dgEmitidos.Rows[i].Cells["VENDA"].Value), Principal.empAtual, Principal.estAtual, numeroRecibo);

                                    txtMotivoCancelamento.Text = "";
                                    dgEmitidos.Focus();
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ao menos uma Nota para realizar o Cancelamento", "Cancelar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBData.Focus();
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }
        }

        private void txtMotivoCCe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnGerarCCe.PerformClick();
        }

        private void txtMotivoCancelamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnCancelar.PerformClick();
        }

        private void txtBData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtBData.Text)))
                {
                    txtBVendaId.Focus();
                }
                else
                    btnEmitidas.PerformClick();

            }
        }

        private void txtBVendaId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBVendaId.Text))
                {
                    txtBChave.Focus();
                }
                else
                    btnEmitidas.PerformClick();
            }
        }

        private void txtBChave_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnEmitidas.PerformClick();
            }
        }

        private void btnImpressaoCCe_Click(object sender, EventArgs e)
        {
            try
            {
                tipo = 3;
                if (MessageBox.Show("Deseja Salvar em PDF (SIM), ou somente Visualizar (Não)?", "Impressão da DANFE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        caminhoDownload = saveFileDialog1.FileName + ".pdf";
                        tipo = 2;
                    }
                }

                if (nfe.ImpCCe("", caminhoDownload, tipo))
                {
                    tslRegistros.Text = "CCe impresso com sucesso!";
                    dgEmitidos.Focus();
                }
                else
                {
                    tslRegistros.Text = "Falha na impressão da CCe.";
                    btnImpressaoCCe.Focus();
                }
            }
            finally
            {
                tslRegistros.Text = "";
            }
        }

        private void txtAno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtInicial.Focus();
        }

        private void txtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFinal.Focus();
        }

        private void txtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtJustificativa.Focus();
        }

        private void txtJustificativa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnInutilizar.PerformClick();
        }

        private void btnInutilizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtAno.Text))
                {
                    MessageBox.Show("Ano não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAno.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtInicial.Text))
                {
                    MessageBox.Show("Número Inicial não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtFinal.Text))
                {
                    MessageBox.Show("Número Final não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtFinal.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtJustificativa.Text))
                {
                    MessageBox.Show("Justificativa não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtJustificativa.Focus();
                    return;
                }

                if (txtJustificativa.Text.Length <= 15)
                {
                    MessageBox.Show("Justificativa tem que ter mais de 15 caracteres!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtJustificativa.Focus();
                    return;
                }
                Cursor = Cursors.WaitCursor;

                txtRetorno.Text = nfe.NfeInutilizacao(
                                        txtAno.Text,
                                        txtInicial.Text,
                                        txtFinal.Text,
                                        Funcoes.RemoveCaracter(txtJustificativa.Text));

                txtRetorno.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inutilizar NF-e", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }
        }

        private void cbEvento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chNfeDest.Focus();
        }

        private void chNfeDest_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtManifestacaoJus.Focus();
        }

        private void txtManifestacaoJus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnManifestacao.PerformClick();
        }

        private void btnManifestacao_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbEvento.SelectedIndex == -1)
                {
                    MessageBox.Show("Necessário selecionar o Evento!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbEvento.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(chNfeDest.Text))
                {
                    MessageBox.Show("Chave NF-e não pode ser em branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chNfeDest.Focus();
                    return;
                }

                if (cbEvento.SelectedIndex == 3)
                {
                    if (String.IsNullOrEmpty(txtManifestacaoJus.Text))
                    {
                        MessageBox.Show("Justificativa não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtManifestacaoJus.Focus();
                        return;
                    }

                    if (txtManifestacaoJus.Text.Length <= 15)
                    {
                        MessageBox.Show("Justificativa tem que ter mais de 15 caracteres!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtManifestacaoJus.Focus();
                        return;
                    }
                }

                string tipoEvento = string.Empty;
                switch (cbEvento.SelectedIndex)
                {
                    case 0: tipoEvento = "210200"; break;
                    case 1: tipoEvento = "210210"; break;
                    case 2: tipoEvento = "210220"; break;
                    case 3: tipoEvento = "210240"; break;
                }

                txtRetornoManif.Text = nfe.GeraManifestacao(chNfeDest.Text,             //chave NFE (apenas numeros, 44 dígitos)
                   "1",                                //Seqüencial do evento para o mesmo tipo de evento. Informar 1
                  tipoEvento,                           //Código do de evento, conforme NT2012.02, valores possíveis: 210200, 210210, 210220 e 210240
                   DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("HH:mm:ss") + "-03:00",   //Data e hora do evento no formato AAAA-MMDDThh:mm:ssTZD
                   Funcoes.RemoveCaracter(txtManifestacaoJus.Text));                        //Justificativa caso seja escolhido evento Operação não Realizada.

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manifestação Destinatário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }
        }

        private void chkUnificar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUnificar.Checked)
            {
                lblInicial.Visible = true;
                lblFinal.Visible = true;
                dtInicial.Visible = true;
                dtFinicial.Visible = true;
                lblID.Visible = true;
                txtIdCliente.Visible = true;
                dtInicial.Focus();
            }
            else
            {
                lblInicial.Visible = false;
                lblFinal.Visible = false;
                dtInicial.Visible = false;
                dtFinicial.Visible = false;
                lblID.Visible = false;
                txtIdCliente.Visible = false;
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinicial.Focus();
        }

        private void dtFinicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtIdCliente.Focus();
        }

        private void txtIdCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscarVenda.PerformClick();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosDevolucao(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();
                    txtPreco.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_PRECOMPRA"]);
                    txtPreco.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPreco.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDesconto.Focus();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodBarras.Text))
            {
                MessageBox.Show("Cód. de Barras não pode ser em branco.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodBarras.Focus();
                return;
            }

            if (String.IsNullOrEmpty(txtPreco.Text) || Convert.ToDouble(txtPreco.Text) == 0)
            {
                MessageBox.Show("Preço não pode ser em branco ou 0.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPreco.Focus();
                return;
            }

            if (String.IsNullOrEmpty(txtQtde.Text) || Convert.ToDouble(txtQtde.Text) == 0)
            {
                MessageBox.Show("Qtde não pode ser em branco ou 0.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQtde.Focus();
                return;
            }

            if (String.IsNullOrEmpty(txtDesconto.Text))
            {
                MessageBox.Show("Desconto não pode ser em branco.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDesconto.Text = "0";
            }

            totalQtde += Convert.ToInt32(txtQtde.Text);
            valorTotal += Convert.ToDouble(txtPreco.Text) * Convert.ToInt32(txtQtde.Text);
            
            dgDevolucao.Rows.Insert(dgDevolucao.RowCount, new Object[] { ID, txtCodBarras.Text, txtDescr.Text, txtPreco.Text, txtQtde.Text, txtDesconto.Text, txtVlBaseIcms.Text, txtVlIcms.Text,
                txtVlBcRT.Text, txtVlIcmsST.Text, txtLote.Text, Funcoes.RemoveCaracter(txtFabricacao.Text) == "" ? "" : txtFabricacao.Text,
                Funcoes.RemoveCaracter(txtValidade.Text) == "" ? "" : txtValidade.Text, BcCOFINS.Text, BcPIS.Text,  AliqCOFINS.Text, AliqPIS.Text, TotCOFINS.Text, TotPIS.Text });
           
            descontoDevolucao = Convert.ToDouble(txtDesconto.Text);

            txtCodBarras.Text = string.Empty;
            txtDescr.Text = string.Empty;
            txtQtde.Text = "0";
            txtPreco.Text = string.Empty;
            txtDesconto.Text = string.Empty;
            txtVlBaseIcms.Text = "0,00";
            txtVlIcms.Text = "0,00";
            txtVlBcRT.Text = "0,00";
            txtVlIcmsST.Text = "0,00";
            txtLote.Text = string.Empty;
            txtValidade.Text = string.Empty;
            txtFabricacao.Text = string.Empty;

            BcCOFINS.Text = "0.00";
            AliqCOFINS.Text = "0.00";
            BcPIS.Text = "0";
            AliqPIS.Text = "0";

            TotPIS.Text = "0.00";
            TotCOFINS.Text = "0.00"; 

            lblQtde.Text = "Qtde Total.: " + totalQtde.ToString();
            lblTotal.Text = "Valor Total: " + String.Format("{0:N}", valorTotal);

            txtCodBarras.Focus();

        }

        private void dgDevolucao_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            totalQtde = 0;
            valorTotal = 0;

            for (int i = 0; i < dgDevolucao.RowCount; i++)
            {
                totalQtde += Convert.ToInt32(dgDevolucao.Rows[i].Cells["PROD_QTDE"].Value);
                valorTotal += Convert.ToDouble(dgDevolucao.Rows[i].Cells["PROD_PRECOMPRA"].Value) * Convert.ToInt32(dgDevolucao.Rows[i].Cells["PROD_QTDE"].Value);
            }

            lblQtde.Text = "Qtde Total.: " + totalQtde.ToString();
            lblTotal.Text = "Valor Total: " + String.Format("{0:N}", valorTotal);

            txtCodBarras.Focus();
        }

        private async void btnGerarDevolucao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgDevolucao.RowCount == 0)
                {
                    MessageBox.Show("Informe ao menos um produto para ser devolvido.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgDevolucao.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtChave.Text))
                {
                    MessageBox.Show("Chave de Acesso pode ser em branco.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtChave.Focus();
                    return;
                }

                if (txtChave.TextLength < 44)
                {
                    MessageBox.Show("Chave de Acesso com tamanho incorreto.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtChave.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtMotivo.Text))
                {
                    MessageBox.Show("Motivo da Devolução pode ser em branco.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMotivo.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpj.Text)))
                {
                    MessageBox.Show("Informe o CNPJ do Destinatário.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpj.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(cmbDevOperacao.Text))
                {
                    MessageBox.Show("Informe o Identificador de Operação.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbDevOperacao.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(cmbIcms.Text))
                {
                    MessageBox.Show("Informe o Identificador de ICMS.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbIcms.Focus();
                    return;
                }

                TotalvBC = 0;
                TotalvICMS = 0;
                TotalvBCSTRet = 0;
                TotalvICMSST = 0;
                descontoDevolucao = 0;

                txtComplemento.Text = txtComplemento.Text.Trim();

                bool retornoDev = await GerarNFeDevolucao();
                if (retornoDev)
                {
                    LimparDevolucao();
                }
                else
                    btnGerarDevolucao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region NOTA_DEVOLUCAO
        public async Task<bool> GerarNFeDevolucao()
        {   
            try
            {
                btnGerarDevolucao.Enabled = false;
                Cursor = Cursors.WaitCursor;
                txtErros.Focus();
                txtErros.Text = "";
                caminhoXml = "";
                DateTime dataDevolucao = DateTime.Now;
                bool validou;

                tslRegistros.Text = "Validando Dados. Aguarde...";
                if (!DadosDeIdentificacaoDevolucao(dataDevolucao))
                    return false;

                if (!DadosDoEmitente())
                    return false;

                validou = await DadosDoDestinatario(txtCnpj.Text, true);
                if (!validou)
                    return false;

                if (!DadosDoProdutoDevolucao())
                    return false;

                if (!DadosTotalizadoresDevolucao())
                    return false;

                DadosDoTransporte();

                DadosDaCobranca();

                if (!DadosDoPagamento(true))
                    return false;

                if (!DadosAdicionais(true))
                    return false;

                autXML = new string[200];
                autXML[0] = "";

                tslRegistros.Text = "Gerando Nota. Aguarde...";

                retornoDll = nfe.GeraNFe(ide, emit, dest, prod, total, transp, cobr, pag, infAdic, autXML, false);

                if (!String.IsNullOrEmpty(retornoDll))
                {
                    txtErros.Text = "Nota Gerada: " + retornoDll;

                    Random r = new Random(6);
                    int nNF = (r.Next(6) + DateTime.Now.Millisecond);

                    dadosDaNota.EmpCodigo = Principal.empAtual;
                    dadosDaNota.EstCodigo = Principal.estAtual;
                    dadosDaNota.VendaId = nNF;
                    dadosDaNota.ChaveNfe = retornoDll.Replace("NFe", "");
                    dadosDaNota.Devolucao = "S";
                    dadosDaNota.OpCadastro = Principal.usuario;
                    dadosDaNota.DtCadastro = DateTime.Now;
                    dadosDaNota.DataEmisssao = DateTime.Now;
                    dadosDaNota.Observacao = "";
                    dadosDaNota.CfID = IdCliente;
                    dadosDaNota.ExcluiNotaPorVendaID(dadosDaNota.VendaId, dadosDaNota.EmpCodigo, dadosDaNota.EstCodigo);

                    if (!dadosDaNota.InserePreNota(dadosDaNota))
                        return false;

                    caminhoXml = Funcoes.LeParametro(6, "322", false) + @"arquivos\" + retornoDll + ".xml";

                    if (!AssinarArquivo())
                        return false;

                    if (!ValidarArquivo())
                        return false;

                    if (!GerarLote())
                        return false;

                    if (!AutorizarNFe(true))
                        return false;

                    Application.DoEvents();
                        System.Threading.Thread.Sleep(10000);

                    if (!ConsultarProcessamentoNFe(true))
                        return false;

                    if (!ConsultarNFe(dataDevolucao, true))
                        return false;

                    if (!ImprimirNFe(dataDevolucao, true))
                        return false;

                    Funcoes.GravaParametro(6, "345", (Convert.ToInt32(Funcoes.LeParametro(6, "345", true)) + 1).ToString(), "GERAL", "Sequencia NFe", "VENDAS", "Identifica o numero de serie de uma NFe", true);
                }
                else
                {
                    MessageBox.Show("Falha ao Gerar NFe", "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                btnGerarDevolucao.Enabled = true;
                tslRegistros.Text = "";
                Cursor = Cursors.Default;

            }
        }

        public bool DadosDeIdentificacaoDevolucao(DateTime data)
        {
            try
            {
                ide = new string[37];
                ide[0] = Funcoes.LeParametro(6, "318", true);                                                                       //<cUF> Código da UF do emitente do Documento Fiscal 
                ide[1] = GeraNumeroRandomico();                                                                        //<cNF> Código Numérico que compõe a Chave de Acesso - 8 DIGITOS A PARTIR DA VERSAO 2.0 (MANUAL 4.01)
                ide[2] = Funcoes.RemoverAcentuacao(txtNaturezaDeOperacao.Text);                                                                                //<natOp> Descrição da Natureza da Operação
                ide[3] = "";                                                                                                        //<indPag> Indicador da forma de pagamento - Retirado na versão 4.00
                ide[4] = "55";                                                                                                      //<mod> Código do Modelo do Documento Fiscal
                ide[5] = "1";                                                                                                       //<serie> Série do Documento Fiscal
                ide[6] = Funcoes.LeParametro(6, "345", true);                                                                       //<nNF> Número do Documento Fiscal
                ide[7] = data.ToString("yyyy-MM-ddTHH:mm:sszzz");                                                                 //<dhEmi> Data e hora de emissão do Documento Fiscal
                ide[8] = data.ToString("yyyy-MM-ddTHH:mm:sszzz");                                                              //<dhSaiEnt> Data e hora de Saída ou da Entrada da Mercadoria/Produto
                ide[25] = cmbDevOperacao.Text.Substring(0, 1);                                                                         //<idDest> Identificador de local de destino da operação
                ide[9] = "1";                                                                                                       //<tpNF> Tipo de Operação - 0=Entrada; 1=Saída
                ide[10] = Funcoes.LeParametro(6, "319", true);                                                                      //<cMunFG> Código do Município de Ocorrência do Fato Gerador

                ide[11] = "1";                                                                                                      //<tpEmis> Tipo de Emissão da NF-e

                ide[12] = "4";                                                                          //<finNFe> Finalidade de emissão da NF-e - 1=NF-e Normal; 2=NF-e Complementar; 3=NF-e Ajuste; 4=Devolução de mercadoria
                ide[14] = "1";                                                                                                      //<indFinal> Indica operação com Consumidor final - 0=Normal; 1=Consumidor final;

                /*0=Não se aplica (por exemplo, Nota Fiscal complementar ou de ajuste);
                1=Operação presencial;
                2=Operação não presencial, pela Internet;
                3=Operação não presencial, Teleatendimento;
                4=NFC-e em operação com entrega a domicílio;
                9=Operação não presencial, outros.*/
                ide[26] = "0";                                                                  //<indPres> Indicador de presença do comprador no estabelecimento comercial no momento da operação

                /* Grupo de informação das NF-e e cupom referenciado - tag NFref - Para multiplos valores utilizar ";" conforme exemplo */
                ide[13] = txtChave.Text.Trim(); //35170205561131000161570010000037131000039314;1231                 		//<NFref> refNFe ou refECF.nCOO

                ide[17] = "55"; //;02                 													//<NFref> refECF mod
                ide[18] = "0"; //;1123123                												//<NFref> refECF nECF

                /* Grupo de informação da NF modelo 1/1A referenciada */
                ide[19] = "";            //<cUF>    --> Utilizar a Tabela do IBGE
                ide[20] = "";            //<AAMM>   --> AAMM data da emissão da NF
                ide[21] = "";            //<CNPJ>   --> CNPJ do emitente da NF
                ide[22] = "";            //<mod>    --> Informar o código do modelo do Documento fiscal: 01 – modelo 01
                ide[23] = "";            //<serie>  --> Informar a série do documento fiscal
                ide[24] = "";            //<nNF>    --> 1 – 999999999            

                /* contingência */
                ide[15] = ""; // "2014-07-01T17:04:00-03:00";                      //<dhCont> Data da entrada em contingência
                ide[16] = ""; // "Teste de funcionamento em contingência";         //<xJust> Justificativa da entrada em contingência

                /* VerProc = Versão do aplicativo do cliente */
                ide[27] = "Versao Aplicativo";

                /* Grupo de informação da NFP (modelo 4) referenciada para Notas de Produtores Rurais*/
                ide[28] = "";            //<cUF>    --> Utilizar a Tabela do IBGE
                ide[29] = "";            //<AAMM>   --> AAMM da emissão da NF
                ide[30] = "";            //<CNPJ>   --> CNPJ do emitente da NF
                ide[31] = "";            //<CPF>    --> CPF do emitente da NF
                ide[32] = "";            //<IE>     --> Inscrição Estadual do emitente da NF
                ide[33] = "";            //<mod>    --> Informar o código do modelo do Documento fiscal: 01 – modelo 01
                ide[34] = "";            //<serie>  --> nformar a série do documento fiscal
                ide[35] = "";            //<nNF>    --> 1 – 999999999
                ide[36] = "";            //<refCTe> --> Referencia ao Cte   

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados de Identificação Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public bool DadosDoProdutoDevolucao()
        {
            try
            {
                prod = new string[dgDevolucao.RowCount, 200];
                TotalCOFINS = 0;
                TotalPIS = 0; 
                for (int x = 0; x < dgDevolucao.RowCount; x++)
                {
                    var buscaItens = new VendasItens();
                    dtRetorno = buscaItens.BuscaProdutosNFeDevolucao(Principal.estAtual, Principal.empAtual, dgDevolucao.Rows[x].Cells["PROD_CODIGO"].Value.ToString());

                    string retorno = CamposProduto(dtRetorno, true);

                    if (String.IsNullOrEmpty(retorno))
                    {
                        //for (int x = 0; x < prod.GetLength(0); x++)
                        //{
                        prod[x, 0] = "CFOP" + dtRetorno.Rows[0]["PROD_CFOP_DEVOLUCAO"];                                                             //<cProd> Código do produto ou serviço
                        prod[x, 1] = dtRetorno.Rows[0]["PROD_CODIGO"].ToString();                                                                   //<cEAN> GTIN (Global Trade Item Number) do produto, antigo código EAN ou código de barras
                        prod[x, 2] = Funcoes.RemoveCaracter(Funcoes.RemoverAcentuacao(dtRetorno.Rows[0]["PROD_DESCR"].ToString()));                 //<xProd> Descrição do produto ou serviço
                        prod[x, 3] = dtRetorno.Rows[0]["NCM"].ToString();                                                                           //<NCM> Código NCM com 8 dígitos
                        prod[x, 109] = "";                                                                                                          //<NVE> Codificação NVE - Nomenclatura de Valor Aduaneiro e Estatística
                        prod[x, 146] = dtRetorno.Rows[0]["PROD_CEST"].ToString();                                                                   //<CEST> 

                        prod[x, 186] = "S";                                                                                                         //<indEscala> Indicador de Escala Relevante - S - Produzido em Escala Relevante; N – Produzido em Escala NÃO Relevante.
                        prod[x, 187] = "";                                                                                                          //<CNPJFab> CNPJ do Fabricante da Mercadoria, obrigatório para produto em escala NÃO relevante.
                        prod[x, 188] = "";                                                                                                          //<cBenef> Código de Benefício Fiscal utilizado pela UF, aplicado ao item.

                        prod[x, 4] = "000";                                                                                                         //<EXTIPI>
                        prod[x, 5] = dtRetorno.Rows[0]["PROD_CFOP_DEVOLUCAO"].ToString();                                                           //<CFOP> Código Fiscal de Operações e Prestações
                        prod[x, 6] = dtRetorno.Rows[0]["PROD_UNIDADE"].ToString().ToUpper();                                                        //<uCom> Uniade Comercial
                        
                        prod[x, 7] = String.Format("{0:0.0000}", Convert.ToInt32(dgDevolucao.Rows[x].Cells["PROD_QTDE"].Value)).Replace(",", ".");                           //<qCom> Quantidade Comercial
                        prod[x, 8] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["PROD_PRECOMPRA"].Value).Replace(".", "").Replace(",", ".");                         //<vUnCom> Valor Unitário de Comercialização
                        prod[x, 9] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["PROD_PRECOMPRA"].Value) * Convert.ToInt32(dgDevolucao.Rows[x].Cells["PROD_QTDE"].Value)).Replace(".", "").Replace(",", ".");                          //<vProd> Valor Total Bruto do Produto ou Serviço
                        prod[x, 12] = String.Format("{0:0.0000}", Convert.ToInt32(dgDevolucao.Rows[x].Cells["PROD_QTDE"].Value)).Replace(",", ".");                            //<qTrib> Quantidade Tributável
                        prod[x, 13] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["PROD_PRECOMPRA"].Value)).Replace(".", "").Replace(",", ".");                     //<vUnTrib> Valor Unitário de tributação
                        prod[x, 16] = dgDevolucao.Rows[x].Cells["DESCONTO"].Value.ToString() == "0" ? "0.00" : String.Format("{0:N}", dgDevolucao.Rows[x].Cells["DESCONTO"].Value).Replace(".", "").Replace(",", ".");                                                                                                                       //<vDesc> Valor do Desconto  
                        prod[x, 20] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["vBC"].Value)).Replace(".", "").Replace(",", ".");                                                                                                           //<vBC> Valor da BC do ICMS
                        prod[x, 22] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["vICMS"].Value)).Replace(".", "").Replace(",", ".");                                                                                                             //<vICMS> Valor do ICMS      
                        prod[x, 49] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["vBCSTRet"].Value)).Replace(".", "").Replace(",", ".");                                                                                                                            //<vBCSTRet> Valor da BC do ICMS Retido Anteriormente
                        prod[x, 51] = String.Format("{0:N}", Convert.ToDouble(dgDevolucao.Rows[x].Cells["vICMSST"].Value)).Replace(".", "").Replace(",", ".");                                                                                                                             //<vICMSST>	Valor do ICMS ST						
                        //prod[x, 71] = dgDevolucao.Rows[x].Cells["Lote"].Value.ToString();                    //<n> Número do Lote do produto - Retirado na versão 4.00 e adicionado no grupo Rastro

                        TotalvBC += Convert.ToDouble(dgDevolucao.Rows[x].Cells["vBC"].Value);
                        TotalvICMS += Convert.ToDouble(dgDevolucao.Rows[x].Cells["vICMS"].Value);
                        TotalvBCSTRet += Convert.ToDouble(dgDevolucao.Rows[x].Cells["vBCSTRet"].Value);
                        TotalvICMSST += Convert.ToDouble(dgDevolucao.Rows[x].Cells["vICMSST"].Value);
                        descontoDevolucao += Convert.ToDouble(dgDevolucao.Rows[x].Cells["DESCONTO"].Value);
                                                
                        prod[x, 10] = dtRetorno.Rows[0]["PROD_CODIGO"].ToString();                                                                  //<cEANTrib> GTIN (Global Trade Item Number) da unidade tributável, antigo código EAN ou código de barras
                        prod[x, 11] = dtRetorno.Rows[0]["PROD_UNIDADE"].ToString().ToUpper();                                                 //<uTrib> Unidade Tributável
                        prod[x, 14] = "";                                                                                                                           //<vFrete> Valor Total do Frete
                        prod[x, 15] = "";                                                                                                                           //<vSeg> Valor Total do Seguro

                        prod[x, 86] = "";                                                                                                                           //<vOutro> Outras despesas acessórias    


                        prod[x, 96] = "";                                                                                                           //<vTotTrib> Valor total dos tributos

                        /* tag ISSQN */
                        prod[x, 39] = "";                                                                                                           //ISSQN <vBC> Valor da Base de Cálculo ISSQN
                        prod[x, 40] = "";                                                                                                           //ISSQN <vAliq> Valor da Aliquota ISSQN
                        prod[x, 41] = "";                                                                                                           //ISSQN <vISSQN> Valor do ISSQN 
                        prod[x, 42] = "";                                                                                                           //ISSQN <cMunFG> Código do município de ocorrência do fato gerador do ISSQN
                        prod[x, 43] = "";                                                                                                           //ISSQN <cListServ>	Item da Lista de Serviços			
                        prod[x, 70] = "";                                                                                                           //ISSQN <cSitTrib> Código da tributação do ISSQN: N – NORMAL; R – RETIDA; S –SUBSTITUTA; I – ISENTA. (v.2.0)

                        //NF-e 3.10
                        prod[x, 119] = "";                                                                                                          //ISSQN <vDeducao> Valor dedução para redução da Base de Cálculo
                        prod[x, 120] = "";                                                                                                          //ISSQN <vOutro> Valor outras retenções
                        prod[x, 121] = "";                                                                                                          //ISSQN <vDescIncond> Valor desconto incondicionado
                        prod[x, 122] = "";                                                                                                          //ISSQN <vDescCond> Valor desconto condicionado
                        prod[x, 123] = "";                                                                                                          //ISSQN <vISSRet> Valor retenção ISS

                        /*<indISS> 
                         * 1=Exigível
                         * 2=Não incidência; 
                         * 3=Isenção; 
                         * 4=Exportação; 
                         * 5=Imunidade; 
                         * 6=Exigibilidade Suspensa por Decisão Judicial; 
                         * 7=Exigibilidade Suspensa por Processo Administrativo; */
                        prod[x, 124] = "";                                                                                                          //ISSQN <indISS> Indicador da exigibilidade do ISS
                        prod[x, 125] = "";                                                                                                          //ISSQN <cMun> Código do Município de incidência do imposto
                        prod[x, 126] = "";                                                                                                          //ISSQN <cPais> Código do País onde o serviço foi prestado
                        prod[x, 127] = "";                                                                                                          //ISSQN <nProcesso> Número do processo judicial ou administrativo de suspensão da exigibilidade
                        prod[x, 128] = "";                                                                                                          //ISSQN <indIncentivo> Indicador de incentivo Fiscal
                        prod[x, 134] = "";                                                                                                          //ISSQN <cServico> Código do serviço prestado dentro do município

                        /* tag ICMS */
                        prod[x, 17] = "0";                                                                                                          //<orig> Origem da mercadoria

                        if (!Funcoes.LeParametro(6, "320", true).Equals("1"))
                        {
                            if (dtRetorno.Rows[0]["PROD_CST"].ToString().Length > 2)
                            {
                                prod[x, 18] = dtRetorno.Rows[0]["PROD_CST"].ToString().Substring(1, 2);                                              //<CST> Tributação do ICMS
                            }
                            else
                            {
                                prod[x, 18] = dtRetorno.Rows[0]["PROD_CST"].ToString();                                                             //<CST> Tributação do ICMS
                            }
                        }
                        else
                        {
                            prod[x, 18] = CSOSN(Funcoes.FormataZeroAEsquerda(dtRetorno.Rows[0]["PROD_CST"].ToString(), 3));                         //<CST> Tributação do ICMS
                        }

                        prod[x, 19] = "0";                                                                                                          //<modBC> Modalidade de determinação da BC do ICMS
                                                                                      // prod[x, 20] = "1996.86";                                                                                                          //<vBC> Valor da BC do ICMS
                        prod[x, 21] = TotalvICMS > 0 ? "18.00" : "0";                                                                                                          //<pICMS> Alíquota do imposto ICMS
                                                                                                                                                    // prod[x, 22] = "359.43";                                                                                                          //<vICMS> Valor do ICMS                

                        /*<modBCST>
                        0=Preço tabelado ou máximo sugerido;
                        1=Lista Negativa (valor);
                        2=Lista Positiva (valor);
                        3=Lista Neutra (valor);
                        4=Margem Valor Agregado (%);
                        5=Pauta (valor);*/
                        prod[x, 46] = dtRetorno.Rows[0]["PROD_LISTA"].ToString() == "P" ? "2" : dtRetorno.Rows[0]["PROD_LISTA"].ToString() == "N" ? "1" : "3";      //<modBCST>	Modalidade de determinação da BC do ICMS ST 		
                        prod[x, 47] = "";                                                                                                                           //<pMVAST>	Percentual da margem de valor Adicionado do ICMS ST 		
                        prod[x, 48] = "";                                                                                                                           //<pRedBCST> Percentual da Redução de BC do ICMS ST		

                        //prod[x, 49] = "2233.76";                                                                                                                  //<vBCSTRet> Valor da BC do ICMS Retido Anteriormente
                        prod[x, 50] = TotalvICMSST > 0 ? "18.00" : "0";                                                                          //<vICMSSTRet> Valor do ICMS Retido Anteriormente

                        //NF-e v4.0
                        prod[x, 198] = "0";                                                                                                                         //<pST> Alíquota suportada pelo Consumidor Final

                        prod[x, 190] = "";                                                                                                                          //<pFCP> Percentual do Fundo de Combate à Pobreza (FCP) 
                        prod[x, 191] = "";                                                                                                                          //<vFCP> Valor do Fundo de Combate à Pobreza (FCP)

                        prod[x, 195] = "0";                                                                                                                         //<vBCFCP> Valor da Base de Cálculo do FCP
                        prod[x, 192] = "0";                                                                                                                         //<vBCFCPST> Valor da Base de Cálculo do FCP retido por Substituição Tributária
                        prod[x, 193] = "2.00";                                                                                                                      //<pFCPST> Percentual do FCP retido por Substituição Tributária
                        prod[x, 194] = "0";                                                                                                                         //<vFCPST> Valor do FCP retido por Substituição Tributária
                        prod[x, 196] = "2.00";                                                                                                                      //<pFCPSTRet> Percentual do FCP retido anteriormente por Substituição Tributária
                        prod[x, 197] = "0";                                                                                                                         //<vFCPSTRet> Valor do FCP retido anteriormente por Substituição Tributária
                        prod[x, 199] = "0";                                                                                                                         //<vBCFCPSTRet> Valor da Base de Cálculo do FCP retido anteriormente

                        prod[x, 142] = "";                                                                                                                          //<vBCSTDest> Valor da BC do ICMS ST da UF destino 
                        prod[x, 143] = "";                                                                                                                          //<vICMSSTDest> Valor do ICMS ST da UF destino

                        // prod[x, 51] = "42.78";                                                                                                                   //<vICMSST>	Valor do ICMS ST						
                        prod[x, 52] = "";                                                                                                                           //<pRedBC> Percentual da Redução de BC

                        prod[x, 80] = "0";                                                                                                                          //<pCredSN>	Alíquota aplicável de cálculo do crédito (Simples Nacional).						
                        prod[x, 81] = "0";                                                                                                                          //<vCredICMSSN>	Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC 123 (Simples Nacional)	

                        prod[x, 85] = "9";                                                                                                                           //<motDesICMS> Motivo da desoneração do ICMS - (ocorrência 1-1) Nota:2013/005 Campo será preenchido quando o campo anterior estiver preenchido. Informar o motivo da desoneração:3=Uso na agropecuária;9=Outros;12=Órgão de fomento e desenvolvimento agropecuário.         

                        /* tag IPI */
                        /* <CST> 
                        01=Entrada tributada com alíquota zero
                        02=Entrada isenta
                        03=Entrada não-tributada
                        04=Entrada imune
                        05=Entrada com suspensão
                        51=Saída tributada com alíquota zero
                        52=Saída isenta
                        53=Saída não-tributada
                        54=Saída imune
                        55=Saída com suspensão*/
                        prod[x, 23] = "53";                                                                                                                         //IPI <CST> Código da situação tributária do IPI

                        /* obs: Informar os campos INDEX 24 e 25 caso o cálculo do IPI seja por alíquota 
                         ou os campos INDEX 78 e 79 caso o cálculo do IPI seja valor por unidade. */
                        prod[x, 78] = string.Empty;                                                                                                                 //IPI <qUnid> Quantidade total na unidade padrão para tributação (somente para os produtos tributados por unidade)
                        prod[x, 79] = string.Empty;                                                                                                                 //IPI <vUnid> Valor por Unidade Tributável

                        prod[x, 24] = "0";                                                                                                                          //IPI <vBC> Valor da BC do IPI
                        prod[x, 25] = string.Empty;                                                                                                                 //IPI <pIPI> Alíquota do IPI
                                                                                                                                                                    /* fim obs */

                        prod[x, 87] = "0";                                                                                                                          //IPI <clEnq> Classe de enquadramento do IPI para Cigarros e Bebidas
                        prod[x, 88] = "00000000000000";                                                                                                             //IPI <CNPJProd> CNPJ do produtor da mercadoria, quando diferente do emitente. Somente para os casos de exportação direta ou indireta.
                        prod[x, 89] = "0";                                                                                                                          //IPI <cSelo> Código do selo de controle IPI
                        prod[x, 90] = "0";                                                                                                                          //IPI <qSelo> Quantidade de selo de controle
                        prod[x, 91] = "999";                                                                                                                        //IPI <cEnq> Código de Enquadramento Legal do IPI

                        prod[x, 26] = string.Empty;                                                                                                                 //IPI <vIPI> Valor do IPI
                                                                                                                                                                    /* tag II */
                        prod[x, 27] = "0.00";                                                                                                                       //II <vBC> Valor BC do Imposto de Importação
                        prod[x, 28] = "0.00";                                                                                                                       //II <vDespAdu> Valor despesas aduaneiras
                        prod[x, 29] = "0.00";                                                                                                                       //II <vII> Valor Imposto de Importação
                        prod[x, 30] = "0.00";                                                                                                                       //II <vIOF> Valor Imposto sobre Operações Financeiras

                        /* tag PIS */
                       // ValorPIS = Convert.ToDouble(dgDevolucao.Rows[x].Cells["VBCPIS"].Value) * (Convert.ToDouble(dgDevolucao.Rows[x].Cells["PALIQPIS"].Value)/100);
                        TotalPIS += dgDevolucao.Rows[x].Cells["valor_pis"].Value.ToString() == "0" ? 0 : Convert.ToDouble(dgDevolucao.Rows[x].Cells["valor_pis"].Value.ToString().Replace(".", ","));
                        prod[x, 31] = Funcoes.LeParametro(16, "01", false); //"07";                                                                                  //<CST> Código de Situação Tributária do PIS
                        prod[x, 32] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["VBCPIS"].Value.ToString() == "0" ? "0" : dgDevolucao.Rows[x].Cells["VBCPIS"].Value.ToString()).Replace(",", ".");//"0"; //<vBC> Valor da Base de Cálculo do PIS
                        prod[x, 33] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["PALIQPIS"].Value.ToString() == "0" ? "0" : dgDevolucao.Rows[x].Cells["PALIQPIS"].Value.ToString()).Replace(",", "."); //<pPIS> Alíquota do PIS (em percentual)
                        prod[x, 34] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["valor_pis"].Value.ToString() == "0" ? "0" : dgDevolucao.Rows[x].Cells["valor_pis"].Value.ToString()).Replace(",", ".");                                                                                     //<vPis> Valor do PIS
                        prod[x, 45] = "";                                                                                                                           //<vAliqProd> Alíquota do PIS (em reais)     
                        prod[x, 77] = "";                                                                                                                           //<qBCProd> Quantidade Vendida

                        /* tag COFINS */
                       // ValorCOFINS = Convert.ToDouble(dgDevolucao.Rows[x].Cells["VBCCOFINS"].Value) * (Convert.ToDouble(dgDevolucao.Rows[x].Cells["PALIQCOFINS"].Value)/100);
                        TotalCOFINS += dgDevolucao.Rows[x].Cells["valor_cofins"].Value.ToString()
                            == "0.00" ? 0 : Convert.ToDouble(dgDevolucao.Rows[x].Cells["valor_cofins"].Value.ToString().Replace(".", ","));
                        prod[x, 35] = Funcoes.LeParametro(16, "01", false); //"07";                                                                                   //<CST> Código de Situação Tributária do Cofins
                        prod[x, 36] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["VBCCOFINS"].Value.ToString() == "0.00" ? "0.00" : dgDevolucao.Rows[x].Cells["VBCCOFINS"].Value.ToString()).Replace(",",".");//"0.00";//<vBC> Valor da Base de Cálculo da COFINS
                        prod[x, 37] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["PALIQCOFINS"].Value.ToString() == "0.00" ? "0.00" : dgDevolucao.Rows[x].Cells["PALIQCOFINS"].Value.ToString()).Replace(",",".");//<pCOFINS> Alíquota da COFINS (em percentual)
                        prod[x, 38] = String.Format("{0:N}", dgDevolucao.Rows[x].Cells["valor_cofins"].Value.ToString()
                            == "0.00" ? "0.00" : dgDevolucao.Rows[x].Cells["valor_cofins"].Value.ToString()).Replace(",", ".");                                                                      //<vCOFINS> Valor da COFINS
                        prod[x, 44] = "";                                                                                                                           //<vAliqProd> Alíquota da COFINS (em reais)    
                        prod[x, 97] = "";                                                                                                                           //<qBCProd> Quantidade Vendida

                        /*tag PISST*/
                        prod[x, 54] = "";                                                                                                                           //<vBC> Valor da Base de Cálculo do PIS
                        prod[x, 55] = "";                                                                                                                           //<pPIS> Alíquota do PIS (em percentual)
                        prod[x, 56] = "";                                                                                                                           //<vPIS> Valor do PIS

                        /* tag COFINSST */
                        prod[x, 57] = "";                                                                                                                           //<vBC> Valor da Base de Cálculo da COFINS
                        prod[x, 58] = "";                                                                                                                           //<pCOFINS> Alíquota da COFINS (em percentual)
                        prod[x, 59] = "";                                                                                                                           //<vCOFINS> Valor da COFINS

                        /* Tag da Declaração de Importação | DI - Multiplas ocorrencias utilizar ";", conforme exemplo*/
                        prod[x, 60] = ""; // "43434";                                                                                                               //<nDI> Número do Documento de Importação (DI, DSI, DIRE, ...)
                        prod[x, 61] = ""; // "2010-06-08";                                                                                                          //<dDI> Data de Registro do documento - Formato: “AAAA-MM-DD”
                        prod[x, 62] = ""; // "Moscou";                                                                                                              //<xLocDesemb> Local de desembaraço
                        prod[x, 63] = ""; // "SP";                                                                                                                  //<UFDesemb> Sigla da UF onde ocorreu o Desembaraço Aduaneiro
                        prod[x, 64] = ""; // "2010-06-22";                                                                                                          //<dDesemb> Data do Desembaraço Aduaneiro
                        prod[x, 65] = ""; // "80809808";                                                                                                            //<cExportador> Código do Exportador
                        prod[x, 66] = ""; // "122;144;133;";                                                                                                        //adi: <nAdicao> Numero da Adição
                        prod[x, 67] = ""; // "233;321;456;";                                                                                                        //adi: <nSeqAdic> Numero sequencial do item dentro da Adição
                        prod[x, 68] = ""; // "455;323;546;";                                                                                                        //adi: <cFabricante> Código do fabricante estrangeiro
                        prod[x, 69] = ""; // "122.22;;482.36;";                                                                                                     //adi: <vDescDI> Valor do desconto do item da DI – Adição

                        prod[x, 53] = "";                                                                                                                           //<infAdProd> Informações Adicionais do Produto


                        /* Grupo do detalhamento de Medicamentos e de matériasprimas farmacêuticas, Multiplas ocorrencias utilizar ";" */
                        /*Utilizar as tags 71,72,73 e 74 e 185 para o grupo de rastro caso, versão 4.00*/
                        prod[x, 71] = dgDevolucao.Rows[x].Cells["Lote"].Value.ToString();       //String.Format("{0:0.000}", Convert.ToInt32(String.IsNullOrEmpty(dgDevolucao.Rows[x].Cells["LOTE"].ToString()) ? 0 : dgDevolucao.Rows[x].Cells["LOTE"].Value)); // "21;13;44;";                                                         //<nLote> Número do Lote do produto - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 72] = String.Format("{0:0.000}", Convert.ToInt32(dgDevolucao.Rows[x].Cells["PROD_QTDE"].Value)).Replace(",", "."); // "123.123;656.656;967.232;";                                                                                            //<qLote> Quantidade de produto no Lote - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 73] = dgDevolucao.Rows[x].Cells["DataFabricacao"].Value.ToString() == "" ? "" : Convert.ToDateTime(dgDevolucao.Rows[x].Cells["DataFabricacao"].Value).ToString("yyyy-MM-dd"); // "2010-01-01;2011-02-02;2009-01-01;";                        //<dFab> Data de fabricação/ Produção - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 74] = dgDevolucao.Rows[x].Cells["DataValidade"].Value.ToString() == "" ? "" : Convert.ToDateTime(dgDevolucao.Rows[x].Cells["DataValidade"].Value).ToString("yyyy-MM-dd");                                                                    //<dVal> Data de validade - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 189] = "";                                                                                                                          //<cAgreg> Código de Agregação

                        prod[x, 185] = dtRetorno.Rows[0]["PROD_REGISTRO_MS"].ToString();                                                                            //<cProdANVISA> Código de Produto da ANVISA

                        if (String.IsNullOrEmpty(dgDevolucao.Rows[x].Cells["Lote"].Value.ToString())) 
                        {
                            prod[x, 75] = "";                                                                                                                           //<vPMC> Preço máximo consumidor
                        }
                        else
                        {
                            prod[x, 75] = String.Format("{0:N}", dtRetorno.Rows[0]["MED_PCO18"]).Replace(".", "").Replace(",", ".");                                                                                                                          //<vPMC> Preço máximo consumidor
                        }

                        prod[x, 76] = "1";                                                                                                                          //<indTot> Indica se valor do Item (vProd) entra no valor total da NF-e (vProd) - 0 = Valor do item (vProd) não compõe o valor total da NF-e; 1 = Valor do item (vProd) compõe o valor total da NF-e (vProd) (v2.0);

                        prod[x, 83] = txtPedido.Text;                                                                                                               //<xPed> Número do Pedido de Compra
                        prod[x, 84] = (x + 1).ToString();                                                                                                           //<nItemPed> Item do Pedido de Compra

                        /* Grupo do detalhamento de Combustíveis */
                        prod[x, 92] = "";                                                                                                                           // <cProdANP> Código de produto da ANP (ocorrência 1-1) --> Nota: se não for informada essa posição, não será gerado o grupo <comb>

                        prod[x, 180] = "";                                                                                                                          //<descANP> Descrição do Produto
                        prod[x, 181] = "";                                                                                                                          //<pGLP> Percentual do GLP derivado do petróleo
                        prod[x, 182] = "";                                                                                                                          //<pGNn> Percentual do Gás Natural Nacional
                        prod[x, 183] = "";                                                                                                                          //<pGNi> Percentual de Gás Natural Importado
                        prod[x, 184] = "";                                                                                                                          //<vPart> Valor de Partida

                        prod[x, 93] = "";                                                                                                                           //<CODIF> Código de autorização / registro do CODIF    (ocorrência 0-1)
                        prod[x, 94] = "";                                                                                                                           //<qTemp>  Quantidade de combustível faturada à temperatura ambiente.   (ocorrência 0-1)
                        prod[x, 95] = "";                                                                                                                           //<UFCons> Sigla da UF de consumo  (ocorrência 1-1)             

                        prod[x, 105] = "";                                                                                                                          //<nFCI> Número de controle da FCI - Ficha de Conteúdo de Importação (ocorrência 0-1) 
                        prod[x, 106] = "0.00";                                                                                                                      //<vICMSDeson> Valor do ICMS desonerado - Informar apenas nos motivos de desoneração  3=Uso na agropecuária; 9=Outros; 12=Órgão de fomento e desenvolvimento agropecuário.(ocorrência 1-1)

                        /*<tpViaTransp>
                        1=Marítima;
                        2=Fluvial
                        3=Lacustre;
                        4=Aérea;
                        5=Postal
                        6=Ferroviária;
                        7=Rodoviária;
                        8=Conduto / Rede Transmissão;
                        9=Meios Próprios;
                        10=Entrada / Saída ficta.
                        11=Courier;
                        12=Handcarry*/
                        prod[x, 107] = "7";                                                                                                                         //<tpViaTransp> Via de transporte internacional informada na Declaração de Importação (DI)
                        prod[x, 108] = "1";                                                                                                                         //<tpIntermedio> Forma de importação quanto a intermediação - 1=Importação por conta própria; 2=Importação por conta e ordem; 3=Importação por encomenda;


                        //<detExport> - Tag destinada a Exportação
                        prod[x, 110] = "";                                                                                                                          //<nDraw> Número do ato concessório de Drawback - Tag destinada a Exportação
                        prod[x, 111] = "";                                                                                                                          //<chNFe> Chave de Acesso da NF-e recebida para exportação - Tag destinada a Exportação
                        prod[x, 112] = "";                                                                                                                          //<nRE> Número do Registro de Exportação - Tag destinada a Exportação
                        prod[x, 113] = "";                                                                                                                          //<qExport> Quantidade do item realmente exportado - Tag destinada a Exportação

                        prod[x, 114] = "";                                                                                                                          //<nRECOPI> Número do RECOPI - Tag para operações com Papel Imune.

                        /* Grupo do detalhamento de Armas */
                        prod[x, 115] = "";                                                                                                                          //<tpArma> Indicador do tipo de arma de fogo - 0=Uso permitido; 1=Uso restrito;
                        prod[x, 116] = "";                                                                                                                          //<nSerie> Número de série da arma
                        prod[x, 117] = "";                                                                                                                          //<nCano> Número de série do cano
                        prod[x, 118] = "";                                                                                                                          //<desc> Descrição completa da arma, compreendendo: calibre, marca, capacidade, tipo de funcionamento, comprimento e demais elementos que permitam a sua perfeita identificação

                        /*Gruno para detalhamento de Devolução. <finNFe> igual a 4.*/
                        prod[x, 129] = "";                                                                                                                          //<pDevol> Percentual da mercadoria devolvida
                        prod[x, 130] = "";                                                                                                                          //<IPI> Informação do IPI devolvido
                        prod[x, 131] = "";                                                                                                                          //<vIPIDevol> Valor do IPI devolvido

                        prod[x, 132] = "";                                                                                                                          //<pMixGN> Percentual de Gás Natural para o produto GLP (cProdANP=210203001)
                        prod[x, 133] = "";                                                                                                                          //<vAFRMM> Valor da AFRMM - Adicional ao Frete para Renovação da Marinha Mercante

                        prod[x, 135] = "";                                                                                                                          //<CNPJ> CNPJ do adquirente ou do encomendante
                        prod[x, 136] = "";                                                                                                                          //<UFTerceiro> Sigla da UF do adquirente ou do encomendante

                        //ICMS 51
                        prod[x, 137] = "";                                                                                                                          //<vICMSOp> Valor do ICMS da Operação
                        prod[x, 138] = "";                                                                                                                          //<pDif> Percentual do diferimento
                        prod[x, 139] = "";                                                                                                                          //<vICMSDif> Valor do ICMS diferido

                        //ICMSUFDest
                        //Para gerar o ICMSUFDest todos os indices do vetor devem conter dados
                        prod[x, 147] = "";                                                                                                                          //<vBCUFDest> Valor da BC do ICMS na UF de destino
                        prod[x, 148] = "";                                                                                                                          //<pFCPUFDest> Percentual do ICMS relativo ao Fundo de Combate à Pobreza (FCP) na UF de destino
                        prod[x, 149] = "";                                                                                                                          //<pICMSUFDest> Alíquota interna da UF de destino
                        prod[x, 150] = "";                                                                                                                          //<pICMSInter> Alíquota interestadual das UF envolvidas
                        prod[x, 151] = "";                                                                                                                          //<pICMSInterPart> Percentual provisório de partilha do ICMS Interestadual
                        prod[x, 152] = "";                                                                                                                          //<vFCPUFDest> Valor do ICMS relativo ao Fundo de Combate à Pobreza (FCP) da UF de destino
                        prod[x, 153] = "";                                                                                                                          //<vICMSUFDest> Valor do ICMS Interestadual para a UF de destino
                        prod[x, 154] = "";                                                                                                                          //<vICMSUFRemet> Valor do ICMS Interestadual para a UF do remetente
                        prod[x, 179] = "";                                                                                                                          //<vBCFCPUFDest> Valor da BC FCP na UF de destino

                        /*Detalhamento Específico de Veículos novos */
                        prod[x, 155] = "";                                                                                                                          //<tpOp> Tipo da operação - 1=Venda concessionária; 2=Faturamento direto para consumidor final; 3=Venda direta para grandes consumidores (frotista, governo, ...); 0=Outros
                        prod[x, 156] = "";                                                                                                                          //<chassi> Chassi do veículo
                        prod[x, 157] = "";                                                                                                                          //<cCor> Cor - Código de cada montadora
                        prod[x, 158] = "";                                                                                                                          //<xCor> Descrição da Cor
                        prod[x, 159] = "";                                                                                                                          //<pot> Potência Motor (CV)
                        prod[x, 160] = "";                                                                                                                          //<cilin> Cilindradas 
                        prod[x, 161] = "";                                                                                                                          //<pesoL> Peso Líquido 
                        prod[x, 162] = "";                                                                                                                          //<pesoB> Peso Bruto
                        prod[x, 163] = "";                                                                                                                          //<nSerie> Serial (série)
                        prod[x, 164] = "";                                                                                                                          //<tpComb> Tipo de combustível - Utilizar Tabela RENAVAM (v2.0); 01=Álcool, 02=Gasolina, 03=Diesel, (...); 16=Álcool/Gasolina; 17=Gasolina/Álcool/GNV; 18=Gasolina/Elétrico
                        prod[x, 165] = "";                                                                                                                          //<nMotor> Número de Motor
                        prod[x, 166] = "";                                                                                                                          //<CMT> Capacidade Máxima de Tração
                        prod[x, 167] = "";                                                                                                                          //<dist> Distância entre eixos
                        prod[x, 168] = "";                                                                                                                          //<anoMod> Ano Modelo de Fabricação
                        prod[x, 169] = "";                                                                                                                          //<anoFab> Ano de Fabricação
                        prod[x, 170] = "";                                                                                                                          //<tpPint> Tipo de Pintura
                        prod[x, 171] = "";                                                                                                                          //<tpVeic> Tipo de Veículo
                        prod[x, 172] = "";                                                                                                                          //<espVeic> Espécie de Veículo - 1=PASSAGEIRO; 2=CARGA; 3=MISTO; 4 = CORRIDA; 5 = TRAÇÃO; 6 = ESPECIAL;
                        prod[x, 173] = "";                                                                                                                          //<VIN> Condição do VIN(chassi) - R=Remarcado; N=Normal
                        prod[x, 174] = "";                                                                                                                          //<condVeic> Condição do Veículo - 1=Acabado; 2=Inacabado; 3=Semiacabado
                        prod[x, 175] = "";                                                                                                                          //<cMod> Código Marca Modelo
                        prod[x, 176] = "";                                                                                                                          //<cCorDENATRAN> Código da Cor - 01=AMARELO, 02=AZUL, 03=BEGE, 04=BRANCA, 05=CINZA, 06=-DOURADA, 07=GRENÁ, 08=LARANJA, 09=MARROM, 10=PRATA, 11=PRETA, 12=ROSA, 13=ROXA, 14=VERDE, 15=VERMELHA, 16=FANTASIA
                        prod[x, 177] = "";                                                                                                                          //<lota> Capacidade máxima de lotação
                        prod[x, 178] = "";
                        //}
                    }
                    else
                    {
                        MessageBox.Show("CADASTRO DE PRODUTOS\n\r" + camposFaltantes);
                        tslRegistros.Text = "";
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados dos Produtos Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public bool DadosTotalizadoresDevolucao()
        {
            try
            {
                   total = new string[42];
                double totalDevolucao = 0;

                for (int i = 0; i < dgDevolucao.RowCount; i++)
                {
                    totalDevolucao += (Convert.ToDouble(dgDevolucao.Rows[i].Cells["PROD_PRECOMPRA"].Value) * Convert.ToInt32(dgDevolucao.Rows[i].Cells["PROD_QTDE"].Value));
                }

                /*<total> TAG de grupo de Valores Totais da NF-eI*/

                total[0] = prod[0, 18] == "101" ? "0.00" : String.Format("{0:N}", TotalvBC).Replace(".", "").Replace(",", ".");                                         //ICMSTot <vBC> Base de Cálculo do ICMS
                total[1] = prod[0,18] == "101" ? "0.00" : String.Format("{0:N}", TotalvICMS).Replace(".", "").Replace(",", ".");                                       //ICMSTot <vICMS> Valor Total do ICMS
                total[27] = "0.00";                                                                                                     //ICMSTot <vICMSDeson> Valor Total do ICMS desonerado
                total[2] = String.Format("{0:N}", TotalvBCSTRet).Replace(".", "").Replace(",", ".");                                    //ICMSTot <vBCST> Base de Cálculo do ICMS ST
                total[3] = String.Format("{0:N}", TotalvICMSST).Replace(".", "").Replace(",", ".");                                     //ICMSTot <vST> Valor Total do ICMS ST
                total[4] = String.Format("{0:N}", totalDevolucao).Replace(".", "").Replace(",", ".");                                   //ICMSTot <vProd> Valor Total dos produtos e serviços
                total[5] = "0.00";                                                                                                      //ICMSTot <vFrete> Valor Total do Frete
                total[6] = "0.00";                                                                                                      //ICMSTot <vSeg> Valor Total do Seguro
                total[7] = descontoDevolucao == 0 ? "0.00" : String.Format("{0:N}", descontoDevolucao).Replace(".", "").Replace(",", ".");//ICMSTot <vDesc> Valor Total do Desconto
                total[8] = "0.00";                                                                                                      //ICMSTot <vII> Valor Total do II
                total[9] = "0.00";                                                                                                      //ICMSTot <vIPI> Valor Total do IPI
                total[10] = String.Format("{0:N}", TotalPIS.ToString()).Replace(".", "").Replace(",", "."); //"0.00";                   //ICMSTot <vPIS> Valor do PIS
                total[11] = String.Format("{0:N}", TotalCOFINS.ToString()).Replace(".", "").Replace(",", ".");//"0.00";                 //ICMSTot <vCOFINS> Valor da COFINS
                total[12] = "0.00";                                                                                                     //ICMSTot <vOutro> Outras Despesas acessórias
                total[13] = String.Format("{0:N}", (totalDevolucao - descontoDevolucao) + TotalvICMSST).Replace(".", "").Replace(",", ".");                                       //ICMSTot <vNF> Valor Total da NF-e

                totalNota = totalDevolucao;

                total[26] = "0.00";                                                                                     //<vTotTrib> Valor aproximado total de tributos federais, estaduais e municipais.

                total[35] = "0.00";                                                                                     //<vICMSUFDest> Valor total do ICMS Interestadual para a UF de destino
                total[36] = "0.00";                                                                                     //<vICMSUFRemet> Valor total do ICMS Interestadual para a UF do remetente
                total[37] = "0.00";                                                                                     //<vFCPUFDest> Valor total do ICMS relativo Fundo de Combate à Pobreza (FCP) da UF de destino
                total[38] = "0.00";                                                                                     //<vFCP> Valor Total do FCP (Fundo de Combate à Pobreza)
                total[39] = "0.00";                                                                                     //<vFCPST> Valor Total do FCP (Fundo de Combate à Pobreza) retido por substituição tributária
                total[40] = "0.00";                                                                                     //<vFCPSTRet> Valor Total do FCP retido anteriormente por Substituição Tributária
                total[41] = "0.00";                                                                                     //<vIPIDevol> Valor Total do IPI devolvido

                total[14] = "";                                                                                         //ISSQNtot <vServ> Valor total dos Serviços sob não-incidência ou não tributados pelo ICMS
                total[15] = "";                                                                                         //ISSQNtot <vBC> Valor total Base de Cálculo do ISS
                total[16] = "";                                                                                         //ISSQNtot <vISS> Valor total do ISS
                total[17] = "";//TotalPIS.ToString().Replace(",",".");                                                       //ISSQNtot <vPIS> Valor total do PIS sobre serviços
                total[18] = "";//TotalCOFINS.ToString().Replace(",", ".");                                                   //ISSQNtot <vCOFINS> Valor total da COFINS sobre serviços
                total[28] = "";                                                                                         //ISSQNtot <dCompet> Data da prestação do serviço - Formato: “AAAA-MM-DD”
                total[29] = "";                                                                                         //ISSQNtot <vDeducao> Valor total dedução para redução da Base de Cálculo
                total[30] = "";                                                                                         //ISSQNtot <vOutro> Valor total outras retenções
                total[31] = "";                                                                                         //ISSQNtot <vDescIncond> Valor total desconto incondicionado
                total[32] = "";                                                                                         //ISSQNtot <vDescCond> Valor total desconto condicionado
                total[33] = "";                                                                                         //ISSQNtot <vISSRet> Valor total retenção ISS
                total[34] = "";                                                                                         //ISSQNtot <cRegTrib> Código do Regime Especial de Tributação - 1=Microempresa Municipal; 2=Estimativa; 3=Sociedade de Profissionais; 4=Cooperativa; 5=Microempresário Individual (MEI); 6=Microempresário e Empresa de Pequeno Porte (ME/EPP)

                /* retTrib: Grupo de Retenções de Tributos */
                total[19] = "";                                                                                         //<vRetPIS> Valor Retido de PIS 
                total[20] = "";                                                                                         //<vRetCOFINS> Valor Retido de COFINS
                total[21] = "";                                                                                         //<vRetCSLL> Valor Retido de CSLL
                total[22] = "";                                                                                         //<vBCIRRF> Base de Cálculo do IRRF
                total[23] = "";                                                                                         //<vIRRF> Valor Retido do IRRF
                total[24] = "";                                                                                         //<vBCRetPrev> Base de Cálculo da Retenção da Previdência Social
                total[25] = "";                                                                                         //<vRetPrev> Valor da Retenção da Previdência Social

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados Totalizadores Devolução", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }
        #endregion

        public string GeraNumeroRandomico()
        {
            Random rdn = new Random();
            numeroRandomico = Convert.ToString(rdn.Next(99999999));
            return numeroRandomico;
        }

        #region DADOS DA NOTA
        public bool DadosDeIdentificacao(DateTime dataVenda)
        {
            try
            {
                ide = new string[37];
                ide[0] = Funcoes.LeParametro(6, "318", true);                                                                       //<cUF> Código da UF do emitente do Documento Fiscal 

                ide[1] = GeraNumeroRandomico();
                if (cmbDocumento.Text.Substring(0, 1) == "1")
                {
                    ide[2] = "VENDA DE MERCADORIA";                                                                                 //<natOp> Descrição da Natureza da Operação
                }
                else if (cmbDocumento.Text.Substring(0, 1) == "0")
                {
                    ide[2] = "COMPRA DE MERCADORIA";                                                                                //<natOp> Descrição da Natureza da Operação
                }
                else
                {
                    ide[2] = "REMESSA PARA CONSERTO";                                                                                //<natOp> Descrição da Natureza da Operação
                }
                ide[3] = "";                                                                                                        //<indPag> Indicador da forma de pagamento - Retirado na versão 4.00
                ide[4] = "55";                                                                                                      //<mod> Código do Modelo do Documento Fiscal
                ide[5] = "1";                                                                                                       //<serie> Série do Documento Fiscal
                ide[6] = Funcoes.LeParametro(6, "345", true);                                                                       //<nNF> Número do Documento Fiscal
                ide[7] = dataVenda.ToString("yyyy-MM-ddTHH:mm:sszzz");                                                              //<dhEmi> Data e hora de emissão do Documento Fiscal
                ide[8] = dataVenda.ToString("yyyy-MM-ddTHH:mm:sszzz");                                                              //<dhSaiEnt> Data e hora de Saída ou da Entrada da Mercadoria/Produto
                ide[25] = cmbOperacao.Text.Substring(0, 1);                                                                         //<idDest> Identificador de local de destino da operação
                if (cmbDocumento.Text.Substring(0, 1) == "2")
                {
                    ide[9] = "1";                                                                         //<tpNF> Tipo de Operação - 0=Entrada; 1=Saída
                }
                else
                    ide[9] = cmbDocumento.Text.Substring(0, 1);                                                                         //<tpNF> Tipo de Operação - 0=Entrada; 1=Saída


                ide[10] = Funcoes.LeParametro(6, "319", true);                                                                      //<cMunFG> Código do Município de Ocorrência do Fato Gerador
                ide[11] = "1";                                                                                                      //<tpEmis> Tipo de Emissão da NF-e

                ide[12] = cmbEmissao.Text.Substring(0, 1);                                                                          //<finNFe> Finalidade de emissão da NF-e - 1=NF-e Normal; 2=NF-e Complementar; 3=NF-e Ajuste; 4=Devolução de mercadoria

                ide[14] = "1";                                                                                                      //<indFinal> Indica operação com Consumidor final - 0=Normal; 1=Consumidor final;

                /*0=Não se aplica (por exemplo, Nota Fiscal complementar ou de ajuste);
                1=Operação presencial;
                2=Operação não presencial, pela Internet;
                3=Operação não presencial, Teleatendimento;
                4=NFC-e em operação com entrega a domicílio;
                9=Operação não presencial, outros.*/
                ide[26] = "0";                                                                  //<indPres> Indicador de presença do comprador no estabelecimento comercial no momento da operação

                /* Grupo de informação das NF-e e cupom referenciado - tag NFref - Para multiplos valores utilizar ";" conforme exemplo */
                ide[13] = ""; //35170205561131000161570010000037131000039314;1231                 		//<NFref> refNFe ou refECF.nCOO

                ide[17] = ""; //;02                 													//<NFref> refECF mod
                ide[18] = ""; //;1123123                												//<NFref> refECF nECF

                /* Grupo de informação da NF modelo 1/1A referenciada */
                ide[19] = "";            //<cUF>    --> Utilizar a Tabela do IBGE
                ide[20] = "";            //<AAMM>   --> AAMM data da emissão da NF
                ide[21] = "";            //<CNPJ>   --> CNPJ do emitente da NF
                ide[22] = "";            //<mod>    --> Informar o código do modelo do Documento fiscal: 01 – modelo 01
                ide[23] = "";            //<serie>  --> Informar a série do documento fiscal
                ide[24] = "";            //<nNF>    --> 1 – 999999999            

                /* contingência */
                ide[15] = ""; // "2014-07-01T17:04:00-03:00";                      //<dhCont> Data da entrada em contingência
                ide[16] = ""; // "Teste de funcionamento em contingência";         //<xJust> Justificativa da entrada em contingência

                /* VerProc = Versão do aplicativo do cliente */
                ide[27] = "Versao Aplicativo";

                /* Grupo de informação da NFP (modelo 4) referenciada para Notas de Produtores Rurais*/
                ide[28] = "";            //<cUF>    --> Utilizar a Tabela do IBGE
                ide[29] = "";            //<AAMM>   --> AAMM da emissão da NF
                ide[30] = "";            //<CNPJ>   --> CNPJ do emitente da NF
                ide[31] = "";            //<CPF>    --> CPF do emitente da NF
                ide[32] = "";            //<IE>     --> Inscrição Estadual do emitente da NF
                ide[33] = "";            //<mod>    --> Informar o código do modelo do Documento fiscal: 01 – modelo 01
                ide[34] = "";            //<serie>  --> nformar a série do documento fiscal
                ide[35] = "";            //<nNF>    --> 1 – 999999999
                ide[36] = "";            //<refCTe> --> Referencia ao Cte   

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados de Identificação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public bool DadosDoEmitente()
        {
            try
            {
                emit = new string[15];

                var busca = new Estabelecimento();
                List<Estabelecimento> dadosEstab = busca.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);

                emit[0] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstRazao);                                             //<xNome>
                emit[1] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstFantasia);                                          //<xFant>
                emit[2] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstEndereco);                                          //<xLgr>
                emit[3] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstNum.ToString());                                    //<nro>
                emit[4] = "";                                                                                            //<xCpl>
                emit[5] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstBairro);                                            //<xBairro>
                emit[6] = Funcoes.LeParametro(6, "319", true);                                                           //<cMun>
                emit[7] = Funcoes.RemoverAcentuacao(dadosEstab[0].EstCidade);                                            //<xMun>
                emit[8] = Funcoes.RemoveCaracter(Convert.ToDouble(dadosEstab[0].EstCep).ToString());                     //<CEP>
                emit[9] = Funcoes.RemoveCaracter(dadosEstab[0].EstFone).Trim();                                          //<fone>
                emit[10] = Funcoes.RemoveCaracter(dadosEstab[0].EstInscricaoEstadual).Trim();                            //<IE>
                emit[11] = Funcoes.RemoveCaracter(dadosEstab[0].EstInscricaoMunicipal).Trim();                           //<IM> Inscrição Municipal do Prestador de Serviço
                emit[12] = "";                                                                                           //<CNAE> CNAE fiscal
                emit[13] = "";                                                                                           //<IEST> IE do Substituto Tributário
                emit[14] = Funcoes.LeParametro(6, "320", true);                                                          //<CRT> Código de Regime Tributário - 1 – Simples Nacional; 2 – Simples Nacional – excesso de sublimite de receita bruta; 3 – Regime Normal


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados de Identificação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public async Task<bool> DadosDoDestinatario(string numDocumento, bool devolucao)
        {
            try
            {
                dest = new string[41];

                var cliente = new Cliente();
                List<Cliente> dados = cliente.DadosClienteCfDocto(numDocumento);

                if (dados.Count > 0)
                {
                    string retorno = CamposEmitente(dados);

                    if (String.IsNullOrEmpty(retorno))
                    {
                        string[] cep = dados[0].CfCEP.Split(',');
                        Endereco end = await ConsultaCEP(cep[0].ToString());

                        if(String.IsNullOrEmpty(end.ibge))
                        {
                            txtErros.Text = "CADASTRO DE CLIENTE\n\rVERIFICAR CEP DO ENDEREÇO DO CLIENTE";
                            return false;
                        }

                        IdCliente = dados[0].CfId;
                        dest[0] = Funcoes.RemoveCaracter(dados[0].CfDocto);                                     //<CNPJ> ou <CPF>
                        dest[38] = "";                                                                          //<idEstrangeiro>
                        dest[1] = Funcoes.RemoverAcentuacao(dados[0].CfNome);                                   //<xNome>
                        dest[2] = Funcoes.RemoverAcentuacao(dados[0].CfEndereco);                               //<xLgr>
                        dest[3] = dados[0].CfNumeroEndereco;                                                    //<nro>
                        dest[4] = "";                                                                           //<xCpl>
                        dest[5] = Funcoes.RemoverAcentuacao(dados[0].CfBairro);                                 //<xBairro>
                        dest[6] = end.ibge;                                                                     //<cMun>
                        dest[7] = Funcoes.RemoverAcentuacao(dados[0].CfCidade);                                 //<xMun>
                        dest[8] = dados[0].CfUF;                                                                //<UF>
                        dest[9] = Funcoes.RemoveCaracter(dados[0].CfCEP);          //<CEP>
                        dest[10] = "1058";                                                                      //<cPais>
                        dest[11] = "BRASIL";                                                                    //<xPais>
                        dest[12] = Funcoes.RemoveCaracter(dados[0].CfTelefone);                                 //<fone>
                        if (!dados[0].CfInscricaoEstadual.Equals("ISENTO") && !dados[0].CfInscricaoEstadual.Equals("ISENTA"))
                        {
                            dest[13] = Funcoes.RemoveCaracter(dados[0].CfInscricaoEstadual);                        //<IE>
                        }
                        else
                        {
                            dest[13] = "";
                        }
                        dest[14] = "";                                                                          //<ISUF>
                        dest[40] = "";                                                                          //<IM>

                        /* Grupo de Exportação */
                        dest[15] = "";                                                                          //<UFSaidaPais>
                        dest[16] = "";                                                                          //<xLocExporta>
                        dest[39] = "";                                                                          //<xLocDespacho>

                        /* Grupo de Compra */
                        dest[17] = "";                                                                          //xNEmp
                        dest[18] = "";                                                                          //xPed
                        dest[19] = "";                                                                          //xCont

                        dest[20] = "";                                                                          //email

                        /* Grupo de identificação do Local de RETIRADA */
                        /* Informar apenas quando for diferente do endereço do remetente. */
                        dest[21] = "";                                                                          //RETIRADA <CNPJ> ou <CPF>
                        dest[22] = "";                                                                          //RETIRADA <xLgr>
                        dest[23] = "";                                                                          //RETIRADA <nro>
                        dest[24] = "";                                                                          //RETIRADA <xCpl>
                        dest[25] = "";                                                                          //RETIRADA <xBairro>
                        dest[26] = "";                                                                          //RETIRADA <cMun>
                        dest[27] = "";                                                                          //RETIRADA <xMun>
                        dest[28] = "";                                                                          //RETIRADA <UF>

                        /* Grupo de identificação do Local de ENTREGA */
                        /* Informar apenas quando for diferente do endereço do remetente. */
                        dest[29] = "";                                                                          //ENTREGA <CNPJ> ou <CPF>
                        dest[30] = "";                                                                          //ENTREGA <xLgr>
                        dest[31] = "";                                                                          //ENTREGA <nro>
                        dest[32] = "";                                                                          //ENTREGA <xCpl>
                        dest[33] = "";                                                                          //ENTREGA <xBairro>
                        dest[34] = "";                                                                          //ENTREGA <cMun>
                        dest[35] = "";                                                                          //ENTREGA <xMun>
                        dest[36] = "";                                                                          //ENTREGA <UF>

                        if(devolucao)
                        {
                            dest[37] = cmbIcms.Text.Substring(0, 1);                                                                        //<indIEDest> 1=Contribuinte ICMS (informar a IE do destinatário);2=Contribuinte isento de Inscrição no cadastro de Contribuintes do ICMS; 9=Não Contribuinte, que pode ou não possuir Inscrição			
                        }
                        else
                        {
                            dest[37] = "9";                                                                         //<indIEDest> 1=Contribuinte ICMS (informar a IE do destinatário);2=Contribuinte isento de Inscrição no cadastro de Contribuintes do ICMS; 9=Não Contribuinte, que pode ou não possuir Inscrição			
                        }


                        return true;
                    }
                    else
                    {
                        if (devolucao)
                        {
                            MessageBox.Show("CADASTRO DE CLIENTE\n\r" + camposFaltantes, "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                            txtErros.Text = "CADASTRO DE CLIENTE\n\r" + camposFaltantes;

                        tslRegistros.Text = "";
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("CNPJ não Cadastrado!", "NF-e Devolução", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpj.Focus();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados do Destinatário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public string CamposEmitente(List<Cliente> dados)
        {
            camposFaltantes = "";
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfDocto)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO DOCUMENTO\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfNome)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO NOME\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfEndereco)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO ENDERECO\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfCidade)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO CIDADE\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfUF)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO UF\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfCEP)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO CEP\n\r";
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(dados[0].CfTelefone)))
            {
                camposFaltantes += Environment.NewLine + "CAMPO TELEFONE\n\r";
            }

            return camposFaltantes;
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {
                throw;
            }
        }

        private void txtChave_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtMotivo.Focus();
        }

        private void txtComplemento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCnpj.Focus();
            }
        }

        private void txtCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbDevOperacao.Focus();
        }

        private void cmbDevOperacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIcms.Focus();
            }
        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtVlBaseIcms.Focus();
            }
        }

        private void txtDesconto_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtDesconto.Text))
            {
                txtDesconto.Text = "0";
            }
        }

        private void txtChave_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbDevOperacao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtComplemento_Validated(object sender, EventArgs e)
        {
            txtComplemento.Text = txtComplemento.Text.Trim();
        }

        private void txtVlBaseIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVlIcms.Focus();
        }

        private void txtVlBaseIcms_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtVlBaseIcms.Text))
            {
                txtVlBaseIcms.Text = "0.00";
            }
        }

        private void txtVlIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVlBcRT.Focus();
        }

        private void txtVlIcms_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtVlIcms.Text))
            {
                txtVlIcms.Text = "0.00";
            }
        }

        private void txtVlBcRT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVlIcmsST.Focus();
        }

        private void txtVlBcRT_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtVlBcRT.Text))
            {
                txtVlBcRT.Text = "0.00";
            }

        }

        private void txtVlIcmsST_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtLote.Focus();
        }

        private void txtVlIcmsST_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtVlIcmsST.Text))
            {
                txtVlIcmsST.Text = "0.00";
            }
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFabricacao.Focus();
        }

        private void txtFabricacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtValidade.Focus();
        }

        private void txtValidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                BcCOFINS.Focus(); 
        }

        private void cmbIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnGerarDevolucao.PerformClick();
        }

        private void BcCOFINS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                AliqCOFINS.Focus(); 
        }

        private void AliqCOFINS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                BcPIS.Focus();
                TotCOFINS.Text = String.Format("{0:N}",  (Math.Round(Convert.ToDouble(BcCOFINS.Text) * Convert.ToDouble(AliqCOFINS.Text) / 100,2)).ToString()).Replace(".", "").Replace(",", ".");
            }
        }

        private void BcPIS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                AliqPIS.Focus(); 
        }

        private void AliqPIS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                TotPIS.Text = String.Format("{0:N}", Math.Round((Convert.ToDouble(BcPIS.Text) * Convert.ToDouble(AliqPIS.Text) / 100),2).ToString()).Replace(".","").Replace(",","."); 
                btnAdicionar.Focus();

            }
        }

        public bool DadosDoProduto(string vendaID)
        {
            try
            {
                var buscaItens = new VendasItens();
                dtRetorno = buscaItens.BuscaProdutosNFE(Principal.estAtual, Principal.empAtual, vendaID, txtIdCliente.Text, dtInicial.Text, dtFinicial.Text, chkPrincipio.Checked);

                prod = new string[dtRetorno.Rows.Count, 200];

                string retorno = CamposProduto(dtRetorno, false);

                if (String.IsNullOrEmpty(retorno))
                {
                    for (int x = 0; x < prod.GetLength(0); x++)
                    {
                        if (cmbEmissao.Text.Substring(0, 1).Equals("4"))
                        {
                            prod[x, 0] = "CFOP" + dtRetorno.Rows[x]["PROD_CFOP_DEVOLUCAO"];                                                         //<cProd> Código do produto ou serviço
                        }
                        else
                        {
                            prod[x, 0] = "CFOP" + dtRetorno.Rows[x]["PROD_CFOP"];                                                                   //<cProd> Código do produto ou serviço
                        }
                        prod[x, 1] = dtRetorno.Rows[x]["PROD_CODIGO"].ToString();                                                                   //<cEAN> GTIN (Global Trade Item Number) do produto, antigo código EAN ou código de barras
                        prod[x, 2] = Funcoes.RemoveCaracter(Funcoes.RemoverAcentuacao(dtRetorno.Rows[x]["PROD_DESCR"].ToString()));                 //<xProd> Descrição do produto ou serviço
                        prod[x, 3] = dtRetorno.Rows[x]["NCM"].ToString();                                                                           //<NCM> Código NCM com 8 dígitos
                        prod[x, 109] = "";                                                                                                          //<NVE> Codificação NVE - Nomenclatura de Valor Aduaneiro e Estatística
                        prod[x, 146] = dtRetorno.Rows[x]["PROD_CEST"].ToString();                                                                   //<CEST> 

                        prod[x, 186] = "S";                                                                                                         //<indEscala> Indicador de Escala Relevante - S - Produzido em Escala Relevante; N – Produzido em Escala NÃO Relevante.
                        prod[x, 187] = "";                                                                                                          //<CNPJFab> CNPJ do Fabricante da Mercadoria, obrigatório para produto em escala NÃO relevante.
                        prod[x, 188] = "";                                                                                                          //<cBenef> Código de Benefício Fiscal utilizado pela UF, aplicado ao item.

                        prod[x, 4] = "000";                                                                                                         //<EXTIPI>
                        if (cmbEmissao.Text.Substring(0, 1).Equals("4"))
                        {
                            prod[x, 5] = dtRetorno.Rows[x]["PROD_CFOP_DEVOLUCAO"].ToString();                                                                 //<CFOP> Código Fiscal de Operações e Prestações
                        }
                        else
                        {
                            prod[x, 5] = dtRetorno.Rows[x]["PROD_CFOP"].ToString();                                                       //<CFOP> Código Fiscal de Operações e Prestações
                        }
                        prod[x, 6] = dtRetorno.Rows[x]["VENDA_ITEM_UNIDADE"].ToString().ToUpper();                                                  //<uCom> Uniade Comercial
                        prod[x, 7] = String.Format("{0:0.0000}", Convert.ToInt32(dtRetorno.Rows[x]["VENDA_ITEM_QTDE"])).Replace(",", ".");                           //<qCom> Quantidade Comercial
                        prod[x, 8] = Convert.ToDouble(dtRetorno.Rows[x]["VENDA_ITEM_UNITARIO"]).ToString().Replace(",", ".");                         //<vUnCom> Valor Unitário de Comercialização
                        prod[x, 9] = Convert.ToDouble(dtRetorno.Rows[x]["VENDA_ITEM_SUBTOTAL"]).ToString().Replace(",", ".");                           //<vProd> Valor Total Bruto do Produto ou Serviço
                        prod[x, 10] = dtRetorno.Rows[x]["PROD_CODIGO"].ToString();                                                                  //<cEANTrib> GTIN (Global Trade Item Number) da unidade tributável, antigo código EAN ou código de barras
                        prod[x, 11] = dtRetorno.Rows[x]["VENDA_ITEM_UNIDADE"].ToString().ToUpper();                                                 //<uTrib> Unidade Tributável
                        prod[x, 12] = String.Format("{0:0.0000}", Convert.ToInt32(dtRetorno.Rows[x]["VENDA_ITEM_QTDE"])).Replace(",", ".");                         //<qTrib> Quantidade Tributável
                        prod[x, 13] = Convert.ToDouble(dtRetorno.Rows[x]["VENDA_ITEM_UNITARIO"]).ToString().Replace(",", ".");                            //<vUnTrib> Valor Unitário de tributação
                        prod[x, 14] = "";                                                                                                                           //<vFrete> Valor Total do Frete
                        prod[x, 15] = "";                                                                                                                           //<vSeg> Valor Total do Seguro
                        prod[x, 16] = Math.Abs(Convert.ToDouble(dtRetorno.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".");               //<vDesc> Valor do Desconto  
                        prod[x, 86] = "";                                                                                                                           //<vOutro> Outras despesas acessórias    


                        prod[x, 96] = "";                                                                                                           //<vTotTrib> Valor total dos tributos

                        /* tag ISSQN */
                        prod[x, 39] = "";                                                                                                           //ISSQN <vBC> Valor da Base de Cálculo ISSQN
                        prod[x, 40] = "";                                                                                                           //ISSQN <vAliq> Valor da Aliquota ISSQN
                        prod[x, 41] = "";                                                                                                           //ISSQN <vISSQN> Valor do ISSQN 
                        prod[x, 42] = "";                                                                                                           //ISSQN <cMunFG> Código do município de ocorrência do fato gerador do ISSQN
                        prod[x, 43] = "";                                                                                                           //ISSQN <cListServ>	Item da Lista de Serviços			
                        prod[x, 70] = "";                                                                                                           //ISSQN <cSitTrib> Código da tributação do ISSQN: N – NORMAL; R – RETIDA; S –SUBSTITUTA; I – ISENTA. (v.2.0)

                        //NF-e 3.10
                        prod[x, 119] = "";                                                                                                          //ISSQN <vDeducao> Valor dedução para redução da Base de Cálculo
                        prod[x, 120] = "";                                                                                                          //ISSQN <vOutro> Valor outras retenções
                        prod[x, 121] = "";                                                                                                          //ISSQN <vDescIncond> Valor desconto incondicionado
                        prod[x, 122] = "";                                                                                                          //ISSQN <vDescCond> Valor desconto condicionado
                        prod[x, 123] = "";                                                                                                          //ISSQN <vISSRet> Valor retenção ISS

                        /*<indISS> 
                         * 1=Exigível
                         * 2=Não incidência; 
                         * 3=Isenção; 
                         * 4=Exportação; 
                         * 5=Imunidade; 
                         * 6=Exigibilidade Suspensa por Decisão Judicial; 
                         * 7=Exigibilidade Suspensa por Processo Administrativo; */
                        prod[x, 124] = "";                                                                                                          //ISSQN <indISS> Indicador da exigibilidade do ISS
                        prod[x, 125] = "";                                                                                                          //ISSQN <cMun> Código do Município de incidência do imposto
                        prod[x, 126] = "";                                                                                                          //ISSQN <cPais> Código do País onde o serviço foi prestado
                        prod[x, 127] = "";                                                                                                          //ISSQN <nProcesso> Número do processo judicial ou administrativo de suspensão da exigibilidade
                        prod[x, 128] = "";                                                                                                          //ISSQN <indIncentivo> Indicador de incentivo Fiscal
                        prod[x, 134] = "";                                                                                                          //ISSQN <cServico> Código do serviço prestado dentro do município

                        /* tag ICMS */
                        prod[x, 17] = "0";                                                                                                          //<orig> Origem da mercadoria

                        if (!Funcoes.LeParametro(6, "320", true).Equals("1"))
                        {
                            if (dtRetorno.Rows[x]["PROD_CST"].ToString().Length > 2)
                            {
                                prod[x, 18] = dtRetorno.Rows[x]["PROD_CST"].ToString().Substring(1, 2);                                              //<CST> Tributação do ICMS
                            }
                            else
                            {
                                prod[x, 18] = dtRetorno.Rows[x]["PROD_CST"].ToString();                                                             //<CST> Tributação do ICMS
                            }
                        }
                        else
                        {
                            prod[x, 18] = CSOSN(Funcoes.FormataZeroAEsquerda(dtRetorno.Rows[x]["PROD_CST"].ToString(), 3));                         //<CST> Tributação do ICMS
                        }

                        prod[x, 19] = "0";                                                                                                          //<modBC> Modalidade de determinação da BC do ICMS
                        prod[x, 20] = "0";                                                                                                          //<vBC> Valor da BC do ICMS
                        prod[x, 21] = "0";                                                                                                          //<pICMS> Alíquota do imposto ICMS
                        prod[x, 22] = "0";                                                                                                          //<vICMS> Valor do ICMS                

                        /*<modBCST>
                        0=Preço tabelado ou máximo sugerido;
                        1=Lista Negativa (valor);
                        2=Lista Positiva (valor);
                        3=Lista Neutra (valor);
                        4=Margem Valor Agregado (%);
                        5=Pauta (valor);*/
                        prod[x, 46] = dtRetorno.Rows[x]["PROD_LISTA"].ToString() == "P" ? "2" : dtRetorno.Rows[x]["PROD_LISTA"].ToString() == "N" ? "1" : "3";      //<modBCST>	Modalidade de determinação da BC do ICMS ST 		
                        prod[x, 47] = "";                                                                                                                           //<pMVAST>	Percentual da margem de valor Adicionado do ICMS ST 		
                        prod[x, 48] = "";                                                                                                                           //<pRedBCST> Percentual da Redução de BC do ICMS ST		

                        prod[x, 49] = "";                                                                                                                           //<vBCSTRet> Valor da BC do ICMS Retido Anteriormente
                        prod[x, 50] = "";                                                                                                                           //<vICMSSTRet> Valor do ICMS Retido Anteriormente

                        //NF-e v4.0
                        prod[x, 198] = "";                                                                                                                          //<pST> Alíquota suportada pelo Consumidor Final

                        prod[x, 190] = "";                                                                                                                          //<pFCP> Percentual do Fundo de Combate à Pobreza (FCP) 
                        prod[x, 191] = "";                                                                                                                          //<vFCP> Valor do Fundo de Combate à Pobreza (FCP)

                        prod[x, 195] = "";                                                                                                                          //<vBCFCP> Valor da Base de Cálculo do FCP
                        prod[x, 192] = "";                                                                                                                          //<vBCFCPST> Valor da Base de Cálculo do FCP retido por Substituição Tributária
                        prod[x, 193] = "";                                                                                                                          //<pFCPST> Percentual do FCP retido por Substituição Tributária
                        prod[x, 194] = "";                                                                                                                          //<vFCPST> Valor do FCP retido por Substituição Tributária
                        prod[x, 196] = "";                                                                                                                          //<pFCPSTRet> Percentual do FCP retido anteriormente por Substituição Tributária
                        prod[x, 197] = "";                                                                                                                          //<vFCPSTRet> Valor do FCP retido anteriormente por Substituição Tributária
                        prod[x, 199] = "";                                                                                                                          //<vBCFCPSTRet> Valor da Base de Cálculo do FCP retido anteriormente

                        prod[x, 142] = "";                                                                                                                          //<vBCSTDest> Valor da BC do ICMS ST da UF destino 
                        prod[x, 143] = "";                                                                                                                          //<vICMSSTDest> Valor do ICMS ST da UF destino

                        prod[x, 51] = "";                                                                                                                           //<vICMSST>	Valor do ICMS ST						
                        prod[x, 52] = "";                                                                                                                           //<pRedBC> Percentual da Redução de BC

                        prod[x, 80] = "";                                                                                                                           //<pCredSN>	Alíquota aplicável de cálculo do crédito (Simples Nacional).						
                        prod[x, 81] = "";                                                                                                                           //<vCredICMSSN>	Valor crédito do ICMS que pode ser aproveitado nos termos do art. 23 da LC 123 (Simples Nacional)	

                        prod[x, 85] = "";                                                                                                                           //<motDesICMS> Motivo da desoneração do ICMS - (ocorrência 1-1) Nota:2013/005 Campo será preenchido quando o campo anterior estiver preenchido. Informar o motivo da desoneração:3=Uso na agropecuária;9=Outros;12=Órgão de fomento e desenvolvimento agropecuário.         

                        /* tag IPI */
                        /* <CST> 
                        01=Entrada tributada com alíquota zero
                        02=Entrada isenta
                        03=Entrada não-tributada
                        04=Entrada imune
                        05=Entrada com suspensão
                        51=Saída tributada com alíquota zero
                        52=Saída isenta
                        53=Saída não-tributada
                        54=Saída imune
                        55=Saída com suspensão*/
                        prod[x, 23] = "53";                                                                                                                         //IPI <CST> Código da situação tributária do IPI

                        /* obs: Informar os campos INDEX 24 e 25 caso o cálculo do IPI seja por alíquota 
                         ou os campos INDEX 78 e 79 caso o cálculo do IPI seja valor por unidade. */
                        prod[x, 78] = string.Empty;                                                                                                                 //IPI <qUnid> Quantidade total na unidade padrão para tributação (somente para os produtos tributados por unidade)
                        prod[x, 79] = string.Empty;                                                                                                                 //IPI <vUnid> Valor por Unidade Tributável

                        prod[x, 24] = "0";                                                                                                                          //IPI <vBC> Valor da BC do IPI
                        prod[x, 25] = string.Empty;                                                                                                                 //IPI <pIPI> Alíquota do IPI
                                                                                                                                                                    /* fim obs */

                        prod[x, 87] = "0";                                                                                                                          //IPI <clEnq> Classe de enquadramento do IPI para Cigarros e Bebidas
                        prod[x, 88] = "00000000000000";                                                                                                             //IPI <CNPJProd> CNPJ do produtor da mercadoria, quando diferente do emitente. Somente para os casos de exportação direta ou indireta.
                        prod[x, 89] = "0";                                                                                                                          //IPI <cSelo> Código do selo de controle IPI
                        prod[x, 90] = "0";                                                                                                                          //IPI <qSelo> Quantidade de selo de controle
                        prod[x, 91] = "999";                                                                                                                        //IPI <cEnq> Código de Enquadramento Legal do IPI

                        prod[x, 26] = string.Empty;                                                                                                                 //IPI <vIPI> Valor do IPI
                                                                                                                                                                    /* tag II */
                        prod[x, 27] = "0.00";                                                                                                                       //II <vBC> Valor BC do Imposto de Importação
                        prod[x, 28] = "0.00";                                                                                                                       //II <vDespAdu> Valor despesas aduaneiras
                        prod[x, 29] = "0.00";                                                                                                                       //II <vII> Valor Imposto de Importação
                        prod[x, 30] = "0.00";                                                                                                                       //II <vIOF> Valor Imposto sobre Operações Financeiras

                        /* tag PIS */
                        prod[x, 31] = "07";                                                                                                                         //<CST> Código de Situação Tributária do PIS
                        prod[x, 32] = "0";                                                                                                                          //<vBC> Valor da Base de Cálculo do PIS
                        prod[x, 33] = "0";                                                                                                                          //<pPIS> Alíquota do PIS (em percentual)
                        prod[x, 34] = "0";                                                                                                                          //<vPis> Valor do PIS
                        prod[x, 45] = "";                                                                                                                           //<vAliqProd> Alíquota do PIS (em reais)     
                        prod[x, 77] = "";                                                                                                                           //<qBCProd> Quantidade Vendida

                        /* tag COFINS */
                        prod[x, 35] = "07";                                                                                                                         //<CST> Código de Situação Tributária do Cofins
                        prod[x, 36] = "0.00";                                                                                                                       //<vBC> Valor da Base de Cálculo da COFINS
                        prod[x, 37] = "0.00";                                                                                                                       //<pCOFINS> Alíquota da COFINS (em percentual)
                        prod[x, 38] = "0.00";                                                                                                                       //<vCOFINS> Valor da COFINS
                        prod[x, 44] = "";                                                                                                                           //<vAliqProd> Alíquota da COFINS (em reais)    
                        prod[x, 97] = "";                                                                                                                           //<qBCProd> Quantidade Vendida

                        /*tag PISST*/
                        prod[x, 54] = "";                                                                                                                           //<vBC> Valor da Base de Cálculo do PIS
                        prod[x, 55] = "";                                                                                                                           //<pPIS> Alíquota do PIS (em percentual)
                        prod[x, 56] = "";                                                                                                                           //<vPIS> Valor do PIS


                        /* tag COFINSST */
                        prod[x, 57] = "";                                                                                                                           //<vBC> Valor da Base de Cálculo da COFINS
                        prod[x, 58] = "";                                                                                                                           //<pCOFINS> Alíquota da COFINS (em percentual)
                        prod[x, 59] = "";                                                                                                                           //<vCOFINS> Valor da COFINS

                        /* Tag da Declaração de Importação | DI - Multiplas ocorrencias utilizar ";", conforme exemplo*/
                        prod[x, 60] = ""; // "43434";                                                                                                               //<nDI> Número do Documento de Importação (DI, DSI, DIRE, ...)
                        prod[x, 61] = ""; // "2010-06-08";                                                                                                          //<dDI> Data de Registro do documento - Formato: “AAAA-MM-DD”
                        prod[x, 62] = ""; // "Moscou";                                                                                                              //<xLocDesemb> Local de desembaraço
                        prod[x, 63] = ""; // "SP";                                                                                                                  //<UFDesemb> Sigla da UF onde ocorreu o Desembaraço Aduaneiro
                        prod[x, 64] = ""; // "2010-06-22";                                                                                                          //<dDesemb> Data do Desembaraço Aduaneiro
                        prod[x, 65] = ""; // "80809808";                                                                                                            //<cExportador> Código do Exportador
                        prod[x, 66] = ""; // "122;144;133;";                                                                                                        //adi: <nAdicao> Numero da Adição
                        prod[x, 67] = ""; // "233;321;456;";                                                                                                        //adi: <nSeqAdic> Numero sequencial do item dentro da Adição
                        prod[x, 68] = ""; // "455;323;546;";                                                                                                        //adi: <cFabricante> Código do fabricante estrangeiro
                        prod[x, 69] = ""; // "122.22;;482.36;";                                                                                                     //adi: <vDescDI> Valor do desconto do item da DI – Adição

                        prod[x, 53] = "";                                                                                                                           //<infAdProd> Informações Adicionais do Produto


                        /* Grupo do detalhamento de Medicamentos e de matériasprimas farmacêuticas, Multiplas ocorrencias utilizar ";" */
                        /*Utilizar as tags 71,72,73 e 74 e 185 para o grupo de rastro caso, versão 4.00*/
                        prod[x, 71] = ""; // "21;13;44;";                                                                                                           //<nLote> Número do Lote do produto - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 72] = ""; // "123.123;656.656;967.232;";                                                                                            //<qLote> Quantidade de produto no Lote - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 73] = ""; // "2010-01-01;2011-02-02;2009-01-01;";                                                                                   //<dFab> Data de fabricação/ Produção - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 74] = "";                                                                                                                           //<dVal> Data de validade - Retirado na versão 4.00 e adicionado no grupo Rastro
                        prod[x, 189] = "";                                                                                                                          //<cAgreg> Código de Agregação

                        prod[x, 185] = dtRetorno.Rows[x]["PROD_REGISTRO_MS"].ToString();                                                                            //<cProdANVISA> Código de Produto da ANVISA
                        prod[x, 75] = "";                                                                                                                           //<vPMC> Preço máximo consumidor

                        prod[x, 76] = "1";                                                                                                                          //<indTot> Indica se valor do Item (vProd) entra no valor total da NF-e (vProd) - 0 = Valor do item (vProd) não compõe o valor total da NF-e; 1 = Valor do item (vProd) compõe o valor total da NF-e (vProd) (v2.0);

                        prod[x, 83] = txtPedido.Text;                                                                                                               //<xPed> Número do Pedido de Compra
                        prod[x, 84] = (x + 1).ToString();                                                                                                           //<nItemPed> Item do Pedido de Compra

                        /* Grupo do detalhamento de Combustíveis */
                        prod[x, 92] = "";                                                                                                                           // <cProdANP> Código de produto da ANP (ocorrência 1-1) --> Nota: se não for informada essa posição, não será gerado o grupo <comb>

                        prod[x, 180] = "";                                                                                                                          //<descANP> Descrição do Produto
                        prod[x, 181] = "";                                                                                                                          //<pGLP> Percentual do GLP derivado do petróleo
                        prod[x, 182] = "";                                                                                                                          //<pGNn> Percentual do Gás Natural Nacional
                        prod[x, 183] = "";                                                                                                                          //<pGNi> Percentual de Gás Natural Importado
                        prod[x, 184] = "";                                                                                                                          //<vPart> Valor de Partida

                        prod[x, 93] = "";                                                                                                                           //<CODIF> Código de autorização / registro do CODIF    (ocorrência 0-1)
                        prod[x, 94] = "";                                                                                                                           //<qTemp>  Quantidade de combustível faturada à temperatura ambiente.   (ocorrência 0-1)
                        prod[x, 95] = "";                                                                                                                           //<UFCons> Sigla da UF de consumo  (ocorrência 1-1)             

                        prod[x, 105] = "";                                                                                                                          //<nFCI> Número de controle da FCI - Ficha de Conteúdo de Importação (ocorrência 0-1) 
                        prod[x, 106] = "0.00";                                                                                                                      //<vICMSDeson> Valor do ICMS desonerado - Informar apenas nos motivos de desoneração  3=Uso na agropecuária; 9=Outros; 12=Órgão de fomento e desenvolvimento agropecuário.(ocorrência 1-1)

                        /*<tpViaTransp>
                        1=Marítima;
                        2=Fluvial
                        3=Lacustre;
                        4=Aérea;
                        5=Postal
                        6=Ferroviária;
                        7=Rodoviária;
                        8=Conduto / Rede Transmissão;
                        9=Meios Próprios;
                        10=Entrada / Saída ficta.
                        11=Courier;
                        12=Handcarry*/
                        prod[x, 107] = "7";                                                                                                                         //<tpViaTransp> Via de transporte internacional informada na Declaração de Importação (DI)
                        prod[x, 108] = "1";                                                                                                                         //<tpIntermedio> Forma de importação quanto a intermediação - 1=Importação por conta própria; 2=Importação por conta e ordem; 3=Importação por encomenda;


                        //<detExport> - Tag destinada a Exportação
                        prod[x, 110] = "";                                                                                                                          //<nDraw> Número do ato concessório de Drawback - Tag destinada a Exportação
                        prod[x, 111] = "";                                                                                                                          //<chNFe> Chave de Acesso da NF-e recebida para exportação - Tag destinada a Exportação
                        prod[x, 112] = "";                                                                                                                          //<nRE> Número do Registro de Exportação - Tag destinada a Exportação
                        prod[x, 113] = "";                                                                                                                          //<qExport> Quantidade do item realmente exportado - Tag destinada a Exportação

                        prod[x, 114] = "";                                                                                                                          //<nRECOPI> Número do RECOPI - Tag para operações com Papel Imune.

                        /* Grupo do detalhamento de Armas */
                        prod[x, 115] = "";                                                                                                                          //<tpArma> Indicador do tipo de arma de fogo - 0=Uso permitido; 1=Uso restrito;
                        prod[x, 116] = "";                                                                                                                          //<nSerie> Número de série da arma
                        prod[x, 117] = "";                                                                                                                          //<nCano> Número de série do cano
                        prod[x, 118] = "";                                                                                                                          //<desc> Descrição completa da arma, compreendendo: calibre, marca, capacidade, tipo de funcionamento, comprimento e demais elementos que permitam a sua perfeita identificação

                        /*Gruno para detalhamento de Devolução. <finNFe> igual a 4.*/
                        prod[x, 129] = "";                                                                                                                          //<pDevol> Percentual da mercadoria devolvida
                        prod[x, 130] = "";                                                                                                                          //<IPI> Informação do IPI devolvido
                        prod[x, 131] = "";                                                                                                                          //<vIPIDevol> Valor do IPI devolvido

                        prod[x, 132] = "";                                                                                                                          //<pMixGN> Percentual de Gás Natural para o produto GLP (cProdANP=210203001)
                        prod[x, 133] = "";                                                                                                                          //<vAFRMM> Valor da AFRMM - Adicional ao Frete para Renovação da Marinha Mercante

                        prod[x, 135] = "";                                                                                                                          //<CNPJ> CNPJ do adquirente ou do encomendante
                        prod[x, 136] = "";                                                                                                                          //<UFTerceiro> Sigla da UF do adquirente ou do encomendante

                        //ICMS 51
                        prod[x, 137] = "";                                                                                                                          //<vICMSOp> Valor do ICMS da Operação
                        prod[x, 138] = "";                                                                                                                          //<pDif> Percentual do diferimento
                        prod[x, 139] = "";                                                                                                                          //<vICMSDif> Valor do ICMS diferido

                        //ICMSUFDest
                        //Para gerar o ICMSUFDest todos os indices do vetor devem conter dados
                        prod[x, 147] = "";                                                                                                                          //<vBCUFDest> Valor da BC do ICMS na UF de destino
                        prod[x, 148] = "";                                                                                                                          //<pFCPUFDest> Percentual do ICMS relativo ao Fundo de Combate à Pobreza (FCP) na UF de destino
                        prod[x, 149] = "";                                                                                                                          //<pICMSUFDest> Alíquota interna da UF de destino
                        prod[x, 150] = "";                                                                                                                          //<pICMSInter> Alíquota interestadual das UF envolvidas
                        prod[x, 151] = "";                                                                                                                          //<pICMSInterPart> Percentual provisório de partilha do ICMS Interestadual
                        prod[x, 152] = "";                                                                                                                          //<vFCPUFDest> Valor do ICMS relativo ao Fundo de Combate à Pobreza (FCP) da UF de destino
                        prod[x, 153] = "";                                                                                                                          //<vICMSUFDest> Valor do ICMS Interestadual para a UF de destino
                        prod[x, 154] = "";                                                                                                                          //<vICMSUFRemet> Valor do ICMS Interestadual para a UF do remetente
                        prod[x, 179] = "";                                                                                                                          //<vBCFCPUFDest> Valor da BC FCP na UF de destino

                        /*Detalhamento Específico de Veículos novos */
                        prod[x, 155] = "";                                                                                                                          //<tpOp> Tipo da operação - 1=Venda concessionária; 2=Faturamento direto para consumidor final; 3=Venda direta para grandes consumidores (frotista, governo, ...); 0=Outros
                        prod[x, 156] = "";                                                                                                                          //<chassi> Chassi do veículo
                        prod[x, 157] = "";                                                                                                                          //<cCor> Cor - Código de cada montadora
                        prod[x, 158] = "";                                                                                                                          //<xCor> Descrição da Cor
                        prod[x, 159] = "";                                                                                                                          //<pot> Potência Motor (CV)
                        prod[x, 160] = "";                                                                                                                          //<cilin> Cilindradas 
                        prod[x, 161] = "";                                                                                                                          //<pesoL> Peso Líquido 
                        prod[x, 162] = "";                                                                                                                          //<pesoB> Peso Bruto
                        prod[x, 163] = "";                                                                                                                          //<nSerie> Serial (série)
                        prod[x, 164] = "";                                                                                                                          //<tpComb> Tipo de combustível - Utilizar Tabela RENAVAM (v2.0); 01=Álcool, 02=Gasolina, 03=Diesel, (...); 16=Álcool/Gasolina; 17=Gasolina/Álcool/GNV; 18=Gasolina/Elétrico
                        prod[x, 165] = "";                                                                                                                          //<nMotor> Número de Motor
                        prod[x, 166] = "";                                                                                                                          //<CMT> Capacidade Máxima de Tração
                        prod[x, 167] = "";                                                                                                                          //<dist> Distância entre eixos
                        prod[x, 168] = "";                                                                                                                          //<anoMod> Ano Modelo de Fabricação
                        prod[x, 169] = "";                                                                                                                          //<anoFab> Ano de Fabricação
                        prod[x, 170] = "";                                                                                                                          //<tpPint> Tipo de Pintura
                        prod[x, 171] = "";                                                                                                                          //<tpVeic> Tipo de Veículo
                        prod[x, 172] = "";                                                                                                                          //<espVeic> Espécie de Veículo - 1=PASSAGEIRO; 2=CARGA; 3=MISTO; 4 = CORRIDA; 5 = TRAÇÃO; 6 = ESPECIAL;
                        prod[x, 173] = "";                                                                                                                          //<VIN> Condição do VIN(chassi) - R=Remarcado; N=Normal
                        prod[x, 174] = "";                                                                                                                          //<condVeic> Condição do Veículo - 1=Acabado; 2=Inacabado; 3=Semiacabado
                        prod[x, 175] = "";                                                                                                                          //<cMod> Código Marca Modelo
                        prod[x, 176] = "";                                                                                                                          //<cCorDENATRAN> Código da Cor - 01=AMARELO, 02=AZUL, 03=BEGE, 04=BRANCA, 05=CINZA, 06=-DOURADA, 07=GRENÁ, 08=LARANJA, 09=MARROM, 10=PRATA, 11=PRETA, 12=ROSA, 13=ROXA, 14=VERDE, 15=VERMELHA, 16=FANTASIA
                        prod[x, 177] = "";                                                                                                                          //<lota> Capacidade máxima de lotação
                        prod[x, 178] = "";
                    }

                    return true;
                }
                else
                {
                    txtErros.Text = "CADASTRO DE PRODUTOS\n\r" + camposFaltantes;
                    txtErros.Focus();
                    tslRegistros.Text = "";
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados dos Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public string CamposProduto(DataTable dados, bool devolucao)
        {
            camposFaltantes = "";

            for (int i = 0; i < dados.Rows.Count; i++)
            {
                if (!devolucao)
                {
                    if (String.IsNullOrEmpty(dados.Rows[i]["PROD_CFOP"].ToString()))
                    {
                        camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO CFOP\n\r";
                    }
                }
                if (String.IsNullOrEmpty(dados.Rows[i]["PROD_CEST"].ToString()))
                {
                    camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO CEST\n\r";
                }
                if (String.IsNullOrEmpty(dados.Rows[i]["NCM"].ToString()))
                {
                    camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO NCM\n\r";
                }
                if (devolucao)
                {
                    if (String.IsNullOrEmpty(dados.Rows[i]["PROD_UNIDADE"].ToString()))
                    {
                        camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO UNIDADE\n\r";
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(dados.Rows[i]["VENDA_ITEM_UNIDADE"].ToString()))
                    {
                        camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO UNIDADE\n\r";
                    }
                }
                if (String.IsNullOrEmpty(dados.Rows[i]["PROD_LISTA"].ToString()))
                {
                    camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO LISTA\n\r";
                }
                if (String.IsNullOrEmpty(dados.Rows[i]["PROD_REGISTRO_MS"].ToString()))
                {
                    camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO REGISTRO MS\n\r";
                }
                if (devolucao)
                {
                    if (String.IsNullOrEmpty(dados.Rows[i]["PROD_CFOP_DEVOLUCAO"].ToString()))
                    {
                        camposFaltantes += Environment.NewLine + "PRODUTO: " + dados.Rows[i]["PROD_CODIGO"].ToString() + " - CAMPO CFOP DEVOLUCAO MS\n\r";
                    }
                }
            }

            return camposFaltantes;
        }

        public string CSOSN(string cst)
        {
            if (cst == "000" || cst == "020")
            {
                return "101";
            }
            else if (cst == "010" || cst == "070")
            {
                return "201";
            }
            else if (cst == "030")
            {
                return "202";
            }
            else if (cst == "060")
            {
                return "500";
            }
            else if (cst == "040" || cst == "041")
            {
                return "102";
            }
            else if (cst == "050" || cst == "051")
            {
                return "300";
            }
            else if (cst == "090")
            {
                return "900";
            }
            else
            {
                return "900";
            }
        }

        public bool DadosTotalizadores(string vendaID)
        {
            try
            {
                total = new string[42];

                var totalizadores = new VendasDados();
                dtRetorno = totalizadores.BuscaTotalizadoresNFE(Principal.empAtual, Principal.estAtual, vendaID, txtIdCliente.Text, dtInicial.Text, dtFinicial.Text);
                /*<total> TAG de grupo de Valores Totais da NF-eI*/

                total[0] = "0.00";                                                                                                      //ICMSTot <vBC> Base de Cálculo do ICMS
                total[1] = "0.00";                                                                                                      //ICMSTot <vICMS> Valor Total do ICMS
                total[27] = "0.00";                                                                                                     //ICMSTot <vICMSDeson> Valor Total do ICMS desonerado
                total[2] = "0.00";                                                                                                      //ICMSTot <vBCST> Base de Cálculo do ICMS ST
                total[3] = "0.00";                                                                                                      //ICMSTot <vST> Valor Total do ICMS ST
                total[4] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[0]["TOTAL"])).Replace(".", "").Replace(",", ".");                     //ICMSTot <vProd> Valor Total dos produtos e serviços
                total[5] = "0.00";                                                                                                      //ICMSTot <vFrete> Valor Total do Frete
                total[6] = "0.00";                                                                                                      //ICMSTot <vSeg> Valor Total do Seguro
                total[7] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[0]["DESCONTO"])).Replace(".", "").Replace(",", ".");          //ICMSTot <vDesc> Valor Total do Desconto
                total[8] = "0.00";                                                                                                      //ICMSTot <vII> Valor Total do II
                total[9] = "0.00";                                                                                                      //ICMSTot <vIPI> Valor Total do IPI
                total[10] = "0.00";                                                                                                    //ICMSTot <vPIS> Valor do PIS
                total[11] = "0.00";                                                                                                     //ICMSTot <vCOFINS> Valor da COFINS
                total[12] = "0.00";                                                                                                     //ICMSTot <vOutro> Outras Despesas acessórias
                total[13] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[0]["VENDA_TOTAL"])).Replace(".", "").Replace(",", ".");              //ICMSTot <vNF> Valor Total da NF-e

                totalNota = Convert.ToDouble(dtRetorno.Rows[0]["VENDA_TOTAL"]);

                total[26] = "0.00";                                                                                     //<vTotTrib> Valor aproximado total de tributos federais, estaduais e municipais.

                total[35] = "0.00";                                                                                     //<vICMSUFDest> Valor total do ICMS Interestadual para a UF de destino
                total[36] = "0.00";                                                                                     //<vICMSUFRemet> Valor total do ICMS Interestadual para a UF do remetente
                total[37] = "0.00";                                                                                     //<vFCPUFDest> Valor total do ICMS relativo Fundo de Combate à Pobreza (FCP) da UF de destino
                total[38] = "0.00";                                                                                     //<vFCP> Valor Total do FCP (Fundo de Combate à Pobreza)
                total[39] = "0.00";                                                                                     //<vFCPST> Valor Total do FCP (Fundo de Combate à Pobreza) retido por substituição tributária
                total[40] = "0.00";                                                                                     //<vFCPSTRet> Valor Total do FCP retido anteriormente por Substituição Tributária
                total[41] = "0.00";                                                                                     //<vIPIDevol> Valor Total do IPI devolvido

                total[14] = "";                                                                                         //ISSQNtot <vServ> Valor total dos Serviços sob não-incidência ou não tributados pelo ICMS
                total[15] = "";                                                                                         //ISSQNtot <vBC> Valor total Base de Cálculo do ISS
                total[16] = "";                                                                                         //ISSQNtot <vISS> Valor total do ISS
                total[17] = "";                                                                                         //ISSQNtot <vPIS> Valor total do PIS sobre serviços
                total[18] = "";                                                                                         //ISSQNtot <vCOFINS> Valor total da COFINS sobre serviços
                total[28] = "";                                                                                         //ISSQNtot <dCompet> Data da prestação do serviço - Formato: “AAAA-MM-DD”
                total[29] = "";                                                                                         //ISSQNtot <vDeducao> Valor total dedução para redução da Base de Cálculo
                total[30] = "";                                                                                         //ISSQNtot <vOutro> Valor total outras retenções
                total[31] = "";                                                                                         //ISSQNtot <vDescIncond> Valor total desconto incondicionado
                total[32] = "";                                                                                         //ISSQNtot <vDescCond> Valor total desconto condicionado
                total[33] = "";                                                                                         //ISSQNtot <vISSRet> Valor total retenção ISS
                total[34] = "";                                                                                         //ISSQNtot <cRegTrib> Código do Regime Especial de Tributação - 1=Microempresa Municipal; 2=Estimativa; 3=Sociedade de Profissionais; 4=Cooperativa; 5=Microempresário Individual (MEI); 6=Microempresário e Empresa de Pequeno Porte (ME/EPP)

                /* retTrib: Grupo de Retenções de Tributos */
                total[19] = "";                                                                                         //<vRetPIS> Valor Retido de PIS 
                total[20] = "";                                                                                         //<vRetCOFINS> Valor Retido de COFINS
                total[21] = "";                                                                                         //<vRetCSLL> Valor Retido de CSLL
                total[22] = "";                                                                                         //<vBCIRRF> Base de Cálculo do IRRF
                total[23] = "";                                                                                         //<vIRRF> Valor Retido do IRRF
                total[24] = "";                                                                                         //<vBCRetPrev> Base de Cálculo da Retenção da Previdência Social
                total[25] = "";                                                                                         //<vRetPrev> Valor da Retenção da Previdência Social

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados Totalizadores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tslRegistros.Text = "";
                return false;
            }
        }

        public bool DadosDoTransporte()
        {
            transp = new string[28];

            transp[0] = "9";                                        //<modFrete> Modalidade do frete - 0=Por conta do emitente; 1=Por conta do destinatário/remetente; 2=Por conta de terceiros; 9=Sem frete.
            transp[1] = "";                                         //<CNPJ> ou <CPF> CNPJ ou CPF do Transportador
            transp[2] = "";                                         //<xNome> Razão Social ou nome
            transp[3] = "";                                         //<IE> Inscrição Estadual do Transportador
            transp[4] = "";                                         //<xEnder> Endereço Completo
            transp[5] = "";                                         //<xMun> Nome do município
            transp[6] = "";                                         //<UF> Sigla da UF

            transp[7] = "";                                         //<placa> Placa do Veículo
            transp[8] = "";                                         //<UF> Sigla da UF
            transp[25] = "";                                        //<balsa> Identificação da balsa
            transp[26] = "";                                        //<vagao> Identificação do vagão

            /* Grupo Volumes - obs: Separe por ; para informar diversos volumes */
            transp[9] = "";                                         //<qVol> Quantidade de volumes transportados
            transp[10] = "";                                        //<esp> Espécie dos volumes transportados
            transp[11] = "";                                        //<marca> Marca dos volumes transportados
            transp[12] = "";                                        //<nVol> Numeração dos volumes transportados
            transp[13] = "";                                        //<pesoL> Peso Líquido (em kg)
            transp[14] = "";                                        //<pesoB> Peso Bruto (em kg)
            transp[27] = "";                                        //<nLacre> Número dos Lacres  

            transp[15] = "";                                        //<RNTC> Registro Nacional de Transportador de Carga (ANTT)

            /* retTransp: Grupo de Retenção do ICMS do transporte */
            transp[16] = "";                                        //<vServ> Valor do Serviço
            transp[17] = "";                                        //<vBCRet> BC da Retenção do ICMS
            transp[18] = "";                                        //<pICMSRet> Alíquota da Retenção
            transp[19] = "";                                        //<vICMSRet> Valor do ICMS Retido
            transp[20] = "";                                        //<CFOP> CFOP
            transp[21] = "";                                        //<cMunFG> Código do município de ocorrência do fato gerador do ICMS do transporte
            transp[22] = "";                                        //<vICMSRet> Valor do ICMS Retido

            return true;
        }


        public bool DadosDaCobranca()
        {
            cobr = new string[7];

            /*<cobr> Dados da Cobrança*/
            cobr[0] = "";                                               //fat <nFat> Número da Fatura
            cobr[1] = "";                                               //fat <vOrig> Valor Original da Fatura
            cobr[2] = "";            		                            //fat <vLiq> Valor Líquido da Fatura
            cobr[6] = "";                                               //fat <vDesc> Valor do desconto

            /* neste ex, existem 2 parcelas */
            cobr[3] = ""; // "01;02;";									//dup <nDup> Número da Duplicata
            cobr[4] = ""; // "2008-05-30;2008-06-30;";    				//dup <dVenc> Data de vencimento - Formato: “AAAA-MM-DD”
            cobr[5] = ""; // "226.64;226.64;";							//dup <vDup> Valor da duplicata

            return true;
        }

        public bool DadosDoPagamento(bool devolucao)
        {
            pag = new string[7];

            if (devolucao)
            {
                pag[0] = "90";                                                           // <tPag> Forma de Pagamento
                pag[1] = "0.00";                                                         // <vPag> Valor do Pagamento
            }
            else
            {
                pag[0] = "01";                                                           // <tPag> Forma de Pagamento
                pag[1] = String.Format("{0:N}", totalNota).Replace(".", "").Replace(",", ".");                         // <vPag> Valor do Pagamento
            }

            /* 1=Pagamento integrado com o sistema de automação da empresa (Ex.: equipamento TEF, Comércio Eletrônico);
             * 2= Pagamento não integrado com o sistema de automação da empresa (Ex.: equipamento POS); */
            pag[2] = "2";                                                // <tpIntegra> Tipo de Integração do processo de pagamento com o sistema de automação da empresa

            pag[3] = "";                                                // <CNPJ> CNPJ da Credenciadora do Cartão
            pag[4] = "";                                                // <tBand> Bandeira da Operadora do Cartão
            pag[5] = "";                                                // <cAut> Numero de Autorização da Operação 
            pag[6] = "";                                                // <vTroco> Valor do Troco

            return true;
        }

        public bool DadosAdicionais(bool devolucao)
        {
            infAdic = new string[17];

            infAdic[0] = "IMPOSTO RECOLHIDO ATRAVES DO CUPOM FISCAL: - IMPOSTO RECOLHIDO POR SUBSTITUICAO TRIBUTARIA CONFORME DECRETO 52665/08.";           //infAdFisco

            if (devolucao)
            {
                infAdic[1] = Funcoes.RemoverAcentuacao(txtComplemento.Text);                                                                                                                  //infCpl
            }
            else
            {
                infAdic[1] = Funcoes.RemoverAcentuacao(txtComplementar.Text);                                                                                                                  //infCpl
            }
            /* obsCont - Grupo do campo de uso livre do contribuinte */
            infAdic[2] = "";                                                                                                                                    //Vendedor
            infAdic[3] = "";                                                                                                                                    //Pedido
            infAdic[4] = string.Empty;                                                                                                                          //Outros

            infAdic[5] = "";                                                                                                                                    //ProcRef <nProc>
            infAdic[6] = "";                                                                                                                                    //ProcRef <indProc>  0=SEFAZ, 1=Justiça Federal, 2=Justiça Estadual, 3=Secex/RFB, 9=Outros
            infAdic[8] = "";
            infAdic[9] = "";
            infAdic[10] = "";
            infAdic[11] = "";
            infAdic[12] = "";
            infAdic[13] = "";
            infAdic[14] = "";
            infAdic[15] = "";
            infAdic[16] = "";

            return true;
        }

        #endregion
    }
}
