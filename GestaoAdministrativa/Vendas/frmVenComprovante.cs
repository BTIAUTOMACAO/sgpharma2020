﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenComprovante : Form
    {
        public frmVenComprovante(long vendaID)
        {
            InitializeComponent();
            MontaComprovante(vendaID);
        }

        private void lbComprovante_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
        public bool MontaComprovante(long vendaID)
        {
            Comprovante comp = new Comprovante();
            DataTable dtVenda = comp.BuscaVendaPorId(vendaID);
            DataTable dtFormaPag = comp.BuscaFormaPagamento(vendaID);

            if (InserirCabecalho(dtVenda.Rows[0]["DATA"].ToString(), dtVenda.Rows[0]["HORA"].ToString()).Equals(true))
            {
                if (ItensVenda(vendaID).Equals(true))
                {
                    lbComprovante.Items.Add("");
                    lbComprovante.Items.Add("TOTAL R$ ".PadLeft(34, ' ') + dtVenda.Rows[0]["VENDA_TOTAL"].ToString());
                    for (int i = 0; i < dtFormaPag.Rows.Count; ++i)
                    {
                        lbComprovante.Items.Add(dtFormaPag.Rows[i]["FORMA_DESCRICAO"].ToString().PadLeft(30, ' ') +" R$ "+  dtFormaPag.Rows[i]["VENDA_VALOR_PARCELA"].ToString());
                    }
                    lbComprovante.Items.Add("-----------------------------------------");
                }
            }
            return true;
        }

        public bool InserirCabecalho(string data, string hora)
        {
            try
            {
                lbComprovante.Items.Add(Funcoes.CentralizaTexto(Principal.nomeAtual, 40));
                lbComprovante.Items.Add("Data: " + data + "          Hora: " + hora);
                lbComprovante.Items.Add("------------------ITENS------------------");

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ItensVenda(long vendaID)
        {
            Comprovante comp = new Comprovante();
            DataTable dtItens = comp.BuscaItensComprovante(vendaID);

            for (int i = 0; i < dtItens.Rows.Count; ++i)
            {

                // FORMATA DESCRICAO 
                string descricao = dtItens.Rows[i]["PROD_DESCR"].ToString().Length > 21 ? dtItens.Rows[i]["PROD_DESCR"].ToString().Remove(21, (dtItens.Rows[i]["PROD_DESCR"].ToString().Length - 21)) : dtItens.Rows[i]["PROD_DESCR"].ToString();

                lbComprovante.Items.Add(dtItens.Rows[i]["PROD_CODIGO"].ToString() + "     " + descricao);
                if (Convert.ToDecimal(dtItens.Rows[i]["VENDA_ITEM_DIFERENCA"]) != 0)
                {
                    // ADICIONA LINHA DE DESCONTO
                    string desconto = dtItens.Rows[i]["VENDA_ITEM_DIFERENCA"].ToString().Length > 24 ? dtItens.Rows[i]["PROD_DESCR"].ToString().Remove(20, (dtItens.Rows[i]["PROD_DESCR"].ToString().Length - 20)) : dtItens.Rows[i]["PROD_DESCR"].ToString();
                    lbComprovante.Items.Add("R$ " + dtItens.Rows[i]["VENDA_ITEM_UNITARIO"].ToString().PadRight((10), ' ') + "(R$ " + dtItens.Rows[i]["VALOR_DESCONTO"] + " DESC)");
                }
                lbComprovante.Items.Add("R$ " + dtItens.Rows[i]["VALOR_UNI_COM_DES"].ToString().PadRight((10), ' ') + "X".PadRight(5, ' ') + dtItens.Rows[i]["VENDA_ITEM_QTDE"].ToString().PadRight(9, ' ') + "=".PadRight(5, ' ') + "R$ " + dtItens.Rows[i]["VENDA_ITEM_TOTAL"].ToString());
            }

            return true;
        }

    }
}
