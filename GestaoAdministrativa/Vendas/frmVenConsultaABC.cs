﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenConsultaABC : Form
    {
        public frmVenConsultaABC()
        {
            InitializeComponent();
        }

        private void frmVenConsultaABC_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmVenConsultaABC_Load(object sender, EventArgs e)
        {
            TXT_CodBarras.Focus();
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnFiltrar.PerformClick();
                    break;
                case Keys.F2:
                    btnLimpar.PerformClick();
                    break;
            }
        }

        private async void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(TXT_CodBarras.Text))
                {
                    Cursor = Cursors.WaitCursor;
                    List<TabelaAbcFarma> prodAbc = await BuscaProdutos(TXT_CodBarras.Text);
                    if (prodAbc.Count > 0)
                    {
                        dgProdutos.Rows.Clear();
                        for (int i = 0; i < prodAbc.Count; i++)
                        {
                            dgProdutos.Rows.Insert(dgProdutos.RowCount, new object[] { prodAbc[i].MedBarra, prodAbc[i].MedDescricao, prodAbc[i].LabNome, prodAbc[i].MedPco, prodAbc[i].MedRegistroMS, prodAbc[i].MedNcm, prodAbc[i].MedLista });
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado!", "Consulta ABC Farma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        TXT_CodBarras.Focus();
                    }
                }
                else
                {

                    MessageBox.Show("Informe o código de barras do produto.", "Consulta ABC Farma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TXT_CodBarras.Focus();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }


        }

        public async Task<List<TabelaAbcFarma>> BuscaProdutos(string CodBarras)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("TabelaAbcFarma/BuscaCodBarrasAbcFarma?CodBarras="+ CodBarras);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<TabelaAbcFarma> allItems = JsonConvert.DeserializeObject<List<TabelaAbcFarma>>(responseBody);
                
                return allItems;
            }
        }
        private void TXT_CodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnFiltrar.PerformClick();
        }

        private void TXT_CodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void db_ConsultaABCFarma_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            dgProdutos.Rows.Clear();
            dgProdutos.Refresh();
            TXT_CodBarras.Clear();
            TXT_CodBarras.Focus();
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
    }
}
