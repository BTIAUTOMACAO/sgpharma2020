﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenBusca : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        DataTable dt = new DataTable();
        #endregion

        public frmVenBusca(DataTable dtRetorno)
        {
            InitializeComponent();
            dt = dtRetorno;
        }

        private void frmVenBusca_Load(object sender, EventArgs e)
        {
            try
            {
                lsbRetorno.DataSource = dt;
                lsbRetorno.DisplayMember = "descr";
                lsbRetorno.ValueMember = "codigo";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Busca", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lsbRetorno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Principal.sRetorno = lsbRetorno.Text;
                Principal.iRetorno = Convert.ToInt32(lsbRetorno.SelectedValue);
                this.Hide();
            }
        }

        private void frmVenBusca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                Principal.sRetorno = "";
                Principal.iRetorno = 0;
                this.Close();
            }
        }

        private void lsbRetorno_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Principal.sRetorno = lsbRetorno.Text;
            Principal.iRetorno = Convert.ToInt32(lsbRetorno.SelectedValue);
            this.Hide();
        }

        private void lsbRetorno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                Principal.sRetorno = "";
                Principal.iRetorno = 0;
                this.Close();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
