﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenEspecie : Form
    {
        frmVenda fVendas;
        public DataTable dtEspecies = new DataTable();
        private decimal total;
        private string exclui;
        private int index;
        private string permiteExcluir;

        public frmVenEspecie(frmVenda form)
        {
            InitializeComponent();
            fVendas = form;
        }

        private void frmVenEspecie_Load(object sender, EventArgs e)
        {
            try
            {
                fVendas.selEspecie = false;
                dtEspecies = tabelaEspecies();
                index = 0;
                exclui = "S";
                permiteExcluir = "S";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenEspecie_Shown(object sender, EventArgs e)
        {
            if (fVendas.vendaBeneficio.Equals("N"))
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ESP_CODIGO", "ESP_DESCRICAO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N' AND ESP_SAT <> 99");
            }
            else
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ESP_CODIGO", "ESP_DESCRICAO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N'");
            }
           
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbEspecies.DataSource = Principal.dtPesq;
                cmbEspecies.DisplayMember = "ESP_DESCRICAO";
                cmbEspecies.ValueMember = "ESP_CODIGO";
                cmbEspecies.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Necessário cadastrar pelo menos uma Espécie,\npara realizar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                this.Close();
            }

            if (fVendas.vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.Text = "CONVENIO";
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if (fVendas.vendaBeneficio.Equals("PARTICULAR"))
            {
                txtValor.Text = lblTotal.Text;
                cmbEspecies.Text = "PARTICULAR";
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if(fVendas.vendaBeneficio.Equals("VIDALINK") && !String.IsNullOrEmpty(fVendas.pagamentoTotalOuParcial))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(14, "26", true));
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if (fVendas.vendaBeneficio.Equals("EPHARMA") && !String.IsNullOrEmpty(fVendas.pagamentoTotalOuParcial))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(14, "35", true));
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if (fVendas.vendaBeneficio.Equals("FARMACIAPOPULAR") && !String.IsNullOrEmpty(fVendas.pagamentoTotalOuParcial))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(14, "32", true));
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if (fVendas.vendaBeneficio.Equals("FUNCIONAL")  && !String.IsNullOrEmpty(fVendas.pagamentoTotalOuParcial))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(14, "47", true));
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
            else if (fVendas.vendaBeneficio.Equals("PORTALDROGARIA") && !String.IsNullOrEmpty(fVendas.pagamentoTotalOuParcial))
            {
                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                cmbEspecies.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(14, "34", true));
                txtID.Text = cmbEspecies.SelectedValue.ToString();
                permiteExcluir = "N";
                CarregaEspecies();
            }
        }

        public DataTable tabelaEspecies()
        {
            DataTable tableEspecies = new DataTable("ESPECIES");

            DataColumn coluna;

            coluna = new DataColumn("ESP_CODIGO", typeof(int));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("ESP_DESCRICAO", typeof(string));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("TOTAL", typeof(decimal));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("EXCLUI", typeof(string));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("CONSIDERA_ESP", typeof(string));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("ESP_VINCULADO", typeof(string));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("ESP_SAT", typeof(string));
            tableEspecies.Columns.Add(coluna);

            coluna = new DataColumn("ESP_ECF", typeof(string));
            tableEspecies.Columns.Add(coluna);

            return tableEspecies;

        }

        private void txtID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbEspecies, txtID);
        }

        private void cmbEspecies_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtID.Text = Convert.ToString(cmbEspecies.SelectedValue);
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbEspecies.Focus();
        }

        private void cmbEspecies_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtValor.Text = String.Format("{0:N}", lblResta.Text);
                txtValor.Focus();
            }
        }

        public void CarregaEspecies()
        {
            var esp = new Especie();

            DataRow row = dtEspecies.NewRow();
            row["ESP_CODIGO"] = Convert.ToInt32(Convert.ToDouble(txtID.Text));
            row["ESP_DESCRICAO"] = cmbEspecies.Text;
            row["TOTAL"] = txtValor.Text;
            row["EXCLUI"] = permiteExcluir;
            row["CONSIDERA_ESP"] = esp.ConsideraEspecie(Convert.ToInt32(Convert.ToDouble(txtID.Text)));
            row["ESP_VINCULADO"] = esp.IdentificaEspecieVinculado(Convert.ToInt32(Convert.ToDouble(txtID.Text)));
            row["ESP_SAT"] = esp.IdentificaEspecieSAT(Convert.ToInt32(Convert.ToDouble(txtID.Text)));
            row["ESP_ECF"] = esp.IdentificaEspecieEcf(Convert.ToInt32(Convert.ToDouble(txtID.Text)));

            dtEspecies.Rows.Add(row);

            total = total + Convert.ToDecimal(txtValor.Text);

            lblVTotal.Text = String.Format("{0:N}", total);

            lblTroco.Text = Convert.ToDecimal(lblVTotal.Text) > Convert.ToDecimal(lblTotal.Text) ? String.Format("{0:N}", Convert.ToDecimal(lblVTotal.Text) - Convert.ToDecimal(lblTotal.Text)) : "0,00";

            lblResta.Text = Convert.ToDecimal(lblVTotal.Text) > Convert.ToDecimal(lblTotal.Text) ? "0,00" : String.Format("{0:N}", Convert.ToDecimal(lblTotal.Text) - total);

            dgEspecies.DataSource = dtEspecies;

            txtID.Text = "";
            cmbEspecies.SelectedIndex = -1;
            txtValor.Text = "0,00";
            if (Convert.ToDecimal(lblResta.Text) != 0)
            {
                txtID.Focus();
                permiteExcluir = "S";
            }
            else
            {
                btnConfirma.Focus();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsiteCampos().Equals(true))
                {
                    CarregaEspecies();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsiteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if(Convert.ToDouble(lblTotal.Text).Equals(Convert.ToDouble(lblVTotal.Text)))
            {
                Principal.mensagem = "Somatorio da(s) Espécie(s) igual ao Total da Venda!";
                Funcoes.Avisa();
                btnConfirma.Focus();
                return false;
            }
            if (cmbEspecies.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar a Espécie.";
                Funcoes.Avisa();
                cmbEspecies.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtValor.Text.Trim()) <= 0)
            {
                Principal.mensagem = "Valor não pode ser Zero.";
                Funcoes.Avisa();
                txtValor.Focus();
                return false;
            }
            if (dgEspecies.RowCount > 0)
            {
                DataRow[] linha_resultado  = dtEspecies.Select("ESP_CODIGO = " + Convert.ToDouble(txtID.Text).ToString());
                if (linha_resultado.Length != 0)
                {
                    Principal.mensagem = "Já existe um valor lançado para esta espécie.";
                    Funcoes.Avisa();
                    txtID.Focus();
                    return false;
                }
            }
            if (dgEspecies.RowCount > 0)
            {
                if(!txtID.Text.Equals("1"))
                {
                    double total = 0;
                    for(int i=0;i < dgEspecies.RowCount; i++)
                    {
                        total += Convert.ToDouble(dgEspecies.Rows[i].Cells["VALOR"].Value);
                    }
                    
                    if (Convert.ToDouble(txtValor.Text) + total > Convert.ToDouble(lblTotal.Text))
                    {
                        Principal.mensagem = "Valor das espécies maior que valor da Venda";
                        Funcoes.Avisa();
                        txtValor.Text = String.Format("{0:N}", Convert.ToDouble(lblTotal.Text) - total);
                        txtValor.Focus();
                        return false;
                    }
                }
            }

            return true;

        }

        private void txtValor_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtValor.Text.Trim()))
            {
                txtValor.Text = "0,00";
            }
            else
            {
                txtValor.Text = String.Format("{0:N}", txtValor.Text);
            }
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.PerformClick();
        }

        private void dgEspecies_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int indice = dgEspecies.Rows.Count - 1;
                //SETA PARA BAIXO//
                if (e.KeyCode == Keys.Down && index < indice)
                {
                    index = index + 1;
                    index = Convert.ToInt32(dgEspecies.Rows[index].Cells[0].Value);
                    exclui = dgEspecies.Rows[index].Cells[3].Value.ToString();
                }
                else
                    //SETA PARA CIMA//
                    if (e.KeyCode == Keys.Up && index < indice)
                {
                    index = index - 1;
                    index = Convert.ToInt32(dgEspecies.Rows[index].Cells[0].Value);
                    exclui = dgEspecies.Rows[index].Cells[3].Value.ToString();
                }

                if (e.KeyCode == Keys.Delete)
                {
                    if (exclui.Equals("S"))
                    {
                        DataRow[] linha_resultado = dtEspecies.Select("ESP_CODIGO = " + index);
                        linha_resultado[0].Delete();

                        dgEspecies.DataSource = dtEspecies;

                        if (dgEspecies.RowCount > 0)
                        {
                            total = Convert.ToDecimal(dtEspecies.Compute("Sum(Total)", ""));
                        }
                        else
                            total = 0;

                        lblResta.Text = Convert.ToDecimal(lblVTotal.Text) > Convert.ToDecimal(lblTotal.Text) ? "0,00" : String.Format("{0:N}", Convert.ToDecimal(lblTotal.Text) - total);

                        lblVTotal.Text = String.Format("{0:N}", total);

                        lblTroco.Text = Convert.ToDecimal(lblVTotal.Text) > Convert.ToDecimal(lblTotal.Text) ? String.Format("{0:N}", Convert.ToDecimal(lblVTotal.Text) - Convert.ToDecimal(lblTotal.Text)) : "0,00";

                        txtID.Focus();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEspecies_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            exclui = dgEspecies.Rows[e.RowIndex].Cells[3].Value.ToString();
            index = Convert.ToInt32(Convert.ToDouble(dgEspecies.Rows[e.RowIndex].Cells[0].Value));
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbEspecies_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConfirma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmVenEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnConfirma.PerformClick();
            }
            else if(e.KeyCode == Keys.Escape)
            {
                fVendas.selEspecie = false;
                this.Close();
            }
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtEspecies.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário selecionar Espécie!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtID.Focus();
                    return;
                }
                else
                {
                    if (dtEspecies.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(lblResta.Text) != 0)
                        {
                            MessageBox.Show("Somátorio Invalido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtID.Focus();
                            return;
                        }

                        fVendas.selEspecie = true;
                        fVendas.valorTroco = Convert.ToDouble(lblTroco.Text);
                    }
                    else
                    {
                        fVendas.valorTroco = Convert.ToDouble(lblTroco.Text);
                        fVendas.selEspecie = false;
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
    }
}
