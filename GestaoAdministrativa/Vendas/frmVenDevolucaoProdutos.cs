﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenDevolucaoProdutos : Form, Botoes
    {
        private ToolStripButton tsbDevolucao = new ToolStripButton("Devolução Produtos");
        private ToolStripSeparator tssDevolucao = new ToolStripSeparator();
        DataTable dt = new DataTable();
        int qtdeTotalDeProdutos;
        double valorTotalDeProdutos;
        long vendaID;
        double valorRestante;
        List<VendasFormaPagamento> vendaPedFormaPagto = new List<VendasFormaPagamento>();
        List<VendasEspecies> vendaEspecie = new List<VendasEspecies>();
        private int colCodigo;

        public frmVenDevolucaoProdutos(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dgProdutos.Rows.Clear();
            dgProdutos.Rows.Clear();
            qtdeTotalDeProdutos = 0;
            valorTotalDeProdutos = 0;
            txtIdentificacao.Text = "";
            lblTotalDev.Text = "";
            lblTotalTroca.Text = "";
            lblDiferenca.Text = "";
            txtDesconto.Text = "0";
            gbEspecies.Visible = false;
            gbFormaPagamento.Visible = false;
            btnConfirmarCaixa.Visible = false;
            lblTiTroco.Visible = false;
            lblTroco.Visible = false;
            colCodigo = 0;
            txtIdentificacao.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        private void frmVenDevolucaoProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbDevolucao.AutoSize = false;
                this.tsbDevolucao.Image = Properties.Resources.remover;
                this.tsbDevolucao.Size = new System.Drawing.Size(145, 20);
                this.tsbDevolucao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDevolucao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDevolucao);
                tsbDevolucao.Click += delegate
                {
                    var devolucao = Application.OpenForms.OfType<frmVenDevolucaoProdutos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    devolucao.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDevolucao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDevolucao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenDevolucaoProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnConfirma.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmarCaixa.PerformClick();
                    break;
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private async void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.RowCount == 0)
                {
                    MessageBox.Show("Necessário ao menos um produto a ser Devolvido!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtIdentificacao.Focus();
                    return;
                }
                if (MessageBox.Show("Confirma a Devolução dos Produtos?", "Devolução de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    await EstornoProduto();
                }
                else
                    btnConfirma.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<bool> EstornoProduto()
        {
            try
            {
                var caixa = new AFCaixa();
                List<AFCaixa> cxStatus = caixa.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);

                if (cxStatus.Count.Equals(0))
                {
                    MessageBox.Show("Lançamento com movimentação de caixa com caixa que ainda não foi aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else if (cxStatus[0].DataAbertura.ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
                {
                    MessageBox.Show("Lançamento com movimentação de caixa com data diferente do caixa aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                //IDENTIFICA SE TEM VALOR DE DIFERENÇA//
                if (Convert.ToDouble(lblDiferenca.Text) > 0)
                {
                    Principal.dtPesq = Util.CarregarCombosPorEmpresa("FORMA_ID", "FORMA_DESCRICAO", "FORMAS_PAGAMENTO", true, "FORMA_LIBERADO = 'S' AND OPERACAO_CAIXA = 'S' OR FORMA_dESCRICAO LIKE 'PARTICULAR%'");
                    if (Principal.dtPesq.Rows.Count > 0)
                    {
                        cmbForma.DataSource = Principal.dtPesq;
                        cmbForma.DisplayMember = "FORMA_DESCRICAO";
                        cmbForma.ValueMember = "FORMA_ID";
                        cmbForma.SelectedIndex = -1;
                    }
                    else
                    {
                        MessageBox.Show("Necessário cadastrar pelo menos uma Forma de Pagamento,\npara realizar uma devolução.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Principal.mdiPrincipal.btnSair.PerformClick();
                        this.Close();
                    }

                    Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ESP_CODIGO", "ESP_DESCRICAO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N'");
                    if (Principal.dtPesq.Rows.Count > 0)
                    {
                        cmbEspecies.DataSource = Principal.dtPesq;
                        cmbEspecies.DisplayMember = "ESP_DESCRICAO";
                        cmbEspecies.ValueMember = "ESP_CODIGO";
                        cmbEspecies.SelectedIndex = -1;
                    }
                    else
                    {
                        MessageBox.Show("Necessário cadastrar pelo menos uma Espécie,\npara realizar uma devolução.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Principal.mdiPrincipal.btnSair.PerformClick();
                        this.Close();
                    }

                    if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_BENEFICIO", "VENDA_ID", vendaID.ToString(), true, false, true).Equals("PARTICULAR"))
                    {
                        string retorno = Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_CON_CODIGO", "VENDA_ID", vendaID.ToString(), true, false, true);
                        retorno = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_FORMA_ID", "CON_CODIGO", retorno);

                        txtID.Text = retorno;
                        cmbForma.SelectedValue = retorno;
                    }

                    gbFormaPagamento.Visible = true;
                    gbEspecies.Visible = true;
                    btnConfirmarCaixa.Visible = true;
                    lblTiTroco.Visible = true;
                    lblTroco.Text = "0,00";
                    lblTroco.Visible = true;
                    txtID.Focus();
                }
                else
                {
                    BancoDados.AbrirTrans();

                    if (RetornaEstoque())
                    {
                        if (TiraDoEstoque())
                        {
                            if (AtualizaMovimentoEstoque())
                            {
                                var prodDevolvidos = new ProdutosDevolvidos();
                                if (!prodDevolvidos.AtualizaStatus(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }

                                var prodTroca = new ProdutosTroca();
                                if (!prodTroca.AtualizaStatus(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }

                                BancoDados.FecharTrans();

                                //IDENTIFICA SE USA ESTOQUE FILIAL//
                                if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                {
                                    dt = prodDevolvidos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID);
                                    //VOLTA O ESTOQUE//
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        await AtualizarEstoqueFiliais("mais", Funcoes.LeParametro(9, "52", false), dt.Rows[i]["PROD_CODIGO"].ToString(), Convert.ToInt32(dt.Rows[i]["QTDE"]));
                                    }

                                    //RETIRA DO ESTOQUE//
                                    for (int x = 0; x < dgProdutos.RowCount; x++)
                                    {
                                        await AtualizarEstoqueFiliais("menos", Funcoes.LeParametro(9, "52", false), dgProdutos.Rows[x].Cells[3].Value.ToString(), Convert.ToInt32(dgProdutos.Rows[x].Cells[5].Value));
                                    }
                                }

                                if (MessageBox.Show("Deseja Imprimir Comprovante?", "Devolução de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    Comprovante();
                                }

                                Limpar();
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtDesconto.Text) || Convert.ToDouble(txtDesconto.Text) > 0)
                {
                    lblDiferenca.Text = String.Format("{0:N}", Convert.ToDouble(lblDiferenca.Text) - Math.Abs(Convert.ToDouble(txtDesconto.Text)));
                    //btnConfirma.Focus();
                }
                else
                {
                    txtDesconto.Text = "0";
                }
            }
        }

        private void txtDesconto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbForma.Focus();
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbForma, txtID);
        }

        private void cmbForma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbForma_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbForma.Focus();
        }

        private void cmbForma_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtID.Text = Convert.ToString(cmbForma.SelectedValue);
            txtValor.Text = lblDiferenca.Text;
            txtValor.Focus();
        }

        private void txtValor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Convert.ToDouble(txtValor.Text) > Convert.ToDouble(lblDiferenca.Text))
                {
                    MessageBox.Show("Valor maior que o valor da Diferença R$ " + String.Format("{0:N}", Convert.ToDouble(lblDiferenca.Text)), "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    double total = 0;
                    int parcelas = 0;
                    double valor = 0;

                    for (int x = 0; x < dgFormas.RowCount; x++)
                    {
                        total = total + Convert.ToDouble(dgFormas.Rows[x].Cells[3].Value);
                    }

                    var buscaFormas = new FormasPagamento();

                    DataTable dtRetorno = buscaFormas.DadosDaFormaDePagamento(Principal.empAtual, Convert.ToInt32(txtID.Text));
                    parcelas = Convert.ToInt32(dtRetorno.Rows[0]["QTDE_PARCELAS"]);
                    DateTime vencimento = DateTime.Now;
                    valor = Math.Round(Convert.ToDouble(txtValor.Text) / parcelas, 2);

                    for (int i = 1; i <= parcelas; i++)
                    {
                        if (dtRetorno.Rows[0]["VENCTO_DIA_FIXO"].ToString().Equals("S"))
                        {
                            vencimento = new DateTime(DateTime.Now.Year, DateTime.Now.Month + i, Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                        }
                        else
                        {
                            if (!Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]).Equals(0))
                            {
                                vencimento = vencimento.AddDays(Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                            }
                        }

                        total = total + valor;

                        dgFormas.Rows.Insert(dgFormas.RowCount, new Object[] { Convert.ToDouble(txtID.Text).ToString(), i, vencimento, valor, dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO" });
                    }

                    valorRestante = valorRestante + total;

                    txtID.Text = "";
                    cmbForma.SelectedIndex = -1;
                    txtValor.Text = "0,00";

                    if (valorRestante > 0)
                    {
                        txtID.Focus();
                    }
                    else
                    {
                        txtEspID.Focus();
                    }
                }
            }
        }

        private void txtEspID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbEspecies.Focus();
        }

        private void txtEspID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtEspID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbEspecies, txtEspID);
        }

        private void cmbEspecies_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbEspecies_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtValorEsp.Text = String.Format("{0:N}", lblDiferenca.Text);
                if (dgFormas.RowCount == 0)
                {
                    MessageBox.Show("Necessário selecionar uma Forma de Pagamento", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtID.Focus();
                }
                else
                    txtValorEsp.Focus();
            }
        }

        private void cmbEspecies_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEspID.Text = Convert.ToString(cmbEspecies.SelectedValue);
        }

        private void txtValorEsp_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValorEsp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                var esp = new Especie();

                dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { Convert.ToInt32(Convert.ToDouble(txtEspID.Text)), cmbEspecies.Text, txtValorEsp.Text, "S", esp.ConsideraEspecie(Convert.ToInt32(Convert.ToDouble(txtEspID.Text))),
                   esp.IdentificaEspecieVinculado(Convert.ToInt32(Convert.ToDouble(txtEspID.Text))),esp.IdentificaEspecieSAT(Convert.ToInt32(Convert.ToDouble(txtEspID.Text))) });

                lblTroco.Text = Convert.ToDecimal(txtValorEsp.Text) > Convert.ToDecimal(lblDiferenca.Text) ? String.Format("{0:N}", Convert.ToDecimal(txtValorEsp.Text) - Convert.ToDecimal(lblDiferenca.Text)) : "0,00";

                txtValorEsp.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorEsp.Text) > Convert.ToDecimal(lblDiferenca.Text) ? 0 : Convert.ToDouble(txtValorEsp.Text) - Convert.ToDouble(lblDiferenca.Text));

                txtID.Text = "";
                cmbEspecies.SelectedIndex = -1;

                if (Convert.ToDouble(txtValorEsp.Text) > 0)
                {
                    txtEspID.Focus();
                }
                else
                {
                    btnConfirmarCaixa.Focus();
                }

            }
        }

        private async void btnConfirmarCaixa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgFormas.RowCount == 0)
                {
                    MessageBox.Show("Necessário selecionar a Espécie para Pagamento!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEspID.Focus();
                }
                else
                {
                    BancoDados.AbrirTrans();

                    if (RetornaEstoque())
                    {
                        if (TiraDoEstoque())
                        {
                            if (AtualizaMovimentoEstoque())
                            {
                                if (InsereMovimentoFinanceiro())
                                {
                                    if (AtualizaInformacoesVendas(vendaID))
                                    {
                                        var prodDevolvidos = new ProdutosDevolvidos();
                                        if (!prodDevolvidos.AtualizaStatus(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            return;
                                        }

                                        var prodTroca = new ProdutosTroca();
                                        if (!prodTroca.AtualizaStatus(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            return;
                                        }

                                        BancoDados.FecharTrans();

                                        if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_BENEFICIO", "VENDA_ID",vendaID.ToString(), true, false, true).Equals("PARTICULAR"))
                                        {
                                            if (!InserirCobranca(colCodigo))
                                            {
                                                BancoDados.ErroTrans();
                                            }
                                     
                                        }

                                        if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                        {
                                            dt = prodDevolvidos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID);
                                            //VOLTA O ESTOQUE//
                                            for (int i = 0; i < dt.Rows.Count; i++)
                                            {
                                                await AtualizarEstoqueFiliais("mais", Funcoes.LeParametro(9, "52", false), dt.Rows[i]["PROD_CODIGO"].ToString(), Convert.ToInt32(dt.Rows[i]["QTDE"]));
                                            }

                                            //RETIRA DO ESTOQUE//
                                            for (int x = 0; x < dgProdutos.RowCount; x++)
                                            {
                                                await AtualizarEstoqueFiliais("menos", Funcoes.LeParametro(9, "52", false), dgProdutos.Rows[x].Cells[3].Value.ToString(), Convert.ToInt32(dgProdutos.Rows[x].Cells[5].Value));
                                            }
                                        }

                                        if (MessageBox.Show("Deseja Imprimir Comprovante?", "Devolução de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                        {
                                            Comprovante();
                                        }

                                        Limpar();
                                    }
                                    else
                                    {
                                        BancoDados.ErroTrans();
                                        MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        MessageBox.Show("Erro ao processar solicitação. Contate o suporte!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool RetornaEstoque()
        {
            try
            {
                var atualizaEstoque = new ProdutoDetalhe();
                var buscaProdutos = new ProdutosDevolvidos();
                dt = buscaProdutos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A", vendaID);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    atualizaEstoque.EstCodigo = Convert.ToInt32(dt.Rows[i]["EST_CODIGO"]);
                    atualizaEstoque.EmpCodigo = Convert.ToInt32(dt.Rows[i]["EMP_CODIGO"]);
                    atualizaEstoque.ProdCodigo = dt.Rows[i]["PROD_CODIGO"].ToString();
                    atualizaEstoque.ProdEstAtual = Convert.ToInt32(dt.Rows[i]["QTDE"]);

                    if (!atualizaEstoque.AtualizaEstoqueAtual(atualizaEstoque, "+", true).Equals(1))
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizaInformacoesVendas(long vendaID)
        {
            try
            {
                for (int i = 0; i < dgFormas.RowCount; i++)
                {
                    var formasPagto = new VendasFormaPagamento();
                    formasPagto.EmpCodigo = Principal.empAtual;
                    formasPagto.EstCodigo = Principal.estAtual;
                    formasPagto.VendaId = vendaID;
                    formasPagto.VendaFormaID = Convert.ToInt32(dgFormas.Rows[i].Cells[0].Value);
                    formasPagto.VendaParcela = Convert.ToInt32(dgFormas.Rows[i].Cells[1].Value);
                    formasPagto.VendaParcelaVencimento = Convert.ToDateTime(dgFormas.Rows[i].Cells[2].Value);
                    formasPagto.VendaValorParcela = Convert.ToDouble(dgFormas.Rows[i].Cells[3].Value);
                    vendaPedFormaPagto.Add(formasPagto);
                }

                if (!InserirFormasDePagamento())
                    return false;

                for (int i = 0; i < dgEspecies.RowCount; i++)
                {
                    vendaEspecie.Add(new VendasEspecies
                    {
                        EstCodigo = Principal.empAtual,
                        EmpCodigo = Principal.estAtual,
                        VendaId = vendaID,
                        EspCodigo = Convert.ToInt32(Convert.ToDouble(dgEspecies.Rows[i].Cells[0].Value)),
                        Valor = Convert.ToDouble(dgEspecies.Rows[i].Cells[2].Value),
                        EspDescricao = dgEspecies.Rows[i].Cells[1].Value.ToString(),
                        EspVinculado = Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_VINCULADO", "ESP_CODIGO", dgEspecies.Rows[i].Cells[0].Value.ToString()),
                        EspSAT = Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_SAT", "ESP_CODIGO", dgEspecies.Rows[i].Cells[0].Value.ToString()),
                    });
                }

                if (!InserirEspecies())
                    return false;

                var dadosVenda = new VendasDados();

                if (!dadosVenda.AtualizaInformacoesDevolucao(Principal.estAtual, Principal.empAtual, vendaID))
                    return false;

                if (!dadosVenda.AtualizaInformacoesDevolucaoTotal(Principal.estAtual, Principal.empAtual, vendaID))
                    return false;
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirCobranca(int colCodigo)
        {
            try
            {
                BancoDados.AbrirTrans();
                List<int> result = vendaPedFormaPagto.Select(o => o.VendaFormaID).Distinct().ToList();
                var somaVenda = new VendasDados();

                for (int x = 0; x < result.Count; x++)
                {
                    List<VendasFormaPagamento> retorno = vendaPedFormaPagto.Where(i => i.VendaFormaID == result[x]).ToList();
                    int id = 0;

                    if (retorno[0].VendaParcela.Equals(1))
                    {
                        id = Funcoes.IdentificaVerificaID("COBRANCA", "COBRANCA_ID", Principal.estAtual, "", Principal.empAtual);
                        //COBRANCA
                        var dadosCobranca = new Cobranca
                            (
                                retorno[0].EmpCodigo,
                                retorno[0].EstCodigo,
                                id,
                                Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_CF_ID", "VENDA_ID", vendaID.ToString(), true, false, true)),
                                DateTime.Now,
                                somaVenda.SomatorioVenda(Principal.empAtual,Principal.estAtual,vendaID),
                                retorno[0].VendaFormaID,
                                colCodigo,
                                vendaID,
                                "C",
                                "A",
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!dadosCobranca.InserirDados(dadosCobranca))
                            return false;
                    }

                    int idParcela = Funcoes.IdentificaVerificaID("COBRANCA_PARCELA", "COBRANCA_PARCELA_ID", Principal.estAtual, "", Principal.empAtual);
                    for (int y = 0; y < retorno.Count; y++)
                    {
                        var dadosParcelas = new CobrancaParcela
                            (
                                vendaPedFormaPagto[x].EmpCodigo,
                                vendaPedFormaPagto[x].EstCodigo,
                                idParcela + y,
                                id,
                                retorno[y].VendaParcela,
                                retorno[y].VendaValorParcela,
                                retorno[y].VendaValorParcela,
                                "A",
                                retorno[y].VendaParcelaVencimento,
                                DateTime.Now,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!dadosParcelas.InserirDados(dadosParcelas))
                            return false;

                    }

                    BancoDados.FecharTrans();
                }
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Cobranca", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirEspecies()
        {
            try
            {
                int id = Funcoes.IdentificaVerificaID("VENDAS_ESPECIES", "VENDA_ESPECIE_ID", Principal.estAtual, "", Principal.empAtual);
                for (int x = 0; x < vendaEspecie.Count; x++)
                {
                    double troco = 0;
                    if (vendaEspecie[x].EspDescricao.Equals("DINHEIRO"))
                    {
                        troco = Convert.ToDouble(lblTroco.Text);
                    }
                    //VENDAS_ESPECIES
                    var dadosEspecies = new VendasEspecies
                            (
                                vendaEspecie[x].EmpCodigo,
                                vendaEspecie[x].EstCodigo,
                                vendaEspecie[x].VendaId,
                                id + x,
                                vendaEspecie[x].EspCodigo,
                                vendaEspecie[x].Valor - troco,
                                "",
                                "",
                                "",
                                DateTime.Now,
                                Principal.usuario
                            );
                    if (!dadosEspecies.InserirDados(dadosEspecies))
                        return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirFormasDePagamento()
        {
            try
            {
                List<int> result = vendaPedFormaPagto.Select(o => o.VendaFormaID).Distinct().ToList();

                int id = Funcoes.IdentificaVerificaID("VENDAS_FORMA_PAGAMENTO", "VENDA_FORMA_PAGTO_ID", Principal.estAtual, "", Principal.empAtual);

                for (int x = 0; x < result.Count; x++)
                {
                    List<VendasFormaPagamento> retorno = vendaPedFormaPagto.Where(i => i.VendaFormaID == result[x]).ToList();

                    for (int i = 0; i < retorno.Count; i++)
                    {
                        //VENDAS_FORMA_PAGAMENTO
                        var dadosFormaPagamento = new VendasFormaPagamento
                            (
                                retorno[i].EmpCodigo,
                                retorno[i].EstCodigo,
                                retorno[i].VendaId,
                                id,
                                retorno[i].VendaFormaID,
                                retorno[i].VendaParcela,
                                retorno[i].VendaValorParcela,
                                retorno[i].VendaParcelaVencimento,
                                DateTime.Now,
                                Principal.usuario
                            );
                        if (!dadosFormaPagamento.InserirDados(dadosFormaPagamento))
                            return false;

                        id += 1;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Forma de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool TiraDoEstoque()
        {
            try
            {
                var atualizaEstoque = new ProdutoDetalhe();
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    atualizaEstoque.EstCodigo = Convert.ToInt32(dgProdutos.Rows[i].Cells[0].Value);
                    atualizaEstoque.EmpCodigo = Convert.ToInt32(dgProdutos.Rows[i].Cells[1].Value);
                    atualizaEstoque.ProdCodigo = dgProdutos.Rows[i].Cells[3].Value.ToString();
                    atualizaEstoque.ProdEstAtual = Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);

                    if (!atualizaEstoque.AtualizaEstoqueAtual(atualizaEstoque, "-", true).Equals(1))
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private async Task AtualizarEstoqueFiliais(string operacao, string filial, string codBarras, int qtde)
        {
            try
            {

                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/DecrementaOuSomaEstoque?codEstab=" + filial
                                    + "&codBarras=" + codBarras
                                    + "&qtd=" + qtde
                                    + "&operacao=" + operacao);
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\DevolucaoProduto.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + codBarras + " / QTDE: " + qtde.ToString());
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Comprovante()
        {
            try
            {
                var dadosEstabelecimento = new Estabelecimento();
                string tamanho, comprovanteDevolucao;
                int quantidade = 0;
                double total = 0;
                List<Cliente> dadosVendaCliente = new List<Cliente>();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                if (retornoEstab.Count > 0)
                {
                    comprovanteDevolucao = Funcoes.CentralizaTexto("DEVOLUCAO DE PRODUTOS", 43) + "\n";
                    comprovanteDevolucao += Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                    comprovanteDevolucao += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                    comprovanteDevolucao += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                    comprovanteDevolucao += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                    tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                    comprovanteDevolucao += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                    comprovanteDevolucao += retornoEstab[0].EstBairro + "\n";
                    comprovanteDevolucao += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                    comprovanteDevolucao += "Data: " + DateTime.Now.ToString("dd/MM/yyyy") + "     Hora: " + DateTime.Now.ToString("HH:mm:ss") + "\n";
                    comprovanteDevolucao += "__________________________________________" + "\n";
                    comprovanteDevolucao += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovanteDevolucao += "__________________________________________" + "\n";
                    comprovanteDevolucao += Funcoes.CentralizaTexto("PRODUTOS DEVOLVIDOS", 43) + "\n";

                    var buscaProdutos = new ProdutosDevolvidos();
                    dt = buscaProdutos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "F", vendaID);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        comprovanteDevolucao += Funcoes.PrencherEspacoEmBranco(dt.Rows[i]["PROD_CODIGO"].ToString(), 13) + " " + (dt.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? dt.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : dt.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                        comprovanteDevolucao += "    R$ " + dt.Rows[i]["PRECO"].ToString().ToString().Replace(",", ".") + " X " + dt.Rows[i]["QTDE"].ToString() + " =  R$ " + dt.Rows[i]["TOTAL"].ToString().Replace(",", ".") + "\n";
                        if (Convert.ToDouble(dt.Rows[i]["DESCONTO"]) > 0)
                        {
                            comprovanteDevolucao += "    DESCONTO R$ " + dt.Rows[i]["DESCONTO"].ToString().Replace(",", ".") + "\n";
                        }

                        quantidade += Convert.ToInt32(dt.Rows[i]["QTDE"]);
                        total += Convert.ToDouble(dt.Rows[i]["TOTAL"]);
                    }

                    comprovanteDevolucao += "==========================================" + "\n";
                    comprovanteDevolucao += "Quantidade de Produtos : " + quantidade + "\n";
                    comprovanteDevolucao += "Valor Total de Produtos: " + String.Format("{0:N}", total) + "\n";
                    comprovanteDevolucao += "==========================================" + "\n";

                    quantidade = 0;
                    total = 0;

                    comprovanteDevolucao += Funcoes.CentralizaTexto("PRODUTOS NOVOS", 43) + "\n";
                    for (int i = 0; i < dgProdutos.Rows.Count; i++)
                    {
                        comprovanteDevolucao += Funcoes.PrencherEspacoEmBranco(dgProdutos.Rows[i].Cells[3].Value.ToString(), 13) + " " + (dgProdutos.Rows[i].Cells[4].Value.ToString().Length > 29 ? dgProdutos.Rows[i].Cells[4].Value.ToString().Substring(0, 28) : dgProdutos.Rows[i].Cells[4].Value) + "\n";
                        comprovanteDevolucao += "    R$ " + dgProdutos.Rows[i].Cells[6].Value.ToString().Replace(",", ".") + " X " + dgProdutos.Rows[i].Cells[5].Value + " =  R$ " + dgProdutos.Rows[i].Cells[8].Value.ToString().Replace(",", ".") + "\n";
                        if (Convert.ToDouble(dgProdutos.Rows[i].Cells[7].Value) > 0)
                        {
                            comprovanteDevolucao += "    DESCONTO R$ " + dgProdutos.Rows[i].Cells[7].Value.ToString().Replace(",", ".") + "\n";
                        }
                        quantidade += Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                        total += Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value);
                    }

                    comprovanteDevolucao += "==========================================" + "\n";
                    comprovanteDevolucao += "Quantidade de Produtos : " + quantidade + "\n";
                    comprovanteDevolucao += "Valor Total de Produtos: " + String.Format("{0:N}", total) + "\n";
                    comprovanteDevolucao += "==========================================" + "\n\n";

                    if (dgFormas.Visible && dgEspecies.Visible)
                    {
                        var descricaoForma = new FormasPagamento();
                        comprovanteDevolucao += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO DIFERENCA", 43) + "\n";

                        for (int x = 0; x < dgFormas.RowCount; x++)
                        {
                            comprovanteDevolucao += descricaoForma.IdentificaFormaDePagamento(Convert.ToInt32(dgFormas.Rows[x].Cells[0].Value), Principal.empAtual) + ".: R$ " + Funcoes.BValor(Convert.ToDouble(dgFormas.Rows[x].Cells[3].Value)) + "\n";
                        }
                    }

                    comprovanteDevolucao += "__________________________________________" + "\n";
                    comprovanteDevolucao += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                    comprovanteDevolucao += "__________________________________________" + "\n\n";


                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovanteDevolucao, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteDevolucao);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                            {
                                writer.WriteLine(comprovanteDevolucao);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovanteDevolucao += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteDevolucao);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        comprovanteDevolucao += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteDevolucao, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizaMovimentoEstoque()
        {
            try
            {
                var removeProduto = new ProdutosTroca();
                var itens = new VendasItens();
                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);
                var buscaProdutos = new ProdutosDevolvidos();
                dt = buscaProdutos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A", vendaID);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = id,
                        ProdCodigo = dt.Rows[i]["PROD_CODIGO"].ToString(),
                        EntQtde = Convert.ToInt32(dt.Rows[i]["QTDE"]),
                        EntValor = Convert.ToDouble(dt.Rows[i]["TOTAL"]),
                        SaiQtde = 0,
                        SaiValor = 0,
                        OperacaoCodigo = "D",
                        DataMovimento = DateTime.Now,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    id += 1;

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;

                    //identifica se o produto da troca e o mesmo//
                    int qtde = removeProduto.IdentificaSeOProdutoEoMesmoEQtde(Convert.ToInt32(dt.Rows[i]["EST_CODIGO"]), Convert.ToInt32(dt.Rows[i]["EMP_CODIGO"]),
                        Convert.ToInt32(txtIdentificacao.Text), Convert.ToInt64(dt.Rows[i]["VENDA_ID"]), dt.Rows[i]["PROD_CODIGO"].ToString());
                    if (qtde.Equals(0))
                    {
                        if (!itens.ExcluirProdutos(Convert.ToInt32(dt.Rows[i]["EST_CODIGO"]), Convert.ToInt32(dt.Rows[i]["EMP_CODIGO"]), Convert.ToInt64(dt.Rows[i]["VENDA_ID"]), dt.Rows[i]["PROD_CODIGO"].ToString(),true))
                            return false;
                    }
                    else
                    {
                        var atualizaItem = new VendasItens();
                        atualizaItem.AtualizaProdutoTroca(Convert.ToInt64(dt.Rows[i]["VENDA_ID"]), Convert.ToInt32(dt.Rows[i]["EMP_CODIGO"]), Convert.ToInt32(dt.Rows[i]["EST_CODIGO"]),
                            dt.Rows[i]["PROD_CODIGO"].ToString(), qtde, Convert.ToDouble(dt.Rows[i]["PRECO"]), Convert.ToDouble(dt.Rows[i]["DESCONTO"]), Convert.ToDouble(dt.Rows[i]["TOTAL"]));
                    }
                }

                int vendaItem = itens.BuscaMaxVendaItem(Convert.ToInt32(dt.Rows[0]["EST_CODIGO"]), Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]), Convert.ToInt64(dt.Rows[0]["VENDA_ID"]));
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = id,
                        ProdCodigo = dgProdutos.Rows[i].Cells[3].Value.ToString(),
                        EntQtde = 0,
                        EntValor = 0,
                        SaiQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value),
                        SaiValor = Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value),
                        OperacaoCodigo = "V",
                        DataMovimento = DateTime.Now,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    id += 1;

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;

                    VendasItens dadosDaVendaItens = new VendasItens();

                    dadosDaVendaItens.EmpCodigo = Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]);
                    dadosDaVendaItens.EstCodigo = Convert.ToInt32(dt.Rows[0]["EST_CODIGO"]);
                    dadosDaVendaItens.VendaID = Convert.ToInt64(dt.Rows[0]["VENDA_ID"]);
                    dadosDaVendaItens.VendaItem = vendaItem;
                    dadosDaVendaItens.ProdID = Convert.ToInt32(dgProdutos.Rows[i].Cells[2].Value);
                    dadosDaVendaItens.ProdCodigo = Convert.ToString(dgProdutos.Rows[i].Cells[3].Value);
                    dadosDaVendaItens.VendaItemQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                    dadosDaVendaItens.VendaItemUnitario = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value);
                    dadosDaVendaItens.VendaItemSubTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                    dadosDaVendaItens.VendaItemDiferenca = ((Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value))
                        - (Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value))) * -1;
                    dadosDaVendaItens.VendaItemTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value);
                    dadosDaVendaItens.VendaItemUnidade = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_UNIDADE", "PROD_CODIGO", Convert.ToString(dgProdutos.Rows[i].Cells[3].Value),
                        false, true, false).ToString();
                    dadosDaVendaItens.VendaPreValor = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value);
                    dadosDaVendaItens.OpCadastro = Principal.usuario;
                    dadosDaVendaItens.DtCadastro = DateTime.Now;
                    dadosDaVendaItens.VendaItemReceita = "N";
                    string comissao = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS_DETALHE", "PROD_COMISSAO", "PROD_CODIGO", Convert.ToString(dgProdutos.Rows[i].Cells[3].Value),
                        true, true, true);
                    dadosDaVendaItens.VendaItemComissao = comissao == "" ? 0 : Convert.ToDouble(comissao);
                    dadosDaVendaItens.VendaItemLote = null;
                    dadosDaVendaItens.VendaPromocao = "N";
                    dadosDaVendaItens.VendaDescLiberado = "N";
                    dadosDaVendaItens.VendaItemIndice = id;
                    dadosDaVendaItens.ColCodigo = Convert.ToInt32(Convert.ToInt64(dt.Rows[0]["COL_CODIGO"]));
                    colCodigo = dadosDaVendaItens.ColCodigo;
                    vendaItem = vendaItem + 1;

                    if (!dadosDaVendaItens.InserirItensPreVenda(dadosDaVendaItens, true))
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InsereMovimentoFinanceiro()
        {
            try
            {
                DateTime dtDevolucao = DateTime.Now;
                long movtoFinanceiroSeq = Convert.ToInt64(Funcoes.IdentificaVerificaID("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_SEQ", Principal.estAtual, " MOVIMENTO_FINANCEIRO_DATA = " + Funcoes.BData(dtDevolucao) + " AND MOVIMENTO_FINANCEIRO_USUARIO = '" + Principal.usuario + "'", Principal.empAtual));
                long idCAixa = Funcoes.GeraIDLong("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID", Principal.estAtual, "", Principal.empAtual);
                long movtoFinanceiroID = Funcoes.GeraIDLong("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_ID", Principal.estAtual, "", Principal.empAtual);

                for (int i = 0; i < dgFormas.RowCount; i++)
                {
                    //MOVIMENTO FINANCEIRO//
                    var mFinanceiro = new MovimentoFinanceiro
                        (
                            Principal.empAtual,
                            Principal.estAtual,
                            movtoFinanceiroID + i,
                            dtDevolucao,
                            dtDevolucao,
                            "V",
                            Principal.usuario,
                            Convert.ToDouble(dgFormas.Rows[i].Cells[3].Value),
                            movtoFinanceiroSeq,
                            DateTime.Now,
                            Principal.usuario,
                            vendaID
                        );

                    if (!mFinanceiro.InserirDados(mFinanceiro))
                        return false;


                    //MOVIMENTO DE CAIXA//
                    var mCaixa = new MovimentoCaixa
                        (
                            Principal.empAtual,
                            Principal.estAtual,
                            idCAixa + i,
                            dtDevolucao,
                            Convert.ToInt32(Funcoes.LeParametro(6, "75", false)),
                            "+",
                            Convert.ToDouble(dgFormas.Rows[i].Cells[3].Value),
                            Principal.usuario,
                            movtoFinanceiroID,
                            movtoFinanceiroSeq,
                            DateTime.Now,
                            Principal.usuario,
                            "",
                            vendaID
                        );

                    if (!mCaixa.InserirDados(mCaixa))
                        return false;

                    long id = Funcoes.GeraIDLong("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID", Principal.estAtual, "", Principal.empAtual);
                    //MOVIMENTO DE CAIXA ESPECIE//
                    for (int y = 0; y < dgEspecies.RowCount; y++)
                    {
                        double troco = 0;
                        if (dgEspecies.Rows[y].Cells[1].Value.Equals("DINHEIRO"))
                        {
                            troco = Convert.ToDouble(lblTroco.Text);
                        }
                        var mCaixaEspecie = new MovimentoCaixaEspecie
                            (
                                Principal.empAtual,
                                Principal.estAtual,
                                id + y,
                                dtDevolucao,
                                Convert.ToInt32(dgEspecies.Rows[y].Cells[0].Value),
                                Convert.ToDouble(dgEspecies.Rows[y].Cells[2].Value) - troco,
                                Principal.usuario,
                                movtoFinanceiroID,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!mCaixaEspecie.InserirDados(mCaixaEspecie))
                            return false;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtIdentificacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtIdentificacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtIdentificacao.Text))
                    {
                        var buscaRegistros = new ProdutosTroca();
                        if (buscaRegistros.VerificaNumIdentificacaoAberta(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A").Equals("A"))
                        {
                            dt = buscaRegistros.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A");
                            vendaID = Convert.ToInt64(dt.Rows[0]["VENDA_ID"]);
                            valorTotalDeProdutos = 0;
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] {  dt.Rows[i]["EST_CODIGO"], dt.Rows[i]["EMP_CODIGO"],dt.Rows[i]["PROD_ID"],
                                dt.Rows[i]["PROD_CODIGO"],dt.Rows[i]["PROD_DESCR"], dt.Rows[i]["QTDE"], dt.Rows[0]["PRECO"], dt.Rows[i]["DESCONTO"], dt.Rows[i]["TOTAL"], dt.Rows[i]["ID"] });

                                valorTotalDeProdutos += Convert.ToDouble(dt.Rows[i]["TOTAL"]);
                            }

                            lblTotalTroca.Text = String.Format("{0:N}", valorTotalDeProdutos);

                            var somatorio = new ProdutosDevolvidos();
                            lblTotalDev.Text = String.Format("{0:N}", somatorio.SomatorioDevolucao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A"));

                            lblDiferenca.Text = String.Format("{0:N}", Convert.ToDouble(lblTotalTroca.Text) - Convert.ToDouble(lblTotalDev.Text));
                            btnConfirma.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Número de Identificação inválido", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtIdentificacao.Text = "";
                            txtIdentificacao.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Necessário informar o Número de Identificação", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgEspecies_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            try
            {
                double somatorio = 0;
                for (int i = 0; i < dgEspecies.RowCount; i++)
                {
                    somatorio += Convert.ToDouble(dgEspecies.Rows[i].Cells[2].Value);
                }

                if (somatorio > Convert.ToDouble(lblDiferenca.Text))
                {
                    lblTroco.Text = String.Format("{0:N}", somatorio - Convert.ToDouble(lblDiferenca.Text));
                }
                else
                {
                    lblTroco.Text = "0,00";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool AtualizaItensVenda()
        {
            try
            {
                //deleta produto do venda_itens//
                var itens = new VendasItens();
                var buscaProdutos = new ProdutosDevolvidos();
                dt = buscaProdutos.BuscaDevolucaoPorIdentificacao(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A", vendaID);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (!itens.ExcluirProdutos(Convert.ToInt32(dt.Rows[i]["EST_CODIGO"]), Convert.ToInt32(dt.Rows[i]["EMP_CODIGO"]), Convert.ToInt64(dt.Rows[i]["VENDA_ID"]), dt.Rows[i]["PROD_CODIGO"].ToString(),true))
                        return false;
                }

                int vendaItem = itens.BuscaMaxVendaItem(Convert.ToInt32(dt.Rows[0]["EST_CODIGO"]), Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]), Convert.ToInt64(dt.Rows[0]["VENDA_ID"]));
                for (int i = 0; i < dgProdutos.Rows.Count; i++)
                {
                    VendasItens dadosDaVendaItens = new VendasItens();

                    dadosDaVendaItens.EmpCodigo = Convert.ToInt32(dt.Rows[0]["EMP_CODIGO"]);
                    dadosDaVendaItens.EstCodigo = Convert.ToInt32(dt.Rows[0]["EST_CODIGO"]);
                    dadosDaVendaItens.VendaID = Convert.ToInt64(dt.Rows[0]["VENDA_ID"]);
                    dadosDaVendaItens.VendaItem = vendaItem;
                    dadosDaVendaItens.ProdID = Convert.ToInt32(dgProdutos.Rows[i].Cells[2].Value);
                    dadosDaVendaItens.ProdCodigo = Convert.ToString(dgProdutos.Rows[i].Cells[3].Value);
                    dadosDaVendaItens.VendaItemQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                    dadosDaVendaItens.VendaItemUnitario = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value);
                    dadosDaVendaItens.VendaItemSubTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                    dadosDaVendaItens.VendaItemDiferenca = ((Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value))
                        - (Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value))) * -1;
                    dadosDaVendaItens.VendaItemTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value);
                    dadosDaVendaItens.VendaItemUnidade = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_UNIDADE", "PROD_CODIGO", Convert.ToString(dgProdutos.Rows[i].Cells[3].Value),
                        false, true, false).ToString();
                    dadosDaVendaItens.VendaPreValor = Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value);
                    dadosDaVendaItens.OpCadastro = Principal.usuario;
                    dadosDaVendaItens.DtCadastro = DateTime.Now;
                    dadosDaVendaItens.VendaItemReceita = "N";
                    dadosDaVendaItens.VendaItemComissao = Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS_DETALHE", "COALESCE(PROD_COMISSAO,0) AS PROD_COMISSAO", "PROD_CODIGO", Convert.ToString(dgProdutos.Rows[i].Cells[3].Value),
                        true, true, true));

                    dadosDaVendaItens.VendaItemLote = null;
                    dadosDaVendaItens.VendaPromocao = "N";
                    dadosDaVendaItens.VendaDescLiberado = "N";

                    vendaItem = vendaItem + 1;
                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
