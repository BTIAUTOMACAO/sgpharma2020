﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenFormaPagto : Form
    {
        private int parcelas;
        private DateTime vencimento;
        private double valor;
        private double total;
        private double totalVenda;
        private double valorRestante;
        public DataTable dtFormas = new DataTable();
        private DataTable dtRetorno = new DataTable();
        private DataTable dtDados = new DataTable();
        frmVenda fVendas;
        private string sCaixa;

        public frmVenFormaPagto(frmVenda form, string statusCaixa)
        {
            InitializeComponent();
            fVendas = form;
            sCaixa = statusCaixa;
        }

        private void frmVenFormaPagto_Load(object sender, EventArgs e)
        {
            try
            {
                fVendas.selForma = false;
                valorRestante = 0;
                lblTotalVenda.Text = "Total da Venda: " + String.Format("{0:N}", fVendas.lblTotal.Text);
                dtFormas = tabelaFormas();
                if (fVendas.vendaBeneficio.Equals("N"))
                {
                    Principal.dtPesq = Util.CarregarCombosPorEmpresa("FORMA_ID", "FORMA_DESCRICAO", "FORMAS_PAGAMENTO", true, "FORMA_LIBERADO = 'S' AND CONV_BENEFICIO = 'N' OR FORMA_ID = 9");
                }
                else
                {
                    Principal.dtPesq = Util.CarregarCombosPorEmpresa("FORMA_ID", "FORMA_DESCRICAO", "FORMAS_PAGAMENTO", true, "FORMA_LIBERADO = 'S'");
                }

                if (Principal.dtPesq.Rows.Count > 0)
                {
                    cmbForma.DataSource = Principal.dtPesq;
                    cmbForma.DisplayMember = "FORMA_DESCRICAO";
                    cmbForma.ValueMember = "FORMA_ID";
                    cmbForma.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Forma de Pagamento,\npara realizar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    this.Close();
                }

                if (fVendas.vendaPedFormaPagto.Count > 0 && sCaixa.Equals("S"))
                {
                    for (int i = 0; i < fVendas.vendaPedFormaPagto.Count; i++)
                    {
                        string operacaoCaixa = Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "OPERACAO_CAIXA", "FORMA_ID", fVendas.vendaPedFormaPagto[i].VendaFormaID.ToString(), false, false, true);
                        DataRow row = dtFormas.NewRow();
                        row["FORMA_ID"] = fVendas.vendaPedFormaPagto[i].VendaFormaID;
                        row["PARCELA"] = fVendas.vendaPedFormaPagto[i].VendaParcela;
                        row["VENC"] = fVendas.vendaPedFormaPagto[i].VendaParcelaVencimento;
                        row["TOTAL"] = fVendas.vendaPedFormaPagto[i].VendaValorParcela;
                        row["COND"] = operacaoCaixa == "S" ? "RECEBIDO (CAIXA)" : "PRAZO";

                        dtFormas.Rows.Add(row);

                        total = total + valor;

                        dgFormas.Rows.Insert(dgFormas.RowCount, new Object[] { fVendas.vendaPedFormaPagto[i].VendaFormaID, fVendas.vendaPedFormaPagto[i].VendaParcela,
                            fVendas.vendaPedFormaPagto[i].VendaParcelaVencimento, fVendas.vendaPedFormaPagto[i].VendaValorParcela,
                            operacaoCaixa == "S" ? "RECEBIDO (CAIXA)" : "PRAZO" });
                    }

                    btnConfirma.Focus();
                }
                else
                {
                    if (!fVendas.codFormaPagto.Equals(0))
                    {
                        if (fVendas.vendaBeneficio.Equals("DROGABELLA/PLANTAO") && fVendas.venOrcamento && fVendas.chkEntrega.Checked && fVendas.diferencaDoValorAutorizado != 0)
                        {
                            fVendas.selForma = true;
                            cmbForma.SelectedValue = fVendas.codFormaPagto;
                            txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                            totalVenda = fVendas.sTotal;
                            CarregaForma();
                            
                            cmbForma.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(6, "356", true));
                            txtValor.Text = String.Format("{0:N}", fVendas.diferencaDoValorAutorizado);
                            totalVenda = fVendas.sTotal;
                            CarregaForma();

                            txtID.Enabled = false;
                            cmbForma.Enabled = false;
                            txtValor.Enabled = false;
                            btnOk.Enabled = false;
                            btnConfirma.Focus();
                        }
                        else if (fVendas.vendaBeneficio.Equals("DROGABELLA/PLANTAO") && !fVendas.venOrcamento)
                        {
                            if (fVendas.diferencaDoValorAutorizado != 0)
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            }
                            else
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.sTotal);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = false;
                                cmbForma.Enabled = false;
                                txtValor.Enabled = false;
                                btnOk.Enabled = false;
                                btnConfirma.Focus();
                            }
                        }
                        else if (fVendas.vendaBeneficio.Equals("VIDALINK") && !fVendas.venOrcamento)
                        {
                            if (fVendas.diferencaDoValorAutorizado != 0)
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            }
                            else
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.valorAutorizado;
                                CarregaForma();

                                txtID.Enabled = false;
                                cmbForma.Enabled = false;
                                txtValor.Enabled = false;
                                btnOk.Enabled = false;
                                btnConfirma.Focus();
                            }
                        }
                        else if (fVendas.vendaBeneficio.Equals("FARMACIAPOPULAR") && !fVendas.venOrcamento)
                        {
                            //if (fVendas.diferencaDoValorAutorizado != 0) //Feito pela ariane, esta dando erro, não aparece as formas de pagamento. 
                            //if (fVendas.pagamentoTotalOuParcial == "P") //Alterado para o metodo antigo 
                            //{
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            //}
                            //else
                            //{
                            //    fVendas.selForma = true;
                            //    cmbForma.SelectedValue = fVendas.codFormaPagto;
                            //    txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                            //    totalVenda = fVendas.valorAutorizado;
                            //    CarregaForma();

                            //    txtID.Enabled = false;
                            //    cmbForma.Enabled = false;
                            //    txtValor.Enabled = false;
                            //    btnOk.Enabled = false;
                            //    btnConfirma.Focus();
                            //}
                        }
                        else if (fVendas.vendaBeneficio.Equals("EPHARMA") && !fVendas.venOrcamento)
                        {
                            if (fVendas.diferencaDoValorAutorizado != 0)
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            }
                            else
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.valorAutorizado;
                                CarregaForma();

                                txtID.Enabled = false;
                                cmbForma.Enabled = false;
                                txtValor.Enabled = false;
                                btnOk.Enabled = false;
                                btnConfirma.Focus();
                            }
                        }
                        else if (fVendas.vendaBeneficio.Equals("FUNCIONAL") && fVendas.venOrcamento && fVendas.chkEntrega.Checked && fVendas.diferencaDoValorAutorizado != 0)
                        {
                            fVendas.selForma = true;
                            cmbForma.SelectedValue = fVendas.codFormaPagto;
                            txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                            totalVenda = fVendas.sTotal;
                            CarregaForma();

                            cmbForma.SelectedValue = Convert.ToInt32(Funcoes.LeParametro(6, "356", true));
                            txtValor.Text = String.Format("{0:N}", fVendas.diferencaDoValorAutorizado);
                            totalVenda = fVendas.sTotal;
                            CarregaForma();

                            txtID.Enabled = false;
                            cmbForma.Enabled = false;
                            txtValor.Enabled = false;
                            btnOk.Enabled = false;
                            btnConfirma.Focus();
                        }
                        else if(fVendas.vendaBeneficio.Equals("FUNCIONAL") && !fVendas.venOrcamento)
                        {
                            if (fVendas.diferencaDoValorAutorizado != 0)
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            }
                            else
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.valorAutorizado;
                                CarregaForma();

                                txtID.Enabled = false;
                                cmbForma.Enabled = false;
                                txtValor.Enabled = false;
                                btnOk.Enabled = false;
                                btnConfirma.Focus();
                            }
                        }
                        else if (fVendas.vendaBeneficio.Equals("PORTALDROGARIA") && !fVendas.venOrcamento)
                        {
                            if (fVendas.diferencaDoValorAutorizado != 0)
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.sTotal;
                                CarregaForma();

                                txtID.Enabled = true;
                                cmbForma.Enabled = true;
                                txtValor.Enabled = true;
                                btnOk.Enabled = true;
                                btnConfirma.Focus();
                            }
                            else
                            {
                                fVendas.selForma = true;
                                cmbForma.SelectedValue = fVendas.codFormaPagto;
                                txtValor.Text = String.Format("{0:N}", fVendas.valorAutorizado);
                                totalVenda = fVendas.valorAutorizado;
                                CarregaForma();

                                txtID.Enabled = false;
                                cmbForma.Enabled = false;
                                txtValor.Enabled = false;
                                btnOk.Enabled = false;
                                btnConfirma.Focus();
                            }
                        }
                        else
                        {
                            fVendas.selForma = true;
                            cmbForma.SelectedValue = fVendas.codFormaPagto;
                            txtValor.Text = String.Format("{0:N}", fVendas.sTotal);
                            totalVenda = fVendas.sTotal;
                            CarregaForma();

                            txtID.Enabled = false;
                            cmbForma.Enabled = false;
                            txtValor.Enabled = false;
                            btnOk.Enabled = false;
                            btnConfirma.Focus();
                        }
                    }
                    else
                    {
                        totalVenda = Convert.ToDouble(fVendas.lblTotal.Text);
                        txtID.Enabled = true;
                        cmbForma.Enabled = true;
                        txtValor.Enabled = true;
                        btnOk.Enabled = true;
                    }

                    if(dgFormas.RowCount == 0)
                    {
                        dgFormas.AllowUserToDeleteRows = true;
                    }
                    else
                    {
                        dgFormas.AllowUserToDeleteRows = false;
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas Pagto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbForma.Focus();
        }

        private void txtID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbForma, txtID);
        }

        private void cmbForma_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtID.Text = Convert.ToString(cmbForma.SelectedValue);
        }

        private void cmbForma_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (valorRestante > 0)
                {
                    txtValor.Text = String.Format("{0:N}", totalVenda - valorRestante);
                }
                else
                    txtValor.Text = String.Format("{0:N}", fVendas.lblTotal.Text);

                txtValor.Focus();
            }
        }

        private void txtValor_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtValor.Text.Trim()))
            {
                txtValor.Text = "0,00";
            }
            else
            {
                txtValor.Text = String.Format("{0:N}", txtValor.Text);
            }
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.PerformClick();
        }

        public bool ConsiteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (cmbForma.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar a Forma de Pagamento.";
                Funcoes.Avisa();
                cmbForma.Focus();
                return false;
            }
            if (Convert.ToDecimal(txtValor.Text.Trim()) <= 0)
            {
                Principal.mensagem = "Valor não pode ser Zero.";
                Funcoes.Avisa();
                txtValor.Focus();
                return false;
            }
            return true;

        }

        public void CarregaForma()
        {
            try
            {
                if (Convert.ToDouble(txtValor.Text) > totalVenda)
                {
                    MessageBox.Show("Valor maior que o valor da Venda R$ " + String.Format("{0:N}", totalVenda), "Formas de Pagto", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    total = 0;
                    parcelas = 0;
                    valor = 0;
                    valorRestante = 0;

                    for (int x = 0; x < dgFormas.RowCount; x++)
                    {
                        total = total + Convert.ToDouble(dgFormas.Rows[x].Cells[3].Value);
                    }

                    if (totalVenda != total)
                    {
                        var buscaFormas = new FormasPagamento();

                        dtRetorno = buscaFormas.DadosDaFormaDePagamento(Principal.empAtual, Convert.ToInt32(txtID.Text));
                        parcelas = Convert.ToInt32(dtRetorno.Rows[0]["QTDE_PARCELAS"]);
                        vencimento = DateTime.Now;
                        valor = Math.Round(Convert.ToDouble(txtValor.Text) / parcelas, 2);

                        for (int i = 1; i <= parcelas; i++)
                        {
                            if (dtRetorno.Rows[0]["VENCTO_DIA_FIXO"].ToString().Equals("S"))
                            {
                                if (DateTime.Now.Month == 12)
                                {
                                    vencimento = new DateTime(DateTime.Now.Year + 1,  i, Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                }
                                else
                                {
                                    vencimento = new DateTime(DateTime.Now.Year, DateTime.Now.Month + i, Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                }
                            }
                            else
                            {
                                if (!Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]).Equals(0))
                                {
                                    vencimento = vencimento.AddDays(Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                }
                            }
                            
                            if (i == parcelas && fVendas.vendaBeneficio.Equals("PARTICULAR"))
                            {
                                double diferenca = Math.Round(totalVenda - (total + valor), 2);
                                if (diferenca != 0)
                                    valor = valor + diferenca;
                            }
                            DataRow row = dtFormas.NewRow();
                            row["FORMA_ID"] = txtID.Text;
                            row["PARCELA"] = i;
                            row["VENC"] = vencimento;
                            row["TOTAL"] = valor;
                            row["COND"] = dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO";

                            dtFormas.Rows.Add(row);

                            total = total + valor;

                            dgFormas.Rows.Insert(dgFormas.RowCount, new Object[] { Convert.ToDouble(txtID.Text).ToString(), i, vencimento, valor, dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO" });
                        }

                        valorRestante = valorRestante + total;

                        if (totalVenda > total)
                        {
                            txtID.Text = "";
                            cmbForma.SelectedIndex = -1;
                            txtValor.Text = String.Format("{0:N}", totalVenda - valorRestante);
                            txtID.Focus();
                        }
                        else
                        {
                            txtID.Text = "";
                            cmbForma.SelectedIndex = -1;
                            txtValor.Text = String.Format("{0:N}", totalVenda - valorRestante);
                            btnConfirma.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Inclusão não permitida. Valor Total está correto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnConfirma.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsiteCampos().Equals(true))
                {
                    CarregaForma();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas de Pagto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public DataTable tabelaFormas()
        {
            DataTable tableFormas = new DataTable("FORMAS_PAGTO");

            DataColumn coluna;

            coluna = new DataColumn("FORMA_ID", typeof(string));
            tableFormas.Columns.Add(coluna);

            coluna = new DataColumn("PARCELA", typeof(string));
            tableFormas.Columns.Add(coluna);

            coluna = new DataColumn("VENC", typeof(DateTime));
            tableFormas.Columns.Add(coluna);

            coluna = new DataColumn("TOTAL", typeof(double));
            tableFormas.Columns.Add(coluna);

            coluna = new DataColumn("COND", typeof(string));
            tableFormas.Columns.Add(coluna);

            return tableFormas;
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgFormas.RowCount > 0)
                {
                    if (dtFormas.Rows[0]["COND"].Equals("RECEBIDO (CAIXA)"))
                    {
                        fVendas.totalVenda = Convert.ToDouble(dtFormas.Compute("SUM(TOTAL)", String.Empty));
                    }
                    else
                        fVendas.totalVenda = 0;


                    if (Math.Round(Convert.ToDouble(dtFormas.Compute("SUM(TOTAL)", String.Empty)),2) < Convert.ToDouble(fVendas.lblTotal.Text))
                    {
                        MessageBox.Show("Somátorio Invalido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtID.Focus();
                        return;
                    }

                    fVendas.selForma = true;
                }
                else
                {
                    fVendas.selForma = false;
                }

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Formas Pagto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                //if (fVendas.venOrcamento && !fVendas.vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                //{
                    fVendas.selForma = false;
                    this.Close();
                //}
            }
            if (e.KeyCode == Keys.F1)
            {
                btnConfirma.PerformClick();
            }
        }

        private void frmVenFormaPagto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbForma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgFormas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConfirma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgFormas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnConfirma.Focus();
            }
        }

        private void dgFormas_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            total = 0;
            parcelas = 0;
            valor = 0;
            valorRestante = 0;

            for (int x = 0; x < dgFormas.RowCount; x++)
            {
                total = total + Convert.ToDouble(dgFormas.Rows[x].Cells[3].Value);
            }

            valorRestante = valorRestante + total;
            txtID.Text = "";
            cmbForma.SelectedIndex = -1;
            txtValor.Text = String.Format("{0:N}", totalVenda - valorRestante);
            dtFormas.Rows.Clear();
            txtID.Focus();
        }
    }
}
