﻿namespace GestaoAdministrativa.Vendas.Funcional
{
    partial class frmFuncionalConfirma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFuncionalConfirma));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSubTitulo = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblValor = new System.Windows.Forms.Label();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnAceitar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.dgvProdutosConfirma = new System.Windows.Forms.DataGridView();
            this.CodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescricaoMedicamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecoMaximo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecoVenda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prod_DPorc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qtde_Auto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Retorno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusProd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAvista = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutosConfirma)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(929, 414);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblAvista);
            this.panel1.Controls.Add(this.lblSubTitulo);
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Controls.Add(this.lblValor);
            this.panel1.Controls.Add(this.btnAlterar);
            this.panel1.Controls.Add(this.btnAceitar);
            this.panel1.Controls.Add(this.btnDescartar);
            this.panel1.Controls.Add(this.dgvProdutosConfirma);
            this.panel1.Location = new System.Drawing.Point(5, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(919, 395);
            this.panel1.TabIndex = 1;
            // 
            // lblSubTitulo
            // 
            this.lblSubTitulo.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblSubTitulo.Location = new System.Drawing.Point(3, 39);
            this.lblSubTitulo.Name = "lblSubTitulo";
            this.lblSubTitulo.Size = new System.Drawing.Size(910, 23);
            this.lblSubTitulo.TabIndex = 169;
            this.lblSubTitulo.Text = "Confira atentamente os preços e as quantidades autorizadas antes de confirmar a V" +
    "enda";
            this.lblSubTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitulo
            // 
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblTitulo.Location = new System.Drawing.Point(2, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(911, 37);
            this.lblTitulo.TabIndex = 168;
            this.lblTitulo.Text = "VENDA PRÉ -AUTORIZADA";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValor
            // 
            this.lblValor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.ForeColor = System.Drawing.Color.Black;
            this.lblValor.Location = new System.Drawing.Point(590, 317);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(323, 28);
            this.lblValor.TabIndex = 167;
            this.lblValor.Text = "Valor a debitar do Cartao R$ 00,00";
            this.lblValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ForeColor = System.Drawing.Color.Black;
            this.btnAlterar.Image = ((System.Drawing.Image)(resources.GetObject("btnAlterar.Image")));
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(657, 346);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(256, 42);
            this.btnAlterar.TabIndex = 166;
            this.btnAlterar.Text = "Alterar os dados da Venda (F3)";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            this.btnAlterar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAlterar_KeyDown);
            // 
            // btnAceitar
            // 
            this.btnAceitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceitar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceitar.ForeColor = System.Drawing.Color.Black;
            this.btnAceitar.Image = ((System.Drawing.Image)(resources.GetObject("btnAceitar.Image")));
            this.btnAceitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceitar.Location = new System.Drawing.Point(324, 346);
            this.btnAceitar.Name = "btnAceitar";
            this.btnAceitar.Size = new System.Drawing.Size(256, 42);
            this.btnAceitar.TabIndex = 165;
            this.btnAceitar.Text = "Aceitar e continuar a Venda (F2)";
            this.btnAceitar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAceitar.UseVisualStyleBackColor = true;
            this.btnAceitar.Click += new System.EventHandler(this.btnAceitar_Click);
            this.btnAceitar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAceitar_KeyDown);
            // 
            // btnDescartar
            // 
            this.btnDescartar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDescartar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDescartar.ForeColor = System.Drawing.Color.Black;
            this.btnDescartar.Image = ((System.Drawing.Image)(resources.GetObject("btnDescartar.Image")));
            this.btnDescartar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDescartar.Location = new System.Drawing.Point(3, 346);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(256, 42);
            this.btnDescartar.TabIndex = 164;
            this.btnDescartar.Text = "Descartar e fazer nova Venda (F1)";
            this.btnDescartar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            this.btnDescartar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDescartar_KeyDown);
            // 
            // dgvProdutosConfirma
            // 
            this.dgvProdutosConfirma.AllowUserToAddRows = false;
            this.dgvProdutosConfirma.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProdutosConfirma.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProdutosConfirma.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProdutosConfirma.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProdutosConfirma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProdutosConfirma.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodBarras,
            this.DescricaoMedicamento,
            this.PrecoMaximo,
            this.PrecoVenda,
            this.Prod_DPorc,
            this.Qtde_Auto,
            this.Retorno,
            this.StatusProd,
            this.StatusCode});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProdutosConfirma.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvProdutosConfirma.Location = new System.Drawing.Point(3, 67);
            this.dgvProdutosConfirma.Name = "dgvProdutosConfirma";
            this.dgvProdutosConfirma.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProdutosConfirma.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvProdutosConfirma.RowHeadersVisible = false;
            this.dgvProdutosConfirma.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            this.dgvProdutosConfirma.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvProdutosConfirma.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProdutosConfirma.Size = new System.Drawing.Size(910, 247);
            this.dgvProdutosConfirma.TabIndex = 158;
            this.dgvProdutosConfirma.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvProdutosConfirma_RowPrePaint);
            this.dgvProdutosConfirma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProdutosConfirma_KeyDown);
            // 
            // CodBarras
            // 
            this.CodBarras.DataPropertyName = "CodBarras";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.CodBarras.DefaultCellStyle = dataGridViewCellStyle3;
            this.CodBarras.HeaderText = "Cód de Barras";
            this.CodBarras.Name = "CodBarras";
            this.CodBarras.ReadOnly = true;
            this.CodBarras.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CodBarras.Width = 110;
            // 
            // DescricaoMedicamento
            // 
            this.DescricaoMedicamento.DataPropertyName = "DescricaoMedicamento";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.DescricaoMedicamento.DefaultCellStyle = dataGridViewCellStyle4;
            this.DescricaoMedicamento.HeaderText = "Descrição";
            this.DescricaoMedicamento.MaxInputLength = 50;
            this.DescricaoMedicamento.Name = "DescricaoMedicamento";
            this.DescricaoMedicamento.ReadOnly = true;
            this.DescricaoMedicamento.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DescricaoMedicamento.Width = 150;
            // 
            // PrecoMaximo
            // 
            this.PrecoMaximo.DataPropertyName = "PrecoMaximo";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.PrecoMaximo.DefaultCellStyle = dataGridViewCellStyle5;
            this.PrecoMaximo.HeaderText = "PMC";
            this.PrecoMaximo.Name = "PrecoMaximo";
            this.PrecoMaximo.ReadOnly = true;
            this.PrecoMaximo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PrecoMaximo.Width = 50;
            // 
            // PrecoVenda
            // 
            this.PrecoVenda.DataPropertyName = "PrecoVenda";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.PrecoVenda.DefaultCellStyle = dataGridViewCellStyle6;
            this.PrecoVenda.HeaderText = "Preço Aut.";
            this.PrecoVenda.Name = "PrecoVenda";
            this.PrecoVenda.ReadOnly = true;
            this.PrecoVenda.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PrecoVenda.Width = 70;
            // 
            // Prod_DPorc
            // 
            this.Prod_DPorc.DataPropertyName = "Prod_DPorc";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.Prod_DPorc.DefaultCellStyle = dataGridViewCellStyle7;
            this.Prod_DPorc.HeaderText = "Desc. %";
            this.Prod_DPorc.Name = "Prod_DPorc";
            this.Prod_DPorc.ReadOnly = true;
            this.Prod_DPorc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Prod_DPorc.Width = 70;
            // 
            // Qtde_Auto
            // 
            this.Qtde_Auto.DataPropertyName = "Qtde_Auto";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.Qtde_Auto.DefaultCellStyle = dataGridViewCellStyle8;
            this.Qtde_Auto.HeaderText = "Qtde. Aut.";
            this.Qtde_Auto.Name = "Qtde_Auto";
            this.Qtde_Auto.ReadOnly = true;
            this.Qtde_Auto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Qtde_Auto.Width = 70;
            // 
            // Retorno
            // 
            this.Retorno.DataPropertyName = "Retorno";
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.Retorno.DefaultCellStyle = dataGridViewCellStyle9;
            this.Retorno.HeaderText = "Observacão (Motivo de Reprovacão)";
            this.Retorno.Name = "Retorno";
            this.Retorno.ReadOnly = true;
            this.Retorno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Retorno.Width = 300;
            // 
            // StatusProd
            // 
            this.StatusProd.DataPropertyName = "StatusProd";
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.StatusProd.DefaultCellStyle = dataGridViewCellStyle10;
            this.StatusProd.HeaderText = "Status";
            this.StatusProd.Name = "StatusProd";
            this.StatusProd.ReadOnly = true;
            this.StatusProd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StatusProd.Width = 70;
            // 
            // StatusCode
            // 
            this.StatusCode.DataPropertyName = "StatusCode";
            this.StatusCode.HeaderText = "StatusCode";
            this.StatusCode.Name = "StatusCode";
            this.StatusCode.ReadOnly = true;
            this.StatusCode.Visible = false;
            // 
            // lblAvista
            // 
            this.lblAvista.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvista.ForeColor = System.Drawing.Color.Black;
            this.lblAvista.Location = new System.Drawing.Point(1, 317);
            this.lblAvista.Name = "lblAvista";
            this.lblAvista.Size = new System.Drawing.Size(323, 28);
            this.lblAvista.TabIndex = 170;
            this.lblAvista.Text = "Valor à Vista R$ 00,00";
            this.lblAvista.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmFuncionalConfirma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(936, 416);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFuncionalConfirma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFuncionalConfirma";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFuncionalConfirma_FormClosed);
            this.Shown += new System.EventHandler(this.frmFuncionalConfirma_Shown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdutosConfirma)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnAceitar;
        private System.Windows.Forms.Button btnDescartar;
        public System.Windows.Forms.Label lblSubTitulo;
        public System.Windows.Forms.Label lblTitulo;
        public System.Windows.Forms.DataGridView dgvProdutosConfirma;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescricaoMedicamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecoMaximo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecoVenda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prod_DPorc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qtde_Auto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Retorno;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusProd;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusCode;
        public System.Windows.Forms.Label lblAvista;
    }
}