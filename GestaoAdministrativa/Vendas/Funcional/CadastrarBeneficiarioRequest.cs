﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class CadastrarBeneficiarioRequest
    {
        public CadastrarBeneficiarioRequest(string cpf, string ean, List<CampoCadastral> campoBeneficiario, List<CampoCadastral> paciente, List<CampoCadastral> produto, List<CampoCadastral> extra)
        {
            this.CPF = cpf;
            this.EAN = ean;
            this.CamposBeneficiario = campoBeneficiario;
            this.CamposPaciente = paciente;
            this.CamposProduto = produto;
            this.CamposExtras = extra;
        }

        public string CPF { get; set; }
        public string EAN { get; set; }
        public List<CampoCadastral> CamposBeneficiario { get; set; }
        public List<CampoCadastral> CamposPaciente { get; set; }
        public List<CampoCadastral> CamposProduto { get; set; }
        public List<CampoCadastral> CamposExtras { get; set; }
    }
}
