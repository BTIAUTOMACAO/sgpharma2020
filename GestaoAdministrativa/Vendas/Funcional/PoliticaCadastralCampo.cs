﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class PoliticaCadastralCampo
    {
        public string Campo { get; set; }
        public string NomeExibixao { get; set; }
        public enum Tipo { String, Integer, Decimal, Boolean, DateTime, Select }
        public string Descricao { get; set; }
        public enum Exibicao { Oculto, Opcional, Obrigatorio }
        public enum Entidade { Beneficiario, Paciente, Produto, Extras }
        public string ValorInicial { get; set; }
        public List<PoliticaCadastralCampoOpcoes> Opcoes { get; set; }
    }
}
