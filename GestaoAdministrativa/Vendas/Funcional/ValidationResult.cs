﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class ValidationResult
    {
        public ValidationError[] Errors { get; set; }
        public bool IsValid { get; set; } 
        public string Message { get; set; }
    }
}
