﻿namespace GestaoAdministrativa.Vendas.Funcional
{
    partial class FrmFuncionalVendaAutorizada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFuncionalVendaAutorizada));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAceitar = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtValorDebitarCartao2 = new System.Windows.Forms.Label();
            this.txtReceberAVista2 = new System.Windows.Forms.Label();
            this.txtTotalPagar2 = new System.Windows.Forms.Label();
            this.txtDesconto2 = new System.Windows.Forms.Label();
            this.txtVlTotal2 = new System.Windows.Forms.Label();
            this.txtHora2 = new System.Windows.Forms.Label();
            this.txtData2 = new System.Windows.Forms.Label();
            this.txtCartao2 = new System.Windows.Forms.Label();
            this.txtTotalPagar = new System.Windows.Forms.Label();
            this.txtValorDebitarCartao = new System.Windows.Forms.Label();
            this.txtReceberAVista = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.Label();
            this.txtVlTotal = new System.Windows.Forms.Label();
            this.txtHora = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.Label();
            this.txtCartao = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSequencial = new System.Windows.Forms.Label();
            this.txtAutorizacao = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(5, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(541, 501);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnAceitar);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lblTitulo);
            this.panel1.Location = new System.Drawing.Point(4, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(530, 486);
            this.panel1.TabIndex = 0;
            // 
            // btnAceitar
            // 
            this.btnAceitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceitar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceitar.ForeColor = System.Drawing.Color.Black;
            this.btnAceitar.Image = ((System.Drawing.Image)(resources.GetObject("btnAceitar.Image")));
            this.btnAceitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceitar.Location = new System.Drawing.Point(12, 439);
            this.btnAceitar.Name = "btnAceitar";
            this.btnAceitar.Size = new System.Drawing.Size(515, 42);
            this.btnAceitar.TabIndex = 172;
            this.btnAceitar.Text = "Aceitar e continuar a Venda (F2)";
            this.btnAceitar.UseVisualStyleBackColor = true;
            this.btnAceitar.Click += new System.EventHandler(this.btnAceitar_Click);
            this.btnAceitar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAceitar_KeyDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtValorDebitarCartao2);
            this.panel3.Controls.Add(this.txtReceberAVista2);
            this.panel3.Controls.Add(this.txtTotalPagar2);
            this.panel3.Controls.Add(this.txtDesconto2);
            this.panel3.Controls.Add(this.txtVlTotal2);
            this.panel3.Controls.Add(this.txtHora2);
            this.panel3.Controls.Add(this.txtData2);
            this.panel3.Controls.Add(this.txtCartao2);
            this.panel3.Controls.Add(this.txtTotalPagar);
            this.panel3.Controls.Add(this.txtValorDebitarCartao);
            this.panel3.Controls.Add(this.txtReceberAVista);
            this.panel3.Controls.Add(this.txtDesconto);
            this.panel3.Controls.Add(this.txtVlTotal);
            this.panel3.Controls.Add(this.txtHora);
            this.panel3.Controls.Add(this.txtData);
            this.panel3.Controls.Add(this.txtCartao);
            this.panel3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(12, 157);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(515, 277);
            this.panel3.TabIndex = 171;
            // 
            // txtValorDebitarCartao2
            // 
            this.txtValorDebitarCartao2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorDebitarCartao2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtValorDebitarCartao2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorDebitarCartao2.ForeColor = System.Drawing.Color.Red;
            this.txtValorDebitarCartao2.Location = new System.Drawing.Point(315, 256);
            this.txtValorDebitarCartao2.Name = "txtValorDebitarCartao2";
            this.txtValorDebitarCartao2.Size = new System.Drawing.Size(197, 17);
            this.txtValorDebitarCartao2.TabIndex = 15;
            this.txtValorDebitarCartao2.Text = ".";
            // 
            // txtReceberAVista2
            // 
            this.txtReceberAVista2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceberAVista2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReceberAVista2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceberAVista2.ForeColor = System.Drawing.Color.Red;
            this.txtReceberAVista2.Location = new System.Drawing.Point(315, 222);
            this.txtReceberAVista2.Name = "txtReceberAVista2";
            this.txtReceberAVista2.Size = new System.Drawing.Size(196, 17);
            this.txtReceberAVista2.TabIndex = 14;
            this.txtReceberAVista2.Text = ".";
            // 
            // txtTotalPagar2
            // 
            this.txtTotalPagar2.AutoSize = true;
            this.txtTotalPagar2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPagar2.ForeColor = System.Drawing.Color.Red;
            this.txtTotalPagar2.Location = new System.Drawing.Point(237, 188);
            this.txtTotalPagar2.Name = "txtTotalPagar2";
            this.txtTotalPagar2.Size = new System.Drawing.Size(17, 17);
            this.txtTotalPagar2.TabIndex = 13;
            this.txtTotalPagar2.Text = ".";
            // 
            // txtDesconto2
            // 
            this.txtDesconto2.AutoSize = true;
            this.txtDesconto2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto2.ForeColor = System.Drawing.Color.Red;
            this.txtDesconto2.Location = new System.Drawing.Point(237, 154);
            this.txtDesconto2.Name = "txtDesconto2";
            this.txtDesconto2.Size = new System.Drawing.Size(17, 17);
            this.txtDesconto2.TabIndex = 12;
            this.txtDesconto2.Text = ".";
            // 
            // txtVlTotal2
            // 
            this.txtVlTotal2.AutoSize = true;
            this.txtVlTotal2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlTotal2.ForeColor = System.Drawing.Color.Red;
            this.txtVlTotal2.Location = new System.Drawing.Point(237, 120);
            this.txtVlTotal2.Name = "txtVlTotal2";
            this.txtVlTotal2.Size = new System.Drawing.Size(17, 17);
            this.txtVlTotal2.TabIndex = 11;
            this.txtVlTotal2.Text = ".";
            // 
            // txtHora2
            // 
            this.txtHora2.AutoSize = true;
            this.txtHora2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHora2.ForeColor = System.Drawing.Color.Red;
            this.txtHora2.Location = new System.Drawing.Point(237, 86);
            this.txtHora2.Name = "txtHora2";
            this.txtHora2.Size = new System.Drawing.Size(17, 17);
            this.txtHora2.TabIndex = 10;
            this.txtHora2.Text = ".";
            // 
            // txtData2
            // 
            this.txtData2.AutoSize = true;
            this.txtData2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData2.ForeColor = System.Drawing.Color.Red;
            this.txtData2.Location = new System.Drawing.Point(237, 52);
            this.txtData2.Name = "txtData2";
            this.txtData2.Size = new System.Drawing.Size(17, 17);
            this.txtData2.TabIndex = 9;
            this.txtData2.Text = ".";
            // 
            // txtCartao2
            // 
            this.txtCartao2.AutoSize = true;
            this.txtCartao2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCartao2.ForeColor = System.Drawing.Color.Red;
            this.txtCartao2.Location = new System.Drawing.Point(237, 18);
            this.txtCartao2.Name = "txtCartao2";
            this.txtCartao2.Size = new System.Drawing.Size(17, 17);
            this.txtCartao2.TabIndex = 8;
            this.txtCartao2.Text = ".";
            // 
            // txtTotalPagar
            // 
            this.txtTotalPagar.AutoSize = true;
            this.txtTotalPagar.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPagar.Location = new System.Drawing.Point(15, 188);
            this.txtTotalPagar.Name = "txtTotalPagar";
            this.txtTotalPagar.Size = new System.Drawing.Size(224, 17);
            this.txtTotalPagar.TabIndex = 7;
            this.txtTotalPagar.Text = "Total a Pagar..........:";
            // 
            // txtValorDebitarCartao
            // 
            this.txtValorDebitarCartao.AutoSize = true;
            this.txtValorDebitarCartao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtValorDebitarCartao.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorDebitarCartao.Location = new System.Drawing.Point(16, 256);
            this.txtValorDebitarCartao.Name = "txtValorDebitarCartao";
            this.txtValorDebitarCartao.Size = new System.Drawing.Size(305, 17);
            this.txtValorDebitarCartao.TabIndex = 6;
            this.txtValorDebitarCartao.Text = "Valor Debitar no Cartão/Convênio:";
            // 
            // txtReceberAVista
            // 
            this.txtReceberAVista.AutoSize = true;
            this.txtReceberAVista.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReceberAVista.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceberAVista.Location = new System.Drawing.Point(16, 222);
            this.txtReceberAVista.Name = "txtReceberAVista";
            this.txtReceberAVista.Size = new System.Drawing.Size(305, 17);
            this.txtReceberAVista.TabIndex = 5;
            this.txtReceberAVista.Text = "Valor Receber a Vista...........:";
            // 
            // txtDesconto
            // 
            this.txtDesconto.AutoSize = true;
            this.txtDesconto.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto.Location = new System.Drawing.Point(17, 154);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(224, 17);
            this.txtDesconto.TabIndex = 4;
            this.txtDesconto.Text = "Desconto...............:";
            // 
            // txtVlTotal
            // 
            this.txtVlTotal.AutoSize = true;
            this.txtVlTotal.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlTotal.Location = new System.Drawing.Point(17, 120);
            this.txtVlTotal.Name = "txtVlTotal";
            this.txtVlTotal.Size = new System.Drawing.Size(224, 17);
            this.txtVlTotal.TabIndex = 3;
            this.txtVlTotal.Text = "Valor Total............:";
            // 
            // txtHora
            // 
            this.txtHora.AutoSize = true;
            this.txtHora.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHora.Location = new System.Drawing.Point(16, 86);
            this.txtHora.Name = "txtHora";
            this.txtHora.Size = new System.Drawing.Size(224, 17);
            this.txtHora.TabIndex = 2;
            this.txtHora.Text = "Hora...................:";
            // 
            // txtData
            // 
            this.txtData.AutoSize = true;
            this.txtData.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(16, 52);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(224, 17);
            this.txtData.TabIndex = 1;
            this.txtData.Text = "Data...................:";
            // 
            // txtCartao
            // 
            this.txtCartao.AutoSize = true;
            this.txtCartao.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCartao.Location = new System.Drawing.Point(15, 18);
            this.txtCartao.Name = "txtCartao";
            this.txtCartao.Size = new System.Drawing.Size(224, 17);
            this.txtCartao.TabIndex = 0;
            this.txtCartao.Text = "Cartão.................:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSequencial);
            this.panel2.Controls.Add(this.txtAutorizacao);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(11, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(516, 100);
            this.panel2.TabIndex = 170;
            // 
            // txtSequencial
            // 
            this.txtSequencial.AutoSize = true;
            this.txtSequencial.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSequencial.ForeColor = System.Drawing.Color.Red;
            this.txtSequencial.Location = new System.Drawing.Point(135, 58);
            this.txtSequencial.Name = "txtSequencial";
            this.txtSequencial.Size = new System.Drawing.Size(190, 31);
            this.txtSequencial.TabIndex = 3;
            this.txtSequencial.Text = "99999999999";
            // 
            // txtAutorizacao
            // 
            this.txtAutorizacao.AutoSize = true;
            this.txtAutorizacao.Font = new System.Drawing.Font("Courier New", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAutorizacao.ForeColor = System.Drawing.Color.Red;
            this.txtAutorizacao.Location = new System.Drawing.Point(134, 15);
            this.txtAutorizacao.Name = "txtAutorizacao";
            this.txtAutorizacao.Size = new System.Drawing.Size(190, 31);
            this.txtAutorizacao.TabIndex = 2;
            this.txtAutorizacao.Text = "99999999999";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Autorização......:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "NSU...................:";
            // 
            // lblTitulo
            // 
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblTitulo.Location = new System.Drawing.Point(0, 10);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(529, 37);
            this.lblTitulo.TabIndex = 169;
            this.lblTitulo.Text = "VENDA AUTORIZADA";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmFuncionalVendaAutorizada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(551, 507);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmFuncionalVendaAutorizada";
            this.Text = "FrmFuncionalVendaAutorizada";
            this.Load += new System.EventHandler(this.FrmFuncionalVendaAutorizada_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmFuncionalVendaAutorizada_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label txtTotalPagar;
        private System.Windows.Forms.Label txtValorDebitarCartao;
        private System.Windows.Forms.Label txtReceberAVista;
        private System.Windows.Forms.Label txtDesconto;
        private System.Windows.Forms.Label txtVlTotal;
        private System.Windows.Forms.Label txtHora;
        private System.Windows.Forms.Label txtData;
        private System.Windows.Forms.Label txtCartao;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txtSequencial;
        private System.Windows.Forms.Label txtAutorizacao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnAceitar;
        private System.Windows.Forms.Label txtValorDebitarCartao2;
        private System.Windows.Forms.Label txtReceberAVista2;
        private System.Windows.Forms.Label txtTotalPagar2;
        private System.Windows.Forms.Label txtDesconto2;
        private System.Windows.Forms.Label txtVlTotal2;
        private System.Windows.Forms.Label txtHora2;
        private System.Windows.Forms.Label txtData2;
        private System.Windows.Forms.Label txtCartao2;
    }
}