﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class AvaliacaoElegibilidade
    {
        public bool ExisteProdutoPrograma { get; set; } 
        public string DescricaoPrograma { get; set; } 
        public string DescricaoProduto { get; set; } 
        public Boolean RequisitarCadastroPrograma { get; set; } 
        public Boolean RequisitarCadastroProduto { get; set; } 
    }
}
