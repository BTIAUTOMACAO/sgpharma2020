﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestaoAdministrativa.Negocio.Funcional;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using GestaoAdministrativa.Negocio;
using System.Diagnostics;
using System.IO;
using GestaoAdministrativa.Geral;
using Newtonsoft.Json;
using System.Net;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public partial class FrmFuncionalConsulta : Form
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        frmVenda fVendas;
        ClienteCartaoFuncional objClienteCartao;
        private DataTable dadosCliente;
        public bool gravar;
        AutenticarResponse autentication;
        AvaliacaoElegibilidadeResponse avaliacaoResponse;
        #endregion

        public FrmFuncionalConsulta(frmVenda form)
        {
            InitializeComponent();
            fVendas = form;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbTipoVenda.SelectedIndex == 0)
                {
                    dtDataReceita.Enabled = true;
                    cbbConselho.Enabled = true;
                    cbbTipoVenda.Enabled = true;
                    cbbUF.Enabled = true;
                    txtCodConselho.Enabled = true;
                }
                else
                {
                    cbbConselho.ResetText();
                    cbbUF.ResetText();
                    dtDataReceita.Enabled = false;
                    cbbConselho.Enabled = false;
                    cbbUF.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Funcional", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            fVendas.txtCliEmp.Text = objClienteCartao.NomeDoPortador;
            fVendas.btnPreAutorizar.Visible = true;
            this.Close();

        }

        private void FrmFuncionalConsulta_Load(object sender, EventArgs e)
        {
            Principal.crm = "";
            cbbTipoVenda.SelectedIndex = 1;
            dtDataReceita.Value = DateTime.Now.Date;
        }

        private void btnFechar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGravar1_Click(object sender, EventArgs e)
        {
            FuncionalConsultaCartaoNegocio objFuncionalConsultaCartao = new FuncionalConsultaCartaoNegocio();

            try
            {
                if (ConsisteCampos())
                {
                    #region Carregar Arquivos 
                    LimparPastas();

                    #region Geração e envio do arquivo .ENV
                    int numeroSequencialDoArquivo = objFuncionalConsultaCartao.GeraSequenciaArquivoAutorizadorSeq();

                    objFuncionalConsultaCartao.GeraTxtDeEnvio(txtCartao.Text, numeroSequencialDoArquivo);

                    #endregion

                    #region Valida arquivo de status
                    objFuncionalConsultaCartao.ValidarArquivoStatus(numeroSequencialDoArquivo);
                    bool validouArquivoDeStatus = Principal.ValidouArquivoDeStatus;
                    if (validouArquivoDeStatus)
                    {
                        if (!objFuncionalConsultaCartao.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                        {
                            MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT está ativo no canto inferior direito." + "\n" + "Tente novamente ou entre em contato com o suporte");
                        return;
                    }
                    #endregion

                    #region Valida arquivo de retorno
                    objFuncionalConsultaCartao.ValidarArquivoDeResposta(numeroSequencialDoArquivo);
                    bool validouArquivoDeResposta = Principal.ValidouArquivoDeResposta;
                    if (!validouArquivoDeResposta)
                    {
                        MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                        return;
                    }
                    #endregion

                    try
                    {
                        //Valida Consistencia de arquivos 
                        objClienteCartao = objFuncionalConsultaCartao.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo);

                        //Carrega Objeto ClienteCartao em uma variável global do fVendas
                        fVendas.objTransacaoClienteCartaoFuncional = objClienteCartao;
                        //if (objClienteCartao.CodigoDaResposta == "00")
                        //{
                        fVendas.objTransacaoClienteCartaoFuncional.CodigoDaResposta = "00"/*objClienteCartao.CodigoDaResposta*/; //Se chegou até aqui é porque encontrou o cadastro
                        fVendas.objTransacaoClienteCartaoFuncional.MapaDeAcoes = objClienteCartao.MapaDeAcoes;
                        fVendas.objTransacaoClienteCartaoFuncional.TipoDaVenda = (cbbTipoVenda.SelectedIndex == 0 ? Enums.TipoVenda.ComReceita : Enums.TipoVenda.SemReceita);
                        if (cbbTipoVenda.SelectedIndex == 0)
                        {
                            fVendas.objTransacaoClienteCartaoFuncional.CodConselho = txtCodConselho.Text;
                            fVendas.objTransacaoClienteCartaoFuncional.ConselhoDaVenda = (cbbConselho.SelectedIndex == 0 ? Enums.Conselho.CRM : Enums.Conselho.CRO);
                            fVendas.objTransacaoClienteCartaoFuncional.UF = cbbUF.Text;
                            fVendas.objTransacaoClienteCartaoFuncional.DataDaTransacao = Convert.ToDateTime(dtDataReceita.Text).ToString("ddMMyyyy");
                        }
                        else
                        {
                            fVendas.objTransacaoClienteCartaoFuncional.DataDaTransacao = DateTime.Now.ToString("ddMMyyyy");
                            //todo Após desenvolvimento remover pois os parametros abaixo é apenas para teste
                            fVendas.objTransacaoClienteCartaoFuncional.UF = "SP";
                            fVendas.objTransacaoClienteCartaoFuncional.ConselhoDaVenda = Enums.Conselho.CRM;
                            fVendas.objTransacaoClienteCartaoFuncional.CodConselho = "123756";
                        }
                        fVendas.objTransacaoClienteCartaoFuncional.Trilha2DoCartao = txtCartao.Text.Trim();
                        fVendas.vendaBeneficio = "FUNCIONAL";
                        fVendas.btnPreAutorizar.Visible = true;

                        //if (objClienteCartao != null && Principal.ValidouConsistenciaArquivos)
                        //{
                        //    txtNome.Text = objClienteCartao.NomeDoPortador;
                        //    txtObs.Text = objClienteCartao.MensagemSobreCliente;
                        //}
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Erro: " + objClienteCartao.TextoParaOperador, "Funcional", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //}

                        #region Incrementa arquivo sequencial AUTORIZADOR.SEQ
                        objFuncionalConsultaCartao.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                        #endregion

                        var cliente = new Cliente();
                        dadosCliente = cliente.SelecionaDadosDoClientePorConveniadaFuncional(txtCartao.Text);
                        btnGravar1.Enabled = true;
                        cbbTipoVenda.Focus();
                    }
                    catch
                    {
                        MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                        return;
                    }
                    finally
                    {
                        Cursor.Current = Cursors.Default;
                    }
                    #endregion

                    if (Gravar())
                    {
                        gravar = true;
                        fVendas.wsCRM = txtCodConselho.Text;
                        Principal.wsMNome = objClienteCartao.NomeDoPortador;
                        fVendas.txtCliEmp.Text = objClienteCartao.NomeDoPortador;
                        fVendas.btnPreAutorizar.Visible = true;
                        fVendas.comReceitaFuncional = cbbTipoVenda.SelectedIndex == 0 ? "S" : "N";

                        this.Close();
                    }
                    else
                    {
                        gravar = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Funcional", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtNome.Text))
            {
                Principal.mensagem = "Venda não Liberada!";
                Funcoes.Avisa();
                txtCartao.Focus();
                return false;
            }
            else if (cbbTipoVenda.SelectedIndex == 0)
            {
                if (cbbConselho.SelectedIndex == -1)
                {
                    Principal.mensagem = "Necessário selecionar o Conselho do Médico!";
                    Funcoes.Avisa();
                    cbbConselho.Focus();
                    return false;
                }

                if (String.IsNullOrEmpty(txtCodConselho.Text))
                {
                    Principal.mensagem = "CRM não pode ser em branco.";
                    Funcoes.Avisa();
                    txtCodConselho.Focus();
                    return false;
                }

                if (cbbUF.SelectedIndex == -1)
                {
                    Principal.mensagem = "Necessário selecionar o Estado!";
                    Funcoes.Avisa();
                    cbbUF.Focus();
                    return false;
                }
            }

            return true;

        }

        public void LimparPastas()
        {
            string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "05", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "05", false), "*.log", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "05", false), "*.env", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "06", false), "*.txt", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }

            arquivos = Directory.GetFiles(Funcoes.LeParametro(14, "06", false), "*.rsp", SearchOption.AllDirectories);
            foreach (string arq in arquivos)
            {
                File.Delete(arq);
            }
        }

        public void GenerateApiKey()
        {
            //Envio de Requisição para conseguir a APIKEY Via REST
            try
            {
                string webAddr = "http://acessohml.funcionalmais.com/AcessoUnicoExterno/REST/api/Authentication";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Headers.Add("Origin", "5");
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //Paramentros de teste fornecidos pela FuncionalCorp. solicitar o cadastro. 
                    string json = "{\"Login\":\"" + Funcoes.LeParametro(14, "49", false) + "\"," +
                              "\"Password\":\"" + Funcoes.LeParametro(14, "50", false) + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    dynamic response = JsonConvert.DeserializeObject(responseText);
                    ValidationResult resultado = response.ResultadoValidacao.ToObject<ValidationResult>();

                    if (resultado.IsValid)
                    {
                        ApiKey apikey = response.ApiKey.ToObject<ApiKey>();
                        autentication = new AutenticarResponse(resultado, apikey);
                    }
                    else
                    {
                        MessageBox.Show("Conexão Invalida, Contate o suporte!", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                    }
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show("Erro " + ex.Message);
                this.Close();
            }
        }

        private void txtCartao_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                FuncionalConsultaCartaoNegocio objFuncionalConsultaCartao = new FuncionalConsultaCartaoNegocio();
                string numeroCartao = "";

                if (e.KeyChar == 13)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    if (!(string.IsNullOrEmpty(txtCartao.Text)))
                    {
                        #region Metodo TVAT
                        /*
                        //DELETA ARQUIVOS

                        LimparPastas();

                        #region Geração e envio do arquivo .ENV
                        int numeroSequencialDoArquivo = objFuncionalConsultaCartao.GeraSequenciaArquivoAutorizadorSeq();

                        objFuncionalConsultaCartao.GeraTxtDeEnvio(txtCartao.Text, numeroSequencialDoArquivo);

                        #endregion

                        #region Valida arquivo de status
                        objFuncionalConsultaCartao.ValidarArquivoStatus(numeroSequencialDoArquivo);
                        bool validouArquivoDeStatus = Principal.ValidouArquivoDeStatus;
                        if (validouArquivoDeStatus)
                        {
                            if (!objFuncionalConsultaCartao.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                            {
                                MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                                return;
                            }

                        }
                        else
                        {
                            MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT está ativo no canto inferior direito." + "\n" + "Tente novamente ou entre em contato com o suporte");
                            return;
                        }
                        #endregion

                        #region Valida arquivo de retorno
                        objFuncionalConsultaCartao.ValidarArquivoDeResposta(numeroSequencialDoArquivo);
                        bool validouArquivoDeResposta = Principal.ValidouArquivoDeResposta;
                        if (!validouArquivoDeResposta)
                        {
                            MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            return;
                        }
                        #endregion

                        try
                        {
                            //Valida Consistencia de arquivos 
                            objClienteCartao = objFuncionalConsultaCartao.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo);

                            //Carrega Objeto ClienteCartao em uma variável global do fVendas
                            fVendas.objTransacaoClienteCartaoFuncional = objClienteCartao;
                            if (objClienteCartao.CodigoDaResposta == "00")
                            {
                                fVendas.objTransacaoClienteCartaoFuncional.CodigoDaResposta = objClienteCartao.CodigoDaResposta;
                                fVendas.objTransacaoClienteCartaoFuncional.MapaDeAcoes = objClienteCartao.MapaDeAcoes;
                                fVendas.objTransacaoClienteCartaoFuncional.TipoDaVenda = (cbbTipoVenda.SelectedIndex == 0 ? Enums.TipoVenda.ComReceita : Enums.TipoVenda.SemReceita);
                                if (cbbTipoVenda.SelectedIndex == 0)
                                {
                                    fVendas.objTransacaoClienteCartaoFuncional.CodConselho = txtCodConselho.Text;
                                    fVendas.objTransacaoClienteCartaoFuncional.ConselhoDaVenda = (cbbConselho.SelectedIndex == 0 ? Enums.Conselho.CRM : Enums.Conselho.CRO);
                                    fVendas.objTransacaoClienteCartaoFuncional.UF = cbbUF.Text;
                                    fVendas.objTransacaoClienteCartaoFuncional.DataDaTransacao = Convert.ToDateTime(dtDataReceita.Text).ToString("ddMMyyyy");
                                }
                                else
                                {
                                    fVendas.objTransacaoClienteCartaoFuncional.DataDaTransacao = DateTime.Now.ToString("ddMMyyyy");
                                    //todo Após desenvolvimento remover pois os parametros abaixo é apenas para teste
                                    fVendas.objTransacaoClienteCartaoFuncional.UF = "SP";
                                    fVendas.objTransacaoClienteCartaoFuncional.ConselhoDaVenda = Enums.Conselho.CRM;
                                    fVendas.objTransacaoClienteCartaoFuncional.CodConselho = "123756";
                                }
                                fVendas.objTransacaoClienteCartaoFuncional.Trilha2DoCartao = txtCartao.Text.Trim();
                                fVendas.vendaBeneficio = "FUNCIONAL";
                                fVendas.btnPreAutorizar.Visible = true;

                                if (objClienteCartao != null && Principal.ValidouConsistenciaArquivos)
                                {
                                    txtNome.Text = objClienteCartao.NomeDoPortador;
                                    txtObs.Text = objClienteCartao.MensagemSobreCliente;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Erro: " + objClienteCartao.TextoParaOperador, "Funcional", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            
                            #region Incrementa arquivo sequencial AUTORIZADOR.SEQ
                            objFuncionalConsultaCartao.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                            #endregion

                            var cliente = new Cliente();
                            dadosCliente = cliente.SelecionaDadosDoClientePorConveniadaFuncional(txtCartao.Text);
                            btnGravar1.Enabled = true;
                            cbbTipoVenda.Focus();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            return;
                        }
                        finally
                        {
                            Cursor.Current = Cursors.Default;
                        }*/
                        #endregion

                        #region Metodo REST
                        try
                        {
                            GenerateApiKey();
                            string webAddr = "http://acessohml.funcionalmais.com/AutorizadorOmniChannelExterno/REST/api/Beneficiario/" + txtCartao.Text;
                            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                            httpWebRequest.Headers.Add("Origem", "5");
                            httpWebRequest.Headers.Add("ApiKey", autentication.apikey.Key);
                            httpWebRequest.Method = "GET";

                            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                            {
                                var responseText = streamReader.ReadToEnd();
                                dynamic response = JsonConvert.DeserializeObject(responseText); //Transformando o json de retorno em objeto do tipo Dinamico
                                ConsultaCartaoResponse resultadoAvaliar = response.ToObject<ConsultaCartaoResponse>(); //Pega somente a parte ResultadoValidacao e atribui no objeto do tipo ValidationResult 
                                if (resultadoAvaliar.IsValid)
                                {
                                    txtNome.Text = resultadoAvaliar.Nome;
                                    string descricao = "";
                                    int tamanho = resultadoAvaliar.Programas.Count;
                                    for (int i = 0; i < tamanho; i++)
                                    {
                                        descricao += resultadoAvaliar.Programas[i].Nome;
                                        descricao += resultadoAvaliar.Programas[i].MensagemVenda.Replace("</p>", "").Replace("<p>", "").RemoverAcentuacao();
                                    }
                                    txtObs.Text = descricao;

                                    btnGravar1.Enabled = true;
                                    cbbTipoVenda.Focus();

                                    //var cliente = new Cliente(); 
                                    //int numeroSequencialDoArquivo = objFuncionalConsultaCartao.GeraSequenciaArquivoAutorizadorSeq(); 
                                    //dadosCliente = cliente.SelecionaDadosDoClientePorConveniadaFuncional(txtCartao.Text); 
                                    //objClienteCartao = objFuncionalConsultaCartao.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo);  
                                    //objClienteCartao = new ClienteCartaoFuncional(); 
                                    //objClienteCartao.NomeDoPortador = resultadoAvaliar.Nome; 

                                }
                                else
                                {
                                    if (resultadoAvaliar.Errors.Length == 1)
                                    {
                                        MessageBox.Show("Cartão Invalido " + resultadoAvaliar.Errors[0].Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                    else
                                    {
                                        string erro = "";
                                        for (int j = 0; j < resultadoAvaliar.Errors.Length; j++)
                                        {
                                            erro += resultadoAvaliar.Errors[j].Message + " \n";
                                        }
                                        MessageBox.Show("Cartão Invalido " + erro, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                    txtCartao.Text = "";
                                    txtNome.Text = "";
                                    txtObs.Text = "";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Ocorreu uma falha durante a consulta " + ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtCartao.Text = "";
                            txtNome.Text = "";
                            txtObs.Text = "";
                        }
                        #endregion
                    }
                    else
                    {
                        MessageBox.Show("Campo cartão é requerido!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCartao.Focus();
                        return;
                    }

                    Cursor.Current = Cursors.Default; 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Funcional", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ExecutaFuncional()
        {
            if (System.Diagnostics.Process.GetProcessesByName("tvat").Length.Equals(0))
            {
                var startInfo = new ProcessStartInfo(Funcoes.LeParametro(14, "48", true));
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                if (System.Diagnostics.Process.GetProcessesByName("tvat").Length.Equals(0))
                {
                    MessageBox.Show("Atenção, execute o TVAT para iniciar a venda", "Funcional", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        private void btnCancela_Click(object sender, EventArgs e)
        {
            try
            {
                if (!gravar)
                {
                    Limpar();
                }
                fVendas.vendaBeneficio = "N";
                fVendas.objTransacaoClienteCartaoFuncional.FecharForm = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Funcional", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Gravar()
        {
            try
            {
                string cpf = "";
                if (dadosCliente.Rows.Count == 0)
                {
                    #region INSERE CLIENTE NA TABELA 
                    //VERIFICA SE CLIENTE JÁ ESTA CADASTRADO//

                    //INCLUI NOVO REGISTRO NA TABELA CLIENTES//
                    cpf = Util.GerarCpf();
                    var cliente = new Cliente
                    {
                        CfDocto = cpf,
                        CfTipoDocto = 0,
                        CfNome = txtNome.Text,
                        CfEndereco = ".",
                        CfNumeroEndereco = "00",
                        CfBairro = ".",
                        CfCEP = "00000000",
                        CfUF = ".",
                        CfCidade = ".",
                        CfStatus = 4,
                        CfEnderecoCobranca = ".",
                        CfNumeroCobranca = "00",
                        CfBairroCobranca = ".",
                        CfCEPCobranca = "00000000",
                        CfCidadeCobranca = ".",
                        CfUFCobranca = ".",
                        CfEnderecoEntrega = ".",
                        CfNumeroEntrega = "0",
                        CfBairroEntrega = ".",
                        CfCEPEntrega = "0",
                        CfCidadeEntrega = ".",
                        CfUFEntrega = ".",
                        CfPais = "BRASIL",
                        CfLiberado = "N",
                        CodigoConveniada = 7000,
                        NumCartao = txtCartao.Text,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };
                    if (!cliente.IncluiClienteConsultaCatrtao(cliente, out fVendas.IDCliente))
                        return false;
                    #endregion
                }
                else
                {
                    cpf = dadosCliente.Rows[0]["CF_DOCTO"].ToString();
                }

                Principal.crm = txtCodConselho.Text;
                Principal.doctoCliente = cpf;
                fVendas.DadosDaVendaCliente(Principal.doctoCliente);
                fVendas.checaMedComSemReceita = false;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Funcional", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Limpar()
        {

            fVendas.txtCodCliEmp.Enabled = true;
            fVendas.txtCliEmp.Enabled = true;
            fVendas.txtCliEmp.Visible = false;
            fVendas.txtCodCliEmp.Text = "";
            fVendas.txtCliEmp.Text = "";
            fVendas.btnPreAutorizar.Visible = false;
            fVendas.txtComanda.Focus();
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Limpar();
                    fVendas.vendaBeneficio = "N";
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancela.PerformClick();
                    break;
                case Keys.F2:
                    btnGravar1.PerformClick();
                    break;
            }

        }

        private void FrmFuncionalConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cbbTipoVenda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbTipoVenda.SelectedIndex == 0)
            {
                dtDataReceita.Enabled = true;
                cbbConselho.Enabled = true;
                cbbTipoVenda.Enabled = true;
                cbbUF.Enabled = true;
                txtCodConselho.Enabled = true;
            }
            else
            {
                cbbConselho.ResetText();
                cbbUF.ResetText();
                dtDataReceita.Enabled = false;
                cbbConselho.Enabled = false;
                cbbUF.Enabled = false;

            }
        }

        private void txtCartao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cbbTipoVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (dtDataReceita.Enabled.Equals(true))
                {
                    dtDataReceita.Focus();
                }
                else
                {
                    btnGravar1.Focus();
                }
            }
        }

        private void dtDataReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cbbConselho.Focus();

            }
        }

        private void cbbConselho_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCodConselho.Focus();

            }
        }

        private void txtCodConselho_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)13)
            {
                var buscaMedico = new Medico();

                DataTable medico = buscaMedico.BuscaDados(txtCodConselho.Text, "");
                if (medico.Rows.Count == 0)
                {
                    cbbUF.Focus();
                }
                else if (medico.Rows.Count == 1)
                {
                    txtCodConselho.Text = medico.Rows[0][0].ToString();
                    if (String.IsNullOrEmpty(medico.Rows[0][3].ToString()))
                    {
                        cbbUF.Focus();
                    }
                    else
                    {
                        cbbUF.Text = medico.Rows[0][3].ToString();
                        btnGravar1.Focus();
                    }
                }
                else
                {
                    using (frmBuscaMedico medicos = new frmBuscaMedico())
                    {
                        medicos.dgMedicos.DataSource = medico;
                        medicos.ShowDialog();
                    }
                    cbbUF.Text = Principal.uf;
                    txtCodConselho.Text = Principal.crm;
                    btnGravar1.Focus();
                }
            }
        }

        private void cbbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnGravar1.PerformClick();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var cadastro = new frmCadastroFuncional(this);
            cadastro.Show(); 
        }
    }
}
