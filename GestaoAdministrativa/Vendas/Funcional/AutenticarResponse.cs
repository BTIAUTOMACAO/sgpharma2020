﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class AutenticarResponse
    {
        public ValidationResult ResultadoValidacao { get; set; } 
        public ApiKey apikey { get; set; }

        public AutenticarResponse(ValidationResult resultado, ApiKey apikey) {
            this.ResultadoValidacao = resultado;
            this.apikey = apikey; 
        }
    }
}
