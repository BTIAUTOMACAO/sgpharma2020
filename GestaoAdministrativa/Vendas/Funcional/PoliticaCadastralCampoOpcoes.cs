﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class PoliticaCadastralCampoOpcoes
    {
        public string Texto { get; set; }
        public string Valor { get; set; }

    }
}
