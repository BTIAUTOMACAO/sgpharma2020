﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using GestaoAdministrativa.Negocio.Funcional;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public partial class FrmFuncionalVendaAutorizada : Form
    {
        frmFuncionalConfirma frmFuncionalConfirma;
        frmVenda fVenda;
        VendaAutorizadaRequest vendaAutorizadaRequest;
        public FrmFuncionalVendaAutorizada()
        {
            InitializeComponent();
        }

        public FrmFuncionalVendaAutorizada(frmFuncionalConfirma frmFuncionalConfirma, frmVenda frmVenda, VendaAutorizadaRequest vAutorizadaRequest)
        {
            this.frmFuncionalConfirma = frmFuncionalConfirma;
            this.fVenda = frmVenda;
            this.vendaAutorizadaRequest = vAutorizadaRequest;
            InitializeComponent();
        }

        private void FrmFuncionalVendaAutorizada_Load(object sender, EventArgs e)
        {
            PreencheCampos();
        }

        private void PreencheCampos()
        {
            Double aReceber = 0;
            Double totalBeneficio = 0;

            aReceber = (Convert.ToDouble(frmFuncionalConfirma.confirmaVendaResponse.ValorPagarAVista)) / 100;
            totalBeneficio = (Convert.ToDouble(frmFuncionalConfirma.confirmaVendaResponse.ValorDaOperacao)) / 100;
            
            if (fVenda.StatusCaixa())
            {
                vendaAutorizadaRequest.TotalAPagar = totalBeneficio + aReceber;
                vendaAutorizadaRequest.ValorTotal = totalBeneficio + aReceber + vendaAutorizadaRequest.Desconto;

                if (aReceber == 0 && totalBeneficio != 0)
                {
                    fVenda.codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "46", true));
                    fVenda.pagamentoTotalOuParcial = "T";
                    fVenda.valorAutorizado = totalBeneficio;
                }
                else if (totalBeneficio.Equals(0))
                {
                    fVenda.pagamentoTotalOuParcial = "";
                    fVenda.diferencaDoValorAutorizado = aReceber;
                }
                else
                {
                    fVenda.codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "46", true));
                    fVenda.diferencaDoValorAutorizado = aReceber;
                    fVenda.pagamentoTotalOuParcial = "P";
                    fVenda.valorAutorizado = totalBeneficio;
                }

                vendaAutorizadaRequest.Nome = Principal.wsMNome;
                txtReceberAVista2.Text = " " + String.Format("{0:N}", aReceber);
                txtValorDebitarCartao2.Text = " " + String.Format("{0:N}", totalBeneficio);
                vendaAutorizadaRequest.ValorAReceberNoCartao = totalBeneficio;
                vendaAutorizadaRequest.ValorAReceberAVista = aReceber;

            }
            else
            {
                string tipoTransacao = "";
                if (fVenda.chkEntrega.Checked)
                {
                    if (!totalBeneficio.Equals(0))
                    {
                        fVenda.codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "46", true));
                    }
                    else
                        fVenda.codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(6, "356", true));
                }
                else
                {
                    DataTable dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                    if (dtPesq.Rows.Count > 0)
                    {
                        fVenda.codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                    }
                }

                vendaAutorizadaRequest.TotalAPagar = totalBeneficio + aReceber;
                vendaAutorizadaRequest.ValorTotal = totalBeneficio + aReceber + vendaAutorizadaRequest.Desconto;

                if (aReceber == 0 && totalBeneficio != 0)
                {
                    tipoTransacao = "T";
                    fVenda.valorAutorizado = totalBeneficio;
                   
                }
                else if (totalBeneficio.Equals(0))
                {
                    tipoTransacao = "V";
                    fVenda.diferencaDoValorAutorizado = 0;
                }
                else
                {
                    tipoTransacao = "P";
                    fVenda.diferencaDoValorAutorizado = aReceber;
                    fVenda.pagamentoTotalOuParcial = "P";
                    fVenda.valorAutorizado = totalBeneficio;
                }

                vendaAutorizadaRequest.Nome = Principal.wsMNome;
                txtReceberAVista2.Text = " " + String.Format("{0:N}", aReceber);
                txtValorDebitarCartao2.Text = " " + String.Format("{0:N}", totalBeneficio);
                vendaAutorizadaRequest.ValorAReceberNoCartao = totalBeneficio;
                vendaAutorizadaRequest.ValorAReceberAVista = aReceber;

                string aux = frmFuncionalConfirma.confirmaVendaResponse.DataTransacao;
                DateTime data = DateTime.ParseExact(aux, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
                string conselho = "";
                
                conselho = fVenda.objTransacaoClienteCartaoFuncional.ConselhoDaVenda.Equals(Enums.Conselho.CRM) ? "CRM" : "CRO";

                FuncionalComanda funcionalComanda = new FuncionalComanda(frmFuncionalConfirma.confirmaVendaResponse.NSU
                    , frmFuncionalConfirma.frmVenda.objTransacaoClienteCartaoFuncional.Trilha2DoCartao
                    , data
                    , vendaAutorizadaRequest.TotalAPagar
                    , vendaAutorizadaRequest.ValorAReceberNoCartao
                    , vendaAutorizadaRequest.ValorAReceberAVista
                    , vendaAutorizadaRequest.Desconto
                    , tipoTransacao
                    , frmFuncionalConfirma.confirmaVendaResponse.NSUDoAutorizador
                    , frmFuncionalConfirma.confirmaVendaResponse.HoraTransacao
                    , vendaAutorizadaRequest.TipoConselhoDaVenda
                    , Principal.crm
                    , vendaAutorizadaRequest.ValorTotal
                    , vendaAutorizadaRequest.Nome
                    );
                funcionalComanda.InsereRegistros(funcionalComanda);

            }

            txtSequencial.Text = "";
            txtSequencial.Text = txtSequencial.Text + " " + frmFuncionalConfirma.confirmaVendaResponse.NSUDoAutorizador.PadLeft(12, '0');
            txtAutorizacao.Text = "";
            txtAutorizacao.Text = txtAutorizacao.Text + " " + frmFuncionalConfirma.confirmaVendaResponse.NSU.PadLeft(12, '0');
            txtCartao2.Text = " " + frmFuncionalConfirma.frmVenda.objTransacaoClienteCartaoFuncional.Trilha2DoCartao;
            string a = frmFuncionalConfirma.confirmaVendaResponse.DataTransacao;
            string dataTransacao = DateTime.ParseExact(a, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            txtData2.Text = " " + dataTransacao;
            txtHora2.Text = " " + DateTime.Now.ToString("HH:mm:ss tt");
            txtVlTotal2.Text = " " + String.Format("{0:N}", vendaAutorizadaRequest.ValorTotal);
            txtDesconto2.Text = " " + String.Format("{0:N}",vendaAutorizadaRequest.Desconto);
            txtTotalPagar2.Text = " " + String.Format("{0:N}", vendaAutorizadaRequest.TotalAPagar);
            
        }

        private void btnAceitar_Click(object sender, EventArgs e)
        {
            fVenda.vendaAutorizadaResponse = new VendaAutoriadaResponse(vendaAutorizadaRequest);
            fVenda.btnPreAutorizar.Visible = false;
            this.Hide();
            this.Close();
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {

                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
            }
        }

        private void FrmFuncionalVendaAutorizada_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnAceitar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }
    }
}
