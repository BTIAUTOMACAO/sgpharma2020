﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class ProgramasParticipantes
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public enum Tipo { PBM, ACESSO }
        public string MensagemVenda { get; set; }
    }
}
