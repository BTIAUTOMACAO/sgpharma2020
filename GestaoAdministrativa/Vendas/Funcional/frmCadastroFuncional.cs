﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Newtonsoft.Json;


namespace GestaoAdministrativa.Vendas.Funcional
{
    public partial class frmCadastroFuncional : Form
    {
        AutenticarResponse autentication;
        AvaliacaoElegibilidadeResponse avaliacaoResponse;
        private FrmFuncionalConsulta frmFuncionalConsulta;

        public frmCadastroFuncional(FrmFuncionalConsulta frmFuncionalConsulta)
        {
            InitializeComponent();
            this.frmFuncionalConsulta = frmFuncionalConsulta;
        }

        private void cpfText_Validated_1(object sender, EventArgs e)
        {
            if (cpfText.Text.Replace("/", "").Replace(".", "").Replace(",", "").Length <= 11)
                cpfText.Mask = "000.000.000/00";
            else
                cpfText.Mask = "00.000.000/0000-00";
        }

        private void cpfText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                eanText.Focus(); 
        }

        private void btnVerifica_Click(object sender, EventArgs e)
        {
            //Verificação de preenchimento dos campos de CPF e EAN 
            if (!String.IsNullOrEmpty(cpfText.Text.Replace(",", "").Replace("/", "")) && !String.IsNullOrEmpty(eanText.Text) && cpfText.Text.Replace(",", "").Replace("/", "").Length >= 11)
            {
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    string webAddr = "http://acessohml.funcionalmais.com/AcessoUnicoExterno/REST/api/CadastroUnico/AvaliarElegibilidade?CPF=" + cpfText.Text.Replace(",", "").Replace("/", "") + "&EAN=" + eanText.Text + "";

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                    httpWebRequest.Headers.Add("Origem", "5");
                    httpWebRequest.Headers.Add("ApiKey", autentication.apikey.Key);
                    httpWebRequest.Method = "GET";

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var responseText = streamReader.ReadToEnd();
                        dynamic response = JsonConvert.DeserializeObject(responseText); //Transformando o json de retorno em objeto do tipo Dinamico
                        ValidationResult resultadoAvaliar = response.ResultadoValidacao.ToObject<ValidationResult>(); //Pega somente a parte ResultadoValidacao e atribui no objeto do tipo ValidationResult
                        if (resultadoAvaliar.IsValid)
                        {
                            AvaliacaoElegibilidade avaliacaoElegibilidade = response.AvaliacaoElegibilidade.ToObject<AvaliacaoElegibilidade>();
                            PoliticaCadastral politicaCadastral = response.PoliticaCadastral.ToObject<PoliticaCadastral>();
                            List<Paciente> Pacientes = response.Pacientes.ToObject<Paciente>();

                            avaliacaoResponse = new AvaliacaoElegibilidadeResponse(resultadoAvaliar, avaliacaoElegibilidade, politicaCadastral, Pacientes);
                        }
                        else
                        {
                            MessageBox.Show("Conexão Invalida, entre em contato com o suporte", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            this.Close();
                        }
                    }
                    if (avaliacaoResponse.AvaliacaoElegibidade.ExisteProdutoPrograma)
                    {
                        if (avaliacaoResponse.AvaliacaoElegibidade.RequisitarCadastroPrograma) //Verificação se é preciso cadastrar na Funcional
                        {
                            //Aqui fica onde vai ser a população dos combobox e visible enable
                            AtivarCampos();
                            txtNome.Focus(); 
                            for (int i = 0; i < avaliacaoResponse.politicaCadastral.CamposBeneficiario[9].Opcoes.Count; i++)
                                cbEstado.Items.Add(avaliacaoResponse.politicaCadastral.CamposBeneficiario[9].Opcoes[i].Texto);

                            for (int i = 0; i < avaliacaoResponse.politicaCadastral.CamposBeneficiario[2].Opcoes.Count; i++)
                                cbSexo.Items.Add(avaliacaoResponse.politicaCadastral.CamposBeneficiario[2].Opcoes[i].Texto);
                        }
                        else
                        {
                            MessageBox.Show("Cliente já cadastrado no programa", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cpfText.Text = "";
                            cpfText.Mask = "";
                            btnFechar.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Produto não existe no programa", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        eanText.Text = "";
                        cpfText.Mask = "";
                        btnFechar.Focus();
                    }
                }
                catch (WebException ex)
                {
                    MessageBox.Show("Erro " + ex.Message);
                    this.Close();
                }
                finally { Cursor.Current = Cursors.Default; }
            }
            else
            {
                if (String.IsNullOrEmpty(cpfText.Text))
                    cpfText.Focus();
                else
                    eanText.Focus();

                MessageBox.Show("Preencha Todos os Campos Corretamente", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cpfText.Mask = "";
            }
        }

        private void frmCadastroFuncional_Load(object sender, EventArgs e)
        {
            //Envio de Requisição para conseguir a APIKEY Via REST
            try
            {
                string webAddr = "http://acessohml.funcionalmais.com/AcessoUnicoExterno/REST/api/Authentication";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Headers.Add("Origin", "5");
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //Paramentros de teste fornecidos pela FuncionalCorp. solicitar o cadastro. 
                    string json = "{\"Login\":\"" + Funcoes.LeParametro(14, "49", false) + "\"," +
                              "\"Password\":\"" + Funcoes.LeParametro(14, "50", false) + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    dynamic response = JsonConvert.DeserializeObject(responseText);
                    ValidationResult resultado = response.ResultadoValidacao.ToObject<ValidationResult>();
                    if (resultado.IsValid)
                    {
                        ApiKey apikey = response.ApiKey.ToObject<ApiKey>();
                        autentication = new AutenticarResponse(resultado, apikey);
                    }
                    else
                    {
                        MessageBox.Show("Conexão Invalida, abra um chamado!", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                    }
                }
                cpfText.Focus();
            }
            catch (WebException ex)
            {
                MessageBox.Show("Erro " + ex.Message);
                this.Close();
            }
        }

        public void AtivarCampos()
        {
            //Textbox
            txtBairro.Visible = true;
            txtCEP.Visible = true;
            txtCidade.Visible = true;
            txtComplemento.Visible = true;
            txtDataNascimento.Visible = true;
            txtEmail.Visible = true;
            txtEndereco.Visible = true;
            txtNome.Visible = true;
            txtNumero.Visible = true;
            txtNumeroRegistro.Visible = true;
            txtTelefoneComercial.Visible = true;
            txtTelefone.Visible = true;
            txtCelular.Visible = true;

            //ComboBox 
            cbEstado.Visible = true;
            cbSexo.Visible = true;

            //Bottuons 
            btnCadastrar.Visible = true;

            //label
            lblBairro.Visible = true;
            lblCelular.Visible = true;
            lblCEP.Visible = true;
            lblCidade.Visible = true;
            lblComplemento.Visible = true;
            lblDataNascimento.Visible = true;
            lblEndereco.Visible = true;
            lblNome.Visible = true;
            lblNumero.Visible = true;
            lblSexo.Visible = true;
            lblUF.Visible = true;
            lblTelefoneComercial.Visible = true;
            lblTelefone.Visible = true;
            lblEmail.Visible = true;
            lblNumeroRegistro.Visible = true;
            label1.Visible = true;

            //Checkbox
            chkContato.Visible = true;
        }

        #region Popularização Listas

        public List<CampoCadastral> CriaListaCampoBeneficio()
        {
            List<CampoCadastral> campoBeneficiario = new List<CampoCadastral>();

            campoBeneficiario.Add(new CampoCadastral() { Campo = "Nome", Valor = txtNome.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Nascimento", Valor = txtDataNascimento.Text.Substring(6, 4) + "-" + txtDataNascimento.Text.Substring(3, 2) + "-" + txtDataNascimento.Text.Substring(0, 2)});
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Sexo", Valor = avaliacaoResponse.politicaCadastral.CamposBeneficiario[2].Opcoes[cbSexo.SelectedIndex].Valor });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "CEP", Valor = txtCEP.Text.Replace("-", "") });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Endereco", Valor = txtEndereco.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Numero", Valor = txtNumero.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Complemento", Valor = txtComplemento.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Bairro", Valor = txtBairro.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Cidade", Valor = txtCidade.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "UF", Valor = avaliacaoResponse.politicaCadastral.CamposBeneficiario[9].Opcoes[cbEstado.SelectedIndex].Valor });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Telefone", Valor = txtTelefone.Text.Replace("(", "").Replace(")", "").Replace("-", "") });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "TelefoneCelular", Valor = txtCelular.Text.Replace("(", "").Replace(")", "").Replace("-", "") });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "TelefoneComercial", Valor = txtTelefoneComercial.Text.Replace("(", "").Replace(")", "").Replace("-", "") });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "Email", Valor = txtEmail.Text });
            campoBeneficiario.Add(new CampoCadastral() { Campo = "PermiteContatoTelefone", Valor = chkContato.Checked.ToString() });

            return campoBeneficiario;
        }
        public static List<CampoCadastral> CriaListaCampoPaciente()
        {
            List<CampoCadastral> campoPaciente = new List<CampoCadastral>();
            return campoPaciente;
        }
        public List<CampoCadastral> CriarListaProduto()
        {
            List<CampoCadastral> campoProduto = new List<CampoCadastral>();
            campoProduto.Add(new CampoCadastral() { Campo = "UF", Valor = avaliacaoResponse.politicaCadastral.CamposBeneficiario[9].Opcoes[cbEstado.SelectedIndex].Valor });
            campoProduto.Add(new CampoCadastral() { Campo = "NumeroRegistroConselho", Valor = txtNumeroRegistro.Text });
            return campoProduto;
        }
        public static List<CampoCadastral> CriarListaExtra()
        {
            List<CampoCadastral> campoExtra = new List<CampoCadastral>();
            return campoExtra;
        }

        #endregion

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            string retorno = CadastroBeneficiario();
            if (Equals(retorno, "OK"))
            {
                MessageBox.Show("Cadastrado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show("Você poderá realizar a venda dentro de 20-30 minutos", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }
            else
                MessageBox.Show(retorno, "Ocorreu um erro ao executar a operação", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public bool ConsisteCampos() {

            if (cbEstado.SelectedIndex == -1) {
                Principal.mensagem = "Selecione um Estado";
                cbEstado.Focus();
                return false;  
            }
            if (cbSexo.SelectedIndex == -1) {
                Principal.mensagem = "Selecione um Sexo";
                cbSexo.Focus();
                return false;  
            }
            if (String.IsNullOrEmpty(txtEmail.Text)) {
                Principal.mensagem = "Insira um E-mail";
                txtEmail.Focus();
                return false;  
            }
            if (String.IsNullOrEmpty(txtNome.Text)) {
                Principal.mensagem = "Insira um Nome";
                txtNome.Focus();
                return false;  
            }
            if (String.IsNullOrEmpty(txtNumeroRegistro.Text)) {
                Principal.mensagem = "Insira um CRM";
                txtNumeroRegistro.Focus();
                return false;  
            }
            if (txtDataNascimento.Text.Length < 10) {
                Principal.mensagem = "Insira uma data Completa";
                txtDataNascimento.Focus();
                return false;   
            }
            if (txtCEP.Text.Length < 9) {
                Principal.mensagem = "Insira um CEP completo";
                txtCEP.Focus();
                return false;  
            }
            return true; 
        }
        public string CadastroBeneficiario()
        {
            Cursor.Current = Cursors.WaitCursor;
            if (ConsisteCampos())
            {
                try
                {
                    string webAddr = "http://acessohml.funcionalmais.com/AcessoUnicoExterno/REST/api/CadastroUnico/Beneficiario";

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                    httpWebRequest.ContentType = "application/json; charset=utf-8";
                    httpWebRequest.Headers.Add("ApiKey", autentication.apikey.Key);
                    httpWebRequest.Headers.Add("Origem", "5");
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        List<CampoCadastral> campoBeneficiario = new List<CampoCadastral>();
                        campoBeneficiario = CriaListaCampoBeneficio();
                        List<CampoCadastral> campoPaciente = null;
                        List<CampoCadastral> campoProduto = null;
                        List<CampoCadastral> campoExtra = null;

                        //Verificação de que se precisa popular as listas 
                        if (avaliacaoResponse.politicaCadastral.CamposPaciente.Count > 0)
                            campoPaciente = CriaListaCampoPaciente();
                        if (avaliacaoResponse.politicaCadastral.CamposProduto.Count > 0)
                            campoProduto = CriarListaProduto();
                        if (avaliacaoResponse.politicaCadastral.CamposExtras.Count > 0)
                            campoExtra = CriarListaExtra();

                        //crianção do objeto para ser cadastrado 
                        CadastrarBeneficiarioRequest beneficiario = new CadastrarBeneficiarioRequest(cpfText.Text.Replace(",", "").Replace("/", ""), eanText.Text, campoBeneficiario, campoPaciente, campoProduto, campoExtra);

                        //Conversão do objeto para json para enviar na requisição 
                        string json = JsonConvert.SerializeObject(beneficiario);
                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var responseText = streamReader.ReadToEnd();
                        dynamic response = JsonConvert.DeserializeObject(responseText);
                        ValidationResult resultado = response.ToObject<ValidationResult>();

                        if (resultado.Errors.Length > 1) //Se houver mais de um erro realiza a junção deles para mostrar de uma vez. 
                        {
                            string aux = "";
                            for (int i = 0; i < resultado.Errors.Length; i++)
                                aux += "\n" + resultado.Errors[i].Message;

                            return resultado.IsValid ? "OK" : "ERROS: " + aux;
                        }
                        return resultado.IsValid ? "OK" : "ERRO: " + resultado.Errors[0].Message;
                    }
                }
                catch (Exception e)
                {
                    return e.Message;
                }
                finally { Cursor.Current = Cursors.Default; }
            }
            else {
                return Principal.mensagem; 
            }
        }

        private void eanText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnVerifica.PerformClick(); 
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                    cbSexo.Focus(); 
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEndereco.Focus(); 
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus(); 
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtComplemento.Focus(); 
        }

        private void txtComplemento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBairro.Focus(); 

        }

        private void cbSexo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEmail.Focus(); 
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumeroRegistro.Focus(); 
        }

        private void txtNumeroRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDataNascimento.Focus(); 
        }

        private void txtDataNascimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCEP.Focus(); 
        }

        private void txtNumero_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus(); 
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cbEstado.Focus(); 
        }

        private void cbEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkContato.Focus(); 
        }

        private void chkContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus(); 
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCelular.Focus();
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefoneComercial.Focus(); 
        }

        private void txtTelefoneComercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnCadastrar.Focus(); 
        }
    }
}
