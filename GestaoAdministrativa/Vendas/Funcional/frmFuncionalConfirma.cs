﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using GestaoAdministrativa.Negocio.Funcional;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public partial class frmFuncionalConfirma : Form
    {
        //VARIAVEIS PUBLICAS
        public frmVenda frmVenda;
        public DataTable tProdutos;
        public ConfirmaVendaResponse objConfirmaVendaResponseVenda;
        public ConfirmaVendaResponse confirmaVendaResponse;


        public FuncionalConfirmaVendaNegocio objFuncionalConfirmaVendaNegocio;
        public FuncionalMensagemConfirmacaoNegocio7_8 objFuncionalMensagemConfirmacaoNegocio7_8;
        public ProdutoConfirmaAutorizacaoRequest produtConfirmaAutorizacao;
        FuncionalUtil.mensagemErro objMensagemErro;
        public VendaAutorizadaRequest objVendaAutorizadaRequest;
        public ProdutoConfirmaAutorizacaoResponse produtoAutorizadoResponse;
        public List<ProdutoConfirmaAutorizacaoResponse> lProdutosAutorizadosResponse;
        public PreAutorizacaoFuncionalResponse objPreAutorizacaoResponse;
        public bool existeProdutoNegado = false;
        public bool descartaAutorizacao = false;


        public frmFuncionalConfirma()
        {
            InitializeComponent();
        }

        public frmFuncionalConfirma(frmVenda frmVenda, DataTable tProdutos, ConfirmaVendaResponse objConfirmaVendaResponseVenda)
        {
            this.frmVenda = frmVenda;
            this.tProdutos = tProdutos;
            this.objConfirmaVendaResponseVenda = objConfirmaVendaResponseVenda;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFuncionalConfirma_Shown(object sender, EventArgs e)
        {
            #region EXIBE VENDA PRE AUTORIZADA NO GRID 

            //CRIA AS COLUNAS DO DATATABLE produtosAutorizaçao
            #region CRIA COLUNAS DATATABLE produtosAutorizacao
            produtConfirmaAutorizacao = new ProdutoConfirmaAutorizacaoRequest();
            DataTable produtosAutorizacao = new DataTable();
            double valorTotalAutorizado = 0.0;
            produtosAutorizacao.Columns.Add("CodBarras", typeof(string));
            produtosAutorizacao.Columns.Add("DescricaoMedicamento", typeof(string));
            produtosAutorizacao.Columns.Add("PrecoMaximo", typeof(string));
            produtosAutorizacao.Columns.Add("PrecoVenda", typeof(string));
            produtosAutorizacao.Columns.Add("Prod_DPorc", typeof(string));
            produtosAutorizacao.Columns.Add("Qtde_Auto", typeof(string));
            produtosAutorizacao.Columns.Add("Retorno", typeof(string));
            produtosAutorizacao.Columns.Add("StatusProd", typeof(string));
            produtosAutorizacao.Columns.Add("StatusCode", typeof(string));
            #endregion

            //DECLARAÇÃO DE VARIAVEIS
            #region DECLARAÇÃO DE VARIAVEIS
            decimal precoVenda = 0.0M;
            decimal precoMaximo = 0.0M;
            int statusDoItem = 0; //RETORNO DO ARQUIVO PRE AUTORIZACAO 7.4
            decimal desconto = 0.0M;
            int quantidade = 0;
            double precoDigitado = 0.0;
            double precoAutorizado = 0.0;
            string msg = "";
            string statusMsg = "";
            string statusCode = "0"; //STATUS CODE = 0 (SUCESSO) STATUS CODE = 1(NÃO AUTORIZADO)
            DataRow row;
            //FIM DE DECLARAÇÃO DE VARIAVEIS 
            objVendaAutorizadaRequest = new VendaAutorizadaRequest();
            objVendaAutorizadaRequest.ValorTotal = 0;
            objVendaAutorizadaRequest.Desconto = 0;
            objVendaAutorizadaRequest.TotalAPagar = 0;
            #endregion

            //TRATA AS INFORMAÇÕES PARA EXIBIR NO GRID
            #region TRATA INFORMAÇÕES PARA EXIBIÇÃO NO GRID DE CONFIRMAÇÃO
            foreach (var item in frmVenda.objPreAutorizacaoResponse.ProdutosPreAutorizacao)
            {

                desconto = 0.0M;
                statusDoItem = Convert.ToInt32(item.StatusItem);


                int numeroDeLinhas = frmVenda.tProdutos.Select("COD_BARRA = '" + item.CodBarras.ToString().Substring(1) + "'").Length;
                if (numeroDeLinhas >= 1)
                {
                    row = frmVenda.tProdutos.Select("COD_BARRA = '" + item.CodBarras.ToString().Substring(1) + "'").First();
                    precoDigitado = Convert.ToDouble(row["PRE_VALOR"].ToString());
                }

                else
                {
                    precoDigitado = 0.0;
                }

                //SE STATUS DO ITEM = APROVADO PARAM 325-YYY = 00 
                if (statusDoItem == 0)
                {
                    precoVenda = (Convert.ToDecimal(item.PrecoVenda) / 100);
                    precoMaximo = (Convert.ToDecimal(item.PrecoMaximo) / 100);
                    quantidade = Convert.ToInt32(item.QuantidadeAutorizada);
                   // valorTotalAutorizado += Convert.ToDouble(precoVenda);
                    precoAutorizado = (Convert.ToDouble(item.PrecoVenda) / 100);
                    msg = "";
                    desconto = Math.Round( 100 - (((Convert.ToDecimal(item.PrecoVenda) / 100) / (Convert.ToDecimal(item.PrecoMaximo) / 100)) * 100),2);
                    //desconto = Math.Round(100 - (((Convert.ToDecimal(item.PrecoVenda) / 100))));
                    statusCode = "0";
                    if (frmVenda.objPreAutorizacaoResponse.CodigoDaResposta != "00")
                    {
                        statusMsg = "Pré-Aprov";
                    }
                    else
                    {
                        if (precoDigitado > precoAutorizado)
                        {
                            msg = "*** Atenção: Preço autorizado menor que o preço solicitado. ***";
                            statusMsg = "Autorizado";
                        }
                        else
                        {
                            statusMsg = "Autorizado";
                        }
                    }
                    string conselhoDaVenda = frmVenda.objTransacaoClienteCartaoFuncional.ConselhoDaVenda.ToString();
                    objVendaAutorizadaRequest.TipoConselhoDaVenda = conselhoDaVenda.Equals("CRM") ? "CRM" : "CRO";
                    objVendaAutorizadaRequest.CodConselho = frmVenda.objTransacaoClienteCartaoFuncional.CodConselho;
                    objVendaAutorizadaRequest.ValorTotal += precoDigitado;
                   // objVendaAutorizadaRequest.TotalAPagar += Convert.ToDouble(precoVenda) * quantidade;
                    objVendaAutorizadaRequest.Desconto += (precoDigitado - Convert.ToDouble(precoVenda));
                    //PREENCRE OBJ SOMENTE COM PRODUTOS APROVADOS 
                    produtoAutorizadoResponse = new ProdutoConfirmaAutorizacaoResponse();
                    lProdutosAutorizadosResponse = new List<ProdutoConfirmaAutorizacaoResponse>();
                    produtoAutorizadoResponse.CodBarras = item.CodBarras;
                    produtoAutorizadoResponse.PrecoAutorizado = precoAutorizado;
                    produtoAutorizadoResponse.PrecoDigitado = precoDigitado;
                    produtoAutorizadoResponse.DescontoValor = Convert.ToDouble(desconto);
                    produtoAutorizadoResponse.Quantidade = quantidade;
                    produtoAutorizadoResponse.ProdTotalDigitado = precoDigitado * quantidade;
                    lProdutosAutorizadosResponse.Add(produtoAutorizadoResponse);
                    objVendaAutorizadaRequest.lProdutosConfirmados = lProdutosAutorizadosResponse;
                }
                // SE NÃO SE PARAM 325-000 != 00  
                else
                {
                    ApagaItemReprovadoTProdutoVenda(item);
                    
                    objMensagemErro.mensagem = FuncionalUtil.IdentificaMensagemDeErro(statusDoItem);
                    objMensagemErro.codigo = statusDoItem.ToString();
                    
                    statusCode = "1";
                    precoVenda = (Convert.ToDecimal(item.PrecoVenda) / 100);
                    precoMaximo = (Convert.ToDecimal(item.PrecoMaximo) / 100);
                    quantidade = Convert.ToInt32(item.QuantidadeAutorizada);
                    //valorTotalAutorizado += Convert.ToDouble(precoVenda);
                    existeProdutoNegado = true;
                    msg = "*** " + objMensagemErro.mensagem + "***";
                    statusMsg = "Negado";
                    precoVenda = Convert.ToDecimal(precoDigitado);
                    desconto = 0.0M;
                }

                produtosAutorizacao.Rows.Add(new String[] { item.CodBarras, item.DescricaoMedicamento, String.Format("{0:N}", precoMaximo == 0 ? precoVenda : precoMaximo), string.Format("{0:N}", precoVenda), desconto.ToString(), quantidade.ToString("D2"), msg, statusMsg, statusCode });
            }
            #endregion


            dgvProdutosConfirma.DataSource = produtosAutorizacao;
            lblValor.Text = "Valor a debitar do Cartao R$ " + String.Format("{0:N}", Convert.ToDouble(frmVenda.objPreAutorizacaoResponse.ValorDaOperacao) / 100);
            lblAvista.Text = "Valor à Vista R$ " + String.Format("{0:N}",Convert.ToDouble(frmVenda.objPreAutorizacaoResponse.ValorPagarAVista) / 100);

            btnAceitar.Focus();
            #endregion
        }

        private void ApagaItemReprovadoTProdutoVenda(ProdutoPreAutorizacao item)
        {
            for (int i = 0; i < frmVenda.tProdutos.Rows.Count; i++)
            {
                DataRow dr = tProdutos.Rows[i];
                if (dr["COD_BARRA"].ToString().PadLeft(13, '0') == item.CodBarras.Substring(1))
                {
                    dr.Delete();
                }
            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {

            #region CANCELAR PRE AUTORIZACAO
            if (frmVenda.objConfirmaVendaResponse.CodigoDaResposta != null)
            {
                FuncionalCancelamentoNegocio objFuncionalCancelamentoNegocio = new FuncionalCancelamentoNegocio();
                int numeroSequencialDoArquivo = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();
                double totalAPagar = objVendaAutorizadaRequest.TotalAPagar * 100;
                objFuncionalCancelamentoNegocio.CancelarFuncional(frmVenda.objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo, totalAPagar.ToString(), frmVenda.objConfirmaVendaResponse);


                #region Valida arquivo de status
                if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo))
                {
                    if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                    {
                        MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                    return;
                }
                #endregion

                #region Valida arquivo de resposta
                if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo))
                {
                    MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                    return;
                }
                #endregion

                #region Validaconsistencia de arquivos enviados ENV e RESP
                if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo))
                {
                    MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                    FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                    //ClearForm(this);
                    return;
                }

                #endregion

                #region INCREMENTA AUTORIZADOR.SEQ
                FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                #endregion
            }

            #endregion

            Principal.sRetorno = "0";
            descartaAutorizacao = true;
            frmVenda.descartaAutorizacaoFuncional = true;
            this.Close();
        }

        private void btnAceitar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (existeProdutoNegado)
            {
                var result = MessageBox.Show("Existe Produtos Negado, Deseja continuar e autorizar os produtos aprovados?", "Confirmação Funcional", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    if (frmVenda.tProdutos.Rows.Count > 0)
                    {
                        #region PRE-AUTORIZACAO FUNCIONAL LAYOUT 7.2(SOLICITA PRE AUTORIZACAO)
                        FuncionalPreAutorizacaoNegocio7_2 objPreAutorizacaoFuncionalNegocio = new FuncionalPreAutorizacaoNegocio7_2();
                        objPreAutorizacaoResponse = new PreAutorizacaoFuncionalResponse();
                        int numeroSequencialDoArquivo_7_2 = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();

                        objPreAutorizacaoFuncionalNegocio.SolicitaPreAutorizacao(frmVenda.objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo_7_2, frmVenda.tProdutos);

                        #region Valida arquivo de status
                        if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo_7_2))
                        {
                            if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo_7_2))
                            {
                                MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                            return;
                        }
                        #endregion

                        #region Valida arquivo de resposta
                        if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo_7_2))
                        {
                            MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            return;
                        }
                        #endregion

                        #region Validaconsistencia de arquivos enviados ENV e RESP
                        if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo_7_2))
                        {
                            MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo_7_2);
                            //ClearForm(this);
                            return;
                        }

                        #endregion

                        #region Preenche objeto PreAutorizacaoResponse
                        string temReceitaMedica = "N";
                        temReceitaMedica = frmVenda.objTransacaoClienteCartaoFuncional.TipoDaVenda.ToString().Equals(Enums.TipoVenda.ComReceita.ToString()) ? "S" : "N";
                        objPreAutorizacaoResponse = objPreAutorizacaoFuncionalNegocio.PreAutorizacaoResponse(numeroSequencialDoArquivo_7_2, temReceitaMedica);
                        #endregion

                        #region APAGA ARQUIVO DE RESPOSTA
                        string arquivoDeReposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo_7_2.ToString().PadLeft(8, '0') + ".rsp";
                        FuncionalUtil.ApagarArquivoDeResposta(arquivoDeReposta);
                        #endregion

                        #region INCREMENTA AUTORIZADOR.SEQ
                        FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo_7_2);
                        #endregion

                        #endregion

                        #region Confirmação PreAutorização LAYOUT 7.4(SOMENTE PRODUTOS AUTORIZADOS)
                        EnviaArquivoDeConfirmacao();
                        #endregion

                        #region Envia Mensagem de Confirmação LAYOUT 7.8
                        EnviaMensagemDeConfirmacao7_8();
                        #endregion

                        #region ABRE FORM VENDA AUTORIZADA
                        FrmFuncionalVendaAutorizada frmFuncionalVendaAutorizada =
                            new FrmFuncionalVendaAutorizada(this, frmVenda, objVendaAutorizadaRequest);
                        this.Hide();
                        frmFuncionalVendaAutorizada.StartPosition = FormStartPosition.CenterScreen;
                        frmFuncionalVendaAutorizada.ShowDialog();
                        #endregion
                        //APÓS RETORNO DO FORM DE VENDA AUTORIZADA FECHA ESTE FORM E 
                        //VOLTA PARA FORM VENDA PARA FINALIZAR VENDA
                    }

                    this.Close();
                }
            }
            else
            {
                this.confirmaVendaResponse = objConfirmaVendaResponseVenda;
                EnviaMensagemDeConfirmacao7_8();

                #region ABRE FORM VENDA AUTORIZADA
                FrmFuncionalVendaAutorizada frmFuncionalVendaAutorizada =
                    new FrmFuncionalVendaAutorizada(this, frmVenda, objVendaAutorizadaRequest);
                this.Hide();
                frmFuncionalVendaAutorizada.StartPosition = FormStartPosition.CenterScreen;
                frmFuncionalVendaAutorizada.ShowDialog();
                #endregion

                //9º APÓS RETORNO DO FORM DE VENDA AUTORIZADA FECHA ESTE FORM 
                //E VOLTA PARA FORM VENDA PARA FINALIZAR VENDA
                this.Close();
            }
            Cursor.Current = Cursors.Default;

        }

        private void EnviaArquivoDeConfirmacao()
        {
            //***************7.4 ENVIA ARQUIVO DE CONFIRMAÇÃO LAYOUT 7.4 AQUI*****************************
            objFuncionalConfirmaVendaNegocio = new FuncionalConfirmaVendaNegocio();
            int numeroSequencialDoArquivo = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();
            //1º GERA O ARQUIVO DE ENVIO - CONFIRMAR VENDA ENV 
            objFuncionalConfirmaVendaNegocio.ConfirmarVendaEnv(frmVenda.objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo, objPreAutorizacaoResponse, frmVenda.tProdutos);

            //2º VALIDA O ARQUIVO DE STATUS
            #region Valida arquivo de status
            if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo))
            {
                if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                {
                    MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                    return;
                }
            }
            else
            {
                MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                return;
            }
            #endregion

            //3º VALIDA ARQUIVO DE RESPOSTA
            #region Valida arquivo de resposta
            if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo))
            {
                MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                return;
            }
            #endregion

            //4º VALIDA CONSISTENCIA DO ARQUIVO ENV E RSP
            #region Validaconsistencia de arquivos enviados ENV e RESP
            if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo))
            {
                MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                //ClearForm(this);
                return;
            }

            #endregion

            //5º CARREGA OBJETO COM O ARQUIVO DE RETORNO
            #region Carrega o objeto confirma venda response
            //string temReceitaMedica = frmVenda.comReceitaFuncional;
            string temReceitaMedica = frmVenda.objTransacaoClienteCartaoFuncional.TipoDaVenda.ToString().Equals(Enums.TipoVenda.ComReceita.ToString()) ? "S" : "N";
            this.confirmaVendaResponse = objFuncionalConfirmaVendaNegocio.ConfirmaVendaRsp(numeroSequencialDoArquivo, temReceitaMedica);
            #endregion

            //6º APAGA O ARQUIVO DE RESPOSTA 
            #region APAGA ARQUIVO DE RESPOSTA
            //string arquivoDeReposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
            //FuncionalUtil.ApagarArquivoDeResposta(arquivoDeReposta);
            #endregion

            //7º INCREMENTA O AUTORIZADOR.SEQ
            #region INCREMENTA AUTORIZADOR.SEQ
            FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
            #endregion
        }

        private void EnviaMensagemDeConfirmacao7_8()
        {
            //***************7.8 ENVIA MENSAGEM DE CONFIRMAÇÃO LAYOUT 7.8 AQUI*****************************
            objFuncionalMensagemConfirmacaoNegocio7_8 = new FuncionalMensagemConfirmacaoNegocio7_8();
            int numeroSequencialDoArquivo = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();
            //1º GERA O ARQUIVO DE ENVIO - CONFIRMAR VENDA ENV 
            objFuncionalMensagemConfirmacaoNegocio7_8.EnviaMensagemConfirmacao(frmVenda.objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo, frmVenda.objPreAutorizacaoResponse, objVendaAutorizadaRequest, confirmaVendaResponse, frmVenda.tProdutos);

            //2º VALIDA O ARQUIVO DE STATUS
            #region Valida arquivo de status
            if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo))
            {
                if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                {
                    MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                    return;
                }
            }
            else
            {
                MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                return;
            }
            #endregion

            //3º VALIDA ARQUIVO DE RESPOSTA
            #region Valida arquivo de resposta
            if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo))
            {
                MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                return;
            }
            #endregion

            //4º VALIDA CONSISTENCIA DO ARQUIVO ENV E RSP
            #region Validaconsistencia de arquivos enviados ENV e RESP
            if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo))
            {
                MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                //ClearForm(this);
                return;
            }

            #endregion

            //5º CARREGA OBJETO COM O ARQUIVO DE RETORNO
            //#region Carrega o objeto confirma venda response
            //string temReceitaMedica = "N";
            //temReceitaMedica = frmVenda.objTransacaoClienteCartaoFuncional.TipoDaVenda.ToString().Equals(Enums.TipoVenda.ComReceita.ToString()) ? "S" : "N";
            //confirmaVendaResponse = objFuncionalConfirmaVendaNegocio.ConfirmaVendaRsp(numeroSequencialDoArquivo, temReceitaMedica);
            //#endregion

            //6º APAGA O ARQUIVO DE RESPOSTA 
            #region APAGA ARQUIVO DE RESPOSTA
            //string arquivoDeReposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
            //FuncionalUtil.ApagarArquivoDeResposta(arquivoDeReposta);
            #endregion

            //7º INCREMENTA O AUTORIZADOR.SEQ
            #region INCREMENTA AUTORIZADOR.SEQ
            FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
            #endregion
        }

        private void frmFuncionalConfirma_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!descartaAutorizacao)
            {
                frmVenda.objConfirmaVendaResponse = confirmaVendaResponse;
                frmVenda.objDadosVendaConfirmadaFuncional = objVendaAutorizadaRequest;
            }
            //frmVenda.list.Add(lProdutosAutorizadosResponse;
        }

        private void dgvProdutosConfirma_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgvProdutosConfirma.Rows[e.RowIndex].Cells[8].Value.ToString() == "1")
            {
                dgvProdutosConfirma.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgvProdutosConfirma.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            //Principal.preAutorizou = true;
            //Principal.sRetorno = "2";
            //this.Close();

            Principal.sRetorno = "0";
            descartaAutorizacao = true;
            frmVenda.descartaAutorizacaoFuncional = true;
            this.Close();
        }

        private void btnDescartar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void btnAceitar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void btnAlterar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void dgvProdutosConfirma_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }
    }
}
