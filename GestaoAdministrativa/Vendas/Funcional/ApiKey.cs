﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class ApiKey
    {
        public string Key { get; set; } //This is ApiKey 
        public DateTime DataExpiracao { get; set; } 
    }
}
