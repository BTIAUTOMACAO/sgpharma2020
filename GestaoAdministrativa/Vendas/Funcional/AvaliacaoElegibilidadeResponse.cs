﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class AvaliacaoElegibilidadeResponse
    {
        public AvaliacaoElegibilidadeResponse(ValidationResult resultadoAvaliar, AvaliacaoElegibilidade avaliacaoElegibilidade, PoliticaCadastral politicaCadastral, List<Paciente> pacientes)
        {
            this.ResutadoValidacao = resultadoAvaliar;
            this.AvaliacaoElegibidade = avaliacaoElegibilidade;
            this.politicaCadastral = politicaCadastral;
            this.Pacientes = pacientes;
        }

        public ValidationResult ResutadoValidacao { get; set; }
        public AvaliacaoElegibilidade AvaliacaoElegibidade { get; set; }
        public PoliticaCadastral politicaCadastral { get; set; }
        public List<Paciente> Pacientes { get; set; }
    }
}
