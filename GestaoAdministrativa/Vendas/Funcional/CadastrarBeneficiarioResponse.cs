﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class CadastrarBeneficiarioResponse
    {
        public ValidationResult ResultadoValidacao { get; set; } 
    }
}
