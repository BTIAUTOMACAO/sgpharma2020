﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class ConsultaCartaoResponse
    {
        public string Nome { get; set; }
        public string NumeroCartao { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public List<ProgramasParticipantes> Programas { get; set; }
        public ValidationError[] Errors { get; set; }
    }
}
