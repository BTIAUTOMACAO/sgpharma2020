﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa.Vendas.Funcional
{
    public class PoliticaCadastral
    {
        public Boolean UtilizaPacientes { get; set; }
        public int LimiteDependentes { get; set; }
        public List<PoliticaCadastralCampo> CamposBeneficiario { get; set; }
        public List<PoliticaCadastralCampo> CamposPaciente { get; set; }
        public List<PoliticaCadastralCampo> CamposProduto { get; set; }
        public List<PoliticaCadastralCampo> CamposExtras { get; set; }
        public Boolean ObrigatorioPrescritor { get; set; }
        public Boolean ObrigatorioAoMenosUmContato { get; set; }
        public List<int> OrigensPermitidas { get; set; }

    }
}
