﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenControleEntrega
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenControleEntrega));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlProdutos = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.VENDA_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ITEM_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFecharProdutos = new System.Windows.Forms.Button();
            this.pnlCancelar = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtMotivoCancelamento = new System.Windows.Forms.TextBox();
            this.btnCancelarCancelamento = new System.Windows.Forms.Button();
            this.btnConfirmarCancelamento = new System.Windows.Forms.Button();
            this.pnlRomaneio = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lblRomaneio = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnCancelarRomaneio = new System.Windows.Forms.Button();
            this.btnConfirmarRomaneiro = new System.Windows.Forms.Button();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.cmbREntregador = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dgEntregas = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportarParaExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbVendedor = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtRomaneio = new System.Windows.Forms.TextBox();
            this.txtVendaID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnRetornar = new System.Windows.Forms.Button();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnVisualizarProdutos = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.ESCOLHA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HORAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_ROMANEIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COL_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_ENDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_BAIRRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_CEP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMA_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CONTROLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.pnlProdutos.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.pnlCancelar.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.pnlRomaneio.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntregas)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.panel6.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pnlProdutos);
            this.panel1.Controls.Add(this.pnlCancelar);
            this.panel1.Controls.Add(this.pnlRomaneio);
            this.panel1.Controls.Add(this.dgEntregas);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Location = new System.Drawing.Point(8, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 495);
            this.panel1.TabIndex = 0;
            // 
            // pnlProdutos
            // 
            this.pnlProdutos.BackColor = System.Drawing.Color.Teal;
            this.pnlProdutos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProdutos.Controls.Add(this.panel5);
            this.pnlProdutos.Location = new System.Drawing.Point(229, 119);
            this.pnlProdutos.Name = "pnlProdutos";
            this.pnlProdutos.Size = new System.Drawing.Size(459, 213);
            this.pnlProdutos.TabIndex = 5;
            this.pnlProdutos.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.groupBox9);
            this.panel5.Location = new System.Drawing.Point(4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(449, 203);
            this.panel5.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dgProdutos);
            this.groupBox9.Controls.Add(this.btnFecharProdutos);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(6, 1);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(435, 195);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Produtos";
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            this.dgProdutos.AllowUserToResizeColumns = false;
            this.dgProdutos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VENDA_ITEM,
            this.PROD_DESCR,
            this.VENDA_ITEM_QTDE,
            this.PROD_CODIGO});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgProdutos.GridColor = System.Drawing.Color.Black;
            this.dgProdutos.Location = new System.Drawing.Point(6, 19);
            this.dgProdutos.Name = "dgProdutos";
            this.dgProdutos.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgProdutos.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutos.Size = new System.Drawing.Size(420, 126);
            this.dgProdutos.TabIndex = 11;
            // 
            // VENDA_ITEM
            // 
            this.VENDA_ITEM.DataPropertyName = "VENDA_ITEM";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.VENDA_ITEM.DefaultCellStyle = dataGridViewCellStyle3;
            this.VENDA_ITEM.HeaderText = "Item";
            this.VENDA_ITEM.Name = "VENDA_ITEM";
            this.VENDA_ITEM.ReadOnly = true;
            this.VENDA_ITEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.VENDA_ITEM.Width = 40;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PROD_DESCR.Width = 325;
            // 
            // VENDA_ITEM_QTDE
            // 
            this.VENDA_ITEM_QTDE.DataPropertyName = "VENDA_ITEM_QTDE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.VENDA_ITEM_QTDE.DefaultCellStyle = dataGridViewCellStyle4;
            this.VENDA_ITEM_QTDE.HeaderText = "Qtde";
            this.VENDA_ITEM_QTDE.Name = "VENDA_ITEM_QTDE";
            this.VENDA_ITEM_QTDE.ReadOnly = true;
            this.VENDA_ITEM_QTDE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.VENDA_ITEM_QTDE.Width = 50;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. Barras";
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Visible = false;
            // 
            // btnFecharProdutos
            // 
            this.btnFecharProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFecharProdutos.ForeColor = System.Drawing.Color.Black;
            this.btnFecharProdutos.Image = ((System.Drawing.Image)(resources.GetObject("btnFecharProdutos.Image")));
            this.btnFecharProdutos.Location = new System.Drawing.Point(384, 151);
            this.btnFecharProdutos.Name = "btnFecharProdutos";
            this.btnFecharProdutos.Size = new System.Drawing.Size(42, 38);
            this.btnFecharProdutos.TabIndex = 10;
            this.btnFecharProdutos.UseVisualStyleBackColor = true;
            this.btnFecharProdutos.Click += new System.EventHandler(this.btnFecharProdutos_Click);
            // 
            // pnlCancelar
            // 
            this.pnlCancelar.BackColor = System.Drawing.Color.Teal;
            this.pnlCancelar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCancelar.Controls.Add(this.panel4);
            this.pnlCancelar.Location = new System.Drawing.Point(428, 160);
            this.pnlCancelar.Name = "pnlCancelar";
            this.pnlCancelar.Size = new System.Drawing.Size(347, 172);
            this.pnlCancelar.TabIndex = 4;
            this.pnlCancelar.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.groupBox10);
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(337, 161);
            this.panel4.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtMotivoCancelamento);
            this.groupBox10.Controls.Add(this.btnCancelarCancelamento);
            this.groupBox10.Controls.Add(this.btnConfirmarCancelamento);
            this.groupBox10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(6, 1);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(324, 155);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Motivo do Retono/Cancelamento da Entrega";
            // 
            // txtMotivoCancelamento
            // 
            this.txtMotivoCancelamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMotivoCancelamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotivoCancelamento.Location = new System.Drawing.Point(11, 21);
            this.txtMotivoCancelamento.Multiline = true;
            this.txtMotivoCancelamento.Name = "txtMotivoCancelamento";
            this.txtMotivoCancelamento.Size = new System.Drawing.Size(301, 77);
            this.txtMotivoCancelamento.TabIndex = 11;
            this.txtMotivoCancelamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMotivoCancelamento_KeyPress);
            // 
            // btnCancelarCancelamento
            // 
            this.btnCancelarCancelamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarCancelamento.ForeColor = System.Drawing.Color.Black;
            this.btnCancelarCancelamento.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarCancelamento.Image")));
            this.btnCancelarCancelamento.Location = new System.Drawing.Point(222, 104);
            this.btnCancelarCancelamento.Name = "btnCancelarCancelamento";
            this.btnCancelarCancelamento.Size = new System.Drawing.Size(42, 45);
            this.btnCancelarCancelamento.TabIndex = 10;
            this.btnCancelarCancelamento.UseVisualStyleBackColor = true;
            this.btnCancelarCancelamento.Click += new System.EventHandler(this.btnCancelarCancelamento_Click);
            // 
            // btnConfirmarCancelamento
            // 
            this.btnConfirmarCancelamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmarCancelamento.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmarCancelamento.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmarCancelamento.Image")));
            this.btnConfirmarCancelamento.Location = new System.Drawing.Point(270, 104);
            this.btnConfirmarCancelamento.Name = "btnConfirmarCancelamento";
            this.btnConfirmarCancelamento.Size = new System.Drawing.Size(42, 45);
            this.btnConfirmarCancelamento.TabIndex = 9;
            this.btnConfirmarCancelamento.UseVisualStyleBackColor = true;
            this.btnConfirmarCancelamento.Click += new System.EventHandler(this.btnConfirmarCancelamento_Click);
            // 
            // pnlRomaneio
            // 
            this.pnlRomaneio.BackColor = System.Drawing.Color.Teal;
            this.pnlRomaneio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRomaneio.Controls.Add(this.panel2);
            this.pnlRomaneio.Location = new System.Drawing.Point(18, 165);
            this.pnlRomaneio.Name = "pnlRomaneio";
            this.pnlRomaneio.Size = new System.Drawing.Size(347, 198);
            this.pnlRomaneio.TabIndex = 3;
            this.pnlRomaneio.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(337, 188);
            this.panel2.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lblRomaneio);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(5, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(325, 58);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Número do Romaneio";
            // 
            // lblRomaneio
            // 
            this.lblRomaneio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRomaneio.ForeColor = System.Drawing.Color.Black;
            this.lblRomaneio.Location = new System.Drawing.Point(6, 18);
            this.lblRomaneio.Name = "lblRomaneio";
            this.lblRomaneio.Size = new System.Drawing.Size(313, 37);
            this.lblRomaneio.TabIndex = 0;
            this.lblRomaneio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btnCancelarRomaneio);
            this.groupBox8.Controls.Add(this.btnConfirmarRomaneiro);
            this.groupBox8.Controls.Add(this.txtNumero);
            this.groupBox8.Controls.Add(this.cmbREntregador);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(6, 58);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(324, 124);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            // 
            // btnCancelarRomaneio
            // 
            this.btnCancelarRomaneio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarRomaneio.ForeColor = System.Drawing.Color.Black;
            this.btnCancelarRomaneio.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarRomaneio.Image")));
            this.btnCancelarRomaneio.Location = new System.Drawing.Point(222, 73);
            this.btnCancelarRomaneio.Name = "btnCancelarRomaneio";
            this.btnCancelarRomaneio.Size = new System.Drawing.Size(42, 45);
            this.btnCancelarRomaneio.TabIndex = 10;
            this.btnCancelarRomaneio.UseVisualStyleBackColor = true;
            this.btnCancelarRomaneio.Click += new System.EventHandler(this.btnCancelarRomaneio_Click);
            this.btnCancelarRomaneio.MouseHover += new System.EventHandler(this.btnCancelarRomaneio_MouseHover);
            // 
            // btnConfirmarRomaneiro
            // 
            this.btnConfirmarRomaneiro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmarRomaneiro.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmarRomaneiro.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmarRomaneiro.Image")));
            this.btnConfirmarRomaneiro.Location = new System.Drawing.Point(270, 74);
            this.btnConfirmarRomaneiro.Name = "btnConfirmarRomaneiro";
            this.btnConfirmarRomaneiro.Size = new System.Drawing.Size(42, 45);
            this.btnConfirmarRomaneiro.TabIndex = 9;
            this.btnConfirmarRomaneiro.UseVisualStyleBackColor = true;
            this.btnConfirmarRomaneiro.Click += new System.EventHandler(this.btnConfirmarRomaneiro_Click);
            this.btnConfirmarRomaneiro.MouseHover += new System.EventHandler(this.btnConfirmarRomaneiro_MouseHover);
            // 
            // txtNumero
            // 
            this.txtNumero.BackColor = System.Drawing.Color.White;
            this.txtNumero.ForeColor = System.Drawing.Color.Black;
            this.txtNumero.Location = new System.Drawing.Point(111, 45);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(100, 22);
            this.txtNumero.TabIndex = 8;
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // cmbREntregador
            // 
            this.cmbREntregador.BackColor = System.Drawing.Color.White;
            this.cmbREntregador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbREntregador.ForeColor = System.Drawing.Color.Black;
            this.cmbREntregador.FormattingEnabled = true;
            this.cmbREntregador.Location = new System.Drawing.Point(111, 15);
            this.cmbREntregador.Name = "cmbREntregador";
            this.cmbREntregador.Size = new System.Drawing.Size(201, 24);
            this.cmbREntregador.TabIndex = 7;
            this.cmbREntregador.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbREntregador_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(24, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "N° Controle:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(27, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Entregador:";
            // 
            // dgEntregas
            // 
            this.dgEntregas.AllowUserToAddRows = false;
            this.dgEntregas.AllowUserToDeleteRows = false;
            this.dgEntregas.AllowUserToResizeColumns = false;
            this.dgEntregas.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEntregas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgEntregas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEntregas.BackgroundColor = System.Drawing.Color.White;
            this.dgEntregas.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntregas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgEntregas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgEntregas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESCOLHA,
            this.EST_CODIGO,
            this.EMP_CODIGO,
            this.ENT_CONTROLE_STATUS,
            this.ENT_CONTROLE_DATA,
            this.HORAS,
            this.VENDA_ID,
            this.CF_DOCTO,
            this.CF_NOME,
            this.ENT_CONTROLE_ROMANEIO,
            this.COL_NOME,
            this.VENDEDOR,
            this.ENT_CONTROLE_ENDER,
            this.ENT_CONTROLE_NUMERO,
            this.ENT_CONTROLE_BAIRRO,
            this.ENT_CONTROLE_CIDADE,
            this.ENT_CONTROLE_CEP,
            this.VENDA_TOTAL,
            this.FORMA_DESCRICAO,
            this.ENT_CONTROLE_ID,
            this.OP_ALTERACAO});
            this.dgEntregas.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEntregas.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgEntregas.Location = new System.Drawing.Point(3, 104);
            this.dgEntregas.Name = "dgEntregas";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntregas.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgEntregas.RowHeadersVisible = false;
            this.dgEntregas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEntregas.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgEntregas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEntregas.Size = new System.Drawing.Size(943, 300);
            this.dgEntregas.TabIndex = 1;
            this.dgEntregas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEntregas_CellClick);
            this.dgEntregas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEntregas_KeyDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportarParaExcelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 26);
            // 
            // exportarParaExcelToolStripMenuItem
            // 
            this.exportarParaExcelToolStripMenuItem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportarParaExcelToolStripMenuItem.Name = "exportarParaExcelToolStripMenuItem";
            this.exportarParaExcelToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.exportarParaExcelToolStripMenuItem.Text = "Exportar para Excel";
            this.exportarParaExcelToolStripMenuItem.Click += new System.EventHandler(this.exportarParaExcelToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblRegistros);
            this.groupBox2.Controls.Add(this.btnFiltrar);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(943, 98);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtros";
            // 
            // lblRegistros
            // 
            this.lblRegistros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistros.ForeColor = System.Drawing.Color.Black;
            this.lblRegistros.Location = new System.Drawing.Point(701, 66);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(229, 23);
            this.lblRegistros.TabIndex = 17;
            this.lblRegistros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.BackColor = System.Drawing.Color.White;
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ForeColor = System.Drawing.Color.Black;
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFiltrar.Location = new System.Drawing.Point(598, 21);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(82, 71);
            this.btnFiltrar.TabIndex = 16;
            this.btnFiltrar.Text = "F1 - Buscar";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFiltrar.UseVisualStyleBackColor = false;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.cmbVendedor);
            this.groupBox6.Controls.Add(this.cmbStatus);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Location = new System.Drawing.Point(341, 14);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(251, 78);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vendedor:";
            // 
            // cmbVendedor
            // 
            this.cmbVendedor.BackColor = System.Drawing.Color.White;
            this.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVendedor.FormattingEnabled = true;
            this.cmbVendedor.Location = new System.Drawing.Point(84, 14);
            this.cmbVendedor.Name = "cmbVendedor";
            this.cmbVendedor.Size = new System.Drawing.Size(158, 24);
            this.cmbVendedor.TabIndex = 5;
            this.cmbVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVendedor_KeyDown);
            this.cmbVendedor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbVendedor_KeyPress);
            // 
            // cmbStatus
            // 
            this.cmbStatus.BackColor = System.Drawing.Color.White;
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "PENDENTE",
            "EM TRÂNSITO",
            "FINALIZADA",
            "CANCELADA",
            "TODAS",
            ""});
            this.cmbStatus.Location = new System.Drawing.Point(84, 44);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(158, 24);
            this.cmbStatus.TabIndex = 7;
            this.cmbStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbStatus_KeyDown);
            this.cmbStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbStatus_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(22, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Status:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtRomaneio);
            this.groupBox5.Controls.Add(this.txtVendaID);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(169, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(170, 79);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            // 
            // txtRomaneio
            // 
            this.txtRomaneio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRomaneio.Location = new System.Drawing.Point(84, 46);
            this.txtRomaneio.Name = "txtRomaneio";
            this.txtRomaneio.Size = new System.Drawing.Size(71, 22);
            this.txtRomaneio.TabIndex = 4;
            this.txtRomaneio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRomaneio_KeyDown);
            this.txtRomaneio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRomaneio_KeyPress);
            // 
            // txtVendaID
            // 
            this.txtVendaID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendaID.Location = new System.Drawing.Point(84, 15);
            this.txtVendaID.Name = "txtVendaID";
            this.txtVendaID.Size = new System.Drawing.Size(71, 22);
            this.txtVendaID.TabIndex = 3;
            this.txtVendaID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVendaID_KeyDown);
            this.txtVendaID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVendaID_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(12, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Venda ID:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(12, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Romaneio:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.dtInicial);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.dtFinal);
            this.groupBox4.Location = new System.Drawing.Point(6, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(161, 79);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Data:";
            // 
            // dtInicial
            // 
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(51, 16);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(96, 22);
            this.dtInicial.TabIndex = 1;
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "até";
            // 
            // dtFinal
            // 
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(51, 47);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(96, 22);
            this.dtFinal.TabIndex = 2;
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnFinalizar);
            this.groupBox3.Controls.Add(this.btnCancelar);
            this.groupBox3.Controls.Add(this.btnRetornar);
            this.groupBox3.Controls.Add(this.btnIniciar);
            this.groupBox3.Controls.Add(this.btnImprimir);
            this.groupBox3.Controls.Add(this.btnVisualizarProdutos);
            this.groupBox3.Controls.Add(this.btnAtualizar);
            this.groupBox3.Controls.Add(this.pictureBox7);
            this.groupBox3.Location = new System.Drawing.Point(3, 402);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(943, 88);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizar.ForeColor = System.Drawing.Color.Black;
            this.btnFinalizar.Image = ((System.Drawing.Image)(resources.GetObject("btnFinalizar.Image")));
            this.btnFinalizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFinalizar.Location = new System.Drawing.Point(851, 11);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(86, 71);
            this.btnFinalizar.TabIndex = 14;
            this.btnFinalizar.Text = "F12-Confirmar";
            this.btnFinalizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            this.btnFinalizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFinalizar_KeyDown);
            this.btnFinalizar.MouseHover += new System.EventHandler(this.btnFinalizar_MouseHover);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.Location = new System.Drawing.Point(760, 11);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(85, 71);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "F11-Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            this.btnCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancelar_KeyDown);
            this.btnCancelar.MouseHover += new System.EventHandler(this.btnCancelar_MouseHover);
            // 
            // btnRetornar
            // 
            this.btnRetornar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetornar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetornar.ForeColor = System.Drawing.Color.Black;
            this.btnRetornar.Image = ((System.Drawing.Image)(resources.GetObject("btnRetornar.Image")));
            this.btnRetornar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnRetornar.Location = new System.Drawing.Point(669, 11);
            this.btnRetornar.Name = "btnRetornar";
            this.btnRetornar.Size = new System.Drawing.Size(85, 71);
            this.btnRetornar.TabIndex = 12;
            this.btnRetornar.Text = "F10-Retornar Entrega";
            this.btnRetornar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRetornar.UseVisualStyleBackColor = true;
            this.btnRetornar.Click += new System.EventHandler(this.btnRetornar_Click);
            this.btnRetornar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnRetornar_KeyDown);
            this.btnRetornar.MouseHover += new System.EventHandler(this.btnRetornar_MouseHover);
            // 
            // btnIniciar
            // 
            this.btnIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIniciar.ForeColor = System.Drawing.Color.Black;
            this.btnIniciar.Image = ((System.Drawing.Image)(resources.GetObject("btnIniciar.Image")));
            this.btnIniciar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnIniciar.Location = new System.Drawing.Point(578, 11);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(85, 71);
            this.btnIniciar.TabIndex = 11;
            this.btnIniciar.Text = "F9-Iniciar Entrega";
            this.btnIniciar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            this.btnIniciar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnIniciar_KeyDown);
            this.btnIniciar.MouseHover += new System.EventHandler(this.btnIniciar_MouseHover);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.ForeColor = System.Drawing.Color.Black;
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnImprimir.Location = new System.Drawing.Point(487, 11);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(85, 71);
            this.btnImprimir.TabIndex = 10;
            this.btnImprimir.Text = "F8-Reimprimir Comprovante";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            this.btnImprimir.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnImprimir_KeyDown);
            this.btnImprimir.MouseHover += new System.EventHandler(this.btnImprimir_MouseHover);
            // 
            // btnVisualizarProdutos
            // 
            this.btnVisualizarProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVisualizarProdutos.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVisualizarProdutos.ForeColor = System.Drawing.Color.Black;
            this.btnVisualizarProdutos.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizarProdutos.Image")));
            this.btnVisualizarProdutos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnVisualizarProdutos.Location = new System.Drawing.Point(396, 11);
            this.btnVisualizarProdutos.Name = "btnVisualizarProdutos";
            this.btnVisualizarProdutos.Size = new System.Drawing.Size(85, 71);
            this.btnVisualizarProdutos.TabIndex = 9;
            this.btnVisualizarProdutos.Text = "F7-Visualizar Produtos";
            this.btnVisualizarProdutos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnVisualizarProdutos.UseVisualStyleBackColor = true;
            this.btnVisualizarProdutos.Click += new System.EventHandler(this.btnVisualizarProdutos_Click);
            this.btnVisualizarProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnVisualizarProdutos_KeyDown);
            this.btnVisualizarProdutos.MouseHover += new System.EventHandler(this.btnVisualizarProdutos_MouseHover);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.ForeColor = System.Drawing.Color.Black;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAtualizar.Location = new System.Drawing.Point(305, 11);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(85, 71);
            this.btnAtualizar.TabIndex = 8;
            this.btnAtualizar.Text = "F6-Atualizar";
            this.btnAtualizar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            this.btnAtualizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAtualizar_KeyDown);
            this.btnAtualizar.MouseHover += new System.EventHandler(this.btnAtualizar_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(6, 11);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(161, 71);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.panel1);
            this.groupBox11.Controls.Add(this.panel6);
            this.groupBox11.Controls.Add(this.statusStrip1);
            this.groupBox11.Location = new System.Drawing.Point(12, 8);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(975, 560);
            this.groupBox11.TabIndex = 28;
            this.groupBox11.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel6.Controls.Add(this.label13);
            this.panel6.ForeColor = System.Drawing.Color.Black;
            this.panel6.Location = new System.Drawing.Point(-1, 7);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(975, 24);
            this.panel6.TabIndex = 42;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(7, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(160, 18);
            this.label13.TabIndex = 39;
            this.label13.Text = "Controle de Entregas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(969, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // ESCOLHA
            // 
            this.ESCOLHA.HeaderText = "";
            this.ESCOLHA.Name = "ESCOLHA";
            this.ESCOLHA.Visible = false;
            this.ESCOLHA.Width = 30;
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "EST_CODIGO";
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EST_CODIGO.Visible = false;
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            this.EMP_CODIGO.HeaderText = "EMP_CODIGO";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EMP_CODIGO.Visible = false;
            // 
            // ENT_CONTROLE_STATUS
            // 
            this.ENT_CONTROLE_STATUS.DataPropertyName = "ENT_CONTROLE_STATUS";
            this.ENT_CONTROLE_STATUS.HeaderText = "Status";
            this.ENT_CONTROLE_STATUS.Name = "ENT_CONTROLE_STATUS";
            this.ENT_CONTROLE_STATUS.ReadOnly = true;
            this.ENT_CONTROLE_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_CONTROLE_DATA
            // 
            this.ENT_CONTROLE_DATA.DataPropertyName = "ENT_CONTROLE_DATA";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ENT_CONTROLE_DATA.DefaultCellStyle = dataGridViewCellStyle10;
            this.ENT_CONTROLE_DATA.HeaderText = "Data";
            this.ENT_CONTROLE_DATA.Name = "ENT_CONTROLE_DATA";
            this.ENT_CONTROLE_DATA.ReadOnly = true;
            this.ENT_CONTROLE_DATA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HORAS
            // 
            this.HORAS.DataPropertyName = "HORAS";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.HORAS.DefaultCellStyle = dataGridViewCellStyle11;
            this.HORAS.HeaderText = "Hora da Venda";
            this.HORAS.Name = "HORAS";
            this.HORAS.ReadOnly = true;
            this.HORAS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDA_ID
            // 
            this.VENDA_ID.DataPropertyName = "VENDA_ID";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.VENDA_ID.DefaultCellStyle = dataGridViewCellStyle12;
            this.VENDA_ID.HeaderText = "Venda ID";
            this.VENDA_ID.Name = "VENDA_ID";
            this.VENDA_ID.ReadOnly = true;
            this.VENDA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "N° Docto";
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome";
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_CONTROLE_ROMANEIO
            // 
            this.ENT_CONTROLE_ROMANEIO.DataPropertyName = "ENT_CONTROLE_ROMANEIO";
            this.ENT_CONTROLE_ROMANEIO.HeaderText = "N° Romaneio";
            this.ENT_CONTROLE_ROMANEIO.Name = "ENT_CONTROLE_ROMANEIO";
            this.ENT_CONTROLE_ROMANEIO.ReadOnly = true;
            this.ENT_CONTROLE_ROMANEIO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // COL_NOME
            // 
            this.COL_NOME.DataPropertyName = "COL_NOME";
            this.COL_NOME.HeaderText = "Entregador";
            this.COL_NOME.Name = "COL_NOME";
            this.COL_NOME.ReadOnly = true;
            this.COL_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDEDOR
            // 
            this.VENDEDOR.DataPropertyName = "VENDEDOR";
            this.VENDEDOR.HeaderText = "Vendedor";
            this.VENDEDOR.Name = "VENDEDOR";
            this.VENDEDOR.ReadOnly = true;
            this.VENDEDOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_CONTROLE_ENDER
            // 
            this.ENT_CONTROLE_ENDER.DataPropertyName = "ENT_CONTROLE_ENDER";
            this.ENT_CONTROLE_ENDER.HeaderText = "Endereço";
            this.ENT_CONTROLE_ENDER.Name = "ENT_CONTROLE_ENDER";
            this.ENT_CONTROLE_ENDER.ReadOnly = true;
            this.ENT_CONTROLE_ENDER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_CONTROLE_ENDER.Width = 200;
            // 
            // ENT_CONTROLE_NUMERO
            // 
            this.ENT_CONTROLE_NUMERO.DataPropertyName = "ENT_CONTROLE_NUMERO";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ENT_CONTROLE_NUMERO.DefaultCellStyle = dataGridViewCellStyle13;
            this.ENT_CONTROLE_NUMERO.HeaderText = "Numero";
            this.ENT_CONTROLE_NUMERO.Name = "ENT_CONTROLE_NUMERO";
            this.ENT_CONTROLE_NUMERO.ReadOnly = true;
            this.ENT_CONTROLE_NUMERO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_CONTROLE_BAIRRO
            // 
            this.ENT_CONTROLE_BAIRRO.DataPropertyName = "ENT_CONTROLE_BAIRRO";
            this.ENT_CONTROLE_BAIRRO.HeaderText = "Bairro";
            this.ENT_CONTROLE_BAIRRO.Name = "ENT_CONTROLE_BAIRRO";
            this.ENT_CONTROLE_BAIRRO.ReadOnly = true;
            this.ENT_CONTROLE_BAIRRO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_CONTROLE_BAIRRO.Width = 150;
            // 
            // ENT_CONTROLE_CIDADE
            // 
            this.ENT_CONTROLE_CIDADE.DataPropertyName = "ENT_CONTROLE_CIDADE";
            this.ENT_CONTROLE_CIDADE.HeaderText = "Cidade";
            this.ENT_CONTROLE_CIDADE.Name = "ENT_CONTROLE_CIDADE";
            this.ENT_CONTROLE_CIDADE.ReadOnly = true;
            this.ENT_CONTROLE_CIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_CONTROLE_CEP
            // 
            this.ENT_CONTROLE_CEP.DataPropertyName = "ENT_CONTROLE_CEP";
            this.ENT_CONTROLE_CEP.HeaderText = "CEP";
            this.ENT_CONTROLE_CEP.Name = "ENT_CONTROLE_CEP";
            this.ENT_CONTROLE_CEP.ReadOnly = true;
            this.ENT_CONTROLE_CEP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDA_TOTAL
            // 
            this.VENDA_TOTAL.DataPropertyName = "VENDA_TOTAL";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = null;
            this.VENDA_TOTAL.DefaultCellStyle = dataGridViewCellStyle14;
            this.VENDA_TOTAL.HeaderText = "Total Venda";
            this.VENDA_TOTAL.Name = "VENDA_TOTAL";
            this.VENDA_TOTAL.ReadOnly = true;
            this.VENDA_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FORMA_DESCRICAO
            // 
            this.FORMA_DESCRICAO.DataPropertyName = "FORMA_DESCRICAO";
            this.FORMA_DESCRICAO.HeaderText = "Forma Pagto";
            this.FORMA_DESCRICAO.Name = "FORMA_DESCRICAO";
            this.FORMA_DESCRICAO.ReadOnly = true;
            this.FORMA_DESCRICAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FORMA_DESCRICAO.Visible = false;
            // 
            // ENT_CONTROLE_ID
            // 
            this.ENT_CONTROLE_ID.DataPropertyName = "ENT_CONTROLE_ID";
            this.ENT_CONTROLE_ID.HeaderText = "ID";
            this.ENT_CONTROLE_ID.Name = "ENT_CONTROLE_ID";
            this.ENT_CONTROLE_ID.ReadOnly = true;
            this.ENT_CONTROLE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OP_ALTERACAO
            // 
            this.OP_ALTERACAO.DataPropertyName = "OP_ALTERACAO";
            this.OP_ALTERACAO.HeaderText = "Alterado Por";
            this.OP_ALTERACAO.Name = "OP_ALTERACAO";
            this.OP_ALTERACAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // frmVenControleEntrega
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox11);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenControleEntrega";
            this.Text = "frmVenControleEntrega";
            this.Load += new System.EventHandler(this.frmVenControleEntrega_Load);
            this.Shown += new System.EventHandler(this.frmVenControleEntrega_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenControleEntrega_KeyDown);
            this.panel1.ResumeLayout(false);
            this.pnlProdutos.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.pnlCancelar.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.pnlRomaneio.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntregas)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgEntregas;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbVendedor;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtRomaneio;
        private System.Windows.Forms.TextBox txtVendaID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnRetornar;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnVisualizarProdutos;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Panel pnlRomaneio;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lblRomaneio;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnCancelarRomaneio;
        private System.Windows.Forms.Button btnConfirmarRomaneiro;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.ComboBox cmbREntregador;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlCancelar;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtMotivoCancelamento;
        private System.Windows.Forms.Button btnCancelarCancelamento;
        private System.Windows.Forms.Button btnConfirmarCancelamento;
        private System.Windows.Forms.Panel pnlProdutos;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.Button btnFecharProdutos;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ITEM_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportarParaExcelToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ESCOLHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn HORAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_ROMANEIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_ENDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_BAIRRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_CEP;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn FORMA_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CONTROLE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_ALTERACAO;
    }
}