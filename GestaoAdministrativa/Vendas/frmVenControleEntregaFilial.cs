﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenControleEntregaFilial : Form, Botoes
    {
        private ToolStripButton tsbLancamento = new ToolStripButton("Cont. Entrega Filial");
        private ToolStripSeparator tssLancamento = new ToolStripSeparator();
        private long vendaID;
        private int IDCliente;
        VendasDados dadosDaVenda = new VendasDados();
        Produto dProduto = new Produto();
        private int qtdeUsoContinuo;
        private int diasProdContinuo;
        private DateTime dataProxima;
        private int qtdeEmbalagem;
        List<VendaItem> venItens = new List<VendaItem>();
        double totalVenda = 0;
        double subTotal = 0;

        public frmVenControleEntregaFilial(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenControleEntregaFilial_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbLancamento);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssLancamento);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbLancamento.AutoSize = false;
                this.tsbLancamento.Image = Properties.Resources.entrega;
                this.tsbLancamento.Size = new System.Drawing.Size(145, 20);
                this.tsbLancamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbLancamento);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssLancamento);
                tsbLancamento.Click += delegate
                {
                    var lancamento = Application.OpenForms.OfType<frmVenControleEntregaFilial>().FirstOrDefault();
                    Util.BotoesGenericos();
                    lancamento.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<List<EntregaFilial>> BuscaEntrega()
        {
            List<EntregaFilial> allItems = new List<EntregaFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/BuscaEntregaPorCodLojaEStatus?codLoja=" + Funcoes.LeParametro(9, "52", true)
                        + "&status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EntregaFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async void Limpar()
        {
            Cursor = Cursors.WaitCursor;
            List<EntregaFilial> dados = await BuscaEntrega();
            if (dados.Count > 0)
            {
                Cursor = Cursors.Default;
                if (dados[0].CodErro == "00")
                {
                    cmbPendentes.DataSource = dados;
                    cmbPendentes.DisplayMember = "NomeFilial";
                    cmbPendentes.ValueMember = "ID";
                    cmbPendentes.SelectedIndex = -1;
                }
                else if (dados[0].CodErro == "01")
                {
                    cmbPendentes.DataSource = null;
                    MessageBox.Show("Nenhuma Entrega Pendente!", "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            lblBairro.Text = "";
            lblCidade.Text = "";
            lblDocumento.Text = "";
            lblEndereco.Text = "";
            lblNome.Text = "";
            lblNumero.Text = "";
            lblTelefone.Text = "";
            lblObservacao.Text = "";
            lblSolicitante.Text = "";
            lblTotal.Text = "";
            lblTroco.Text = "";
            dgProdutos.Rows.Clear();
            gbEntrega.Visible = false;
            
        }

        private async void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if(cmbPendentes.SelectedIndex != -1)
                {
                    Cursor = Cursors.WaitCursor;

                    List<EntregaFilial> dados = await BuscaEntregaPorID(Convert.ToInt32(cmbPendentes.SelectedValue));

                    if (dados.Count > 0)
                    {
                        if (dados[0].CodErro == "00")
                        {
                            lblNome.Text = dados[0].CfNome;
                            lblEndereco.Text = dados[0].CfEndereco;
                            lblNumero.Text = dados[0].CfNumero;
                            lblBairro.Text = dados[0].CfBairro;
                            lblCidade.Text = dados[0].CfCidade;
                            lblTelefone.Text = dados[0].CfTelefone;
                            lblSolicitante.Text = dados[0].Solicitante;
                            lblDocumento.Text = dados[0].CfDocto;
                            lblObservacao.Text = dados[0].Observacao;
                            lblTroco.Text = String.Format("{0:N}",dados[0].Troco - dados[0].TotalEntrega);
                            lblTotal.Text = String.Format("{0:N}", dados[0].TotalEntrega);
                            lblDocumento.Text = dados[0].CfDocto;

                            List<EntregaFilialItens> dadosItens = await BuscaEntregaItensPorID(Convert.ToInt32(cmbPendentes.SelectedValue));

                            if (dadosItens.Count > 0)
                            {
                                if (dadosItens[0].CodErro == "00")
                                {
                                    for(int i=0; i < dadosItens.Count; i++)
                                    {
                                        dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { dadosItens[i].ID,
                                            dadosItens[i].ProdCodigo,
                                            Util.SelecionaCampoEspecificoDaTabela("PRODUTOS","PROD_DESCR", "PROD_CODIGO",dadosItens[i].ProdCodigo,false, true, false),
                                            dadosItens[i].Qtde,
                                            dadosItens[i].Preco,
                                            dadosItens[i].Desconto,
                                            dadosItens[i].Subtotal,
                                            dadosItens[i].Total, 
                                            dadosItens[i].Comissao
                                        });
                                    }
                                }
                            }

                            Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                            DataRow row = Principal.dtPesq.NewRow();
                            Principal.dtPesq.Rows.InsertAt(row, 0);
                            cmbUsuario.DataSource = Principal.dtPesq;
                            cmbUsuario.DisplayMember = "COL_NOME";
                            cmbUsuario.ValueMember = "COL_CODIGO";
                            cmbUsuario.SelectedIndex = 0;

                            gbEntrega.Visible = true;
                        }
                        else
                        {
                            gbEntrega.Visible = false;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Necessário selecionar uma Entrega", "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<List<EntregaFilial>> BuscaEntregaPorID(int id)
        {
            List<EntregaFilial> allItems = new List<EntregaFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/BuscaEntregaPorID?ID=" + id);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EntregaFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<EntregaFilialItens>> BuscaEntregaItensPorID(int id)
        {
            List<EntregaFilialItens> allItems = new List<EntregaFilialItens>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/BuscaEntregaItensPorID?ID=" + id);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EntregaFilialItens>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private async void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja Cancelar a Entrega?", "Controle de Entrega Filial", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    await AtualizaStatus(Convert.ToInt32(cmbPendentes.SelectedValue), "E", 0);

                    MessageBox.Show("Entrega Cancela com Sucesso!", "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Limpar();
                }
                else
                {
                    btnRealizar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<bool> AtualizaStatus(int ID, string status, long vendaID)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/AtualizacaoDeStatus?ID=" + ID + "&status=" + status + "&vendaID=" + vendaID);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Atualiza.", "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.WaitCursor; 
            }
        }
        
        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F2:
                    btnOK.PerformClick();
                    break;
            }
        }

        private void frmVenControleEntregaFilial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbPendentes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnOK_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnRealizar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private async void btnRealizar_Click(object sender, EventArgs e)
        {
            try
            {
                if(cmbUsuario.SelectedIndex == -1)
                {
                    MessageBox.Show("Necessário selecionar um Vendedor!", "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbUsuario.Focus();
                    return;
                }

                if(MessageBox.Show("Confirma a Entrega?", "Controle de Entrega Filial", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    GeraIDVenda();

                    var cliente = new Cliente();
                    //INDENTIFICA SE PRECISA CADASTRAR O CLIENTE//
                    if (cliente.BuscaClientePorCPF(lblDocumento.Text).Rows.Count == 0)
                    {
                        //INCLUI NOVO REGISTRO NA TABELA CLIENTES//
                        cliente.CfDocto = lblDocumento.Text;
                        cliente.CfTipoDocto = 0;
                        cliente.CfNome = lblNome.Text;
                        cliente.CfEndereco = lblEndereco.Text;
                        cliente.CfNumeroEndereco = lblNumero.Text;
                        cliente.CfBairro = lblBairro.Text;
                        cliente.CfCEP = ".";
                        cliente.CfUF = "SP";
                        cliente.CfCidade = lblCidade.Text;
                        cliente.CfStatus = 4;
                        cliente.CfTelefone = lblTelefone.Text;
                        cliente.CfEnderecoCobranca = lblEndereco.Text;
                        cliente.CfNumeroCobranca = lblNumero.Text;
                        cliente.CfBairroCobranca = lblBairro.Text;
                        cliente.CfCEPCobranca = ".";
                        cliente.CfCidadeCobranca = lblCidade.Text;
                        cliente.CfUFCobranca = "SP";
                        cliente.CfEnderecoEntrega = lblEndereco.Text;
                        cliente.CfNumeroEntrega = lblNumero.Text;
                        cliente.CfBairroEntrega = lblBairro.Text;
                        cliente.CfCEPEntrega = ".";
                        cliente.CfCidadeEntrega = lblCidade.Text;
                        cliente.CfUFEntrega = "SP";
                        cliente.CfPais = "BRASIL";
                        cliente.CfLiberado = "N";
                        cliente.CodigoConveniada = Convert.ToInt32(Funcoes.LeParametro(3, "20", true));
                        cliente.NumCartao = "0";
                        cliente.DtCadastro = DateTime.Now;
                        cliente.OpCadastro = Principal.usuario;
                        
                        if (!cliente.IncluiClienteConsultaCatrtao(cliente, out IDCliente))
                            return;
                    }
                    else
                    {
                        IDCliente = Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_ID", "CF_DOCTO", lblDocumento.Text, false, true, false));
                    }

                    if(InserirDadosPreVenda())
                    {
                        BancoDados.FecharTrans();

                        if(ComprovanteOrcamento())
                        {
                            ProtocoloEntrega();

                            await AtualizaStatus(Convert.ToInt32(cmbPendentes.SelectedValue), "F", dadosDaVenda.VendaID);

                            Limpar();
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void GeraIDVenda()
        {
            vendaID = Funcoes.GeraIDLong("VENDAS_CONTADOR", "VENDA_ID");
            var atualizaID = new VendasContador();

            atualizaID.AtualizaDados(vendaID);
        }

        public bool InserirDadosPreVenda()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                totalVenda = 0;
                subTotal = 0;
                for (int i=0; i < dgProdutos.RowCount; i++)
                {
                    totalVenda += Convert.ToDouble(dgProdutos.Rows[i].Cells["Total"].Value);
                    subTotal += Convert.ToDouble(dgProdutos.Rows[i].Cells["Subtotal"].Value);
                }

                string prodUsoContinuo;

                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                #region DADOS DA VENDA
                dadosDaVenda.EmpCodigo = Principal.empAtual;
                dadosDaVenda.EstCodigo = Principal.estAtual;
                dadosDaVenda.VendaID = vendaID;
                dadosDaVenda.VendaBeneficio = "ENTREGA";
                
                BancoDados.AbrirTrans();
                DateTime dtPedido = DateTime.Now;

                dadosDaVenda.VendaCfID = IDCliente;
                dadosDaVenda.VendaData = dtPedido;
                dadosDaVenda.VendaDataHora = dtPedido;

                if (!String.IsNullOrEmpty(Funcoes.LeParametro(3, "20", true)))
                {
                    dadosDaVenda.VendaConCodigo = Funcoes.LeParametro(3, "20", true);
                }
                
                dadosDaVenda.VendaTotal = totalVenda;
                dadosDaVenda.VendaSubTotal = subTotal;
                dadosDaVenda.VendaDiferenca = 0;
                dadosDaVenda.VendaNfDevolucao = "N";
                dadosDaVenda.VendaMedico = "";
                dadosDaVenda.VendaNumReceita = "";
                dadosDaVenda.VendaEntrega = "S";
                dadosDaVenda.OpCadastro = Principal.usuario;
                dadosDaVenda.DtCadastro = DateTime.Now;
                dadosDaVenda.VendaQtde = dgProdutos.Rows.Count;
                dadosDaVenda.DtAlteracao = DateTime.Now;
                dadosDaVenda.OpAlteracao = Principal.usuario;
                dadosDaVenda.VendaAjusteGeral = 0;
                dadosDaVenda.VendaStatus = "P";
                dadosDaVenda.VendaComanda = 1;
                dadosDaVenda.VendaEmissao = null;
                dadosDaVenda.VendaColCodigo = Convert.ToInt32(cmbUsuario.SelectedValue);
               
                if (!dadosDaVenda.InserirDadosPreVenda(dadosDaVenda, true))
                    return false;
                #endregion

                #region DADOS PEDIDOS ITENS
                var vendasItens = new VendasItens();

                for (int i = 0; i < dgProdutos.Rows.Count; i++)
                {
                    VendasItens dadosDaVendaItens = new VendasItens();

                    prodUsoContinuo = Funcoes.LeParametro(6, "88", false);

                    if (dProduto.IdentificaUsoContinuo(dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString()))
                    {
                        if ((prodUsoContinuo.Equals("V")) || (prodUsoContinuo.Equals("C")) || prodUsoContinuo.Equals("T"))
                        {
                            //prodContCliente = true;
                            var cliContinuo = new ClienteContinuo(
                                Principal.doctoCliente,
                                dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(),
                                0,
                                Convert.ToInt32(dgProdutos.Rows[i].Cells["Qtde"].Value),
                                DateTime.Now,
                                DateTime.Now
                                );
                            Principal.dtRetorno = cliContinuo.IdentificaClienteXProduto(dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(), Principal.doctoCliente);
                            if (Principal.dtRetorno.Rows.Count.Equals(0))
                            {
                                if (!cliContinuo.InsereRegistros(cliContinuo))
                                    return false;
                            }
                            else
                            {
                                qtdeUsoContinuo = Principal.dtRetorno.Rows[0]["CONT_QTDE_DIA"].ToString() == "" ? 1 : Convert.ToInt32(Principal.dtRetorno.Rows[0]["CONT_QTDE_DIA"]);
                                if (qtdeUsoContinuo != 0)
                                {
                                    diasProdContinuo = (Convert.ToInt32(dgProdutos.Rows[i].Cells["Qtde"].Value) * qtdeEmbalagem) / qtdeUsoContinuo;
                                }

                                dataProxima = dtPedido.AddDays(diasProdContinuo);
                                if (!cliContinuo.AtualizaDados(dtPedido, Convert.ToInt32(dgProdutos.Rows[i].Cells["Qtde"].Value), dataProxima, Principal.doctoCliente, dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString()))
                                    return false;
                            }
                        }
                    }


                    dadosDaVendaItens.EmpCodigo = Principal.empAtual;
                    dadosDaVendaItens.EstCodigo = Principal.estAtual;
                    dadosDaVendaItens.VendaID = vendaID;
                    dadosDaVendaItens.VendaItem = i + 1;
                    dadosDaVendaItens.ProdID = Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(), false, true, false));
                    dadosDaVendaItens.ProdCodigo = dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString();
                    dadosDaVendaItens.VendaItemQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells["Qtde"].Value);
                    dadosDaVendaItens.VendaItemUnitario = Convert.ToDouble(dgProdutos.Rows[i].Cells["Preco"].Value);
                    dadosDaVendaItens.VendaItemSubTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells["SubTotal"].Value);
                    dadosDaVendaItens.VendaItemDiferenca = Math.Abs(Convert.ToDouble(dgProdutos.Rows[i].Cells["Desconto"].Value));
                    dadosDaVendaItens.VendaItemTotal = Convert.ToDouble(dgProdutos.Rows[i].Cells["Total"].Value);
                    dadosDaVendaItens.VendaItemUnidade = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_UNIDADE", "PROD_CODIGO", dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(), false, true, false).ToUpper();
                    dadosDaVendaItens.VendaPreValor = Convert.ToDouble(dgProdutos.Rows[i].Cells["Preco"].Value);
                    dadosDaVendaItens.OpCadastro = Principal.usuario;
                    dadosDaVendaItens.DtCadastro = DateTime.Now;
                    dadosDaVendaItens.VendaItemReceita = "N";
                    dadosDaVendaItens.VendaItemComissao = Convert.ToDouble(dgProdutos.Rows[i].Cells["COMISSAO"].Value);
                    dadosDaVendaItens.ColCodigo = dadosDaVenda.VendaColCodigo;
                    dadosDaVendaItens.VendaItemLote = null;

                    if (Funcoes.LeParametro(6, "220", true).Equals("S"))
                    {
                        dadosDaVendaItens.VendaPromocao = "N";
                        dadosDaVendaItens.VendaDescLiberado = "N";
                    }
                    else
                    {
                        dadosDaVendaItens.VendaPromocao = "N";
                        dadosDaVendaItens.VendaDescLiberado = "N";
                    }
                    
                    if (!dadosDaVendaItens.InserirItensPreVenda(dadosDaVendaItens))
                        return false;
                }
                #endregion

                int idForma = Funcoes.IdentificaVerificaID("VENDAS_FORMA_PAGAMENTO", "VENDA_FORMA_PAGTO_ID", Principal.estAtual, "", Principal.empAtual);

                var dadosFormaPagamento = new VendasFormaPagamento
                           (
                               dadosDaVenda.EmpCodigo,
                               dadosDaVenda.EstCodigo,
                               dadosDaVenda.VendaID,
                               idForma,
                               Convert.ToInt32(Funcoes.LeParametro(6, "356", true)),
                               1,
                               dadosDaVenda.VendaTotal,
                               DateTime.Now,
                               DateTime.Now,
                               Principal.usuario
                           );
                if (!dadosFormaPagamento.InserirDados(dadosFormaPagamento))
                    return false;

                int idEntrega = Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA", "ENT_CONTROLE_ID");
                var dadosEntrega = new ControleEntrega
                    (
                        dadosDaVenda.EmpCodigo,
                        dadosDaVenda.EstCodigo,
                        idEntrega,
                        dadosDaVenda.VendaID,
                        dadosDaVenda.VendaCfID,
                        0,
                        0,
                        0,
                        dadosDaVenda.VendaDataHora,
                        lblEndereco.Text,
                        Funcoes.RemoveCaracter(lblNumero.Text) == "" ? "0" : lblNumero.Text,
                        lblBairro.Text,
                        "0000000",
                        lblCidade.Text,
                        "SP",
                        lblTelefone.Text,
                        Convert.ToDouble(lblTotal.Text),
                        "",
                        lblObservacao.Text,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                if (!dadosEntrega.InsereRegistros(dadosEntrega))
                    return false;

                var dadosEntregaStatus = new ControleEntregaStatus
                    (
                        dadosDaVenda.EmpCodigo,
                        dadosDaVenda.EstCodigo,
                        Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                        idEntrega,
                        1,
                        0,
                        dadosDaVenda.VendaDataHora,
                        Principal.usuario,
                        "",
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                if (!dadosEntregaStatus.InsereRegistros(dadosEntregaStatus))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Dados Pre-Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool ComprovanteOrcamento()
        {
            try
            {
                if (MessageBox.Show("Imprime Comprovante?", "Comprovante Orcamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Cursor = Cursors.WaitCursor;
                    string  comprovanteVenda = "";
                    string tamanho;
                    var dadosEstabelecimento = new Estabelecimento();
                    List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                    if (retornoEstab.Count > 0)
                    {
                        for (int y = 1; y <= Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); y++)
                        {
                            comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                            comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                            comprovanteVenda += "N. VENDA: " + dadosDaVenda.VendaID + "\n";
                            comprovanteVenda += "Data: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("HH:mm:ss") + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";
                            comprovanteVenda += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";
                            for (int i = 0; i < dgProdutos.Rows.Count; i++)
                            {
                                string prodDescricao = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_DESCR", "PROD_CODIGO", dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(), false, true, false).ToUpper();

                                double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(dgProdutos.Rows[i].Cells["Preco"].Value)) / Convert.ToUInt32(dgProdutos.Rows[i].Cells["Qtde"].Value), 2) * 100);
                                comprovanteVenda += Funcoes.PrencherEspacoEmBranco(dgProdutos.Rows[i].Cells["ProdCodigo"].Value.ToString(), 13) + " " + (prodDescricao.Length > 29 ? prodDescricao.Substring(0, 28) : prodDescricao) + "\n";
                                if (Convert.ToDouble(dgProdutos.Rows[i].Cells["Desconto"].Value) > 0)
                                {
                                    comprovanteVenda += "    R$ " + dgProdutos.Rows[i].Cells["Preco"].Value.ToString().Replace(",", ".") + " (Desc = R$ " + dgProdutos.Rows[i].Cells["Desconto"].Value.ToString().Replace(",", ".") + ")\n";
                                }
                                comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + dgProdutos.Rows[i].Cells["Qtde"].Value + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(dgProdutos.Rows[i].Cells["Qtde"].Value)).ToString().Replace(",", ".") + "\n";
                            }
                            comprovanteVenda += "==========================================" + "\n";
                            var colaborador = new Colaborador();
                            comprovanteVenda += "VENDEDOR.: " + colaborador.NomeColaborador(dadosDaVenda.VendaColCodigo.ToString(), Principal.empAtual) + "\n";
                            comprovanteVenda += "SUBTOTAL.: R$ " + subTotal.ToString().Replace(",", ".") + "\n";

                            if ((subTotal - totalVenda) != 0)
                            {
                                comprovanteVenda += "DESCONTO.: R$ " + Math.Abs(subTotal - totalVenda).ToString().Replace(",", ".") + "\n";
                            }
                            comprovanteVenda += "TOTAL....: R$ " + totalVenda.ToString().Replace(",", ".") + "\n";

                            var descricaoForma = new FormasPagamento();
                            comprovanteVenda += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO", 43) + "\n";
                            
                            comprovanteVenda += "CONTRA-ENTREGA.: R$ " + String.Format("{0:N}", totalVenda) + "\n";
                            
                            comprovanteVenda += "__________________________________________" + "\n";


                            comprovanteVenda += "00";
                            comprovanteVenda += " - " + lblNome.Text + "\n";
                            comprovanteVenda += lblEndereco.Text + "," + lblNumero.Text + "\n";
                            comprovanteVenda += lblCidade.Text + "/" + "SP" + "\n";
                            comprovanteVenda += "CPF.:" + lblDocumento.Text + "\n";


                            comprovanteVenda += "__________________________________________" + "\n";
                            comprovanteVenda += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                            comprovanteVenda += "__________________________________________" + "\n\n\n";

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                     && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    string s_cmdTX = "\n";
                                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                    }
                                    else
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                    }

                                    iRetorno = BematechImpressora.IniciaPorta("USB");

                                    iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                    s_cmdTX = "\r\n";
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                    iRetorno = BematechImpressora.FechaPorta();
                                }
                                else
                                {
                                    //IMPRIME COMPROVANTE
                                    comprovanteVenda += "\n\n\n\n\n";
                                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                    }
                                }
                            }
                            else
                            {
                                comprovanteVenda += "\n\n\n\n";
                                //IMPRIME COMPROVANTE
                                int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Orcamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ProtocoloEntrega()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string protocoloDeEntrega = "__________________________________________" + "\n";
                protocoloDeEntrega += Funcoes.CentralizaTexto("PROTOCOLO DE ENTREGA", 43) + "\n";
                protocoloDeEntrega += "__________________________________________" + "\n";
                protocoloDeEntrega += "VENDA.........: " + dadosDaVenda.VendaID + "\n";
                protocoloDeEntrega += "DATA..........: " + dadosDaVenda.VendaData + "\n";
                protocoloDeEntrega += "CLIENTE.......: " + lblNome.Text + "\n";
                protocoloDeEntrega += "ENDERECO......: " + lblEndereco.Text + "\n";
                protocoloDeEntrega += "NUMERO........: " + lblNumero.Text + "\n";
                protocoloDeEntrega += "BAIRRO........: " + lblBairro.Text + "\n";
                protocoloDeEntrega += "CIDADE........: " + lblCidade.Text + "\n";
                protocoloDeEntrega += "CEP...........: 0\n";
                protocoloDeEntrega += "TELEFONE......: " + lblTelefone.Text + "\n";
                protocoloDeEntrega += "TOTAL DA NOTA.: " + String.Format("{0:N}", dadosDaVenda.VendaTotal) + "\n";
               
                    if (Convert.ToDouble(lblTroco.Text) != 0)
                    {
                        protocoloDeEntrega += "TROCO PARA....: " + String.Format("{0:N}", Convert.ToDouble(lblTroco.Text) + dadosDaVenda.VendaTotal) + "\n";
                        protocoloDeEntrega += "TROCO.........: " + String.Format("{0:N}", Convert.ToDouble(lblTroco.Text)) + "\n";
                    }
                    else
                    {
                        protocoloDeEntrega += "TROCO.........: 0,00\n";
                    }
                
                protocoloDeEntrega += "OBSERVACAO....: " + lblObservacao.Text + "\n\n\n";
                protocoloDeEntrega += "__________________________________________" + "\n";
                protocoloDeEntrega += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                protocoloDeEntrega += "__________________________________________" + "\n\n\n";

                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                         && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        string s_cmdTX = "\n";
                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                        }
                        else
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                        }

                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(protocoloDeEntrega, 3, 0, 0, 0, 0);

                        s_cmdTX = "\r\n";
                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.AcionaGuilhotina(0);

                        iRetorno = BematechImpressora.FechaPorta();
                    }
                    else
                    {
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocoloDeEntrega);
                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                }
                else
                {
                    protocoloDeEntrega += "\n\n\n\n";
                    //IMPRIME COMPROVANTE
                    int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocoloDeEntrega, 0);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Protocolo de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
