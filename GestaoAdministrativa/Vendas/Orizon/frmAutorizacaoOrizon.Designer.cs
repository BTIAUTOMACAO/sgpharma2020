﻿namespace GestaoAdministrativa.Vendas.Orizon
{
    partial class frmAutorizacaoOrizon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutorizacaoOrizon));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.COD_BARRAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUATIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRECO_CONSUMIDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRECO_VENDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRECO_COM_DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALOR_REEMBOLSO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FLAG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnEfetivar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAutorizacao = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(6, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(995, 321);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Autorização Orizon";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(6, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(983, 295);
            this.panel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgProdutos);
            this.groupBox3.Location = new System.Drawing.Point(3, 87);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(808, 191);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Produtos Autorizados";
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            this.dgProdutos.AllowUserToResizeColumns = false;
            this.dgProdutos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COD_BARRAS,
            this.PROD_DESCR,
            this.QUATIDADE,
            this.PRECO_CONSUMIDOR,
            this.PRECO_VENDA,
            this.PRECO_COM_DESCONTO,
            this.VALOR_REEMBOLSO,
            this.FLAG});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgProdutos.GridColor = System.Drawing.Color.Black;
            this.dgProdutos.Location = new System.Drawing.Point(8, 21);
            this.dgProdutos.Name = "dgProdutos";
            this.dgProdutos.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgProdutos.RowHeadersVisible = false;
            this.dgProdutos.RowHeadersWidth = 20;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutos.Size = new System.Drawing.Size(792, 162);
            this.dgProdutos.TabIndex = 16;
            // 
            // COD_BARRAS
            // 
            this.COD_BARRAS.DataPropertyName = "COD_BARRAS";
            this.COD_BARRAS.HeaderText = "Cód. Barras";
            this.COD_BARRAS.MinimumWidth = 2;
            this.COD_BARRAS.Name = "COD_BARRAS";
            this.COD_BARRAS.ReadOnly = true;
            this.COD_BARRAS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.MinimumWidth = 2;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESCR.Width = 220;
            // 
            // QUATIDADE
            // 
            this.QUATIDADE.DataPropertyName = "QUATIDADE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.QUATIDADE.DefaultCellStyle = dataGridViewCellStyle3;
            this.QUATIDADE.HeaderText = "Qtde.";
            this.QUATIDADE.MinimumWidth = 2;
            this.QUATIDADE.Name = "QUATIDADE";
            this.QUATIDADE.ReadOnly = true;
            this.QUATIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QUATIDADE.Width = 45;
            // 
            // PRECO_CONSUMIDOR
            // 
            this.PRECO_CONSUMIDOR.DataPropertyName = "PRECO_CONSUMIDOR";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.PRECO_CONSUMIDOR.DefaultCellStyle = dataGridViewCellStyle4;
            this.PRECO_CONSUMIDOR.HeaderText = "Preço Consumidor";
            this.PRECO_CONSUMIDOR.MinimumWidth = 2;
            this.PRECO_CONSUMIDOR.Name = "PRECO_CONSUMIDOR";
            this.PRECO_CONSUMIDOR.ReadOnly = true;
            this.PRECO_CONSUMIDOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRECO_CONSUMIDOR.Width = 135;
            // 
            // PRECO_VENDA
            // 
            this.PRECO_VENDA.DataPropertyName = "PRECO_VENDA";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PRECO_VENDA.DefaultCellStyle = dataGridViewCellStyle5;
            this.PRECO_VENDA.HeaderText = "Preço Venda";
            this.PRECO_VENDA.MinimumWidth = 2;
            this.PRECO_VENDA.Name = "PRECO_VENDA";
            this.PRECO_VENDA.ReadOnly = true;
            this.PRECO_VENDA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRECO_VENDA.Width = 112;
            // 
            // PRECO_COM_DESCONTO
            // 
            this.PRECO_COM_DESCONTO.DataPropertyName = "PRECO_COM_DESCONTO";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.PRECO_COM_DESCONTO.DefaultCellStyle = dataGridViewCellStyle6;
            this.PRECO_COM_DESCONTO.HeaderText = "Preco C/ Desc.";
            this.PRECO_COM_DESCONTO.MinimumWidth = 2;
            this.PRECO_COM_DESCONTO.Name = "PRECO_COM_DESCONTO";
            this.PRECO_COM_DESCONTO.ReadOnly = true;
            this.PRECO_COM_DESCONTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRECO_COM_DESCONTO.Width = 107;
            // 
            // VALOR_REEMBOLSO
            // 
            this.VALOR_REEMBOLSO.DataPropertyName = "VALOR_REEMBOLSO";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.VALOR_REEMBOLSO.DefaultCellStyle = dataGridViewCellStyle7;
            this.VALOR_REEMBOLSO.HeaderText = "Valor Reembolso";
            this.VALOR_REEMBOLSO.MinimumWidth = 2;
            this.VALOR_REEMBOLSO.Name = "VALOR_REEMBOLSO";
            this.VALOR_REEMBOLSO.ReadOnly = true;
            this.VALOR_REEMBOLSO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VALOR_REEMBOLSO.Width = 130;
            // 
            // FLAG
            // 
            this.FLAG.DataPropertyName = "FLAG";
            this.FLAG.HeaderText = "FLAG";
            this.FLAG.Name = "FLAG";
            this.FLAG.ReadOnly = true;
            this.FLAG.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblTotal);
            this.groupBox4.Controls.Add(this.btnFechar);
            this.groupBox4.Controls.Add(this.btnEfetivar);
            this.groupBox4.Controls.Add(this.btnLimpar);
            this.groupBox4.Controls.Add(this.pictureBox1);
            this.groupBox4.Location = new System.Drawing.Point(822, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(156, 286);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            // 
            // lblTotal
            // 
            this.lblTotal.ForeColor = System.Drawing.Color.Red;
            this.lblTotal.Location = new System.Drawing.Point(6, 321);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(144, 28);
            this.lblTotal.TabIndex = 5;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.ForeColor = System.Drawing.Color.Black;
            this.btnFechar.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar.Image")));
            this.btnFechar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFechar.Location = new System.Drawing.Point(6, 184);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(144, 38);
            this.btnFechar.TabIndex = 4;
            this.btnFechar.Text = "Fechar (ESC)";
            this.btnFechar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnEfetivar
            // 
            this.btnEfetivar.Enabled = false;
            this.btnEfetivar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEfetivar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEfetivar.ForeColor = System.Drawing.Color.Black;
            this.btnEfetivar.Image = ((System.Drawing.Image)(resources.GetObject("btnEfetivar.Image")));
            this.btnEfetivar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEfetivar.Location = new System.Drawing.Point(6, 136);
            this.btnEfetivar.Name = "btnEfetivar";
            this.btnEfetivar.Size = new System.Drawing.Size(144, 38);
            this.btnEfetivar.TabIndex = 2;
            this.btnEfetivar.Text = "Efetivar (F3)";
            this.btnEfetivar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEfetivar.UseVisualStyleBackColor = true;
            this.btnEfetivar.Click += new System.EventHandler(this.btnEfetivar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.ForeColor = System.Drawing.Color.Black;
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(6, 84);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(144, 38);
            this.btnLimpar.TabIndex = 1;
            this.btnLimpar.Text = "Limpar (F2)";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnConsultar);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtAutorizacao);
            this.groupBox2.Location = new System.Drawing.Point(3, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(808, 55);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Consultar Autorização";
            // 
            // btnConsultar
            // 
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.Black;
            this.btnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.Image")));
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(449, 14);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(130, 35);
            this.btnConsultar.TabIndex = 6;
            this.btnConsultar.Text = "Consultar (F1)";
            this.btnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.label3.Location = new System.Drawing.Point(17, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Número da Autorização";
            // 
            // txtAutorizacao
            // 
            this.txtAutorizacao.BackColor = System.Drawing.Color.White;
            this.txtAutorizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAutorizacao.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAutorizacao.ForeColor = System.Drawing.Color.Black;
            this.txtAutorizacao.Location = new System.Drawing.Point(194, 17);
            this.txtAutorizacao.MaxLength = 12;
            this.txtAutorizacao.Name = "txtAutorizacao";
            this.txtAutorizacao.Size = new System.Drawing.Size(234, 32);
            this.txtAutorizacao.TabIndex = 1;
            this.txtAutorizacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAutorizacao_KeyPress);
            // 
            // frmAutorizacaoOrizon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(1005, 325);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAutorizacaoOrizon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenuOrizon";
            this.Load += new System.EventHandler(this.frmAutorizacaoOrizon_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtAutorizacao;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnEfetivar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_BARRAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUATIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRECO_CONSUMIDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRECO_VENDA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRECO_COM_DESCONTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALOR_REEMBOLSO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FLAG;
    }
}