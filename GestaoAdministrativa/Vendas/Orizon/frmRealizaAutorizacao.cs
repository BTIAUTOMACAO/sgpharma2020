﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Orizon
{
    public partial class frmRealizaAutorizacao : Form
    {
        private frmVenda fVenda;
        public frmRealizaAutorizacao(frmVenda frmVenda)
        {
            fVenda = frmVenda;
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void frmRealizaAutorizacao_Load(object sender, EventArgs e)
        {

            CarregaCombos();
            Limpar();
        }

        public void CarregaCombos()
        {

            DataTable dtPesq = Util.CarregarCombosPorEstabelecimento("COD_CONV", "NOME_DESCR", "EPHARMA_CONVENIOS");
            cmbConvenios.DataSource = dtPesq;
            cmbConvenios.DisplayMember = "NOME_DESCR";
            cmbConvenios.ValueMember = "COD_CONV";
        }
        private void txtCrmCro_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13 && !String.IsNullOrEmpty(txtCrmCro.Text.Trim()))
            {
                BuscaMedicos();
            }
        }
        private void BuscaMedicos()
        {
            Cursor = Cursors.WaitCursor;

            Medico med = new Medico();
            DataTable dtMedico = med.BuscaDados(txtCrmCro.Text.Trim(), txtNome.Text.Trim());

            if (dtMedico.Rows.Count == 0)
            {
                MessageBox.Show("CRM / CRO não encontrado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCrmCro.Focus();
            }
            else if (dtMedico.Rows.Count == 1)
            {
                txtNome.Text = dtMedico.Rows[0]["MED_NOME"].ToString();
                cmbUf.SelectedText = dtMedico.Rows[0]["MED_UF"].ToString();
                dtReceita.Focus();
            }
            else
            {
                using (frmBuscaMedico buscaMedico = new frmBuscaMedico())
                {
                    buscaMedico.dgMedicos.DataSource = dtMedico;
                    buscaMedico.ShowDialog();
                }

                txtNome.Text = Principal.medico;
                cmbUf.SelectedText = Principal.uf;
                txtCrmCro.Text = Principal.crm;
            }
            Cursor = Cursors.Default;
        }

        private void BuscaProdutos()
        {
            Produto pro = new Produto();

            if (txtDescricao.Text != "")
            {
                using (frmBuscaProd buscaProd = new frmBuscaProd())
                {
                    buscaProd.BuscaProd(txtDescricao.Text.ToUpper(), false);
                    buscaProd.ShowDialog();
                }
            }
            string codBarras = txtCodigo.Text.Trim() != "" ? txtCodigo.Text.Trim() : Principal.codBarra;
            DataTable dtProduto = pro.BuscaProdutos(codBarras);

            if (dtProduto.Rows.Count != 0)
            {
                txtCodigo.Text = dtProduto.Rows[0]["PROD_CODIGO"].ToString();
                txtDescricao.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                txtPreco.Text = dtProduto.Rows[0]["PRE_VALOR"].ToString();
                txtQtd.Focus();
            }
            else
            {
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
            }

        }



        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        public void Limpar()
        {
            txtCrmCro.Text = String.Empty;
            txtNome.Text = String.Empty;
            txtCodigo.Text = String.Empty;
            txtDescricao.Text = String.Empty;
            txtQtd.Text = "0";
            txtPreco.Text = "0,00";
            txtCartao.Text = String.Empty;
            cmbUf.SelectedIndex = 24;
            cmbPrescritor.SelectedIndex = -1;
            cmbConvenios.SelectedIndex = 0;
            dgProdutos.Rows.Clear();
        }

        public bool ConsisteDados()
        {
            if (cmbConvenios.SelectedIndex.Equals(-1))
            {
                MessageBox.Show("Selecione o Identificador", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (String.IsNullOrEmpty(txtCartao.Text.Trim()))
            {
                MessageBox.Show("Informe o numero do Cartão", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCartao.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCrmCro.Text.Trim()))
            {
                MessageBox.Show("Informe o registro CRM / CRO", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCrmCro.Focus();
                return false;
            }
            if (cmbPrescritor.SelectedIndex.Equals(-1))
            {
                MessageBox.Show("Informe o Prescritor ", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbPrescritor.Focus();
                return false;
            }
            if (dgProdutos.RowCount.Equals(0))
            {
                MessageBox.Show("Adicione pelo menos um produto para efetuar a ", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbPrescritor.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool ConsisteDadosProdutos()
        {
            if (String.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                MessageBox.Show("Código de Barras não pode estar vazio", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodigo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                MessageBox.Show("Descrição não pode estar vazia", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodigo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtd.Text))
            {
                MessageBox.Show("Quantidade não pode estar vazia", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQtd.Focus();
                return false;
            }
            if (Convert.ToInt16(txtQtd.Text) <= 0)
            {
                MessageBox.Show("Quantidade inválida", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQtd.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtPreco.Text))
            {
                MessageBox.Show("Quantidade inválida", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPreco.Focus();
                return false;
            }
            if (Convert.ToDouble(txtPreco.Text) <= 0.00)
            {
                MessageBox.Show("Valor inválida", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPreco.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool MontaPedido(string sequencia)
        {
            try
            {
                int presc_cod = cmbPrescritor.Text.Equals("CRM") ? 1 : 2;
                string linha = sequencia + "01" + txtCartao.Text.PadLeft(19, ' ') + cmbConvenios.SelectedValue + String.Format("{0:yy/MM/dd}", dtReceita.Value).Replace("/", "") + presc_cod + txtCrmCro.Text.PadLeft(9, ' ') + cmbUf.Text + "".PadRight(153, ' ') + Environment.NewLine;
                string linhaProdutos = "";

                for (var i = 0; i < dgProdutos.RowCount; ++i)
                {
                    string valor = (Convert.ToDecimal(dgProdutos.Rows[i].Cells["PRECO"].Value.ToString().Replace(",", "")) * 100).ToString().PadLeft(7, '0');
                    linhaProdutos = linhaProdutos + dgProdutos.Rows[i].Cells["CODIGO"].Value.ToString().PadLeft(13, '0') + "".PadLeft(15, '0') + dgProdutos.Rows[i].Cells["QTD"].Value.ToString().PadLeft(2, '0') + valor + valor + Environment.NewLine;
                }

                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha + linhaProdutos);
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linha + linhaProdutos);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool LeRetorno(string sequencia)
        {
            try
            {
                var inicio = DateTime.UtcNow;
                while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
                {
                    if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                    {
                        MessageBox.Show("Erro:  Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

                string retornoSaldo = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
                Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + sequencia + ".txt", retornoSaldo);

                string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");

                if (retorno[0].Substring(6, 2).Equals("ER"))
                {
                    MessageBox.Show(retorno[0].Substring(9, 40).Trim(), "Retorno  E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                if (retorno[0].Substring(6, 2).Equals("OK"))
                {
                    int transacao = Convert.ToInt32(retorno[0].Substring(48, 7));
                    int nsu = Convert.ToInt32(retorno[0].Substring(55, 12));

                    Epharma epharma = new Epharma();
                    epharma.Contador();
                    epharma.LimparPastas();
                    string seq = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");

                    ConsultaAutorizacao(nsu, seq);

                    inicio = DateTime.UtcNow;
                    while (!File.Exists(Funcoes.LeParametro(14, "14", false) + seq + ".txt"))
                    {
                        if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                        {
                            MessageBox.Show("Erro: Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

                    LeRetornoConsula(seq, transacao);

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public void ConsultaAutorizacao(int numNSU, string sequencia)
        {
            string linhaConsulta = sequencia + "04" + numNSU.ToString().PadLeft(12, '0') + "1";

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linhaConsulta);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linhaConsulta);
        }

        public void LeRetornoConsula(string sequencia, int transacao)
        {
            //GRAVA LOG
            string retornoSaldo = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"LOGS\REQ_" + sequencia + ".txt", retornoSaldo);

            string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");

            if (retorno[0].Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retorno[0].Substring(9, 40).Trim(), "Retorno  E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            if (retorno[0].Substring(6, 2).Equals("OK"))
            {
                int nsu = Convert.ToInt32(retorno[0].Substring(55, 12));

                EpharmaVendas venda = new EpharmaVendas();
                venda.NumTrans = transacao;
                venda.NSU = nsu;
                venda.Cartao = txtCartao.Text.Trim();
                venda.Identificacao = cmbConvenios.Text.Trim();

                venda.Status = "P";

                if (venda.InsereVenda(venda).Equals(true))
                {
                    EpharmaProdutos itens = new EpharmaProdutos();
                    for (var i = 1; i < retorno.Count(); ++i)
                    {
                        itens.NumTrans = transacao;
                        itens.NSU = nsu;
                        itens.ProdCodigo = retorno[i].Substring(0, 13);
                        itens.Qtd = Convert.ToInt16(retorno[i].Substring(13, 2));
                        itens.ValorMaximo = Convert.ToInt32(retorno[i].Substring(15, 7)) * 0.01;
                        itens.ValorFinal = Convert.ToInt32(retorno[i].Substring(22, 7)) * 0.01;
                        itens.ValorFabrica = Convert.ToInt32(retorno[i].Substring(29, 7)) * 0.01;
                        itens.ValorAquisicaoUnitario = Convert.ToInt32(retorno[i].Substring(36, 7)) * 0.01;
                        itens.ValorRepasse = Convert.ToInt32(retorno[i].Substring(43, 7)) * 0.01;

                        itens.InsereProdutos(itens);

                    };
                    lblMsg.Visible = false;
                    //frmProdutos telaProdutos = new frmProdutos(nsu, transacao, fVenda);
                    //telaProdutos.ShowDialog();
                    this.Close();
                }
            }
        }

        public void InsereDadosGrid()
        {
            if (ConsisteDadosProdutos().Equals(true))
            {
                dgProdutos.Rows.Add(new object[] { txtCodigo.Text, txtDescricao.Text, txtQtd.Text, txtPreco.Text });

                lblValor.Text = (Convert.ToDecimal(lblValor.Text) + (Convert.ToDecimal(txtPreco.Text) * Convert.ToInt16(txtQtd.Text))).ToString();
                txtCodigo.Text = String.Empty;
                txtDescricao.Text = String.Empty;
                txtQtd.Text = "0";
                txtPreco.Text = "0,00";
            }
        }
        private void btnAutorizacao_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (ConsisteDados().Equals(true))
            {
                lblMsg.Visible = true;
                Epharma epharma = new Epharma();
                epharma.Contador();
                string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");
                if (MontaPedido(sequencia).Equals(true))
                {
                    LeRetorno(sequencia);
                }
            }
            Cursor = Cursors.Default;
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                BuscaProdutos();
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                BuscaProdutos();
            }
        }

        private void txtQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                InsereDadosGrid();
            }
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                InsereDadosGrid();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtCrmCro.Text.Trim()) && !String.IsNullOrEmpty(txtNome.Text.Trim()))
                {
                    BuscaMedicos();
                }
                dtReceita.Focus();

            }
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            InsereDadosGrid();
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                int indice = dgProdutos.CurrentCell.RowIndex;
                lblValor.Text = (Convert.ToDecimal(lblValor.Text) - (Convert.ToDecimal(dgProdutos.Rows[indice].Cells["PRECO"].Value) * Convert.ToInt16(dgProdutos.Rows[indice].Cells["QTD"].Value))).ToString();
                dgProdutos.Rows.RemoveAt(indice);
            }
        }


        private void txtCartao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtCrmCro.Focus();
            }
        }

        private void dtReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbPrescritor.Focus();
            }
        }
    }
}
