﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Orizon
{
    public partial class frmAutorizacaoOrizon : Form
    {
        frmVenda fVenda;

        public frmAutorizacaoOrizon(frmVenda formVenda)
        {
            InitializeComponent();
            fVenda = formVenda;
        }

        public void ConsultaAutorizacao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteDados().Equals(true))
                {
                    OrizonBeneficio orizon = new OrizonBeneficio();
                    string sequencia = orizon.ConsultaAutorizacao(txtAutorizacao.Text.Trim());
                    if (orizon.LeRetornoAutorizacao(sequencia).Equals(true))
                    {
                        string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt");
                        BancoDados.AbrirTrans();
                        OrizonVenda venda = new OrizonVenda();
                        OrizonProdutos vendaItens = new OrizonProdutos();

                        if (vendaItens.BuscaItensPorAutorizacao(txtAutorizacao.Text.Trim()).Rows.Count.Equals(0))
                        {
                            venda.Autorizacao = txtAutorizacao.Text.Trim();
                            venda.NsuTrans = retorno[0].Substring(48, 7);
                            venda.NsuHost = retorno[0].Substring(55, 12); ;
                            venda.Status = 'P';
                            if (venda.InsereVenda(venda).Equals(true))
                            {

                                for (int i = 1; i < retorno.Count(); i++)
                                {
                                    vendaItens.Autorizacao = txtAutorizacao.Text.Trim();
                                    vendaItens.CodBarras = retorno[i].Substring(0, 13);
                                    vendaItens.Quantidade = Convert.ToInt16(retorno[i].Substring(13, 2));
                                    vendaItens.PrecoConsumidor = Convert.ToDouble(Convert.ToInt32(retorno[i].Substring(15, 7)) * 0.01);
                                    vendaItens.PrecoVenda = Convert.ToDouble(Convert.ToInt32(retorno[i].Substring(22, 7)) * 0.01);
                                    vendaItens.PrecoComDesconto = Convert.ToDouble(Convert.ToInt32(retorno[i].Substring(29, 7)) * 0.01);
                                    vendaItens.Flag = retorno[i].Substring(36, 7);
                                    vendaItens.ValorReembolso = Convert.ToDouble(Convert.ToInt32(retorno[i].Substring(43, 7)) * 0.01);

                                    if (vendaItens.InsereItensVenda(vendaItens).Equals(false))
                                    {
                                        BancoDados.ErroTrans();
                                        MessageBox.Show("Erro ao Consultar Autorização", "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        break;
                                    }
                                }
                                BancoDados.FecharTrans();

                            }
                        }

                        DataTable dtProdutos = vendaItens.BuscaItensPorAutorizacao(txtAutorizacao.Text.Trim());
                        for (int i = 0; i < dtProdutos.Rows.Count; i++)
                        {
                            dtProdutos.Rows[i]["PRECO_CONSUMIDOR"] = Convert.ToDouble(dtProdutos.Rows[i]["PRECO_CONSUMIDOR"]) * Convert.ToInt16(dtProdutos.Rows[i]["QUATIDADE"]);
                            dtProdutos.Rows[i]["PRECO_VENDA"] = Convert.ToDouble(dtProdutos.Rows[i]["PRECO_VENDA"]) * Convert.ToInt16(dtProdutos.Rows[i]["QUATIDADE"]);
                            dtProdutos.Rows[i]["PRECO_COM_DESCONTO"] = Convert.ToDouble(dtProdutos.Rows[i]["PRECO_COM_DESCONTO"]) * Convert.ToInt16(dtProdutos.Rows[i]["QUATIDADE"]);
                            dtProdutos.Rows[i]["VALOR_REEMBOLSO"] = Convert.ToDouble(dtProdutos.Rows[i]["VALOR_REEMBOLSO"]) * Convert.ToInt16(dtProdutos.Rows[i]["QUATIDADE"]);
                        }

                        dgProdutos.DataSource = dtProdutos;
                        btnEfetivar.Enabled = true;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool ConsisteDados()
        {
            if (String.IsNullOrEmpty(txtAutorizacao.Text.Trim()))
            {
                MessageBox.Show("Numero da Autorização não pode estar vazios ", "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }

        #region Botoes
        private void txtAutorizacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 13)
            {
                ConsultaAutorizacao();
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultaAutorizacao(); ;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else if (keyData == Keys.F1)
            {
                ConsultaAutorizacao();
                return true;
            }
            else if (keyData == Keys.F2)
            {
                Limpar();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        public void Limpar()
        {
            txtAutorizacao.Text = String.Empty;
            dgProdutos.Rows.Clear();
        }

        private void btnEfetivar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtAutorizacao.Text.Trim()))
                {
                    MessageBox.Show("Numero da Autorização não pode estar vazios ", "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    fVenda.vendaNumeroNSU = txtAutorizacao.Text.Trim();
                    fVenda.vendaBeneficio = "ORIZON";
                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        #endregion

        private void frmAutorizacaoOrizon_Load(object sender, EventArgs e)
        {
            OrizonBeneficio orizon = new OrizonBeneficio();
            orizon.LimparPastas();
        }
    }
}
