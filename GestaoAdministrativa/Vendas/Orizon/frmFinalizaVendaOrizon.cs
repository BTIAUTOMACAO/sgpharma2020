﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Orizon
{
    public partial class frmFinalizaVendaOrizon : Form
    {
        frmVenda fVenda;
        public frmFinalizaVendaOrizon(frmVenda formVendas)
        {
            InitializeComponent();
            fVenda = formVendas;
        }

        private void frmFinalizaVendaOrizon_Load(object sender, EventArgs e)
        {
            try
            {
                Application.DoEvents();
                this.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmFinalizaVendaOrizon_Shown(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                lblMensagem.Text = "Enviando Efetivação da Venda!";
                lblMensagem.Refresh();
                OrizonBeneficio orizon = new OrizonBeneficio();
                string sequencia = orizon.EnviaVenda(fVenda.vendaNumeroNSU, fVenda.numCFE);

                lblMensagem.Text = "Esperando Confirmação da Venda!";
                lblMensagem.Refresh();

                if (orizon.RetornoVenda(sequencia).Equals(true))
                {
                    string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "43", false) + sequencia + ".txt");

                    OrizonVenda venda = new OrizonVenda();
                    venda.Autorizacao = fVenda.vendaNumeroNSU;
                    venda.VendaID = fVenda.vendaID;
                    venda.Status = 'C';
                    venda.NsuTrans = retorno[0].Substring(48, 7);
                    venda.AtualizaStatusVenda(venda);

                    fVenda.cupomBeneficio = retorno[1];
                    fVenda.sequencia = sequencia;
                    lblMensagem.Text = "Gerando Relatório Gerencial";
                    lblMensagem.Refresh();
                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Orizon", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }
    }
}
