﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenVSenha : Form
    {
        public bool sValida;
        private string colCodigo;

        public frmVenVSenha(string col_codigo)
        {
            InitializeComponent();
            colCodigo = col_codigo;
            RegisterFocusEvents(this.Controls);
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnOK.PerformClick();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtSenha.Text))
            {
                var dColaborador = new Colaborador();
                Principal.sRetorno = dColaborador.ValidaSenhaColaborador(Principal.empAtual, colCodigo, "V", "S", "X");
                if (Funcoes.DescriptografaSenha(Principal.sRetorno).Equals(txtSenha.Text))
                {
                    sValida = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Senha Inválida!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sValida = true;
                    txtSenha.Text = "";
                    txtSenha.Focus();
                }
            }
            else
            {
                MessageBox.Show("Senha não pode ser em branco!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSenha.Focus();
            }
        }

        private void frmVenVSenha_Load(object sender, EventArgs e)
        {
            sValida = false;
            txtSenha.Focus();
        }


        private void frmVenVSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) 
            {
                sValida = false;
                this.Close();
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                sValida = false;
                this.Close();
            }
        }
    }
}
