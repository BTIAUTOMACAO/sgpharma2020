﻿using System;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Collections.Generic;
using System.Drawing;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenReceita : Form
    {
        private bool novoMedico;
        private frmVenda fVendas;

        public frmVenReceita(frmVenda frmVendas)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            fVendas = frmVendas;
        }

        private void txtCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUF.Focus();
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtReceita.Focus();
        }

        private void dtReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNReceita.Focus();
        }

        private void txtNReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbConselho.Focus();
        }

        private void cmbConselho_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnOk.PerformClick();
            }

        }

        private void txtCrm_Validated(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtCrm.Text.Trim()))
                {
                    var dadosMedicos = new Medico();
                    List<Medico> retorno = dadosMedicos.BuscaDadosMedico(txtCrm.Text);
                    if (retorno.Count > 0)
                    {
                        txtNome.Text = retorno[0].MedNome;
                        cmbUF.Text = retorno[0].MedUf;
                        novoMedico = false;
                        dtReceita.Focus();
                    }
                    else
                    {
                        novoMedico = true;
                        MessageBox.Show("CRM não cadastrado!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtNome.Text = "";
                        cmbUF.SelectedIndex = -1;
                        cmbUF.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados da Receita", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenReceita_Load(object sender, EventArgs e)
        {
            try
            {
                dtReceita.Value = DateTime.Now;
                novoMedico = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados da Receita", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsiteCampos().Equals(true))
                {
                    if (novoMedico.Equals(true))
                    {
                        var dadosMedicos = new Medico();

                        dadosMedicos.MedNome = txtNome.Text;
                        dadosMedicos.MedCodigo = txtCrm.Text;
                        dadosMedicos.MedCrm = txtCrm.Text;
                        dadosMedicos.MedUf = cmbUF.Text;
                        dadosMedicos.DtCadastro = DateTime.Now;
                        dadosMedicos.OpCadastro = Principal.usuario;
                        dadosMedicos.MedID = Funcoes.IdentificaVerificaID("MEDICOS", "MED_ID");

                        dadosMedicos.InsereRegistros(dadosMedicos);
                    }

                    fVendas.wsCRM = txtCrm.Text.Trim();
                    fVendas.wsUF = cmbUF.Text;
                    fVendas.wsMNome = txtNome.Text.Trim().ToUpper();
                    fVendas.wsData = dtReceita.Value;
                    fVendas.wsNumReceita = txtNReceita.Text.Trim();
                    fVendas.wsConselho = cmbConselho.Text.Substring(0, 3);

                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Dados da Receita", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsiteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCrm.Text.Trim()))
            {
                Principal.mensagem = "Número do CRM não pode ser em branco.";
                Funcoes.Avisa();
                txtCrm.Focus();
                return false;
            }
            if (cmbUF.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar a UF.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNReceita.Text.Trim()))
            {
                Principal.mensagem = "Número da receita não pode ser em branco.";
                Funcoes.Avisa();
                txtNReceita.Focus();
                return false;
            }
            if (cmbConselho.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar o conselho profissional.";
                Funcoes.Avisa();
                cmbConselho.Focus();
                return false;
            }
            return true;

        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            fVendas.wsCRM = String.Empty;
            fVendas.wsUF = String.Empty;
            fVendas.wsMNome = String.Empty;
            fVendas.wsData = DateTime.Now;
            fVendas.wsNumReceita = String.Empty;
            fVendas.wsConselho = String.Empty;

            this.Close();
        }


        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancela.PerformClick();
                    break;
                case Keys.F2:
                    btnOk.PerformClick();
                    break;
            }
        }

        private void txtCrm_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbConselho_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Fechar (F1)");
        }

        private void btnOk_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnOk, "Confirmar (F2)");
        }

        private void frmVenReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
    }
}
