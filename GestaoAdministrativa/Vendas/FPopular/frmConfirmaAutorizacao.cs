﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Xml;
using GestaoAdministrativa.DAL;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using GestaoAdministrativa.WSFarmaciaPopular;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.FPopular;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmConfirmaAutorizacao : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        ServicoSolicitacaoWSClient wsFP = new ServicoSolicitacaoWSClient();
        private AutorizacaoDTO dtAutorizacao;
        private UsuarioFarmaciaDTO dtUsuario;
        
        public bool gravar;
        private int numLinhas;
        public string gridMedicamentos;
        private double totalSolicitacao = 0;
        private double totalMS = 0;
        private double totalCliente = 0;
        #endregion

        public frmConfirmaAutorizacao(AutorizacaoDTO autorizacao, UsuarioFarmaciaDTO usuario) {
            dtAutorizacao = autorizacao;
            dtUsuario = usuario;
            InitializeComponent();
        }
        
        private void frmConfirmaAutorizacao_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.Default;
                gravar = false;
                txtIdSolicitacao.Text = dtAutorizacao.coSolicitacaoFarmacia.ToString();
                txtNomeCliente.Text = dtAutorizacao.noPessoa.ToString();
                txtCodAutorizacao.Text = dtAutorizacao.inAutorizacaoSolicitacao.ToString();
                txtNumAutorizacao.Text = dtAutorizacao.nuAutorizacao.ToString();

                gridMedicamentos = "=============================  M E D I C A M E N T O S  =============================";
                
                numLinhas = dtAutorizacao.arrMedicamentoDTO.Length;

                for(int i = 0; i < numLinhas; i++)
                {
                    gridMedicamentos += "\r\n\r\n";
                    gridMedicamentos += "     Código de Barras:\t\t" + dtAutorizacao.arrMedicamentoDTO[i].coCodigoBarra.ToString();
                    gridMedicamentos += "\r\n";

                    gridMedicamentos += "     Quantidade Solicitada:\t\t" + dtAutorizacao.arrMedicamentoDTO[i].qtSolicitada.ToString();
                    gridMedicamentos += "\r\n";
                    
                    gridMedicamentos += "     Quantidade Autorizada:\t\t" + dtAutorizacao.arrMedicamentoDTO[i].qtAutorizada.ToString();
                    gridMedicamentos += "\r\n";

                    gridMedicamentos += "     Unidade Apresentada:\t\t" + dtAutorizacao.arrMedicamentoDTO[i].dsUnidApresentacao.ToString();
                    gridMedicamentos += "\r\n";
                    
                    gridMedicamentos += "     Autorização do Medicamento:\t" + dtAutorizacao.arrMedicamentoDTO[i].inAutorizacaoMedicamento.ToString();
                    gridMedicamentos += "\r\n";

                    gridMedicamentos += "     Preço Unitário do Medicamento:\t" + "R$ " + dtAutorizacao.arrMedicamentoDTO[i].vlPrecoVenda.ToString("0.00");
                    gridMedicamentos += "\r\n";

                    gridMedicamentos += "     Valor Subsidiado pelo MS:\t" + "R$ " + dtAutorizacao.arrMedicamentoDTO[i].vlPrecoSubsidiadoMS.ToString("0.00");
                    gridMedicamentos += "\r\n";

                    gridMedicamentos += "     Valor Subsidiado pelo Cliente:\t" + "R$ " + dtAutorizacao.arrMedicamentoDTO[i].vlPrecoSubsidiadoPaciente.ToString("0.00");
                    gridMedicamentos += "\r\n";



                    gridMedicamentos += "\r\n";
                    gridMedicamentos += "                                   --------------- # ---------- # ---------------";

                    totalSolicitacao += dtAutorizacao.arrMedicamentoDTO[i].vlPrecoVenda;
                    totalMS += dtAutorizacao.arrMedicamentoDTO[i].vlPrecoSubsidiadoMS;
                    totalCliente += dtAutorizacao.arrMedicamentoDTO[i].vlPrecoSubsidiadoPaciente;
                }

                txtMedicamentos.Text = gridMedicamentos;
                txtTotalSolicitacao.Text = "R$  " + totalSolicitacao.ToString("0.00");
                txtTotalMS.Text = "R$  " + totalMS.ToString("0.00");
                txtTotalCliente.Text = "R$  " + totalCliente.ToString("0.00");
                btnGravar.Enabled = false;
                btnAvancar.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 2 - Confirma Autorizacão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAvancar_Click(object sender, EventArgs e)
        {
            btnAvancar.Enabled = false;
            btnCancelar.Enabled = false;

            if (FarmaciaPopular.enviarConfirmacao(dtAutorizacao, dtUsuario))
            {
                MessageBox.Show("Solicitação Confirmada com Sucesso! \r\nPressione o botão CONCLUIR para retornar à tela de Venda!", "Solicitação Confirmada - Farmácia Popular");
                Principal.venBeneficio = "TRUE";
                btnGravar.Enabled = true;
                btnGravar.Focus();
            }
            else
            {
                MessageBox.Show("Falha ao realizar a comunicação com o Web Service. Tente novamente em alguns instantes!", "Falha na Confirmação - Farmácia Popular");
                Principal.venBeneficio = "FALSE";
                btnCancelar.Enabled = true;
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.venBeneficio = "TRUE";
                this.Hide();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 2 - Confirma Autorizacão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza de que deseja cancelar a solicitação?", "Farmácia Popular", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                Principal.venBeneficio = "FALSE";
                this.Close();
            }
        }
        
        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmConfirmaAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
        
        private void btnGravar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
        
        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Principal.venBeneficio = "FALSE";
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F2:
                    btnAvancar.PerformClick();
                    break;
                case Keys.F3:
                    btnGravar.PerformClick();
                    break;
            }
            
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancelar, "Cancelar (F1)");
        }

        private void btnGravar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnGravar, "Gravar (F2)");
        }

        private void txtNomeCliente_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtIdSolicitacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNumAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTotalSolicitacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTotalMS_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTotalCliente_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
        
    }
}
