﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Xml;
using GestaoAdministrativa.DAL;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using GestaoAdministrativa.WSFarmaciaPopular;
using GestaoAdministrativa.Geral;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmExecutaSolicitacao : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS

        public bool gravar;
        private DataTable dtDadosAcesso;
        private DataTable dtMedicamentos;
        private DataTable dtDadosSolicitacao = new DataTable();
        DataColumn dc;
        frmVenda fvenda;
        private string idVendedor;
        private bool retorno;
        
        #endregion
        
        public frmExecutaSolicitacao(string idVendedor, DataTable medicamentos, frmVenda venda) {
            this.idVendedor = idVendedor;
            dtMedicamentos = medicamentos;
            fvenda = venda;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }
        
        private void frmExecutaSolicitacao_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.Default;
                gravar = false;
                dtDadosAcesso = DALFPopular.dadosAcessoFP(idVendedor);
                txtCnpjFarmacia.Text = dtDadosAcesso.Rows[0][2].ToString();
                txtCpfVendedor.Text = dtDadosAcesso.Rows[0][3].ToString();
                dtpDataReceita.Format = DateTimePickerFormat.Custom;
                dtpDataReceita.CustomFormat = "dd/MM/yyyy";
                dc = new DataColumn("paramSolicitacaoFP", typeof(String));
                dtDadosSolicitacao.Columns.Add(dc);
                txtCpfCliente.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 1 - Executa Solicitação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public bool Gravar()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    dtDadosSolicitacao.Rows.Clear();
                    dtDadosSolicitacao.Rows.Add(txtCpfCliente.Text);
                    dtDadosSolicitacao.Rows.Add(txtNomeCliente.Text);
                    dtDadosSolicitacao.Rows.Add(dtpDataReceita.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    dtDadosSolicitacao.Rows.Add(txtCrmMedico.Text);
                    dtDadosSolicitacao.Rows.Add(cmbUFMedico.SelectedItem.ToString());

                    return true;
                    
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 1 - Executa Solicitação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Gravar().Equals(true))
                {
                    gravar = true;
                    if (FarmaciaPopular.transmitirSolicitacao(dtDadosAcesso, dtMedicamentos, dtDadosSolicitacao, out fvenda.vendaNumeroNSU))
                    {
                        Principal.venBeneficio = "TRUE";
                        fvenda.vendaBeneficio = "FARMACIAPOPULAR";
                        this.Hide();
                        this.Close();
                    }
                    else
                    {
                        Principal.venBeneficio = "FALSE";
                        fvenda.vendaBeneficio = "";
                        fvenda.vendaNumeroNSU = "";
                        fvenda.validouFP = false;
                    }
                }
                else
                {
                    gravar = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 1 - Executa Solicitação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            try
            {
                if (gravar.Equals(false))
                {
                    Principal.venBeneficio = "FALSE";
                    Principal.SolicitacaoID = "0";
                    fvenda.validouFP = false;
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Passo 1 - Executa Solicitação", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsisteCampos()
        {

            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCpfCliente.Text.Replace("-", "").Replace(".", "").Trim()))
            {
                Principal.mensagem = "O CPF não pode estar em branco.";
                Funcoes.Avisa();
                txtCpfCliente.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNomeCliente.Text.Trim()))
            {
                Principal.mensagem = "O nome do cliente não pode estar em branco.";
                Funcoes.Avisa();
                txtNomeCliente.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCrmMedico.Text.Trim()))
            {
                Principal.mensagem = "O CRM não pode estar em branco.";
                Funcoes.Avisa();
                txtCrmMedico.Focus();
                return false;
            }
            if (cmbUFMedico.SelectedIndex.Equals(-1))
            {
                Principal.mensagem = "É necessário selecionar o estado do CRM.";
                Funcoes.Avisa();
                cmbUFMedico.Focus();
                return false;
            }

            return true;
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmExecutaSolicitacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnGravar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmExecutaSolicitacao_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (gravar.Equals(false))
            {
                Principal.SolicitacaoID = "0";
            }
        }
        
        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fvenda.validouFP = false;
                    fvenda.vendaNumeroNSU = "";
                    fvenda.vendaBeneficio = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F2:
                    btnGravar.PerformClick();
                    break;
            }
            
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancelar, "Cancelar (F1)");
        }

        private void btnGravar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnGravar, "Gravar (F2)");
        }

        private void txtCpfCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if(e.KeyChar == (char)13)
            {
                Principal.doctoCliente = txtCpfCliente.Text;
                var dadosCli = new Cliente();
                DataTable cliente = dadosCli.BuscaClientePorCPF(txtCpfCliente.Text);

                if(cliente.Rows.Count == 0)
                {
                    if(Funcoes.LeParametro(6,"358", false).Equals("S"))
                    {
                        MessageBox.Show("O cpf do cliente não está cadastrado no sistema.\r\nPressione 'OK' para realizar o cadastro!", "Farmácia Popular");
                        frmVenCadastroRapidoCliente frmCadastro = new frmVenCadastroRapidoCliente(fvenda);
                        frmCadastro.ShowDialog();
                        txtNomeCliente.Text = Principal.nomeCliFor;
                        dtpDataReceita.Focus();
                    }
                    else
                    {
                        txtNomeCliente.Focus();
                    }
                }
                else
                {
                    Principal.idCliFor = cliente.Rows[0][0].ToString();
                    txtNomeCliente.Text = cliente.Rows[0][1].ToString();
                    dtpDataReceita.Focus();
                }
            }
        }

        private void txtCrmMedico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)13)
            {
                var buscaMedico = new Medico();

                DataTable medico = buscaMedico.BuscaDados(txtCrmMedico.Text, "");
                if(medico.Rows.Count == 0)
                { 
                    cmbUFMedico.Focus();
                }
                else if (medico.Rows.Count == 1)
                {
                    txtCrmMedico.Text = medico.Rows[0][0].ToString();
                    txtNome.Text = medico.Rows[0][1].ToString();
                    if (String.IsNullOrEmpty(medico.Rows[0][3].ToString()))
                    {
                        cmbUFMedico.Focus();
                    }
                    else
                    {
                        cmbUFMedico.Text = medico.Rows[0][3].ToString();
                        btnGravar.Focus();
                    }
                }
                else
                {
                    using (frmBuscaMedico medicos = new frmBuscaMedico())
                    {
                        medicos.dgMedicos.DataSource = medico;
                        medicos.ShowDialog();
                    }
                    txtNome.Text = Principal.medico;
                    cmbUFMedico.Text = Principal.uf;
                    txtCrmMedico.Text = Principal.crm;
                    btnGravar.Focus();
                }
            }
        }

        private void txtCnpjFarmacia_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCpfVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCpfCliente_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNomeCliente_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtpDataReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCrmMedico_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUFMedico_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtpDataReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCrmMedico.Focus();
        }

        private void txtNomeCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                dtpDataReceita.Focus();
            }
        }

        private void txtNomeCliente_Validated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtNomeCliente.Text))
            {
                txtNomeCliente.Focus();
            }
            else
            {
                var dadosCli = new Cliente();
                DataTable cliente = dadosCli.BuscaClientePorCPF(txtCpfCliente.Text);
                int id;

                if (cliente.Rows.Count == 0)
                {
                    var dadosCliente = new Cliente
                    {
                        CfDocto = txtCpfCliente.Text,
                        CfTipoDocto = 0,
                        CfNome = txtNomeCliente.Text,
                        CfEndereco = ".",
                        CfNumeroEndereco = ".",
                        CfBairro = ".",
                        CfCEP = "0",
                        CfUF = ".",
                        CfCidade = ".",
                        CfStatus = 4,
                        CfEnderecoCobranca = ".",
                        CfNumeroCobranca = ".",
                        CfBairroCobranca = ".",
                        CfCEPCobranca = "0",
                        CfCidadeCobranca = ".",
                        CfUFCobranca = ".",
                        CfEnderecoEntrega = ".",
                        CfNumeroEntrega = ".",
                        CfBairroEntrega = ".",
                        CfCEPEntrega = "0",
                        CfCidadeEntrega = ".",
                        CfUFEntrega = ".",
                        CfPais = "BRASIL",
                        CfLiberado = "N",
                        CodigoConveniada = 0,
                        NumCartao = null,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };
                    if (!dadosCliente.IncluiClienteConsultaCatrtao(dadosCliente, out id))
                        return;

                    Principal.idCliFor = id.ToString();
                    dtpDataReceita.Focus();
                }
                else
                {
                    Principal.idCliFor = cliente.Rows[0][0].ToString();
                    txtNomeCliente.Text = cliente.Rows[0][1].ToString();
                    dtpDataReceita.Focus();
                }
            }
        }

        private void gbEnviarSolicitacao_Enter(object sender, EventArgs e)
        {

        }
    }
}
