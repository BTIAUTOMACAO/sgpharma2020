﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Vendas;
using System.Threading;

namespace GestaoAdministrativa.FPopular
{
    public partial class frmQtdMedicamentoFP : Form
    {
        int i;
        DataTable dtMedicamentos;
        frmVenda fVendas;
        frmVenBeneficios fBeneficios;

        public frmQtdMedicamentoFP(int indice, DataTable medicamentos, frmVenda frmVenda)
        {
            i = indice;
            dtMedicamentos = medicamentos;
            InitializeComponent();
            fVendas = frmVenda;
            RegisterFocusEvents(this.Controls);
        }

        private void frmQtdMedicamentoFP_Load(object sender, EventArgs e)
        {
            try
            {
                txtNomeMedicamento.Text = dtMedicamentos.Rows[i][1].ToString();

                if (DAL.DALFPopular.getPrecoMedicamentoFP(dtMedicamentos.Rows[i][0].ToString()).Equals(0))
                {
                    txtPreco.Text = dtMedicamentos.Rows[i][4].ToString();
                }
                else
                {
                    txtPreco.Text = DAL.DALFPopular.getPrecoMedicamentoFP(dtMedicamentos.Rows[i][0].ToString()).ToString("0.00");
                }

                if (DAL.DALFPopular.getQtdMedicamentoFP(dtMedicamentos.Rows[i][0].ToString()).Equals(0))
                {
                    MessageBox.Show("Necessario corrigir cadastro do Produto campo QTDE POR UNIDADE", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    fVendas.validouFP = false;
                    this.Close();
                }
                else
                {
                    string qtdMedicamento = DAL.DALFPopular.getQtdMedicamentoFP(dtMedicamentos.Rows[i][0].ToString()).ToString("0");
                    string qtdCaixas = dtMedicamentos.Rows[i][3].ToString();
                    int totalSolicitado = Convert.ToInt16(qtdMedicamento) * Convert.ToInt16(qtdCaixas);
                    txtQtdSolicitada.Text = totalSolicitado.ToString();
                    txtPosologia.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                fVendas.validouFP = false;
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtQtdSolicitada.Text)))
                {
                    MessageBox.Show("A quantidade solicitada pelo cliente não pode estar em branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fVendas.validouFP = false;
                    txtQtdSolicitada.Focus();
                }
                else if (String.IsNullOrEmpty(txtPosologia.Text.Trim()))
                {
                    MessageBox.Show("Informe a quantidade diária prescrita.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fVendas.validouFP = false;
                    txtPosologia.Focus();
                }
                else if (String.IsNullOrEmpty(txtPreco.Text.Trim()))
                {
                    MessageBox.Show("Informe o preço do medicamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fVendas.validouFP = false;
                    txtPreco.Focus();
                }
                else
                {
                    fVendas.validouFP = true;
                    dtMedicamentos.Rows[i][2] = txtQtdSolicitada.Text;
                    dtMedicamentos.Rows[i][3] = txtPosologia.Text;
                    dtMedicamentos.Rows[i][4] = txtPreco.Text;
                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                fVendas.validouFP = false;
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void frmFPopVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                fVendas.validouFP = false;
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void txtIDVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void txtPosologia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPreco.Focus();

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)27)
            {
                fBeneficios.abortou = true;
                this.Close();
            }
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();

            if (e.KeyChar == (char)27)
            {
                fBeneficios.abortou = true;
                this.Close();
            }
        }

        private void txtQtdSolicitada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPosologia.Focus();

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == (char)27)
            {
                fBeneficios.abortou = true;
                this.Close();
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtPreco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                fVendas.validouFP = false;
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void txtQtdSolicitada_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                fVendas.validouFP = false;
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void btnConfirmar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConfirmar, "Confirmar (F1)");
        }

        private void txtPosologia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                fVendas.validouFP = false;
                this.Close();
            }
            else if (e.KeyCode == Keys.F1)
            {
                btnConfirmar.PerformClick();
            }
        }
    }
}
