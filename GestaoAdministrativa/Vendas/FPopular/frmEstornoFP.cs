﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.WSFarmaciaPopular;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.FPopular
{
    public partial class frmEstornoFP : Form, Botoes
    {
        private ToolStripButton tsbEstorno = new ToolStripButton("Estorno Manual");
        private ToolStripSeparator tssEstorno = new ToolStripSeparator();
        private string ID;

        public frmEstornoFP(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEstorno);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEstorno);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEstorno.AutoSize = false;
                this.tsbEstorno.Image = Properties.Resources.vendas;
                this.tsbEstorno.Size = new System.Drawing.Size(150, 20);
                this.tsbEstorno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEstorno);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEstorno);
                tsbEstorno.Click += delegate
                {
                    var ajuste = Application.OpenForms.OfType<frmEstornoFP>().FirstOrDefault();
                    Funcoes.BotoesCadastro(ajuste.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    ajuste.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmEstornoFP_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    //btnBuscar.PerformClick();
                    break;
            }
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }
                    
                    txtQtde.Text = "0";
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();
                    txtQtde.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtQtde.Text))
                {
                    btnAdicionar.Focus();
                }
                else
                {
                    MessageBox.Show("Número não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtQtde.Focus();
                }
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()) && String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Produto não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if ((Convert.ToInt32(txtQtde.Text) == 0))
            {
                Principal.mensagem = "Quantidade não pode ser Zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            return true;
        }


        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if(ConsisteCampos())
                {
                    dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { txtCodBarras.Text, txtDescr.Text, txtQtde.Text });
                    txtCodBarras.Text = "";
                    txtDescr.Text = "";
                    txtQtde.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            txtAutorizacao.Text = "";
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtQtde.Text = "0";
            dgProdutos.Rows.Clear();
            txtAutorizacao.Focus();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtAutorizacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void txtAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnEstorno_Click(object sender, EventArgs e)
        {
            try
            {
                if(String.IsNullOrEmpty(txtAutorizacao.Text))
                {
                    MessageBox.Show("Autorização não pode ser em Branco!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAutorizacao.Focus();
                }
                else if(dgProdutos.RowCount == 0)
                {
                    MessageBox.Show("Necessário ao menos um Produto para fazer estorno!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProdutos.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;
                    List<MedicamentoDTO> medicamentos = new List<MedicamentoDTO>();

                    for(int i=0; i<dgProdutos.RowCount; i++)
                    {
                        var dados = new MedicamentoDTO();
                        dados.coCodigoBarra = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString();
                        dados.qtDevolvida = Convert.ToInt32(dgProdutos.Rows[i].Cells["QTDE"].Value);
                        medicamentos.Add(dados);
                    }

                    if(FarmaciaPopular.estornoManual(txtAutorizacao.Text.Trim(), medicamentos))
                    {
                        MessageBox.Show("Estorno Realizado com Sucesso!", "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Manual", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
