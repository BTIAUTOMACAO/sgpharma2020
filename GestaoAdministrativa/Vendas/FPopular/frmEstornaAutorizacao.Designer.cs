﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmEstornaAutorizacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstornaAutorizacao));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnEstorno = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtDataFinal = new System.Windows.Forms.DateTimePicker();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.txtAutorizacao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCPFCliente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDataInicial = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgCancelar = new System.Windows.Forms.DataGridView();
            this.dsRelatorioAnalitico1 = new GestaoAdministrativa.Caixa.dsRelatorioAnalitico();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.CHKESTORNO = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NUMEROAUTORIZACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPFCLIENTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATAVENDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALCLIENTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTALVENDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorioAnalitico1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEstorno
            // 
            this.btnEstorno.BackColor = System.Drawing.Color.White;
            this.btnEstorno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstorno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstorno.Image = ((System.Drawing.Image)(resources.GetObject("btnEstorno.Image")));
            this.btnEstorno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstorno.Location = new System.Drawing.Point(738, 442);
            this.btnEstorno.Name = "btnEstorno";
            this.btnEstorno.Size = new System.Drawing.Size(217, 87);
            this.btnEstorno.TabIndex = 3;
            this.btnEstorno.Text = "Estornar Autorização";
            this.btnEstorno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstorno.UseVisualStyleBackColor = false;
            this.btnEstorno.Click += new System.EventHandler(this.btnEstorno_Click);
            this.btnEstorno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEstorno_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.dtDataFinal);
            this.groupBox3.Controls.Add(this.btnFiltrar);
            this.groupBox3.Controls.Add(this.txtAutorizacao);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtCPFCliente);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.dtDataInicial);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(9, 436);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(563, 93);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtro";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Data Final";
            // 
            // dtDataFinal
            // 
            this.dtDataFinal.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDataFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataFinal.Location = new System.Drawing.Point(92, 54);
            this.dtDataFinal.Name = "dtDataFinal";
            this.dtDataFinal.Size = new System.Drawing.Size(102, 22);
            this.dtDataFinal.TabIndex = 7;
            this.dtDataFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtDataFinal_KeyDown);
            this.dtDataFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtDataFinal_KeyPress);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.BackColor = System.Drawing.Color.White;
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ForeColor = System.Drawing.Color.Black;
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFiltrar.Location = new System.Drawing.Point(476, 17);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(79, 61);
            this.btnFiltrar.TabIndex = 6;
            this.btnFiltrar.Text = "Pesquisar";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFiltrar.UseVisualStyleBackColor = false;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // txtAutorizacao
            // 
            this.txtAutorizacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAutorizacao.Location = new System.Drawing.Point(320, 54);
            this.txtAutorizacao.Name = "txtAutorizacao";
            this.txtAutorizacao.Size = new System.Drawing.Size(137, 22);
            this.txtAutorizacao.TabIndex = 5;
            this.txtAutorizacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAutorizacao_KeyDown);
            this.txtAutorizacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAutorizacao_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(214, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "CPF Cliente";
            // 
            // txtCPFCliente
            // 
            this.txtCPFCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPFCliente.Location = new System.Drawing.Point(320, 21);
            this.txtCPFCliente.Name = "txtCPFCliente";
            this.txtCPFCliente.Size = new System.Drawing.Size(137, 22);
            this.txtCPFCliente.TabIndex = 3;
            this.txtCPFCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPFCliente_KeyDown);
            this.txtCPFCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPFCliente_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(214, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nº Autorização";
            // 
            // dtDataInicial
            // 
            this.dtDataInicial.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDataInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataInicial.Location = new System.Drawing.Point(92, 21);
            this.dtDataInicial.Name = "dtDataInicial";
            this.dtDataInicial.Size = new System.Drawing.Size(102, 22);
            this.dtDataInicial.TabIndex = 1;
            this.dtDataInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtData_KeyDown);
            this.dtDataInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Inicial";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgCancelar);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(9, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(946, 393);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vendas Farmácia Popular";
            // 
            // dgCancelar
            // 
            this.dgCancelar.AllowUserToAddRows = false;
            this.dgCancelar.AllowUserToDeleteRows = false;
            this.dgCancelar.AllowUserToResizeColumns = false;
            this.dgCancelar.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCancelar.BackgroundColor = System.Drawing.Color.White;
            this.dgCancelar.CausesValidation = false;
            this.dgCancelar.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgCancelar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCancelar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CHKESTORNO,
            this.NUMEROAUTORIZACAO,
            this.VENDA_ID,
            this.CPFCLIENTE,
            this.DATAVENDA,
            this.TOTALMS,
            this.TOTALCLIENTE,
            this.TOTALVENDA});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCancelar.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgCancelar.GridColor = System.Drawing.Color.Black;
            this.dgCancelar.Location = new System.Drawing.Point(6, 21);
            this.dgCancelar.Name = "dgCancelar";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelar.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgCancelar.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelar.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgCancelar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCancelar.Size = new System.Drawing.Size(934, 366);
            this.dgCancelar.TabIndex = 0;
            this.dgCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCancelar_KeyDown);
            // 
            // dsRelatorioAnalitico1
            // 
            this.dsRelatorioAnalitico1.DataSetName = "dsRelatorioAnalitico";
            this.dsRelatorioAnalitico1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.btnEstorno);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(961, 560);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(961, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(184, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Estorno Famácia Popular";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(955, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // CHKESTORNO
            // 
            this.CHKESTORNO.DataPropertyName = "CHKESTORNO";
            this.CHKESTORNO.FalseValue = "false";
            this.CHKESTORNO.HeaderText = "";
            this.CHKESTORNO.Name = "CHKESTORNO";
            this.CHKESTORNO.TrueValue = "true";
            this.CHKESTORNO.Visible = false;
            this.CHKESTORNO.Width = 20;
            // 
            // NUMEROAUTORIZACAO
            // 
            this.NUMEROAUTORIZACAO.DataPropertyName = "NUMEROAUTORIZACAO";
            this.NUMEROAUTORIZACAO.HeaderText = "Número da Autorização";
            this.NUMEROAUTORIZACAO.Name = "NUMEROAUTORIZACAO";
            this.NUMEROAUTORIZACAO.Width = 190;
            // 
            // VENDA_ID
            // 
            this.VENDA_ID.DataPropertyName = "VENDA_ID";
            this.VENDA_ID.HeaderText = "ID da Venda";
            this.VENDA_ID.Name = "VENDA_ID";
            this.VENDA_ID.ReadOnly = true;
            this.VENDA_ID.Visible = false;
            this.VENDA_ID.Width = 110;
            // 
            // CPFCLIENTE
            // 
            this.CPFCLIENTE.DataPropertyName = "CPFCLIENTE";
            this.CPFCLIENTE.HeaderText = "CPF do Cliente";
            this.CPFCLIENTE.Name = "CPFCLIENTE";
            this.CPFCLIENTE.ReadOnly = true;
            this.CPFCLIENTE.Width = 150;
            // 
            // DATAVENDA
            // 
            this.DATAVENDA.DataPropertyName = "DATAVENDA";
            this.DATAVENDA.HeaderText = "Data da Venda";
            this.DATAVENDA.Name = "DATAVENDA";
            this.DATAVENDA.Width = 150;
            // 
            // TOTALMS
            // 
            this.TOTALMS.DataPropertyName = "TOTALMS";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.TOTALMS.DefaultCellStyle = dataGridViewCellStyle3;
            this.TOTALMS.HeaderText = "Total do MS";
            this.TOTALMS.Name = "TOTALMS";
            this.TOTALMS.ReadOnly = true;
            this.TOTALMS.Width = 108;
            // 
            // TOTALCLIENTE
            // 
            this.TOTALCLIENTE.DataPropertyName = "TOTALCLIENTE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.TOTALCLIENTE.DefaultCellStyle = dataGridViewCellStyle4;
            this.TOTALCLIENTE.HeaderText = "Total do Cliente";
            this.TOTALCLIENTE.Name = "TOTALCLIENTE";
            this.TOTALCLIENTE.ReadOnly = true;
            this.TOTALCLIENTE.Width = 150;
            // 
            // TOTALVENDA
            // 
            this.TOTALVENDA.DataPropertyName = "TOTALVENDA";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.TOTALVENDA.DefaultCellStyle = dataGridViewCellStyle5;
            this.TOTALVENDA.HeaderText = "Valor da Venda";
            this.TOTALVENDA.Name = "TOTALVENDA";
            this.TOTALVENDA.ReadOnly = true;
            this.TOTALVENDA.Width = 150;
            // 
            // frmEstornaAutorizacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstornaAutorizacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Cancelamento de Vendas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEstornaAutorizacao_Load);
            this.Shown += new System.EventHandler(this.frmEstornaAutorizacao_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEstornaAutorizacao_KeyDown);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsRelatorioAnalitico1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgCancelar;
        private System.Windows.Forms.TextBox txtAutorizacao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCPFCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDataInicial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEstorno;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtDataFinal;
        private Caixa.dsRelatorioAnalitico dsRelatorioAnalitico1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CHKESTORNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMEROAUTORIZACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPFCLIENTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATAVENDA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALCLIENTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTALVENDA;
    }
}