﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmEstornaAutorizacao: Form, Botoes
    {
        private ToolStripButton tsbFPopular = new ToolStripButton("Estorno FPopular");
        private ToolStripSeparator tssFPopular = new ToolStripSeparator();
        VendasDados vendaFiltro = new VendasDados();
        DataTable dtRetorno;

        public frmEstornaAutorizacao(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmEstornaAutorizacao_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dtDataInicial.Value = DateTime.Now.AddDays(-14);
                dtDataFinal.Value = DateTime.Now;
                txtCPFCliente.MaxLength = 11;
                txtAutorizacao.MaxLength = 15;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtDataFinal.Focus();
        }

        private void dtDataFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCPFCliente.Focus();
        }

        private void txtCPFCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtAutorizacao.Focus();

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtAutorizacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.PerformClick();

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnEstorno.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void frmEstornaAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtData_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtDataFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCPFCliente_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnEstorno_Click(object sender, EventArgs e)
        {
            if (dgCancelar.RowCount > 0)
            {
                if (MessageBox.Show("O estorno através desta tela não emite comprovantes de cancelamento de vendas. Para tal, utilize a tela de Cancelamento de Vendas!\r\nPara prosseguir, pressione o botão SIM.", "Estorno Farmácia Popular", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        if (IdentificaRegistroSelecionado())
                        {
                            for (int i = 0; i < dgCancelar.RowCount; i++)
                            {
                                if (dgCancelar.Rows[i].Selected == true)
                                {
                                    if (FarmaciaPopular.estornarSolicitacao(dgCancelar.Rows[i].Cells[1].Value.ToString()))
                                    {
                                        MessageBox.Show("Estorno realizado com sucesso! \r\nPara realizar uma nova solicitação, utilize a Tela de Vendas!", "Estorno Farmácia Popular");
                                        btnFiltrar_Click(sender, e);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Erro ao cancelar a solicitação!", "Estorno Farmácia Popular");
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("É necessário selecionar ao menos um registro para realizar o cancelamento!", "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (dgCancelar.RowCount > 0)
                            {
                                dgCancelar.Focus();
                            }
                            else
                            {
                                dtDataInicial.Focus();
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro: " + ex.Message, "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Nenhuma Venda para ser Estornada!", "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        
        private void btnEstorno_Click(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmEstornaAutorizacao_Shown(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                dtRetorno = DAL.DALFPopular.getVendasFP(dtDataInicial.Value, dtDataFinal.Value);
                dgCancelar.DataSource = dtRetorno;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                dtRetorno = DAL.DALFPopular.getVendasFP(dtDataInicial.Value, dtDataFinal.Value, txtCPFCliente.Text, txtAutorizacao.Text);
                dgCancelar.DataSource = dtRetorno;
            }
            finally
            {
                Cursor = Cursors.Default;
            }


        }

        public bool IdentificaRegistroSelecionado()
        {
            bool selecionouLinha = false;

            foreach (DataGridViewRow row in dgCancelar.Rows)
            {
                if (row.Selected == true)
                {
                    selecionouLinha = true;
                }
            }
            return selecionouLinha;
        }


        public void Botao()
        {
            try
            {
                this.tsbFPopular.AutoSize = false;
                this.tsbFPopular.Image = Properties.Resources.remover;
                this.tsbFPopular.Size = new System.Drawing.Size(130, 20);
                this.tsbFPopular.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbFPopular);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssFPopular);
                tsbFPopular.Click += delegate
                {
                    var estorno = Application.OpenForms.OfType<frmEstornaAutorizacao>().FirstOrDefault();
                    Funcoes.BotoesCadastro(estorno.Name);
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    estorno.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbFPopular);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssFPopular);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estorno Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
