﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmConfirmaAutorizacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfirmaAutorizacao));
            this.gbEnviarSolicitacao = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotalCliente = new System.Windows.Forms.TextBox();
            this.txtTotalMS = new System.Windows.Forms.TextBox();
            this.txtTotalSolicitacao = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMedicamentos = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIdSolicitacao = new System.Windows.Forms.MaskedTextBox();
            this.txtCodAutorizacao = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNumAutorizacao = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomeCliente = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAvancar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.gbEnviarSolicitacao.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbEnviarSolicitacao
            // 
            this.gbEnviarSolicitacao.Controls.Add(this.panel1);
            this.gbEnviarSolicitacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEnviarSolicitacao.ForeColor = System.Drawing.Color.White;
            this.gbEnviarSolicitacao.Location = new System.Drawing.Point(5, 3);
            this.gbEnviarSolicitacao.Name = "gbEnviarSolicitacao";
            this.gbEnviarSolicitacao.Size = new System.Drawing.Size(718, 454);
            this.gbEnviarSolicitacao.TabIndex = 0;
            this.gbEnviarSolicitacao.TabStop = false;
            this.gbEnviarSolicitacao.Text = "Confirmação de Autorização";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnGravar);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnAvancar);
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Location = new System.Drawing.Point(6, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(706, 427);
            this.panel1.TabIndex = 1;
            // 
            // btnGravar
            // 
            this.btnGravar.BackColor = System.Drawing.Color.White;
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.ForeColor = System.Drawing.Color.Black;
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(490, 377);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(210, 44);
            this.btnGravar.TabIndex = 15;
            this.btnGravar.Text = "Concluir (F3)";
            this.btnGravar.UseVisualStyleBackColor = false;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            this.btnGravar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnGravar_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTotalCliente);
            this.groupBox1.Controls.Add(this.txtTotalMS);
            this.groupBox1.Controls.Add(this.txtTotalSolicitacao);
            this.groupBox1.Location = new System.Drawing.Point(6, 295);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(694, 76);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resumo da Solicitação";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(543, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Custo para o Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(262, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Valor Custeado pelo MS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(13, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Preço do Pedido";
            // 
            // txtTotalCliente
            // 
            this.txtTotalCliente.BackColor = System.Drawing.Color.White;
            this.txtTotalCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalCliente.ForeColor = System.Drawing.Color.Black;
            this.txtTotalCliente.Location = new System.Drawing.Point(546, 42);
            this.txtTotalCliente.Name = "txtTotalCliente";
            this.txtTotalCliente.ReadOnly = true;
            this.txtTotalCliente.Size = new System.Drawing.Size(134, 22);
            this.txtTotalCliente.TabIndex = 2;
            this.txtTotalCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTotalCliente_KeyDown);
            // 
            // txtTotalMS
            // 
            this.txtTotalMS.BackColor = System.Drawing.Color.White;
            this.txtTotalMS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMS.ForeColor = System.Drawing.Color.Black;
            this.txtTotalMS.Location = new System.Drawing.Point(263, 42);
            this.txtTotalMS.Name = "txtTotalMS";
            this.txtTotalMS.ReadOnly = true;
            this.txtTotalMS.Size = new System.Drawing.Size(159, 22);
            this.txtTotalMS.TabIndex = 1;
            this.txtTotalMS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalMS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTotalMS_KeyDown);
            // 
            // txtTotalSolicitacao
            // 
            this.txtTotalSolicitacao.BackColor = System.Drawing.Color.White;
            this.txtTotalSolicitacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalSolicitacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalSolicitacao.ForeColor = System.Drawing.Color.Black;
            this.txtTotalSolicitacao.Location = new System.Drawing.Point(16, 42);
            this.txtTotalSolicitacao.Name = "txtTotalSolicitacao";
            this.txtTotalSolicitacao.ReadOnly = true;
            this.txtTotalSolicitacao.Size = new System.Drawing.Size(123, 22);
            this.txtTotalSolicitacao.TabIndex = 0;
            this.txtTotalSolicitacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalSolicitacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTotalSolicitacao_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtMedicamentos);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(694, 215);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detalhamento dos Produtos";
            // 
            // txtMedicamentos
            // 
            this.txtMedicamentos.BackColor = System.Drawing.Color.White;
            this.txtMedicamentos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicamentos.ForeColor = System.Drawing.Color.Black;
            this.txtMedicamentos.Location = new System.Drawing.Point(16, 21);
            this.txtMedicamentos.Multiline = true;
            this.txtMedicamentos.Name = "txtMedicamentos";
            this.txtMedicamentos.ReadOnly = true;
            this.txtMedicamentos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMedicamentos.Size = new System.Drawing.Size(664, 182);
            this.txtMedicamentos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtIdSolicitacao);
            this.groupBox2.Controls.Add(this.txtCodAutorizacao);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtNumAutorizacao);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNomeCliente);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 79);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da Solicitação";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(255, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "ID da Requisição";
            // 
            // txtIdSolicitacao
            // 
            this.txtIdSolicitacao.BackColor = System.Drawing.Color.White;
            this.txtIdSolicitacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIdSolicitacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdSolicitacao.ForeColor = System.Drawing.Color.Black;
            this.txtIdSolicitacao.Location = new System.Drawing.Point(259, 44);
            this.txtIdSolicitacao.Name = "txtIdSolicitacao";
            this.txtIdSolicitacao.ReadOnly = true;
            this.txtIdSolicitacao.Size = new System.Drawing.Size(121, 22);
            this.txtIdSolicitacao.TabIndex = 7;
            this.txtIdSolicitacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtIdSolicitacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdSolicitacao_KeyDown);
            // 
            // txtCodAutorizacao
            // 
            this.txtCodAutorizacao.BackColor = System.Drawing.Color.White;
            this.txtCodAutorizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodAutorizacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodAutorizacao.ForeColor = System.Drawing.Color.Black;
            this.txtCodAutorizacao.Location = new System.Drawing.Point(399, 44);
            this.txtCodAutorizacao.Name = "txtCodAutorizacao";
            this.txtCodAutorizacao.ReadOnly = true;
            this.txtCodAutorizacao.Size = new System.Drawing.Size(122, 22);
            this.txtCodAutorizacao.TabIndex = 6;
            this.txtCodAutorizacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodAutorizacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodAutorizacao_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(396, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cod. de Retorno";
            // 
            // txtNumAutorizacao
            // 
            this.txtNumAutorizacao.BackColor = System.Drawing.Color.White;
            this.txtNumAutorizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumAutorizacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumAutorizacao.ForeColor = System.Drawing.Color.Black;
            this.txtNumAutorizacao.Location = new System.Drawing.Point(540, 44);
            this.txtNumAutorizacao.Mask = "000,000,000,000,000";
            this.txtNumAutorizacao.Name = "txtNumAutorizacao";
            this.txtNumAutorizacao.ReadOnly = true;
            this.txtNumAutorizacao.Size = new System.Drawing.Size(140, 22);
            this.txtNumAutorizacao.TabIndex = 4;
            this.txtNumAutorizacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNumAutorizacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumAutorizacao_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(537, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nº Autorização";
            // 
            // txtNomeCliente
            // 
            this.txtNomeCliente.BackColor = System.Drawing.Color.White;
            this.txtNomeCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeCliente.ForeColor = System.Drawing.Color.Black;
            this.txtNomeCliente.Location = new System.Drawing.Point(16, 44);
            this.txtNomeCliente.Name = "txtNomeCliente";
            this.txtNomeCliente.ReadOnly = true;
            this.txtNomeCliente.Size = new System.Drawing.Size(230, 22);
            this.txtNomeCliente.TabIndex = 2;
            this.txtNomeCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNomeCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNomeCliente_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome do Cliente";
            // 
            // btnAvancar
            // 
            this.btnAvancar.BackColor = System.Drawing.Color.White;
            this.btnAvancar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAvancar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAvancar.ForeColor = System.Drawing.Color.Black;
            this.btnAvancar.Image = ((System.Drawing.Image)(resources.GetObject("btnAvancar.Image")));
            this.btnAvancar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAvancar.Location = new System.Drawing.Point(245, 377);
            this.btnAvancar.Name = "btnAvancar";
            this.btnAvancar.Size = new System.Drawing.Size(210, 44);
            this.btnAvancar.TabIndex = 8;
            this.btnAvancar.Text = "   Prosseguir com a   Confirmação (F2)";
            this.btnAvancar.UseVisualStyleBackColor = false;
            this.btnAvancar.Click += new System.EventHandler(this.btnAvancar_Click);
            this.btnAvancar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnGravar_KeyDown);
            this.btnAvancar.MouseHover += new System.EventHandler(this.btnGravar_MouseHover);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.White;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(6, 377);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(210, 44);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar (F1)";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancela_Click);
            this.btnCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancela_KeyDown);
            this.btnCancelar.MouseHover += new System.EventHandler(this.btnCancela_MouseHover);
            // 
            // frmConfirmaAutorizacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(726, 460);
            this.Controls.Add(this.gbEnviarSolicitacao);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfirmaAutorizacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmConfirmaAutorizacao_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmConfirmaAutorizacao_KeyDown);
            this.gbEnviarSolicitacao.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.GroupBox gbEnviarSolicitacao;
        private System.Windows.Forms.Button btnAvancar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MaskedTextBox txtNomeCliente;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotalCliente;
        private System.Windows.Forms.TextBox txtTotalMS;
        private System.Windows.Forms.TextBox txtTotalSolicitacao;
        private System.Windows.Forms.TextBox txtMedicamentos;
        private System.Windows.Forms.MaskedTextBox txtNumAutorizacao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtCodAutorizacao;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox txtIdSolicitacao;
    }
}