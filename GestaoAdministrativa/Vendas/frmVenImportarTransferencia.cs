﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{

    public partial class frmVenImportarTransferencia : Form, Botoes
    {
        private ToolStripButton tsbTransferencia = new ToolStripButton("Imp. Trans. Produtos");
        private ToolStripSeparator tssTransferencia = new ToolStripSeparator();
        private bool possuiProdutosNaoCadastrados;

        public frmVenImportarTransferencia(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTransferencia);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenImportarTransferencia_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dgProdutos.Visible = false;
                btnImportar.Visible = false;
                gbInformacoes.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTransferencia.AutoSize = false;
                this.tsbTransferencia.Image = Properties.Resources.vendas;
                this.tsbTransferencia.Size = new System.Drawing.Size(150, 20);
                this.tsbTransferencia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTransferencia);
                tsbTransferencia.Click += delegate
                {
                    var transferencia = Application.OpenForms.OfType<frmVenImportarTransferencia>().FirstOrDefault();
                    Util.BotoesGenericos();
                    transferencia.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            txtID.Text = "";
            dgProdutos.Visible = false;
            btnImportar.Visible = false;
            gbInformacoes.Visible = false;
            txtID.Focus();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public async Task<List<Transferencia>> BuscaTransferenciaPorID()
        {
            List<Transferencia> allItems = new List<Transferencia>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Transferencia/BuscaTransferenciaPorID?ID=" + txtID.Text);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Transferencia>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<TransferenciaItens>> BuscaTransferenciaItensPorID()
        {
            List<TransferenciaItens> allItems = new List<TransferenciaItens>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Transferencia/BuscaTransferenciaItensPorID?ID=" + txtID.Text);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<TransferenciaItens>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private async void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtID.Text))
                {
                    Cursor = Cursors.WaitCursor;

                    List<Transferencia> dados = await BuscaTransferenciaPorID();
                    if (dados.Count > 0)
                    {
                        if (dados[0].CodErro == "00")
                        {
                            lblFilial.Text = dados[0].CodLoja.ToString();
                            txtObservacao.Text = dados[0].Observacao;
                            
                            List<TransferenciaItens> itens = await BuscaTransferenciaItensPorID();
                            if (itens.Count > 0)
                            {
                                if(IdentificaProdutoNaoCadastrados(itens))
                                {
                                    if (itens[0].CodErro == "00")
                                    {
                                        int totalQtde = itens.Sum(x => x.Qtde);
                                        btnImportar.Visible = true;
                                        gbInformacoes.Visible = true;
                                        dgProdutos.Visible = true;
                                        dgProdutos.DataSource = itens;
                                        lblQtde.Text = totalQtde.ToString();
                                        btnImportar.Focus();
                                    }
                                    else
                                    {
                                        btnImportar.Visible = false;
                                        gbInformacoes.Visible = false;
                                        dgProdutos.Visible = false;
                                        MessageBox.Show("Erro: " + itens[0].CodErro + " - " + dados[0].Mensagem, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                        else if(dados[0].CodErro == "01")
                        {
                            MessageBox.Show("Nenhum registro encontrado!", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtID.Text = "";
                        }
                        else
                        {
                            MessageBox.Show("Erro: " + dados[0].CodErro + " - " + dados[0].Mensagem, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Necessário informar o ID!", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool IdentificaProdutoNaoCadastrados(List<TransferenciaItens> dados)
        {
            try
            {
                dgProdutosNaoCadastrados.Rows.Clear();
                possuiProdutosNaoCadastrados = false;
                int indiceProd = 0;
                string[,] produtosNaoCadastrados = new string[dados.Count, 4];
                string[] produtosSemCodBarra = new string[dados.Count];

                for (int i = 0; i < dados.Count; i++)
                {
                    var dadosProduto = new ProdutoDetalhe
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        ProdCodigo = dados[i].ProdCodigo,
                    };

                    if (dadosProduto.IndentificaProdutoCadastradoProdutoDetalhe(dadosProduto).Equals(false))
                    {
                        produtosNaoCadastrados[indiceProd, 0] = Convert.ToInt64(dados[i].ProdCodigo).ToString();
                        produtosNaoCadastrados[indiceProd, 1] = "";
                        produtosNaoCadastrados[indiceProd, 2] = "UN";
                        produtosNaoCadastrados[indiceProd, 3] = dados[i].Valor.ToString();
                        
                        possuiProdutosNaoCadastrados = true;
                        indiceProd += 1;
                        
                    }
                }

                if (possuiProdutosNaoCadastrados == true)
                {
                    pnlProdutos.Location = new Point((this.panel2.Width / 2) - (this.pnlProdutos.Size.Width / 2), (this.panel2.Height / 2) - (this.pnlProdutos.Size.Height / 2));
                    pnlProdutos.Visible = true;

                    for (int i = 0; i < indiceProd; i++)
                    {
                        dgProdutosNaoCadastrados.Rows.Insert(dgProdutosNaoCadastrados.RowCount, new Object[] { i + 1, produtosNaoCadastrados[i, 0],
                            produtosNaoCadastrados[i, 1], produtosNaoCadastrados[i, 2], produtosNaoCadastrados[i, 3] });
                    }

                    if (dgProdutosNaoCadastrados.ColumnCount == 5)
                    {
                        DataTable dt = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, " DEP_DESABILITADO = 'N'");
                        DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                        combo.HeaderText = "Departamentos";
                        combo.Name = "Departamento";
                        ArrayList row = new ArrayList();

                        foreach (DataRow dr in dt.Rows)
                        {
                            row.Add(dr["DEP_DESCR"].ToString());
                        }

                        combo.Items.AddRange(row.ToArray());
                        dgProdutosNaoCadastrados.Columns.Add(combo);
                    }
                    btnOk.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnImportar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmVenImportarTransferencia_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.F2:
                    btnImportar.PerformClick();
                    break;
            }
        }

        public bool AtualizaEstoque()
        {
            try
            {
                var estoque = new ProdutoDetalhe();
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    estoque.EmpCodigo = Principal.empAtual;
                    estoque.EstCodigo = Principal.estAtual;
                    estoque.ProdCodigo = dgProdutos.Rows[i].Cells[1].Value.ToString();
                    estoque.ProdEstAtual = Convert.ToInt32(dgProdutos.Rows[i].Cells[2].Value);

                    estoque.AtualizaEstoqueAtual(estoque, "+", true);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private async void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.RowCount > 0)
                {
                    if (MessageBox.Show("Confirma a Importação de Produtos?", "Importar Transferência de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Cursor = Cursors.WaitCursor;
                        BancoDados.AbrirTrans();
                        if (!AtualizaEstoque())
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        if (!AtualizaMovimentoEstoque())
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        BancoDados.FecharTrans();
                        
                        await ExcluiTransferencia();

                        MessageBox.Show("Importação Realizada com Sucesso!", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Limpar();
                    }
                    else
                        txtID.Focus();
                }
                else
                {
                    MessageBox.Show("Necessário adicionar ao menos um produto!", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProdutos.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool AtualizaMovimentoEstoque()
        {
            try
            {
                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = id,
                        ProdCodigo = dgProdutos.Rows[i].Cells[1].Value.ToString(),
                        EntQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[2].Value),
                        EntValor = 0,
                        SaiQtde = 0,
                        SaiValor =0,
                        OperacaoCodigo = "T",
                        DataMovimento = DateTime.Now,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    id += 1;

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> ExcluiTransferencia()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Transferencia/ExcluiTransferenciaID?id=" + txtID.Text);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao excluir Transferência.", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private async void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                pnlProdutos.Visible = false;
                bool insertSucesso = true;

                for (int i = 0; i < dgProdutosNaoCadastrados.RowCount; i++)
                {
                    int id = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID");
                    BancoDados.AbrirTrans();

                    var cadProduto = new Produto
                    {
                        ProdCodBarras = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        ProdDescr = dgProdutosNaoCadastrados.Rows[i].Cells[2].Value.ToString(),
                        ProdUnidade = dgProdutosNaoCadastrados.Rows[i].Cells[3].Value.ToString(),
                        ProdTipo = "P",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadProduto.IdentificaSeProdutoEstaCadastrado(dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString()).Equals(false))
                    {
                        if (cadProduto.InsereProdutosNovos(cadProduto).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            insertSucesso = false;
                            break;
                        }
                    }

                    int codDepartmento = Convert.ToInt32(Funcoes.LeParametro(9, "41", true));

                    if (!String.IsNullOrEmpty(dgProdutosNaoCadastrados.Rows[i].Cells[5].Value.ToString()))
                    {
                        codDepartmento = Convert.ToInt32(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("DEPARTAMENTOS", "DEP_CODIGO", "DEP_DESCR", dgProdutosNaoCadastrados.Rows[i].Cells[5].Value.ToString(), false, true)));
                    }
                    var cadDetalhe = new ProdutoDetalhe
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        DepCodigo = codDepartmento,
                        ProdPreCompra = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[4].Value.ToString().Replace(".", ","))),
                        ProdDTEstIni = DateTime.Now,
                        ProdLiberado = "A",
                        ProdEcf = "FF",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadDetalhe.IndentificaProdutoCadastradoProdutoDetalhe(cadDetalhe).Equals(false))
                    {
                        if (cadDetalhe.InsereProdutosDetalheNovos(cadDetalhe).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            insertSucesso = false;
                            break;
                        }
                    }

                    var cadPreco = new Preco
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        PreValor = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[4].Value.ToString().Replace(".", ","))),
                        TabCodigo = 1,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadPreco.IndentificaProdutoCadastradoNaPrecos(cadPreco).Equals(false))
                    {
                        if (cadPreco.InsereProdutosPrecosNovos(cadPreco).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            insertSucesso = false;
                            break;
                        }
                    }

                    BancoDados.FecharTrans();
                }

                if (insertSucesso.Equals(true))
                {
                    List<Transferencia> dados = await BuscaTransferenciaPorID();
                    if (dados.Count > 0)
                    {
                        if (dados[0].CodErro == "00")
                        {
                            lblFilial.Text = dados[0].CodLoja.ToString();
                            txtObservacao.Text = dados[0].Observacao;

                            List<TransferenciaItens> itens = await BuscaTransferenciaItensPorID();
                            if (itens.Count > 0)
                            {
                                if (itens[0].CodErro == "00")
                                {
                                    int totalQtde = itens.Sum(x => x.Qtde);
                                    btnImportar.Visible = true;
                                    gbInformacoes.Visible = true;
                                    dgProdutos.Visible = true;
                                    dgProdutos.DataSource = itens;
                                    lblQtde.Text = totalQtde.ToString();
                                    btnImportar.Focus();
                                }
                                else
                                {
                                    btnImportar.Visible = false;
                                    gbInformacoes.Visible = false;
                                    dgProdutos.Visible = false;
                                    MessageBox.Show("Erro: " + itens[0].CodErro + " - " + dados[0].Mensagem, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                        else if (dados[0].CodErro == "01")
                        {
                            MessageBox.Show("Nenhum registro encontrado!", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtID.Text = "";
                        }
                        else
                        {
                            MessageBox.Show("Erro: " + dados[0].CodErro + " - " + dados[0].Mensagem, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
            }
        }

        //private void btnOk_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        pnlProdutos.Visible = false;
        //        bool insertSucesso = true;

        //        for (int i = 0; i < dgProdutosNaoCadastrados.RowCount; i++)
        //        {
        //            int id = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID");
        //            BancoDados.AbrirTrans();

        //            var cadProduto = new Produto
        //            {
        //                ProdCodBarras = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
        //                ProdDescr = dgProdutosNaoCadastrados.Rows[i].Cells[2].Value.ToString(),
        //                ProdUnidade = dgProdutosNaoCadastrados.Rows[i].Cells[3].Value.ToString(),
        //                ProdTipo = "P",
        //                DtCadastro = DateTime.Now,
        //                OpCadastro = Principal.usuario,
        //                ProdID = id
        //            };

        //            if (cadProduto.IdentificaSeProdutoEstaCadastrado(dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString()).Equals(false))
        //            {
        //                if (cadProduto.InsereProdutosNovos(cadProduto).Equals(false))
        //                {
        //                    BancoDados.ErroTrans();
        //                    insertSucesso = false;
        //                    break;
        //                }
        //            }

        //            int codDepartmento = Convert.ToInt32(Funcoes.LeParametro(9, "41", true));

        //            if (!String.IsNullOrEmpty(dgProdutosNaoCadastrados.Rows[i].Cells[5].Value.ToString()))
        //            {
        //                codDepartmento = Convert.ToInt32(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("DEPARTAMENTOS", "DEP_CODIGO", "DEP_DESCR", dgProdutosNaoCadastrados.Rows[i].Cells[5].Value.ToString(), false, true)));
        //            }
        //            var cadDetalhe = new ProdutoDetalhe
        //            {
        //                EmpCodigo = Principal.empAtual,
        //                EstCodigo = Principal.estAtual,
        //                ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
        //                DepCodigo = codDepartmento,
        //                ProdPreCompra = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[4].Value.ToString().Replace(".", ","))),
        //                ProdDTEstIni = DateTime.Now,
        //                ProdLiberado = "A",
        //                ProdEcf = "FF",
        //                DtCadastro = DateTime.Now,
        //                OpCadastro = Principal.usuario,
        //                ProdID = id
        //            };

        //            if (cadDetalhe.IndentificaProdutoCadastradoProdutoDetalhe(cadDetalhe).Equals(false))
        //            {
        //                if (cadDetalhe.InsereProdutosDetalheNovos(cadDetalhe).Equals(false))
        //                {
        //                    BancoDados.ErroTrans();
        //                    insertSucesso = false;
        //                    break;
        //                }
        //            }

        //            var cadPreco = new Preco
        //            {
        //                EmpCodigo = Principal.empAtual,
        //                EstCodigo = Principal.estAtual,
        //                ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
        //                PreValor = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[4].Value.ToString().Replace(".", ","))),
        //                TabCodigo = 1,
        //                DtCadastro = DateTime.Now,
        //                OpCadastro = Principal.usuario,
        //                ProdID = id
        //            };

        //            if (cadPreco.IndentificaProdutoCadastradoNaPrecos(cadPreco).Equals(false))
        //            {
        //                if (cadPreco.InsereProdutosPrecosNovos(cadPreco).Equals(false))
        //                {
        //                    BancoDados.ErroTrans();
        //                    insertSucesso = false;
        //                    break;
        //                }
        //            }

        //            BancoDados.FecharTrans();
        //        }

        //        if (insertSucesso.Equals(true))
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        BancoDados.ErroTrans();
        //    }
        //}
    }
}
