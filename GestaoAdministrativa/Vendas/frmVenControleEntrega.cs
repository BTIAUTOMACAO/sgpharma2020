﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenControleEntrega : Form, Botoes
    {
        private ToolStripButton tsbControleEntrega = new ToolStripButton("Controle Entrega");
        private ToolStripSeparator tssControleEntrega = new ToolStripSeparator();
        ControleEntrega dadosEntrega = new ControleEntrega();
        private int colEntregador;
        private string numControle;
        private int numeroRomaneio;
        private string observacao;
        private int operacao;
        private int indice;

        public frmVenControleEntrega(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now.AddDays(-1);
            dtFinal.Value = DateTime.Now;
            pnlRomaneio.Visible = false;
            pnlCancelar.Visible = false;
            pnlProdutos.Visible = false;
            lblRegistros.Text = "";
            observacao = "";
            cmbStatus.Text = "";
            timer1.Start();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbControleEntrega);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssControleEntrega);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Botao()
        {
            try
            {
                this.tsbControleEntrega.AutoSize = false;
                this.tsbControleEntrega.Image = Properties.Resources.entrega;
                this.tsbControleEntrega.Size = new System.Drawing.Size(125, 20);
                this.tsbControleEntrega.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbControleEntrega);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssControleEntrega);
                tsbControleEntrega.Click += delegate
                {
                    var ControleEntrega = Application.OpenForms.OfType<frmVenControleEntrega>().FirstOrDefault();
                    Util.BotoesGenericos();
                    ControleEntrega.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtVendaID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void txtVendaID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtRomaneio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void txtRomaneio_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void cmbEntregador_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbEntregador_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void cmbStatus_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.Focus();
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Sair();
                    this.Close();
                    break;
                case Keys.F1:
                    btnFiltrar.PerformClick();
                    break;
                case Keys.F6:
                    btnAtualizar.PerformClick();
                    break;
                case Keys.F7:
                    btnVisualizarProdutos.PerformClick();
                    break;
                case Keys.F8:
                    btnImprimir.PerformClick();
                    break;
                case Keys.F9:
                    btnIniciar.PerformClick();
                    break;
                case Keys.F10:
                    btnRetornar.PerformClick();
                    break;
                case Keys.F11:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F12:
                    btnFinalizar.PerformClick();
                    break;
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtFinal.Value < dtInicial.Value)
                {
                    MessageBox.Show("Data Final menor que a Data Inicial!", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtFinal.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                //Recurso no qual é para fazer com que o selector não mude quando alterar status ou quando atualizar pelo timer. 
                int row = 0;
                for (int i = 0; i < dgEntregas.RowCount; i++) {
                    if (dgEntregas.Rows[i].Selected == true) {
                        row = i; 
                    }
                }

                dgEntregas.DataSource = dadosEntrega.BuscaEntregas(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, txtVendaID.Text == "" ? 0 : Convert.ToInt64(txtVendaID.Text),
                    txtRomaneio.Text == "" ? 0 : Convert.ToInt32(txtRomaneio.Text), cmbVendedor.SelectedIndex == -1 ? -1 : Convert.ToInt32(cmbVendedor.SelectedValue),
                    cmbStatus.SelectedIndex);

                double total = 0;
                for (int i = 0; i < dgEntregas.RowCount; i++)
                {
                    total = total + Convert.ToDouble(dgEntregas.Rows[i].Cells["VENDA_TOTAL"].Value);
                }

                if (total > 0)
                {
                    lblRegistros.Text = "Total de Vendas: " + String.Format("{0:N}", total);
                }
                else
                    lblRegistros.Text = "";

                tslRegistros.Text = "Registros Encontrados: " + dgEntregas.RowCount;

                dgEntregas.ClearSelection();
                if (row != 0)
                    dgEntregas.Rows[row].Selected = true; 

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void frmVenControleEntrega_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void frmVenControleEntrega_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();

                //CARREGA VENDEDORES//
                dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N' AND COL_STATUS = 'V'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Vendedor!.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbVendedor.DataSource = dtPesq;
                    cmbVendedor.DisplayMember = "COL_NOME";
                    cmbVendedor.ValueMember = "COL_CODIGO";

                    cmbVendedor.SelectedIndex = -1;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmVenControleEntrega_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
            dgEntregas.DataSource = dadosEntrega.BuscaEntregas(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, txtVendaID.Text == "" ? 0 : Convert.ToInt64(txtVendaID.Text),
                txtRomaneio.Text == "" ? 0 : Convert.ToInt32(txtRomaneio.Text), cmbVendedor.SelectedIndex, cmbStatus.SelectedIndex);

            double total = 0;
            for (int i = 0; i < dgEntregas.RowCount; i++)
            {
                total = total + Convert.ToDouble(dgEntregas.Rows[i].Cells["VENDA_TOTAL"].Value);
            }

            if (total > 0)
            {
                lblRegistros.Text = "Total de Vendas: " + String.Format("{0:N}", total);
            }
            else
                lblRegistros.Text = "";


            tslRegistros.Text = "Registros Encontrados: " + dgEntregas.RowCount;

        }

        private void btnFiltrar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFiltrar, "Buscar Entregas");
        }

        private void dgEntregas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int row = 0;
            for (int i = 0; i < dgEntregas.RowCount; i++) {
                if (dgEntregas.Rows[i].Selected == true)
                    row = i; 
            }

            dgEntregas.DataSource = dadosEntrega.BuscaEntregas(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, txtVendaID.Text == "" ? 0 : Convert.ToInt64(txtVendaID.Text),
                txtRomaneio.Text == "" ? 0 : Convert.ToInt32(txtRomaneio.Text), cmbVendedor.SelectedIndex, cmbStatus.SelectedIndex);

            tslRegistros.Text = "Registros Encontrados: " + dgEntregas.RowCount;

            double total = 0;
            for (int i = 0; i < dgEntregas.RowCount; i++)
            {
                total = total + Convert.ToDouble(dgEntregas.Rows[i].Cells["VENDA_TOTAL"].Value);
            }

            if (total > 0)
            {
                lblRegistros.Text = "Total de Vendas: " + String.Format("{0:N}", total);
            }
            else
                lblRegistros.Text = "";

            dgEntregas.ClearSelection();
            if (row != 0)
                dgEntregas.Rows[row].Selected = true; 
        }

        private void btnAtualizar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAtualizar, "F6 - Atualizar Entregas");
        }

        private void btnVisualizarProdutos_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnVisualizarProdutos, "F7 - Visualizar Produtos");
        }

        private void btnIniciar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnIniciar, "F9 - Iniciar Entrega");
        }

        private void btnRetornar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnRetornar, "F10 - Retornar Entrega");
        }

        private void btnImprimir_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnImprimir, "F8 - Imprimir Listagem");
        }

        private void btnCancelar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancelar, "F11 - Cancelar Entrega");
        }

        private void btnFinalizar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFinalizar, "F12 - Finalizar Entrega");
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            int row = 0;
            for (int i = 0; i < dgEntregas.RowCount; i++) {
                if (dgEntregas.Rows[i].Selected == true)
                    row = i; 
            }
            dgEntregas.DataSource = dadosEntrega.BuscaEntregas(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, txtVendaID.Text == "" ? 0 : Convert.ToInt64(txtVendaID.Text),
                txtRomaneio.Text == "" ? 0 : Convert.ToInt32(txtRomaneio.Text), cmbVendedor.SelectedIndex, cmbStatus.SelectedIndex);

            tslRegistros.Text = "Registros Encontrados: " + dgEntregas.RowCount;

            double total = 0;
            for (int i = 0; i < dgEntregas.RowCount; i++)
            {
                total = total + Convert.ToDouble(dgEntregas.Rows[i].Cells["VENDA_TOTAL"].Value);
            }

            if (total > 0)
            {
                lblRegistros.Text = "Total de Vendas: " + String.Format("{0:N}", total);
            }
            else
                lblRegistros.Text = "";

            dgEntregas.ClearSelection();
            if (row != 0) {
                dgEntregas.Rows[row].Selected = true; 
            }
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.RowCount > 0)
                {
                    
                    for (int i = 0; i < dgEntregas.RowCount; i++)
                    {
                        if (dgEntregas.Rows[i].Selected == true)
                        {
                            if (dgEntregas.Rows[i].Cells[2].Value.ToString() != "PENDENTE")
                            {
                                MessageBox.Show("Para realizar esta operação a entrega deve estar com status [PENDENTE].", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                    }
                        

                        if (MessageBox.Show("Confirma o início da(s) Entrega(s)?", "Controle de Entrega", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                        == DialogResult.Yes)
                    {
                        numeroRomaneio = Convert.ToInt32(Funcoes.LeParametro(6, "23", false));
                        Funcoes.GravaParametro(6, "23", (numeroRomaneio + 1).ToString());

                        ColetaDadosDoRomaneio(numeroRomaneio);

                    }
                    else
                        btnIniciar.Focus();
                }
                else
                {
                    MessageBox.Show("Não há nenhuma Entrega Listada", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ColetaDadosDoRomaneio(int numRomaneio)
        {
            try
            {
                pnlRomaneio.Visible = true;
                pnlRomaneio.Location = new Point((panel1.Size.Width / 2) - (this.pnlRomaneio.Size.Width / 2), (panel1.Size.Height / 2) - (this.pnlRomaneio.Size.Height / 2));
                pnlRomaneio.Refresh();

                lblRomaneio.Text = Funcoes.FormataZeroAEsquerda(numRomaneio.ToString(), 6);

                //CARREGA ENTREGADORES//
                cmbREntregador.DataSource = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N' AND COL_STATUS = 'T'"); ;
                cmbREntregador.DisplayMember = "COL_NOME";
                cmbREntregador.ValueMember = "COL_CODIGO";
                cmbREntregador.SelectedIndex = -1;
                pnlRomaneio.Refresh();

                txtNumero.Text = "";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool ColetaDadosDoCancelamento()
        {
            try
            {
                txtMotivoCancelamento.Text = "";
                pnlCancelar.Visible = true;
                pnlCancelar.Location = new Point((panel1.Size.Width / 2) - (this.pnlCancelar.Size.Width / 2), (panel1.Size.Height / 2) - (this.pnlCancelar.Size.Height / 2));
                pnlCancelar.Refresh();
                txtMotivoCancelamento.Focus();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool IdentificaRegistroSelecionado()
        {
            bool selecionouLinha = false;

            for (int i = 0; i < dgEntregas.RowCount; i++)
            {
                if (dgEntregas.Rows[i].Cells[0].Value != null)
                {
                    if (dgEntregas.Rows[i].Cells[0].Value.Equals(true))
                    {
                        selecionouLinha = true;
                    }
                }
            }

            return selecionouLinha;
        }


        private void cmbREntregador_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmarRomaneiro.PerformClick();
        }

        private void btnConfirmarRomaneiro_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConfirmarRomaneiro, "Confirmar Romaneio");
        }

        private void btnCancelarRomaneio_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancelarRomaneio, "Cancelar Romaneio");
        }

        private void btnConfirmarRomaneiro_Click(object sender, EventArgs e)
        {
            if (cmbREntregador.SelectedIndex != -1)
            {
                colEntregador = Convert.ToInt32(cmbREntregador.SelectedValue);
                numControle = txtNumero.Text;
                pnlRomaneio.Visible = false;

                IniciarEntrega();
            }
            else
            {
                MessageBox.Show("Selecione um Entregador!", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbREntregador.Focus();
            }
        }

        public bool IniciarEntrega()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                int erro = 0;


                for (int i = 0; i < dgEntregas.RowCount; i++)
                {
                    if (dgEntregas.Rows[i].Selected == true)
                    {
                        if (dgEntregas.Rows[i].Cells[2].Value.ToString().Equals("PENDENTE"))
                        {
                            BancoDados.AbrirTrans();

                            if (!dadosEntrega.AtualizaRomaneioStatusNumControle(Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value), Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value), false, colEntregador, numeroRomaneio, numControle, 1))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var dadosStatus = new ControleEntregaStatus(
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value),
                                Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                0,
                                1,
                                DateTime.Now,
                                Principal.usuario,
                                "ENTREGA EM TRÂNSITO",
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                                );

                            if (!dadosStatus.InsereRegistros(dadosStatus))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }
                        }
                        else
                            erro += 1;

                        BancoDados.FecharTrans();
                    }
                }

                Cursor = Cursors.Default;

                if (erro.Equals(0))
                {
                    MessageBox.Show("Operação concluída com sucesso!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operação concluída, mas alguma(s) entrega(s)\n não estava(m) com o Status [PENDENTE] e por isso foi(ram) ignorada(s)!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                btnFiltrar.PerformClick();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnRetornar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.RowCount > 0)
                {
                    for (int i = 0; i < dgEntregas.RowCount; i++)
                    {
                        if (dgEntregas.Rows[i].Selected == true)
                        {
                            if (dgEntregas.Rows[i].Cells[2].Value.ToString() != "EM TRÂNSITO")
                            {
                                MessageBox.Show("Para realizar esta operação a entrega deve estar com status [EM TRÂNSITO].", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                    }
                    
                    if (MessageBox.Show("Confirma o retorno da(s) Entrega(s)?", "Controle de Entrega", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                        == DialogResult.Yes)
                    {
                        operacao = 0;
                        ColetaDadosDoCancelamento();
                    }
                    else
                        btnIniciar.Focus();
                }
                else
                {
                    MessageBox.Show("Não há nenhuma Entrega Listada", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelarRomaneio_Click(object sender, EventArgs e)
        {
            colEntregador = 0;
            numControle = "";
            pnlRomaneio.Visible = false;
            btnFiltrar.Focus();
        }

        private void btnCancelarCancelamento_Click(object sender, EventArgs e)
        {
            operacao = -1;
            pnlCancelar.Visible = false;
            btnFiltrar.Focus();
        }

        private void btnConfirmarCancelamento_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtMotivoCancelamento.Text))
                {
                    observacao = txtMotivoCancelamento.Text;
                    pnlCancelar.Visible = false;
                    if (operacao.Equals(0))
                    {
                        RetornarEntrega();
                    }
                    else
                    {
                        CancelarEntrega();
                    }
                }
                else
                {
                    MessageBox.Show("Motivo não pode ser em branco!", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtMotivoCancelamento.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool RetornarEntrega()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                int erro = 0;

                for (int i = 0; i < dgEntregas.RowCount; i++)
                {
                    if (dgEntregas.Rows[i].Selected == true)
                    {
                        if (dgEntregas.Rows[i].Cells[2].Value.ToString().Equals("EM TRÂNSITO"))
                        {
                            BancoDados.AbrirTrans();

                            if (!dadosEntrega.AtualizaRomaneioStatusNumControle(Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value), Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value), true))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var dadosStatus = new ControleEntregaStatus(
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value),
                                Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                0,
                                0,
                                DateTime.Now,
                                Principal.usuario,
                                observacao,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                                );

                            if (!dadosStatus.InsereRegistros(dadosStatus))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }
                        }
                        else
                            erro += 1;

                        BancoDados.FecharTrans();

                    }
                }

                Cursor = Cursors.Default;
                if (erro.Equals(0))
                {
                    MessageBox.Show("Operação concluída com sucesso!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operação concluída, mas alguma(s) entrega(s)\n não estava(m) com o Status [EM TRÂNSITO] e por isso foi(ram) ignorada(s)!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                btnFiltrar.PerformClick();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtMotivoCancelamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnConfirmarCancelamento.PerformClick();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.RowCount > 0)
                {
                    if (dgEntregas.SelectedRows.Count == 1)
                    {
                        for (int i = 0; i < dgEntregas.RowCount; i++)
                        {
                            if (dgEntregas.Rows[i].Selected == true)
                            {
                                if (dgEntregas.Rows[i].Cells[2].Value.ToString() != "PENDENTE")
                                {
                                    MessageBox.Show("Para realizar esta operação a entrega deve estar com status [PENDENTE].", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }

                    if (MessageBox.Show("Confirma o cancelamento da(s) Entrega(s)?", "Controle de Entrega", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                        == DialogResult.Yes)
                    {
                        frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                        senhaSup.ShowDialog();
                        if (senhaSup.sValida.Equals(true))
                        {
                            operacao = 1;
                            ColetaDadosDoCancelamento();
                        }
                        else
                            btnCancelar.Focus();
                    }
                        else
                            btnCancelar.Focus();
                }
                else
                {
                    MessageBox.Show("Não há nenhuma Entrega Listada", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool CancelarEntrega()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                int erro = 0;

                for (int i = 0; i < dgEntregas.RowCount; i++)
                {
                    if (dgEntregas.Rows[i].Selected == true)
                    {
                        if (dgEntregas.Rows[i].Cells[2].Value.ToString().Equals("PENDENTE"))
                        {
                            if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_BENEFICIO", "VENDA_ID", dgEntregas.Rows[i].Cells[5].Value.ToString(), false, true) != "N")
                            {
                                MessageBox.Show("VENDA BENEFICIO NÃO PODE SER CANCELADA NECESSÁRIO CONFIRMAR E DEPOIS SOLICIAR O CANCELAMENTO", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }

                            BancoDados.AbrirTrans();
                            if (!dadosEntrega.AtualizaRomaneioStatusNumControle(Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value), Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value), false, 0, 0, "", 9))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var dadosStatus = new ControleEntregaStatus(
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value),
                                Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                0,
                                9,
                                DateTime.Now,
                                Principal.usuario,
                                observacao,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                                );

                            if (!dadosStatus.InsereRegistros(dadosStatus))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var atualizaVenda = new VendasDados();
                            if(!atualizaVenda.CancelaEntrega(Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value)))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }
                        }
                        else
                            erro += 1;

                        BancoDados.FecharTrans();
                    }
                }

                Cursor = Cursors.Default;
                if (erro.Equals(0))
                {
                    MessageBox.Show("Operação concluída com sucesso!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Operação concluída, mas alguma(s) entrega(s)\n não estava(m) com o Status [PENDENTE] e por isso foi(ram) ignorada(s)!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                btnFiltrar.PerformClick();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.RowCount > 0)
                {
                    if (dgEntregas.SelectedRows.Count == 1)
                    {
                        for (int i = 0; i < dgEntregas.RowCount; i++)
                        {
                            if (dgEntregas.Rows[i].Selected == true)
                            {
                                if (dgEntregas.Rows[i].Cells[2].Value.ToString() != "EM TRÂNSITO")
                                {
                                    MessageBox.Show("Para realizar esta operação a entrega deve estar com status [EM TRÂNSITO].", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }

                    var caixa = new AFCaixa();
                    List<AFCaixa> cxStatus = caixa.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);

                    if (cxStatus.Count.Equals(0))
                    {
                        MessageBox.Show("Lançamento com movimentação de caixa com caixa que ainda não foi aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (cxStatus[0].DataAbertura.ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        MessageBox.Show("Lançamento com movimentação de caixa com data diferente do caixa aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (MessageBox.Show("Confirma a finalização da(s) Entrega(s)?", "Controle de Entrega", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                        == DialogResult.Yes)
                    {
                        ConfirmarEntrega();
                    }
                    else
                        btnIniciar.Focus();


                }
                else
                {
                    MessageBox.Show("Não há nenhuma Entrega Listada", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConfirmarEntrega()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                int erro = 0;

                for (int i = 0; i < dgEntregas.RowCount; i++)
                {
                    if (dgEntregas.Rows[i].Selected == true)
                    {
                        if (dgEntregas.Rows[i].Cells[2].Value.ToString().Equals("EM TRÂNSITO"))
                        {
                            frmVenControleEntregaRecebimento entregaRecebimento = new frmVenControleEntregaRecebimento(Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value),
                                dgEntregas.Rows[i].Cells[4].Value.ToString(), Convert.ToInt32(dgEntregas.Rows[i].Cells[3].Value),
                                Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value), Convert.ToDouble(dgEntregas.Rows[i].Cells[16].Value));
                            entregaRecebimento.ShowDialog();

                            if (entregaRecebimento.validou)
                            {
                                BancoDados.AbrirTrans();

                                if (!dadosEntrega.AtualizaRomaneioStatusNumControle(Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                    Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value), Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                    Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value), false, 0, 0, "", 2))
                                {
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                var dadosStatus = new ControleEntregaStatus(
                                    Convert.ToInt32(dgEntregas.Rows[i].Cells[6].Value),
                                    Convert.ToInt32(dgEntregas.Rows[i].Cells[7].Value),
                                    Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                                    Convert.ToInt32(dgEntregas.Rows[i].Cells[19].Value),
                                    0,
                                    2,
                                    DateTime.Now,
                                    Principal.usuario,
                                    "ENTREGA FINALIZADA",
                                    DateTime.Now,
                                    Principal.usuario,
                                    DateTime.Now,
                                    Principal.usuario
                                    );

                                if (!dadosStatus.InsereRegistros(dadosStatus))
                                {
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                BancoDados.FecharTrans();
                            }
                        }
                        else
                            erro += 1;

                        Cursor = Cursors.Default;


                        if (erro.Equals(0))
                        {
                            MessageBox.Show("Operação concluída com sucesso!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Operação concluída, mas alguma(s) entrega(s)\n não estava(m) com o Status [EM TRÂNSITO] e por isso foi(ram) ignorada(s)!", "Controle de entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

                btnFiltrar.PerformClick();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnFecharProdutos_Click(object sender, EventArgs e)
        {
            pnlProdutos.Visible = false;
            btnVisualizarProdutos.Focus();
        }

        private void btnVisualizarProdutos_Click(object sender, EventArgs e)
        {
            try
            {
                var dadosItens = new VendasItens();
                dgProdutos.DataSource = dadosItens.BuscaItensControleDeEntrega(Convert.ToInt32(dgEntregas.Rows[indice].Cells[6].Value),
                                        Convert.ToInt32(dgEntregas.Rows[indice].Cells[7].Value), Convert.ToInt64(dgEntregas.Rows[indice].Cells[5].Value));
                pnlProdutos.Visible = true;
                pnlProdutos.Location = new Point((panel1.Size.Width / 2) - (this.pnlProdutos.Size.Width / 2), (panel1.Size.Height / 2) - (this.pnlProdutos.Size.Height / 2));
                pnlProdutos.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEntregas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (dgEntregas.RowCount > 0)
            {
                if (dgEntregas.SelectedRows.Count == 1)
                {
                    for (int i = 0; i < dgEntregas.RowCount; i++)
                    {
                        if (dgEntregas.Rows[i].Selected == true)
                        {
                            ComprovanteVenda(Convert.ToInt64(dgEntregas.Rows[i].Cells[5].Value), Principal.empAtual, Principal.estAtual, Convert.ToDateTime(dgEntregas.Rows[i].Cells[1].Value));
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Não há nenhuma Entrega Listada", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnImprimir_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAtualizar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnVisualizarProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnIniciar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnRetornar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnFinalizar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public bool ComprovanteVenda(long vendaID, int empCodigo, int estCodigo, DateTime dataVenda)
        {
            try
            {
                DataTable dadosVenda = new DataTable();
                DataTable tProdutos = new DataTable();
                Cursor = Cursors.WaitCursor;
                string tamanho, comprovanteVenda;
                var dadosEstabelecimento = new Estabelecimento();
                List<Cliente> dadosVendaCliente = new List<Cliente>();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(estCodigo, empCodigo);
                if (retornoEstab.Count > 0)
                {
                    var vendaFormaPagto = new VendasFormaPagamento();
                    DataTable dtForma = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(empCodigo, estCodigo, vendaID);

                    int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtForma.Rows[0]["QTDE_VIAS"]);
                    
                    for (int y = 1; y <= qtdeVias; y++)
                    {
                        comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                        comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                        comprovanteVenda += "N. VENDA: " + vendaID + "\n";
                        comprovanteVenda += "Data: " + Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dataVenda).ToString("HH:mm:ss") + "\n";
                        comprovanteVenda += "_________________________________________" + "\n";
                        comprovanteVenda += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VLITEM R$" + "\n";
                        comprovanteVenda += "_________________________________________" + "\n";

                        var itensVendas = new VendasItens();

                        tProdutos = itensVendas.BuscaItensDaVendaPorID(vendaID, estCodigo, empCodigo);
                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]), 2) * 100);
                            comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[i]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                comprovanteVenda += "    R$ " + tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        double desconto = Math.Abs(Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")));
                        comprovanteVenda += "=========================================" + "\n";

                        dadosVenda = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", vendaID.ToString(), true);

                        var colaborador = new Colaborador();
                        comprovanteVenda += "VENDEDOR.: R$ " + colaborador.NomeColaborador(dadosVenda.Rows[0]["VENDA_COL_CODIGO"].ToString(), estCodigo) + "\n";
                        comprovanteVenda += "SUBTOTAL.: R$ " + String.Format("{0:N}", dadosVenda.Rows[0]["VENDA_SUBTOTAL"]) + "\n";

                        if (Math.Abs(Convert.ToDouble(desconto)) > 0 || Math.Abs(Convert.ToDouble(dadosVenda.Rows[0]["VENDA_DIFERENCA"])) != 0)
                        {
                            comprovanteVenda += "DESCONTO.: R$ " + String.Format("{0:N}", Math.Abs(desconto)) + "   AJUSTE.: R$ " + String.Format("{0:N}", Math.Abs(Convert.ToDouble(dadosVenda.Rows[0]["VENDA_DIFERENCA"]))) + "\n";
                        }
                        comprovanteVenda += "TOTAL....: R$ " + String.Format("{0:N}", dadosVenda.Rows[0]["VENDA_TOTAL"]) + "\n";
                        
                        var descricaoForma = new FormasPagamento();
                        comprovanteVenda += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO", 43) + "\n";

                        DataTable dtRetorno = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(empCodigo, estCodigo, vendaID);

                        for (int x = 0; x < dtRetorno.Rows.Count; x++)
                        {
                            comprovanteVenda += descricaoForma.IdentificaFormaDePagamento(Convert.ToInt32(dtRetorno.Rows[x]["VENDA_FORMA_ID"]), Convert.ToInt32(dtRetorno.Rows[x]["EMP_CODIGO"]))
                                + ".: R$ " + Funcoes.BValor(Convert.ToDouble(dtRetorno.Rows[x]["VENDA_VALOR_PARCELA"])) + "\n";
                        }
                        comprovanteVenda += "_________________________________________" + "\n";

                        var dCliente = new Cliente();
                        dadosVendaCliente = dCliente.DadosClienteCfDocto(Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_DOCTO", "CF_ID", dadosVenda.Rows[0]["VENDA_CF_ID"].ToString()));
                        if (dadosVendaCliente.Count > 0)
                        {
                            comprovanteVenda += dadosVendaCliente[0].CfCodigo == "" ? "00" : dadosVendaCliente[0].CfCodigo;
                            comprovanteVenda += " - " + dadosVendaCliente[0].CfNome + "\n";
                            comprovanteVenda += dadosVendaCliente[0].CfEndereco + "," + dadosVendaCliente[0].CfNumeroEndereco + "\n";
                            comprovanteVenda += dadosVendaCliente[0].CfCidade + "/" + dadosVendaCliente[0].CfUF + "\n";
                            comprovanteVenda += "CPF.:" + dadosVendaCliente[0].CfDocto + "\n";
                            if(!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString()) && dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() != "0")
                            {
                                comprovanteVenda += "EMPRESA: " + dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString()  + " - " 
                                    + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS","CON_NOME","CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString()) + "\n";
                            }
                           
                            comprovanteVenda += "_________________________________________" + "\n";
                        }

                        if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO"))
                        {
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().PadLeft(38 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().Length, ' ') + "\n";
                            }
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao com Receita:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().PadLeft(26 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().Length, ' ') + "\n";
                            }
                            comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                            comprovanteVenda += "Cartao:                  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().PadLeft(33 - dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length, ' ')) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).PadLeft(32 - (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length, ' ')) + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_MEDICO"].ToString()))
                            {
                                comprovanteVenda += "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"];
                            }

                            comprovanteVenda += "\n";

                            comprovanteVenda += "_________________________________________" + "\n";
                        }
                        else if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("FUNCIONAL"))
                        {
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString() + "\n";
                            }
                            comprovanteVenda += "Administradora:  FUNCIONAL CARD" + "\n";
                            comprovanteVenda += "Cartao:  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString()) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).ToString()) + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_MEDICO"].ToString()))
                            {
                                comprovanteVenda += "CRM: " + dadosVenda.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVenda.Rows[0]["VENDA_NUM_RECEITA"];
                            }

                            comprovanteVenda += "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                        }

                        comprovanteVenda += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                        comprovanteVenda += "_________________________________________" + "\n\n";

                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                            {
                                using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                                {
                                    writer.WriteLine(comprovanteVenda);
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovanteVenda += "\n\n\n\n\n";
                                Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                }
                            }
                        }
                        else
                        {
                            comprovanteVenda += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }

                    string wsObrigaReceita = dadosVenda.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString();

                    if (dadosVenda.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && wsObrigaReceita.Equals("S"))
                    {
                        for (int i = 1; i <= Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); i++)
                        {
                            comprovanteVenda = Funcoes.CentralizaTexto("COMPROVANTE", 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n\n";
                            comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                            comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                            comprovanteVenda += "N. VENDA: " + vendaID + "\n";
                            comprovanteVenda += "Data: " + Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dataVenda).ToString("HH:mm:ss") + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().PadLeft(38 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString().Length, ' ') + "\n";
                            }
                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += "Autorizacao com Receita:" + dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().PadLeft(26 - dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString().Length, ' ') + "\n";
                            }
                            comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                            comprovanteVenda += "Cartao:                  " + dadosVenda.Rows[0]["VENDA_CARTAO"] + "\n";
                            comprovanteVenda += "Conveniado:  " + (dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                                : dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().PadLeft(33 - dadosVenda.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length, ' ')) + "\n";

                            string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                            comprovanteVenda += "Empresa: " + ((dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).PadLeft(32 - (dadosVenda.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length, ' ')) + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                            {
                                comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS SEM RECEITA", 43) + "\n";
                                for (int x = 0; x < tProdutos.Rows.Count; x++)
                                {
                                    if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                                    {
                                        double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                        comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                        if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                        {
                                            comprovanteVenda += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                        }
                                        comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                                    }
                                }
                                comprovanteVenda += "_________________________________________" + "\n";
                            }

                            if (!String.IsNullOrEmpty(dadosVenda.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                            {
                                comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS COM RECEITA", 43) + "\n";
                                for (int y = 0; y < tProdutos.Rows.Count; y++)
                                {
                                    if (tProdutos.Rows[y]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                                    {
                                        double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[y]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[y]["VENDA_ITEM_QTDE"]), 2) * 100);
                                        comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[y]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[y]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[y]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[y]["PROD_DESCR"].ToString()) + "\n";
                                        if (Math.Abs(Convert.ToDouble(tProdutos.Rows[y]["VENDA_ITEM_DIFERENCA"])) > 0)
                                        {
                                            comprovanteVenda += "    R$ " + tProdutos.Rows[y]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[y]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                        }
                                        comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[y]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[y]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                                    }
                                }
                                comprovanteVenda += "_________________________________________" + "\n";
                            }

                            comprovanteVenda += "Declaro ter verificado a classificacao" + "\n";
                            comprovanteVenda += "das compras com e sem receita para fins" + "\n";
                            comprovanteVenda += "de desconto." + "\n\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += "DROGA BELLA AGRADECE TUDO DE BOM P/ VOCE" + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                     && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    string s_cmdTX = "\n";
                                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                    }
                                    else
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                    }

                                    iRetorno = BematechImpressora.IniciaPorta("USB");

                                    iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                    s_cmdTX = "\r\n";
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                    iRetorno = BematechImpressora.FechaPorta();
                                }
                                else
                                {
                                    //IMPRIME COMPROVANTE
                                    comprovanteVenda += "\n\n\n\n\n";
                                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                    }
                                }
                            }
                            else
                            {
                                comprovanteVenda += "\n\n\n\n";
                                //IMPRIME COMPROVANTE
                                int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }
                        }
                    }

                    if (Funcoes.LeParametro(6, "97", false).Equals("S"))
                    {
                        ProtocoloEntrega(vendaID, empCodigo, estCodigo, dataVenda, dadosVendaCliente[0].CfNome, Convert.ToDouble(dadosVenda.Rows[0]["VENDA_TOTAL"]));
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Reimpressão Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpressão Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ProtocoloEntrega(long vendaID, int empCodigo, int estCodigo, DateTime dataVenda, string nome, double total)
        {
            try
            {
                string protocoloDeEntrega;
                Cursor = Cursors.WaitCursor;

                DataTable dadosEntrega = Util.SelecionaRegistrosTodosOuEspecifico("CONTROLE_ENTREGA", "", "VENDA_ID", vendaID.ToString(), true);

                if (dadosEntrega.Rows.Count > 0)
                {
                    protocoloDeEntrega = "__________________________________________" + "\n";
                    protocoloDeEntrega += Funcoes.CentralizaTexto("PROTOCOLO DE ENTREGA", 43) + "\n";
                    protocoloDeEntrega += "__________________________________________" + "\n";
                    protocoloDeEntrega += "VENDA.........: " + vendaID + "\n";
                    protocoloDeEntrega += "DATA..........: " + dataVenda.ToString("dd/MM/yyyy") + "\n";
                    protocoloDeEntrega += "CLIENTE.......: " + nome + "\n";
                    protocoloDeEntrega += "ENDERECO......: " + dadosEntrega.Rows[0]["ENT_CONTROLE_ENDER"] + "\n";
                    protocoloDeEntrega += "NUMERO........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_NUM_ENT"] + "\n";
                    protocoloDeEntrega += "BAIRRO........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_BAIRRO"] + "\n";
                    protocoloDeEntrega += "CIDADE........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_CIDADE"] + "\n";
                    protocoloDeEntrega += "CEP...........: " + dadosEntrega.Rows[0]["ENT_CONTROLE_CEP"] + "\n";
                    protocoloDeEntrega += "TELEFONE......: " + dadosEntrega.Rows[0]["ENT_CONTROLE_FONE"] + "\n";
                    protocoloDeEntrega += "TOTAL DA NOTA.: " + String.Format("{0:N}", total) + "\n";
                    if (Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]) != 0)
                    {
                        protocoloDeEntrega += "TROCO PARA....: " + String.Format("{0:N}", Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]) + total) + "\n";
                        protocoloDeEntrega += "TROCO.........: " + String.Format("{0:N}", (Convert.ToDouble(dadosEntrega.Rows[0]["ENT_CONTROLE_VR_TROCO"]))) + "\n";
                    }
                    else
                    {
                        protocoloDeEntrega += "TROCO.........: 0,00\n";
                    }
                    protocoloDeEntrega += "OBSERVACAO....: " + dadosEntrega.Rows[0]["ENT_CONTROLE_OBSERVACAO"] + "\n\n\n";
                    protocoloDeEntrega += "__________________________________________" + "\n";
                    protocoloDeEntrega += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                    protocoloDeEntrega += "__________________________________________" + "\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(protocoloDeEntrega, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(protocoloDeEntrega);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                            {
                                writer.WriteLine(protocoloDeEntrega);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            protocoloDeEntrega += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocoloDeEntrega);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        protocoloDeEntrega += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocoloDeEntrega, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Protocolo de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void exportarParaExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntregas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEntregas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEntregas.RowCount;
                    for (i = 0; i <= dgEntregas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEntregas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEntregas.ColumnCount; k++)
                    {
                        if (dgEntregas.Columns[k].Visible == true)
                        {
                            switch (dgEntregas.Columns[k].HeaderText)
                            {
                                case "Data":
                                    numCel = Principal.GetColunaExcel(dgEntregas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntregas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEntregas.ColumnCount : indice) + Convert.ToString(dgEntregas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    planilha.Columns.AutoFit();

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }
    }
}
