﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenLancamentoEntrega : Form, Botoes
    {
        private ToolStripButton tsbLancamento = new ToolStripButton("Lan. Entrega");
        private ToolStripSeparator tssLancamento = new ToolStripSeparator();
        private string ID;
        private int idCliente;
        private int totalQtde;
        private double valorTotal;
        private int id;
        private DataTable dtLePrdutos = new DataTable();
        private Produto buscaProduto = new Produto();
        private int index;
        private int idEntrega;

        public frmVenLancamentoEntrega(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void frmVenLancamentoEntrega_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtQtdeEstoque") && (control.Name != "txtPreco"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbLancamento);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssLancamento);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbLancamento.AutoSize = false;
                this.tsbLancamento.Image = Properties.Resources.entrega;
                this.tsbLancamento.Size = new System.Drawing.Size(130, 20);
                this.tsbLancamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbLancamento);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssLancamento);
                tsbLancamento.Click += delegate
                {
                    var lancamento = Application.OpenForms.OfType<frmVenLancamentoEntrega>().FirstOrDefault();
                    Util.BotoesGenericos();
                    lancamento.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void frmVenLancamentoEntrega_Shown(object sender, EventArgs e)
        {
            cmbGrupos.Enabled = true;
            List<EstabelecimentoFilial> dadosEstb = await EnviaOuRecebeDadosMysql.BuscaEstabelecimentos();

            cmbGrupos.DataSource = dadosEstb;
            cmbGrupos.ValueMember = "codEstabelecimento";
            cmbGrupos.DisplayMember = "nomeEstabelecimento";
            cmbGrupos.SelectedIndex = -1;

            Principal.dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = -1;
            }
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

        private void cmbGrupos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbFBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtFBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtTroco_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmVenLancamentoEntrega_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                    {
                        Cursor = Cursors.WaitCursor;
                        var buscaFornecedor = new Cliente();
                        DataTable cliente = new DataTable();
                        string strOrdem;
                        if (cmbFBusca.SelectedIndex == 0)
                        {
                            cliente = buscaFornecedor.DadosClienteFiltro(txtFBusca.Text, 0, out strOrdem);
                        }
                        else if (cmbFBusca.SelectedIndex == 1)
                        {
                            cliente = cliente = buscaFornecedor.DadosClienteFiltro(txtFBusca.Text, 1, out strOrdem);
                        }
                        if (cliente.Rows.Count.Equals(1))
                        {
                            idCliente = Convert.ToInt32(cliente.Rows[0]["CF_ID"]);
                            txtFNome.Text = cliente.Rows[0]["CF_NOME"].ToString();
                            txtFCnpj.Text = cliente.Rows[0]["CF_DOCTO"].ToString();
                            txtFCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString() + "/" + cliente.Rows[0]["CF_UF"].ToString();
                            txtEndereco.Text = cliente.Rows[0]["CF_ENDER"].ToString();
                            txtNumero.Text = cliente.Rows[0]["CF_END_NUM"].ToString();
                            txtBairro.Text = cliente.Rows[0]["CF_BAIRRO"].ToString();
                            txtCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString();
                            txtTelefone.Text = cliente.Rows[0]["CF_FONE"].ToString();
                            cmbFBusca.SelectedIndex = 0;
                            txtCodBarras.Focus();
                        }
                        else if (cliente.Rows.Count.Equals(0))
                        {
                            MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtFBusca.Focus();
                        }
                        else
                        {
                            using (frmBuscaForn t = new frmBuscaForn("cliente"))
                            {
                                t.dgCliFor.DataSource = cliente;
                                t.ShowDialog();

                                if (!String.IsNullOrEmpty(t.cfNome))
                                {
                                    idCliente = t.cfID;
                                    cliente = buscaFornecedor.DadosClienteFiltro(t.cfID.ToString(), 2, out strOrdem);
                                    txtFNome.Text = cliente.Rows[0]["CF_NOME"].ToString();
                                    txtFCnpj.Text = cliente.Rows[0]["CF_DOCTO"].ToString();
                                    txtFCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString() + "/" + cliente.Rows[0]["CF_UF"].ToString();
                                    txtEndereco.Text = cliente.Rows[0]["CF_ENDER"].ToString();
                                    txtNumero.Text = cliente.Rows[0]["CF_END_NUM"].ToString();
                                    txtBairro.Text = cliente.Rows[0]["CF_BAIRRO"].ToString();
                                    txtCidade.Text = cliente.Rows[0]["CF_CIDADE"].ToString();
                                    txtTelefone.Text = cliente.Rows[0]["CF_FONE"].ToString();
                                    cmbFBusca.SelectedIndex = 0;
                                    txtCodBarras.Focus();
                                }
                                else
                                {
                                    cmbFBusca.SelectedIndex = 0;
                                    txtFBusca.Text = "";
                                    txtFBusca.Focus();
                                }
                            }
                        }

                    }
                    else
                    {
                        cmbGrupos.Focus();
                    }
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Limpar()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtQtde.Text = "";
            txtQtdeEstoque.Text = "";
            txtFBusca.Text = String.Empty;
            txtFCidade.Text = "";
            txtFCnpj.Text = "";
            txtFNome.Text = "";
            cmbFBusca.SelectedIndex =0;
            cmbGrupos.SelectedIndex = -1;
            dgProdutos.Rows.Clear();
            dgProdutos.AllowUserToDeleteRows = true;
            cmbVendedor.SelectedIndex = -1;
            totalQtde = 0;
            valorTotal = 0;
            index = 0;
            id = 0;
            idEntrega = 0;
            txtEndereco.Text = "";
            txtNumero.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            txtTroco.Text = "0,00";
            lblTroco.Text = "";
            txtObs.Text = "";
            cmbGrupos.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                dtLePrdutos = buscaProduto.BuscaProdutosTransferencia(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }

                    txtPreco.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PRE_VALOR"]);
                    txtQtdeEstoque.Text = dtLePrdutos.Rows[0]["PROD_ESTATUAL"].ToString();
                    txtQtde.Text = "0";
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();

                    txtQtde.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                valorTotal = 0;
                totalQtde = 0;
                if (ConsisteCampos())
                {
                    bool achou = false;
                    int qtde = 0;
                    for (int i = 0; i < dgProdutos.RowCount; i++)
                    {
                        if (dgProdutos.Rows[i].Cells[3].Value.ToString() == txtCodBarras.Text)
                        {
                            qtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value);
                            dgProdutos.Rows[i].Cells["QTDE"].Value = qtde + Convert.ToInt32(txtQtde.Text);
                            dgProdutos.Rows[i].Cells["SUBTOTAL"].Value = Convert.ToDouble(dgProdutos.Rows[i].Cells["PRECO"].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells["QTDE"].Value);
                            dgProdutos.Rows[i].Cells["TOTAL"].Value = Convert.ToDouble(dgProdutos.Rows[i].Cells["SUBTOTAL"].Value) - Convert.ToDouble(dgProdutos.Rows[i].Cells["DESCONTO"].Value);
                            achou = true;
                        }
                    }

                    if (!achou)
                    {
                        dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { Principal.empAtual, Principal.estAtual, ID, txtCodBarras.Text, txtDescr.Text, txtPreco.Text, txtQtde.Text,
                            Convert.ToDouble(txtPreco.Text) * Convert.ToInt32(txtQtde.Text), 0, Convert.ToDouble(txtPreco.Text) * Convert.ToInt32(txtQtde.Text), Util.SelecionaCampoEspecificoDaTabela("PRODUTOS_DETALHE","PROD_COMISSAO","PROD_CODIGO",txtCodBarras.Text,true, true,true) });
                    }

                    txtCodBarras.Text = string.Empty;
                    txtDescr.Text = string.Empty;
                    txtQtde.Text = "0";
                    txtPreco.Text = string.Empty;
                    txtQtdeEstoque.Text = "0";

                    for (int x= 0; x < dgProdutos.RowCount; x++)
                    {
                        valorTotal += Convert.ToDouble(dgProdutos.Rows[x].Cells["TOTAL"].Value);
                        totalQtde += Convert.ToInt32(dgProdutos.Rows[x].Cells["QTDE"].Value);
                    }

                    lblQtde.Text = totalQtde.ToString();
                    lblTotal.Text = String.Format("{0:N}", valorTotal);

                    txtCodBarras.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. Barras não pode ser em Branco!";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()))
            {
                Principal.mensagem = "Qtde não pode ser em Branco!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (Convert.ToInt32(txtQtde.Text) == 0)
            {
                Principal.mensagem = "Qtde não pode ser Zero!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }


            return true;
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAdicionar.PerformClick();
        }

        private void dgProdutos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                valorTotal = 0;
                totalQtde = 0;
                Cursor = Cursors.WaitCursor;
                switch (dgProdutos.Columns[e.ColumnIndex].Name)
                {
                    case "QTDE":
                        dgProdutos.Rows[e.RowIndex].Cells["SUBTOTAL"].Value = Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["PRECO"].Value) * Convert.ToInt32(dgProdutos.Rows[e.RowIndex].Cells["QTDE"].Value);
                        dgProdutos.Rows[e.RowIndex].Cells["TOTAL"].Value = Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["SUBTOTAL"].Value) - Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["DESCONTO"].Value);
                        break;
                    case "DESCONTO":
                        dgProdutos.Rows[e.RowIndex].Cells["TOTAL"].Value = Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["SUBTOTAL"].Value) - Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["DESCONTO"].Value);
                        break;
                }

                for (int x = 0; x < dgProdutos.RowCount; x++)
                {
                    valorTotal += Convert.ToDouble(dgProdutos.Rows[x].Cells["TOTAL"].Value);
                    totalQtde += Convert.ToInt32(dgProdutos.Rows[x].Cells["QTDE"].Value);
                }

                lblQtde.Text = totalQtde.ToString();
                lblTotal.Text = String.Format("{0:N}", valorTotal);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void dgProdutos_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            valorTotal = 0;
            totalQtde = 0;
            for (int x = 0; x < dgProdutos.RowCount; x++)
            {
                valorTotal += Convert.ToDouble(dgProdutos.Rows[x].Cells["TOTAL"].Value);
                totalQtde += Convert.ToInt32(dgProdutos.Rows[x].Cells["QTDE"].Value);
            }

            lblQtde.Text = totalQtde.ToString();
            lblTotal.Text = String.Format("{0:N}", valorTotal);
        }

        private void txtTroco_Validated(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtTroco.Text.Trim()))
                {
                    if (Convert.ToDouble(txtTroco.Text) > 0)
                    {
                        lblTroco.Visible = true;
                        if (Convert.ToDouble(txtTroco.Text) > Convert.ToDouble(lblTotal.Text))
                        {
                            txtTroco.Text = String.Format("{0:N}", Convert.ToDouble(txtTroco.Text));
                            lblTroco.Text = "TROCO: " + String.Format("{0:C}", Convert.ToDouble(txtTroco.Text) - Convert.ToDouble(lblTotal.Text));
                        }
                        else
                        {
                            txtTroco.Text = String.Format("{0:N}", Convert.ToDouble(txtTroco.Text));
                            lblTroco.Text = "TROCO: R$ 0,00";
                        }
                    }
                    else
                        txtObs.Focus();
                }
                else
                {
                    txtTroco.Text = "0,00";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBairro.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTroco.Focus();
        }

        private void txtTroco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbVendedor.Focus();
        }

        private async void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCamposEntrega())
                {
                    if (MessageBox.Show("Confirma a Entrega Entre Filiais?", "Lançamento de Entrega Filial", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        idEntrega = Funcoes.IdentificaVerificaID("ENTREGA_FILIAL", "CODIGO");
                        Cursor = Cursors.WaitCursor;
                        BancoDados.AbrirTrans();

                        var dados = new EntregaFilial();
                        dados.EmpCodigo = Principal.empAtual;
                        dados.EstCodigo = Principal.estAtual;
                        dados.Codigo = idEntrega;
                        dados.Solicitante = cmbVendedor.Text;
                        dados.LojaOrigem = Convert.ToInt32(Funcoes.LeParametro(9, "52", false));
                        dados.LojaDestino = Convert.ToInt32(cmbGrupos.SelectedValue);
                        dados.CfDocto = txtFCnpj.Text;
                        dados.CfEndereco = txtEndereco.Text;
                        dados.CfNumero = txtNumero.Text;
                        dados.CfBairro = txtBairro.Text;
                        dados.CfCidade = txtCidade.Text;
                        dados.CfTelefone = txtTelefone.Text;
                        dados.Troco = Convert.ToDouble(txtTroco.Text);
                        dados.TotalEntrega = Convert.ToDouble(lblTotal.Text);
                        dados.Observacao = txtObs.Text;
                        dados.OpCadastro = Principal.usuario;
                        dados.DtCadastro = DateTime.Now;
                        dados.Status = "P";


                        if (!dados.InsereRegistros(dados))
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        var dadosItens = new EntregaFilialItens();
                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            dadosItens.EmpCodigo = Principal.empAtual;
                            dadosItens.EstCodigo = Principal.estAtual;
                            dadosItens.Codigo = idEntrega;
                            dadosItens.ProdCodigo = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString();
                            dadosItens.Qtde = Convert.ToInt32(dgProdutos.Rows[i].Cells["QTDE"].Value);
                            dadosItens.Preco = Convert.ToDouble(dgProdutos.Rows[i].Cells["PRECO"].Value);
                            dadosItens.Subtotal = Convert.ToDouble(dgProdutos.Rows[i].Cells["SUBTOTAL"].Value);
                            dadosItens.Desconto = Convert.ToDouble(dgProdutos.Rows[i].Cells["DESCONTO"].Value);
                            dadosItens.Total = Convert.ToDouble(dgProdutos.Rows[i].Cells["TOTAL"].Value);
                            dadosItens.Comissao = dgProdutos.Rows[i].Cells["COMISSAO"].Value.ToString() == "" ? 0 : Convert.ToDouble(dgProdutos.Rows[i].Cells["COMISSAO"].Value);
                            if (!dadosItens.InsereRegistros(dadosItens))
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                        }

                        await InsereEntrega();

                        BancoDados.FecharTrans();

                        BancoDados.ExecuteNoQuery("UPDATE ENTREGA_FILIAL SET ID = " + id + " WHERE CODIGO = " + idEntrega, null);

                        MessageBox.Show("Entrega Lançada com Sucesso!","Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Limpar();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> InsereEntrega()
        {
            try
            {

                id = await EnviaEntrega();

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Entrega/InsereItensEntregaDeProdutos?id=" + id
                                        + "&prodCodigo=" + dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString()
                                        + "&qtde=" + dgProdutos.Rows[i].Cells["QTDE"].Value
                                        + "&preco=" + dgProdutos.Rows[i].Cells["PRECO"].Value.ToString().Replace(",", ".")
                                        + "&subTotal=" + dgProdutos.Rows[i].Cells["SUBTOTAL"].Value.ToString().Replace(",", ".")
                                        + "&desconto=" + dgProdutos.Rows[i].Cells["DESCONTO"].Value.ToString().Replace(",", ".")
                                        + "&total=" + dgProdutos.Rows[i].Cells["TOTAL"].Value.ToString().Replace(",", ".")
                                        + "&comissao=" + (dgProdutos.Rows[i].Cells["COMISSAO"].Value.ToString() == "" ? "0" : dgProdutos.Rows[i].Cells["COMISSAO"].Value.ToString().Replace(",", ".")));
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\EntregaProduto.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString() + " / QTDE: " + dgProdutos.Rows[i].Cells["QTDE"].Value);
                            }
                        }
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<int> EnviaEntrega()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/InsereEntrega?LojaOrigem=" + Funcoes.LeParametro(9, "52", true)
                        + "&CodGrupo=" + Funcoes.LeParametro(9, "53", true) + "&LojaDestino=" + cmbGrupos.SelectedValue + "&Solicitante=" + cmbVendedor.Text + "&Codigo=" + idEntrega
                        + "&CfDocto=" + txtFCnpj.Text + "&CfNome=" + txtFNome.Text + "&CfEndereco=" + txtEndereco.Text + "&CfNumero=" + txtNumero.Text + "&CfBairro=" + txtBairro.Text
                        + "&CfCidade=" + txtCidade.Text + "&CfTelefone=" + txtTelefone.Text + "&Troco=" + txtTroco.Text.Replace(",",".") + "&TotalEntrega=" + lblTotal.Text.Replace(",",".")
                        + "&Observacao=" + txtObs.Text + "&Status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return Convert.ToInt32(responseBody);
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private bool ConsisteCamposEntrega()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (cmbGrupos.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar uma Filial";
                Funcoes.Avisa();
                cmbGrupos.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFNome.Text.Trim()))
            {
                Principal.mensagem = "Cliente não pode ser em Branco";
                Funcoes.Avisa();
                txtFNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em Branco";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNumero.Text.Trim()))
            {
                Principal.mensagem = "Número não pode ser em Branco";
                Funcoes.Avisa();
                txtNumero.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNumero.Text.Trim()))
            {
                Principal.mensagem = "Número não pode ser em Branco";
                Funcoes.Avisa();
                txtNumero.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtBairro.Text.Trim()))
            {
                Principal.mensagem = "Bairro não pode ser em Branco";
                Funcoes.Avisa();
                txtBairro.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em Branco";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtTelefone.Text.Trim()))
            {
                Principal.mensagem = "Telefone não pode ser em Branco";
                Funcoes.Avisa();
                txtTelefone.Focus();
                return false;
            }
            if (cmbVendedor.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar um Vendedor";
                Funcoes.Avisa();
                cmbVendedor.Focus();
                return false;
            }
            return true;
        }
    }
}
