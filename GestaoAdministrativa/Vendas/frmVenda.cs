﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Classes.Funcional;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Negocio.Beneficios;
using GestaoAdministrativa.Negocio.Funcional;
using GestaoAdministrativa.SAT;
using GestaoAdministrativa.Vendas.DrogabellaPlantao;
using GestaoAdministrativa.Vendas.EPharma;
using GestaoAdministrativa.Vendas.Funcional;
using GestaoAdministrativa.Vendas.Orizon;
using GestaoAdministrativa.Vendas.PortalDaDrogaria;
using GestaoAdministrativa.Vendas.VidaLink;
using GestaoAdministrativa.Properties;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenda : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbVendas = new ToolStripButton();
        private ToolStripSeparator tssVendas = new ToolStripSeparator();

        private string[] dadosProdutos = new string[30];
        private string[,] prodAut = new string[15, 29];
        public string codCliente;
        private bool transAberta;
        public int statusCliente;
        public decimal tDesconto;
        private bool altDesc;
        public DataRow[] linha_resultado;
        private string supSenha;
        private double subTotal;
        public double sTotal;
        private double sAjuste;
        private string[] split;
        private string retorno;
        private XmlDocument retornoXml;
        private GestaoAdministrativa.WSConvenio.wsconvenio ws;
        private string codErro;
        private string[,] wsRetornoProdutos = new string[30, 12];
        private int quantProd;
        private bool passouItens;
        public double descMin;
        public double descMax;
        public int codFormaPagto;
        public bool selForma;
        public bool selEspecie;
        private DateTime dtPedido;
        public double totalVenda;
        public bool venOrcamento;
        private bool permtVenda;
        private bool valida;
        public bool descontoCaixa;
        public bool exclusaoComanda;
        public long vendaID;
        public string permiteTroca;
        public string venCompPedido;
        public string filtro;
        private bool prodContCliente;
        private int qtdeUsoContinuo;
        private int qtdeEmbalagem;
        private int diasProdContinuo;
        private DateTime dataProxima;
        private bool inserir;
        public int qtdeAutorizadaPortalDrogaria;
        //public string nomeColaborador;
        public string doctoCliente;
        public int IDCliente;
        public int wsConID;
        public string wsEmpresa;
        public string wsNomeEmpresa;
        public string wsNomeCartao;
        public string wsConWeb;
        public string wsConCodConv;
        public string wsCRM;
        public string wsNumReceita;
        public long movtoFinanceiroID;
        public long movtoFinanceiroSeq;
        public string wsNumCartao;
        public string wsCartao;
        public string wsSenhaPadrao;
        public string wsObrigaReceita;
        public int tipoVenda;
        public bool checaMedComSemReceita;
        public string wsComSemRec;
        public string wsPreAutoriza;
        public string wsRegras;
        public string vendaBeneficio;
        public int wsQtdeProdutosRetorno;
        public string wsTransID;
        public bool preAutorizou;
        public bool preAutorizouFuncional;
        public double wsSaldoCartao;
        public string wsUF;
        public string wsMNome;
        public string wsTitular;
        public DateTime wsData;
        public string wsConselho;
        public bool wsAbreOutraTransacao;
        public double totalVendaComReceita;
        public double totalVendaSemReceita;
        public double totalVendaConvenio;
        public string senhaConveniado;
        public string novaSenhaConveniado;
        public bool vendaDeComanda;
        public string vendaNumeroCaixa;
        public string vendaNumeroNota;
        public string wsProdutosCupomReceita;
        public string wsProdutosCupomSemReceita;
        public string wsDadosMedico;
        public string vendaNumeroNSU;
        public string lotePortalDrogaria;
        public string codAutorizacaoPortalDrogaria;
        public string numTrans;
        public string numCFE;
        public string vendaAutor;

        //dados entrega//
        public string vEntr_End;
        public string vEntr_Numero;
        public string vEntr_Bairro;
        public string vEntr_Cidade;
        public string vEntr_Cep;
        public string vEntr_Fone;
        public string vEntr_UF;
        public double vEntr_Valor;
        public string vEntr_Obs;
        public bool vEntr_Canc;

        public double totalImpFederal;
        public double totalImpEstadual;
        public double totalImpMunicipal;
        public double diferencaDoValorAutorizado;
        public double impostoFederal;
        public double impostoEstadual;
        public double impostoMunicipal;
        public double valorAutorizado;
        public bool entregaIsenta;
        public double valorTroco;
        //IMPRESSAO//
        private string comprovanteVenda;
        private string protocoloDeEntrega;
        private bool imprimeComprovante;
        private string comprovanteEspecie;
        //SAT//
        private string xmlSAT;
        private string numeroCFe;

        //BENEFICIO//
        public string cupomBeneficio;
        public string pagamentoTotalOuParcial;
        public bool bloqueiaProduto;
        public string numeroSequencia;
        public string numeroAutorizacao;
        public string sequencia;
        public string mensagem, erro;
        public string validou;
        public bool validouFP;

        List<VendaItem> venItens = new List<VendaItem>();
        List<Cliente> dadosVendaCliente = new List<Cliente>();
        List<PedidoItem> venPedidoItens = new List<PedidoItem>();
        List<PedidoParcela> venPedidoParcelas = new List<PedidoParcela>();
        List<ProdutosAlterados> produtosAlterados = new List<ProdutosAlterados>();

        private DataTable dtBProd = new DataTable();
        private DataTable dtRetorno = new DataTable();
        private DataTable dtPesq = new DataTable();
        public DataTable tProdutos = new DataTable();

        AFCaixa cxStatus = new AFCaixa();
        Colaborador dColaborador = new Colaborador();
        Comanda dComanda = new Comanda();
        Conveniada dConveniada = new Conveniada();
        Cliente dCliente = new Cliente();
        Produto dProduto = new Produto();

        VendasDados dadosDaVenda = new VendasDados();
        VendasFormaPagamento dadosDaVendaPagamento = new VendasFormaPagamento();
        List<VendasItens> vendaItens = new List<VendasItens>();
        public List<VendasFormaPagamento> vendaPedFormaPagto = new List<VendasFormaPagamento>();
        List<VendasEspecies> vendaEspecie = new List<VendasEspecies>();


        //Funcional
        public ClienteCartaoFuncional objTransacaoClienteCartaoFuncional = new ClienteCartaoFuncional();
        public PreAutorizacaoFuncionalResponse objPreAutorizacaoResponse = new PreAutorizacaoFuncionalResponse();
        public ProdutoFuncionalRequest produtoFuncionalRequest = new ProdutoFuncionalRequest();
        public DataTable produtosConfirmadosFuncional = new DataTable();
        public ConfirmaVendaResponse objConfirmaVendaResponse = new ConfirmaVendaResponse();
        public VendaAutorizadaRequest objDadosVendaConfirmadaFuncional = new VendaAutorizadaRequest();
        public List<string> listaProdutosNegadoAutorFuncional = new List<string>();
        public bool descartaAutorizacaoFuncional = false;
        public VendaAutoriadaResponse vendaAutorizadaResponse;
        public string comReceitaFuncional = "N";
        public DataTable dtDadosComandaFuncional;

        private bool vendaControlado;
        //private bool servidor;
        #endregion

        public frmVenda(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenda_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                this.Close();
            }
        }

        public void ValidarVendedor()
        {
            txtVendedor.Text = Convert.ToInt32(txtVendedor.Text).ToString();

            retorno = dColaborador.DadosColaborador(Principal.empAtual, txtVendedor.Text.Trim(), true, "V", "S", "X");
 
            if (Funcoes.LeParametro(4, "73", true).Equals("S"))
            {
                if (retorno.Equals("S") || retorno.Equals("X"))
                {
                    DateTime dtInicial = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
                    DateTime dtFinal = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");

                    double retiradas = dColaborador.TotalVendasPorPeriodo(dtInicial, dtFinal, Principal.usuario, "-");
                    double entradas = dColaborador.TotalVendasPorPeriodo(dtInicial, dtFinal, Principal.usuario, "+");

                    double emCaixa = entradas - retiradas;
                    double valorMaximo = Convert.ToDouble(Funcoes.LeParametro(4, "74", true));
                    if (emCaixa >= valorMaximo)
                    {
                        MessageBox.Show("Necessário realizar SANGRIA! Total em Caixa: R$ " + String.Format("{0:N}", emCaixa), "SG PHARMA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            if (!String.IsNullOrEmpty(retorno))
            {
                if (Funcoes.LeParametro(6, "352", true).Equals("S"))
                {
                    if (Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao) != retorno && retorno != "S")
                    {
                        MessageBox.Show("Perfil não Liberado para logar nessa Estação!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtVendedor.Text = "";
                        txtVendedor.Focus();
                        return;
                    }
                }

                frmVenVSenha senha = new frmVenVSenha(txtVendedor.Text.Trim());
                //IDENTIFICA SE OBRIGA SENHA DO COLABORADOR//
                if (Funcoes.LeParametro(6, "346", true).Equals("S"))
                {
                    senha.ShowDialog();
                }
                else
                    senha.sValida = true;


                if (senha.sValida.Equals(true))
                {
                    CamposVisiveis();
                    this.Text = "SG PHARMA - Sistema de Gestão - VENDA POR: " + Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", txtVendedor.Text) + " USUÁRIO:  " + Principal.usuario.ToUpper();

                    var caixa = new AFCaixa();

                    string cxStatus = caixa.IdentificaStatusDoCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);
                    if (String.IsNullOrEmpty(cxStatus))
                    {
                        imgStatus.Image = Properties.Resources.fechado;
                        toolTip1.SetToolTip(this.imgStatus, "Caixa Fechado");
                    }
                    else if (Convert.ToDateTime(cxStatus).ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        imgStatus.Image = Properties.Resources.atencao;
                        toolTip1.SetToolTip(this.imgStatus, "Caixa Data do Dia Anterior");
                    }
                    else
                    {
                        imgStatus.Image = Properties.Resources.aberto;
                        toolTip1.SetToolTip(this.imgStatus, "Caixa Aberto");
                    }


                    if (Principal.tipoCupom.Equals("P"))
                    {
                        imgTipoTela.Visible = true;
                        imgTipoTela.Image = Properties.Resources.orcamento;
                        toolTip1.SetToolTip(this.imgTipoTela, "Orçamento");
                    }
                    else
                    {
                        imgTipoTela.Visible = true;
                        imgTipoTela.Image = Properties.Resources.fiscal;
                        toolTip1.SetToolTip(this.imgTipoTela, "Cupom Fiscal");
                    }

                    txtCodCliEmp.Text = "";
                    txtCliEmp.Text = "";

                    dtPedido = DateTime.Now;

                    Principal.codigoVendedor = txtVendedor.Text;

                    InserirCabecalho();

                    lblCliEmp.Visible = true;
                    lblCliEmp.Text = "Cliente/Emp.";
                    txtCodCliEmp.Text = "";
                    txtCliEmp.Text = "";
                    txtCodCliEmp.Visible = true;
                    txtCodCliEmp.Enabled = true;
                    txtCliEmp.Visible = true;
                    txtTelefone.Visible = true;
                    txtTelefone.Enabled = true;
                    txtTelefone.Text = "";
                    lblTelefone.Visible = true;
                    dtPedido = DateTime.Now;

                    txtComanda.Focus();
                }
                else
                {
                    txtVendedor.Text = "";
                    txtVendedor.Enabled = true;
                    txtVendedor.Focus();
                }

            }
            else
            {
                MessageBox.Show("Vendedor não cadastrado", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtVendedor.Text = "";
                txtVendedor.Focus();
            }
        }

        private void txtVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtVendedor.Text))
                {
                    ValidarVendedor();
                }
                else
                {
                    cmbVendedor.Focus();
                }
            }
        }

        public void CamposVisiveis()
        {
            lblComanda.Visible = true;
            txtComanda.Visible = true;
            pnlCliente.Visible = true;
            tlpProduto.Visible = true;
            tlpCupom.Visible = true;
            tlpAjuste.Visible = true;
            tlpDiversos.Visible = true;
            btnBeneficios.Visible = true;
            btnConsultaProdutos.Visible = true;
            btnUltimosProdutos.Visible = true;
            btnBuluario.Visible = true;
            BTN_ConsultaABCFarma.Visible = true;
            btnLimpar.Visible = true;
            btnReimpressao.Visible = true;
            btnFVenda.Visible = true;
            btnAtalhos.Visible = true;
            imgStatus.Visible = true;
            tlpProdutos.BackColor = Color.Transparent;
            tableLayoutPanel5.BackColor = Color.Transparent;
            tableLayoutPanel4.BackColor = Color.Transparent;
            tableLayoutPanel10.BackColor = Color.Transparent;
            tlpDadosProdutos.BackgroundImage = Properties.Resources.logoVenda;
            tlpDadosProdutos.Visible = true;
        }

        public void CamposInvisiveis()
        {
            CamposProdutoInvisivel();
            lblComanda.Visible = false;
            txtComanda.Visible = false;
            pnlCliente.Visible = false;
            tlpProduto.Visible = false;
            tlpDadosProdutos.Visible = false;
            tlpCupom.Visible = false;
            tlpAjuste.Visible = false;
            tlpDiversos.Visible = false;
            btnBeneficios.Visible = false;
            btnConsultaProdutos.Visible = false;
            btnUltimosProdutos.Visible = false;
            btnBuluario.Visible = false;
            BTN_ConsultaABCFarma.Visible = false;
            btnLimpar.Visible = false;
            btnReimpressao.Visible = false;
            btnFVenda.Visible = false;
            btnAtalhos.Visible = false;
            imgStatus.Visible = false;
            label11.Visible = false;
        }

        public void CamposProdutoVisivel()
        {
            tlpProdutos.BackColor = Color.White;
            tableLayoutPanel5.BackColor = Color.White;
            tableLayoutPanel4.BackColor = Color.White;
            tableLayoutPanel10.BackColor = Color.White;
            tlpDadosProdutos.BackgroundImage = null;
            pcbCesto.Visible = true;
            lblCodBarras.Visible = true;
            lblDescricao.Visible = true;
            lblNumCodBarras.Visible = true;
            lblNome.Visible = true;
            label7.Visible = true;
            txtVUni.Visible = true;
            label8.Visible = true;
            label9.Visible = true;
            label6.Visible = true;
            txtPQtde.Visible = true;
            txtSTotal.Visible = true;
            txtPDesc.Visible = true;
            txtVDesc.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            txtEstoque.Visible = true;
            txtVTotal.Visible = true;
            btnExcluir.Visible = true;
            btnCancela.Visible = true;
            btnOk.Visible = true;
            label11.Visible = true;
        }

        public void CamposProdutoInvisivel()
        {
            tlpProdutos.BackColor = Color.Transparent;
            tableLayoutPanel5.BackColor = Color.Transparent;
            tableLayoutPanel4.BackColor = Color.Transparent;
            tableLayoutPanel10.BackColor = Color.Transparent;
            tlpDadosProdutos.BackgroundImage = Properties.Resources.logoVenda;
            tlpDadosProdutos.Refresh();
            pcbCesto.Visible = false;
            lblCodBarras.Visible = false;
            lblDescricao.Visible = false;
            lblNumCodBarras.Visible = false;
            lblNome.Visible = false;
            label7.Visible = false;
            txtVUni.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label6.Visible = false;
            txtPQtde.Visible = false;
            txtSTotal.Visible = false;
            txtPDesc.Visible = false;
            txtVDesc.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            txtEstoque.Visible = false;
            txtVTotal.Visible = false;
            btnExcluir.Visible = false;
            btnCancela.Visible = false;
            btnOk.Visible = false;
            lblComissao.Visible = false;
            lblCusto.Visible = false;
            txtCusto.Visible = false;
            txtComissao.Visible = false;
            label11.Visible = false;
            tlpDadosProdutos.Visible = true;
        }

        public bool InserirCabecalho()
        {
            try
            {
                retorno = Util.SelecionaCampoEspecificoDaTabela("ESTABELECIMENTOS", "EST_FANTASIA", "EST_CODIGO", Principal.estAtual.ToString());
                listCupom.Items.Add(Funcoes.CentralizaTexto(retorno, 56));
                listCupom.Items.Add("Data: " + DateTime.Now.ToString("dd/MM/yyyy") + "                        Hora: " + DateTime.Now.ToString("HH:mm:ss"));
                listCupom.Items.Add("--------------------------ITENS------------------------");
                listCupom.Items.Add("COD. DESCRIÇÃO QTDE UN VL.UNIT(R$) DESC(R$) VL.ITEM(R$)");
                listCupom.Items.Add("-------------------------------------------------------");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtComanda_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBeneficios.PerformClick();
                    break;
                case Keys.F2:
                    btnConsultaProdutos.PerformClick();
                    break;
                case Keys.F3:
                    if (btnUltimosProdutos.Enabled)
                    {
                        btnUltimosProdutos.PerformClick();
                    }
                    else
                    {
                        MessageBox.Show("Venda sem cliente! Consulta não realizada.", "Últimos Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodCliEmp.Focus();
                    }
                    break;
                case Keys.F4:
                    btnBuluario.PerformClick();
                    break;
                case Keys.F5:
                    BTN_ConsultaABCFarma.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F6:
                    btnLimpar.PerformClick();
                    break;
                case Keys.F7:
                    btnReimpressao.PerformClick();
                    break;
                case Keys.Insert:
                    btnFVenda.PerformClick();
                    break;
                case Keys.F8:
                    btnAtalhos.PerformClick();
                    break;
            }

            //ATALHOS COM CONTROL//
            if (e.Control && e.KeyCode == Keys.P)
            {
                btnPreAutorizar.PerformClick();
            }
            else if (e.Control && e.KeyCode == Keys.E)
            {
                if (chkEntrega.Checked)
                {
                    chkEntrega.Checked = false;
                }
                else
                    chkEntrega.Checked = true;
            }
            else if (e.Control && e.KeyCode == Keys.Q)
            {
                txtQtde.Focus();
            }
            else if (e.Control && e.KeyCode == Keys.C)
            {
                frmVenVSenha senha = new frmVenVSenha(txtVendedor.Text.Trim());
                senha.ShowDialog();

                if (senha.sValida.Equals(true))
                {
                    frmVenVisualizarComissao comissao = new frmVenVisualizarComissao(Convert.ToInt32(txtVendedor.Text.Trim()));
                    comissao.ShowDialog();
                }
                else
                    txtVendedor.Focus();
            }
            else if (e.Control && e.KeyCode == Keys.N)
            {
                if (txtCodCliEmp.Enabled)
                {
                    frmVenCadastroRapidoCliente cadastroCliente = new Vendas.frmVenCadastroRapidoCliente(this);
                    cadastroCliente.ShowDialog();
                    if (!txtCodCliEmp.Enabled)
                    {
                        btnUltimosProdutos.Enabled = true;
                    }
                    txtProduto.Focus();
                }
            }
            else if (e.Control && e.KeyCode == Keys.F)
            {
                retorno = cxStatus.IdentificaStatusDoCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);
                if (!String.IsNullOrEmpty(retorno))
                {
                    if (Principal.tipoCupom.Equals("C"))
                    {
                        Principal.tipoCupom = "P";
                        imgTipoTela.Image = Properties.Resources.orcamento;
                        toolTip1.SetToolTip(this.imgTipoTela, "Orçamento");
                    }
                    else
                    {
                        Principal.tipoCupom = "C";
                        imgTipoTela.Image = Properties.Resources.fiscal;
                        toolTip1.SetToolTip(this.imgTipoTela, "Cupom Fiscal");
                    }
                }
                else
                {
                    Principal.tipoCupom = "P";
                    imgTipoTela.Image = Properties.Resources.orcamento;
                    toolTip1.SetToolTip(this.imgTipoTela, "Orçamento");
                }
            }
            else if (e.Control && e.KeyCode == Keys.R)
            {
                frmVenFalteiro faltas = new Vendas.frmVenFalteiro(this);
                faltas.ShowDialog();
            }
            else if (e.Control && e.KeyCode == Keys.A)
            {
                txtAjuste.Focus();
            }
            else if (e.Control && e.KeyCode == Keys.O)
            {
                if (vendaID == 0)
                {
                    codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(6, "116", true));
                    lblConveniada.Visible = true;
                    lblConveniada.Text = "ORÇAMENTO";
                }
                else
                {
                    MessageBox.Show("Orçamento não permitida Venda já pré-gravada", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblConveniada.Visible = false;
                    lblConveniada.Text = "";
                }
            }
            else if (e.Control && e.KeyCode == Keys.M)
            {
                if (!Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("C") || !Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F"))
                {
                    txtCodCliEmp.Enabled = true;
                    txtCodCliEmp.Text = "";
                    txtCliEmp.Enabled = true;
                    txtCliEmp.Text = "";
                    txtTelefone.Enabled = true;
                    txtTelefone.Text = "";
                    lblConveniada.Visible = false;
                    txtCodCliEmp.Focus();
                }
            }
            else if (e.Control && e.KeyCode == Keys.W && dadosDaVenda.VendaID == 0)
            {
                string vendedor = Interaction.InputBox("Digite o código do Vendedor", "Código Vendedor");
                if (String.IsNullOrEmpty(vendedor))
                {
                    MessageBox.Show("Informe o código do Vendedor", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    vendedor = Interaction.InputBox("Digite o código do Vendedor", "Código Vendedor");
                }
                else
                {
                    txtVendedor.Text = vendedor;
                    retorno = dColaborador.DadosColaborador(Principal.empAtual, txtVendedor.Text.Trim(), true, "V", "S", "X");
                    if (Funcoes.LeParametro(4, "73", true).Equals("S"))
                    {
                        if (retorno.Equals("S") || retorno.Equals("X"))
                        {
                            DateTime dtInicial = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00:00");
                            DateTime dtFinal = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");

                            double retiradas = dColaborador.TotalVendasPorPeriodo(dtInicial, dtFinal, Principal.usuario, "-");
                            double entradas = dColaborador.TotalVendasPorPeriodo(dtInicial, dtFinal, Principal.usuario, "+");

                            double emCaixa = entradas - retiradas;
                            double valorMaximo = Convert.ToDouble(Funcoes.LeParametro(4, "74", true));
                            if (emCaixa >= valorMaximo)
                            {
                                MessageBox.Show("Necessário realizar SANGRIA! Total em Caixa: R$ " + String.Format("{0:N}", emCaixa), "SG PHARMA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(retorno))
                    {
                        if (Funcoes.LeParametro(6, "352", true).Equals("S"))
                        {
                            if (Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao) != retorno && retorno != "S")
                            {
                                MessageBox.Show("Perfil não Liberado para logar nessa Estação!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtVendedor.Text = "";
                                txtVendedor.Focus();
                                return;
                            }
                        }

                        txtVendedor.Enabled = false;
                        frmVenVSenha senha = new frmVenVSenha(txtVendedor.Text.Trim());
                        //IDENTIFICA SE OBRIGA SENHA DO COLABORADOR//
                        if (Funcoes.LeParametro(6, "346", true).Equals("S"))
                        {
                            senha.ShowDialog();
                        }
                        else
                            senha.sValida = true;


                        if (senha.sValida.Equals(true))
                        {
                            CamposVisiveis();
                            this.Text = "SG PHARMA - Sistema de Gestão - VENDA POR: " + Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", txtVendedor.Text) + " USUÁRIO:  " + Principal.usuario.ToUpper();

                            Principal.codigoVendedor = txtVendedor.Text;
                        }
                        else
                        {
                            txtVendedor.Text = "";
                            txtVendedor.Enabled = true;
                            txtVendedor.Focus();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Vendedor não cadastrado", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtVendedor.Enabled = true;
                        txtVendedor.Text = "";
                        txtVendedor.Focus();
                    }
                }
            }
        }

        private void txtComanda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }

            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtComanda.Text.Trim()))
                {
                    if (!CarregarComanda())
                    {
                        txtComanda.Focus();
                    }
                    else if (!String.IsNullOrEmpty(txtCodCliEmp.Text))
                    {
                        txtProduto.Focus();
                    }
                    else
                        txtCodCliEmp.Focus();
                }
                else
                {
                    txtCodCliEmp.Focus();
                }
            }

        }

        public bool CarregarComanda()
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Comanda";

                //VERIFICA SE UTILIZA CADASTRO DE COMANDA//
                if (Funcoes.LeParametro(6, "137", true).Equals("S"))
                {
                    if (String.IsNullOrEmpty(dComanda.ValidaComanda(Principal.empAtual, Principal.estAtual, txtComanda.Text.Trim())))
                    {
                        MessageBox.Show("Comanda Inválida! Verifique o cadastro de comanda", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtComanda.Text = String.Empty;
                        txtComanda.Focus();
                        return false;
                    }
                }

                //VERIFICA SE COMANDA ESTA ABERTA//
                if (dComanda.IdentificaComandaAberta(Principal.estAtual, Principal.empAtual, txtComanda.Text.Trim()).Equals("A"))
                {
                    #region COMANDA ABERTA
                    if (Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("C") || Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F"))
                    {
                        vendaDeComanda = true;
                    }
                    else
                        vendaDeComanda = false;

                    dtPesq = dadosDaVenda.BuscaDadosDaVendaPorComanda(txtComanda.Text.Trim(), Principal.estAtual, Principal.empAtual);
                    vendaID = Convert.ToInt64(dtPesq.Rows[0]["VENDA_ID"]);
                    txtGAjuste.Text = String.Format("{0:N}", dtPesq.Rows[0]["VENDA_DIFERENCA"]);
                    dtPedido = DateTime.Now;
                    if (Funcoes.LeParametro(6, "360", true).Equals("S"))
                    {
                        txtAjuste.Text = String.Format("{0:N}", dtPesq.Rows[0]["VENDA_AJUSTE_GERAL"]);
                    }
                    //DADOS DO CLIENTE//
                    Principal.doctoCliente = dtPesq.Rows[0]["CF_DOCTO"].ToString();
                    dtRetorno = dCliente.DadosClienteFiltro(Principal.doctoCliente, 1, out filtro);
                    if (dtRetorno.Rows.Count == 1)
                    {
                        lblCliEmp.Visible = true;
                        lblCliEmp.Text = "Cliente/Emp.";
                        txtCodCliEmp.Visible = true;
                        txtCliEmp.Visible = true;
                        codCliente = dtRetorno.Rows[0]["CF_CODIGO"].ToString() == "" ? "0" : dtRetorno.Rows[0]["CF_CODIGO"].ToString();
                        txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                        doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                        IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                        statusCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_STATUS"]);
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CON_CODIGO"].ToString()))
                        {
                            var conveniada = new Conveniada();
                            double desconto = conveniada.IdentificaDescontoConveniada(dtRetorno.Rows[0]["CON_CODIGO"].ToString());
                            if (desconto != 0)
                            {
                                if (Funcoes.LeParametro(6, "360", true).Equals("S"))
                                {
                                    txtAjuste.Text = String.Format("{0:N}", desconto * -1);
                                }
                            }

                            txtCodCliEmp.Text = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                            wsConID = Convert.ToInt32(dtRetorno.Rows[0]["CON_ID"]);
                            wsEmpresa = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                            wsConWeb = dtRetorno.Rows[0]["CON_WEB"].ToString();
                            wsConCodConv = dtRetorno.Rows[0]["CON_COD_CONV"].ToString();
                        }
                        else
                        {
                            txtCodCliEmp.Text = codCliente.ToString();
                        }

                        txtCodCliEmp.Enabled = false;
                        DadosDaVendaCliente(Principal.doctoCliente);

                        this.Text = "SG PHARMA - Sistema de Gestão - VENDA POR: " + Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", dtPesq.Rows[0]["VENDA_COL_CODIGO"].ToString()) + "       USUÁRIO:  " + Principal.usuario.ToUpper();

                    }
                    else
                    {
                        Principal.mensagem = "Necessário incluir os dados do Cliente!";
                        Funcoes.Avisa();
                        txtComanda.Focus();
                        return false;
                    }

                    vendaBeneficio = dtPesq.Rows[0]["VENDA_BENEFICIO"].ToString();
                    dadosDaVenda.VendaColCodigo = Convert.ToInt32(dtPesq.Rows[0]["VENDA_COL_CODIGO"]);

                    if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                    {
                        dadosDaVenda.VendaCartao = dtPesq.Rows[0]["VENDA_CARTAO"].ToString();
                        dadosDaVenda.VendaNomeCartao = dtPesq.Rows[0]["VENDA_NOME_CARTAO"].ToString();
                        dadosDaVenda.VendaConCodigo = dtPesq.Rows[0]["VENDA_CON_CODIGO"].ToString();
                        dadosDaVenda.VendaMedico = dtPesq.Rows[0]["VENDA_MEDICO"].ToString();
                        dadosDaVenda.VendaNumReceita = dtPesq.Rows[0]["VENDA_NUM_RECEITA"].ToString();
                        dadosDaVenda.VendaTransID = dtPesq.Rows[0]["VENDA_TRANS_ID"].ToString();
                        dadosDaVenda.VendaTransIDReceita = dtPesq.Rows[0]["VENDA_TRANS_ID_RECEITA"].ToString();
                        dadosDaVenda.VendaCartaoCodigo = dtPesq.Rows[0]["VENDA_CARTAO_CODIGO"].ToString();
                        dadosDaVenda.VendaAutorizacao = dtPesq.Rows[0]["VENDA_AUTORIZACAO"].ToString();
                        dadosDaVenda.VendaAutorizacaoReceita = dtPesq.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString();
                        dadosDaVenda.VendaConvenioParcela = Convert.ToInt32(dtPesq.Rows[0]["VENDA_CONVENIO_PARCELA"]);
                        if (!String.IsNullOrEmpty(dadosDaVenda.VendaTransID) && !String.IsNullOrEmpty(dadosDaVenda.VendaTransIDReceita))
                        {
                            checaMedComSemReceita = true;
                        }
                        else
                            checaMedComSemReceita = false;

                        wsCartao = dtPesq.Rows[0]["VENDA_CARTAO"].ToString();

                        wsNumCartao = dtPesq.Rows[0]["VENDA_CARTAO"].ToString();
                        wsNomeCartao = dtPesq.Rows[0]["VENDA_NOME_CARTAO"].ToString();
                        wsEmpresa = dtPesq.Rows[0]["VENDA_CON_CODIGO"].ToString();
                        wsObrigaReceita = dtPesq.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString();
                        wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", wsEmpresa);
                    }
                    else if (vendaBeneficio.Equals("FUNCIONAL"))
                    {
                        preAutorizouFuncional = true;
                        dadosDaVenda.VendaCartao = dtPesq.Rows[0]["VENDA_CARTAO"].ToString();
                        dadosDaVenda.VendaNomeCartao = dtPesq.Rows[0]["VENDA_NOME_CARTAO"].ToString();
                        dadosDaVenda.VendaConCodigo = dtPesq.Rows[0]["VENDA_CON_CODIGO"].ToString();
                        dadosDaVenda.VendaMedico = dtPesq.Rows[0]["VENDA_MEDICO"].ToString();
                        dadosDaVenda.VendaNumReceita = dtPesq.Rows[0]["VENDA_NUM_RECEITA"].ToString();
                        dadosDaVenda.VendaTransID = dtPesq.Rows[0]["VENDA_TRANS_ID"].ToString();
                        dadosDaVenda.VendaAutorizacao = dtPesq.Rows[0]["VENDA_AUTORIZACAO"].ToString();
                    }

                    var caixaBeneficio = new AFCaixa();
                    List<AFCaixa> status = caixaBeneficio.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);

                    if (!status.Count.Equals(0))
                    {
                        if (vendaBeneficio.Equals("VIDALINK"))
                        {
                            MessageBox.Show("VENDA BENEFICIO VIDALINK", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (!Principal.tipoCupom.Equals("C"))
                            {
                                MessageBox.Show("Utilize o Fiscal para Finalizar Venda VIDALINK", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }
                            bloqueiaProduto = true;
                            var comandaVidaLink = new BeneficioVidaLinkComanda();
                            dtRetorno = comandaVidaLink.BuscaComandaPorVendaID(vendaID);
                            vendaNumeroNSU = dtRetorno.Rows[0]["NSU"].ToString();
                        }
                        else if (vendaBeneficio.Equals("PORTALDROGARIA"))
                        {
                            MessageBox.Show("VENDA BENEFICIO PORTAL DA DROGARIA", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (!Principal.tipoCupom.Equals("C"))
                            {
                                MessageBox.Show("Utilize o Fiscal para Finalizar Venda Portal da Drogaria", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }
                            bloqueiaProduto = true;
                            var comandaPortalDaDrogaria = new BeneficioNovartisComanda();

                            dtRetorno = comandaPortalDaDrogaria.BuscaComandaPorVendaID(vendaID);
                            vendaNumeroNSU = dtRetorno.Rows[0]["NSU"].ToString();
                            wsNumCartao = dtRetorno.Rows[0]["CARTAO"].ToString();
                        }
                        else if (vendaBeneficio.Equals("FARMACIAPOPULAR"))
                        {
                            MessageBox.Show("VENDA BENEFICIO FARMACIAPOPULAR", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (!Principal.tipoCupom.Equals("C"))
                            {
                                MessageBox.Show("Utilize o Fiscal para Finalizar Venda Farmácia Popular", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }
                            bloqueiaProduto = true;

                            var comandaFarmaciaPopular = new FpComanda();
                            dtRetorno = comandaFarmaciaPopular.BuscaComandaPorVendaID(vendaID);
                            if (dtRetorno.Rows.Count > 0)
                                vendaNumeroNSU = dtRetorno.Rows[0]["DATASUS"].ToString();
                            else
                            {
                                MessageBox.Show("Erro ao buscar Venda Farmácia Popular Contate o Suporte", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else if (vendaBeneficio.Equals("EPHARMA"))
                        {
                            MessageBox.Show("VENDA BENEFICIO E-PHARMA", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (!Principal.tipoCupom.Equals("C"))
                            {
                                MessageBox.Show("Utilize o Fiscal para Finalizar Venda E-Pharma ", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return false;
                            }

                            var comandaEpharma = new EpharmaComanda();
                            dtRetorno = comandaEpharma.BuscaComandaPorVendaID(vendaID);
                            vendaNumeroNSU = dtRetorno.Rows[0]["NSU"].ToString();
                        }
                    }
                    else
                        venOrcamento = true;

                    var buscaItensVendas = new VendasItens();
                    dtPesq = buscaItensVendas.BuscaItensDaVendaPorID(vendaID, Principal.estAtual, Principal.empAtual);
                    for (int i = 0; i < dtPesq.Rows.Count; i++)
                    {
                        Array.Clear(dadosProdutos, 0, 27);

                        dadosProdutos[0] = dtPesq.Rows[i]["PROD_ID"].ToString();
                        dadosProdutos[1] = dtPesq.Rows[i]["PROD_CODIGO"].ToString();
                        dadosProdutos[2] = dtPesq.Rows[i]["PROD_DESCR"].ToString();
                        dadosProdutos[3] = dtPesq.Rows[i]["VENDA_ITEM_UNIDADE"].ToString();
                        dadosProdutos[4] = dtPesq.Rows[i]["VENDA_ITEM_UNITARIO"].ToString();
                        dadosProdutos[5] = dtPesq.Rows[i]["VENDA_ITEM_QTDE"].ToString();
                        dadosProdutos[6] = dtPesq.Rows[i]["VENDA_ITEM_SUBTOTAL"].ToString();
                        dadosProdutos[7] = Math.Abs(Convert.ToDecimal(dtPesq.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString();
                        dadosProdutos[8] = String.Format("{0:N}", Math.Round((Math.Abs(Convert.ToDecimal(dtPesq.Rows[i]["VENDA_ITEM_DIFERENCA"])) * 100) / Convert.ToDecimal(dtPesq.Rows[i]["VENDA_ITEM_SUBTOTAL"])));
                        dadosProdutos[9] = dtPesq.Rows[i]["VENDA_ITEM_TOTAL"].ToString();
                        dadosProdutos[12] = dtPesq.Rows[i]["VENDA_ITEM_COMISSAO"].ToString();
                        dadosProdutos[13] = dtPesq.Rows[i]["VENDA_PROMOCAO"].ToString();
                        dadosProdutos[14] = dtPesq.Rows[i]["VENDA_ITEM_RECEITA"].ToString();
                        dadosProdutos[16] = dtPesq.Rows[i]["PROD_ECF"].ToString();
                        dadosProdutos[17] = dtPesq.Rows[i]["VENDA_COL_CODIGO"].ToString();
                        if (vendaBeneficio.Equals("PARTICULAR") && Funcoes.LeParametro(6, "385", false).Equals("S"))
                        {
                            dadosProdutos[18] = "N";
                        }
                        else
                        {
                            dadosProdutos[18] = vendaBeneficio == "N" ? "N" : "S";
                        }

                        dadosProdutos[21] = dtPesq.Rows[i]["VENDA_DESC_LIBERADO"].ToString();
                        dadosProdutos[22] = dtPesq.Rows[i]["PROD_ESTATUAL"].ToString();
                        dadosProdutos[23] = dtPesq.Rows[i]["PROD_CUSME"].ToString();
                        dadosProdutos[24] = dtPesq.Rows[i]["NCM"].ToString();
                        dadosProdutos[25] = dtPesq.Rows[i]["VENDA_PRE_VALOR"].ToString();
                        dadosProdutos[26] = dtPesq.Rows[i]["PROD_CUSME"].ToString();
                        dadosProdutos[27] = dtPesq.Rows[i]["ALIQ_NAC"].ToString();
                        dadosProdutos[29] = Util.SelecionaCampoEspecificoDaTabela("PRECOS", "BLOQUEIA_DESCONTO", "PROD_CODIGO", dtPesq.Rows[i]["PROD_CODIGO"].ToString(), true, true, true);

                        //VERIFICA SE PRODUTO E CONTROLADO
                        if (dtPesq.Rows[i]["PROD_CONTROLADO"].ToString().Equals("S"))
                        {
                            dadosProdutos[10] = "S";
                            dadosProdutos[11] = dtPesq.Rows[i]["VENDA_ITEM_LOTE"].ToString();
                        }
                        else
                        {
                            dadosProdutos[10] = "N";
                            dadosProdutos[11] = dtPesq.Rows[i]["VENDA_ITEM_LOTE"].ToString();
                        }

                        //PARAMETRO QUE IDENTIFICA SE VERIFICA DESCONTO DO PRODUTO//
                        if (Funcoes.LeParametro(6, "220", true).Equals("S"))
                        {
                            int depCod, clasCod, subCod, prodCod, conId;

                            prodCod = dtPesq.Rows[i]["PROD_ID"].ToString() == "" ? 0 : Convert.ToInt32(dtPesq.Rows[i]["PROD_ID"].ToString());
                            conId = Principal.wsConID;
                            depCod = dtPesq.Rows[i]["DEP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDouble(dtPesq.Rows[i]["DEP_CODIGO"]));
                            clasCod = dtPesq.Rows[i]["CLAS_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDouble(dtPesq.Rows[i]["CLAS_CODIGO"]));
                            subCod = dtPesq.Rows[i]["SUB_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDouble(dtPesq.Rows[i]["SUB_CODIGO"]));

                            dtRetorno = Funcoes.VerificaProdDesconto(Principal.estAtual, conId, prodCod, depCod, clasCod, subCod);
                            if (dtRetorno.Rows.Count != 0)
                            {
                                //VERIFICA SE RETORNOU MAIS DE UMA CONDIÇÃO DE DESCONTO//
                                if (dtRetorno.Rows.Count == 1)
                                {
                                    dadosProdutos[19] = dtRetorno.Rows[0]["DESC_MIN"].ToString();
                                    dadosProdutos[20] = dtRetorno.Rows[0]["DESC_MAX"].ToString();
                                }
                            }
                            else
                            {
                                dadosProdutos[19] = "0";
                                dadosProdutos[20] = "0";
                            }
                        }
                        else
                        {
                            dadosProdutos[19] = "0";
                            dadosProdutos[20] = "0";
                        }

                        InserirProdutos(dadosProdutos, tProdutos);
                        InserirProdCupom(tProdutos);
                        btnUltimosProdutos.Enabled = true;
                    }

                    object sumObject;
                    sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                    lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                    var caixa = new AFCaixa();
                    string cxStatus = caixa.IdentificaStatusDoCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);
                    if (String.IsNullOrEmpty(cxStatus))

                    {
                        dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                        if (dtPesq.Rows.Count > 0)
                        {
                            codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                        }
                        else
                        {
                            Principal.mensagem = "Por Favor, inserir Condição de Pagamento - COMANDA";
                            Funcoes.Avisa();
                        }
                    }
                    else
                    {
                        if (vendaBeneficio.Equals("VIDALINK"))
                        {
                            var buscaDados = new BeneficioVidaLinkProdutos();
                            dtRetorno = buscaDados.IdentificaSomatorioVenda(vendaNumeroNSU);
                            if (Convert.ToDouble(dtRetorno.Rows[0]["AVISTA"]) == 0 && Convert.ToDouble(dtRetorno.Rows[0]["RECEBER"]) > 0)
                            {
                                sumObject = tProdutos.Compute("Sum(PROD_TOTAL)", "");

                                dtPesq = buscaDados.BuscaProdutosPorNSU(vendaNumeroNSU);
                                for (int i = 0; i < dtPesq.Rows.Count; i++)
                                {
                                    valorAutorizado += Convert.ToDouble(dtPesq.Rows[i]["PR_CLIENTE_ARECEB"]) * Convert.ToInt32(dtPesq.Rows[i]["QTD"]);
                                }

                                if (Convert.ToDouble(sumObject) != valorAutorizado)
                                    valorAutorizado = Convert.ToDouble(sumObject);

                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "25", true));
                                pagamentoTotalOuParcial = "T";
                            }
                            else if (Convert.ToDouble(dtRetorno.Rows[0]["AVISTA"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["RECEBER"]) > 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "25", true));
                                pagamentoTotalOuParcial = "P";
                                dtPesq = buscaDados.BuscaProdutosPorNSU(vendaNumeroNSU);
                                for (int i = 0; i < dtPesq.Rows.Count; i++)
                                {
                                    diferencaDoValorAutorizado += Convert.ToDouble(dtPesq.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtPesq.Rows[i]["QTD"]);
                                }
                                valorAutorizado = Convert.ToDouble(lblSubTotal.Text) - diferencaDoValorAutorizado;
                            }
                        }
                        else if (vendaBeneficio.Equals("EPHARMA"))
                        {
                            EpharmaProdutos itens = new EpharmaProdutos();
                            DataTable dtItens = itens.BuscaItensPorNsu(Convert.ToInt32(vendaNumeroNSU), Convert.ToInt32(numTrans));
                            Double aReceber = 0;
                            Double totalBeneficio = 0;
                            double totalEpharmaVenda = 0;

                            for (int i = 0; i < dtItens.Rows.Count; i++)
                            {
                                aReceber = aReceber + (Convert.ToDouble(dtItens.Rows[i]["VL_PFINAL"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                                totalBeneficio = totalBeneficio + (Convert.ToDouble(dtItens.Rows[i]["VL_REPASSE"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                                totalEpharmaVenda = totalEpharmaVenda + (Convert.ToDouble(dtItens.Rows[i]["VL_PFABRICA"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                            }

                            if (aReceber == 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "18", true));
                                pagamentoTotalOuParcial = "T";
                                valorAutorizado = totalBeneficio;
                            }
                            else if (aReceber != totalEpharmaVenda)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "18", true));
                                diferencaDoValorAutorizado = aReceber;
                                pagamentoTotalOuParcial = "P";
                                valorAutorizado = totalBeneficio;
                            }
                        }
                        else if (vendaBeneficio.Equals("FARMACIAPOPULAR"))
                        {
                            var buscaDados = new FpMedicamentosSolicitacao();
                            dtRetorno = buscaDados.IdentificaSomatorioVenda(Funcoes.RemoveCaracter(vendaNumeroNSU));
                            if (Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]) == 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "31", true));
                                pagamentoTotalOuParcial = "T";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]);
                            }
                            else if (Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]) > 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "31", true));
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) - Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]);
                                pagamentoTotalOuParcial = "P";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]);
                            }
                        }
                        else if (vendaBeneficio.Equals("FUNCIONAL"))
                        {
                            var buscaDados = new FuncionalComanda();
                            vendaNumeroNSU = dadosDaVenda.VendaTransID;
                            dtRetorno = buscaDados.BuscaDadosComandaFuncional((vendaNumeroNSU.PadLeft(6, '0')));

                            if (dtRetorno.Rows.Count != 0)
                            {
                                dtDadosComandaFuncional = dtRetorno;
                            }


                            if (Convert.ToString(dtRetorno.Rows[0]["TIPO_TRANSACAO"]).Equals("T"))
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "46", true));
                                pagamentoTotalOuParcial = "T";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["VALOR_CARTAO"]);
                            }
                            else if (Convert.ToString(dtRetorno.Rows[0]["TIPO_TRANSACAO"]).Equals("P"))
                            {

                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "46", true));
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["VALOR_AVISTA"]);
                                pagamentoTotalOuParcial = "P";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["VALOR_CARTAO"]);
                            }
                            else
                            {
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["TOTAL_A_PAGAR"]);
                            }
                        }
                        else if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                        {
                            var buscaDados = new DrogabellaComanda();
                            dtRetorno = buscaDados.IdentificaSeVendaEPorComanda(vendaID);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                retorno = Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "FORMA_ID", "FORMA_DESCRICAO", "CONVENIO", false, true, true);
                                if (!String.IsNullOrEmpty(retorno))
                                {
                                    codFormaPagto = Convert.ToInt32(retorno);
                                }

                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["VALOR_AUTORIZADO"]);
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["VALOR_DIFERENCA"]);
                            }
                            else
                            {
                                dtPesq = dadosDaVendaPagamento.BuscaFormasPagamentosPorVendaID(Principal.empAtual, Principal.estAtual, vendaID);
                                for (int i = 0; i < dtPesq.Rows.Count; i++)
                                {
                                    var formasPagto = new VendasFormaPagamento();
                                    formasPagto.EmpCodigo = Convert.ToInt32(dtPesq.Rows[i]["EMP_CODIGO"]);
                                    formasPagto.EstCodigo = Convert.ToInt32(dtPesq.Rows[i]["EST_CODIGO"]);
                                    formasPagto.VendaId = Convert.ToInt64(dtPesq.Rows[i]["VENDA_ID"]);
                                    formasPagto.VendaFormaID = Convert.ToInt32(dtPesq.Rows[i]["VENDA_FORMA_ID"]);
                                    formasPagto.VendaParcela = Convert.ToInt32(dtPesq.Rows[i]["VENDA_PARCELA"]);
                                    formasPagto.VendaParcelaVencimento = Convert.ToDateTime(dtPesq.Rows[i]["VENDA_PARCELA_VENCIMENTO"]);
                                    formasPagto.VendaValorParcela = Convert.ToDouble(dtPesq.Rows[i]["VENDA_VALOR_PARCELA"]);
                                    vendaPedFormaPagto.Add(formasPagto);

                                    retorno = Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "FORMA_ID", "FORMA_DESCRICAO", "CONVENIO", false, true, true);
                                    if (retorno.Equals(formasPagto.VendaFormaID.ToString()))
                                    {
                                        valorAutorizado = Convert.ToDouble(dtPesq.Rows[i]["VENDA_VALOR_PARCELA"]);
                                    }
                                }
                            }

                            bloqueiaProduto = true;

                            for (int x = 0; x < tProdutos.Rows.Count; x++)
                            {
                                double valorDUni = 0;
                                if (tProdutos.Rows[x]["PROD_REC"].ToString().Equals("N"))
                                {
                                    valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                                    wsProdutosCupomSemReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["COD_BARRA"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                    if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"])) > 0)
                                    {
                                        wsProdutosCupomSemReceita += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"])).ToString().Replace(",", ".") + ")\n";
                                    }
                                    wsProdutosCupomSemReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                                }
                                else if (tProdutos.Rows[x]["PROD_REC"].ToString().Equals("S"))
                                {
                                    valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                                    wsProdutosCupomReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["COD_BARRA"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                    if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"])) > 0)
                                    {
                                        wsProdutosCupomReceita += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"])).ToString().Replace(",", ".") + ")\n";
                                    }
                                    wsProdutosCupomReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                                }
                            }

                            if (!String.IsNullOrEmpty(dadosDaVenda.VendaMedico))
                            {
                                wsDadosMedico += "CRM: " + dadosDaVenda.VendaMedico + " Num. Receita: " + dadosDaVenda.VendaNumReceita;
                            }
                        }

                        #region AJUSTE
                        if (Convert.ToDecimal(txtGAjuste.Text) > 0)
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)) + Convert.ToDecimal(txtGAjuste.Text));
                            txtGAjuste.ForeColor = System.Drawing.Color.Black;
                        }
                        else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                            txtGAjuste.ForeColor = System.Drawing.Color.Red;
                        }
                        else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                            txtGAjuste.ForeColor = System.Drawing.Color.Red;
                        }
                        #endregion

                        txtProduto.Focus();
                    }

                    #endregion
                }
                else
                {
                    if (permtVenda.Equals(true))
                    {
                        Principal.mensagem = "Comanda/Orçamento não encontrada!";
                        Funcoes.Avisa();
                        Limpar();
                        return false;
                    }
                    else if (Principal.tipoCupom.Equals("C"))
                    {
                        MessageBox.Show("Comanda em Branco!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                        return false;
                    }

                    #region NOVA COMANDA
                    vendaDeComanda = false;
                    lblRegistros.Text = "";
                    venOrcamento = true;
                    lblCliEmp.Visible = true;
                    txtCodCliEmp.Visible = true;
                    txtCliEmp.Visible = true;
                    txtTelefone.Visible = true;
                    lblTelefone.Visible = true;
                    txtTelefone.Enabled = true;
                    dtPedido = DateTime.Now;

                    //VERIFICA SE CAIXA ESTA ABERTO//
                    retorno = cxStatus.IdentificaStatusDoCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);
                    if (String.IsNullOrEmpty(retorno))
                    {
                        dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                        if (dtPesq.Rows.Count > 0)
                        {
                            codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                        }
                        else
                        {
                            Principal.mensagem = "Por Favor, inserir Condição de Pagamento - COMANDA";
                            Funcoes.Avisa();
                        }
                    }
                    txtCodCliEmp.Focus();
                    #endregion
                }

                txtProduto.Focus();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Limpar()
        {
            Funcoes.LimpaFormularios(this);
            CamposInvisiveis();
            lblConveniada.Visible = false;
            btnPreAutorizar.Visible = false;
            imgTipoTela.Visible = false;
            codCliente = "0";
            retorno = "";
            listCupom.Items.Clear();
            dadosDaVenda = null;
            dadosDaVenda = new Negocio.VendasDados();
            txtCodCliEmp.Text = String.Empty;
            txtCliEmp.Text = String.Empty;
            txtVendedor.Enabled = true;
            txtCodCliEmp.Enabled = true;
            preAutorizouFuncional = false;
            txtTelefone.Visible = false;
            txtTelefone.Enabled = false;
            txtTelefone.Text = "";
            lblTelefone.Visible = false;

            //VARIAVEIS GLOBAIS//
            wsCartao = String.Empty;
            wsPreAutoriza = String.Empty;
            wsEmpresa = String.Empty;
            tipoVenda = 0;
            checaMedComSemReceita = false;
            tProdutos.Rows.Clear();
            preAutorizou = false;
            wsCRM = String.Empty;
            wsUF = String.Empty;
            wsMNome = String.Empty;
            wsData = DateTime.Now;
            wsNumReceita = String.Empty;
            wsConselho = String.Empty;
            vendaBeneficio = "N";
            wsAbreOutraTransacao = false;
            wsNomeCartao = String.Empty;
            wsComSemRec = "N";
            lblRegistros.Text = "";
            wsConWeb = "0";
            wsSaldoCartao = 0;
            wsQtdeProdutosRetorno = 0;
            totalVendaComReceita = 0;
            totalVendaSemReceita = 0;
            vendaDeComanda = false;
            //VARIAVEIS DE VENDA//
            protocoloDeEntrega = "";
            comprovanteVenda = "";
            selForma = false;
            selEspecie = false;
            descontoCaixa = true;
            permtVenda = true;
            venOrcamento = false;
            Principal.NumCpfCnpj = "";
            Principal.NomeNP = "";
            vendaNumeroCaixa = "";
            wsConID = 0;
            wsEmpresa = "0";
            wsConWeb = "0";
            wsConCodConv = "0";
            vendaNumeroNota = "";
            wsProdutosCupomReceita = "";
            wsProdutosCupomSemReceita = "";
            wsDadosMedico = "";
            diferencaDoValorAutorizado = 0;
            txtBuscaPreco.Text = "0,00";
            valorAutorizado = 0;
            totalVendaConvenio = 0;
            entregaIsenta = false;
            txtGAjuste.ForeColor = System.Drawing.Color.Black;
            txtAjuste.ForeColor = System.Drawing.Color.Black;
            imprimeComprovante = false;
            txtQtde.Text = "1";
            txtAjuste.Text = "0,00";
            txtGAjuste.Text = "0,00";
            lblSubTotal.Text = "0,00";
            lblTotal.Text = "0,00";
            lblDesconto.Text = "0,00";
            codFormaPagto = 0;
            vendaID = 0;
            vendaNumeroNSU = "";
            vendaEspecie.Clear();
            venItens.Clear();
            cupomBeneficio = "";
            pagamentoTotalOuParcial = "";
            lotePortalDrogaria = "";
            codAutorizacaoPortalDrogaria = "";

            List<AFCaixa> cStatus = cxStatus.DataCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);
            if (cStatus.Count.Equals(0))
            {
                permtVenda = false;
            }
            else if (DateTime.Now.ToString("dd/MM/yyyy") != cStatus[0].DataAbertura.ToString("dd/MM/yyyy"))
            {
                permtVenda = false;
            }

            if (Funcoes.LeParametro(6, "384", false).Equals("S"))
            {
                if ((Funcoes.LeParametro(6, "353", false, Principal.nomeEstacao) == "C" ||
                    Funcoes.LeParametro(6, "353", false, Principal.nomeEstacao) == "F") && !permtVenda)
                {
                    MessageBox.Show("Estação CAIXA com CAIXA FECHADO ou com DATA DO DIA ANTERIOR. Por Favor Verificar!", "Validade Caixa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                }
            }
            lblConveniada.Text = "";
            numeroCFe = "";
            vendaPedFormaPagto.Clear();
            produtosAlterados.Clear();
            vendaItens.Clear();
            vendaEspecie.Clear();
            venItens.Clear();
            inserir = true;
            transAberta = false;
            btnUltimosProdutos.Enabled = false;
            Principal.NumeroSessao = "";
            valorTroco = 0;
            bloqueiaProduto = false;
            numeroSequencia = "";
            dadosDaVendaPagamento = null;
            dadosDaVendaPagamento = new Negocio.VendasFormaPagamento();
            Principal.SolicitacaoID = "0";
            exclusaoComanda = true;
            wsObrigaReceita = "N";
            vendaControlado = false;
            vEntr_End = "";
            vEntr_Numero = "";
            vEntr_Bairro = "";
            vEntr_Cidade = "";
            vEntr_Cep = "";
            vEntr_Fone = "";
            vEntr_UF = "";
            vEntr_Valor = 0;
            vEntr_Obs = "";
            lblProdutoOferecer.Text = Funcoes.LeParametro(6, "374", false);

            Principal.dtPesq = Util.CarregarCombosPorEmpresa("to_char(COL_CODIGO) as COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = -1;
            }

            timer1.Start();
            this.Text = "SG Pharma - Sistema de Gestão";
            txtVendedor.Focus();
        }

        private void txtCodCliEmp_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodCliEmp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                int validaNumero;
                try
                {
                    lblConveniada.Visible = false;
                    if (txtCodCliEmp.Text.Trim() != "" && vendaBeneficio.Equals("N"))
                    {
                        //VERIFICA SE E NUMERO OU LETRA, CASO SEJA NUMERO BUSCA EMPRESAS PARTICULAR OU CASO SEJA LETRA BUSCA CLIENTES//
                        if (int.TryParse(txtCodCliEmp.Text.Trim(), out validaNumero))
                        {
                            if (txtCodCliEmp.Text.Trim() == "7000")
                            {
                                Cursor = Cursors.WaitCursor;
                                vendaBeneficio = "FUNCIONAL";

                                FrmFuncionalConsulta objFormFuncionalConsulta = new FrmFuncionalConsulta(this);
                                objFormFuncionalConsulta.StartPosition = FormStartPosition.CenterScreen;
                                objFormFuncionalConsulta.ShowDialog();
                                btnUltimosProdutos.Enabled = true;
                                if (vendaBeneficio.Equals("N"))
                                {
                                    Limpar();
                                    Cursor = Cursors.Default;
                                    return;
                                }
                                else
                                {
                                    if (objTransacaoClienteCartaoFuncional.CodigoDaResposta != "00")
                                    {
                                        LimparFuncional();
                                        Cursor = Cursors.Default;
                                        MessageBox.Show("Erro Inesperado! Tente Novamente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                    else if (objTransacaoClienteCartaoFuncional.FecharForm == true)
                                    {
                                        LimparFuncional();
                                        Cursor = Cursors.Default;
                                        return;
                                    }
                                }
                            }
                            if (vendaBeneficio == "FUNCIONAL")
                            {
                                dtRetorno = dCliente.DadosClientePorEmpresaConveniadaFuncional(this.objTransacaoClienteCartaoFuncional.Trilha2DoCartao.Trim().ToUpper());

                            }
                            else
                            {
                                //BUSCA PELO CODIGO DA CONVENIADA
                                dtRetorno = dCliente.DadosClientePorEmpresaConveniada(txtCodCliEmp.Text.Trim().ToUpper());
                                if (dtRetorno.Rows.Count == 0)
                                {
                                    //SE NAO ACHA NADA BUSCA PELO CODIGO DO CLIENTE
                                    dtRetorno = dCliente.DadosClientePorCfCodigo(txtCodCliEmp.Text.Trim().ToUpper());
                                }
                            }
                        }
                        else
                        {
                            dtRetorno = dCliente.DadosClienteFiltro(txtCodCliEmp.Text.Trim().ToUpper(), 0, out filtro);
                        }


                        if (dtRetorno.Rows.Count == 1)
                        {
                            Principal.doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                            codCliente = dtRetorno.Rows[0]["CF_CODIGO"].ToString() == "" ? "0" : dtRetorno.Rows[0]["CF_CODIGO"].ToString();
                            txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                            doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                            IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                            statusCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_STATUS"]);
                            if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CON_CODIGO"].ToString()))
                            {
                                txtCodCliEmp.Text = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                                wsConID = Convert.ToInt32(dtRetorno.Rows[0]["CON_ID"]);
                                wsEmpresa = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                                wsConWeb = dtRetorno.Rows[0]["CON_WEB"].ToString();
                                wsConCodConv = dtRetorno.Rows[0]["CON_COD_CONV"].ToString();

                                if (Funcoes.LeParametro(6, "368", false).Equals("S"))
                                {
                                    if (wsConWeb.Equals("0") || wsConWeb.Equals("4"))
                                    {
                                        lblConveniada.Visible = true;
                                        lblConveniada.Text = dtRetorno.Rows[0]["CON_NOME"].ToString();
                                    }
                                    else
                                        lblConveniada.Visible = false;
                                }
                            }
                            else
                            {
                                if (codCliente == "0")
                                {
                                    txtCodCliEmp.Text = IDCliente.ToString();
                                }
                                else
                                    txtCodCliEmp.Text = codCliente;
                            }

                            if (wsConWeb == "1" && wsNumCartao != "")
                            {
                                MessageBox.Show("Cliente Drogabella/Plantão Card, necessário informar Cartão busca pelo nome não gera Autorização.\n"
                                    + "Escolher a opção Benefícios-Drogabella ou Plantão Card e buscar as informações do Cliente pelo Cartao!", "Validação Convênio",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtCodCliEmp.Text = "";
                                txtCliEmp.Text = "";
                                txtTelefone.Text = "";
                                txtTelefone.Enabled = true;
                                txtCodCliEmp.Enabled = true;
                                txtCliEmp.Enabled = true;
                                return;
                            }

                            if (Convert.ToDouble(dtRetorno.Rows[0]["CON_DESCONTO"]) != 0)
                            {
                                txtAjuste.Text = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[0]["CON_DESCONTO"]) * -1);
                            }


                            txtCodCliEmp.Enabled = false;
                            txtCliEmp.Enabled = false;

                            if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString()))
                            {
                                MessageBox.Show(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString().ToUpper(), "Observação do Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            if (statusCliente.Equals(1) || statusCliente.Equals(3) || statusCliente.Equals(5))
                            {
                                MessageBox.Show("Cliente Bloqueado! Verifique o cadastro.", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                if (Funcoes.LeParametro(6, "190", false).Equals("S"))
                                {
                                    frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                    senhaSup.ShowDialog();
                                    if (senhaSup.sValida.Equals(false))
                                    {
                                        MessageBox.Show("Não é possível prosseguir a venda sem a senha de autorização!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        Limpar();
                                        return;
                                    }
                                }
                            }
                            DadosDaVendaCliente(Principal.doctoCliente);
                            btnUltimosProdutos.Enabled = true;

                            if (!String.IsNullOrEmpty(txtAjuste.Text) && Convert.ToDouble(txtAjuste.Text) != 0)
                            {
                                txtAjuste.Focus();
                            }

                            txtProduto.Focus();
                        }
                        else if (dtRetorno.Rows.Count == 0 && !(vendaBeneficio == "FUNCIONAL"))
                        {
                            MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtCodCliEmp.Text = "";
                            txtCodCliEmp.Focus();
                        }
                        else
                        {
                            using (frmBuscaForn t = new frmBuscaForn("cliente"))
                            {
                                t.dgCliFor.DataSource = dtRetorno;
                                t.ShowDialog();

                                if (!String.IsNullOrEmpty(t.cfNome))
                                {
                                    Principal.doctoCliente = t.cfDocto;
                                    codCliente = Convert.ToString(t.cfCodigo);
                                    txtCliEmp.Text = t.cfNome;
                                    doctoCliente = t.cfDocto;
                                    IDCliente = t.cfID;
                                    statusCliente = t.cfStatus;
                                    if (!String.IsNullOrEmpty(t.conCodigo))
                                    {
                                        txtCodCliEmp.Text = t.conCodigo;
                                        wsConID = t.conId;
                                        wsEmpresa = t.conCodigo;
                                        wsConWeb = t.conWeb;
                                        wsConCodConv = t.conCodConv;
                                    }
                                    else
                                    {
                                        if (codCliente == "0")
                                        {
                                            txtCodCliEmp.Text = IDCliente.ToString();
                                        }
                                        else
                                            txtCodCliEmp.Text = codCliente;
                                    }

                                    if (wsConWeb == "1" && wsNumCartao != "")
                                    {
                                        MessageBox.Show("Cliente Drogabella/Plantão Card, necessário informar Cartão busca pelo nome não gera Autorização.\n"
                                            + "Escolher a opção Benefícios-Drogabella ou Plantão Card e buscar as informações do Cliente pelo Cartao!", "Validação Convênio",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtCodCliEmp.Text = "";
                                        txtCliEmp.Text = "";
                                        txtTelefone.Text = "";
                                        txtTelefone.Enabled = true;
                                        txtCodCliEmp.Enabled = true;
                                        txtCliEmp.Enabled = true;
                                        return;
                                    }

                                    if (t.conDesconto != 0)
                                    {
                                        txtAjuste.Text = String.Format("{0:N}", t.conDesconto * -1);
                                    }

                                    txtTelefone.Enabled = false;
                                    txtCodCliEmp.Enabled = false;
                                    txtCliEmp.Enabled = false;

                                    dtRetorno = dCliente.DadosClienteFiltro(Convert.ToString(IDCliente), 2, out filtro);
                                    if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString()))
                                    {
                                        MessageBox.Show(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString().ToUpper(), "Observação do Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    if (statusCliente.Equals(1) || statusCliente.Equals(3) || statusCliente.Equals(5))
                                    {
                                        MessageBox.Show("Cliente Bloqueado! Verifique o cadastro.", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        if (Funcoes.LeParametro(6, "190", false).Equals("S"))
                                        {
                                            frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                            senhaSup.ShowDialog();
                                            if (senhaSup.sValida.Equals(false))
                                            {
                                                MessageBox.Show("Não é possível prosseguir a venda sem a senha de autorização!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                Limpar();
                                                return;
                                            }
                                        }
                                    }

                                    if (Funcoes.LeParametro(6, "368", false).Equals("S"))
                                    {
                                        if (!String.IsNullOrEmpty(t.conCodigo))
                                        {
                                            if (wsConWeb.Equals("0") || wsConWeb.Equals("4"))
                                            {
                                                lblConveniada.Visible = true;
                                                lblConveniada.Text = dtRetorno.Rows[0]["CON_NOME"].ToString();
                                            }
                                            else
                                                lblConveniada.Visible = false;
                                        }
                                        else
                                            lblConveniada.Visible = false;
                                    }

                                    DadosDaVendaCliente(Principal.doctoCliente);
                                    btnUltimosProdutos.Enabled = true;


                                    if (!String.IsNullOrEmpty(txtAjuste.Text) && Convert.ToDouble(txtAjuste.Text) != 0)
                                    {
                                        txtAjuste.Focus();
                                    }

                                    txtProduto.Focus();
                                }
                                else
                                {
                                    txtCodCliEmp.Enabled = true;
                                    txtCodCliEmp.Text = "";
                                    txtCodCliEmp.Focus();
                                }
                            }
                        }

                        #region VALIDACAO PARTICULAR
                        if (wsConWeb.Equals("4"))
                        {
                            retorno = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_FORMA_ID", "CON_CODIGO", txtCodCliEmp.Text);

                            if (!String.IsNullOrEmpty(retorno))
                                codFormaPagto = Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_FORMA_ID", "CON_CODIGO", txtCodCliEmp.Text));

                            if (Funcoes.LeParametro(6, "126", true).Equals("S"))
                            {
                                if (!ChecaLimiteParticular())
                                {
                                    Limpar();
                                    return;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        txtTelefone.Focus();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (wsConWeb.Equals("4"))
                    {
                        vendaBeneficio = "PARTICULAR";
                    }
                    Cursor = Cursors.Default;
                }
            }
        }

        private void txtProduto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try{
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtProduto.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(txtCodCliEmp.Text))
                        {
                            codCliente = Funcoes.LeParametro(6, "114", true);
                            txtCodCliEmp.Text = codCliente.ToString();
                            dtRetorno = dCliente.DadosClienteFiltro(codCliente.ToString(), 4, out filtro);
                            IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                            doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                            Principal.doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                            DadosDaVendaCliente(Principal.doctoCliente);
                            txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                            btnUltimosProdutos.Enabled = true;
                        }

                        if (!bloqueiaProduto)
                        {
                            if (!Funcoes.IdentificaSeContemSomenteLetras(txtProduto.Text.Trim()))
                            {
                                if (lerProdutos().Equals(false))
                                {
                                    Cursor = Cursors.WaitCursor;
                                    Application.DoEvents();

                                    txtProduto.Text = "";
                                    //FORM DE BUSCA DE PRODUTOS//
                                    using (frmBuscaProd buscaProd = new frmBuscaProd(true))
                                    {
                                        buscaProd.txtCodigo.Focus();
                                        buscaProd.ShowDialog();
                                    }
                                    split = Principal.codBarra.Split(',');
                                    if (split.Length > 0)
                                    {
                                        for (int i = 0; i < split.Length; i++)
                                        {
                                            if (!String.IsNullOrEmpty(split[i]))
                                            {
                                                txtProduto.Text = split[i];
                                                lerProdutos();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        txtProduto.Text = Principal.codBarra;
                                        lerProdutos();
                                    }
                                }
                            }
                            else
                            {
                                //FORM DE BUSCA DE PRODUTOS//
                                using (frmBuscaProd buscaProd = new frmBuscaProd(true))
                                {
                                    buscaProd.txtDescr.Text = txtProduto.Text.ToUpper();
                                    buscaProd.ShowDialog();
                                }
                                if (!String.IsNullOrEmpty(Principal.codBarra))
                                {
                                    split = Principal.codBarra.Split(',');
                                    if (split.Length > 0)
                                    {
                                        for (int i = 0; i < split.Length; i++)
                                        {
                                            if (!String.IsNullOrEmpty(split[i]))
                                            {
                                                txtProduto.Text = split[i];
                                                lerProdutos();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        txtProduto.Text = Principal.codBarra;
                                        lerProdutos();
                                    }
                                }
                                else
                                {
                                    txtProduto.Text = "";
                                    txtProduto.Focus();
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Inclusão de Novos Produtos não Permitida!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtProduto.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool lerProdutos()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Application.DoEvents();

                descMin = 0;
                descMax = 0;
                int depCod, clasCod, subCod, prodCod, qtdeEstque, qtdeTabela;
                string prodDescr;
                bool entrou = false;

                qtdeEstque = 0;
                qtdeTabela = 0;

                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Ler Produtos";

                dtBProd = dProduto.BuscaDadosProdutosPorCodigoBarras(Principal.estAtual, Principal.empAtual, txtProduto.Text.Trim());
                if (dtBProd.Rows.Count == 0)
                {
                    MessageBox.Show("Produto não Cadastrado!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else
                {
                    if (Funcoes.LeParametro(9, "56", false).Equals("S"))
                    {
                        if (Convert.ToInt32(dtBProd.Rows[0]["PROD_ESTMIN"]) != 0)
                        {
                            if (Convert.ToInt32(dtBProd.Rows[0]["PROD_ESTATUAL"]) <= Convert.ToInt32(dtBProd.Rows[0]["PROD_ESTMIN"]))
                            {
                                Principal.mensagem = "Produto com estoque mínimo! Atenção para reposição";
                                Funcoes.Avisa();
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(dtBProd.Rows[0]["PROD_MENSAGEM"].ToString()))
                    {
                        MessageBox.Show(dtBProd.Rows[0]["PROD_MENSAGEM"].ToString(), "Inf. Produto", MessageBoxButtons.OK);
                    }
                    prodCod = Convert.ToInt32(dtBProd.Rows[0]["PROD_ID"]);
                    depCod = dtBProd.Rows[0]["DEP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBProd.Rows[0]["DEP_CODIGO"]);
                    clasCod = dtBProd.Rows[0]["CLAS_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBProd.Rows[0]["CLAS_CODIGO"]);
                    subCod = dtBProd.Rows[0]["SUB_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBProd.Rows[0]["SUB_CODIGO"]);
                    prodDescr = dtBProd.Rows[0]["PROD_DESCR"].ToString();
                    qtdeEstque = Convert.ToInt32(dtBProd.Rows[0]["PROD_ESTATUAL"]);

                    dadosProdutos[0] = dtBProd.Rows[0]["PROD_ID"].ToString();
                    dadosProdutos[1] = txtProduto.Text.Trim();
                    dadosProdutos[2] = dtBProd.Rows[0]["PROD_DESCR"].ToString();
                    dadosProdutos[3] = dtBProd.Rows[0]["PROD_UNIDADE"].ToString();

                    if (Funcoes.LeParametro(6, "381", false).Equals("S") && vendaBeneficio.Equals("N"))
                    {
                        if (Convert.ToDouble(dtBProd.Rows[0]["PRECO_MAXIMO"]) > Convert.ToDouble(dtBProd.Rows[0]["PRE_VALOR"]))
                        {
                            dadosProdutos[4] = dtBProd.Rows[0]["PRECO_MAXIMO"].ToString();
                            dadosProdutos[28] = dtBProd.Rows[0]["PRECO_MAXIMO"].ToString();
                            dadosProdutos[6] = (Convert.ToDecimal(dtBProd.Rows[0]["PRECO_MAXIMO"]) * int.Parse(txtQtde.Text)).ToString();

                            double des = Convert.ToDouble(dtBProd.Rows[0]["PRECO_MAXIMO"]) - Convert.ToDouble(dtBProd.Rows[0]["PRE_VALOR"]);

                            dadosProdutos[7] = des.ToString();
                            dadosProdutos[8] = ((des * 100) / Convert.ToDouble(dtBProd.Rows[0]["PRECO_MAXIMO"])).ToString();
                            dadosProdutos[9] = ((Convert.ToDecimal(dtBProd.Rows[0]["PRECO_MAXIMO"]) * int.Parse(txtQtde.Text)) - Convert.ToDecimal(dadosProdutos[7])).ToString();
                            dadosProdutos[29] = "S";
                        }
                        else
                        {
                            dadosProdutos[4] = dtBProd.Rows[0]["PRE_VALOR"].ToString();
                            dadosProdutos[28] = dtBProd.Rows[0]["PRECO_MAXIMO"].ToString();
                            dadosProdutos[6] = (Convert.ToDecimal(dtBProd.Rows[0]["PRE_VALOR"]) * int.Parse(txtQtde.Text)).ToString();
                            dadosProdutos[7] = "0,00";
                            dadosProdutos[8] = "0,00";
                            dadosProdutos[9] = ((Convert.ToDecimal(dtBProd.Rows[0]["PRE_VALOR"]) * int.Parse(txtQtde.Text)) - Convert.ToDecimal(dadosProdutos[7])).ToString();
                            dadosProdutos[29] = dtBProd.Rows[0]["BLOQUEIA_DESCONTO"].ToString();
                        }
                    }
                    else
                    {
                        dadosProdutos[4] = dtBProd.Rows[0]["PRE_VALOR"].ToString();
                        dadosProdutos[28] = dtBProd.Rows[0]["PRECO_MAXIMO"].ToString();
                        dadosProdutos[6] = (Convert.ToDecimal(dtBProd.Rows[0]["PRE_VALOR"]) * int.Parse(txtQtde.Text)).ToString();
                        dadosProdutos[7] = "0,00";
                        dadosProdutos[8] = "0,00";
                        dadosProdutos[9] = ((Convert.ToDecimal(dtBProd.Rows[0]["PRE_VALOR"]) * int.Parse(txtQtde.Text)) - Convert.ToDecimal(dadosProdutos[7])).ToString();
                        dadosProdutos[29] = dtBProd.Rows[0]["BLOQUEIA_DESCONTO"].ToString();
                    }


                    dadosProdutos[5] = txtQtde.Text;
                    dadosProdutos[12] = Convert.ToDecimal(dtBProd.Rows[0]["PROD_COMISSAO"]).ToString();
                    dadosProdutos[13] = "N";
                    dadosProdutos[16] = dtBProd.Rows[0]["PROD_ECF"].ToString();
                    dadosProdutos[17] = txtVendedor.Text;
                    dadosProdutos[18] = "N";
                    dadosProdutos[22] = Convert.ToInt32(dtBProd.Rows[0]["PROD_ESTATUAL"]).ToString();
                    dadosProdutos[23] = dtBProd.Rows[0]["PROD_ULTCUSME"].ToString();
                    dadosProdutos[24] = dtBProd.Rows[0]["NCM"].ToString();
                    dadosProdutos[25] = dtBProd.Rows[0]["PRE_VALOR"].ToString();
                    dadosProdutos[26] = dtBProd.Rows[0]["PROD_CUSME"].ToString();
                    dadosProdutos[27] = dtBProd.Rows[0]["ALIQ_NAC"].ToString() == "" ? "0" : dtBProd.Rows[0]["ALIQ_NAC"].ToString();
                    dadosProdutos[21] = "N";


                    //VERIFICA SE PRODUTO E CONTROLADO
                    if (dtBProd.Rows[0]["PROD_CONTROLADO"].ToString().Equals("S"))
                    {
                        vendaControlado = true;
                        if (Funcoes.LeParametro(6, "359", true).Equals("S"))
                        {
                            dadosProdutos[10] = "S";
                            dadosProdutos[11] = Interaction.InputBox("Digite o Lote do Produto", "Lote do Produto");
                        }
                        else
                        {
                            dadosProdutos[10] = "S";
                            dadosProdutos[11] = "";
                        }
                    }
                    else
                        dadosProdutos[10] = "N";

                    //PARAMETRO QUE IDENTIFICA SE VERIFICA DESCONTO DO PRODUTO//
                    if (Funcoes.LeParametro(6, "220", true).Equals("S"))
                    {
                        dtRetorno = Funcoes.VerificaProdDesconto(Principal.estAtual, txtCodCliEmp.Text == Funcoes.LeParametro(6, "114", false) ? 0 :
                            Convert.ToInt32(wsConID), prodCod, depCod, clasCod, subCod);
                        if (dtRetorno.Rows.Count != 0)
                        {
                            //VERIFICA SE RETORNOU MAIS DE UMA CONDIÇÃO DE DESCONTO//
                            if (dtRetorno.Rows.Count == 1)
                            {
                                dadosProdutos[19] = dtRetorno.Rows[0]["DESC_MIN"].ToString();
                                dadosProdutos[20] = dtRetorno.Rows[0]["DESC_MAX"].ToString();
                            }
                        }
                        else
                        {
                            dadosProdutos[19] = "0";
                            dadosProdutos[20] = "0";
                        }
                    }
                    else
                    {
                        dadosProdutos[19] = "0";
                        dadosProdutos[20] = "0";
                    }

                    if (Convert.ToDecimal(dtBProd.Rows[0]["PRE_VALOR"]) == 0)
                    {
                        Principal.mensagem = "Preço de Venda igual a ZERO. Verifique o Cadastro!";
                        Funcoes.Avisa();
                        return false;
                    }

                    passouItens = false;

                    if (checaMedComSemReceita.Equals(true) || tipoVenda.Equals(3))
                    {
                        if (wsComSemRec.Equals("S"))
                        {
                            if (MessageBox.Show("O Medicamento " + txtProduto.Text.Trim() + " - " + prodDescr.ToUpper() + " possui Receita?", "Venda Convênio", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                dadosProdutos[14] = "S";
                            }
                            else
                                dadosProdutos[14] = "N";

                        }
                        else
                            dadosProdutos[14] = "S";
                    }
                    else
                    {
                        dadosProdutos[14] = "N";
                    }

                    //INCLUI NOVO REGISTRO//
                    InserirProdutos(dadosProdutos, tProdutos);

                    //VERIFICA SE ESTOQUE DO PRODUTO E MENOR QUE 0//
                    linha_resultado = tProdutos.Select("COD_BARRA = '" + txtProduto.Text.Trim() + "'");
                    if (linha_resultado.Length != 0)
                    {
                        for (int i = 0; i < linha_resultado.Length; i++)
                        {
                            qtdeTabela += Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]);
                        }
                    }

                    if (Funcoes.LeParametro(6, "115", true).Equals("S"))
                    {
                        if ((qtdeEstque - qtdeTabela) < 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Produto " + txtProduto.Text.Trim() + " abaixo do estoque!\n\nQuantidade de estoque atual é " + qtdeEstque;
                            Funcoes.Avisa();
                        }
                    }


                    Cursor = Cursors.WaitCursor;

                    #region VERIFICA SE PRODUTO ESTA EM PROMOCAO
                    linha_resultado = tProdutos.Select("COD_BARRA = '" + txtProduto.Text.Trim() + "'");
                    if (linha_resultado.Length != 0)
                    {
                        if (Funcoes.LeParametro(6, "388", false).Equals("N"))
                        {
                            dtRetorno = Funcoes.IdentificaProdutoEmPromocao(Principal.empAtual, Convert.ToInt32(linha_resultado[0]["PROD_ID"]), depCod, clasCod, subCod);
                            if (dtRetorno.Rows.Count != 0)
                            {
                                if (dtRetorno.Rows[0]["DESCONTO_PROGRESSIVO"].ToString() == "S" && Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]) == Convert.ToInt32(dtRetorno.Rows[0]["LEVE"]))
                                {
                                    if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                    {
                                        if (DateTime.Now.Day >= Convert.ToInt32(dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(dtRetorno.Rows[0]["DATA_FIM"]))
                                        {
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_PROMO"] = "S";
                                            linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                            linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                            linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                            linha_resultado[0].EndEdit();
                                        }
                                    }
                                    else if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                    {
                                        if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                            && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                        {
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_PROMO"] = "S";
                                            linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                            linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                            linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                            linha_resultado[0].EndEdit();
                                        }
                                    }
                                }
                                else
                                {
                                    //VERIFICA SE RETORNOU MAIS DE UMA CONDIÇÃO DE DESCONTO//
                                    if (dtRetorno.Rows.Count == 1)
                                    {
                                        if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                        {
                                            if (DateTime.Now.Day >= Convert.ToInt32(dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(dtRetorno.Rows[0]["DATA_FIM"]))
                                            {
                                                if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                                {
                                                    linha_resultado[0].BeginEdit();
                                                    linha_resultado[0]["PROD_PROMO"] = "S";
                                                    decimal porcentagem = Math.Round((100 * Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"])) / Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]), 2);
                                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(porcentagem));
                                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2));
                                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                        - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2), 2));
                                                    linha_resultado[0].EndEdit();
                                                }
                                                else if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                                {
                                                    linha_resultado[0].BeginEdit();
                                                    linha_resultado[0]["PROD_PROMO"] = "S";
                                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"])));
                                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2));
                                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                        - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2), 2));
                                                    linha_resultado[0].EndEdit();
                                                }
                                            }
                                        }
                                        else if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                        {
                                            if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                                && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                            {
                                                if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                                {
                                                    linha_resultado[0].BeginEdit();
                                                    linha_resultado[0]["PROD_PROMO"] = "S";
                                                    decimal porcentagem = Math.Round((100 * Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"])) / Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]), 2);
                                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(porcentagem));
                                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2));
                                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                        - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2), 2));
                                                    linha_resultado[0].EndEdit();
                                                }
                                                else if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                                {
                                                    linha_resultado[0].BeginEdit();
                                                    linha_resultado[0]["PROD_PROMO"] = "S";
                                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"])));
                                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2));
                                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                        - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2), 2));
                                                    linha_resultado[0].EndEdit();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (Funcoes.LeParametro(6, "388", false).Equals("S") && Funcoes.LeParametro(6, "389", false) != vendaBeneficio) 
                        {
                            if (Funcoes.LeParametro(6, "389", false) != vendaBeneficio)
                            {
                                dtRetorno = Funcoes.IdentificaProdutoEmPromocao(Principal.empAtual, Convert.ToInt32(linha_resultado[0]["PROD_ID"]), depCod, clasCod, subCod);
                                if (dtRetorno.Rows.Count != 0)
                                {
                                    if (dtRetorno.Rows[0]["DESCONTO_PROGRESSIVO"].ToString() == "S" && Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]) == Convert.ToInt32(dtRetorno.Rows[0]["LEVE"]))
                                    {
                                        if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                        {
                                            if (DateTime.Now.Day >= Convert.ToInt32(dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(dtRetorno.Rows[0]["DATA_FIM"]))
                                            {
                                                linha_resultado[0].BeginEdit();
                                                linha_resultado[0]["PROD_PROMO"] = "S";
                                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                    - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                                linha_resultado[0].EndEdit();
                                            }
                                        }
                                        else if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                        {
                                            if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                                && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                            {
                                                linha_resultado[0].BeginEdit();
                                                linha_resultado[0]["PROD_PROMO"] = "S";
                                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                    - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                                linha_resultado[0].EndEdit();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //VERIFICA SE RETORNOU MAIS DE UMA CONDIÇÃO DE DESCONTO//
                                        if (dtRetorno.Rows.Count == 1)
                                        {
                                            if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                            {
                                                if (DateTime.Now.Day >= Convert.ToInt32(dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(dtRetorno.Rows[0]["DATA_FIM"]))
                                                {
                                                    if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                                        decimal porcentagem = Math.Round((100 * Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"])) / Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]), 2);
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(porcentagem));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                            - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2), 2));
                                                        linha_resultado[0].EndEdit();
                                                    }
                                                    else if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"])));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                            - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2), 2));
                                                        linha_resultado[0].EndEdit();
                                                    }
                                                }
                                            }
                                            else if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                            {
                                                if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                                    && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                                {
                                                    if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                                        decimal porcentagem = Math.Round((100 * Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PRECO"])) / Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]), 2);
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(porcentagem));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                            - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(porcentagem)) / 100, 2), 2));
                                                        linha_resultado[0].EndEdit();
                                                    }
                                                    else if (Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"])));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_VUNI"]) * Convert.ToInt32(linha_resultado[0]["PROD_QTDE"]))
                                                            - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PROMO_PORC"]))) / 100, 2), 2));
                                                        linha_resultado[0].EndEdit();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //VERIFICA SE AJUSTE E DIFERENTE DE O//
                    txtAjuste.Text = txtAjuste.Text.Trim() == "" ? "0,00" : txtAjuste.Text;
                    if (!Convert.ToDecimal(txtAjuste.Text.Trim()).Equals("0"))
                    {
                        #region CHECA REGRAS DE DESCONTO
                        if (!Funcoes.LeParametro(6, "84", false).Equals(""))
                        {
                            tDesconto = Convert.ToDecimal(Funcoes.LeParametro(6, "84", false));
                        }
                        else
                            tDesconto = 100;


                        linha_resultado = tProdutos.Select("COD_BARRA = '" + txtProduto.Text.Trim() + "'");
                        if (linha_resultado.Length != 0)
                        {
                            if (Funcoes.LeParametro(6, "220", false).Equals("S") && linha_resultado[0]["PROD_PROMO"].Equals("N")
                                && linha_resultado[0]["DESC_LIB"].Equals("N") && Convert.ToDecimal(linha_resultado[0]["PROD_DVAL"]) != 0
                                && linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N")
                            {
                                if (!Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]).Equals(0))
                                {
                                    //CONSIDERA O AJUSTE COMO DESCONTO//
                                    if (Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())) > Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]) && entrou.Equals(false))
                                    {
                                        linha_resultado[0].BeginEdit();
                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]));
                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Convert.ToDecimal(linha_resultado[0]["DESC_MAX"])) / 100, 2));
                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(linha_resultado[0]["PROD_TOTAL"]) - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Convert.ToDecimal(linha_resultado[0]["DESC_MAX"])) / 100, 2), 2));
                                        linha_resultado[0].EndEdit();

                                        entrou = true;
                                    }
                                }
                                else if (Convert.ToDecimal(linha_resultado[0]["PROD_DVAL"]) == 0 && Convert.ToDecimal(txtAjuste.Text.Trim()) != 0)
                                {
                                    linha_resultado[0].BeginEdit();
                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(linha_resultado[0]["PROD_TOTAL"]) - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2), 2));
                                    linha_resultado[0].EndEdit();
                                }
                            }
                            //VERIFICA SE AJUSTE A MENOR QUE O TETO DE DESCONTO//
                            else if (Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())) < tDesconto && linha_resultado[0]["PROD_PROMO"].Equals("N") && linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N")
                            {
                                object qtde;
                                qtde = tProdutos.Compute("Sum(PROD_QTDE)", "COD_BARRA = '" + txtProduto.Text.Trim() + "'"); //Verifica a quantidade desse procuto na lista; 
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                if (Convert.ToInt32(qtde) == 1) //Feito essa condição para aplicar o desconto no somatório correto. 
                                {
                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(linha_resultado[0]["PROD_TOTAL"]) - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2), 2));
                                }
                            linha_resultado[0].EndEdit();
                            }
                            //CONSIDERA TETO DE DESCONTO COMO PORCENTAGEM CASO AJUSTE SEJA MAIOR//
                            else if (linha_resultado[0]["PROD_PROMO"].Equals("N") && linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N")
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", tDesconto);
                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * tDesconto) / 100, 2));
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(linha_resultado[0]["PROD_TOTAL"]) - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * tDesconto) / 100, 2), 2));
                                linha_resultado[0].EndEdit();
                            }

                            if (!String.IsNullOrEmpty(txtCodCliEmp.Text))
                            {
                                if (txtCodCliEmp.Text != Funcoes.LeParametro(6, "114", false))
                                {
                                    if (Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_OBRIGA_REGRA_DESCONTO", "CON_CODIGO", txtCodCliEmp.Text).Equals("S") && linha_resultado[0]["PROD_PROMO"].Equals("N") && linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N")
                                    {
                                        linha_resultado[0].BeginEdit();
                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]));
                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Convert.ToDecimal(linha_resultado[0]["DESC_MAX"])) / 100, 2));
                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(linha_resultado[0]["PROD_TOTAL"]) - Math.Round((Convert.ToDecimal(linha_resultado[0]["PROD_SUBTOTAL"]) * Convert.ToDecimal(linha_resultado[0]["DESC_MAX"])) / 100, 2), 2));
                                        linha_resultado[0].EndEdit();
                                    }
                                }
                            }
                        }

                        #endregion
                    }


                    //INSERE PRODUTOS NO CUPOM//
                    InserirProdCupom(tProdutos);

                }

                Array.Clear(dadosProdutos, 0, 20);
                txtQtde.Text = "1";
                txtProduto.Text = "";
                txtBuscaPreco.Text = "0,00";
                Principal.codBarra = "";

                IsentaTaxaEntrega();

                object sumObject;
                sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                txtProduto.Focus();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtQtde.Text = "1";
                txtProduto.Text = "";
                txtProduto.Focus();
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool InserirProdCupom(DataTable dtProd)
        {
            try
            {
                listCupom.Items.Clear();
                InserirCabecalho();

                subTotal = 0;
                sTotal = 0;
                sAjuste = 0;

                for (int i = 0; i < dtProd.Rows.Count; i++)
                {
                    if (Convert.ToInt16(dtProd.Rows[i]["PROD_QTDE"]).Equals(1))
                    {
                        retorno = dtProd.Rows[i]["PROD_DESCR"].ToString().Length > 22 ? dtProd.Rows[i]["PROD_DESCR"].ToString().ToUpper().Substring(0, 22) : dtProd.Rows[i]["PROD_DESCR"].ToString().ToUpper().PadRight(22, ' ');
                        listCupom.Items.Add(String.Format("{0,-14}{1,-22}{2,4}{3,2}{4,12}", dtProd.Rows[i]["COD_BARRA"], retorno, dtProd.Rows[i]["PROD_QTDE"], dtProd.Rows[i]["PROD_UNIDADE"], String.Format("{0:N}", Convert.ToDecimal(dtProd.Rows[i]["PROD_VUNI"]) * Convert.ToInt32(dtProd.Rows[i]["PROD_QTDE"]))));
                    }
                    else
                    {
                        retorno = dtProd.Rows[i]["PROD_DESCR"].ToString().Length > 14 ? dtProd.Rows[i]["PROD_DESCR"].ToString().ToUpper().Substring(0, 14) : dtProd.Rows[i]["PROD_DESCR"].ToString().ToUpper();
                        listCupom.Items.Add(String.Format("{0,-14}{1,-14}{2,4}{3,2}{4,-7}{5,10}", dtProd.Rows[i]["COD_BARRA"], retorno, dtProd.Rows[i]["PROD_QTDE"], dtProd.Rows[i]["PROD_UNIDADE"] + " X ", String.Format("{0:N}", Convert.ToDecimal(dtProd.Rows[i]["PROD_VUNI"])), String.Format("{0:N}", Convert.ToDecimal(dtProd.Rows[i]["PROD_VUNI"]) * Convert.ToInt32(dtProd.Rows[i]["PROD_QTDE"]))));
                    }
                    if (!Convert.ToDecimal(dtProd.Rows[i]["PROD_DVAL"]).Equals(0))
                    {
                        //PRODUTO COM DESCONTO//
                        listCupom.Items.Add(String.Format("{0,-14}{1,20}{2,20}", "desconto", "-" + String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtProd.Rows[i]["PROD_DVAL"]))), String.Format("{0:N}", Convert.ToDecimal(dtProd.Rows[i]["PROD_TOTAL"]))));
                    }

                    subTotal = subTotal + (Convert.ToDouble(dtProd.Rows[i]["PROD_VUNI"]) * Convert.ToInt32(dtProd.Rows[i]["PROD_QTDE"]));
                }

                if (tProdutos.Rows.Count > 0)
                {
                    sTotal = Convert.ToDouble(tProdutos.Compute("SUM(PROD_TOTAL)", ""));
                    sAjuste = Convert.ToDouble(tProdutos.Compute("SUM(PROD_DVAL)", ""));
                }
                else
                {
                    txtGAjuste.Text = "0,00";
                }

                lblSubTotal.Text = String.Format("{0:N}", subTotal);
                if (Convert.ToDouble(txtGAjuste.Text) > 0)
                    lblTotal.Text = String.Format("{0:N}", sTotal + Math.Abs(Convert.ToDouble(txtGAjuste.Text)));
                else
                    lblTotal.Text = String.Format("{0:N}", sTotal - Math.Abs(Convert.ToDouble(txtGAjuste.Text)));

                lblDesconto.Text = String.Format("{0:N}", sAjuste);

                //verificaTaxaEntr();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inserir Cupom", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public void InserirProdutos(string[] dadosProd, DataTable tProdutos)
        {
            #region INSERE PRODUTOS NO DATATABLE
            try
            {
                //VERIFICA SE CODIGO DE BARRAS JA EXISTE NA TABELA CASO NAO TENHA INSERE, SENAO SÓ ATUALIZA OS DADOS// 
                linha_resultado = tProdutos.Select("PROD_ID = '" + dadosProd[0] + "'");

                if (linha_resultado.Length == 0)
                {
                    DataRow row = tProdutos.NewRow();
                    row["PROD_ID"] = dadosProd[0];
                    row["COD_BARRA"] = dadosProd[1];
                    row["PROD_DESCR"] = dadosProd[2];
                    row["PROD_UNIDADE"] = dadosProd[3];
                    row["PROD_VUNI"] = Convert.ToDecimal(dadosProd[4]);
                    row["PROD_QTDE"] = Convert.ToDouble(dadosProd[5]);
                    row["PROD_SUBTOTAL"] = Convert.ToDecimal(dadosProd[6]);
                    row["PROD_DVAL"] = dadosProd[7];
                    row["PROD_DPORC"] = dadosProd[8];
                    row["PROD_TOTAL"] = Convert.ToDecimal(dadosProd[9]);
                    row["PROD_CONTROLADO"] = dadosProd[10];
                    row["PROD_LOTE"] = dadosProd[11];
                    row["PROD_DB"] = Convert.ToDecimal(dadosProd[12]);
                    row["PROD_PROMO"] = dadosProd[13];
                    row["PROD_REC"] = dadosProd[14];
                    row["CLAS_FISC"] = dadosProd[15];
                    row["ECF"] = dadosProd[16];
                    row["VENDEDOR"] = Convert.ToDouble(dadosProd[17]);
                    row["BENEFICIO"] = dadosProd[18];
                    row["DESC_MIN"] = dadosProdutos[19];
                    row["DESC_MAX"] = dadosProdutos[20];
                    row["DESC_LIB"] = dadosProdutos[21];
                    row["PROD_ESTATUAL"] = Convert.ToDouble(dadosProdutos[22]);
                    row["PROD_ULTCUSME"] = dadosProdutos[23];
                    row["NCM"] = dadosProdutos[24];
                    row["PRE_VALOR"] = dadosProdutos[25];
                    row["PROD_CUSME"] = dadosProdutos[26];
                    row["ALIQUOTA"] = dadosProdutos[27];
                    row["PRECO_MAXIMO"] = (dadosProdutos[28] == null ? "0" : dadosProdutos[28]);
                    row["BLOQUEIA_DESCONTO"] = dadosProdutos[29];
                    tProdutos.Rows.Add(row);
                }
                else
                {
                    string qtde = linha_resultado[0]["PROD_QTDE"].ToString();

                    int qtdeAtual = Convert.ToInt16(qtde) + Convert.ToInt16(dadosProd[5]);

                    decimal valorTotal;

                    valorTotal = Convert.ToDecimal(dadosProd[4]) * qtdeAtual;

                    int PorcDesc = Convert.ToInt16(linha_resultado[0]["PROD_DPORC"]);
                    decimal valDesc = (valorTotal * PorcDesc) / 100;

                    linha_resultado[0].BeginEdit();

                    linha_resultado[0]["PROD_QTDE"] = qtdeAtual;
                    linha_resultado[0]["PROD_SUBTOTAL"] = valorTotal.ToString("##0.00");
                    linha_resultado[0]["PROD_DVAL"] = valDesc.ToString("##0.00");
                    linha_resultado[0]["PROD_TOTAL"] = (valorTotal - valDesc).ToString("##0.00");

                    linha_resultado[0].EndEdit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inserir Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }

        public DataTable tabelaProdutos()
        {
            #region TABELA QUE GRAVA ITENS DA VENDA
            DataTable tableProduto = new DataTable("PRODUTOS");

            DataColumn coluna;

            coluna = new DataColumn("PROD_ID", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("COD_BARRA", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_DESCR", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_UNIDADE", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_VUNI", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_QTDE", typeof(int));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_SUBTOTAL", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_DVAL", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_DPORC", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_TOTAL", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_CONTROLADO", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_LOTE", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_DB", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_PROMO", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_REC", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("CLAS_FISC", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("ECF", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("VENDEDOR", typeof(int));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("BENEFICIO", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("DESC_MIN", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("DESC_MAX", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("DESC_LIB", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_ESTATUAL", typeof(int));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_ULTCUSME", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("NCM", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PRE_VALOR", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PROD_CUSME", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("ALIQUOTA", typeof(string));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("PRECO_MAXIMO", typeof(decimal));
            tableProduto.Columns.Add(coluna);

            coluna = new DataColumn("BLOQUEIA_DESCONTO", typeof(string));
            tableProduto.Columns.Add(coluna);

            return tableProduto;

            #endregion
        }

        public void verificaTaxaEntr()
        {
            #region VERIFICA SE COBRA TAXA DE ENTREGA
            try
            {
                if (Funcoes.LeParametro(6, "306", true).Equals("S") && tProdutos.Rows.Count > 0)
                {
                    if (Principal.flag.Equals(false))
                    {
                        if (Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_ISENCAO"]) > 0)
                        {
                            if (Convert.ToDecimal(lblSubTotal.Text) < Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_ISENCAO"]))
                            {
                                if (chkEntrega.Checked.Equals(true))
                                {
                                    lblSubTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) + Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_VALOR"]));
                                }
                                else if (chkEntrega.Checked.Equals(false) && Convert.ToDecimal(lblSubTotal.Text) != Convert.ToDecimal(tProdutos.Compute("SUM(PROD_TOTAL)", "")))
                                {
                                    lblSubTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_VALOR"]));
                                }

                                if (Convert.ToDecimal(txtGAjuste.Text) >= 0)
                                {
                                    lblTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) + Convert.ToDecimal(txtGAjuste.Text));
                                }
                                else
                                    lblTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                            }
                        }
                        else
                        {
                            if (chkEntrega.Checked.Equals(true))
                            {
                                lblSubTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) + Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_VALOR"]));
                            }
                            else if (chkEntrega.Checked.Equals(false) && Convert.ToDecimal(lblSubTotal.Text) > 0)
                            {
                                lblSubTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(Principal.dtBusca.Rows[0]["ENTR_VALOR"]));
                            }

                            if (Convert.ToDecimal(txtGAjuste.Text) >= 0)
                            {
                                lblTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) + Convert.ToDecimal(txtGAjuste.Text));
                            }
                            else
                                lblTotal.Text = String.Format("{0:N}", Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                        }
                    }

                    //}
                    //else
                    //{
                    //    MessageBox.Show("Necessário realizadar o cadastro da taxa de entrega", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    chkEntrega.Checked = false;
                    //    txtProduto.Focus();
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || Convert.ToInt32(txtQtde.Text.Trim()).Equals(0))
                    {
                        MessageBox.Show("Quantidade não pode em branco ou zero", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtQtde.Text = "1";
                        txtQtde.Focus();
                    }
                    else
                        txtProduto.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }


        private void txtAjuste_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtAjuste_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtProduto.Focus();
            }
        }

        private void txtAjuste_Validated(object sender, EventArgs e)
        {
            try
            {
                bool alterou = false;

                txtAjuste.Text = txtAjuste.Text.Trim() == "" ? "0,00" : txtAjuste.Text;


                if (!Convert.ToDecimal(txtAjuste.Text.Trim()).Equals(0))
                {
                    if (tProdutos.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Não existem produtos a serem calculados!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        descontoCaixa = true;
                        //VERIFICA SE BLOQUEIA DESCONTO NO CAIXA//
                        if (Funcoes.LeParametro(6, "317", true).Equals("S"))
                        {
                            var colaborador = new Colaborador();
                            if (!String.IsNullOrEmpty(colaborador.StatusColaborador(Principal.estAtual, txtVendedor.Text, "X")))
                            {
                                if (MessageBox.Show("Desconto não permitido! Deseja solicitar liberação?", "Liberação de Desconto", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                    senhaSup.ShowDialog();
                                    if (senhaSup.sValida.Equals(false))
                                    {
                                        descontoCaixa = false;
                                    }
                                }
                            }
                        }
                        if (descontoCaixa)
                        {
                            if (Convert.ToDecimal(txtAjuste.Text.Trim()) < 0)
                            {
                                txtAjuste.ForeColor = System.Drawing.Color.Red;
                                txtAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtAjuste.Text.Trim()));
                            }
                            else
                            {
                                txtAjuste.ForeColor = System.Drawing.Color.Red;
                                txtAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtAjuste.Text.Trim()) * -1);
                            }

                            #region CHECA REGRAS DE DESCONTO
                            if (!Funcoes.LeParametro(6, "84", false).Equals(""))
                            {
                                tDesconto = Convert.ToDecimal(Funcoes.LeParametro(6, "84", false));
                            }
                            else
                                tDesconto = 100;

                            alterou = false;
                            for (int i = 0; i < tProdutos.Rows.Count; i++)
                            {
                                linha_resultado = tProdutos.Select("COD_BARRA = '" + tProdutos.Rows[i]["COD_BARRA"] + "'");
                                if (linha_resultado.Length != 0)
                                {
                                    if (linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N" && linha_resultado[0]["PROD_PROMO"].ToString().Equals("N"))
                                    {
                                        if (Funcoes.LeParametro(6, "220", false).Equals("S") && tProdutos.Rows[i]["PROD_PROMO"].Equals("N") && tProdutos.Rows[i]["DESC_LIB"].Equals("N"))
                                        {
                                            if (Convert.ToDouble(linha_resultado[0]["DESC_MAX"]) == 0)
                                            {
                                                linha_resultado[0].BeginEdit();
                                                linha_resultado[0]["DESC_LIB"] = "S";
                                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(txtAjuste.Text.Trim())) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                linha_resultado[0].EndEdit();

                                                alterou = true;
                                            }
                                            else if (Math.Abs(Convert.ToDouble(txtAjuste.Text)) == Convert.ToDouble(linha_resultado[0]["DESC_MAX"]))
                                            {
                                                linha_resultado[0].BeginEdit();
                                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"]));
                                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100, 2));
                                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                linha_resultado[0].EndEdit();

                                                alterou = true;
                                            }
                                            else if (Math.Abs(Convert.ToDouble(txtAjuste.Text)) > Convert.ToDouble(linha_resultado[0]["DESC_MAX"]))
                                            {
                                                if (MessageBox.Show("Valor do desconto acima do percentual permitido!\nPercentual máximo permitido é de " + linha_resultado[0]["DESC_MAX"] + "%\n\r"
                                                        + "Deseja solicitar liberação para permitir percentual maior?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                                {
                                                    frmVenSupSenha senha = new frmVenSupSenha(0);
                                                    senha.ShowDialog();
                                                    if (senha.sValida.Equals(true))
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["DESC_LIB"] = "S";
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(txtAjuste.Text.Trim())) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                        linha_resultado[0].EndEdit();

                                                        alterou = true;
                                                    }
                                                    else
                                                    {
                                                        linha_resultado[0].BeginEdit();
                                                        linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"]));
                                                        linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100, 2));
                                                        linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                        linha_resultado[0].EndEdit();

                                                        alterou = true;
                                                    }
                                                }
                                                else
                                                {
                                                    linha_resultado[0].BeginEdit();
                                                    linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"]));
                                                    linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100, 2));
                                                    linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(tProdutos.Rows[i]["DESC_MAX"])) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                    linha_resultado[0].EndEdit();

                                                    alterou = true;
                                                }
                                            }
                                            else if (Math.Abs(Convert.ToDouble(txtAjuste.Text)) < Convert.ToDouble(linha_resultado[0]["DESC_MAX"]))
                                            {
                                                linha_resultado[0].BeginEdit();
                                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(txtAjuste.Text.Trim())) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                                linha_resultado[0].EndEdit();

                                                alterou = true;
                                            }
                                        }
                                        //VERIFICA SE AJUSTE A MENOR QUE O TETO DE DESCONTO//
                                        else if (Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())) < tDesconto && tProdutos.Rows[i]["PROD_PROMO"].Equals("N"))
                                        {
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim())));
                                            linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * Math.Abs(Convert.ToDecimal(txtAjuste.Text.Trim()))) / 100, 2));
                                            linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToDecimal(txtAjuste.Text.Trim())) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                            linha_resultado[0].EndEdit();

                                            alterou = true;
                                        }
                                        //CONSIDERA TETO DE DESCONTO COMO PORCENTAGEM CASO AJUSTE SEJA MAIOR//
                                        else if (tProdutos.Rows[i]["PROD_PROMO"].Equals("N"))
                                        {
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", tDesconto);
                                            linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_SUBTOTAL"]) * tDesconto) / 100, 2));
                                            linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) - Math.Abs((Convert.ToDecimal(tProdutos.Rows[i]["PROD_VUNI"]) * tDesconto) / 100), 2) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]));
                                            linha_resultado[0].EndEdit();

                                            alterou = true;
                                        }
                                    }
                                }
                            }
                            if (alterou.Equals(true))
                            {
                                InserirProdCupom(tProdutos);
                            }
                            txtProduto.Focus();
                            #endregion
                        }
                        else
                            txtAjuste.Text = "0,00";
                    }
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void listCupom_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (listCupom.SelectedIndex >= 5)
                {
                    Cursor = Cursors.WaitCursor;

                    if (!listCupom.SelectedItem.ToString().Substring(0, 2).Equals("de"))
                    {
                        linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                        if (linha_resultado.Length != 0)
                        {
                            //LIBERAÇÃO DE ALTERAÇÃO NA PROMOÇÃO//
                            if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "380", false).Equals("S"))
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_PROMO"] = "N";
                                linha_resultado[0].EndEdit();

                                linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                            }

                            if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("N"))
                            {
                                MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " já esta em promoção, desconto não permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtProduto.Focus();
                            }
                            else if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("S") && !vendaBeneficio.Equals("PARTICULAR") && !chkEntrega.Checked)
                            {
                                MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " já esta em promoção, desconto não permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtProduto.Focus();
                            }
                            else if (linha_resultado[0]["BENEFICIO"].ToString().Equals("N") || vendaBeneficio.Equals("PORTALDROGARIA") || vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                            {
                                if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("S") && (vendaBeneficio.Equals("PARTICULAR") || chkEntrega.Checked))
                                {
                                    linha_resultado[0].BeginEdit();
                                    linha_resultado[0]["PROD_PROMO"] = "N";
                                    linha_resultado[0].EndEdit();

                                    linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                                }

                                if (!Funcoes.LeParametro(6, "84", true).Equals(""))
                                {
                                    tDesconto = Convert.ToDecimal(Funcoes.LeParametro(6, "84", false));
                                }
                                else
                                    tDesconto = 100;

                                CamposProdutoVisivel();

                                //EXIBE O CAMPO DE COMISSAO//
                                if (Funcoes.LeParametro(6, "117", false).Equals("S"))
                                {
                                    lblComissao.Visible = true;
                                    txtComissao.Visible = true;
                                }
                                else
                                {
                                    lblComissao.Visible = false;
                                    txtComissao.Visible = false;
                                }

                                //EXIBE O CAMPO DE ULTIMO CUSTO//
                                if (Funcoes.LeParametro(6, "349", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    lblCusto.Visible = true;
                                    txtCusto.Visible = true;
                                }
                                else
                                {
                                    lblCusto.Visible = false;
                                    txtCusto.Visible = false;
                                }

                                btnOk.Enabled = true;
                                btnCancela.Enabled = true;
                                btnExcluir.Enabled = true;
                                txtVUni.ReadOnly = false;
                                txtPQtde.ReadOnly = false;
                                txtSTotal.ReadOnly = false;
                                txtPDesc.ReadOnly = false;
                                txtVDesc.ReadOnly = false;
                                txtVTotal.ReadOnly = false;

                                lblNumCodBarras.Text = linha_resultado[0]["COD_BARRA"].ToString();
                                lblNome.Text = linha_resultado[0]["PROD_DESCR"].ToString().Length <= 20 ? linha_resultado[0]["PROD_DESCR"].ToString() : linha_resultado[0]["PROD_DESCR"].ToString().Substring(0, 20);
                                txtVUni.Text = String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]);
                                txtPQtde.Text = linha_resultado[0]["PROD_QTDE"].ToString();
                                txtSTotal.Text = String.Format("{0:N}", linha_resultado[0]["PROD_SUBTOTAL"]);
                                txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"].ToString() == "" ? 0 : Math.Abs(Convert.ToDouble(linha_resultado[0]["PROD_DPORC"])));
                                txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"].ToString() == "" ? 0 : linha_resultado[0]["PROD_DVAL"]);
                                txtVTotal.Text = String.Format("{0:N}", linha_resultado[0]["PROD_TOTAL"]);
                                txtEstoque.Text = linha_resultado[0]["PROD_ESTATUAL"].ToString();
                                txtComissao.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DB"]);
                                txtCusto.Text = String.Format("{0:N}", linha_resultado[0]["PROD_ULTCUSME"]);
                                txtPQtde.Focus();

                            }
                            else
                            {
                                MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " de benefício, alteração não permitida!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtProduto.Focus();
                            }
                        }
                        else
                        {
                            txtProduto.Focus();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtVUni_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPQtde.Focus();
        }

        private void txtVUni_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtVUni.ReadOnly.Equals(false))
                {
                    if (Funcoes.LeParametro(6, "07", false).Equals("N") && linha_resultado[0]["COD_BARRA"].ToString() != Funcoes.LeParametro(6, "369", false))
                    {
                        if (!String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]).Equals(String.Format("{0:N}", txtVUni.Text)))
                        {
                            MessageBox.Show("Não é permitido alterar o valor unitário do produto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtVUni.Text = String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]);
                            txtVUni.Focus();
                        }
                    }
                    else if (Funcoes.LeParametro(6, "07", false).Equals("S") && linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "S")
                    {
                        if (!String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]).Equals(String.Format("{0:N}", txtVUni.Text)))
                        {
                            MessageBox.Show("Não é permitido alterar o valor unitário do produto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtVUni.Text = String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]);
                            txtVUni.Focus();
                        }
                    }
                    else if (!String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]).Equals(String.Format("{0:N}", txtVUni.Text)))
                    {
                        txtSTotal.Text = String.Format("{0:N}", Convert.ToInt32(txtPQtde.Text.Trim()) * Convert.ToDecimal(txtVUni.Text.Trim()));
                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                        txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                        txtVTotal.Text = String.Format("{0:N}", Math.Round(Convert.ToDecimal(txtVUni.Text.Trim()) - Math.Abs((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100), 2) * Convert.ToInt32(txtPQtde.Text.Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    lblSubTotal.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtSTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPDesc.Focus();
        }

        private void txtSTotal_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtSTotal.ReadOnly.Equals(false))
                {
                    if (!String.IsNullOrEmpty(txtSTotal.Text))
                        txtSTotal.Text = String.Format("{0:N}", Convert.ToInt32(txtPQtde.Text.Trim()) * Convert.ToDecimal(txtVUni.Text.Trim()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVDesc.Focus();
        }

        private void txtPDesc_Validated(object sender, EventArgs e)
        {
            try
            {
                supSenha = String.Empty;
                if (txtPDesc.ReadOnly.Equals(false))
                {
                    if (!String.IsNullOrEmpty(txtPDesc.Text))
                    {
                        if (Convert.ToDecimal(linha_resultado[0]["PROD_DPORC"]) != Convert.ToDecimal(txtPDesc.Text.Trim()))
                        {
                            if (linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N" && linha_resultado[0]["PROD_PROMO"].ToString().Equals("N"))
                            {
                                //VERIFICA SE PERCENTUAL ESTA DENTRO DO MAXIMO PERMITIDO//
                                if (Convert.ToDecimal(txtPDesc.Text.Trim()) <= tDesconto)
                                {
                                    descontoCaixa = true;
                                    //VERIFICA SE BLOQUEIA DESCONTO NO CAIXA//
                                    if (Funcoes.LeParametro(6, "317", true).Equals("S"))
                                    {
                                        var colaborador = new Colaborador();
                                        if (!String.IsNullOrEmpty(colaborador.StatusColaborador(Principal.estAtual, txtVendedor.Text, "X")))
                                        {
                                            if (MessageBox.Show("Desconto não permitido! Deseja solicitar liberação?", "Liberação de Desconto", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                frmVenSupSenha senha = new frmVenSupSenha(0);
                                                senha.ShowDialog();
                                                if (senha.sValida.Equals(false))
                                                {
                                                    descontoCaixa = false;
                                                }
                                            }
                                            else
                                                descontoCaixa = false;
                                        }
                                    }
                                    if (descontoCaixa)
                                    {
                                        if (Funcoes.LeParametro(6, "220", false).Equals("S"))
                                        {
                                            #region VERIFICA PORCENTAGEM DE DESCONTO
                                            if (linha_resultado[0]["DESC_LIB"].ToString().Equals("N"))
                                            {
                                                if (Convert.ToDecimal(txtPDesc.Text.Trim()) > Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]) && Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]) != 0)
                                                {
                                                    if (MessageBox.Show("Valor do desconto acima do percentual permitido!\nPercentual máximo permitido é de " + linha_resultado[0]["DESC_MAX"] + "%\n\r"
                                                        + "Deseja solicitar liberação para permitir percentual maior?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                                    {
                                                        frmVenSupSenha senha = new frmVenSupSenha(0);
                                                        senha.ShowDialog();
                                                        if (senha.sValida.Equals(true))
                                                        {
                                                            linha_resultado[0].BeginEdit();
                                                            linha_resultado[0]["DESC_LIB"] = "S";
                                                            linha_resultado[0].EndEdit();

                                                            txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtPDesc.Text.Trim()));
                                                            txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                            txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                            txtVDesc.Focus();
                                                        }
                                                        else
                                                        {
                                                            txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                                            txtPDesc.Focus();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                        txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                    }
                                                }
                                                else if (Convert.ToDecimal(txtPDesc.Text.Trim()) < Convert.ToDecimal(linha_resultado[0]["DESC_MIN"]))
                                                {
                                                    txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(linha_resultado[0]["DESC_MIN"]));
                                                    txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                    txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                }
                                                else
                                                {
                                                    txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtPDesc.Text.Trim()));
                                                    txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                    txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                }
                                            }
                                            else
                                            {
                                                txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtPDesc.Text.Trim()));
                                                txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtPDesc.Text.Trim()));
                                            txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                            txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                        }
                                    }
                                    else
                                    {
                                        txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                        txtPDesc.Focus();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Percentual acima do máximo permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                    txtPDesc.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Desconto não pode ser incluído neste produto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                txtPDesc.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVTotal.Focus();
        }

        private void txtVDesc_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtVDesc.ReadOnly.Equals(false))
                {
                    if (!String.IsNullOrEmpty(txtVDesc.Text))
                    {
                        supSenha = String.Empty;

                        if (Convert.ToDecimal(linha_resultado[0]["PROD_DVAL"]) != Convert.ToDecimal(txtVDesc.Text.Trim()))
                        {
                            if (linha_resultado[0]["BLOQUEIA_DESCONTO"].ToString() == "N" && linha_resultado[0]["PROD_PROMO"].ToString().Equals("N"))
                            {
                                //VERIFICA SE PERCENTUAL ESTA DENTRO DO MAXIMO PERMITIDO//
                                if (Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2) <= tDesconto)
                                {
                                    descontoCaixa = true;
                                    //VERIFICA SE BLOQUEIA DESCONTO NO CAIXA//
                                    if (Funcoes.LeParametro(6, "317", true).Equals("S"))
                                    {
                                        var colaborador = new Colaborador();
                                        if (!String.IsNullOrEmpty(colaborador.StatusColaborador(Principal.estAtual, txtVendedor.Text, "X")))
                                        {
                                            if (MessageBox.Show("Desconto não permitido! Deseja solicitar liberação?", "Liberação de Desconto", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                                senhaSup.ShowDialog();
                                                if (senhaSup.sValida.Equals(false))
                                                {
                                                    descontoCaixa = false;
                                                }
                                            }
                                            else
                                                descontoCaixa = false;
                                        }
                                    }
                                    if (descontoCaixa)
                                    {
                                        if (Funcoes.LeParametro(6, "220", false).Equals("S"))
                                        {
                                            #region VERIFICA PORCENTAGEM DE DESCONTO
                                            if (linha_resultado[0]["DESC_LIB"].ToString().Equals("N"))
                                            {
                                                decimal porcentagem = Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2);

                                                porcentagem = porcentagem - Convert.ToDecimal(0.02);

                                                if (porcentagem > Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]) && Convert.ToDecimal(linha_resultado[0]["DESC_MAX"]) != 0)
                                                {
                                                    if (MessageBox.Show("Valor do desconto acima do percentual permitido!\nPercentual máximo permitido é de " + linha_resultado[0]["DESC_MAX"] + "%\n\r"
                                                        + "Deseja solicitar liberação para permitir percentual maior?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                                                    {
                                                        frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                                        senhaSup.ShowDialog();
                                                        if (senhaSup.sValida.Equals(true))
                                                        {
                                                            linha_resultado[0].BeginEdit();
                                                            linha_resultado[0]["DESC_LIB"] = "S";
                                                            linha_resultado[0].EndEdit();

                                                            txtVDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtVDesc.Text.Trim()));
                                                            txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                                                            txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"]);
                                                        txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"]);
                                                        txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                    }
                                                }
                                                else if (porcentagem < Convert.ToDecimal(linha_resultado[0]["DESC_MIN"]))
                                                {
                                                    txtPDesc.Text = String.Format("{0:N}", Convert.ToDecimal(linha_resultado[0]["DESC_MIN"]));
                                                    txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                                                    txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                }
                                                else
                                                {
                                                    txtVDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtVDesc.Text.Trim()));
                                                    txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                                                    txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                                }
                                            }
                                            else
                                            {
                                                txtVDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtVDesc.Text.Trim()));
                                                txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                                                txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            txtVDesc.Text = String.Format("{0:N}", Convert.ToDecimal(txtVDesc.Text.Trim()));
                                            txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                                            txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                                        }
                                    }
                                    else
                                    {
                                        txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"]);
                                        txtVDesc.Focus();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Desconto acima do percentual permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"]);
                                    txtVDesc.Focus();

                                }
                            }
                            else
                            {
                                MessageBox.Show("Desconto não pode ser incluído neste produto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"]);
                                txtVDesc.Focus();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.PerformClick();
        }

        private void txtVTotal_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtVTotal.ReadOnly.Equals(false))
                {
                    if (!String.IsNullOrEmpty(txtVTotal.Text))
                    {
                        txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToInt32(txtPQtde.Text.Trim())) - Convert.ToDecimal(txtVDesc.Text.Trim()), 2));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                exclusaoComanda = true;
                if (Funcoes.LeParametro(6, "362", true).Equals("S") && vendaDeComanda && !String.IsNullOrEmpty(txtComanda.Text) && exclusaoComanda.Equals(true))
                {
                    if (MessageBox.Show("Exclusão não permitida! Deseja solicitar liberação?", "Liberação de Desconto", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        frmVenSupSenha senha = new frmVenSupSenha(0);
                        senha.ShowDialog();
                        if (senha.sValida.Equals(false))
                        {
                            exclusaoComanda = false;
                        }
                    }
                    else
                        exclusaoComanda = false;
                }
                if (exclusaoComanda)
                {
                    linha_resultado = tProdutos.Select("COD_BARRA = '" + lblNumCodBarras.Text + "'");
                    linha_resultado[0].Delete();
                    InserirProdCupom(tProdutos);
                    limparProd();
                    IsentaTaxaEntrega();
                    CamposProdutoInvisivel();

                    object sumObject;
                    sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                    lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                    txtProduto.Focus();
                }
                else
                {
                    limparProd();
                    IsentaTaxaEntrega();
                    CamposProdutoInvisivel();

                    object sumObject;
                    sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                    lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                    if (String.IsNullOrEmpty(sumObject.ToString()))
                    {
                        txtGAjuste.Text = "0,00";
                    }
                    txtProduto.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //LIMPA INFORMAÇÕES INDIVIDUAIS DO PRODUTO SELECIONADO//
        public void limparProd()
        {
            #region LIMPA OS CAMPOS
            txtVUni.Text = "0,00";
            txtPQtde.Text = "0";
            txtSTotal.Text = "0,00";
            txtPDesc.Text = "0,00";
            txtVDesc.Text = "0,00";
            txtVTotal.Text = "0,00";
            txtEstoque.Text = String.Empty;
            txtComissao.Text = String.Empty;
            txtCusto.Text = String.Empty;
            txtComissao.Visible = false;
            lblComissao.Visible = false;
            lblNumCodBarras.Text = String.Empty;
            lblNome.Text = String.Empty;
            btnOk.Enabled = false;
            btnCancela.Enabled = false;
            btnExcluir.Enabled = false;
            lblCusto.Visible = false;
            txtCusto.Visible = false;
            txtVUni.ReadOnly = true;
            txtPQtde.ReadOnly = true;
            txtSTotal.ReadOnly = true;
            txtPDesc.ReadOnly = true;
            txtVDesc.ReadOnly = true;
            txtVTotal.ReadOnly = true;
            tlpDadosProdutos.Visible = false;
            txtProduto.Focus();
            #endregion
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            object sumObject;
            sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

            lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

            limparProd();
            CamposProdutoInvisivel();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                linha_resultado = tProdutos.Select("COD_BARRA = '" + lblNumCodBarras.Text + "'");
                linha_resultado[0].BeginEdit();
                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", txtVUni.Text);
                linha_resultado[0]["PROD_QTDE"] = txtPQtde.Text;
                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", txtSTotal.Text);
                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", txtPDesc.Text);
                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", txtVDesc.Text);
                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", txtVTotal.Text);
                linha_resultado[0].EndEdit();

                InserirProdCupom(tProdutos);
                limparProd();
                IsentaTaxaEntrega();
                CamposProdutoInvisivel();

                object sumObject;
                sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                txtProduto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtGAjuste_TextChanged(object sender, EventArgs e)
        {
            if (txtGAjuste.Text.Trim().Length > 0)
            {
                if (txtGAjuste.Text.Substring(0, 1).Equals("-"))
                {
                    txtGAjuste.ForeColor = System.Drawing.Color.Red;
                }
                else
                    txtGAjuste.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void txtGAjuste_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                valida = false;
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtGAjuste.Text))
                    {
                        if (Funcoes.LeParametro(6, "205", true).Equals("S"))
                        {
                            txtGAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtGAjuste.Text));
                            if (Convert.ToDecimal(txtGAjuste.Text) > Convert.ToDecimal(Funcoes.LeParametro(6, "206", true)) ||
                                Convert.ToDecimal(txtGAjuste.Text) < Convert.ToDecimal(Funcoes.LeParametro(6, "206", true)) * -1)
                            {
                                MessageBox.Show("Valor de ajuste acima/abaixo do valor máximo permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                txtGAjuste.Text = "0,00";
                                lblTotal.Text = String.Format("{0:N}", String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text))));
                                txtGAjuste.Focus();
                            }
                            else
                            {
                                if (Convert.ToDecimal(txtGAjuste.Text) > 0)
                                {
                                    valida = true;
                                    lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)) + Convert.ToDecimal(txtGAjuste.Text));
                                    txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                    txtProduto.Focus();
                                }
                                else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                                {
                                    valida = true;
                                    lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                                    txtGAjuste.ForeColor = System.Drawing.Color.Red;
                                    txtProduto.Focus();
                                }
                                else
                                {
                                    valida = true;
                                    lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)));
                                    txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                    txtProduto.Focus();
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToDecimal(txtGAjuste.Text) > 0)
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)) + Convert.ToDecimal(txtGAjuste.Text));
                                txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                txtProduto.Focus();
                            }
                            else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                                txtGAjuste.ForeColor = System.Drawing.Color.Red;
                                txtProduto.Focus();
                            }
                            else
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)));
                                txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                txtProduto.Focus();
                            }
                        }

                        sTotal = Convert.ToDouble(lblTotal.Text);
                    }
                    else
                        txtGAjuste.Text = "0,00";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtGAjuste_Validated(object sender, EventArgs e)
        {
            try
            {
                if (valida.Equals(false))
                {
                    if (Funcoes.LeParametro(6, "205", true).Equals("S"))
                    {
                        txtGAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtGAjuste.Text));
                        if (Convert.ToDecimal(txtGAjuste.Text) > Convert.ToDecimal(Funcoes.LeParametro(6, "206", true)) ||
                            Convert.ToDecimal(txtGAjuste.Text) < Convert.ToDecimal(Funcoes.LeParametro(6, "206", true)) * -1)
                        {
                            MessageBox.Show("Valor de ajuste acima/abaixo do valor máximo permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtGAjuste.Text = "0,00";
                            lblTotal.Text = String.Format("{0:N}", String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text))));
                            txtGAjuste.Focus();
                        }
                        else
                        {
                            if (Convert.ToDecimal(txtGAjuste.Text) > 0)
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)) + Convert.ToDecimal(txtGAjuste.Text));
                                txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                txtProduto.Focus();
                            }
                            else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                                txtGAjuste.ForeColor = System.Drawing.Color.Red;
                                txtProduto.Focus();
                            }
                            else
                            {
                                valida = true;
                                lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)));
                                txtGAjuste.ForeColor = System.Drawing.Color.Black;
                                txtProduto.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToDecimal(txtGAjuste.Text) > 0)
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)) + Convert.ToDecimal(txtGAjuste.Text));
                            txtGAjuste.ForeColor = System.Drawing.Color.Black;
                            txtProduto.Focus();
                        }
                        else if (Convert.ToDecimal(txtGAjuste.Text) < 0)
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Math.Abs(Convert.ToDecimal(lblDesconto.Text))) - Math.Abs(Convert.ToDecimal(txtGAjuste.Text)));
                            txtGAjuste.ForeColor = System.Drawing.Color.Red;
                            txtProduto.Focus();
                        }
                        else
                        {
                            valida = true;
                            lblTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(lblSubTotal.Text) - Convert.ToDecimal(lblDesconto.Text)));
                            txtGAjuste.ForeColor = System.Drawing.Color.Black;
                            txtProduto.Focus();
                        }
                    }

                    sTotal = Convert.ToDouble(lblTotal.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAtalhos, "F8-Informações de Atalho");
        }

        private void btnFVenda_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFVenda, "INSERT-Finalizar Venda");
        }

        private async void btnFVenda_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCampos())
                {
                    if (Inserir())
                    {
                        if (permtVenda)
                        {
                            if (Funcoes.LeParametro(6, "376", true).Equals("S"))
                            {
                                if (vendaBeneficio.Equals("VIDALINK") || vendaBeneficio.Equals("EPHARMA") || vendaBeneficio.Equals("FARMACIAPOPULAR") || vendaBeneficio.Equals("PORTALDROGARIA")
                                    || vendaBeneficio.Equals("ORIZON"))
                                {
                                    Principal.tipoCupom = "C";
                                }
                            }

                            switch (Principal.tipoCupom)
                            {
                                case "P":
                                    //IMPRESSÃO DE COMPROVANTE//
                                    if (!ComprovanteOrcamento())
                                    {
                                        LimparErro();
                                        BancoDados.ErroTrans();
                                        return;
                                    }
                                    break;
                                case "C":
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
                                    {
                                        if (!ImprimirCupomFiscal())
                                        {
                                            LimparErro();
                                            Imprimir.ImpFiscalUltCupom();
                                            BancoDados.ErroTrans();
                                            return;
                                        }
                                    }
                                    else if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    {
                                        if (!SATCupomFiscal())
                                        {
                                            LimparErro();
                                            BancoDados.ErroTrans();
                                            return;
                                        }
                                    }
                                    break;
                            }
                        }
                        else if (chkEntrega.Checked)
                        {
                            if (!ComprovanteOrcamento())
                            {
                                LimparErro();
                                BancoDados.ErroTrans();
                                return;
                            }
                        }

                        #region IMPRESSAO BENEFICIOS

                        int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(vendaPedFormaPagto[0].QtdeVias);

                        if (vendaBeneficio.Equals("VIDALINK") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO VIDALINK
                            var buscaComanda = new BeneficioVidaLinkComanda();
                            dtRetorno = buscaComanda.IdentificaSeVendaEPorComanda(vendaNumeroNSU);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                buscaComanda.ExcluirComandaPorNSU(vendaNumeroNSU);
                            }

                            dadosDaVenda.VendaAutorizacao = vendaNumeroNSU;

                            frmVidaLinkFinalizaVenda vendaVidaLink = new frmVidaLinkFinalizaVenda(this);
                            vendaVidaLink.ShowDialog();

                            if (!String.IsNullOrEmpty(cupomBeneficio))
                            {
                                ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                                Cursor = Cursors.WaitCursor;

                                if (!ComprovantesVinculados.CupomVinculadoBeneficios(cupomBeneficio, "", vendaNumeroNota, qtdeVias))
                                {
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    Limpar();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            else
                            {
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));
                                else
                                    Imprimir.ImpFiscalUltCupom();

                                Limpar();
                                BancoDados.ErroTrans();
                                return;
                            }
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("EPHARMA") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO EPHARMA
                            dadosDaVenda.VendaAutorizacao = vendaNumeroNSU;
                            numCFE = numeroCFe;
                            vendaAutor = vendaNumeroNSU;

                            frmFinalizaVenda venda = new frmFinalizaVenda(this);
                            venda.ShowDialog();

                            if (!String.IsNullOrEmpty(cupomBeneficio))
                            {
                                ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                                Cursor = Cursors.WaitCursor;

                                if (!ComprovantesVinculados.CupomVinculadoBeneficios(cupomBeneficio, "", vendaNumeroNota, qtdeVias))
                                {
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            else
                            {
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));
                                else
                                    Imprimir.ImpFiscalUltCupom();

                                Limpar();
                                BancoDados.ErroTrans();
                                return;
                            }
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("ORIZON") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO ORIZON
                            dadosDaVenda.VendaAutorizacao = vendaNumeroNSU;
                            numCFE = vendaNumeroNota;
                            vendaAutor = vendaNumeroNSU;

                            frmFinalizaVendaOrizon venda = new frmFinalizaVendaOrizon(this);
                            venda.ShowDialog();

                            if (!String.IsNullOrEmpty(cupomBeneficio))
                            {
                                ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                                Cursor = Cursors.WaitCursor;
                                OrizonBeneficio orizon = new OrizonBeneficio();
                                if (!ComprovantesVinculados.CupomVinculadoBeneficios(cupomBeneficio, "", vendaNumeroNota, qtdeVias))
                                {
                                    orizon.ConfirmaVenda(sequencia, "ER");

                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                                else
                                {
                                    orizon.ConfirmaVenda(sequencia, "OK");
                                }
                            }
                            else
                            {
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));
                                else
                                    Imprimir.ImpFiscalUltCupom();

                                Limpar();
                                BancoDados.ErroTrans();
                                return;
                            }
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("PORTALDROGARIA") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO PORTAL DA DROGARIA
                            dadosDaVenda.VendaAutorizacao = vendaNumeroNSU;

                            frmPortalDrogariaFinalizaVenda finalizaPortalDrogaria = new frmPortalDrogariaFinalizaVenda(this);
                            finalizaPortalDrogaria.ShowDialog();

                            if (!String.IsNullOrEmpty(cupomBeneficio))
                            {
                                ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                                Cursor = Cursors.WaitCursor;

                                if (!ComprovantesVinculados.CupomVinculadoBeneficios(cupomBeneficio, vendaBeneficio, vendaNumeroNota, qtdeVias))
                                {
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            else
                            {
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));
                                else
                                    Imprimir.ImpFiscalUltCupom();

                                Limpar();
                                BancoDados.ErroTrans();
                                return;
                            }
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("FARMACIAPOPULAR") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO FARMACIA POPULAR
                            dadosDaVenda.VendaAutorizacao = Funcoes.RemoveCaracter(vendaNumeroNSU);

                            var cupom = new FpSolicitacao();
                            cupomBeneficio = cupom.BuscaCupomPorNsu(dadosDaVenda.VendaAutorizacao);

                            if (!String.IsNullOrEmpty(cupomBeneficio))
                            {
                                ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                                Cursor = Cursors.WaitCursor;

                                if (!ComprovantesVinculados.CupomVinculadoBeneficios(cupomBeneficio, vendaBeneficio, vendaNumeroNota, qtdeVias))
                                {
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            else
                            {
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));
                                else
                                    Imprimir.ImpFiscalUltCupom();

                                LimparErro();
                                BancoDados.ErroTrans();
                                return;
                            }
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("FUNCIONAL") && !venOrcamento && Principal.tipoCupom.Equals("C"))
                        {
                            #region IMPRESSAO FUNCIONAL
                            dadosDaVenda.VendaAutorizacao = objConfirmaVendaResponse.NSU;
                            numCFE = vendaNumeroNota;
                            vendaAutor = objConfirmaVendaResponse.NSU;

                            ExibirMensagem("Imprimindo Relatório Gerencial. Aguarde...");
                            Cursor = Cursors.WaitCursor;

                            string produtosComprov = "";
                            for (int x = 0; x < tProdutos.Rows.Count; x++)
                            {
                                double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                                produtosComprov += tProdutos.Rows[x]["COD_BARRA"] + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"]) > 0)
                                {
                                    produtosComprov += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[x]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[x]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                                }
                                produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }

                            if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                if (!ComprovantesVinculados.CupomVinculadoFuncionalSAT(objTransacaoClienteCartaoFuncional, objConfirmaVendaResponse, objPreAutorizacaoResponse, vendaAutorizadaResponse, vendaNumeroNota, pagamentoTotalOuParcial, diferencaDoValorAutorizado, dtDadosComandaFuncional, produtosComprov, qtdeVias))
                                {
                                    if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                        CancelaCupomSat(numeroCFe, Convert.ToDouble(lblTotal.Text));

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            else if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
                            {
                                if (!ComprovantesVinculados.CupomVinculadoFuncionalFiscal(objTransacaoClienteCartaoFuncional, objConfirmaVendaResponse, objPreAutorizacaoResponse, vendaAutorizadaResponse, vendaNumeroNota, pagamentoTotalOuParcial, diferencaDoValorAutorizado, dtDadosComandaFuncional, produtosComprov, qtdeVias))
                                {
                                    Imprimir.ImpFiscalUltCupom();

                                    LimparErro();
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                            #endregion
                        }
                        #endregion

                        if (Funcoes.LeParametro(6, "97", false).Equals("S") && chkEntrega.Checked && imprimeComprovante)
                        {
                            ProtocoloEntrega();
                        }
                        else if (Funcoes.LeParametro(6, "97", false).Equals("S") && chkEntrega.Checked && Funcoes.LeParametro(6, "393", false).Equals("S"))
                        {
                            ProtocoloEntrega();
                        }

                        BancoDados.FecharTrans();

                        //VERIFICACAO TABELA VENDAS//
                        if (dadosDaVenda.BuscaVendaPorVendaId(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID).Rows.Count == 0)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\ErroVendas.txt", true))
                            {
                                writer.WriteLine(dadosDaVenda.VendaID);
                            }

                            BancoDados.AbrirTrans();
                            if (dadosDaVenda.InserirDadosPreVenda(dadosDaVenda, true))
                            {
                                BancoDados.FecharTrans();
                            }
                        }

                        if (!venOrcamento)
                        {
                            dadosDaVenda.AtualizarMovimentoFinanceiro(movtoFinanceiroSeq, dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID);

                            var upEspecie = new VendasEspecies();
                            upEspecie.AtualizaOperador(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID);

                            var upForma = new VendasFormaPagamento();
                            upForma.AtualizaOperador(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID);
                        }

                        #region FINALIZACAO BENEFICIOS
                        if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                        {
                            dadosDaVenda.AtualizaAutorizacaoDrogabellaPlantao(dadosDaVenda.VendaCartao, dadosDaVenda.VendaNomeCartao, dadosDaVenda.VendaConCodigo, dadosDaVenda.VendaCartaoCodigo,
                                dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, dadosDaVenda.VendaTransID, dadosDaVenda.VendaTransIDReceita,
                                dadosDaVenda.VendaAutorizacao, dadosDaVenda.VendaAutorizacaoReceita, dadosDaVenda.VendaConvenioParcela);

                            if (!venOrcamento)
                            {
                                var deletaComanda = new DrogabellaComanda();
                                deletaComanda.ExcluirDadosPorVendaId(dadosDaVenda.VendaID);
                            }
                        }
                        else if (vendaBeneficio.Equals("FUNCIONAL"))
                        {
                            if (objTransacaoClienteCartaoFuncional.Trilha2DoCartao != null)
                            {
                                dadosDaVenda.AtualizaAutorizacaoFuncional(objTransacaoClienteCartaoFuncional.Trilha2DoCartao, objTransacaoClienteCartaoFuncional.NomeDoPortador, "7000", dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, objConfirmaVendaResponse.NSU, objConfirmaVendaResponse.NSUDoAutorizador, Principal.crm);
                            }

                            if (!venOrcamento)
                            {
                                var deletaComanda = new FuncionalComanda();
                                deletaComanda.ExcluirDadosPorNSU(vendaNumeroNSU);
                            }
                        }
                        else if (vendaBeneficio.Equals("VIDALINK") && !venOrcamento)
                        {
                            #region FINALIZACAO VIDALINK
                            var atualizaVendaVidaLink = new BeneficioVidaLink();
                            atualizaVendaVidaLink.AtualizaVendaIdPorNsu(dadosDaVenda.VendaAutorizacao, vendaID);

                            atualizaVendaVidaLink.AtualizaStatusPorNsu(dadosDaVenda.VendaAutorizacao, "C");

                            var atualizaDados = new BeneficioVidaLinkProdutos();
                            atualizaDados.AtualizaVendaIdPorNsu(dadosDaVenda.VendaAutorizacao, vendaID);

                            var deletaComanda = new BeneficioVidaLinkComanda();
                            deletaComanda.ExcluirDadosPorNSU(dadosDaVenda.VendaAutorizacao, vendaID);

                            dadosDaVenda.AtualizaAutorizacaoBeneficio(dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, dadosDaVenda.VendaAutorizacao);
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("PORTALDROGARIA") && !venOrcamento)
                        {
                            Negocio.Beneficios.PortalDaDrogaria vendaPortalDaDrogaria = new Negocio.Beneficios.PortalDaDrogaria();
                            vendaPortalDaDrogaria.EnviaConfirmacaoDeTransacao(vendaNumeroNSU);

                            var atualizaVenda = new BeneficioNovartisEfetivado();
                            atualizaVenda.AtualizaDadosLoteECod(vendaNumeroNSU, lotePortalDrogaria, codAutorizacaoPortalDrogaria);

                            var atualizaProdutos = new BeneficioNovartisProdutos();
                            atualizaProdutos.AtualizaStatusCartaoPorNsu(vendaNumeroNSU, "F", wsNumCartao, vendaID);

                            atualizaVenda.AtualizaDadosNsu(vendaNumeroNSU, "F", wsNumCartao, vendaID);

                            var excluiPendente = new BeneficioNovartisPendente();
                            excluiPendente.ExcluirDadosPorNSU(vendaNumeroNSU);

                            var excluiComanda = new BeneficioNovartisComanda();
                            excluiComanda.ExcluirDadosPorNSU(vendaNumeroNSU);

                            dadosDaVenda.AtualizaAutorizacaoBeneficio(dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, dadosDaVenda.VendaAutorizacao);
                        }
                        else if (vendaBeneficio.Equals("EPHARMA") && !venOrcamento)
                        {
                            dadosDaVenda.AtualizaAutorizacaoBeneficio(dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, dadosDaVenda.VendaAutorizacao);
                            EpharmaComanda comandaEpharma = new EpharmaComanda();
                            comandaEpharma.ExcluirDadosPorNSU(dadosDaVenda.VendaAutorizacao, vendaID);
                        }
                        else if (vendaBeneficio.Equals("FARMACIAPOPULAR") && !venOrcamento)
                        {
                            dadosDaVenda.AtualizaAutorizacaoBeneficio(dadosDaVenda.VendaID, dadosDaVenda.EmpCodigo, dadosDaVenda.EstCodigo, dadosDaVenda.VendaAutorizacao);

                            var vendaFP = new FpSolicitacao();
                            vendaFP.AtualizaVendaIdPorNsu(dadosDaVenda.VendaAutorizacao, vendaID);

                            var comandaFP = new FpComanda();
                            comandaFP.ExcluirDadosPorNSU(dadosDaVenda.VendaAutorizacao);
                        }
                        #endregion

                        if (Principal.tipoCupom.Equals("C") && !venOrcamento)
                        {
                            #region GRAVA INFORMAÇÕES SOBRE O CUPOM
                            dadosDaVenda.AtualizarInformacoesFiscais(Principal.estAtual, Principal.empAtual, dadosDaVenda.VendaID, vendaNumeroNota);

                            if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                string emitido = "S";
                                Principal.NumeroSessao = "1";

                                var dadosFiscais = new CuponsSAT
                                (
                                    dadosDaVenda.EmpCodigo,
                                    dadosDaVenda.EstCodigo,
                                    dadosDaVenda.VendaID,
                                    vendaNumeroNota,
                                    numeroCFe,
                                    dadosDaVenda.VendaTotal,
                                    emitido,
                                    "N",
                                    Principal.NumeroSessao,
                                    DateTime.Now,
                                    Principal.usuario,
                                    DateTime.Now,
                                    Principal.usuario
                                );

                                dadosFiscais.InserirDados(dadosFiscais);
                            }
                            #endregion
                        }

                        if (Util.TesteIPServidorNuvem())
                        {
                            if (Funcoes.LeParametro(9, "51", false).Equals("S") && !venOrcamento)
                            {
                                await DecrementaEstoqueDeFiliais();
                            }
                        }
                        Limpar();
                    }
                    else
                    {
                        LimparErro();
                        if (transAberta)
                        {
                            BancoDados.ErroTrans();
                            if (codFormaPagto.ToString() == Funcoes.LeParametro(6, "116", true))
                            {
                                Limpar();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                OcultarMensagem();
                Cursor = Cursors.Default;
            }

        }

        public void CancelaCupomSat(string chave, double total)
        {
            FileStream fs;
            StreamWriter sw;
            string mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
            string numeroCupom, arquivoSATResposta;
            string xmlSAT, retorno;
            var updateCupom = new CuponsSAT();

            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
            {
                xmlSAT = Sat.MontaXmlCancelamento(chave);
                if (!String.IsNullOrEmpty(xmlSAT))
                {
                    retorno = Sat.SatCancelarUltimaVenda(chave, xmlSAT);

                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCupom))
                    {
                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesComErro\" + mes.ToUpper() + @"\" + chave + ".xml", FileMode.Create);
                        sw = new StreamWriter(fs);
                        sw.WriteLine(xmlSAT);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                    }

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesCanEnviados\" + mes.ToUpper() + @"\" + chave + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    //CONVERTE BASE64 PARA STRING//
                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    if (Sat.SatImpressaoComprovanteCancelamento(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml",
                        numeroCupom, total, out vendaNumeroNota))
                    {
                        updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, numeroCupom, DateTime.Now, Principal.usuario, numeroCupom);
                        MessageBox.Show("Cupom Cancelado!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                int iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                if (iRetorno != 1)
                {
                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, chave, DateTime.Now, Principal.usuario, "");
                    MessageBox.Show("Cupom Cancelado!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public void LimparErro()
        {
            //vendaID = 0;
            //inserir = true;
            vendaPedFormaPagto.Clear();
            venItens.Clear();
            vendaItens.Clear();
            Cursor = Cursors.Default;
        }

        public async Task DecrementaEstoqueDeFiliais()
        {
            try
            {

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                        + "&codBarras=" + tProdutos.Rows[i]["COD_BARRA"].ToString()
                                        + "&qtd=" + Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]) + "&operacao=menos");
                        if (!response.IsSuccessStatusCode)
                        {
                            Console.WriteLine("06206");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public bool Inserir()
        {
            try
            {
                #region VALIDACAO PORTALDROGARIA
                if (vendaBeneficio.Equals("PORTALDROGARIA") && !vendaDeComanda && !String.IsNullOrEmpty(txtComanda.Text))
                {
                    if (!ValidaProdutosPortalDaDrogaria())
                    {
                        Limpar();
                        return false;
                    }
                }
                else if (vendaBeneficio.Equals("PORTALDROGARIA") && String.IsNullOrEmpty(txtComanda.Text) && StatusCaixa())
                {
                    if (!ValidaProdutosPortalDaDrogaria())
                    {
                        Limpar();
                        return false;
                    }
                }
                #endregion

                #region VALIDACAO PARTICULAR
                if (wsConWeb.Equals("4") && vendaBeneficio.Equals("PARTICULAR"))
                {
                    retorno = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_FORMA_ID", "CON_CODIGO", txtCodCliEmp.Text);

                    if (!String.IsNullOrEmpty(retorno))
                        codFormaPagto = Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_FORMA_ID", "CON_CODIGO", txtCodCliEmp.Text));

                    //if (Funcoes.LeParametro(6, "126", true).Equals("S"))
                    //{
                    //    if (!ChecaLimiteParticular())
                    //    {
                    //        return false;
                    //    }
                    //}
                }
                #endregion

                #region VALIDAÇÃO FUNCIONAL 
                if (vendaBeneficio.Equals("FUNCIONAL") && !preAutorizouFuncional)
                {
                    MessageBox.Show("A Venda para este convênio deve ser Pré-Autorizada Primeiro!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnPreAutorizar.Focus();
                    return false;
                }
                #endregion

                #region VERIFICA SE VENDA FOI PRE-AUTORIZADA PARA EMPRESAS COM REGRAS DE CONVÊNIO
                if ((wsPreAutoriza == "S") && (preAutorizou == false))
                {
                    MessageBox.Show("A Venda para este convênio deve ser Pré-Autorizada Primeiro!", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnPreAutorizar.Focus();
                    return false;
                }
                //SE VENDA FOI PRE-AUTORIZADA VERIFICA SE DADOS ATUAIS ESTAO IGUAIS AO DA AUTORIZACAO//
                if ((wsPreAutoriza == "S") && (preAutorizou == true))
                {
                    if ((tProdutos.Compute("SUM(PROD_QTDE)", String.Empty).ToString()) != Convert.ToString(wsQtdeProdutosRetorno))
                    {
                        MessageBox.Show("A quantidade de produtos na tela está diferente da última pré-autorização feita.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnPreAutorizar.Focus();
                        return false;
                    }
                    else
                    {
                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            if (tProdutos.Rows[i]["PROD_ID"].ToString() != prodAut[i, 0])
                            {
                                preAutorizou = false;
                            }
                            if (tProdutos.Rows[i]["PROD_QTDE"].ToString() != prodAut[i, 5])
                            {
                                preAutorizou = false;
                            }
                            if ((tProdutos.Rows[i]["PROD_TOTAL"].ToString()) != prodAut[i, 9])
                            {
                                preAutorizou = false;
                            }
                        }
                        if (preAutorizou.Equals(false))
                        {
                            MessageBox.Show("As informações atuais estão diferentes da última pré-autorização feita. Realize a pré-autorização novamente!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnPreAutorizar.Focus();
                            return false;
                        }
                    }
                }
                #endregion

                #region VENDA COM RECEITA
                if (tipoVenda.Equals(1) || tipoVenda.Equals(3))
                {
                    frmVenReceita venReceita = new frmVenReceita(this);
                    venReceita.ShowDialog();

                    if (String.IsNullOrEmpty(wsCRM))
                    {
                        MessageBox.Show("Dados do Médico não pode ser em branco!", "Receita Médica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                #endregion

                if (permtVenda)
                {
                    if (String.IsNullOrEmpty(txtComanda.Text))
                    {
                        dtPedido = DateTime.Now;
                        venOrcamento = false;
                    }
                }

                #region VALIDACAO DE CAIXA
                var caixa = new AFCaixa();
                List<AFCaixa> cxStatus = caixa.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);
                movtoFinanceiroSeq = 0;
                if (!venOrcamento)
                {
                    if (cxStatus.Count.Equals(0))
                    {
                        MessageBox.Show("Lançamento com movimentação de caixa com caixa que ainda não foi aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    if (wsConWeb != "4")
                    {
                        movtoFinanceiroSeq = Convert.ToInt64(Funcoes.IdentificaVerificaID("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_SEQ", Principal.estAtual, " MOVIMENTO_FINANCEIRO_DATA = " + Funcoes.BData(dtPedido) + " AND MOVIMENTO_FINANCEIRO_USUARIO = '" + Principal.usuario + "'", Principal.empAtual));
                    }
                }
                #endregion

                #region VALIDACOES DROGABELLA/PLANTAO
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && !vendaDeComanda)
                {
                    var conveniada = new Conveniada();
                    if (conveniada.IdentificaParcelado(txtCodCliEmp.Text).Equals("S"))
                    {
                        string parcela = Interaction.InputBox("Digite Número de Parcelas", "Parcelas");
                        dadosDaVenda.VendaConvenioParcela = parcela == "" ? 1 : Convert.ToInt32(parcela);
                    }
                    else
                    {
                        dadosDaVenda.VendaConvenioParcela = 1;
                    }

                    if (Convert.ToDouble(lblTotal.Text) > wsSaldoCartao && wsPreAutoriza == "N" && tipoVenda != 1 && dadosDaVenda.VendaConvenioParcela == 1)
                    {
                        if (MessageBox.Show("Valor da Venda maior que o Limite Disponivel!\nTotal da Venda: R$ " + String.Format("{0:N}", Convert.ToDouble(lblTotal.Text)) + "\nLimite Disponível: R$" + String.Format("{0:N}", wsSaldoCartao)
                            + "\nDiferença: R$" + String.Format("{0:N}", Convert.ToDouble(lblTotal.Text) - wsSaldoCartao) + "\nDeseja continuar para pagar a diferença?", "Vendas", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            diferencaDoValorAutorizado = Convert.ToDouble(lblTotal.Text) - wsSaldoCartao;
                        }
                        else
                            return false;
                    }
                    else if (Convert.ToDouble(lblTotal.Text) > wsSaldoCartao && wsPreAutoriza == "S")
                    {
                        MessageBox.Show("Valor da Venda maior que o Limite Disponivel!\nTotal da Venda: R$ " + String.Format("{0:N}", Convert.ToDouble(lblTotal.Text)) + "\nLimite Disponível: R$" + String.Format("{0:N}", wsSaldoCartao), "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
                #endregion

                #region VENDA CONTROLADO
                if (Funcoes.LeParametro(6, "127", true).Equals("S") && vendaControlado)
                {
                    if (!VendaControlado())
                    {
                        return false;
                    }
                }
                #endregion

                #region VERIFICA SE VENDA E TEM ENTREGA
                if (chkEntrega.Checked.Equals(true))
                {
                    frmVenEntrega entrega = new frmVenEntrega(this);
                    if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && diferencaDoValorAutorizado != 0)
                    {
                        entrega.valorVenda = diferencaDoValorAutorizado;
                    }
                    else
                        entrega.valorVenda = Convert.ToDouble(lblTotal.Text);

                    entrega.ShowDialog();

                    if (!vEntr_Canc)
                    {
                        if (vendaBeneficio.Equals("N") && chkEntrega.Checked && !Principal.tipoCupom.Equals("C"))
                        {
                            codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(6, "356", true));
                            venOrcamento = true;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                #endregion

                #region DROGABELLA/PLANTAO
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && !vendaDeComanda)
                {
                    if (Convert.ToDouble(lblTotal.Text) > wsSaldoCartao && wsPreAutoriza == "N" && tipoVenda != 1 && dadosDaVenda.VendaConvenioParcela == 1)
                    {
                        if (diferencaDoValorAutorizado != 0)
                        {
                            diferencaDoValorAutorizado = Convert.ToDouble(lblTotal.Text) - wsSaldoCartao;
                            valorAutorizado = wsSaldoCartao;
                            double saldoAtualizado = wsSaldoCartao;

                            for (int i = 0; i < tProdutos.Rows.Count; i++)
                            {
                                var dadosProdutosAlterados = new ProdutosAlterados();
                                if (saldoAtualizado > 0 && Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"]) < saldoAtualizado)
                                {
                                    dadosProdutosAlterados.CodBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();
                                    dadosProdutosAlterados.Descricao = tProdutos.Rows[i]["PROD_DESCR"].ToString();
                                    dadosProdutosAlterados.Qtde = Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]);
                                    dadosProdutosAlterados.ProdUnitario = Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]);
                                    dadosProdutosAlterados.DescontoValor = Math.Round(Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]), 2);
                                    dadosProdutosAlterados.ProdTotal = Math.Round(Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"]), 2);
                                    produtosAlterados.Add(dadosProdutosAlterados);
                                }
                                else
                                {
                                    dadosProdutosAlterados.CodBarras = tProdutos.Rows[i]["COD_BARRA"].ToString();
                                    dadosProdutosAlterados.Descricao = tProdutos.Rows[i]["PROD_DESCR"].ToString();
                                    dadosProdutosAlterados.Qtde = Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]);
                                    dadosProdutosAlterados.ProdUnitario = Math.Round(saldoAtualizado / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2);
                                    dadosProdutosAlterados.DescontoValor = 0;
                                    dadosProdutosAlterados.ProdTotal = Math.Round(saldoAtualizado, 2);
                                    produtosAlterados.Add(dadosProdutosAlterados);
                                    saldoAtualizado = 0;
                                    break;
                                }

                                saldoAtualizado = Math.Round(saldoAtualizado - Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"]), 2);
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        movtoFinanceiroSeq = 0;
                        valorAutorizado = Convert.ToDouble(lblTotal.Text);

                        if (dadosDaVenda.VendaConvenioParcela != 1)
                        {
                            double valorParcela = valorAutorizado / dadosDaVenda.VendaConvenioParcela;

                            if (valorParcela > wsSaldoCartao)
                            {
                                MessageBox.Show("Saldo Insuficiente para Compra Parcela", "Convênio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }
                }
                #endregion

                #region FORMA DE PAGAMENTO
                frmVenFormaPagto venFormaPagto = new frmVenFormaPagto(this, cxStatus.Count == 0 ? "N" : cxStatus[0].CaixaAberto);
                venFormaPagto.dtFormas = venFormaPagto.tabelaFormas();
                if (Funcoes.LeParametro(6, "390", false).Equals("S"))
                {
                    if (!vendaBeneficio.Equals("N"))
                    {
                        double total = 0;
                        int parcelas = 0;
                        double valor = 0;

                        if (vendaBeneficio.Equals("PARTICULAR"))
                        {
                            #region PARTICULAR
                            var buscaFormas = new FormasPagamento();

                            dtRetorno = buscaFormas.DadosDaFormaDePagamento(Principal.empAtual, codFormaPagto);
                            parcelas = Convert.ToInt32(dtRetorno.Rows[0]["QTDE_PARCELAS"]);
                            DateTime vencimento = DateTime.Now;
                            valor = Math.Round(Convert.ToDouble(lblTotal.Text) / parcelas, 2);

                            for (int i = 1; i <= parcelas; i++)
                            {
                                if (dtRetorno.Rows[0]["VENCTO_DIA_FIXO"].ToString().Equals("S"))
                                {
                                    if (DateTime.Now.Month == 12)
                                    {
                                        vencimento = new DateTime(DateTime.Now.Year + 1, i, Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                    }
                                    else
                                    {
                                        vencimento = new DateTime(DateTime.Now.Year, DateTime.Now.Month + i, Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                    }
                                }
                                else
                                {
                                    if (!Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]).Equals(0))
                                    {
                                        vencimento = vencimento.AddDays(Convert.ToInt32(dtRetorno.Rows[0]["DIA_VENCTO_FIXO"]));
                                    }
                                }

                                if (i == parcelas && vendaBeneficio.Equals("PARTICULAR"))
                                {
                                    double diferenca = Math.Round(Convert.ToDouble(lblTotal.Text) - (total + valor), 2);
                                    if (diferenca != 0)
                                        valor = valor + diferenca;
                                }

                                DataRow row = venFormaPagto.dtFormas.NewRow();
                                row["FORMA_ID"] = codFormaPagto;
                                row["PARCELA"] = i;
                                row["VENC"] = vencimento;
                                row["TOTAL"] = valor;
                                row["COND"] = dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO";

                                venFormaPagto.dtFormas.Rows.Add(row);

                                total = total + valor;
                            }

                            selForma = true;
                            #endregion
                        }
                        else if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && diferencaDoValorAutorizado == 0)
                        {
                            #region DROGABELLA/PLANTAO
                            DataRow row = venFormaPagto.dtFormas.NewRow();
                            row["FORMA_ID"] = codFormaPagto == 0 ? 1 : codFormaPagto;
                            row["PARCELA"] = 1;
                            row["VENC"] = DateTime.Now;
                            row["TOTAL"] = Convert.ToDouble(lblTotal.Text);
                            var buscaFormas = new FormasPagamento();
                            dtRetorno = buscaFormas.DadosDaFormaDePagamento(Principal.empAtual, codFormaPagto == 0 ? 1 : codFormaPagto);
                            row["COND"] = dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO";

                            venFormaPagto.dtFormas.Rows.Add(row);

                            selForma = true;
                            #endregion
                        }
                        else
                        {
                            venFormaPagto.ShowDialog();
                        }
                    }
                    else
                    {
                        DataRow row = venFormaPagto.dtFormas.NewRow();
                        row["FORMA_ID"] = codFormaPagto == 0 ? 1 : codFormaPagto;
                        row["PARCELA"] = 1;
                        row["VENC"] = DateTime.Now;
                        row["TOTAL"] = Convert.ToDouble(lblTotal.Text);
                        var buscaFormas = new FormasPagamento();
                        dtRetorno = buscaFormas.DadosDaFormaDePagamento(Principal.empAtual, codFormaPagto == 0 ? 1 : codFormaPagto);
                        row["COND"] = dtRetorno.Rows[0]["OPERACAO_CAIXA"].ToString() == "S" ? "RECEBIDO (CAIXA)" : "PRAZO";

                        venFormaPagto.dtFormas.Rows.Add(row);

                        selForma = true;
                    }
                }
                else
                {
                    venFormaPagto.ShowDialog();
                }

                if (!selForma)
                {
                    return false;
                }
                #endregion

                #region ESPECIES
                frmVenEspecie venEspecie = new frmVenEspecie(this);
                if (codFormaPagto != 9)
                {
                    if (cxStatus.Count > 0 && codFormaPagto != Convert.ToInt32(Funcoes.LeParametro(6, "356", true)))
                    {
                        if (selForma.Equals(true) && cxStatus[0].CaixaAberto.Equals("S"))
                        {
                            venEspecie.dtEspecies = venEspecie.tabelaEspecies();

                            if (Funcoes.LeParametro(6, "390", false).Equals("S"))
                            {
                                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && diferencaDoValorAutorizado == 0)
                                {
                                    var esp = new Especie();

                                    DataRow row = venEspecie.dtEspecies.NewRow();
                                    row["ESP_CODIGO"] = 4;
                                    row["ESP_DESCRICAO"] = "CONVENIO";
                                    row["TOTAL"] = lblTotal.Text;
                                    row["EXCLUI"] = "N";
                                    row["CONSIDERA_ESP"] = esp.ConsideraEspecie(4);
                                    row["ESP_VINCULADO"] = esp.IdentificaEspecieVinculado(4);
                                    row["ESP_SAT"] = esp.IdentificaEspecieSAT(4);
                                    row["ESP_ECF"] = esp.IdentificaEspecieEcf(4);

                                    venEspecie.dtEspecies.Rows.Add(row);

                                    selEspecie = true;
                                }
                                else if (vendaBeneficio.Equals("PARTICULAR"))
                                {
                                    var esp = new Especie();

                                    DataRow row = venEspecie.dtEspecies.NewRow();
                                    row["ESP_CODIGO"] = 5;
                                    row["ESP_DESCRICAO"] = "PARTICULAR";
                                    row["TOTAL"] = lblTotal.Text;
                                    row["EXCLUI"] = "N";
                                    row["CONSIDERA_ESP"] = esp.ConsideraEspecie(5);
                                    row["ESP_VINCULADO"] = esp.IdentificaEspecieVinculado(5);
                                    row["ESP_SAT"] = esp.IdentificaEspecieSAT(5);
                                    row["ESP_ECF"] = esp.IdentificaEspecieEcf(5);

                                    venEspecie.dtEspecies.Rows.Add(row);

                                    selEspecie = true;
                                }
                                else
                                {
                                    vendaEspecie.Clear();
                                    venEspecie.lblTotal.Text = lblTotal.Text;
                                    venEspecie.lblResta.Text = lblTotal.Text;
                                    venEspecie.ShowDialog();
                                }
                            }
                            else
                            {
                                vendaEspecie.Clear();
                                venEspecie.lblTotal.Text = lblTotal.Text;
                                venEspecie.lblResta.Text = lblTotal.Text;
                                venEspecie.ShowDialog();
                            }

                            if (!selEspecie)
                            {
                                return false;
                            }
                        }
                        else
                            return venOrcamento;
                    }
                }
                #endregion

                #region DROGABELLA/PLANTAO
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && !vendaDeComanda)
                {
                    if (Convert.ToDouble(lblTotal.Text) > wsSaldoCartao && wsPreAutoriza == "N" && tipoVenda != 1 && dadosDaVenda.VendaConvenioParcela == 1)
                    {
                        if (!VendaBeneficioDrogabellaPlantaoParcial())
                        {
                            return false;
                        }
                        if (!FinalizaBeneficioDrogabellaPlantao())
                        {
                            return false;
                        }

                    }
                    else
                    {
                        if (!VendaBeneficioDrogabellaPlantao())
                        {
                            return false;
                        }

                        if (!FinalizaBeneficioDrogabellaPlantao())
                        {
                            return false;
                        }
                    }
                }
                #endregion

                #region VERIFICA SE JÁ FOI GERADO NUMERO DO PEDIDO
                if (!vendaID.Equals(0))
                {
                    inserir = false;
                }
                else
                    GeraIDVenda();
                #endregion

                #region BENEFICIOS
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && !vendaDeComanda)
                {
                    if (venOrcamento && !chkEntrega.Checked)
                    {
                        var identificaComanda = new DrogabellaComanda();
                        dtRetorno = identificaComanda.IdentificaSeVendaEPorComanda(vendaID);
                        if (dtRetorno.Rows.Count == 0)
                        {
                            identificaComanda.VendaId = vendaID;
                            identificaComanda.ValorAutorizado = valorAutorizado;
                            identificaComanda.ValorDiferenca = diferencaDoValorAutorizado;
                            identificaComanda.NumeroComanda = txtComanda.Text;

                            if (!identificaComanda.InsereRegistros(identificaComanda))
                            {
                                return false;
                            }
                        }
                        else
                            identificaComanda.ExcluirDadosPorVendaId(vendaID, false);
                    }
                }
                else if (venOrcamento && vendaBeneficio.Equals("VIDALINK"))
                {
                    var identificaComanda = new BeneficioVidaLinkComanda();
                    dtRetorno = identificaComanda.IdentificaSeVendaEPorComanda(vendaNumeroNSU, vendaID);
                    if (dtRetorno.Rows.Count == 0)
                    {
                        identificaComanda.NSU = vendaNumeroNSU;
                        identificaComanda.VendaID = vendaID;
                        identificaComanda.NumSeq = numeroSequencia;
                        identificaComanda.OpCadastro = Principal.usuario;
                        identificaComanda.DtCadastro = DateTime.Now;

                        if (!identificaComanda.InsereRegistros(identificaComanda))
                        {
                            vendaID = 0;
                            inserir = true;
                            return false;
                        }
                    }
                    else
                        identificaComanda.ExcluirDadosPorNSU(vendaNumeroNSU, 0, false);
                }
                else if (venOrcamento && vendaBeneficio.Equals("EPHARMA"))
                {
                    var comanda = new EpharmaComanda();
                    dtRetorno = comanda.IdentificaSeVendaEPorComanda(vendaNumeroNSU, vendaID);
                    if (dtRetorno.Rows.Count == 0)
                    {
                        comanda.NSU = vendaNumeroNSU;
                        comanda.VendaID = vendaID;
                        comanda.OpCadastro = Principal.usuario;
                        comanda.DtCadastro = DateTime.Now;

                        comanda.InsereDados(comanda);
                    }
                }
                else if (venOrcamento && vendaBeneficio.Equals("PORTALDROGARIA"))
                {
                    var identificaComanda = new BeneficioNovartisComanda();
                    dtRetorno = identificaComanda.IdentificaSeVendaEPorComanda(vendaNumeroNSU, vendaID);
                    if (dtRetorno.Rows.Count == 0)
                    {
                        identificaComanda.NSU = vendaNumeroNSU;
                        identificaComanda.VendaID = vendaID;
                        identificaComanda.Cartao = wsNumCartao;
                        identificaComanda.OpCadastro = Principal.usuario;
                        identificaComanda.DtCadastro = DateTime.Now;

                        if (!identificaComanda.InsereRegistros(identificaComanda))
                        {
                            vendaID = 0;
                            inserir = true;
                            return false;
                        }
                    }
                    else
                        identificaComanda.ExcluirDadosPorNSU(vendaNumeroNSU, 0, false);
                }
                else if (venOrcamento && vendaBeneficio.Equals("FARMACIAPOPULAR"))
                {
                    movtoFinanceiroSeq = 0;
                    var identificaComanda = new FpComanda();
                    dtRetorno = identificaComanda.IdentificaSeVendaEPorComanda(vendaNumeroNSU, vendaID);
                    if (dtRetorno.Rows.Count == 0)
                    {
                        identificaComanda.NSU = Funcoes.RemoveCaracter(vendaNumeroNSU);
                        identificaComanda.VendaID = vendaID;
                        identificaComanda.OpCadastro = Principal.usuario;
                        identificaComanda.DtCadastro = DateTime.Now;

                        if (!identificaComanda.InsereRegistros(identificaComanda))
                        {
                            vendaID = 0;
                            inserir = true;
                            return false;
                        }
                    }
                    else
                        identificaComanda.ExcluirDadosPorNSU(vendaNumeroNSU, 0, false);
                }

                if (!venOrcamento && pagamentoTotalOuParcial.Equals("T"))
                {
                    movtoFinanceiroSeq = 0;
                }

                if (!venOrcamento && vendaBeneficio.Equals("PORTALDROGARIA"))
                {
                    double subsidio = 0;
                    var dadosSubsidio = new BeneficioNovartisProdutos();
                    dtRetorno = dadosSubsidio.BuscaValorDoSubsidio(vendaNumeroNSU);
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        subsidio += Convert.ToDouble(dtRetorno.Rows[i]["VALOR_SUBSIDIO"]);
                    }

                    if (subsidio > 0)
                    {
                        if (subsidio == Convert.ToDouble(lblTotal.Text))
                        {
                            codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "33", true));
                            diferencaDoValorAutorizado = 0;
                            pagamentoTotalOuParcial = "T";
                            valorAutorizado = subsidio;
                        }
                        else
                        {
                            codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "33", true));
                            diferencaDoValorAutorizado = Convert.ToDouble(lblTotal.Text) - subsidio;
                            pagamentoTotalOuParcial = "P";
                            valorAutorizado = subsidio;
                        }
                    }
                }
                #endregion

                if (Funcoes.LeParametro(6, "386", false).Equals("S") && vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                {
                    retorno = "Número Autorização Drogabella : " + dadosDaVenda.VendaTransID;
                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaTransIDReceita))
                    {
                        retorno += "\n\nNúmero Autorização Drogabella com receita: " + dadosDaVenda.VendaTransIDReceita;
                    }
                    MessageBox.Show(retorno, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (InserirDadosPreVenda(cxStatus.Count == 0 ? "N" : cxStatus[0].CaixaAberto).Equals(true))
                {
                    #region OPERAÇÕES CAIXA
                    Cursor = Cursors.WaitCursor;

                    #region FORMA DE PAGAMENTO 
                    vendaPedFormaPagto.Clear();

                    for (int i = 0; i < venFormaPagto.dtFormas.Rows.Count; i++)
                    {
                        var formasPagto = new VendasFormaPagamento();
                        formasPagto.EmpCodigo = dadosDaVenda.EmpCodigo;
                        formasPagto.EstCodigo = dadosDaVenda.EstCodigo;
                        formasPagto.VendaId = dadosDaVenda.VendaID;
                        formasPagto.VendaFormaID = Convert.ToInt32(venFormaPagto.dtFormas.Rows[i]["FORMA_ID"]);
                        formasPagto.VendaParcela = Convert.ToInt32(venFormaPagto.dtFormas.Rows[i]["PARCELA"]);
                        formasPagto.VendaParcelaVencimento = Convert.ToDateTime(venFormaPagto.dtFormas.Rows[i]["VENC"]);
                        formasPagto.VendaValorParcela = Convert.ToDouble(venFormaPagto.dtFormas.Rows[i]["TOTAL"]);
                        formasPagto.QtdeVias = Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "QTDE_VIAS", "FORMA_ID", formasPagto.VendaFormaID.ToString(), false, false, true);
                        vendaPedFormaPagto.Add(formasPagto);
                    }

                    List<VendasFormaPagamento> retorno = vendaPedFormaPagto.Where(i => i.VendaFormaID == Convert.ToInt32(Funcoes.LeParametro(6, "116", true))).ToList();
                    if (retorno.Count != 0)
                    {
                        ComprovanteOrcamento();
                        codFormaPagto = retorno[0].VendaFormaID;
                        return false;
                    }
                    #endregion

                    #region ESPECIE
                    vendaEspecie.Clear();
                    for (int i = 0; i < venEspecie.dtEspecies.Rows.Count; i++)
                    {
                        vendaEspecie.Add(new VendasEspecies
                        {
                            EstCodigo = dadosDaVenda.EstCodigo,
                            EmpCodigo = dadosDaVenda.EmpCodigo,
                            VendaId = dadosDaVenda.VendaID,
                            EspCodigo = Convert.ToInt32(Convert.ToDouble(venEspecie.dtEspecies.Rows[i]["ESP_CODIGO"])),
                            Valor = Convert.ToDouble(venEspecie.dtEspecies.Rows[i]["TOTAL"]),
                            EspDescricao = venEspecie.dtEspecies.Rows[i]["ESP_DESCRICAO"].ToString(),
                            EspVinculado = venEspecie.dtEspecies.Rows[i]["ESP_VINCULADO"].ToString(),
                            EspSAT = venEspecie.dtEspecies.Rows[i]["ESP_SAT"].ToString(),
                            EspEcf = venEspecie.dtEspecies.Rows[i]["ESP_ECF"].ToString(),
                            OpCadastro = Principal.usuario,
                            DtCadastro = DateTime.Now
                        });
                    }
                    #endregion

                    #region GRAVA PARTICULAR
                    if (wsConWeb.Equals("4") && !venOrcamento && vendaBeneficio.Equals("PARTICULAR"))
                    {
                        if (!InserirCobranca())
                        {
                            vendaID = 0;
                            inserir = true;
                            return false;
                        }
                    }
                    #endregion

                    #region ENTREGA
                    if (chkEntrega.Checked)
                    {
                        if (venOrcamento)
                        {
                            if (vEntr_Valor > Convert.ToDouble(lblTotal.Text))
                            {
                                if (!vendaBeneficio.Equals("DROGABELLA/PLANTAO") && diferencaDoValorAutorizado == 0)
                                    vEntr_Valor = Math.Round(vEntr_Valor - Convert.ToDouble(lblTotal.Text), 2);
                            }
                            int id = Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA", "ENT_CONTROLE_ID");
                            var dadosEntrega = new ControleEntrega
                                (
                                    dadosDaVenda.EmpCodigo,
                                    dadosDaVenda.EstCodigo,
                                    id,
                                    dadosDaVenda.VendaID,
                                    dadosDaVenda.VendaCfID,
                                    0,
                                    0,
                                    0,
                                    dadosDaVenda.VendaDataHora,
                                    vEntr_End,
                                    Funcoes.RemoveCaracter(vEntr_Numero) == "" ? "0" : vEntr_Numero,
                                    vEntr_Bairro,
                                    vEntr_Cep,
                                    vEntr_Cidade,
                                    vEntr_UF,
                                    vEntr_Fone,
                                    vEntr_Valor,
                                    "",
                                    vEntr_Obs,
                                    DateTime.Now,
                                    Principal.usuario,
                                    DateTime.Now,
                                    Principal.usuario
                                );

                            if (!dadosEntrega.InsereRegistros(dadosEntrega))
                                return false;

                            var dadosEntregaStatus = new ControleEntregaStatus
                                (
                                    dadosDaVenda.EmpCodigo,
                                    dadosDaVenda.EstCodigo,
                                    Funcoes.IdentificaVerificaID("CONTROLE_ENTREGA_STATUS", "ENT_ST_ID"),
                                    id,
                                    1,
                                    0,
                                    dadosDaVenda.VendaDataHora,
                                    Principal.usuario,
                                    "",
                                    DateTime.Now,
                                    Principal.usuario,
                                    DateTime.Now,
                                    Principal.usuario
                                );

                            if (!dadosEntregaStatus.InsereRegistros(dadosEntregaStatus))
                                return false;
                        }

                        if (!Principal.tipoCupom.Equals("C"))
                        {
                            venOrcamento = true;
                            permtVenda = false;
                        }


                    }
                    #endregion

                    if (!InserirFormasDePagamento())
                        return false;

                    if (!InserirEspecies())
                        return false;

                    if (!InserirMovimentos())
                        return false;

                    return true;
                    #endregion
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public bool InserirMovimentos()
        {
            try
            {
                long idCAixa = Funcoes.GeraIDLong("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID", dadosDaVenda.EstCodigo, "", dadosDaVenda.EmpCodigo);
                movtoFinanceiroID = Funcoes.GeraIDLong("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_ID", dadosDaVenda.EstCodigo, "", dadosDaVenda.EmpCodigo);

                for (int i = 0; i < vendaPedFormaPagto.Count; i++)
                {
                    if (Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "OPERACAO_CAIXA", "FORMA_ID", vendaPedFormaPagto[i].VendaFormaID.ToString(), false, false, true).Equals("S"))
                    {
                        //MOVIMENTO FINANCEIRO//
                        var mFinanceiro = new MovimentoFinanceiro
                            (
                                dadosDaVenda.EmpCodigo,
                                dadosDaVenda.EstCodigo,
                                movtoFinanceiroID + i,
                                dtPedido,
                                dtPedido,
                                "V",
                                Principal.usuario,
                                vendaPedFormaPagto[i].VendaValorParcela,
                                movtoFinanceiroSeq,
                                DateTime.Now,
                                Principal.usuario,
                                vendaID
                            );

                        if (!mFinanceiro.InserirDados(mFinanceiro))
                            return false;


                        //MOVIMENTO DE CAIXA//
                        var mCaixa = new MovimentoCaixa
                            (
                                dadosDaVenda.EmpCodigo,
                                dadosDaVenda.EstCodigo,
                                idCAixa + i,
                                dtPedido,
                                Convert.ToInt32(Funcoes.LeParametro(6, "75", false)),
                                "+",
                                vendaPedFormaPagto[i].VendaValorParcela,
                                Principal.usuario,
                                movtoFinanceiroID,
                                movtoFinanceiroSeq,
                                DateTime.Now,
                                Principal.usuario,
                                "ORIGEM VENDAS",
                                vendaID
                            );

                        if (!mCaixa.InserirDados(mCaixa))
                            return false;

                        long id = Funcoes.GeraIDLong("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID", dadosDaVenda.EstCodigo, "", dadosDaVenda.EmpCodigo);
                        //MOVIMENTO DE CAIXA ESPECIE//
                        for (int y = 0; y < vendaEspecie.Count; y++)
                        {
                            if (Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_CAIXA", "ESP_CODIGO", vendaEspecie[y].EspCodigo.ToString()).Equals("S"))
                            {
                                double troco = 0;
                                if (vendaEspecie[y].EspDescricao.Equals("DINHEIRO"))
                                {
                                    troco = valorTroco;
                                }
                                var mCaixaEspecie = new MovimentoCaixaEspecie
                                    (
                                        dadosDaVenda.EmpCodigo,
                                        dadosDaVenda.EstCodigo,
                                        id + y,
                                        dtPedido,
                                        vendaEspecie[y].EspCodigo,
                                        vendaEspecie[y].Valor - troco,
                                        Principal.usuario,
                                        movtoFinanceiroID,
                                        DateTime.Now,
                                        Principal.usuario
                                    );

                                if (!mCaixaEspecie.InserirDados(mCaixaEspecie))
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Movimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirEspecies()
        {
            try
            {
                int id = Funcoes.IdentificaVerificaID("VENDAS_ESPECIES", "VENDA_ESPECIE_ID", Principal.estAtual, "", Principal.empAtual);
                for (int x = 0; x < vendaEspecie.Count; x++)
                {
                    double troco = 0;
                    if (vendaEspecie[x].EspDescricao.Equals("DINHEIRO"))
                    {
                        troco = valorTroco;
                    }
                    //VENDAS_ESPECIES
                    var dadosEspecies = new VendasEspecies
                            (
                                vendaEspecie[x].EmpCodigo,
                                vendaEspecie[x].EstCodigo,
                                vendaEspecie[x].VendaId,
                                id + x,
                                vendaEspecie[x].EspCodigo,
                                vendaEspecie[x].Valor - troco,
                                "",
                                "",
                                "",
                                DateTime.Now,
                                Principal.usuario.Trim()
                            );
                    if (!dadosEspecies.InserirDados(dadosEspecies))
                        return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirFormasDePagamento()
        {
            try
            {
                List<int> result = vendaPedFormaPagto.Select(o => o.VendaFormaID).Distinct().ToList();

                int id = Funcoes.IdentificaVerificaID("VENDAS_FORMA_PAGAMENTO", "VENDA_FORMA_PAGTO_ID", Principal.estAtual, "", Principal.empAtual);

                for (int x = 0; x < result.Count; x++)
                {
                    List<VendasFormaPagamento> retorno = vendaPedFormaPagto.Where(i => i.VendaFormaID == result[x]).ToList();

                    for (int i = 0; i < retorno.Count; i++)
                    {
                        //VENDAS_FORMA_PAGAMENTO
                        var dadosFormaPagamento = new VendasFormaPagamento
                            (
                                retorno[i].EmpCodigo,
                                retorno[i].EstCodigo,
                                retorno[i].VendaId,
                                id,
                                retorno[i].VendaFormaID,
                                retorno[i].VendaParcela,
                                retorno[i].VendaValorParcela,
                                retorno[i].VendaParcelaVencimento,
                                DateTime.Now,
                                Principal.usuario
                            );
                        if (!dadosFormaPagamento.InserirDados(dadosFormaPagamento))
                            return false;

                        id += 1;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Forma de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirCobranca()
        {
            try
            {
                List<int> result = vendaPedFormaPagto.Select(o => o.VendaFormaID).Distinct().ToList();

                for (int x = 0; x < result.Count; x++)
                {
                    List<VendasFormaPagamento> retorno = vendaPedFormaPagto.Where(i => i.VendaFormaID == result[x]).ToList();
                    int id = 0;

                    if (retorno[0].VendaParcela.Equals(1))
                    {
                        id = Funcoes.IdentificaVerificaID("COBRANCA", "COBRANCA_ID", Principal.estAtual, "", Principal.empAtual);
                        //COBRANCA
                        var dadosCobranca = new Cobranca
                            (
                                retorno[0].EmpCodigo,
                                retorno[0].EstCodigo,
                                id,
                                dadosDaVenda.VendaCfID,
                                DateTime.Now,
                                dadosDaVenda.VendaTotal,
                                retorno[0].VendaFormaID,
                                dadosDaVenda.VendaColCodigo,
                                dadosDaVenda.VendaID,
                                "C",
                                "A",
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!dadosCobranca.InserirDados(dadosCobranca))
                            return false;
                    }

                    int idParcela = Funcoes.IdentificaVerificaID("COBRANCA_PARCELA", "COBRANCA_PARCELA_ID", Principal.estAtual, "", Principal.empAtual);
                    for (int y = 0; y < retorno.Count; y++)
                    {
                        var dadosParcelas = new CobrancaParcela
                            (
                                vendaPedFormaPagto[x].EmpCodigo,
                                vendaPedFormaPagto[x].EstCodigo,
                                idParcela + y,
                                id,
                                retorno[y].VendaParcela,
                                retorno[y].VendaValorParcela,
                                retorno[y].VendaValorParcela,
                                "A",
                                retorno[y].VendaParcelaVencimento,
                                DateTime.Now,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!dadosParcelas.InserirDados(dadosParcelas))
                            return false;

                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Cobranca", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            retorno = cxStatus.IdentificaStatusDoCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);
            if (String.IsNullOrEmpty(retorno) && String.IsNullOrEmpty(txtComanda.Text))
            {
                Principal.mensagem = "Necessário informar N° de Comanda!";
                Funcoes.Avisa();
                txtComanda.Focus();
                return false;
            }
            if (tProdutos.Rows.Count.Equals(0))
            {
                Principal.mensagem = "Adicione ao menos um item ao pedido!";
                Funcoes.Avisa();
                txtProduto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodCliEmp.Text))
            {
                Principal.mensagem = "Selecione o Cliente";
                Funcoes.Avisa();
                txtCodCliEmp.Focus();
                return false;
            }
            return true;
        }

        public bool InserirDadosPreVenda(string statusCaixa)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string prodUsoContinuo;

                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                #region DADOS DA VENDA
                dadosDaVenda.EmpCodigo = Principal.empAtual;
                dadosDaVenda.EstCodigo = Principal.estAtual;
                dadosDaVenda.VendaID = vendaID;
                dadosDaVenda.VendaBeneficio = vendaBeneficio;

                if (!inserir)
                {
                    var dadosItens = new VendasItens();
                    dtRetorno = dadosItens.BuscaCodBarrasPorVendaID(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo,
                                dadosDaVenda.VendaID);
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        linha_resultado = tProdutos.Select("COD_BARRA = '" + dtRetorno.Rows[i]["PROD_CODIGO"].ToString() + "'");
                        if (linha_resultado.Length == 0)
                        {
                            dadosItens.ExcluirProdutos(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo,
                              dadosDaVenda.VendaID, dtRetorno.Rows[i]["PROD_CODIGO"].ToString());
                        }
                    }

                    dadosDaVendaPagamento.ExcluirDados(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID);

                    var excluiLog = new LogLiberacaoDesc();
                    excluiLog.ExcluirDados(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, dadosDaVenda.VendaID);
                }

                BancoDados.AbrirTrans();
                transAberta = true;

                dadosDaVenda.VendaCfID = IDCliente;
                dadosDaVenda.VendaData = dtPedido;
                dadosDaVenda.VendaDataHora = dtPedido;

                if (!String.IsNullOrEmpty(wsEmpresa))
                {
                    dadosDaVenda.VendaConCodigo = wsEmpresa;
                }


                dadosDaVenda.VendaTotal = Convert.ToDouble(lblTotal.Text);
                dadosDaVenda.VendaSubTotal = Convert.ToDouble(lblSubTotal.Text);
                dadosDaVenda.VendaDiferenca = Convert.ToDouble(txtGAjuste.Text);
                dadosDaVenda.VendaNfDevolucao = "N";
                dadosDaVenda.VendaMedico = wsCRM;
                dadosDaVenda.VendaNumReceita = wsNumReceita;
                dadosDaVenda.VendaEntrega = chkEntrega.Checked == false ? "N" : vendaBeneficio == "DROGABELLA/PLANTAO" ? "N" : "S";
                dadosDaVenda.OpCadastro = Principal.usuario;
                dadosDaVenda.DtCadastro = DateTime.Now;
                dadosDaVenda.VendaQtde = tProdutos.Rows.Count;
                dadosDaVenda.DtAlteracao = DateTime.Now;
                dadosDaVenda.OpAlteracao = Principal.usuario;
                dadosDaVenda.VendaAjusteGeral = Convert.ToDouble(txtAjuste.Text);
                dadosDaVenda.VendaObrigaReceita = wsObrigaReceita;

                if (venOrcamento && !String.IsNullOrEmpty(txtComanda.Text))
                {
                    if (chkEntrega.Checked)
                    {
                        dadosDaVenda.VendaStatus = "P";
                    }
                    else
                        dadosDaVenda.VendaStatus = "A";

                    dadosDaVenda.VendaComanda = Convert.ToInt32(txtComanda.Text);
                    dadosDaVenda.VendaEmissao = null;
                    dadosDaVenda.VendaColCodigo = Convert.ToInt32(txtVendedor.Text);
                }
                else
                {
                    dadosDaVenda.VendaStatus = "F";
                    dadosDaVenda.VendaComanda = txtComanda.Text == "" ? 0 : Convert.ToInt32(txtComanda.Text);
                    dadosDaVenda.VendaEmissao = DateTime.Now.ToString("dd/MM/yyyy");
                    if (String.IsNullOrEmpty(dadosDaVenda.VendaColCodigo.ToString()) || dadosDaVenda.VendaColCodigo == 0)
                    {
                        dadosDaVenda.VendaColCodigo = Convert.ToInt32(txtVendedor.Text);
                    }
                }

                if (!dadosDaVenda.InserirDadosPreVenda(dadosDaVenda, inserir))
                    return false;
                #endregion

                #region DADOS PEDIDOS ITENS
                var vendasItens = new VendasItens();

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    impostoEstadual = 0;
                    impostoFederal = 0;
                    impostoMunicipal = 0;

                    VendasItens dadosDaVendaItens = new VendasItens();

                    prodUsoContinuo = Funcoes.LeParametro(6, "88", false);

                    if (dProduto.IdentificaUsoContinuo(tProdutos.Rows[i]["COD_BARRA"].ToString()))
                    {
                        if ((prodUsoContinuo.Equals("V") && !permtVenda) || (prodUsoContinuo.Equals("C") && permtVenda) || prodUsoContinuo.Equals("T"))
                        {
                            prodContCliente = true;
                            var cliContinuo = new ClienteContinuo(
                                Principal.doctoCliente,
                                tProdutos.Rows[i]["COD_BARRA"].ToString().Trim(),
                                0,
                                Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]),
                                DateTime.Now,
                                DateTime.Now
                                );
                            Principal.dtRetorno = cliContinuo.IdentificaClienteXProduto(tProdutos.Rows[i]["COD_BARRA"].ToString(), Principal.doctoCliente);
                            if (Principal.dtRetorno.Rows.Count.Equals(0))
                            {
                                if (!cliContinuo.InsereRegistros(cliContinuo))
                                    return false;
                            }
                            else
                            {
                                qtdeUsoContinuo = Principal.dtRetorno.Rows[0]["CONT_QTDE_DIA"].ToString() == "" ? 1 : Convert.ToInt32(Principal.dtRetorno.Rows[0]["CONT_QTDE_DIA"]);
                                if (qtdeUsoContinuo != 0)
                                {
                                    diasProdContinuo = (Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]) * qtdeEmbalagem) / qtdeUsoContinuo;
                                }

                                dataProxima = dtPedido.AddDays(diasProdContinuo);
                                if (!cliContinuo.AtualizaDados(dtPedido, Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), dataProxima, Principal.doctoCliente, tProdutos.Rows[i]["COD_BARRA"].ToString()))
                                    return false;
                            }
                        }
                    }


                    dadosDaVendaItens.EmpCodigo = Principal.empAtual;
                    dadosDaVendaItens.EstCodigo = Principal.estAtual;
                    dadosDaVendaItens.VendaID = vendaID;
                    dadosDaVendaItens.VendaItem = i + 1;
                    dadosDaVendaItens.ProdID = Convert.ToInt32(tProdutos.Rows[i]["PROD_ID"]);
                    dadosDaVendaItens.ProdCodigo = tProdutos.Rows[i]["COD_BARRA"].ToString().Trim();
                    dadosDaVendaItens.VendaItemQtde = Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]);
                    dadosDaVendaItens.VendaItemUnitario = Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]);
                    dadosDaVendaItens.VendaItemSubTotal = Convert.ToDouble(tProdutos.Rows[i]["PROD_SUBTOTAL"]);
                    dadosDaVendaItens.VendaItemDiferenca = Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]));
                    dadosDaVendaItens.VendaItemTotal = Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"]);
                    dadosDaVendaItens.VendaItemUnidade = tProdutos.Rows[i]["PROD_UNIDADE"].ToString();
                    dadosDaVendaItens.VendaPreValor = Convert.ToDouble(tProdutos.Rows[i]["PRE_VALOR"]);
                    dadosDaVendaItens.OpCadastro = Principal.usuario;
                    dadosDaVendaItens.DtCadastro = DateTime.Now;
                    dadosDaVendaItens.VendaItemReceita = tProdutos.Rows[i]["PROD_REC"].ToString();
                    dadosDaVendaItens.VendaItemComissao = Convert.ToDouble(tProdutos.Rows[i]["PROD_DB"]);
                    dadosDaVendaItens.ColCodigo = dadosDaVenda.VendaColCodigo;

                    if (tProdutos.Rows[i]["PROD_CONTROLADO"].ToString().Equals("S") || Funcoes.LeParametro(6, "351", true).Equals("S"))
                    {
                        dadosDaVendaItens.VendaItemLote = tProdutos.Rows[i]["PROD_LOTE"].ToString();
                    }
                    else
                        dadosDaVendaItens.VendaItemLote = null;

                    if (Funcoes.LeParametro(6, "220", true).Equals("S"))
                    {
                        dadosDaVendaItens.VendaPromocao = tProdutos.Rows[i]["PROD_PROMO"].ToString();
                        dadosDaVendaItens.VendaDescLiberado = tProdutos.Rows[i]["DESC_LIB"].ToString();
                    }
                    else
                    {
                        dadosDaVendaItens.VendaPromocao = "N";
                        dadosDaVendaItens.VendaDescLiberado = "N";
                    }

                    if (venOrcamento.Equals(false))
                    {
                        //ATUALIZA ESTOQUE ATUAL//
                        if (Funcoes.LeParametro(6, "82", true).Equals("S"))
                        {
                            if (!dProduto.AtualizaEstoque(tProdutos.Rows[i]["COD_BARRA"].ToString(), Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), "-", true))
                                return false;
                        }
                        else
                        {
                            if ((Convert.ToInt32(tProdutos.Rows[i]["PROD_ESTATUAL"]) - Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])) < 0)
                            {
                                BancoDados.ErroTrans();
                                Principal.msgEstilo = MessageBoxButtons.OK;
                                Principal.msgIcone = MessageBoxIcon.Warning;
                                Principal.msgTitulo = "Vendas";
                                Principal.mensagem = "Não foi possível incluir o pedido.\n"
                                    + "Produto " + tProdutos.Rows[i]["COD_BARRA"] + " abaixo do estoque!\n"
                                    + "Quantidade de estoque atual é " + tProdutos.Rows[i]["PROD_ESTATUAL"];
                                Funcoes.Avisa();

                                return false;
                            }
                            else
                            {
                                if (!dProduto.AtualizaEstoque(tProdutos.Rows[i]["COD_BARRA"].ToString(), Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), "-", true))
                                    return false;
                            }
                        }

                        var dadosMovimento = new MovEstoque
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EstIndice = id,
                            ProdCodigo = tProdutos.Rows[i]["COD_BARRA"].ToString(),
                            EntQtde = 0,
                            EntValor = 0,
                            SaiQtde = Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]),
                            SaiValor = Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]) * Convert.ToDouble(tProdutos.Rows[i]["PROD_CUSME"]),
                            OperacaoCodigo = "V",
                            DataMovimento = DateTime.Now,
                            DtCadastro = DateTime.Now,
                            OpCadastro = Principal.usuario
                        };

                        if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                            return false;

                        dadosDaVendaItens.VendaItemIndice = id;

                        id += 1;

                        if (Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"])) != 0)
                        {
                            var alteracao = new LogLiberacaoDesc(
                                   Principal.usuario,
                                   tProdutos.Rows[i]["COD_BARRA"].ToString(),
                                   Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]),
                                   Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]),
                                   Convert.ToDouble(tProdutos.Rows[i]["DESC_MAX"]),
                                   Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["PROD_DPORC"])),
                                   Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"]),
                                   dadosDaVenda.VendaColCodigo,
                                   dadosDaVenda.EmpCodigo,
                                   dadosDaVenda.EstCodigo,
                                   dadosDaVenda.VendaID,
                                   DateTime.Now
                                   );

                            alteracao.InsereRegistros(alteracao);
                        }
                    }
                    else
                    {
                        if (Funcoes.LeParametro(6, "82", true).Equals("N"))
                        {
                            if ((Convert.ToInt32(tProdutos.Rows[i]["PROD_ESTATUAL"]) - Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])) < 0)
                            {
                                BancoDados.ErroTrans();
                                Principal.msgEstilo = MessageBoxButtons.OK;
                                Principal.msgIcone = MessageBoxIcon.Warning;
                                Principal.msgTitulo = "Vendas";
                                Principal.mensagem = "Não foi possível incluir o pedido.\n"
                                    + "Produto " + tProdutos.Rows[i]["COD_BARRA"] + " abaixo do estoque!\n"
                                    + "Quantidade de estoque atual é " + tProdutos.Rows[i]["PROD_ESTATUAL"];
                                Funcoes.Avisa();

                                return false;
                            }
                        }
                        dadosDaVendaItens.VendaItemIndice = 0;
                    }

                    venItens.Add(new VendaItem
                    {
                        ItemECF = tProdutos.Rows[i]["ECF"].ToString(),
                        ItemQtde = tProdutos.Rows[i]["PROD_QTDE"].ToString(),
                        ItemVlUnit = tProdutos.Rows[i]["PROD_VUNI"].ToString(),
                        ItemTipoDesc = "D$",
                        ItemVlDesc = tProdutos.Rows[i]["PROD_DVAL"].ToString(),
                        ItemCodBarra = tProdutos.Rows[i]["COD_BARRA"].ToString(),
                        ItemUniade = tProdutos.Rows[i]["PROD_UNIDADE"].ToString(),
                        ItemDescr = tProdutos.Rows[i]["PROD_DESCR"].ToString(),
                        ItemNcm = tProdutos.Rows[i]["NCM"].ToString(),
                        ItemTipo = "0",
                        ItemAliquota = tProdutos.Rows[i]["ALIQUOTA"].ToString()
                    });

                    if (!dadosDaVendaItens.InserirItensPreVenda(dadosDaVendaItens, false, txtVendedor.Text))
                        return false;

                    vendaItens.Add(dadosDaVendaItens);
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Dados Pre-Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void DadosDaVendaCliente(string cfDocto)
        {
            dadosVendaCliente = dCliente.DadosClienteCfDocto(cfDocto);
        }

        private void chkEntrega_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private bool ImprimirCupomFiscal()
        {
            try
            {
                if (Imprimir.VerificaModeloImpressoraFiscal().Equals(false))
                {
                    return false;
                }

                #region CPF/CNPJ
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    if (!dadosVendaCliente.Count.Equals(0))
                    {
                        if (dadosVendaCliente[0].CfTipoDocto.Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dadosVendaCliente[0].CfDocto;
                            notaPaulista.txtNome.Text = dadosVendaCliente[0].CfNome;
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dadosVendaCliente[0].CfDocto;
                            notaPaulista.txtNome.Text = dadosVendaCliente[0].CfNome;
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }
                #endregion

                string modeloImpressora = Funcoes.LeParametro(6, "08", true, Funcoes.LeParametro(6, "123", true).Equals("N") ? "GERAL" : Principal.nomeEstacao).ToUpper();

                Cursor = Cursors.WaitCursor;

                ExibirMensagem("Imprimindo Cupom Fiscal. Aguarde...");

                retorno = Imprimir.ImpFiscalInformacoesECF("107", vendaNumeroCaixa);
                if (String.IsNullOrEmpty(retorno))
                    return false;

                vendaNumeroCaixa = retorno;

                vendaNumeroNota = Imprimir.ImpFiscalNumeroCupom();

                if (modeloImpressora.Equals("EPSON"))
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt64(vendaNumeroNota.Substring(58, 6)) + 1);
                }
                else
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt32(vendaNumeroNota) + 1);
                }

                if (!Imprimir.ImpFiscalAbreCupom(Principal.NumCpfCnpj, Principal.NomeNP))
                    return false;

                if (!Imprimir.ImpFiscalVenderItem(venItens))
                    return false;

                if (!Imprimir.ImpFiscalTotalizarCupom("D", txtGAjuste.Text))
                    return false;
                
                if (!Imprimir.ImpFiscalMeioPagamento(vendaEspecie))
                    return false;

                var colaborador = new Colaborador();
                string mensagem = "";

                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual) + "    " + "Num.Pedido:" + dadosDaVenda.VendaID, 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";

                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacao))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao:" + dadosDaVenda.VendaAutorizacao, 48);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacaoReceita))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao com Receita:" + dadosDaVenda.VendaAutorizacaoReceita, 48);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    mensagem += Funcoes.PrencherEspacoEmBranco("Administradora: DROGABELLA/PLANTAO CARD", 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Cartao: " + wsNumCartao, 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Conveniado:  " + (wsNomeCartao.Length > 33 ? wsNomeCartao.Substring(0, 32) : wsNomeCartao), 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("EMPRESA: " + ((wsEmpresa + " - " + wsNomeEmpresa).Length > 40 ? (wsEmpresa + " - " + wsNomeEmpresa).Substring(0, 40)
                        : (wsEmpresa + " - " + wsNomeEmpresa)), 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }
                else if (vendaBeneficio.Equals("PARTICULAR"))
                {
                    string nomeConveniada = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosDaVenda.VendaConCodigo);
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual) + "    " + "Num.Pedido:" + dadosDaVenda.VendaID, 48);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Empresa: " + ((dadosDaVenda.VendaConCodigo + " - "
                        + nomeConveniada).Length > 32 ?
                        (dadosDaVenda.VendaConCodigo + " - " + nomeConveniada).Substring(0, 32)
                       : (dadosDaVenda.VendaConCodigo + " - " + nomeConveniada).ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Nome: " + Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_NOME", "CF_ID", dadosDaVenda.VendaCfID.ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }

                #region LEI DE IMPOSTO
                double impostoFederal, impostoEstadual, impostoMunicipal;
                var buscaImpostos = new Ibpt();

                for (int i = 0; i < venItens.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    dtRetorno = buscaImpostos.BuscaImpostosPorNCM(venItens[i].ItemNcm);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"]) > 0)
                            impostoFederal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"]) > 0)
                            impostoEstadual = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"]) > 0)
                            impostoMunicipal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"])) / 100;

                        totalImpFederal += impostoFederal;
                        totalImpEstadual += impostoEstadual;
                        totalImpMunicipal += impostoMunicipal;
                    }
                }

                mensagem += Funcoes.PrencherEspacoEmBranco("Trib aprox R$:" + String.Format("{0:N}", totalImpFederal).Replace(",", ".") + " Fed, " + String.Format("{0:N}", totalImpEstadual).Replace(",", ".") + " Est, " + String.Format("{0:N}", totalImpMunicipal).Replace(",", ".") + " Mun", 48);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco("Fonte:IBPT   5oi7eW", 48);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco(Funcoes.LeParametro(2, "28", false), 48);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                #endregion

                if (!Imprimir.ImpFiscalFechaCupomMsg(mensagem))
                    return false;

                #region CUPONS VINCULADOS
                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(vendaPedFormaPagto[0].QtdeVias);

                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoFiscal(qtdeVias, vendaNumeroNota, Convert.ToDouble(lblTotal.Text),
                            vendaEspecie[i].Valor, vendaEspecie[i].EspEcf, colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && vendaBeneficio.Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        ComprovantesVinculados.VinculadoDrogabellaPlantaoFiscal(qtdeVias, vendaNumeroNota,
                           dadosDaVenda.VendaDataHora, Convert.ToDouble(lblTotal.Text),
                         vendaEspecie[0].Valor, vendaEspecie[0].EspEcf, dadosDaVenda.VendaAutorizacao, dadosDaVenda.VendaAutorizacaoReceita, dadosDaVenda.VendaID,
                         wsProdutosCupomSemReceita, wsProdutosCupomReceita, wsNomeEmpresa, wsEmpresa, wsNumCartao, wsNomeCartao, chkEntrega.Checked, wsObrigaReceita, wsDadosMedico,
                         colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual), dadosDaVenda.VendaConvenioParcela);
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && wsConWeb.Equals("4"))
                    {
                        string produtosComprov = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                            produtosComprov += tProdutos.Rows[x]["COD_BARRA"] + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"]) > 0)
                            {
                                produtosComprov += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[x]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[x]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                            }
                            produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        ComprovantesVinculados.VinculadoParticularFiscal(qtdeVias, vendaNumeroNota, dadosDaVenda.VendaID,
                            Convert.ToDouble(lblTotal.Text), vendaEspecie[i].Valor, vendaEspecie[i].EspEcf, colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual),
                            vendaPedFormaPagto, txtCliEmp.Text, dadosDaVenda.VendaConCodigo, produtosComprov);
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + " - " + ex.Source + " - " + ex.StackTrace, "Impressão Cupom Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                OcultarMensagem();
            }
        }

        private void ExibirMensagem(string msg)
        {
            lblMsg.Text = msg;
            lblMsg.Refresh();
            pnlMsg.Visible = true;
            pnlMsg.Location = new Point((this.Size.Width / 2) - (this.pnlMsg.Size.Width / 2), (this.Size.Height / 2) - (this.pnlMsg.Size.Height / 2));
            pnlMsg.Refresh();
        }

        private void OcultarMensagem()
        {
            lblMsg.Text = "mensagem";
            pnlMsg.Visible = false;
        }

        private void btnBeneficios_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnBeneficios, "F1-Beneficios");
        }

        private void btnBeneficios_Click(object sender, EventArgs e)
        {
            try
            {
                if (StatusCaixa() || !String.IsNullOrEmpty(txtComanda.Text))
                {
                    Cursor = Cursors.WaitCursor;
                    frmVenBeneficios venBeneficios = new frmVenBeneficios(this);
                    venBeneficios.ShowDialog();

                    if (Principal.venBeneficio == "FALSE" && vendaBeneficio.Equals("FARMACIAPOPULAR"))
                    {
                        Limpar();
                        return;
                    }

                    if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && tProdutos.Rows.Count > 0)
                    {
                        MessageBox.Show("Necessário informar os produtos novamente devido a venda ser Convênio", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            linha_resultado = tProdutos.Select("COD_BARRA = '" + tProdutos.Rows[i]["COD_BARRA"] + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].Delete();
                            }
                        }

                        InserirProdCupom(tProdutos);
                        limparProd();
                        IsentaTaxaEntrega();
                        CamposProdutoInvisivel();

                        object sumObject;
                        sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                        lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                        txtProduto.Focus();
                    }

                    //IDENTIFICA QUAL TIPO DE BENEFICIO//
                    if (vendaBeneficio.Equals("FARMACIAPOPULAR") && !string.IsNullOrEmpty(Principal.idCliFor))
                    {
                        #region FARMACIA POPULAR
                        IDCliente = Convert.ToInt32(Principal.idCliFor);

                        var buscaDados = new FpMedicamentosSolicitacao();
                        DataTable dtRetorno = buscaDados.BuscaProdutosPorNSU(Funcoes.RemoveCaracter(vendaNumeroNSU));
                        for (int i = 0; i < dtRetorno.Rows.Count; i++)
                        {
                            dtPedido = DateTime.Now;

                            linha_resultado = tProdutos.Select("COD_BARRA = '" + dtRetorno.Rows[i]["CODBARRAS"].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                double qtde = Convert.ToDouble(dtRetorno.Rows[i]["QTDAUTORIZADA"]) / Convert.ToDouble(dtRetorno.Rows[i]["PROD_QTDE_UN_COMP_FP"]);
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", dtRetorno.Rows[i]["PRECOVENDA"]);
                                linha_resultado[0]["PROD_QTDE"] = qtde;
                                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PRECOVENDA"]) * qtde);
                                linha_resultado[0]["BENEFICIO"] = "S";
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PRECOVENDA"]) * qtde);
                                linha_resultado[0].EndEdit();
                                InserirProdCupom(tProdutos);
                            }
                            bloqueiaProduto = true;
                        }

                        if (StatusCaixa())
                        {
                            dtRetorno = buscaDados.IdentificaSomatorioVenda(Funcoes.RemoveCaracter(vendaNumeroNSU));
                            if (Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]) == 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "31", true));
                                pagamentoTotalOuParcial = "T";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]);
                            }
                            else if (Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]) > 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "31", true));
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]) - Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOCLIENTE"]);
                                pagamentoTotalOuParcial = "P";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["SUBSIDIADOMS"]);
                            }
                        }
                        else
                        {
                            dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                            if (dtPesq.Rows.Count > 0)
                            {
                                codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                            }
                        }

                        if (String.IsNullOrEmpty(txtComanda.Text))
                        {
                            lblCliEmp.Visible = true;
                            txtCodCliEmp.Visible = true;
                            txtCliEmp.Visible = true;
                        }
                        txtProduto.Focus();
                        #endregion
                    }
                    else if (vendaBeneficio.Equals("VIDALINK") && !String.IsNullOrEmpty(vendaNumeroNSU))
                    {
                        #region VIDALINK
                        var buscaDados = new BeneficioVidaLinkProdutos();
                        DataTable dtRetorno = buscaDados.BuscaProdutosPorNSU(vendaNumeroNSU);
                        for (int i = 0; i < dtRetorno.Rows.Count; i++)
                        {
                            dtPedido = DateTime.Now;
                            txtProduto.Text = dtRetorno.Rows[i]["PROD_CODIGO"].ToString();
                            txtQtde.Text = dtRetorno.Rows[i]["QTD"].ToString();
                            lerProdutos();

                            linha_resultado = tProdutos.Select("COD_BARRA = '" + dtRetorno.Rows[i]["PROD_CODIGO"].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", dtRetorno.Rows[i]["PR_VENDA"]);
                                linha_resultado[0]["PROD_QTDE"] = dtRetorno.Rows[i]["QTD"];
                                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                linha_resultado[0]["BENEFICIO"] = "S";
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                linha_resultado[0].EndEdit();
                                InserirProdCupom(tProdutos);
                            }
                        }

                        if (StatusCaixa())
                        {
                            dtRetorno = buscaDados.IdentificaSomatorioVenda(vendaNumeroNSU);
                            if (Convert.ToDouble(dtRetorno.Rows[0]["AVISTA"]) == 0 && Convert.ToDouble(dtRetorno.Rows[0]["RECEBER"]) > 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "25", true));
                                pagamentoTotalOuParcial = "T";
                                valorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["RECEBER"]);

                                object sumObject;
                                sumObject = tProdutos.Compute("Sum(PROD_TOTAL)", "");

                                if (Convert.ToDouble(sumObject) != valorAutorizado)
                                    valorAutorizado = Convert.ToDouble(sumObject);

                            }
                            else if (Convert.ToDouble(dtRetorno.Rows[0]["AVISTA"]) > 0 && Convert.ToDouble(dtRetorno.Rows[0]["RECEBER"]) > 0)
                            {
                                codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "25", true));
                                diferencaDoValorAutorizado = Convert.ToDouble(dtRetorno.Rows[0]["AVISTA"]);
                                pagamentoTotalOuParcial = "P";
                                valorAutorizado = Convert.ToDouble(lblSubTotal.Text) - diferencaDoValorAutorizado;
                            }
                        }
                        else
                        {
                            dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                            if (dtPesq.Rows.Count > 0)
                            {
                                codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                            }
                        }

                        if (String.IsNullOrEmpty(txtComanda.Text) || String.IsNullOrEmpty(txtCodCliEmp.Text))
                        {
                            lblCliEmp.Visible = true;
                            txtCodCliEmp.Visible = true;
                            txtCliEmp.Visible = true;
                            BuscaConsumidor();
                        }
                        #endregion
                    }
                    else if (vendaBeneficio.Equals("EPHARMA") && !String.IsNullOrEmpty(vendaNumeroNSU))
                    {
                        #region EPHARMA
                        EpharmaProdutos itens = new EpharmaProdutos();
                        DataTable dtItens = itens.BuscaItensPorNsu(Convert.ToInt32(vendaNumeroNSU), Convert.ToInt32(numTrans));
                        Double aReceber = 0;
                        Double totalBeneficio = 0;

                        for (int i = 0; i < dtItens.Rows.Count; i++)
                        {
                            dtPedido = DateTime.Now;
                            txtProduto.Text = dtItens.Rows[i]["PROD_CODIGO"].ToString();
                            txtQtde.Text = dtItens.Rows[i]["QTD"].ToString();
                            lerProdutos();

                            linha_resultado = tProdutos.Select("COD_BARRA = '" + dtItens.Rows[i]["PROD_CODIGO"].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", dtItens.Rows[i]["VL_MAXIMO"]);
                                linha_resultado[0]["PROD_QTDE"] = dtItens.Rows[i]["QTD"];
                                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtItens.Rows[i]["VL_MAXIMO"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                                linha_resultado[0]["BENEFICIO"] = "S";
                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["VL_MAXIMO"]) - Convert.ToDouble(dtItens.Rows[i]["VL_PFABRICA"])) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["VL_MAXIMO"]) / Convert.ToDouble(dtItens.Rows[i]["VL_PFABRICA"])) * 100);
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["VL_PFABRICA"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"])));
                                linha_resultado[0].EndEdit();
                                InserirProdCupom(tProdutos);

                                aReceber = aReceber + (Convert.ToDouble(dtItens.Rows[i]["VL_PFINAL"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                                totalBeneficio = totalBeneficio + (Convert.ToDouble(dtItens.Rows[i]["VL_REPASSE"]) * Convert.ToInt32(dtItens.Rows[i]["QTD"]));
                            }

                            if (StatusCaixa())
                            {
                                if (aReceber == 0)
                                {
                                    codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "18", true));
                                    pagamentoTotalOuParcial = "T";
                                    valorAutorizado = totalBeneficio;
                                }
                                else if (!totalBeneficio.Equals(0) && !aReceber.Equals(0))
                                {
                                    codFormaPagto = Convert.ToInt32(Funcoes.LeParametro(14, "18", true));
                                    diferencaDoValorAutorizado = aReceber;
                                    pagamentoTotalOuParcial = "P";
                                    valorAutorizado = totalBeneficio;
                                }
                                else
                                {
                                    diferencaDoValorAutorizado = aReceber;
                                }
                            }
                            else
                            {
                                dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                                if (dtPesq.Rows.Count > 0)
                                {
                                    codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                                }
                            }

                            if (String.IsNullOrEmpty(txtComanda.Text) || String.IsNullOrEmpty(txtCodCliEmp.Text))
                            {
                                lblCliEmp.Visible = true;
                                txtCodCliEmp.Visible = true;
                                txtCliEmp.Visible = true;
                                BuscaConsumidor();
                            }
                        }
                        #endregion
                    }
                    else if (vendaBeneficio.Equals("ORIZON") && !String.IsNullOrEmpty(vendaNumeroNSU))
                    {
                        #region ORIZON
                        OrizonProdutos vendaItens = new OrizonProdutos();
                        DataTable dtItens = vendaItens.BuscaItensPorAutorizacao(vendaNumeroNSU);
                        Double aReceber = 0;
                        Double totalBeneficio = 0;

                        for (int i = 0; i < dtItens.Rows.Count; i++)
                        {
                            dtPedido = DateTime.Now;
                            txtProduto.Text = dtItens.Rows[i]["COD_BARRAS"].ToString();
                            txtQtde.Text = dtItens.Rows[i]["QUATIDADE"].ToString();
                            lerProdutos();

                            linha_resultado = tProdutos.Select("COD_BARRA = '" + dtItens.Rows[i]["COD_BARRAS"].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", dtItens.Rows[i]["PRECO_CONSUMIDOR"]);
                                linha_resultado[0]["PROD_QTDE"] = dtItens.Rows[i]["QUATIDADE"];
                                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtItens.Rows[i]["PRECO_CONSUMIDOR"]) * Convert.ToInt32(dtItens.Rows[i]["QUATIDADE"]));
                                linha_resultado[0]["BENEFICIO"] = "S";
                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["PRECO_CONSUMIDOR"]) - Convert.ToDouble(dtItens.Rows[i]["PRECO_COM_DESCONTO"])) * Convert.ToInt32(dtItens.Rows[i]["QUATIDADE"]));
                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["PRECO_CONSUMIDOR"]) / Convert.ToDouble(dtItens.Rows[i]["PRECO_COM_DESCONTO"])) * 100);
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", (Convert.ToDouble(dtItens.Rows[i]["PRECO_COM_DESCONTO"]) * Convert.ToInt32(dtItens.Rows[i]["QUATIDADE"])));
                                linha_resultado[0].EndEdit();
                                InserirProdCupom(tProdutos);

                                aReceber = aReceber + (Convert.ToDouble(dtItens.Rows[i]["PRECO_COM_DESCONTO"]) * Convert.ToInt32(dtItens.Rows[i]["QUATIDADE"]));
                                totalBeneficio = totalBeneficio + (Convert.ToDouble(dtItens.Rows[i]["VALOR_REEMBOLSO"]) * Convert.ToInt32(dtItens.Rows[i]["QUATIDADE"]));
                            }
                            if (!StatusCaixa())
                            {
                                dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                                if (dtPesq.Rows.Count > 0)
                                {
                                    codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                                }
                            }

                            if (String.IsNullOrEmpty(txtComanda.Text) || String.IsNullOrEmpty(txtCodCliEmp.Text))
                            {
                                lblCliEmp.Visible = true;
                                txtCodCliEmp.Visible = true;
                                txtCliEmp.Visible = true;
                                BuscaConsumidor();
                            }
                        }
                        #endregion
                    }
                    else if (vendaBeneficio.Equals("PORTALDROGARIA") && !String.IsNullOrEmpty(vendaNumeroNSU))
                    {
                        #region PORTALDADROGARIA
                        var buscaDados = new BeneficioNovartisProdutos();
                        DataTable dtRetorno = buscaDados.BuscaProdutosPorNsu(vendaNumeroNSU);
                        for (int i = 0; i < dtRetorno.Rows.Count; i++)
                        {
                            dtPedido = DateTime.Now;
                            txtProduto.Text = dtRetorno.Rows[i]["PROD_CODIGO"].ToString();
                            txtQtde.Text = dtRetorno.Rows[i]["QTD"].ToString();
                            lerProdutos();

                            linha_resultado = tProdutos.Select("COD_BARRA = '" + dtRetorno.Rows[i]["PROD_CODIGO"].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", dtRetorno.Rows[i]["PRE_BRUTO"]);
                                linha_resultado[0]["PROD_QTDE"] = dtRetorno.Rows[i]["QTD"];
                                linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                linha_resultado[0]["BENEFICIO"] = "S";
                                double desconto = Math.Round(((Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * Math.Abs(Convert.ToDouble(dtRetorno.Rows[i]["VALOR_DESCONTO"]))) / 100, 2);
                                linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", desconto);
                                linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", dtRetorno.Rows[i]["VALOR_DESCONTO"]);
                                linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", (Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) - desconto);
                                linha_resultado[0].EndEdit();
                                InserirProdCupom(tProdutos);
                            }
                        }

                        if (!StatusCaixa())
                        {
                            dtPesq = Util.RegistrosPorEmpresa("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "COMANDA", true, true);
                            if (dtPesq.Rows.Count > 0)
                            {
                                codFormaPagto = Convert.ToInt32(dtPesq.Rows[0]["FORMA_ID"]);
                            }
                        }

                        if (String.IsNullOrEmpty(txtComanda.Text) || String.IsNullOrEmpty(txtCodCliEmp.Text))
                        {
                            lblCliEmp.Visible = true;
                            txtCodCliEmp.Visible = true;
                            txtCliEmp.Visible = true;
                            BuscaConsumidor();
                        }
                        #endregion
                    }

                    if (!String.IsNullOrEmpty(vendaBeneficio) || vendaBeneficio.Equals("FARMACIAPOPULAR"))
                    {
                        dtRetorno = dCliente.DadosClienteFiltro(IDCliente.ToString(), 2, out filtro);
                        if (dtRetorno.Rows.Count == 1)
                        {
                            codCliente = dtRetorno.Rows[0]["CF_CODIGO"].ToString() == "" ? "0" : dtRetorno.Rows[0]["CF_CODIGO"].ToString();
                            txtCodCliEmp.Text = codCliente;
                            txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                            doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                            IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                            statusCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_STATUS"]);
                            if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CON_CODIGO"].ToString()))
                            {
                                txtCodCliEmp.Text = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                                wsConID = Convert.ToInt32(dtRetorno.Rows[0]["CON_ID"]);
                                wsEmpresa = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                                wsConWeb = dtRetorno.Rows[0]["CON_WEB"].ToString();
                                wsConCodConv = dtRetorno.Rows[0]["CON_COD_CONV"].ToString();
                            }

                            txtCodCliEmp.Enabled = false;

                            if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString()))
                            {
                                MessageBox.Show(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString().ToUpper(), "Observação do Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            if (statusCliente.Equals(1) || statusCliente.Equals(3) || statusCliente.Equals(5))
                            {
                                MessageBox.Show("Cliente Bloqueado! Verifique o cadastro.", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                if (Funcoes.LeParametro(6, "190", false).Equals("S"))
                                {
                                    frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                    senhaSup.ShowDialog();
                                    if (senhaSup.sValida.Equals(false))
                                    {
                                        MessageBox.Show("Não é possível prosseguir a venda sem a senha de autorização!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        Limpar();
                                        return;
                                    }
                                }
                            }
                            DadosDaVendaCliente(Principal.doctoCliente);

                            if (!String.IsNullOrEmpty(txtAjuste.Text) && Convert.ToDouble(txtAjuste.Text) != 0)
                            {
                                if (Convert.ToDecimal(txtAjuste.Text.Trim()) < 0)
                                {
                                    txtAjuste.ForeColor = System.Drawing.Color.Red;
                                    txtAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtAjuste.Text.Trim()));
                                }
                                else
                                {
                                    txtAjuste.ForeColor = System.Drawing.Color.Red;
                                    txtAjuste.Text = String.Format("{0:N}", Convert.ToDecimal(txtAjuste.Text.Trim()) * -1);
                                }

                                txtAjuste.Focus();
                            }

                            txtProduto.Focus();
                        }
                    }
                    else
                    {
                        txtComanda.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário informar o Número da Comanda!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtComanda.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnPreAutorizar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnPreAutorizar, "Pré-Autorizar");
        }

        private void btnPreAutorizar_Click(object sender, EventArgs e)
        {
            if (vendaBeneficio.ToUpper() == "FUNCIONAL")
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    if (tProdutos.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Adicione ao menos um item ao pedido", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProduto.Focus();
                    }
                    else if (tProdutos.Rows.Count > 10)
                    {
                        MessageBox.Show("Dez(10) é o número máximo de produtos, exclua algum ítem da lista e passe em outra autorização", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProduto.Focus();
                    }
                    else
                    {
                        if (!chkEntrega.Checked)
                        {
                            if (MessageBox.Show("Venda de Entrega?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                chkEntrega.Checked = true;
                            }
                            else
                                chkEntrega.Checked = false;
                        }

                        #region PRE-AUTORIZACAO FUNCIONAL LAYOUT 7.2(SOLICITA PRE AUTORIZACAO)
                        FuncionalPreAutorizacaoNegocio7_2 objPreAutorizacaoFuncionalNegocio = new FuncionalPreAutorizacaoNegocio7_2();

                        //CARREGO UMA LISTA PRA AUXILIAR A GUARDAR PARAMETROS DOS PRODUTOS QUE ADICIONEI NO GRID
                        List<ProdutoFuncionalRequest> produtosFuncionalRequest = new List<ProdutoFuncionalRequest>();
                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            double precoDigitado = Convert.ToDouble(tProdutos.Rows[i]["PRE_VALOR"]);
                            string codBarras = Convert.ToString(tProdutos.Rows[i]["COD_BARRA"]);
                            //double desconto = Convert.ToDouble(tProdutos.Rows[i]["VLRDESC"]);
                            produtosFuncionalRequest.Add(new ProdutoFuncionalRequest()
                            {
                                CodBarras = codBarras,
                                PrecoDigitado = precoDigitado,
                                Desconto = 0.0
                                //Desconto = desconto
                            });
                        }

                        double sum = produtosFuncionalRequest.Sum(x => x.PrecoDigitado);

                        int numeroSequencialDoArquivo = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();

                        objPreAutorizacaoFuncionalNegocio.SolicitaPreAutorizacao(objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo, tProdutos);

                        #region Valida arquivo de status
                        if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo))
                        {
                            if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo))
                            {
                                MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                            return;
                        }
                        #endregion

                        #region Valida arquivo de resposta
                        if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo))
                        {
                            MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            return;
                        }
                        #endregion

                        #region Validaconsistencia de arquivos enviados ENV e RESP
                        if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo))
                        {
                            MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                            FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                            ClearForm(this);
                            return;
                        }

                        #endregion

                        #region Preenche objeto PreAutorizacaoResponse
                        string temReceitaMedica = "N";
                        temReceitaMedica = objTransacaoClienteCartaoFuncional.TipoDaVenda.ToString().Equals(Enums.TipoVenda.ComReceita.ToString()) ? "S" : "N";
                        objPreAutorizacaoResponse = objPreAutorizacaoFuncionalNegocio.PreAutorizacaoResponse(numeroSequencialDoArquivo, temReceitaMedica);
                        #endregion
                        #region APAGA ARQUIVO DE RESPOSTA
                        string arquivoDeReposta = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
                        FuncionalUtil.ApagarArquivoDeResposta(arquivoDeReposta);
                        #endregion

                        #region INCREMENTA AUTORIZADOR.SEQ
                        FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo);
                        #endregion

                        #endregion
                        //SE TODOS OS PRODUTOS FORAM AUTORIZADOS JA CONFIRMA VENDA LAYOUT 7.4 E CHAMA PROXIMO FORM
                        #region VENDA PRE AUTORIZADA LAYOUT 7.4(VENDA PRE AUTORIZADA)
                        if (objPreAutorizacaoResponse.CodigoDaResposta == "00")
                        {
                            #region confirma 7.4
                            FuncionalConfirmaVendaNegocio objFuncionalConfirmaVendaNegocio = new FuncionalConfirmaVendaNegocio();

                            int numeroSequencialDoArquivo_2 = FuncionalUtil.GeraSequenciaArquivoAutorizadorSeq();

                            objFuncionalConfirmaVendaNegocio.ConfirmarVendaEnv(objTransacaoClienteCartaoFuncional, numeroSequencialDoArquivo_2, objPreAutorizacaoResponse, tProdutos);

                            #region Valida arquivo de status
                            if (FuncionalUtil.ValidarArquivoStatus(numeroSequencialDoArquivo_2))
                            {
                                if (!FuncionalUtil.ApagarArquivoDeStatus(numeroSequencialDoArquivo_2))
                                {
                                    MessageBox.Show("Ocorreu um problema ao excluir o arquivo de status!" + "\n" + "Tente novamente ou entre em contato com o suporte");
                                    return;
                                }
                            }
                            else
                            {
                                MessageBox.Show("O arquivo de status não pode ser verificado!" + "\n" + "Verifique se o TVAT foi iniciado, tente novamente ou entre em contato com o suporte");
                                return;
                            }
                            #endregion

                            #region Valida arquivo de resposta
                            if (!FuncionalUtil.ValidarArquivoDeResposta(numeroSequencialDoArquivo_2))
                            {
                                MessageBox.Show("O arquivo de resposta não pode ser verificado!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                                return;
                            }
                            #endregion

                            #region Validaconsistencia de arquivos enviados ENV e RESP
                            if (!FuncionalUtil.ValidaConsistenciaArquivosEnviados(numeroSequencialDoArquivo_2))
                            {
                                MessageBox.Show("Os arquivos de envio e resposta não são consistentes!" + "\n" + "Tente novamente ou entre em contato com o suporte.");
                                FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo_2);
                                ClearForm(this);
                                return;
                            }

                            #endregion

                            #region Preenche objeto ConfirmacaooResponse
                            string temReceitaMedica_2 = "N";
                            temReceitaMedica = objTransacaoClienteCartaoFuncional.TipoDaVenda.ToString().Equals(Enums.TipoVenda.ComReceita.ToString()) ? "S" : "N";
                            objConfirmaVendaResponse = objFuncionalConfirmaVendaNegocio.ConfirmaVendaRsp(numeroSequencialDoArquivo_2, comReceitaFuncional);
                            #endregion

                            #region APAGA ARQUIVO DE RESPOSTA
                            string arquivoDeReposta_2 = @"" + Funcoes.LeParametro(14, "06", false) + numeroSequencialDoArquivo.ToString().PadLeft(8, '0') + ".rsp";
                            FuncionalUtil.ApagarArquivoDeResposta(arquivoDeReposta);
                            #endregion

                            #region INCREMENTA AUTORIZADOR.SEQ
                            FuncionalUtil.IncrementaSequenciaArquivoAutorizadorSeq(numeroSequencialDoArquivo_2);
                            #endregion
                            #endregion
                        }

                        if (objPreAutorizacaoResponse.CodigoDaResposta == "00" || objPreAutorizacaoResponse.CodigoDaResposta == "26")
                        {
                            //SE CONTIVER PRODUTOS QUE NÃO FOI AUTORIZADO
                            #region CHAMA FORM PARA CONFIRMAR PRE AUTORIZAÇÃO ENVIA ARQUIVO LAYOUT 7.8(MENSAGEM VENDA) OU PODE CONFIRMAR 7.4 TAMBÉM
                            //CARREGA PRODUTOS DO GRID
                            DataTable produtosAutorizacao = new DataTable();
                            produtosAutorizacao.Columns.Add("CodBarras", typeof(string));
                            produtosAutorizacao.Columns.Add("DescricaoMedicamento", typeof(string));
                            produtosAutorizacao.Columns.Add("PrecoMaximo", typeof(string));
                            produtosAutorizacao.Columns.Add("PrecoVenda", typeof(string));
                            produtosAutorizacao.Columns.Add("Desconto", typeof(string));
                            produtosAutorizacao.Columns.Add("QtdeAutorizado", typeof(string));
                            produtosAutorizacao.Columns.Add("Observacao", typeof(string));
                            produtosAutorizacao.Columns.Add("Status", typeof(string));
                            foreach (var item in objPreAutorizacaoResponse.ProdutosPreAutorizacao)
                            {
                                decimal desconto = 0.0M;
                                if ((Convert.ToDecimal(item.PrecoMaximo) / 100) != 0)
                                {
                                    desconto = 100 - (((Convert.ToDecimal(item.PrecoVenda) / 100) / (Convert.ToDecimal(item.PrecoMaximo) / 100)) * 100);
                                }

                                produtosAutorizacao.Rows.Add(new String[] { item.CodBarras, item.DescricaoMedicamento, item.PrecoMaximo, item.PrecoVenda, String.Format("{0:N}", desconto), item.QuantidadeAutorizada, "", "Autorizado" });
                            }
                            frmFuncionalConfirma objFormFuncionalConfirma = new frmFuncionalConfirma(this, tProdutos, objConfirmaVendaResponse);
                            objFormFuncionalConfirma.StartPosition = FormStartPosition.CenterScreen;
                            objFormFuncionalConfirma.ShowDialog();
                            #endregion

                            //APÓS FECHAMENTO EXECUTA AQUI
                            //COMPARA PREÇOS AUTORIZADOS COM OS PREÇOS DIGITADOS
                            if (!descartaAutorizacaoFuncional && tProdutos.Rows.Count > 0)
                            {
                                #region Compara Preços Autorizados com preços digitados
                                foreach (var item in objConfirmaVendaResponse.ProdutosPreAutorizacao)
                                {
                                    if (!(item.StatusItem == "00"))
                                    {
                                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                                        {
                                            DataRow dr = tProdutos.Rows[i];
                                            if (dr["COD_BARRA"].ToString().PadLeft(13, '0') == item.CodBarras.Substring(1))
                                            {
                                                dr.Delete();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DataRow row = tProdutos.Select("COD_BARRA = " + item.CodBarras.ToString()).First();

                                        if (row.Table.Rows.Count == 0)
                                        {
                                            row = tProdutos.Select("COD_BARRA like '%" + Convert.ToInt64(item.CodBarras) + "'").First();
                                        }

                                        if (row.Table.Rows.Count > 0)
                                        {
                                            double precoDigitado = Convert.ToDouble(row["PRE_VALOR"].ToString());
                                            double precoAutorizado = (Convert.ToDouble(item.PrecoVenda) / 100);

                                            if (precoDigitado > precoAutorizado)
                                            {
                                                foreach (DataRow dr in tProdutos.Rows)
                                                {
                                                    string cod1 = item.CodBarras.Substring(1);
                                                    string cod2 = dr["COD_BARRA"].ToString();
                                                    if (dr["COD_BARRA"].ToString().Equals(item.CodBarras.Substring(1)))
                                                    {
                                                        dr["PRE_VALOR"] = (Convert.ToDouble(item.PrecoVenda) / 100);
                                                        dr["PROD_VUNI"] = (Convert.ToDouble(item.PrecoVenda) / 100);
                                                        dr["PROD_QTDE"] = Convert.ToDouble(item.QuantidadeAutorizada);
                                                        decimal valor = ((Convert.ToDecimal(item.PrecoVenda) / 100) * Convert.ToDecimal(item.QuantidadeAutorizada));
                                                        dr["PROD_SUBTOTAL"] = ((Convert.ToDecimal(item.PrecoVenda) / 100) * Convert.ToDecimal(item.QuantidadeAutorizada));
                                                        dr["PROD_TOTAL"] = ((Convert.ToDecimal(item.PrecoVenda) / 100) * Convert.ToDecimal(item.QuantidadeAutorizada));
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Produto " + item.CodBarras + " com erro de validação", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                    }
                                }

                                if (tProdutos.Rows.Count > 0)
                                {
                                    preAutorizouFuncional = true;
                                    InserirProdCupom(tProdutos);
                                }
                                else
                                {
                                    LimparFuncional();
                                }
                                #endregion
                            }
                            else
                            {
                                #region Descartar a venda funcional
                                preAutorizouFuncional = false;
                                tProdutos.Clear();
                                listCupom.Items.Clear();
                                txtQtde.Text = "1";
                                txtAjuste.Text = "0,00";
                                txtGAjuste.Text = "0,00";
                                lblSubTotal.Text = "0,00";
                                lblTotal.Text = "0,00";
                                lblDesconto.Text = "0,00";
                                codFormaPagto = 0;
                                vendaID = 0;
                                descartaAutorizacaoFuncional = false;
                                InserirCabecalho();
                                #endregion
                            }
                        }
                        else
                        {
                            MessageBox.Show("Erro " + objPreAutorizacaoResponse.CodigoDaResposta + ": " + objPreAutorizacaoResponse.TextoParaOperador, "Funcional", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                        #endregion
                    }
                    Cursor.Current = Cursors.Default;

                }
                catch (Exception ex)
                {

                    MessageBox.Show("Erro: " + ex.Message, "Erro Pré-Autorizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                #region PRE-AUTORIZACAO DROGABELLA/PLANTAOCARD
                try
                {
                    if (!chkEntrega.Checked)
                    {
                        if (MessageBox.Show("Venda de Entrega?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            chkEntrega.Checked = true;
                        }
                        else
                            chkEntrega.Checked = false;
                    }
                    wsQtdeProdutosRetorno = 0;
                    frmVenRetornoWS venRetornoWS = new frmVenRetornoWS();

                    //VERIFICA SE DATATABLE PRODUTOS ESTA VAZIO//
                    if (tProdutos.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Adicione ao menos um item ao pedido", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProduto.Focus();
                    }
                    else
                    {
                        //ABRE TRANSACAO//
                        Cursor = Cursors.WaitCursor;
                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);

                        //VERIFICA SE HOUVE RETORNO DO XML//
                        if (retornoXml != null)
                        {
                            codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                            if (codErro == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                if (tipoVenda == 3)
                                    dadosDaVenda.VendaTransIDReceita = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                else
                                    dadosDaVenda.VendaTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;

                                retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosPreAutorizacao(wsCartao, wsTransID, tProdutos, wsRetornoProdutos);

                                if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                                {
                                    MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    #region Verifica retorno do WebService
                                    quantProd = Convert.ToInt16(retornoXml.GetElementsByTagName("PRODUTO").Count);

                                    for (int i = 0; i < quantProd; i++)
                                    {
                                        wsRetornoProdutos[i, 1] = retornoXml.GetElementsByTagName("PRODUTO")[i]["CODBARRAS"].InnerText;
                                        wsRetornoProdutos[i, 2] = retornoXml.GetElementsByTagName("PRODUTO")[i]["STATUSPROD"].InnerText;
                                        wsRetornoProdutos[i, 3] = retornoXml.GetElementsByTagName("PRODUTO")[i]["QTDE"].InnerText;
                                        wsRetornoProdutos[i, 11] = tProdutos.Rows[i]["PROD_ID"].ToString();

                                        //CASO TENHA DESCONTO DA LOJA//
                                        if (wsRetornoProdutos[i, 2] == "3")
                                        {
                                            wsRetornoProdutos[i, 4] = tProdutos.Rows[i]["PROD_VUNI"].ToString();
                                            wsRetornoProdutos[i, 5] = tProdutos.Rows[i]["PROD_SUBTOTAL"].ToString();
                                            wsRetornoProdutos[i, 6] = tProdutos.Rows[i]["PROD_DPORC"].ToString();
                                            wsRetornoProdutos[i, 7] = tProdutos.Rows[i]["PROD_DVAL"].ToString();
                                            wsRetornoProdutos[i, 8] = tProdutos.Rows[i]["PROD_TOTAL"].ToString();
                                        }
                                        else
                                        {
                                            wsRetornoProdutos[i, 4] = String.Format("{0:N}", Convert.ToDouble(retornoXml.GetElementsByTagName("PRODUTO")[i]["PRCUNIT"].InnerText) / 100);
                                            wsRetornoProdutos[i, 5] = String.Format("{0:N}", Convert.ToDouble(retornoXml.GetElementsByTagName("PRODUTO")[i]["VLRBRU"].InnerText) / 100);
                                            wsRetornoProdutos[i, 6] = String.Format("{0:N}", Convert.ToDouble(retornoXml.GetElementsByTagName("PRODUTO")[i]["PERCDESC"].InnerText) / 100);
                                            wsRetornoProdutos[i, 7] = String.Format("{0:N}", Convert.ToDouble(retornoXml.GetElementsByTagName("PRODUTO")[i]["VLRDESC"].InnerText) / 100);
                                            wsRetornoProdutos[i, 8] = String.Format("{0:N}", Convert.ToDouble(retornoXml.GetElementsByTagName("PRODUTO")[i]["VLRLIQ"].InnerText) / 100);

                                        }

                                        if (retornoXml.GetElementsByTagName("PRODUTO")[i]["OBRIGRECEITA"].InnerText.Equals("false"))
                                        {
                                            wsRetornoProdutos[i, 9] = "Não";
                                        }
                                        else
                                            wsRetornoProdutos[i, 9] = "Sim";

                                        if (wsRetornoProdutos[i, 2] == "2")
                                        {
                                            venRetornoWS.lblTitulo.Text = "PRE-AUTORIZAÇÃO";
                                            venRetornoWS.lblSubTitulo.Text = "Não foram autorizados todos os produtos. Confira abaixo os motivos das inconsistências.";
                                        }

                                        if (wsRetornoProdutos[i, 2] == "0")
                                        {
                                            retorno = "Aplicado desconto por grupo de produtos.";
                                        }
                                        else
                                            if (wsRetornoProdutos[i, 2] == "1")
                                        {
                                            retorno = "Sem desconto na administradora.";
                                        }
                                        else
                                                if (wsRetornoProdutos[i, 2] == "2")
                                        {
                                            retorno = "Produto bloqueado para venda no convênio.";
                                        }
                                        else
                                                    if (wsRetornoProdutos[i, 2] == "3")
                                        {
                                            retorno = "Desconto da loja mantido.";
                                        }

                                        venRetornoWS.dgProd.Rows.Insert(venRetornoWS.dgProd.RowCount, new Object[] { wsRetornoProdutos[i, 11], wsRetornoProdutos[i, 1],
                                        wsRetornoProdutos[i, 0], wsRetornoProdutos[i, 10], wsRetornoProdutos[i, 3], wsRetornoProdutos[i, 4],
                                        wsRetornoProdutos[i, 6], wsRetornoProdutos[i, 7], wsRetornoProdutos[i, 8],wsRetornoProdutos[i, 9], retorno, wsRetornoProdutos[i, 2]});

                                    }
                                    #endregion

                                    venRetornoWS.ShowDialog();

                                    #region Obtem retorno da tela de pre-autorizacao
                                    if (Principal.sRetorno.Equals("0"))//Descartar a Venda
                                    {
                                        Limpar();
                                    }
                                    else if (Principal.sRetorno.Equals("1")) //Aceitar e continuar a Venda
                                    {
                                        Array.Clear(prodAut, 0, 375);
                                        int contador = 0;
                                        for (int i = 0; i < venRetornoWS.dgProd.RowCount; i++)
                                        {
                                            if (!venRetornoWS.dgProd.Rows[i].Cells[11].Value.Equals("2"))
                                            {
                                                prodAut[contador, 0] = venRetornoWS.dgProd.Rows[i].Cells[0].Value.ToString();
                                                prodAut[contador, 1] = Convert.ToInt64(venRetornoWS.dgProd.Rows[i].Cells[1].Value).ToString();
                                                prodAut[contador, 2] = venRetornoWS.dgProd.Rows[i].Cells[2].Value.ToString();
                                                prodAut[contador, 4] = venRetornoWS.dgProd.Rows[i].Cells[5].Value.ToString();
                                                prodAut[contador, 5] = venRetornoWS.dgProd.Rows[i].Cells[4].Value.ToString();
                                                prodAut[contador, 6] = String.Format("{0:N}", Convert.ToDouble(venRetornoWS.dgProd.Rows[i].Cells[5].Value) * Convert.ToInt32(venRetornoWS.dgProd.Rows[i].Cells[4].Value));
                                                prodAut[contador, 7] = venRetornoWS.dgProd.Rows[i].Cells[7].Value.ToString();
                                                prodAut[contador, 8] = venRetornoWS.dgProd.Rows[i].Cells[6].Value.ToString();
                                                prodAut[contador, 9] = venRetornoWS.dgProd.Rows[i].Cells[8].Value.ToString();

                                                linha_resultado = tProdutos.Select("COD_BARRA = '" + Convert.ToInt64(venRetornoWS.dgProd.Rows[i].Cells[1].Value) + "'");

                                                if (linha_resultado.Length == 0)
                                                {
                                                    linha_resultado = tProdutos.Select("COD_BARRA = '" + venRetornoWS.dgProd.Rows[i].Cells[1].Value + "'");
                                                }

                                                if (linha_resultado.Length == 0)
                                                {
                                                    linha_resultado = tProdutos.Select("COD_BARRA like '%" + Convert.ToInt64(venRetornoWS.dgProd.Rows[i].Cells[1].Value) + "'");
                                                }

                                                if (linha_resultado.Length != 0)
                                                {
                                                    prodAut[contador, 3] = linha_resultado[0]["PROD_UNIDADE"].ToString();
                                                    prodAut[contador, 10] = linha_resultado[0]["PROD_CONTROLADO"].ToString();
                                                    prodAut[contador, 11] = linha_resultado[0]["PROD_LOTE"].ToString();
                                                    prodAut[contador, 12] = linha_resultado[0]["PROD_DB"].ToString();
                                                    prodAut[contador, 13] = linha_resultado[0]["PROD_PROMO"].ToString();
                                                    prodAut[contador, 14] = linha_resultado[0]["PROD_REC"].ToString();
                                                    prodAut[contador, 15] = linha_resultado[0]["CLAS_FISC"].ToString();
                                                    prodAut[contador, 16] = linha_resultado[0]["ECF"].ToString();
                                                    prodAut[contador, 17] = linha_resultado[0]["VENDEDOR"].ToString();
                                                    prodAut[contador, 18] = "S";
                                                    prodAut[contador, 19] = linha_resultado[0]["DESC_MIN"].ToString();
                                                    prodAut[contador, 20] = linha_resultado[0]["DESC_MAX"].ToString();
                                                    prodAut[contador, 21] = linha_resultado[0]["DESC_LIB"].ToString();
                                                    prodAut[contador, 22] = linha_resultado[0]["PROD_ESTATUAL"].ToString();
                                                    prodAut[contador, 23] = linha_resultado[0]["PROD_ULTCUSME"].ToString();
                                                    prodAut[contador, 24] = linha_resultado[0]["NCM"].ToString();
                                                    prodAut[contador, 25] = linha_resultado[0]["PRE_VALOR"].ToString();
                                                    prodAut[contador, 26] = linha_resultado[0]["PROD_CUSME"].ToString();
                                                    prodAut[contador, 27] = linha_resultado[0]["ALIQUOTA"].ToString();
                                                    prodAut[contador, 28] = linha_resultado[0]["PRECO_MAXIMO"].ToString();

                                                    wsQtdeProdutosRetorno += Convert.ToInt32(venRetornoWS.dgProd.Rows[i].Cells[4].Value);
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Produto " + venRetornoWS.dgProd.Rows[i].Cells[1].Value + " com erro na validação do código de barras", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                }
                                                contador += 1;
                                            }
                                        }

                                        //Insere no datatable os produtos autorizados//
                                        InserirProdutosAuto(prodAut, tProdutos, contador);
                                        //Atualiza as informações do cupom//
                                        InserirProdCupom(tProdutos);

                                        preAutorizou = true;

                                        txtProduto.Focus();
                                    }
                                    else if (Principal.sRetorno.Equals("2"))//Alterar os dados da Venda
                                    {
                                        txtProduto.Focus();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message, "Erro Pré-Autorizar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
                #endregion
            }
        }


        public void LimparFuncional()
        {
            Funcoes.LimpaFormularios(this);
            //CamposInvisiveis();

            codCliente = "0";
            retorno = "";
            listCupom.Items.Clear();

            txtVendedor.Enabled = true;
            txtCodCliEmp.Enabled = true;

            txtCodCliEmp.Text = String.Empty;
            txtCliEmp.Text = String.Empty;

            //VARIAVEIS GLOBAIS//
            wsCartao = String.Empty;
            wsPreAutoriza = String.Empty;
            wsEmpresa = String.Empty;
            tipoVenda = 0;
            checaMedComSemReceita = false;
            tProdutos.Rows.Clear();
            preAutorizou = false;
            wsCRM = String.Empty;
            wsUF = String.Empty;
            wsMNome = String.Empty;
            wsData = DateTime.Now;
            wsNumReceita = String.Empty;
            wsConselho = String.Empty;
            vendaBeneficio = "N";
            wsAbreOutraTransacao = false;
            wsNomeCartao = String.Empty;
            wsComSemRec = "N";
            wsConWeb = "0";
            wsSaldoCartao = 0;
            wsQtdeProdutosRetorno = 0;
            totalVendaComReceita = 0;
            totalVendaSemReceita = 0;
            vendaDeComanda = false;
            comprovanteVenda = "";
            selForma = false;
            selEspecie = false;
            descontoCaixa = true;
            permtVenda = true;
            venOrcamento = false;
            vendaNumeroCaixa = "";
            wsConID = 0;
            wsEmpresa = "0";
            wsConWeb = "0";
            wsConCodConv = "0";
            vendaNumeroNota = "";
            wsProdutosCupomReceita = "";
            wsProdutosCupomSemReceita = "";
            wsDadosMedico = "";
            diferencaDoValorAutorizado = 0;
            valorAutorizado = 0;
            totalVendaConvenio = 0;
            entregaIsenta = false;
            txtGAjuste.ForeColor = System.Drawing.Color.Black;
            txtAjuste.ForeColor = System.Drawing.Color.Black;

            txtQtde.Text = "1";
            txtAjuste.Text = "0,00";
            txtGAjuste.Text = "0,00";
            lblSubTotal.Text = "0,00";
            lblTotal.Text = "0,00";
            lblDesconto.Text = "0,00";
            codFormaPagto = 0;
            vendaID = 0;

            vendaEspecie.Clear();
            venItens.Clear();

            List<AFCaixa> cStatus = cxStatus.DataCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);
            if (cStatus.Count.Equals(0))
            {
                permtVenda = false;
            }
            else if (DateTime.Now.ToString("dd/MM/yyyy") != cStatus[0].DataAbertura.ToString("dd/MM/yyyy"))
            {
                permtVenda = false;
            }

            vendaPedFormaPagto.Clear();
            produtosAlterados.Clear();
            vendaItens.Clear();
            vendaEspecie.Clear();
            venItens.Clear();
            inserir = true;
            transAberta = false;

            txtVendedor.Focus();

        }

        public static void ClearForm(System.Windows.Forms.Control parent)
        {
            foreach (System.Windows.Forms.Control ctrControl in parent.Controls)
            {
                //Loop through all controls
                if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.TextBox)))
                {
                    //Check to see if it's a textbox
                    ((System.Windows.Forms.TextBox)ctrControl).Text = string.Empty;
                    //If it is then set the text to String.Empty (empty textbox)
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.RichTextBox)))
                {
                    //If its a RichTextBox clear the text
                    ((System.Windows.Forms.RichTextBox)ctrControl).Text = string.Empty;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.ComboBox)))
                {
                    //Next check if it's a dropdown list
                    ((System.Windows.Forms.ComboBox)ctrControl).SelectedIndex = -1;
                    //If it is then set its SelectedIndex to 0
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.CheckBox)))
                {
                    //Next uncheck all checkboxes
                    ((System.Windows.Forms.CheckBox)ctrControl).Checked = false;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(System.Windows.Forms.RadioButton)))
                {
                    //Unselect all RadioButtons
                    ((System.Windows.Forms.RadioButton)ctrControl).Checked = false;
                }
                if (ctrControl.Controls.Count > 0)
                {
                    //Call itself to get all other controls in other containers
                    ClearForm(ctrControl);
                }
            }
        }

        public void InserirProdutosAuto(string[,] dadosProd, DataTable tProdutos, int linhas)
        {
            #region INSERE PRODUTOS NO DATATABLE
            try
            {
                tProdutos.Rows.Clear();

                for (int i = 0; i < linhas; i++)
                {
                    DataRow row = tProdutos.NewRow();
                    row["PROD_ID"] = dadosProd[i, 0];
                    row["COD_BARRA"] = dadosProd[i, 1];
                    row["PROD_DESCR"] = dadosProd[i, 2];
                    row["PROD_UNIDADE"] = dadosProd[i, 3];
                    row["PROD_VUNI"] = dadosProd[i, 4];
                    row["PROD_QTDE"] = dadosProd[i, 5];
                    row["PROD_SUBTOTAL"] = dadosProd[i, 6];
                    row["PROD_DVAL"] = dadosProd[i, 7];
                    row["PROD_DPORC"] = dadosProd[i, 8];
                    row["PROD_TOTAL"] = dadosProd[i, 9];
                    row["PROD_CONTROLADO"] = dadosProd[i, 10];
                    row["PROD_LOTE"] = dadosProd[i, 11];
                    row["PROD_DB"] = dadosProd[i, 12];
                    row["PROD_PROMO"] = dadosProd[i, 13];
                    row["PROD_REC"] = dadosProd[i, 14];
                    row["CLAS_FISC"] = dadosProd[i, 15];
                    row["ECF"] = dadosProd[i, 16];
                    row["VENDEDOR"] = dadosProd[i, 17];
                    row["BENEFICIO"] = dadosProd[i, 18];
                    row["DESC_MIN"] = dadosProd[i, 19];
                    row["DESC_MAX"] = dadosProd[i, 20];
                    row["DESC_LIB"] = dadosProd[i, 21];
                    row["PROD_ESTATUAL"] = dadosProd[i, 22];
                    row["PROD_ULTCUSME"] = dadosProd[i, 23];
                    row["NCM"] = dadosProdutos[24];
                    row["PRE_VALOR"] = dadosProdutos[25];
                    row["PROD_CUSME"] = dadosProdutos[26];
                    row["ALIQUOTA"] = dadosProdutos[27];
                    row["PRECO_MAXIMO"] = (dadosProdutos[28] == null ? "0" : dadosProdutos[28]);
                    tProdutos.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inserir Produtos Autorizados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }

        private void btnPreAutorizar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public bool VendaBeneficioDrogabellaPlantao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                dadosDaVenda.VendaCartao = wsNumCartao;
                dadosDaVenda.VendaNomeCartao = wsNomeCartao;
                dadosDaVenda.VendaConCodigo = wsEmpresa;
                dadosDaVenda.VendaCartaoCodigo = wsCartao;

                if (chkEntrega.Checked)
                {
                    DateTime Data = DateTime.Now;
                    //OBTEM A SENHA DE ENTREGA//
                    string ObtemSenha = Funcoes.FormataZeroAEsquerda(Convert.ToString((Convert.ToInt16(Data.Hour) - 1)), 2)
                        + Funcoes.FormataZeroAEsquerda(Convert.ToString(Data.Day), 2) + Funcoes.FormataZeroAEsquerda(Convert.ToString(Data.Minute), 2);

                    senhaConveniado = ObtemSenha;
                }
                else
                {
                    frmVenSenhaBeneficio senhaCartao = new frmVenSenhaBeneficio(this);
                    senhaCartao.ShowDialog();

                    if (String.IsNullOrEmpty(senhaConveniado))
                    {
                        MessageBox.Show("Senha não pode ser em branco!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (checaMedComSemReceita)
                {
                    wsAbreOutraTransacao = true;
                    totalVendaComReceita = 0;
                    totalVendaSemReceita = 0;

                    for (int i = 0; i < tProdutos.Rows.Count; i++)
                    {
                        if (tProdutos.Rows[i]["PROD_REC"].ToString() == "S")
                        {
                            totalVendaComReceita = totalVendaComReceita + ((Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"])) * 100);
                        }
                        else
                        {
                            totalVendaSemReceita = totalVendaSemReceita + ((Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"])) * 100);
                        }
                    }

                    #region VENDA SEM RECEITA
                    if (wsPreAutoriza.Equals("N"))
                    {
                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);
                        codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                        if (codErro == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                            dadosDaVenda.VendaTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                        }

                    }

                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosSemReceita(wsCartao, wsTransID, tProdutos, out wsProdutosCupomSemReceita);

                    if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoSemReceita(wsCartao, wsTransID, totalVendaSemReceita,
                            wsSenhaPadrao == "S" ? wsNumCartao.Substring(12) : senhaConveniado, wsSenhaPadrao == "S" ? novaSenhaConveniado : "", dadosDaVenda.VendaConvenioParcela);

                        if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    #endregion

                    #region VENDA COM RECEITA
                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.LerCartao(wsNumCartao);

                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                    if (codErro == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        wsCartao = retornoXml.GetElementsByTagName("CARTAO_NUMERO").Item(0).InnerText
                        + retornoXml.GetElementsByTagName("CARTAO_DIGITO").Item(0).InnerText;

                        if ((wsPreAutoriza == "N") || (wsAbreOutraTransacao == true))
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);

                            codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                            if (codErro == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                            else
                            {
                                wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                dadosDaVenda.VendaTransIDReceita = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                            }
                        }

                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosComReceita(wsCartao, wsTransID, tProdutos, out wsProdutosCupomReceita);

                        if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoComReceita(wsCartao, wsTransID, tProdutos, wsConselho,
                                wsUF, wsCRM, wsNumReceita, wsData, totalVendaComReceita, wsSenhaPadrao == "S" ? novaSenhaConveniado : senhaConveniado, out wsDadosMedico, wsSenhaPadrao == "S" ? senhaConveniado : "", dadosDaVenda.VendaConvenioParcela);

                            if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    totalVendaComReceita = 0;
                    totalVendaSemReceita = 0;

                    for (int i = 0; i < tProdutos.Rows.Count; i++)
                    {
                        if (tProdutos.Rows[i]["PROD_REC"].ToString() == "S")
                        {
                            totalVendaComReceita = totalVendaComReceita + ((Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"])) * 100);
                        }
                        else
                        {
                            totalVendaSemReceita = totalVendaSemReceita + ((Convert.ToDouble(tProdutos.Rows[i]["PROD_TOTAL"])) * 100);
                        }
                    }

                    if (tipoVenda == 2)
                    {
                        #region VENDA SEM RECEITA
                        if (wsPreAutoriza.Equals("N"))
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);
                            codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                            if (codErro == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                            else
                            {
                                wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                dadosDaVenda.VendaTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                            }
                        }

                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosSemReceita(wsCartao, wsTransID, tProdutos, out wsProdutosCupomSemReceita);

                        if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoSemReceita(wsCartao, wsTransID, totalVendaSemReceita,
                                wsSenhaPadrao == "S" ? wsNumCartao.Substring(12) : senhaConveniado, wsSenhaPadrao == "S" ? novaSenhaConveniado : "", dadosDaVenda.VendaConvenioParcela);

                            if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region VENDA COM RECEITA
                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.LerCartao(wsNumCartao);

                        codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                        if (codErro == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            wsCartao = retornoXml.GetElementsByTagName("CARTAO_NUMERO").Item(0).InnerText
                            + retornoXml.GetElementsByTagName("CARTAO_DIGITO").Item(0).InnerText;

                            if ((wsPreAutoriza == "N") || (wsAbreOutraTransacao == true))
                            {
                                retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);

                                codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                                if (codErro == "1")
                                {
                                    MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                                else
                                {
                                    wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                    dadosDaVenda.VendaTransIDReceita = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                                }
                            }

                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosComReceita(wsCartao, wsTransID, tProdutos, out wsProdutosCupomReceita);

                            if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                            else
                            {
                                retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoComReceita(wsCartao, wsTransID, tProdutos, wsConselho,
                                    wsUF, wsCRM, wsNumReceita, wsData, totalVendaComReceita, wsSenhaPadrao == "S" ? novaSenhaConveniado : senhaConveniado, out wsDadosMedico, wsSenhaPadrao == "S" ? senhaConveniado : "", dadosDaVenda.VendaConvenioParcela);

                                if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                                {
                                    MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                        }
                        #endregion
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Drogabella/Plantao", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool FinalizaBeneficioDrogabellaPlantao()
        {
            #region FINALIZA VENDA CONVENIO
            try
            {
                Cursor = Cursors.WaitCursor;
                Principal.wsConexao = Funcoes.LeParametro(14, "01", true);

                retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ConfirmarTransacao(dadosDaVenda.VendaCartaoCodigo, tipoVenda == 3 ? dadosDaVenda.VendaTransIDReceita : dadosDaVenda.VendaTransID);
                codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                if (codErro == "1")
                {
                    MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Confirmar Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ConsultarTransacao(tipoVenda == 3 ? dadosDaVenda.VendaTransIDReceita : dadosDaVenda.VendaTransID);
                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                    if (codErro == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Confirmar Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        if (tipoVenda.Equals(3))
                        {
                            dadosDaVenda.VendaAutorizacaoReceita = retornoXml.GetElementsByTagName("AUTORIZACAO_NUM").Item(0).InnerText;
                        }
                        else
                            dadosDaVenda.VendaAutorizacao = retornoXml.GetElementsByTagName("AUTORIZACAO_NUM").Item(0).InnerText;
                    }
                }

                if (checaMedComSemReceita)
                {
                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ConfirmarTransacao(dadosDaVenda.VendaCartaoCodigo, dadosDaVenda.VendaTransIDReceita);
                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                    if (codErro == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Confirmar Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ConsultarTransacao(dadosDaVenda.VendaTransIDReceita);
                        codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                        if (codErro == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Confirmar Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                            dadosDaVenda.VendaAutorizacaoReceita = retornoXml.GetElementsByTagName("AUTORIZACAO_NUM").Item(0).InnerText;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Drogabella/Plantao", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            #endregion
        }

        public bool VendaBeneficioDrogabellaPlantaoParcial()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                dadosDaVenda.VendaCartao = wsNumCartao;
                dadosDaVenda.VendaNomeCartao = wsNomeCartao;
                dadosDaVenda.VendaConCodigo = wsEmpresa;
                dadosDaVenda.VendaCartaoCodigo = wsCartao;

                if (chkEntrega.Checked)
                {
                    DateTime Data = DateTime.Now;
                    //OBTEM A SENHA DE ENTREGA//
                    string ObtemSenha = Funcoes.FormataZeroAEsquerda(Convert.ToString((Convert.ToInt16(Data.Hour) - 1)), 2)
                        + Funcoes.FormataZeroAEsquerda(Convert.ToString(Data.Day), 2) + Funcoes.FormataZeroAEsquerda(Convert.ToString(Data.Minute), 2);

                    senhaConveniado = ObtemSenha;
                }
                else
                {
                    frmVenSenhaBeneficio senhaCartao = new frmVenSenhaBeneficio(this);
                    senhaCartao.ShowDialog();

                    if (String.IsNullOrEmpty(senhaConveniado))
                    {
                        MessageBox.Show("Senha não pode ser em branco!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                totalVendaConvenio = 0;

                for (int i = 0; i < produtosAlterados.Count; i++)
                {
                    totalVendaConvenio = totalVendaConvenio + (produtosAlterados[i].ProdTotal * 100);
                }

                if (tipoVenda == 2)
                {
                    #region VENDA SEM RECEITA

                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);
                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                    if (codErro == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                        dadosDaVenda.VendaTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;

                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosParcial(wsCartao, wsTransID, produtosAlterados, out wsProdutosCupomSemReceita);

                        if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoSemReceita(wsCartao, wsTransID, totalVendaConvenio,
                                wsSenhaPadrao == "S" ? wsNumCartao.Substring(12) : senhaConveniado, wsSenhaPadrao == "S" ? novaSenhaConveniado : "");

                            if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    #region VENDA COM RECEITA
                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.LerCartao(wsNumCartao);

                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                    if (codErro == "1")
                    {
                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        wsCartao = retornoXml.GetElementsByTagName("CARTAO_NUMERO").Item(0).InnerText
                        + retornoXml.GetElementsByTagName("CARTAO_DIGITO").Item(0).InnerText;

                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.AbrirTransacao(wsCartao, wsEmpresa, chkEntrega.Checked);

                        codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;

                        if (codErro == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Abrir Transação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            wsTransID = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                            dadosDaVenda.VendaTransIDReceita = retornoXml.GetElementsByTagName("TRANSID").Item(0).InnerText;
                        }

                        retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.ValidarProdutosParcial(wsCartao, wsTransID, produtosAlterados, out wsProdutosCupomReceita);

                        if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                        {
                            MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        else
                        {
                            retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.FecharTransacaoComReceitaParcial(wsCartao, wsTransID, produtosAlterados, wsConselho,
                                wsUF, wsCRM, wsNumReceita, wsData, totalVendaConvenio, wsSenhaPadrao == "S" ? novaSenhaConveniado : senhaConveniado, out wsDadosMedico, wsSenhaPadrao == "S" ? senhaConveniado : "");

                            if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText == "1")
                            {
                                MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro Validar Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Drogabella/Plantao", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnReimpressao_Click(object sender, EventArgs e)
        {
            try
            {
                frmVenReimpressaoComp venReimpComp = new frmVenReimpressaoComp();
                venReimpComp.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool SATCupomFiscal()
        {
            try
            {
                if (!Sat.VerificaModeloSAT())
                {
                    return false;
                }

                var colaborador = new Colaborador();
                string arquivoSATResposta, mes;
                mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();

                ChecaPastaXML(mes);

                Principal.NumCpfCnpj = "";
                Principal.NomeNP = "";
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    if (!dadosVendaCliente.Count.Equals(0))
                    {
                        if (dadosVendaCliente[0].CfTipoDocto.Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dadosVendaCliente[0].CfDocto;
                            notaPaulista.txtNome.Text = dadosVendaCliente[0].CfNome;
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dadosVendaCliente[0].CfDocto;
                            notaPaulista.txtNome.Text = dadosVendaCliente[0].CfNome;
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }

                Cursor = Cursors.WaitCursor;

                ExibirMensagem("Trasmitindo SAT Fiscal. Aguarde...");

                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (MontarXmlSAT(Principal.NumCpfCnpj, Principal.NomeNP).Equals(false))
                        return false;

                    retorno = Sat.SatEnviarDadosDaVenda(xmlSAT);

                    FileStream fs;
                    StreamWriter sw;

                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCFe))
                    {
                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesComErro\" + mes.ToUpper() + @"\" + dadosDaVenda.VendaID + ".xml", FileMode.Create);
                        sw = new StreamWriter(fs);
                        sw.WriteLine(xmlSAT);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                        return false;
                    }

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    //CONVERTE BASE64 PARA STRING//
                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    ExibirMensagem("Imprimindo Comprovante. Aguarde...");

                    string mensagemPMC = "";

                    if (Funcoes.LeParametro(6, "378", false).Equals("S"))
                    {
                        double somaTotal = 0;
                        for (int i = 0; i < vendaItens.Count; i++)
                        {
                            double pmc = Convert.ToDouble(BancoDados.selecionarRegistro("SELECT COALESCE(med_pco18,0) AS med_pco18 FROM PRECOS WHERE PROD_CODIGO = '" + vendaItens[i].ProdCodigo + "'"
                                + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual, "med_pco18"));

                            double diferenca = vendaItens[i].VendaPreValor - pmc;

                            if (diferenca < 0)
                            {
                                somaTotal = somaTotal + Math.Abs(diferenca);
                            }
                        }

                        if (somaTotal > 0)
                        {
                            mensagemPMC = "ECONONIA DE R$ " + String.Format("{0:N}", somaTotal) + " COM BASE NO PMC UNIT.\n\n";
                        }
                    }

                    Sat.SatImpressaoComprovante(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml",
                        totalImpFederal, totalImpEstadual, totalImpMunicipal, numeroCFe, out vendaNumeroNota, colaborador.NomeColaborador(dadosDaVenda.VendaColCodigo.ToString(), Principal.empAtual), dadosDaVenda.VendaID,
                        vEntr_End + " " + vEntr_Bairro, txtCliEmp.Text, mensagemPMC, comprovanteEspecie);

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);

                }
                else
                {
                    int iRetorno;

                    iRetorno = DarumaDLL.iConfigurarGuilhotina_DUAL_DarumaFramework("1", "2");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    iRetorno = DarumaDLL.eDefinirProduto_Daruma("SAT");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    if (!String.IsNullOrEmpty(Principal.NumCpfCnpj))
                    {
                        xmlSAT = "<dest><" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(Principal.NumCpfCnpj) + "</" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">"
                            + "<xNome>" + Principal.NomeNP + "</xNome></dest>";
                    }
                    else
                        xmlSAT = "";

                    iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        if (iRetorno == -130)
                        {
                            iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                            else
                            {
                                iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }

                        }
                        else
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    var dadosProdutos = new Produto();

                    for (int i = 0; i < vendaItens.Count; i++)
                    {
                        DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, vendaItens[i].ProdID);

                        xmlSAT = "<prod>";
                        xmlSAT += "<cProd>" + vendaItens[i].ProdCodigo + "</cProd>";
                        xmlSAT += "<xProd>" + Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString()) + "</xProd>";
                        xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"] + "</NCM>";
                        xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                        xmlSAT += "<uCom>" + vendaItens[i].VendaItemUnidade + "</uCom>";
                        xmlSAT += "<qCom>" + vendaItens[i].VendaItemQtde.ToString("#0.0000").Replace(",", ".") + "</qCom>";
                        xmlSAT += "<vUnCom>" + vendaItens[i].VendaItemUnitario.ToString("#0.000").Replace(",", ".") + "</vUnCom>";
                        xmlSAT += "<indRegra>A</indRegra>";
                        xmlSAT += "</prod>";
                        xmlSAT += "<imposto>";

                        xmlSAT += "<ICMS>";

                        if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                            dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>101</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>201</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>202</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>500</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>102</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>300</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else
                        {
                            xmlSAT += "<ICMSSN900>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>900</CSOSN>";
                            xmlSAT += "<pICMS>0.00</pICMS>";
                            xmlSAT += "</ICMSSN900>";
                        }
                        xmlSAT += "</ICMS>";
                        xmlSAT += "<PIS>";
                        xmlSAT += "<PISNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString() + "</CST>";
                        xmlSAT += "</PISNT>";
                        xmlSAT += "</PIS>";
                        xmlSAT += "<COFINS>";
                        xmlSAT += "<COFINSNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString() + "</CST>";
                        xmlSAT += "</COFINSNT>";
                        xmlSAT += "</COFINS>";
                        xmlSAT += "</imposto>";

                        iRetorno = DarumaDLL.aCFeVender_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    double descontoTotal = Convert.ToDouble(lblDesconto.Text) + Math.Abs(Convert.ToDouble(txtGAjuste.Text));

                    xmlSAT = "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(descontoTotal)).Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "<vCFeLei12741>0.00</vCFeLei12741>";
                    xmlSAT += "</total>";

                    iRetorno = DarumaDLL.aCFeTotalizar_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    for (int i = 0; i < vendaEspecie.Count; i++)
                    {
                        xmlSAT = "<MP><cMP>" + vendaEspecie[i].EspSAT + "</cMP>";
                        xmlSAT += "<vMP>" + String.Format("{0:N}", vendaEspecie[i].Valor).ToString().Replace(",", ".") + "</vMP>";
                        if (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04"))
                        {
                            xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                        }
                        xmlSAT += "</MP>";

                        iRetorno = DarumaDLL.aCFeEfetuarPagamento_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    iRetorno = DarumaDLL.tCFeEncerrar_SAT_Daruma("<infAdic><infCpl>" + Funcoes.LeParametro(2, "28", true) + "\nVendedor: "
                        + colaborador.NomeColaborador(dadosDaVenda.VendaColCodigo.ToString(), Principal.empAtual) + " \nVenda ID: " + dadosDaVenda.VendaID + "</infCpl></infAdic>");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    StringBuilder str_RetornoStatus = new StringBuilder(400);
                    iRetorno = DarumaDLL.rConsultarStatus_SAT_Daruma(str_RetornoStatus);

                    split = str_RetornoStatus.ToString().Split('|');

                    System.IO.File.Move(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml", @"" + Funcoes.LeParametro(15, "06", true)
                        + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\CFe" + split[17] + ".xml");

                    System.IO.File.Delete(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml");

                    numeroCFe = "CFe" + split[17];

                    iRetorno = DarumaDLL.rInfoEstendida_SAT_Daruma("1", str_RetornoStatus);
                    vendaNumeroNota = str_RetornoStatus.ToString();

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);
                }

                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(vendaPedFormaPagto[0].QtdeVias);

                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(lblTotal.Text),
                            vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && vendaBeneficio.Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        ComprovantesVinculados.VinculadoDrogabellaPlantaoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(lblTotal.Text),
                         vendaEspecie[0].Valor, vendaEspecie[0].EspDescricao, dadosDaVenda.VendaAutorizacao, dadosDaVenda.VendaAutorizacaoReceita, dadosDaVenda.VendaID,
                         wsProdutosCupomSemReceita, wsProdutosCupomReceita, wsNomeEmpresa, wsEmpresa, wsNumCartao, wsNomeCartao, chkEntrega.Checked, wsObrigaReceita, wsDadosMedico,
                         colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual), dadosDaVenda.VendaConvenioParcela);
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && wsConWeb.Equals("4"))
                    {
                        string produtosComprov = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                            produtosComprov += tProdutos.Rows[x]["COD_BARRA"] + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"]) > 0)
                            {
                                produtosComprov += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[x]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[x]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                            }
                            produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        ComprovantesVinculados.VinculadoParticular(qtdeVias, vendaNumeroNota,
                           @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(lblTotal.Text),
                           vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual), produtosComprov, vendaPedFormaPagto, txtCliEmp.Text, dadosDaVenda.VendaConCodigo);
                    }

                    if (Funcoes.LeParametro(6, "363", true).Equals("S"))
                    {
                        if (Convert.ToDouble(lblDesconto.Text) > 0 || Math.Abs(Convert.ToDouble(txtGAjuste.Text)) > 0)
                        {
                            string produtosComprov = "";
                            for (int x = 0; x < tProdutos.Rows.Count; x++)
                            {
                                double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["PROD_QTDE"]), 2) * 100);
                                if (Convert.ToDouble(tProdutos.Rows[x]["PROD_DVAL"]) > 0)
                                {
                                    produtosComprov += tProdutos.Rows[x]["COD_BARRA"] + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                    produtosComprov += "    R$ " + tProdutos.Rows[x]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[x]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[x]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                                    produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                                }
                            }

                            if (!String.IsNullOrEmpty(produtosComprov))
                            {
                                ComprovantesVinculados.VinculadoDesconto(vendaNumeroNota, @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", Convert.ToDouble(lblSubTotal.Text),
                                Convert.ToDouble(lblTotal.Text), colaborador.NomeColaborador(txtVendedor.Text, Principal.empAtual), produtosComprov);
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                OcultarMensagem();
            }
        }

        public void ChecaPastaXML(string mes)
        {
            #region IDENTIFICA SE EXISTE A PASTA DO MES
            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesComErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesComErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesSemErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesSemErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesComErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesComErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesComErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesComErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper());
            }

            if (!Directory.Exists(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesCanEnviados\" + mes.ToUpper()))
            {
                Directory.CreateDirectory(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesCanEnviados\" + mes.ToUpper());
            }
            #endregion
        }

        public async Task BuscaIBPT(string ncm, string descricao, double valor, string unidade, string codBarras)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(6, "371", false));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("?token=" + Funcoes.LeParametro(6, "370", false) +
                            "&cnpj=" + Funcoes.RemoveCaracter(Funcoes.LeParametro(15, "07", true)) + "&codigo=" + ncm + "&uf=SP&ex=0&codigoInterno=" + codBarras + "&descricao="
                            + descricao + "&unidadeMedida=" + unidade + "&valor=" + valor.ToString().Replace(",", ".") + "&gtin=" + codBarras);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(responseBody);

                        impostoFederal = Convert.ToDouble(data.ValorTributoNacional);
                        impostoEstadual = Convert.ToDouble(data.ValorTributoEstadual);
                        impostoMunicipal = Convert.ToDouble(data.ValorTributoMunicipal);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Espécie", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public async void Imposto(string ncm, string descricao, double valor, string unidade, string codBarras)
        {
            await BuscaIBPT(ncm, descricao, valor, unidade, codBarras);
        }

        public bool MontarXmlSAT(string numeroDocumento, string nomeNP)
        {
            #region MONTA XML SAT
            try
            {
                totalImpFederal = 0;
                totalImpEstadual = 0;
                totalImpMunicipal = 0;

                xmlSAT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xmlSAT += "<CFe>";
                xmlSAT += "<infCFe versaoDadosEnt=\"" + Funcoes.LeParametro(15, "02", true) + "\">";
                xmlSAT += "<ide>";
                xmlSAT += "<CNPJ>" + Funcoes.LeParametro(15, "07", true) + "</CNPJ>";
                xmlSAT += "<signAC>" + Funcoes.LeParametro(15, "03", true, Principal.nomeEstacao) + "</signAC>";
                xmlSAT += "<numeroCaixa>" + Funcoes.LeParametro(15, "04", true, Principal.nomeEstacao) + "</numeroCaixa>";
                xmlSAT += "</ide>";

                var estabelecimentos = new Estabelecimento();
                List<Estabelecimento> dadosFiscais = estabelecimentos.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                if (dadosFiscais.Count > 0)
                {
                    xmlSAT += "<emit>";
                    xmlSAT += "<CNPJ>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstCNPJ) + "</CNPJ>";
                    xmlSAT += "<IE>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoEstadual) + "</IE>";
                    xmlSAT += "<IM>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoMunicipal) + "</IM>";
                    xmlSAT += "<cRegTribISSQN>1</cRegTribISSQN>";
                    xmlSAT += "<indRatISSQN>N</indRatISSQN>";
                    xmlSAT += "</emit>";
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (!String.IsNullOrEmpty(numeroDocumento))
                {
                    xmlSAT += "<dest><" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(numeroDocumento) + "</" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">"
                        + "<xNome>" + nomeNP + "</xNome></dest>";
                }
                else
                    xmlSAT += "<dest/>";

                var dadosProdutos = new Produto();

                for (int i = 0; i < vendaItens.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(dadosDaVenda.EstCodigo, dadosDaVenda.EmpCodigo, vendaItens[i].ProdID);

                    xmlSAT += "<det nItem=\"" + (i + 1) + "\">";
                    xmlSAT += "<prod>";
                    xmlSAT += "<cProd>" + vendaItens[i].ProdCodigo + "</cProd>";
                    xmlSAT += "<xProd>" + Funcoes.RemoveCaracter(Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString())) + "</xProd>";
                    xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"].ToString().Trim() + "</NCM>";
                    xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                    xmlSAT += "<uCom>" + vendaItens[i].VendaItemUnidade + "</uCom>";
                    xmlSAT += "<qCom>" + vendaItens[i].VendaItemQtde.ToString("#0.0000").Replace(".", "").Replace(",", ".") + "</qCom>";
                    xmlSAT += "<vUnCom>" + Math.Round(vendaItens[i].VendaItemUnitario, 2).ToString("#0.000").Replace(".", "").Replace(",", ".") + "</vUnCom>";
                    xmlSAT += "<indRegra>A</indRegra>";
                    xmlSAT += "</prod>";
                    xmlSAT += "<imposto>";

                    //Imposto(dtDadosProdutos.Rows[0]["NCM"].ToString(), Funcoes.RemoveCaracter(Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString())), vendaItens[i].VendaItemUnitario, vendaItens[i].VendaItemUnidade, vendaItens[i].ProdCodigo);

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"]) > 0)
                        impostoFederal = (((vendaItens[i].VendaItemUnitario * vendaItens[i].VendaItemQtde)
                            - vendaItens[i].VendaItemDiferenca) * Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"]) > 0)
                        impostoEstadual = (((vendaItens[i].VendaItemUnitario * vendaItens[i].VendaItemQtde)
                            - vendaItens[i].VendaItemDiferenca) * Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"]) > 0)
                        impostoMunicipal = (((vendaItens[i].VendaItemUnitario * vendaItens[i].VendaItemQtde)
                            - vendaItens[i].VendaItemDiferenca) * Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"])) / 100;

                    totalImpFederal += impostoFederal;
                    totalImpEstadual += impostoEstadual;
                    totalImpMunicipal += impostoMunicipal;

                    xmlSAT += "<vItem12741>" + String.Format("{0:N}", impostoFederal + impostoEstadual + impostoMunicipal).Replace(",", ".") + "</vItem12741>";
                    xmlSAT += "<ICMS>";

                    if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>201</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>202</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>500</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>300</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else
                    {
                        xmlSAT += "<ICMSSN900>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>900</CSOSN>";
                        xmlSAT += "<pICMS>0.00</pICMS>";
                        xmlSAT += "</ICMSSN900>";
                    }
                    xmlSAT += "</ICMS>";
                    xmlSAT += "<PIS>";
                    xmlSAT += "<PISNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString();
                    xmlSAT += "</CST>";
                    xmlSAT += "</PISNT>";
                    xmlSAT += "</PIS>";
                    xmlSAT += "<COFINS>";
                    xmlSAT += "<COFINSNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString();
                    xmlSAT += "</CST>";
                    xmlSAT += "</COFINSNT>";
                    xmlSAT += "</COFINS>";
                    xmlSAT += "</imposto>";
                    xmlSAT += "</det>";
                }
                double descontoTotal = Convert.ToDouble(lblDesconto.Text) + Math.Abs(Convert.ToDouble(txtGAjuste.Text));
                if (Math.Abs(descontoTotal) > 0)
                {
                    xmlSAT += "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(Math.Round(descontoTotal, 2))).Replace(".", "").Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "</total>";
                }
                else
                {
                    xmlSAT += "<total/>";
                }

                xmlSAT += "<pgto>";

                comprovanteEspecie = "";
                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    xmlSAT += "<MP><cMP>" + vendaEspecie[i].EspSAT + "</cMP>";
                    xmlSAT += "<vMP>" + String.Format("{0:N}", Math.Round(vendaEspecie[i].Valor, 2)).ToString().Replace(".", "").Replace(",", ".") + "</vMP>";
                    if (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04"))
                    {
                        xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                    }
                    xmlSAT += "</MP>";

                    comprovanteEspecie += vendaEspecie[i].EspDescricao + String.Format("{0:N}", Math.Round(vendaEspecie[i].Valor, 2)).PadLeft((45 - vendaEspecie[i].EspDescricao.Length) - Math.Round(vendaEspecie[i].Valor, 2).ToString().Length, ' ') + "\n";
                }

                xmlSAT += "</pgto>";
                xmlSAT += "</infCFe>";
                xmlSAT += "</CFe>";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "XML SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion
        }

        public bool ComprovanteOrcamento()
        {
            try
            {
                if (MessageBox.Show("Imprime Comprovante?", "Comprovante Orcamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    double desconto = 0;
                    Cursor = Cursors.WaitCursor;
                    comprovanteVenda = "";
                    imprimeComprovante = true;
                    string tamanho;
                    var dadosEstabelecimento = new Estabelecimento();
                    List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                    if (retornoEstab.Count > 0)
                    {
                        ExibirMensagem("Imprimindo Comprovante. Aguarde...");

                        int qtde = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(vendaPedFormaPagto[0].QtdeVias);

                        for (int y = 1; y <= qtde; y++)
                        {
                            comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                            comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                            comprovanteVenda += "N. VENDA: " + dadosDaVenda.VendaID + "\n";
                            comprovanteVenda += "Data: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("HH:mm:ss") + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VLITEM R$" + "\n";
                            comprovanteVenda += "_________________________________________" + "\n";
                            for (int i = 0; i < tProdutos.Rows.Count; i++)
                            {
                                double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["PROD_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2) * 100);
                                comprovanteVenda += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[i]["COD_BARRA"].ToString(), 13) + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                                if (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) > 0)
                                {
                                    comprovanteVenda += "    R$ " + tProdutos.Rows[i]["PROD_VUNI"].ToString().Replace(",", ".") + " (% " + tProdutos.Rows[i]["PROD_DPORC"].ToString().Replace(",", ".") + " Desc = R$ " + tProdutos.Rows[i]["PROD_DVAL"].ToString().Replace(",", ".") + ")\n";
                                }
                                comprovanteVenda += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["PROD_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                            comprovanteVenda += "=========================================" + "\n";
                            var colaborador = new Colaborador();
                            comprovanteVenda += "VENDEDOR.: " + colaborador.NomeColaborador(dadosDaVenda.VendaColCodigo.ToString(), Principal.empAtual) + "\n";
                            comprovanteVenda += "SUBTOTAL.: R$ " + String.Format("{0:N}", lblSubTotal.Text) + "\n";

                            if (Math.Abs(Convert.ToDouble(lblDesconto.Text)) > 0 || Math.Abs(Convert.ToDouble(txtGAjuste.Text)) != 0)
                            {
                                comprovanteVenda += "DESCONTO.: R$ " + String.Format("{0:N}", Math.Abs(Convert.ToDouble(lblDesconto.Text))) + "   AJUSTE.: R$ " + String.Format("{0:N}", Math.Abs(Convert.ToDouble(txtGAjuste.Text))) + "\n";
                            }
                            comprovanteVenda += "TOTAL....: R$ " + String.Format("{0:N}", lblTotal.Text) + "\n";

                            desconto = Convert.ToDouble(lblSubTotal.Text) - Convert.ToDouble(lblTotal.Text);

                            var descricaoForma = new FormasPagamento();
                            comprovanteVenda += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO", 43) + "\n";

                            for (int x = 0; x < vendaPedFormaPagto.Count; x++)
                            {
                                comprovanteVenda += descricaoForma.IdentificaFormaDePagamento(vendaPedFormaPagto[x].VendaFormaID, vendaPedFormaPagto[x].EmpCodigo) + ".: R$ " + String.Format("{0:N}", vendaPedFormaPagto[x].VendaValorParcela) + "\n";
                            }

                            if (vendaEspecie.Count > 0)
                            {
                                comprovanteVenda += Funcoes.CentralizaTexto("ESPECIE(S)", 43) + "\n";

                                for (int a = 0; a < vendaEspecie.Count; a++)
                                {
                                    comprovanteVenda += Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_DESCRICAO", "ESP_CODIGO", vendaEspecie[a].EspCodigo.ToString()) + ".: R$ " + String.Format("{0:N}", vendaEspecie[a].Valor) + "\n";
                                }
                            }
                            comprovanteVenda += "_________________________________________" + "\n";

                            if (dadosVendaCliente.Count > 0)
                            {
                                comprovanteVenda += dadosVendaCliente[0].CfCodigo == "" ? "00" : dadosVendaCliente[0].CfCodigo;
                                comprovanteVenda += " - " + dadosVendaCliente[0].CfNome + "\n";
                                comprovanteVenda += dadosVendaCliente[0].CfEndereco + "," + dadosVendaCliente[0].CfNumeroEndereco + "\n";
                                comprovanteVenda += dadosVendaCliente[0].CfBairro + "\n";
                                comprovanteVenda += dadosVendaCliente[0].CfCidade + "/" + dadosVendaCliente[0].CfUF + "\n";
                                comprovanteVenda += "CPF.:" + dadosVendaCliente[0].CfDocto + "\n";
                                if (!String.IsNullOrEmpty(dadosDaVenda.VendaConCodigo) && dadosDaVenda.VendaConCodigo != "0")
                                {
                                    comprovanteVenda += "EMPRESA: " + dadosDaVenda.VendaConCodigo + " - "
                                        + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosDaVenda.VendaConCodigo) + "\n";
                                }
                                comprovanteVenda += "_________________________________________" + "\n";
                            }

                            bool entrouFuncional = false;

                            if (vendaEspecie.Count == 0 && chkEntrega.Checked)
                            {
                                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                                {
                                    comprovanteVenda += "_________________________________________" + "\n";
                                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacao))
                                    {
                                        comprovanteVenda += "Autorizacao:" + dadosDaVenda.VendaAutorizacao.PadLeft(38 - dadosDaVenda.VendaAutorizacao.Length, ' ') + "\n";
                                    }
                                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacaoReceita))
                                    {
                                        comprovanteVenda += "Autorizacao com Receita:" + dadosDaVenda.VendaAutorizacaoReceita.PadLeft(26 - dadosDaVenda.VendaAutorizacaoReceita.Length, ' ') + "\n";
                                    }
                                    comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                                    comprovanteVenda += "Cartao:                  " + wsNumCartao + "\n";
                                    comprovanteVenda += "Conveniado:  " + (wsNomeCartao.Length > 33 ? wsNomeCartao.Substring(0, 32) : wsNomeCartao.PadLeft(33 - wsNomeCartao.Length, ' ')) + "\n";
                                    comprovanteVenda += "Empresa: " + ((wsEmpresa + " - " + wsNomeEmpresa).Length > 32 ? (wsEmpresa + " - " + wsNomeEmpresa).Substring(0, 32)
                                        : (wsEmpresa + " - " + wsNomeEmpresa).PadLeft(32 - (wsEmpresa + " - " + wsNomeEmpresa).Length, ' ')) + "\n";
                                    if (!dadosDaVenda.VendaConvenioParcela.Equals(1))
                                    {
                                        comprovanteVenda += "N. Parcelas: " + dadosDaVenda.VendaConvenioParcela.ToString().PadLeft(29 - dadosDaVenda.VendaConvenioParcela.ToString().Length);
                                    }
                                    if (!String.IsNullOrEmpty(wsDadosMedico))
                                    {
                                        comprovanteVenda += wsDadosMedico;
                                    }
                                    comprovanteVenda += "\n";
                                    if (chkEntrega.Checked)
                                    {
                                        comprovanteVenda += "AUTORIZADO MEDIANTE A SENHA DE ENTREGA" + "\n";
                                    }
                                    else
                                    {
                                        comprovanteVenda += "AUTORIZADO MEDIANTE A SENHA PESSOAL" + "\n";
                                    }
                                }
                                else if (wsConWeb.Equals("4"))
                                {
                                    if (Funcoes.LeParametro(6, "93", false).Equals("S"))
                                    {
                                        comprovanteVenda += "_________________________________________" + "\n";
                                        comprovanteVenda += Funcoes.CentralizaTexto("DESCRICAO DAS PARCELAS", 43) + "\n";
                                        comprovanteVenda += "PARC  VENCIMENTO  VALOR\n";
                                        for (int z = 0; z < vendaPedFormaPagto.Count; z++)
                                        {
                                            comprovanteVenda += Funcoes.FormataZeroAEsquerda(vendaPedFormaPagto[z].VendaParcela.ToString(), 4) + "  " + vendaPedFormaPagto[z].VendaParcelaVencimento.ToString("dd/MM/yyyy") + "  " + String.Format("{0:N}", vendaPedFormaPagto[z].VendaValorParcela).Replace(",", ".") + "\n";
                                        }

                                    }
                                }
                            }

                            for (int i = 0; i < vendaEspecie.Count; i++)
                            {
                                if (vendaEspecie[i].EspVinculado.Equals("S") && vendaBeneficio.Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                                {
                                    comprovanteVenda += "_________________________________________" + "\n";
                                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacao))
                                    {
                                        comprovanteVenda += "Autorizacao:" + dadosDaVenda.VendaAutorizacao.PadLeft(38 - dadosDaVenda.VendaAutorizacao.Length, ' ') + "\n";
                                    }
                                    if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacaoReceita))
                                    {
                                        comprovanteVenda += "Autorizacao com Receita:" + dadosDaVenda.VendaAutorizacaoReceita.PadLeft(26 - dadosDaVenda.VendaAutorizacaoReceita.Length, ' ') + "\n";
                                    }
                                    comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                                    comprovanteVenda += "Cartao:                  " + wsNumCartao + "\n";
                                    comprovanteVenda += "Conveniado:  " + (wsNomeCartao.Length > 33 ? wsNomeCartao.Substring(0, 32) : wsNomeCartao.PadLeft(33 - wsNomeCartao.Length, ' ')) + "\n";
                                    comprovanteVenda += "Empresa: " + ((wsEmpresa + " - " + wsNomeEmpresa).Length > 32 ? (wsEmpresa + " - " + wsNomeEmpresa).Substring(0, 32)
                                        : (wsEmpresa + " - " + wsNomeEmpresa).PadLeft(32 - (wsEmpresa + " - " + wsNomeEmpresa).Length, ' ')) + "\n";
                                    if (!dadosDaVenda.VendaConvenioParcela.Equals(1))
                                    {
                                        comprovanteVenda += "N. Parcelas: " + dadosDaVenda.VendaConvenioParcela.ToString().PadLeft(29 - dadosDaVenda.VendaConvenioParcela.ToString().Length);
                                    }
                                    if (!String.IsNullOrEmpty(wsDadosMedico))
                                    {
                                        comprovanteVenda += wsDadosMedico;
                                    }
                                    comprovanteVenda += "\n";
                                    if (chkEntrega.Checked)
                                    {
                                        comprovanteVenda += "AUTORIZADO MEDIANTE A SENHA DE ENTREGA" + "\n";
                                    }
                                    else
                                    {
                                        comprovanteVenda += "AUTORIZADO MEDIANTE A SENHA PESSOAL" + "\n";
                                    }
                                }
                                else if (vendaEspecie[i].EspVinculado.Equals("S") && wsConWeb.Equals("4"))
                                {
                                    if (Funcoes.LeParametro(6, "93", false).Equals("S"))
                                    {
                                        comprovanteVenda += "_________________________________________" + "\n";
                                        comprovanteVenda += Funcoes.CentralizaTexto("DESCRICAO DAS PARCELAS", 43) + "\n";
                                        comprovanteVenda += "PARC  VENCIMENTO  VALOR\n";
                                        for (int z = 0; z < vendaPedFormaPagto.Count; z++)
                                        {
                                            comprovanteVenda += Funcoes.FormataZeroAEsquerda(vendaPedFormaPagto[z].VendaParcela.ToString(), 4) + "  " + vendaPedFormaPagto[z].VendaParcelaVencimento.ToString("dd/MM/yyyy") + "  " + String.Format("{0:N}", vendaPedFormaPagto[z].VendaValorParcela).Replace(",", ".") + "\n";
                                        }

                                    }
                                }
                                else if (vendaBeneficio.Equals("FUNCIONAL") && !entrouFuncional)
                                {
                                    entrouFuncional = true;
                                    comprovanteVenda += "_________________________________________" + "\n";

                                    if (dtDadosComandaFuncional != null)
                                    {
                                        comprovanteVenda += "CARTAO: " + dtDadosComandaFuncional.Rows[0]["CARTAO"].ToString() + "\n";
                                        comprovanteVenda += "NOME: " + dtDadosComandaFuncional.Rows[0]["NOME"].ToString() + "\n";
                                        string a = dtDadosComandaFuncional.Rows[0]["DATA"].ToString();
                                        string dataTransacao = a;

                                        string horaTransacao = DateTime.ParseExact(dtDadosComandaFuncional.Rows[0]["HORA"].ToString(), "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                                        comprovanteVenda += "DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n";

                                        if (!String.IsNullOrEmpty(dtDadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString()))
                                        {
                                            comprovanteVenda += dtDadosComandaFuncional.Rows[0]["CONSELHO"].ToString() + ": " + dtDadosComandaFuncional.Rows[0]["COD_CONSELHO"].ToString() + "\n";
                                        }

                                        comprovanteVenda += "AUTORIZACAO: " + dtDadosComandaFuncional.Rows[0]["NSU_AUTORIZADOR"].ToString() + "\n";
                                        comprovanteVenda += "NSU: " + dtDadosComandaFuncional.Rows[0]["NSU"].ToString() + "\n";

                                        comprovanteVenda += "TOTAL DA COMPRA: " + dtDadosComandaFuncional.Rows[0]["TOTAL_VENDA"].ToString() + "\n";
                                        comprovanteVenda += "DESCONTO: " + dtDadosComandaFuncional.Rows[0]["DESCONTO"].ToString() + "\n";
                                        comprovanteVenda += "TOTAL A PAGAR: " + dtDadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString() + "\n";
                                        comprovanteVenda += "(" + Util.toExtenso(Convert.ToDecimal(dtDadosComandaFuncional.Rows[0]["TOTAL_A_PAGAR"].ToString())) + ")" + "\n";
                                        if (pagamentoTotalOuParcial == "T")
                                        {
                                            comprovanteVenda += "VALOR A PAGAR NO CARTAO: " + dtDadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n";
                                        }
                                        else if (pagamentoTotalOuParcial == "P")
                                        {
                                            comprovanteVenda += "VALOR A PAGAR A VISTA: " + dtDadosComandaFuncional.Rows[0]["VALOR_CARTAO"].ToString() + "\n";
                                            comprovanteVenda += "VALOR A PAGAR NO CARTAO: " + dtDadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n";
                                        }
                                        else
                                        {
                                            comprovanteVenda += "VALOR A PAGAR A VISTA: " + dtDadosComandaFuncional.Rows[0]["VALOR_AVISTA"].ToString() + "\n";
                                        }
                                    }
                                    else
                                    {
                                        comprovanteVenda += "CARTAO: " + objTransacaoClienteCartaoFuncional.Trilha2DoCartao + "\n";
                                        comprovanteVenda += "NOME: " + objTransacaoClienteCartaoFuncional.NomeDoPortador + "\n";
                                        string a = objConfirmaVendaResponse.DataTransacao;
                                        string dataTransacao = DateTime.ParseExact(a, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                                        string horaTransacao = DateTime.ParseExact(objConfirmaVendaResponse.HoraTransacao, "HHmmss", System.Globalization.CultureInfo.InvariantCulture).ToString("HH:mm:ss");
                                        //string hora = objConfirmaVendaResponse.HoraTransacao;
                                        comprovanteVenda += "DATA: " + Convert.ToDateTime(dataTransacao).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(horaTransacao).ToString("HH:mm:ss") + "\n";

                                        if (objConfirmaVendaResponse.VendaComReceita == "S")
                                        {
                                            comprovanteVenda += vendaAutorizadaResponse.TipoConselhoDaVenda + ": " + Principal.crm + "\n";
                                        }
                                        comprovanteVenda += "AUTORIZACAO: " + objConfirmaVendaResponse.NSUDoAutorizador + "\n";
                                        comprovanteVenda += "NSU: " + objConfirmaVendaResponse.NSU + "\n";

                                        comprovanteVenda += "TOTAL DA COMPRA: " + String.Format("{0:N}", vendaAutorizadaResponse.ValorTotal) + "\n";
                                        comprovanteVenda += "DESCONTO: " + vendaAutorizadaResponse.Desconto.ToString("C") + "\n";
                                        comprovanteVenda += "TOTAL A PAGAR: " + String.Format("{0:N}", vendaAutorizadaResponse.TotalAPagar) + "\n";
                                        comprovanteVenda += "(" + Util.toExtenso(Convert.ToDecimal(vendaAutorizadaResponse.TotalAPagar)) + ")" + "\n";
                                        if (pagamentoTotalOuParcial == "T")
                                        {
                                            comprovanteVenda += "VALOR A PAGAR NO CARTAO: " + vendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n";
                                        }
                                        else if (pagamentoTotalOuParcial == "P")
                                        {
                                            comprovanteVenda += "VALOR A PAGAR A VISTA: " + vendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n";
                                            comprovanteVenda += "VALOR A PAGAR NO CARTAO: " + vendaAutorizadaResponse.ValorAReceberNoCartao.ToString("C") + "\n";
                                        }
                                        else
                                        {
                                            comprovanteVenda += "VALOR A PAGAR A VISTA: " + vendaAutorizadaResponse.ValorAReceberAVista.ToString("C") + "\n";
                                        }
                                    }

                                    if (y == 2)
                                    {
                                        comprovanteVenda += "\n\n\n";
                                        comprovanteVenda += "_________________________________________" + "\n";
                                        comprovanteVenda += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n\n\n";
                                        comprovanteVenda += "_________________________________________" + "\n";
                                        comprovanteVenda += Funcoes.CentralizaTexto("RG", 43) + "\n";
                                    }
                                }

                                comprovanteVenda += "\n";
                            }


                            comprovanteVenda += "_________________________________________" + "\n";
                            comprovanteVenda += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                            if (desconto > 0)
                            {
                                comprovanteVenda += "VOCE ECONOMIZOU R$ " + desconto.ToString().Replace(",", ".") + "\n";
                            }

                            if (Funcoes.LeParametro(6, "378", false).Equals("S"))
                            {
                                double somaTotal = 0;
                                for (int i = 0; i < vendaItens.Count; i++)
                                {
                                    double pmc = Convert.ToDouble(BancoDados.selecionarRegistro("SELECT COALESCE(med_pco18,0) AS med_pco18 FROM PRECOS WHERE PROD_CODIGO = '" + vendaItens[i].ProdCodigo + "'"
                                        + " AND EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual, "med_pco18"));

                                    double diferenca = vendaItens[i].VendaPreValor - pmc;

                                    if (diferenca < 0)
                                    {
                                        somaTotal = somaTotal + Math.Abs(diferenca);
                                    }
                                }

                                if (somaTotal > 0)
                                {
                                    comprovanteVenda += "ECONONIA DE R$ " + String.Format("{0:N}", somaTotal) + " COM BASE NO PMC UNIT.\n";
                                }
                            }
                            comprovanteVenda += "_________________________________________" + "\n\n\n";

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                     && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    string s_cmdTX = "\n";
                                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                    }
                                    else
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                    }

                                    iRetorno = BematechImpressora.IniciaPorta("USB");

                                    iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                    s_cmdTX = "\r\n";
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                        BematechImpressora.AcionaGuilhotina(0);
                                    }

                                    iRetorno = BematechImpressora.FechaPorta();
                                }
                                else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                                    if (iRetorno != 1)
                                    {
                                        return false;
                                    }

                                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);
                                    if (iRetorno != 1)
                                    {
                                        return false;
                                    }

                                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                                    if (iRetorno != 1)
                                    {
                                        return false;
                                    }

                                    iRetorno = InterfaceEpsonNF.FechaPorta();
                                    if (iRetorno != 1)
                                    {
                                        return false;
                                    }
                                }
                                else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                                {
                                    using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + dadosDaVenda.VendaID + ".txt", true))
                                    {
                                        writer.WriteLine(comprovanteVenda);
                                    }
                                }
                                else
                                {
                                    //IMPRIME COMPROVANTE
                                    comprovanteVenda += "\n\n\n\n\n";
                                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                    }
                                }
                            }
                            else
                            {
                                comprovanteVenda += "\n\n\n\n";
                                //IMPRIME COMPROVANTE
                                int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }
                        }

                        if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && wsObrigaReceita.Equals("S"))
                        {
                            for (int i = 1; i <= qtde; i++)
                            {
                                comprovanteVenda = Funcoes.CentralizaTexto("COMPROVANTE", 43) + "\n";
                                comprovanteVenda += Funcoes.CentralizaTexto("NAO E DOCUMENTO FISCAL", 43) + "\n\n";
                                comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                                comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                                comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                                comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                                tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                                comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                                comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                                comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                                comprovanteVenda += "N. VENDA: " + dadosDaVenda.VendaID + "\n";
                                comprovanteVenda += "Data: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dadosDaVenda.VendaDataHora).ToString("HH:mm:ss") + "\n";
                                comprovanteVenda += "_________________________________________" + "\n";
                                if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacao))
                                {
                                    comprovanteVenda += "Autorizacao:" + dadosDaVenda.VendaAutorizacao.PadLeft(38 - dadosDaVenda.VendaAutorizacao.Length, ' ') + "\n";
                                }
                                if (!String.IsNullOrEmpty(dadosDaVenda.VendaAutorizacaoReceita))
                                {
                                    comprovanteVenda += "Autorizacao com Receita:" + dadosDaVenda.VendaAutorizacaoReceita.PadLeft(26 - dadosDaVenda.VendaAutorizacaoReceita.Length, ' ') + "\n";
                                }
                                comprovanteVenda += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                                comprovanteVenda += "Cartao:                  " + wsNumCartao + "\n";
                                comprovanteVenda += "Conveniado:  " + (wsNomeCartao.Length > 33 ? wsNomeCartao.Substring(0, 32) : wsNomeCartao.PadLeft(33 - wsNomeCartao.Length, ' ')) + "\n";
                                comprovanteVenda += "Empresa: " + ((wsEmpresa + " - " + wsNomeEmpresa).Length > 32 ? (wsEmpresa + " - " + wsNomeEmpresa).Substring(0, 32)
                                    : (wsEmpresa + " - " + wsNomeEmpresa).PadLeft(32 - (wsEmpresa + " - " + wsNomeEmpresa).Length, ' ')) + "\n";
                                comprovanteVenda += "_________________________________________" + "\n";

                                if (!String.IsNullOrEmpty(wsProdutosCupomSemReceita))
                                {
                                    comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS SEM RECEITA", 43) + "\n";
                                    comprovanteVenda += wsProdutosCupomSemReceita;
                                    comprovanteVenda += "_________________________________________" + "\n";
                                }

                                if (!String.IsNullOrEmpty(wsProdutosCupomReceita))
                                {
                                    comprovanteVenda += Funcoes.CentralizaTexto("COMPRAS COM RECEITA", 43) + "\n";
                                    comprovanteVenda += wsProdutosCupomReceita;
                                    comprovanteVenda += "_________________________________________" + "\n";
                                }

                                comprovanteVenda += "Declaro ter verificado a classificacao" + "\n";
                                comprovanteVenda += "das compras com e sem receita para fins" + "\n";
                                comprovanteVenda += "de desconto." + "\n\n";
                                comprovanteVenda += "_________________________________________" + "\n";
                                comprovanteVenda += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                                comprovanteVenda += "_________________________________________" + "\n";
                                comprovanteVenda += "DROGA BELLA AGRADECE TUDO DE BOM P/ VOCE" + "\n";
                                comprovanteVenda += "_________________________________________" + "\n\n\n";


                                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                                {
                                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                         && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    {
                                        int iRetorno;
                                        string s_cmdTX = "\n";
                                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                        {
                                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                        }
                                        else
                                        {
                                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                        }

                                        iRetorno = BematechImpressora.IniciaPorta("USB");

                                        iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                        s_cmdTX = "\r\n";
                                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                        {
                                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                            BematechImpressora.AcionaGuilhotina(0);
                                        }
                                        iRetorno = BematechImpressora.FechaPorta();
                                    }
                                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                    {
                                        int iRetorno;
                                        iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                        iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                        iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                        iRetorno = InterfaceEpsonNF.FechaPorta();
                                    }
                                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                                    {
                                        using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\Drogabella" + dadosDaVenda.VendaID + ".txt", true))
                                        {
                                            writer.WriteLine(comprovanteVenda);
                                        }
                                    }
                                    else
                                    {
                                        //IMPRIME COMPROVANTE
                                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                        {
                                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                        }
                                    }
                                }
                                else
                                {
                                    comprovanteVenda += "\n\n\n\n";
                                    //IMPRIME COMPROVANTE
                                    int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                    if (iRetorno != 1)
                                    {
                                        MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Orcamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                OcultarMensagem();
            }
        }

        private void chkEntrega_CheckedChanged(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(6, "306", true).Equals("S"))
            {
                if (chkEntrega.Checked)
                {
                    retorno = BancoDados.selecionarRegistro("SELECT EMPRESA_ID FROM TAXA_ENTREGA WHERE EMPRESA_ID = " + txtCodCliEmp.Text, "EMPRESA_ID");
                    if (string.IsNullOrEmpty(retorno) && !entregaIsenta)
                    {
                        txtProduto.Text = Funcoes.LeParametro(6, "307", true);
                        lerProdutos();
                    }
                }
                else
                {
                    linha_resultado = tProdutos.Select("COD_BARRA = '" + Funcoes.LeParametro(6, "307", true) + "'");
                    if (linha_resultado.Length > 0)
                        linha_resultado[0].Delete();
                    entregaIsenta = false;
                    InserirProdCupom(tProdutos);
                    txtProduto.Focus();
                }
            }
        }

        public bool IsentaTaxaEntrega()
        {
            try
            {
                if (Funcoes.LeParametro(6, "310", true).Equals("S") && Funcoes.LeParametro(6, "306", true).Equals("S") && chkEntrega.Checked)
                {
                    linha_resultado = tProdutos.Select("COD_BARRA = '" + Funcoes.LeParametro(6, "307", true) + "'");
                    double totalVenda = 0;
                    if (tProdutos.Rows.Count > 0)
                    {
                        totalVenda = Convert.ToDouble(tProdutos.Compute("SUM(PROD_TOTAL)", ""));
                        if (linha_resultado.Length > 0)
                            totalVenda = totalVenda - Convert.ToDouble(linha_resultado[0]["PROD_VUNI"]);
                    }

                    if (totalVenda > Convert.ToDouble(Funcoes.LeParametro(6, "311", true)))
                    {
                        if (linha_resultado.Length > 0)
                            linha_resultado[0].Delete();

                        InserirProdCupom(tProdutos);
                        entregaIsenta = true;
                        txtProduto.Focus();
                    }
                    else
                    {
                        if (linha_resultado.Length == 0)
                        {
                            txtProduto.Text = Funcoes.LeParametro(6, "307", true);
                            entregaIsenta = false;
                            lerProdutos();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Taxa Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                frmVenAtalhos atalhos = new frmVenAtalhos();
                atalhos.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ProtocoloEntrega()
        {
            try
            {
                ExibirMensagem("Imprimindo Protocolo. Aguarde...");

                Cursor = Cursors.WaitCursor;

                #region CONTROLE DE ENTREGA
                if (Funcoes.LeParametro(2, "33", false).Equals("S"))
                {
                    protocoloDeEntrega = "_________________________________________" + "\n";
                    protocoloDeEntrega += Funcoes.CentralizaTexto("CONTROLE DE ENTREGA", 43) + "\n";
                    protocoloDeEntrega += "_________________________________________" + "\n";
                    protocoloDeEntrega += "VENDA.........: " + dadosDaVenda.VendaID + "\n";
                    protocoloDeEntrega += "DATA..........: " + dadosDaVenda.VendaData + "\n";
                    protocoloDeEntrega += "TIPO VENDA....: " + (vendaBeneficio == "N" ? "A VISTA" : vendaBeneficio) + "\n";
                    protocoloDeEntrega += "CAIXA.........:__________________________\n";
                    protocoloDeEntrega += "MOTO BOY......:__________________________\n";
                    protocoloDeEntrega += "_________________________________________\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(protocoloDeEntrega, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(protocoloDeEntrega);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + dadosDaVenda.VendaID + ".txt", true))
                            {
                                writer.WriteLine(protocoloDeEntrega);
                            }
                        }
                        else
                        {
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocoloDeEntrega);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        protocoloDeEntrega += "\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocoloDeEntrega, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                #endregion

                protocoloDeEntrega = "_________________________________________" + "\n";
                protocoloDeEntrega += Funcoes.CentralizaTexto("PROTOCOLO DE ENTREGA", 43) + "\n";
                protocoloDeEntrega += "_________________________________________" + "\n";
                protocoloDeEntrega += "VENDA.........: " + dadosDaVenda.VendaID + "\n";
                protocoloDeEntrega += "DATA..........: " + dadosDaVenda.VendaData + "\n";
                protocoloDeEntrega += "CLIENTE.......: " + dadosVendaCliente[0].CfNome + "\n";
                protocoloDeEntrega += "ENDERECO......: " + vEntr_End + "\n";
                protocoloDeEntrega += "NUMERO........: " + vEntr_Numero + "\n";
                protocoloDeEntrega += "BAIRRO........: " + vEntr_Bairro + "\n";
                protocoloDeEntrega += "CIDADE........: " + vEntr_Cidade + "\n";
                protocoloDeEntrega += "CEP...........: " + vEntr_Cep + "\n";
                protocoloDeEntrega += "TELEFONE......: " + vEntr_Fone + "\n";
                protocoloDeEntrega += "TOTAL DA NOTA.: " + String.Format("{0:N}", dadosDaVenda.VendaTotal) + "\n";
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && diferencaDoValorAutorizado != 0)
                {
                    protocoloDeEntrega += "CONVENIO......: " + String.Format("{0:N}", valorAutorizado) + "\n";
                    protocoloDeEntrega += "DIFERENCA.....: " + String.Format("{0:N}", diferencaDoValorAutorizado) + "\n";
                    if (vEntr_Valor != 0)
                    {
                        protocoloDeEntrega += "TROCO PARA....: " + String.Format("{0:N}", vEntr_Valor) + "\n";
                        protocoloDeEntrega += "TROCO.........: " + String.Format("{0:N}", vEntr_Valor - diferencaDoValorAutorizado) + "\n";
                    }
                    else
                    {
                        protocoloDeEntrega += "TROCO.........: 0,00\n";
                    }
                }
                else
                {
                    if (vEntr_Valor != 0)
                    {
                        protocoloDeEntrega += "TROCO PARA....: " + String.Format("{0:N}", vEntr_Valor + dadosDaVenda.VendaTotal) + "\n";
                        protocoloDeEntrega += "TROCO.........: " + String.Format("{0:N}", vEntr_Valor) + "\n";
                    }
                    else
                    {
                        protocoloDeEntrega += "TROCO.........: 0,00\n";
                    }
                }
                protocoloDeEntrega += "OBSERVACAO....: " + vEntr_Obs + "\n\n\n";
                protocoloDeEntrega += "_________________________________________" + "\n";
                protocoloDeEntrega += Funcoes.CentralizaTexto("ASSINATURA", 43) + "\n";
                protocoloDeEntrega += "_________________________________________" + "\n\n\n\n\n\n";

                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                         && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        string s_cmdTX = "\n";
                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                        }
                        else
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                        }

                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(protocoloDeEntrega, 3, 0, 0, 0, 0);

                        s_cmdTX = "\r\n";
                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                        {
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            BematechImpressora.AcionaGuilhotina(0);
                        }

                        iRetorno = BematechImpressora.FechaPorta();
                    }
                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                        iRetorno = InterfaceEpsonNF.ImprimeTexto(protocoloDeEntrega);

                        iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                        iRetorno = InterfaceEpsonNF.FechaPorta();

                    }
                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                    {
                        using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + dadosDaVenda.VendaID + ".txt", true))
                        {
                            writer.WriteLine(protocoloDeEntrega);
                        }
                    }
                    else
                    {
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocoloDeEntrega);
                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                }
                else
                {
                    protocoloDeEntrega += "\n\n\n\n";
                    //IMPRIME COMPROVANTE
                    int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocoloDeEntrega, 0);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Protocolo de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                OcultarMensagem();
            }
        }

        private void listCupom_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (listCupom.SelectedIndex >= 5)
                    {
                        Cursor = Cursors.WaitCursor;

                        if (!listCupom.SelectedItem.ToString().Substring(0, 2).Equals("de"))
                        {
                            linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                //LIBERAÇÃO DE ALTERAÇÃO NA PROMOÇÃO//
                                if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "380", false).Equals("S"))
                                {
                                    linha_resultado[0].BeginEdit();
                                    linha_resultado[0]["PROD_PROMO"] = "N";
                                    linha_resultado[0].EndEdit();

                                    linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                                }

                                if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("N"))
                                {
                                    MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " já esta em promoção, desconto não permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtProduto.Focus();
                                }
                                else if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("S") && !vendaBeneficio.Equals("PARTICULAR") && !chkEntrega.Checked)
                                {
                                    MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " já esta em promoção, desconto não permitido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtProduto.Focus();
                                }
                                else if (linha_resultado[0]["BENEFICIO"].ToString().Equals("N") || vendaBeneficio.Equals("PORTALDROGARIA") || vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                                {
                                    if (linha_resultado[0]["PROD_PROMO"].ToString().Equals("S") && Funcoes.LeParametro(6, "375", false).Equals("S") && (vendaBeneficio.Equals("PARTICULAR") || chkEntrega.Checked))
                                    {
                                        linha_resultado[0].BeginEdit();
                                        linha_resultado[0]["PROD_PROMO"] = "N";
                                        linha_resultado[0].EndEdit();

                                        linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                                    }

                                    if (!Funcoes.LeParametro(6, "84", true).Equals(""))
                                    {
                                        tDesconto = Convert.ToDecimal(Funcoes.LeParametro(6, "84", false));
                                    }
                                    else
                                        tDesconto = 100;

                                    CamposProdutoVisivel();

                                    //EXIBE O CAMPO DE COMISSAO//
                                    if (Funcoes.LeParametro(6, "117", false).Equals("S"))
                                    {
                                        lblComissao.Visible = true;
                                        txtComissao.Visible = true;
                                    }
                                    else
                                    {
                                        lblComissao.Visible = false;
                                        txtComissao.Visible = false;
                                    }

                                    //EXIBE O CAMPO DE ULTIMO CUSTO//
                                    if (Funcoes.LeParametro(6, "349", true, Principal.nomeEstacao).Equals("S"))
                                    {
                                        lblCusto.Visible = true;
                                        txtCusto.Visible = true;
                                    }
                                    else
                                    {
                                        lblCusto.Visible = false;
                                        txtCusto.Visible = false;
                                    }

                                    btnOk.Enabled = true;
                                    btnCancela.Enabled = true;
                                    btnExcluir.Enabled = true;
                                    txtVUni.ReadOnly = false;
                                    txtPQtde.ReadOnly = false;
                                    txtSTotal.ReadOnly = false;
                                    txtPDesc.ReadOnly = false;
                                    txtVDesc.ReadOnly = false;
                                    txtVTotal.ReadOnly = false;

                                    lblNumCodBarras.Text = linha_resultado[0]["COD_BARRA"].ToString();
                                    lblNome.Text = linha_resultado[0]["PROD_DESCR"].ToString().Length <= 20 ? linha_resultado[0]["PROD_DESCR"].ToString() : linha_resultado[0]["PROD_DESCR"].ToString().Substring(0, 20);
                                    txtVUni.Text = String.Format("{0:N}", linha_resultado[0]["PROD_VUNI"]);
                                    txtPQtde.Text = linha_resultado[0]["PROD_QTDE"].ToString();
                                    txtSTotal.Text = String.Format("{0:N}", linha_resultado[0]["PROD_SUBTOTAL"]);
                                    txtPDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DPORC"].ToString() == "" ? 0 : Math.Abs(Convert.ToDouble(linha_resultado[0]["PROD_DPORC"])));
                                    txtVDesc.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DVAL"].ToString() == "" ? 0 : linha_resultado[0]["PROD_DVAL"]);
                                    txtVTotal.Text = String.Format("{0:N}", linha_resultado[0]["PROD_TOTAL"]);
                                    txtEstoque.Text = linha_resultado[0]["PROD_ESTATUAL"].ToString();
                                    txtComissao.Text = String.Format("{0:N}", linha_resultado[0]["PROD_DB"]);
                                    txtCusto.Text = String.Format("{0:N}", linha_resultado[0]["PROD_ULTCUSME"]);
                                    txtPQtde.Focus();

                                }
                                else
                                {
                                    MessageBox.Show("Produto: " + linha_resultado[0]["COD_BARRA"] + " de benefício, alteração não permitida!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtProduto.Focus();
                                }
                            }
                            else
                            {
                                txtProduto.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnReimpressao_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnReimpressao, "F7-Reimpressão");
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void btnLimpar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnLimpar, "F6-Limpar Venda");
        }

        private void btnConsultaProdutos_Click(object sender, EventArgs e)
        {
            frmBuscaVProd consultaProdutos = new frmBuscaVProd();
            consultaProdutos.ShowDialog();
        }

        private void BTN_ConsultaABCFarma_Click(object sender, EventArgs e)
        {
            frmChamados abrirChamado = new frmChamados();
            abrirChamado.ShowDialog();
        }

        private void btnConsultaProdutos_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultaProdutos, "F2-Consulta Rápida de Produtos");
        }

        private void btnBeneficios_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnConsultaProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnReimpressao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnFVenda_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnUltimosProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnUltimosProdutos_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultaProdutos, "F3-Últimos Produtos");
        }

        private void btnUltimosProdutos_Click(object sender, EventArgs e)
        {
            frmUltimosProdutos ultimosProdutos = new frmUltimosProdutos(IDCliente);
            ultimosProdutos.ShowDialog();
        }

        public bool ChecaLimiteParticular()
        {
            try
            {
                if (Funcoes.LeParametro(0, "09", true).Equals("N"))
                    return true;

                if (dadosVendaCliente.Count.Equals(0))
                {
                    DadosDaVendaCliente(Principal.doctoCliente);
                }

                int diasCarencia = Funcoes.LeParametro(6, "99", false) == "" ? 0 : Convert.ToInt32(Funcoes.LeParametro(6, "99", false));

                var parcelasAtrasadas = new CobrancaParcela();

                double atrasado = parcelasAtrasadas.SaldoDataMenorQueVencimento(Principal.empAtual, Principal.estAtual, DateTime.Now.AddDays(diasCarencia * -1), IDCliente);
                double avencer = parcelasAtrasadas.SaldoDataMaiorQueVencimento(Principal.empAtual, Principal.estAtual, DateTime.Now.AddDays(diasCarencia * -1), IDCliente);

                if (atrasado > 0 && Funcoes.LeParametro(0, "52", false).Equals("N"))
                {
                    MessageBox.Show("Este Cliente possui contas em aberto.\nVencidas: " + String.Format("{0:N}", atrasado) + "\nA vencer: " + String.Format("{0:N}", avencer), "Checa Limite", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (avencer > 0)
                {
                    MessageBox.Show("Este Cliente possui contas em aberto.\nA vencer: " + String.Format("{0:N}", avencer), "Checa Limite", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (Funcoes.LeParametro(0, "10", false).Equals("S"))
                {
                    if (dadosVendaCliente[0].CfLimite > 0)
                    {
                        if (Funcoes.LeParametro(0, "52", false).Equals("N"))
                        {
                            if (atrasado + avencer + Convert.ToDouble(lblTotal.Text) > dadosVendaCliente[0].CfLimite)
                            {
                                Principal.msgEstilo = MessageBoxButtons.OK;
                                Principal.msgIcone = MessageBoxIcon.Information;
                                Principal.msgTitulo = "Checa Limite";

                                Principal.mensagem = "Cliente já atingiu limite de compra.\n"
                                    + "Vencidas: " + String.Format("{0:N}", atrasado) + "\n"
                                    + "A Vencer: " + String.Format("{0:N}", avencer) + "\n"
                                    + "Compra Atual: " + String.Format("{0:N}", Convert.ToDouble(lblTotal.Text)) + "\n"
                                    + "Total Geral: " + String.Format("{0:N}", atrasado + avencer + Convert.ToDouble(lblTotal.Text)) + "\n"
                                    + "Limite: " + String.Format("{0:N}", dadosVendaCliente[0].CfLimite) + "\n";

                                Funcoes.Avisa();

                                if (Funcoes.LeParametro(6, "357", false).Equals("S"))
                                    return false;
                            }
                        }
                        else if (avencer + Convert.ToDouble(lblTotal.Text) > dadosVendaCliente[0].CfLimite)
                        {
                            Principal.msgEstilo = MessageBoxButtons.OK;
                            Principal.msgIcone = MessageBoxIcon.Information;
                            Principal.msgTitulo = "Checa Limite";

                            Principal.mensagem = "Compra maior que o Limite Disponível!\n"
                                + "Compra Atual: " + String.Format("{0:N}", Convert.ToDouble(lblTotal.Text)) + "\n"
                                + "Total à Vencer: " + String.Format("{0:N}", avencer) + "\n"
                                + "Limite: " + String.Format("{0:N}", dadosVendaCliente[0].CfLimite) + "\n"
                                + "Limite Disponível: " + String.Format("{0:N}", dadosVendaCliente[0].CfLimite - (avencer + Convert.ToDouble(lblTotal.Text))) + "\n";

                            Funcoes.Avisa();

                            if (Funcoes.LeParametro(6, "357", false).Equals("S"))
                                return false;
                        }
                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Checa Limite", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool StatusCaixa()
        {
            var caixa = new AFCaixa();
            List<AFCaixa> cxStatus = caixa.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);

            if (cxStatus.Count.Equals(0))
            {
                return false;
            }
            else if (cxStatus[0].DataAbertura.ToString("dd/MM/yyyy") != dtPedido.ToString("dd/MM/yyyy"))
            {
                return false;
            }

            return true;

        }

        public void BuscaConsumidor()
        {
            codCliente = Funcoes.LeParametro(6, "114", true);
            txtCodCliEmp.Text = codCliente.ToString();
            dtRetorno = dCliente.DadosClienteFiltro(codCliente.ToString(), 4, out filtro);
            IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
            doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
            Principal.doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
            btnUltimosProdutos.Enabled = true;
        }

        private void btnBuluario_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuluario_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnBuluario, "F4-Bulário Eletrônico");
        }

        private void btnBuluario_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            System.Diagnostics.Process.Start("http://www.anvisa.gov.br/datavisa/fila_bula/index.asp");
            Cursor = Cursors.Default;
        }

        private void chkEntrega_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.chkEntrega, "CTRL + E -Marca/Desmarca Entrega");
        }

        private void txtQtde_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.txtQtde, "CTRL + Q -Foco na Quantidade");
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtTelefone.Text))
                {
                    dtRetorno = dCliente.DadosClienteFiltro(txtTelefone.Text, 3, out filtro);
                    if (dtRetorno.Rows.Count == 1)
                    {
                        IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                        doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CON_CODIGO"].ToString()))
                        {
                            txtCodCliEmp.Text = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                            wsConID = Convert.ToInt32(dtRetorno.Rows[0]["CON_ID"]);
                            wsEmpresa = dtRetorno.Rows[0]["CON_CODIGO"].ToString();
                            wsConWeb = dtRetorno.Rows[0]["CON_WEB"].ToString();
                            wsConCodConv = dtRetorno.Rows[0]["CON_COD_CONV"].ToString();

                            if (Funcoes.LeParametro(6, "368", false).Equals("S"))
                            {
                                if (wsConWeb.Equals("0") || wsConWeb.Equals("4"))
                                {
                                    lblConveniada.Visible = true;
                                    lblConveniada.Text = dtRetorno.Rows[0]["CON_NOME"].ToString();
                                }
                                else
                                    lblConveniada.Visible = false;
                            }
                        }
                        else
                        {
                            if (codCliente == "0")
                            {
                                txtCodCliEmp.Text = IDCliente.ToString();
                            }
                            else
                                txtCodCliEmp.Text = codCliente;
                        }

                        if (wsConWeb == "1" && wsNumCartao != "")
                        {
                            MessageBox.Show("Cliente Drogabella/Plantão Card, necessário informar Cartão busca pelo nome não gera Autorização.\n"
                                + "Escolher a opção Benefícios-Drogabella ou Plantão Card e buscar as informações do Cliente pelo Cartao!", "Validação Convênio",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtCodCliEmp.Text = "";
                            txtCliEmp.Text = "";
                            txtTelefone.Text = "";
                            txtTelefone.Enabled = true;
                            txtCodCliEmp.Enabled = true;
                            txtCliEmp.Enabled = true;
                            return;
                        }

                        Principal.doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                        DadosDaVendaCliente(Principal.doctoCliente);
                        txtCodCliEmp.Enabled = false;
                        txtTelefone.Enabled = false;
                        txtCliEmp.Enabled = false;
                        txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                        btnUltimosProdutos.Enabled = true;

                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString()))
                        {
                            MessageBox.Show(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString().ToUpper(), "Observação do Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        if (statusCliente.Equals(1) || statusCliente.Equals(3) || statusCliente.Equals(5))
                        {
                            MessageBox.Show("Cliente Bloqueado! Verifique o cadastro.", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            if (Funcoes.LeParametro(6, "190", false).Equals("S"))
                            {
                                frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                senhaSup.ShowDialog();
                                if (senhaSup.sValida.Equals(false))
                                {
                                    MessageBox.Show("Não é possível prosseguir a venda sem a senha de autorização!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    Limpar();
                                    return;
                                }
                            }
                        }

                        if (Convert.ToDouble(dtRetorno.Rows[0]["CON_DESCONTO"]) != 0)
                        {
                            txtAjuste.Text = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[0]["CON_DESCONTO"]) * -1);
                        }

                        txtProduto.Focus();
                    }
                    else if (dtRetorno.Rows.Count > 1)
                    {
                        using (frmBuscaForn t = new frmBuscaForn("cliente"))
                        {
                            t.dgCliFor.DataSource = dtRetorno;
                            t.ShowDialog();

                            if (!String.IsNullOrEmpty(t.cfNome))
                            {
                                Principal.doctoCliente = t.cfDocto;
                                codCliente = Convert.ToString(t.cfCodigo);
                                txtCliEmp.Text = t.cfNome;
                                doctoCliente = t.cfDocto;
                                IDCliente = t.cfID;
                                statusCliente = t.cfStatus;
                                if (!String.IsNullOrEmpty(t.conCodigo))
                                {
                                    txtCodCliEmp.Text = t.conCodigo;
                                    wsConID = t.conId;
                                    wsEmpresa = t.conCodigo;
                                    wsConWeb = t.conWeb;
                                    wsConCodConv = t.conCodConv;
                                }
                                else
                                {
                                    if (codCliente == "0")
                                    {
                                        txtCodCliEmp.Text = IDCliente.ToString();
                                    }
                                    else
                                        txtCodCliEmp.Text = codCliente;
                                }

                                if (wsConWeb == "1" && wsNumCartao != "")
                                {
                                    MessageBox.Show("Cliente Drogabella/Plantão Card, necessário informar Cartão busca pelo nome não gera Autorização.\n"
                                        + "Escolher a opção Benefícios-Drogabella ou Plantão Card e buscar as informações do Cliente pelo Cartao!", "Validação Convênio",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    txtCodCliEmp.Text = "";
                                    txtCliEmp.Text = "";
                                    txtTelefone.Text = "";
                                    txtTelefone.Enabled = true;
                                    txtCodCliEmp.Enabled = true;
                                    txtCliEmp.Enabled = true;
                                    return;
                                }

                                if (t.conDesconto != 0)
                                {
                                    txtAjuste.Text = String.Format("{0:N}", t.conDesconto * -1);
                                }

                                txtCodCliEmp.Enabled = false;

                                dtRetorno = dCliente.DadosClienteFiltro(Convert.ToString(IDCliente), 2, out filtro);
                                if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString()))
                                {
                                    MessageBox.Show(dtRetorno.Rows[0]["CF_OBSERVACAO"].ToString().ToUpper(), "Observação do Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                if (statusCliente.Equals(1) || statusCliente.Equals(3) || statusCliente.Equals(5))
                                {
                                    MessageBox.Show("Cliente Bloqueado! Verifique o cadastro.", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    if (Funcoes.LeParametro(6, "190", false).Equals("S"))
                                    {
                                        frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                                        senhaSup.ShowDialog();
                                        if (senhaSup.sValida.Equals(false))
                                        {
                                            MessageBox.Show("Não é possível prosseguir a venda sem a senha de autorização!", "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            Limpar();
                                            return;
                                        }
                                    }
                                }
                                DadosDaVendaCliente(Principal.doctoCliente);
                                btnUltimosProdutos.Enabled = true;
                                txtProduto.Focus();
                            }
                            else
                            {
                                txtCodCliEmp.Enabled = true;
                                txtCodCliEmp.Text = "";
                                txtCodCliEmp.Focus();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTelefone.Focus();
                    }
                }
                else
                {
                    codCliente = Funcoes.LeParametro(6, "114", true);
                    txtCodCliEmp.Text = codCliente.ToString();
                    dtRetorno = dCliente.DadosClienteFiltro(codCliente.ToString(), 4, out filtro);
                    IDCliente = Convert.ToInt32(dtRetorno.Rows[0]["CF_ID"]);
                    doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                    Principal.doctoCliente = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                    DadosDaVendaCliente(Principal.doctoCliente);
                    txtCodCliEmp.Enabled = false;
                    txtTelefone.Enabled = false;
                    txtCliEmp.Enabled = false;
                    txtCliEmp.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                    btnUltimosProdutos.Enabled = true;
                    txtProduto.Focus();
                }
            }
        }

        private void txtVUni_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtPQtde_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtSTotal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtPDesc_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtVDesc_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtVTotal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtEstoque_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtComissao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCusto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnExcluir_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtGAjuste_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void BTN_ConsultaABCFarma_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void BTN_ConsultaABCFarma_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.BTN_ConsultaABCFarma, "F5 - Abrir Chamado");
        }

        private void listCupom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (listCupom.SelectedIndex >= 5)
                {
                    Cursor = Cursors.WaitCursor;

                    if (!listCupom.SelectedItem.ToString().Substring(0, 2).Equals("de"))
                    {
                        exclusaoComanda = true;
                        if (Funcoes.LeParametro(6, "362", true).Equals("S") && vendaDeComanda && !String.IsNullOrEmpty(txtComanda.Text) && exclusaoComanda.Equals(true))
                        {
                            if (MessageBox.Show("Exclusão não permitida! Deseja solicitar liberação?", "Liberação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                            {
                                frmVenSupSenha senha = new frmVenSupSenha(0);
                                senha.ShowDialog();
                                if (senha.sValida.Equals(false))
                                {
                                    exclusaoComanda = false;
                                }
                            }
                            else
                                exclusaoComanda = false;
                        }

                        if (exclusaoComanda)
                        {
                            linha_resultado = tProdutos.Select("COD_BARRA = '" + listCupom.SelectedItem.ToString().Substring(0, 13).Trim() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                linha_resultado[0].Delete();
                                InserirProdCupom(tProdutos);
                                limparProd();
                                IsentaTaxaEntrega();
                                CamposProdutoInvisivel();

                                object sumObject;
                                sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

                                lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

                                txtProduto.Focus();
                            }
                        }
                        else
                        {
                            txtProduto.Focus();
                        }
                    }

                    Cursor = Cursors.Default;
                }
            }
            else
                teclaAtalho(e);
        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtPQtde_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPQtde.ReadOnly.Equals(false))
                {
                    if (txtPQtde.Text.Trim().Equals("") || Convert.ToInt32(txtPQtde.Text.Trim()).Equals(0))
                    {
                        MessageBox.Show("Quantidade não pode em branco ou zero", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtPQtde.Text = linha_resultado[0]["PROD_QTDE"].ToString();
                        txtSTotal.Text = String.Format("{0:N}", Convert.ToInt32(txtPQtde.Text.Trim()) * Convert.ToDecimal(txtVUni.Text.Trim()));
                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                        txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                        txtVTotal.Text = String.Format("{0:N}", Math.Round(Convert.ToDecimal(txtVUni.Text.Trim()) - Math.Abs((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100), 2) * Convert.ToInt32(txtPQtde.Text.Trim()));
                        txtPQtde.Focus();
                    }
                    else
                    {
                        txtSTotal.Text = String.Format("{0:N}", Convert.ToInt32(txtPQtde.Text.Trim()) * Convert.ToDecimal(txtVUni.Text.Trim()));
                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100, 2));
                        txtPDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVDesc.Text.Trim()) * 100) / Convert.ToDecimal(txtSTotal.Text.Trim()), 2));
                        txtVTotal.Text = String.Format("{0:N}", Math.Round(Convert.ToDecimal(txtVUni.Text.Trim()) - Math.Abs((Convert.ToDecimal(txtVUni.Text.Trim()) * Convert.ToDecimal(txtPDesc.Text.Trim())) / 100), 2) * Convert.ToInt32(txtPQtde.Text.Trim()));
                        txtSTotal.Focus();
                    }


                    linha_resultado = tProdutos.Select("COD_BARRA = '" + lblNumCodBarras.Text.Trim() + "'");
                    if (linha_resultado.Length != 0)
                    {
                        dtRetorno = Funcoes.IdentificaProdutoEmPromocao(Principal.empAtual, Convert.ToInt32(linha_resultado[0]["PROD_ID"]), 0, 0, 0);
                        if (dtRetorno.Rows.Count == 1)
                        {
                            if (dtRetorno.Rows[0]["DESCONTO_PROGRESSIVO"].ToString() == "S" && Convert.ToInt32(txtPQtde.Text) == Convert.ToInt32(dtRetorno.Rows[0]["LEVE"]))
                            {
                                if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                {
                                    if (DateTime.Now.Day >= Convert.ToInt32(dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(dtRetorno.Rows[0]["DATA_FIM"]))
                                    {
                                        linha_resultado[0].BeginEdit();
                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                        txtPDesc.Text = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                        txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text) * Convert.ToInt32(txtPQtde.Text))
                                            - Math.Round((Convert.ToDecimal(txtSTotal.Text) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                        linha_resultado[0].EndEdit();

                                        AtualizaDesconto();
                                    }
                                }
                                else if (dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                {
                                    if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                        && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                    {
                                        linha_resultado[0].BeginEdit();
                                        linha_resultado[0]["PROD_PROMO"] = "S";
                                        txtPDesc.Text = String.Format("{0:N}", Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"])));
                                        txtVDesc.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtSTotal.Text) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2));
                                        txtVTotal.Text = String.Format("{0:N}", Math.Round((Convert.ToDecimal(txtVUni.Text) * Convert.ToInt32(txtPQtde.Text))
                                            - Math.Round((Convert.ToDecimal(txtSTotal.Text) * Math.Abs(Convert.ToDecimal(dtRetorno.Rows[0]["PORCENTAGEM"]))) / 100, 2), 2));
                                        linha_resultado[0].EndEdit();

                                        AtualizaDesconto();
                                    }
                                }
                            }
                        }
                    }

                }
                else
                    txtSTotal.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtualizaDesconto()
        {
            linha_resultado = tProdutos.Select("COD_BARRA = '" + lblNumCodBarras.Text + "'");
            linha_resultado[0].BeginEdit();
            linha_resultado[0]["PROD_VUNI"] = String.Format("{0:N}", txtVUni.Text);
            linha_resultado[0]["PROD_QTDE"] = txtPQtde.Text;
            linha_resultado[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", txtSTotal.Text);
            linha_resultado[0]["PROD_DPORC"] = String.Format("{0:N}", txtPDesc.Text);
            linha_resultado[0]["PROD_DVAL"] = String.Format("{0:N}", txtVDesc.Text);
            linha_resultado[0]["PROD_TOTAL"] = String.Format("{0:N}", txtVTotal.Text);
            linha_resultado[0].EndEdit();

            InserirProdCupom(tProdutos);
            limparProd();
            IsentaTaxaEntrega();
            CamposProdutoInvisivel();

            object sumObject;
            sumObject = tProdutos.Compute("Sum(PROD_QTDE)", "");

            lblRegistros.Text = "TOTAL DE PRODUTOS: " + sumObject.ToString();

            txtProduto.Focus();
        }

        public bool ValidaProdutosPortalDaDrogaria()
        {
            try
            {
                ExibirMensagem("Validando Venda. Aguarde...");

                Cursor = Cursors.WaitCursor;

                double precoBruto = 0;
                double precoLiquido = 0;
                double valorReceber = 0;

                bool diferenca = false;
                var buscaDados = new BeneficioNovartisProdutos();
                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    dtRetorno = buscaDados.BuscaProdutosPorNsuProdCodigo(vendaNumeroNSU, tProdutos.Rows[i]["COD_BARRA"].ToString());
                    if (dtRetorno.Rows.Count == 0)
                    {
                        diferenca = true;
                        var produtosAutorizados = new BeneficioNovartisProdutos(
                              vendaNumeroNSU,
                              0,
                              tProdutos.Rows[i]["COD_BARRA"].ToString(),
                              Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]),
                              Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]),
                              Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])),
                              Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])),
                              Convert.ToDouble(tProdutos.Rows[i]["PROD_DPORC"]) * -1,
                              "V",
                              "",
                              DateTime.Now,
                              0,
                              "N",
                              DateTime.Now,
                              Principal.usuario
                              );

                        if (!produtosAutorizados.InsereRegistros(produtosAutorizados, false))
                        {
                            return false;
                        }

                        precoBruto += Math.Round(Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2);
                        precoLiquido += Math.Round(Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2);
                        valorReceber += Math.Round(Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])) * Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), 2);
                    }
                    else
                    {
                        diferenca = true;
                        if (Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]) != Convert.ToInt32(dtRetorno.Rows[0]["QTD"]))
                        {
                            var dadosAtualiza = new BeneficioNovartisProdutos();
                            dadosAtualiza.AtualizaQtdEValoresProdutos(vendaNumeroNSU, Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"]), Convert.ToDouble(Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"])),
                                 Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])),
                                 Convert.ToDouble(tProdutos.Rows[i]["PROD_VUNI"]) - (Convert.ToDouble(tProdutos.Rows[i]["PROD_DVAL"]) / Convert.ToInt32(tProdutos.Rows[i]["PROD_QTDE"])),
                                 Convert.ToDouble(tProdutos.Rows[i]["PROD_DPORC"]) * -1, tProdutos.Rows[i]["COD_BARRA"].ToString());

                        }
                    }
                }

                if (diferenca)
                {
                    //VALIDA DESCONTOS 
                    Negocio.Beneficios.PortalDaDrogaria vendaPortal = new Negocio.Beneficios.PortalDaDrogaria();

                    vendaPortal.EnviaTransacao("0", vendaNumeroNSU, wsNumCartao, true);

                    string caminho;
                    List<RetornoPortalDrogaria> retono = vendaPortal.RetornoTransacaoVenda(this, out caminho);

                    if (retono.Count > 0)
                    {
                        var novartisProduto = new BeneficioNovartisProdutos();

                        if (!String.IsNullOrEmpty(erro))
                        {
                            if (Convert.ToInt32(erro) > 9 && Convert.ToInt32(erro) != 43)
                            {
                                Cursor = Cursors.Default;
                                MessageBox.Show(mensagem, "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                vendaPortal.LimparPastas();

                                var excluiEfetivado = new BeneficioNovartisEfetivado();
                                excluiEfetivado.ExcluirDadosPorNSU(vendaNumeroNSU);

                                novartisProduto.ExcluirDadosPorNSU(vendaNumeroNSU);

                                var excluiPendentes = new BeneficioNovartisPendente();
                                excluiPendentes.ExcluirDadosPorNSU(vendaNumeroNSU);

                                var excluiComandas = new BeneficioNovartisComanda();
                                excluiComandas.ExcluirDadosPorNSU(vendaNumeroNSU);

                                return false;
                            }
                            else if (Convert.ToInt32(erro) == 43)
                            {
                                Cursor = Cursors.Default;
                                MessageBox.Show(mensagem, "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                vendaPortal.LimparPastas();

                                var excluiEfetivado = new BeneficioNovartisEfetivado();
                                excluiEfetivado.ExcluirDadosPorNSU(vendaNumeroNSU);

                                novartisProduto.ExcluirDadosPorNSU(vendaNumeroNSU);

                                var excluiPendentes = new BeneficioNovartisPendente();
                                excluiPendentes.ExcluirDadosPorNSU(vendaNumeroNSU);

                                var excluiComandas = new BeneficioNovartisComanda();
                                excluiComandas.ExcluirDadosPorNSU(vendaNumeroNSU);

                                return false;
                            }
                        }

                        var qtdeAutorizada = File.ReadAllLines(caminho)
                            .Where(l => l.StartsWith("901-000"))
                            .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                            .ToList();

                        DataTable dtRetorno = novartisProduto.BuscaProdutosPorNsu(vendaNumeroNSU);

                        for (int i = 0; i < Convert.ToInt32(qtdeAutorizada[0]); i++)
                        {
                            var codBarras = File.ReadAllLines(caminho)
                                  .Where(l => l.StartsWith("902-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                  .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                  .ToList();

                            DataRow[] linha_resultado = dtRetorno.Select("PROD_CODIGO = '" + codBarras[0].ToString() + "'");
                            if (linha_resultado.Length != 0)
                            {
                                var qtde = File.ReadAllLines(caminho)
                                  .Where(l => l.StartsWith("905-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                  .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                  .ToList();

                                //VERIFICA QUANTIDADE DIFERENTE//
                                if (Convert.ToInt32(qtde[0]) != Convert.ToInt32(linha_resultado[0]["QTD"]))
                                {
                                    novartisProduto.AtualizaQtdeProduto(vendaNumeroNSU, Convert.ToInt32(qtde[0]), codBarras[0].ToString());

                                    dtRetorno = novartisProduto.BuscaProdutosPorNsu(vendaNumeroNSU);

                                    DataRow[] linha_resultado3 = tProdutos.Select("COD_BARRA = '" + codBarras[0].ToString() + "'");
                                    if (linha_resultado3.Length != 0)
                                    {
                                        linha_resultado3[0].BeginEdit();
                                        linha_resultado3[0]["PROD_VUNI"] = String.Format("{0:N}", dtRetorno.Rows[i]["PRE_BRUTO"]);
                                        linha_resultado3[0]["PROD_QTDE"] = Convert.ToInt32(qtde[0]);
                                        linha_resultado3[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(qtde[0]));
                                        linha_resultado3[0]["BENEFICIO"] = "S";
                                        double descontoPor = Math.Round(((Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(qtde[0])) * Math.Abs(Convert.ToDouble(dtRetorno.Rows[i]["VALOR_DESCONTO"]))) / 100, 2);
                                        linha_resultado3[0]["PROD_DVAL"] = String.Format("{0:N}", descontoPor);
                                        linha_resultado3[0]["PROD_DPORC"] = String.Format("{0:N}", dtRetorno.Rows[i]["VALOR_DESCONTO"]);
                                        linha_resultado3[0]["PROD_TOTAL"] = String.Format("{0:N}", (Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(qtde[0])) - descontoPor);
                                        linha_resultado3[0].EndEdit();
                                        InserirProdCupom(tProdutos);
                                    }

                                    MessageBox.Show("PRODUTO: " + codBarras[0].ToString() + "\n\nQUANTIDADE AUTORIZADA DIVERGENTE DA SOLICTADA. QTDE AUTORIZADA: " + qtde[0] + "\nATENÇÃO NA TELA DE VENDA PARA QTDE AUTORIZADA!", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                var status = File.ReadAllLines(caminho)
                                    .Where(l => l.StartsWith("919-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                    .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                    .ToList();

                                //VERIFICA STATUS PRODUTO
                                if (status[0].ToString() != "00")
                                {
                                    if (status[0].ToString() == "41")
                                    {
                                        if (MessageBox.Show("PRODUTO: " + codBarras[0].ToString() + "\n\nSem desconto na central. Continua a Venda do Produto com o Desconto?", "ATENÇÃO", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                                        {
                                            MessageBox.Show("PRODUTO: " + codBarras[0].ToString() + "\n\nEXCLUÍDO NA TELA DE VENDA PARA PRODUTOS E VALORES!", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                            novartisProduto.ExcluiProdutoPorNSUeCodBarras(vendaNumeroNSU, codBarras[0].ToString());

                                            DataRow[] linha_resultado2 = tProdutos.Select("COD_BARRA = '" + codBarras[0].ToString() + "'");
                                            linha_resultado2[0].Delete();
                                            InserirProdCupom(tProdutos);
                                        }
                                    }
                                }

                                if (status[0].ToString() == "00")
                                {
                                    var desconto = File.ReadAllLines(caminho)
                                   .Where(l => l.StartsWith("909-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                   .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                   .ToList();

                                    //VERIFICA DE DESCONTO FOI ALTERADO//
                                    if ((Convert.ToDouble(desconto[0]) / 100) != Convert.ToDouble(linha_resultado[0]["VALOR_DESCONTO"]) * -1)
                                    {
                                        novartisProduto.AtualizaDescontoProduto(vendaNumeroNSU, (Convert.ToDouble(desconto[0]) / 100) * -1, codBarras[0].ToString());

                                        dtRetorno = novartisProduto.BuscaProdutosPorNsu(vendaNumeroNSU);

                                        DataRow[] linha_resultado1 = tProdutos.Select("COD_BARRA = '" + codBarras[0].ToString() + "'");
                                        if (linha_resultado1.Length != 0)
                                        {
                                            linha_resultado1[0].BeginEdit();
                                            linha_resultado1[0]["PROD_VUNI"] = String.Format("{0:N}", dtRetorno.Rows[i]["PRE_BRUTO"]);
                                            linha_resultado1[0]["PROD_QTDE"] = dtRetorno.Rows[i]["QTD"];
                                            linha_resultado1[0]["PROD_SUBTOTAL"] = String.Format("{0:N}", Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                            linha_resultado1[0]["BENEFICIO"] = "S";
                                            double descontoPor = Math.Round(((Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * Math.Abs(Convert.ToDouble(dtRetorno.Rows[i]["VALOR_DESCONTO"]))) / 100, 2);
                                            linha_resultado1[0]["PROD_DVAL"] = String.Format("{0:N}", descontoPor);
                                            linha_resultado1[0]["PROD_DPORC"] = String.Format("{0:N}", dtRetorno.Rows[i]["VALOR_DESCONTO"]);
                                            linha_resultado1[0]["PROD_TOTAL"] = String.Format("{0:N}", (Convert.ToDouble(dtRetorno.Rows[i]["PRE_BRUTO"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) - descontoPor);
                                            linha_resultado1[0].EndEdit();
                                            InserirProdCupom(tProdutos);
                                        }

                                        MessageBox.Show("PRODUTO: " + codBarras[0].ToString() + "\n\nDESCONTO DIFERENTE DO AUTORIZADO.VERIFIQUE A TELA DE VENDAS!", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }



                                    var valorLiquido = File.ReadAllLines(caminho)
                                          .Where(l => l.StartsWith("911-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                          .ToList();

                                    var valorAReceber = File.ReadAllLines(caminho)
                                         .Where(l => l.StartsWith("912-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                         .ToList();

                                    if (((Convert.ToDouble(valorLiquido[0]) / 100) - (Convert.ToDouble(valorAReceber[0]) / 100)) != 0)
                                    {
                                        double subsidio = ((Convert.ToDouble(valorLiquido[0]) / 100) - (Convert.ToDouble(valorAReceber[0]) / 100));

                                        novartisProduto.AtualizaSubsidioProduto(vendaNumeroNSU, subsidio, codBarras[0].ToString());
                                    }
                                }
                            }
                        }
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                OcultarMensagem();
            }
        }

        public void GeraIDVenda()
        {
            //Ira realizar um retardo no processamento caso algum caixa realize a mesma situação e não faça a mesma operação, ao mesmo tempo;
            System.Threading.Thread.Sleep(new Random().Next(800,6000)); 
            vendaID = Funcoes.GeraIDLong("VENDAS_CONTADOR", "VENDA_ID");
            var atualizaID = new VendasContador();

            atualizaID.AtualizaDados(vendaID);

            inserir = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((lblProdutoOferecer.Left + lblProdutoOferecer.Width) < this.Width)
            {
                lblProdutoOferecer.Left += 10;
            }
            else
                lblProdutoOferecer.Left = -50;


            if (lblProdutoOferecer.ForeColor == Color.Black)
            {
                lblProdutoOferecer.ForeColor = Color.Red;
            }
            else if (lblProdutoOferecer.ForeColor == Color.Red)
            {
                lblProdutoOferecer.ForeColor = Color.Black;
            }
        }

        private void txtBuscaPreco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtBuscaPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtBuscaPreco.Text) && !Convert.ToDouble(txtBuscaPreco.Text).Equals(0))
                {
                    var buscaProduto = new Produto();
                    dtRetorno = buscaProduto.BuscaDadosProdutosPesquisaDetalhada(Principal.estAtual, Principal.empAtual, "", "", txtBuscaPreco.Text.Replace(",", "."), "", "");
                    if (dtRetorno.Rows.Count == 1)
                    {
                        txtProduto.Text = dtRetorno.Rows[0]["PROD_CODIGO"].ToString();
                    }
                    else
                    {
                        //FORM DE BUSCA DE PRODUTOS//
                        using (frmBuscaProd buscaProd = new frmBuscaProd(true))
                        {
                            buscaProd.txtPreco.Text = txtBuscaPreco.Text.ToUpper();
                            buscaProd.ShowDialog();
                        }
                        if (!String.IsNullOrEmpty(Principal.codBarra))
                        {
                            split = Principal.codBarra.Split(',');
                            if (split.Length > 0)
                            {
                                for (int i = 0; i < split.Length; i++)
                                {
                                    if (!String.IsNullOrEmpty(split[i]))
                                    {
                                        txtProduto.Text = split[i];
                                        lerProdutos();
                                    }
                                }
                            }
                            else
                            {
                                txtProduto.Text = Principal.codBarra;
                                lerProdutos();
                            }
                        }
                        else
                        {
                            txtProduto.Text = "";
                            txtProduto.Focus();
                        }
                    }                                           
                }
                else
                {
                    txtBuscaPreco.Text = "0,00";
                    txtProduto.Focus();
                }
            }
        }

        private void frmVenda_FormClosing(object sender, FormClosingEventArgs e)
        {
            Principal.contFormularios = Principal.contFormularios - 1;
        }

        private void lblTotal_Click(object sender, EventArgs e)
        {

        }

        private void txtVendedor_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbVendedor, txtVendedor);
            if (cmbVendedor.SelectedIndex != -1)
            {
                txtComanda.Focus();
            }
        }

        private void cmbVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbVendedor.SelectedIndex != -1)
                {
                    MessageBox.Show("Necessário selecionar o Vendedor", "SGPharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbVendedor.Focus();
                }
            }
        }

        private void cmbVendedor_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            txtVendedor.Text = Convert.ToString(cmbVendedor.SelectedValue);
        }

        private void cmbVendedor_KeyDown_1(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbVendedor_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbVendedor.SelectedIndex != -1)
                    ValidarVendedor();
            }
        }

        private void frmVenda_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            tProdutos = tabelaProdutos();
            Limpar();

            ws = new GestaoAdministrativa.WSConvenio.wsconvenio();
        }

        public bool VendaControlado()
        {
            try
            {
                frmVenSNGPC controlado = new frmVenSNGPC(tProdutos, this);
                controlado.ShowDialog();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
