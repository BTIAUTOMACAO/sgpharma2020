﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenBeneficios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenBeneficios));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button6 = new System.Windows.Forms.Button();
            this.btnOrizon = new System.Windows.Forms.Button();
            this.btnPortalDrogaria = new System.Windows.Forms.Button();
            this.btnVidaLink = new System.Windows.Forms.Button();
            this.btnEpharma = new System.Windows.Forms.Button();
            this.btnFPopular = new System.Windows.Forms.Button();
            this.btnRpc = new System.Windows.Forms.Button();
            this.btnDrogabella = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(202)))), ((int)(((byte)(226)))));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.groupBox1.Location = new System.Drawing.Point(6, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 242);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Benefícios";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.btnOrizon);
            this.panel1.Controls.Add(this.btnPortalDrogaria);
            this.panel1.Controls.Add(this.btnVidaLink);
            this.panel1.Controls.Add(this.btnEpharma);
            this.panel1.Controls.Add(this.btnFPopular);
            this.panel1.Controls.Add(this.btnRpc);
            this.panel1.Controls.Add(this.btnDrogabella);
            this.panel1.Location = new System.Drawing.Point(5, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(357, 219);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 63);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(239, 146);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(110, 64);
            this.button6.TabIndex = 8;
            this.button6.Text = "pharmalink";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            // 
            // btnOrizon
            // 
            this.btnOrizon.BackColor = System.Drawing.Color.Black;
            this.btnOrizon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrizon.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrizon.ForeColor = System.Drawing.Color.Black;
            this.btnOrizon.Image = ((System.Drawing.Image)(resources.GetObject("btnOrizon.Image")));
            this.btnOrizon.Location = new System.Drawing.Point(123, 146);
            this.btnOrizon.Name = "btnOrizon";
            this.btnOrizon.Size = new System.Drawing.Size(110, 64);
            this.btnOrizon.TabIndex = 7;
            this.btnOrizon.UseVisualStyleBackColor = false;
            this.btnOrizon.Click += new System.EventHandler(this.btnOrizon_Click);
            this.btnOrizon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnOrizon_KeyDown);
            this.btnOrizon.MouseHover += new System.EventHandler(this.btnOrizon_MouseHover);
            // 
            // btnPortalDrogaria
            // 
            this.btnPortalDrogaria.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.btnPortalDrogaria.Enabled = false;
            this.btnPortalDrogaria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPortalDrogaria.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPortalDrogaria.ForeColor = System.Drawing.Color.Black;
            this.btnPortalDrogaria.Image = ((System.Drawing.Image)(resources.GetObject("btnPortalDrogaria.Image")));
            this.btnPortalDrogaria.Location = new System.Drawing.Point(7, 146);
            this.btnPortalDrogaria.Name = "btnPortalDrogaria";
            this.btnPortalDrogaria.Size = new System.Drawing.Size(110, 64);
            this.btnPortalDrogaria.TabIndex = 6;
            this.btnPortalDrogaria.UseVisualStyleBackColor = false;
            this.btnPortalDrogaria.Click += new System.EventHandler(this.btnPortalDrogaria_Click);
            this.btnPortalDrogaria.MouseHover += new System.EventHandler(this.btnPortalDrogaria_MouseHover);
            // 
            // btnVidaLink
            // 
            this.btnVidaLink.Enabled = false;
            this.btnVidaLink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVidaLink.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVidaLink.ForeColor = System.Drawing.Color.Black;
            this.btnVidaLink.Image = ((System.Drawing.Image)(resources.GetObject("btnVidaLink.Image")));
            this.btnVidaLink.Location = new System.Drawing.Point(239, 77);
            this.btnVidaLink.Name = "btnVidaLink";
            this.btnVidaLink.Size = new System.Drawing.Size(110, 64);
            this.btnVidaLink.TabIndex = 5;
            this.btnVidaLink.UseVisualStyleBackColor = true;
            this.btnVidaLink.Click += new System.EventHandler(this.btnVidaLink_Click);
            this.btnVidaLink.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnVidaLink_KeyDown);
            this.btnVidaLink.MouseHover += new System.EventHandler(this.btnVidaLink_MouseHover);
            // 
            // btnEpharma
            // 
            this.btnEpharma.BackColor = System.Drawing.Color.Transparent;
            this.btnEpharma.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEpharma.BackgroundImage")));
            this.btnEpharma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEpharma.Enabled = false;
            this.btnEpharma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEpharma.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEpharma.ForeColor = System.Drawing.Color.Black;
            this.btnEpharma.Location = new System.Drawing.Point(123, 76);
            this.btnEpharma.Name = "btnEpharma";
            this.btnEpharma.Size = new System.Drawing.Size(110, 64);
            this.btnEpharma.TabIndex = 4;
            this.btnEpharma.UseVisualStyleBackColor = false;
            this.btnEpharma.Click += new System.EventHandler(this.btnEpharma_Click);
            this.btnEpharma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnEpharma_KeyDown);
            this.btnEpharma.MouseHover += new System.EventHandler(this.btnEpharma_MouseHover);
            // 
            // btnFPopular
            // 
            this.btnFPopular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.btnFPopular.Enabled = false;
            this.btnFPopular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFPopular.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFPopular.ForeColor = System.Drawing.Color.Black;
            this.btnFPopular.Image = ((System.Drawing.Image)(resources.GetObject("btnFPopular.Image")));
            this.btnFPopular.Location = new System.Drawing.Point(7, 76);
            this.btnFPopular.Name = "btnFPopular";
            this.btnFPopular.Size = new System.Drawing.Size(110, 64);
            this.btnFPopular.TabIndex = 3;
            this.btnFPopular.UseVisualStyleBackColor = false;
            this.btnFPopular.Click += new System.EventHandler(this.btnFPopular_Click);
            this.btnFPopular.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFPopular_KeyDown);
            this.btnFPopular.MouseHover += new System.EventHandler(this.btnFPopular_MouseHover);
            // 
            // btnRpc
            // 
            this.btnRpc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRpc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRpc.ForeColor = System.Drawing.Color.Black;
            this.btnRpc.Image = ((System.Drawing.Image)(resources.GetObject("btnRpc.Image")));
            this.btnRpc.Location = new System.Drawing.Point(239, 7);
            this.btnRpc.Name = "btnRpc";
            this.btnRpc.Size = new System.Drawing.Size(110, 64);
            this.btnRpc.TabIndex = 1;
            this.btnRpc.UseVisualStyleBackColor = true;
            this.btnRpc.Click += new System.EventHandler(this.btnRpc_Click);
            this.btnRpc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnRpc_KeyDown);
            this.btnRpc.MouseHover += new System.EventHandler(this.btnRpc_MouseHover);
            // 
            // btnDrogabella
            // 
            this.btnDrogabella.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDrogabella.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDrogabella.ForeColor = System.Drawing.Color.Black;
            this.btnDrogabella.Image = ((System.Drawing.Image)(resources.GetObject("btnDrogabella.Image")));
            this.btnDrogabella.Location = new System.Drawing.Point(123, 7);
            this.btnDrogabella.Name = "btnDrogabella";
            this.btnDrogabella.Size = new System.Drawing.Size(110, 64);
            this.btnDrogabella.TabIndex = 0;
            this.btnDrogabella.UseVisualStyleBackColor = true;
            this.btnDrogabella.Click += new System.EventHandler(this.btnDrogabella_Click);
            this.btnDrogabella.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDrogabella_KeyDown);
            this.btnDrogabella.MouseHover += new System.EventHandler(this.btnDrogabella_MouseHover);
            // 
            // frmVenBeneficios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(202)))), ((int)(((byte)(226)))));
            this.ClientSize = new System.Drawing.Size(378, 245);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenBeneficios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Benefícios";
            this.Load += new System.EventHandler(this.frmVenBeneficios_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenBeneficios_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button button6;
        public System.Windows.Forms.Button btnOrizon;
        public System.Windows.Forms.Button btnPortalDrogaria;
        public System.Windows.Forms.Button btnVidaLink;
        public System.Windows.Forms.Button btnEpharma;
        public System.Windows.Forms.Button btnRpc;
        public System.Windows.Forms.Button btnDrogabella;
        public System.Windows.Forms.Button btnFPopular;
    }
}