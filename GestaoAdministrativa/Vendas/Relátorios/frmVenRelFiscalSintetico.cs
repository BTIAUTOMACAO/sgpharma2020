﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelFiscalSintetico : Form
    {
        private frmVenRelFiscais relatorio;
        private DataTable dtRelatorio;

        public frmVenRelFiscalSintetico(frmVenRelFiscais frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }
        private void frmVenRelFiscalSintetico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
         {
             var fiscalsintetico = new ReportDataSource("fiscalSintetico", dtRelatorio);
             this.rwRelatorio.LocalReport.DataSources.Clear();
             this.rwRelatorio.LocalReport.DataSources.Add(fiscalsintetico);
         }


    }
}
