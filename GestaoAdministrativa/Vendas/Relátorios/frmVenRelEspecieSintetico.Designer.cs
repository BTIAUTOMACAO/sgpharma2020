﻿namespace GestaoAdministrativa.Vendas.Relátorios
{
    partial class frmVenRelEspecieSintetico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelEspecieSintetico));
            this.rwRelatorio = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rwRelatorio
            // 
            this.rwRelatorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rwRelatorio.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Vendas.Relátorios.relEspecieSintetico.rdlc";
            this.rwRelatorio.Location = new System.Drawing.Point(0, 0);
            this.rwRelatorio.Name = "rwRelatorio";
            this.rwRelatorio.Size = new System.Drawing.Size(994, 578);
            this.rwRelatorio.TabIndex = 0;
            // 
            // frmVenRelEspecieSintetico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rwRelatorio);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVenRelEspecieSintetico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVenRelEspecieSintetico";
            this.Load += new System.EventHandler(this.frmVenRelEspecieSintetico_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rwRelatorio;
    }
}