﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelDescontosConcedidos : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Desc. Concedidos");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelDescontosConcedidos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelDescontosConcedidos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbUsuario.SelectedIndex = -1;
            rdbDia.Checked = true;
            dtInicial.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(170, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelDescontosConcedidos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                    return;
                }

                DataTable dtRetorno = new DataTable();
                var relatorio = new Produto();

                if (rdbDia.Checked)
                {
                    dtRetorno = relatorio.DescontosConcedidosPorDia(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, cmbUsuario.SelectedIndex <= 0 ? 0 : Convert.ToInt32(cmbUsuario.SelectedValue));
                    if(dtRetorno.Rows.Count > 0)
                    {
                        frmVenDescConcedidos visRelatorio = new frmVenDescConcedidos(this, dtRetorno,"Filtro por Dia","Data");
                        visRelatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontrado", "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if(rdbForma.Checked)
                {
                    dtRetorno = relatorio.DescontosConcedidosPorFormaDePagamento(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, cmbUsuario.SelectedIndex <= 0 ? 0 : Convert.ToInt32(cmbUsuario.SelectedValue));
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenDescConcedidos visRelatorio = new frmVenDescConcedidos(this, dtRetorno, "Filtro por Forma de Pagamento", "Forma de Pagto.");
                        visRelatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontrado", "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    dtRetorno = relatorio.DescontosConcedidosPorDepartamento(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, cmbUsuario.SelectedIndex <= 0 ? 0 : Convert.ToInt32(cmbUsuario.SelectedValue));
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenDescConcedidos visRelatorio = new frmVenDescConcedidos(this, dtRetorno, "Filtro por Departamento", "Departamento");
                        visRelatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontrado", "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Descontos Concedidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenRelDescontosConcedidos_Shown(object sender, EventArgs e)
        {
            Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

            DataRow row = Principal.dtPesq.NewRow();
            Principal.dtPesq.Rows.InsertAt(row, 0);
            cmbUsuario.DataSource = Principal.dtPesq;
            cmbUsuario.DisplayMember = "COL_NOME";
            cmbUsuario.ValueMember = "COL_CODIGO";
            cmbUsuario.SelectedIndex = 0;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
