﻿namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    partial class frmVenRelProdutosPorPeriodoSintetico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelProdutosPorPeriodoSintetico));
            this.rpwProdutoSintetico = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwProdutoSintetico
            // 
            this.rpwProdutoSintetico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwProdutoSintetico.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Vendas.Relátorios.ArquivosReport.relVenRelProdutosPorPeriodo" +
    "Sintetico.rdlc";
            this.rpwProdutoSintetico.Location = new System.Drawing.Point(0, 0);
            this.rpwProdutoSintetico.Name = "rpwProdutoSintetico";
            this.rpwProdutoSintetico.Size = new System.Drawing.Size(994, 578);
            this.rpwProdutoSintetico.TabIndex = 0;
            // 
            // frmVenRelProdutosPorPeriodoSintetico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwProdutoSintetico);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVenRelProdutosPorPeriodoSintetico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendas Reletório Produtos Por Período Sintético";
            this.Load += new System.EventHandler(this.frmVenRelProdutosPorPeriodoSintetico_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwProdutoSintetico;
    }
}