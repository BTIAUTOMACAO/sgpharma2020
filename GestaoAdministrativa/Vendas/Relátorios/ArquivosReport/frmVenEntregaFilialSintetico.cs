﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenEntregaFilialSintetico : Form
    {
        private frmVenRelEntregaFilial relatorio;
        private DataTable dtRelatorio;

        public frmVenEntregaFilialSintetico(frmVenRelEntregaFilial frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmVenEntregaFilialSintetico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[5];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("status", relatorio.cmbStatus.Text);
            parametro[4] = new ReportParameter("vendedor", relatorio.cmbUsuario.Text == "" ? "TODOS VENDEDORES" : relatorio.cmbUsuario.Text);
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var sintetico = new ReportDataSource("sintetico", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(sintetico);
        }
    }
}
