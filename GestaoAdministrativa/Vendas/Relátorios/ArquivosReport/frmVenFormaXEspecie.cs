﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenFormaXEspecie : Form
    {
        private frmVenRelFormaXEspecie relatorio;
        private DataTable dtRelatorio;

        public frmVenFormaXEspecie(frmVenRelFormaXEspecie frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmVenFormaXEspecie_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var relatorio = new ReportDataSource("relatorio", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(relatorio);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new VendasDados();
            DataTable dtItens = tItens.BuscaFormaPagtoPorVendaID(e.Parameters["VENDA_ID"].Values.First(), Principal.empAtual, Principal.estAtual);

            var itens = new ReportDataSource("dsForma", dtItens);
            e.DataSources.Add(itens);

            dtItens = tItens.BuscaEspeciePorVendaID(e.Parameters["VENDA_ID"].Values.First(), Principal.empAtual, Principal.estAtual);
            var especie = new ReportDataSource("dsEspecie", dtItens);
            e.DataSources.Add(especie);
        }
    }
}
