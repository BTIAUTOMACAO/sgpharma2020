﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmRelatorio : Form
    {
        private string relatorio;
        private DataTable dtRelatorio;
        private DateTime dataIni;
        private DateTime dataFim;

        public frmRelatorio(string tipo, DataTable dtRetorno, DateTime dtInicial, DateTime dtFinal)
        {
            InitializeComponent();
            relatorio = tipo;
            dtRelatorio = dtRetorno;
            dataIni = dtInicial;
            dataFim = dtFinal;
        }

        private void frmRelatorio_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaRelatorio(relatorio);
            MontaDadosEstabelecimento();
            
            this.rwRelatorio.RefreshReport();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dataIni.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", dataFim.ToString("dd/MM/yyyy"));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio(string tipoRelatorio)
        {
            switch (tipoRelatorio)
            {
                case "comissaoAnalitico":
                    var comissaoAnalitico = new ReportDataSource("comissaoAnalitico", dtRelatorio);
                    this.rwRelatorio.LocalReport.DataSources.Clear();
                    this.rwRelatorio.LocalReport.DataSources.Add(comissaoAnalitico);
                    break;

            }
           
        }
    }
}
