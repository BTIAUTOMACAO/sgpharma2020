﻿namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    partial class frmVenRelProdutosPorPeriodoAnalitico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelProdutosPorPeriodoAnalitico));
            this.rpwProdutoAnalitico = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwProdutoAnalitico
            // 
            this.rpwProdutoAnalitico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwProdutoAnalitico.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Vendas.Relátorios.ArquivosReport.relVenRelProdutosPorPeriodo" +
    "Analitico.rdlc";
            this.rpwProdutoAnalitico.Location = new System.Drawing.Point(0, 0);
            this.rpwProdutoAnalitico.Name = "rpwProdutoAnalitico";
            this.rpwProdutoAnalitico.Size = new System.Drawing.Size(994, 578);
            this.rpwProdutoAnalitico.TabIndex = 0;
            // 
            // frmVenRelProdutosPorPeriodoAnalitico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwProdutoAnalitico);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVenRelProdutosPorPeriodoAnalitico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendas Reletório Produtos Por Período Sintético Analítico";
            this.Load += new System.EventHandler(this.frmVenRelProdutosPorPeriodoAnalitico_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwProdutoAnalitico;
    }
}