﻿namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    partial class frmVenSintetico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rwRelatorio = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rwRelatorio
            // 
            this.rwRelatorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rwRelatorio.Font = new System.Drawing.Font("Arial", 9.75F);
            this.rwRelatorio.ForeColor = System.Drawing.Color.Black;
            this.rwRelatorio.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Vendas.Relátorios.ArquivosReport.relVenSintetico.rdlc";
            this.rwRelatorio.Location = new System.Drawing.Point(0, 0);
            this.rwRelatorio.Name = "rwRelatorio";
            this.rwRelatorio.Size = new System.Drawing.Size(994, 578);
            this.rwRelatorio.TabIndex = 0;
            this.rwRelatorio.Load += new System.EventHandler(this.rwRelatorio_Load);
            // 
            // frmVenSintetico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rwRelatorio);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVenSintetico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVenSintetico";
            this.Load += new System.EventHandler(this.frmVenSintetico_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rwRelatorio;
    }
}