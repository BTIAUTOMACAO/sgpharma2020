﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenRelProdutosPorPeriodoAnalitico : Form
    {
        DateTime dtInicial, dtFinal;
        bool quebraDia, ordemAlfabetica;
        string tipoQuebra, filtroProduto, colCodigo;

        public frmVenRelProdutosPorPeriodoAnalitico(DateTime inicial, DateTime final, bool quebraPorDia, bool ordem, string quebra, string filtro, string codVendedor)
        {
            dtInicial = Convert.ToDateTime(inicial.ToShortDateString() + " 00:00:00");
            dtFinal = Convert.ToDateTime(final.ToShortDateString() + " 23:59:59");
            quebraDia = quebraPorDia;
            ordemAlfabetica = ordem;
            tipoQuebra = quebra;
            colCodigo = codVendedor;
            filtroProduto = filtro;

            InitializeComponent();
        }

        private void frmVenRelProdutosPorPeriodoAnalitico_Load(object sender, EventArgs e)
        {
            CarregasDadosEstabelecimento();
            CarregasDadosRelatorio();
            this.rpwProdutoAnalitico.RefreshReport();
        }

        public void CarregasDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial.ToShortDateString());
            parametro[2] = new ReportParameter("dtFinal", dtFinal.ToShortDateString());
            this.rpwProdutoAnalitico.LocalReport.SetParameters(parametro);
        }

        public void CarregasDadosRelatorio()
        {
            ProdutoDetalhe produto = new ProdutoDetalhe();
            DataTable dtVendas = produto.BuscaProdutosVendosPorPeriodoAnalitico(dtInicial, dtFinal, quebraDia, ordemAlfabetica, tipoQuebra, filtroProduto, colCodigo);
            var produ = new ReportDataSource("produtos", dtVendas);
            rpwProdutoAnalitico.LocalReport.DataSources.Clear();
            rpwProdutoAnalitico.LocalReport.DataSources.Add(produ);
        }
    }
}
