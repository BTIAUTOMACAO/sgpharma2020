﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenCurvaAbc : Form
    {
        private frmVenRelCurvaAbc relatorio;
        private DataTable dtRelatorio;

        public frmVenCurvaAbc(frmVenRelCurvaAbc frmRelatorio, DataTable dtBusca)
        {
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
            InitializeComponent();
        }

        private void frmVenCurvaAbc_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.reportViewer1.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }


        public void MontaRelatorio()
        {
            var curva = new ReportDataSource("dsCurva", dtRelatorio);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(curva);
        }
    }
}
