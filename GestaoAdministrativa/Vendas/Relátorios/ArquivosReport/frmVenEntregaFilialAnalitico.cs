﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenEntregaFilialAnalitico : Form
    {
        private frmVenRelEntregaFilial relatorio;
        private DataTable dtRelatorio;

        public frmVenEntregaFilialAnalitico(frmVenRelEntregaFilial frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmVenEntregaFilialAnalitico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[5];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("status", relatorio.cmbStatus.Text);
            parametro[4] = new ReportParameter("vendedor", relatorio.cmbUsuario.Text == "" ? "TODOS VENDEDORES" : relatorio.cmbUsuario.Text);
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var analitico = new ReportDataSource("analitico", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(analitico);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new EntregaFilialItens();
            DataTable dtItens = tItens.BuscaItensEntrega(e.Parameters["CODIGO"].Values.First(), Principal.empAtual, Principal.estAtual);

            var itens = new ReportDataSource("dsItens", dtItens);
            e.DataSources.Add(itens);
        }
    }
}
