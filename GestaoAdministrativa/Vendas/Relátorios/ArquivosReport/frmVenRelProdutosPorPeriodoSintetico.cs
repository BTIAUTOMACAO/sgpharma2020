﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenRelProdutosPorPeriodoSintetico : Form
    {
        DateTime dtInicial, dtFinal;
        bool quebraDia;
        string codVendedor, filtros;

        public frmVenRelProdutosPorPeriodoSintetico(DateTime inicial, DateTime final, bool quebraPorDia, string colCodigo, string filtro)
        {
            dtInicial = Convert.ToDateTime(inicial.ToShortDateString() + " 00:00:00");
            dtFinal = Convert.ToDateTime(final.ToShortDateString() + " 23:59:59");
            quebraDia = quebraPorDia;
            codVendedor = colCodigo;
            filtros = filtro;
            InitializeComponent();
        }

        private void frmVenRelProdutosPorPeriodoSintetico_Load(object sender, EventArgs e)
        {
            CarregasDadosEstabelecimento();
            CarregasDadosRelatorio();
            this.rpwProdutoSintetico.RefreshReport();
        }

        public void CarregasDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial.ToShortDateString());
            parametro[2] = new ReportParameter("dtFinal", dtFinal.ToShortDateString());
            this.rpwProdutoSintetico.LocalReport.SetParameters(parametro);
        }

        public void CarregasDadosRelatorio()
        {
            ProdutoDetalhe produto = new ProdutoDetalhe();
            DataTable dtVendas = produto.BuscaProdutosVendosPorPeriodo(dtInicial, dtFinal, quebraDia, codVendedor, filtros);
            var departamento = new ReportDataSource("ProdutosSintetico", dtVendas);
            rpwProdutoSintetico.LocalReport.DataSources.Clear();
            rpwProdutoSintetico.LocalReport.DataSources.Add(departamento);
        }
    }
}
