﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios.ArquivosReport
{
    public partial class frmVenRelFormaPagtoSintetico : Form
    {
        private frmVenRelFormaPagamento relatorio;
        private DataTable dtRelatorio;

        public frmVenRelFormaPagtoSintetico(frmVenRelFormaPagamento frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }


        private void frmVenRelFormaPagtoSintetico_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                MontaDadosEstabelecimento();
                MontaRelatorio();
                this.rwRelatorio.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void MontaDadosEstabelecimento()
        {
            try
            {
                ReportParameter[] parametro = new ReportParameter[3];
                parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
                parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
                parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
                this.rwRelatorio.LocalReport.SetParameters(parametro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void MontaRelatorio()
        {
            try
            {
                var sintetico = new ReportDataSource("formaPagtoSintetico", dtRelatorio);
                this.rwRelatorio.LocalReport.DataSources.Clear();
                this.rwRelatorio.LocalReport.DataSources.Add(sintetico);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
