﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelCustoxProduto : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Produtos X Custo");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelCustoxProduto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }
        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenRelCustoxProduto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelCustoxProduto>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtFinal.Value = DateTime.Now;
            dtInicial.Value = DateTime.Now;
            rdbNenhuma.Checked = true;
            rdbAlfabetica.Checked = true;
            filtroProd = "";
            dtInicial.Focus();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAlfabetica.Focus();
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAlfabetica_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAlfabetica_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbNumerico.Focus();
        }

        private void rdbNumerico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbNumerico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbNenhuma.Focus();
        }

        private void rdbNenhuma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbDepartamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSubClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void chkQuebraPorDia_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnPesquisar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelCustoxProduto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtFinal.Value.CompareTo(dtInicial.Value) == -1)
                {
                    MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Rel. Produtos mais Vendidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var relatorio = new Produto();
                if (rdbNenhuma.Checked)
                {
                   dtRetorno = relatorio.ProdutosXCustosSemQuebra(Principal.empAtual, Principal.estAtual, dtInicial.Text, dtFinal.Text, rdbAlfabetica.Checked == true ? 0 : 1, filtroProd);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenProdXCustoSemQuebra relatorioDados = new frmVenProdXCustoSemQuebra(this, dtRetorno);
                        relatorioDados.Text = "Produtos X Custo";
                        relatorioDados.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtInicial.Focus();
                    }
                }
                else
                {
                    dtRetorno = relatorio.ProdutosXCustosQuebra(Principal.empAtual, Principal.estAtual, dtInicial.Text, dtFinal.Text, rdbAlfabetica.Checked == true ? 0 : 1, filtroProd, rdbDepartamento.Checked == true ? 0 : rdbClasse.Checked == true ? 1 : 2);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenProdXCustoQuebra dados = new frmVenProdXCustoQuebra(this, dtRetorno);
                        dados.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Rel. Produto X Custo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtInicial.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
