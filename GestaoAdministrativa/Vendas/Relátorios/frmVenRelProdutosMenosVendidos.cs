﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelProdutosMenosVendidos : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Prod. Sem Movimento");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelProdutosMenosVendidos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelProdutosMenosVendidos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtLinhas.Text = "";
            cmbDepartamento.SelectedIndex = -1;
            dtInicial.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(190, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelProdutosMenosVendidos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenRelProdutosMenosVendidos_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", false, "DEP_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbDepartamento.DataSource = Principal.dtPesq;
                cmbDepartamento.DisplayMember = "DEP_DESCR";
                cmbDepartamento.ValueMember = "DEP_CODIGO";
                cmbDepartamento.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtLinhas.Focus();
        }

        private void txtLinhas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbDepartamento.Focus();
        }

        private void cmbDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;

            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtLinhas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbDepartamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelProdutosMenosVendidos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;

                DataTable dtRetorno = new DataTable();
                var relatorio = new Produto();
                dtRetorno = relatorio.ProdutosSemMovimento(Principal.estAtual, Principal.empAtual, txtLinhas.Text == "" ? "0" : txtLinhas.Text, cmbDepartamento.SelectedIndex <= 0 ? 0 : Convert.ToInt32(cmbDepartamento.SelectedValue), dtInicial.Text, dtFinal.Text);
                if (dtRetorno.Rows.Count > 0)
                {
                    frmRelProdutosSemMovimento visRelatorio = new Relátorios.frmRelProdutosSemMovimento(this, dtRetorno);
                    visRelatorio.Text = "Relatório de Produtos Sem Movimento";
                    visRelatorio.Show();
                }
                else
                {
                    MessageBox.Show("Nenhum Registro Encontrado", "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produtos sem Movimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
