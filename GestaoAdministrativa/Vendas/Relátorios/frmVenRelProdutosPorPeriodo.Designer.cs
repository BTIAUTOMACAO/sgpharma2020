﻿namespace GestaoAdministrativa.Vendas.Relátorios
{
    partial class frmVenRelProdutosPorPeriodo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelProdutosPorPeriodo));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmbVendedor = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbSintetico = new System.Windows.Forms.RadioButton();
            this.rdbAnalitico = new System.Windows.Forms.RadioButton();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.grpQuebra = new System.Windows.Forms.GroupBox();
            this.rdbSubClasse = new System.Windows.Forms.RadioButton();
            this.rdbClasse = new System.Windows.Forms.RadioButton();
            this.rdbDepartamento = new System.Windows.Forms.RadioButton();
            this.rdbNenhuma = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkQuebraPorDia = new System.Windows.Forms.CheckBox();
            this.grpOrdenar = new System.Windows.Forms.GroupBox();
            this.rdbNumerico = new System.Windows.Forms.RadioButton();
            this.rdbAlfabetica = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpQuebra.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.grpOrdenar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.btnPesquisar);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.btnFiltrar);
            this.groupBox4.Controls.Add(this.grpQuebra);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.grpOrdenar);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(12, 4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(932, 207);
            this.groupBox4.TabIndex = 161;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtros";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cmbVendedor);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(398, 92);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(207, 49);
            this.groupBox6.TabIndex = 161;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Vendedor";
            // 
            // cmbVendedor
            // 
            this.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVendedor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVendedor.ForeColor = System.Drawing.Color.Black;
            this.cmbVendedor.FormattingEnabled = true;
            this.cmbVendedor.Location = new System.Drawing.Point(11, 18);
            this.cmbVendedor.Name = "cmbVendedor";
            this.cmbVendedor.Size = new System.Drawing.Size(183, 24);
            this.cmbVendedor.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(17, 26);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(314, 58);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Período Data";
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(208, 21);
            this.dtFinal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(100, 22);
            this.dtFinal.TabIndex = 6;
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(56, 20);
            this.dtInicial.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(100, 22);
            this.dtInicial.TabIndex = 5;
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(162, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inicial";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(780, 151);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(142, 48);
            this.btnPesquisar.TabIndex = 160;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            this.btnPesquisar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPesquisar_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdbSintetico);
            this.groupBox3.Controls.Add(this.rdbAnalitico);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(337, 26);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(190, 58);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo";
            // 
            // rdbSintetico
            // 
            this.rdbSintetico.AutoSize = true;
            this.rdbSintetico.ForeColor = System.Drawing.Color.Navy;
            this.rdbSintetico.Location = new System.Drawing.Point(104, 20);
            this.rdbSintetico.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbSintetico.Name = "rdbSintetico";
            this.rdbSintetico.Size = new System.Drawing.Size(82, 20);
            this.rdbSintetico.TabIndex = 1;
            this.rdbSintetico.TabStop = true;
            this.rdbSintetico.Text = "Sintético";
            this.rdbSintetico.UseVisualStyleBackColor = true;
            this.rdbSintetico.CheckedChanged += new System.EventHandler(this.rdbSintetico_CheckedChanged);
            this.rdbSintetico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSintetico_KeyDown);
            // 
            // rdbAnalitico
            // 
            this.rdbAnalitico.AutoSize = true;
            this.rdbAnalitico.ForeColor = System.Drawing.Color.Navy;
            this.rdbAnalitico.Location = new System.Drawing.Point(16, 21);
            this.rdbAnalitico.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbAnalitico.Name = "rdbAnalitico";
            this.rdbAnalitico.Size = new System.Drawing.Size(82, 20);
            this.rdbAnalitico.TabIndex = 0;
            this.rdbAnalitico.TabStop = true;
            this.rdbAnalitico.Text = "Analítico";
            this.rdbAnalitico.UseVisualStyleBackColor = true;
            this.rdbAnalitico.CheckedChanged += new System.EventHandler(this.rdbAnalitico_CheckedChanged);
            this.rdbAnalitico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAnalitico_KeyDown);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(683, 151);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(91, 48);
            this.btnFiltrar.TabIndex = 159;
            this.btnFiltrar.Text = "Filtros";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // grpQuebra
            // 
            this.grpQuebra.Controls.Add(this.rdbSubClasse);
            this.grpQuebra.Controls.Add(this.rdbClasse);
            this.grpQuebra.Controls.Add(this.rdbDepartamento);
            this.grpQuebra.Controls.Add(this.rdbNenhuma);
            this.grpQuebra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpQuebra.Location = new System.Drawing.Point(533, 26);
            this.grpQuebra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Name = "grpQuebra";
            this.grpQuebra.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Size = new System.Drawing.Size(389, 58);
            this.grpQuebra.TabIndex = 2;
            this.grpQuebra.TabStop = false;
            this.grpQuebra.Text = "Quebra por";
            // 
            // rdbSubClasse
            // 
            this.rdbSubClasse.AutoSize = true;
            this.rdbSubClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbSubClasse.Location = new System.Drawing.Point(293, 23);
            this.rdbSubClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbSubClasse.Name = "rdbSubClasse";
            this.rdbSubClasse.Size = new System.Drawing.Size(92, 20);
            this.rdbSubClasse.TabIndex = 3;
            this.rdbSubClasse.TabStop = true;
            this.rdbSubClasse.Text = "SubClasse";
            this.rdbSubClasse.UseVisualStyleBackColor = true;
            this.rdbSubClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSubClasse_KeyDown);
            // 
            // rdbClasse
            // 
            this.rdbClasse.AutoSize = true;
            this.rdbClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbClasse.Location = new System.Drawing.Point(217, 23);
            this.rdbClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbClasse.Name = "rdbClasse";
            this.rdbClasse.Size = new System.Drawing.Size(67, 20);
            this.rdbClasse.TabIndex = 2;
            this.rdbClasse.TabStop = true;
            this.rdbClasse.Text = "Classe";
            this.rdbClasse.UseVisualStyleBackColor = true;
            this.rdbClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbClasse_KeyDown);
            // 
            // rdbDepartamento
            // 
            this.rdbDepartamento.AutoSize = true;
            this.rdbDepartamento.ForeColor = System.Drawing.Color.Navy;
            this.rdbDepartamento.Location = new System.Drawing.Point(95, 23);
            this.rdbDepartamento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbDepartamento.Name = "rdbDepartamento";
            this.rdbDepartamento.Size = new System.Drawing.Size(116, 20);
            this.rdbDepartamento.TabIndex = 1;
            this.rdbDepartamento.TabStop = true;
            this.rdbDepartamento.Text = "Departamento";
            this.rdbDepartamento.UseVisualStyleBackColor = true;
            this.rdbDepartamento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbDepartamento_KeyDown);
            // 
            // rdbNenhuma
            // 
            this.rdbNenhuma.AutoSize = true;
            this.rdbNenhuma.ForeColor = System.Drawing.Color.Navy;
            this.rdbNenhuma.Location = new System.Drawing.Point(9, 23);
            this.rdbNenhuma.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbNenhuma.Name = "rdbNenhuma";
            this.rdbNenhuma.Size = new System.Drawing.Size(87, 20);
            this.rdbNenhuma.TabIndex = 0;
            this.rdbNenhuma.TabStop = true;
            this.rdbNenhuma.Text = "Nenhuma";
            this.rdbNenhuma.UseVisualStyleBackColor = true;
            this.rdbNenhuma.CheckedChanged += new System.EventHandler(this.rdbNenhuma_CheckedChanged);
            this.rdbNenhuma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbNenhuma_KeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkQuebraPorDia);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(17, 92);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Size = new System.Drawing.Size(156, 49);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Quebra por ";
            // 
            // chkQuebraPorDia
            // 
            this.chkQuebraPorDia.AutoSize = true;
            this.chkQuebraPorDia.ForeColor = System.Drawing.Color.Navy;
            this.chkQuebraPorDia.Location = new System.Drawing.Point(9, 21);
            this.chkQuebraPorDia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkQuebraPorDia.Name = "chkQuebraPorDia";
            this.chkQuebraPorDia.Size = new System.Drawing.Size(129, 20);
            this.chkQuebraPorDia.TabIndex = 1;
            this.chkQuebraPorDia.Text = "Quebrar por Dia";
            this.chkQuebraPorDia.UseVisualStyleBackColor = true;
            this.chkQuebraPorDia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkQuebraPorDia_KeyDown);
            // 
            // grpOrdenar
            // 
            this.grpOrdenar.Controls.Add(this.rdbNumerico);
            this.grpOrdenar.Controls.Add(this.rdbAlfabetica);
            this.grpOrdenar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpOrdenar.Location = new System.Drawing.Point(185, 92);
            this.grpOrdenar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpOrdenar.Name = "grpOrdenar";
            this.grpOrdenar.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpOrdenar.Size = new System.Drawing.Size(207, 49);
            this.grpOrdenar.TabIndex = 2;
            this.grpOrdenar.TabStop = false;
            this.grpOrdenar.Text = "Ordem";
            // 
            // rdbNumerico
            // 
            this.rdbNumerico.AutoSize = true;
            this.rdbNumerico.ForeColor = System.Drawing.Color.Navy;
            this.rdbNumerico.Location = new System.Drawing.Point(112, 20);
            this.rdbNumerico.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbNumerico.Name = "rdbNumerico";
            this.rdbNumerico.Size = new System.Drawing.Size(87, 20);
            this.rdbNumerico.TabIndex = 1;
            this.rdbNumerico.TabStop = true;
            this.rdbNumerico.Text = "Numérica";
            this.rdbNumerico.UseVisualStyleBackColor = true;
            this.rdbNumerico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbNumerico_KeyDown);
            // 
            // rdbAlfabetica
            // 
            this.rdbAlfabetica.AutoSize = true;
            this.rdbAlfabetica.ForeColor = System.Drawing.Color.Navy;
            this.rdbAlfabetica.Location = new System.Drawing.Point(16, 20);
            this.rdbAlfabetica.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbAlfabetica.Name = "rdbAlfabetica";
            this.rdbAlfabetica.Size = new System.Drawing.Size(90, 20);
            this.rdbAlfabetica.TabIndex = 0;
            this.rdbAlfabetica.TabStop = true;
            this.rdbAlfabetica.Text = "Alfabética";
            this.rdbAlfabetica.UseVisualStyleBackColor = true;
            this.rdbAlfabetica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAlfabetica_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 162;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Location = new System.Drawing.Point(7, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 495);
            this.panel2.TabIndex = 43;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(252, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Relatório de Produtos por Período";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenRelProdutosPorPeriodo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenRelProdutosPorPeriodo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmVenRelProdutosPorPeriodo";
            this.Load += new System.EventHandler(this.frmVenRelProdutosPorPeriodo_Load);
            this.Shown += new System.EventHandler(this.frmVenRelProdutosPorPeriodo_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenRelProdutosPorPeriodo_KeyDown);
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpQuebra.ResumeLayout(false);
            this.grpQuebra.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.grpOrdenar.ResumeLayout(false);
            this.grpOrdenar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grpOrdenar;
        private System.Windows.Forms.RadioButton rdbNumerico;
        private System.Windows.Forms.RadioButton rdbAlfabetica;
        private System.Windows.Forms.GroupBox grpQuebra;
        private System.Windows.Forms.RadioButton rdbSubClasse;
        private System.Windows.Forms.RadioButton rdbClasse;
        private System.Windows.Forms.RadioButton rdbDepartamento;
        private System.Windows.Forms.RadioButton rdbNenhuma;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbSintetico;
        private System.Windows.Forms.RadioButton rdbAnalitico;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DateTimePicker dtFinal;
        public System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chkQuebraPorDia;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cmbVendedor;
    }
}