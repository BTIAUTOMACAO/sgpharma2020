﻿namespace GestaoAdministrativa.Vendas.Relátorios
{
    partial class frmVenRelCustoxProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelCustoxProduto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.grpQuebra = new System.Windows.Forms.GroupBox();
            this.rdbSubClasse = new System.Windows.Forms.RadioButton();
            this.rdbClasse = new System.Windows.Forms.RadioButton();
            this.rdbDepartamento = new System.Windows.Forms.RadioButton();
            this.rdbNenhuma = new System.Windows.Forms.RadioButton();
            this.grpOrdenar = new System.Windows.Forms.GroupBox();
            this.rdbNumerico = new System.Windows.Forms.RadioButton();
            this.rdbAlfabetica = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpQuebra.SuspendLayout();
            this.grpOrdenar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 163;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Location = new System.Drawing.Point(7, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 495);
            this.panel2.TabIndex = 43;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.btnPesquisar);
            this.groupBox4.Controls.Add(this.btnFiltrar);
            this.groupBox4.Controls.Add(this.grpQuebra);
            this.groupBox4.Controls.Add(this.grpOrdenar);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(12, 4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(932, 149);
            this.groupBox4.TabIndex = 161;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtros";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 26);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(314, 58);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Período Data";
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(208, 23);
            this.dtFinal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(100, 22);
            this.dtFinal.TabIndex = 6;
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(56, 22);
            this.dtInicial.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(100, 22);
            this.dtInicial.TabIndex = 5;
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(162, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inicial";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(780, 92);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(142, 48);
            this.btnPesquisar.TabIndex = 160;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            this.btnPesquisar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPesquisar_KeyDown);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(683, 92);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(91, 48);
            this.btnFiltrar.TabIndex = 159;
            this.btnFiltrar.Text = "Filtros";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // grpQuebra
            // 
            this.grpQuebra.Controls.Add(this.rdbSubClasse);
            this.grpQuebra.Controls.Add(this.rdbClasse);
            this.grpQuebra.Controls.Add(this.rdbDepartamento);
            this.grpQuebra.Controls.Add(this.rdbNenhuma);
            this.grpQuebra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpQuebra.Location = new System.Drawing.Point(533, 26);
            this.grpQuebra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Name = "grpQuebra";
            this.grpQuebra.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Size = new System.Drawing.Size(389, 58);
            this.grpQuebra.TabIndex = 2;
            this.grpQuebra.TabStop = false;
            this.grpQuebra.Text = "Quebra por";
            // 
            // rdbSubClasse
            // 
            this.rdbSubClasse.AutoSize = true;
            this.rdbSubClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbSubClasse.Location = new System.Drawing.Point(293, 23);
            this.rdbSubClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbSubClasse.Name = "rdbSubClasse";
            this.rdbSubClasse.Size = new System.Drawing.Size(92, 20);
            this.rdbSubClasse.TabIndex = 3;
            this.rdbSubClasse.TabStop = true;
            this.rdbSubClasse.Text = "SubClasse";
            this.rdbSubClasse.UseVisualStyleBackColor = true;
            this.rdbSubClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSubClasse_KeyDown);
            // 
            // rdbClasse
            // 
            this.rdbClasse.AutoSize = true;
            this.rdbClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbClasse.Location = new System.Drawing.Point(217, 23);
            this.rdbClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbClasse.Name = "rdbClasse";
            this.rdbClasse.Size = new System.Drawing.Size(67, 20);
            this.rdbClasse.TabIndex = 2;
            this.rdbClasse.TabStop = true;
            this.rdbClasse.Text = "Classe";
            this.rdbClasse.UseVisualStyleBackColor = true;
            this.rdbClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbClasse_KeyDown);
            // 
            // rdbDepartamento
            // 
            this.rdbDepartamento.AutoSize = true;
            this.rdbDepartamento.ForeColor = System.Drawing.Color.Navy;
            this.rdbDepartamento.Location = new System.Drawing.Point(95, 23);
            this.rdbDepartamento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbDepartamento.Name = "rdbDepartamento";
            this.rdbDepartamento.Size = new System.Drawing.Size(116, 20);
            this.rdbDepartamento.TabIndex = 1;
            this.rdbDepartamento.TabStop = true;
            this.rdbDepartamento.Text = "Departamento";
            this.rdbDepartamento.UseVisualStyleBackColor = true;
            this.rdbDepartamento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbDepartamento_KeyDown);
            // 
            // rdbNenhuma
            // 
            this.rdbNenhuma.AutoSize = true;
            this.rdbNenhuma.ForeColor = System.Drawing.Color.Navy;
            this.rdbNenhuma.Location = new System.Drawing.Point(9, 23);
            this.rdbNenhuma.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbNenhuma.Name = "rdbNenhuma";
            this.rdbNenhuma.Size = new System.Drawing.Size(87, 20);
            this.rdbNenhuma.TabIndex = 0;
            this.rdbNenhuma.TabStop = true;
            this.rdbNenhuma.Text = "Nenhuma";
            this.rdbNenhuma.UseVisualStyleBackColor = true;
            this.rdbNenhuma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbNenhuma_KeyDown);
            // 
            // grpOrdenar
            // 
            this.grpOrdenar.Controls.Add(this.rdbNumerico);
            this.grpOrdenar.Controls.Add(this.rdbAlfabetica);
            this.grpOrdenar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpOrdenar.Location = new System.Drawing.Point(329, 26);
            this.grpOrdenar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpOrdenar.Name = "grpOrdenar";
            this.grpOrdenar.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpOrdenar.Size = new System.Drawing.Size(191, 58);
            this.grpOrdenar.TabIndex = 2;
            this.grpOrdenar.TabStop = false;
            this.grpOrdenar.Text = "Ordem";
            // 
            // rdbNumerico
            // 
            this.rdbNumerico.AutoSize = true;
            this.rdbNumerico.ForeColor = System.Drawing.Color.Navy;
            this.rdbNumerico.Location = new System.Drawing.Point(103, 22);
            this.rdbNumerico.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbNumerico.Name = "rdbNumerico";
            this.rdbNumerico.Size = new System.Drawing.Size(87, 20);
            this.rdbNumerico.TabIndex = 1;
            this.rdbNumerico.TabStop = true;
            this.rdbNumerico.Text = "Numérica";
            this.rdbNumerico.UseVisualStyleBackColor = true;
            this.rdbNumerico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbNumerico_KeyDown);
            this.rdbNumerico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbNumerico_KeyPress);
            // 
            // rdbAlfabetica
            // 
            this.rdbAlfabetica.AutoSize = true;
            this.rdbAlfabetica.ForeColor = System.Drawing.Color.Navy;
            this.rdbAlfabetica.Location = new System.Drawing.Point(7, 22);
            this.rdbAlfabetica.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbAlfabetica.Name = "rdbAlfabetica";
            this.rdbAlfabetica.Size = new System.Drawing.Size(90, 20);
            this.rdbAlfabetica.TabIndex = 0;
            this.rdbAlfabetica.TabStop = true;
            this.rdbAlfabetica.Text = "Alfabética";
            this.rdbAlfabetica.UseVisualStyleBackColor = true;
            this.rdbAlfabetica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAlfabetica_KeyDown);
            this.rdbAlfabetica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbAlfabetica_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(224, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Relatório de Produtos X Custo";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenRelCustoxProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenRelCustoxProduto";
            this.Text = "frmVenRelCustoxProduto";
            this.Load += new System.EventHandler(this.frmVenRelCustoxProduto_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenRelCustoxProduto_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpQuebra.ResumeLayout(false);
            this.grpQuebra.PerformLayout();
            this.grpOrdenar.ResumeLayout(false);
            this.grpOrdenar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DateTimePicker dtFinal;
        public System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.GroupBox grpQuebra;
        private System.Windows.Forms.RadioButton rdbSubClasse;
        private System.Windows.Forms.RadioButton rdbClasse;
        private System.Windows.Forms.RadioButton rdbDepartamento;
        private System.Windows.Forms.RadioButton rdbNenhuma;
        private System.Windows.Forms.GroupBox grpOrdenar;
        private System.Windows.Forms.RadioButton rdbNumerico;
        private System.Windows.Forms.RadioButton rdbAlfabetica;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
    }
}