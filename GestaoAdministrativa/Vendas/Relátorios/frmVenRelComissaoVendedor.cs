﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelComissaoVendedor : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Comissão Vendedor");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelComissaoVendedor(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelComissaoVendedor_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(170, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelComissaoVendedor>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbFormaPagamento.SelectedIndex = -1;
            cmbUsuario.SelectedIndex = -1;
            rdbResumido.Checked = true;
            dtInicial.Focus();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var busca = new VendasDados();
                DataTable dtRetorno = new DataTable();
                if (rdbResumido.Checked)
                {
                    dtRetorno = busca.BuscaRelatorioComissaoPorVendedorResumido(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0,Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                        cmbFormaPagamento.SelectedIndex > 0 ? Convert.ToInt32(cmbFormaPagamento.SelectedValue) : 0);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenComissaoResumido relatorio = new frmVenComissaoResumido(this, dtRetorno);
                        relatorio.Text = "Comissão por Vendedor Resumido";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
                else if(rdbSintetico.Checked)
                {
                    dtRetorno = busca.BuscaRelatorioComissaoPorVendedorSintetico(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                        cmbFormaPagamento.SelectedIndex);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenComissaoSintetico relatorio = new frmVenComissaoSintetico(this, dtRetorno);
                        relatorio.Text = "Comissão por Vendedor Sintético";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
                else if (rdbAnalitico.Checked)
                {
                    dtRetorno = busca.BuscaRelatorioComissaoPorVendedorAnalitico(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                        cmbFormaPagamento.SelectedIndex);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenComissaoAnalitico relatorio = new frmVenComissaoAnalitico(this,dtRetorno);
                        relatorio.Text = "Comissão por Vendedor Analítico";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmVenRelComissaoVendedor_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "COL_NOME";
                cmbUsuario.ValueMember = "COL_CODIGO";
                cmbUsuario.SelectedIndex = 0;

                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("FORMA_ID", "FORMA_DESCRICAO", "FORMAS_PAGAMENTO", false, "FORMA_LIBERADO= 'S'");

                DataRow row1 = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row1, 0);
                cmbFormaPagamento.DataSource = Principal.dtPesq;
                cmbFormaPagamento.DisplayMember = "FORMA_DESCRICAO";
                cmbFormaPagamento.ValueMember = "FORMA_ID";
                cmbFormaPagamento.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Vendedor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAnalitico.Focus();
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSintetico.Focus();
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbResumido.Focus();
        }

        private void rdbResumido_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbResumido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUsuario.Focus();
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbFormaPagamento.Focus();
        }

        private void cmbFormaPagamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbFormaPagamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelComissaoVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;

            }
        }
    }
}
