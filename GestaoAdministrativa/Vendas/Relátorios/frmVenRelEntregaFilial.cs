﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelEntregaFilial : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Venda Período");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelEntregaFilial(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelEntregaFilial_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelEntregaFilial>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbUsuario.SelectedIndex = -1;
            rdbSintetico.Checked = true;
            rdbSolicitadas.Checked = true;
            cmbStatus.SelectedIndex = 1;
            dtInicial.Focus();
        }

        private void frmVenRelEntregaFilial_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "COL_NOME";
                cmbUsuario.ValueMember = "COL_CODIGO";
                cmbUsuario.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if(cmbStatus.SelectedIndex == -1)
                {
                    Principal.mensagem = "Necessário selecionar um Status";
                    Funcoes.Avisa();
                    cmbStatus.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var busca = new VendasDados();
                DataTable dtRetorno = new DataTable();

                if(rdbSolicitadas.Checked)
                {
                    if(rdbSintetico.Checked)
                    {
                        dtRetorno = busca.BuscaEntregaSolicitadaSintetico(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                            cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0,cmbStatus.SelectedIndex == 0 ? "E" : cmbStatus.SelectedIndex == 1 ? "F" : "P");

                        if(dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenEntregaFilialSintetico relatorio = new frmVenEntregaFilialSintetico(this, dtRetorno);
                            relatorio.Text = "Vendas Entrega Filial Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaEntregaSolicitadaAnalitico(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                            cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, cmbStatus.SelectedIndex == 0 ? "E" : cmbStatus.SelectedIndex == 1 ? "F" : "P");

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenEntregaFilialAnalitico relatorio = new frmVenEntregaFilialAnalitico(this, dtRetorno);
                            relatorio.Text = "Vendas Entrega Filial Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                }
                else
                {
                    if (rdbSintetico.Checked)
                    {
                        dtRetorno = busca.BuscaRelatorioPeriodoSintetico(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                       "00:00:00", "23:59:59", cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, 0, "ENTREGA");

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenEntregaSintetico relatorio = new frmVenEntregaSintetico(this, dtRetorno);
                            relatorio.Text = "Vendas Entrega Filial Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaRelatorioPeriodoAnalitico(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                       "00:00:00", "23:59:59", cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, 0, "ENTREGA");

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenEntregaAnalitico relatorio = new frmVenEntregaAnalitico(this, dtRetorno);
                            relatorio.Text = "Vendas Entrega Filial Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Venda Entrega Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                dtFinal.Focus();
            }
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUsuario.Focus();
        }

        private void cmbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbStatus.Focus();
        }

        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSolicitadas.Focus();
        }

        private void rdbSolicitadas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbRealizadas.Focus();
        }

        private void rdbRealizadas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAnalitico.Focus();
        }

        private void rdbAnalitico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSintetico.Focus();
        }

        private void rdbSintetico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbStatus_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSolicitadas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbRealizadas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelEntregaFilial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
