﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelProdutosPorPeriodo : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Produtos Período");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelProdutosPorPeriodo(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }
        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenRelProdutosPorPeriodo_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelProdutosPorPeriodo>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtFinal.Value = DateTime.Now;
            dtInicial.Value = DateTime.Now;
            dtInicial.MaxDate = DateTime.Now;
            dtFinal.MaxDate = DateTime.Now;
            rdbAnalitico.Checked = true;
            rdbNenhuma.Checked = true;
            rdbAlfabetica.Checked = true;
            cmbVendedor.SelectedIndex = -1;
            dtInicial.Focus();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void rdbSintetico_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbSintetico.Checked.Equals(true))
            {
                grpOrdenar.Enabled = false;
                grpQuebra.Enabled = false;

            }
        }

        private void rdbAnalitico_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAnalitico.Checked.Equals(true))
            {
                grpOrdenar.Enabled = true;
                grpQuebra.Enabled = true;
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }
        
        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ConsisteDados().Equals(true))
                {
                    if (rdbSintetico.Checked.Equals(true))
                    {
                        frmVenRelProdutosPorPeriodoSintetico relatorio = new frmVenRelProdutosPorPeriodoSintetico(dtInicial.Value, dtFinal.Value, chkQuebraPorDia.Checked, cmbVendedor.SelectedIndex <= 0 ? "0" : Convert.ToDouble(cmbVendedor.SelectedValue).ToString(), filtroProd);
                        relatorio.Show();
                    }
                    else
                    {
                        string tipoQuebra = rdbNenhuma.Checked.Equals(true) ? "Nenhuma" : rdbDepartamento.Checked.Equals(true) ? "Departamento" : rdbClasse.Checked.Equals(true) ? "Classe" : "SubClasse" ;
                        frmVenRelProdutosPorPeriodoAnalitico relatorio = new frmVenRelProdutosPorPeriodoAnalitico(dtInicial.Value, dtFinal.Value, chkQuebraPorDia.Checked , rdbAlfabetica.Checked, tipoQuebra, filtroProd, cmbVendedor.SelectedIndex <= 0 ? "0" : Convert.ToDouble(cmbVendedor.SelectedValue).ToString());
                        relatorio.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool ConsisteDados()
        {
            if (dtInicial.Value > dtFinal.Value)
            {
                MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtInicial.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void frmVenRelProdutosPorPeriodo_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Período", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisar.PerformClick();
                    break;
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbNenhuma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbDepartamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSubClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void chkQuebraPorDia_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAlfabetica_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbNumerico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnPesquisar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelProdutosPorPeriodo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbNenhuma_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
