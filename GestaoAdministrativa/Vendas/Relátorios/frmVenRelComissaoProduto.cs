﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelComissaoProduto : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Comissão Produto");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelComissaoProduto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelComissaoProduto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelComissaoProduto>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbUsuario.SelectedIndex = -1;
            rdbSintetico.Checked = true;
            rdbNenhuma.Checked = true;
            btnFiltrar.Enabled = false;
            chkComissao.Checked = true;
            filtroProd = "";
            dtInicial.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void frmVenRelComissaoProduto_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("COL_CODIGO", "COL_NOME", "COLABORADORES", false, "COL_DESABILITADO= 'N' AND EMP_CODIGO = " + Principal.empAtual);

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "COL_NOME";
                cmbUsuario.ValueMember = "COL_CODIGO";
                cmbUsuario.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;

            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbNenhuma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbDepartamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSubClasse_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var busca = new VendasDados();
                DataTable dtRetorno = new DataTable();
                if (rdbSintetico.Checked)
                {
                    if(rdbNenhuma.Checked)
                    {
                        dtRetorno = busca.BuscaRelatorioComissaoPorProdutoSinteticoSemQuebra(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, chkComissao.Checked);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenComissaoProdSintetico relatorio = new frmVenComissaoProdSintetico(this, dtRetorno);
                            relatorio.Text = "Comissão de Vendedor por Produto Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaRelatorioComissaoPorProdutoSinteticoComQuebra(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual,
                            dtInicial.Text, dtFinal.Text, chkComissao.Checked,filtroProd, rdbDepartamento.Checked == true ? 1 : rdbClasse.Checked == true ? 2 : 3);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenComissaoSintQuebra relatorio = new frmVenComissaoSintQuebra(this, dtRetorno);
                            relatorio.Text = "Comissão de Vendedor por Produto Sintético";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                }
                else if (rdbAnalitico.Checked)
                {
                    if (rdbNenhuma.Checked)
                    {
                        dtRetorno = busca.BuscaRelatorioComissaoPorProdutoAnalitico(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text,
                            chkComissao.Checked);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenComissaoProdAnalitico relatorio = new frmVenComissaoProdAnalitico(this, dtRetorno);
                            relatorio.Text = "Comissão de Vendedor por Produto Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                    else
                    {
                        dtRetorno = busca.BuscaRelatorioComissaoPorProdutoAnaliticoComQuebra(cmbUsuario.SelectedIndex > 0 ? Convert.ToInt32(cmbUsuario.SelectedValue) : 0, Principal.estAtual, Principal.empAtual, 
                            dtInicial.Text, dtFinal.Text, chkComissao.Checked, filtroProd, rdbDepartamento.Checked == true ? 1 : rdbClasse.Checked == true ? 2 : 3);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            Cursor = Cursors.Default;
                            frmVenComissaoAnaQuebra relatorio = new frmVenComissaoAnaQuebra(this, dtRetorno);
                            relatorio.Text = "Comissão de Vendedor por Produto Analítico";
                            relatorio.ShowDialog();
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                            MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                        }
                    }
                }

                filtroProd = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void rdbNenhuma_CheckedChanged(object sender, EventArgs e)
        {
            if (!rdbNenhuma.Checked)
            {
                btnFiltrar.Enabled = false;
            }
            else
                btnFiltrar.Enabled = true;

            filtroProd = "";
        }

        private void rdbClasse_CheckedChanged(object sender, EventArgs e)
        {
            filtroProd = "";
            if (rdbClasse.Checked)
                btnFiltrar.Enabled = true;
        }

        private void rdbSubClasse_CheckedChanged(object sender, EventArgs e)
        {
            filtroProd = "";
            if (rdbClasse.Checked)
                btnFiltrar.Enabled = true;
        }

        private void rdbDepartamento_CheckedChanged(object sender, EventArgs e)
        {
            filtroProd = "";
            if (rdbDepartamento.Checked)
                btnFiltrar.Enabled = true;
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAnalitico.Focus();
        }

        private void rdbAnalitico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSintetico.Focus();
        }

        private void rdbSintetico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUsuario.Focus();
        }

        private void cmbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbNenhuma.Focus();
        }

        private void rdbNenhuma_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbDepartamento.Focus();
        }

        private void rdbDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbClasse.Focus();
        }

        private void rdbClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSubClasse.Focus();
        }

        private void rdbSubClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.Focus();
        }
    }
}
