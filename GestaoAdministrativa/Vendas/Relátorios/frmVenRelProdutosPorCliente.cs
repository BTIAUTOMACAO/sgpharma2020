﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelProdutosPorCliente : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Produtos Cliente");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelProdutosPorCliente(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtFNome") && (control.Name != "txtFCnpj") && (control.Name != "txtFCidade"))
                {
                    if ((control is TextBox) ||
                        (control is RichTextBox) ||
                        (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                }
                RegisterFocusEvents(control.Controls);
            }
        }
        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                {
                    var buscaFornecedor = new Cliente();
                    string filtro = "";
                    DataTable dtRetorno = new DataTable();
                    if (cmbFBusca.SelectedIndex == 0)
                    {
                        dtRetorno = buscaFornecedor.DadosClienteFiltro(txtFBusca.Text,0, out filtro);
                    }
                    else if (cmbFBusca.SelectedIndex == 1)
                    {
                        dtRetorno = buscaFornecedor.DadosClienteFiltro(txtFBusca.Text, 1, out filtro);
                    }
                    if (dtRetorno.Rows.Count.Equals(1))
                    {
                        txtFNome.Text = dtRetorno.Rows[0]["CF_NOME"].ToString();
                        txtFCnpj.Text = dtRetorno.Rows[0]["CF_DOCTO"].ToString();
                        txtFCidade.Text = dtRetorno.Rows[0]["CF_CIDADE"].ToString() + "/" + dtRetorno.Rows[0]["CF_UF"].ToString(); ;
                        cmbFBusca.SelectedIndex = 0;
                        dtInicial.Focus();
                    }
                    else if (dtRetorno.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Cliente não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbFBusca.SelectedIndex = 0;
                        txtFBusca.Text = "";
                        txtFBusca.Focus();
                    }
                    else
                    {
                        using (var t = new frmBuscaForn("cliente"))
                        {
                            t.dgCliFor.DataSource = dtRetorno;
                            t.ShowDialog();

                            if (!String.IsNullOrEmpty(t.cfNome))
                            {
                                txtFNome.Text = t.cfNome;
                                txtFCnpj.Text = t.cfDocto;
                                txtFCidade.Text = t.cfCidade;
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                dtInicial.Focus();
                            }
                            else
                            {
                                cmbFBusca.SelectedIndex = 0;
                                txtFBusca.Text = "";
                                txtFBusca.Focus();
                            }
                        }
                    }

                }
                else
                    dtInicial.Focus();
            }
        }

        private void frmVenRelProdutosPorCliente_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void Limpar()
        {
            dtFinal.Value = DateTime.Now;
            dtInicial.Value = DateTime.Now;
            txtFBusca.Text = "";
            txtFCidade.Text = "";
            txtFCnpj.Text = "";
            txtFNome.Text = "";
            cmbFBusca.SelectedIndex = 0;
            rdbNenhum.Checked = true;
            chkDepartamento.Enabled = false;
            chkClasse.Enabled = false;
            chkSubclasse.Enabled = false;
            rdbAlfabetica.Checked = true;
            txtFBusca.Focus();
        }


        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelProdutosPorCliente>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtFNome.Text))
                {
                    MessageBox.Show("Informe um Cliente para a busca", "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtFNome.Focus();
                    return;
                }
                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A Data Inicial não pode ser maior que a Data Final!", "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var relatorio = new Produto();
                if (rdbNenhum.Checked)
                {
                    dtRetorno = relatorio.ProdutosPorCliente(Principal.empAtual, Principal.estAtual, dtInicial.Text, dtFinal.Text, txtFCnpj.Text, rdbAlfabetica.Checked == true ? 0 : 1, filtroProd);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenProdutoCliente dados = new ArquivosReport.frmVenProdutoCliente(this, dtRetorno, txtFNome.Text);
                        dados.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtFBusca.Focus();
                    }
                }
                else
                {
                    if (!chkDepartamento.Checked && !chkClasse.Checked && !chkSubclasse.Checked)
                    {
                        MessageBox.Show("Necessário selecionar uma quebra!", "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        chkDepartamento.Focus();
                        return;
                    }

                    dtRetorno = relatorio.ProdutosPorClienteQuebra(Principal.empAtual, Principal.estAtual, dtInicial.Text, dtFinal.Text, txtFCnpj.Text, rdbAlfabetica.Checked == true ? 0 : 1, filtroProd, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmVenProdutoClienteQuebra dados = new ArquivosReport.frmVenProdutoClienteQuebra(this, dtRetorno, txtFNome.Text);
                        dados.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtFBusca.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Produto por Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void rdbNenhum_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbNenhum.Checked == false)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
            }
        }

        private void rdbOutras_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbNenhum.Checked == false)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
            }
        }

        private void chkDepartamento_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDepartamento.Checked)
            {
                chkClasse.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkClasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkSubclasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSubclasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkClasse.Checked = false;
            }
        }
    }
}
