﻿namespace GestaoAdministrativa.Vendas.Relátorios
{
    partial class frmVenRelComissaoProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRelComissaoProduto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.grpQuebra = new System.Windows.Forms.GroupBox();
            this.rdbSubClasse = new System.Windows.Forms.RadioButton();
            this.rdbClasse = new System.Windows.Forms.RadioButton();
            this.rdbDepartamento = new System.Windows.Forms.RadioButton();
            this.rdbNenhuma = new System.Windows.Forms.RadioButton();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkComissao = new System.Windows.Forms.CheckBox();
            this.cmbUsuario = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbSintetico = new System.Windows.Forms.RadioButton();
            this.rdbAnalitico = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpQuebra.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnFiltrar);
            this.panel2.Controls.Add(this.grpQuebra);
            this.panel2.Controls.Add(this.btnBuscar);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(7, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 495);
            this.panel2.TabIndex = 43;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Enabled = false;
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(743, 10);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(91, 93);
            this.btnFiltrar.TabIndex = 160;
            this.btnFiltrar.Text = "Filtros";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // grpQuebra
            // 
            this.grpQuebra.Controls.Add(this.rdbSubClasse);
            this.grpQuebra.Controls.Add(this.rdbClasse);
            this.grpQuebra.Controls.Add(this.rdbDepartamento);
            this.grpQuebra.Controls.Add(this.rdbNenhuma);
            this.grpQuebra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpQuebra.Location = new System.Drawing.Point(498, 3);
            this.grpQuebra.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Name = "grpQuebra";
            this.grpQuebra.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpQuebra.Size = new System.Drawing.Size(235, 100);
            this.grpQuebra.TabIndex = 5;
            this.grpQuebra.TabStop = false;
            this.grpQuebra.Text = "Quebra por";
            // 
            // rdbSubClasse
            // 
            this.rdbSubClasse.AutoSize = true;
            this.rdbSubClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbSubClasse.Location = new System.Drawing.Point(137, 57);
            this.rdbSubClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbSubClasse.Name = "rdbSubClasse";
            this.rdbSubClasse.Size = new System.Drawing.Size(92, 20);
            this.rdbSubClasse.TabIndex = 3;
            this.rdbSubClasse.TabStop = true;
            this.rdbSubClasse.Text = "SubClasse";
            this.rdbSubClasse.UseVisualStyleBackColor = true;
            this.rdbSubClasse.CheckedChanged += new System.EventHandler(this.rdbSubClasse_CheckedChanged);
            this.rdbSubClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSubClasse_KeyDown);
            this.rdbSubClasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbSubClasse_KeyPress);
            // 
            // rdbClasse
            // 
            this.rdbClasse.AutoSize = true;
            this.rdbClasse.ForeColor = System.Drawing.Color.Navy;
            this.rdbClasse.Location = new System.Drawing.Point(137, 29);
            this.rdbClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbClasse.Name = "rdbClasse";
            this.rdbClasse.Size = new System.Drawing.Size(67, 20);
            this.rdbClasse.TabIndex = 2;
            this.rdbClasse.TabStop = true;
            this.rdbClasse.Text = "Classe";
            this.rdbClasse.UseVisualStyleBackColor = true;
            this.rdbClasse.CheckedChanged += new System.EventHandler(this.rdbClasse_CheckedChanged);
            this.rdbClasse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbClasse_KeyDown);
            this.rdbClasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbClasse_KeyPress);
            // 
            // rdbDepartamento
            // 
            this.rdbDepartamento.AutoSize = true;
            this.rdbDepartamento.ForeColor = System.Drawing.Color.Navy;
            this.rdbDepartamento.Location = new System.Drawing.Point(9, 57);
            this.rdbDepartamento.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbDepartamento.Name = "rdbDepartamento";
            this.rdbDepartamento.Size = new System.Drawing.Size(116, 20);
            this.rdbDepartamento.TabIndex = 1;
            this.rdbDepartamento.TabStop = true;
            this.rdbDepartamento.Text = "Departamento";
            this.rdbDepartamento.UseVisualStyleBackColor = true;
            this.rdbDepartamento.CheckedChanged += new System.EventHandler(this.rdbDepartamento_CheckedChanged);
            this.rdbDepartamento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbDepartamento_KeyDown);
            this.rdbDepartamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbDepartamento_KeyPress);
            // 
            // rdbNenhuma
            // 
            this.rdbNenhuma.AutoSize = true;
            this.rdbNenhuma.ForeColor = System.Drawing.Color.Navy;
            this.rdbNenhuma.Location = new System.Drawing.Point(9, 29);
            this.rdbNenhuma.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdbNenhuma.Name = "rdbNenhuma";
            this.rdbNenhuma.Size = new System.Drawing.Size(87, 20);
            this.rdbNenhuma.TabIndex = 0;
            this.rdbNenhuma.TabStop = true;
            this.rdbNenhuma.Text = "Nenhuma";
            this.rdbNenhuma.UseVisualStyleBackColor = true;
            this.rdbNenhuma.CheckedChanged += new System.EventHandler(this.rdbNenhuma_CheckedChanged);
            this.rdbNenhuma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbNenhuma_KeyDown);
            this.rdbNenhuma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbNenhuma_KeyPress);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBuscar.Location = new System.Drawing.Point(840, 10);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(105, 93);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "F1 -Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            this.btnBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBuscar_KeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkComissao);
            this.groupBox5.Controls.Add(this.cmbUsuario);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(294, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(198, 100);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Filtro";
            // 
            // chkComissao
            // 
            this.chkComissao.AutoSize = true;
            this.chkComissao.ForeColor = System.Drawing.Color.Navy;
            this.chkComissao.Location = new System.Drawing.Point(12, 74);
            this.chkComissao.Name = "chkComissao";
            this.chkComissao.Size = new System.Drawing.Size(136, 20);
            this.chkComissao.TabIndex = 232;
            this.chkComissao.Text = "Comissão Zerada";
            this.chkComissao.UseVisualStyleBackColor = true;
            // 
            // cmbUsuario
            // 
            this.cmbUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsuario.ForeColor = System.Drawing.Color.Black;
            this.cmbUsuario.FormattingEnabled = true;
            this.cmbUsuario.Location = new System.Drawing.Point(12, 44);
            this.cmbUsuario.Name = "cmbUsuario";
            this.cmbUsuario.Size = new System.Drawing.Size(175, 24);
            this.cmbUsuario.TabIndex = 231;
            this.cmbUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUsuario_KeyDown);
            this.cmbUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUsuario_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(9, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "Vendedor";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdbSintetico);
            this.groupBox3.Controls.Add(this.rdbAnalitico);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(182, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(105, 100);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo";
            // 
            // rdbSintetico
            // 
            this.rdbSintetico.AutoSize = true;
            this.rdbSintetico.ForeColor = System.Drawing.Color.Navy;
            this.rdbSintetico.Location = new System.Drawing.Point(13, 58);
            this.rdbSintetico.Name = "rdbSintetico";
            this.rdbSintetico.Size = new System.Drawing.Size(82, 20);
            this.rdbSintetico.TabIndex = 1;
            this.rdbSintetico.TabStop = true;
            this.rdbSintetico.Text = "Sintético";
            this.rdbSintetico.UseVisualStyleBackColor = true;
            this.rdbSintetico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSintetico_KeyDown);
            this.rdbSintetico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbSintetico_KeyPress);
            // 
            // rdbAnalitico
            // 
            this.rdbAnalitico.AutoSize = true;
            this.rdbAnalitico.ForeColor = System.Drawing.Color.Navy;
            this.rdbAnalitico.Location = new System.Drawing.Point(13, 29);
            this.rdbAnalitico.Name = "rdbAnalitico";
            this.rdbAnalitico.Size = new System.Drawing.Size(82, 20);
            this.rdbAnalitico.TabIndex = 0;
            this.rdbAnalitico.TabStop = true;
            this.rdbAnalitico.Text = "Analítico";
            this.rdbAnalitico.UseVisualStyleBackColor = true;
            this.rdbAnalitico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAnalitico_KeyDown);
            this.rdbAnalitico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbAnalitico_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(164, 100);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Período Data";
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(59, 58);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(86, 22);
            this.dtFinal.TabIndex = 6;
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(59, 25);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(86, 22);
            this.dtInicial.TabIndex = 5;
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inicial";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(257, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Relatório de Comissão por Produto";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenRelComissaoProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenRelComissaoProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmVenRelComissaoProduto";
            this.Load += new System.EventHandler(this.frmVenRelComissaoProduto_Load);
            this.Shown += new System.EventHandler(this.frmVenRelComissaoProduto_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.grpQuebra.ResumeLayout(false);
            this.grpQuebra.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.ComboBox cmbUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbSintetico;
        private System.Windows.Forms.RadioButton rdbAnalitico;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DateTimePicker dtFinal;
        public System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox grpQuebra;
        private System.Windows.Forms.RadioButton rdbNenhuma;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.CheckBox chkComissao;
        public System.Windows.Forms.RadioButton rdbSubClasse;
        public System.Windows.Forms.RadioButton rdbClasse;
        public System.Windows.Forms.RadioButton rdbDepartamento;
    }
}