﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas.Relátorios.ArquivosReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenRelFiscais : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Vendas Fiscais");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmVenRelFiscais(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenRelFiscais_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Vendas Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            rdbCfop.Checked = true;
            rdbTodos.Checked = true;
            dtInicial.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Vendas Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(170, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmVenRelFiscais>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Vendas Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Vendas Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
        
        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;

            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbFormaPagamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbCfop_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbImpostos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbVendas_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenRelFiscais_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                String escolha = "";
                if (rdbTodos.Checked)
                    escolha = rdbTodos.Text;
                else if (rdbConcluido.Checked)
                    escolha = rdbConcluido.Text;
                else
                    escolha = rdbCancelado.Text;

                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                var busca = new VendasDados();
                DataTable dtRetorno = new DataTable();

                if (rdbCfop.Checked)
                {
                    dtRetorno = busca.BuscaVendasPorCfop(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, escolha);
                   
                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenRelFiscalSintetico relatorio = new frmVenRelFiscalSintetico(this, dtRetorno);
                        relatorio.Text = "Relatório Fiscal de Totalização por CFOP";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Relatório Fiscal de Totalização por CFOP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
                if (rdbVendas.Checked)
                {
                    dtRetorno = busca.BuscaVendasPorNumeroNota(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, escolha);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenSintetico relatorio = new frmVenSintetico (this, dtRetorno);
                        relatorio.Text = "Relatório Fiscal de Vendas";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Relatório Fiscal de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
                if (rdbImpostos.Checked)
                {
                    dtRetorno = busca.BuscaImpostosPorData(Principal.estAtual, Principal.empAtual, dtInicial.Text, dtFinal.Text, escolha);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        Cursor = Cursors.Default;
                        frmVenImpostoSintetico relatorio = new frmVenImpostoSintetico(this, dtRetorno);
                        relatorio.Text = "Relatório Fiscal de Impostos";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Relatório Fiscal de Impostos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Vendas Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }
    }
}
