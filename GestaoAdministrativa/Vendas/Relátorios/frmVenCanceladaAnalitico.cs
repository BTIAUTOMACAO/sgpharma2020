﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace GestaoAdministrativa.Vendas.Relátorios
{
    public partial class frmVenCanceladaAnalitico : Form
    {
        private frmVenRelCanceladas relatorio;
        private DataTable dtRelatorio;

        public frmVenCanceladaAnalitico(frmVenRelCanceladas frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmVenCanceladaAnalitico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var analitico = new ReportDataSource("analitico", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(analitico);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new Negocio.VendasItens();
            DataTable dtItens = tItens.BuscaProdutosPorVendaId(Principal.estAtual, Principal.empAtual, e.Parameters["ID"].Values.First());

            var itens = new ReportDataSource("dsItens", dtItens);
            e.DataSources.Add(itens);
        }
    }
}
