﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.FPopular;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Negocio.Beneficios;
using System.Diagnostics;
using GestaoAdministrativa.Vendas.VidaLink;
using GestaoAdministrativa.Vendas.EPharma;
using GestaoAdministrativa.Vendas.PortalDaDrogaria;
using System.IO;
using System.Threading;
using GestaoAdministrativa.Vendas.Orizon;
using MessagingToolkit.QRCode.Codec;
using System.Drawing.Printing;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenBeneficios : Form
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        frmVenda fVendas;
        DataTable dtMedicamentos = new DataTable();
        public bool abortou = false;
        #endregion

        public frmVenBeneficios(frmVenda form)
        {
            InitializeComponent();
            fVendas = form;
        }

        private void frmVenBeneficios_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnDrogabella_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Principal.wsConexao = Funcoes.LeParametro(14, "01", true);
                if (!String.IsNullOrEmpty(Principal.wsConexao))
                {
                    GestaoAdministrativa.WSConvenio.wsconvenio ws = new GestaoAdministrativa.WSConvenio.wsconvenio();
                    ws.Url = Principal.wsConexao;

                    frmVenConsultarCartao consultarCartao = new frmVenConsultarCartao(fVendas);
                    consultarCartao.gbCartao.Text = "Consultar Cartão Convênio Drogabella";
                    this.Hide();
                    this.Close();
                    consultarCartao.Text = "Consultar Cartão Convênio Drogabella";
                    consultarCartao.ShowDialog();
                    if (!consultarCartao.gravar)
                    {
                        fVendas.Limpar();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnRpc_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Principal.wsConexao = Funcoes.LeParametro(14, "01", true);
                if (!String.IsNullOrEmpty(Principal.wsConexao))
                {
                    GestaoAdministrativa.WSConvenio.wsconvenio ws = new GestaoAdministrativa.WSConvenio.wsconvenio();
                    ws.Url = Principal.wsConexao;

                    frmVenConsultarCartao consultarCartao = new frmVenConsultarCartao(fVendas);
                    this.Hide();
                    this.Close();
                    consultarCartao.Text = "Consultar Cartão Convênio Plantão Card";
                    consultarCartao.ShowDialog();

                    if (!consultarCartao.gravar)
                        fVendas.Limpar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmVenBeneficios_Load(object sender, EventArgs e)
        {
            try
            {
                this.Focus();

                //FARMACIA POPULAR//
                if (Funcoes.LeParametro(14, "04", true).Equals("S"))
                {
                    btnFPopular.Enabled = true;
                }
                else
                    btnFPopular.Enabled = false;

                if (Funcoes.LeParametro(14, "10", true).Equals("S"))
                {
                    btnEpharma.Enabled = true;
                }
                else
                    btnEpharma.Enabled = false;

                if (Funcoes.LeParametro(14, "21", true).Equals("S"))
                {
                    btnVidaLink.Enabled = true;
                }
                else
                    btnVidaLink.Enabled = false;

                if (Funcoes.LeParametro(14, "27", true).Equals("S"))
                {
                    btnPortalDrogaria.Enabled = true;
                }
                else
                    btnPortalDrogaria.Enabled = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDrogabella.PerformClick();
                    break;
                case Keys.F2:
                    btnRpc.PerformClick();
                    break;
                case Keys.F3:
                    btnFPopular.PerformClick();
                    break;
                case Keys.F4:
                    btnEpharma.PerformClick();
                    break;
                case Keys.F5:
                    btnVidaLink.PerformClick();
                    break;
                case Keys.F6:
                    btnPortalDrogaria.PerformClick();
                    break;
                case Keys.F7:
                    btnOrizon.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void btnDrogabella_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnRpc_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnDrogabella_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnDrogabella, "F1 - Convênio Drogabella");
        }

        private void btnRpc_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnRpc, "F2 - Convênio Plantão Card");
        }

        private void btnFPopular_Click(object sender, EventArgs e)
        {
            if (fVendas.listCupom.Items.Count > 5)
            {
                fVendas.validouFP = false;
                fVendas.vendaBeneficio = "FARMACIAPOPULAR";

                this.Hide();
                this.Close();
                //string[] dadosProduto;
                DataColumn dc;

                dc = new DataColumn("coCodigoBarras", typeof(String));
                dtMedicamentos.Columns.Add(dc);

                dc = new DataColumn("nomeProduto", typeof(String));
                dtMedicamentos.Columns.Add(dc);

                dc = new DataColumn("qtPrescrita", typeof(String));
                dtMedicamentos.Columns.Add(dc);

                dc = new DataColumn("qtSolicitada", typeof(String));
                dtMedicamentos.Columns.Add(dc);

                dc = new DataColumn("vlPrecoVenda", typeof(String));
                dtMedicamentos.Columns.Add(dc);

                for (int i = 0; i < fVendas.tProdutos.Rows.Count; i++)
                {
                    dtMedicamentos.Rows.Add(fVendas.tProdutos.Rows[i]["COD_BARRA"].ToString(),
                                            fVendas.tProdutos.Rows[i]["PROD_DESCR"].ToString(),
                                            null, fVendas.tProdutos.Rows[i]["PROD_QTDE"].ToString(),
                                            fVendas.tProdutos.Rows[i]["PRE_VALOR"].ToString());
                }

                for (int i = 0; i < dtMedicamentos.Rows.Count; i++)
                {
                    frmQtdMedicamentoFP qtdMedicamento = new frmQtdMedicamentoFP(i, dtMedicamentos, fVendas);
                    qtdMedicamento.ShowDialog();
                }

                if (fVendas.validouFP)
                {
                    frmExecutaSolicitacao frmExec = new frmExecutaSolicitacao(fVendas.txtVendedor.Text, dtMedicamentos, fVendas);
                    frmExec.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Operação cancelada pelo operador. \r\nPara realizar a venda do benefício, reinicie o procedimento na tela de vendas!", "Farmácia Popular");
                    this.Hide();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Selecione os medicamentos antes de iniciar o módulo da Farmácia Popular!", "Farmácia Popular");
                this.Hide();
                this.Close();
            }

        }

        private void btnEpharma_Click(object sender, EventArgs e)
        {
            try
            {
                Application.DoEvents();
                Cursor = Cursors.WaitCursor;
                if (Funcoes.LeParametro(14, "16", false, Principal.nomeEstacao).Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                {
                    //if (ExecutaEPharma(false).Equals(true))
                    //{
                        if (VerificaParametrosEPharma().Equals(true))
                        {
                            VerificarAtividade();
                        }
                    //}
                }
                else
                {
                    Funcoes.GravaParametro(14, "16", DateTime.Now.ToString("dd/MM/yyyy"), Principal.nomeEstacao, "Data da ultima atualizacao E-Pharma", "BENEFICIOS", "Data da ultima atualizacao E-Pharma");
                    //if (ExecutaEPharma(true).Equals(true))
                    //{
                        if (VerificaParametrosEPharma().Equals(true))
                        {
                            Epharma epharma = new Epharma();
                            string sequencia = epharma.EnviaFechamento();
                            var inicio = DateTime.UtcNow;
                            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
                            {
                                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(120))
                                {
                                    MessageBox.Show("Erro:  Não foi possível realizar autorização ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }

                            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));

                            if (epharma.RetornoFechamento(sequencia).Equals(true))
                            {
                                VerificarAtividade();
                            }
                        }

                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        #region E-Pharma
        public bool ExecutaEPharma(bool maximizado)
        {
            if (System.Diagnostics.Process.GetProcessesByName("PBMS_PDV").Length.Equals(0))
            {
                var startInfo = new ProcessStartInfo(Funcoes.LeParametro(14, "15", true));
                startInfo.WindowStyle = maximizado == true ? ProcessWindowStyle.Normal : ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                if (System.Diagnostics.Process.GetProcessesByName("PBMS_PDV").Length.Equals(0))
                {
                    MessageBox.Show("Atenção, execute o E-Pharma para iniciar a venda", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnEpharma.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public bool VerificaParametrosEPharma()
        {
            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "11", true)) || Funcoes.LeParametro(14, "11", true).Length != 14)
            {
                MessageBox.Show("CNPJ do Estabelecimento não esta configurado corretamente nos parâmetros", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "12", true)))
            {
                MessageBox.Show("Caminho da pasta Raiz não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "13", true)))
            {
                MessageBox.Show("Caminho da pasta de Envio não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "14", true)))
            {
                MessageBox.Show("Caminho da pasta de Resposta não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        public void VerificarAtividade()
        {

            Epharma vendaEpharma = new Epharma();
            EpharmaVendas venda = new EpharmaVendas();
            vendaEpharma.LimparPastas();
            if (vendaEpharma.EnvioTesteDeAtividade().Equals(true))
            {
                if (vendaEpharma.RetornoTesteDeAtividade().Equals(true))
                {
                    if (vendaEpharma.EnvioInicializacao().Equals(true))
                    {
                        if (vendaEpharma.RetornoInicializacao().Equals(true))
                        {
                            DataTable dtPendentes = venda.BuscaVendasPorStatus('P');
                            if (!dtPendentes.Rows.Count.Equals(0))
                            {
                                for (int i = 0; i < dtPendentes.Rows.Count; ++i)
                                {
                                    vendaEpharma.CancelamentoAutorizacao(Convert.ToInt32(dtPendentes.Rows[i]["NSU"]), Convert.ToInt32(dtPendentes.Rows[i]["NUM_TRANS"]));
                                }
                            }

                            vendaEpharma.LimparPastas();
                            frmMenuEPharma ePharma = new frmMenuEPharma(fVendas);
                            ePharma.ShowDialog();
                            vendaEpharma.LimparPastas();
                            this.Hide();
                            this.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Erro ao tentar conectar ao E-Pharma \n Time Out ", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btnEpharma.Focus();
                }
            }
        }
        #endregion

        private void btnVidaLink_Click(object sender, EventArgs e)
        {
            try
            {
                frmVidaLinkAutorizacao autorizacaoVidaLink = new frmVidaLinkAutorizacao(fVendas);
                this.Hide();
                this.Close();
                autorizacaoVidaLink.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPortalDrogaria_Click(object sender, EventArgs e)
        {
            try
            {
                frmPortalDrogariaAutorizacao autorizacaoPortalDrogaria = new frmPortalDrogariaAutorizacao(fVendas);
                this.Hide();
                this.Close();
                autorizacaoPortalDrogaria.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefícios", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #region Orizon  
        private void btnOrizon_Click(object sender, EventArgs e)
        {
            if (VerificaParametrosOrizon().Equals(true))
            {
                if (ExecutaOrizon().Equals(true))
                {
                    frmAutorizacaoOrizon orizon = new frmAutorizacaoOrizon(fVendas);
                    this.Hide();
                    this.Close();
                    orizon.ShowDialog();
                }
            }
        }


        public bool VerificaParametrosOrizon()
        {

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "40", true)))
            {
                MessageBox.Show("Caminho do executável não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "41", true)))
            {
                MessageBox.Show("Caminho da pasta Raiz não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "42", true)))
            {
                MessageBox.Show("Caminho da pasta de Envio não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "43", true)))
            {
                MessageBox.Show("Caminho da pasta de Resposta não configurado", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnEpharma.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.LeParametro(14, "45", false, Principal.nomeEstacao)))
            {
                Funcoes.GravaParametro(14, "45", "0001", Principal.nomeEstacao, "Identificação do PDV", "BENEFICIOS", "Identificação do Orizon");
                return true;
            }
            else
            {
                return true;
            }
        }

        public bool ExecutaOrizon()
        {

            if (System.Diagnostics.Process.GetProcessesByName("FileExchanger").Length.Equals(0))
            {

                var startInfo = new ProcessStartInfo(Funcoes.LeParametro(14, "40", true));
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;

                Process.Start(startInfo);
                if (System.Diagnostics.Process.GetProcessesByName("FileExchanger").Length.Equals(0))
                {
                    MessageBox.Show("Atenção, execute o FileExchanger para iniciar a venda", "Orizon", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnEpharma.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion

        private void btnFPopular_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnEpharma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnVidaLink_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnOrizon_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFPopular_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFPopular, "F3 - Farmácia Popular");
        }

        private void btnEpharma_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnEpharma, "F4 - E-Pharma");
        }

        private void btnVidaLink_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnVidaLink, "F5 - Vida Link");
        }

        private void btnPortalDrogaria_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnPortalDrogaria, "F6 - Portal da Drogaria");
        }

        private void btnOrizon_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnOrizon, "F7 - Orizon");
        }
    }
}


