﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenCadastroRapidoCliente : Form
    {
        private frmVenda fVenda;

        public frmVenCadastroRapidoCliente(frmVenda venda)
        {
            fVenda = venda;
            InitializeComponent();
        }

        private void frmVenCadastroRapidoCliente_Load(object sender, EventArgs e)
        {
            if (fVenda.vendaBeneficio.Equals("FARMACIAPOPULAR"))
            {
                txtCpf.Text = Principal.doctoCliente;
                rdbFisica.Checked = true;
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbFisica.Focus();
        }

        private void rdbFisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbFisica.Checked.Equals(true))
            {
                txtCpf.Mask = "000,000,000-00";
                txtCpf.Focus();
            }
        }

        private void rdbJuridica_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbJuridica.Checked.Equals(true))
            {
                txtCpf.Mask = "00,000,000/0000-00";
                txtCpf.Focus();
            }
        }

        private void rdbFisica_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbJuridica_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtInsRG.Focus();
        }

        private void txtCpf_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtInsRG_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtInsRG_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodigo.Focus();
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtECep.Focus();
            }
        }

        private void txtCodigo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                txtCodigo.Text = Funcoes.FormataZeroAEsquerda(Funcoes.IdentificaVerificaID("CLIFOR", "CF_CODIGO").ToString(), Convert.ToInt32(Funcoes.LeParametro(3, "12", false)));
                txtECep.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro Rápido de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBairro.Text))
                {
                    txtBairro.Focus();
                }
                else
                {
                    txtTelefone.Focus();
                }
            }
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtECep.Focus();
        }

        private void txtECep_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private async void txtECep_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtECep.Text.Replace("-", "")) && !txtECep.Text.Replace("-", "").Equals(00000000))
                    {
                        Endereco end = await ConsultaCEP(txtECep.Text);
                        if (end.bairro != null)
                        {
                            txtEndereco.Text = end.logradouro.ToUpper();
                            txtBairro.Text = end.bairro.Length > 20 ? Funcoes.RemoverAcentuacao(end.bairro.Substring(0, 19).ToUpper()) : Funcoes.RemoverAcentuacao(end.bairro.ToUpper());
                            txtCidade.Text = Funcoes.RemoverAcentuacao(end.localidade.ToUpper());
                            cmbCUF.Text = end.uf;
                            Cursor = Cursors.Default;
                            txtNumero.Focus();
                        }
                    }
                    else if(!String.IsNullOrEmpty(txtEndereco.Text))
                    {
                        txtCidade.Focus();
                    }
                    else
                        txtEndereco.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro Rápido de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbCUF.Focus();
        }

        private void cmbCUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbCUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
        

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodigoConveniada.Focus();
        }

        private void txtCodigoConveniada_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodigoConveniada_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtCodigoConveniada.Text))
                    {
                        txtNomeConveniada.Text = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", txtCodigoConveniada.Text, false, true);
                        if (String.IsNullOrEmpty(txtNomeConveniada.Text))
                        {
                            MessageBox.Show("Nenhum registro encontrado", "Busca Conveniada", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtCodigoConveniada.Text = "";
                            txtCodigoConveniada.Focus();
                        }
                        else
                            btnConfirmar.Focus();
                    }
                    else
                        btnConfirmar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro Rápido de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNomeConveniada_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNomeConveniada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.Focus();
        }
        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnConfirmar.PerformClick();
                    break;
                case Keys.F2:
                    btnCancelar.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    fVenda.txtCodCliEmp.Focus();
                    break;
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if(String.IsNullOrEmpty(txtNome.Text))
            {
                Principal.mensagem = "Nome não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCpf.Text)))
            {
                Principal.mensagem = "CPF não pode ser em branco.";
                Funcoes.Avisa();
                txtCpf.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNumero.Text.Trim()))
            {
                Principal.mensagem = "Número não pode ser em branco.";
                Funcoes.Avisa();
                txtNumero.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtBairro.Text.Trim()))
            {
                Principal.mensagem = "Bairro não pode ser em branco.";
                Funcoes.Avisa();
                txtBairro.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtECep.Text.Replace("-", "").Trim()))
            {
                Principal.mensagem = "CEP não pode ser em branco.";
                Funcoes.Avisa();
                txtECep.Focus();
                return false;
            }
            if (cmbCUF.SelectedIndex.Equals(-1))
            {
                Principal.mensagem = "Necessário selecionar o estado.";
                Funcoes.Avisa();
                cmbCUF.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtTelefone.Text.Trim()))
            {
                Principal.mensagem = "Telefone não pode ser em branco.";
                Funcoes.Avisa();
                txtTelefone.Focus();
                return false;
            }

            return true;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if(ConsisteCampos())
                {
                    #region INSERE CLIENTE NA TABELA 
                    //VERIFICA SE CLIENTE JÁ ESTA CADASTRADO//
                    Principal.strSql = "SELECT * FROM CLIFOR WHERE CF_DOCTO = '" + txtCpf.Text + "'";
                    Principal.dtPesq = BancoDados.selecionarRegistros(Principal.strSql);
                    if (!String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_DOCTO", "CF_DOCTO", txtCpf.Text, false, true)))
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Cliente já cadastrado.";
                        Funcoes.Avisa();
                        txtCpf.Text = "";
                        txtCpf.Focus();
                        return;
                    }

                    //INCLUI NOVO REGISTRO NA TABELA CLIENTES//
                    var cliente = new Cliente
                    {
                        CfDocto = txtCpf.Text,
                        CfTipoDocto = rdbFisica.Checked ? 0 : 1,
                        CfNome = txtNome.Text,
                        CfEndereco = txtEndereco.Text,
                        CfNumeroEndereco = txtNumero.Text,
                        CfBairro = txtBairro.Text,
                        CfCEP = txtECep.Text,
                        CfUF = cmbCUF.Text,
                        CfCidade = txtCidade.Text,
                        CfStatus = 4,
                        CfTelefone = txtTelefone.Text,
                        CfEnderecoCobranca = txtEndereco.Text,
                        CfNumeroCobranca = txtNumero.Text,
                        CfBairroCobranca = txtBairro.Text,
                        CfCEPCobranca = txtECep.Text,
                        CfCidadeCobranca = txtCidade.Text,
                        CfUFCobranca = cmbCUF.Text,
                        CfEnderecoEntrega = txtEndereco.Text,
                        CfNumeroEntrega = txtNumero.Text,
                        CfBairroEntrega = txtBairro.Text,
                        CfCEPEntrega = txtECep.Text,
                        CfCidadeEntrega = txtCidade.Text,
                        CfUFEntrega = cmbCUF.Text,
                        CfPais = "BRASIL",
                        CfLiberado = "N",
                        CodigoConveniada = txtCodigoConveniada.Text == "" ? 0 : Convert.ToInt32(txtCodigoConveniada.Text),
                        NumCartao = "0",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };
                    if (!cliente.IncluiClienteConsultaCatrtao(cliente, out fVenda.IDCliente))
                        return;
                    #endregion

                    Principal.doctoCliente = txtCpf.Text;
                    Principal.nomeCliFor = txtNome.Text;
                    Principal.idCliFor = fVenda.IDCliente.ToString();
                    fVenda.codCliente = txtCodigo.Text == "" ? "0" : txtCodigo.Text;
                    fVenda.txtCliEmp.Text = txtNome.Text;
                    fVenda.doctoCliente = txtCpf.Text;
                    fVenda.txtCodCliEmp.Text  = txtCodigoConveniada.Text;

                    var dadosConveniada = new Conveniada();
                    DataTable dt = dadosConveniada.BuscaConveniadaParticular(txtCodigoConveniada.Text);
                    if(dt.Rows.Count > 0)
                    {
                        fVenda.wsConID = Convert.ToInt32(dt.Rows[0]["CON_ID"]);
                        fVenda.wsEmpresa = dt.Rows[0]["CON_CODIGO"].ToString();
                        fVenda.wsConWeb = dt.Rows[0]["CON_WEB"].ToString();
                        fVenda.wsConCodConv = dt.Rows[0]["CON_COD_CONV"].ToString();
                    }

                    fVenda.txtCodCliEmp.Enabled = false;
                    fVenda.DadosDaVendaCliente(Principal.doctoCliente);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cadastro Rápido de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox9, "Campo Obrigatório");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        { 
            this.Close();
            fVenda.txtCodCliEmp.Focus();
        }

        private void rdbFisica_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar  == 13)
            {
                txtInsRG.Focus();
            }
        }

        private void rdbJuridica_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtInsRG.Focus();
            }
        }

        private void txtCpf_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtCpf.Text = Util.GerarCpf();
            txtCpf.Enabled = true;
            txtInsRG.Focus();
        }
    }
}
