﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Negocio.Beneficios;
using GestaoAdministrativa.SAT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenCancelaNF : Form, Botoes
    {
        private ToolStripButton tsbCancelamento = new ToolStripButton("Cancelar Vendas");
        private ToolStripSeparator tssCancelamento = new ToolStripSeparator();

        VendasDados vendaFiltro = new VendasDados();

        DateTime dataCaixa;
        private XmlDocument retornoXml;
        private GestaoAdministrativa.WSConvenio.wsconvenio ws;
        private string codErro;
        private string comprovanteCancelamento;
        private string numeroNota;

        public frmVenCancelaNF(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenCancelaNF_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dtData.Value = DateTime.Now;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPInicial.Focus();
        }

        private void txtPInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltrar.PerformClick();
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void frmVenCancelaNF_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtData_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtPInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtPFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private async void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                bool imprime = false;
                var cxStatus = new AFCaixa();
                List<AFCaixa> cStatus = cxStatus.DataCaixa(Principal.empAtual, Principal.estAtual, "S", Principal.usuario);

                if (IdentificaRegistroSelecionado())
                {
                    using (frmOcorrencia ocorrencia = new frmOcorrencia())
                    {
                        ocorrencia.ShowDialog();
                    }

                    if (MessageBox.Show("Imprime Comprovante de Cancelamento?", "Cancelamento de Vendas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        imprime = true;
                    }

                    for (int i = 0; i < dgCancelar.RowCount; i++)
                    {
                        if (dgCancelar.Rows[i].Selected == true)
                        {
                            if (Funcoes.LeParametro(0, "13", true).Equals("S"))
                            {
                                if (cStatus.Count.Equals(0))
                                {
                                    MessageBox.Show("A Venda N° " + dgCancelar.Rows[i].Cells[3].Value + " não pode ser cancelada pois o caixa\ndo " + dgCancelar.Rows[i].Cells[7].Value + " não está aberto!", "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    continue;
                                }
                                else
                                    dataCaixa = cStatus[0].DataAbertura;
                            }
                            else
                                dataCaixa = DateTime.Now;

                            BancoDados.AbrirTrans();

                            var cancelaVenda = new VendasDados();
                            if (!cancelaVenda.CancelaVenda(Convert.ToInt32(dgCancelar.Rows[i].Cells[1].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[2].Value),
                                Convert.ToInt64(dgCancelar.Rows[i].Cells[3].Value), dataCaixa, Principal.usuario))
                            {
                                MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                BancoDados.ErroTrans();
                                continue;
                            }

                            if (Convert.ToString(dgCancelar.Rows[i].Cells[9].Value).Equals("PARTICULAR"))
                            {
                                var cancelaCobranca = new Cobranca();
                                if (!cancelaCobranca.CancelaCobranca(Convert.ToInt32(dgCancelar.Rows[i].Cells[1].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[2].Value),
                                Convert.ToInt64(dgCancelar.Rows[i].Cells[3].Value), dataCaixa, Principal.usuario))
                                {
                                    MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                var cancelaCobrancaParcela = new CobrancaParcela();
                                if (!cancelaCobrancaParcela.CancelaCobrancaParcela(Convert.ToInt32(dgCancelar.Rows[i].Cells[1].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[2].Value),
                                Util.SelecionaCampoEspecificoDaTabela("COBRANCA", "COBRANCA_ID", "COBRANCA_VENDA_ID", dgCancelar.Rows[i].Cells[3].Value.ToString(), true, false, true), dataCaixa, Principal.usuario))
                                {
                                    MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    continue;
                                }
                            }

                            if (!Convert.ToInt32(dgCancelar.Rows[i].Cells[11].Value).Equals(0))
                            {
                                var cancelaMovFinanceiro = new MovimentoFinanceiro();
                                if (!cancelaMovFinanceiro.ExcluiMovimentoFinanceiroPorDataHora(Convert.ToDateTime(dgCancelar.Rows[i].Cells[10].Value)))
                                {
                                    MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                var cancelaMovCaixa = new MovimentoCaixa();
                                if (!cancelaMovCaixa.ExcluiMovimentoCaixaPorDataHora(Convert.ToDateTime(dgCancelar.Rows[i].Cells[10].Value)))
                                {
                                    MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                var cancelaMovCaixaEsp = new MovimentoCaixaEspecie();
                                if (!cancelaMovCaixaEsp.ExcluirMovimentoCaixaEspeciePorDataHora(Convert.ToDateTime(dgCancelar.Rows[i].Cells[10].Value)))
                                {
                                    MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    continue;
                                }
                            }

                            bool erro = false;
                            var cancelaItens = new VendasItens();
                            DataTable dtRetorno = cancelaItens.BuscaItensDaVendaParaCancelamento(Convert.ToInt32(dgCancelar.Rows[i].Cells[2].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[1].Value),
                                Convert.ToInt64(dgCancelar.Rows[i].Cells[3].Value));
                            for (int x = 0; x < dtRetorno.Rows.Count; x++)
                            {
                                var voltaEstoque = new ProdutoDetalhe();
                                if (!voltaEstoque.AtualizaEstoqueAtualPorCodigo(dtRetorno.Rows[x]["PROD_CODIGO"].ToString(), Convert.ToInt32(dtRetorno.Rows[x]["PROD_ID"]),
                                    Convert.ToInt32(dtRetorno.Rows[x]["EMP_CODIGO"]), Convert.ToInt32(dtRetorno.Rows[x]["EST_CODIGO"]),
                                    Convert.ToInt32(dtRetorno.Rows[x]["VENDA_ITEM_QTDE"]), "+"))
                                {
                                    BancoDados.ErroTrans();
                                    erro = true;
                                    continue;
                                }

                                var dadosMovimento = new MovEstoque
                                {
                                    EstCodigo = Principal.estAtual,
                                    EmpCodigo = Principal.empAtual,
                                    EstIndice = Convert.ToInt64(dtRetorno.Rows[x]["VENDA_ITEM_INDICE"]),
                                    ProdCodigo = dtRetorno.Rows[x]["PROD_CODIGO"].ToString(),
                                    EntQtde = 0,
                                    EntValor = 0,
                                    SaiQtde = 0,
                                    SaiValor = 0,
                                    OperacaoCodigo = "C",
                                    DataMovimento = DateTime.Now,
                                    DtCadastro = DateTime.Now,
                                    OpCadastro = Principal.usuario,
                                };

                                if (!dadosMovimento.ManutencaoMovimentoEstoque("D", dadosMovimento))
                                {
                                    BancoDados.ErroTrans();
                                    continue;
                                }

                                if (Util.TesteIPServidorNuvem())
                                {
                                    if (Funcoes.LeParametro(9, "51", false).Equals("S"))
                                    {
                                        await IncrementaEstoqueDeFiliais(dtRetorno.Rows[x]["PROD_CODIGO"].ToString(), Convert.ToInt32(dtRetorno.Rows[x]["VENDA_ITEM_QTDE"]));
                                    }
                                }

                                Funcoes.GravaLogExclusao("VENDAS", dgCancelar.Rows[i].Cells[3].Value.ToString(), Principal.usuario, Convert.ToDateTime(Principal.data), 
                                    "VENDAS", dgCancelar.Rows[i].Cells[3].Value.ToString(), Principal.motivo, Principal.estAtual);
                            }

                            if (erro)
                            {
                                MessageBox.Show("Erro ao cancelar venda ID: " + dgCancelar.Rows[i].Cells[3].Value, "Cancelar Venda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                BancoDados.ErroTrans();
                                continue;
                            }
                            if (Convert.ToString(dgCancelar.Rows[i].Cells[9].Value).Equals("DROGABELLA/PLANTAO"))
                            {
                                Principal.wsConexao = Funcoes.LeParametro(14, "01", true);

                                if (Convert.ToString(dgCancelar.Rows[i].Cells[12].Value) != "0")
                                {
                                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.CancelarTransacao(Convert.ToString(dgCancelar.Rows[i].Cells[12].Value));
                                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                                    if (codErro == "1")
                                    {
                                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro ao Cancelar Autorização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        BancoDados.ErroTrans();
                                        continue;
                                    }
                                }

                                if (Convert.ToString(dgCancelar.Rows[i].Cells[13].Value) != "0")
                                {
                                    retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.CancelarTransacao(Convert.ToString(dgCancelar.Rows[i].Cells[13].Value));
                                    codErro = retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText;
                                    if (codErro == "1")
                                    {
                                        MessageBox.Show("Erro: " + retornoXml.GetElementsByTagName("MSG").Item(0).InnerText, "Erro ao Cancelar Autorização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        BancoDados.ErroTrans();
                                        continue;
                                    }
                                }
                            }
                            else if (Convert.ToString(dgCancelar.Rows[i].Cells[9].Value).Equals("EPHARMA"))
                            {
                                Epharma epharma = new Epharma();
                                if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
                                {
                                    epharma.CancelamentoVenda(dgCancelar.Rows[i].Cells["VENDA_AUTORIZACAO"].Value.ToString());
                                }
                                else
                                {
                                    CuponsSAT sat = new CuponsSAT();
                                    DataTable dtSat = sat.BuscaHoraCupom(Convert.ToInt64(dgCancelar.Rows[i].Cells["VENDA_ID"].Value));

                                    if (Math.Abs((DateTime.Now - Convert.ToDateTime(dtSat.Rows[0]["DT_CADASTRO"])).TotalMinutes) < 30)
                                    {
                                        CancelaCupomSat(dtSat.Rows[0]["VENDA_NUM_CFE"].ToString(), Convert.ToDouble(dtSat.Rows[0]["VENDA_TOTAL"]));
                                        if (!String.IsNullOrEmpty(numeroNota))
                                        {
                                            epharma.CancelamentoVenda(dgCancelar.Rows[i].Cells["VENDA_AUTORIZACAO"].Value.ToString());
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Venda não pode ser cancelada \n Vendas realizadados com mais de 30 minutos ", "E-Pharma");
                                        BancoDados.ErroTrans();
                                        continue;
                                    }
                                }

                            }

                            BancoDados.FecharTrans();

                            if (Convert.ToString(dgCancelar.Rows[i].Cells[9].Value).Equals("FARMACIAPOPULAR"))
                            {
                                MessageBox.Show("Necessário realizar o Estorno através da Tela de Estorno Farmácia Popular!", "Estorno Farmácia Popular");
                            }

                            if (imprime)
                            {
                                ComprovanteCancelamento(Convert.ToInt64(dgCancelar.Rows[i].Cells[3].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[1].Value), Convert.ToInt32(dgCancelar.Rows[i].Cells[2].Value),
                                     Convert.ToDateTime(dgCancelar.Rows[i].Cells[10].Value), dgCancelar.Rows[i].Cells[14].Value.ToString(), Convert.ToDouble(dgCancelar.Rows[i].Cells[15].Value),
                                     Convert.ToDouble(dgCancelar.Rows[i].Cells[16].Value), Convert.ToDouble(dgCancelar.Rows[i].Cells[5].Value), dgCancelar.Rows[i].Cells[25].Value.ToString(),
                                     dgCancelar.Rows[i].Cells[9].Value.ToString(), dgCancelar.Rows[i].Cells[17].Value.ToString(), dgCancelar.Rows[i].Cells[22].Value.ToString(),
                                     dgCancelar.Rows[i].Cells[26].Value.ToString(), dgCancelar.Rows[i].Cells[18].Value.ToString(), dgCancelar.Rows[i].Cells[19].Value.ToString(),
                                     dgCancelar.Rows[i].Cells[24].Value.ToString());
                            }
                        }

                    }

                    MessageBox.Show("Vendas Canceladas com Sucesso!", "Cancelamento da Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    btnFiltrar.PerformClick();
                }
                else
                {
                    MessageBox.Show("Necessário selecionar ao menos um registro para cancelamento!", "Cancelamento da Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (dgCancelar.RowCount > 0)
                    {
                        dgCancelar.Focus();
                    }
                    else
                    {
                        dtData.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public async Task IncrementaEstoqueDeFiliais(string prodCodigo, int qtd)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                    + "&codBarras=" + prodCodigo
                                    + "&qtd=" + qtd + "&operacao=mais");
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\Vendas.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + prodCodigo + " / QTDE: " + qtd);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public bool ComprovanteCancelamento(long vendaID, int empCodigo, int estCodigo, DateTime dataVenda, string colCodigo, double subTotal, double ajuste, double totalVenda,
            string cfDocto, string beneficio, string VendaAutorizacao, string VendaAutorizacaoReceita, string wsNumCartao, string wsNomeCartao, string wsEmpresa, string wsNomeEmpresa
            )
        {
            try
            {
                string tamanho;
                var dadosEstabelecimento = new Estabelecimento();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(estCodigo, empCodigo);
                if (retornoEstab.Count > 0)
                {
                    for (int y = 1; y <= Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); y++)
                    {
                        comprovanteCancelamento = Funcoes.CentralizaTexto("CANCELAMENTO DE VENDA", 43) + "\n";
                        comprovanteCancelamento += Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovanteCancelamento += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovanteCancelamento += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovanteCancelamento += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovanteCancelamento += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovanteCancelamento += retornoEstab[0].EstBairro + "\n";
                        comprovanteCancelamento += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                        comprovanteCancelamento += "N. VENDA: " + vendaID + "\n";
                        comprovanteCancelamento += "Data: " + Convert.ToDateTime(dataVenda).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dataVenda).ToString("HH:mm:ss") + "\n";
                        comprovanteCancelamento += "__________________________________________" + "\n";
                        comprovanteCancelamento += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                        comprovanteCancelamento += "__________________________________________" + "\n";

                        var itensVendas = new VendasItens();

                        DataTable tProdutos = itensVendas.BuscaItensDaVendaPorID(vendaID, estCodigo, empCodigo);
                        for (int i = 0; i < tProdutos.Rows.Count; i++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[i]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]), 2) * 100);
                            comprovanteCancelamento += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[i]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[i]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                comprovanteCancelamento += "    R$ " + tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            comprovanteCancelamento += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[i]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[i]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        double desconto = Math.Abs(Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")));

                        comprovanteCancelamento += "==========================================" + "\n";
                        var colaborador = new Colaborador();
                        comprovanteCancelamento += "VENDEDOR.: R$ " + colaborador.NomeColaborador(colCodigo, estCodigo) + "\n";
                        comprovanteCancelamento += "SUBTOTAL.: R$ " + subTotal.ToString().Replace(",", ".") + "\n";

                        if (Convert.ToDouble(desconto) > 0 || Math.Abs(ajuste) != 0)
                        {
                            comprovanteCancelamento += "DESCONTO.: R$ " + desconto.ToString().Replace(",", ".") + "   AJUSTE.: R$ " + ajuste.ToString().Replace(",", ".") + "\n";
                        }
                        comprovanteCancelamento += "TOTAL....: R$ " + totalVenda.ToString().Replace(",", ".") + "\n";

                        var vendaFormaPagto = new VendasFormaPagamento();
                        var descricaoForma = new FormasPagamento();
                        comprovanteCancelamento += Funcoes.CentralizaTexto("MEIO(S) DE PAGAMENTO", 43) + "\n";

                        DataTable dtRetorno = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(empCodigo, estCodigo, vendaID);

                        for (int x = 0; x < dtRetorno.Rows.Count; x++)
                        {
                            comprovanteCancelamento += descricaoForma.IdentificaFormaDePagamento(Convert.ToInt32(dtRetorno.Rows[x]["VENDA_FORMA_ID"]), Convert.ToInt32(dtRetorno.Rows[x]["EMP_CODIGO"]))
                                + ".: R$ " + Funcoes.BValor(Convert.ToDouble(dtRetorno.Rows[x]["VENDA_VALOR_PARCELA"])) + "\n";
                        }
                        comprovanteCancelamento += "__________________________________________" + "\n";

                        var dCliente = new Cliente();
                        List<Cliente> dadosVendaCliente = dCliente.DadosClienteCfDocto(cfDocto);
                        if (dadosVendaCliente.Count > 0)
                        {
                            comprovanteCancelamento += dadosVendaCliente[0].CfCodigo == "" ? "00" : dadosVendaCliente[0].CfCodigo;
                            comprovanteCancelamento += " - " + dadosVendaCliente[0].CfNome + "\n";
                            comprovanteCancelamento += dadosVendaCliente[0].CfEndereco + "," + dadosVendaCliente[0].CfNumeroEndereco + "\n";
                            comprovanteCancelamento += dadosVendaCliente[0].CfCidade + "/" + dadosVendaCliente[0].CfUF + "\n";
                            comprovanteCancelamento += "CPF.:" + dadosVendaCliente[0].CfDocto + "\n";
                            comprovanteCancelamento += "__________________________________________" + "\n";
                        }

                        if (beneficio.Equals("DROGABELLA/PLANTAO"))
                        {
                            if (!String.IsNullOrEmpty(VendaAutorizacao))
                            {
                                comprovanteCancelamento += "Autorizacao:" + VendaAutorizacao.PadLeft(38 - VendaAutorizacao.Length, ' ') + "\n";
                            }
                            if (!String.IsNullOrEmpty(VendaAutorizacaoReceita))
                            {
                                comprovanteCancelamento += "Autorizacao com Receita:" + VendaAutorizacaoReceita.PadLeft(26 - VendaAutorizacaoReceita.Length, ' ') + "\n";
                            }
                            comprovanteCancelamento += "Administradora:   DROGABELLA/PLANTAO CARD" + "\n";
                            comprovanteCancelamento += "Cartao:                  " + wsNumCartao + "\n";
                            comprovanteCancelamento += "Conveniado:  " + (wsNomeCartao.Length > 33 ? wsNomeCartao.Substring(0, 32) : wsNomeCartao.PadLeft(33 - wsNomeCartao.Length, ' ')) + "\n";
                            comprovanteCancelamento += "Empresa: " + ((wsEmpresa + " - " + wsNomeEmpresa).Length > 32 ? (wsEmpresa + " - " + wsNomeEmpresa).Substring(0, 32)
                                : (wsEmpresa + " - " + wsNomeEmpresa).PadLeft(32 - (wsEmpresa + " - " + wsNomeEmpresa).Length, ' ')) + "\n";

                            comprovanteCancelamento += "\n";
                        }
                        comprovanteCancelamento += "__________________________________________" + "\n";
                        comprovanteCancelamento += Funcoes.LeParametro(2, "28", true).ToUpper() + "\n";
                        comprovanteCancelamento += "__________________________________________" + "\n\n";

                        //IMPRIME COMPROVANTE
                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovanteCancelamento, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteCancelamento);

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                            {
                                using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\" + vendaID + ".txt", true))
                                {
                                    writer.WriteLine(comprovanteCancelamento);
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovanteCancelamento += "\n\n\n\n\n";
                                Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteCancelamento);
                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                }
                            }
                        }
                        else
                        {
                            comprovanteCancelamento += "\n\n\n\n";
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteCancelamento, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comprovante Orcamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmVenCancelaNF_Shown(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = vendaFiltro.BuscaDadosDaVendaPorEmissao(dtData.Value, Principal.estAtual, Principal.empAtual);
                if (dtRetorno.Rows.Count > 0)
                {
                    dgCancelar.DataSource = dtRetorno;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado", "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCancelar.DataSource = dtRetorno;
                    dtData.Focus();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = vendaFiltro.BuscaDadosDaVendaPorEmissao(dtData.Value, Principal.estAtual, Principal.empAtual, txtPInicial.Text);
                if (dtRetorno.Rows.Count > 0)
                {
                    dgCancelar.DataSource = dtRetorno;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado", "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCancelar.DataSource = dtRetorno;
                    dtData.Focus();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool IdentificaRegistroSelecionado()
        {
            bool selecionouLinha = false;

            for (int i = 0; i < dgCancelar.RowCount; i++)
            {
                if (dgCancelar.Rows[i].Selected == true)
                {
                    selecionouLinha = true;
                }
            }

            return selecionouLinha;
        }

        public void Botao()
        {
            try
            {
                this.tsbCancelamento.AutoSize = false;
                this.tsbCancelamento.Image = Properties.Resources.remover;
                this.tsbCancelamento.Size = new System.Drawing.Size(130, 20);
                this.tsbCancelamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbCancelamento);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssCancelamento);
                tsbCancelamento.Click += delegate
                {
                    var cancelarVenda = Application.OpenForms.OfType<frmVenCancelaNF>().FirstOrDefault();
                    Util.BotoesGenericos();
                    cancelarVenda.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbCancelamento);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssCancelamento);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void CancelaCupomSat(string chave, double total)
        {
            FileStream fs;
            StreamWriter sw;
            string mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
            string numeroCupom, arquivoSATResposta;
            string xmlSAT, retorno;
            var updateCupom = new CuponsSAT();

            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
            {
                xmlSAT = Sat.MontaXmlCancelamento(chave);
                if (!String.IsNullOrEmpty(xmlSAT))
                {
                    retorno = Sat.SatCancelarUltimaVenda(chave, xmlSAT);

                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCupom))
                    {
                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesComErro\" + mes.ToUpper() + @"\" + chave + ".xml", FileMode.Create);
                        sw = new StreamWriter(fs);
                        sw.WriteLine(xmlSAT);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                    }

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesCanEnviados\" + mes.ToUpper() + @"\" + chave + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    //CONVERTE BASE64 PARA STRING//
                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    if (Sat.SatImpressaoComprovanteCancelamento(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml",
                        numeroCupom, total, out numeroNota))
                    {
                        updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, numeroCupom, DateTime.Now, Principal.usuario, numeroCupom);
                        MessageBox.Show("Cupom Cancelado!", "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                int iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                if (iRetorno != 1)
                {
                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, chave, DateTime.Now, Principal.usuario,"");
                    MessageBox.Show("Cupom Cancelado!", "Cancelamento de Vendas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void dgCancelar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dgCancelar.Rows[e.RowIndex].Cells[0].Value != null)
                {
                    if (dgCancelar.Rows[e.RowIndex].Cells[0].Value.Equals(true))
                    {
                        dgCancelar.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                    else
                        dgCancelar.Rows[e.RowIndex].Cells[0].Value = true;
                }
                else
                    dgCancelar.Rows[e.RowIndex].Cells[0].Value = true;
            }
        }
    }
}
