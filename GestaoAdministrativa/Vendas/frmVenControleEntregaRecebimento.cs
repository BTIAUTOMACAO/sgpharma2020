﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenControleEntregaRecebimento : Form
    {
        private long idVenda;
        private string entregador;
        private int numRomaneiro;
        private int controleID;
        private VendasFormaPagamento dadosPagamento = new VendasFormaPagamento();
        private double totalEspecie;
        private double totalVenda;
        private long movtoFinanceiroSeq;
        private string vendaBeneficio;
        private long movtoFinanceiroID;
        private DateTime dtPedido;
        private VendasDados dadosVenda = new VendasDados();
        private VendasEspecies dadosEspeciesPagto = new VendasEspecies();
        private bool vendaEmitido;
        public bool validou;
        DataTable tProdutos = new DataTable();
        DataTable dadosVendaEntrega = new DataTable();
        private string xmlSAT;
        private double totalImpFederal;
        private double totalImpEstadual;
        private double totalImpMunicipal;
        private string vendaNumeroNota;

        public frmVenControleEntregaRecebimento(long vendaID, string nomeEntregador, int romaneio, int entControleID, double total)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            idVenda = vendaID;
            entregador = nomeEntregador;
            numRomaneiro = romaneio;
            controleID = entControleID;
            totalVenda = total;
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmControleEntregaRecebimento_Load(object sender, EventArgs e)
        {
            try
            {
                validou = false;
                txtValor.Text = String.Format("{0:N}", totalVenda);
                lblTroco.Text = "0,00";
                lblTotal.Text = "0,00";
                totalEspecie = 0;
                lblVendaId.Text = idVenda.ToString();
                lblRomaneio.Text = numRomaneiro.ToString();
                lblEntregador.Text = entregador;
                movtoFinanceiroSeq = 0;

                //Alterado pra quando agendar uma entrega no dia anterior, entrar no dia que for entregue. 
                //dtPedido = Convert.ToDateTime(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DATA_HORA", "VENDA_ID", idVenda.ToString(), true, false, true));
                DateTime datavenda = Convert.ToDateTime(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DATA_HORA", "VENDA_ID", idVenda.ToString(), true, false, true));
                if (datavenda.ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
                {
                    /*<0 − If date1 is earlier than date2
                       0 − If date1 is the same as date2
                      >0 − If date1 is later than date2 */
                    int result = DateTime.Compare(datavenda, DateTime.Now);
                    if (result < 0)
                        dtPedido = DateTime.Now;
                    else
                        dtPedido = datavenda;
                }
                else
                    dtPedido = datavenda; 

                dgFormaPagamento.DataSource = dadosPagamento.BuscaFormasPagamentosParaEntrega(Principal.empAtual, Principal.estAtual, idVenda);

                DataTable dt = Util.CarregarCombosPorEmpresa("FORMA_DESCRICAO", "FORMA_ID", "FORMAS_PAGAMENTO", true, "FORMA_LIBERADO = 'S'");
                DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                combo.HeaderText = "Forma de Pagamento";
                combo.Name = "FORMA_DESCRICAO";
                combo.Width = 150;
                ArrayList row = new ArrayList();

                foreach (DataRow dr in dt.Rows)
                {
                    row.Add(dr["FORMA_DESCRICAO"].ToString());
                }

                combo.Items.AddRange(row.ToArray());
                dgFormaPagamento.Columns.Add(combo);

                for (int x = 0; x < dgFormaPagamento.RowCount; x++)
                {
                    dgFormaPagamento.Rows[x].Cells[9].Value = dgFormaPagamento.Rows[x].Cells[6].Value;
                }

                cmbEspecies.DisplayMember = "ESP_DESCRICAO";
                cmbEspecies.ValueMember = "ESP_CODIGO";
                cmbEspecies.DataSource = Util.CarregarCombosPorEmpresa("ESP_DESCRICAO", "ESP_CODIGO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N'");

                cmbEspecies.SelectedIndex = -1;

                var dadosEspecies = new Especie();
                dt = dadosEspeciesPagto.BuscaEspeciesPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { dt.Rows[i]["ESP_CODIGO"],
                             Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_DESCRICAO", "ESP_CODIGO", dt.Rows[i]["ESP_CODIGO"].ToString()),dt.Rows[i]["VENDA_ESPECIE_VALOR"],
                            dadosEspecies.ConsideraEspecie(Convert.ToInt32( dt.Rows[i]["ESP_CODIGO"])), dadosEspecies.IdentificaEspecieVinculado(Convert.ToInt32( dt.Rows[i]["ESP_CODIGO"])),
                            dadosEspecies.IdentificaEspecieSAT(Convert.ToInt32( dt.Rows[i]["ESP_CODIGO"])) });

                        totalEspecie = +Convert.ToDouble(dt.Rows[i]["VENDA_ESPECIE_VALOR"]);
                        txtValor.Text = "0,00";

                        lblTotal.Text = String.Format("{0:N}", totalEspecie);
                    }
                    if (totalEspecie > totalVenda)
                    {
                        lblTroco.Text = String.Format("{0:N}", totalEspecie - totalVenda);
                    }
                    else
                    {
                        lblTroco.Text = "0,00";
                    }
                }

                vendaBeneficio = Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_BENEFICIO", "VENDA_ID", idVenda.ToString(), true, false, true);
                if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_EMITIDO", "VENDA_ID", idVenda.ToString(), true, true, true).Equals("S") || (!vendaBeneficio.Equals("N") && vendaBeneficio.Equals("DROGABELLA/PLANTAO")))
                {
                    vendaEmitido = true;
                    dgEspecies.ReadOnly = true;
                    dgEspecies.AllowUserToDeleteRows = false;
                    dgFormaPagamento.ReadOnly = true;
                    if ((vendaBeneficio.Equals("DROGABELLA/PLANTAO")) && dgFormaPagamento.RowCount == 2)
                        dgFormaPagamento.ReadOnly = false;
                }
                else
                {
                    vendaEmitido = false;
                    dgEspecies.ReadOnly = false;
                    dgEspecies.AllowUserToDeleteRows = true;
                    dgFormaPagamento.ReadOnly = false;
                }

                var especies = new Especie();
                var dadosFormas = new VendasFormaPagamento();
                double valorDiferenca = 0;
                dt = dadosFormas.BuscaFormaPagtoPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);
                if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["VENDA_FORMA_ID"].ToString().Equals("4"))
                        {
                            dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { 4,
                             "CONVENIO" ,dt.Rows[i]["VENDA_VALOR_PARCELA"],
                            "N", "S", "99" });

                            valorDiferenca = Convert.ToDouble(dt.Rows[i]["VENDA_VALOR_PARCELA"]);
                        }
                    }

                    txtValor.Text = String.Format("{0:N}", totalVenda - valorDiferenca);
                    totalEspecie = valorDiferenca;
                    lblTotal.Text = String.Format("{0:N}", totalEspecie);
                }
                else if (vendaBeneficio.Equals("PARTICULAR"))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        valorDiferenca = valorDiferenca + Convert.ToDouble(dt.Rows[i]["VENDA_VALOR_PARCELA"]);
                    }

                    dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { 4,
                             "PARTICULAR" ,valorDiferenca,
                            "N", "S", "99" });

                    txtValor.Text = "0,00";
                    totalEspecie = valorDiferenca;
                    lblTotal.Text = String.Format("{0:N}", totalEspecie);
                }
                else if (vendaBeneficio.Equals("FUNCIONAL"))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["VENDA_FORMA_ID"].ToString().Equals("8"))
                        {
                            dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { 4,
                             "FUNCIONAL" ,dt.Rows[i]["VENDA_VALOR_PARCELA"],
                            "N", "S", "99" });

                            valorDiferenca = Convert.ToDouble(dt.Rows[i]["VENDA_VALOR_PARCELA"]);
                        }
                    }

                    txtValor.Text = String.Format("{0:N}", totalVenda - valorDiferenca);
                    totalEspecie = valorDiferenca;
                    lblTotal.Text = String.Format("{0:N}", totalEspecie);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecies_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtValor.Focus();
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtValor.Text))
                {
                    if (Convert.ToDouble(txtValor.Text) > 0)
                    {
                        var dadosEspecies = new Especie();
                        dgEspecies.Rows.Insert(dgEspecies.RowCount, new Object[] { cmbEspecies.SelectedValue, cmbEspecies.Text, txtValor.Text,
                            dadosEspecies.ConsideraEspecie(Convert.ToInt32(cmbEspecies.SelectedValue)), dadosEspecies.IdentificaEspecieVinculado(Convert.ToInt32(cmbEspecies.SelectedValue)),
                            dadosEspecies.IdentificaEspecieSAT(Convert.ToInt32(cmbEspecies.SelectedValue)) });

                        totalEspecie = totalEspecie + Convert.ToDouble(txtValor.Text);
                        txtValor.Text = "0,00";

                        lblTotal.Text = String.Format("{0:N}", totalEspecie);

                        if (totalEspecie > totalVenda)
                        {
                            lblTroco.Text = String.Format("{0:N}", totalEspecie - totalVenda);
                        }
                        else
                        {
                            lblTroco.Text = "0,00";
                        }
                    }
                    else
                    {
                        totalEspecie = 0;
                        for (int i = 0; i < dgEspecies.RowCount; i++)
                        {
                            totalEspecie = totalEspecie + Convert.ToDouble(dgEspecies.Rows[i].Cells[2].Value);
                        }
                        if (totalEspecie < totalVenda)
                        {
                            txtValor.Text = "0,00";
                            MessageBox.Show("Valor não pode ser Zero!", "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtValor.Focus();
                        }
                        else
                            btnConfirmar.PerformClick();
                    }

                    cmbEspecies.SelectedIndex = -1;
                }

            }
        }

        private void txtValor_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtValor.Text))
            {
                txtValor.Text = String.Format("{0:N}", Convert.ToDouble(txtValor.Text));
            }
        }

        private void dgEspecies_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            totalEspecie = 0;
            for (int i = 0; i < dgEspecies.RowCount; i++)
            {
                totalEspecie = totalEspecie + Convert.ToDouble(dgEspecies.Rows[i].Cells[2].Value);
            }
            if (totalEspecie > totalVenda)
            {
                lblTroco.Text = String.Format("{0:N}", totalEspecie - totalVenda);
            }
            else
            {
                lblTroco.Text = "0,00";
            }
            lblTotal.Text = String.Format("{0:N}", totalEspecie);
        }

        public bool IdentificaContraEntrega()
        {
            bool selecionouLinha = true;

            for (int i = 0; i < dgFormaPagamento.RowCount; i++)
            {
                if (dgFormaPagamento.Rows[i].Cells[9].Value != null)
                {
                    if (dgFormaPagamento.Rows[i].Cells[9].Value.ToString() ==
                        Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "FORMA_DESCRICAO", "FORMA_ID", Funcoes.LeParametro(6, "356", false), false, false, true))
                    {
                        MessageBox.Show("Ainda existem itens para [CONTRA-ENTREGA]. Não é possível continuar!", "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        selecionouLinha = false;
                        break;
                    }
                }
            }

            return selecionouLinha;
        }

        private async void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (IdentificaContraEntrega())
                {
                    if (totalEspecie < totalVenda)
                    {
                        MessageBox.Show("Valor informado é menor que o valor esperado.\nNão e possível continuar!", "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    Cursor = Cursors.WaitCursor;
                    btnConfirmar.Enabled = false;
                    btnCancelar.Enabled = false;
                    movtoFinanceiroSeq = 0;

                    if (vendaBeneficio.Equals("N") && !vendaEmitido)
                    {
                        movtoFinanceiroSeq = Convert.ToInt64(Funcoes.IdentificaVerificaID("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_SEQ", Principal.estAtual, " MOVIMENTO_FINANCEIRO_DATA = " + Funcoes.BData(dtPedido) + " AND MOVIMENTO_FINANCEIRO_USUARIO = '" + Principal.usuario + "'", Principal.empAtual));
                    }
                    else if (vendaBeneficio.Equals("DROGABELLA/PLANTAO") && dgFormaPagamento.RowCount > 1)
                    {
                        movtoFinanceiroSeq = Convert.ToInt64(Funcoes.IdentificaVerificaID("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_SEQ", Principal.estAtual, " MOVIMENTO_FINANCEIRO_DATA = " + Funcoes.BData(dtPedido) + " AND MOVIMENTO_FINANCEIRO_USUARIO = '" + Principal.usuario + "'", Principal.empAtual));
                    }

                    if (vendaBeneficio.Equals("DROGABELLA/PLANTAO"))
                    {
                        // movtoFinanceiroSeq = 0;
                        vendaEmitido = false;
                    }

                    bool erro = false;
                    long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                    BancoDados.AbrirTrans();

                    if (!vendaEmitido)
                    {
                        for (int i = 0; i < dgFormaPagamento.RowCount; i++)
                        {
                            if (!dgFormaPagamento.Rows[i].Cells[9].Value.Equals(dgFormaPagamento.Rows[i].Cells[6].Value))
                            {
                                if (!dadosPagamento.AtualizaFormaDePagamento(Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "FORMA_ID", "FORMA_DESCRICAO",
                                    dgFormaPagamento.Rows[i].Cells[9].Value.ToString(), false, true, true)), Convert.ToInt32(dgFormaPagamento.Rows[i].Cells[1].Value),
                                    Convert.ToInt32(dgFormaPagamento.Rows[i].Cells[0].Value), Convert.ToInt64(dgFormaPagamento.Rows[i].Cells[2].Value),
                                    Convert.ToInt32(dgFormaPagamento.Rows[i].Cells[8].Value), Principal.usuario))
                                {
                                    erro = true;
                                    break;
                                }
                            }
                        }

                        if (erro)
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        if (!RetiraDoEstoque(id, idVenda))
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        if (movtoFinanceiroSeq != 0)
                        {
                            if (!InserirMovimentos())
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                        }

                        if (!InserirEspecies())
                        {
                            BancoDados.ErroTrans();
                            return;
                        }
                    }

                    if (vendaBeneficio.Equals("PARTICULAR")) //Tem que ser particular; 
                    {
                        for (int i = 0; i < dgEspecies.RowCount; i++) //constar no grid de especies;
                        {
                            if (dgEspecies.Rows[i].Cells[1].Value.Equals("PARTICULAR"))
                            {
                                if (!InserirCobranca(Convert.ToInt32(dgEspecies.Rows[i].Cells["ESP_VALOR"].Value)))
                                {
                                    BancoDados.ErroTrans();
                                    return;
                                }
                            }
                        }
                    }                    

                    if (!dadosVenda.AtualizarMovimentoFinanceiroEStatus(movtoFinanceiroSeq, Convert.ToInt32(dgFormaPagamento.Rows[0].Cells[1].Value),
                          Convert.ToInt32(dgFormaPagamento.Rows[0].Cells[0].Value), Convert.ToInt64(dgFormaPagamento.Rows[0].Cells[2].Value), "F"))
                    {
                        BancoDados.ErroTrans();
                        return;
                    }

                    validou = true;

                    if (Funcoes.LeParametro(6, "353", false, Principal.nomeEstacao).Equals("F") && Funcoes.LeParametro(6, "364", false).Equals("S"))
                    {
                        if (Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_EMITIDO", "VENDA_ID", idVenda.ToString(), false, true).Equals("S"))
                        {
                            if (MessageBox.Show("Cupom Fiscal já emitido! Deseja emitir novamente?", "Reimpr.Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                        }

                        if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            if (!SATCupomFiscal(idVenda))
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                        }
                        else if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
                        {
                            if (!ImprimirCupomFiscal(idVenda))
                            {
                                Imprimir.ImpFiscalUltCupom();
                                BancoDados.ErroTrans();
                                return;
                            }
                        }
                    }

                    BancoDados.FecharTrans();
                    
                    if (Funcoes.LeParametro(9, "51", false).Equals("S"))
                    {
                        if (Util.TesteIPServidorNuvem())
                        {
                            var vendasItens = new VendasItens();
                            DataTable dtRetorno = vendasItens.BuscaItensControleDeEntrega(Principal.estAtual, Principal.empAtual, idVenda);

                            await DecrementaEstoqueDeFiliais(dtRetorno);
                        }
                    }
                    

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                btnConfirmar.Enabled = true;
                btnCancelar.Enabled = true;
            }
        }

        public bool InserirCobranca(int valor)
        {
            try
            {
                int id = 0;

                for (int x = 0; x < dgFormaPagamento.RowCount; x++)
                {
                    //Feito a Vista
                    if (dgFormaPagamento.Rows[x].Cells["VENDA_PARCELA"].Value.ToString().Equals("1"))
                    {
                        id = Funcoes.IdentificaVerificaID("COBRANCA", "COBRANCA_ID", Principal.estAtual, "", Principal.empAtual);
                        //COBRANCA
                        var dadosCobranca = new Cobranca
                            (
                                Convert.ToInt32(dgFormaPagamento.Rows[x].Cells["EMP_CODIGO"].Value),
                                Convert.ToInt32(dgFormaPagamento.Rows[x].Cells["EST_CODIGO"].Value),
                                id,
                                Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_CF_ID", "VENDA_ID", idVenda.ToString(), false, true)),
                                DateTime.Now,
                                valor, //totalVenda,
                                Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "FORMA_ID", "FORMA_DESCRICAO",
                                dgFormaPagamento.Rows[x].Cells["FORMA_ID"].Value.ToString(), false, true, true)),
                                Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_COL_CODIGO", "VENDA_ID", idVenda.ToString(), false, true)),
                                idVenda,
                                "C",
                                "A",
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario,
                                DateTime.Now,
                                Principal.usuario
                            );

                        if (!dadosCobranca.InserirDados(dadosCobranca))
                            return false;
                    }

                    int idParcela = Funcoes.IdentificaVerificaID("COBRANCA_PARCELA", "COBRANCA_PARCELA_ID", Principal.estAtual, "", Principal.empAtual);

                    var dadosParcelas = new CobrancaParcela
                        (
                            Convert.ToInt32(dgFormaPagamento.Rows[x].Cells["EMP_CODIGO"].Value),
                            Convert.ToInt32(dgFormaPagamento.Rows[x].Cells["EST_CODIGO"].Value),
                            idParcela + x,
                            id,
                            Convert.ToInt32(dgFormaPagamento.Rows[x].Cells["VENDA_PARCELA"].Value),
                            Convert.ToDouble(dgFormaPagamento.Rows[x].Cells["VENDA_VALOR_PARCELA"].Value),
                            Convert.ToDouble(dgFormaPagamento.Rows[x].Cells["VENDA_VALOR_PARCELA"].Value),
                            "A",
                            Convert.ToDateTime(dgFormaPagamento.Rows[x].Cells["VENDA_PARCELA_VENCIMENTO"].Value),
                            DateTime.Now,
                            DateTime.Now,
                            Principal.usuario,
                            DateTime.Now,
                            Principal.usuario
                        );

                    if (!dadosParcelas.InserirDados(dadosParcelas))
                        return false;

                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Cobranca", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task DecrementaEstoqueDeFiliais(DataTable dt)
        {
            try
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                        + "&codBarras=" + dt.Rows[i]["PROD_CODIGO"].ToString()
                                        + "&qtd=" + Convert.ToInt32(dt.Rows[i]["VENDA_ITEM_QTDE"]) + "&operacao=menos");
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\Entrega.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + dt.Rows[i]["PROD_CODIGO"].ToString() + " / QTDE: " + dt.Rows[i]["VENDA_ITEM_QTDE"]);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        public bool InserirMovimentos()
        {
            try
            {
                long idCAixa = Funcoes.GeraIDLong("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID", Principal.estAtual, "", Principal.empAtual);
                movtoFinanceiroID = Funcoes.GeraIDLong("MOVIMENTO_FINANCEIRO", "MOVIMENTO_FINANCEIRO_ID", Principal.estAtual, "", Principal.empAtual);

                for (int i = 0; i < dgFormaPagamento.RowCount; i++)
                {
                    if (Util.SelecionaCampoEspecificoDaTabela("FORMAS_PAGAMENTO", "OPERACAO_CAIXA", "FORMA_DESCRICAO", Convert.ToString(dgFormaPagamento.Rows[i].Cells[9].Value), false, true, true).Equals("S"))
                    {
                        //MOVIMENTO FINANCEIRO//
                        var mFinanceiro = new MovimentoFinanceiro
                            (
                                Principal.empAtual,
                                Principal.estAtual,
                                movtoFinanceiroID + i,
                                dtPedido,
                                dtPedido,
                                "V",
                                Principal.usuario,
                                Convert.ToDouble(dgFormaPagamento.Rows[i].Cells[5].Value),
                                movtoFinanceiroSeq,
                                DateTime.Now,
                                Principal.usuario,
                                idVenda
                            );

                        if (!mFinanceiro.InserirDados(mFinanceiro))
                            return false;


                        //MOVIMENTO DE CAIXA//
                        var mCaixa = new MovimentoCaixa
                            (
                                Principal.empAtual,
                                Principal.estAtual,
                                idCAixa + i,
                                dtPedido,
                                Convert.ToInt32(Funcoes.LeParametro(6, "75", false)),
                                "+",
                                Convert.ToDouble(dgFormaPagamento.Rows[i].Cells[5].Value),
                                Principal.usuario,
                                movtoFinanceiroID,
                                movtoFinanceiroSeq,
                                DateTime.Now,
                                Principal.usuario,
                                "ORIGEM VENDA ENTREGA",
                                idVenda
                            );

                        if (!mCaixa.InserirDados(mCaixa))
                            return false;

                        long id = Funcoes.GeraIDLong("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID", Principal.estAtual, "", Principal.empAtual);
                        //MOVIMENTO DE CAIXA ESPECIE//
                        for (int y = 0; y < dgEspecies.RowCount; y++)
                        {
                            if (Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES", "ESP_CAIXA", "ESP_CODIGO", dgEspecies.Rows[y].Cells[0].Value.ToString()).Equals("S"))
                            {
                                var mCaixaEspecie = new MovimentoCaixaEspecie
                                (
                                    Principal.empAtual,
                                    Principal.estAtual,
                                    id + y,
                                    dtPedido,
                                    Convert.ToInt32(dgEspecies.Rows[y].Cells[0].Value),
                                    Convert.ToDouble(dgEspecies.Rows[y].Cells[2].Value),
                                    Principal.usuario,
                                    movtoFinanceiroID,
                                    DateTime.Now,
                                    Principal.usuario
                                );

                                if (!mCaixaEspecie.InserirDados(mCaixaEspecie))
                                    return false;
                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Movimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool InserirEspecies()
        {
            try
            {
                int id = Funcoes.IdentificaVerificaID("VENDAS_ESPECIES", "VENDA_ESPECIE_ID", Principal.estAtual, "", Principal.empAtual);
                for (int x = 0; x < dgEspecies.RowCount; x++)
                {
                    //VENDAS_ESPECIES
                    var dadosEspecies = new VendasEspecies
                            (
                                Principal.empAtual,
                                Principal.estAtual,
                                idVenda,
                                id + x,
                                Convert.ToInt32(dgEspecies.Rows[x].Cells[0].Value),
                                Convert.ToDouble(dgEspecies.Rows[x].Cells[2].Value),
                                "",
                                "",
                                "",
                                DateTime.Now,
                                Principal.usuario
                            );
                    if (!dadosEspecies.InserirDados(dadosEspecies))
                        return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Inserir Espécies", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnConfirmar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConfirmar, "F1 - Confirmar");
        }

        private void btnCancelar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancelar, "F2 - Cancelar");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                validou = false;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgFormaPagamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbEspecies_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgEspecies_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmControleEntregaRecebimento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnConfirmar.PerformClick();
                    break;
                case Keys.F2:
                    btnCancelar.PerformClick();
                    break;
            }
        }

        public bool RetiraDoEstoque(long id, long vendaID)
        {
            try
            {
                var dProduto = new Produto();
                var vendasItens = new VendasItens();
                DataTable dtRetorno = vendasItens.BuscaItensDaVendaPorID(vendaID, Principal.empAtual, Principal.estAtual);

                for (int i = 0; i < dtRetorno.Rows.Count; i++)
                {
                    if (!dProduto.AtualizaEstoque(dtRetorno.Rows[i]["PROD_CODIGO"].ToString(), Convert.ToInt32(dtRetorno.Rows[i]["VENDA_ITEM_QTDE"]), "-", true))
                        return false;

                    if (!vendasItens.AtualizaIndiceEntrega(vendaID, Principal.empAtual, Principal.estAtual, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(), id))
                        return false;

                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = id,
                        ProdCodigo = dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        EntQtde = 0,
                        EntValor = 0,
                        SaiQtde = Convert.ToInt32(dtRetorno.Rows[i]["VENDA_ITEM_QTDE"]),
                        SaiValor = Convert.ToInt32(dtRetorno.Rows[i]["VENDA_ITEM_QTDE"]) * Convert.ToDouble(dtRetorno.Rows[i]["PROD_CUSME"]),
                        OperacaoCodigo = "V",
                        DataMovimento = DateTime.Now,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;

                    id += 1;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro Tirar Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool SATCupomFiscal(long idVenda)
        {
            #region SAT
            try
            {
                string numeroCFe;
                if (!Sat.VerificaModeloSAT())
                {
                    return false;
                }

                Principal.NumCpfCnpj = "";
                Principal.NomeNP = "";
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    var dadosCliente = new Cliente();
                    DataTable dt = dadosCliente.SelecionaDadosDoClientePorVendaID(idVenda, Principal.empAtual, Principal.estAtual);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dt.Rows[0]["CF_TIPO_DOCTO"]).Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }

                Cursor = Cursors.WaitCursor;
                var colaborador = new Colaborador();
                string arquivoSATResposta, mes;
                mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                dadosVendaEntrega = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", idVenda.ToString(), true);
                var dadosEspecies = new VendasEspecies();

                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    #region OUTROS SAT
                    if (!MontarXmlSAT(Principal.NumCpfCnpj, Principal.NomeNP, idVenda))
                        return false;

                    string retorno = Sat.SatEnviarDadosDaVenda(xmlSAT);

                    FileStream fs;
                    StreamWriter sw;
                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCFe))
                    {
                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesComErro\" + mes.ToUpper() + @"\" + idVenda + ".xml", FileMode.Create);
                        sw = new StreamWriter(fs);
                        sw.WriteLine(xmlSAT);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                        return false;
                    }

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesEnviados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    //CONVERTE BASE64 PARA STRING//
                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(xmlSAT);
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    Sat.SatImpressaoComprovante(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml",
                        totalImpFederal, totalImpEstadual, totalImpMunicipal, numeroCFe, out vendaNumeroNota, colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), idVenda,
                        "", "");

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);
                    #endregion
                }
                else
                {
                    #region DARUMA
                    int iRetorno;

                    iRetorno = DarumaDLL.iConfigurarGuilhotina_DUAL_DarumaFramework("1", "2");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    iRetorno = DarumaDLL.eDefinirProduto_Daruma("SAT");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    if (!String.IsNullOrEmpty(Principal.NumCpfCnpj))
                    {
                        xmlSAT = "<dest><" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(Principal.NumCpfCnpj) + "</" + (Principal.NumCpfCnpj.Length <= 14 ? "CPF" : "CNPJ") + ">"
                            + "<xNome>" + Principal.NomeNP + "</xNome></dest>";
                    }
                    else
                        xmlSAT = "";

                    iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        if (iRetorno == -130)
                        {
                            iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                            else
                            {
                                iRetorno = DarumaDLL.aCFeAbrir_SAT_Daruma(xmlSAT);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return false;
                                }
                            }

                        }
                        else
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    var dadosProdutos = new Produto();

                    var dadosItens = new VendasItens();
                    tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                    for (int i = 0; i < tProdutos.Rows.Count; i++)
                    {
                        DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(Convert.ToInt32(tProdutos.Rows[i]["EST_CODIGO"]), Convert.ToInt32(tProdutos.Rows[i]["EMP_CODIGO"]),
                            Convert.ToInt32(tProdutos.Rows[i]["PROD_ID"]));

                        xmlSAT = "<prod>";
                        xmlSAT += "<cProd>" + tProdutos.Rows[i]["PROD_CODIGO"] + "</cProd>";
                        xmlSAT += "<xProd>" + Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString()) + "</xProd>";
                        xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"] + "</NCM>";
                        xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                        xmlSAT += "<uCom>" + tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"] + "</uCom>";
                        xmlSAT += "<qCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]).ToString("#0.0000").Replace(",", ".") + "</qCom>";
                        xmlSAT += "<vUnCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]).ToString("#0.000").Replace(",", ".") + "</vUnCom>";
                        xmlSAT += "<indRegra>A</indRegra>";
                        xmlSAT += "</prod>";
                        xmlSAT += "<imposto>";

                        xmlSAT += "<ICMS>";

                        if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                            dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>101</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>201</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>202</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>500</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>102</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                          dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                        {
                            xmlSAT += "<ICMSSN102>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>300</CSOSN>";
                            xmlSAT += "</ICMSSN102>";
                        }
                        else
                        {
                            xmlSAT += "<ICMSSN900>";
                            xmlSAT += "<Orig>0</Orig>";
                            xmlSAT += "<CSOSN>900</CSOSN>";
                            xmlSAT += "<pICMS>0.00</pICMS>";
                            xmlSAT += "</ICMSSN900>";
                        }
                        xmlSAT += "</ICMS>";
                        xmlSAT += "<PIS>";
                        xmlSAT += "<PISNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6,"392",false).ToString() + "</CST>";
                        xmlSAT += "</PISNT>";
                        xmlSAT += "</PIS>";
                        xmlSAT += "<COFINS>";
                        xmlSAT += "<COFINSNT>";
                        xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString() + "</CST>";
                        xmlSAT += "</COFINSNT>";
                        xmlSAT += "</COFINS>";
                        xmlSAT += "</imposto>";

                        iRetorno = DarumaDLL.aCFeVender_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    double descontoTotal = Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")) + Math.Abs(Convert.ToDouble(
                    Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));

                    xmlSAT = "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(descontoTotal)).Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "<vCFeLei12741>0.00</vCFeLei12741>";
                    xmlSAT += "</total>";

                    iRetorno = DarumaDLL.aCFeTotalizar_SAT_Daruma(xmlSAT);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    DataTable dtRetorno = dadosEspecies.BuscaEspeciesESatPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        xmlSAT = "<MP><cMP>" + dtRetorno.Rows[i]["ESP_SAT"] + "</cMP>";
                        xmlSAT += "<vMP>" + String.Format("{0:N}", dtRetorno.Rows[i]["VENDA_ESPECIE_VALOR"]).ToString().Replace(",", ".") + "</vMP>";
                        if (dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("03") || dtRetorno.Rows[i]["ESP_SAT"].ToString().Equals("04"))
                        {
                            xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                        }
                        xmlSAT += "</MP>";

                        iRetorno = DarumaDLL.aCFeEfetuarPagamento_SAT_Daruma(xmlSAT);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }

                    iRetorno = DarumaDLL.tCFeEncerrar_SAT_Daruma("<infAdic><infCpl>" + Funcoes.LeParametro(2, "28", true) + "</infCpl></infAdic>");
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    StringBuilder str_RetornoStatus = new StringBuilder(400);
                    iRetorno = DarumaDLL.rConsultarStatus_SAT_Daruma(str_RetornoStatus);

                    string[] split = str_RetornoStatus.ToString().Split('|');

                    System.IO.File.Move(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml", @"" + Funcoes.LeParametro(15, "06", true)
                        + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\CFe" + split[17] + ".xml");

                    System.IO.File.Delete(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFe" + split[17] + ".xml");

                    numeroCFe = "CFe" + split[17];

                    iRetorno = DarumaDLL.rInfoEstendida_SAT_Daruma("1", str_RetornoStatus);
                    vendaNumeroNota = str_RetornoStatus.ToString();

                    Funcoes.GravaParametro(6, "330", numeroCFe, Principal.nomeEstacao);
                    #endregion
                }

                List<VendasEspecies> vendaEspecie = dadosEspecies.DadosEspeciesPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                var vendaFormaPagto = new VendasFormaPagamento();
                DataTable dtForma = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtForma.Rows[0]["QTDE_VIAS"]);
                
                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", totalVenda,
                            vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && dadosVendaEntrega.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        string wsProdutosCupomSemReceita = "";
                        string wsProdutosCupomReceita = "";
                        double valorDUni;

                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;
                            if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomSemReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomSemReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomSemReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                            else if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                        }

                        ComprovantesVinculados.VinculadoDrogabellaPlantaoSAT(qtdeVias, vendaNumeroNota,
                            @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", totalVenda,
                         vendaEspecie[0].Valor, vendaEspecie[0].EspDescricao, dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(),
                         idVenda, wsProdutosCupomSemReceita, wsProdutosCupomReceita, Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true),
                         dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_CARTAO"].ToString(),
                         dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_ENTREGA"].ToString() == "S" ? true : false,
                         dadosVendaEntrega.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_MEDICO"].ToString() == "" ? "" :
                         "CRM: " + dadosVendaEntrega.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVendaEntrega.Rows[0]["VENDA_NUM_RECEITA"],
                         colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), Convert.ToInt32(dadosVendaEntrega.Rows[0]["VENDA_CONVENIO_PARCELA"]));

                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_WEB", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true).Equals("4"))
                    {
                        string produtosComprov = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            double valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                            produtosComprov += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                produtosComprov += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            produtosComprov += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                        }

                        var dadosPagamento = new VendasFormaPagamento();
                        List<VendasFormaPagamento> vendaPedFormaPagto = dadosPagamento.DadosFormaPagamentoPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                        ComprovantesVinculados.VinculadoParticular(qtdeVias, vendaNumeroNota,
                           @"" + Funcoes.LeParametro(15, "06", true) + @"\CFesProcessados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCFe + ".xml", totalVenda,
                           vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), produtosComprov, vendaPedFormaPagto,
                           Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_NOME", "CF_ID", dadosVendaEntrega.Rows[0]["VENDA_CF_ID"].ToString(), false, true), dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString());
                    }
                }

                var dadosFiscais = new CuponsSAT
                    (
                        Principal.empAtual,
                        Principal.estAtual,
                        idVenda,
                        vendaNumeroNota,
                        numeroCFe,
                        totalVenda,
                        "S",
                        "N",
                        Principal.NumeroSessao,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                dadosFiscais.InserirDados(dadosFiscais, true);

                dadosVenda.AtualizarInformacoesFiscais(Principal.estAtual, Principal.empAtual, idVenda, vendaNumeroNota, true);

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            #endregion
        }

        public bool MontarXmlSAT(string numeroDocumento, string nomeNP, long idVenda)
        {
            #region MONTA XML SAT
            try
            {
                totalImpFederal = 0;
                totalImpEstadual = 0;
                totalImpMunicipal = 0;

                xmlSAT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                xmlSAT += "<CFe>";
                xmlSAT += "<infCFe versaoDadosEnt=\"" + Funcoes.LeParametro(15, "02", true) + "\">";
                xmlSAT += "<ide>";
                xmlSAT += "<CNPJ>" + Funcoes.LeParametro(15, "07", true) + "</CNPJ>";
                xmlSAT += "<signAC>" + Funcoes.LeParametro(15, "03", true,Principal.nomeEstacao) + "</signAC>";
                xmlSAT += "<numeroCaixa>" + Funcoes.LeParametro(15, "04", true, Principal.nomeEstacao) + "</numeroCaixa>";
                xmlSAT += "</ide>";

                var estabelecimentos = new Estabelecimento();
                List<Estabelecimento> dadosFiscais = estabelecimentos.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                if (dadosFiscais.Count > 0)
                {
                    xmlSAT += "<emit>";
                    xmlSAT += "<CNPJ>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstCNPJ) + "</CNPJ>";
                    xmlSAT += "<IE>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoEstadual) + "</IE>";
                    xmlSAT += "<IM>" + Funcoes.RemoveCaracter(dadosFiscais[0].EstInscricaoMunicipal) + "</IM>";
                    xmlSAT += "<cRegTribISSQN>1</cRegTribISSQN>";
                    xmlSAT += "<indRatISSQN>N</indRatISSQN>";
                    xmlSAT += "</emit>";
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (!String.IsNullOrEmpty(numeroDocumento))
                {
                    xmlSAT += "<dest><" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">" + Funcoes.RemoveCaracter(numeroDocumento) + "</" + (numeroDocumento.Length <= 14 ? "CPF" : "CNPJ") + ">"
                        + "<xNome>" + nomeNP + "</xNome></dest>";
                }
                else
                    xmlSAT += "<dest/>";


                var dadosProdutos = new Produto();

                double impostoFederal;
                double impostoEstadual;
                double impostoMunicipal;

                var dadosItens = new VendasItens();
                tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    DataTable dtDadosProdutos = dadosProdutos.DadosFiscaisSAT(Convert.ToInt32(tProdutos.Rows[i]["EST_CODIGO"]), Convert.ToInt32(tProdutos.Rows[i]["EMP_CODIGO"]),
                        Convert.ToInt32(tProdutos.Rows[i]["PROD_ID"]));

                    xmlSAT += "<det nItem=\"" + (i + 1) + "\">";
                    xmlSAT += "<prod>";
                    xmlSAT += "<cProd>" + tProdutos.Rows[i]["PROD_CODIGO"] + "</cProd>";
                    xmlSAT += "<xProd>" + Funcoes.RemoverAcentuacao(dtDadosProdutos.Rows[0]["PROD_DESCR"].ToString()) + "</xProd>";
                    xmlSAT += "<NCM>" + dtDadosProdutos.Rows[0]["NCM"] + "</NCM>";
                    xmlSAT += "<CFOP>" + dtDadosProdutos.Rows[0]["CFOP"] + "</CFOP>";
                    xmlSAT += "<uCom>" + tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"] + "</uCom>";
                    xmlSAT += "<qCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]).ToString("#0.0000").Replace(",", ".") + "</qCom>";
                    xmlSAT += "<vUnCom>" + Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]).ToString("#0.000").Replace(",", ".") + "</vUnCom>";
                    xmlSAT += "<indRegra>A</indRegra>";
                    xmlSAT += "</prod>";
                    xmlSAT += "<imposto>";

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"]) > 0)
                        impostoFederal = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["NACIONAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"]) > 0)
                        impostoEstadual = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["ESTADUAL"])) / 100;

                    if (Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"]) > 0)
                        impostoMunicipal = (((Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"]) * Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_QTDE"]))
                            - Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])) * Convert.ToDouble(dtDadosProdutos.Rows[0]["MUNICIPAL"])) / 100;

                    totalImpFederal += impostoFederal;
                    totalImpEstadual += impostoEstadual;
                    totalImpMunicipal += impostoMunicipal;

                    xmlSAT += "<vItem12741>" + String.Format("{0:N}", impostoFederal + impostoEstadual + impostoMunicipal).Replace(",", ".") + "</vItem12741>";
                    xmlSAT += "<ICMS>";

                    if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("000") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("020") ||
                        dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("00") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("20"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("010") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("070") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("10") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("70"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>201</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("030") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("30"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>202</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("060") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("60"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>500</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("040") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("041") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("40") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("41"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>102</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else if (dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("050") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("051") ||
                      dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("50") || dtDadosProdutos.Rows[0]["PROD_CST"].ToString().Equals("51"))
                    {
                        xmlSAT += "<ICMSSN102>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>300</CSOSN>";
                        xmlSAT += "</ICMSSN102>";
                    }
                    else
                    {
                        xmlSAT += "<ICMSSN900>";
                        xmlSAT += "<Orig>0</Orig>";
                        xmlSAT += "<CSOSN>900</CSOSN>";
                        xmlSAT += "<pICMS>0.00</pICMS>";
                        xmlSAT += "</ICMSSN900>";
                    }
                    xmlSAT += "</ICMS>";
                    xmlSAT += "<PIS>";
                    xmlSAT += "<PISNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString();
                    xmlSAT += "</CST>";
                    xmlSAT += "</PISNT>";
                    xmlSAT += "</PIS>";
                    xmlSAT += "<COFINS>";
                    xmlSAT += "<COFINSNT>";
                    xmlSAT += "<CST>" + Funcoes.LeParametro(6, "392", false).ToString();
                    xmlSAT += "</CST>";
                    xmlSAT += "</COFINSNT>";
                    xmlSAT += "</COFINS>";
                    xmlSAT += "</imposto>";
                    xmlSAT += "</det>";
                }

                double descontoTotal = Convert.ToDouble(tProdutos.Compute("SUM(VENDA_ITEM_DIFERENCA)", "")) + Math.Abs(Convert.ToDouble(
                    Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));
                if (Math.Abs(descontoTotal) > 0)
                {
                    xmlSAT += "<total>";
                    xmlSAT += "<DescAcrEntr>";
                    xmlSAT += "<vDescSubtot>" + String.Format("{0:N}", Math.Abs(descontoTotal)).Replace(",", ".") + "</vDescSubtot>";
                    xmlSAT += "</DescAcrEntr>";
                    xmlSAT += "</total>";
                }
                else
                {
                    xmlSAT += "<total/>";
                }

                xmlSAT += "<pgto>";

                for (int i = 0; i < dgEspecies.Rows.Count; i++)
                {
                    xmlSAT += "<MP><cMP>" + dgEspecies.Rows[i].Cells["ESP_SAT"].Value + "</cMP>";
                    xmlSAT += "<vMP>" + String.Format("{0:N}", dgEspecies.Rows[i].Cells["ESP_VALOR"].Value).ToString().Replace(",", ".") + "</vMP>";
                    if (dgEspecies.Rows[i].Cells["ESP_SAT"].Value.ToString().Equals("03") || dgEspecies.Rows[i].Cells["ESP_SAT"].Value.ToString().Equals("04"))
                    {
                        xmlSAT += "<cAdmC>" + Funcoes.LeParametro(15, "08", true, Principal.nomeEstacao) + "</cAdmC>";
                    }
                    xmlSAT += "</MP>";
                }

                xmlSAT += "</pgto>";
                xmlSAT += "</infCFe>";
                xmlSAT += "</CFe>";

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "XML SAT Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            #endregion
        }

        private bool ImprimirCupomFiscal(long idVenda)
        {
            #region CUPOM FISCAL
            try
            {
                if (Imprimir.VerificaModeloImpressoraFiscal().Equals(false))
                {
                    return false;
                }

                #region CPF/CNPJ
                if (MessageBox.Show("Imprimi Cupom Fiscal com CPF ou CNPJ?", "Impressão de Comprovante", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    frmVenNotaPaulista notaPaulista = new frmVenNotaPaulista();
                    var dadosCliente = new Cliente();
                    DataTable dt = dadosCliente.SelecionaDadosDoClientePorVendaID(idVenda, Principal.empAtual, Principal.estAtual);
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dt.Rows[0]["CF_TIPO_DOCTO"]).Equals(0))
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "000,000,000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }
                        else
                        {
                            notaPaulista.rdbCpf.Checked = true;
                            notaPaulista.txtDocto.Mask = "00,000,000/0000-00";
                            notaPaulista.txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                            notaPaulista.txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                        }

                        notaPaulista.ShowDialog();
                    }
                    else
                    {
                        notaPaulista.rdbCpf.Checked = true;
                        notaPaulista.txtDocto.Mask = "000,000,000-00";
                        notaPaulista.ShowDialog();
                    }
                }
                #endregion
                
                string modeloImpressora = Funcoes.LeParametro(6, "08", true, Funcoes.LeParametro(6, "123", true).Equals("N") ? "GERAL" : Principal.nomeEstacao).ToUpper();
                
                Cursor = Cursors.WaitCursor;
                string vendaNumeroCaixa = "";

                string retorno = Imprimir.ImpFiscalInformacoesECF("107", vendaNumeroCaixa);
                if (String.IsNullOrEmpty(retorno))
                    return false;

                vendaNumeroNota = Imprimir.ImpFiscalNumeroCupom();

                if (modeloImpressora.Equals("EPSON"))
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt64(vendaNumeroNota.Substring(58, 6)) + 1);
                }
                else
                {
                    vendaNumeroNota = Convert.ToString(Convert.ToInt32(vendaNumeroNota) + 1);
                }

                if (!Imprimir.ImpFiscalAbreCupom(Principal.NumCpfCnpj, Principal.NomeNP))
                    return false;

                List<VendaItem> venItens = new List<VendaItem>();

                var dadosItens = new VendasItens();
                tProdutos = dadosItens.BuscaItensDaVendaPorID(idVenda, Principal.estAtual, Principal.empAtual);

                for (int i = 0; i < tProdutos.Rows.Count; i++)
                {
                    venItens.Add(new VendaItem
                    {
                        ItemECF = tProdutos.Rows[i]["PROD_ECF"].ToString(),
                        ItemQtde = tProdutos.Rows[i]["VENDA_ITEM_QTDE"].ToString(),
                        ItemVlUnit = tProdutos.Rows[i]["VENDA_ITEM_UNITARIO"].ToString(),
                        ItemTipoDesc = "D$",
                        ItemVlDesc = Math.Abs(Convert.ToDouble(tProdutos.Rows[i]["VENDA_ITEM_DIFERENCA"])).ToString(),
                        ItemCodBarra = tProdutos.Rows[i]["PROD_CODIGO"].ToString(),
                        ItemUniade = tProdutos.Rows[i]["VENDA_ITEM_UNIDADE"].ToString(),
                        ItemDescr = tProdutos.Rows[i]["PROD_DESCR"].ToString(),
                        ItemNcm = tProdutos.Rows[i]["NCM"].ToString(),
                        ItemTipo = "0"
                    });
                }

                if (!Imprimir.ImpFiscalVenderItem(venItens))
                    return false;

                double descontoTotal = Math.Abs(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("VENDAS", "VENDA_DIFERENCA", "VENDA_ID", idVenda.ToString(), true, false, true)));

                if (!Imprimir.ImpFiscalTotalizarCupom("D", descontoTotal.ToString()))
                    return false;

                List<VendasEspecies> vendaEspecie = new List<VendasEspecies>();
                var dadosEspecies = new VendasEspecies();
                vendaEspecie = dadosEspecies.DadosEspeciesPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                if (!Imprimir.ImpFiscalMeioPagamento(vendaEspecie))
                    return false;

                var colaborador = new Colaborador();
                dadosVendaEntrega = Util.SelecionaRegistrosTodosOuEspecifico("VENDAS", "", "VENDA_ID", idVenda.ToString(), true);

                string mensagem = "";
                string wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);
                if (dadosVendaEntrega.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual)
                        + "    " + "Num.Pedido:" + idVenda, 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    if (!String.IsNullOrEmpty(dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao:" + dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO"].ToString(), 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    if (!String.IsNullOrEmpty(dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao com Receita:" + dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(), 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                    mensagem += Funcoes.PrencherEspacoEmBranco("Administradora: DROGABELLA/PLANTAO CARD", 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Cartao: " + dadosVendaEntrega.Rows[0]["VENDA_CARTAO"].ToString(), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Conveniado:  " + (dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ?
                        dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32) : dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("EMPRESA: " + ((dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " +
                        wsNomeEmpresa).Length > 40 ? (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 40)
                        : (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa)), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }
                else if (dadosVendaEntrega.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("FUNCIONAL"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Autorizacao:" + dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO"].ToString(), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Administradora:  FUNCIONAL CARD", 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Cartao:  " + dadosVendaEntrega.Rows[0]["VENDA_CARTAO"], 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Conveniado:  " + (dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString().Length > 33 ? dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString().Substring(0, 32)
                        : dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";

                    wsNomeEmpresa = Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true);

                    mensagem += Funcoes.PrencherEspacoEmBranco("Empresa: " + ((dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Length > 32 ? (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).Substring(0, 32)
                        : (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + wsNomeEmpresa).ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";

                    if (!String.IsNullOrEmpty(dadosVendaEntrega.Rows[0]["VENDA_MEDICO"].ToString()))
                    {
                        mensagem += Funcoes.PrencherEspacoEmBranco("CRM: " + dadosVendaEntrega.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVendaEntrega.Rows[0]["VENDA_NUM_RECEITA"], 49);
                        if (modeloImpressora.Equals("EPSON"))
                            mensagem += "\n";
                    }
                }
                else if (dadosVendaEntrega.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("PARTICULAR"))
                {
                    mensagem += Funcoes.PrencherEspacoEmBranco("Vendedor: " + colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual)
                      + "    " + "Num.Pedido:" + idVenda, 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Empresa: " + ((dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - "
                        + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString())).Length > 32 ?
                        (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString())).Substring(0, 32)
                       : (dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString() + " - " + Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_NOME", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString())).ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                    mensagem += Funcoes.PrencherEspacoEmBranco("Nome: " + Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CON_NOME", "CF_ID", dadosVendaEntrega.Rows[0]["VENDA_CF_ID"].ToString()), 49);
                    if (modeloImpressora.Equals("EPSON"))
                        mensagem += "\n";
                }

                #region LEI DE IMPOSTO
                double impostoFederal, impostoEstadual, impostoMunicipal;
                var buscaImpostos = new Ibpt();

                for (int i = 0; i < venItens.Count; i++)
                {
                    impostoFederal = 0;
                    impostoEstadual = 0;
                    impostoMunicipal = 0;

                    DataTable dtRetorno = buscaImpostos.BuscaImpostosPorNCM(venItens[i].ItemNcm);

                    if (dtRetorno.Rows.Count > 0)
                    {
                        if (Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"]) > 0)
                            impostoFederal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["NACIONAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"]) > 0)
                            impostoEstadual = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["ESTADUAL"])) / 100;

                        if (Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"]) > 0)
                            impostoMunicipal = (((Convert.ToDouble(venItens[i].ItemVlUnit) * Convert.ToInt32(venItens[i].ItemQtde))
                                - Convert.ToDouble(venItens[i].ItemVlDesc)) * Convert.ToDouble(dtRetorno.Rows[0]["MUNICIPAL"])) / 100;

                        totalImpFederal += impostoFederal;
                        totalImpEstadual += impostoEstadual;
                        totalImpMunicipal += impostoMunicipal;
                    }
                }

                mensagem += Funcoes.PrencherEspacoEmBranco("Trib aprox R$:" + String.Format("{0:N}", totalImpFederal).Replace(",", ".") + " Fed, " + String.Format("{0:N}", totalImpEstadual).Replace(",", ".") + " Est, " + String.Format("{0:N}", totalImpMunicipal).Replace(",", ".") + " Mun", 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco("Fonte:IBPT   5oi7eW", 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                mensagem += Funcoes.PrencherEspacoEmBranco(Funcoes.LeParametro(2, "28", false), 49);
                if (modeloImpressora.Equals("EPSON"))
                    mensagem += "\n";
                #endregion

                if (!Imprimir.ImpFiscalFechaCupomMsg(mensagem))
                    return false;

                #region CUPONS VINCULADOS
                double valorDUni;

                var vendaFormaPagto = new VendasFormaPagamento();
                DataTable dtForma = vendaFormaPagto.BuscaFormasPagamentosPorVendaID(Principal.empAtual, Principal.estAtual, idVenda);

                int qtdeVias = Funcoes.LeParametro(2, "37", false).Equals("N") ? Convert.ToInt32(Funcoes.LeParametro(2, "26", false)) : Convert.ToInt32(dtForma.Rows[0]["QTDE_VIAS"]);
                
                for (int i = 0; i < vendaEspecie.Count; i++)
                {
                    if (vendaEspecie[i].EspVinculado.Equals("S") && (vendaEspecie[i].EspSAT.Equals("03") || vendaEspecie[i].EspSAT.Equals("04")))
                    {
                        ComprovantesVinculados.VinculadoDebitoCreditoFiscal(qtdeVias, vendaNumeroNota, totalVenda,
                            vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && dadosVendaEntrega.Rows[0]["VENDA_BENEFICIO"].ToString().Equals("DROGABELLA/PLANTAO") && vendaEspecie[i].EspDescricao.Equals("CONVENIO"))
                    {
                        string wsProdutosCupomSemReceita = "";
                        string wsProdutosCupomReceita = "";
                       
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;
                            if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("N"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomSemReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomSemReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomSemReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                            else if (tProdutos.Rows[x]["VENDA_ITEM_RECEITA"].ToString().Equals("S"))
                            {
                                valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                                wsProdutosCupomReceita += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                                if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                                {
                                    wsProdutosCupomReceita += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                                }
                                wsProdutosCupomReceita += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            }
                        }

                        ComprovantesVinculados.VinculadoDrogabellaPlantaoFiscal(qtdeVias, vendaNumeroNota,
                            Convert.ToDateTime(dadosVendaEntrega.Rows[0]["VENDA_DATA_HORA"]), totalVenda,
                            vendaEspecie[0].Valor, vendaEspecie[0].EspDescricao, dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_AUTORIZACAO_RECEITA"].ToString(),
                            idVenda, wsProdutosCupomSemReceita, wsProdutosCupomReceita, wsNomeEmpresa,
                            dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_CARTAO"].ToString(),
                            dadosVendaEntrega.Rows[0]["VENDA_NOME_CARTAO"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_ENTREGA"].ToString() == "S" ? true : false,
                            dadosVendaEntrega.Rows[0]["VENDA_OBRIGA_RECEITA"].ToString(), dadosVendaEntrega.Rows[0]["VENDA_MEDICO"].ToString() == "" ? "" :
                            "CRM: " + dadosVendaEntrega.Rows[0]["VENDA_MEDICO"] + " Num. Receita: " + dadosVendaEntrega.Rows[0]["VENDA_NUM_RECEITA"],
                            colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), Convert.ToInt32(dadosVendaEntrega.Rows[0]["VENDA_CONVENIO_PARCELA"]));
                    }
                    else if (vendaEspecie[i].EspVinculado.Equals("S") && Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_WEB", "CON_CODIGO", dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), false, true).Equals("4"))
                    {
                        var dadosPagamento = new VendasFormaPagamento();
                        List<VendasFormaPagamento> vendaPedFormaPagto = dadosPagamento.DadosFormaPagamentoPorVendaID(idVenda, Principal.empAtual, Principal.estAtual);

                        string produtos = "";
                        for (int x = 0; x < tProdutos.Rows.Count; x++)
                        {
                            valorDUni = 0;
                            
                            valorDUni = Convert.ToDouble(Math.Round((Convert.ToDecimal(tProdutos.Rows[x]["VENDA_ITEM_TOTAL"])) / Convert.ToUInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"]), 2) * 100);
                            produtos += Funcoes.PrencherEspacoEmBranco(tProdutos.Rows[x]["PROD_CODIGO"].ToString(), 13) + " " + (tProdutos.Rows[x]["PROD_DESCR"].ToString().Length > 29 ? tProdutos.Rows[x]["PROD_DESCR"].ToString().Substring(0, 28) : tProdutos.Rows[x]["PROD_DESCR"].ToString()) + "\n";
                            if (Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])) > 0)
                            {
                                produtos += "    R$ " + tProdutos.Rows[x]["VENDA_ITEM_UNITARIO"].ToString().Replace(",", ".") + " ( Desc = R$ " + Math.Abs(Convert.ToDouble(tProdutos.Rows[x]["VENDA_ITEM_DIFERENCA"])).ToString().Replace(",", ".") + ")\n";
                            }
                            produtos += "    R$ " + (valorDUni / 100).ToString().Replace(",", ".") + " X " + tProdutos.Rows[x]["VENDA_ITEM_QTDE"] + " =  R$ " + ((valorDUni / 100) * Convert.ToInt32(tProdutos.Rows[x]["VENDA_ITEM_QTDE"])).ToString().Replace(",", ".") + "\n";
                            
                        }

                        ComprovantesVinculados.VinculadoParticularFiscal(qtdeVias, vendaNumeroNota, idVenda, totalVenda,
                           vendaEspecie[i].Valor, vendaEspecie[i].EspDescricao, colaborador.NomeColaborador(dadosVendaEntrega.Rows[0]["VENDA_COL_CODIGO"].ToString(), Principal.empAtual), vendaPedFormaPagto,
                           Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_NOME", "CF_ID", dadosVendaEntrega.Rows[0]["VENDA_CF_ID"].ToString(), false, true), dadosVendaEntrega.Rows[0]["VENDA_CON_CODIGO"].ToString(), produtos);
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão Cupom Fiscal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            #endregion
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
