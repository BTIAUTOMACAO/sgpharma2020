﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenFalteiro : Form
    {
        private frmVenda venda;

        public frmVenFalteiro(frmVenda fVenda)
        {
            InitializeComponent();
            venda = fVenda;
            RegisterFocusEvents(this.Controls);
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosTransferencia(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }
                    txtQtde.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {

            try
            {
                if (txtDescr.Text.Trim() != "" && String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCliente.Focus();
        }

        private void txtCliente_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAdicionar.PerformClick();
        }

        private void frmVenFalteiro_Load(object sender, EventArgs e)
        {
            try
            {
                txtCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    this.btnAdicionar.PerformClick();
                    break;

            }
        }

        private async void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtQtde.Text))
                {
                    if (Convert.ToInt32(txtQtde.Text) > 0)
                    {
                        Cursor = Cursors.WaitCursor;
                        int id = Funcoes.IdentificaVerificaID("LANCAMENTO_FALTAS", "ID", 0, "", 0);
                        var faltas = new LancamentoFaltas(
                            Principal.empAtual,
                            Principal.estAtual,
                            id,
                            txtCodBarras.Text,
                            Convert.ToInt32(txtQtde.Text),
                            txtCliente.Text,
                            txtObs.Text,
                            Principal.nomeEstacao,
                            "P",
                            Convert.ToInt32(venda.txtVendedor.Text),
                            DateTime.Now,
                            Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", venda.txtVendedor.Text), 
                            DateTime.Now,
                            Principal.usuario,
                            0
                            );

                        if (faltas.InsereRegistros(faltas))
                        {
                            await InsereFalta(faltas);

                            Cursor = Cursors.Default;
                            MessageBox.Show("Lançamento realizado com sucesso!", "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Funcoes.GravaLogInclusao("ID", id.ToString(), Principal.usuario, "LACAMENTO_FALTAS", txtCodBarras.Text, Principal.estAtual, Principal.empAtual);
                            txtCodBarras.Text = "";
                            txtDescr.Text = "";
                            txtQtde.Text = "";
                            txtCliente.Text = "";
                            txtObs.Text = "";

                            if (MessageBox.Show("Deseja fazer outro lançamento?", "Lançamento de Faltas", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                txtCodBarras.Focus();
                            }
                            else
                                this.Close();
                        }
                        else
                            txtCodBarras.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Quatidade não pode ser Zero!", "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtQtde.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Quatidade não pode ser em Branco!", "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQtde.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> InsereFalta(LancamentoFaltas falta)
        {
            try
            {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Falteiro/InsereFalteiro?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true)
                                        + "&idDaLoja=" + falta.ID 
                                        + "&prodCodigo=" + falta.ProdCodigo 
                                        + "&qtde=" + falta.Qtde
                                        + "&observacao=" + falta.Observacao 
                                        + "&usuario=" + falta.OpCadastro);
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\Falteiro.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + falta.ProdCodigo + " / QTDE: " + falta.Qtde);
                            }
                        }
                    }

                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenFalteiro_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }
    }
}
