﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.EPharma
{
    public partial class frmMenuEPharma : Form
    {
        private frmVenda fVenda;
        public frmMenuEPharma(frmVenda frmVenda)
        {
            InitializeComponent();
            fVenda = frmVenda;
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnConsultaSaldo_Click(object sender, EventArgs e)
        {
            frmConsultaSaldo saldo = new frmConsultaSaldo();
            saldo.ShowDialog();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            fVenda.vendaBeneficio = "";
            fVenda.vendaNumeroNSU = "";
            this.Close();
        }

        private void btnAutorizacao_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
            frmAutorizacao autorizacao = new frmAutorizacao(fVenda);
            autorizacao.ShowDialog();

        }

        private void btnReimpressao_Click(object sender, EventArgs e)
        {
            frmReimpressão impressao = new frmReimpressão();
            impressao.ShowDialog();
        }

        private void btnConsultaSaldo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnReimpressao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnSair_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmMenuEPharma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnConsultaSaldo.PerformClick();
                    break;
                case Keys.F2:
                    btnAutorizacao.PerformClick();
                    break;
                case Keys.F3:
                    btnReimpressao.PerformClick();
                    break;
                case Keys.F4:
                    btnSair.PerformClick();
                    break;
            }
        }

        private void btnConsultaSaldo_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultaSaldo, "Consulta Saldo (F1)");
        }

        private void btnAutorizacao_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAutorizacao, "Autorização (F2)");
        }

        private void btnReimpressao_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnReimpressao, "Reimpressão (F3)");
        }

        private void btnSair_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnSair, "Sair (F4)");
        }
            }
}
