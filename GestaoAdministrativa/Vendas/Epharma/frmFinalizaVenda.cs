﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.EPharma
{
    public partial class frmFinalizaVenda : Form
    {
        frmVenda fVenda;

        public frmFinalizaVenda(frmVenda formVendas)
        {
            InitializeComponent();
            fVenda = formVendas;
        }

        private void frmFinalizaVenda_Load(object sender, EventArgs e)
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                lblMensagem.Text = "Enviando Confirmação da Venda!";
                lblMensagem.Refresh();
                this.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void lblMensagem_Click(object sender, EventArgs e)
        {

        }

        private void frmFinalizaVenda_Shown(object sender, EventArgs e)
        {
            try
            {
                Epharma epharma = new Epharma();
                string sequencia = epharma.EnviaVenda(fVenda.vendaNumeroNSU, fVenda.vendaNumeroNota, fVenda.numCFE);

                lblMensagem.Text = "Esperando Confirmação da Venda!";
                lblMensagem.Refresh();

                if (epharma.LeRetornoVenda(sequencia).Equals(true))
                {
                    string teste = fVenda.vendaAutor;
                    string[] retorno = File.ReadAllLines(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
                    EpharmaVendas vendas = new EpharmaVendas
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        NumTrans = retorno[0].Substring(48, 7).Trim() == "" ? 0 : Convert.ToInt32(retorno[0].Substring(48, 7)),
                        NSU = Convert.ToInt32(retorno[0].Substring(55, 12)),
                        VendaId = fVenda.vendaID,
                        NumDoc = Convert.ToInt32(fVenda.vendaNumeroNota),
                        ECF = Funcoes.LeParametro(15, "10", false),
                        Status = "C",
                        Tp_conv = 1,
                        Autorizacao = Convert.ToInt32(fVenda.vendaAutor),

                    };
                    DataTable dtVenda = vendas.BuscaVendasPorStatus('P');
                    vendas.AtualizaVenda(vendas, dtVenda);

                    fVenda.cupomBeneficio = retorno[1];
                    lblMensagem.Text = "Gerando Relatório Gerencial";
                    lblMensagem.Refresh();
                    this.Hide();
                    this.Close();
                }
                else
                {
                    fVenda.cupomBeneficio = "";
                    this.Hide();
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
