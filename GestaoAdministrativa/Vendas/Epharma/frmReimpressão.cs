﻿using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.EPharma
{
    public partial class frmReimpressão : Form
    {
        public frmReimpressão()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtDoc.Text = String.Empty;
            txtDoc.Focus();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtAutorizacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13 && !String.IsNullOrEmpty(txtDoc.Text))
            {
                Reimpressao();
            }
        }

        public void Reimpressao()
         {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteDados().Equals(true))
                {
                    Epharma epharma = new Epharma();
                    EpharmaVendas vendaEpharma = new EpharmaVendas();
                    string numTrans = vendaEpharma.BuscaNumeroTrans(txtDoc.Text.TrimStart('0'));
                    if (String.IsNullOrEmpty(numTrans))
                    {
                        MessageBox.Show("Documento não encontrado", "Reimpressão E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        string sequencia = epharma.SolicitaReimpressao(numTrans);
                        if (String.IsNullOrEmpty(sequencia))
                        {
                            MessageBox.Show("Não foi possivel enviar a solicitação de Reimpressão", "Reimpressão E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string retorno = epharma.LeRetornoReimpressao(sequencia);

                            if (!String.IsNullOrEmpty(retorno.Trim()))
                            {
                                ComprovantesVinculados.CupomVinculadoBeneficios(retorno);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpressão E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool ConsisteDados()
        {
            if (String.IsNullOrEmpty(txtDoc.Text.Trim()))
            {
                MessageBox.Show("Campo Autorização não pode estar vazio", "Reimpressão E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
            {
                return true;
            }

        }

        private void btnReimpressao_Click(object sender, EventArgs e)
        {
            Reimpressao();
        }

        private void btnReimpressao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnSair_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmReimpressão_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnReimpressao.PerformClick();
                    break;
                case Keys.F2:
                    btnLimpar.PerformClick();
                    break;
                case Keys.F3:
                    btnSair.PerformClick();
                    break;
            }
        }

        private void btnReimpressao_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnReimpressao, "Reimpressão (F1)");
        }

        private void btnLimpar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnLimpar, "Limpar (F2)");
        }

        private void btnSair_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnSair, "Sair (F3)");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmReimpressão_Load(object sender, EventArgs e)
        {
            try
            {
                txtDoc.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reimpressão E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
