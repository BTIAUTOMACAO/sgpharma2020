﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.EPharma
{
    public partial class frmConsultaSaldo : Form
    {
        public frmConsultaSaldo()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void frmConsultaSaldo_Load(object sender, EventArgs e)
        {
            DataTable dtPesq = Util.CarregarCombosPorEstabelecimento("COD_CONV", "NOME_DESCR", "EPHARMA_CONVENIOS");
            cmbConvenios.DataSource = dtPesq;
            cmbConvenios.DisplayMember = "NOME_DESCR";
            cmbConvenios.ValueMember = "COD_CONV";
            cmbConvenios.SelectedIndex = -1;
            cmbConvenios.Focus();
        }

        private void txtCartao_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 13)
            {
                ConsultaSaldo();
            }
        }

        public void ConsultaSaldo()
        {
            Cursor = Cursors.WaitCursor;
            Epharma epharma = new Epharma();
            epharma.Contador();
            string sequencia = Util.LerArquivoTXT(Funcoes.LeParametro(14, "12", false) + "ePharma.SEQ");

            SolicitaSaldo(sequencia);
            if (Retorno(sequencia).Equals(true))
            {
                epharma.LimparPastas();
            }
            Cursor = Cursors.Default;
        }
        public void SolicitaSaldo(string sequencia)
        {
            string linha = sequencia + "70" + txtCartao.Text.PadLeft(19, ' ') + cmbConvenios.SelectedValue + "".PadRight(113, ' ');

            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "13", false) + sequencia + ".txt", linha);
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\ENV_" + sequencia + ".txt", linha);
        }

        public bool Retorno(string sequencia)
        {
            var inicio = DateTime.UtcNow;
            while (!File.Exists(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt"))
            {
                if (DateTime.UtcNow - inicio > TimeSpan.FromSeconds(30))
                {
                    MessageBox.Show("Erro: Não foi possível verificar o Saldo ", "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            Thread.Sleep(Convert.ToInt16(Funcoes.LeParametro(14, "17", false)));
            string retornoSaldo = Util.LerArquivoTXT(Funcoes.LeParametro(14, "14", false) + sequencia + ".txt");
            Util.CriarArquivoTXT(Funcoes.LeParametro(14, "12", false) + @"\LOGS\REQ_" + sequencia + ".txt", retornoSaldo);

            if (retornoSaldo.Substring(6, 2).Equals("ER"))
            {
                MessageBox.Show(retornoSaldo.Substring(8, 40).Trim(), "Retorno  Saldo E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            else
            {
                lblSaldo.Text = "Saldo: R$ " + string.Format("{0:0,0.00}", (Convert.ToDouble(retornoSaldo.Substring(48, 7)) / 100) * 0.01);
                lblSaldo.Visible = true;
                return true;
            }

        }

        #region Botoes
        private void btnConsultaSaldo_Click(object sender, EventArgs e)
        {
            ConsultaSaldo();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            cmbConvenios.SelectedIndex = -1;
            txtCartao.Text = String.Empty;
            lblSaldo.Text = String.Empty;
            lblSaldo.Visible = false;
            cmbConvenios.Focus();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void cmbConvenios_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbConvenios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCartao.Focus();
        }

        private void txtCartao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnConsultaSaldo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnSair_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmConsultaSaldo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnConsultaSaldo.PerformClick();
                    break;
                case Keys.F2:
                    btnLimpar.PerformClick();
                    break;
                case Keys.F3:
                    btnSair.PerformClick();
                    break;
            }
        }

        private void btnConsultaSaldo_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultaSaldo, "Consulta Saldo (F1)");
        }

        private void btnLimpar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnLimpar, "Limpar (F2)");
        }

        private void btnSair_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnSair, "Sair (F3)");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
    }
}
