﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio.Beneficios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.EPharma
{
    public partial class frmProdutos : Form
    {
        int nsu, numTrans;
        private frmVenda fVenda;
        public frmProdutos(int nSU, int nTrans, frmVenda frmVenda)
        {
            InitializeComponent();
            fVenda = frmVenda;
            nsu = nSU;
            numTrans = nTrans;
        }

        private void frmEPharmaProdutos_Load(object sender, EventArgs e)
        {
            CarregaDados();
        }
        public void CarregaDados()
        {
            EpharmaProdutos vendas = new EpharmaProdutos();
            DataTable dtProdutos = vendas.BuscaItensPorNsu(nsu, numTrans);
            dgProdutos.DataSource = dtProdutos;

            double valorCliente = 0;
            double valorDesconto = 0;

            for (int i = 0; i < dtProdutos.Rows.Count; ++i)
            {
                valorCliente = valorCliente + (Convert.ToDouble(dtProdutos.Rows[i]["VL_PFINAL"]) * Convert.ToInt32(dtProdutos.Rows[i]["QTD"]));
                valorDesconto = valorDesconto + (Convert.ToDouble(dtProdutos.Rows[i]["VL_PFABRICA"]) * Convert.ToInt32(dtProdutos.Rows[i]["QTD"]));
            }
            lblValorCliente.Text = String.Format("{0:0,0.00}", valorCliente);
            lblFinal.Text = String.Format("{0:0,0.00}", valorDesconto);
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            fVenda.vendaBeneficio = "EPHARMA";
            fVenda.vendaNumeroNSU = nsu.ToString();
            fVenda.numTrans = numTrans.ToString();
            this.Hide();
            this.Close();

        }
        
        private void btnDescartar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnDescartar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnDescartar, "Descartar (F1)");
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnConfirmar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConfirmar, "Confirmar (F2)");
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(" Tem certeza que deseja descartar essa autorização ?", "E-Pharma", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Epharma epharma = new Epharma();

                if (epharma.CancelamentoAutorizacao(nsu, numTrans).Equals(true))
                {
                    MessageBox.Show("Autorização descartada com sucesso ", "E-Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    EpharmaVendas vendas = new EpharmaVendas();
                    DataTable dtProdutos = vendas.BuscaVendasPorNSU(nsu);
                    vendas.AtualizaStatusVenda(nsu, numTrans, "D");
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Hide();
                    this.Close();
                }
            }
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

    }
}
