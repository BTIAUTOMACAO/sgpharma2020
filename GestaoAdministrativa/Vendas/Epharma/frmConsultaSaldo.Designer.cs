﻿namespace GestaoAdministrativa.Vendas.EPharma
{
    partial class frmConsultaSaldo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultaSaldo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSair = new System.Windows.Forms.Button();
            this.txtCartao = new System.Windows.Forms.TextBox();
            this.lblIdentificador = new System.Windows.Forms.Label();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnConsultaSaldo = new System.Windows.Forms.Button();
            this.cmbConvenios = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(4, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 389);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consulta Saldo";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblSaldo);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnSair);
            this.panel1.Controls.Add(this.txtCartao);
            this.panel1.Controls.Add(this.lblIdentificador);
            this.panel1.Controls.Add(this.btnLimpar);
            this.panel1.Controls.Add(this.btnConsultaSaldo);
            this.panel1.Controls.Add(this.cmbConvenios);
            this.panel1.Location = new System.Drawing.Point(7, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(411, 361);
            this.panel1.TabIndex = 0;
            // 
            // lblSaldo
            // 
            this.lblSaldo.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaldo.ForeColor = System.Drawing.Color.Black;
            this.lblSaldo.Location = new System.Drawing.Point(14, 247);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(382, 51);
            this.lblSaldo.TabIndex = 9;
            this.lblSaldo.Text = "Saldo: R$ 00,00";
            this.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSaldo.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(14, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(382, 100);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.label1.Location = new System.Drawing.Point(49, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cartão :";
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSair.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ForeColor = System.Drawing.Color.Black;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(277, 308);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(119, 43);
            this.btnSair.TabIndex = 8;
            this.btnSair.Text = "Sair (F3)";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            this.btnSair.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSair_KeyDown);
            this.btnSair.MouseHover += new System.EventHandler(this.btnSair_MouseHover);
            // 
            // txtCartao
            // 
            this.txtCartao.BackColor = System.Drawing.Color.White;
            this.txtCartao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCartao.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCartao.ForeColor = System.Drawing.Color.Black;
            this.txtCartao.Location = new System.Drawing.Point(113, 143);
            this.txtCartao.MaxLength = 19;
            this.txtCartao.Name = "txtCartao";
            this.txtCartao.Size = new System.Drawing.Size(283, 29);
            this.txtCartao.TabIndex = 5;
            this.txtCartao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCartao_KeyDown);
            this.txtCartao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCartao_KeyPress);
            // 
            // lblIdentificador
            // 
            this.lblIdentificador.AutoSize = true;
            this.lblIdentificador.BackColor = System.Drawing.Color.White;
            this.lblIdentificador.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.lblIdentificador.Location = new System.Drawing.Point(11, 117);
            this.lblIdentificador.Name = "lblIdentificador";
            this.lblIdentificador.Size = new System.Drawing.Size(96, 16);
            this.lblIdentificador.TabIndex = 3;
            this.lblIdentificador.Text = "Identificador :";
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.ForeColor = System.Drawing.Color.Black;
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(152, 308);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(119, 43);
            this.btnLimpar.TabIndex = 7;
            this.btnLimpar.Text = "Limpar (F2)";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            this.btnLimpar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnLimpar_KeyDown);
            this.btnLimpar.MouseHover += new System.EventHandler(this.btnLimpar_MouseHover);
            // 
            // btnConsultaSaldo
            // 
            this.btnConsultaSaldo.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultaSaldo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultaSaldo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaSaldo.ForeColor = System.Drawing.Color.Black;
            this.btnConsultaSaldo.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaSaldo.Image")));
            this.btnConsultaSaldo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultaSaldo.Location = new System.Drawing.Point(14, 192);
            this.btnConsultaSaldo.Name = "btnConsultaSaldo";
            this.btnConsultaSaldo.Size = new System.Drawing.Size(382, 43);
            this.btnConsultaSaldo.TabIndex = 6;
            this.btnConsultaSaldo.Text = "Consulta Saldo (F1)";
            this.btnConsultaSaldo.UseVisualStyleBackColor = false;
            this.btnConsultaSaldo.Click += new System.EventHandler(this.btnConsultaSaldo_Click);
            this.btnConsultaSaldo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConsultaSaldo_KeyDown);
            this.btnConsultaSaldo.MouseHover += new System.EventHandler(this.btnConsultaSaldo_MouseHover);
            // 
            // cmbConvenios
            // 
            this.cmbConvenios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConvenios.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConvenios.ForeColor = System.Drawing.Color.Black;
            this.cmbConvenios.FormattingEnabled = true;
            this.cmbConvenios.Location = new System.Drawing.Point(113, 112);
            this.cmbConvenios.Name = "cmbConvenios";
            this.cmbConvenios.Size = new System.Drawing.Size(283, 24);
            this.cmbConvenios.TabIndex = 2;
            this.cmbConvenios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbConvenios_KeyDown);
            this.cmbConvenios.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbConvenios_KeyPress);
            // 
            // frmConsultaSaldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.ClientSize = new System.Drawing.Size(433, 394);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConsultaSaldo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta Saldo";
            this.Load += new System.EventHandler(this.frmConsultaSaldo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmConsultaSaldo_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbConvenios;
        private System.Windows.Forms.TextBox txtCartao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIdentificador;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnConsultaSaldo;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}