﻿namespace GestaoAdministrativa.Vendas.EPharma
{
    partial class frmMenuEPharma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenuEPharma));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSair = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnReimpressao = new System.Windows.Forms.Button();
            this.btnConsultaSaldo = new System.Windows.Forms.Button();
            this.btnAutorizacao = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnSair);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnReimpressao);
            this.panel1.Controls.Add(this.btnConsultaSaldo);
            this.panel1.Controls.Add(this.btnAutorizacao);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 342);
            this.panel1.TabIndex = 0;
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSair.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ForeColor = System.Drawing.Color.Black;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(213, 292);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(177, 43);
            this.btnSair.TabIndex = 9;
            this.btnSair.Text = "Sair (F4)";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            this.btnSair.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSair_KeyDown);
            this.btnSair.MouseHover += new System.EventHandler(this.btnSair_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(9, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(382, 100);
            this.panel2.TabIndex = 0;
            // 
            // btnReimpressao
            // 
            this.btnReimpressao.BackColor = System.Drawing.Color.Transparent;
            this.btnReimpressao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReimpressao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimpressao.ForeColor = System.Drawing.Color.Black;
            this.btnReimpressao.Image = ((System.Drawing.Image)(resources.GetObject("btnReimpressao.Image")));
            this.btnReimpressao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReimpressao.Location = new System.Drawing.Point(9, 228);
            this.btnReimpressao.Name = "btnReimpressao";
            this.btnReimpressao.Size = new System.Drawing.Size(381, 43);
            this.btnReimpressao.TabIndex = 3;
            this.btnReimpressao.Text = "Reimpressão (F3)";
            this.btnReimpressao.UseVisualStyleBackColor = false;
            this.btnReimpressao.Click += new System.EventHandler(this.btnReimpressao_Click);
            this.btnReimpressao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnReimpressao_KeyDown);
            this.btnReimpressao.MouseHover += new System.EventHandler(this.btnReimpressao_MouseHover);
            // 
            // btnConsultaSaldo
            // 
            this.btnConsultaSaldo.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultaSaldo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultaSaldo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaSaldo.ForeColor = System.Drawing.Color.Black;
            this.btnConsultaSaldo.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaSaldo.Image")));
            this.btnConsultaSaldo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultaSaldo.Location = new System.Drawing.Point(9, 130);
            this.btnConsultaSaldo.Name = "btnConsultaSaldo";
            this.btnConsultaSaldo.Size = new System.Drawing.Size(381, 43);
            this.btnConsultaSaldo.TabIndex = 1;
            this.btnConsultaSaldo.Text = "Consulta Saldo (F1)";
            this.btnConsultaSaldo.UseVisualStyleBackColor = false;
            this.btnConsultaSaldo.Click += new System.EventHandler(this.btnConsultaSaldo_Click);
            this.btnConsultaSaldo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConsultaSaldo_KeyDown);
            this.btnConsultaSaldo.MouseHover += new System.EventHandler(this.btnConsultaSaldo_MouseHover);
            // 
            // btnAutorizacao
            // 
            this.btnAutorizacao.BackColor = System.Drawing.Color.Transparent;
            this.btnAutorizacao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutorizacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutorizacao.ForeColor = System.Drawing.Color.Black;
            this.btnAutorizacao.Image = ((System.Drawing.Image)(resources.GetObject("btnAutorizacao.Image")));
            this.btnAutorizacao.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAutorizacao.Location = new System.Drawing.Point(9, 179);
            this.btnAutorizacao.Name = "btnAutorizacao";
            this.btnAutorizacao.Size = new System.Drawing.Size(381, 43);
            this.btnAutorizacao.TabIndex = 2;
            this.btnAutorizacao.Text = "Autorização (F2)";
            this.btnAutorizacao.UseVisualStyleBackColor = false;
            this.btnAutorizacao.Click += new System.EventHandler(this.btnAutorizacao_Click);
            this.btnAutorizacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAutorizacao_KeyDown);
            this.btnAutorizacao.MouseHover += new System.EventHandler(this.btnAutorizacao_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(5, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 368);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "E-Pharma";
            // 
            // frmMenuEPharma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(97)))), ((int)(((byte)(161)))));
            this.ClientSize = new System.Drawing.Size(422, 370);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMenuEPharma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu E-Pharma";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMenuEPharma_KeyDown);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReimpressao;
        private System.Windows.Forms.Button btnAutorizacao;
        private System.Windows.Forms.Button btnConsultaSaldo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}