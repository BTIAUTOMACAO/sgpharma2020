﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Xml;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenConsultarCartao : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private XmlDocument retornoXml;
        string retorno;
        private string[,] dados = new string[14, 3];
        public bool gravar;
        private DataTable dadosCliente;
        frmVenda fVendas;
        #endregion

        public frmVenConsultarCartao(frmVenda form)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            fVendas = form;
        }

        private void frmVenConsultarCartao_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.Default;
                gravar = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consultar Cartão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCartao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                fVendas.wsCartao = "";
                if (!String.IsNullOrEmpty(txtCartao.Text.Trim()))
                {
                    if (txtCartao.Text.Length > 16)
                    {
                        int cartao = txtCartao.Text.IndexOf("606421");
                        txtCartao.Text = txtCartao.Text.Substring(cartao, 16);
                    }
                    
                    LerCartao();
                }
            }
        }

        public bool LerCartao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                retornoXml = GestaoAdministrativa.Negocio.Beneficios.DrogabellaPlantao.LerCartao(txtCartao.Text);

                if (retornoXml.GetElementsByTagName("STATUS").Item(0).InnerText.Equals("1"))
                {
                    MessageBox.Show(retornoXml.GetElementsByTagName("MSG").Item(0).InnerText.ToUpper(), "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                //VERIFICA SE CARTÃO ESTA BLOQUEADO
                if (retornoXml.GetElementsByTagName("CARTAO_LIB").Item(0).InnerText != "S")
                {
                    MessageBox.Show("CARTÃO BLOQUEADO", "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                //VERIFICA SE TIITULAR ESTA BLOQUEADO//
                if (retornoXml.GetElementsByTagName("TITULAR_LIB").Item(0).InnerText != "S")
                {
                    MessageBox.Show("TITULAR BLOQUEADO", "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                //VERIFICA SE EMPRESA ESTA BLOQUEADA//
                if (retornoXml.GetElementsByTagName("EMPRESA_LIB").Item(0).InnerText != "S")
                {
                    MessageBox.Show("EMPRESA BLOQUEADA", "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }


                txtCodigo.Text = retornoXml.GetElementsByTagName("EMPRESA_ID").Item(0).InnerText;
                txtEmpresa.Text = retornoXml.GetElementsByTagName("EMPRESA_NOME").Item(0).InnerText;
                txtNome.Text = retornoXml.GetElementsByTagName("CARTAO_NOME").Item(0).InnerText;
                txtTitular.Text = retornoXml.GetElementsByTagName("TITULAR_NOME").Item(0).InnerText;
                txtSituacao.Text = retornoXml.GetElementsByTagName("STATUS_CARTAO").Item(0).InnerText;
                txtLimiteMes.Text = String.Format("{0:0.00}", Convert.ToDecimal(retornoXml.GetElementsByTagName("TITULAR_LIMITEMES").Item(0).InnerText) / 100);
                txtVGasto.Text = String.Format("{0:0.00}", Convert.ToDecimal(retornoXml.GetElementsByTagName("SALDO_DEVEDOR").Item(0).InnerText) / 100);
                txtVRestante.Text = String.Format("{0:0.00}", Convert.ToDecimal(retornoXml.GetElementsByTagName("SALDO_RESTANTE").Item(0).InnerText) / 100);
                txtMatricula.Text = retornoXml.GetElementsByTagName("TITULAR_MATRICULA").Item(0).InnerText;
                txtOEmpresa.Text = retornoXml.GetElementsByTagName("EMPRESA_OBS1").Item(0).InnerText;
                txtOFuncionario.Text = retornoXml.GetElementsByTagName("TITULAR_OBS1").Item(0).InnerText;
                fVendas.wsNumCartao = txtCartao.Text.Trim();
                fVendas.wsCartao = retornoXml.GetElementsByTagName("CARTAO_NUMERO").Item(0).InnerText + retornoXml.GetElementsByTagName("CARTAO_DIGITO").Item(0).InnerText;
                fVendas.wsPreAutoriza = retornoXml.GetElementsByTagName("PRE_AUTORIZA").Item(0).InnerText;
                fVendas.wsEmpresa = retornoXml.GetElementsByTagName("EMPRESA_ID").Item(0).InnerText;
                fVendas.wsNomeCartao = retornoXml.GetElementsByTagName("CARTAO_NOME").Item(0).InnerText;
                fVendas.vendaBeneficio = "DROGABELLA/PLANTAO";
                fVendas.txtCodCliEmp.Text = retornoXml.GetElementsByTagName("EMPRESA_ID").Item(0).InnerText;
                fVendas.txtCodCliEmp.Enabled = false;
                fVendas.txtCliEmp.Text = retornoXml.GetElementsByTagName("EMPRESA_NOME").Item(0).InnerText;
                fVendas.txtCliEmp.Enabled = false;
                fVendas.wsSenhaPadrao = retornoXml.GetElementsByTagName("SENHA_PADRAO").Item(0).InnerText;
                fVendas.wsObrigaReceita = retornoXml.GetElementsByTagName("OBRIGA_RECEITA_SGPHARMA").Item(0).InnerText;
                fVendas.wsSaldoCartao = Convert.ToDouble(retornoXml.GetElementsByTagName("SALDO_RESTANTE").Item(0).InnerText) / 100;
                fVendas.wsNomeEmpresa = retornoXml.GetElementsByTagName("EMPRESA_NOME").Item(0).InnerText;
                fVendas.wsTitular = retornoXml.GetElementsByTagName("TITULAR_NOME").Item(0).InnerText;
                txtDataFechamento.Text = retornoXml.GetElementsByTagName("DATA_FECHAMENTO").Item(0).InnerText;

                if (!String.IsNullOrEmpty(retornoXml.GetElementsByTagName("DEPENDENTES").Item(0).InnerText))
                {
                    string[] nomes = retornoXml.GetElementsByTagName("DEPENDENTES").Item(0).InnerText.Split('\n');
                    string dep = "";
                    for (int x = 0; x < nomes.Length; x++)
                    {
                        if (!String.IsNullOrEmpty(nomes[x]))
                            dep += nomes[x] + "\r\n";
                    }
                    txtDependentes.Text = dep;
                }
                else
                    txtDependentes.Text = "";

                fVendas.txtCliEmp.Visible = true;
                fVendas.lblCliEmp.Visible = true;
                fVendas.txtCodCliEmp.Visible = true;

                if (fVendas.wsPreAutoriza.Equals("S"))
                {
                    fVendas.btnPreAutorizar.Visible = true;
                }
                else
                    fVendas.btnPreAutorizar.Visible = false;
                
                fVendas.codFormaPagto = 4;
                
                var cliente = new Cliente();
                dadosCliente = cliente.SelecionaDadosDoClientePorConveniada(txtCartao.Text, txtCodigo.Text);
                if (dadosCliente.Rows.Count > 0)
                {
                    txtCpf.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_DOCTO"].ToString());
                    txtCpf.Enabled = false;
                    txtEndereco.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_ENDER"].ToString());
                    txtNumero.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_END_NUM"].ToString());
                    txtBairro.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_BAIRRO"].ToString());
                    txtCidade.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_CIDADE"].ToString());
                    txtCep.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_CEP"].ToString().Replace(",", ""));
                    cmbUF.Text = Funcoes.ChecaCampoVazio(dadosCliente.Rows[0]["CF_UF"].ToString());
                    fVendas.IDCliente = Convert.ToInt32(dadosCliente.Rows[0]["CF_ID"]);
                    btnGravar.Enabled = true;
                    btnGravar.Focus();
                }
                else
                {
                    txtCpf.Text = Util.GerarCpf();
                    txtCpf.Enabled = true;
                    btnGravar.Enabled = true;
                    btnGravar.Focus();
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Gravar()
        {
            try
            {
                var conveniada = new Conveniada();

                if (dadosCliente.Rows.Count == 0)
                {
                    if (String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("CONVENIADAS", "CON_CODIGO", "CON_CODIGO", txtCodigo.Text)))
                    {
                        if (!conveniada.InserirConveniadaConsultaCartao(txtCodigo.Text, txtEmpresa.Text, Principal.usuario, DateTime.Now))
                            return false;
                    }

                    #region INSERE CLIENTE NA TABELA 
                    //VERIFICA SE CLIENTE JÁ ESTA CADASTRADO//
                    Principal.strSql = "SELECT * FROM CLIFOR WHERE CF_DOCTO = '" + txtCpf.Text + "'";
                    Principal.dtPesq = BancoDados.selecionarRegistros(Principal.strSql);
                    if (!String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("CLIFOR", "CF_DOCTO", "CF_DOCTO", txtCpf.Text, false, true)))
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Cliente já cadastrado.";
                        Funcoes.Avisa();
                        txtCpf.Text = "";
                        txtCpf.Focus();
                        return false;
                    }

                    //INCLUI NOVO REGISTRO NA TABELA CLIENTES//
                    var cliente = new Cliente
                    {
                        CfDocto = txtCpf.Text,
                        CfTipoDocto = 0,
                        CfNome = txtNome.Text,
                        CfEndereco = ".",
                        CfNumeroEndereco = ".",
                        CfBairro = ".",
                        CfCEP = "00000000",
                        CfUF = ".",
                        CfCidade = ".",
                        CfStatus = 4,
                        CfEnderecoCobranca = ".",
                        CfNumeroCobranca = ".",
                        CfBairroCobranca = ".",
                        CfCEPCobranca = "00000000",
                        CfCidadeCobranca = ".",
                        CfUFCobranca = ".",
                        CfEnderecoEntrega = ".",
                        CfNumeroEntrega = ".",
                        CfBairroEntrega = ".",
                        CfCEPEntrega = "00000000",
                        CfCidadeEntrega = ".",
                        CfUFEntrega = ".",
                        CfPais = "BRASIL",
                        CfLiberado = "N",
                        CodigoConveniada = Convert.ToInt32(txtCodigo.Text),
                        NumCartao = txtCartao.Text,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };
                    if (!cliente.IncluiClienteConsultaCatrtao(cliente, out fVendas.IDCliente))
                        return false;
                    #endregion
                }

                double desconto = conveniada.IdentificaDescontoConveniada(txtCodigo.Text);
                if (desconto != 0)
                {
                    fVendas.txtAjuste.Text = String.Format("{0:N}", desconto * -1);
                }

                Principal.doctoCliente = txtCpf.Text;
                fVendas.DadosDaVendaCliente(Principal.doctoCliente);
                fVendas.checaMedComSemReceita = false;
                fVendas.btnUltimosProdutos.Enabled = true;
                if (fVendas.wsObrigaReceita.Equals("S"))
                {
                    if (MessageBox.Show("Venda com receita?", "Consultar Cartões", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                    {
                        //VENDA COM RECEITA//
                        if (MessageBox.Show("Todos os medicamentos possuem receita?", "Consultar Cartões", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.No)
                        {
                            fVendas.checaMedComSemReceita = true;
                            fVendas.wsComSemRec = "S";
                            fVendas.tipoVenda = 1;
                        }
                        else
                        {
                            fVendas.checaMedComSemReceita = false;
                            fVendas.wsComSemRec = "N";
                            fVendas.tipoVenda = 3;
                        }
                    }
                    else
                    {
                        //VENDA SEM RECEITA//
                        fVendas.tipoVenda = 2;
                    }
                }
                else
                {
                    //VENDA SEM RECEITA//
                    fVendas.tipoVenda = 2;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Gravar())
                {
                    if (fVendas.wsSaldoCartao.Equals(0))
                    {
                        MessageBox.Show("Atenção! Cliente não possui Saldo.", "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        fVendas.txtCodCliEmp.Text = "";
                        fVendas.txtCliEmp.Text = "";
                        fVendas.Limpar();
                        gravar = false;
                    }
                    else
                        gravar = true;


                    this.Close();
                }
                else
                    gravar = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            try
            {
                if (!gravar)
                {
                    fVendas.txtCodCliEmp.Enabled = true;
                    fVendas.txtCliEmp.Enabled = true;
                    fVendas.txtCodCliEmp.Text = string.Empty;
                    fVendas.txtCliEmp.Text = string.Empty;
                    fVendas.Limpar();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consultar Cartões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtBairro.Text.Trim()))
            {
                Principal.mensagem = "Bairro não pode ser em branco.";
                Funcoes.Avisa();
                txtBairro.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCep.Text.Replace("-", "").Trim()))
            {
                Principal.mensagem = "CEP não pode ser em branco.";
                Funcoes.Avisa();
                txtCep.Focus();
                return false;
            }
            if (cmbUF.SelectedIndex.Equals(-1))
            {
                Principal.mensagem = "Necessário selecionar o estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }

            return true;
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private async void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtCep.Text.Replace("-", "").Trim()))
                {
                    Endereco end = await ConsultaCEP(txtCep.Text);
                    if (end.bairro != null)
                    {
                        txtEndereco.Text = end.logradouro.ToUpper();
                        txtBairro.Text = Funcoes.RemoverAcentuacao(end.bairro.ToUpper());
                        txtCidade.Text = Funcoes.RemoverAcentuacao(end.localidade.ToUpper());
                        cmbUF.Text = end.uf;
                        Cursor = Cursors.Default;
                        txtNumero.Focus();
                    }
                }
                else
                    txtEndereco.Focus();
            }
        }

        private async Task<Endereco> ConsultaCEP(string cep)
        {

            Endereco end;

            try
            {
                using (var client = new HttpClient())
                {

                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri("https://viacep.com.br/ws/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(cep + "/json/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();


                    end = JsonConvert.DeserializeObject<Endereco>(response.Content.ReadAsStringAsync().Result);
                }

                return end;

            }
            catch
            {

                throw;
            }
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnGravar.Focus();
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtTitular_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtSituacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtLimiteMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtVGasto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtVRestante_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtMatricula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtOEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void txtOFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCpf.Enabled.Equals(true))
                {
                    txtCpf.Focus();
                }
                else
                {
                    txtEndereco.Focus();
                }
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmVenConsultarCartao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCartao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCpf_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCep_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnGravar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmVenConsultarCartao_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (gravar.Equals(false))
            //{
            //    Limpar();
            //}
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtBairro.Text))
                {
                    btnGravar.Focus();
                }
                else
                    txtBairro.Focus();
            }
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVendas.Limpar();
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancela.PerformClick();
                    break;
                case Keys.F2:
                    btnGravar.PerformClick();
                    break;
            }

        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Cancelar (F1)");
        }

        private void btnGravar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnGravar, "Gravae (F2)");
        }

        private void txtTitular_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCartao_TextChanged(object sender, EventArgs e)
        {

        }

        private void gbCartao_Enter(object sender, EventArgs e)
        {

        }
    }
}
