﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenRetornoWS : Form
    {
        public frmVenRetornoWS()
        {
            InitializeComponent();
        }

        private void frmVenRetornoWS_Load(object sender, EventArgs e)
        {
            Principal.sRetorno = "";
            Cursor = Cursors.Default;
            dgProd.Focus();
        }

        private void dgProd_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgProd.Rows[e.RowIndex].Cells[11].Value.ToString() == "2")
            {
                dgProd.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgProd.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            Principal.sRetorno = "0";
            this.Close();
        }

        private void btnAceitar_Click(object sender, EventArgs e)
        {
            Principal.sRetorno = "1";
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Principal.preAutorizou = true;
            Principal.sRetorno = "2";
            this.Close();
        }

        private void frmVenRetornoWS_Shown(object sender, EventArgs e)
        {
            try
            {
                double total = 0;
                for (int i = 0; i < dgProd.RowCount; i++)
                {
                    total += Convert.ToDouble(dgProd.Rows[i].Cells[8].Value);
                }

                lblValor.Text = "Valor a debitar do Cartao R$ " + String.Format("{0:N}", total);
                btnAceitar.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pré-Autorização", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDescartar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void btnAceitar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void btnAlterar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }

        private void dgProd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnDescartar.PerformClick();
                    break;
                case Keys.F2:
                    btnAceitar.PerformClick();
                    break;
                case Keys.F3:
                    btnAlterar.PerformClick();
                    break;
            }
        }
    }
}
