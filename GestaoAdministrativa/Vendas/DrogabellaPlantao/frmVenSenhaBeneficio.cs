﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.DrogabellaPlantao
{
    public partial class frmVenSenhaBeneficio : Form
    {
        private frmVenda fVendas;
        bool duasSenhas;
        public frmVenSenhaBeneficio(frmVenda fvendas)
        {
            RegisterFocusEvents(this.Controls);
            InitializeComponent();
            fVendas = fvendas;
        }

        private void frmVenSenhaBeneficio_Load(object sender, EventArgs e)
        {
            try
            {
                if (fVendas.wsSenhaPadrao.Equals("S"))
                {
                    duasSenhas = true;
                    lblMensagem.Text = "Nova Senha";
                    txtSenha.Focus();
                }
                else
                {
                    lblMensagem.Text = "Informe a Senha";
                    duasSenhas = false;
                    txtSenha.Focus();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Senha Benefício", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOK.PerformClick();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtSenha.Text))
                {
                    if (txtSenha.Text.Length < 4)
                    {
                        MessageBox.Show("Senha com menos de 4 Digitos", "Senha Conveniado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSenha.Text = "";
                        txtSenha.Focus();
                    }
                    else if (duasSenhas)
                    {
                        if (txtSenha.Text.Equals(fVendas.wsNumCartao.Substring(12)))
                        {
                            MessageBox.Show("Nova Senha não pode ser 4 Últimos Dígitos do Cartão!", "Senha Conveniado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            duasSenhas = true;
                            lblMensagem.Text = "Nova Senha";
                            txtSenha.Text = "";
                            txtSenha.Focus();
                        }
                        else if (txtSenha.Text.Equals("1111"))
                        {
                            MessageBox.Show("Nova Senha não pode ser 1111!", "Senha Conveniado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            duasSenhas = true;
                            lblMensagem.Text = "Nova Senha";
                            txtSenha.Text = "";
                            txtSenha.Focus();
                        }
                        else
                        {
                            fVendas.novaSenhaConveniado = txtSenha.Text;
                            lblMensagem.Text = "Conf. Nova Senha";
                            txtSenha.Text = "";
                            duasSenhas = false;
                            txtSenha.Focus();
                        }
                    }
                    else
                    {
                        fVendas.senhaConveniado = txtSenha.Text;

                        if (!String.IsNullOrEmpty(fVendas.novaSenhaConveniado))
                        {
                            if (fVendas.novaSenhaConveniado != fVendas.senhaConveniado)
                            {
                                MessageBox.Show("Senhas Diferentes! Digite Novamente.", "Senha Conveniado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                duasSenhas = true;
                                lblMensagem.Text = "Nova Senha";
                                txtSenha.Text = "";
                                txtSenha.Focus();
                            }
                            else
                                this.Close();
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Senha em branco!", "Senha Conveniado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSenha.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Senha Benefício", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                fVendas.senhaConveniado = "";
                fVendas.novaSenhaConveniado = "";
                this.Close();
            }
        }

        private void btnOK_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                fVendas.senhaConveniado = "";
                fVendas.novaSenhaConveniado = "";
                this.Close();
            }
        }

        private void frmVenSenhaBeneficio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                fVendas.senhaConveniado = "";
                fVendas.novaSenhaConveniado = "";
                this.Close();
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
    }
}
