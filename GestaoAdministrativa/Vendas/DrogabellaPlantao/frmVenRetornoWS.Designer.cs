﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenRetornoWS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenRetornoWS));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblValor = new System.Windows.Forms.Label();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnAceitar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.lblSubTitulo = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.dgProd = new System.Windows.Forms.DataGridView();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COD_BARRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTDE_AUTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_SUBTOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DPORC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DVAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_REC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RETORNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUSPROD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblEstab = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProd)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.groupBox1.Location = new System.Drawing.Point(4, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(985, 414);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblValor);
            this.panel2.Controls.Add(this.btnAlterar);
            this.panel2.Controls.Add(this.btnAceitar);
            this.panel2.Controls.Add(this.btnDescartar);
            this.panel2.Controls.Add(this.lblSubTitulo);
            this.panel2.Controls.Add(this.lblTitulo);
            this.panel2.Controls.Add(this.dgProd);
            this.panel2.Controls.Add(this.lblEstab);
            this.panel2.Location = new System.Drawing.Point(5, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(974, 394);
            this.panel2.TabIndex = 43;
            // 
            // lblValor
            // 
            this.lblValor.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.ForeColor = System.Drawing.Color.Black;
            this.lblValor.Location = new System.Drawing.Point(572, 317);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(397, 28);
            this.lblValor.TabIndex = 164;
            this.lblValor.Text = "Valor a debitar do Cartao R$ 00,00";
            this.lblValor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAlterar
            // 
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ForeColor = System.Drawing.Color.Black;
            this.btnAlterar.Image = ((System.Drawing.Image)(resources.GetObject("btnAlterar.Image")));
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAlterar.Location = new System.Drawing.Point(653, 348);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(316, 42);
            this.btnAlterar.TabIndex = 163;
            this.btnAlterar.Text = "Alterar os dados da Venda (F3)";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            this.btnAlterar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAlterar_KeyDown);
            // 
            // btnAceitar
            // 
            this.btnAceitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceitar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceitar.ForeColor = System.Drawing.Color.Black;
            this.btnAceitar.Image = ((System.Drawing.Image)(resources.GetObject("btnAceitar.Image")));
            this.btnAceitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceitar.Location = new System.Drawing.Point(327, 348);
            this.btnAceitar.Name = "btnAceitar";
            this.btnAceitar.Size = new System.Drawing.Size(316, 42);
            this.btnAceitar.TabIndex = 162;
            this.btnAceitar.Text = "Aceitar e continuar a Venda (F2)";
            this.btnAceitar.UseVisualStyleBackColor = true;
            this.btnAceitar.Click += new System.EventHandler(this.btnAceitar_Click);
            this.btnAceitar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAceitar_KeyDown);
            // 
            // btnDescartar
            // 
            this.btnDescartar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDescartar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDescartar.ForeColor = System.Drawing.Color.Black;
            this.btnDescartar.Image = ((System.Drawing.Image)(resources.GetObject("btnDescartar.Image")));
            this.btnDescartar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDescartar.Location = new System.Drawing.Point(3, 348);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(316, 42);
            this.btnDescartar.TabIndex = 161;
            this.btnDescartar.Text = "Descartar e fazer nova Venda (F1)";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            this.btnDescartar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDescartar_KeyDown);
            // 
            // lblSubTitulo
            // 
            this.lblSubTitulo.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblSubTitulo.Location = new System.Drawing.Point(0, 37);
            this.lblSubTitulo.Name = "lblSubTitulo";
            this.lblSubTitulo.Size = new System.Drawing.Size(968, 23);
            this.lblSubTitulo.TabIndex = 160;
            this.lblSubTitulo.Text = "Confira atentamente os preços e as quantidades autorizadas antes de confirmar a V" +
    "enda";
            this.lblSubTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitulo
            // 
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblTitulo.Location = new System.Drawing.Point(-1, 1);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(969, 37);
            this.lblTitulo.TabIndex = 159;
            this.lblTitulo.Text = "VENDA PRÉ -AUTORIZADA";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgProd
            // 
            this.dgProd.AllowUserToAddRows = false;
            this.dgProd.AllowUserToDeleteRows = false;
            this.dgProd.AllowUserToResizeColumns = false;
            this.dgProd.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            this.dgProd.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProd.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgProd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PROD_CODIGO,
            this.COD_BARRA,
            this.PROD_DESC,
            this.PROD_QTDE,
            this.QTDE_AUTO,
            this.PROD_SUBTOTAL,
            this.PROD_DPORC,
            this.PROD_DVAL,
            this.PROD_TOTAL,
            this.PROD_REC,
            this.RETORNO,
            this.STATUSPROD});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProd.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgProd.Enabled = false;
            this.dgProd.GridColor = System.Drawing.Color.White;
            this.dgProd.Location = new System.Drawing.Point(3, 67);
            this.dgProd.Name = "dgProd";
            this.dgProd.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProd.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgProd.RowHeadersVisible = false;
            this.dgProd.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            this.dgProd.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgProd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProd.Size = new System.Drawing.Size(966, 247);
            this.dgProd.TabIndex = 158;
            this.dgProd.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgProd_RowPrePaint);
            this.dgProd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProd_KeyDown);
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Código";
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_CODIGO.Visible = false;
            // 
            // COD_BARRA
            // 
            this.COD_BARRA.DataPropertyName = "COD_BARRA";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.COD_BARRA.DefaultCellStyle = dataGridViewCellStyle3;
            this.COD_BARRA.HeaderText = "Cód. de Barras";
            this.COD_BARRA.Name = "COD_BARRA";
            this.COD_BARRA.ReadOnly = true;
            this.COD_BARRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_DESC
            // 
            this.PROD_DESC.DataPropertyName = "PROD_DESCR";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_DESC.DefaultCellStyle = dataGridViewCellStyle4;
            this.PROD_DESC.HeaderText = "Descrição";
            this.PROD_DESC.Name = "PROD_DESC";
            this.PROD_DESC.ReadOnly = true;
            this.PROD_DESC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESC.Width = 165;
            // 
            // PROD_QTDE
            // 
            this.PROD_QTDE.DataPropertyName = "PROD_QTDE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_QTDE.DefaultCellStyle = dataGridViewCellStyle5;
            this.PROD_QTDE.HeaderText = "Qtde.";
            this.PROD_QTDE.Name = "PROD_QTDE";
            this.PROD_QTDE.ReadOnly = true;
            this.PROD_QTDE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_QTDE.Width = 45;
            // 
            // QTDE_AUTO
            // 
            this.QTDE_AUTO.DataPropertyName = "QTDE_AUTO";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.QTDE_AUTO.DefaultCellStyle = dataGridViewCellStyle6;
            this.QTDE_AUTO.HeaderText = "Qtde. Aut.";
            this.QTDE_AUTO.Name = "QTDE_AUTO";
            this.QTDE_AUTO.ReadOnly = true;
            this.QTDE_AUTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QTDE_AUTO.Width = 75;
            // 
            // PROD_SUBTOTAL
            // 
            this.PROD_SUBTOTAL.DataPropertyName = "PROD_SUBTOTAL";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_SUBTOTAL.DefaultCellStyle = dataGridViewCellStyle7;
            this.PROD_SUBTOTAL.HeaderText = "Preço";
            this.PROD_SUBTOTAL.Name = "PROD_SUBTOTAL";
            this.PROD_SUBTOTAL.ReadOnly = true;
            this.PROD_SUBTOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_SUBTOTAL.Width = 65;
            // 
            // PROD_DPORC
            // 
            this.PROD_DPORC.DataPropertyName = "PROD_DPORC";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_DPORC.DefaultCellStyle = dataGridViewCellStyle8;
            this.PROD_DPORC.HeaderText = "% Desc.";
            this.PROD_DPORC.Name = "PROD_DPORC";
            this.PROD_DPORC.ReadOnly = true;
            this.PROD_DPORC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DPORC.Width = 70;
            // 
            // PROD_DVAL
            // 
            this.PROD_DVAL.DataPropertyName = "PROD_DVAL";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_DVAL.DefaultCellStyle = dataGridViewCellStyle9;
            this.PROD_DVAL.HeaderText = "Vl. Desc.";
            this.PROD_DVAL.Name = "PROD_DVAL";
            this.PROD_DVAL.ReadOnly = true;
            this.PROD_DVAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DVAL.Width = 70;
            // 
            // PROD_TOTAL
            // 
            this.PROD_TOTAL.DataPropertyName = "PROD_TOTAL";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_TOTAL.DefaultCellStyle = dataGridViewCellStyle10;
            this.PROD_TOTAL.HeaderText = "Vl. Liq.";
            this.PROD_TOTAL.Name = "PROD_TOTAL";
            this.PROD_TOTAL.ReadOnly = true;
            this.PROD_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_TOTAL.Width = 65;
            // 
            // PROD_REC
            // 
            this.PROD_REC.DataPropertyName = "PROD_REC";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.PROD_REC.DefaultCellStyle = dataGridViewCellStyle11;
            this.PROD_REC.HeaderText = "Receita";
            this.PROD_REC.Name = "PROD_REC";
            this.PROD_REC.ReadOnly = true;
            this.PROD_REC.Width = 55;
            // 
            // RETORNO
            // 
            this.RETORNO.DataPropertyName = "RETORNO";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.RETORNO.DefaultCellStyle = dataGridViewCellStyle12;
            this.RETORNO.HeaderText = "Retorno da Administradora";
            this.RETORNO.Name = "RETORNO";
            this.RETORNO.ReadOnly = true;
            this.RETORNO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RETORNO.Width = 253;
            // 
            // STATUSPROD
            // 
            this.STATUSPROD.DataPropertyName = "STATUSPROD";
            this.STATUSPROD.HeaderText = "Status";
            this.STATUSPROD.Name = "STATUSPROD";
            this.STATUSPROD.ReadOnly = true;
            this.STATUSPROD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.STATUSPROD.Visible = false;
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(6, 8);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 152;
            this.lblEstab.Tag = "";
            // 
            // frmVenRetornoWS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(30)))), ((int)(((byte)(66)))));
            this.ClientSize = new System.Drawing.Size(993, 416);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVenRetornoWS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Venda Pré-Autorizada";
            this.Load += new System.EventHandler(this.frmVenRetornoWS_Load);
            this.Shown += new System.EventHandler(this.frmVenRetornoWS_Shown);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dgProd;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnAceitar;
        private System.Windows.Forms.Button btnDescartar;
        public System.Windows.Forms.Label lblSubTitulo;
        public System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_BARRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTDE_AUTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_SUBTOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DPORC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DVAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RETORNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn STATUSPROD;
        public System.Windows.Forms.Label lblValor;
    }
}