﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenControleEntregaRecebimento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenControleEntregaRecebimento));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblTroco = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.dgEspecies = new System.Windows.Forms.DataGridView();
            this.ESP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_CAIXA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_VINCULADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_SAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbEspecies = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgFormaPagamento = new System.Windows.Forms.DataGridView();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_PARCELA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_PARCELA_VENCIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_VALOR_PARCELA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPERACAO_CAIXA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_FORMA_PAGTO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblEntregador = new System.Windows.Forms.Label();
            this.lblRomaneio = new System.Windows.Forms.Label();
            this.lblVendaId = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecies)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFormaPagamento)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 500);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Finalizar Entrega";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(6, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 473);
            this.panel1.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblTroco);
            this.groupBox4.Controls.Add(this.lblTotal);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.btnCancelar);
            this.groupBox4.Controls.Add(this.btnConfirmar);
            this.groupBox4.Controls.Add(this.dgEspecies);
            this.groupBox4.Controls.Add(this.txtValor);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cmbEspecies);
            this.groupBox4.Location = new System.Drawing.Point(3, 229);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(408, 238);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Espécies";
            // 
            // lblTroco
            // 
            this.lblTroco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTroco.ForeColor = System.Drawing.Color.Red;
            this.lblTroco.Location = new System.Drawing.Point(59, 208);
            this.lblTroco.Name = "lblTroco";
            this.lblTroco.Size = new System.Drawing.Size(85, 15);
            this.lblTroco.TabIndex = 16;
            this.lblTroco.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Navy;
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTotal.Location = new System.Drawing.Point(59, 188);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(85, 15);
            this.lblTotal.TabIndex = 15;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Troco:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Total:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(312, 187);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(42, 45);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            this.btnCancelar.MouseHover += new System.EventHandler(this.btnCancelar_MouseHover);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.Location = new System.Drawing.Point(360, 187);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(42, 45);
            this.btnConfirmar.TabIndex = 11;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            this.btnConfirmar.MouseHover += new System.EventHandler(this.btnConfirmar_MouseHover);
            // 
            // dgEspecies
            // 
            this.dgEspecies.AllowUserToAddRows = false;
            this.dgEspecies.AllowUserToResizeColumns = false;
            this.dgEspecies.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecies.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgEspecies.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecies.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgEspecies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgEspecies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESP_CODIGO,
            this.ESP_DESCRICAO,
            this.ESP_VALOR,
            this.ESP_CAIXA,
            this.ESP_VINCULADO,
            this.ESP_SAT});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEspecies.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgEspecies.GridColor = System.Drawing.Color.Black;
            this.dgEspecies.Location = new System.Drawing.Point(6, 51);
            this.dgEspecies.Name = "dgEspecies";
            this.dgEspecies.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecies.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgEspecies.RowHeadersWidth = 30;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecies.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgEspecies.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEspecies.Size = new System.Drawing.Size(396, 130);
            this.dgEspecies.TabIndex = 3;
            this.dgEspecies.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgEspecies_RowsRemoved);
            this.dgEspecies.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEspecies_KeyDown);
            // 
            // ESP_CODIGO
            // 
            this.ESP_CODIGO.DataPropertyName = "ESP_CODIGO";
            this.ESP_CODIGO.HeaderText = "Código";
            this.ESP_CODIGO.Name = "ESP_CODIGO";
            this.ESP_CODIGO.ReadOnly = true;
            this.ESP_CODIGO.Visible = false;
            // 
            // ESP_DESCRICAO
            // 
            this.ESP_DESCRICAO.DataPropertyName = "ESP_DESCRICAO";
            this.ESP_DESCRICAO.HeaderText = "Espécie";
            this.ESP_DESCRICAO.Name = "ESP_DESCRICAO";
            this.ESP_DESCRICAO.ReadOnly = true;
            this.ESP_DESCRICAO.Width = 250;
            // 
            // ESP_VALOR
            // 
            this.ESP_VALOR.DataPropertyName = "ESP_VALOR";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.ESP_VALOR.DefaultCellStyle = dataGridViewCellStyle3;
            this.ESP_VALOR.HeaderText = "Valor";
            this.ESP_VALOR.Name = "ESP_VALOR";
            this.ESP_VALOR.ReadOnly = true;
            this.ESP_VALOR.Width = 110;
            // 
            // ESP_CAIXA
            // 
            this.ESP_CAIXA.DataPropertyName = "ESP_CAIXA";
            this.ESP_CAIXA.HeaderText = "ESP_CAIXA";
            this.ESP_CAIXA.Name = "ESP_CAIXA";
            this.ESP_CAIXA.ReadOnly = true;
            this.ESP_CAIXA.Visible = false;
            // 
            // ESP_VINCULADO
            // 
            this.ESP_VINCULADO.DataPropertyName = "ESP_VINCULADO";
            this.ESP_VINCULADO.HeaderText = "ESP_VINCULADO";
            this.ESP_VINCULADO.Name = "ESP_VINCULADO";
            this.ESP_VINCULADO.ReadOnly = true;
            this.ESP_VINCULADO.Visible = false;
            // 
            // ESP_SAT
            // 
            this.ESP_SAT.DataPropertyName = "ESP_SAT";
            this.ESP_SAT.HeaderText = "ESP_SAT";
            this.ESP_SAT.Name = "ESP_SAT";
            this.ESP_SAT.ReadOnly = true;
            this.ESP_SAT.Visible = false;
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.Color.White;
            this.txtValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.ForeColor = System.Drawing.Color.Black;
            this.txtValor.Location = new System.Drawing.Point(247, 23);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(155, 22);
            this.txtValor.TabIndex = 2;
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValor_KeyDown);
            this.txtValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValor_KeyPress);
            this.txtValor.Validated += new System.EventHandler(this.txtValor_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(203, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Valor:";
            // 
            // cmbEspecies
            // 
            this.cmbEspecies.BackColor = System.Drawing.Color.White;
            this.cmbEspecies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEspecies.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEspecies.ForeColor = System.Drawing.Color.Black;
            this.cmbEspecies.FormattingEnabled = true;
            this.cmbEspecies.Location = new System.Drawing.Point(6, 21);
            this.cmbEspecies.Name = "cmbEspecies";
            this.cmbEspecies.Size = new System.Drawing.Size(186, 24);
            this.cmbEspecies.TabIndex = 0;
            this.cmbEspecies.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEspecies_KeyDown);
            this.cmbEspecies.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEspecies_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgFormaPagamento);
            this.groupBox3.Location = new System.Drawing.Point(3, 85);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(408, 144);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Forma de Pagamento";
            // 
            // dgFormaPagamento
            // 
            this.dgFormaPagamento.AllowUserToAddRows = false;
            this.dgFormaPagamento.AllowUserToDeleteRows = false;
            this.dgFormaPagamento.AllowUserToResizeColumns = false;
            this.dgFormaPagamento.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFormaPagamento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgFormaPagamento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFormaPagamento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgFormaPagamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFormaPagamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMP_CODIGO,
            this.EST_CODIGO,
            this.VENDA_ID,
            this.VENDA_PARCELA,
            this.VENDA_PARCELA_VENCIMENTO,
            this.VENDA_VALOR_PARCELA,
            this.FORMA_ID,
            this.OPERACAO_CAIXA,
            this.VENDA_FORMA_PAGTO_ID});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFormaPagamento.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgFormaPagamento.GridColor = System.Drawing.Color.Black;
            this.dgFormaPagamento.Location = new System.Drawing.Point(6, 21);
            this.dgFormaPagamento.Name = "dgFormaPagamento";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFormaPagamento.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgFormaPagamento.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFormaPagamento.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgFormaPagamento.Size = new System.Drawing.Size(396, 117);
            this.dgFormaPagamento.TabIndex = 0;
            this.dgFormaPagamento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgFormaPagamento_KeyDown);
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            this.EMP_CODIGO.HeaderText = "EMP_CODIGO";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.Visible = false;
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "EST_CODIGO";
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.Visible = false;
            // 
            // VENDA_ID
            // 
            this.VENDA_ID.DataPropertyName = "VENDA_ID";
            this.VENDA_ID.HeaderText = "VENDA_ID";
            this.VENDA_ID.Name = "VENDA_ID";
            this.VENDA_ID.Visible = false;
            // 
            // VENDA_PARCELA
            // 
            this.VENDA_PARCELA.DataPropertyName = "VENDA_PARCELA";
            this.VENDA_PARCELA.HeaderText = "Parcela";
            this.VENDA_PARCELA.Name = "VENDA_PARCELA";
            this.VENDA_PARCELA.Width = 60;
            // 
            // VENDA_PARCELA_VENCIMENTO
            // 
            this.VENDA_PARCELA_VENCIMENTO.DataPropertyName = "VENDA_PARCELA_VENCIMENTO";
            this.VENDA_PARCELA_VENCIMENTO.HeaderText = "Vencimento";
            this.VENDA_PARCELA_VENCIMENTO.Name = "VENDA_PARCELA_VENCIMENTO";
            // 
            // VENDA_VALOR_PARCELA
            // 
            this.VENDA_VALOR_PARCELA.DataPropertyName = "VENDA_VALOR_PARCELA";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.VENDA_VALOR_PARCELA.DefaultCellStyle = dataGridViewCellStyle9;
            this.VENDA_VALOR_PARCELA.HeaderText = "Valor";
            this.VENDA_VALOR_PARCELA.Name = "VENDA_VALOR_PARCELA";
            this.VENDA_VALOR_PARCELA.Width = 80;
            // 
            // FORMA_ID
            // 
            this.FORMA_ID.DataPropertyName = "FORMA_ID";
            this.FORMA_ID.HeaderText = "FORMA_ID";
            this.FORMA_ID.Name = "FORMA_ID";
            this.FORMA_ID.Visible = false;
            // 
            // OPERACAO_CAIXA
            // 
            this.OPERACAO_CAIXA.DataPropertyName = "OPERACAO_CAIXA";
            this.OPERACAO_CAIXA.HeaderText = "OPERACAO_CAIXA";
            this.OPERACAO_CAIXA.Name = "OPERACAO_CAIXA";
            this.OPERACAO_CAIXA.Visible = false;
            // 
            // VENDA_FORMA_PAGTO_ID
            // 
            this.VENDA_FORMA_PAGTO_ID.DataPropertyName = "VENDA_FORMA_PAGTO_ID";
            this.VENDA_FORMA_PAGTO_ID.HeaderText = "VENDA_FORMA_PAGTO_ID";
            this.VENDA_FORMA_PAGTO_ID.Name = "VENDA_FORMA_PAGTO_ID";
            this.VENDA_FORMA_PAGTO_ID.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblEntregador);
            this.groupBox2.Controls.Add(this.lblRomaneio);
            this.groupBox2.Controls.Add(this.lblVendaId);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(408, 80);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da Entrega";
            // 
            // lblEntregador
            // 
            this.lblEntregador.Location = new System.Drawing.Point(106, 44);
            this.lblEntregador.Name = "lblEntregador";
            this.lblEntregador.Size = new System.Drawing.Size(261, 26);
            this.lblEntregador.TabIndex = 5;
            this.lblEntregador.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblRomaneio
            // 
            this.lblRomaneio.Location = new System.Drawing.Point(281, 15);
            this.lblRomaneio.Name = "lblRomaneio";
            this.lblRomaneio.Size = new System.Drawing.Size(86, 26);
            this.lblRomaneio.TabIndex = 4;
            this.lblRomaneio.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblVendaId
            // 
            this.lblVendaId.Location = new System.Drawing.Point(106, 15);
            this.lblVendaId.Name = "lblVendaId";
            this.lblVendaId.Size = new System.Drawing.Size(86, 26);
            this.lblVendaId.TabIndex = 3;
            this.lblVendaId.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(198, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Romaneio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Entregador:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Venda ID:";
            // 
            // frmVenControleEntregaRecebimento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(442, 507);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenControleEntregaRecebimento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmControleEntregaRecebimento";
            this.Load += new System.EventHandler(this.frmControleEntregaRecebimento_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmControleEntregaRecebimento_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecies)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgFormaPagamento)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEntregador;
        private System.Windows.Forms.Label lblRomaneio;
        private System.Windows.Forms.Label lblVendaId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgFormaPagamento;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgEspecies;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbEspecies;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Label lblTroco;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_CAIXA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_VINCULADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_SAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_PARCELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_PARCELA_VENCIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_VALOR_PARCELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FORMA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPERACAO_CAIXA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_FORMA_PAGTO_ID;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}