﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenLancamentoDevolucao : Form, Botoes
    {
        private ToolStripButton tsbDevolucao = new ToolStripButton("Lanc. Dev. Produtos");
        private ToolStripSeparator tssDevolucao = new ToolStripSeparator();
        int qtdeTotalDeProdutos;
        double valorTotalDeProdutos;
        DataTable dt = new DataTable();
        double valorRestante;
        double valorTotalDeProdutosTroca;

        public frmVenLancamentoDevolucao(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenLancamentoDevolucao_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Botao()
        {
            try
            {
                this.tsbDevolucao.AutoSize = false;
                this.tsbDevolucao.Image = Properties.Resources.remover;
                this.tsbDevolucao.Size = new System.Drawing.Size(160, 20);
                this.tsbDevolucao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbDevolucao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssDevolucao);
                tsbDevolucao.Click += delegate
                {
                    var devolucao = Application.OpenForms.OfType<frmVenLancamentoDevolucao>().FirstOrDefault();
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnLimpar.Enabled = false;
                    devolucao.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbDevolucao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssDevolucao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dgProdutos.Rows.Clear();
            dgTroca.Rows.Clear();
            qtdeTotalDeProdutos = 0;
            valorTotalDeProdutos = 0;
            lblTotal.Text = "";
            lblQtde.Text = "";
            txtCodBarras.Text = "";
            txtQtde.Text = "";
            txtTrocaProduto.Text = "";
            txtQtdeTroca.Text = "";
            lblRestante.Text = "";
            lblTotalTroca.Text = "";
            lblDiferenca.Text = "";
            txtIdentificacao.Text = "";
            txtVendaID.Text = "";
            txtVendedor.Text = "";
            txtCodBarras.Focus();
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnCancela.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirma.PerformClick();
                    break;
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Funcoes.SomenteNumeros(e);
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtCodBarras.Text))
                    {
                        var buscaProduto = new Preco();
                        buscaProduto.EstCodigo = Principal.estAtual;
                        buscaProduto.EmpCodigo = Principal.empAtual;
                        buscaProduto.ProdCodigo = txtCodBarras.Text;

                        dt = buscaProduto.ProdutosDevolucao(buscaProduto.EstCodigo, buscaProduto.EmpCodigo, buscaProduto.ProdCodigo);

                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("Produto não Cadastrado", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtCodBarras.Text = "";
                            txtCodBarras.Focus();
                        }
                        else
                        {
                            txtQtde.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cód. de Barras não pode ser em branco!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (String.IsNullOrEmpty(txtQtde.Text) || Convert.ToInt32(txtQtde.Text) != 0)
                    {
                        bool achou = false;
                        int qtde = 0;
                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            if (dgProdutos.Rows[i].Cells[3].Value.ToString() == txtCodBarras.Text)
                            {
                                qtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                                dgProdutos.Rows[i].Cells[5].Value = qtde + Convert.ToInt32(txtQtde.Text);
                                double desconto = (Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToDouble(dgProdutos.Rows[i].Cells[7].Value)) / 100;
                                dgProdutos.Rows[i].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value)
                                    * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value)) - (desconto * Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value)));
                                achou = true;
                            }
                        }

                        if (!achou)
                        {
                            dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { dt.Rows[0]["EST_CODIGO"],dt.Rows[0]["EMP_CODIGO"],dt.Rows[0]["PROD_ID"],
                        dt.Rows[0]["PROD_CODIGO"],dt.Rows[0]["PROD_DESCR"], txtQtde.Text, dt.Rows[0]["PRE_VALOR"], 0, Convert.ToDouble(dt.Rows[0]["PRE_VALOR"]) * Convert.ToInt32(txtQtde.Text) });

                        }
                       
                        AtualizaTotais();

                        txtCodBarras.Text = "";
                        txtQtde.Text = "";
                        txtCodBarras.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Quantidade não pode ser em branco/ou zero!", "Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtQtde.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgProdutos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                double desconto;
                Cursor = Cursors.WaitCursor;
                switch (dgProdutos.Columns[e.ColumnIndex].Name)
                {
                    case "DESCONTO":
                        desconto = (Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells[6].Value) * Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells[7].Value)) / 100;
                        dgProdutos.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells[6].Value)
                            * Convert.ToInt32(dgProdutos.Rows[e.RowIndex].Cells[5].Value)) - (desconto * Convert.ToInt32(dgProdutos.Rows[e.RowIndex].Cells[5].Value)));
                        break;
                }
                AtualizaTotais();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgProdutos_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            AtualizaTotais();
            txtCodBarras.Focus();
        }

        private void txtTrocaProduto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtTrocaProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Funcoes.SomenteNumeros(e);
                if (e.KeyChar == 13)
                {
                    if (!String.IsNullOrEmpty(txtTrocaProduto.Text))
                    {
                        var buscaProduto = new Preco();
                        buscaProduto.EstCodigo = Principal.estAtual;
                        buscaProduto.EmpCodigo = Principal.empAtual;
                        buscaProduto.ProdCodigo = txtTrocaProduto.Text;

                        dt = buscaProduto.ProdutosDevolucao(buscaProduto.EstCodigo, buscaProduto.EmpCodigo, buscaProduto.ProdCodigo);

                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("Produto não Cadastrado", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtTrocaProduto.Text = "";
                            txtTrocaProduto.Focus();
                        }
                        else
                        {
                            txtQtdeTroca.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cód. de Barras não pode ser em branco!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTrocaProduto.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtQtdeTroca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtdeTroca_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (dgProdutos.RowCount > 0)
                    {
                        if (String.IsNullOrEmpty(txtQtdeTroca.Text) || Convert.ToInt32(txtQtdeTroca.Text) != 0)
                        {
                            bool achou = false;
                            int qtde = 0;
                            for (int i = 0; i < dgTroca.RowCount; i++)
                            {
                                if (dgTroca.Rows[i].Cells[3].Value.ToString() == txtTrocaProduto.Text)
                                {
                                    qtde = Convert.ToInt32(dgTroca.Rows[i].Cells[5].Value);
                                    dgTroca.Rows[i].Cells[5].Value = qtde + Convert.ToInt32(txtQtdeTroca.Text);
                                    double desconto = (Convert.ToDouble(dgTroca.Rows[i].Cells[6].Value) * Convert.ToDouble(dgTroca.Rows[i].Cells[7].Value)) / 100;
                                    dgTroca.Rows[i].Cells[8].Value = String.Format("{0:N}", Math.Round((Convert.ToDouble(dgTroca.Rows[i].Cells[6].Value)
                                        * Convert.ToInt32(dgTroca.Rows[i].Cells[5].Value)) - (desconto * Convert.ToInt32(dgTroca.Rows[i].Cells[5].Value)), 2));
                                    achou = true;
                                }
                            }

                            if (!achou)
                            {
                                dgTroca.Rows.Insert(dgTroca.RowCount, new Object[] { dt.Rows[0]["EST_CODIGO"],dt.Rows[0]["EMP_CODIGO"],dt.Rows[0]["PROD_ID"],
                                    dt.Rows[0]["PROD_CODIGO"],dt.Rows[0]["PROD_DESCR"], txtQtdeTroca.Text, dt.Rows[0]["PRE_VALOR"], 0, Convert.ToDouble(dt.Rows[0]["PRE_VALOR"]) * Convert.ToInt32(txtQtdeTroca.Text) });
                            }

                            valorTotalDeProdutosTroca = 0;

                            for (int i = 0; i < dgTroca.RowCount; i++)
                            {
                                valorTotalDeProdutosTroca += Convert.ToDouble(dgTroca.Rows[i].Cells[8].Value);
                            }

                            valorRestante = Convert.ToDouble(lblTotal.Text) - valorTotalDeProdutosTroca;

                            lblTotalTroca.Text = String.Format("{0:N}", valorTotalDeProdutosTroca);
                            if (valorRestante >= 0)
                            {
                                lblRestante.Text = String.Format("{0:N}", valorRestante);
                                lblDiferenca.Text = "0,00";
                                lblDiferenca.ForeColor = Color.Black;
                            }
                            else
                            {
                                lblRestante.Text = "0,00";
                                lblDiferenca.Text = String.Format("{0:N}", Math.Abs(valorRestante));
                                lblDiferenca.ForeColor = Color.Red;
                            }
                            txtTrocaProduto.Text = "";
                            txtQtdeTroca.Text = "";
                            txtTrocaProduto.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Quantidade não pode ser em branco/ou zero!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtQtdeTroca.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Necessário ao menos um produto para realizar Troca!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgTroca_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                double desconto;
                Cursor = Cursors.WaitCursor;
                switch (dgTroca.Columns[e.ColumnIndex].Name)
                {
                    case "VALOR_DESCONTO":
                        desconto = (Convert.ToDouble(dgTroca.Rows[e.RowIndex].Cells[6].Value) * Convert.ToDouble(dgTroca.Rows[e.RowIndex].Cells[7].Value)) / 100;
                                dgTroca.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", Math.Round((Convert.ToDouble(dgTroca.Rows[e.RowIndex].Cells[6].Value)
                                    * Convert.ToInt32(dgTroca.Rows[e.RowIndex].Cells[5].Value)) - (desconto * Convert.ToInt32(dgTroca.Rows[e.RowIndex].Cells[5].Value)), 2));
                                lblTotalTroca.Text = String.Format("{0:N}", dgTroca.Rows[e.RowIndex].Cells[8].Value);
                        break;
                }

                AtualizaTotais();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDesconto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConfirma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.RowCount == 0)
                {
                    MessageBox.Show("Necessário ao menos um produto a ser Devolvido!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCodBarras.Focus();
                    return;
                }
                if (dgTroca.RowCount == 0)
                {
                    MessageBox.Show("Necessário ao menos um produto para Troca!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTrocaProduto.Focus();
                    return;
                }
                if (Convert.ToDouble(lblRestante.Text) > 0)
                {
                    MessageBox.Show("Valor Total de Produtos é diferente do Valor de Produtos Devolvidos.\nNecessário ser maior ou igual!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTrocaProduto.Focus();
                    return;
                }
                if (String.IsNullOrEmpty(txtVendedor.Text))
                {
                    MessageBox.Show("Informe o código do Vendedor!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtVendedor.Focus();
                    return;
                }
                if (String.IsNullOrEmpty(txtVendaID.Text))
                {
                    MessageBox.Show("Informe o número da Nota!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtVendaID.Focus();
                    return;
                }
                if (String.IsNullOrEmpty(txtIdentificacao.Text))
                {
                    MessageBox.Show("Informe o número de Identificação!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtIdentificacao.Focus();
                    return;
                }
                var buscaTroca = new ProdutosDevolvidos();
                string retorno = buscaTroca.VerificaNumIdentificacaoAberta(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIdentificacao.Text), "A");
                if (!String.IsNullOrEmpty(retorno))
                {
                    MessageBox.Show("Número Identificação pendente em outra Devolução. Por favor verifique!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtIdentificacao.Focus();
                    return;
                }
                if (MessageBox.Show("Confirma a Devolução dos Produtos?", "Lançamento Devolução de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    if (IncluirDevolucao())
                    {
                        Limpar();
                    }
                }
                else
                    btnConfirma.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool IncluirDevolucao()
        {
            try
            {
                int id = Funcoes.IdentificaVerificaID("PRODUTOS_DEVOLVIDOS", "ID", Principal.empAtual, "", Principal.estAtual);
                int idTroca = Funcoes.IdentificaVerificaID("PRODUTOS_TROCA", "ID", Principal.empAtual, "", Principal.estAtual);
                BancoDados.AbrirTrans();
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    var produtosDevolvidos = new ProdutosDevolvidos(
                        id + i,
                        Principal.empAtual,
                        Principal.estAtual,
                        dgProdutos.Rows[i].Cells[3].Value.ToString(),
                        Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value),
                        Convert.ToDouble(dgProdutos.Rows[i].Cells[6].Value),
                        Convert.ToDouble(dgProdutos.Rows[i].Cells[7].Value),
                        Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value),
                        Principal.usuario,
                        DateTime.Now,
                        Convert.ToInt32(txtIdentificacao.Text),
                        "A",
                        Convert.ToInt64(txtVendaID.Text),
                        Convert.ToInt32(txtVendedor.Text)
                        );

                    if (!produtosDevolvidos.InsereRegistros(produtosDevolvidos))
                    {
                        BancoDados.ErroTrans();
                        return false;
                    }
                }


                for (int x = 0; x < dgTroca.RowCount; x++)
                {
                    var produtosTroca = new ProdutosTroca(
                        idTroca + x,
                        Principal.empAtual,
                        Principal.estAtual,
                        dgTroca.Rows[x].Cells[3].Value.ToString(),
                        Convert.ToInt32(dgTroca.Rows[x].Cells[5].Value),
                        Convert.ToDouble(dgTroca.Rows[x].Cells[6].Value),
                        Convert.ToDouble(dgTroca.Rows[x].Cells[7].Value),
                        Convert.ToDouble(dgTroca.Rows[x].Cells[8].Value),
                        Principal.usuario,
                        DateTime.Now,
                        Convert.ToInt32(txtIdentificacao.Text),
                        "A",
                        Convert.ToInt64(txtVendaID.Text),
                        Convert.ToInt32(txtVendedor.Text)
                        );

                    if (!produtosTroca.InsereRegistros(produtosTroca))
                    {
                        BancoDados.ErroTrans();
                        return false;
                    }
                }

                BancoDados.FecharTrans();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void dgTroca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgTroca_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            AtualizaTotais();
            txtTrocaProduto.Focus();
        }

        private void txtIdentificacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtIdentificacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtIdentificacao.Text))
                {
                    MessageBox.Show("Num. de Identificação não pode ser em branco", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIdentificacao.Focus();
                }
                else
                    btnConfirma.PerformClick();
            }
        }

        private void txtVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtVendaID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtVendedor.Text))
                {
                    MessageBox.Show("Informe o código do Vendedor", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtVendedor.Focus();
                }
                else
                {
                    if (String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", txtVendedor.Text, false, false, true)))
                    {
                        MessageBox.Show("Vendedor não cadastrado. Por favor verifique!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtVendedor.Focus();
                    }
                    else
                    {
                        txtVendaID.Focus();
                    }

                }
            }
        }

        private void txtVendaID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtVendaID.Text))
                {
                    txtVendaID.Focus();
                }
                else
                {
                    bool achou = false;
                    var buscaItens = new VendasItens();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            if (String.IsNullOrEmpty(buscaItens.IdentificaSeCodBarrasPorID(Principal.empAtual, Principal.estAtual, Convert.ToInt64(txtVendaID.Text), dgProdutos.Rows[i].Cells[3].Value.ToString())))
                            {
                                achou = true;
                            }
                        }

                        if (achou)
                        {
                            MessageBox.Show("Existe(m) um ou mais produtos devolvidos que não foi encontrado neste Venda ID.\nPor favor, verifique!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtVendaID.Text = "";
                            txtVendaID.Focus();
                        }
                        else
                            txtIdentificacao.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Venda ID Inválido!", "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtVendaID.Focus();
                    }
                }
            }
        }

        public void AtualizaTotais()
        {
            try
            {
                qtdeTotalDeProdutos = 0;
                valorTotalDeProdutos = 0;

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    qtdeTotalDeProdutos += Convert.ToInt32(dgProdutos.Rows[i].Cells[5].Value);
                    valorTotalDeProdutos += Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value);
                }

                lblQtde.Text = qtdeTotalDeProdutos.ToString();
                lblTotal.Text = String.Format("{0:N}", valorTotalDeProdutos);
                
                qtdeTotalDeProdutos = 0;
                valorTotalDeProdutos = 0;

                for (int i = 0; i < dgTroca.RowCount; i++)
                {
                    valorTotalDeProdutos += Convert.ToDouble(dgTroca.Rows[i].Cells[8].Value);
                }

                lblTotalTroca.Text = String.Format("{0:N}", valorTotalDeProdutos);
                
                valorRestante = Convert.ToDouble(lblTotal.Text) - Convert.ToDouble(lblTotalTroca.Text);
                if (valorRestante >= 0)
                {
                    lblRestante.Text = String.Format("{0:N}", valorRestante);
                    lblDiferenca.Text = "0,00";
                    lblDiferenca.ForeColor = Color.Black;
                }
                else
                {
                    lblRestante.Text = "0,00";
                    lblDiferenca.Text = String.Format("{0:N}", Math.Abs(valorRestante));
                    lblDiferenca.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento Devolução de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
