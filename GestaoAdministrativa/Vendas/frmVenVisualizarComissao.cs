﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenVisualizarComissao : Form
    {
        private int codigo;

        public frmVenVisualizarComissao(int colCodigo)
        {
            InitializeComponent();
            codigo = colCodigo;
        }

        private void frmVenVisualizarComissao_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Visualizar Comissão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenVisualizarComissao_Shown(object sender, EventArgs e)
        {
            try
            {
                DataTable dtRetorno = new DataTable();
                var buscaComissao = new VendasDados();

                dtRetorno = buscaComissao.BuscaComissaoPorVendedor(codigo, Principal.estAtual, Principal.empAtual);

                if (dtRetorno.Rows.Count > 0)
                {
                    dgComissao.DataSource = dtRetorno;
                    object sumObject;
                    sumObject = dtRetorno.Compute("Sum(VALOR)", "");

                    lblTotal.Text = "TOTAL: " + sumObject.ToString();
                }
                else
                {
                    dgComissao.DataSource = dtRetorno;
                    lblTotal.Text = "";
                    MessageBox.Show("Nenhum registro de comissão encontrato!", "Visualizar Comissão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                dgComissao.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Visualizar Comissão", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgComissao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVenVisualizarComissao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;

            }
        }
    }
}
