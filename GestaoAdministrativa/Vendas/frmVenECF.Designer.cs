﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenECF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenECF));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbECF = new System.Windows.Forms.TabControl();
            this.tpFiscal = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtMsg2 = new System.Windows.Forms.TextBox();
            this.txtMsg1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCancelaFiscal = new System.Windows.Forms.Button();
            this.btnReducaoZ = new System.Windows.Forms.Button();
            this.btnLeituraX = new System.Windows.Forms.Button();
            this.tpSAT = new System.Windows.Forms.TabPage();
            this.gbCancelamento = new System.Windows.Forms.GroupBox();
            this.btnCancelarCupom = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dgCancelamento = new System.Windows.Forms.DataGridView();
            this.DATA_EMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HORA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHAVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIBERADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_EMITIDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbMes = new System.Windows.Forms.ComboBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnStatus = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.tbECF.SuspendLayout();
            this.tpFiscal.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpSAT.SuspendLayout();
            this.gbCancelamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelamento)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbECF);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // tbECF
            // 
            this.tbECF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbECF.Controls.Add(this.tpFiscal);
            this.tbECF.Controls.Add(this.tpSAT);
            this.tbECF.Location = new System.Drawing.Point(6, 37);
            this.tbECF.Name = "tbECF";
            this.tbECF.SelectedIndex = 0;
            this.tbECF.Size = new System.Drawing.Size(962, 495);
            this.tbECF.TabIndex = 44;
            // 
            // tpFiscal
            // 
            this.tpFiscal.Controls.Add(this.groupBox3);
            this.tpFiscal.Controls.Add(this.groupBox2);
            this.tpFiscal.ForeColor = System.Drawing.Color.Black;
            this.tpFiscal.Location = new System.Drawing.Point(4, 25);
            this.tpFiscal.Name = "tpFiscal";
            this.tpFiscal.Padding = new System.Windows.Forms.Padding(3);
            this.tpFiscal.Size = new System.Drawing.Size(954, 466);
            this.tpFiscal.TabIndex = 0;
            this.tpFiscal.Text = "Fiscal";
            this.tpFiscal.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnOk);
            this.groupBox3.Controls.Add(this.txtMsg2);
            this.groupBox3.Controls.Add(this.txtMsg1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(292, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(652, 172);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mensagem Promocional";
            // 
            // btnOk
            // 
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.ForeColor = System.Drawing.Color.Black;
            this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
            this.btnOk.Location = new System.Drawing.Point(580, 127);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(51, 39);
            this.btnOk.TabIndex = 4;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtMsg2
            // 
            this.txtMsg2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMsg2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsg2.Location = new System.Drawing.Point(22, 95);
            this.txtMsg2.Name = "txtMsg2";
            this.txtMsg2.Size = new System.Drawing.Size(609, 26);
            this.txtMsg2.TabIndex = 3;
            this.txtMsg2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMsg2_KeyPress);
            // 
            // txtMsg1
            // 
            this.txtMsg1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMsg1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsg1.Location = new System.Drawing.Point(22, 44);
            this.txtMsg1.Name = "txtMsg1";
            this.txtMsg1.Size = new System.Drawing.Size(609, 26);
            this.txtMsg1.TabIndex = 2;
            this.txtMsg1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMsg1_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mensagem 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mensagem 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCancelaFiscal);
            this.groupBox2.Controls.Add(this.btnReducaoZ);
            this.groupBox2.Controls.Add(this.btnLeituraX);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(11, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 454);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Funções Gerais";
            // 
            // btnCancelaFiscal
            // 
            this.btnCancelaFiscal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelaFiscal.ForeColor = System.Drawing.Color.Black;
            this.btnCancelaFiscal.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelaFiscal.Image")));
            this.btnCancelaFiscal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelaFiscal.Location = new System.Drawing.Point(17, 228);
            this.btnCancelaFiscal.Name = "btnCancelaFiscal";
            this.btnCancelaFiscal.Size = new System.Drawing.Size(230, 88);
            this.btnCancelaFiscal.TabIndex = 8;
            this.btnCancelaFiscal.Text = "               Cancela Último Cupom            Fiscal";
            this.btnCancelaFiscal.UseVisualStyleBackColor = true;
            this.btnCancelaFiscal.Click += new System.EventHandler(this.btnCancelaFiscal_Click);
            // 
            // btnReducaoZ
            // 
            this.btnReducaoZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReducaoZ.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReducaoZ.ForeColor = System.Drawing.Color.Black;
            this.btnReducaoZ.Image = ((System.Drawing.Image)(resources.GetObject("btnReducaoZ.Image")));
            this.btnReducaoZ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReducaoZ.Location = new System.Drawing.Point(17, 123);
            this.btnReducaoZ.Name = "btnReducaoZ";
            this.btnReducaoZ.Size = new System.Drawing.Size(230, 88);
            this.btnReducaoZ.TabIndex = 4;
            this.btnReducaoZ.Text = "       Redução Z";
            this.btnReducaoZ.UseVisualStyleBackColor = true;
            this.btnReducaoZ.Click += new System.EventHandler(this.btnReducaoZ_Click);
            // 
            // btnLeituraX
            // 
            this.btnLeituraX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeituraX.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeituraX.ForeColor = System.Drawing.Color.Black;
            this.btnLeituraX.Image = ((System.Drawing.Image)(resources.GetObject("btnLeituraX.Image")));
            this.btnLeituraX.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLeituraX.Location = new System.Drawing.Point(17, 21);
            this.btnLeituraX.Name = "btnLeituraX";
            this.btnLeituraX.Size = new System.Drawing.Size(230, 88);
            this.btnLeituraX.TabIndex = 0;
            this.btnLeituraX.Text = "       Leitura X";
            this.btnLeituraX.UseVisualStyleBackColor = true;
            this.btnLeituraX.Click += new System.EventHandler(this.btnLeituraX_Click);
            // 
            // tpSAT
            // 
            this.tpSAT.Controls.Add(this.gbCancelamento);
            this.tpSAT.Controls.Add(this.groupBox4);
            this.tpSAT.Location = new System.Drawing.Point(4, 25);
            this.tpSAT.Name = "tpSAT";
            this.tpSAT.Padding = new System.Windows.Forms.Padding(3);
            this.tpSAT.Size = new System.Drawing.Size(954, 466);
            this.tpSAT.TabIndex = 1;
            this.tpSAT.Text = "SAT";
            this.tpSAT.UseVisualStyleBackColor = true;
            // 
            // gbCancelamento
            // 
            this.gbCancelamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCancelamento.Controls.Add(this.btnCancelarCupom);
            this.gbCancelamento.Controls.Add(this.btnFiltrar);
            this.gbCancelamento.Controls.Add(this.dtData);
            this.gbCancelamento.Controls.Add(this.label3);
            this.gbCancelamento.Controls.Add(this.dgCancelamento);
            this.gbCancelamento.ForeColor = System.Drawing.Color.Black;
            this.gbCancelamento.Location = new System.Drawing.Point(8, 103);
            this.gbCancelamento.Name = "gbCancelamento";
            this.gbCancelamento.Size = new System.Drawing.Size(940, 357);
            this.gbCancelamento.TabIndex = 2;
            this.gbCancelamento.TabStop = false;
            this.gbCancelamento.Text = "Cancelamento de Vendas";
            this.gbCancelamento.Visible = false;
            // 
            // btnCancelarCupom
            // 
            this.btnCancelarCupom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelarCupom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarCupom.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarCupom.Image")));
            this.btnCancelarCupom.Location = new System.Drawing.Point(891, 313);
            this.btnCancelarCupom.Name = "btnCancelarCupom";
            this.btnCancelarCupom.Size = new System.Drawing.Size(43, 38);
            this.btnCancelarCupom.TabIndex = 4;
            this.btnCancelarCupom.UseVisualStyleBackColor = true;
            this.btnCancelarCupom.Click += new System.EventHandler(this.btnCancelarCupom_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.Location = new System.Drawing.Point(163, 313);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(43, 38);
            this.btnFiltrar.TabIndex = 3;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // dtData
            // 
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(52, 329);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(104, 22);
            this.dtData.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Data:";
            // 
            // dgCancelamento
            // 
            this.dgCancelamento.AllowUserToAddRows = false;
            this.dgCancelamento.AllowUserToDeleteRows = false;
            this.dgCancelamento.AllowUserToResizeColumns = false;
            this.dgCancelamento.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelamento.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgCancelamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCancelamento.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelamento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgCancelamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCancelamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DATA_EMISSAO,
            this.HORA,
            this.CHAVE,
            this.LIBERADO,
            this.VENDA_EMITIDO,
            this.VENDA_TOTAL});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCancelamento.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgCancelamento.GridColor = System.Drawing.Color.Black;
            this.dgCancelamento.Location = new System.Drawing.Point(9, 21);
            this.dgCancelamento.Name = "dgCancelamento";
            this.dgCancelamento.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelamento.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgCancelamento.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelamento.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgCancelamento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCancelamento.Size = new System.Drawing.Size(924, 286);
            this.dgCancelamento.TabIndex = 0;
            this.dgCancelamento.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCancelamento_CellClick);
            this.dgCancelamento.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgCancelamento_RowHeaderMouseClick);
            this.dgCancelamento.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgCancelamento_RowPrePaint);
            // 
            // DATA_EMISSAO
            // 
            this.DATA_EMISSAO.DataPropertyName = "DATA_EMISSAO";
            this.DATA_EMISSAO.HeaderText = "Data Emissao";
            this.DATA_EMISSAO.Name = "DATA_EMISSAO";
            this.DATA_EMISSAO.ReadOnly = true;
            this.DATA_EMISSAO.Width = 120;
            // 
            // HORA
            // 
            this.HORA.DataPropertyName = "HORA";
            this.HORA.HeaderText = "Hora";
            this.HORA.Name = "HORA";
            this.HORA.ReadOnly = true;
            // 
            // CHAVE
            // 
            this.CHAVE.DataPropertyName = "VENDA_NUM_CFE";
            this.CHAVE.HeaderText = "Chave Consulta";
            this.CHAVE.Name = "CHAVE";
            this.CHAVE.ReadOnly = true;
            this.CHAVE.Width = 480;
            // 
            // LIBERADO
            // 
            this.LIBERADO.DataPropertyName = "LIBERADO";
            this.LIBERADO.HeaderText = "Liberado Cancelamento";
            this.LIBERADO.Name = "LIBERADO";
            this.LIBERADO.ReadOnly = true;
            this.LIBERADO.Width = 200;
            // 
            // VENDA_EMITIDO
            // 
            this.VENDA_EMITIDO.DataPropertyName = "VENDA_EMITIDO";
            this.VENDA_EMITIDO.HeaderText = "Emitido";
            this.VENDA_EMITIDO.Name = "VENDA_EMITIDO";
            this.VENDA_EMITIDO.ReadOnly = true;
            this.VENDA_EMITIDO.Visible = false;
            // 
            // VENDA_TOTAL
            // 
            this.VENDA_TOTAL.DataPropertyName = "VENDA_TOTAL";
            this.VENDA_TOTAL.HeaderText = "Total";
            this.VENDA_TOTAL.Name = "VENDA_TOTAL";
            this.VENDA_TOTAL.ReadOnly = true;
            this.VENDA_TOTAL.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.txtEmail);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtAno);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cmbMes);
            this.groupBox4.Controls.Add(this.btnEnviar);
            this.groupBox4.Controls.Add(this.btnAtualizar);
            this.groupBox4.Controls.Add(this.btnStatus);
            this.groupBox4.Controls.Add(this.btnCancelar);
            this.groupBox4.Controls.Add(this.btnConsultar);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(8, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(940, 91);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Funções Gerais";
            // 
            // txtEmail
            // 
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(733, 23);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(200, 22);
            this.txtEmail.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(692, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Email";
            // 
            // txtAno
            // 
            this.txtAno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAno.Location = new System.Drawing.Point(559, 60);
            this.txtAno.MaxLength = 4;
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(88, 22);
            this.txtAno.TabIndex = 11;
            this.txtAno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAno_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(520, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ano";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(520, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mês";
            // 
            // cmbMes
            // 
            this.cmbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMes.FormattingEnabled = true;
            this.cmbMes.Items.AddRange(new object[] {
            "SELECIONE",
            "JANEIRO",
            "FEVEREIRO",
            "MARCO",
            "ABRIL",
            "MAIO",
            "JUNHO",
            "JULHO",
            "AGOSTO",
            "SETEMBRO",
            "OUTUBRO",
            "NOVEMBRO",
            "DEZEMBRO"});
            this.cmbMes.Location = new System.Drawing.Point(559, 21);
            this.cmbMes.Name = "cmbMes";
            this.cmbMes.Size = new System.Drawing.Size(127, 24);
            this.cmbMes.TabIndex = 8;
            this.cmbMes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMes_KeyPress);
            // 
            // btnEnviar
            // 
            this.btnEnviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnviar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.ForeColor = System.Drawing.Color.Black;
            this.btnEnviar.Image = ((System.Drawing.Image)(resources.GetObject("btnEnviar.Image")));
            this.btnEnviar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEnviar.Location = new System.Drawing.Point(787, 48);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(146, 34);
            this.btnEnviar.TabIndex = 7;
            this.btnEnviar.Text = "Enviar Arquivos ";
            this.btnEnviar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.ForeColor = System.Drawing.Color.Black;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtualizar.Location = new System.Drawing.Point(274, 21);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(118, 61);
            this.btnAtualizar.TabIndex = 6;
            this.btnAtualizar.Text = "     Atualizar Software";
            this.btnAtualizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // btnStatus
            // 
            this.btnStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStatus.ForeColor = System.Drawing.Color.Black;
            this.btnStatus.Image = ((System.Drawing.Image)(resources.GetObject("btnStatus.Image")));
            this.btnStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStatus.Location = new System.Drawing.Point(137, 21);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(131, 61);
            this.btnStatus.TabIndex = 5;
            this.btnStatus.Text = "     Status Operacional";
            this.btnStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStatus.UseVisualStyleBackColor = true;
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(398, 21);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(116, 61);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "     Cancelar Cupom";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.Black;
            this.btnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.Image")));
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(10, 21);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(121, 61);
            this.btnConsultar.TabIndex = 0;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Utilitários Fiscais";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenECF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenECF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmVenECF";
            this.Load += new System.EventHandler(this.frmVenECF_Load);
            this.Shown += new System.EventHandler(this.frmVenECF_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbECF.ResumeLayout(false);
            this.tpFiscal.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tpSAT.ResumeLayout(false);
            this.gbCancelamento.ResumeLayout(false);
            this.gbCancelamento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelamento)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnReducaoZ;
        private System.Windows.Forms.Button btnCancelaFiscal;
        private System.Windows.Forms.Button btnLeituraX;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtMsg2;
        private System.Windows.Forms.TextBox txtMsg1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TabControl tbECF;
        private System.Windows.Forms.TabPage tpFiscal;
        private System.Windows.Forms.TabPage tpSAT;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.GroupBox gbCancelamento;
        private System.Windows.Forms.DataGridView dgCancelamento;
        private System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancelarCupom;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_EMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn HORA;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHAVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIBERADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_EMITIDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TOTAL;
        private System.Windows.Forms.TextBox txtAno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbMes;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
    }
}