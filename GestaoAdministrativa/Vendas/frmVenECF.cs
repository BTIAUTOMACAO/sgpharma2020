﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Ecf;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.SAT;
using GestaoAdministrativa.Negocio;
using System.Xml;
using System.IO;
using System.IO.Compression;
using System.Net.Mail;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenECF : Form, Botoes
    {
        private ToolStripButton tsbUtiEcf = new ToolStripButton("Utilitários ECF");
        private ToolStripSeparator tssUtiEcf = new ToolStripSeparator();
        private string retorno;
        private int indice;
        private string xmlSAT;

        public frmVenECF(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
        }

        private void frmVenECF_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                gbCancelamento.Visible = false;
                dtData.Value = DateTime.Now;
                txtMsg1.Text = Funcoes.LeParametro(2, "28", false);
                txtMsg2.Text = Funcoes.LeParametro(2, "29", false);
                indice = 0;
                cmbMes.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários ECF", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Botao()
        {
            try
            {
                this.tsbUtiEcf.AutoSize = false;
                this.tsbUtiEcf.Image = Properties.Resources.impressora;
                this.tsbUtiEcf.Size = new System.Drawing.Size(125, 20);
                this.tsbUtiEcf.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbUtiEcf);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssUtiEcf);
                tsbUtiEcf.Click += delegate
                {
                    var utilECF = Application.OpenForms.OfType<frmVenECF>().FirstOrDefault();
                    Funcoes.BotoesCadastro(utilECF.Name);
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    utilECF.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbUtiEcf);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssUtiEcf);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void frmVenECF_Shown(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("E"))
            {
                if (Imprimir.VerificaModeloImpressoraFiscal().Equals(false))
                {
                    this.Close();
                }
                else
                {
                    tbECF.SelectTab(0);
                }
            }
            else if (Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
            {
                if (!Sat.VerificaModeloSAT())
                {
                    this.Close();
                }
                else
                {
                    tbECF.SelectTab(1);
                }
            }
        }


        private void btnLeituraX_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Imprimir.ImpFiscalLeituraX();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro() { }
        public void Anterior() { }
        public void Ultimo() { }
        public void Proximo() { }
        public void ImprimirRelatorio() { }
        public void Limpar() { }
        public void AtalhoGrade() { }
        public void AtalhoFicha() { }
        public bool Incluir() { return true; }
        public bool Atualiza() { return true; }
        public bool Excluir() { return true; }

        private void btnReducaoZ_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Imprimir.ImpFiscalReducaoZ();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnCancelaFiscal_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (Imprimir.ImpFiscalUltCupom())
                {
                    MessageBox.Show("Cancelado com Sucesso!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtMsg1.Text.Trim()) && String.IsNullOrEmpty(txtMsg2.Text.Trim()))
                {
                    MessageBox.Show("Necessário inserir pelo menos uma Mensagem Promocional", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMsg1.Focus();
                    return;
                }

                if (Funcoes.LeParametro(2, "01", false).Equals("N"))
                {
                    if (MessageBox.Show("Mensagem Promocional não está configurada para ser exibida. Deseja alterar?", "Utilitários Fiscais", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Funcoes.GravaParametro(2, "01", "S");
                    }
                }

                if (!String.IsNullOrEmpty(txtMsg1.Text.Trim()))
                {
                    Funcoes.GravaParametro(2, "02", txtMsg1.Text.Trim());
                }

                if (!String.IsNullOrEmpty(txtMsg2.Text.Trim()))
                {
                    Funcoes.GravaParametro(2, "03", txtMsg1.Text.Trim());
                }

                MessageBox.Show("Operação realizada com Sucesso!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnLeituraX.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtMsg1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtMsg2.Focus();
        }

        private void txtMsg2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.PerformClick();
        }

        public void Ajuda() { }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                gbCancelamento.Visible = false;
                Cursor = Cursors.WaitCursor;
                retorno = Sat.SatConsultarStatus();
                Cursor = Cursors.Default;
                if (Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    MessageBox.Show("Retorno SAT: " + retorno, "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Sat.TrataRetornoSAT(retorno);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            try
            {
                gbCancelamento.Visible = false;
                Cursor = Cursors.WaitCursor;
                retorno = Sat.SatConsultarStatusOperacional();
                Cursor = Cursors.Default;
                if (Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    MessageBox.Show("Retorno SAT: " + retorno, "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Sat.TrataRetornoSAT(retorno);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            try
            {
                gbCancelamento.Visible = false;
                Cursor = Cursors.WaitCursor;
                retorno = Sat.SatAtualizarSoftware();
                Cursor = Cursors.Default;
                Sat.TrataRetornoSAT(retorno);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                gbCancelamento.Visible = true;
                var cupons = new CuponsSAT();
                dgCancelamento.Rows.Clear();
                dgCancelamento.DataSource = cupons.BuscaCuponsDoDia(dtData.Value.ToString("dd/MM/yyyy"));
                if (dgCancelamento.Rows.Count == 0)
                {
                    MessageBox.Show("Nenhum registro encontrado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                var cupons = new CuponsSAT();
                dgCancelamento.DataSource = cupons.BuscaCuponsDoDia(dtData.Value.ToString("dd/MM/yyyy"));
                if (dgCancelamento.Rows.Count == 0)
                {
                    MessageBox.Show("Nenhum registro encontrado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCancelamento_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgCancelamento.Rows[e.RowIndex].Cells[3].Value.ToString() == "X")
            {
                dgCancelamento.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgCancelamento.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
            else
            {
                dgCancelamento.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
                dgCancelamento.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Green;
            }
        }



        private void btnCancelarCupom_Click(object sender, EventArgs e)
        {
            try
            {

                FileStream fs;
                StreamWriter sw;
                string mes = Funcoes.MesExtenso(DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) + DateTime.Now.Year.ToString();
                string numeroCupom, arquivoSATResposta;

                if (dgCancelamento.Rows.Count > 0)
                {
                    if (dgCancelamento.Rows[indice].Cells[3].Value.ToString().Equals("S"))
                    {
                        if (MessageBox.Show("Confirma o Cancelamento do Cupom?", "Utilitários Fiscais", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            var updateCupom = new CuponsSAT();
                            Cursor = Cursors.WaitCursor;

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                xmlSAT = Sat.MontaXmlCancelamento(dgCancelamento.Rows[indice].Cells[2].Value.ToString());
                                if (!String.IsNullOrEmpty(xmlSAT))
                                {
                                    retorno = Sat.SatCancelarUltimaVenda(dgCancelamento.Rows[indice].Cells[2].Value.ToString(), xmlSAT);

                                    if (!Sat.TrataRetornoSATVenda(retorno, out arquivoSATResposta, out numeroCupom))
                                    {
                                        fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesComErro\" + mes.ToUpper() + @"\" + dgCancelamento.Rows[indice].Cells[2].Value.ToString() + ".xml", FileMode.Create);
                                        sw = new StreamWriter(fs);
                                        sw.WriteLine(xmlSAT);
                                        sw.Flush();
                                        sw.Close();
                                        fs.Close();
                                        return;
                                    }

                                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesCanEnviados\" + mes.ToUpper() + @"\" + dgCancelamento.Rows[indice].Cells[2].Value.ToString() + ".xml", FileMode.Create);
                                    sw = new StreamWriter(fs);
                                    sw.WriteLine(xmlSAT);
                                    sw.Flush();
                                    sw.Close();
                                    fs.Close();

                                    //CONVERTE BASE64 PARA STRING//
                                    xmlSAT = Sat.Base64Decode(arquivoSATResposta);

                                    fs = new FileStream(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml", FileMode.Create);
                                    sw = new StreamWriter(fs);
                                    sw.WriteLine(xmlSAT);
                                    sw.Flush();
                                    sw.Close();
                                    fs.Close();

                                    if (Sat.SatImpressaoComprovanteCancelamento(@"" + Funcoes.LeParametro(15, "06", true) + @"\CFesCancelados\CFesSemErro\" + mes.ToUpper() + @"\" + numeroCupom + ".xml",
                                        numeroCupom, Convert.ToDouble(dgCancelamento.Rows[indice].Cells[5].Value), out retorno))
                                    {
                                        updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, dgCancelamento.Rows[indice].Cells[2].Value.ToString(), DateTime.Now, Principal.usuario, numeroCupom);
                                        MessageBox.Show("Cupom Cancelado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                            else
                            {
                                int iRetorno = DarumaDLL.tCFeCancelar_SAT_Daruma();
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno SAT: " + DarumaDLL.TrataRetorno(iRetorno), "D-SAT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    updateCupom.CancelaCupom(Principal.estAtual, Principal.empAtual, dgCancelamento.Rows[indice].Cells[2].Value.ToString(), DateTime.Now, Principal.usuario, "");
                                    MessageBox.Show("Cupom Cancelado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }

                            dgCancelamento.DataSource = updateCupom.BuscaCuponsDoDia(dtData.Value.ToString("dd/MM/yyyy"));
                        }
                    }
                    else
                    {
                        MessageBox.Show("Prazo de 30 minutos ultrapassado. Cupom não pode ser mais cancelado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Nenhum Registro!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgCancelamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void dgCancelamento_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void cmbMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(cmbMes.SelectedIndex > 0)
            {
                txtAno.Focus();
            }
        }

        private void txtAno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtEmail.Focus();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbMes.SelectedIndex <= 0)
                {
                    MessageBox.Show("Necessário selecionar um Mês", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbMes.Focus();
                }
                else if (String.IsNullOrEmpty(txtAno.Text))
                {
                    MessageBox.Show("Necessário informar um Ano", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtAno.Focus();
                }
                else if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    MessageBox.Show("Necessário informar um E-mail", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;

                    string diretorio = @"C:\SAT\CFesProcessados\CFesSemErro\" + cmbMes.Text + txtAno.Text; //Caminho do diretório
                    string arquivo = @"C:\SAT\CFesProcessados\CFesSemErro\" + cmbMes.Text + txtAno.Text + ".zip"; //Caminho do arquivo zip a ser criado
                    if (!File.Exists(arquivo))
                    {
                        ZipFile.CreateFromDirectory(diretorio, arquivo);
                    }

                    if (File.Exists(arquivo))
                    {
                        SmtpClient client = new SmtpClient("smtp.cdconectec.com.br");
                        client.Port = 587;
                        client.EnableSsl = false;
                        client.Timeout = 100000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;

                        client.Credentials = new System.Net.NetworkCredential("sgpharma@cdconectec.com.br", "Sat@#2019");
                        MailMessage msg = new MailMessage();
                        msg.To.Add(new MailAddress(txtEmail.Text));

                        Attachment anexar = new Attachment(arquivo);
                        msg.Attachments.Add(anexar);

                        msg.From = new MailAddress("sgpharma@cdconectec.com.br");
                        msg.Subject = "ARQUIVOS FISCAIS DROGARIA " + Funcoes.LeParametro(6, "129", false) + " - " + cmbMes.Text + "/" + txtAno.Text;
                        msg.Body = "Segue arquivos SAT do mês de " + cmbMes.Text + " de " + txtAno.Text;

                        client.Send(msg);

                        MessageBox.Show("E-mail enviado com sucesso!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbMes.SelectedIndex = 0;
                        txtAno.Text = "";
                        txtEmail.Text = "";
                        cmbMes.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Verifique se existe a pasta do mês/ano informado!", "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbMes.Focus();
                    }


                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Utilitários Fiscais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
