﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenTransferenciaProdutos : Form, Botoes
    {
        private ToolStripButton tsbTransferencia = new ToolStripButton("Trans. Produtos");
        private ToolStripSeparator tssTransferencia = new ToolStripSeparator();
        private string ID;
        private double custo;
        private double valor;
        private int totalQtde;
        private double valorTotal;
        private int id;
        private DataTable dtLePrdutos = new DataTable();
        private Produto buscaProduto = new Produto();
        private int index;
        private int idTransferencia;


        public frmVenTransferenciaProdutos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public async void Limpar()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtQtde.Text = "";
            txtQtdeEstoque.Text = "";
            txtFBusca.Text = String.Empty;
            txtFCidade.Text = "";
            txtFCnpj.Text = "";
            txtFNome.Text = "";
            cmbFBusca.SelectedIndex = 0;
            cmbGrupos.SelectedIndex = -1;
            cmbPendentes.SelectedIndex = -1;
            dgProdutos.Rows.Clear();
            dgProdutos.Visible = false;
            btnConfirmar.Visible = false;
            gbInformacoes.Visible = false;
            dgProdutos.AllowUserToDeleteRows = true;
            cmbVendedor.SelectedIndex = -1;
            totalQtde = 0;
            valorTotal = 0;
            index = 0;

            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
            {
                cmbGrupos.Enabled = true;

                cmbPendentes.Enabled = true;
                List<Reserva> dados = await BuscaReservasDeProdutos();
                if (dados.Count > 0)
                {
                    if (dados[0].CodErro == "00")
                    {
                        cmbPendentes.DataSource = dados;
                        cmbPendentes.DisplayMember = "NomeFilial";
                        cmbPendentes.ValueMember = "LojaOrigem";
                        cmbPendentes.SelectedIndex = -1;
                    }
                }
            }
            else
            {
                cmbPendentes.Enabled = false;
                cmbGrupos.Enabled = false;
            }

            cmbFBusca.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTransferencia);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        private void frmVenTransferenciaProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dgProdutos.Visible = false;
                btnConfirmar.Visible = false;
                gbInformacoes.Visible = false;
                totalQtde = 0;
                valorTotal = 0;
                id = 0;
                index = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTransferencia.AutoSize = false;
                this.tsbTransferencia.Image = Properties.Resources.vendas;
                this.tsbTransferencia.Size = new System.Drawing.Size(130, 20);
                this.tsbTransferencia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTransferencia);
                tsbTransferencia.Click += delegate
                {
                    var transferencia = Application.OpenForms.OfType<frmVenTransferenciaProdutos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    transferencia.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                {
                    var buscaFornecedor = new Cliente();
                    List<Cliente> cliente = new List<Cliente>();
                    if (cmbFBusca.SelectedIndex == 0)
                    {
                        cliente = buscaFornecedor.BuscaDadosFornecedor(1, txtFBusca.Text.Trim().ToUpper(), 1);
                    }
                    else if (cmbFBusca.SelectedIndex == 1)
                    {
                        long cnpj = Convert.ToInt64(txtFBusca.Text);
                        cliente = buscaFornecedor.BuscaDadosFornecedor(2, String.Format(@"{0:00\.000\.000\/0000\-00}", cnpj), 1);
                    }
                    if (cliente.Count.Equals(1))
                    {
                        txtFNome.Text = cliente[0].CfNome;
                        txtFCnpj.Text = cliente[0].CfDocto;
                        txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                        cmbFBusca.SelectedIndex = 0;
                        txtCodBarras.Focus();
                    }
                    else if (cliente.Count.Equals(0))
                    {
                        MessageBox.Show("Fornecedor não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbFBusca.SelectedIndex = 0;
                        txtFBusca.Text = "";
                        txtFBusca.Focus();
                    }
                    else
                    {
                        using (var t = new frmBuscaForn("fornecedor"))
                        {
                            for (int i = 0; i < cliente.Count; i++)
                            {
                                t.dgCliFor.Rows.Insert(t.dgCliFor.RowCount, new Object[] { "0", "0", cliente[i].CfCodigo, cliente[i].CfNome, cliente[i].CfApelido, cliente[i].CfObservacao,
                                    cliente[i].CfDocto, cliente[i].CfEndereco, cliente[i].CfBairro, cliente[i].CfCidade, cliente[i].CfUF, cliente[i].CfId, "0", "0", "0", cliente[i].CfStatus, 0 });
                            }
                            t.ShowDialog();
                        }

                        if (!String.IsNullOrEmpty(Principal.cnpjCliFor))
                        {
                            txtFNome.Text = Principal.nomeCliFor;
                            txtFCnpj.Text = Principal.cnpjCliFor;
                            txtFCidade.Text = Principal.cidadeCliFor;
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                        }
                        else
                        {
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtFBusca.Focus();
                        }

                    }

                }
                else
                {
                    cmbGrupos.Focus();
                }
            }
        }
        
        private async void frmVenTransferenciaProdutos_Shown(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
            {
                cmbGrupos.Enabled = true;
                List<EstabelecimentoFilial> dadosEstb =  await EnviaOuRecebeDadosMysql.BuscaEstabelecimentos();
                
                cmbGrupos.DataSource = dadosEstb;
                cmbGrupos.ValueMember = "codEstabelecimento";
                cmbGrupos.DisplayMember = "nomeEstabelecimento";
                cmbGrupos.SelectedIndex = -1;

                cmbPendentes.Enabled = true;
                List<Reserva> dados = await BuscaReservasDeProdutos();
                if(dados.Count > 0)
                {
                    if(dados[0].CodErro == "00")
                    {
                        cmbPendentes.DataSource = dados;
                        cmbPendentes.DisplayMember = "NomeFilial";
                        cmbPendentes.ValueMember = "LojaOrigem";
                        cmbPendentes.SelectedIndex = -1;
                    }
                }
            }
            else
            {
                cmbGrupos.Enabled = false;
                cmbPendentes.Enabled = false;
            }


            Principal.dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = -1;
            }

            txtFBusca.Focus();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCampos())
                {
                    if (!dgProdutos.Visible)
                        dgProdutos.Visible = true;

                    if (!btnConfirmar.Visible)
                        btnConfirmar.Visible = true;

                    if (!gbInformacoes.Visible)
                        gbInformacoes.Visible = true;

                    bool achou = false;
                    int qtde = 0;
                    for (int i = 0; i < dgProdutos.RowCount; i++)
                    {
                        if (dgProdutos.Rows[i].Cells[3].Value.ToString() == txtCodBarras.Text)
                        {
                            qtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value);
                            dgProdutos.Rows[i].Cells[6].Value = qtde + Convert.ToInt32(txtQtde.Text);
                            achou = true;
                        }
                    }

                    totalQtde += Convert.ToInt32(txtQtde.Text);
                    valorTotal += Convert.ToDouble(txtPreco.Text);

                    if (!achou)
                    {
                        dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { Principal.empAtual, Principal.estAtual, ID, txtCodBarras.Text, txtDescr.Text, txtPreco.Text, txtQtde.Text, custo, valor });
                    }

                    txtCodBarras.Text = string.Empty;
                    txtDescr.Text = string.Empty;
                    txtQtde.Text = "0";
                    txtPreco.Text = string.Empty;
                    txtQtdeEstoque.Text = "0";

                    lblQtde.Text = totalQtde.ToString();
                    lblTotal.Text = String.Format("{0:N}", valorTotal);

                    txtCodBarras.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. Barras não pode ser em Branco!";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()))
            {
                Principal.mensagem = "Qtde não pode ser em Branco!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (Convert.ToInt32(txtQtde.Text) == 0)
            {
                Principal.mensagem = "Qtde não pode ser Zero!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }


            return true;
        }

        public bool LeProdutosCod()
        {
            try
            {
                custo = 0;
                valor = 0;
                Cursor = Cursors.WaitCursor;
                
                dtLePrdutos = buscaProduto.BuscaProdutosTransferencia(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }

                    txtPreco.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_PRECOMPRA"]);
                    txtQtdeEstoque.Text = dtLePrdutos.Rows[0]["PROD_ESTATUAL"].ToString();
                    txtQtde.Text = "0";
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();
                    custo = Convert.ToDouble(dtLePrdutos.Rows[0]["CUSTO"]);
                    valor = Convert.ToDouble(dtLePrdutos.Rows[0]["PRE_VALOR"]);
                    
                    txtQtde.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtQtdeEstoque") && (control.Name != "txtPreco"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void cmbGrupos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtQtde.Text) || Convert.ToInt32(txtQtde.Text) != 0)
                {
                    btnAdicionar.PerformClick();
                }
                else
                {
                    MessageBox.Show("Quantidade não pode ser em branco/zero!", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQtde.Text = "0";
                    txtQtde.Focus();
                }
            }

        }

        private void txtQtde_Validated(object sender, EventArgs e)
        {
        //    if (!String.IsNullOrEmpty(txtQtde.Text) || Convert.ToInt32(txtQtde.Text) != 0)
        //    {
        //        if (Convert.ToDouble(txtQtdeEstoque.Text) < Convert.ToDouble(txtQtde.Text))
        //        {
        //            MessageBox.Show("Quantidade de estoque inferior!", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //    }
        }

        private void btnAdicionar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAdicionar, "F1 - Adicionar Produto");
        }

        private void btnConfirmar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConfirmar, "F2 - Confirmar");
        }

        private async void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.RowCount > 0)
                {
                    if(cmbVendedor.SelectedIndex == -1)
                    {
                        MessageBox.Show("Necessário selecionar um Vendedor", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbVendedor.Focus();
                        return;
                    }
                    if (MessageBox.Show("Confirma a Transferência de Produtos?", "Transferência de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        idTransferencia = Funcoes.IdentificaVerificaID("TRANSFERENCIA", "ID");
                        Cursor = Cursors.WaitCursor;
                        BancoDados.AbrirTrans();
                        if (!RemoveEstoque())
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        if(!AtualizaMovimentoEstoque())
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        var dados = new Transferencia();
                        if (!dados.InsereRegistros(Principal.estAtual, Principal.empAtual, idTransferencia, txtObservacao.Text, Principal.nomeEstacao, 
                            (cmbGrupos.SelectedIndex != -1 ? cmbGrupos.Text : txtFNome.Text + " - " + txtFCnpj.Text), (cmbGrupos.SelectedIndex != -1 ? Convert.ToInt32(cmbGrupos.SelectedValue) : 0)))
                        {
                            BancoDados.ErroTrans();
                            return;
                        }

                        var dadosItens = new TransferenciaItens();
                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            if (!dadosItens.InsereRegistros(Convert.ToInt32(dgProdutos.Rows[i].Cells[1].Value), Convert.ToInt32(dgProdutos.Rows[i].Cells[0].Value),
                                idTransferencia, dgProdutos.Rows[i].Cells[3].Value.ToString(), Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value), Convert.ToDouble(dgProdutos.Rows[i].Cells[5].Value)))
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                        }

                        int idFalta = Funcoes.IdentificaVerificaID("LANCAMENTO_FALTAS", "ID", 0, "", 0);

                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            var faltas = new LancamentoFaltas(
                            Convert.ToInt32(dgProdutos.Rows[i].Cells[1].Value),
                            Convert.ToInt32(dgProdutos.Rows[i].Cells[0].Value),
                            idFalta,
                            dgProdutos.Rows[i].Cells[3].Value.ToString(),
                            Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value),
                            "",
                            "",
                            Principal.nomeEstacao,
                            "P",
                            Convert.ToInt32(cmbVendedor.SelectedValue),
                            DateTime.Now,
                            cmbVendedor.Text,
                            DateTime.Now,
                            cmbVendedor.Text,
                            idTransferencia
                            );

                            if (faltas.InsereRegistros(faltas, true))
                            {
                                await InsereFalta(faltas);
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                return;
                            }
                            
                            idFalta = idFalta + 1;
                        }

                        BancoDados.FecharTrans();

                        //IDENTIFICA SE USA ESTOQUE FILIAL PARA ATUALIZAÇÃO OU INCLUSÃO//
                        if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                        {
                            //TIRA DO ESTOQUE DA ATUAL//
                            await AtualizarEstoqueFiliais("menos", Funcoes.LeParametro(9, "52", true));
                            
                            if (cmbGrupos.SelectedIndex != -1)
                            {
                                //SOMA PARA A LOJA DESEJADA//
                                await AtualizarEstoqueFiliais("mais", cmbGrupos.SelectedValue.ToString());

                                //INSERE TRANSFERENCIA PARA PODER IMPORTAR NA OUTRA FILIAL//
                                await InsereTransferencia();
                            }
                            else
                            {
                                id = 0;
                            }
                            
                            if(cmbPendentes.SelectedIndex != -1)
                            {
                                string[] split = cmbPendentes.Text.Split('-');
                                
                                await AtualizaStatusReserva(Convert.ToInt32(split[0]));
                            }
                            
                            dados.AtualizarCodigoTransferencia(id, Principal.estAtual, Principal.empAtual, idTransferencia);
                        }
                        
                        Cursor = Cursors.Default;

                        if (MessageBox.Show("Deseja imprimir comprovante de transferência?", "Transferência de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            Cursor = Cursors.WaitCursor;
                            Comprovante();
                        }

                        Limpar();
                    }
                    else
                        txtCodBarras.Focus();
                }
                else
                {
                    MessageBox.Show("Necessário adicionar ao menos um produto!", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProdutos.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> InsereFalta(LancamentoFaltas falta)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Falteiro/InsereFalteiro?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                    + "&grupoID=" + Funcoes.LeParametro(9, "53", true)
                                    + "&idDaLoja=" + falta.ID
                                    + "&prodCodigo=" + falta.ProdCodigo
                                    + "&qtde=" + falta.Qtde
                                    + "&observacao=" + falta.Observacao
                                    + "&usuario=" + falta.OpCadastro);
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\Faltas.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + falta.ProdCodigo + " / QTDE: " + falta.Qtde);
                        }
                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool AtualizaMovimentoEstoque()
        {
            try
            {
                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = id,
                        ProdCodigo = dgProdutos.Rows[i].Cells[3].Value.ToString(),
                        EntQtde = 0,
                        EntValor = 0,
                        SaiQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value),
                        SaiValor = Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value) * Convert.ToDouble(dgProdutos.Rows[i].Cells[8].Value),
                        OperacaoCodigo = "T",
                        DataMovimento = DateTime.Now,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    id += 1;

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;
                }
                

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }



        private async Task AtualizarEstoqueFiliais(string operacao, string filial)
        {
            try
            {
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoque?codBarra=" + dgProdutos.Rows[i].Cells[3].Value.ToString()
                                        + "&descricao=" + dgProdutos.Rows[i].Cells[4].Value.ToString()
                                        + "&qtde=" + Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value)
                                        + "&precoCusto=" + dgProdutos.Rows[i].Cells[7].Value.ToString().Replace(",", ".")
                                        + "&precoVenda=" + dgProdutos.Rows[i].Cells[8].Value.ToString().Replace(",", ".") + "&codEstabelecimento=" + filial + "&operacao=" + operacao);
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\TransferenciaProduto.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + dgProdutos.Rows[i].Cells[3].Value.ToString() + " / QTDE: " + dgProdutos.Rows[i].Cells[6].Value);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool RemoveEstoque()
        {
            try
            {
                var estoque = new ProdutoDetalhe();
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    estoque.EmpCodigo = Convert.ToInt32(dgProdutos.Rows[i].Cells[0].Value);
                    estoque.EstCodigo = Convert.ToInt32(dgProdutos.Rows[i].Cells[1].Value);
                    estoque.ProdCodigo = dgProdutos.Rows[i].Cells[3].Value.ToString();
                    estoque.ProdEstAtual = Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value);

                    estoque.AtualizaEstoqueAtual(estoque, "-", true);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<int> EnviaTransacao()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Transferencia/InsereTransferenciaDeProdutos?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&observacao=" + txtObservacao.Text + "&usuario=" + Principal.usuario + "&estabDestino=" + cmbGrupos.SelectedValue);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    
                    return Convert.ToInt32(responseBody);
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        public async Task<bool> InsereTransferencia()
        {
            try
            {

                id = await EnviaTransacao();

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Transferencia/InsereItensTransferenciaDeProdutos?id=" + id
                                        + "&prodCodigo=" + dgProdutos.Rows[i].Cells[3].Value.ToString()
                                        + "&qtde=" + Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value));
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\TransferenciaProdutos.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + dgProdutos.Rows[i].Cells[3].Value.ToString() + " / QTDE: " + dgProdutos.Rows[i].Cells[6].Value);
                            }
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void cmbGrupos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtFBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmVenTransferenciaProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

        public bool Comprovante()
        {
            try
            {
                var dadosEstabelecimento = new Estabelecimento();
                string tamanho, comprovanteTransferencia;
                int quantidade = 0;
                double total = 0;
                List<Cliente> dadosVendaCliente = new List<Cliente>();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                if (retornoEstab.Count > 0)
                {
                    comprovanteTransferencia = Funcoes.CentralizaTexto("TRANSFERENCIA DE PRODUTOS", 43) + "\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto("ENTRE FILIAIS", 43) + "\n\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                    comprovanteTransferencia += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                    comprovanteTransferencia += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                    tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                    comprovanteTransferencia += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                    comprovanteTransferencia += retornoEstab[0].EstBairro + "\n";
                    comprovanteTransferencia += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                    comprovanteTransferencia += "Data: " + DateTime.Now.ToString("dd/MM/yyyy") + "     Hora: " + DateTime.Now.ToString("HH:mm:ss") + "\n";
                    comprovanteTransferencia += "Filial: " + (cmbGrupos.SelectedIndex != -1 ? cmbGrupos.Text : txtFNome.Text + " - " + txtFCnpj.Text) + "\n";
                    comprovanteTransferencia += "Vendedor: " + cmbVendedor.Text + "\n";
                    comprovanteTransferencia += "Id para Cancelamento: " + idTransferencia + "\n";
                    comprovanteTransferencia += "__________________________________________" + "\n";
                    comprovanteTransferencia += "#|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovanteTransferencia += "__________________________________________" + "\n";

                    for (int i = 0; i < dgProdutos.Rows.Count; i++)
                    {
                        comprovanteTransferencia += Funcoes.PrencherEspacoEmBranco(dgProdutos.Rows[i].Cells[3].Value.ToString(), 13) + " " + (dgProdutos.Rows[i].Cells[4].Value.ToString().Length > 29 ? dgProdutos.Rows[i].Cells[4].Value.ToString().Substring(0, 28) : dgProdutos.Rows[i].Cells[4].Value) + "\n";
                        comprovanteTransferencia += "    R$ " + String.Format("{0:N}", dgProdutos.Rows[i].Cells[5].Value) + " X " + dgProdutos.Rows[i].Cells[6].Value + " =  R$ " + String.Format("{0:N}", Convert.ToDouble(dgProdutos.Rows[i].Cells[5].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value)) + "\n";

                        quantidade += Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value);
                        total += Convert.ToDouble(dgProdutos.Rows[i].Cells[5].Value) * Convert.ToInt32(dgProdutos.Rows[i].Cells[6].Value);
                    }

                    comprovanteTransferencia += "==========================================" + "\n\n";
                    comprovanteTransferencia += "Quantidade de Produtos : " + quantidade + "\n";
                    comprovanteTransferencia += "Valor Total de Produtos: " + String.Format("{0:N}", total) +"\n";
                    if(id != 0)
                    {
                        comprovanteTransferencia += "ID da Transferência: " + id + "\n";
                    }
                    if(!String.IsNullOrEmpty(txtObservacao.Text))
                    {
                        comprovanteTransferencia += "Observação: " + txtObservacao.Text.ToUpper() + "\n";
                    }
                    comprovanteTransferencia += "==========================================" + "\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovanteTransferencia, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteTransferencia);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\Transferencia.txt", true))
                            {
                                writer.WriteLine(comprovanteTransferencia);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovanteTransferencia += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteTransferencia);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        comprovanteTransferencia += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteTransferencia, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObservacao.Focus();
        }

        private void txtObservacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();
        }

        public async Task<List<Reserva>> BuscaReservasDeProdutos()
        {
            List<Reserva> allItems = new List<Reserva>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/BuscaReservaPorCodLojaEStatus?codLoja=" + Funcoes.LeParametro(9, "52", true)
                        + "&status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Reserva>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void cmbPendentes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnPendentes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private async void btnPendentes_Click(object sender, EventArgs e)
        {
            if(cmbPendentes.SelectedIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                cmbGrupos.SelectedValue = cmbPendentes.SelectedValue;
                string[] split = cmbPendentes.Text.Split('-');

                List<ReservaItens> itens = await BuscaPendentesItensPorID(Convert.ToInt32(split[0]));

                if(itens.Count > 0)
                {
                    if(itens[0].CodErro == "00")
                    {
                        if (!dgProdutos.Visible)
                            dgProdutos.Visible = true;

                        if (!btnConfirmar.Visible)
                            btnConfirmar.Visible = true;

                        if (!gbInformacoes.Visible)
                            gbInformacoes.Visible = true;

                        dgProdutos.AllowUserToDeleteRows = false;
                      
                        totalQtde = 0;
                        valorTotal = 0;

                        for (int i=0; i < itens.Count; i++)
                        {
                            custo = 0;
                            valor = 0;
                            dtLePrdutos = buscaProduto.BuscaProdutosTransferencia(Principal.estAtual, Principal.empAtual, itens[i].CodBarras);
                            if (dtLePrdutos.Rows.Count > 0)
                            {
                                dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { Principal.empAtual, Principal.estAtual, dtLePrdutos.Rows[0]["PROD_ID"].ToString(),
                                    itens[i].CodBarras, dtLePrdutos.Rows[0]["PROD_DESCR"].ToString(), dtLePrdutos.Rows[0]["PROD_PRECOMPRA"], itens[i].Qtde,
                                    dtLePrdutos.Rows[0]["CUSTO"], dtLePrdutos.Rows[0]["PRE_VALOR"] });

                                totalQtde += itens[i].Qtde;
                                valorTotal += Convert.ToDouble(dtLePrdutos.Rows[0]["PROD_PRECOMPRA"]);
                            }
                        }

                        lblQtde.Text = totalQtde.ToString();
                        lblTotal.Text = String.Format("{0:N}", valorTotal);
                        
                        Cursor = Cursors.Default;
                        cmbVendedor.Focus();
                    }
                    else
                        MessageBox.Show("Erro " + itens[0].CodErro + ": " + itens[0].Mensagem, "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado. Necessário solicitar a filial para refazer a reserva.", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Necessário escolher uma Reserva Pendente!", "Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        public async Task<List<ReservaItens>> BuscaPendentesItensPorID(int idReserva)
        {
            List<ReservaItens> allItems = new List<ReservaItens>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/BuscaReservatensPorID?ID=" + idReserva);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<ReservaItens>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void cmbPendentes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnPendentes.PerformClick();
            }
        }

        private void dgProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            index = e.RowIndex;
        }

        public async Task<bool> AtualizaStatusReserva(int idReserva)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/AtualizacaoDeStatus?id=" + idReserva
                        + "&status=E");
                    return response.IsSuccessStatusCode;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

    }
}
