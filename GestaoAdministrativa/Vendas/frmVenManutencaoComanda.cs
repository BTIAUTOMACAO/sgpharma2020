﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenManutencaoComanda : Form, Botoes
    {
        private ToolStripButton tsbComanda = new ToolStripButton("Manut. de Comanda");
        private ToolStripSeparator tssComanda = new ToolStripSeparator();

        VendasDados vendaFiltro = new VendasDados();
        private long vendaID;
        private DataTable dtRetorno = new DataTable();

        public frmVenManutencaoComanda(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnFiltrar.PerformClick();
                    break;
                case Keys.F2:
                    btnExcluir.PerformClick();
                    break;
            }
        }

        private void frmVenManutencaoComanda_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Botao()
        {
            try
            {
                this.tsbComanda.AutoSize = false;
                this.tsbComanda.Image = Properties.Resources.vendas;
                this.tsbComanda.Size = new System.Drawing.Size(145, 20);
                this.tsbComanda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbComanda);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssComanda);
                tsbComanda.Click += delegate
                {
                    var comanda = Application.OpenForms.OfType<frmVenManutencaoComanda>().FirstOrDefault();
                    Util.BotoesGenericos();
                    comanda.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            Txt_NComanda.Text = "";
            db_Comanda.Rows.Clear();
            Txt_NComanda.Focus();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbComanda);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssComanda);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Txt_NComanda.Text))
                {
                    Cursor = Cursors.WaitCursor;
                    VendasDados vendasDados = new VendasDados();
                    dtRetorno = vendasDados.BuscaComandaAberta(Txt_NComanda.Text);
                  
                    if (dtRetorno.Rows.Count == 0)
                    {
                        MessageBox.Show("Não existe este número de comanda.", "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        vendaID = Convert.ToInt64(dtRetorno.Rows[0]["VENDA_ID"]);
                        db_Comanda.DataSource = dtRetorno;
                    }
                }
                else
                {
                    MessageBox.Show("Informe o número da comanda.", "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Txt_NComanda.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Txt_NComanda_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnFiltrar.PerformClick();
        }
        
        private void btnCancela_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseja Limpar esta comanda?", "Manutenção de Comanda", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(Txt_NComanda.Text))
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        Cursor = Cursors.WaitCursor;
                        VendasDados vendasDados = new VendasDados();
                        if (vendasDados.ExcluiComandaAberta(Txt_NComanda.Text, vendaID))
                        {
                            Funcoes.GravaLogExclusao("VENDAS", Txt_NComanda.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "VENDAS", Txt_NComanda.Text, Principal.motivo, Principal.estAtual);
                            MessageBox.Show("Comanda Limpa!", "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            dtRetorno = vendasDados.BuscaComandaAberta(Txt_NComanda.Text);
                            db_Comanda.DataSource = dtRetorno;
                            Txt_NComanda.Text = "";
                            Txt_NComanda.Focus();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Comanda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void frmVenManutencaoComanda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void btnExcluir_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void Txt_NComanda_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void db_Comanda_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void Txt_NComanda_TextChanged(object sender, EventArgs e)
        {

        }
    }
}