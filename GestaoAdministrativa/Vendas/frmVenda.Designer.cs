﻿using System;

namespace GestaoAdministrativa.Vendas
{
    partial class frmVenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenda));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCliente = new System.Windows.Forms.Panel();
            this.lblConveniada = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.btnPreAutorizar = new System.Windows.Forms.Button();
            this.txtCliEmp = new System.Windows.Forms.TextBox();
            this.txtCodCliEmp = new System.Windows.Forms.TextBox();
            this.lblCliEmp = new System.Windows.Forms.Label();
            this.tlpDiversos = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tlpDadosProdutos = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.txtVUni = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPQtde = new System.Windows.Forms.TextBox();
            this.txtVDesc = new System.Windows.Forms.TextBox();
            this.txtPDesc = new System.Windows.Forms.TextBox();
            this.txtSTotal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancela = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblComissao = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVTotal = new System.Windows.Forms.TextBox();
            this.txtCusto = new System.Windows.Forms.TextBox();
            this.txtComissao = new System.Windows.Forms.TextBox();
            this.txtEstoque = new System.Windows.Forms.TextBox();
            this.lblCusto = new System.Windows.Forms.Label();
            this.tlpProdutos = new System.Windows.Forms.TableLayoutPanel();
            this.pcbCesto = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblNumCodBarras = new System.Windows.Forms.Label();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.lblCodBarras = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.tlpCupom = new System.Windows.Forms.TableLayoutPanel();
            this.listCupom = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblProdutoOferecer = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBeneficios = new System.Windows.Forms.Button();
            this.btnConsultaProdutos = new System.Windows.Forms.Button();
            this.btnUltimosProdutos = new System.Windows.Forms.Button();
            this.btnBuluario = new System.Windows.Forms.Button();
            this.BTN_ConsultaABCFarma = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnReimpressao = new System.Windows.Forms.Button();
            this.btnAtalhos = new System.Windows.Forms.Button();
            this.btnFVenda = new System.Windows.Forms.Button();
            this.tlpProduto = new System.Windows.Forms.TableLayoutPanel();
            this.txtBuscaPreco = new System.Windows.Forms.TextBox();
            this.lblQtde = new System.Windows.Forms.Label();
            this.chkEntrega = new System.Windows.Forms.CheckBox();
            this.lblAjuste = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.lblProduto = new System.Windows.Forms.Label();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAjuste = new System.Windows.Forms.TextBox();
            this.tlpAjuste = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtGAjuste = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVendedor = new System.Windows.Forms.TextBox();
            this.imgStatus = new System.Windows.Forms.PictureBox();
            this.imgTipoTela = new System.Windows.Forms.PictureBox();
            this.cmbVendedor = new System.Windows.Forms.ComboBox();
            this.txtComanda = new System.Windows.Forms.TextBox();
            this.lblComanda = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlMsg = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.pnlCliente.SuspendLayout();
            this.tlpDiversos.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tlpDadosProdutos.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tlpProdutos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCesto)).BeginInit();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tlpCupom.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tlpProduto.SuspendLayout();
            this.tlpAjuste.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTipoTela)).BeginInit();
            this.pnlMsg.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pnlCliente, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tlpDiversos, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tlpProduto, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tlpAjuste, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(964, 610);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pnlCliente
            // 
            this.pnlCliente.BackColor = System.Drawing.Color.White;
            this.pnlCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCliente.Controls.Add(this.lblConveniada);
            this.pnlCliente.Controls.Add(this.txtTelefone);
            this.pnlCliente.Controls.Add(this.lblTelefone);
            this.pnlCliente.Controls.Add(this.btnPreAutorizar);
            this.pnlCliente.Controls.Add(this.txtCliEmp);
            this.pnlCliente.Controls.Add(this.txtCodCliEmp);
            this.pnlCliente.Controls.Add(this.lblCliEmp);
            this.pnlCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCliente.Location = new System.Drawing.Point(3, 49);
            this.pnlCliente.Name = "pnlCliente";
            this.pnlCliente.Size = new System.Drawing.Size(958, 48);
            this.pnlCliente.TabIndex = 2;
            this.pnlCliente.Visible = false;
            // 
            // lblConveniada
            // 
            this.lblConveniada.AutoSize = true;
            this.lblConveniada.Font = new System.Drawing.Font("Arial", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConveniada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblConveniada.Location = new System.Drawing.Point(811, 10);
            this.lblConveniada.Name = "lblConveniada";
            this.lblConveniada.Size = new System.Drawing.Size(0, 27);
            this.lblConveniada.TabIndex = 27;
            // 
            // txtTelefone
            // 
            this.txtTelefone.BackColor = System.Drawing.Color.White;
            this.txtTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefone.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.Location = new System.Drawing.Point(674, 7);
            this.txtTelefone.MaxLength = 50;
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(134, 32);
            this.txtTelefone.TabIndex = 26;
            this.txtTelefone.Visible = false;
            this.txtTelefone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTelefone_KeyDown);
            this.txtTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone_KeyPress);
            // 
            // lblTelefone
            // 
            this.lblTelefone.BackColor = System.Drawing.Color.Transparent;
            this.lblTelefone.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblTelefone.Location = new System.Drawing.Point(633, 7);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(46, 28);
            this.lblTelefone.TabIndex = 25;
            this.lblTelefone.Text = "Tel.";
            this.lblTelefone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTelefone.Visible = false;
            // 
            // btnPreAutorizar
            // 
            this.btnPreAutorizar.BackColor = System.Drawing.Color.Transparent;
            this.btnPreAutorizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPreAutorizar.BackgroundImage")));
            this.btnPreAutorizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPreAutorizar.FlatAppearance.BorderSize = 0;
            this.btnPreAutorizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreAutorizar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreAutorizar.ForeColor = System.Drawing.Color.Navy;
            this.btnPreAutorizar.Image = ((System.Drawing.Image)(resources.GetObject("btnPreAutorizar.Image")));
            this.btnPreAutorizar.Location = new System.Drawing.Point(904, 3);
            this.btnPreAutorizar.Name = "btnPreAutorizar";
            this.btnPreAutorizar.Size = new System.Drawing.Size(47, 40);
            this.btnPreAutorizar.TabIndex = 24;
            this.toolTip1.SetToolTip(this.btnPreAutorizar, "Pré-Autorização");
            this.btnPreAutorizar.UseVisualStyleBackColor = false;
            this.btnPreAutorizar.Visible = false;
            this.btnPreAutorizar.Click += new System.EventHandler(this.btnPreAutorizar_Click);
            this.btnPreAutorizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPreAutorizar_KeyDown);
            this.btnPreAutorizar.MouseHover += new System.EventHandler(this.btnPreAutorizar_MouseHover);
            // 
            // txtCliEmp
            // 
            this.txtCliEmp.BackColor = System.Drawing.Color.White;
            this.txtCliEmp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCliEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCliEmp.Enabled = false;
            this.txtCliEmp.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCliEmp.ForeColor = System.Drawing.Color.Black;
            this.txtCliEmp.Location = new System.Drawing.Point(225, 7);
            this.txtCliEmp.MaxLength = 50;
            this.txtCliEmp.Name = "txtCliEmp";
            this.txtCliEmp.Size = new System.Drawing.Size(405, 32);
            this.txtCliEmp.TabIndex = 23;
            this.txtCliEmp.Visible = false;
            // 
            // txtCodCliEmp
            // 
            this.txtCodCliEmp.BackColor = System.Drawing.Color.White;
            this.txtCodCliEmp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodCliEmp.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodCliEmp.ForeColor = System.Drawing.Color.Black;
            this.txtCodCliEmp.Location = new System.Drawing.Point(138, 7);
            this.txtCodCliEmp.MaxLength = 50;
            this.txtCodCliEmp.Name = "txtCodCliEmp";
            this.txtCodCliEmp.Size = new System.Drawing.Size(77, 32);
            this.txtCodCliEmp.TabIndex = 21;
            this.txtCodCliEmp.Visible = false;
            this.txtCodCliEmp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodCliEmp_KeyDown);
            this.txtCodCliEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliEmp_KeyPress);
            // 
            // lblCliEmp
            // 
            this.lblCliEmp.BackColor = System.Drawing.Color.Transparent;
            this.lblCliEmp.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliEmp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblCliEmp.Location = new System.Drawing.Point(-2, 7);
            this.lblCliEmp.Name = "lblCliEmp";
            this.lblCliEmp.Size = new System.Drawing.Size(137, 28);
            this.lblCliEmp.TabIndex = 22;
            this.lblCliEmp.Text = "Cliente/Emp.";
            this.lblCliEmp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCliEmp.Visible = false;
            // 
            // tlpDiversos
            // 
            this.tlpDiversos.ColumnCount = 2;
            this.tlpDiversos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.20251F));
            this.tlpDiversos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.79749F));
            this.tlpDiversos.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tlpDiversos.Controls.Add(this.tlpCupom, 0, 0);
            this.tlpDiversos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDiversos.Location = new System.Drawing.Point(3, 153);
            this.tlpDiversos.Name = "tlpDiversos";
            this.tlpDiversos.RowCount = 1;
            this.tlpDiversos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDiversos.Size = new System.Drawing.Size(958, 314);
            this.tlpDiversos.TabIndex = 0;
            this.tlpDiversos.Visible = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.tlpDadosProdutos, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel13, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(551, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(404, 308);
            this.tableLayoutPanel9.TabIndex = 47;
            // 
            // tlpDadosProdutos
            // 
            this.tlpDadosProdutos.BackColor = System.Drawing.Color.White;
            this.tlpDadosProdutos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlpDadosProdutos.ColumnCount = 1;
            this.tlpDadosProdutos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDadosProdutos.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tlpDadosProdutos.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tlpDadosProdutos.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tlpDadosProdutos.Controls.Add(this.tlpProdutos, 0, 0);
            this.tlpDadosProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDadosProdutos.Location = new System.Drawing.Point(3, 3);
            this.tlpDadosProdutos.Name = "tlpDadosProdutos";
            this.tlpDadosProdutos.RowCount = 4;
            this.tlpDadosProdutos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDadosProdutos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tlpDadosProdutos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tlpDadosProdutos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tlpDadosProdutos.Size = new System.Drawing.Size(398, 252);
            this.tlpDadosProdutos.TabIndex = 2;
            this.tlpDadosProdutos.Visible = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel10.ColumnCount = 5;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.Controls.Add(this.txtVUni, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtPQtde, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtVDesc, 3, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtPDesc, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtSTotal, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.label11, 4, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 74);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.59322F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.40678F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(392, 55);
            this.tableLayoutPanel10.TabIndex = 4;
            // 
            // txtVUni
            // 
            this.txtVUni.BackColor = System.Drawing.Color.White;
            this.txtVUni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVUni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVUni.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVUni.Location = new System.Drawing.Point(3, 22);
            this.txtVUni.Name = "txtVUni";
            this.txtVUni.ReadOnly = true;
            this.txtVUni.Size = new System.Drawing.Size(72, 32);
            this.txtVUni.TabIndex = 51;
            this.txtVUni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVUni.Visible = false;
            this.txtVUni.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVUni_KeyDown);
            this.txtVUni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVUni_KeyPress);
            this.txtVUni.Validated += new System.EventHandler(this.txtVUni_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label6.Location = new System.Drawing.Point(159, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 19);
            this.label6.TabIndex = 50;
            this.label6.Text = "Subtotal";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label7.Location = new System.Drawing.Point(81, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 19);
            this.label7.TabIndex = 49;
            this.label7.Text = "Qtde.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 19);
            this.label8.TabIndex = 48;
            this.label8.Text = "Vl. Unit.";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPQtde
            // 
            this.txtPQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPQtde.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPQtde.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPQtde.Location = new System.Drawing.Point(81, 22);
            this.txtPQtde.Name = "txtPQtde";
            this.txtPQtde.Size = new System.Drawing.Size(72, 32);
            this.txtPQtde.TabIndex = 46;
            this.txtPQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPQtde.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPQtde_KeyDown);
            this.txtPQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPQtde_KeyPress);
            this.txtPQtde.Validated += new System.EventHandler(this.txtPQtde_Validated);
            // 
            // txtVDesc
            // 
            this.txtVDesc.BackColor = System.Drawing.Color.White;
            this.txtVDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVDesc.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVDesc.Location = new System.Drawing.Point(315, 22);
            this.txtVDesc.Name = "txtVDesc";
            this.txtVDesc.ReadOnly = true;
            this.txtVDesc.Size = new System.Drawing.Size(74, 32);
            this.txtVDesc.TabIndex = 43;
            this.txtVDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVDesc.Visible = false;
            this.txtVDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVDesc_KeyDown);
            this.txtVDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVDesc_KeyPress);
            this.txtVDesc.Validated += new System.EventHandler(this.txtVDesc_Validated);
            // 
            // txtPDesc
            // 
            this.txtPDesc.BackColor = System.Drawing.Color.White;
            this.txtPDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPDesc.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPDesc.Location = new System.Drawing.Point(237, 22);
            this.txtPDesc.Name = "txtPDesc";
            this.txtPDesc.ReadOnly = true;
            this.txtPDesc.Size = new System.Drawing.Size(72, 32);
            this.txtPDesc.TabIndex = 44;
            this.txtPDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPDesc_KeyDown);
            this.txtPDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPDesc_KeyPress);
            this.txtPDesc.Validated += new System.EventHandler(this.txtPDesc_Validated);
            // 
            // txtSTotal
            // 
            this.txtSTotal.BackColor = System.Drawing.Color.White;
            this.txtSTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSTotal.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTotal.Location = new System.Drawing.Point(159, 22);
            this.txtSTotal.Name = "txtSTotal";
            this.txtSTotal.ReadOnly = true;
            this.txtSTotal.Size = new System.Drawing.Size(72, 32);
            this.txtSTotal.TabIndex = 45;
            this.txtSTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSTotal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSTotal_KeyDown);
            this.txtSTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSTotal_KeyPress);
            this.txtSTotal.Validated += new System.EventHandler(this.txtSTotal_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label9.Location = new System.Drawing.Point(237, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 19);
            this.label9.TabIndex = 47;
            this.label9.Text = "D %";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label11.Location = new System.Drawing.Point(315, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 19);
            this.label11.TabIndex = 52;
            this.label11.Text = "D $";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel4.Controls.Add(this.btnOk, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCancela, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnExcluir, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 203);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(392, 46);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
            this.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOk.FlatAppearance.BorderSize = 0;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
            this.btnOk.Location = new System.Drawing.Point(298, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(91, 40);
            this.btnOk.TabIndex = 41;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            this.btnOk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnOk_KeyDown);
            // 
            // btnCancela
            // 
            this.btnCancela.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancela.BackgroundImage")));
            this.btnCancela.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancela.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancela.FlatAppearance.BorderSize = 0;
            this.btnCancela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancela.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancela.Image = ((System.Drawing.Image)(resources.GetObject("btnCancela.Image")));
            this.btnCancela.Location = new System.Drawing.Point(201, 3);
            this.btnCancela.Name = "btnCancela";
            this.btnCancela.Size = new System.Drawing.Size(91, 40);
            this.btnCancela.TabIndex = 42;
            this.btnCancela.UseVisualStyleBackColor = true;
            this.btnCancela.Click += new System.EventHandler(this.btnCancela_Click);
            this.btnCancela.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancela_KeyDown);
            // 
            // btnExcluir
            // 
            this.btnExcluir.BackColor = System.Drawing.Color.Transparent;
            this.btnExcluir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExcluir.BackgroundImage")));
            this.btnExcluir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExcluir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExcluir.FlatAppearance.BorderSize = 0;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExcluir.ForeColor = System.Drawing.Color.Black;
            this.btnExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluir.Image")));
            this.btnExcluir.Location = new System.Drawing.Point(104, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(91, 40);
            this.btnExcluir.TabIndex = 43;
            this.btnExcluir.UseVisualStyleBackColor = false;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            this.btnExcluir.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnExcluir_KeyDown);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Controls.Add(this.lblComissao, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label4, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtVTotal, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtCusto, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtComissao, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtEstoque, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblCusto, 3, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 135);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.59322F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.40678F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(392, 62);
            this.tableLayoutPanel5.TabIndex = 1;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel5_Paint);
            // 
            // lblComissao
            // 
            this.lblComissao.AutoSize = true;
            this.lblComissao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblComissao.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComissao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblComissao.Location = new System.Drawing.Point(159, 0);
            this.lblComissao.Name = "lblComissao";
            this.lblComissao.Size = new System.Drawing.Size(72, 22);
            this.lblComissao.TabIndex = 50;
            this.lblComissao.Text = "DB";
            this.lblComissao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label4.Location = new System.Drawing.Point(81, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 22);
            this.label4.TabIndex = 49;
            this.label4.Text = "Estoque";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 22);
            this.label3.TabIndex = 48;
            this.label3.Text = "Vl. Total";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtVTotal
            // 
            this.txtVTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVTotal.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVTotal.Location = new System.Drawing.Point(3, 25);
            this.txtVTotal.Name = "txtVTotal";
            this.txtVTotal.Size = new System.Drawing.Size(72, 32);
            this.txtVTotal.TabIndex = 46;
            this.txtVTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVTotal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVTotal_KeyDown);
            this.txtVTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVTotal_KeyPress);
            this.txtVTotal.Validated += new System.EventHandler(this.txtVTotal_Validated);
            // 
            // txtCusto
            // 
            this.txtCusto.BackColor = System.Drawing.Color.White;
            this.txtCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCusto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCusto.Enabled = false;
            this.txtCusto.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCusto.Location = new System.Drawing.Point(237, 25);
            this.txtCusto.Name = "txtCusto";
            this.txtCusto.ReadOnly = true;
            this.txtCusto.Size = new System.Drawing.Size(72, 32);
            this.txtCusto.TabIndex = 43;
            this.txtCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCusto.Visible = false;
            this.txtCusto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCusto_KeyDown);
            // 
            // txtComissao
            // 
            this.txtComissao.BackColor = System.Drawing.Color.White;
            this.txtComissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComissao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComissao.Enabled = false;
            this.txtComissao.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComissao.Location = new System.Drawing.Point(159, 25);
            this.txtComissao.Name = "txtComissao";
            this.txtComissao.ReadOnly = true;
            this.txtComissao.Size = new System.Drawing.Size(72, 32);
            this.txtComissao.TabIndex = 44;
            this.txtComissao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComissao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtComissao_KeyDown);
            // 
            // txtEstoque
            // 
            this.txtEstoque.BackColor = System.Drawing.Color.White;
            this.txtEstoque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstoque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstoque.Enabled = false;
            this.txtEstoque.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstoque.Location = new System.Drawing.Point(81, 25);
            this.txtEstoque.Name = "txtEstoque";
            this.txtEstoque.ReadOnly = true;
            this.txtEstoque.Size = new System.Drawing.Size(72, 32);
            this.txtEstoque.TabIndex = 45;
            this.txtEstoque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEstoque.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEstoque_KeyDown);
            // 
            // lblCusto
            // 
            this.lblCusto.AutoSize = true;
            this.lblCusto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCusto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCusto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblCusto.Location = new System.Drawing.Point(237, 0);
            this.lblCusto.Name = "lblCusto";
            this.lblCusto.Size = new System.Drawing.Size(72, 22);
            this.lblCusto.TabIndex = 47;
            this.lblCusto.Text = "Custo";
            this.lblCusto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCusto.Visible = false;
            // 
            // tlpProdutos
            // 
            this.tlpProdutos.BackColor = System.Drawing.Color.Transparent;
            this.tlpProdutos.ColumnCount = 2;
            this.tlpProdutos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.30392F));
            this.tlpProdutos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.69608F));
            this.tlpProdutos.Controls.Add(this.pcbCesto, 0, 0);
            this.tlpProdutos.Controls.Add(this.tableLayoutPanel12, 1, 0);
            this.tlpProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpProdutos.Location = new System.Drawing.Point(3, 3);
            this.tlpProdutos.Name = "tlpProdutos";
            this.tlpProdutos.RowCount = 1;
            this.tlpProdutos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpProdutos.Size = new System.Drawing.Size(392, 65);
            this.tlpProdutos.TabIndex = 3;
            // 
            // pcbCesto
            // 
            this.pcbCesto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcbCesto.BackgroundImage")));
            this.pcbCesto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pcbCesto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbCesto.Location = new System.Drawing.Point(3, 3);
            this.pcbCesto.Name = "pcbCesto";
            this.pcbCesto.Size = new System.Drawing.Size(81, 59);
            this.pcbCesto.TabIndex = 0;
            this.pcbCesto.TabStop = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.lblNome, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblNumCodBarras, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblDescricao, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.lblCodBarras, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(90, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(299, 59);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNome.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblNome.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblNome.Location = new System.Drawing.Point(176, 29);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(120, 30);
            this.lblNome.TabIndex = 3;
            // 
            // lblNumCodBarras
            // 
            this.lblNumCodBarras.AutoSize = true;
            this.lblNumCodBarras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNumCodBarras.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumCodBarras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblNumCodBarras.Location = new System.Drawing.Point(3, 29);
            this.lblNumCodBarras.Name = "lblNumCodBarras";
            this.lblNumCodBarras.Size = new System.Drawing.Size(167, 30);
            this.lblNumCodBarras.TabIndex = 2;
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescricao.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.ForeColor = System.Drawing.Color.Black;
            this.lblDescricao.Location = new System.Drawing.Point(176, 0);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(120, 29);
            this.lblDescricao.TabIndex = 1;
            this.lblDescricao.Text = "Descrição";
            this.lblDescricao.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblCodBarras
            // 
            this.lblCodBarras.AutoSize = true;
            this.lblCodBarras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodBarras.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarras.ForeColor = System.Drawing.Color.Black;
            this.lblCodBarras.Location = new System.Drawing.Point(3, 0);
            this.lblCodBarras.Name = "lblCodBarras";
            this.lblCodBarras.Size = new System.Drawing.Size(167, 29);
            this.lblCodBarras.TabIndex = 0;
            this.lblCodBarras.Text = "Cód. de Barras";
            this.lblCodBarras.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel13.BackgroundImage")));
            this.tableLayoutPanel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Controls.Add(this.lblRegistros, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 261);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.81818F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(398, 44);
            this.tableLayoutPanel13.TabIndex = 3;
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRegistros.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistros.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblRegistros.Location = new System.Drawing.Point(3, 0);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(392, 44);
            this.lblRegistros.TabIndex = 0;
            this.lblRegistros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tlpCupom
            // 
            this.tlpCupom.BackColor = System.Drawing.Color.White;
            this.tlpCupom.ColumnCount = 1;
            this.tlpCupom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCupom.Controls.Add(this.listCupom, 0, 0);
            this.tlpCupom.Controls.Add(this.panel1, 0, 1);
            this.tlpCupom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCupom.Location = new System.Drawing.Point(3, 3);
            this.tlpCupom.Name = "tlpCupom";
            this.tlpCupom.RowCount = 2;
            this.tlpCupom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCupom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpCupom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 308F));
            this.tlpCupom.Size = new System.Drawing.Size(542, 308);
            this.tlpCupom.TabIndex = 48;
            this.tlpCupom.Visible = false;
            // 
            // listCupom
            // 
            this.listCupom.BackColor = System.Drawing.Color.Ivory;
            this.listCupom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCupom.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listCupom.ForeColor = System.Drawing.Color.Black;
            this.listCupom.FormattingEnabled = true;
            this.listCupom.ItemHeight = 18;
            this.listCupom.Location = new System.Drawing.Point(3, 3);
            this.listCupom.Name = "listCupom";
            this.listCupom.Size = new System.Drawing.Size(536, 268);
            this.listCupom.TabIndex = 47;
            this.listCupom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listCupom_KeyDown);
            this.listCupom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listCupom_KeyPress);
            this.listCupom.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listCupom_MouseDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblProdutoOferecer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(3, 277);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 28);
            this.panel1.TabIndex = 48;
            // 
            // lblProdutoOferecer
            // 
            this.lblProdutoOferecer.AutoSize = true;
            this.lblProdutoOferecer.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdutoOferecer.ForeColor = System.Drawing.Color.Black;
            this.lblProdutoOferecer.Location = new System.Drawing.Point(0, 0);
            this.lblProdutoOferecer.Name = "lblProdutoOferecer";
            this.lblProdutoOferecer.Size = new System.Drawing.Size(0, 22);
            this.lblProdutoOferecer.TabIndex = 0;
            this.lblProdutoOferecer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 9;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.37375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.01F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnBeneficios, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnConsultaProdutos, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnUltimosProdutos, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnBuluario, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.BTN_ConsultaABCFarma, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnLimpar, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnReimpressao, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnAtalhos, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnFVenda, 8, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 543);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(958, 64);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // btnBeneficios
            // 
            this.btnBeneficios.BackColor = System.Drawing.Color.Transparent;
            this.btnBeneficios.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBeneficios.BackgroundImage")));
            this.btnBeneficios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBeneficios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBeneficios.FlatAppearance.BorderSize = 0;
            this.btnBeneficios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBeneficios.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBeneficios.ForeColor = System.Drawing.Color.Black;
            this.btnBeneficios.Image = ((System.Drawing.Image)(resources.GetObject("btnBeneficios.Image")));
            this.btnBeneficios.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBeneficios.Location = new System.Drawing.Point(3, 3);
            this.btnBeneficios.Name = "btnBeneficios";
            this.btnBeneficios.Size = new System.Drawing.Size(83, 58);
            this.btnBeneficios.TabIndex = 0;
            this.btnBeneficios.Text = "Benefícios";
            this.btnBeneficios.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBeneficios.UseVisualStyleBackColor = false;
            this.btnBeneficios.Click += new System.EventHandler(this.btnBeneficios_Click);
            this.btnBeneficios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBeneficios_KeyDown);
            this.btnBeneficios.MouseHover += new System.EventHandler(this.btnBeneficios_MouseHover);
            // 
            // btnConsultaProdutos
            // 
            this.btnConsultaProdutos.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultaProdutos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConsultaProdutos.BackgroundImage")));
            this.btnConsultaProdutos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsultaProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnConsultaProdutos.FlatAppearance.BorderSize = 0;
            this.btnConsultaProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultaProdutos.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultaProdutos.ForeColor = System.Drawing.Color.Black;
            this.btnConsultaProdutos.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaProdutos.Image")));
            this.btnConsultaProdutos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnConsultaProdutos.Location = new System.Drawing.Point(92, 3);
            this.btnConsultaProdutos.Name = "btnConsultaProdutos";
            this.btnConsultaProdutos.Size = new System.Drawing.Size(83, 58);
            this.btnConsultaProdutos.TabIndex = 1;
            this.btnConsultaProdutos.Text = "Cons. Produtos";
            this.btnConsultaProdutos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConsultaProdutos.UseVisualStyleBackColor = false;
            this.btnConsultaProdutos.Click += new System.EventHandler(this.btnConsultaProdutos_Click);
            this.btnConsultaProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConsultaProdutos_KeyDown);
            this.btnConsultaProdutos.MouseHover += new System.EventHandler(this.btnConsultaProdutos_MouseHover);
            // 
            // btnUltimosProdutos
            // 
            this.btnUltimosProdutos.BackColor = System.Drawing.Color.Transparent;
            this.btnUltimosProdutos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUltimosProdutos.BackgroundImage")));
            this.btnUltimosProdutos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUltimosProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUltimosProdutos.Enabled = false;
            this.btnUltimosProdutos.FlatAppearance.BorderSize = 0;
            this.btnUltimosProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUltimosProdutos.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUltimosProdutos.ForeColor = System.Drawing.Color.Black;
            this.btnUltimosProdutos.Image = ((System.Drawing.Image)(resources.GetObject("btnUltimosProdutos.Image")));
            this.btnUltimosProdutos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnUltimosProdutos.Location = new System.Drawing.Point(181, 3);
            this.btnUltimosProdutos.Name = "btnUltimosProdutos";
            this.btnUltimosProdutos.Size = new System.Drawing.Size(83, 58);
            this.btnUltimosProdutos.TabIndex = 2;
            this.btnUltimosProdutos.Text = "Últimos Produtos";
            this.btnUltimosProdutos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnUltimosProdutos.UseVisualStyleBackColor = false;
            this.btnUltimosProdutos.Click += new System.EventHandler(this.btnUltimosProdutos_Click);
            this.btnUltimosProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnUltimosProdutos_KeyDown);
            this.btnUltimosProdutos.MouseHover += new System.EventHandler(this.btnUltimosProdutos_MouseHover);
            // 
            // btnBuluario
            // 
            this.btnBuluario.BackColor = System.Drawing.Color.Transparent;
            this.btnBuluario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBuluario.BackgroundImage")));
            this.btnBuluario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuluario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBuluario.FlatAppearance.BorderSize = 0;
            this.btnBuluario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuluario.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuluario.Image = ((System.Drawing.Image)(resources.GetObject("btnBuluario.Image")));
            this.btnBuluario.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBuluario.Location = new System.Drawing.Point(270, 3);
            this.btnBuluario.Name = "btnBuluario";
            this.btnBuluario.Size = new System.Drawing.Size(83, 58);
            this.btnBuluario.TabIndex = 3;
            this.btnBuluario.Text = "Bulário Eletrônico";
            this.btnBuluario.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBuluario.UseVisualStyleBackColor = false;
            this.btnBuluario.Click += new System.EventHandler(this.btnBuluario_Click);
            this.btnBuluario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBuluario_KeyDown);
            this.btnBuluario.MouseHover += new System.EventHandler(this.btnBuluario_MouseHover);
            // 
            // BTN_ConsultaABCFarma
            // 
            this.BTN_ConsultaABCFarma.BackColor = System.Drawing.Color.Transparent;
            this.BTN_ConsultaABCFarma.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BTN_ConsultaABCFarma.BackgroundImage")));
            this.BTN_ConsultaABCFarma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BTN_ConsultaABCFarma.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_ConsultaABCFarma.FlatAppearance.BorderSize = 0;
            this.BTN_ConsultaABCFarma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ConsultaABCFarma.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ConsultaABCFarma.Image = ((System.Drawing.Image)(resources.GetObject("BTN_ConsultaABCFarma.Image")));
            this.BTN_ConsultaABCFarma.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BTN_ConsultaABCFarma.Location = new System.Drawing.Point(359, 3);
            this.BTN_ConsultaABCFarma.Name = "BTN_ConsultaABCFarma";
            this.BTN_ConsultaABCFarma.Size = new System.Drawing.Size(83, 58);
            this.BTN_ConsultaABCFarma.TabIndex = 4;
            this.BTN_ConsultaABCFarma.Text = "Abrir Chamado";
            this.BTN_ConsultaABCFarma.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BTN_ConsultaABCFarma.UseVisualStyleBackColor = false;
            this.BTN_ConsultaABCFarma.Visible = false;
            this.BTN_ConsultaABCFarma.Click += new System.EventHandler(this.BTN_ConsultaABCFarma_Click);
            this.BTN_ConsultaABCFarma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BTN_ConsultaABCFarma_KeyDown);
            this.BTN_ConsultaABCFarma.MouseHover += new System.EventHandler(this.BTN_ConsultaABCFarma_MouseHover);
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.BackgroundImage")));
            this.btnLimpar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLimpar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLimpar.FlatAppearance.BorderSize = 0;
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.ForeColor = System.Drawing.Color.Black;
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLimpar.Location = new System.Drawing.Point(448, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(83, 58);
            this.btnLimpar.TabIndex = 5;
            this.btnLimpar.Text = "Limpar Venda";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            this.btnLimpar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnLimpar_KeyDown);
            this.btnLimpar.MouseHover += new System.EventHandler(this.btnLimpar_MouseHover);
            // 
            // btnReimpressao
            // 
            this.btnReimpressao.BackColor = System.Drawing.Color.Transparent;
            this.btnReimpressao.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReimpressao.BackgroundImage")));
            this.btnReimpressao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReimpressao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReimpressao.FlatAppearance.BorderSize = 0;
            this.btnReimpressao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReimpressao.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimpressao.ForeColor = System.Drawing.Color.Black;
            this.btnReimpressao.Image = ((System.Drawing.Image)(resources.GetObject("btnReimpressao.Image")));
            this.btnReimpressao.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReimpressao.Location = new System.Drawing.Point(537, 3);
            this.btnReimpressao.Name = "btnReimpressao";
            this.btnReimpressao.Size = new System.Drawing.Size(83, 58);
            this.btnReimpressao.TabIndex = 6;
            this.btnReimpressao.Text = "Reimpressão";
            this.btnReimpressao.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReimpressao.UseVisualStyleBackColor = false;
            this.btnReimpressao.Click += new System.EventHandler(this.btnReimpressao_Click);
            this.btnReimpressao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnReimpressao_KeyDown);
            this.btnReimpressao.MouseHover += new System.EventHandler(this.btnReimpressao_MouseHover);
            // 
            // btnAtalhos
            // 
            this.btnAtalhos.BackColor = System.Drawing.Color.Transparent;
            this.btnAtalhos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAtalhos.BackgroundImage")));
            this.btnAtalhos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAtalhos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAtalhos.FlatAppearance.BorderSize = 0;
            this.btnAtalhos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtalhos.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtalhos.ForeColor = System.Drawing.Color.Black;
            this.btnAtalhos.Image = ((System.Drawing.Image)(resources.GetObject("btnAtalhos.Image")));
            this.btnAtalhos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAtalhos.Location = new System.Drawing.Point(626, 3);
            this.btnAtalhos.Name = "btnAtalhos";
            this.btnAtalhos.Size = new System.Drawing.Size(83, 58);
            this.btnAtalhos.TabIndex = 7;
            this.btnAtalhos.Text = "Atalhos";
            this.btnAtalhos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAtalhos.UseVisualStyleBackColor = false;
            this.btnAtalhos.Click += new System.EventHandler(this.btnCancelar_Click);
            this.btnAtalhos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancelar_KeyDown);
            this.btnAtalhos.MouseHover += new System.EventHandler(this.btnCancelar_MouseHover);
            // 
            // btnFVenda
            // 
            this.btnFVenda.BackColor = System.Drawing.Color.Transparent;
            this.btnFVenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFVenda.BackgroundImage")));
            this.btnFVenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFVenda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFVenda.FlatAppearance.BorderSize = 0;
            this.btnFVenda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFVenda.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFVenda.ForeColor = System.Drawing.Color.Black;
            this.btnFVenda.Image = ((System.Drawing.Image)(resources.GetObject("btnFVenda.Image")));
            this.btnFVenda.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFVenda.Location = new System.Drawing.Point(715, 3);
            this.btnFVenda.Name = "btnFVenda";
            this.btnFVenda.Size = new System.Drawing.Size(240, 58);
            this.btnFVenda.TabIndex = 8;
            this.btnFVenda.Text = "Finalizar Venda";
            this.btnFVenda.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFVenda.UseVisualStyleBackColor = false;
            this.btnFVenda.Click += new System.EventHandler(this.btnFVenda_Click);
            this.btnFVenda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFVenda_KeyDown);
            this.btnFVenda.MouseHover += new System.EventHandler(this.btnFVenda_MouseHover);
            // 
            // tlpProduto
            // 
            this.tlpProduto.BackColor = System.Drawing.Color.Transparent;
            this.tlpProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tlpProduto.BackgroundImage")));
            this.tlpProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlpProduto.ColumnCount = 9;
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tlpProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tlpProduto.Controls.Add(this.txtBuscaPreco, 5, 0);
            this.tlpProduto.Controls.Add(this.lblQtde, 0, 0);
            this.tlpProduto.Controls.Add(this.chkEntrega, 8, 0);
            this.tlpProduto.Controls.Add(this.lblAjuste, 4, 0);
            this.tlpProduto.Controls.Add(this.txtProduto, 3, 0);
            this.tlpProduto.Controls.Add(this.lblProduto, 2, 0);
            this.tlpProduto.Controls.Add(this.txtQtde, 1, 0);
            this.tlpProduto.Controls.Add(this.label12, 6, 0);
            this.tlpProduto.Controls.Add(this.txtAjuste, 7, 0);
            this.tlpProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpProduto.Location = new System.Drawing.Point(3, 103);
            this.tlpProduto.Name = "tlpProduto";
            this.tlpProduto.RowCount = 1;
            this.tlpProduto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpProduto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tlpProduto.Size = new System.Drawing.Size(958, 44);
            this.tlpProduto.TabIndex = 6;
            this.tlpProduto.Visible = false;
            // 
            // txtBuscaPreco
            // 
            this.txtBuscaPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBuscaPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBuscaPreco.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscaPreco.Location = new System.Drawing.Point(639, 3);
            this.txtBuscaPreco.MaxLength = 18;
            this.txtBuscaPreco.Name = "txtBuscaPreco";
            this.txtBuscaPreco.Size = new System.Drawing.Size(78, 35);
            this.txtBuscaPreco.TabIndex = 52;
            this.txtBuscaPreco.Text = "0,00";
            this.txtBuscaPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBuscaPreco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBuscaPreco_KeyDown);
            this.txtBuscaPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscaPreco_KeyPress);
            // 
            // lblQtde
            // 
            this.lblQtde.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblQtde.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtde.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblQtde.Image = ((System.Drawing.Image)(resources.GetObject("lblQtde.Image")));
            this.lblQtde.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblQtde.Location = new System.Drawing.Point(3, 0);
            this.lblQtde.Name = "lblQtde";
            this.lblQtde.Size = new System.Drawing.Size(94, 44);
            this.lblQtde.TabIndex = 54;
            this.lblQtde.Text = "Qtde.";
            this.lblQtde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkEntrega
            // 
            this.chkEntrega.AutoSize = true;
            this.chkEntrega.BackColor = System.Drawing.Color.Transparent;
            this.chkEntrega.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkEntrega.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEntrega.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.chkEntrega.Location = new System.Drawing.Point(853, 3);
            this.chkEntrega.Name = "chkEntrega";
            this.chkEntrega.Size = new System.Drawing.Size(102, 38);
            this.chkEntrega.TabIndex = 51;
            this.chkEntrega.Text = "Entrega";
            this.chkEntrega.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkEntrega.UseVisualStyleBackColor = false;
            this.chkEntrega.CheckedChanged += new System.EventHandler(this.chkEntrega_CheckedChanged);
            this.chkEntrega.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkEntrega_KeyDown);
            this.chkEntrega.MouseHover += new System.EventHandler(this.chkEntrega_MouseHover);
            // 
            // lblAjuste
            // 
            this.lblAjuste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAjuste.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAjuste.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblAjuste.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAjuste.Location = new System.Drawing.Point(563, 0);
            this.lblAjuste.Name = "lblAjuste";
            this.lblAjuste.Size = new System.Drawing.Size(70, 44);
            this.lblAjuste.TabIndex = 50;
            this.lblAjuste.Text = "Preço";
            this.lblAjuste.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtProduto
            // 
            this.txtProduto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProduto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtProduto.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProduto.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProduto.Location = new System.Drawing.Point(273, 3);
            this.txtProduto.MaxLength = 50;
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(284, 35);
            this.txtProduto.TabIndex = 51;
            this.txtProduto.WordWrap = false;
            this.txtProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProduto_KeyDown);
            this.txtProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProduto_KeyPress);
            // 
            // lblProduto
            // 
            this.lblProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProduto.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblProduto.Image = ((System.Drawing.Image)(resources.GetObject("lblProduto.Image")));
            this.lblProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblProduto.Location = new System.Drawing.Point(143, 0);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(124, 44);
            this.lblProduto.TabIndex = 52;
            this.lblProduto.Text = "Produto";
            this.lblProduto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtQtde
            // 
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(103, 3);
            this.txtQtde.MaxLength = 18;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(34, 35);
            this.txtQtde.TabIndex = 53;
            this.txtQtde.Text = "1";
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQtde_KeyDown);
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            this.txtQtde.MouseHover += new System.EventHandler(this.txtQtde_MouseHover);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label12.Image = ((System.Drawing.Image)(resources.GetObject("label12.Image")));
            this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Location = new System.Drawing.Point(723, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 44);
            this.label12.TabIndex = 56;
            this.label12.Text = "%";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAjuste
            // 
            this.txtAjuste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAjuste.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.txtAjuste.Location = new System.Drawing.Point(786, 3);
            this.txtAjuste.Name = "txtAjuste";
            this.txtAjuste.Size = new System.Drawing.Size(61, 35);
            this.txtAjuste.TabIndex = 57;
            this.txtAjuste.Text = "0,00";
            this.txtAjuste.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAjuste.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAjuste_KeyDown);
            this.txtAjuste.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAjuste_KeyPress);
            this.txtAjuste.Validated += new System.EventHandler(this.txtAjuste_Validated);
            // 
            // tlpAjuste
            // 
            this.tlpAjuste.BackColor = System.Drawing.Color.Transparent;
            this.tlpAjuste.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tlpAjuste.BackgroundImage")));
            this.tlpAjuste.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlpAjuste.ColumnCount = 4;
            this.tlpAjuste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpAjuste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpAjuste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpAjuste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpAjuste.Controls.Add(this.tableLayoutPanel11, 3, 0);
            this.tlpAjuste.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tlpAjuste.Controls.Add(this.tableLayoutPanel6, 2, 0);
            this.tlpAjuste.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tlpAjuste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAjuste.Location = new System.Drawing.Point(3, 473);
            this.tlpAjuste.Name = "tlpAjuste";
            this.tlpAjuste.RowCount = 1;
            this.tlpAjuste.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAjuste.Size = new System.Drawing.Size(958, 64);
            this.tlpAjuste.TabIndex = 7;
            this.tlpAjuste.Visible = false;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.53191F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.46809F));
            this.tableLayoutPanel11.Controls.Add(this.lblTotal, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(720, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(235, 58);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotal.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.White;
            this.lblTotal.Location = new System.Drawing.Point(109, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(123, 58);
            this.lblTotal.TabIndex = 4;
            this.lblTotal.Text = "0,00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTotal.Click += new System.EventHandler(this.lblTotal_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 58);
            this.label14.TabIndex = 6;
            this.label14.Text = "Total:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.21888F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.78112F));
            this.tableLayoutPanel8.Controls.Add(this.lblSubTotal, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(233, 58);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSubTotal.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.ForeColor = System.Drawing.Color.Black;
            this.lblSubTotal.Location = new System.Drawing.Point(126, 0);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(104, 58);
            this.lblSubTotal.TabIndex = 3;
            this.lblSubTotal.Text = "0,00";
            this.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 58);
            this.label5.TabIndex = 4;
            this.label5.Text = "Subtotal:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.txtGAjuste, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(481, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(233, 58);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // txtGAjuste
            // 
            this.txtGAjuste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGAjuste.BackColor = System.Drawing.Color.White;
            this.txtGAjuste.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGAjuste.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGAjuste.ForeColor = System.Drawing.Color.Black;
            this.txtGAjuste.Location = new System.Drawing.Point(119, 12);
            this.txtGAjuste.Name = "txtGAjuste";
            this.txtGAjuste.Size = new System.Drawing.Size(111, 34);
            this.txtGAjuste.TabIndex = 7;
            this.txtGAjuste.Text = "0,00";
            this.txtGAjuste.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGAjuste.TextChanged += new System.EventHandler(this.txtGAjuste_TextChanged);
            this.txtGAjuste.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGAjuste_KeyDown);
            this.txtGAjuste.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGAjuste_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 58);
            this.label13.TabIndex = 8;
            this.label13.Text = "Ajuste:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.3691F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.6309F));
            this.tableLayoutPanel7.Controls.Add(this.lblDesconto, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(242, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(233, 58);
            this.tableLayoutPanel7.TabIndex = 5;
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.BackColor = System.Drawing.Color.Transparent;
            this.lblDesconto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesconto.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesconto.ForeColor = System.Drawing.Color.Red;
            this.lblDesconto.Location = new System.Drawing.Point(138, 0);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(92, 58);
            this.lblDesconto.TabIndex = 1;
            this.lblDesconto.Text = "0,00";
            this.lblDesconto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 58);
            this.label10.TabIndex = 2;
            this.label10.Text = "Desconto:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel2.BackgroundImage")));
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtVendedor, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.imgStatus, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.imgTipoTela, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmbVendedor, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtComanda, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblComanda, 3, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(958, 40);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 40);
            this.label1.TabIndex = 10;
            this.label1.Text = "Vendedor";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVendedor
            // 
            this.txtVendedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendedor.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendedor.Location = new System.Drawing.Point(144, 3);
            this.txtVendedor.MaxLength = 18;
            this.txtVendedor.Name = "txtVendedor";
            this.txtVendedor.Size = new System.Drawing.Size(46, 32);
            this.txtVendedor.TabIndex = 8;
            this.txtVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVendedor_KeyDown);
            this.txtVendedor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVendedor_KeyPress);
            this.txtVendedor.Validated += new System.EventHandler(this.txtVendedor_Validated);
            // 
            // imgStatus
            // 
            this.imgStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.imgStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgStatus.Location = new System.Drawing.Point(905, 3);
            this.imgStatus.Name = "imgStatus";
            this.imgStatus.Size = new System.Drawing.Size(50, 34);
            this.imgStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgStatus.TabIndex = 14;
            this.imgStatus.TabStop = false;
            this.imgStatus.Visible = false;
            // 
            // imgTipoTela
            // 
            this.imgTipoTela.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgTipoTela.Location = new System.Drawing.Point(838, 3);
            this.imgTipoTela.Name = "imgTipoTela";
            this.imgTipoTela.Size = new System.Drawing.Size(61, 34);
            this.imgTipoTela.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgTipoTela.TabIndex = 15;
            this.imgTipoTela.TabStop = false;
            // 
            // cmbVendedor
            // 
            this.cmbVendedor.BackColor = System.Drawing.Color.White;
            this.cmbVendedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVendedor.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVendedor.ForeColor = System.Drawing.Color.Black;
            this.cmbVendedor.FormattingEnabled = true;
            this.cmbVendedor.Location = new System.Drawing.Point(196, 3);
            this.cmbVendedor.Name = "cmbVendedor";
            this.cmbVendedor.Size = new System.Drawing.Size(176, 32);
            this.cmbVendedor.TabIndex = 16;
            this.cmbVendedor.SelectedIndexChanged += new System.EventHandler(this.cmbVendedor_SelectedIndexChanged_1);
            this.cmbVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVendedor_KeyDown_1);
            this.cmbVendedor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbVendedor_KeyPress_1);
            // 
            // txtComanda
            // 
            this.txtComanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComanda.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComanda.Location = new System.Drawing.Point(491, 3);
            this.txtComanda.MaxLength = 18;
            this.txtComanda.Name = "txtComanda";
            this.txtComanda.Size = new System.Drawing.Size(66, 32);
            this.txtComanda.TabIndex = 9;
            this.txtComanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComanda.Visible = false;
            this.txtComanda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtComanda_KeyDown);
            this.txtComanda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComanda_KeyPress);
            // 
            // lblComanda
            // 
            this.lblComanda.AutoSize = true;
            this.lblComanda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblComanda.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComanda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblComanda.Location = new System.Drawing.Point(378, 0);
            this.lblComanda.Name = "lblComanda";
            this.lblComanda.Size = new System.Drawing.Size(107, 40);
            this.lblComanda.TabIndex = 17;
            this.lblComanda.Text = "Comanda";
            this.lblComanda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlMsg
            // 
            this.pnlMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(202)))), ((int)(((byte)(226)))));
            this.pnlMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMsg.Controls.Add(this.panel4);
            this.pnlMsg.Location = new System.Drawing.Point(279, 278);
            this.pnlMsg.Name = "pnlMsg";
            this.pnlMsg.Size = new System.Drawing.Size(407, 58);
            this.pnlMsg.TabIndex = 50;
            this.pnlMsg.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lblMsg);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(399, 49);
            this.panel4.TabIndex = 0;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(51)))), ((int)(((byte)(77)))));
            this.lblMsg.Location = new System.Drawing.Point(-1, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(399, 47);
            this.lblMsg.TabIndex = 0;
            this.lblMsg.Text = "mensagem ";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(964, 610);
            this.Controls.Add(this.pnlMsg);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenda";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVenda_FormClosing);
            this.Load += new System.EventHandler(this.frmVenda_Load);
            this.Shown += new System.EventHandler(this.frmVenda_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pnlCliente.ResumeLayout(false);
            this.pnlCliente.PerformLayout();
            this.tlpDiversos.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tlpDadosProdutos.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tlpProdutos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbCesto)).EndInit();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tlpCupom.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tlpProduto.ResumeLayout(false);
            this.tlpProduto.PerformLayout();
            this.tlpAjuste.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTipoTela)).EndInit();
            this.pnlMsg.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pnlCliente;
        private System.Windows.Forms.TableLayoutPanel tlpDiversos;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtVendedor;
        public System.Windows.Forms.TextBox txtComanda;
        public System.Windows.Forms.Button btnPreAutorizar;
        public System.Windows.Forms.TextBox txtCliEmp;
        public System.Windows.Forms.TextBox txtCodCliEmp;
        public System.Windows.Forms.Label lblCliEmp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tlpProduto;
        private System.Windows.Forms.Label lblAjuste;
        public System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.Label lblQtde;
        private System.Windows.Forms.Button btnBeneficios;
        private System.Windows.Forms.Button btnConsultaProdutos;
        private System.Windows.Forms.Button btnBuluario;
        private System.Windows.Forms.Button BTN_ConsultaABCFarma;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnReimpressao;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel pnlMsg;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox imgStatus;
        private System.Windows.Forms.TableLayoutPanel tlpAjuste;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lblSubTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox txtGAjuste;
        private System.Windows.Forms.PictureBox imgTipoTela;
        public System.Windows.Forms.Label lblTotal;
        public System.Windows.Forms.TextBox txtTelefone;
        public System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lblDesconto;
        public System.Windows.Forms.CheckBox chkEntrega;
        public System.Windows.Forms.Button btnUltimosProdutos;
        private System.Windows.Forms.Label lblConveniada;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.TextBox txtBuscaPreco;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtAjuste;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tlpDadosProdutos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public System.Windows.Forms.TextBox txtVUni;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtPQtde;
        public System.Windows.Forms.TextBox txtVDesc;
        public System.Windows.Forms.TextBox txtPDesc;
        public System.Windows.Forms.TextBox txtSTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button btnOk;
        public System.Windows.Forms.Button btnCancela;
        public System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblComissao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtVTotal;
        public System.Windows.Forms.TextBox txtCusto;
        public System.Windows.Forms.TextBox txtComissao;
        public System.Windows.Forms.TextBox txtEstoque;
        private System.Windows.Forms.Label lblCusto;
        private System.Windows.Forms.TableLayoutPanel tlpProdutos;
        private System.Windows.Forms.PictureBox pcbCesto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblNumCodBarras;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.Label lblCodBarras;
        private System.Windows.Forms.TableLayoutPanel tlpCupom;
        public System.Windows.Forms.ListBox listCupom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblProdutoOferecer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.Button btnAtalhos;
        private System.Windows.Forms.Button btnFVenda;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbVendedor;
        private System.Windows.Forms.Label lblComanda;
    }
}