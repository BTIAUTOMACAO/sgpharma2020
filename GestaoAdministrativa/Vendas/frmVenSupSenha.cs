﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenSupSenha : Form
    {
        public bool sValida;
        private int senhaOrigem;

        public frmVenSupSenha(int origem)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            this.senhaOrigem = origem;
        }

        private void frmVenSupSenha_Load(object sender, EventArgs e)
        {
            try
            {
                sValida = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Senha Sup.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVenSupSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                sValida = false;
                this.Close();
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnOK.PerformClick();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtSenha.Text.Trim()))
                {
                    MessageBox.Show("Senha não pode ser em branco", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSenha.Focus();
                }
                else if (senhaOrigem.Equals(0))
                {
                    if (txtSenha.Text.Trim().Equals(Funcoes.LeParametro(6, "77", false)))
                    {
                        sValida = true;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Senha Inválida, tente novamente!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSenha.Text = "";
                        txtSenha.Focus();
                    }
                }
                else if (senhaOrigem.Equals(1))
                {
                    if (txtSenha.Text.Trim().Equals("alleb@bti"))
                    {
                        sValida = true;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Senha Inválida, tente novamente!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSenha.Text = "";
                        txtSenha.Focus();
                    }
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Senha Sup.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                sValida = false;
                this.Close();
            }
        }

        private void btnOK_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                sValida = false;
                this.Close();
            }
        }
    }
}
