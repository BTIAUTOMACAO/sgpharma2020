﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenNotaPaulista : Form
    {
        public frmVenNotaPaulista()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenNotaPaulista_Load(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtDocto.Text)))
                {
                    txtDocto.Focus();
                }
                else if (String.IsNullOrEmpty(txtNome.Text.Trim()))
                {
                    txtNome.Focus();
                }
                else
                {
                    Principal.NumCpfCnpj = txtDocto.Text;
                    Principal.NomeNP = txtNome.Text;
                    txtDocto.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Nota Paulista", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbCpf_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbCpf.Checked.Equals(true))
            {
                txtDocto.Mask = "000,000,000-00";
                txtDocto.Focus();
            }
        }

        private void rdbCnpj_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbCnpj.Checked.Equals(true))
            {
                txtDocto.Mask = "00,000,000/0000-00";
                txtDocto.Focus();
            }
        }

        private void txtDocto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void rdbCpf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void rdbCnpj_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.PerformClick();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Principal.NumCpfCnpj = txtDocto.Text;
            Principal.NomeNP = txtNome.Text;
            this.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
    }
}
