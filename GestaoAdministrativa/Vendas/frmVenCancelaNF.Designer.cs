﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenCancelaNF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenCancelaNF));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.txtPInicial = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgCancelar = new System.Windows.Forms.DataGridView();
            this.CANCELA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_EMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_NUM_NOTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_BENEFICIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_DATA_HORA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOVIMENTO_FINANCEIRO_SEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TRANS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_TRANS_ID_RECEITA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_COL_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_SUBTOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_DIFERENCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_AUTORIZACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_NOME_CARTAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_MEDICO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_NUM_RECEITA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_AUTORIZACAO_RECEITA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDA_CARTAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelar)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(6, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(963, 488);
            this.panel1.TabIndex = 0;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(752, 384);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(202, 87);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "    Confirma o       Cancelamento  (F1)";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            this.btnCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancelar_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnFiltrar);
            this.groupBox3.Controls.Add(this.txtPInicial);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.dtData);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 378);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(319, 93);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtro";
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ForeColor = System.Drawing.Color.Black;
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFiltrar.Location = new System.Drawing.Point(235, 21);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(75, 61);
            this.btnFiltrar.TabIndex = 6;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            this.btnFiltrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltrar_KeyDown);
            // 
            // txtPInicial
            // 
            this.txtPInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPInicial.Location = new System.Drawing.Point(118, 60);
            this.txtPInicial.Name = "txtPInicial";
            this.txtPInicial.Size = new System.Drawing.Size(102, 22);
            this.txtPInicial.TabIndex = 3;
            this.txtPInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPInicial_KeyDown);
            this.txtPInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPInicial_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(47, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Venda ID";
            // 
            // dtData
            // 
            this.dtData.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(118, 21);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(102, 22);
            this.dtData.TabIndex = 1;
            this.dtData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtData_KeyDown);
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data da Venda";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgCancelar);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(948, 376);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vendas";
            // 
            // dgCancelar
            // 
            this.dgCancelar.AllowUserToAddRows = false;
            this.dgCancelar.AllowUserToDeleteRows = false;
            this.dgCancelar.AllowUserToResizeColumns = false;
            this.dgCancelar.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCancelar.BackgroundColor = System.Drawing.Color.White;
            this.dgCancelar.CausesValidation = false;
            this.dgCancelar.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgCancelar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgCancelar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CANCELA,
            this.EMP_CODIGO,
            this.EST_CODIGO,
            this.VENDA_ID,
            this.CF_NOME,
            this.VENDA_EMISSAO,
            this.VENDA_TOTAL,
            this.VENDA_NUM_NOTA,
            this.OP_CADASTRO,
            this.VENDA_BENEFICIO,
            this.VENDA_DATA_HORA,
            this.MOVIMENTO_FINANCEIRO_SEQ,
            this.VENDA_TRANS_ID,
            this.VENDA_TRANS_ID_RECEITA,
            this.VENDA_COL_CODIGO,
            this.VENDA_SUBTOTAL,
            this.VENDA_DIFERENCA,
            this.VENDA_AUTORIZACAO,
            this.VENDA_NOME_CARTAO,
            this.VENDA_CON_CODIGO,
            this.VENDA_MEDICO,
            this.VENDA_NUM_RECEITA,
            this.VENDA_AUTORIZACAO_RECEITA,
            this.MED_NOME,
            this.CON_NOME,
            this.CF_DOCTO,
            this.VENDA_CARTAO});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCancelar.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgCancelar.GridColor = System.Drawing.Color.Black;
            this.dgCancelar.Location = new System.Drawing.Point(6, 21);
            this.dgCancelar.Name = "dgCancelar";
            this.dgCancelar.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCancelar.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgCancelar.RowHeadersVisible = false;
            this.dgCancelar.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCancelar.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgCancelar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCancelar.Size = new System.Drawing.Size(936, 349);
            this.dgCancelar.TabIndex = 0;
            this.dgCancelar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCancelar_CellClick);
            this.dgCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCancelar_KeyDown);
            // 
            // CANCELA
            // 
            this.CANCELA.HeaderText = "";
            this.CANCELA.Name = "CANCELA";
            this.CANCELA.ReadOnly = true;
            this.CANCELA.Visible = false;
            this.CANCELA.Width = 20;
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            this.EMP_CODIGO.HeaderText = "Emp. Codigo";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            this.EMP_CODIGO.Visible = false;
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "Est. Codigo";
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.Visible = false;
            // 
            // VENDA_ID
            // 
            this.VENDA_ID.DataPropertyName = "VENDA_ID";
            this.VENDA_ID.FillWeight = 200F;
            this.VENDA_ID.HeaderText = "Venda ID";
            this.VENDA_ID.Name = "VENDA_ID";
            this.VENDA_ID.ReadOnly = true;
            this.VENDA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_ID.Width = 89;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Cliente";
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_NOME.Width = 250;
            // 
            // VENDA_EMISSAO
            // 
            this.VENDA_EMISSAO.DataPropertyName = "VENDA_EMISSAO";
            this.VENDA_EMISSAO.HeaderText = "Emissão";
            this.VENDA_EMISSAO.Name = "VENDA_EMISSAO";
            this.VENDA_EMISSAO.ReadOnly = true;
            this.VENDA_EMISSAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDA_TOTAL
            // 
            this.VENDA_TOTAL.DataPropertyName = "VENDA_TOTAL";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.VENDA_TOTAL.DefaultCellStyle = dataGridViewCellStyle3;
            this.VENDA_TOTAL.HeaderText = "Valor da Venda";
            this.VENDA_TOTAL.Name = "VENDA_TOTAL";
            this.VENDA_TOTAL.ReadOnly = true;
            this.VENDA_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_TOTAL.Width = 130;
            // 
            // VENDA_NUM_NOTA
            // 
            this.VENDA_NUM_NOTA.DataPropertyName = "VENDA_NUM_NOTA";
            this.VENDA_NUM_NOTA.HeaderText = "Num. Nota";
            this.VENDA_NUM_NOTA.Name = "VENDA_NUM_NOTA";
            this.VENDA_NUM_NOTA.ReadOnly = true;
            this.VENDA_NUM_NOTA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_NUM_NOTA.Width = 110;
            // 
            // OP_CADASTRO
            // 
            this.OP_CADASTRO.DataPropertyName = "OP_CADASTRO";
            this.OP_CADASTRO.HeaderText = "Usuário";
            this.OP_CADASTRO.Name = "OP_CADASTRO";
            this.OP_CADASTRO.ReadOnly = true;
            this.OP_CADASTRO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_CADASTRO.Width = 80;
            // 
            // VENDA_BENEFICIO
            // 
            this.VENDA_BENEFICIO.DataPropertyName = "VENDA_BENEFICIO";
            this.VENDA_BENEFICIO.HeaderText = "Benefício";
            this.VENDA_BENEFICIO.Name = "VENDA_BENEFICIO";
            this.VENDA_BENEFICIO.ReadOnly = true;
            this.VENDA_BENEFICIO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_BENEFICIO.Width = 140;
            // 
            // VENDA_DATA_HORA
            // 
            this.VENDA_DATA_HORA.DataPropertyName = "VENDA_DATA_HORA";
            this.VENDA_DATA_HORA.HeaderText = "Data/Hora";
            this.VENDA_DATA_HORA.Name = "VENDA_DATA_HORA";
            this.VENDA_DATA_HORA.ReadOnly = true;
            this.VENDA_DATA_HORA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_DATA_HORA.Visible = false;
            // 
            // MOVIMENTO_FINANCEIRO_SEQ
            // 
            this.MOVIMENTO_FINANCEIRO_SEQ.DataPropertyName = "MOVIMENTO_FINANCEIRO_SEQ";
            this.MOVIMENTO_FINANCEIRO_SEQ.HeaderText = "Seq";
            this.MOVIMENTO_FINANCEIRO_SEQ.Name = "MOVIMENTO_FINANCEIRO_SEQ";
            this.MOVIMENTO_FINANCEIRO_SEQ.ReadOnly = true;
            this.MOVIMENTO_FINANCEIRO_SEQ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MOVIMENTO_FINANCEIRO_SEQ.Visible = false;
            // 
            // VENDA_TRANS_ID
            // 
            this.VENDA_TRANS_ID.DataPropertyName = "VENDA_TRANS_ID";
            this.VENDA_TRANS_ID.HeaderText = "Trans ID";
            this.VENDA_TRANS_ID.Name = "VENDA_TRANS_ID";
            this.VENDA_TRANS_ID.ReadOnly = true;
            this.VENDA_TRANS_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_TRANS_ID.Visible = false;
            // 
            // VENDA_TRANS_ID_RECEITA
            // 
            this.VENDA_TRANS_ID_RECEITA.DataPropertyName = "VENDA_TRANS_ID_RECEITA";
            this.VENDA_TRANS_ID_RECEITA.HeaderText = "Trans ID Rec";
            this.VENDA_TRANS_ID_RECEITA.Name = "VENDA_TRANS_ID_RECEITA";
            this.VENDA_TRANS_ID_RECEITA.ReadOnly = true;
            this.VENDA_TRANS_ID_RECEITA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_TRANS_ID_RECEITA.Visible = false;
            // 
            // VENDA_COL_CODIGO
            // 
            this.VENDA_COL_CODIGO.DataPropertyName = "VENDA_COL_CODIGO";
            this.VENDA_COL_CODIGO.HeaderText = "Colaborador";
            this.VENDA_COL_CODIGO.Name = "VENDA_COL_CODIGO";
            this.VENDA_COL_CODIGO.ReadOnly = true;
            this.VENDA_COL_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_COL_CODIGO.Visible = false;
            // 
            // VENDA_SUBTOTAL
            // 
            this.VENDA_SUBTOTAL.DataPropertyName = "VENDA_SUBTOTAL";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.VENDA_SUBTOTAL.DefaultCellStyle = dataGridViewCellStyle4;
            this.VENDA_SUBTOTAL.HeaderText = "Subtotal";
            this.VENDA_SUBTOTAL.Name = "VENDA_SUBTOTAL";
            this.VENDA_SUBTOTAL.ReadOnly = true;
            this.VENDA_SUBTOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_SUBTOTAL.Visible = false;
            // 
            // VENDA_DIFERENCA
            // 
            this.VENDA_DIFERENCA.DataPropertyName = "VENDA_DIFERENCA";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.VENDA_DIFERENCA.DefaultCellStyle = dataGridViewCellStyle5;
            this.VENDA_DIFERENCA.HeaderText = "Diferenca";
            this.VENDA_DIFERENCA.Name = "VENDA_DIFERENCA";
            this.VENDA_DIFERENCA.ReadOnly = true;
            this.VENDA_DIFERENCA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VENDA_DIFERENCA.Visible = false;
            // 
            // VENDA_AUTORIZACAO
            // 
            this.VENDA_AUTORIZACAO.DataPropertyName = "VENDA_AUTORIZACAO";
            this.VENDA_AUTORIZACAO.HeaderText = "Autorização";
            this.VENDA_AUTORIZACAO.Name = "VENDA_AUTORIZACAO";
            this.VENDA_AUTORIZACAO.ReadOnly = true;
            this.VENDA_AUTORIZACAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VENDA_NOME_CARTAO
            // 
            this.VENDA_NOME_CARTAO.DataPropertyName = "VENDA_NOME_CARTAO";
            this.VENDA_NOME_CARTAO.HeaderText = "VENDA_NOME_CARTAO";
            this.VENDA_NOME_CARTAO.Name = "VENDA_NOME_CARTAO";
            this.VENDA_NOME_CARTAO.ReadOnly = true;
            this.VENDA_NOME_CARTAO.Visible = false;
            // 
            // VENDA_CON_CODIGO
            // 
            this.VENDA_CON_CODIGO.DataPropertyName = "VENDA_CON_CODIGO";
            this.VENDA_CON_CODIGO.HeaderText = "VENDA_CON_CODIGO";
            this.VENDA_CON_CODIGO.Name = "VENDA_CON_CODIGO";
            this.VENDA_CON_CODIGO.ReadOnly = true;
            this.VENDA_CON_CODIGO.Visible = false;
            // 
            // VENDA_MEDICO
            // 
            this.VENDA_MEDICO.DataPropertyName = "VENDA_MEDICO";
            this.VENDA_MEDICO.HeaderText = "VENDA_MEDICO";
            this.VENDA_MEDICO.Name = "VENDA_MEDICO";
            this.VENDA_MEDICO.ReadOnly = true;
            this.VENDA_MEDICO.Visible = false;
            // 
            // VENDA_NUM_RECEITA
            // 
            this.VENDA_NUM_RECEITA.DataPropertyName = "VENDA_NUM_RECEITA";
            this.VENDA_NUM_RECEITA.HeaderText = "VENDA_NUM_RECEITA";
            this.VENDA_NUM_RECEITA.Name = "VENDA_NUM_RECEITA";
            this.VENDA_NUM_RECEITA.ReadOnly = true;
            this.VENDA_NUM_RECEITA.Visible = false;
            // 
            // VENDA_AUTORIZACAO_RECEITA
            // 
            this.VENDA_AUTORIZACAO_RECEITA.DataPropertyName = "VENDA_AUTORIZACAO_RECEITA";
            this.VENDA_AUTORIZACAO_RECEITA.HeaderText = "VENDA_AUTORIZACAO_RECEITA";
            this.VENDA_AUTORIZACAO_RECEITA.Name = "VENDA_AUTORIZACAO_RECEITA";
            this.VENDA_AUTORIZACAO_RECEITA.ReadOnly = true;
            this.VENDA_AUTORIZACAO_RECEITA.Visible = false;
            // 
            // MED_NOME
            // 
            this.MED_NOME.DataPropertyName = "MED_NOME";
            this.MED_NOME.HeaderText = "MED_NOME";
            this.MED_NOME.Name = "MED_NOME";
            this.MED_NOME.ReadOnly = true;
            this.MED_NOME.Visible = false;
            // 
            // CON_NOME
            // 
            this.CON_NOME.DataPropertyName = "CON_NOME";
            this.CON_NOME.HeaderText = "CON_NOME";
            this.CON_NOME.Name = "CON_NOME";
            this.CON_NOME.ReadOnly = true;
            this.CON_NOME.Visible = false;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CF_DOCTO";
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.Visible = false;
            // 
            // VENDA_CARTAO
            // 
            this.VENDA_CARTAO.DataPropertyName = "VENDA_CARTAO";
            this.VENDA_CARTAO.HeaderText = "VENDA_CARTAO";
            this.VENDA_CARTAO.Name = "VENDA_CARTAO";
            this.VENDA_CARTAO.ReadOnly = true;
            this.VENDA_CARTAO.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.Controls.Add(this.panel2);
            this.groupBox4.Controls.Add(this.statusStrip1);
            this.groupBox4.Location = new System.Drawing.Point(10, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(975, 560);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel2.Controls.Add(this.label10);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(-1, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(975, 24);
            this.panel2.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cancelamento de Vendas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(969, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenCancelaNF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox4);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenCancelaNF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Cancelamento de Vendas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmVenCancelaNF_Load);
            this.Shown += new System.EventHandler(this.frmVenCancelaNF_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenCancelaNF_KeyDown);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCancelar)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgCancelar;
        private System.Windows.Forms.TextBox txtPInicial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CANCELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_EMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_NUM_NOTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_BENEFICIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_DATA_HORA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOVIMENTO_FINANCEIRO_SEQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TRANS_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_TRANS_ID_RECEITA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_COL_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_SUBTOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_DIFERENCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_AUTORIZACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_NOME_CARTAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_MEDICO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_NUM_RECEITA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_AUTORIZACAO_RECEITA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDA_CARTAO;
    }
}