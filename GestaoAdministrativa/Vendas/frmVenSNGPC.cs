﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenSNGPC : Form
    {
        private DataTable dtProdutos = new DataTable();
        frmVenda fVendas;
        private string[,] dados = new string[30, 4];
        private int contMatriz;
        private int contador;
        private int qtde;

        public frmVenSNGPC(DataTable produtos, frmVenda venda)
        {
            InitializeComponent();
            dtProdutos = produtos;
            fVendas = venda;
            RegisterFocusEvents(this.Controls);
        }

        private void frmVenSNGPC_Load(object sender, EventArgs e)
        {

        }

        private void frmVenSNGPC_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
            Limpar();
            qtde = 0;
            contMatriz = 0;
            contador = 0;
            DataRow[] result = dtProdutos.Select("PROD_CONTROLADO = 'S'");
            qtde = result.Length;

            foreach (DataRow row in result)
            {
                dados[contMatriz, 0] = row["COD_BARRA"].ToString();
                dados[contMatriz, 1] = row["PROD_DESCR"].ToString();
                dados[contMatriz, 2] = row["PROD_QTDE"].ToString();
                dados[contMatriz, 3] = row["PROD_LOTE"].ToString();
                contMatriz = contMatriz + 1;
            }
            
            if(contMatriz > 0)
            {
                txtCodBarras.Text = dados[contador, 0];
                txtDescr.Text = dados[contador, 1];
                txtQtde.Text = dados[contador, 2];
                txtReg.Text = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_REGISTRO_MS", "PROD_CODIGO", txtCodBarras.Text, false, true);
                contador = 1;
                BuscaLotes();

                txtDocto.Focus();
            }

            if (fVendas.txtCodCliEmp.Text != Funcoes.LeParametro(6, "114", false))
            {
                var dadosCliente = new Cliente();
                DataTable dt = dadosCliente.BuscaClientePorID(fVendas.IDCliente);
                if (dt.Rows.Count > 0)
                {
                    txtDocto.Text = dt.Rows[0]["CF_DOCTO"].ToString();
                    txtNome.Text = dt.Rows[0]["CF_NOME"].ToString();
                    dtData.Focus();
                }
                else
                {
                    txtDocto.Focus();
                }
            }
        }

        public bool BuscaLotes()
        {
            try
            {
                var vendas = new SngpcControleVendas();
                DataTable dtLotes = vendas.BuscaLotes(txtCodBarras.Text);

                dtLotes.Rows.Add(0, "Informar Lote");
                cmbLote.DataSource = dtLotes;
                cmbLote.DisplayMember = "LOTE";
                cmbLote.ValueMember = "QTDE";
                cmbLote.SelectedIndex = -1;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool CarregaCombos()
        {
            try
            {

                SngpcTabela tabelas = new SngpcTabela();
                //CARREGA TIPO DE RECEITUARIO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(1);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de receituário,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbTipoRec.DataSource = Principal.dtPesq;
                    cmbTipoRec.DisplayMember = "TAB_DESCRICAO";
                    cmbTipoRec.ValueMember = "TAB_SEQ";
                    cmbTipoRec.SelectedIndex = -1;
                }

                //CARREGA TIPO DE DOCUMENTO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(9);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de documento,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbDocto.DataSource = Principal.dtPesq;
                    cmbDocto.DisplayMember = "TAB_DESCRICAO";
                    cmbDocto.ValueMember = "TAB_SEQ";
                    cmbDocto.SelectedIndex = -1;

                }

                //CARREGA TIPO DE USO DO MEDICAMENTO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(2);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de uso do medicamento,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUMed.DataSource = Principal.dtPesq;
                    cmbUMed.DisplayMember = "TAB_DESCRICAO";
                    cmbUMed.ValueMember = "TAB_SEQ";
                    cmbUMed.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(4);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de conselho profissional,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbConselho.DataSource = Principal.dtPesq;
                    cmbConselho.DisplayMember = "TAB_DESCRICAO";
                    cmbConselho.ValueMember = "TAB_SEQ";
                    cmbConselho.SelectedIndex = -1;
                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(5);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma unidade federativa,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUF.DataSource = Principal.dtPesq;
                    cmbUF.DisplayMember = "TAB_CODIGO";
                    cmbUF.ValueMember = "TAB_SEQ";
                    cmbUF.SelectedIndex = -1;

                    cmbMUF.DataSource = Principal.dtPesq;
                    cmbMUF.DisplayMember = "TAB_CODIGO";
                    cmbMUF.ValueMember = "TAB_SEQ";
                    cmbMUF.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(10);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um orgão expedidor,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbOrgao.DataSource = Principal.dtPesq;
                    cmbOrgao.DisplayMember = "TAB_DESCRICAO";
                    cmbOrgao.ValueMember = "TAB_SEQ";
                    cmbOrgao.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(11);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um sexo,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbPSexo.DataSource = Principal.dtPesq;
                    cmbPSexo.DisplayMember = "TAB_DESCRICAO";
                    cmbPSexo.ValueMember = "TAB_SEQ";
                    cmbPSexo.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(12);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de unidade de idade,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbPUnidade.DataSource = Principal.dtPesq;
                    cmbPUnidade.DisplayMember = "TAB_DESCRICAO";
                    cmbPUnidade.ValueMember = "TAB_SEQ";
                    cmbPUnidade.SelectedIndex = -1;

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Limpar()
        {
            cmbLote.DataSource = null;
            cmbDocto.SelectedIndex = -1;
            cmbOrgao.SelectedIndex = -1;
            cmbUF.SelectedIndex = -1;
            txtDocto.Text = "";
            txtNome.Text = "";
            dtData.Value = DateTime.Now;
            txtPNome.Text = "";
            txtPIdade.Text = "";
            cmbPUnidade.SelectedIndex = -1;
            cmbPSexo.SelectedIndex = -1;
            txtCrm.Text = "";
            txtMedico.Text = "";
            cmbConselho.SelectedIndex = -1;
            txtNReceita.Text = "";
            dtReceita.Value = DateTime.Now;
            cmbMUF.SelectedIndex = -1;
            cmbUMed.SelectedIndex = -1;
            cmbTipoRec.SelectedIndex = -1;
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtReg.Text = "";
            txtLote.Text = "";
            txtQtde.Text = "0";
            cmbDocto.Text = "CARTEIRA DE IDENTIDADE";
            cmbOrgao.Text = "SECRETÁRIA DE SEGURANÇA PÚBLICA";
            cmbUF.Text = "SP";
            dtData.Value = DateTime.Now;
            dtReceita.Value = DateTime.Now;
            txtDocto.Focus();
        }

        private void cmbLote_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLote.Text == "Informar Lote")
            {
                txtLote.Visible = true;
                lblLote.Visible = true;
                txtQtdeEstoque.Text = "0";
                txtLote.Focus();
            }
            else
            {
                txtLote.Visible = false;
                lblLote.Visible = false;
                txtQtdeEstoque.Text = Convert.ToString(cmbLote.SelectedValue);
            }
        }

        private void cmbDocto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
            }
            
        }

        private void cmbOrgao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtDocto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtData_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtPNome_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtPIdade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbPUnidade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbPSexo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCrm_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtMedico_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbConselho_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtReceita_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbMUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUMed_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbTipoRec_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtCodBarras") && (control.Name != "txtReg") && (control.Name != "txtQtdeEstoque") && (control.Name != "txtDescr") && (control.Name != "txtQtde"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtReg_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbLote_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtQtdeEstoque_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbOrgao.Focus();
        }

        private void cmbOrgao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUF.Focus();
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtData.Focus();
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPNome.Focus();
        }

        private void txtPNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPIdade.Focus();
        }

        private void txtPIdade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbPUnidade.Focus();
        }

        private void cmbPUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbPSexo.Focus();
        }

        private void txtCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtCrm.Text.Trim() != "")
                    {
                        Cursor = Cursors.WaitCursor;
                        Medico med = new Medico();
                        txtMedico.Text = med.BuscaMedicoPorCRM(txtCrm.Text);
                        Cursor = Cursors.Default;
                        cmbConselho.Focus();
                    }
                    else
                        txtMedico.Focus();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtMedico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbConselho.Focus();
        }

        private void cmbConselho_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNReceita.Focus();
        }

        private void txtNReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtReceita.Focus();
        }

        private void dtReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbMUF.Focus();
        }

        private void cmbMUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUMed.Focus();
        }

        private void cmbUMed_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbTipoRec.Focus();
        }

        private void cmbTipoRec_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDescr.Focus();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtReg.Focus();
        }

        private void txtReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbLote.Focus();
        }

        private void cmbLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbLote.Text.Equals("Informar Lote"))
                {
                    txtLote.Focus();
                }
                else
                {
                    txtQtde.Focus();
                }
            }
        }

        private void txtLote_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();
        }

        private void cmbPSexo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCrm.Focus();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (Incluir())
            {
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                    if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência ", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtData.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        SngpcVenda venda = new SngpcVenda()
                        {

                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            VenID = Funcoes.IdentificaVerificaID("SNGPC_VENDAS", "VEN_SEQ", 0, ""),
                            VenCompTd = cmbDocto.SelectedValue.ToString(),
                            VenCompOE = cmbOrgao.SelectedValue.ToString(),
                            VenCompUF = cmbUF.SelectedValue.ToString(),
                            VenCompDocto = txtDocto.Text.Trim(),
                            VenCompNome = txtNome.Text.Trim(),
                            VenData = Convert.ToDateTime(dtData.Text),
                            VenPacNome = txtPNome.Text.Trim(),
                            VenPacIdade = txtPIdade.Text.Trim() == "" ? 0 : Convert.ToInt32(txtPIdade.Text.Trim()),
                            VenPacUnidade = cmbPUnidade.SelectedValue.ToString(),
                            VenPacSexo = cmbPSexo.SelectedValue.ToString(),
                            VenPrescNum = txtCrm.Text,
                            VenPrescNome = txtMedico.Text,
                            VenPrescCP = cmbConselho.SelectedValue.ToString(),
                            VenNumNota = txtNReceita.Text,
                            VenPrescData = Convert.ToDateTime(dtReceita.Text),
                            VenPrescUF = cmbMUF.SelectedValue.ToString(),
                            VenUso = cmbUMed.SelectedValue.ToString(),
                            VenTipoReceita = cmbTipoRec.SelectedValue.ToString(),
                            ProdCodigo = txtCodBarras.Text.Trim(),
                            VenMedLote = cmbLote.Text == "Informar Lote" ? txtLote.Text : cmbLote.Text,
                            VenMedQtde = Convert.ToInt32(txtQtde.Text),
                            VenMS = txtReg.Text.Trim()

                        };
                        BancoDados.AbrirTrans();
                        if (venda.IncluirVenda(venda).Equals(true))
                        {
                            var prod = new Produto();
                            DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);

                            if (cmbLote.Text != "Informar Lote")
                            {
                                SngpcControleVendas controVenda = new SngpcControleVendas();

                                controVenda = new SngpcControleVendas()
                                {
                                    ProCodigo = txtCodBarras.Text,
                                    Lote = cmbLote.Text,
                                    Qtde = Convert.ToInt32(txtQtde.Text),
                                    Data = DateTime.Now
                                };

                                if (controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda).Equals(false))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Cursor = Cursors.Default;
                                    return false;
                                }
                                else
                                {
                                    BancoDados.FecharTrans();
  
                                    MessageBox.Show("Venda Realizada com sucesso", "Vendas SNGPC", MessageBoxButtons.OK);

                                    Cursor = Cursors.Default;
                                    if (contador != qtde)
                                    {
                                        if (MessageBox.Show("Matem os dados do Paciente?", "Vendas SNGPC", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                        {
                                            Limpar();
                                        }
                                        else
                                        {
                                            txtCodBarras.Text = "";
                                            txtDescr.Text = "";
                                            txtReg.Text = "";
                                            cmbLote.SelectedIndex = -1;
                                            txtQtdeEstoque.Text = "0";
                                            txtLote.Text = "";
                                            txtLote.Visible = false;
                                            txtQtde.Text = "0";
                                            cmbUF.Text = "SP";
                                            txtCodBarras.Focus();
                                        }

                                        txtCodBarras.Text = dados[contador, 0];
                                        txtDescr.Text = dados[contador, 1];
                                        txtQtde.Text = dados[contador, 2];
                                        txtReg.Text = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_REGISTRO_MS", "PROD_CODIGO", txtCodBarras.Text, false, true);
                                        contador = contador + 1;
                                        BuscaLotes();

                                        txtDocto.Focus();
                                    }
                                    else
                                    {
                                        this.Close();
                                    }
                                    
                                    return true;
                                }
                            }
                            else
                            {
                                BancoDados.FecharTrans();
                                
                                MessageBox.Show("Venda Realizada com sucesso", "Vendas SNGPC", MessageBoxButtons.OK);
                                Cursor = Cursors.Default;

                                if (contador != qtde)
                                {
                                    if (MessageBox.Show("Matem os dados do Paciente?", "Vendas SNGPC", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                    {
                                        Limpar();
                                    }
                                    else
                                    {
                                        txtCodBarras.Text = "";
                                        txtDescr.Text = "";
                                        txtReg.Text = "";
                                        cmbLote.SelectedIndex = -1;
                                        txtQtdeEstoque.Text = "0";
                                        txtLote.Text = "";
                                        txtLote.Visible = false;
                                        txtQtde.Text = "0";
                                        cmbUF.Text = "SP";
                                        txtCodBarras.Focus();
                                    }

                                    txtCodBarras.Text = dados[contador, 0];
                                    txtDescr.Text = dados[contador, 1];
                                    txtQtde.Text = dados[contador, 2];
                                    txtReg.Text = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_REGISTRO_MS", "PROD_CODIGO", txtCodBarras.Text, false, true);
                                    contador = contador + 1;
                                    BuscaLotes();

                                    txtDocto.Focus();
                                }
                                else
                                {
                                    this.Close();
                                }
                                return true;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Cursor = Cursors.Default;
                            return false;
                        }
                    }
                }
                else
                {
                    Cursor = Cursors.Default;
                    return false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        
        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbDocto.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de documento.";
                Funcoes.Avisa();
                cmbDocto.Focus();
                return false;
            }
            if (Equals(cmbOrgao.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um orgão expedidor.";
                Funcoes.Avisa();
                cmbOrgao.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbOrgao.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDocto.Text.Trim()))
            {
                Principal.mensagem = "Número do documento não pode estar em branco.";
                Funcoes.Avisa();
                txtDocto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome do cliente não pode estar em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtPNome.Text.Trim()))
            {
                Principal.mensagem = "Nome do paciente não pode ser em branco.";
                Funcoes.Avisa();
                txtPNome.Focus();
                return false;
            }
            if (Equals(cmbPSexo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o sexo do paciente.";
                Funcoes.Avisa();
                cmbPSexo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCrm.Text.Trim()))
            {
                Principal.mensagem = "CRM não pode ser em branco.";
                Funcoes.Avisa();
                txtCrm.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtMedico.Text.Trim()))
            {
                Principal.mensagem = "Nome do médico não pode ser em branco.";
                Funcoes.Avisa();
                txtMedico.Focus();
                return false;
            }
            if (Equals(cmbConselho.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o conselho profissional.";
                Funcoes.Avisa();
                cmbConselho.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNReceita.Text.Trim()))
            {
                Principal.mensagem = "Número da receita não pode ser em branco.";
                Funcoes.Avisa();
                txtNReceita.Focus();
                return false;
            }
            if (Equals(cmbMUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbMUF.Focus();
                return false;
            }
            if (Equals(cmbUMed.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o uso do medicamento.";
                Funcoes.Avisa();
                cmbUMed.Focus();
                return false;
            }
            if (Equals(cmbTipoRec.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de receita.";
                Funcoes.Avisa();
                cmbTipoRec.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtReg.Text.Trim()))
            {
                Principal.mensagem = "Reg. Min. da Saúde não pode ser em branco.";
                Funcoes.Avisa();
                txtReg.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || Equals(txtQtde.Text, 0))
            {
                Principal.mensagem = "Qtde não pode ser zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (cmbLote.SelectedIndex == -1)
            {
                Principal.mensagem = "Lote não pode estar vazio.";
                Funcoes.Avisa();
                cmbLote.Focus();
                return false;
            }
            if (txtLote.Visible == true && String.IsNullOrEmpty(txtLote.Text.Trim()))
            {
                Principal.mensagem = "Lote não pode ser em branco.";
                Funcoes.Avisa();
                txtLote.Focus();
                return false;
            }
            if (cmbLote.Text != "Informar Lote" && Convert.ToInt32(txtQtde.Text) > Convert.ToInt32(txtQtdeEstoque.Text))
            {
                Principal.mensagem = "Qtde não pode ser maior do que a Qtde em Estoque";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
