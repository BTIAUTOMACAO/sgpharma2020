﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenComprovante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenComprovante));
            this.lbComprovante = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbComprovante
            // 
            this.lbComprovante.BackColor = System.Drawing.Color.Ivory;
            this.lbComprovante.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbComprovante.FormattingEnabled = true;
            this.lbComprovante.ItemHeight = 17;
            this.lbComprovante.Location = new System.Drawing.Point(0, 0);
            this.lbComprovante.Name = "lbComprovante";
            this.lbComprovante.Size = new System.Drawing.Size(380, 395);
            this.lbComprovante.TabIndex = 0;
            this.lbComprovante.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbComprovante_KeyDown);
            // 
            // frmVenComprovante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 395);
            this.Controls.Add(this.lbComprovante);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenComprovante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendas -Vizualiza Comprovante";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbComprovante;
    }
}