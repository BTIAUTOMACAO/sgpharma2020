﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.PortalDaDrogaria
{
    public partial class frmPortalDrogariaFinalizaVenda : Form
    {
        frmVenda fVenda;
        Negocio.Beneficios.PortalDaDrogaria vendaPortalDaDrogaria = new Negocio.Beneficios.PortalDaDrogaria();
        public string erro;
        public string mensagem;
        public string validou;
        private bool erroDeVenda;
        private string operadoraVenda;

        public frmPortalDrogariaFinalizaVenda(frmVenda formVendas)
        {
            InitializeComponent();
            fVenda = formVendas;
        }

        private void frmPortalDrogariaFinalizaVenda_Load(object sender, EventArgs e)
        {
            try
            {
                erroDeVenda = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmPortalDrogariaFinalizaVenda_Shown(object sender, EventArgs e)
        {
            try
            {
                fVenda.cupomBeneficio = "";
                Cursor = Cursors.WaitCursor;

                lblMensagem.Text = "Enviando Confirmação da Venda!";
                lblMensagem.Refresh();

                vendaPortalDaDrogaria.EnviaTransacao(fVenda.vendaNumeroNota, fVenda.vendaNumeroNSU, fVenda.wsNumCartao);

                lblMensagem.Text = "Esperando Efetivação da Venda!";
                lblMensagem.Refresh();

                string caminho;
                List<RetornoPortalDrogaria> retono = vendaPortalDaDrogaria.RetornoTransacaoFinalizacao(this, out caminho);

                if (retono.Count > 0)
                {
                    var oeradora = File.ReadAllLines(caminho)
                       .Where(l => l.StartsWith("040-000"))
                       .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                       .ToList();

                    operadoraVenda = oeradora[0].ToString();

                    if (!String.IsNullOrEmpty(erro))
                    {
                        if (Convert.ToInt32(erro) > 9 && Convert.ToInt32(erro) != 43 && String.IsNullOrEmpty(validou))
                        {
                            erroDeVenda = true;
                            Cursor = Cursors.Default;
                            lblMensagem.Text = mensagem;
                            lblMensagem.Refresh();
                            return;
                        }
                        else if (Convert.ToInt32(erro) > 40 && !String.IsNullOrEmpty(mensagem))
                        {
                            erroDeVenda = true;
                            Cursor = Cursors.Default;
                            lblMensagem.Text = mensagem;
                            lblMensagem.Refresh();
                            return;
                        }
                    }
                    
                    var lote = File.ReadAllLines(caminho)
                         .Where(l => l.StartsWith("014-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                    var codAutorizacao = File.ReadAllLines(caminho)
                        .Where(l => l.StartsWith("013-000"))
                        .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                        .ToList();

                    var linhas = File.ReadAllLines(caminho)
                                   .Where(l => l.StartsWith("028-000"))
                                   .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                   .ToList();

                    for (int i = 0; i < (Convert.ToInt32(linhas[0])); i++)
                    {
                        var cupom = File.ReadAllLines(caminho)
                            .Where(l => l.StartsWith("029-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                            .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                            .ToList();

                        fVenda.cupomBeneficio = fVenda.cupomBeneficio + cupom[0].ToString() + "\n@";
                    }
                    
                    fVenda.lotePortalDrogaria = lote[0].ToString();
                    fVenda.codAutorizacaoPortalDrogaria = codAutorizacao[0].ToString();
                 
                    lblMensagem.Text = "";
                    lblMensagem.Refresh();
                }

                lblMensagem.Text = "";
                lblMensagem.Refresh();

                this.Hide();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            try
            {
                if(erroDeVenda)
                {
                    lblMensagem.Text = "Venda será anulada! Aguarde...";
                    lblMensagem.Refresh();

                    Negocio.Beneficios.PortalDaDrogaria vendaPortal = new Negocio.Beneficios.PortalDaDrogaria();

                    vendaPortal.EnviaAnulaVenda(operadoraVenda, fVenda.vendaNumeroNSU);

                    List<RetornoPortalDrogaria> retorno = vendaPortal.RetornoAnulaVendaCupomFiscal();
                    if (retorno.Count > 0)
                    {
                        MessageBox.Show("001- Operação anterior cancelada com Sucesso!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        var excluiEfetivado = new BeneficioNovartisEfetivado();
                        excluiEfetivado.ExcluirDadosPorNSU(fVenda.vendaNumeroNSU);

                        var excluiProduto = new BeneficioNovartisProdutos();
                        excluiProduto.ExcluirDadosPorNSU(fVenda.vendaNumeroNSU);

                        var excluiPendentes = new BeneficioNovartisPendente();
                        excluiPendentes.ExcluirDadosPorNSU(fVenda.vendaNumeroNSU);

                        var excluiComandas = new BeneficioNovartisComanda();
                        excluiComandas.ExcluirDadosPorNSU(fVenda.vendaNumeroNSU);
                    }

                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
