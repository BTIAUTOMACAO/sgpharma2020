﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.PortalDaDrogaria
{
    public partial class frmPortalDrogariaAutorizacao : Form
    {
        DataTable dtRetorno = new DataTable();
        frmVenda fVenda;
        Negocio.Beneficios.PortalDaDrogaria vendaPortal = new Negocio.Beneficios.PortalDaDrogaria();
        public string erro;
        public string mensagem;
        public string validou;

        public frmPortalDrogariaAutorizacao(frmVenda formVenda)
        {
            InitializeComponent();
            fVenda = formVenda;
        }

        private void frmPortalDrogariaAutorizacao_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensagem.Text = "";
                cmbOperadoras.Enabled = true;
                txtNSU.Enabled = true;
                btnConsultar.Enabled = true;
                btnLimpar.Enabled = true;
                btnEfetivar.Enabled = false;

                cmbOperadoras.SelectedIndex = -1;
                cmbOperadoras.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmPortalDrogariaAutorizacao_Shown(object sender, EventArgs e)
        {
            try
            {
                Application.DoEvents();

                string retorno = Util.LerArquivoTXT(Funcoes.LeParametro(14, "28", false) + "TCTB_OPR.txt");

                if (!String.IsNullOrEmpty(retorno))
                {
                    string[] retornoVenda = retorno.Split('\n');

                    for (int i = 0; i < (retornoVenda.Length - 1); i++)
                    {
                        cmbOperadoras.Items.Add(retornoVenda[i].Substring(0, 20).Trim());
                    }
                }

                Cursor = Cursors.WaitCursor;
                lblMensagem.Text = "Verificando se existe Venda Pendente. Aguarde!";
                lblMensagem.Refresh();

                var buscaPendente = new BeneficioNovartisPendente();
                dtRetorno = buscaPendente.BuscaVendasPendentes();
                if (dtRetorno.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        if (dtRetorno.Rows[i]["TIPO_TRANSACAO"].ToString().Equals("V"))
                        {
                            string nsu = dtRetorno.Rows[i]["NSU"].ToString();

                            vendaPortal.EnviaAnulaVenda(dtRetorno.Rows[i]["OPERADORA"].ToString(), nsu);

                            string caminho;
                            List<RetornoPortalDrogaria> retono = vendaPortal.RetornoAnulaVenda(this, out caminho);
                            if (retono.Count > 0)
                            {
                                Cursor = Cursors.Default;
                                MessageBox.Show("Houve algum erro na venda com NSU: " + nsu + ", por esse motivo foi realizado o cancelamento da mesma!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                buscaPendente.ExcluirDadosPorNSU(nsu);

                                var excluiEfetivado = new BeneficioNovartisEfetivado();
                                excluiEfetivado.ExcluirDadosPorNSU(nsu);

                                var excluiProduto = new BeneficioNovartisProdutos();
                                excluiProduto.ExcluirDadosPorNSU(nsu);
                            }

                            Cursor = Cursors.WaitCursor;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                lblMensagem.Text = "";
                lblMensagem.Refresh();
            }
        }

        private void cmbOperadoras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbOperadoras.SelectedIndex != -1)
                    txtNSU.Focus();
                else
                    cmbOperadoras.Focus();
            }

        }

        private void txtCartao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtNSU.Text))
                {
                    txtCartaoCpf.Focus();
                }
                else
                {
                    btnConsultar.PerformClick();
                }
            }

        }

        private void txtCartao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cmbOperadoras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCampos())
                {
                    if (!vendaPortal.IdentificaConexaoComInternet())
                    {
                        MessageBox.Show("Verifique sua Conexão com a Internet!", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    lblMensagem.Text = "Consultando NSU. Aguardando retorno!";
                    lblMensagem.Refresh();

                    Cursor = Cursors.WaitCursor;

                    vendaPortal.EnviaRecuperacaoDePreAutorizacao(cmbOperadoras.Text, txtNSU.Text, fVenda.vendaID, txtCartaoCpf.Text);

                    string caminho;

                    lblMensagem.Text = "Aguardando Retorno!";
                    lblMensagem.Refresh();

                    List<RetornoPortalDrogaria> retono = vendaPortal.RetornoRecuperacaoDePreAutorizacao(this, out caminho);

                    if (retono.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(erro))
                        {
                            if (Convert.ToInt32(erro) > 9 && Convert.ToInt32(erro) != 43 && String.IsNullOrEmpty(validou))
                            {

                                Cursor = Cursors.Default;
                                lblMensagem.Text = mensagem;
                                lblMensagem.Refresh();
                                txtNSU.Focus();
                                vendaPortal.LimparPastas();
                                return;
                            }
                            else if (Convert.ToInt32(erro) == 43 && String.IsNullOrEmpty(validou))
                            {

                                Cursor = Cursors.Default;
                                lblMensagem.Text = mensagem;
                                lblMensagem.Refresh();
                                txtNSU.Focus();
                                vendaPortal.LimparPastas();
                                return;
                            }
                            else if (!String.IsNullOrEmpty(mensagem))
                            {
                                Cursor = Cursors.Default;
                                lblMensagem.Text = mensagem;
                                lblMensagem.Refresh();
                                txtNSU.Focus();
                                vendaPortal.LimparPastas();
                                return;
                            }

                        }

                        if (String.IsNullOrEmpty(validou))
                        {
                            Cursor = Cursors.Default;
                            lblMensagem.Text = "Nenhum produto autorizado!";
                            lblMensagem.Refresh();
                            txtNSU.Focus();
                            return;
                        }

                        var nsuEstabelecimento = File.ReadAllLines(caminho)
                          .Where(l => l.StartsWith("012-000"))
                          .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                          .ToList();

                        if (nsuEstabelecimento.Count > 0)
                            fVenda.vendaNumeroNSU = Convert.ToInt64(nsuEstabelecimento[0]).ToString();

                        var vendaPendente = new BeneficioNovartisPendente(
                            fVenda.vendaNumeroNSU,
                            cmbOperadoras.Text,
                            "V",
                            DateTime.Now,
                            Principal.nomeEstacao,
                            DateTime.Now,
                            Principal.usuario
                            );

                        double precoBruto = 0;
                        double precoLiquido = 0;
                        double valorReceber = 0;
                        BancoDados.AbrirTrans();

                        var qtdeAutorizada = File.ReadAllLines(caminho)
                         .Where(l => l.StartsWith("901-000"))
                         .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                         .ToList();

                        if (!vendaPendente.InsereRegistros(vendaPendente))
                        {
                            fVenda.vendaBeneficio = "";
                            fVenda.vendaNumeroNSU = "";
                            return;
                        }

                        for (int i = 0; i < Convert.ToInt32(qtdeAutorizada[0]); i++)
                        {
                            var codBarras = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("902-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var qtde = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("905-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var bruto = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("910-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var liquido = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("911-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var receber = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("911-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var desconto = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("909-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            var codRetorno = File.ReadAllLines(caminho)
                                .Where(l => l.StartsWith("919-" + Funcoes.FormataZeroAEsquerda(i.ToString(), 3)))
                                .Select(l => l.Substring(l.LastIndexOf("=") + 2))
                                .ToList();

                            double valorSubsidio = Convert.ToDouble(liquido[0]) - Convert.ToDouble(receber[0]);
                            valorSubsidio = (valorSubsidio * 100) / Convert.ToDouble(bruto[0]);

                            string statusMedicamento = "";
                            if (Convert.ToInt32(codRetorno[0]) >= 0 && Convert.ToInt32(codRetorno[0]) <= 9 && Convert.ToInt32(codRetorno[0]) != 41)
                            {
                                statusMedicamento = "A";
                            }
                            else if (Convert.ToInt32(codRetorno[0]) == 41)
                            {
                                statusMedicamento = "S";
                            }
                            else if (Convert.ToInt32(codRetorno[0]) > 9)
                            {
                                statusMedicamento = "R";
                            }

                            if (statusMedicamento != "R")
                            {
                                var produtos = new ProdutoDetalhe();
                                DataTable dadosProdutos = produtos.BuscaDepClasSub(Principal.empAtual, Principal.estAtual, codBarras[0].ToString());

                                if (dadosProdutos.Rows.Count > 0)
                                {
                                    double preValor = Convert.ToDouble(dadosProdutos.Rows[0]["PRE_VALOR"]);
                                    double preBruto = ((Convert.ToDouble(bruto[0]) / 100) * Convert.ToInt32(qtde[0])) / Convert.ToInt32(qtde[0]);
                                    if (preValor != preBruto)
                                    {
                                        preBruto = preValor;
                                    }

                                    double porDesconto = (Convert.ToDouble(desconto[0]) / 100) * -1;

                                    dadosProdutos = Funcoes.VerificaProdDesconto(Principal.estAtual, Principal.wsConID, Convert.ToInt32(dadosProdutos.Rows[0]["PROD_ID"]), Convert.ToInt32(dadosProdutos.Rows[0]["DEP_CODIGO"]),
                                        Convert.ToInt32(dadosProdutos.Rows[0]["CLAS_CODIGO"]), Convert.ToInt32(dadosProdutos.Rows[0]["SUB_CODIGO"]));
                                    if (dadosProdutos.Rows.Count != 0)
                                    {
                                        if ((Convert.ToDouble(desconto[0]) / 100) < Convert.ToDouble(dadosProdutos.Rows[0]["DESC_MAX"]))
                                        {
                                            porDesconto = Convert.ToDouble(dadosProdutos.Rows[0]["DESC_MAX"]) * -1;
                                        }
                                    }

                                    double valorDesconto = Math.Round((preBruto * Math.Abs(porDesconto)) / 100, 2);
                                    double preLiquido = Math.Round(preBruto - valorDesconto, 2);

                                    precoBruto += preBruto * Convert.ToInt32(qtde[0]);
                                    precoLiquido += preLiquido * Convert.ToInt32(qtde[0]);
                                    valorReceber += preLiquido * Convert.ToInt32(qtde[0]);

                                    var produtosAutorizados = new BeneficioNovartisProdutos(
                                    fVenda.vendaNumeroNSU,
                                    0,
                                    codBarras[0].ToString(),
                                    Convert.ToInt32(qtde[0]),
                                    preBruto,
                                    preLiquido,
                                    preLiquido,
                                    porDesconto,
                                    statusMedicamento,
                                    "",
                                    DateTime.Now,
                                    valorSubsidio,
                                    "N",
                                    DateTime.Now,
                                    Principal.usuario
                                    );

                                    if (!produtosAutorizados.InsereRegistros(produtosAutorizados))
                                    {
                                        BancoDados.ErroTrans();
                                        fVenda.vendaBeneficio = "";
                                        fVenda.vendaNumeroNSU = "";
                                        return;
                                    }
                                }else
                                {
                                    MessageBox.Show("PRODUTO: " + codBarras[0].ToString() + "\nNão cadastrado, verifique o cadastro primeiro.", "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }

                        var dadosAutorizacao = new BeneficioNovartisEfetivado(
                          fVenda.vendaNumeroNSU,
                          0,
                          "",
                          "",
                          precoBruto,
                          precoLiquido,
                          "",
                          cmbOperadoras.Text,
                          valorReceber,
                          DateTime.Now,
                          "A",
                          DateTime.Now,
                          Principal.usuario
                          );

                        if (!dadosAutorizacao.InsereRegistros(dadosAutorizacao))
                        {
                            BancoDados.ErroTrans();
                            fVenda.vendaBeneficio = "";
                            fVenda.vendaNumeroNSU = "";
                            return;
                        }

                        BancoDados.FecharTrans();

                        cmbOperadoras.Enabled = false;
                        txtNSU.Enabled = false;
                        btnConsultar.Enabled = false;
                        lblMensagem.Text = "";
                        btnEfetivar.Enabled = true;

                        var buscaProdutos = new Negocio.BeneficioNovartisProdutos();
                        DataTable dtRetorno = buscaProdutos.BuscaProdutosPorNsuSomatorio(fVenda.vendaNumeroNSU);

                        dgProdutos.DataSource = dtRetorno;

                        lblMensagem.Text = "Verifique se os Medicamentos e suas respectivas Quantidades e Descontos!";
                        lblMensagem.Refresh();

                        btnEfetivar.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnConsultar.PerformClick();
                    break;
                case Keys.F2:
                    btnLimpar.PerformClick();
                    break;
                case Keys.F3:
                    btnEfetivar.PerformClick();
                    break;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (cmbOperadoras.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar uma Operadora!";
                Funcoes.Avisa();
                cmbOperadoras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNSU.Text) && String.IsNullOrEmpty(txtCartaoCpf.Text))
            {
                Principal.mensagem = "Necessário informar algum critério de Busca!";
                Funcoes.Avisa();
                txtNSU.Focus();
                return false;
            }

            return true;
        }

        private void btnConsultar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnLimpar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnEfetivar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFechar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            fVenda.vendaBeneficio = "";
            fVenda.vendaNumeroNSU = "";
            this.Close();
        }

        private void btnConsultar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultar, "Consultar (F1)");
        }

        private void btnLimpar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnLimpar, "Limpar (F2)");
        }

        private void btnEfetivar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnEfetivar, "Efetivar (F3)");
        }

        private void btnFechar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFechar, "Fechar (ESC)");
        }

        private void btnEfetivar_Click(object sender, EventArgs e)
        {
            try
            {
                //if (String.IsNullOrEmpty(txtCartaoCpf.Text))
                //{
                //    fVenda.wsNumCartao = Interaction.InputBox("Digite o Número do Cartão", "Benefício Portal da Drogaria");
                //    if (String.IsNullOrEmpty(fVenda.wsNumCartao))
                //    {
                //        fVenda.wsNumCartao = Interaction.InputBox("Digite o Número do Cartão", "Benefício Portal da Drogaria");
                //    }

                //    lblMensagem.Text = "Esperando Efetivação da Venda!";
                //    lblMensagem.Refresh();
                //}
                //else
                //    fVenda.wsNumCartao = txtCartaoCpf.Text;

                fVenda.vendaBeneficio = "PORTALDROGARIA";

                this.Hide();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício Portal da Drogaria", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            lblMensagem.Text = "";
            cmbOperadoras.Enabled = true;
            txtNSU.Enabled = true;
            btnConsultar.Enabled = true;
            btnLimpar.Enabled = true;
            btnEfetivar.Enabled = false;

            cmbOperadoras.SelectedIndex = -1;
            cmbOperadoras.Focus();
        }

        private void txtCartaoCpf_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtCartaoCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtCartaoCpf.Text))
                {
                    txtCartaoCpf.Focus();
                }
                else
                {
                    btnConsultar.PerformClick();
                }
            }
        }
    }
}
