﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.VidaLink
{
    public partial class frmVidaLinkFinalizaVenda : Form
    {
        frmVenda fVenda;
        Negocio.Beneficios.VidaLink vendaVidaLink = new Negocio.Beneficios.VidaLink();

        public frmVidaLinkFinalizaVenda(frmVenda formVendas)
        {
            InitializeComponent();
            fVenda = formVendas;
        }

        private void frmVidaLinkFinalizaVenda_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                lblMensagem.Text = "Enviando Confirmação da Venda!";
                lblMensagem.Refresh();

                if(vendaVidaLink.EnviarVenda(fVenda.vendaNumeroNSU, fVenda.vendaNumeroNota))
                {
                    lblMensagem.Text = "Esperando Confirmação da Venda!";
                    lblMensagem.Refresh();

                    vendaVidaLink.RetonoDaVenda(out fVenda.cupomBeneficio);
                    
                    vendaVidaLink.LimparPastas();
                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
