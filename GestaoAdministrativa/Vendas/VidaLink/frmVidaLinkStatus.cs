﻿using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.VidaLink
{
    public partial class frmVidaLinkStatus : Form
    {
        double vlTotal, vlAVista, vlDesconto, vlReceber, vlSubsidio, vlReembolso, vlReceberCliente, vlPorcentagem, vlEconomizado, vlComissao, vlPercentual;

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fVenda.vendaBeneficio = "";
            fVenda.vendaNumeroNSU = "";
            this.Close();
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmVidaLinkStatus_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

        frmVenda fVenda;

        public frmVidaLinkStatus(frmVenda formVenda)
        {
            InitializeComponent();
            fVenda = formVenda;
        }

        private void frmVidaLinkStatus_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                vlTotal = 0;
                vlAVista = 0;
                vlDesconto = 0;
                vlReceber = 0;
                vlReembolso = 0;
                vlReceberCliente = 0;
                vlPorcentagem = 0;
                vlEconomizado = 0;
                vlComissao = 0;
                vlPercentual = 0;

                var dados = new BeneficioVidaLink();
                DataTable dtRetorno = dados.BuscaDadosDaVendaPorNsu(fVenda.vendaNumeroNSU);
                if (dtRetorno.Rows.Count > 0)
                {
                    lblAutorizacao.Text = fVenda.vendaNumeroNSU;
                    lblPlanoConvenio.Text = dtRetorno.Rows[0]["PLANO_CONVENIO"].ToString().ToUpper();
                    lblCartao.Text = dtRetorno.Rows[0]["CARTAO_CONVENIO"].ToString().ToUpper();
                    
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        if(Convert.ToInt32(dtRetorno.Rows[i]["TIPO_AUTORIZACAO"]).Equals(2) || Convert.ToInt32(dtRetorno.Rows[i]["TIPO_AUTORIZACAO"]).Equals(4))
                        {
                            vlTotal += Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlReceberCliente += Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlAVista += Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlReceber += Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_ARECEB"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlSubsidio += Convert.ToDouble(dtRetorno.Rows[i]["VL_SUBSIDIO"]);
                            vlReembolso += Convert.ToDouble(dtRetorno.Rows[i]["VL_REEMBOLSO"]);

                            if(Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) != 0)
                            {
                                vlEconomizado += (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) - (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                            }
                            else
                                vlEconomizado += (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) - Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_ARECEB"])) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);

                            if (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) > 0)
                            {
                                vlDesconto = (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * 100;
                                vlDesconto = vlDesconto / (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                vlPercentual = vlPercentual + Math.Round(-100 + Math.Round(vlDesconto));
                            }
                            else
                            {
                                vlDesconto = ((Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) - Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"])) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * 100;
                                vlDesconto = vlDesconto / (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                vlPercentual = Math.Round(vlDesconto);
                            }

                            vlPorcentagem += Convert.ToDouble(dtRetorno.Rows[i]["PORCENT_DESC"]);
                            vlComissao += Convert.ToDouble(dtRetorno.Rows[i]["COMISSAO_VLINK"]);
                        }
                        else
                        {
                            vlTotal += Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlReceberCliente += Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlAVista += Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlReceber += Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_ARECEB"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]);
                            vlSubsidio += Convert.ToDouble(dtRetorno.Rows[i]["VL_SUBSIDIO"]);
                            vlReembolso += Convert.ToDouble(dtRetorno.Rows[i]["VL_REEMBOLSO"]);

                            if (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) != 0)
                            {
                                vlEconomizado += (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) - (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                            }
                            else
                                vlEconomizado += (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) -
                                     ((Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) + Convert.ToDouble(dtRetorno.Rows[i]["VL_REEMBOLSO"])) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));

                            if (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) > 0)
                            {
                                vlDesconto = (Convert.ToDouble(dtRetorno.Rows[i]["PR_CLIENTE_AVISTA"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * 100;
                                vlDesconto = vlDesconto / (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                vlPercentual = vlPercentual + Math.Round(-100 + Math.Round(vlDesconto));
                            }
                            else
                            {
                                vlDesconto = ((Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) - Convert.ToDouble(dtRetorno.Rows[i]["PR_VENDA"])) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"])) * 100;
                                vlDesconto = vlDesconto / (Convert.ToDouble(dtRetorno.Rows[i]["PR_MAX"]) * Convert.ToInt32(dtRetorno.Rows[i]["QTD"]));
                                vlPercentual = Math.Round(vlDesconto);
                            }

                            vlPorcentagem += Convert.ToDouble(dtRetorno.Rows[i]["PORCENT_DESC"]);
                            vlComissao += Convert.ToDouble(dtRetorno.Rows[i]["COMISSAO_VLINK"]);
                        }
                    }

                    lblTotalVenda.Text = String.Format("{0:N}", vlTotal);
                    lblValorFarmacia.Text = String.Format("{0:N}", vlAVista);
                    lblDescontoFolha.Text = String.Format("{0:N}", vlReceber);
                    lblValorEconomizado.Text = String.Format("{0:N}", vlEconomizado);
                    lblSubsidioLoja.Text = String.Format("{0:N}", vlSubsidio);
                    lblReembolso.Text = String.Format("{0:N}", vlReembolso);
                    lblReposicao.Text = String.Format("{0:N}", vlPorcentagem / dtRetorno.Rows.Count);
                    lblComissao.Text = String.Format("{0:N}", vlComissao / dtRetorno.Rows.Count);
                    lblDesconto.Text = String.Format("{0:N}", vlPercentual / dtRetorno.Rows.Count);
                    lblReceber.Text = String.Format("{0:N}", vlReceberCliente);
                    
                }
                btnConfirmar.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
