﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.VidaLink
{
    public partial class frmVidaLinkAutorizacao : Form
    {
        private frmVenda fVenda;
        private BeneficioVidaLinkComanda dadosComanda = new BeneficioVidaLinkComanda();
        private BeneficioVidaLink dadosVendas = new BeneficioVidaLink();
        private BeneficioVidaLinkProdutos dadosProdutos = new BeneficioVidaLinkProdutos();
        private DataTable dtRetorno = new DataTable();
        Negocio.Beneficios.VidaLink vendaVidaLink = new Negocio.Beneficios.VidaLink();


        public frmVidaLinkAutorizacao(frmVenda frmVenda)
        {
            InitializeComponent();
            fVenda = frmVenda;
        }

        private void frmVidaLinkAutorizacao_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensagem.Text = "";
                dtRetorno = dadosComanda.BuscaRegistrosDeComanda();
                for(int i=0; i < dtRetorno.Rows.Count; i++)
                {
                    if(!String.IsNullOrEmpty(dadosVendas.IdentificaVendaVidaLink(dtRetorno.Rows[i]["NSU"].ToString(), Convert.ToInt64(dtRetorno.Rows[i]["VENDA_ID"]))))
                    {
                        dadosVendas.ExcluirDadosPorNSU(dtRetorno.Rows[i]["NSU"].ToString(), Convert.ToInt64(dtRetorno.Rows[i]["VENDA_ID"]));

                        dadosProdutos.ExcluirDadosPorNSU(dtRetorno.Rows[i]["NSU"].ToString(), Convert.ToInt64(dtRetorno.Rows[i]["VENDA_ID"]));

                        dadosComanda.ExcluirDadosPorNSU(dtRetorno.Rows[i]["NSU"].ToString(), Convert.ToInt64(dtRetorno.Rows[i]["VENDA_ID"]));
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVidaLinkAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnAutorizacao.PerformClick();
                    break;
            }
        }

        private void txtAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnAutorizacao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnAutorizacao_Click(object sender, EventArgs e)
        {
            try
            {
                if(!string.IsNullOrEmpty(txtAutorizacao.Text.Trim()))
                {
                    Cursor = Cursors.WaitCursor;
                    if (!String.IsNullOrEmpty(dadosVendas.IdentificaVendaVidaLinkPorStatusENsu(txtAutorizacao.Text,"C")))
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Autorização já Concluída!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtAutorizacao.Text = "";
                        txtAutorizacao.Focus();
                        return;
                    }

                    string retorno = dadosVendas.IdentificaVendaVidaLinkPorStatusENsu(txtAutorizacao.Text, "P");
                    if (!String.IsNullOrEmpty(retorno))
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Autorização cadastrada no banco de dados!\nNão será possível reutilizá-lo.", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        if(retorno.Equals(Principal.nomeEstacao))
                        {
                            dadosVendas.ExcluirDadosPorNSU(txtAutorizacao.Text);

                            dadosProdutos.ExcluirDadosPorNSU(txtAutorizacao.Text);
                        }

                        MessageBox.Show("Autorização excluída, pois a mesma não foi concluída!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        txtAutorizacao.Text = "";
                        txtAutorizacao.Focus();
                        return;
                    }
                    Cursor = Cursors.WaitCursor;

                    lblMensagem.Text = "Enviando Teste de Atividade!";
                    lblMensagem.Refresh();

                    vendaVidaLink.LimparPastas();

                    if(vendaVidaLink.EnvioTesteDeAtividade())
                    {
                        lblMensagem.Text = "Retorno Teste de Atividade!";
                        lblMensagem.Refresh();

                        if (vendaVidaLink.RetornoTesteDeAtividade(out fVenda.numeroSequencia))
                        {
                            lblMensagem.Text = "Consultando Autorização!";
                            lblMensagem.Refresh();

                            vendaVidaLink.LimparPastas();

                            if(vendaVidaLink.EnviaConsulta(txtAutorizacao.Text.Trim()))
                            {
                                lblMensagem.Text = "Retorno da Consulta da Autorização!";
                                lblMensagem.Refresh();

                                if (vendaVidaLink.RetornoConsulta(out fVenda.vendaNumeroNSU))
                                {
                                    fVenda.vendaBeneficio = "VIDALINK";
                                    vendaVidaLink.LimparPastas();
                                    this.Hide();
                                    this.Close();
                                    frmVidaLinkRetornoProdutos retornoProduto = new VidaLink.frmVidaLinkRetornoProdutos(fVenda);
                                    retornoProduto.ShowDialog();
                                }
                            }
                        }
                    }

                    lblMensagem.Text = "";
                    lblMensagem.Refresh();
                }
                else
                {
                    MessageBox.Show("Número de Autorização não pode ser em Branco!", "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAutorizacao.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void txtAutorizacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAutorizacao.PerformClick();
        }
    }
}
