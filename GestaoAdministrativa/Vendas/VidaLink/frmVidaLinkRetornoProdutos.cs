﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Vendas.VidaLink
{
    public partial class frmVidaLinkRetornoProdutos : Form
    {
        frmVenda fVenda;

        public frmVidaLinkRetornoProdutos(frmVenda formVenda)
        {
            InitializeComponent();
            fVenda = formVenda;
        }

        private void frmVidaLinkRetornoProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                var buscaDados = new BeneficioVidaLinkProdutos(); 
                DataTable dtRetorno = buscaDados.BuscaProdutosPorNSU(fVenda.vendaNumeroNSU);

                dgProdutosVidaLink.DataSource = dtRetorno;

                double receber = 0;
                double receberCliente = 0;

                for(int i=0; i<dgProdutosVidaLink.RowCount; i++)
                {
                    receberCliente += Convert.ToDouble(dgProdutosVidaLink.Rows[i].Cells[5].Value) * Convert.ToInt32(dgProdutosVidaLink.Rows[i].Cells[0].Value);
                    receber += Convert.ToDouble(dgProdutosVidaLink.Rows[i].Cells[6].Value) * Convert.ToInt32(dgProdutosVidaLink.Rows[i].Cells[0].Value);
                }

                lblCliente.Text =  "R$:  " + String.Format("{0:N}", receberCliente);
                lblCliente.Refresh();
                lblDesconto.Text = "R$:  " + String.Format("{0:N}", receber);
                lblDesconto.Refresh();

                if (dgProdutosVidaLink.Rows[0].Cells[8].Value.ToString().Equals("2") || dgProdutosVidaLink.Rows[0].Cells[8].Value.ToString().Equals("4"))
                {
                    fVenda.bloqueiaProduto = true;
                    lblMensagem.Text = "NÃO É PERMITO INCLUSÃO DE PRODUTOS PARA ESTA VENDA!";
                }
                else if (dgProdutosVidaLink.Rows[0].Cells[8].Value.ToString().Equals("1") || dgProdutosVidaLink.Rows[0].Cells[8].Value.ToString().Equals("3"))
                {
                    fVenda.bloqueiaProduto = false;
                    lblMensagem.Text = "É PERMITO INCLUSÃO DE PRODUTOS PARA ESTA VENDA!";
                }
                else if(dgProdutosVidaLink.Rows[0].Cells[8].Value.ToString().Equals("0"))
                {
                    fVenda.bloqueiaProduto = false;
                    lblMensagem.Text = "VENDA À VISTA!";
                }
                lblMensagem.Refresh();
                btnConfirmar.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmVidaLinkRetornoProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void dgProdutosVidaLink_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    fVenda.vendaBeneficio = "";
                    fVenda.vendaNumeroNSU = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            fVenda.vendaBeneficio = "";
            fVenda.vendaNumeroNSU = "";
            this.Close();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                this.Close();
                frmVidaLinkStatus statusVenda = new VidaLink.frmVidaLinkStatus(fVenda);
                statusVenda.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Benefício VidaLink", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
