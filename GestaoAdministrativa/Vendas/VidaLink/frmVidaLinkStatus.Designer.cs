﻿namespace GestaoAdministrativa.Vendas.VidaLink
{
    partial class frmVidaLinkStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVidaLinkStatus));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblReceber = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.lblComissao = new System.Windows.Forms.Label();
            this.lblReposicao = new System.Windows.Forms.Label();
            this.lblReembolso = new System.Windows.Forms.Label();
            this.lblSubsidioLoja = new System.Windows.Forms.Label();
            this.lblDescontoFolha = new System.Windows.Forms.Label();
            this.lblValorEconomizado = new System.Windows.Forms.Label();
            this.lblTotalVenda = new System.Windows.Forms.Label();
            this.lblValorFarmacia = new System.Windows.Forms.Label();
            this.lblCartao = new System.Windows.Forms.Label();
            this.lblPlanoConvenio = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblAutorizacao = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(5, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(510, 581);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status da Venda VidaLink";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnConfirmar);
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 556);
            this.panel1.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(147, 504);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(134, 47);
            this.btnConfirmar.TabIndex = 13;
            this.btnConfirmar.Text = "Confirmar (F2)";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            this.btnConfirmar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConfirmar_KeyDown);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(7, 504);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(134, 47);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "Cancelar (F1)";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            this.btnCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancelar_KeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblReceber);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.lblDesconto);
            this.groupBox3.Controls.Add(this.lblComissao);
            this.groupBox3.Controls.Add(this.lblReposicao);
            this.groupBox3.Controls.Add(this.lblReembolso);
            this.groupBox3.Controls.Add(this.lblSubsidioLoja);
            this.groupBox3.Controls.Add(this.lblDescontoFolha);
            this.groupBox3.Controls.Add(this.lblValorEconomizado);
            this.groupBox3.Controls.Add(this.lblTotalVenda);
            this.groupBox3.Controls.Add(this.lblValorFarmacia);
            this.groupBox3.Controls.Add(this.lblCartao);
            this.groupBox3.Controls.Add(this.lblPlanoConvenio);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(7, 44);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(481, 348);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // lblReceber
            // 
            this.lblReceber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblReceber.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceber.ForeColor = System.Drawing.Color.Red;
            this.lblReceber.Location = new System.Drawing.Point(229, 151);
            this.lblReceber.Name = "lblReceber";
            this.lblReceber.Size = new System.Drawing.Size(248, 19);
            this.lblReceber.TabIndex = 24;
            this.lblReceber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label14.Location = new System.Drawing.Point(8, 151);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(227, 19);
            this.label14.TabIndex = 23;
            this.label14.Text = "Vl. Receber do Cliente...........:";
            // 
            // lblDesconto
            // 
            this.lblDesconto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesconto.ForeColor = System.Drawing.Color.Red;
            this.lblDesconto.Location = new System.Drawing.Point(227, 322);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(248, 19);
            this.lblDesconto.TabIndex = 22;
            this.lblDesconto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblComissao
            // 
            this.lblComissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComissao.ForeColor = System.Drawing.Color.Red;
            this.lblComissao.Location = new System.Drawing.Point(227, 295);
            this.lblComissao.Name = "lblComissao";
            this.lblComissao.Size = new System.Drawing.Size(248, 19);
            this.lblComissao.TabIndex = 21;
            this.lblComissao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReposicao
            // 
            this.lblReposicao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReposicao.ForeColor = System.Drawing.Color.Red;
            this.lblReposicao.Location = new System.Drawing.Point(227, 266);
            this.lblReposicao.Name = "lblReposicao";
            this.lblReposicao.Size = new System.Drawing.Size(248, 19);
            this.lblReposicao.TabIndex = 20;
            this.lblReposicao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReembolso
            // 
            this.lblReembolso.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReembolso.ForeColor = System.Drawing.Color.Red;
            this.lblReembolso.Location = new System.Drawing.Point(228, 237);
            this.lblReembolso.Name = "lblReembolso";
            this.lblReembolso.Size = new System.Drawing.Size(248, 19);
            this.lblReembolso.TabIndex = 19;
            this.lblReembolso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSubsidioLoja
            // 
            this.lblSubsidioLoja.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubsidioLoja.ForeColor = System.Drawing.Color.Red;
            this.lblSubsidioLoja.Location = new System.Drawing.Point(228, 206);
            this.lblSubsidioLoja.Name = "lblSubsidioLoja";
            this.lblSubsidioLoja.Size = new System.Drawing.Size(248, 19);
            this.lblSubsidioLoja.TabIndex = 18;
            this.lblSubsidioLoja.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescontoFolha
            // 
            this.lblDescontoFolha.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescontoFolha.ForeColor = System.Drawing.Color.Red;
            this.lblDescontoFolha.Location = new System.Drawing.Point(228, 178);
            this.lblDescontoFolha.Name = "lblDescontoFolha";
            this.lblDescontoFolha.Size = new System.Drawing.Size(248, 19);
            this.lblDescontoFolha.TabIndex = 17;
            this.lblDescontoFolha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblValorEconomizado
            // 
            this.lblValorEconomizado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblValorEconomizado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorEconomizado.ForeColor = System.Drawing.Color.Red;
            this.lblValorEconomizado.Location = new System.Drawing.Point(228, 122);
            this.lblValorEconomizado.Name = "lblValorEconomizado";
            this.lblValorEconomizado.Size = new System.Drawing.Size(248, 19);
            this.lblValorEconomizado.TabIndex = 16;
            this.lblValorEconomizado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalVenda
            // 
            this.lblTotalVenda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblTotalVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalVenda.ForeColor = System.Drawing.Color.Red;
            this.lblTotalVenda.Location = new System.Drawing.Point(228, 94);
            this.lblTotalVenda.Name = "lblTotalVenda";
            this.lblTotalVenda.Size = new System.Drawing.Size(248, 19);
            this.lblTotalVenda.TabIndex = 15;
            this.lblTotalVenda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblValorFarmacia
            // 
            this.lblValorFarmacia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblValorFarmacia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorFarmacia.ForeColor = System.Drawing.Color.Red;
            this.lblValorFarmacia.Location = new System.Drawing.Point(228, 66);
            this.lblValorFarmacia.Name = "lblValorFarmacia";
            this.lblValorFarmacia.Size = new System.Drawing.Size(248, 19);
            this.lblValorFarmacia.TabIndex = 14;
            this.lblValorFarmacia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCartao
            // 
            this.lblCartao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCartao.ForeColor = System.Drawing.Color.Red;
            this.lblCartao.Location = new System.Drawing.Point(228, 38);
            this.lblCartao.Name = "lblCartao";
            this.lblCartao.Size = new System.Drawing.Size(248, 19);
            this.lblCartao.TabIndex = 13;
            this.lblCartao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPlanoConvenio
            // 
            this.lblPlanoConvenio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlanoConvenio.ForeColor = System.Drawing.Color.Red;
            this.lblPlanoConvenio.Location = new System.Drawing.Point(228, 12);
            this.lblPlanoConvenio.Name = "lblPlanoConvenio";
            this.lblPlanoConvenio.Size = new System.Drawing.Size(248, 19);
            this.lblPlanoConvenio.TabIndex = 12;
            this.lblPlanoConvenio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label12.Location = new System.Drawing.Point(7, 322);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(225, 19);
            this.label12.TabIndex = 10;
            this.label12.Text = "% Desconto.............................:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label11.Location = new System.Drawing.Point(7, 295);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(225, 19);
            this.label11.TabIndex = 9;
            this.label11.Text = "% Comissão (Vida Link).......:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label10.Location = new System.Drawing.Point(7, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(225, 19);
            this.label10.TabIndex = 8;
            this.label10.Text = "% Reposição (Farmácia)......:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label9.Location = new System.Drawing.Point(7, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(225, 19);
            this.label9.TabIndex = 7;
            this.label9.Text = "Vl. Reembolso.........................:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label8.Location = new System.Drawing.Point(6, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(227, 19);
            this.label8.TabIndex = 6;
            this.label8.Text = "Vl. Subsídio Loja.....................:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label7.Location = new System.Drawing.Point(7, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(227, 19);
            this.label7.TabIndex = 5;
            this.label7.Text = "Vl. Desc. em Folha..................:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label6.Location = new System.Drawing.Point(7, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(228, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "Vl. Economizado.....................:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label5.Location = new System.Drawing.Point(7, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "Vl. Total da Venda...................:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label4.Location = new System.Drawing.Point(6, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(227, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "Vl. da Farmácia........................:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(7, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Cartão........................................:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label2.Location = new System.Drawing.Point(6, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Plano do Convênio................:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblAutorizacao);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 42);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // lblAutorizacao
            // 
            this.lblAutorizacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutorizacao.ForeColor = System.Drawing.Color.Red;
            this.lblAutorizacao.Location = new System.Drawing.Point(227, 14);
            this.lblAutorizacao.Name = "lblAutorizacao";
            this.lblAutorizacao.Size = new System.Drawing.Size(248, 19);
            this.lblAutorizacao.TabIndex = 11;
            this.lblAutorizacao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Autorização.............................:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(97)))), ((int)(((byte)(155)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(371, 394);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(118, 157);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // frmVidaLinkStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(182)))), ((int)(((byte)(146)))));
            this.ClientSize = new System.Drawing.Size(518, 585);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVidaLinkStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVidaLinkStatus";
            this.Load += new System.EventHandler(this.frmVidaLinkStatus_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVidaLinkStatus_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.Label lblComissao;
        private System.Windows.Forms.Label lblReposicao;
        private System.Windows.Forms.Label lblReembolso;
        private System.Windows.Forms.Label lblSubsidioLoja;
        private System.Windows.Forms.Label lblDescontoFolha;
        private System.Windows.Forms.Label lblValorEconomizado;
        private System.Windows.Forms.Label lblTotalVenda;
        private System.Windows.Forms.Label lblValorFarmacia;
        private System.Windows.Forms.Label lblCartao;
        private System.Windows.Forms.Label lblPlanoConvenio;
        private System.Windows.Forms.Label lblAutorizacao;
        private System.Windows.Forms.Label lblReceber;
        private System.Windows.Forms.Label label14;
    }
}