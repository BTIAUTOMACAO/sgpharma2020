﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsulta));
            this.gbEnviarSolicitacao = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtpDataReceita = new System.Windows.Forms.DateTimePicker();
            this.cmbUFMedico = new System.Windows.Forms.ComboBox();
            this.txtCrmMedico = new System.Windows.Forms.TextBox();
            this.txtCpfCliente = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNomeCliente = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCnpjFarmacia = new System.Windows.Forms.MaskedTextBox();
            this.txtCpfVendedor = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbEnviarSolicitacao.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbEnviarSolicitacao
            // 
            this.gbEnviarSolicitacao.Controls.Add(this.panel1);
            this.gbEnviarSolicitacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEnviarSolicitacao.ForeColor = System.Drawing.Color.White;
            this.gbEnviarSolicitacao.Location = new System.Drawing.Point(5, 3);
            this.gbEnviarSolicitacao.Name = "gbEnviarSolicitacao";
            this.gbEnviarSolicitacao.Size = new System.Drawing.Size(520, 288);
            this.gbEnviarSolicitacao.TabIndex = 0;
            this.gbEnviarSolicitacao.TabStop = false;
            this.gbEnviarSolicitacao.Text = "Envio de Solicitação";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnGravar);
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Location = new System.Drawing.Point(6, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(508, 259);
            this.panel1.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtpDataReceita);
            this.groupBox3.Controls.Add(this.cmbUFMedico);
            this.groupBox3.Controls.Add(this.txtCrmMedico);
            this.groupBox3.Controls.Add(this.txtCpfCliente);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtNomeCliente);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(7, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(493, 117);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Solicitação";
            // 
            // dtpDataReceita
            // 
            this.dtpDataReceita.Checked = false;
            this.dtpDataReceita.CustomFormat = "";
            this.dtpDataReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataReceita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataReceita.Location = new System.Drawing.Point(22, 85);
            this.dtpDataReceita.Name = "dtpDataReceita";
            this.dtpDataReceita.Size = new System.Drawing.Size(123, 22);
            this.dtpDataReceita.TabIndex = 5;
            this.dtpDataReceita.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpDataReceita_KeyDown);
            this.dtpDataReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpDataReceita_KeyPress);
            // 
            // cmbUFMedico
            // 
            this.cmbUFMedico.BackColor = System.Drawing.Color.White;
            this.cmbUFMedico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUFMedico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUFMedico.ForeColor = System.Drawing.Color.Black;
            this.cmbUFMedico.FormattingEnabled = true;
            this.cmbUFMedico.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cmbUFMedico.Location = new System.Drawing.Point(342, 83);
            this.cmbUFMedico.Name = "cmbUFMedico";
            this.cmbUFMedico.Size = new System.Drawing.Size(92, 24);
            this.cmbUFMedico.TabIndex = 7;
            this.cmbUFMedico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUFMedico_KeyDown);
            // 
            // txtCrmMedico
            // 
            this.txtCrmMedico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrmMedico.ForeColor = System.Drawing.Color.Black;
            this.txtCrmMedico.Location = new System.Drawing.Point(157, 85);
            this.txtCrmMedico.Name = "txtCrmMedico";
            this.txtCrmMedico.Size = new System.Drawing.Size(167, 22);
            this.txtCrmMedico.TabIndex = 6;
            this.txtCrmMedico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCrmMedico_KeyDown);
            this.txtCrmMedico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCrmMedico_KeyPress);
            // 
            // txtCpfCliente
            // 
            this.txtCpfCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCpfCliente.Location = new System.Drawing.Point(22, 39);
            this.txtCpfCliente.Mask = "000,000,000-00";
            this.txtCpfCliente.Name = "txtCpfCliente";
            this.txtCpfCliente.Size = new System.Drawing.Size(123, 22);
            this.txtCpfCliente.TabIndex = 3;
            this.txtCpfCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCpfCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCpfCliente_KeyDown);
            this.txtCpfCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCpfCliente_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(339, 64);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 16);
            this.label17.TabIndex = 5;
            this.label17.Text = "UF do Médico";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(154, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 16);
            this.label15.TabIndex = 3;
            this.label15.Text = "CRM do Médico";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(19, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 16);
            this.label14.TabIndex = 2;
            this.label14.Text = "Data da Receita";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(154, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 16);
            this.label13.TabIndex = 1;
            this.label13.Text = "Nome";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(21, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "CPF";
            // 
            // txtNomeCliente
            // 
            this.txtNomeCliente.BackColor = System.Drawing.Color.White;
            this.txtNomeCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNomeCliente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeCliente.ForeColor = System.Drawing.Color.Black;
            this.txtNomeCliente.Location = new System.Drawing.Point(157, 39);
            this.txtNomeCliente.Name = "txtNomeCliente";
            this.txtNomeCliente.ReadOnly = true;
            this.txtNomeCliente.Size = new System.Drawing.Size(330, 22);
            this.txtNomeCliente.TabIndex = 4;
            this.txtNomeCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNomeCliente_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCnpjFarmacia);
            this.groupBox2.Controls.Add(this.txtCpfVendedor);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(8, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(492, 75);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados da Farmácia";
            // 
            // txtCnpjFarmacia
            // 
            this.txtCnpjFarmacia.BackColor = System.Drawing.Color.White;
            this.txtCnpjFarmacia.ForeColor = System.Drawing.Color.Black;
            this.txtCnpjFarmacia.Location = new System.Drawing.Point(21, 41);
            this.txtCnpjFarmacia.Mask = "00,000,000/0000-00";
            this.txtCnpjFarmacia.Name = "txtCnpjFarmacia";
            this.txtCnpjFarmacia.ReadOnly = true;
            this.txtCnpjFarmacia.Size = new System.Drawing.Size(123, 22);
            this.txtCnpjFarmacia.TabIndex = 1;
            this.txtCnpjFarmacia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCnpjFarmacia_KeyDown);
            // 
            // txtCpfVendedor
            // 
            this.txtCpfVendedor.BackColor = System.Drawing.Color.White;
            this.txtCpfVendedor.ForeColor = System.Drawing.Color.Black;
            this.txtCpfVendedor.Location = new System.Drawing.Point(156, 41);
            this.txtCpfVendedor.Mask = "000,000,000-00";
            this.txtCpfVendedor.Name = "txtCpfVendedor";
            this.txtCpfVendedor.ReadOnly = true;
            this.txtCpfVendedor.Size = new System.Drawing.Size(123, 22);
            this.txtCpfVendedor.TabIndex = 2;
            this.txtCpfVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCpfVendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCpfVendedor_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(153, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "CPF do Vendedor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(18, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "CNPJ";
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.ForeColor = System.Drawing.Color.Black;
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(387, 208);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(113, 42);
            this.btnGravar.TabIndex = 8;
            this.btnGravar.Text = "Enviar (F2)";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            this.btnGravar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnGravar_KeyDown);
            this.btnGravar.MouseHover += new System.EventHandler(this.btnGravar_MouseHover);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(249, 208);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(132, 42);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar (F1)";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancela_Click);
            this.btnCancelar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancela_KeyDown);
            this.btnCancelar.MouseHover += new System.EventHandler(this.btnCancela_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 198);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(104, 52);
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // frmConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(87)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(528, 293);
            this.Controls.Add(this.gbEnviarSolicitacao);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmConsulta_FormClosed);
            this.Load += new System.EventHandler(this.frmConsulta_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmConsulta_KeyDown);
            this.gbEnviarSolicitacao.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtCpfCliente;
        private System.Windows.Forms.TextBox txtNomeCliente;
        private System.Windows.Forms.TextBox txtCrmMedico;
        private System.Windows.Forms.ComboBox cmbUFMedico;
        public System.Windows.Forms.GroupBox gbEnviarSolicitacao;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtCnpjFarmacia;
        private System.Windows.Forms.MaskedTextBox txtCpfVendedor;
        private System.Windows.Forms.DateTimePicker dtpDataReceita;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}