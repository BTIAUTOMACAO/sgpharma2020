﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenDevolucaoProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenDevolucaoProdutos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTroco = new System.Windows.Forms.Label();
            this.lblTiTroco = new System.Windows.Forms.Label();
            this.btnConfirmarCaixa = new System.Windows.Forms.Button();
            this.gbEspecies = new System.Windows.Forms.GroupBox();
            this.dgEspecies = new System.Windows.Forms.DataGridView();
            this.ESP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONSIDERA_ESP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValorEsp = new System.Windows.Forms.TextBox();
            this.cmbEspecies = new System.Windows.Forms.ComboBox();
            this.txtEspID = new System.Windows.Forms.TextBox();
            this.gbFormaPagamento = new System.Windows.Forms.GroupBox();
            this.dgFormas = new System.Windows.Forms.DataGridView();
            this.CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.cmbForma = new System.Windows.Forms.ComboBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnConfirma = new System.Windows.Forms.Button();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.lblDiferenca = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTotalTroca = new System.Windows.Forms.Label();
            this.lblTotalDev = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtIdentificacao = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbEspecies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecies)).BeginInit();
            this.gbFormaPagamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFormas)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblTroco);
            this.panel2.Controls.Add(this.lblTiTroco);
            this.panel2.Controls.Add(this.btnConfirmarCaixa);
            this.panel2.Controls.Add(this.gbEspecies);
            this.panel2.Controls.Add(this.gbFormaPagamento);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Location = new System.Drawing.Point(7, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 495);
            this.panel2.TabIndex = 43;
            // 
            // lblTroco
            // 
            this.lblTroco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTroco.ForeColor = System.Drawing.Color.Black;
            this.lblTroco.Location = new System.Drawing.Point(842, 334);
            this.lblTroco.Name = "lblTroco";
            this.lblTroco.Size = new System.Drawing.Size(103, 15);
            this.lblTroco.TabIndex = 14;
            this.lblTroco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTroco.Visible = false;
            // 
            // lblTiTroco
            // 
            this.lblTiTroco.AutoSize = true;
            this.lblTiTroco.Location = new System.Drawing.Point(792, 334);
            this.lblTiTroco.Name = "lblTiTroco";
            this.lblTiTroco.Size = new System.Drawing.Size(44, 16);
            this.lblTiTroco.TabIndex = 13;
            this.lblTiTroco.Text = "Troco:";
            this.lblTiTroco.Visible = false;
            // 
            // btnConfirmarCaixa
            // 
            this.btnConfirmarCaixa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmarCaixa.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmarCaixa.Image")));
            this.btnConfirmarCaixa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmarCaixa.Location = new System.Drawing.Point(792, 431);
            this.btnConfirmarCaixa.Name = "btnConfirmarCaixa";
            this.btnConfirmarCaixa.Size = new System.Drawing.Size(153, 52);
            this.btnConfirmarCaixa.TabIndex = 4;
            this.btnConfirmarCaixa.Text = "      Confirmar Oper. Caixa (F2)";
            this.btnConfirmarCaixa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmarCaixa.UseVisualStyleBackColor = true;
            this.btnConfirmarCaixa.Visible = false;
            this.btnConfirmarCaixa.Click += new System.EventHandler(this.btnConfirmarCaixa_Click);
            // 
            // gbEspecies
            // 
            this.gbEspecies.Controls.Add(this.dgEspecies);
            this.gbEspecies.Controls.Add(this.txtValorEsp);
            this.gbEspecies.Controls.Add(this.cmbEspecies);
            this.gbEspecies.Controls.Add(this.txtEspID);
            this.gbEspecies.Location = new System.Drawing.Point(399, 314);
            this.gbEspecies.Name = "gbEspecies";
            this.gbEspecies.Size = new System.Drawing.Size(387, 169);
            this.gbEspecies.TabIndex = 3;
            this.gbEspecies.TabStop = false;
            this.gbEspecies.Text = "Espécies";
            this.gbEspecies.Visible = false;
            // 
            // dgEspecies
            // 
            this.dgEspecies.AllowUserToAddRows = false;
            this.dgEspecies.AllowUserToResizeColumns = false;
            this.dgEspecies.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecies.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgEspecies.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecies.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgEspecies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEspecies.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESP_CODIGO,
            this.ESP_DESCRICAO,
            this.dataGridViewTextBoxColumn9,
            this.EXC,
            this.CONSIDERA_ESP});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEspecies.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgEspecies.GridColor = System.Drawing.Color.Black;
            this.dgEspecies.Location = new System.Drawing.Point(18, 48);
            this.dgEspecies.Name = "dgEspecies";
            this.dgEspecies.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecies.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgEspecies.RowHeadersWidth = 22;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecies.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgEspecies.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEspecies.Size = new System.Drawing.Size(363, 114);
            this.dgEspecies.TabIndex = 14;
            this.dgEspecies.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgEspecies_RowsRemoved);
            // 
            // ESP_CODIGO
            // 
            this.ESP_CODIGO.DataPropertyName = "ESP_CODIGO";
            this.ESP_CODIGO.HeaderText = "Especie ID";
            this.ESP_CODIGO.Name = "ESP_CODIGO";
            this.ESP_CODIGO.ReadOnly = true;
            this.ESP_CODIGO.Visible = false;
            // 
            // ESP_DESCRICAO
            // 
            this.ESP_DESCRICAO.DataPropertyName = "ESP_DESCRICAO";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ESP_DESCRICAO.DefaultCellStyle = dataGridViewCellStyle3;
            this.ESP_DESCRICAO.HeaderText = "Espécie";
            this.ESP_DESCRICAO.Name = "ESP_DESCRICAO";
            this.ESP_DESCRICAO.ReadOnly = true;
            this.ESP_DESCRICAO.Width = 150;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "TOTAL";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn9.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // EXC
            // 
            this.EXC.DataPropertyName = "EXCLUI";
            this.EXC.HeaderText = "Exclui";
            this.EXC.Name = "EXC";
            this.EXC.ReadOnly = true;
            this.EXC.Visible = false;
            // 
            // CONSIDERA_ESP
            // 
            this.CONSIDERA_ESP.DataPropertyName = "CONSIDERA_ESP";
            this.CONSIDERA_ESP.HeaderText = "Esp.";
            this.CONSIDERA_ESP.Name = "CONSIDERA_ESP";
            this.CONSIDERA_ESP.ReadOnly = true;
            this.CONSIDERA_ESP.Visible = false;
            // 
            // txtValorEsp
            // 
            this.txtValorEsp.BackColor = System.Drawing.Color.White;
            this.txtValorEsp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorEsp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorEsp.ForeColor = System.Drawing.Color.Black;
            this.txtValorEsp.Location = new System.Drawing.Point(298, 20);
            this.txtValorEsp.Name = "txtValorEsp";
            this.txtValorEsp.Size = new System.Drawing.Size(83, 22);
            this.txtValorEsp.TabIndex = 11;
            this.txtValorEsp.Text = "0,00";
            this.txtValorEsp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorEsp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorEsp_KeyDown);
            this.txtValorEsp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorEsp_KeyPress);
            // 
            // cmbEspecies
            // 
            this.cmbEspecies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEspecies.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEspecies.FormattingEnabled = true;
            this.cmbEspecies.Location = new System.Drawing.Point(68, 20);
            this.cmbEspecies.Name = "cmbEspecies";
            this.cmbEspecies.Size = new System.Drawing.Size(224, 24);
            this.cmbEspecies.TabIndex = 10;
            this.cmbEspecies.SelectedIndexChanged += new System.EventHandler(this.cmbEspecies_SelectedIndexChanged);
            this.cmbEspecies.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEspecies_KeyDown);
            this.cmbEspecies.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEspecies_KeyPress);
            // 
            // txtEspID
            // 
            this.txtEspID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEspID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEspID.ForeColor = System.Drawing.Color.Black;
            this.txtEspID.Location = new System.Drawing.Point(18, 21);
            this.txtEspID.Name = "txtEspID";
            this.txtEspID.Size = new System.Drawing.Size(44, 22);
            this.txtEspID.TabIndex = 9;
            this.txtEspID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEspID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEspID_KeyDown);
            this.txtEspID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEspID_KeyPress);
            this.txtEspID.Validated += new System.EventHandler(this.txtEspID_Validated);
            // 
            // gbFormaPagamento
            // 
            this.gbFormaPagamento.Controls.Add(this.dgFormas);
            this.gbFormaPagamento.Controls.Add(this.txtValor);
            this.gbFormaPagamento.Controls.Add(this.cmbForma);
            this.gbFormaPagamento.Controls.Add(this.txtID);
            this.gbFormaPagamento.Location = new System.Drawing.Point(6, 314);
            this.gbFormaPagamento.Name = "gbFormaPagamento";
            this.gbFormaPagamento.Size = new System.Drawing.Size(387, 169);
            this.gbFormaPagamento.TabIndex = 2;
            this.gbFormaPagamento.TabStop = false;
            this.gbFormaPagamento.Text = "Forma de Pagamento";
            this.gbFormaPagamento.Visible = false;
            // 
            // dgFormas
            // 
            this.dgFormas.AllowUserToAddRows = false;
            this.dgFormas.AllowUserToDeleteRows = false;
            this.dgFormas.AllowUserToResizeColumns = false;
            this.dgFormas.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFormas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgFormas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFormas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgFormas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFormas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CODIGO,
            this.PAR,
            this.VEN,
            this.VAL,
            this.CON});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFormas.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgFormas.GridColor = System.Drawing.Color.Black;
            this.dgFormas.Location = new System.Drawing.Point(18, 50);
            this.dgFormas.Name = "dgFormas";
            this.dgFormas.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFormas.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgFormas.RowHeadersVisible = false;
            this.dgFormas.RowHeadersWidth = 22;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.dgFormas.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgFormas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFormas.Size = new System.Drawing.Size(363, 113);
            this.dgFormas.TabIndex = 14;
            // 
            // CODIGO
            // 
            this.CODIGO.DataPropertyName = "FORMA_ID";
            dataGridViewCellStyle10.Format = "N0";
            dataGridViewCellStyle10.NullValue = null;
            this.CODIGO.DefaultCellStyle = dataGridViewCellStyle10;
            this.CODIGO.HeaderText = "ID";
            this.CODIGO.Name = "CODIGO";
            this.CODIGO.ReadOnly = true;
            this.CODIGO.Width = 40;
            // 
            // PAR
            // 
            this.PAR.DataPropertyName = "PARCELA";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PAR.DefaultCellStyle = dataGridViewCellStyle11;
            this.PAR.HeaderText = "Parcela";
            this.PAR.Name = "PAR";
            this.PAR.ReadOnly = true;
            this.PAR.Width = 70;
            // 
            // VEN
            // 
            this.VEN.DataPropertyName = "VENC";
            dataGridViewCellStyle12.Format = "d";
            dataGridViewCellStyle12.NullValue = null;
            this.VEN.DefaultCellStyle = dataGridViewCellStyle12;
            this.VEN.HeaderText = "Vencimento";
            this.VEN.Name = "VEN";
            this.VEN.ReadOnly = true;
            this.VEN.Width = 90;
            // 
            // VAL
            // 
            this.VAL.DataPropertyName = "TOTAL";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            dataGridViewCellStyle13.NullValue = null;
            this.VAL.DefaultCellStyle = dataGridViewCellStyle13;
            this.VAL.HeaderText = "Valor";
            this.VAL.Name = "VAL";
            this.VAL.ReadOnly = true;
            this.VAL.Width = 80;
            // 
            // CON
            // 
            this.CON.DataPropertyName = "COND";
            this.CON.HeaderText = "Condição";
            this.CON.Name = "CON";
            this.CON.ReadOnly = true;
            this.CON.Width = 142;
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.Color.White;
            this.txtValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.ForeColor = System.Drawing.Color.Black;
            this.txtValor.Location = new System.Drawing.Point(298, 20);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(83, 22);
            this.txtValor.TabIndex = 13;
            this.txtValor.Text = "0,00";
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValor_KeyDown);
            this.txtValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValor_KeyPress);
            // 
            // cmbForma
            // 
            this.cmbForma.BackColor = System.Drawing.Color.White;
            this.cmbForma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbForma.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbForma.FormattingEnabled = true;
            this.cmbForma.Location = new System.Drawing.Point(68, 20);
            this.cmbForma.Name = "cmbForma";
            this.cmbForma.Size = new System.Drawing.Size(224, 24);
            this.cmbForma.TabIndex = 9;
            this.cmbForma.SelectedIndexChanged += new System.EventHandler(this.cmbForma_SelectedIndexChanged);
            this.cmbForma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbForma_KeyDown);
            this.cmbForma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbForma_KeyPress);
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.White;
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.ForeColor = System.Drawing.Color.Black;
            this.txtID.Location = new System.Drawing.Point(18, 21);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(44, 22);
            this.txtID.TabIndex = 8;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyDown);
            this.txtID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtID_KeyPress);
            this.txtID.Validated += new System.EventHandler(this.txtID_Validated);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnConfirma);
            this.groupBox3.Controls.Add(this.txtDesconto);
            this.groupBox3.Controls.Add(this.lblDesconto);
            this.groupBox3.Controls.Add(this.lblDiferenca);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.lblTotalTroca);
            this.groupBox3.Controls.Add(this.lblTotalDev);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.dgProdutos);
            this.groupBox3.Controls.Add(this.txtIdentificacao);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(6, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 312);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Finalizar Devolução";
            // 
            // btnConfirma
            // 
            this.btnConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirma.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirma.Image")));
            this.btnConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirma.Location = new System.Drawing.Point(736, 248);
            this.btnConfirma.Name = "btnConfirma";
            this.btnConfirma.Size = new System.Drawing.Size(194, 52);
            this.btnConfirma.TabIndex = 18;
            this.btnConfirma.Text = "Confirma Devolução (F1)";
            this.btnConfirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirma.UseVisualStyleBackColor = true;
            this.btnConfirma.Click += new System.EventHandler(this.btnConfirma_Click);
            this.btnConfirma.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConfirma_KeyDown);
            // 
            // txtDesconto
            // 
            this.txtDesconto.BackColor = System.Drawing.Color.White;
            this.txtDesconto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto.ForeColor = System.Drawing.Color.Red;
            this.txtDesconto.Location = new System.Drawing.Point(853, 217);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(77, 22);
            this.txtDesconto.TabIndex = 17;
            this.txtDesconto.Text = "0";
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDesconto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDesconto_KeyDown);
            this.txtDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesconto_KeyPress);
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.Location = new System.Drawing.Point(733, 223);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(67, 16);
            this.lblDesconto.TabIndex = 16;
            this.lblDesconto.Text = "Desconto:";
            // 
            // lblDiferenca
            // 
            this.lblDiferenca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiferenca.ForeColor = System.Drawing.Color.Red;
            this.lblDiferenca.Location = new System.Drawing.Point(807, 198);
            this.lblDiferenca.Name = "lblDiferenca";
            this.lblDiferenca.Size = new System.Drawing.Size(123, 15);
            this.lblDiferenca.TabIndex = 13;
            this.lblDiferenca.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(733, 197);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = "Diferença:";
            // 
            // lblTotalTroca
            // 
            this.lblTotalTroca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTroca.ForeColor = System.Drawing.Color.Black;
            this.lblTotalTroca.Location = new System.Drawing.Point(842, 170);
            this.lblTotalTroca.Name = "lblTotalTroca";
            this.lblTotalTroca.Size = new System.Drawing.Size(88, 16);
            this.lblTotalTroca.TabIndex = 11;
            this.lblTotalTroca.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalDev
            // 
            this.lblTotalDev.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDev.ForeColor = System.Drawing.Color.Black;
            this.lblTotalDev.Location = new System.Drawing.Point(845, 142);
            this.lblTotalDev.Name = "lblTotalDev";
            this.lblTotalDev.Size = new System.Drawing.Size(85, 15);
            this.lblTotalDev.TabIndex = 10;
            this.lblTotalDev.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(733, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 16);
            this.label8.TabIndex = 9;
            this.label8.Text = "Total de Produtos:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(733, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Valor Devolvido:";
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            this.dgProdutos.AllowUserToResizeColumns = false;
            this.dgProdutos.AllowUserToResizeRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.DESCONTO,
            this.dataGridViewTextBoxColumn8,
            this.ID});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgProdutos.GridColor = System.Drawing.Color.Black;
            this.dgProdutos.Location = new System.Drawing.Point(18, 49);
            this.dgProdutos.Name = "dgProdutos";
            this.dgProdutos.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgProdutos.RowHeadersVisible = false;
            this.dgProdutos.RowHeadersWidth = 20;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutos.Size = new System.Drawing.Size(709, 251);
            this.dgProdutos.TabIndex = 5;
            this.dgProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProdutos_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "EST_CODIGO";
            this.dataGridViewTextBoxColumn1.HeaderText = "EST_CODIGO";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "EMP_CODIGO";
            this.dataGridViewTextBoxColumn2.HeaderText = "EMP_CODIGO";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PROD_ID";
            this.dataGridViewTextBoxColumn3.HeaderText = "PROD_ID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PROD_CODIGO";
            this.dataGridViewTextBoxColumn4.HeaderText = "Cód. de Barras";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 120;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PROD_DESCR";
            this.dataGridViewTextBoxColumn5.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 250;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "QTDE";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn6.HeaderText = "Qtde.";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 50;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "VALOR";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn7.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 75;
            // 
            // DESCONTO
            // 
            this.DESCONTO.DataPropertyName = "DESCONTO";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = null;
            this.DESCONTO.DefaultCellStyle = dataGridViewCellStyle21;
            this.DESCONTO.HeaderText = "Desconto";
            this.DESCONTO.Name = "DESCONTO";
            this.DESCONTO.ReadOnly = true;
            this.DESCONTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "TOTAL";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            dataGridViewCellStyle22.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn8.HeaderText = "Total";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 80;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // txtIdentificacao
            // 
            this.txtIdentificacao.Location = new System.Drawing.Point(98, 21);
            this.txtIdentificacao.Name = "txtIdentificacao";
            this.txtIdentificacao.Size = new System.Drawing.Size(161, 22);
            this.txtIdentificacao.TabIndex = 1;
            this.txtIdentificacao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdentificacao_KeyDown);
            this.txtIdentificacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdentificacao_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Identificação";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(175, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Devolução de Produtos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmVenDevolucaoProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenDevolucaoProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmVenDevolucaoProdutos";
            this.Load += new System.EventHandler(this.frmVenDevolucaoProdutos_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVenDevolucaoProdutos_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbEspecies.ResumeLayout(false);
            this.gbEspecies.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecies)).EndInit();
            this.gbFormaPagamento.ResumeLayout(false);
            this.gbFormaPagamento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFormas)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.TextBox txtIdentificacao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTotalTroca;
        private System.Windows.Forms.Label lblTotalDev;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDiferenca;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gbFormaPagamento;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.GroupBox gbEspecies;
        private System.Windows.Forms.ComboBox cmbForma;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.DataGridView dgFormas;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON;
        private System.Windows.Forms.TextBox txtValorEsp;
        private System.Windows.Forms.ComboBox cmbEspecies;
        private System.Windows.Forms.TextBox txtEspID;
        private System.Windows.Forms.DataGridView dgEspecies;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXC;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONSIDERA_ESP;
        private System.Windows.Forms.Button btnConfirmarCaixa;
        private System.Windows.Forms.Label lblTroco;
        private System.Windows.Forms.Label lblTiTroco;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCONTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnConfirma;
    }
}