﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Vendas
{
    public partial class frmVenEntrega : Form
    {
        public double valorVenda;
        private int indiceGrid;
        private frmVenda fVendas;

        public frmVenEntrega(frmVenda fvendas)
        {
            InitializeComponent();
            fVendas = fvendas;
        }

        private void frmVenEntrega_Load(object sender, EventArgs e)
        {
            btnCancela.Enabled = true;

            lblTroco.Visible = false;
            txtTroco.Text = "0,00";
            fVendas.vEntr_End = "";
            fVendas.vEntr_Bairro = "";
            fVendas.vEntr_Cidade = "";
            fVendas.vEntr_Cep = "";
            fVendas.vEntr_Fone = "";
            fVendas.vEntr_UF = "";
            fVendas.vEntr_Valor = 0;
            fVendas.vEntr_Obs = "";
            fVendas.vEntr_Numero = "";
            fVendas.vEntr_Canc = true;
            cmbUF.Text = "SP";
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNumero.Focus();
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCep.Focus();
        }

        private void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCidade.Focus();
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUF.Focus();
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTelefone.Focus();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtTroco.Focus();
        }

        private void txtTroco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOk.Focus();
        }

        private void frmVenEntrega_Shown(object sender, EventArgs e)
        {
            try
            {
                var cliente = new Cliente();
                DataTable dtRetorno = cliente.SelecionaDadosDoClientePorDocto(fVendas.doctoCliente);

                if (dtRetorno.Rows.Count != 0)
                {
                    dgEndereco.Rows.Insert(dgEndereco.RowCount, new Object[] { "ENTREGA", dtRetorno.Rows[0]["CF_ENDERENT"], dtRetorno.Rows[0]["CF_NUMEROENT"],
                        dtRetorno.Rows[0]["CF_BAIRROENT"], dtRetorno.Rows[0]["CF_CEPENT"], dtRetorno.Rows[0]["CF_CIDADEENT"],
                        dtRetorno.Rows[0]["CF_UFENT"]});

                    dgEndereco.Rows.Insert(dgEndereco.RowCount, new Object[] { "NORMAL", dtRetorno.Rows[0]["CF_ENDER"], dtRetorno.Rows[0]["CF_END_NUM"],
                        dtRetorno.Rows[0]["CF_BAIRRO"], dtRetorno.Rows[0]["CF_CEP"], dtRetorno.Rows[0]["CF_CIDADE"],
                        dtRetorno.Rows[0]["CF_UF"]});

                    dgEndereco.Rows.Insert(dgEndereco.RowCount, new Object[] { "COBRANÇA", dtRetorno.Rows[0]["CF_ENDERCOB"], dtRetorno.Rows[0]["CF_NUMEROCOB"],
                        dtRetorno.Rows[0]["CF_BAIRROCOB"], dtRetorno.Rows[0]["CF_CEPCOB"], dtRetorno.Rows[0]["CF_CIDADECOB"],
                        dtRetorno.Rows[0]["CF_UFCOB"]});

                    txtTelefone.Text = dtRetorno.Rows[0]["CF_FONE"].ToString();
                }

                //CARREGAR TRES ULTIMOS ENDERECOS DE ENTREGA//
                var ultEntregas = new ControleEntrega();
                string descrEntrega = "";
                dtRetorno = ultEntregas.UltimasEntregas(fVendas.IDCliente, Principal.estAtual, Principal.empAtual);
                for(int i=0; i < dtRetorno.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        descrEntrega = "ÚLTIMA ENTREGA";
                    }
                    else if (i == 1)
                    {
                        descrEntrega = "PENÚLTIMA ENTREGA";
                    }
                    else if (i == 2)
                    {
                        descrEntrega = "ANTEPENÚLTIMA ENTREGA";
                    }
                    else
                        break;

                    dgEndereco.Rows.Insert(dgEndereco.RowCount, new Object[] { descrEntrega, dtRetorno.Rows[i]["ENT_CONTROLE_ENDER"],dtRetorno.Rows[i]["ENT_CONTROLE_NUMERO"],
                        dtRetorno.Rows[i]["ENT_CONTROLE_BAIRRO"], dtRetorno.Rows[i]["ENT_CONTROLE_CEP"], dtRetorno.Rows[0]["ENT_CONTROLE_CIDADE"],
                        dtRetorno.Rows[i]["ENT_CONTROLE_UF"]});
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Informações da Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtEndereco.Text = dgEndereco.Rows[indiceGrid].Cells[1].Value.ToString();
                txtNumero.Text = Funcoes.RemoveCaracter(dgEndereco.Rows[indiceGrid].Cells[2].Value.ToString()) == "" ? "0" : dgEndereco.Rows[indiceGrid].Cells[2].Value.ToString();
                txtBairro.Text = dgEndereco.Rows[indiceGrid].Cells[3].Value.ToString();
                txtCep.Text = dgEndereco.Rows[indiceGrid].Cells[4].Value.ToString();
                txtCidade.Text = dgEndereco.Rows[indiceGrid].Cells[5].Value.ToString();
                cmbUF.Text = dgEndereco.Rows[indiceGrid].Cells[6].Value.ToString() == "" ? "SP" : dgEndereco.Rows[indiceGrid].Cells[6].Value.ToString();
                txtTelefone.Focus();
            }
        }

        private void dgEndereco_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void txtTroco_Validated(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtTroco.Text.Trim()))
                {
                    if (Convert.ToDouble(txtTroco.Text) > 0)
                    {
                        lblTroco.Visible = true;
                        if (Convert.ToDouble(txtTroco.Text) > valorVenda)
                        {
                            txtTroco.Text = String.Format("{0:N}", Convert.ToDouble(txtTroco.Text));
                            lblTroco.Text = "TROCO: " + String.Format("{0:C}", Convert.ToDouble(txtTroco.Text) - valorVenda);
                        }
                        else
                        {
                            txtTroco.Text = String.Format("{0:N}", Convert.ToDouble(txtTroco.Text));
                            lblTroco.Text = "TROCO: R$ 0,00";
                        }
                    }
                    else
                        txtObs.Focus();
                }
                else
                {
                    txtTroco.Text = "0,00";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Informações da Entrega", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            fVendas.vEntr_End = "";
            fVendas.vEntr_Numero = "";
            fVendas.vEntr_Bairro = "";
            fVendas.vEntr_Cidade = "";
            fVendas.vEntr_Cep = "";
            fVendas.vEntr_Fone = "";
            fVendas.vEntr_UF = "";
            fVendas.vEntr_Valor = 0;
            fVendas.vEntr_Obs = "";
            fVendas.vEntr_Canc = true;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (ConsisteCampos().Equals(true))
            {
                fVendas.vEntr_End = txtEndereco.Text.Trim().ToUpper();
                fVendas.vEntr_Numero = txtNumero.Text.Trim().ToUpper();
                fVendas.vEntr_Bairro = txtBairro.Text.Trim().ToUpper();
                fVendas.vEntr_Cidade = txtCidade.Text.Trim().ToUpper();
                fVendas.vEntr_Cep = txtCep.Text.Trim().ToUpper();
                fVendas.vEntr_Fone = txtTelefone.Text.Trim().ToUpper();
                fVendas.vEntr_UF = cmbUF.Text;
                fVendas.vEntr_Valor = Convert.ToDouble(txtTroco.Text);
                fVendas.vEntr_Obs = txtObs.Text.Trim().ToUpper();
                fVendas.vEntr_Canc = false;
                this.Close();
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtEndereco.Text.Trim()))
            {
                Principal.mensagem = "Endereço não pode ser em branco.";
                Funcoes.Avisa();
                txtEndereco.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtBairro.Text.Trim()))
            {
                Principal.mensagem = "Bairro não pode ser em branco.";
                Funcoes.Avisa();
                txtBairro.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCidade.Text.Trim()))
            {
                Principal.mensagem = "Cidade não pode ser em branco.";
                Funcoes.Avisa();
                txtCidade.Focus();
                return false;
            }
            if (cmbUF.SelectedIndex == -1)
            {
                Principal.mensagem = "Necessário selecionar a UF";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            return true;
        }

        private void dgEndereco_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtEndereco.Text = dgEndereco.Rows[indiceGrid].Cells[1].Value.ToString();
            txtNumero.Text = Funcoes.RemoveCaracter(dgEndereco.Rows[indiceGrid].Cells[2].Value.ToString()) == "" ? "0" : dgEndereco.Rows[indiceGrid].Cells[2].Value.ToString();
            txtBairro.Text = dgEndereco.Rows[indiceGrid].Cells[3].Value.ToString();
            txtCep.Text = dgEndereco.Rows[indiceGrid].Cells[4].Value.ToString();
            txtCidade.Text = dgEndereco.Rows[indiceGrid].Cells[5].Value.ToString();
            cmbUF.Text = dgEndereco.Rows[indiceGrid].Cells[6].Value.ToString() == "" ? "SP" : dgEndereco.Rows[indiceGrid].Cells[6].Value.ToString();
            txtTelefone.Focus();
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Fechar (F1)");
        }

        private void btnOk_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnOk, "Confirmar (F2)");
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.F1:
                    btnCancela.PerformClick();
                    break;
                case Keys.F2:
                    btnOk.PerformClick();
                    break;
            }
        }

        private void frmVenEntrega_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCep_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbUF_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTroco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtBairro.Focus();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
