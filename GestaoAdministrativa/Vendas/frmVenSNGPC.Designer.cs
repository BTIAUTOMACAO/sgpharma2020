﻿namespace GestaoAdministrativa.Vendas
{
    partial class frmVenSNGPC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenSNGPC));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtQtdeEstoque = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbLote = new System.Windows.Forms.ComboBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.txtLote = new System.Windows.Forms.TextBox();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.txtReg = new System.Windows.Forms.TextBox();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.cmbTipoRec = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbUMed = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cmbMUF = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.dtReceita = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.txtNReceita = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbConselho = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMedico = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCrm = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbPSexo = new System.Windows.Forms.ComboBox();
            this.cmbPUnidade = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPIdade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPNome = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbUF = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbOrgao = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLote = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbDocto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(5, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(940, 449);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Venda de Produto Controlado";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnConfirmar);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txtQtdeEstoque);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.cmbLote);
            this.panel1.Controls.Add(this.pictureBox11);
            this.panel1.Controls.Add(this.txtLote);
            this.panel1.Controls.Add(this.txtQtde);
            this.panel1.Controls.Add(this.txtReg);
            this.panel1.Controls.Add(this.txtDescr);
            this.panel1.Controls.Add(this.txtCodBarras);
            this.panel1.Controls.Add(this.cmbTipoRec);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.cmbUMed);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.cmbMUF);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.dtReceita);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.txtNReceita);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.cmbConselho);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.txtMedico);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.txtCrm);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.dtData);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtDocto);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cmbUF);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cmbOrgao);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblLote);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cmbDocto);
            this.panel1.Controls.Add(this.label3);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(6, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(928, 423);
            this.panel1.TabIndex = 0;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(774, 367);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(141, 51);
            this.btnConfirmar.TabIndex = 291;
            this.btnConfirmar.Text = "Confirmar (F2)";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(627, 367);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 51);
            this.button1.TabIndex = 290;
            this.button1.Text = "Cancelar (F1)";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtQtdeEstoque
            // 
            this.txtQtdeEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtQtdeEstoque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtdeEstoque.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdeEstoque.Location = new System.Drawing.Point(237, 338);
            this.txtQtdeEstoque.MaxLength = 3;
            this.txtQtdeEstoque.Name = "txtQtdeEstoque";
            this.txtQtdeEstoque.ReadOnly = true;
            this.txtQtdeEstoque.Size = new System.Drawing.Size(114, 22);
            this.txtQtdeEstoque.TabIndex = 272;
            this.txtQtdeEstoque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtdeEstoque.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQtdeEstoque_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(235, 318);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 16);
            this.label11.TabIndex = 289;
            this.label11.Text = "Qtde. em Estoque";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(4, 318);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 16);
            this.label12.TabIndex = 287;
            this.label12.Text = "Lote";
            // 
            // cmbLote
            // 
            this.cmbLote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLote.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cmbLote.FormattingEnabled = true;
            this.cmbLote.Location = new System.Drawing.Point(7, 338);
            this.cmbLote.Name = "cmbLote";
            this.cmbLote.Size = new System.Drawing.Size(215, 24);
            this.cmbLote.TabIndex = 271;
            this.cmbLote.SelectedIndexChanged += new System.EventHandler(this.cmbLote_SelectedIndexChanged);
            this.cmbLote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbLote_KeyDown);
            this.cmbLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLote_KeyPress);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(612, 52);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 279;
            this.pictureBox11.TabStop = false;
            // 
            // txtLote
            // 
            this.txtLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLote.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLote.Location = new System.Drawing.Point(367, 338);
            this.txtLote.MaxLength = 20;
            this.txtLote.Name = "txtLote";
            this.txtLote.Size = new System.Drawing.Size(109, 22);
            this.txtLote.TabIndex = 273;
            this.txtLote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLote_KeyDown);
            this.txtLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLote_KeyPress);
            // 
            // txtQtde
            // 
            this.txtQtde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(488, 338);
            this.txtQtde.MaxLength = 3;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.ReadOnly = true;
            this.txtQtde.Size = new System.Drawing.Size(58, 22);
            this.txtQtde.TabIndex = 276;
            this.txtQtde.Text = "0";
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg
            // 
            this.txtReg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReg.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReg.Location = new System.Drawing.Point(720, 289);
            this.txtReg.MaxLength = 13;
            this.txtReg.Name = "txtReg";
            this.txtReg.ReadOnly = true;
            this.txtReg.Size = new System.Drawing.Size(195, 22);
            this.txtReg.TabIndex = 270;
            this.txtReg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReg_KeyDown);
            this.txtReg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReg_KeyPress);
            // 
            // txtDescr
            // 
            this.txtDescr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(162, 289);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.ReadOnly = true;
            this.txtDescr.Size = new System.Drawing.Size(547, 22);
            this.txtDescr.TabIndex = 269;
            this.txtDescr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDescr_KeyDown);
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(7, 289);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.ReadOnly = true;
            this.txtCodBarras.Size = new System.Drawing.Size(144, 22);
            this.txtCodBarras.TabIndex = 268;
            this.txtCodBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodBarras_KeyDown);
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // cmbTipoRec
            // 
            this.cmbTipoRec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoRec.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoRec.FormattingEnabled = true;
            this.cmbTipoRec.Location = new System.Drawing.Point(488, 237);
            this.cmbTipoRec.Name = "cmbTipoRec";
            this.cmbTipoRec.Size = new System.Drawing.Size(427, 24);
            this.cmbTipoRec.TabIndex = 267;
            this.cmbTipoRec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTipoRec_KeyDown);
            this.cmbTipoRec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTipoRec_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(485, 220);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(133, 16);
            this.label30.TabIndex = 266;
            this.label30.Text = "Tipo do Receituário";
            // 
            // cmbUMed
            // 
            this.cmbUMed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUMed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUMed.FormattingEnabled = true;
            this.cmbUMed.Location = new System.Drawing.Point(320, 237);
            this.cmbUMed.Name = "cmbUMed";
            this.cmbUMed.Size = new System.Drawing.Size(156, 24);
            this.cmbUMed.TabIndex = 265;
            this.cmbUMed.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUMed_KeyDown);
            this.cmbUMed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUMed_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(317, 220);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 16);
            this.label29.TabIndex = 264;
            this.label29.Text = "Uso do Medicamento";
            // 
            // cmbMUF
            // 
            this.cmbMUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMUF.FormattingEnabled = true;
            this.cmbMUF.Location = new System.Drawing.Point(246, 237);
            this.cmbMUF.Name = "cmbMUF";
            this.cmbMUF.Size = new System.Drawing.Size(62, 24);
            this.cmbMUF.TabIndex = 263;
            this.cmbMUF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbMUF_KeyDown);
            this.cmbMUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMUF_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(243, 220);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 16);
            this.label28.TabIndex = 262;
            this.label28.Text = "UF";
            // 
            // dtReceita
            // 
            this.dtReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtReceita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtReceita.Location = new System.Drawing.Point(114, 237);
            this.dtReceita.Name = "dtReceita";
            this.dtReceita.Size = new System.Drawing.Size(122, 22);
            this.dtReceita.TabIndex = 261;
            this.dtReceita.Value = new System.DateTime(2013, 11, 28, 0, 0, 0, 0);
            this.dtReceita.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtReceita_KeyDown);
            this.dtReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtReceita_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(113, 220);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 16);
            this.label27.TabIndex = 260;
            this.label27.Text = "Data da Receita";
            // 
            // txtNReceita
            // 
            this.txtNReceita.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNReceita.Location = new System.Drawing.Point(7, 237);
            this.txtNReceita.MaxLength = 10;
            this.txtNReceita.Name = "txtNReceita";
            this.txtNReceita.Size = new System.Drawing.Size(96, 22);
            this.txtNReceita.TabIndex = 259;
            this.txtNReceita.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNReceita_KeyDown);
            this.txtNReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNReceita_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(4, 220);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 16);
            this.label26.TabIndex = 258;
            this.label26.Text = "N° Receita";
            // 
            // cmbConselho
            // 
            this.cmbConselho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConselho.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConselho.FormattingEnabled = true;
            this.cmbConselho.Location = new System.Drawing.Point(565, 193);
            this.cmbConselho.Name = "cmbConselho";
            this.cmbConselho.Size = new System.Drawing.Size(350, 24);
            this.cmbConselho.TabIndex = 257;
            this.cmbConselho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbConselho_KeyDown);
            this.cmbConselho.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbConselho_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(562, 174);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(145, 16);
            this.label25.TabIndex = 256;
            this.label25.Text = "Conselho Profissional";
            // 
            // txtMedico
            // 
            this.txtMedico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMedico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMedico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedico.Location = new System.Drawing.Point(162, 193);
            this.txtMedico.MaxLength = 40;
            this.txtMedico.Name = "txtMedico";
            this.txtMedico.Size = new System.Drawing.Size(391, 22);
            this.txtMedico.TabIndex = 255;
            this.txtMedico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMedico_KeyDown);
            this.txtMedico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMedico_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(166, 174);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 16);
            this.label24.TabIndex = 254;
            this.label24.Text = "Médico";
            // 
            // txtCrm
            // 
            this.txtCrm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCrm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCrm.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrm.Location = new System.Drawing.Point(7, 193);
            this.txtCrm.MaxLength = 13;
            this.txtCrm.Name = "txtCrm";
            this.txtCrm.Size = new System.Drawing.Size(144, 22);
            this.txtCrm.TabIndex = 253;
            this.txtCrm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCrm_KeyDown);
            this.txtCrm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCrm_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(4, 174);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 16);
            this.label23.TabIndex = 252;
            this.label23.Text = "CRM";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbPSexo);
            this.groupBox4.Controls.Add(this.cmbPUnidade);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.txtPIdade);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txtPNome);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(7, 98);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(908, 69);
            this.groupBox4.TabIndex = 251;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dados do Paciente";
            // 
            // cmbPSexo
            // 
            this.cmbPSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPSexo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPSexo.FormattingEnabled = true;
            this.cmbPSexo.Location = new System.Drawing.Point(814, 40);
            this.cmbPSexo.Name = "cmbPSexo";
            this.cmbPSexo.Size = new System.Drawing.Size(81, 24);
            this.cmbPSexo.TabIndex = 190;
            this.cmbPSexo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPSexo_KeyDown);
            this.cmbPSexo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPSexo_KeyPress);
            // 
            // cmbPUnidade
            // 
            this.cmbPUnidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPUnidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPUnidade.FormattingEnabled = true;
            this.cmbPUnidade.Location = new System.Drawing.Point(660, 40);
            this.cmbPUnidade.Name = "cmbPUnidade";
            this.cmbPUnidade.Size = new System.Drawing.Size(145, 24);
            this.cmbPUnidade.TabIndex = 189;
            this.cmbPUnidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPUnidade_KeyDown);
            this.cmbPUnidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPUnidade_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(811, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 16);
            this.label22.TabIndex = 188;
            this.label22.Text = "Sexo";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(657, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 16);
            this.label21.TabIndex = 187;
            this.label21.Text = "Unidade";
            // 
            // txtPIdade
            // 
            this.txtPIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPIdade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPIdade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPIdade.Location = new System.Drawing.Point(557, 40);
            this.txtPIdade.MaxLength = 40;
            this.txtPIdade.Name = "txtPIdade";
            this.txtPIdade.Size = new System.Drawing.Size(96, 22);
            this.txtPIdade.TabIndex = 186;
            this.txtPIdade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPIdade_KeyDown);
            this.txtPIdade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPIdade_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(555, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 16);
            this.label19.TabIndex = 185;
            this.label19.Text = "Idade";
            // 
            // txtPNome
            // 
            this.txtPNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPNome.Location = new System.Drawing.Point(9, 40);
            this.txtPNome.MaxLength = 40;
            this.txtPNome.Name = "txtPNome";
            this.txtPNome.Size = new System.Drawing.Size(537, 22);
            this.txtPNome.TabIndex = 184;
            this.txtPNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPNome_KeyDown);
            this.txtPNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPNome_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(6, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 16);
            this.label18.TabIndex = 181;
            this.label18.Text = "Nome";
            // 
            // dtData
            // 
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(565, 70);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(120, 22);
            this.dtData.TabIndex = 250;
            this.dtData.Value = new System.DateTime(2013, 11, 28, 0, 0, 0, 0);
            this.dtData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtData_KeyDown);
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(564, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 16);
            this.label17.TabIndex = 249;
            this.label17.Text = "Data";
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(162, 70);
            this.txtNome.MaxLength = 80;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(391, 22);
            this.txtNome.TabIndex = 248;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(161, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 16);
            this.label6.TabIndex = 247;
            this.label6.Text = "Nome";
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(7, 70);
            this.txtDocto.MaxLength = 30;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(144, 22);
            this.txtDocto.TabIndex = 246;
            this.txtDocto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocto_KeyDown);
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 245;
            this.label5.Text = "N° do Documento";
            // 
            // cmbUF
            // 
            this.cmbUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUF.FormattingEnabled = true;
            this.cmbUF.Location = new System.Drawing.Point(853, 23);
            this.cmbUF.Name = "cmbUF";
            this.cmbUF.Size = new System.Drawing.Size(62, 24);
            this.cmbUF.TabIndex = 244;
            this.cmbUF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUF_KeyDown);
            this.cmbUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUF_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(850, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 16);
            this.label4.TabIndex = 243;
            this.label4.Text = "UF";
            // 
            // cmbOrgao
            // 
            this.cmbOrgao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrgao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrgao.FormattingEnabled = true;
            this.cmbOrgao.Location = new System.Drawing.Point(493, 23);
            this.cmbOrgao.Name = "cmbOrgao";
            this.cmbOrgao.Size = new System.Drawing.Size(352, 24);
            this.cmbOrgao.TabIndex = 242;
            this.cmbOrgao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOrgao_KeyDown);
            this.cmbOrgao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbOrgao_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(493, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 241;
            this.label2.Text = "Orgão Expedidor";
            // 
            // lblLote
            // 
            this.lblLote.AutoSize = true;
            this.lblLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLote.ForeColor = System.Drawing.Color.Navy;
            this.lblLote.Location = new System.Drawing.Point(364, 318);
            this.lblLote.Name = "lblLote";
            this.lblLote.Size = new System.Drawing.Size(36, 16);
            this.lblLote.TabIndex = 235;
            this.lblLote.Text = "Lote";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(485, 318);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 16);
            this.label14.TabIndex = 234;
            this.label14.Text = "Qtde.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(717, 270);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 16);
            this.label13.TabIndex = 233;
            this.label13.Text = "Reg. Min. da Saúde";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(161, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 16);
            this.label9.TabIndex = 232;
            this.label9.Text = "Descrição";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(4, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 16);
            this.label7.TabIndex = 231;
            this.label7.Text = "Código";
            // 
            // cmbDocto
            // 
            this.cmbDocto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocto.FormattingEnabled = true;
            this.cmbDocto.Location = new System.Drawing.Point(7, 23);
            this.cmbDocto.Name = "cmbDocto";
            this.cmbDocto.Size = new System.Drawing.Size(477, 24);
            this.cmbDocto.TabIndex = 230;
            this.cmbDocto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDocto_KeyDown);
            this.cmbDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDocto_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 229;
            this.label3.Text = "Tipo de Documento";
            // 
            // frmVenSNGPC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientSize = new System.Drawing.Size(948, 452);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmVenSNGPC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVenSNGPC";
            this.Load += new System.EventHandler(this.frmVenSNGPC_Load);
            this.Shown += new System.EventHandler(this.frmVenSNGPC_Shown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtQtdeEstoque;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbLote;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TextBox txtLote;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.TextBox txtReg;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.ComboBox cmbTipoRec;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbUMed;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cmbMUF;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dtReceita;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtNReceita;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbConselho;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMedico;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCrm;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cmbPSexo;
        private System.Windows.Forms.ComboBox cmbPUnidade;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPIdade;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPNome;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbUF;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbOrgao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLote;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbDocto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button button1;
    }
}