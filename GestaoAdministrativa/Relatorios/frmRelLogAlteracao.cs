﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelLogAlteracao : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Log Alteração");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmRelLogAlteracao(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmRelLogAlteracao_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbUsuario.SelectedIndex = -1;
            dtInicial.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmRelLogAlteracao>().FirstOrDefault();
                    Funcoes.BotoesCadastro(relatorio.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmRelLogAlteracao_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ID", "LOGIN_ID", "USUARIO_SYSTEM", false, "LIBERADO= 'S'");

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "LOGIN_ID";
                cmbUsuario.SelectedIndex = 0;
               
                dtInicial.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUsuario.Focus();
        }

        private void cmbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmRelLogAlteracao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();

                string sql = "SELECT * FROM LOG_ALTERACAO WHERE EMP_CODIGO = " + Principal.empAtual + " AND EST_CODIGO = " + Principal.estAtual
                    + " AND LOG_DATA BETWEEN TO_DATE('" + dtInicial.Value.ToString("dd/MM/yyyy") + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + dtFinal.Value.ToString("dd/MM/yyyy")
                    + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";

                if(cmbUsuario.SelectedIndex > 0)
                {
                    sql += " AND LOG_OPERADOR = '" + cmbUsuario.Text + "'";
                }

                if(rdbAlteracao.Checked)
                {
                    sql += " AND LOG_OPERACAO = 'ALTERAÇÃO'";
                }
                else if(rdbDelete.Checked)
                {
                    sql += " AND LOG_OPERACAO = 'EXCLUSÃO'";
                }
                else if(rdbInclusao.Checked)
                {
                    sql += " AND LOG_OPERACAO = 'INCLUSÃO'";
                }

                sql += " ORDER BY LOG_OPERACAO, LOG_DATA";

                dtRetorno = BancoDados.GetDataTable(sql, null);
                if (dtRetorno.Rows.Count > 0)
                {
                    frmRelatorioLog relatorio = new frmRelatorioLog(this, dtRetorno);
                    relatorio.Text = "Logs de Alteração/Exclusão/Inclusão";
                    relatorio.ShowDialog();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Nenhum registro encontrado!", "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Log Alteração", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
