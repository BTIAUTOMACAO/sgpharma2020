﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelEntregasEmAberto : Form
    {
        public frmRelEntregasEmAberto()
        {
            InitializeComponent();
        }

        private void frmRelEntregasEmAberto_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.WindowState = FormWindowState.Maximized;
                MontaDadosEstabelecimento();
                MontaRelatorio();
                this.reportViewer1.RefreshReport();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void MontaDadosEstabelecimento()
        {
            Microsoft.Reporting.WinForms.ReportParameter[] parametro = new ReportParameter[1];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var busca = new ControleEntrega();
                DataTable dtRelatorio = busca.BuscaEntregasPendentes();
                var entrega = new ReportDataSource("dsEntregas", dtRelatorio);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(entrega);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Entregas em Aberto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
