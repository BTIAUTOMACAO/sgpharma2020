﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelEntrega : Form
    {
        private frmRelEntregaPeriodo relatorio;
        private DataTable dtRelatorio;

        public frmRelEntrega(frmRelEntregaPeriodo frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
            this.WindowState = FormWindowState.Maximized;
        }

        private void frmRelEntrega_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Text);
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Text);
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var relatorio = new ReportDataSource("dsEntrega", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(relatorio);
        }
    }
}
