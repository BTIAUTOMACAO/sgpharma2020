﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelComandaAberto : Form
    {
        public frmRelComandaAberto()
        {
            InitializeComponent();
            reportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        private void frmRelComandaAberto_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.WindowState = FormWindowState.Maximized;
                MontaDadosEstabelecimento();
                MontaRelatorio();
                this.reportViewer1.RefreshReport();
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public void MontaDadosEstabelecimento()
        {
            Microsoft.Reporting.WinForms.ReportParameter[] parametro = new ReportParameter[1];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var busca = new VendasDados();
                DataTable dtRelatorio = busca.RelatorioComandaAberta(Principal.empAtual, Principal.estAtual);
                var comanda = new ReportDataSource("Comanda", dtRelatorio);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(comanda);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Comandas em Aberto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

          public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new VendasItens();
            DataTable dtItens = tItens.BuscaItens(e.Parameters["ID"].Values.First(), Principal.empAtual, Principal.estAtual);
            
            var itens = new ReportDataSource("dsItens", dtItens);
            e.DataSources.Add(itens);
        }
    }
}
