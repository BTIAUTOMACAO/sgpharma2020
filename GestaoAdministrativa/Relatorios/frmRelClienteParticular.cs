﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelClienteParticular : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Clientes por Emp.");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmRelClienteParticular(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmRelClienteParticular_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();
                dtPesq = Util.CarregarCombosPorEstabelecimento("CON_CODIGO", "CON_NOME", "CONVENIADAS", false, "CON_DESABILITADO = 'N' AND CON_WEB = 4");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Empresa Particular.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbConveniada.DataSource = dtPesq;
                    cmbConveniada.DisplayMember = "CON_NOME";
                    cmbConveniada.ValueMember = "CON_CODIGO";

                    cmbConveniada.SelectedIndex = -1;
                }
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Limpar()
        {
            cmbConveniada.SelectedIndex = -1;
            cmbClientes.DataSource = null;
            txtEmpresa.Text = "";
            txtCliente.Text = "";
            txtEmpresa.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmRelClienteParticular>().FirstOrDefault();
                    Funcoes.BotoesCadastro(relatorio.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmRelClienteParticular_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void txtEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbConveniada.Focus();
        }

        private void txtEmpresa_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbConveniada, txtEmpresa);
            if (cmbConveniada.SelectedIndex != -1)
            {
                DataTable dtPesq = new DataTable();
                dtPesq = BancoDados.selecionarRegistros("SELECT CF_ID, CF_NOME FROM CLIFOR WHERE CON_CODIGO = " + txtEmpresa.Text + " AND cf_desabilitado = 'N' AND cf_status >= 2 ORDER BY CF_NOME");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Nenhum Cliente nesta Empresa Particular", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    cmbClientes.DataSource = dtPesq;
                    cmbClientes.DisplayMember = "CF_NOME";
                    cmbClientes.ValueMember = "CF_ID";

                    cmbClientes.SelectedIndex = -1;

                    txtCliente.Focus();
                }
            }
        }

        private void cmbConveniada_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEmpresa.Text = Convert.ToString(cmbConveniada.SelectedValue);
            if(txtEmpresa.Text != "System.Data.DataRowView")
            {
                if (!String.IsNullOrEmpty(txtEmpresa.Text))
                {
                    DataTable dtPesq = new DataTable();
                    dtPesq = BancoDados.selecionarRegistros("SELECT CF_ID, CF_NOME FROM CLIFOR WHERE CON_CODIGO = " + txtEmpresa.Text + " AND cf_desabilitado = 'N' AND cf_status >= 2 ORDER BY CF_NOME");
                    if (dtPesq.Rows.Count == 0)
                    {
                        MessageBox.Show("Nenhum Cliente nesta Empresa Particular", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        cmbClientes.DataSource = dtPesq;
                        cmbClientes.DisplayMember = "CF_NOME";
                        cmbClientes.ValueMember = "CF_ID";

                        cmbClientes.SelectedIndex = -1;

                        txtCliente.Focus();
                    }
                }
                else
                    txtEmpresa.Text = "";
            }
        }

        private void cmbConveniada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCliente.Focus();
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.Focus();
                    break;
            }
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbClientes.Focus();
        }

        private void txtCliente_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbClientes, txtCliente);
            if (cmbClientes.SelectedIndex != -1)
                btnBuscar.Focus();
        }

        private void cmbClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCliente.Text = Convert.ToString(cmbClientes.SelectedValue);
        }

        private void cmbClientes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.Focus();
        }

        private void txtEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbConveniada_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbClientes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCliente_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmRelClienteParticular_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (cmbConveniada.SelectedIndex == -1)
                {
                    Principal.mensagem = "Necessário selecionar uma Empresa Conveniada";
                    Funcoes.Avisa();
                    cmbConveniada.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var busca = new Cliente();
                dtRetorno = busca.BucaClientesPorEmpresaConveniada(txtEmpresa.Text, cmbClientes.SelectedIndex == -1 ? "" : cmbClientes.Text);

                if (dtRetorno.Rows.Count > 0)
                {
                    frmRelCliEmpresa relatorio = new frmRelCliEmpresa(this, dtRetorno);
                    relatorio.Text = "Cliente por Empresas";
                    relatorio.ShowDialog();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Nenhum registro encontrado!", "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Clientes por Empresas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
