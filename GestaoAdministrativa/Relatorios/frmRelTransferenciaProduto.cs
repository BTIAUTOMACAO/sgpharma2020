﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Relatorios
{
    public partial class frmRelTransferenciaProduto : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Trans. Produto");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmRelTransferenciaProduto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmRelTransferenciaProduto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            cmbUsuario.SelectedIndex = -1;
            txtID.Text = "";
            txtIDCancelamento.Text = "";
            dtInicial.Focus();
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmRelTransferenciaProduto>().FirstOrDefault();
                    Funcoes.BotoesCadastro(relatorio.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private async void frmRelTransferenciaProduto_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ID", "LOGIN_ID", "USUARIO_SYSTEM", false, "LIBERADO= 'S'");

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "LOGIN_ID";
                cmbUsuario.SelectedIndex = 0;

                List<Transferencia> dados = await BuscaTransferenciaPendentes();
                if (dados.Count > 0)
                {
                    if (dados[0].CodErro == "00")
                    {
                        dgPendentes.DataSource = dados;
                    }
                }

                List<Reserva> dadosReserva = await BuscaReservasDeProdutos();
                if (dadosReserva.Count > 0)
                {
                    if (dadosReserva[0].CodErro == "00")
                    {
                        cmbPendentes.DataSource = dadosReserva;
                        cmbPendentes.DisplayMember = "NomeFilial";
                        cmbPendentes.ValueMember = "LojaOrigem";
                        cmbPendentes.SelectedIndex = -1;
                    }
                }

                dtInicial.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<List<Reserva>> BuscaReservasDeProdutos()
        {
            List<Reserva> allItems = new List<Reserva>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/BuscaReservaPorCodLojaEStatus?codLoja=" + Funcoes.LeParametro(9, "52", true)
                        + "&status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Reserva>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<Transferencia>> BuscaTransferenciaPendentes()
        {
            List<Transferencia> allItems = new List<Transferencia>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Transferencia/TransfereciasPendentes?codLoja=" + Funcoes.LeParametro(9, "52", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Transferencia>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var busca = new Transferencia();
                dtRetorno = busca.BuscaTransferencias(dtInicial.Text, dtFinal.Text, Principal.estAtual, Principal.empAtual, cmbUsuario.Text);

                if (dtRetorno.Rows.Count > 0)
                {
                    frmRelatorioTransferencia relatorio = new frmRelatorioTransferencia(this, dtRetorno);
                    relatorio.Text = "Transferência de Produtos";
                    relatorio.ShowDialog();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Nenhum registro encontrado!", "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnFiltro.PerformClick();
        }

        private void btnFiltro_Click(object sender, EventArgs e)
        {
            try
            {

                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtID.Text)))
                {
                    MessageBox.Show("Informe o ID da Transferência!", "Impressão de Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtID.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;

                var dados = new Transferencia();
                DataTable dt = dados.ReimpressaoDeTransferencias(Principal.empAtual, Principal.estAtual, txtID.Text);
                if (dt.Rows.Count > 0)
                {
                    var dadosEstabelecimento = new Estabelecimento();
                    string tamanho, comprovanteTransferencia;
                    int quantidade = 0;
                    double total = 0;
                    List<Cliente> dadosVendaCliente = new List<Cliente>();
                    List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);

                    comprovanteTransferencia = Funcoes.CentralizaTexto("TRANSFERENCIA DE PRODUTOS", 43) + "\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto("ENTRE FILIAIS", 43) + "\n\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                    comprovanteTransferencia += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                    comprovanteTransferencia += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                    comprovanteTransferencia += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                    tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                    comprovanteTransferencia += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                    comprovanteTransferencia += retornoEstab[0].EstBairro + "\n";
                    comprovanteTransferencia += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                    comprovanteTransferencia += "Data: " + DateTime.Now.ToString("dd/MM/yyyy") + "     Hora: " + DateTime.Now.ToString("HH:mm:ss") + "\n";
                    comprovanteTransferencia += "Filial: " + dt.Rows[0]["LOJA"] + "\n";
                    comprovanteTransferencia += "Operador: " + dt.Rows[0]["OPCADASTRO"].ToString().ToUpper() + "\n";
                    comprovanteTransferencia += "_________________________________________" + "\n";
                    comprovanteTransferencia += "|COD|DESC|QT|UN|VL UN R$|ST|AQ|VL ITEM R$" + "\n";
                    comprovanteTransferencia += "_________________________________________" + "\n";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        comprovanteTransferencia += Funcoes.PrencherEspacoEmBranco(dt.Rows[i]["PROD_CODIGO"].ToString(), 13) + " " + (dt.Rows[i]["PROD_DESCR"].ToString().Length > 27 ? dt.Rows[i]["PROD_DESCR"].ToString().Substring(0, 26) : dt.Rows[i]["PROD_DESCR"].ToString()) + "\n";
                        comprovanteTransferencia += "    R$ " + String.Format("{0:N}", dt.Rows[i]["VALOR"]) + " X " + dt.Rows[i]["QTDE"] + " =  R$ " + String.Format("{0:N}", Convert.ToDouble(dt.Rows[i]["VALOR"]) * Convert.ToInt32(dt.Rows[i]["QTDE"])) + "\n";

                        quantidade += Convert.ToInt32(dt.Rows[i]["QTDE"]);
                        total += Convert.ToDouble(dt.Rows[i]["VALOR"]) * Convert.ToInt32(dt.Rows[i]["QTDE"]);
                    }

                    comprovanteTransferencia += "=========================================" + "\n\n";
                    comprovanteTransferencia += "Quantidade de Produtos : " + quantidade + "\n";
                    comprovanteTransferencia += "Valor Total de Produtos: " + String.Format("{0:N}", total) + "\n";
                    comprovanteTransferencia += "ID da Transferência: " + txtID.Text + "\n";

                    if (!String.IsNullOrEmpty(dt.Rows[0]["OBSERVACAO"].ToString()))
                    {
                        comprovanteTransferencia += "Observação: " + dt.Rows[0]["OBSERVACAO"].ToString() + "\n";
                    }
                    comprovanteTransferencia += "=========================================" + "\n\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovanteTransferencia, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovanteTransferencia += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteTransferencia);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        comprovanteTransferencia += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteTransferencia, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado", "Impressão de Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Impressão de Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.F2:
                    btnFiltro.PerformClick();
                    break;
                case Keys.F3:
                    btnCancelar.PerformClick();
                    break;
                case Keys.F4:
                    btnCancelarReserva.PerformClick();
                    break;

            }
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnFiltro_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtIDCancelamento_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgPendentes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtIDCancelamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnCancelar.PerformClick();
        }

        private async void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtIDCancelamento.Text))
                {
                    MessageBox.Show("Informe o ID para o Cancelamento", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtIDCancelamento.Focus();
                }
                else
                {
                    if (MessageBox.Show("Deseja cancelar a Transferência?", "Cancelamento de Transferência de Produtos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        Cursor = Cursors.WaitCursor;

                        var faltas = new LancamentoFaltas();
                        if (!faltas.AtualizaStatusDaFaltaPorCodigo(Convert.ToInt32(txtIDCancelamento.Text), Principal.estAtual, Principal.empAtual, "E"))
                            return;

                        Principal.dtPesq = faltas.BuscaFaltasPorIdTransferencia(Principal.empAtual, Principal.estAtual, Convert.ToInt32(txtIDCancelamento.Text));
                        for (int i = 0; i < Principal.dtPesq.Rows.Count; i++)
                        {
                            await AtualizaStatus(Convert.ToInt32(Principal.dtPesq.Rows[i]["ID"]));
                        }
                        
                        var excluir = new Transferencia();
                        if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                        {
                            await ExcluiTransferencia(Util.SelecionaCampoEspecificoDaTabela("TRANSFERENCIA", "CODIGO", "ID", txtIDCancelamento.Text, true, false, true));

                            await DecrementaESomaEstoqueDeFiliais(Util.SelecionaCampoEspecificoDaTabela("TRANSFERENCIA", "CODIGO_LOJA", "ID", txtIDCancelamento.Text, true, false, true));
                        }

                        if (!excluir.ExcluirDados(Principal.estAtual, Principal.empAtual, Convert.ToInt64(txtIDCancelamento.Text)))
                            return;

                        MessageBox.Show("Cancelamento efetuado com sucesso!", "Cancelamento de Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtIDCancelamento.Text = "";
                        dtInicial.Focus();
                    }
                    else
                        return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Cancelamento de Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public async Task<bool> ExcluiTransferencia(string codigo)
        {
            try
            {
                if (codigo != "0")
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Transferencia/ExcluiTransferenciaID?id=" + codigo);
                        if (!response.IsSuccessStatusCode)
                        {
                            MessageBox.Show("Erro ao excluir Transferência.", "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Importar Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public async Task DecrementaESomaEstoqueDeFiliais(string codLoja)
        {
            try
            {
                var busca = new Transferencia();
                Principal.dtPesq = busca.ReimpressaoDeTransferencias(Principal.empAtual, Principal.estAtual, txtIDCancelamento.Text);
                for (int i = 0; i < Principal.dtPesq.Rows.Count; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                        + "&codBarras=" + Principal.dtPesq.Rows[i]["PROD_CODIGO"].ToString()
                                        + "&qtd=" + Convert.ToInt32(Principal.dtPesq.Rows[i]["QTDE"]) + "&operacao=mais");
                        if (!response.IsSuccessStatusCode)
                        {
                            return;
                        }

                        if (codLoja != "0")
                        {
                            response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + codLoja
                                            + "&codBarras=" + Principal.dtPesq.Rows[i]["PROD_CODIGO"].ToString()
                                            + "&qtd=" + Convert.ToInt32(Principal.dtPesq.Rows[i]["QTDE"]) + "&operacao=menos");
                            if (!response.IsSuccessStatusCode)
                            {
                                return;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> AtualizaStatus(int idLoja)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Falteiro/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&idLoja=" + idLoja
                        + "&status=E");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status.", "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private async void btnCancelarReserva_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbPendentes.SelectedIndex != -1)
                {
                    Cursor = Cursors.WaitCursor;

                    string[] split = cmbPendentes.Text.Split('-');

                    await AtualizaStatusReserva(Convert.ToInt32(split[0]));

                    MessageBox.Show("Reserva Cancela com Sucesso!", "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    List<Reserva> dadosReserva = await BuscaReservasDeProdutos();
                    if (dadosReserva.Count > 0)
                    {
                        if (dadosReserva[0].CodErro == "00")
                        {
                            cmbPendentes.DataSource = dadosReserva;
                            cmbPendentes.DisplayMember = "NomeFilial";
                            cmbPendentes.ValueMember = "LojaOrigem";
                            cmbPendentes.SelectedIndex = -1;
                        }
                    }

                    cmbPendentes.Focus();

                }
                else
                {
                    MessageBox.Show("Necessário Selecionar uma Reserva para Cancelar", "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Rel. Transferência de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> AtualizaStatusReserva(int idReserva)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/AtualizacaoDeStatus?id=" + idReserva
                        + "&status=C");
                    return response.IsSuccessStatusCode;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void btnCancelarReserva_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbPendentes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }
    }
}
