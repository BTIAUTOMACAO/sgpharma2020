﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Ecf
{
    class Daruma : IModeloImpressora
    {
        public Daruma() { }

        public int iRetorno;
        public string sBuffer = string.Empty;
        public string Str_LabelInputBox, Str_TextoInputBox, Str_Retorno_InputBox;

        public string InputBox(string LB_stringInputBox, string TB_InputBox)
        {
            Str_LabelInputBox = string.Empty;
            Str_TextoInputBox = string.Empty;
            Str_Retorno_InputBox = string.Empty;

            Str_LabelInputBox = LB_stringInputBox;
            Str_TextoInputBox = TB_InputBox;

            return Str_Retorno_InputBox;
        }

        //Metodo para tratamento dos retornos
        public void TrataRetorno(int intRetorno)
        {
            StringBuilder Str_Msg_Retorno_Metodo = new StringBuilder(300);
            StringBuilder Str_Msg_Erro = new StringBuilder(); Str_Msg_Erro.Length = 300;
            StringBuilder Str_Msg_Aviso = new StringBuilder(); Str_Msg_Aviso.Length = 300;

            eInterpretarRetorno_ECF_Daruma(intRetorno, Str_Msg_Retorno_Metodo);
            eRetornarAvisoErroUltimoCMD_ECF_Daruma(Str_Msg_Aviso, Str_Msg_Erro);

            MessageBox.Show("Retorno do Metodo = "
                + Str_Msg_Retorno_Metodo + "\r\n"
                + "Num.Erro = " + Str_Msg_Erro + "\r\n"
                + "Num.Aviso= " + Str_Msg_Aviso, "Daruma Framework - Retorno do Metodo");

        }

        public bool ImpFiscalEstado()
        {
            iRetorno = eBuscarPortaVelocidade_ECF_Daruma();
            if (iRetorno != 1)
            {
                MessageBox.Show("Impressora desligada!", "Utilitários ECF");
                return false;
            }
            return true;
        }


        public bool ImpFiscalLeituraX()
        {
            iRetorno = iLeituraX_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
            }
            return true;
        }

        public bool ImpFiscalReducaoZ()
        {
            iRetorno = iReducaoZ_ECF_Daruma("", "");
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return false;
            }
            return true;
        }

        public bool ImpFiscalAbreCupom(string nDocto = "", string nome = "", string endereco = "")
        {
            iRetorno = eBuscarPortaVelocidade_ECF_Daruma();
            if (iRetorno != 1)
            {
                MessageBox.Show("Impressora desligada!", "DarumaFramework");
            }
            eDefinirProduto_Daruma("ECF");

            if (String.IsNullOrEmpty(nDocto))
            {
                iRetorno = iCFAbrirPadrao_ECF_Daruma();
            }
            else
            {
                iRetorno = iCFAbrir_ECF_Daruma(nDocto, nome, endereco);
            }

            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return false;
            }
            return true;
        }

        public bool ImpFiscalVenderItem(List<VendaItem> Itens)
        {
            for (int i = 0; i < Itens.Count; i++)
            {
                iRetorno = iCFVender_ECF_Daruma("I1", Itens[i].ItemQtde, Itens[i].ItemVlUnit, Itens[i].ItemTipoDesc,
                    Itens[i].ItemVlDesc, Itens[i].ItemCodBarra, Itens[i].ItemUniade, Itens[i].ItemDescr.Length > 29 ? Itens[i].ItemDescr.Substring(0, 28) : Itens[i].ItemDescr);
                if (!iRetorno.Equals(1))
                {
                    TrataRetorno(iRetorno);
                    ImpFiscalUltCupom();
                    return false;
                }
            }
            return true;
        }

        public bool ImpFiscalTotalizarCupom(string tipo, string desconto)
        {
            tipo = "D$";
            iRetorno = iCFTotalizarCupom_ECF_Daruma(tipo, desconto);
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                ImpFiscalUltCupom();
                return false;
            }
            return true;
        }

        public bool ImpFiscalMeioPagamento(List<VendasEspecies> VendaEspecies)
        {
            for (int i = 0; i < VendaEspecies.Count; i++)
            {
                iRetorno = iCFEfetuarPagamentoFormatado_ECF_Daruma(VendaEspecies[i].EspEcf, String.Format("{0:N}", Convert.ToDecimal(VendaEspecies[i].Valor)));
                if (!iRetorno.Equals(1))
                {
                    TrataRetorno(iRetorno);
                    ImpFiscalUltCupom();
                    return false;
                }
            }
            return true;
        }

        public bool ImpFiscalFechaCupom()
        {
            iRetorno = iCFEncerrarPadrao_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                ImpFiscalUltCupom();
                return false;
            }
            return true;
        }


        public bool ImpFiscalFechaCupomMsg(string msgPromocional)
        {
            iRetorno = iCFEncerrarConfigMsg_ECF_Daruma(msgPromocional);
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                ImpFiscalUltCupom();
                return false;
            }
            return true;
        }

        public bool ImpFiscalUltCupom()
        {
            iRetorno = iCFCancelar_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return false;
            }
            return true;
        }

        public string ImpFiscalInformacoesECF(string indice, string tamanho)
        {
            int iTamanho;
            int.TryParse(tamanho, out iTamanho);

            StringBuilder informacao = new StringBuilder(iTamanho);
            informacao.Length = iTamanho;

            iRetorno = rRetornarInformacao_ECF_Daruma(indice, informacao);
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return "";
            }
            return informacao.ToString().Trim();
        }

        public bool ImpFiscalAbrirVinculado(string formaPgto, string parcelas, string docOrigem, string valor)
        {
            parcelas = "1";
            iRetorno = iCCDAbrirSimplificado_ECF_Daruma(formaPgto, parcelas, docOrigem, valor);
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return false;
            }
            return true;
        }

        public string ImpFiscalNumeroCupom()
        {
            string vendaNumeroNota = "";
            vendaNumeroNota = ImpFiscalInformacoesECF("26", vendaNumeroNota);
            return vendaNumeroNota;
        }

        public bool ImpFiscalTextoVinculado(string mensagem)
        {
            iRetorno = iRGImprimirTexto_ECF_Daruma(mensagem);
            if (!iRetorno.Equals(1))
            {
                if (iRetorno != -12 && iRetorno != -6)
                {
                    TrataRetorno(iRetorno);
                    return false;
                }
                else
                {
                    if (MessageBox.Show("Falha durante a impressao. Tentar Novamente?", "Erro Impressora", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Principal.ReimprimeGerencial = true;
                        return false;
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        public bool ImpFiscalFechaVinculado()
        {
            iRetorno = iCCDFechar_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                TrataRetorno(iRetorno);
                return false;
            }
            return true;
        }

        public bool ImpFiscalAbreGerencial()
        {
            iRetorno = iRGAbrirPadrao_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                if (iRetorno != -12 && iRetorno != -6)
                {
                    TrataRetorno(iRetorno);
                    return false;
                }
                else
                {
                    if (MessageBox.Show("Falha durante a impressao. Tentar Novamente?", "Erro Impressora", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        ImpFiscalAbreGerencial();
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        public bool ImpFiscalFechaGerencial()
        {
            iRetorno = iRGFechar_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                if (iRetorno != -12 && iRetorno != -6)
                {
                    TrataRetorno(iRetorno);
                    return false;
                }
                else
                {
                    if (MessageBox.Show("Falha durante a impressao. Tentar Novamente?", "Erro Impressora", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        ImpFiscalFechaGerencial();
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        public bool ImpFiscalAbreGerencial(string mensagem)
        {
            iRetorno = iRGAbrirPadrao_ECF_Daruma();
            if (!iRetorno.Equals(1))
            {
                if (iRetorno != -12 && iRetorno != -6)
                {
                    TrataRetorno(iRetorno);
                    return false;
                }
                else
                {
                    if (MessageBox.Show("Falha durante a impressao. Tentar Novamente?", "Erro Impressora", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Principal.ReimprimeGerencial = true;
                        return false;
                    }
                    else
                        return false;
                }
            }
            return true;
        }

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iRGFechar_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iRGImprimirTexto_ECF_Daruma(string pszTexto);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int eDefinirProduto_Daruma(string sProduto);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int confCFNCM_ECF_Daruma(string pszCodNCM, string pszTipo);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int eBuscarPortaVelocidade_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iLeituraX_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iReducaoZ_ECF_Daruma(string NamelessParameter1, string NamelessParameter2);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFCancelar_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFAbrir_ECF_Daruma(string pszCPF, string pszNome, string pszEndereco);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFAbrirPadrao_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFVender_ECF_Daruma(string pszCargaTributaria, string pszQuantidade, string pszPrecoUnitario, string pszTipoDescAcresc, string pszValorDescAcresc, string pszCodigoItem, string pszUnidadeMedida, string pszDescricaoItem);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFTotalizarCupom_ECF_Daruma(string pszTipoDescAcresc, string pszValorDescAcresc);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFEncerrarPadrao_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFEncerrarConfigMsg_ECF_Daruma(string pszMensagem);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCFEfetuarPagamentoFormatado_ECF_Daruma(string pszFormaPgto, string pszValor);

        [DllImport("DarumaFramework.dll")]
        public static extern int eInterpretarRetorno_ECF_Daruma(int iRetorno, StringBuilder pszDescRet);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int eRetornarAvisoErroUltimoCMD_ECF_Daruma(StringBuilder pszDescAviso, StringBuilder pszDescErro);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCCDAbrirSimplificado_ECF_Daruma(string pszFormaPgto, string pszParcelas, string pszDocOrigem, string pszValor);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int rRetornarInformacao_ECF_Daruma(string pszIndice, StringBuilder pszRetornar);

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iRGAbrirPadrao_ECF_Daruma();

        [DllImport("DarumaFrameWork.dll")]
        public static extern int iCCDFechar_ECF_Daruma();
    }

}
