﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestaoAdministrativa.Negocio;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GestaoAdministrativa.Ecf
{
    public class Elgin : IModeloImpressora
    {
        private int IRetorno;

        public bool ImpFiscalAbreCupom(string nDocto = "", string nome = "", string endereco = "")
        {
            if (String.IsNullOrEmpty(nDocto))
            {
                IRetorno = Elgin_AbreCupom("");
            }
            else
            {
                IRetorno = Elgin_AbreCupom(nDocto);
            }
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }
        
        public bool ImpFiscalAbreGerencial(string mensagem)
        {
            IRetorno = Elgin_AbreRelatorioGerencial();
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalAbrirVinculado(string formaPgto, string parcelas, string docOrigem, string valor)
        {
            IRetorno = Elgin_AbreComprovanteNaoFiscalVinculado(formaPgto, String.Format("{0:N}", Convert.ToDouble(valor)), docOrigem);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalEstado()
        {
            throw new NotImplementedException();
        }

        public bool ImpFiscalFechaCupom()
        {
            throw new NotImplementedException();
        }

        public bool ImpFiscalFechaCupomMsg(string msgPromocional)
        {
            IRetorno = Elgin_TerminaFechamentoCupom(msgPromocional);
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalFechaGerencial()
        {
            IRetorno = Elgin_FechaRelatorioGerencial();
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalFechaVinculado()
        {
            IRetorno = Elgin_FechaComprovanteNaoFiscalVinculado();
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public string ImpFiscalInformacoesECF(string indice, string informacao)
        {
            string NumCaixa = new string('\x20', 4);
            IRetorno = Elgin_NumeroCaixa(ref NumCaixa);
            Analisa_iRetorno(IRetorno);
            return NumCaixa;
        }

        public bool ImpFiscalLeituraX()
        {
            IRetorno = Elgin_LeituraX();
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalMeioPagamento(List<VendasEspecies> VendaEspecies)
        {
            for (int i = 0; i < VendaEspecies.Count; i++)
            {
                IRetorno = Elgin_EfetuaFormaPagamento(VendaEspecies[i].EspEcf, String.Format("{0:N}", Convert.ToDecimal(VendaEspecies[i].Valor)));
                Analisa_iRetorno(IRetorno);
                if (IRetorno != 1)
                    return false;
            }
            return true;
        }

        public string ImpFiscalNumeroCupom()
        {
            string NumCupom = new string('\x20', 6);
            IRetorno = Elgin_NumeroCupom(ref NumCupom);
            Analisa_iRetorno(IRetorno);
            return NumCupom;
        }

        public bool ImpFiscalReducaoZ()
        {
            string data = new string('\x20', 10);
            string hora = new string('\x20', 10);

            IRetorno = Elgin_DataHoraImpressora(ref data, ref hora);
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
            {
                IRetorno = Elgin_ReducaoZ(data, hora);
                Analisa_iRetorno(IRetorno);
                if (IRetorno == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool ImpFiscalTextoVinculado(string mensagem)
        {
            IRetorno = Elgin_RelatorioGerencial(mensagem);
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalTotalizarCupom(string tipo, string desconto)
        {
            IRetorno = Elgin_IniciaFechamentoCupom(tipo, "$", String.Format("{0:N}", Math.Abs(Convert.ToDouble(desconto))).Replace(",", ""));
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalUltCupom()
        {
            IRetorno = Elgin_CancelaCupom();
            Analisa_iRetorno(IRetorno);
            if (IRetorno == 1)
                return true;
            else
                return false;
        }

        public bool ImpFiscalVenderItem(List<VendaItem> Itens)
        {
            for (int i = 0; i < Itens.Count; i++)
            {
                IRetorno = Elgin_VendeItem(Itens[i].ItemCodBarra, Itens[i].ItemDescr.Length > 29 ? Itens[i].ItemDescr.Substring(0, 28) : Itens[i].ItemDescr,
                   "1800", "I", Itens[i].ItemQtde, 2, String.Format("{0:N}", Convert.ToDouble(Itens[i].ItemVlUnit)), "$",
                    String.Format("{0:N}", Convert.ToDouble(Itens[i].ItemVlDesc)));
                Analisa_iRetorno(IRetorno);
                if (IRetorno != 1)
                    return false;
            }
            return true;
        }

        public void Analisa_iRetorno(int retorno)
        {
            string MSG = "";
            string MSGCaption = "Atenção";
            MessageBoxIcon MSGIco = MessageBoxIcon.Information;
            if (retorno != 1)
            {
                switch (retorno)
                {
                    case 0:
                        MSG = "Erro na comunicação. Verifique!";
                        MSGCaption = "Erro";
                        MSGIco = MessageBoxIcon.Error;
                        break;
                    case -1:
                        MSG = "Erro de Execução na Função. Verifique!";
                        MSGCaption = "Erro";
                        MSGIco = MessageBoxIcon.Error;
                        break;
                    case -2:
                        MSG = "Parâmetro Inválido!";
                        MSGCaption = "Erro";
                        MSGIco = MessageBoxIcon.Error;
                        break;
                    case -4:
                        MSG = "O arquivo de inicialização Elgin.ini não foi encontrado no diretório de sistema do Windows.";
                        break;
                    case -5:
                        MSG = "Erro ao Abrir a Porta de Comunicação.";
                        MSGCaption = "Erro";
                        MSGIco = MessageBoxIcon.Error;
                        break;
                    case -24:
                        MSG = "Forma de pagamento não programada.";
                        break;
                    case -27:
                        MSG = "Status da impressora diferente de 6,0,0 (ACK, ByVal ST1 e ST2).";
                        break;
                    case -36:
                        MSG = "Forma de pagamento não finalizada.";
                        break;
                    case -50:
                        MSG = "Número de série inválido.";
                        break;
                }
                if (MSG.Length != 0)
                    System.Windows.Forms.MessageBox.Show(MSG, MSGCaption, MessageBoxButtons.OK, MSGIco);
            }

        }

        public bool ObtemRetornoECF(out string strMensagemErro)
        {
            int iCodErro =0;
            string strErroMsg = new string('\x20', 100);
            
            IRetorno = Elgin_RetornoImpressora(iCodErro, ref strErroMsg);

            strMensagemErro = "Erro nº: " + iCodErro + " - " + strErroMsg;

            if (IRetorno == 1)
            {
                return true;
            }
            else
                return false;
        }

        [DllImport("Elgin.dll")]
        public static extern int Elgin_IniciaFechamentoCupom(string AcrescimoDesconto, string TipoAcrescimoDesconto, string ValorAcrescimoDesconto);

        [DllImport("Elgin.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int Elgin_VendeItem(string Codigo, string Descricao, string Aliquota, string TipoQuantidade, string Quantidade, int CasasDecimais, string ValorUnitario, string TipoDesconto, string Desconto);
    
        [DllImport("Elgin.dll")]
        public static extern int Elgin_AbreCupom(string CGC_CPF);

        [DllImport("Elgin.dll")]
        public static extern int Elgin_NumeroCaixa(string NumeroCaixa);

        [DllImport("Elgin.dll")]
        public static extern int Elgin_AbreRelatorioGerencial();

        [DllImport("elgin.dll")]
        public static extern int Elgin_LeituraX();

        [DllImport("elgin.dll")]
        public static extern int Elgin_ReducaoZ(string Data, string Hora);

        [DllImport("elgin.dll")]
        public static extern int Elgin_CancelaCupom();

        [DllImport("elgin.dll")]
        public static extern int Elgin_DataHoraImpressora([MarshalAs(UnmanagedType.VBByRefStr)] ref string Data, [MarshalAs(UnmanagedType.VBByRefStr)] ref string Hora);

        [DllImport("elgin.dll")]
        public static extern int Elgin_RetornoImpressora(int iCodErro, [MarshalAs(UnmanagedType.VBByRefStr)] ref string strErroMsg);

        [DllImport("elgin.dll")]
        public static extern int Elgin_NumeroCaixa([MarshalAs(UnmanagedType.VBByRefStr)] ref string NumeroCaixa);

        [DllImport("elgin.dll")]
        public static extern int Elgin_NumeroCupom([MarshalAs(UnmanagedType.VBByRefStr)] ref string NumeroCupom);

        [DllImport("elgin.dll")]
        public static extern int Elgin_EfetuaFormaPagamento(string FormaPagamento, string ValorFormaPagamento);

        [DllImport("elgin.dll")]
        public static extern int Elgin_TerminaFechamentoCupom(string Mensagem);

        [DllImport("elgin.dll")]
        public static extern int Elgin_AbreComprovanteNaoFiscalVinculado(string FormaPagamento, string Valor, string NumeroCupom);

        [DllImport("elgin.dll")]
        public static extern int Elgin_FechaComprovanteNaoFiscalVinculado();

        [DllImport("elgin.dll")]
        public static extern int Elgin_RelatorioGerencial(string Texto);

        [DllImport("elgin.dll")]
        public static extern int Elgin_FechaRelatorioGerencial();

    }
}
