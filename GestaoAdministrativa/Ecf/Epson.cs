﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestaoAdministrativa.Negocio;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GestaoAdministrativa.Ecf
{
    public class Epson : IModeloImpressora
    {
        private int IRetorno;

        public Epson() { }

        public bool ImpFiscalAbreCupom(string nDocto = "", string nome = "", string endereco = "")
        {
            if (String.IsNullOrEmpty(nDocto))
            {
                IRetorno = EPSON_Fiscal_Abrir_Cupom("", "", "", "", 1);
            }
            else
            {
                IRetorno = EPSON_Fiscal_Abrir_Cupom(nDocto, "", "", "", 1);
            }
            if (IRetorno != 0)
            {
                atualizaRetorno(IRetorno, "EPSON_Fiscal_Abrir_Cupom");
                return false;
            }
            else
                return true;
        }

        public bool ImpFiscalAbreGerencial(string mensagem)
        {
            IRetorno = EPSON_NaoFiscal_Abrir_Relatorio_Gerencial("1");
            if (IRetorno == 0)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_NaoFiscal_Abrir_Relatorio_Gerencial");
                return false;
            }
        }

        public bool AbrirPorta()
        {
            IRetorno = EPSON_Serial_Abrir_Porta(115200, Convert.ToInt32(Funcoes.LeParametro(2, "36", false)));
            if (IRetorno != 1)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_Serial_Abrir_Porta");
                return false;
            }
        }

        public bool AbrirJornadaFiscal()
        {
            IRetorno = EPSON_RelatorioFiscal_Abrir_Jornada();
            if (IRetorno != 1)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_RelatorioFiscal_Abrir_Jornada");
                return false;
            }
        }

        public bool ImpFiscalAbrirVinculado(string formaPgto, string parcelas, string docOrigem, string valor)
        {
            throw new NotImplementedException();
        }

        public bool ImpFiscalEstado()
        {
            throw new NotImplementedException();
        }

        public bool ImpFiscalFechaCupom()
        {
            IRetorno = EPSON_Fiscal_Fechar_Cupom(false, true);
            if (IRetorno == 0)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_Fiscal_Fechar_Cupom");
                return false;
            }

        }

        public bool ImpFiscalFechaCupomMsg(string msgPromocional)
        {
            string[] split = msgPromocional.ToString().Split('\n');
            
            if(split.Length == 4)
            {
                IRetorno = EPSON_Fiscal_Imprimir_Mensagem("<AN>" + (split[0].ToString() != "" ? split[0] : "")
                , "<AN>" + (split[1].ToString() != "" ? split[1] : ""), "<AN>" + (split[2].ToString() != "" ? split[2] : ""), "", "", "", "", "");
            }
            else if (split.Length == 7)
            {
                IRetorno = EPSON_Fiscal_Imprimir_Mensagem("<AN>" + (split[0].ToString() != "" ? split[0] : "")
               , "<AN>" + (split[1].ToString() != "" ? split[1] : ""), "<AN>" + (split[2].ToString() != "" ? split[2] : ""),
               "<AN>" + (split[3].ToString() != "" ? split[3] : ""),
               "<AN>" + (split[4].ToString() != "" ? split[4] : ""),
               "<AN>" + (split[5].ToString() != "" ? split[5] : ""),
               "<AN>" + (split[6].ToString() != "" ? split[6] : ""),
               "");
            }
            else
            {
                IRetorno = EPSON_Fiscal_Imprimir_Mensagem("<AN>" + (split[0].ToString() != "" ? split[0] : "")
               , "<AN>" + (split[1].ToString() != "" ? split[1] : ""), "<AN>" + (split[2].ToString() != "" ? split[2] : ""),
               "<AN>" + (split[3].ToString() != "" ? split[3] : ""),
               "<AN>" + (split[4].ToString() != "" ? split[4] : ""),
               "<AN>" + (split[5].ToString() != "" ? split[5] : ""),
               "<AN>" + (split[6].ToString() != "" ? split[6] : ""),
               "<AN>" + (split[7].ToString() != "" ? split[7] : ""));
            }
            
            if (IRetorno != 0)
            {
                atualizaRetorno(IRetorno, "EPSON_Fiscal_Imprimir_Mensagem");
                return false;
            }
            
            if (IRetorno == 0)
            {
                if (ImpFiscalFechaCupom())
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool ImpFiscalFechaGerencial()
        {
            IRetorno = EPSON_NaoFiscal_Fechar_Relatorio_Gerencial(true);
            if(IRetorno == 0)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_NaoFiscal_Fechar_Relatorio_Gerencial");
                return false;
            }
            
        }

        public bool ImpFiscalFechaVinculado()
        {
            throw new NotImplementedException();
        }

        public string ImpFiscalInformacoesECF(string indice, string informacao)
        {
            if (AbrirPorta())
            {
                string szDados = new String(' ', 7);
                IRetorno = EPSON_Obter_Numero_ECF_Loja(ref szDados);
                if (IRetorno == 0x00)
                    return szDados;
                else
                {
                    atualizaRetorno(IRetorno, "EPSON_Obter_Numero_ECF_Loja");
                    return "ERRO";
                }
            }
            else
                return "ERRO";
        }

        public bool ImpFiscalLeituraX()
        {
            if (AbrirPorta())
            {
                if (AbrirJornadaFiscal())
                {
                    IRetorno = EPSON_RelatorioFiscal_LeituraX();
                    if (IRetorno != 1)
                    {
                        return true;
                    }
                    else
                    {
                        atualizaRetorno(IRetorno, "EPSON_RelatorioFiscal_LeituraX");
                        return false;
                    }
                }
                else
                    return false;
            }
            else
                return false;
        }

        public bool ImpFiscalMeioPagamento(List<VendasEspecies> VendaEspecies)
        {
            for (int i = 0; i < VendaEspecies.Count; i++)
            {
                IRetorno = EPSON_Fiscal_Pagamento(VendaEspecies[i].EspEcf, String.Format("{0:N}", Convert.ToDecimal(VendaEspecies[i].Valor)).Replace(",", ""), 2, "", "");
                if (IRetorno != 0)
                {
                    atualizaRetorno(IRetorno, "EPSON_Obter_Numero_ECF_Loja");
                    return false;
                }
            }
            return true;
        }

        public string ImpFiscalNumeroCupom()
        {
            string szDados = new String(' ', 67);
            IRetorno = EPSON_Obter_Dados_Jornada(ref szDados);
            return szDados;
        }

        public bool ImpFiscalReducaoZ()
        {
            if (AbrirPorta())
            {
                string szCRZ = new String(' ', 4);
                IRetorno = EPSON_RelatorioFiscal_RZ("", "", 2, ref szCRZ);

                if (IRetorno == 0x00)
                {
                    return true;
                }
                else
                {
                    atualizaRetorno(IRetorno, "EPSON_RelatorioFiscal_RZ");
                    return false;
                }
            }
            else
                return false;
        }

        public bool ImpFiscalTextoVinculado(string mensagem)
        {
            IRetorno = EPSON_NaoFiscal_Imprimir_Linha(mensagem.Replace("\n",""));
            if (IRetorno == 0)
                return true;
            else
            {
                atualizaRetorno(IRetorno, "EPSON_NaoFiscal_Imprimir_Linha");
                return false;
            }
        }

        public bool ImpFiscalTotalizarCupom(string tipo, string desconto)
        {
            if (Convert.ToDouble(desconto) != 0)
            {
                IRetorno = EPSON_Fiscal_Desconto_Acrescimo_Subtotal(String.Format("{0:N}", Math.Abs(Convert.ToDouble(desconto))).Replace(",", ""), 2, true, false);
                if (IRetorno == 0)
                {
                    return true;
                }
                else
                {
                    atualizaRetorno(IRetorno, "EPSON_Fiscal_Desconto_Acrescimo_Subtotal");
                    return false;
                }
            }
            else
                return true;
        }

        public bool ImpFiscalUltCupom()
        {
            IRetorno = EPSON_Fiscal_Cancelar_Cupom();
            if (IRetorno != 1)
                return true;
            else
            {
                atualizaRetorno(IRetorno, "EPSON_Fiscal_Cancelar_Cupom");
                return false;
            }
        }

        public bool ImpFiscalVenderItem(List<VendaItem> Itens)
        {
            for (int i = 0; i < Itens.Count; i++)
            {
                IRetorno = EPSON_Fiscal_Vender_Item(Itens[i].ItemCodBarra, Itens[i].ItemDescr.Length > 29 ? Itens[i].ItemDescr.Substring(0, 28) : Itens[i].ItemDescr,
                   Funcoes.FormataZeroADireita(Itens[i].ItemQtde, 4), 3, Itens[i].ItemUniade, Convert.ToDouble(Itens[i].ItemVlUnit).ToString(".000").Replace(",", ""), 3,
                    Funcoes.LeParametro(6, "379", false), 1);
                if (IRetorno != 0)
                {
                    atualizaRetorno(IRetorno, "EPSON_Fiscal_Vender_Item");
                    return false;
                }

                if (Convert.ToDouble(Itens[i].ItemVlDesc) != 0)
                {
                    if (!DescontoItem(Convert.ToDouble(Itens[i].ItemVlDesc)))
                        return false;
                }
            }

            return true;
        }

        public bool DescontoItem(double valorDesconto)
        {
            IRetorno = EPSON_Fiscal_Desconto_Acrescimo_Item(valorDesconto.ToString(".000").Replace(",", ""), 3, true, false);
            if (IRetorno == 0)
            {
                return true;
            }
            else
            {
                atualizaRetorno(IRetorno, "EPSON_Fiscal_Desconto_Acrescimo_Item");
                return false;
            }
        }

        private void atualizaRetorno(int iRetorno, string funcName)
        {
            string szEstadoImpressora = new String(' ', 16);
            string szEstadoFiscal = new String(' ', 16);
            string szRetornoComando = new String(' ', 4);
            string szMsgErro = new String(' ', 100);

            iRetorno = EPSON_Obter_Estado_ImpressoraEX(ref szEstadoImpressora, ref szEstadoFiscal, ref szRetornoComando, ref szMsgErro);

            string mensagem = funcName;
            mensagem += szMsgErro;

            if (iRetorno != 0)
                mensagem += "ERRO";
            else
            {
                if (iRetorno != 0)
                    mensagem += "ERRO";
                else
                    mensagem += "SUCESSO";

                //---------------------------------------------------------------------------------------------------
                // Exibe o estado do mecanismo impressor
                //---------------------------------------------------------------------------------------------------
                mensagem += "";

                if (szEstadoImpressora.Substring(0, 1) == "1")  //Posição 1
                    mensagem += "Impressora Offline - ";
                else
                    mensagem += "Impressora Online - ";

                if (szEstadoImpressora.Substring(1, 1) == "1")  //Posição 2
                    mensagem += "Erro de impressão - ";

                if (szEstadoImpressora.Substring(2, 1) == "1")  //Posição 3
                    mensagem += "Tampa superior aberta - ";

                if (szEstadoImpressora.Substring(3, 1) == "1")  //Posição 4
                    mensagem += "Gaveta = 1 - ";
                else
                    mensagem += "Gaveta = 0 - ";

                //Posição 5 Reservada - Não utilizada

                if (szEstadoImpressora.Substring(5, 2) == "00")     //Posição 6 e 7
                    mensagem += "Estação recibo - ";
                else if (szEstadoImpressora.Substring(5, 2) == "01")    //Posição 6 e 7
                    mensagem += "Estação cheque - ";
                else if (szEstadoImpressora.Substring(5, 2) == "10")    //Posição 6 e 7
                    mensagem += "Estação Autenticação - ";
                else if (szEstadoImpressora.Substring(5, 2) == "11")    //Posição 6 e 7
                    mensagem += "Leitura do MICR - ";

                if (szEstadoImpressora.Substring(7, 1) == "1")  //Posição 8
                    mensagem += "Aguardando retirada do papel - ";

                if (szEstadoImpressora.Substring(8, 1) == "1")  //Posição 9
                    mensagem += "Aguardando inserção do papel - ";

                if (szEstadoImpressora.Substring(9, 1) == "1")  //Posição 10
                    mensagem += "Sensor inferior da estação de cheque Acionado - ";

                if (szEstadoImpressora.Substring(10, 1) == "1") //Posição 11
                    mensagem += "Sensor superior da estação do cheque Acionado - ";

                if (szEstadoImpressora.Substring(11, 1) == "1") //Posição 12
                    mensagem += "Sensor de autenticação Acionado - ";

                //Posição 13 e 14 Reservada - Não utilizada

                if (szEstadoImpressora.Substring(14, 1) == "1") //Posição 15
                    mensagem += "Sem papel - ";

                if (szEstadoImpressora.Substring(15, 1) == "1") //Posição 16
                    mensagem += "Pouco papel - ";
                //---------------------------------------------------------------------------------------------------

                //---------------------------------------------------------------------------------------------------
                // Exibe o estado fiscal
                //---------------------------------------------------------------------------------------------------
                //T3.Text = "";

                if (szEstadoFiscal.Substring(0, 2) == "00")     //Posição 1 e 2
                    mensagem += "Modo bloqueado - ";
                else if (szEstadoFiscal.Substring(0, 2) == "10")    //Posição 1 e 2
                    mensagem += "Modo manufatura (Não-Fiscalizado) - ";
                else if (szEstadoFiscal.Substring(0, 2) == "11")    //Posição 1 e 2
                    mensagem += "Modo Fiscalizado - ";

                //Posição 3 Reservada - Não utilizada

                if (szEstadoFiscal.Substring(3, 1) == "1")      //Posição 4
                    mensagem += "Modo de Intervenção Técnica - ";
                else
                    mensagem += "Modo de operação normal - ";

                if (szEstadoFiscal.Substring(4, 2) == "00")     //Posição 5 e 6
                    mensagem += "Memória Fiscal em operação normal - ";
                else if (szEstadoFiscal.Substring(4, 2) == "01")    //Posição 5 e 6
                    mensagem += "Memória Fiscal em esgotamento - ";
                else if (szEstadoFiscal.Substring(4, 2) == "10")    //Posição 5 e 6
                    mensagem += "Memória Fiscal cheia - ";
                if (szEstadoFiscal.Substring(4, 2) == "11")     //Posição 5 e 6
                    mensagem += "Erro de leitura/escrita da Memória Fiscal - ";

                //Posições 7 e 8 Reservads - Não utilizadas

                if (szEstadoFiscal.Substring(8, 1) == "1")      //Posição 9
                    mensagem += "Período de vendas aberto - ";
                else
                    mensagem += "Período de vendas fechado - ";

                //Posições 10, 11 e 12 Reservads - Não utilizadas

                if (szEstadoFiscal.Substring(12, 4) == "0000")      //Posição 13, 14, 15 e 16
                    mensagem += "Documento fechado - ";
                else if (szEstadoFiscal.Substring(12, 4) == "0001") //Posição 13, 14, 15 e 16
                    mensagem += "Cupom Fiscal aberto - ";
                else if (szEstadoFiscal.Substring(12, 4) == "0010") //Posição 13, 14, 15 e 16
                    mensagem += "Comprovante de Crédito ou Débito - ";
                else if (szEstadoFiscal.Substring(12, 4) == "0011") //Posição 13, 14, 15 e 16
                    mensagem += "Estorno de Comprovante de Crédito ou Débito - ";
                else if (szEstadoFiscal.Substring(12, 4) == "0100") //Posição 13, 14, 15 e 16
                    mensagem += "Relatório Gerencial - ";
                else if (szEstadoFiscal.Substring(12, 4) == "1000") //Posição 13, 14, 15 e 16
                    mensagem += "Comprovante Não-Fiscal - ";
                else if (szEstadoFiscal.Substring(12, 4) == "1001") //Posição 13, 14, 15 e 16
                    mensagem += "Cheque ou autenticação - ";
                //---------------------------------------------------------------------------------------------------
                MessageBox.Show(mensagem);

            }
        }

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Obter_Estado_ImpressoraEX([MarshalAs(UnmanagedType.VBByRefStr)] ref string szEstadoImpressora, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szEstadoFiscal, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szRetornoComando, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szMsgErro);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Abrir_Cupom(string pszCNPJ, string pszRazaoSocial, string pszEndereco1, string pszEndereco2, int dwPosicao);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_NaoFiscal_Abrir_Relatorio_Gerencial(string pszNumeroRelatorio);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Obter_Numero_ECF_Loja([MarshalAs(UnmanagedType.VBByRefStr)] ref string pszDados);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Obter_Dados_Jornada([MarshalAs(UnmanagedType.VBByRefStr)] ref string pszDados);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Vender_Item(string pszCodigo, string pszDescricao, string pszQuantidade, int dwQuantCasasDecimais, string pszUnidade, string pszPrecoUnidade, int dwPrecoCasasDecimais, string pszAliquotas, int dwLinhas);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Pagamento(string pszNumeroPagamento, string pszValorPagamento, int dwCasasDecimais, string pszDescricao1, string pszDescricao2);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Imprimir_Mensagem(string pszLinhaTexto1, string pszLinhaTexto2, string pszLinhaTexto3, string pszLinhaTexto4, string pszLinhaTexto5, string pszLinhaTexto6, string pszLinhaTexto7, string pszLinhaTexto8);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Fechar_Cupom(bool bCortarPapel, bool bAdicional);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_NaoFiscal_Imprimir_Linha(string pszLinha);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_RelatorioFiscal_LeituraX();

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_RelatorioFiscal_RZ(string pszData, string pszHora, int dwHorarioVerao, [MarshalAs(UnmanagedType.VBByRefStr)] ref string pszCRZ);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Serial_Abrir_Porta(int dwVelocidade, int wPorta);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_RelatorioFiscal_Abrir_Jornada();

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Cancelar_Cupom();

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_NaoFiscal_Fechar_Relatorio_Gerencial(bool bCortarPapel);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Desconto_Acrescimo_Item(string pszValor, int dwCasasDecimais, bool bDesconto, bool bPercentagem);

        [DllImport("InterfaceEpson.dll")]
        public static extern int EPSON_Fiscal_Desconto_Acrescimo_Subtotal(string pszValor, int dwCasasDecimais, bool bDesconto, bool bPercentagem);
    }
}
