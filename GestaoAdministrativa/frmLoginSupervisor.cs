﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestaoAdministrativa
{
    public partial class frmLoginSupervisor : Form
    {
        public frmLoginSupervisor()
        {
            InitializeComponent();
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnCancela, "Cancelar");
        }

        private void btnLogin_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnLogin, "Confirmar Senha");
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (txtSenha.Text == "bti@bella")
            {
                ////VERIFICA SE FORM JÁ ESTÁ ABERTO//
                //if (Application.OpenForms.OfType<>().Count() == 0)
                //{
                //    frmCadUsuario cadUsuario = new frmCadUsuario();
                //    int x = (MdiParent.Width / 2) - (cadUsuario.Width / 2);
                //    cadUsuario.Location = new Point(x, 0);
                //    cadUsuario.MdiParent = MdiParent;
                //    cadUsuario.Show();
                //    this.Close();
                //}
               
            }
            else
            {
                MessageBox.Show("Senha incorreta", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSenha.Focus();
            }

            Cursor = Cursors.Default;
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(sender, e);
            }
        }
    }
}
