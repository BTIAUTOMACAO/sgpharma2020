﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Cadastros;
using GestaoAdministrativa.SNGPC;
using GestaoAdministrativa.Estoque;
using GestaoAdministrativa.Caixa;
using GestaoAdministrativa.Financeiro;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Vendas;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.FPopular;
using GestaoAdministrativa.SNGPC.Relatorios;
using GestaoAdministrativa.Vendas.Relátorios;
using GestaoAdministrativa.Properties;
using GestaoAdministrativa.Relatorios;
using GestaoAdministrativa.Financeiro.Relatorios;
using GestaoAdministrativa.Vendas.NFE;
using Tulpep.NotificationWindow;
using System.Media;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Vendas.FPopular;
using System.Diagnostics;
using System.IO;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.Estoque.Filial;
using Microsoft.VisualBasic;

namespace GestaoAdministrativa
{
    public partial class MDIPrincipal : Form
    {
        private int childFormNumber = 0;
        PopupNotifier mensagemReserva = new PopupNotifier();
        PopupNotifier mensagemEntrega = new PopupNotifier();
        PopupNotifier aviso = new PopupNotifier();
        private int codLoja;
        private int codGrupo;
        //bool servidor;

        public MDIPrincipal()
        {
            InitializeComponent();
            mensagemReserva.Click += new System.EventHandler(mensagemReserva_Click);
            mensagemEntrega.Click += new System.EventHandler(mensagemEntrega_Click);
            aviso.Click += new System.EventHandler(aviso_Click);
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDICadastro_Load(object sender, EventArgs e)
        {
            try
            {
                Principal.contFormularios = 0;
                tslData.Text = Funcoes.GetDataAtual();
                tslMensagem.Text = Funcoes.MudaMensagem("");

                codGrupo = Convert.ToInt32(Funcoes.LeParametro(9, "53", true));
                codLoja = Convert.ToInt32(Funcoes.LeParametro(9, "52", true));

                if (Util.TesteIPServidorNuvem())
                {
                    if (Funcoes.LeParametro(9, "51", true).Equals("S") && (Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("B") || Funcoes.LeParametro(6, "382", true).Equals("S")))
                    {
                        timerReserva.Start();
                        timerEntrega.Start();
                    }

                    if(Funcoes.LeParametro(9,"51",true).Equals("S") && Funcoes.LeParametro(6,"365",false,Principal.nomeEstacao).Equals("S"))
                    {
                        timerPreco.Start();

                        timerPromocaoFilial.Start(); 
                    }
                }

                if (Funcoes.LeParametro(9, "63", false, Principal.nomeEstacao).Equals("S") && Funcoes.LeParametro(9, "64", false) != DateTime.Now.ToString("yyyydd"))
                {
                    if (File.Exists(@"C:\BTI\backup.bat"))
                    {
                        System.Diagnostics.Process.Start(@"C:\BTI\backup.bat");
                    }

                    Funcoes.GravaParametro(9, "64", DateTime.Now.ToString("yyyydd"), "GERAL", "Data Ultimo Backup", "ESTOQUE", "Identifica data do último backup", true);
                }

                if (Funcoes.LeParametro(2, "34", true).Equals("S") && Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                   && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                {
                    timerImpressao.Start();
                }

                btnMensagem.Image = Properties.Resources.mensagem;

                if (Util.TesteIPServidorNuvem())
                {
                    timerAvisos.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsmSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja realmente encerrar o sistema?", "Encerrando...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void MDICadastro_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    btnAtualiza.PerformClick();
                    break;
                case Keys.CapsLock:
                    UpCapsLock();
                    break;
                case Keys.NumLock:
                    UpNumLock();
                    break;
                case Keys.Insert:
                    UpInsert();
                    btnIncluir.PerformClick();
                    break;
                case Keys.F4:
                    btnExcluir.PerformClick();
                    break;
                case Keys.F6:
                    btnLimpar.PerformClick();
                    break;
                case Keys.F9:
                    btnPrimeiro.PerformClick();
                    break;
                case Keys.F10:
                    btnAnterior.PerformClick();
                    break;
                case Keys.F11:
                    btnProximo.PerformClick();
                    break;
                case Keys.F12:
                    btnUltimo.PerformClick();
                    break;
                case Keys.F1:
                    AtalhoGrade();
                    break;
                case Keys.F2:
                    AtalhoFicha();
                    break;
                case Keys.F5:
                    Ajuda();
                    break;

            }
        }

        private void PressKeyboardButton(Keys keyCode)
        {
            const int KEYEVENTF_EXTENDEDKEY = 0x1;
            const int KEYEVENTF_KEYUP = 0x2;

            keybd_event((byte)keyCode, 0x45, KEYEVENTF_EXTENDEDKEY, 0);
            keybd_event((byte)keyCode, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }

        private void UpNumLock()
        {
            bool NumLock = (GetKeyState((int)Keys.NumLock)) != 0;

            if (NumLock)
            {
                tslNum.Text = "NUM";
            }
            else
            {
                tslNum.Text = string.Empty;
            }
        }

        private void UpInsert()
        {
            bool Insert = (GetKeyState((int)Keys.Insert)) != 0;

            if (Insert)
            {
                tslInsert.Text = "INS";
            }
            else
            {
                tslInsert.Text = string.Empty;
            }
        }

        private void UpCapsLock()
        {
            bool CapsLock = (GetKeyState((int)Keys.CapsLock)) != 0;

            if (CapsLock)
            {
                tslCaps.Text = "CAPS";
            }
            else
            {
                tslCaps.Text = String.Empty;
            }

            this.Refresh();
        }

        private void tslCaps_DoubleClick(object sender, EventArgs e)
        {
            PressKeyboardButton(Keys.NumLock);
            UpCapsLock();
        }

        private void tslNum_DoubleClick(object sender, EventArgs e)
        {
            PressKeyboardButton(Keys.NumLock);
            UpNumLock();
        }

        private void tslInsert_DoubleClick(object sender, EventArgs e)
        {
            PressKeyboardButton(Keys.Insert);
            UpInsert();
        }

        private void tsmLogoff_Click(object sender, EventArgs e)
        {
            frmLogin login = new frmLogin();
            login.Text = "Login - [" + BancoDados.LeINI("Conexao", "Banco") + "] - Versão " + Principal.verSistema;
            login.ShowDialog();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                Principal.contFormularios = Principal.contFormularios - 1;

                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Sair();
                }
                this.ActiveMdiChild.Close();

                //VERIFICA SE TEM MAIS FORM ABERTO//
                if (this.MdiChildren.Length > 0)
                {
                    if (this.ActiveMdiChild is Botoes)
                    {
                        Funcoes.BotoesCadastro(this.ActiveMdiChild.Name);
                        ((Botoes)this.ActiveMdiChild).FormularioFoco();
                    }
                }
                else
                {
                    HabilitaBotoes("SSSS");
                    Principal.mdiPrincipal.btnAtualiza.Enabled = true;
                    Principal.mdiPrincipal.btnExcluir.Enabled = true;
                    Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
            }
            else
            {
                if (MessageBox.Show("Deseja realmente encerrar o sistema?", "Encerrando...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }

        }

        private void btnLogoff_Click(object sender, EventArgs e)
        {
            try
            {
                int id = this.MdiChildren.Length;

                if (id > 0)
                {
                    if (MessageBox.Show("Todas as janelas serão fechadas. Deseja continar?", "SG Pharma", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {

                        for (int i = 0; i < id; i++)
                        {
                            if (this.ActiveMdiChild is Botoes)
                            {
                                ((Botoes)this.ActiveMdiChild).Sair();
                            }
                            this.ActiveMdiChild.Close();
                        }
                        Principal.contFormularios = 0;

                        frmLogin login = new frmLogin();
                        login.Text = "Login - [" + BancoDados.LeINI("Conexao", "Banco") + "] - Versão " + Principal.verSistema;
                        login.ShowDialog();
                    }
                }
                else
                {
                    frmLogin login = new frmLogin();
                    login.Text = "Login - [" + BancoDados.LeINI("Conexao", "Banco") + "] - Versão " + Principal.verSistema;
                    login.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).AtalhoGrade();
                }
            }
        }

        public void AtalhoFicha()
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).AtalhoFicha();
                }
            }
        }

        public void Ajuda()
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Ajuda();
                }
            }
            else
            {
                Cursor = Cursors.WaitCursor;
                ProcessStartInfo sInfo = new ProcessStartInfo("http://www.drogabella.com.br/SGPHARMA/Manual/GuiaGA.html");
                Process.Start(sInfo);
                Cursor = Cursors.Default;
            }
        }

        private void btnPrimeiro_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Primeiro();
                }
            }
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Anterior();
                }
            }
        }

        private void btnProximo_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Proximo();
                }
            }
        }

        private void btnUltimo_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Ultimo();
                }
            }
        }

        private void btnAtualiza_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Atualiza();
                }
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Excluir();
                }
            }
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Incluir();
                }
            }

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).Limpar();
                }
            }
        }

        private void btnConPrecos_Click(object sender, EventArgs e)
        {
            using (frmBuscaVProd buscaPreco = new frmBuscaVProd())
            {
                buscaPreco.ShowDialog();
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (this.ActiveMdiChild is Botoes)
                {
                    ((Botoes)this.ActiveMdiChild).ImprimirRelatorio();
                }
            }
        }

        public void HabilitaBotoes(string botoes)
        {
            if (botoes.Substring(0, 1) == "N")
            {
                btnPrimeiro.Enabled = false;
            }
            else
            {
                btnPrimeiro.Enabled = true;
            }

            if (botoes.Substring(1, 1) == "N")
            {
                btnAnterior.Enabled = false;
            }
            else
            {
                btnAnterior.Enabled = true;
            }

            btnProximo.Enabled = botoes.Substring(2, 1) != "N";

            if (botoes.Substring(3, 1) == "N")
            {
                btnUltimo.Enabled = false;
            }
            else
            {
                btnUltimo.Enabled = true;
            }
        }

        private void tsmCadGrupos_Click(object sender, EventArgs e)
        {
            var cadGrupos = Application.OpenForms.OfType<frmGrupos>().FirstOrDefault();
            if (cadGrupos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var grupos = new frmGrupos(this);
                    int x = (this.Width / 2) - (grupos.Width / 2);
                    grupos.Location = new Point(x, 0);
                    grupos.MdiParent = this;
                    grupos.Botao();
                    grupos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadGrupos.FormularioFoco();
                cadGrupos.Focus();
            }
        }

        private void tsmCadUsuario_Click(object sender, EventArgs e)
        {
            var cadUsuarios = Application.OpenForms.OfType<frmUsuarios>().FirstOrDefault();
            if (cadUsuarios == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var usuarios = new frmUsuarios(this);
                    int x = (this.Width / 2) - (usuarios.Width / 2);
                    usuarios.Location = new Point(x, 0);
                    usuarios.MdiParent = this;
                    usuarios.Botao();
                    usuarios.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadUsuarios.FormularioFoco();
                cadUsuarios.Focus();
            }
        }

        private void tsmEstabelecimentos_Click(object sender, EventArgs e)
        {
            var cadEstab = Application.OpenForms.OfType<frmCadEstabelecimentos>().FirstOrDefault();
            if (cadEstab == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estabelecimentos = new frmCadEstabelecimentos(this);
                    int x = (this.Width / 2) - (estabelecimentos.Width / 2);
                    estabelecimentos.Location = new Point(x, 0);
                    estabelecimentos.MdiParent = this;
                    estabelecimentos.Botao();
                    estabelecimentos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEstab.FormularioFoco();
                cadEstab.Focus();
            }
        }

        private void MDICadastro_Activated(object sender, EventArgs e)
        {
            try
            {
                DataTable dtPermissoes = new DataTable();
                var permissaoUsuario = new Usuario
                {
                    LoginID = Principal.usuario,
                };
                dtPermissoes = permissaoUsuario.BuscaPermissoesUsuario(permissaoUsuario);
                if (dtPermissoes.Rows.Count != 0)
                {
                    for (int i = 0; i < dtPermissoes.Rows.Count; i++)
                    {
                        string menu = (string)dtPermissoes.Rows[i]["MENU"];
                        switch (menu)
                        {
                            case "MGeral":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MGeral")
                                    {
                                        MGeral.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MGeral.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MGeral")
                                    {
                                        MGeral.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MGeral.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmUsuarios":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmUsuarios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmUsuarios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MCadastro":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MCadastro")
                                    {
                                        MCadastro.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MCadastro.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MCadastro")
                                    {
                                        MCadastro.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MCadastro.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmCVendas":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmCVendas.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmCVendas.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MSngpc":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MSngpc")
                                    {
                                        MSngpc.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MSngpc.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MSngpc")
                                    {
                                        MSngpc.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MSngpc.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmRelatoriosSNGPC":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmRelatoriosSNGPC.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmRelatoriosSNGPC.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "tsmInventario":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmInventario.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmInventario.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MEstoque":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MEstoque")
                                    {
                                        MEstoque.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MEstoque.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MEstoque")
                                    {
                                        MEstoque.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MEstoque.Enabled = false;
                                    }
                                }
                                break;
                            case "MCaixa":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MCaixa")
                                    {
                                        MCaixa.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MCaixa.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MCaixa")
                                    {
                                        MCaixa.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MCaixa.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmCRelatorios":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmCRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmCRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;

                            case "tsmSuporte":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmSuporte.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmSuporte.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MVendas":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MVendas")
                                    {
                                        MVendas.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MVendas.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MVendas")
                                    {
                                        MVendas.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MVendas.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmVenRelatorios":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmVenRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmVenRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MRelatorios":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MRelatorios")
                                    {
                                        MRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MRelatorios.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MRelatorios")
                                    {
                                        MRelatorios.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MRelatorios.Enabled = false;
                                    }
                                }
                                break;
                            case "MContPagar":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MContPagar")
                                    {
                                        MContPagar.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MContPagar.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MContPagar")
                                    {
                                        MContPagar.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MContPagar.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmFinRelatorio":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmFinRelatorio.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmFinRelatorio.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MContReceber":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MContReceber")
                                    {
                                        MContReceber.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MContReceber.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MContReceber")
                                    {
                                        MContReceber.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MContReceber.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmRecRelatorio":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmRecRelatorio.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmRecRelatorio.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                            case "MAberturaChamado":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MAberturaChamado")
                                    {
                                        MAberturaChamado.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                    }
                                    else
                                    {
                                        MAberturaChamado.Enabled = true;
                                    }
                                }
                                else
                                {
                                    if ((string)dtPermissoes.Rows[i]["NOME"] != "MAberturaChamado")
                                    {
                                        MAberturaChamado.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                    }
                                    else
                                    {
                                        MAberturaChamado.Enabled = false;
                                    }
                                }
                                break;
                            case "tsmManutencaoDeFilial":
                                if ((string)dtPermissoes.Rows[i]["ACESSA"] == "S")
                                {
                                    tsmManutencaoDeFilial.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = true;
                                }
                                else
                                {
                                    tsmManutencaoDeFilial.DropDownItems[dtPermissoes.Rows[i]["NOME"].ToString()].Enabled = false;
                                }
                                break;
                        }
                    }
                }

                tslMensagem.Text = Funcoes.MudaMensagem("");
                Principal.wsCredenciado = Funcoes.LeParametro(6, "53", true);
                Principal.wsSenhaAcesso = Funcoes.LeParametro(6, "54", true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("{0}", ex.Message), "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAjuda_Click(object sender, EventArgs e)
        {
            Ajuda();
        }

        private void btnInternet_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.conectectecnologia.com.br/sgPharma/admin/Portal%20Sgpharma/login.html");
            Process.Start(sInfo);
            Cursor = Cursors.Default;
        }

        private void btnSuporte_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmFormularioChamado>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmFormularioChamado(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void MDIPrincipal_Shown(object sender, EventArgs e)
        {
            var dadosEstabelecimento = new Estabelecimento();
            List<Estabelecimento> buscaEstabelecimento = new List<Estabelecimento>();
            buscaEstabelecimento = dadosEstabelecimento.IdentificaEstabelecimentos();
            if (buscaEstabelecimento.Count.Equals(0))
            {
                MessageBox.Show(
                    "Necessário realizar o cadastro de um Estabelecimento",
                    "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnEstab.Enabled = false;
            }
            else
                if (buscaEstabelecimento.Count.Equals(1))
            {
                Principal.estAtual = buscaEstabelecimento[0].EstCodigo;
                Principal.empAtual = buscaEstabelecimento[0].EmpCodigo;
                tslMensagem.Text = Funcoes.MudaMensagem("");
                btnEstab.Enabled = false;
            }
            else
            {
                var estEscolha = new frmEstEscolha();
                estEscolha.ShowDialog();
                tslMensagem.Text = Funcoes.MudaMensagem("");
                btnEstab.Enabled = true;
            }


        }

        private void btnEstab_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                if (MessageBox.Show("Para efetuar a troca de estabelecimento é necessario que todas as janelas estejam fechadas!\nDeseja continuar?", "Seleção de Estabelecimento", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                {
                    //FECHA TODAS AS JANELAS//
                    int forms = this.MdiChildren.Length;
                    for (int i = 0; i < forms; i++)
                    {
                        if (this.ActiveMdiChild is Botoes)
                        {
                            ((Botoes)this.ActiveMdiChild).Sair();
                        }
                        this.ActiveMdiChild.Close();
                    }
                    HabilitaBotoes("SSSS");
                    Principal.mdiPrincipal.btnAtualiza.Enabled = true;
                    Principal.mdiPrincipal.btnExcluir.Enabled = true;
                    Principal.mdiPrincipal.btnIncluir.Enabled = true;
                    var estEscolha = new frmEstEscolha();
                    estEscolha.ShowDialog();
                }
            }
            else
            {
                var estEscolha = new frmEstEscolha();
                estEscolha.ShowDialog();
            }
        }

        private void tsmColaboradores_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadColab = Application.OpenForms.OfType<frmCadColaboradores>().FirstOrDefault();
            if (cadColab == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var colaboradores = new frmCadColaboradores(this);
                    colaboradores.MdiParent = this;
                    colaboradores.Botao();
                    colaboradores.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadColab.FormularioFoco();
                cadColab.Focus();
            }
        }

        private void tsmOperacoes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadOperDC = Application.OpenForms.OfType<frmCadOperacaoDC>().FirstOrDefault();
            if (cadOperDC == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var operacoesDC = new frmCadOperacaoDC(this);
                    operacoesDC.MdiParent = this;
                    operacoesDC.Botao();
                    operacoesDC.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadOperDC.FormularioFoco();
                cadOperDC.Focus();
            }
        }

        private void tsmEspecies_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEspecie = Application.OpenForms.OfType<frmCadEspecies>().FirstOrDefault();
            if (cadEspecie == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var especie = new frmCadEspecies(this);
                    especie.MdiParent = this;
                    especie.Botao();
                    especie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEspecie.FormularioFoco();
                cadEspecie.Focus();
            }
        }

        private void tsmDepartamento_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadDep = Application.OpenForms.OfType<frmCadDepartamentos>().FirstOrDefault();
            if (cadDep == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var departamento = new frmCadDepartamentos(this);
                    departamento.MdiParent = this;
                    departamento.Botao();
                    departamento.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadDep.FormularioFoco();
                cadDep.Focus();
            }
        }

        private void tsmClasses_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadClasse = Application.OpenForms.OfType<frmCadClasses>().FirstOrDefault();
            if (cadClasse == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var classes = new frmCadClasses(this);
                    classes.MdiParent = this;
                    classes.Botao();
                    classes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadClasse.FormularioFoco();
                cadClasse.Focus();
            }
        }

        private void tsmSubClasse_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadSubclasse = Application.OpenForms.OfType<frmCadSubclasse>().FirstOrDefault();
            if (cadSubclasse == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var subclasse = new frmCadSubclasse(this);
                    subclasse.MdiParent = this;
                    subclasse.Botao();
                    subclasse.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadSubclasse.FormularioFoco();
                cadSubclasse.Focus();
            }
        }

        private void tsmFabricantes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadFab = Application.OpenForms.OfType<frmCadFabricantes>().FirstOrDefault();
            if (cadFab == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var fabricantes = new frmCadFabricantes(this);
                    fabricantes.MdiParent = this;
                    fabricantes.Botao();
                    fabricantes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadFab.FormularioFoco();
                cadFab.Focus();
            }
        }

        private void tsmPrinAtivo_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadPrinAtivo = Application.OpenForms.OfType<frmCadPrinAtivo>().FirstOrDefault();
            if (cadPrinAtivo == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var prinAtivo = new frmCadPrinAtivo(this);
                    prinAtivo.MdiParent = this;
                    prinAtivo.Botao();
                    prinAtivo.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadPrinAtivo.FormularioFoco();
                cadPrinAtivo.Focus();
            }
        }

        private void tsmTabPrecos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTabPrecos = Application.OpenForms.OfType<frmCadTabPrecos>().FirstOrDefault();
            if (cadTabPrecos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var tabPrecos = new frmCadTabPrecos(this);
                    tabPrecos.MdiParent = this;
                    tabPrecos.Botao();
                    tabPrecos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTabPrecos.FormularioFoco();
                cadTabPrecos.Focus();
            }
        }

        private void tsmNatOper_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadNatOper = Application.OpenForms.OfType<frmCadNatOper>().FirstOrDefault();
            if (cadNatOper == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var natOper = new frmCadNatOper(this);
                    natOper.MdiParent = this;
                    natOper.Botao();
                    natOper.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadNatOper.FormularioFoco();
                cadNatOper.Focus();
            }
        }

        private void tsmTipoDocto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTipoDocto = Application.OpenForms.OfType<frmCadTipoDocto>().FirstOrDefault();
            if (cadTipoDocto == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var tipoDocto = new frmCadTipoDocto(this);
                    tipoDocto.MdiParent = this;
                    tipoDocto.Botao();
                    tipoDocto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTipoDocto.FormularioFoco();
                cadTipoDocto.Focus();
            }
        }

        private void tsmUnidade_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTipoUni = Application.OpenForms.OfType<frmCadUnidade>().FirstOrDefault();
            if (cadTipoUni == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var tipoUni = new frmCadUnidade(this);
                    tipoUni.MdiParent = this;
                    tipoUni.Botao();
                    tipoUni.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTipoUni.FormularioFoco();
                cadTipoUni.Focus();
            }
        }

        private void tsmProdutos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadProdutos = Application.OpenForms.OfType<frmCadProdutos>().FirstOrDefault();
            if (cadProdutos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produtos = new frmCadProdutos(this);
                    produtos.MdiParent = this;
                    produtos.Botao();
                    produtos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadProdutos.FormularioFoco();
                cadProdutos.Focus();
            }
        }

        private void tsmFornecedores_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadFornecedores = Application.OpenForms.OfType<frmCadFornecedores>().FirstOrDefault();
            if (cadFornecedores == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var fornecedores = new frmCadFornecedores(this);
                    fornecedores.MdiParent = this;
                    fornecedores.Botao();
                    fornecedores.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadFornecedores.FormularioFoco();
                cadFornecedores.Focus();
            }
        }

        private void tsmGrupos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadGrupos = Application.OpenForms.OfType<frmCadGrupos>().FirstOrDefault();
            if (cadGrupos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var gruposClientes = new frmCadGrupos(this);
                    gruposClientes.MdiParent = this;
                    gruposClientes.Botao();
                    gruposClientes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadGrupos.FormularioFoco();
                cadGrupos.Focus();
            }
        }

        private void tsmCondicoes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadCondicoes = Application.OpenForms.OfType<frmCadCondicoes>().FirstOrDefault();
            if (cadCondicoes == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var condicoes = new frmCadCondicoes(this);
                    condicoes.MdiParent = this;
                    condicoes.Botao();
                    condicoes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadCondicoes.FormularioFoco();
                cadCondicoes.Focus();
            }
        }

        private void tsmClientes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadClientes = Application.OpenForms.OfType<frmCadClientes>().FirstOrDefault();
            if (cadClientes == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var clientes = new frmCadClientes(this);
                    clientes.MdiParent = this;
                    clientes.Botao();
                    clientes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadClientes.FormularioFoco();
                cadClientes.Focus();
            }
        }

        private void tsmConveniadas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadConveniadas = Application.OpenForms.OfType<frmCadConveniadas>().FirstOrDefault();
            if (cadConveniadas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var conveniadas = new frmCadConveniadas(this);
                    conveniadas.MdiParent = this;
                    conveniadas.Botao();
                    conveniadas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadConveniadas.FormularioFoco();
                cadConveniadas.Focus();
            }
        }

        private void tsmEspecificacoes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEspecificacoes = Application.OpenForms.OfType<frmCadEspecificacoes>().FirstOrDefault();
            if (cadEspecificacoes == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var especificacoes = new frmCadEspecificacoes(this);
                    especificacoes.MdiParent = this;
                    especificacoes.Botao();
                    especificacoes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEspecificacoes.FormularioFoco();
                cadEspecificacoes.Focus();
            }
        }

        private void MDIPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (MessageBox.Show("Deseja realmente encerrar o sistema?", "Encerrando...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //{
            //    e.Cancel = true;
            //}
        }

        private void tsmMedicos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadMedicos = Application.OpenForms.OfType<frmCadMedicos>().FirstOrDefault();
            if (cadMedicos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var medicos = new frmCadMedicos(this);
                    medicos.MdiParent = this;
                    medicos.Botao();
                    medicos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadMedicos.FormularioFoco();
                cadMedicos.Focus();
            }
        }

        private void MDIPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            BancoDados.fecharConexao();
        }

        [DllImport("user32.dll")]
        internal static extern short GetKeyState(int keyCode);

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        private void tsmSngpcTecnico_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTecnicos = Application.OpenForms.OfType<frmSngpcTecnicos>().FirstOrDefault();
            if (cadTecnicos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var tecnicos = new frmSngpcTecnicos(this);
                    tecnicos.MdiParent = this;
                    tecnicos.Botao();
                    tecnicos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTecnicos.FormularioFoco();
                cadTecnicos.Focus();
            }
        }

        private void tsmSngpcTabelas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTabelas = Application.OpenForms.OfType<frmSngpcTabelas>().FirstOrDefault();
            if (cadTabelas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var tabelas = new frmSngpcTabelas(this);
                    tabelas.MdiParent = this;
                    tabelas.Botao();
                    tabelas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTabelas.FormularioFoco();
                cadTabelas.Focus();
            }
        }

        private void tsmLanInventario_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadLancamento = Application.OpenForms.OfType<frmSngpcLanInventario>().FirstOrDefault();
            if (cadLancamento == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var lancamento = new frmSngpcLanInventario(this);
                    lancamento.MdiParent = this;
                    lancamento.Botao();
                    lancamento.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadLancamento.FormularioFoco();
                cadLancamento.Focus();
            }
        }

        private void tsmPerdas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadPerdas = Application.OpenForms.OfType<frmSngpcPerda>().FirstOrDefault();
            if (cadPerdas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var perdas = new frmSngpcPerda(this);
                    perdas.MdiParent = this;
                    perdas.Botao();
                    perdas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadPerdas.FormularioFoco();
                cadPerdas.Focus();
            }
        }

        private void tsmEntrTrans_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEntradas = Application.OpenForms.OfType<frmSngpcEntrTrans>().FirstOrDefault();
            if (cadEntradas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var entradas = new frmSngpcEntrTrans(this);
                    entradas.MdiParent = this;
                    entradas.Botao();
                    entradas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEntradas.FormularioFoco();
                cadEntradas.Focus();
            }
        }

        private void tsmVendaMed_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadVendas = Application.OpenForms.OfType<frmSngpcVendas>().FirstOrDefault();
            if (cadVendas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var vendas = new frmSngpcVendas(this);
                    vendas.MdiParent = this;
                    vendas.Botao();
                    vendas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadVendas.FormularioFoco();
                cadVendas.Focus();
            }
        }

        private void tsmInventario_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadInventario = Application.OpenForms.OfType<frmSngpcInventario>().FirstOrDefault();
            if (cadInventario == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var inventario = new frmSngpcInventario(this);
                    inventario.MdiParent = this;
                    inventario.Botao();
                    inventario.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadInventario.FormularioFoco();
                cadInventario.Focus();
            }
        }

        private void tsmTransferencia_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadTransferencia = Application.OpenForms.OfType<frmSngpcTransferencia>().FirstOrDefault();
            if (cadTransferencia == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var transferencia = new frmSngpcTransferencia(this);
                    transferencia.MdiParent = this;
                    transferencia.Botao();
                    transferencia.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadTransferencia.FormularioFoco();
                cadTransferencia.Focus();
            }
        }

        private void tsmEstEntradas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEstEntradas = Application.OpenForms.OfType<frmEstEntradas>().FirstOrDefault();
            if (cadEstEntradas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estEntradas = new frmEstEntradas(this);
                    estEntradas.MdiParent = this;
                    estEntradas.Botao();
                    estEntradas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEstEntradas.FormularioFoco();
                cadEstEntradas.Focus();
            }
        }

        private void tsmAtuPreProd_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEstAtuPreProd = Application.OpenForms.OfType<frmEstAtuPreProd>().FirstOrDefault();
            if (cadEstAtuPreProd == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estAtuPreProd = new frmEstAtuPreProd(this);
                    estAtuPreProd.MdiParent = this;
                    estAtuPreProd.Botao();
                    estAtuPreProd.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEstAtuPreProd.FormularioFoco();
                cadEstAtuPreProd.Focus();
            }
        }

        private void tsmRelBalanco_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelBalanco>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var rel = new frmEstRelBalanco(this);
                    rel.MdiParent = this;
                    rel.Botao();
                    rel.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
            }
        }

        private void tsmRelValidade_Click(object sender, EventArgs e)
        {

        }

        private void tsmEstAjuste_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEstAjustes = Application.OpenForms.OfType<frmEstAjustes>().FirstOrDefault();
            if (cadEstAjustes == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estAjustes = new frmEstAjustes(this);
                    estAjustes.MdiParent = this;
                    estAjustes.BringToFront();
                    estAjustes.Botao();
                    estAjustes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEstAjustes.FormularioFoco();
            }
        }

        private void tsmEstAtuLinear_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEstAtuLinear = Application.OpenForms.OfType<frmEstAlteracaoQuantidadeLinear>().FirstOrDefault();
            if (cadEstAtuLinear == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estAtuLinear = new frmEstAlteracaoQuantidadeLinear(this);
                    estAtuLinear.MdiParent = this;
                    estAtuLinear.Botao();
                    estAtuLinear.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEstAtuLinear.FormularioFoco();
                cadEstAtuLinear.Focus();
            }
        }

        private void tsmAberFecha_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadCxAFCaixa = Application.OpenForms.OfType<frmAFCaixa>().FirstOrDefault();
            if (cadCxAFCaixa == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxAFCaixa = new frmAFCaixa(this);
                    cxAFCaixa.Botao();
                    cxAFCaixa.MdiParent = this;
                    cxAFCaixa.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadCxAFCaixa.FormularioFoco();
                cadCxAFCaixa.Focus();
            }
        }

        private void tsmCanAberFecha_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//

            var cadAFCaixaCan = Application.OpenForms.OfType<frmAFCancelaCaixa>().FirstOrDefault();
            if (cadAFCaixaCan == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxAFCaixaCan = new frmAFCancelaCaixa(this);
                    cxAFCaixaCan.Botao();
                    cxAFCaixaCan.MdiParent = this;
                    cxAFCaixaCan.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadAFCaixaCan.FormularioFoco();
            }
        }

        private void tsmMovCaixa_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadCxMovCaixa = Application.OpenForms.OfType<frmCxMovCaixa>().FirstOrDefault();
            if (cadCxMovCaixa == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxMovCaixa = new frmCxMovCaixa(this);
                    cxMovCaixa.MdiParent = this;
                    cxMovCaixa.Botao();
                    cxMovCaixa.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadCxMovCaixa.FormularioFoco();
                cadCxMovCaixa.Focus();
            }
        }

        private void tsmRecebimentos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadRecebimentos = Application.OpenForms.OfType<frmCxRecebimentos>().FirstOrDefault();
            if (cadRecebimentos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxRecebimentos = new frmCxRecebimentos(this);
                    cxRecebimentos.MdiParent = this;
                    cxRecebimentos.Botao();
                    cxRecebimentos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadRecebimentos.FormularioFoco();
                cadRecebimentos.Focus();
            }
        }

        private void tsmRecebConveniadas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadRecebConv = Application.OpenForms.OfType<frmCxRecebConv>().FirstOrDefault();
            if (cadRecebConv == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxRecebConv = new frmCxRecebConv(this);
                    cxRecebConv.MdiParent = this;
                    cxRecebConv.Botao();
                    cxRecebConv.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadRecebConv.FormularioFoco();
                cadRecebConv.Focus();
            }
        }

        private void tsmCadDespesas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadDespesas = Application.OpenForms.OfType<frmFinCadDesp>().FirstOrDefault();
            if (cadDespesas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var finDespesas = new frmFinCadDesp(this);
                    finDespesas.MdiParent = this;
                    finDespesas.Botao();
                    finDespesas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadDespesas.FormularioFoco();
                cadDespesas.Focus();
            }
        }

        private void tsmFinContasPagar_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var finContasPagar = Application.OpenForms.OfType<frmFinContasPagar>().FirstOrDefault();
            if (finContasPagar == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var finContPagar = new frmFinContasPagar(this);
                    finContPagar.MdiParent = this;
                    finContPagar.Botao();
                    finContPagar.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                finContasPagar.FormularioFoco();
                finContasPagar.Focus();
            }
        }

        private void tsmFinDespesas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var finDespesas = Application.OpenForms.OfType<frmFinDespesas>().FirstOrDefault();
            if (finDespesas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var despesas = new frmFinDespesas(this);
                    despesas.MdiParent = this;
                    despesas.Botao();
                    despesas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                finDespesas.FormularioFoco();
                finDespesas.Focus();
            }
        }

        private void tsmFinRelatorio_Click(object sender, EventArgs e)
        {

        }

        private void tsmParametros_Click(object sender, EventArgs e)
        {
            var gParametros = Application.OpenForms.OfType<frmParametros>().FirstOrDefault();
            if (gParametros == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var parametros = new frmParametros(this);
                    parametros.MdiParent = this;
                    parametros.Botao();
                    parametros.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                gParametros.FormularioFoco();
                gParametros.Focus();
            }
        }

        private void tsmVenComanda_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var gVendas = Application.OpenForms.OfType<frmVenda>().FirstOrDefault();
            if (gVendas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var vendas = new frmVenda(this);
                    if (Funcoes.LeParametro(6, "361", true).Equals("S"))
                    {
                        if (Funcoes.LeParametro(6, "377", true, Principal.nomeEstacao).Equals("S"))
                        {
                            if (Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F"))
                            {
                                Principal.tipoCupom = "C";
                            }
                            else
                                Principal.tipoCupom = "P";
                        }
                        else
                            Principal.tipoCupom = "P";
                    }
                    else
                    {
                        Principal.tipoCupom = "P";
                    }
                    vendas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                gVendas.Focus();
            }
        }

        private void tsmPromocoes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadPromocoes = Application.OpenForms.OfType<frmCadPromocoes>().FirstOrDefault();
            if (cadPromocoes == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var promocoes = new frmCadPromocoes(this);
                    promocoes.MdiParent = this;
                    promocoes.Botao();
                    promocoes.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadPromocoes.FormularioFoco();
                cadPromocoes.Focus();
            }
        }

        private void tsmDescontos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadDescontos = Application.OpenForms.OfType<frmCadDescontos>().FirstOrDefault();
            if (cadDescontos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var descontos = new frmCadDescontos(this);
                    descontos.MdiParent = this;
                    descontos.Botao();
                    descontos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadDescontos.FormularioFoco();
                cadDescontos.Focus();
            }
        }

        private void tsmTEntregas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEntregas = Application.OpenForms.OfType<frmCadEntregas>().FirstOrDefault();
            if (cadEntregas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var entregas = new frmCadEntregas(this);
                    entregas.MdiParent = this;
                    entregas.Botao();
                    entregas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEntregas.FormularioFoco();
                cadEntregas.Focus();
            }
        }


        private void tsmComandas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadComanda = Application.OpenForms.OfType<frmCadComanda>().FirstOrDefault();
            if (cadComanda == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var comanda = new frmCadComanda(this);
                    comanda.MdiParent = this;
                    comanda.Botao();
                    comanda.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadComanda.FormularioFoco();
                cadComanda.Focus();
            }
        }

        private void tsmAtuTabIBPT_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var estAtuIBPT = Application.OpenForms.OfType<frmEstAtuIBPT>().FirstOrDefault();
            if (estAtuIBPT == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var atuIBPT = new frmEstAtuIBPT(this);
                    atuIBPT.MdiParent = this;
                    atuIBPT.Botao();
                    atuIBPT.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                estAtuIBPT.FormularioFoco();
                estAtuIBPT.Focus();
            }
        }

        private void tsmCodNCM_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadProdNCM = Application.OpenForms.OfType<frmCadProdNCM>().FirstOrDefault();
            if (cadProdNCM == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var prodNCM = new frmCadProdNCM(this);
                    prodNCM.MdiParent = this;
                    prodNCM.Botao();
                    prodNCM.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadProdNCM.FormularioFoco();
                cadProdNCM.Focus();
            }
        }

        private void tsmCFechaDiario_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var rCxRelFechaDiario = Application.OpenForms.OfType<frmCxRelFechaDiario>().FirstOrDefault();
            if (rCxRelFechaDiario == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cxRelFechaDiario = new frmCxRelFechaDiario(this);
                    cxRelFechaDiario.MdiParent = this;
                    cxRelFechaDiario.Botao();
                    cxRelFechaDiario.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                rCxRelFechaDiario.FormularioFoco();
                rCxRelFechaDiario.Focus();
            }
        }

        private void tsmFPVendedor_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadVenFPopular = Application.OpenForms.OfType<frmCadFPVendedor>().FirstOrDefault();
            if (cadVenFPopular == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cadFPopular = new frmCadFPVendedor(this);
                    cadFPopular.MdiParent = this;
                    cadFPopular.Botao();
                    cadFPopular.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadVenFPopular.FormularioFoco();
                cadVenFPopular.Focus();
            }
        }

        private void tsmVenUtilitariosEcf_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var venUtiEcf = Application.OpenForms.OfType<frmVenECF>().FirstOrDefault();
            if (venUtiEcf == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var utiEcf = new frmVenECF(this);
                    utiEcf.MdiParent = this;
                    utiEcf.Botao();
                    utiEcf.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                venUtiEcf.FormularioFoco();
                venUtiEcf.Focus();
            }
        }

        private void tsmManutParcelas_Click(object sender, EventArgs e)
        {

        }

        private void tsmCadTitulo_Click(object sender, EventArgs e)
        {
        }

        private void tsmLancamentoTitulos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var lancTitulos = Application.OpenForms.OfType<frmFinTitulos>().FirstOrDefault();
            if (lancTitulos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var finTitulos = new frmFinTitulos(this);
                    finTitulos.MdiParent = this;
                    finTitulos.Botao();
                    finTitulos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                lancTitulos.FormularioFoco();
                lancTitulos.Focus();
            }
        }

        private void tsmRecManutParcelas_Click(object sender, EventArgs e)
        {

        }

        private void tsmFinContasReceber_Click(object sender, EventArgs e)
        {

        }

        private void tsmEmpresas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadEmpresa = Application.OpenForms.OfType<frmCadEmpresa>().FirstOrDefault();
            if (cadEmpresa == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var fCadEmpresa = new frmCadEmpresa(this);
                    fCadEmpresa.MdiParent = this;
                    fCadEmpresa.Botao();
                    fCadEmpresa.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadEmpresa.FormularioFoco();
                cadEmpresa.Focus();
            }
        }

        private void tsmEstoqueFiliais_Click(object sender, EventArgs e)
        {
            frmVenSupSenha senhaSup = new frmVenSupSenha(1);
            senhaSup.ShowDialog();
            if (senhaSup.sValida.Equals(true))
            {
                //VERIFICA SE FORM JÁ ESTÁ ABERTO//
                var estFiliais = Application.OpenForms.OfType<frmEstFiliais>().FirstOrDefault();
                if (estFiliais == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var filiais = new frmEstFiliais(this);
                        filiais.MdiParent = this;
                        filiais.Botao();
                        filiais.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    estFiliais.FormularioFoco();
                    estFiliais.Focus();
                }
            }
        }

        private void tsmHistorioDeEnvios_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var rHistoricoEnvios = Application.OpenForms.OfType<frmSngpcHistoricoEnvios>().FirstOrDefault();
            if (rHistoricoEnvios == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var historicoEnvios = new frmSngpcHistoricoEnvios(this);
                    historicoEnvios.MdiParent = this;
                    historicoEnvios.Botao();
                    historicoEnvios.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                rHistoricoEnvios.FormularioFoco();
                rHistoricoEnvios.Focus();
            }
        }

        private void tsmCancelamentoVendas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var vCancelamento = Application.OpenForms.OfType<frmVenCancelaNF>().FirstOrDefault();
            if (vCancelamento == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cancelamento = new frmVenCancelaNF(this);
                    cancelamento.MdiParent = this;
                    cancelamento.StartPosition = FormStartPosition.CenterScreen;
                    cancelamento.Botao();
                    cancelamento.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                vCancelamento.FormularioFoco();
                vCancelamento.Focus();
            }
        }

        private void tsmControleEntrega_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var vControleEntrega = Application.OpenForms.OfType<frmVenControleEntrega>().FirstOrDefault();
            if (vControleEntrega == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var entrega = new frmVenControleEntrega(this);
                    entrega.MdiParent = this;
                    entrega.StartPosition = FormStartPosition.CenterScreen;
                    entrega.Botao();
                    entrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                vControleEntrega.FormularioFoco();
                vControleEntrega.Focus();
            }
        }

        private void tsmMovimentoEspecie_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var movimentoEspecie = Application.OpenForms.OfType<frmMovimentoEspecie>().FirstOrDefault();
            if (movimentoEspecie == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var movEspecie = new frmMovimentoEspecie(this);
                    movEspecie.MdiParent = this;
                    movEspecie.Botao();
                    movEspecie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                movimentoEspecie.FormularioFoco();
                movimentoEspecie.Focus();
            }
        }

        private void tsmMovimentacaoOperaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var movimentoOperacao = Application.OpenForms.OfType<frmMovimentacaoPorOperacao>().FirstOrDefault();
            if (movimentoOperacao == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var movOperacao = new frmMovimentacaoPorOperacao(this);
                    movOperacao.MdiParent = this;
                    movOperacao.Botao();
                    movOperacao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                movimentoOperacao.FormularioFoco();
            }
        }

        private void resumoDiárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var resumoDiario = Application.OpenForms.OfType<frmResumoDiario>().FirstOrDefault();
            if (resumoDiario == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var resuDiario = new frmResumoDiario(this);
                    resuDiario.MdiParent = this;
                    resuDiario.Botao();
                    resuDiario.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                resumoDiario.FormularioFoco();
            }
        }

        private void resumoGeralDeVendasPorPerídoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var resumoGeral = Application.OpenForms.OfType<frmResumoGeralVendasPeriodo>().FirstOrDefault();
            if (resumoGeral == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var resumo = new frmResumoGeralVendasPeriodo(this);
                    resumo.MdiParent = this;
                    resumo.Botao();
                    resumo.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                resumoGeral.FormularioFoco();
            }

        }

        private void recebimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var recebimentos = Application.OpenForms.OfType<frmRecebimentos>().FirstOrDefault();
            if (recebimentos == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var recebi = new frmRecebimentos(this);
                    recebi.MdiParent = this;
                    recebi.Botao();
                    recebi.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                recebimentos.FormularioFoco();
            }
        }

        private void perdasDeMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO// 
            var perdas = Application.OpenForms.OfType<frmSngpcRelatorioPerda>().FirstOrDefault();
            if (perdas == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relarioPerda = new frmSngpcRelatorioPerda(this);
                    relarioPerda.MdiParent = this;
                    relarioPerda.Botao();
                    relarioPerda.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                //  perdas.Focus();
                perdas.FormularioFoco();
            }

        }

        private void comprasDeMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var compras = Application.OpenForms.OfType<frmSngpcRelatorioCompras>().FirstOrDefault();
            if (compras == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relarioPerda = new frmSngpcRelatorioCompras(this);
                    relarioPerda.MdiParent = this;
                    relarioPerda.Botoes();
                    relarioPerda.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                compras.FormularioFoco();
            }
        }

        private void vendasDeMedicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var vendas = Application.OpenForms.OfType<frmSngpcRelatorioVendas>().FirstOrDefault();
            if (vendas == null)
            {

                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioVendas = new frmSngpcRelatorioVendas(this);
                    relatorioVendas.MdiParent = this;
                    relatorioVendas.Botoes();
                    relatorioVendas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                vendas.FormularioFoco();
            }
        }

        private void atualizaçãoLinearDePreçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var alteracaoPreco = Application.OpenForms.OfType<frmEstAlteracaoPrecoLinear>().FirstOrDefault();
            if (alteracaoPreco == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var preco = new frmEstAlteracaoPrecoLinear(this);
                    preco.MdiParent = this;
                    preco.Botao();
                    preco.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                alteracaoPreco.FormularioFoco();
                alteracaoPreco.Focus();
            }
        }

        private void tsmVenTransferencia_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var fTransferencia = Application.OpenForms.OfType<frmVenTransferenciaProdutos>().FirstOrDefault();
            if (fTransferencia == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var transferencia = new frmVenTransferenciaProdutos(this);
                    transferencia.MdiParent = this;
                    transferencia.Botao();
                    transferencia.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                fTransferencia.FormularioFoco();
                fTransferencia.Focus();
            }
        }

        private void tsmDevolucaoDeProdutos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var fDevolucao = Application.OpenForms.OfType<frmVenDevolucaoProdutos>().FirstOrDefault();
            if (fDevolucao == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var devolucao = new frmVenDevolucaoProdutos(this);
                    devolucao.MdiParent = this;
                    devolucao.Botao();
                    devolucao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                fDevolucao.FormularioFoco();
            }
        }


        private void autorizaçõesFarmáciaPopularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var vFarmaciaPopular = Application.OpenForms.OfType<frmEstornaAutorizacao>().FirstOrDefault();
            if (vFarmaciaPopular == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var fpopular = new frmEstornaAutorizacao(this);
                    fpopular.MdiParent = this;
                    fpopular.StartPosition = FormStartPosition.CenterScreen;
                    fpopular.Botao();
                    fpopular.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                vFarmaciaPopular.FormularioFoco();
                vFarmaciaPopular.Focus();
            }
        }

        private void manutencaoDeProdutosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVenSupSenha senhaSup = new frmVenSupSenha(1);
            senhaSup.ShowDialog();
            if (senhaSup.sValida.Equals(true))
            {
                //VERIFICA SE FORM JÁ ESTÁ ABERTO//
                var manutencao = Application.OpenForms.OfType<frmEstManutencaoProdutos>().FirstOrDefault();
                if (manutencao == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var manutencaoProdutos = new frmEstManutencaoProdutos(this);
                        manutencaoProdutos.Botao();
                        manutencaoProdutos.MdiParent = this;
                        manutencaoProdutos.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    manutencao.FormularioFoco();
                }
            }
        }

        private void cadastroDeVendedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadVenFPopular = Application.OpenForms.OfType<frmCadFPVendedor>().FirstOrDefault();
            if (cadVenFPopular == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cadFPopular = new frmCadFPVendedor(this);
                    cadFPopular.MdiParent = this;
                    cadFPopular.Botao();
                    cadFPopular.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadVenFPopular.FormularioFoco();
                cadVenFPopular.Focus();
            }
        }

        private void tsmReposicaoProdutos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relReposicao = Application.OpenForms.OfType<frmEstRelReposicaoProdutos>().FirstOrDefault();
            if (relReposicao == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var reposicao = new frmEstRelReposicaoProdutos(this);
                    reposicao.MdiParent = this;
                    reposicao.StartPosition = FormStartPosition.CenterScreen;
                    reposicao.Botao();
                    reposicao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relReposicao.FormularioFoco();
                relReposicao.Focus();
            }
        }

        private void tsmRecRelatorio_Click(object sender, EventArgs e)
        {

        }

        private void dsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadInventario = Application.OpenForms.OfType<frmSngpcInventario>().FirstOrDefault();
            if (cadInventario == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var inventario = new frmSngpcInventario(this);
                    inventario.MdiParent = this;
                    inventario.Botao();
                    inventario.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadInventario.FormularioFoco();
                cadInventario.Focus();
            }
        }

        private void tsmLanInventario2_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var cadLancamento = Application.OpenForms.OfType<frmSngpcLanInventario>().FirstOrDefault();
            if (cadLancamento == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var lancamento = new frmSngpcLanInventario(this);
                    lancamento.MdiParent = this;
                    lancamento.Botao();
                    lancamento.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                cadLancamento.FormularioFoco();
                cadLancamento.Focus();
            }
        }

        private void tsmSenha_Click(object sender, EventArgs e)
        {
            frmVenSupSenha senhaSup = new frmVenSupSenha(1);
            senhaSup.ShowDialog();
            if (senhaSup.sValida.Equals(true))
            {
                frmSenha cSenha = new frmSenha();
                cSenha.ShowDialog();

            }

        }

        private void tsmEstManutencaoCodBarras_Click(object sender, EventArgs e)
        {
            frmVenSupSenha senhaSup = new frmVenSupSenha(1);
            senhaSup.ShowDialog();
            if (senhaSup.sValida.Equals(true))
            {
                var manutencaoCodBarras = Application.OpenForms.OfType<frmEstManutencaoCodBarras>().FirstOrDefault();
                if (manutencaoCodBarras == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var CodBarras = new frmEstManutencaoCodBarras(this);
                        CodBarras.MdiParent = this;
                        CodBarras.Botao();
                        CodBarras.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    manutencaoCodBarras.FormularioFoco();
                    manutencaoCodBarras.Focus();
                }
            }
        }

        private void tsmVenImportarTransferencia_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var impTransferencia = Application.OpenForms.OfType<frmVenImportarTransferencia>().FirstOrDefault();
            if (impTransferencia == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var importar = new frmVenImportarTransferencia(this);
                    importar.MdiParent = this;
                    importar.Botao();
                    importar.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                impTransferencia.FormularioFoco();
            }
        }

        private void btnAcesso_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            System.Diagnostics.Process.Start("https://download.teamviewer.com/download/version_10x/TeamViewerQS-idctudvre3.exe");
            Cursor = Cursors.Default;
        }

        private void tsmVenRelPeriodo_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelPeriodo>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relPeriodo = new frmVenRelPeriodo(this);
                    relPeriodo.MdiParent = this;
                    relPeriodo.Botao();
                    relPeriodo.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmVenRelFormaPagto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelFormaPagamento>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relPeriodo = new frmVenRelFormaPagamento(this);
                    relPeriodo.MdiParent = this;
                    relPeriodo.Botao();
                    relPeriodo.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmVenRelComissaoVen_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelComissaoVendedor>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relComissao = new frmVenRelComissaoVendedor(this);
                    relComissao.MdiParent = this;
                    relComissao.Botao();
                    relComissao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmVenRelEspecie_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelEspecie>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relEspecie = new frmVenRelEspecie(this);
                    relEspecie.MdiParent = this;
                    relEspecie.Botao();
                    relEspecie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog1 = new PrintDialog();
            printDialog1.Document = printDocument1;
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string nomeImpressora = printDialog1.PrinterSettings.PrinterName;

                Funcoes.GravaParametro(2, "110", nomeImpressora, Principal.nomeEstacao, "Impressora para Impressão de Comprovante Cancelamento (Impressão texto)", "IMPRESSORA", "Impressora para Impressão de Comprovante Cancelamento (Impressão texto)");
                Funcoes.GravaParametro(2, "15", nomeImpressora, Principal.nomeEstacao, "Impressora para impressão de operações de caixa (Impressão Texto)", "IMPRESSORA", "Impressora para impressão de operações de caixa (Impressão Texto)");
                Funcoes.GravaParametro(2, "18", nomeImpressora, Principal.nomeEstacao, "Impressora para Impressão de Vendas(Impressão texto)", "IMPRESSORA", "Impressora para Impressão de Vendas(Impressão texto)");
            }
        }

        private void tsmLancamentoDevolucao_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var fDevolucao = Application.OpenForms.OfType<frmVenLancamentoDevolucao>().FirstOrDefault();
            if (fDevolucao == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var devolucao = new frmVenLancamentoDevolucao(this);
                    devolucao.MdiParent = this;
                    devolucao.Botao();
                    devolucao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                fDevolucao.FormularioFoco();
            }
        }

        private void tsmManutencaoComanda_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var manutencaoComanda = Application.OpenForms.OfType<frmVenManutencaoComanda>().FirstOrDefault();
            if (manutencaoComanda == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var manutencao = new frmVenManutencaoComanda(this);
                    manutencao.MdiParent = this;
                    manutencao.Botao();
                    manutencao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                manutencaoComanda.FormularioFoco();
            }
        }

        private void tsmTabelaDCB_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var estAtuDCB = Application.OpenForms.OfType<frmEstAtuDCB>().FirstOrDefault();
            if (estAtuDCB == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var atuDCB = new frmEstAtuDCB(this);
                    atuDCB.MdiParent = this;
                    atuDCB.Botao();
                    atuDCB.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                estAtuDCB.FormularioFoco();
                estAtuDCB.Focus();
            }
        }

        private void tsmCadPrecosFP_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var precosFP = Application.OpenForms.OfType<frmCadPrecosFP>().FirstOrDefault();
            if (precosFP == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var precos = new frmCadPrecosFP(this);
                    precos.MdiParent = this;
                    precos.Botao();
                    precos.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                precosFP.FormularioFoco();
                precosFP.Focus();
            }
        }

        private void tsmRelComandasAberto_Click(object sender, EventArgs e)
        {
            frmRelComandaAberto relatorio = new frmRelComandaAberto();
            relatorio.Text = "Comandas em Aberto";
            relatorio.ShowDialog();
        }

        private void tsmRelPagFornecedores_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelBoletosFornecedores>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relEspecie = new frmRelBoletosFornecedores(this);
                    relEspecie.MdiParent = this;
                    relEspecie.Botao();
                    relEspecie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
            }
        }

        private void tsmRelFichaProduto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelFichaProduto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relEspecie = new frmEstRelFichaProduto(this);
                    relEspecie.MdiParent = this;
                    relEspecie.Botao();
                    relEspecie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
            }
        }

        private void tsmRelTransferencia_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelTransferenciaProduto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relEspecie = new frmRelTransferenciaProduto(this);
                    relEspecie.MdiParent = this;
                    relEspecie.Botao();
                    relEspecie.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
            }
        }

        private void tsmVenNFe_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var nfe = Application.OpenForms.OfType<frmVenNFE>().FirstOrDefault();
            if (nfe == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var emissaoNFe = new frmVenNFE(this);
                    emissaoNFe.MdiParent = this;
                    emissaoNFe.Botao();
                    emissaoNFe.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                nfe.FormularioFoco();
            }
        }

        private void tsmRelEntregasAberto_Click(object sender, EventArgs e)
        {
            frmRelEntregasEmAberto relatorio = new frmRelEntregasEmAberto();
            relatorio.Text = "Entregas em Aberto";
            relatorio.ShowDialog();
        }

        private void tsmCartaCobranca_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var carta = Application.OpenForms.OfType<frmFinCartaCobranca>().FirstOrDefault();
            if (carta == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var cartaCobranca = new frmFinCartaCobranca(this);
                    cartaCobranca.MdiParent = this;
                    cartaCobranca.Botao();
                    cartaCobranca.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                carta.FormularioFoco();
            }
        }

        private void tsmProdutoPorPeriodo_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var produtoPorPeriodo = Application.OpenForms.OfType<frmVenRelProdutosPorPeriodo>().FirstOrDefault();
            if (produtoPorPeriodo == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmVenRelProdutosPorPeriodo(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                produtoPorPeriodo.FormularioFoco();
                produtoPorPeriodo.Focus();
            }

        }

        private void tsmRelInventario_Click(object sender, EventArgs e)
        {
            frmRelatorioDeInventario relatorio = new frmRelatorioDeInventario();
            relatorio.Text = "Inventário SNGPC";
            relatorio.ShowDialog();
        }

        private void tsmProdutosMaisVendidos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelProdutosVendidos>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmVenRelProdutosVendidos(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmProdutoPorCliente_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelProdutosPorCliente>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmVenRelProdutosPorCliente(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelVenDescontoConcedido_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelDescontosConcedidos>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmVenRelDescontosConcedidos(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private async void timerReserva_Tick(object sender, EventArgs e)
        {
            if (Util.TesteIPServidorNuvem())
            {
                List<Reserva> dados = await BuscaReservasDeProdutos();

                if (dados.Count > 0)
                {
                    if (dados[0].CodErro == "00")
                    {
                        mensagemReserva.Image = Properties.Resources.alert;
                        mensagemReserva.ImagePadding = new Padding(10);
                        mensagemReserva.TitlePadding = new Padding(10);
                        mensagemReserva.ContentPadding = new Padding(10);
                        mensagemReserva.AnimationDuration = 2;
                        mensagemReserva.TitleText = "Reserva de Produtos";
                        mensagemReserva.ContentText = "\nVERIFICAR RESERVA(S) PENDENTE(S)";
                        mensagemReserva.Popup();

                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notificacao);
                        simpleSound.Play();
                    }
                }
            }
        }

        private void mensagemReserva_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Reserva(s) Pendente(s). Verificar na Tela de Transferência de Produtos", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public async Task<List<Reserva>> BuscaReservasDeProdutos()
        {
            List<Reserva> allItems = new List<Reserva>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/BuscaReservaPorCodLojaEStatus?codLoja=" + Funcoes.LeParametro(9, "52", true)
                        + "&status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Reserva>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void tsmRelEmpresaConveniada_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelEmpresasConveniadas>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmRelEmpresasConveniadas(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmVenRelComissaoProd_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelComissaoProduto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relComissao = new frmVenRelComissaoProduto(this);
                    relComissao.MdiParent = this;
                    relComissao.Botao();
                    relComissao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelVendaFiscal_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelFiscais>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relComissao = new frmVenRelFiscais(this);
                    relComissao.MdiParent = this;
                    relComissao.Botao();
                    relComissao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmCaixaXContas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmFinCaixaXPagar>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relComissao = new frmFinCaixaXPagar(this);
                    relComissao.MdiParent = this;
                    relComissao.Botao();
                    relComissao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmManutencaoEstoque_Click(object sender, EventArgs e)
        {
            frmVenSupSenha senhaSup = new frmVenSupSenha(1);
            senhaSup.ShowDialog();
            if (senhaSup.sValida.Equals(true))
            {//VERIFICA SE FORM JÁ ESTÁ ABERTO//
                var estoque = Application.OpenForms.OfType<frmEstManutencaoEstoque>().FirstOrDefault();
                if (estoque == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var relComissao = new frmEstManutencaoEstoque(this);
                        relComissao.MdiParent = this;
                        relComissao.Botao();
                        relComissao.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    estoque.FormularioFoco();
                    estoque.Focus();
                }
            }
        }

        private void tsmBalancoSngpc_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var sngpc = Application.OpenForms.OfType<frmSngpcRelBalanco>().FirstOrDefault();
            if (sngpc == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relSngpc = new frmSngpcRelBalanco(this);
                    relSngpc.MdiParent = this;
                    relSngpc.Botao();
                    relSngpc.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                sngpc.FormularioFoco();
                sngpc.Focus();
            }
        }

        private void tsmRelCurvaAbc_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelCurvaAbc>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmVenRelCurvaAbc(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelListaPosNeg_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelListaPosNeg>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmRelListaPosNeg(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void MContPagar_Click(object sender, EventArgs e)
        {

        }

        private void tsmRelRecebimentoTitulo_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelTitlosPagos>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmRelTitlosPagos(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelProdXCusto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelCustoxProduto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmVenRelCustoxProduto(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmManutencaoCpfCnpj_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmCadManutencaoCpfCnpj>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmCadManutencaoCpfCnpj(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelVendasCanceladas_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelCanceladas>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmVenRelCanceladas(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelAbaixoEst_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelMaxMin>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relCurva = new frmEstRelMaxMin(this);
                    relCurva.MdiParent = this;
                    relCurva.Botao();
                    relCurva.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelProdutoPorEspecie_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelVenProdutoPorEspecie>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var rel = new frmRelVenProdutoPorEspecie(this);
                    rel.MdiParent = this;
                    rel.Botao();
                    rel.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmLancamentoEntrega_Click(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
            {
                //VERIFICA SE FORM JÁ ESTÁ ABERTO//
                var entrega = Application.OpenForms.OfType<frmVenLancamentoEntrega>().FirstOrDefault();
                if (entrega == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var lancamentoEntrega = new frmVenLancamentoEntrega(this);
                        lancamentoEntrega.MdiParent = this;
                        lancamentoEntrega.Botao();
                        lancamentoEntrega.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    entrega.FormularioFoco();
                    entrega.Focus();
                }
            }
            else
            {
                MessageBox.Show("Estabelecimento sem Outra Filial", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #region ENTREGA FILIAL
        private async void timerEntrega_Tick(object sender, EventArgs e)
        {
            if (Util.TesteIPServidorNuvem())
            {
                List<EntregaFilial> dados = await BuscaEntregaFilial();

                if (dados.Count > 0)
                {
                    if (dados[0].CodErro == "00")
                    {
                        mensagemEntrega.Image = Properties.Resources.entrega;
                        mensagemEntrega.ImagePadding = new Padding(10);
                        mensagemEntrega.TitlePadding = new Padding(10);
                        mensagemEntrega.ContentPadding = new Padding(10);
                        mensagemEntrega.AnimationDuration = 2;
                        mensagemEntrega.TitleText = "Entrega entre Filiais";
                        mensagemEntrega.ContentText = "\nVERIFICAR ENTREGA(S) PENDENTE(S)";
                        mensagemEntrega.Popup();

                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notificacao);
                        simpleSound.Play();
                    }
                }
            }
        }

        private void mensagemEntrega_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Entrega(s) Pendente(s). Verificar na Tela ", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public async Task<List<EntregaFilial>> BuscaEntregaFilial()
        {
            List<EntregaFilial> allItems = new List<EntregaFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/BuscaEntregaPorCodLojaEStatus?codLoja=" + Funcoes.LeParametro(9, "52", true)
                        + "&status=P");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EntregaFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }
        #endregion

        private void tsmControleEntregaFilial_Click(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
            {
                //VERIFICA SE FORM JÁ ESTÁ ABERTO//
                var entrega = Application.OpenForms.OfType<frmVenControleEntregaFilial>().FirstOrDefault();
                if (entrega == null)
                {
                    if (Principal.contFormularios <= 13)
                    {
                        Principal.contFormularios = Principal.contFormularios + 1;
                        var controleEntrega = new frmVenControleEntregaFilial(this);
                        controleEntrega.MdiParent = this;
                        controleEntrega.Botao();
                        controleEntrega.Show();
                    }
                    else
                    {
                        MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    entrega.FormularioFoco();
                    entrega.Focus();
                }
            }
            else
            {
                MessageBox.Show("Estabelecimento sem Outra Filial", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void tsmVendaEntregaFilial_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelEntregaFilial>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioEntrega = new frmVenRelEntregaFilial(this);
                    relatorioEntrega.MdiParent = this;
                    relatorioEntrega.Botao();
                    relatorioEntrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmProdutosMenosVendidos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelProdutosMenosVendidos>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioEntrega = new frmVenRelProdutosMenosVendidos(this);
                    relatorioEntrega.MdiParent = this;
                    relatorioEntrega.Botao();
                    relatorioEntrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelFormaXEspecie_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelFormaXEspecie>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioEntrega = new frmVenRelFormaXEspecie(this);
                    relatorioEntrega.MdiParent = this;
                    relatorioEntrega.Botao();
                    relatorioEntrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelEntDocumento_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelEntDoc>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioEntrega = new frmEstRelEntDoc(this);
                    relatorioEntrega.MdiParent = this;
                    relatorioEntrega.Botao();
                    relatorioEntrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void relatórioDeEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRel>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioEntrega = new frmEstRel(this);
                    relatorioEntrega.MdiParent = this;
                    relatorioEntrega.Botao();
                    relatorioEntrega.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelImpostos_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelImposto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioImposto = new frmEstRelImposto(this);
                    relatorioImposto.MdiParent = this;
                    relatorioImposto.Botao();
                    relatorioImposto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelComissaoProduto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelComissaoProduto>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioComissao = new frmEstRelComissaoProduto(this);
                    relatorioComissao.MdiParent = this;
                    relatorioComissao.Botao();
                    relatorioComissao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmVenEstornoManual_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var estorno = Application.OpenForms.OfType<frmEstornoFP>().FirstOrDefault();
            if (estorno == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var estornoFP = new frmEstornoFP(this);
                    estornoFP.MdiParent = this;
                    estornoFP.Botao();
                    estornoFP.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                estorno.FormularioFoco();
                estorno.Focus();
            }
        }

        private void tsmRelLucroXCusto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmVenRelCustoxLucro>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioVendas = new frmVenRelCustoxLucro(this);
                    relatorioVendas.MdiParent = this;
                    relatorioVendas.Botao();
                    relatorioVendas.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelAjustes_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelAjuste>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioAjuste = new frmEstRelAjuste(this);
                    relatorioAjuste.MdiParent = this;
                    relatorioAjuste.Botao();
                    relatorioAjuste.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelEmpresaParticular_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelClienteParticular>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioCliente = new frmRelClienteParticular(this);
                    relatorioCliente.MdiParent = this;
                    relatorioCliente.Botao();
                    relatorioCliente.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmManutencaoCusto_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var manutencao = Application.OpenForms.OfType<frmEstManutencaoUltCusto>().FirstOrDefault();
            if (manutencao == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var manutencaoCusto = new frmEstManutencaoUltCusto(this);
                    manutencaoCusto.MdiParent = this;
                    manutencaoCusto.Botao();
                    manutencaoCusto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                manutencao.FormularioFoco();
                manutencao.Focus();
            }
        }

        private void tsmEstAbcFarma_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var estAtuPreProd = Application.OpenForms.OfType<frmEstAtuAbcFarma>().FirstOrDefault();
            if (estAtuPreProd == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var atuPreProd = new frmEstAtuAbcFarma(this);
                    atuPreProd.MdiParent = this;
                    atuPreProd.Botao();
                    atuPreProd.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                estAtuPreProd.FormularioFoco();
                estAtuPreProd.Focus();
            }
        }

        private void tsmRelPrecosAlterados_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstRelPrecosAlterados>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioABCFarma = new frmEstRelPrecosAlterados(this);
                    relatorioABCFarma.MdiParent = this;
                    relatorioABCFarma.Botao();
                    relatorioABCFarma.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmLogAlteracao_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelLogAlteracao>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioABCFarma = new frmRelLogAlteracao(this);
                    relatorioABCFarma.MdiParent = this;
                    relatorioABCFarma.Botao();
                    relatorioABCFarma.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmEntregaPeriodo_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelEntregaPeriodo>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var relatorioABCFarma = new frmRelEntregaPeriodo(this);
                    relatorioABCFarma.MdiParent = this;
                    relatorioABCFarma.Botao();
                    relatorioABCFarma.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void tsmRelEmpresasConvenio_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmRelEmpresaConvenio>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmRelEmpresaConvenio(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void btnSolicitacaoSuporte_Click(object sender, EventArgs e)
        {

        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            if (Funcoes.LeParametro(9, "63", false, Principal.nomeEstacao).Equals("S"))
            {
                if (File.Exists(@"C:\BTI\backup.bat"))
                {
                    System.Diagnostics.Process.Start(@"C:\BTI\backup.bat");
                }
            }
            else
            {
                MessageBox.Show("Estação não é Servidor, bakup não disponível nesta estação!", "SGPharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timerImpressao_Tick(object sender, EventArgs e)
        {
            try
            {
                string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(2, "24", true), "*.txt", SearchOption.AllDirectories);
                foreach (string arq in arquivos)
                {
                    FileInfo arquivo = new FileInfo(arq);
                    string texto = System.IO.File.ReadAllText(arq);

                    int iRetorno;
                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");
                    if (iRetorno != 1)
                    {
                        arquivo.Delete();
                        return;
                    }

                    iRetorno = InterfaceEpsonNF.ImprimeTexto(texto);
                    if (iRetorno != 1)
                    {
                        arquivo.Delete();
                        return;
                    }

                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);
                    if (iRetorno != 1)
                    {
                        arquivo.Delete();
                        return;
                    }

                    iRetorno = InterfaceEpsonNF.FechaPorta();
                    if (iRetorno != 1)
                    {
                        arquivo.Delete();
                        return;
                    }
                    
                    arquivo.Delete();
                }
            }
            catch{
                string[] arquivos = Directory.GetFiles(Funcoes.LeParametro(2, "24", true), "*.txt", SearchOption.AllDirectories);
                foreach (string arq in arquivos)
                {
                    FileInfo arquivo = new FileInfo(arq);
                    arquivo.Delete();
                }
            }
        }

        private async void timerAvisos_Tick(object sender, EventArgs e)
        {
            if (Util.TesteIPServidorNuvem())
            {
                List<EstabelecimentoFilial> dadosEstabelecimento = await BuscaDadosEstabelecimento();

                if (dadosEstabelecimento[0].CodErro == "00")
                {
                    if (dadosEstabelecimento[0].ExibeAviso == "S")
                    {
                        List<Avisos> dados = await BuscaAvisos();

                        if (dados.Count > 0)
                        {
                            if (dados[0].CodErro == "00")
                            {
                                btnMensagem.Image = Properties.Resources.nova_mensagem;
                                btnMensagem.ToolTipText = "VOCÊ POSSUI NOVAS MENSAGENS!";

                                aviso.Image = Properties.Resources.nova_mensagem;
                                aviso.ImagePadding = new Padding(10);
                                aviso.TitlePadding = new Padding(10);
                                aviso.ContentPadding = new Padding(10);
                                aviso.AnimationDuration = 2;
                                aviso.TitleText = "Mensagens";
                                aviso.ContentText = "\nVOCÊ POSSUI NOVA(S) MENSAGEM(NS)!";
                                aviso.Popup();

                                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notificacao);
                                simpleSound.Play();
                            }
                            else
                            {
                                btnMensagem.Image = Properties.Resources.mensagem;
                                btnMensagem.ToolTipText = "MENSAGENS!";
                            }
                        }
                    }
                }
            }
        }

        public async Task<List<Avisos>> BuscaAvisos()
        {
            List<Avisos> allItems = new List<Avisos>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Chamados/BuscaAvisos?codEstabelecimento=" + Funcoes.LeParametro(9, "52", true));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Avisos>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<EstabelecimentoFilial>> BuscaDadosEstabelecimento()
        {
            List<EstabelecimentoFilial> allItems = new List<EstabelecimentoFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaDadosDoEstabelecimentos?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private void btnMensagem_Click(object sender, EventArgs e)
        {
            AtualizaTabelaPorCampo("EXIBE_AVISO");

            btnMensagem.Image = Properties.Resources.mensagem;
            btnMensagem.ToolTipText = "MENSAGENS!";

            Cursor = Cursors.Default;

            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmAvisos>().FirstOrDefault();
            if (relatorio == null)
            {
                Principal.contFormularios = Principal.contFormularios + 1;
                var produto = new frmAvisos(this);
                //produto.MdiParent = this;
                produto.Show();

            }
            else
            {
                relatorio.Focus();
            }
        }

        public async void AtualizaTabelaPorCampo(string campo)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false) + "&atualiza=N&campo=" + campo);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Estabelecimento", "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void aviso_Click(object sender, System.EventArgs e)
        {
            AtualizaTabelaPorCampo("EXIBE_AVISO");

            btnMensagem.Image = Properties.Resources.mensagem;
            btnMensagem.ToolTipText = "MENSAGENS!";

            Cursor = Cursors.Default;

            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmAvisos>().FirstOrDefault();
            if (relatorio == null)
            {
                Principal.contFormularios = Principal.contFormularios + 1;
                var produto = new frmAvisos(this);
                produto.MdiParent = this;
                produto.Show();

            }
            else
            {
                relatorio.Focus();
            }
        }

        private void tsmComissaoPorDepartamento_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var relatorio = Application.OpenForms.OfType<frmEstComissaoDepartamento>().FirstOrDefault();
            if (relatorio == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var produto = new frmEstComissaoDepartamento(this);
                    produto.MdiParent = this;
                    produto.Botao();
                    produto.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                relatorio.FormularioFoco();
                relatorio.Focus();
            }
        }

        private void chamadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var chamados = new frmChamados();
            chamados.ShowDialog();
        }

        private void tsmManuAtualizacaoPreco_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var manutPreco = Application.OpenForms.OfType<frmManuFilialPreco>().FirstOrDefault();
            if (manutPreco == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var manutencao = new frmManuFilialPreco(this);
                    manutencao.MdiParent = this;
                    manutencao.Botao();
                    manutencao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                manutPreco.FormularioFoco();
                manutPreco.Focus();
            }
        }

        private void timerPreco_Tick(object sender, EventArgs e)
        {
            if (Util.TesteIPServidorNuvem())
            {
                AtualizaPrecoFilial();

                AtualizaComissaoFilial();
            }
        }

        #region ATUALIZAÇÕES FILIAIS
        public async void AtualizaPrecoFilial()
        {
            List<AtualizaPrecoFilial> dados = await EnviaOuRecebeDadosMysql.BuscaAtualizacaoPreco(codLoja, codGrupo, 'S');

            if (dados.Count > 0)
            {
                if (dados[0].CodErro == "00")
                {
                    ProdutoDetalhe prod = new ProdutoDetalhe();
                    Preco preco = new Preco();
                    var buscaProduto = new Produto();

                    double novaMargem = 0;
                    int prodId = 0;
                    double margem = 0;
                    double preCompra = 0;
                    double ultimoCusto = 0;
                    double cusIni = 0;
                    double preValor = 0;

                    for (int i = 0; i < dados.Count; i++)
                    {
                        Principal.dtBusca = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, dados[i].CodDeBarras);
                        prodId = Convert.ToInt32(Principal.dtBusca.Rows[0]["PROD_ID"]);
                        margem = Convert.ToDouble(Principal.dtBusca.Rows[0]["PROD_MARGEM"]);
                        preCompra = Convert.ToDouble(Principal.dtBusca.Rows[0]["PROD_PRECOMPRA"]);
                        ultimoCusto = Convert.ToDouble(Principal.dtBusca.Rows[0]["PROD_ULTCUSME"]);
                        cusIni = Convert.ToDouble(Principal.dtBusca.Rows[0]["PROD_CUSME"]);
                        preValor = Convert.ToDouble(Principal.dtBusca.Rows[0]["PRE_VALOR"]);
                        
                        if (Funcoes.LeParametro(9, "62", false).Equals("PROD_ULTCUSME"))
                        {
                            if (ultimoCusto > 0)
                            {
                                novaMargem = ((dados[i].ValorDeVenda / ultimoCusto) - 1) * 100;
                            }
                            else if (cusIni > 0)
                            {
                                novaMargem = ((dados[i].ValorDeVenda / cusIni) - 1) * 100;
                            }
                        }
                        else if (Funcoes.LeParametro(9, "62", false).Equals("PROD_PRECOMPRA"))
                        {
                            if (Convert.ToDouble(preCompra) > 0)
                            {
                                novaMargem = ((dados[i].ValorDeVenda / preCompra) - 1) * 100;
                            }
                        }
                        BancoDados.AbrirTrans();

                        if (prod.AtualizaMargem(prodId, margem, novaMargem).Equals(true))
                        {
                            if (preco.AtualizaPrecoPorID(prodId, dados[i].ValorDeVenda, preValor).Equals(true))
                            {
                                LogAlteracaoPreco logPreco = new LogAlteracaoPreco()
                                {
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    ProCodigo = dados[i].CodDeBarras,
                                    DataAlteracao = DateTime.Now,
                                    ValorAntigo = preValor,
                                    ValorNovo = dados[i].ValorDeVenda,
                                    Usuario = ("FILIAL-" + dados[i].Operador).Length > 20 ? ("FILIAL-" + dados[i].Operador).Substring(0, 19) : "FILIAL-" + dados[i].Operador,

                                };
                                if (logPreco.InsereLog(logPreco).Equals(false))
                                {
                                    MessageBox.Show("Erro ao tentar atualizar os Preços", "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                }

                                BancoDados.FecharTrans();

                                await EnviaOuRecebeDadosMysql.AtualizaCampoFilial(codLoja, codGrupo, 'E', "ATUALIZA_PRECO_FILIAL", "ATUALIZA", "COD_LOJA_DESTINO");
                            }
                            else
                            {
                                MessageBox.Show("Erro ao tentar atualizar os Preços", "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                BancoDados.ErroTrans();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Erro ao tentar atualizar as Margens", "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();

                        }
                    }
                }
            }
        }

        public async void AtualizaComissaoFilial()
        {
            List<AtualizaComissaoFilial> dados = await EnviaOuRecebeDadosMysql.BuscaAtualizacaoComissao(codLoja, codGrupo, 'S');

            if (dados.Count > 0)
            {
                if (dados[0].CodErro == "00")
                {
                    ProdutoDetalhe prod = new ProdutoDetalhe();
                    var buscaProduto = new Produto();
                    
                    int prodId = 0;
                    double comissao = 0;

                    for (int i = 0; i < dados.Count; i++)
                    {
                        Principal.dtBusca = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, dados[i].CodDeBarras);
                        prodId = Convert.ToInt32(Principal.dtBusca.Rows[0]["PROD_ID"]);
                        comissao = Convert.ToDouble(Principal.dtBusca.Rows[0]["PROD_COMISSAO"]);

                        BancoDados.AbrirTrans();

                        if (prod.AtualizaComissao(prodId, comissao, dados[i].Comissao).Equals(true))
                        {
                            BancoDados.FecharTrans();

                            await EnviaOuRecebeDadosMysql.AtualizaCampoFilial(codLoja, codGrupo, 'E', "ATUALIZA_COMISSAO_FILIAL", "ATUALIZA", "COD_LOJA_DESTINO");
                            
                        }
                        else
                        {
                            MessageBox.Show("Erro ao tentar atualizar as Margens", "Manutenção de Comissão Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();

                        }
                    }
                }
            }
        }
        #endregion

        private void tsmManuAtualizacaoComissao_Click(object sender, EventArgs e)
        {
            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
            var manutPreco = Application.OpenForms.OfType<frmManuFilialComissao>().FirstOrDefault();
            if (manutPreco == null)
            {
                if (Principal.contFormularios <= 13)
                {
                    Principal.contFormularios = Principal.contFormularios + 1;
                    var manutencao = new frmManuFilialComissao(this);
                    manutencao.MdiParent = this;
                    manutencao.Botao();
                    manutencao.Show();
                }
                else
                {
                    MessageBox.Show("FECHE OS FORMULARIOS", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                manutPreco.FormularioFoco();
                manutPreco.Focus();
            }
        }

        private void teste_Click(object sender, EventArgs e)
        {

        }

        private void tsmAlterarSenha_Click(object sender, EventArgs e)
        {
            if (tsmUsuarios.Enabled)
            {
                string senhaSupervisor = Interaction.InputBox("Digite a nova Senha de Liberação", "Senha");
                if (!String.IsNullOrEmpty(senhaSupervisor))
                {
                    Funcoes.GravaParametro(6, "77", senhaSupervisor, "GERAL", "Senha para Liberação de Vendas para Clientes Bloqueados", "VENDAS", "Informe uma senha para que seja possível a liberação de venda aos clientes bloqueados no sistema", true);
                    MessageBox.Show("Senha Alterada com Sucesso", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Alteração não permitida para esse Perfil de Usuário", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private async void timerPromocaoFilial_Tick(object sender, EventArgs e)
        {
            List<Promocao> promo = new List<Promocao>();
            if (Util.TesteIPServidorNuvem())
            {
                promo = await BuscaPromoFilial(Funcoes.LeParametro(9, "52", false));
                if (promo != null && promo.Count > 0)
                {
                    List<EstabelecimentoFilial> dadosFilial = await BuscaFilial(promo[0].codDestino); //Retorna os dados das filiais

                    if (dadosFilial[0].CodErro == "00")
                    {
                        if (dadosFilial.Count() > 0)
                        {
                            if (promo.Count == 1)
                            {
                                if (promo[0].atualiza.Equals("S"))
                                {
                                    AtualizaPromo(promo[0].CodBarras.ToString(), promo[0].codDestino, promo[0].codOrigem);
                                    CadastrarPromocao(promo[0]);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < promo.Count; i++)
                                {
                                    if (promo[i].atualiza.Equals("S"))
                                    {
                                        AtualizaPromo(promo[i].CodBarras.ToString(), promo[i].codDestino, promo[i].codOrigem);
                                        CadastrarPromocao(promo[i]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //Busca promoção para aquele grupo de filiais
        public async Task<List<Promocao>> BuscaPromoFilial(string codEmp)
        {
            List<Promocao> promo = new List<Promocao>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("PromocaoFilial/BuscaPromo?codDestino=" + codEmp);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    promo = JsonConvert.DeserializeObject<List<Promocao>>(responseBody);

                    return promo;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao Buscar Promocoes de Filiais, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return promo;
            }
        }
        public async Task<List<EstabelecimentoFilial>> BuscaFilial(string codDestino)
        {
            List<EstabelecimentoFilial> allItems = new List<EstabelecimentoFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync("PromocaoFilial/BuscaFilial?codDestino=" + codDestino + "&grupoid=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao Buscar Filiais, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }
        public async void AtualizaPromo(string CodBarras, string codDestino, string codOrigem)
        {
            try
            {
                List<Promocao> promo = await BuscaPromoFilial(Funcoes.LeParametro(9, "52", false));
                if (promo != null && promo.Count > 0)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("PromocaoFilial/AtualizaPromo?CodBarras=" + CodBarras + "&codDestino=" + codDestino + "&codOrigem=" + codOrigem);
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("ERRO " + e.Message, "ATUALIZA PROMO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void CadastrarPromocao(Promocao promo)
        {
            string prodid = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", promo.CodBarras.ToString(), false, true);
            if (VerificaPromoOracle(prodid))
            {
                try
                {
                    string id = Convert.ToString(Funcoes.IdentificaVerificaID("PROMOCOES", "PROMO_CODIGO", 0, "", Principal.empAtual));
                    var promocao = new Promocao(
                            Principal.empAtual,
                            Convert.ToInt32(id),
                            "PROMOCAO - " + id,
                            promo.PromoTipo, //cmbPeriodo.SelectedIndex == 0 ? "D" : "F",
                            promo.DataIni, //cmbPeriodo.SelectedIndex == 0 ? dtInicial.Value.ToString("dd/MM/yyyy") : "",
                            promo.DataFim, //cmbPeriodo.SelectedIndex == 0 ? dtFinal.Value.ToString("dd/MM/yyyy") : "",
                            promo.DiaIni, //cmbPeriodo.SelectedIndex == 1 ? txtDtIni.Text.Trim() : "",
                            promo.DiaFim, //cmbPeriodo.SelectedIndex == 1 ? txtDtFim.Text.Trim() : "",
                            Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", promo.CodBarras.ToString(), false, true)),
                            Convert.ToInt32(promo.DepCodigo), //cmbDepto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepto.SelectedValue),
                            Convert.ToInt32(promo.ClasCodigo), //cmbClasse.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbClasse.SelectedValue),
                            Convert.ToInt32(promo.SubCodigo), //cmbSubClas.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbSubClas.SelectedValue),
                            Convert.ToDouble(promo.PromoPreco), // Convert.ToDouble(txtPreco.Text), 
                            Convert.ToDouble(promo.PromoPorc), //Convert.ToDouble(txtPorc.Text), 
                            "N", //chkLiberado.Checked == true ? "N" : "S", 
                            DateTime.Now, //Data Cadastro 
                            promo.OpCadastro, //Operador 
                            DateTime.Now, //Data Alteração 
                            promo.OpCadastro, //Operador Alteração 
                            promo.DescontoProgressivo, //chkDescontoProgressivo.Checked ? "S" : "N", 
                            Convert.ToInt32(promo.Leve), //txtLeve.Text == "" ? 0 : Convert.ToInt32(txtLeve.Text), 
                            Convert.ToInt32(promo.Pague), //txtPague.Text == "" ? 0 : Convert.ToInt32(txtPague.Text), 
                            Convert.ToDouble(promo.Porcentagem) //txtPorcentagem.Text == "" ? 0 : Convert.ToDouble(txtPorcentagem.Text) 
                        );

                    #region INSERE NOVO REGISTRO | LOG
                    if (promocao.InsereRegistros(promocao).Equals(true))
                    {
                        Funcoes.GravaLogInclusao("PROD_CODIGO", id, Principal.usuario, "PROMOCOES", id, Principal.estAtual);
                        //Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message, "Cadastrar Promocão", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
        public bool VerificaPromoOracle(string prodid)
        {
            Promocao promo = new Promocao();
            DataTable retorno = promo.VerificaProdID(prodid);

            if (retorno.Rows.Count > 0)
                if (promo.DisablePromo(prodid))
                    return true;
            return true;
        }
    }
}
