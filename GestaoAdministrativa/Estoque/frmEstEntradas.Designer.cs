﻿namespace GestaoAdministrativa.Estoque
{
    partial class frmEstEntradas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstEntradas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcEntradas = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBCnpj = new System.Windows.Forms.MaskedTextBox();
            this.cmbBComp = new System.Windows.Forms.ComboBox();
            this.cmbBNOper = new System.Windows.Forms.ComboBox();
            this.txtBValor = new System.Windows.Forms.TextBox();
            this.txtBFatura = new System.Windows.Forms.TextBox();
            this.txtBSerie = new System.Windows.Forms.TextBox();
            this.txtBNumero = new System.Windows.Forms.TextBox();
            this.txtBEmissao = new System.Windows.Forms.MaskedTextBox();
            this.cmbBDocto = new System.Windows.Forms.ComboBox();
            this.txtBLancto = new System.Windows.Forms.MaskedTextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgEntradas = new System.Windows.Forms.DataGridView();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DTLANC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DTEMIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_SERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_NO_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAG_COMPRADOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_OBS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BASEICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_IMPICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ISENTOICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_OUTRAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DIVERSAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BASEIPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_IMPIPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ISENTOIPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_OUTIPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BASESUBST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_SUBST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BASEISS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_IMPISS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_IRRF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VFRETE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_IMPFRETE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VIA_XML = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHAVE_ACESSO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_HORA_EMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsEntradas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnlProdutos = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.btnCancela = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgProdutosNaoCadastrados = new System.Windows.Forms.DataGridView();
            this.CAD_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAD_PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAD_PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAD_UNIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAD_PRECO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtChaveAcesso = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.btnNfe = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txtValorCofins = new System.Windows.Forms.TextBox();
            this.txtTotalTributos = new System.Windows.Forms.TextBox();
            this.txtVICMSUFDest = new System.Windows.Forms.TextBox();
            this.txtValorIpi = new System.Windows.Forms.TextBox();
            this.txtOutrasDespesas = new System.Windows.Forms.TextBox();
            this.txtValorDesconto = new System.Windows.Forms.TextBox();
            this.txtValorSeguro = new System.Windows.Forms.TextBox();
            this.txtFrete = new System.Windows.Forms.TextBox();
            this.txtTotalProdutos = new System.Windows.Forms.TextBox();
            this.txtValorPIS = new System.Windows.Forms.TextBox();
            this.txtValorFCP = new System.Windows.Forms.TextBox();
            this.txtICMSUFRemet = new System.Windows.Forms.TextBox();
            this.txtVImpImportacao = new System.Windows.Forms.TextBox();
            this.txtVICMSST = new System.Windows.Forms.TextBox();
            this.txtIIcms = new System.Windows.Forms.TextBox();
            this.txtVIcms = new System.Windows.Forms.TextBox();
            this.txtBIcms = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnItens = new System.Windows.Forms.Button();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.dgParcelas = new System.Windows.Forms.DataGridView();
            this.ENT_VALPARC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VENCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtPVencimento = new System.Windows.Forms.DateTimePicker();
            this.txtPTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtFCidade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFCnpj = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFNome = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFBusca = new System.Windows.Forms.TextBox();
            this.cmbFBusca = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNCompra = new System.Windows.Forms.TextBox();
            this.cmbComp = new System.Windows.Forms.ComboBox();
            this.txtComp = new System.Windows.Forms.TextBox();
            this.cmbNOper = new System.Windows.Forms.ComboBox();
            this.txtNTotal = new System.Windows.Forms.TextBox();
            this.txtFatura = new System.Windows.Forms.TextBox();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.dtEmissao = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDocto = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtLancamento = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtEntID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpItens = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtCest = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.txtPisCst = new System.Windows.Forms.TextBox();
            this.txtPVIpi = new System.Windows.Forms.TextBox();
            this.txtCstConfis = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.txtCfop = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.txtNCM = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.cmbOrigem = new System.Windows.Forms.ComboBox();
            this.label88 = new System.Windows.Forms.Label();
            this.txtAliIpi = new System.Windows.Forms.TextBox();
            this.txtBaseIpi = new System.Windows.Forms.TextBox();
            this.txtCstIpi = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.txtGDesc = new System.Windows.Forms.TextBox();
            this.txtGTotal = new System.Windows.Forms.TextBox();
            this.txtGEqtde = new System.Windows.Forms.TextBox();
            this.txtGQtde = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.dgItens = new System.Windows.Forms.DataGridView();
            this.ENT_SEQUENCIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_UNIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_QTDESTOQUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_UNITARIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESC_PORC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DESCONTOITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_TOTITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_MARGEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRE_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_COMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALOR_PMC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_ESTATUAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CONTROLADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ALIQICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VALOR_BC_ICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VALORICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BC_ICMS_ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ICMS_ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_REDUCAOBASEICMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ICMS_RET = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ENQ_IPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CST_IPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_BASE_IPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ALIQIPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_ORIGEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_NCM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CFOP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CST_COFINS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_VALOR_IPI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_CST_PIS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_DTVALIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO_COMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COMP_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CADASTRADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EST_INDICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_PRECOMPRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_REGMS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.txtEnqIpi = new System.Windows.Forms.TextBox();
            this.txtICMSRet = new System.Windows.Forms.TextBox();
            this.txtBcIcmsRet = new System.Windows.Forms.TextBox();
            this.txtICMSST = new System.Windows.Forms.TextBox();
            this.txtBIcmsSt = new System.Windows.Forms.TextBox();
            this.txtCstIcms = new System.Windows.Forms.TextBox();
            this.txtPVIcms = new System.Windows.Forms.TextBox();
            this.txtVBIcms = new System.Windows.Forms.TextBox();
            this.dtValidade = new System.Windows.Forms.DateTimePicker();
            this.cmbIcms = new System.Windows.Forms.ComboBox();
            this.txtLote = new System.Windows.Forms.TextBox();
            this.txtPVenda = new System.Windows.Forms.TextBox();
            this.txtPMargem = new System.Windows.Forms.TextBox();
            this.txtPVTotal = new System.Windows.Forms.TextBox();
            this.txtPDescUni = new System.Windows.Forms.TextBox();
            this.txtPDesc = new System.Windows.Forms.TextBox();
            this.txtVUni = new System.Windows.Forms.TextBox();
            this.txtEQtde = new System.Windows.Forms.TextBox();
            this.txtUn = new System.Windows.Forms.TextBox();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txtComissao = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtItem = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtPCMedio = new System.Windows.Forms.TextBox();
            this.txtPUCusto = new System.Windows.Forms.TextBox();
            this.txtPVCompra = new System.Windows.Forms.TextBox();
            this.txtPEstoque = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.ofdXML = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcEntradas.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntradas)).BeginInit();
            this.cmsEntradas.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.pnlProdutos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutosNaoCadastrados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tpItens.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItens)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcEntradas);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(979, 560);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(979, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Entrada de Notas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(973, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcEntradas
            // 
            this.tcEntradas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcEntradas.Controls.Add(this.tpGrade);
            this.tcEntradas.Controls.Add(this.tpFicha);
            this.tcEntradas.Controls.Add(this.tpItens);
            this.tcEntradas.Location = new System.Drawing.Point(6, 33);
            this.tcEntradas.Name = "tcEntradas";
            this.tcEntradas.SelectedIndex = 0;
            this.tcEntradas.Size = new System.Drawing.Size(970, 499);
            this.tcEntradas.TabIndex = 40;
            this.tcEntradas.SelectedIndexChanged += new System.EventHandler(this.tcEntradas_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.BackColor = System.Drawing.Color.White;
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBCnpj);
            this.tpGrade.Controls.Add(this.cmbBComp);
            this.tpGrade.Controls.Add(this.cmbBNOper);
            this.tpGrade.Controls.Add(this.txtBValor);
            this.tpGrade.Controls.Add(this.txtBFatura);
            this.tpGrade.Controls.Add(this.txtBSerie);
            this.tpGrade.Controls.Add(this.txtBNumero);
            this.tpGrade.Controls.Add(this.txtBEmissao);
            this.tpGrade.Controls.Add(this.cmbBDocto);
            this.tpGrade.Controls.Add(this.txtBLancto);
            this.tpGrade.Controls.Add(this.label77);
            this.tpGrade.Controls.Add(this.label76);
            this.tpGrade.Controls.Add(this.label75);
            this.tpGrade.Controls.Add(this.label74);
            this.tpGrade.Controls.Add(this.label73);
            this.tpGrade.Controls.Add(this.label47);
            this.tpGrade.Controls.Add(this.label20);
            this.tpGrade.Controls.Add(this.label37);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.dgEntradas);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(962, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(654, 424);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 172;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBCnpj
            // 
            this.txtBCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCnpj.Location = new System.Drawing.Point(458, 440);
            this.txtBCnpj.Mask = "99,999,999/9999-99";
            this.txtBCnpj.Name = "txtBCnpj";
            this.txtBCnpj.Size = new System.Drawing.Size(185, 22);
            this.txtBCnpj.TabIndex = 171;
            this.txtBCnpj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCnpj_KeyPress);
            // 
            // cmbBComp
            // 
            this.cmbBComp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBComp.FormattingEnabled = true;
            this.cmbBComp.Location = new System.Drawing.Point(223, 438);
            this.cmbBComp.Name = "cmbBComp";
            this.cmbBComp.Size = new System.Drawing.Size(219, 24);
            this.cmbBComp.TabIndex = 170;
            this.cmbBComp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBComp_KeyPress);
            // 
            // cmbBNOper
            // 
            this.cmbBNOper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBNOper.FormattingEnabled = true;
            this.cmbBNOper.Location = new System.Drawing.Point(9, 438);
            this.cmbBNOper.Name = "cmbBNOper";
            this.cmbBNOper.Size = new System.Drawing.Size(202, 24);
            this.cmbBNOper.TabIndex = 169;
            this.cmbBNOper.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBNOper_KeyPress);
            // 
            // txtBValor
            // 
            this.txtBValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBValor.Location = new System.Drawing.Point(760, 393);
            this.txtBValor.MaxLength = 18;
            this.txtBValor.Name = "txtBValor";
            this.txtBValor.Size = new System.Drawing.Size(90, 22);
            this.txtBValor.TabIndex = 168;
            this.txtBValor.Text = "0,00";
            this.txtBValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBValor_KeyPress);
            this.txtBValor.Validated += new System.EventHandler(this.txtBValor_Validated);
            // 
            // txtBFatura
            // 
            this.txtBFatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBFatura.Location = new System.Drawing.Point(617, 393);
            this.txtBFatura.MaxLength = 15;
            this.txtBFatura.Name = "txtBFatura";
            this.txtBFatura.Size = new System.Drawing.Size(131, 22);
            this.txtBFatura.TabIndex = 165;
            this.txtBFatura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBFatura_KeyPress);
            // 
            // txtBSerie
            // 
            this.txtBSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBSerie.Location = new System.Drawing.Point(531, 393);
            this.txtBSerie.MaxLength = 5;
            this.txtBSerie.Name = "txtBSerie";
            this.txtBSerie.Size = new System.Drawing.Size(71, 22);
            this.txtBSerie.TabIndex = 164;
            this.txtBSerie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBSerie_KeyPress);
            // 
            // txtBNumero
            // 
            this.txtBNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNumero.Location = new System.Drawing.Point(379, 393);
            this.txtBNumero.MaxLength = 15;
            this.txtBNumero.Name = "txtBNumero";
            this.txtBNumero.Size = new System.Drawing.Size(137, 22);
            this.txtBNumero.TabIndex = 163;
            this.txtBNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNumero_KeyPress);
            // 
            // txtBEmissao
            // 
            this.txtBEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBEmissao.Location = new System.Drawing.Point(275, 393);
            this.txtBEmissao.Mask = "00/00/0000";
            this.txtBEmissao.Name = "txtBEmissao";
            this.txtBEmissao.Size = new System.Drawing.Size(90, 22);
            this.txtBEmissao.TabIndex = 162;
            this.txtBEmissao.ValidatingType = typeof(System.DateTime);
            this.txtBEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBEmissao_KeyPress);
            // 
            // cmbBDocto
            // 
            this.cmbBDocto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBDocto.FormattingEnabled = true;
            this.cmbBDocto.Location = new System.Drawing.Point(114, 391);
            this.cmbBDocto.Name = "cmbBDocto";
            this.cmbBDocto.Size = new System.Drawing.Size(146, 24);
            this.cmbBDocto.TabIndex = 161;
            this.cmbBDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBDocto_KeyPress);
            // 
            // txtBLancto
            // 
            this.txtBLancto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBLancto.Location = new System.Drawing.Point(9, 393);
            this.txtBLancto.Mask = "00/00/0000";
            this.txtBLancto.Name = "txtBLancto";
            this.txtBLancto.Size = new System.Drawing.Size(90, 22);
            this.txtBLancto.TabIndex = 160;
            this.txtBLancto.ValidatingType = typeof(System.DateTime);
            this.txtBLancto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBLancto_KeyPress);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Navy;
            this.label77.Location = new System.Drawing.Point(455, 422);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(42, 16);
            this.label77.TabIndex = 159;
            this.label77.Text = "CNPJ";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Navy;
            this.label76.Location = new System.Drawing.Point(220, 421);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(79, 16);
            this.label76.TabIndex = 158;
            this.label76.Text = "Comprador";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Navy;
            this.label75.Location = new System.Drawing.Point(6, 420);
            this.label75.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(119, 16);
            this.label75.TabIndex = 157;
            this.label75.Text = "Nat. de Operação";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Navy;
            this.label74.Location = new System.Drawing.Point(757, 375);
            this.label74.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(76, 16);
            this.label74.TabIndex = 156;
            this.label74.Text = "Valor Total";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Navy;
            this.label73.Location = new System.Drawing.Point(614, 375);
            this.label73.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(67, 16);
            this.label73.TabIndex = 155;
            this.label73.Text = "N° Fatura";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Navy;
            this.label47.Location = new System.Drawing.Point(528, 375);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 16);
            this.label47.TabIndex = 154;
            this.label47.Text = "Série";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 375);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 16);
            this.label20.TabIndex = 148;
            this.label20.Text = "Dt. Lancto.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(376, 375);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(62, 16);
            this.label37.TabIndex = 145;
            this.label37.Text = "N° Docto";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(272, 375);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 143;
            this.label15.Text = "Dt. Emissão";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(111, 375);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 141;
            this.label1.Text = "Tipo de Docto.";
            // 
            // dgEntradas
            // 
            this.dgEntradas.AllowUserToAddRows = false;
            this.dgEntradas.AllowUserToDeleteRows = false;
            this.dgEntradas.AllowUserToOrderColumns = true;
            this.dgEntradas.AllowUserToResizeColumns = false;
            this.dgEntradas.AllowUserToResizeRows = false;
            dataGridViewCellStyle87.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEntradas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle87;
            this.dgEntradas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEntradas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle88.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle88.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle88.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntradas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle88;
            this.dgEntradas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEntradas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EST_CODIGO,
            this.EMP_CODIGO,
            this.ENT_ID,
            this.ENT_DTLANC,
            this.TIP_CODIGO,
            this.ENT_DTEMIS,
            this.ENT_DOCTO,
            this.ENT_SERIE,
            this.PAG_DOCTO,
            this.ENT_TOTAL,
            this.ENT_NO_CODIGO,
            this.PAG_COMPRADOR,
            this.CF_DOCTO,
            this.ENT_OBS,
            this.ENT_BASEICMS,
            this.ENT_IMPICMS,
            this.ENT_ISENTOICMS,
            this.ENT_OUTRAS,
            this.ENT_DIVERSAS,
            this.ENT_BASEIPI,
            this.ENT_IMPIPI,
            this.ENT_ISENTOIPI,
            this.ENT_OUTIPI,
            this.ENT_BASESUBST,
            this.ENT_SUBST,
            this.ENT_BASEISS,
            this.ENT_IMPISS,
            this.ENT_IRRF,
            this.ENT_VFRETE,
            this.ENT_IMPFRETE,
            this.ENT_DESCONTO,
            this.DAT_ALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO,
            this.ENT_VIA_XML,
            this.CHAVE_ACESSO,
            this.DATA_HORA_EMISSAO});
            this.dgEntradas.ContextMenuStrip = this.cmsEntradas;
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle111.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle111.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle111.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle111.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle111.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle111.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEntradas.DefaultCellStyle = dataGridViewCellStyle111;
            this.dgEntradas.GridColor = System.Drawing.Color.LightGray;
            this.dgEntradas.Location = new System.Drawing.Point(6, 6);
            this.dgEntradas.MultiSelect = false;
            this.dgEntradas.Name = "dgEntradas";
            this.dgEntradas.ReadOnly = true;
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle112.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle112.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle112.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle112.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle112.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntradas.RowHeadersDefaultCellStyle = dataGridViewCellStyle112;
            this.dgEntradas.RowHeadersVisible = false;
            dataGridViewCellStyle113.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle113.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle113.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEntradas.RowsDefaultCellStyle = dataGridViewCellStyle113;
            this.dgEntradas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEntradas.Size = new System.Drawing.Size(947, 357);
            this.dgEntradas.TabIndex = 1;
            this.dgEntradas.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEntradas_CellMouseClick);
            this.dgEntradas.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEntradas_CellMouseDoubleClick);
            this.dgEntradas.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgEntradas_ColumnHeaderMouseClick);
            this.dgEntradas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEntradas_KeyDown);
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            dataGridViewCellStyle89.Format = "N0";
            dataGridViewCellStyle89.NullValue = null;
            this.EST_CODIGO.DefaultCellStyle = dataGridViewCellStyle89;
            this.EST_CODIGO.HeaderText = "Estab. ID";
            this.EST_CODIGO.MaxInputLength = 3;
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.Width = 90;
            // 
            // EMP_CODIGO
            // 
            this.EMP_CODIGO.DataPropertyName = "EMP_CODIGO";
            dataGridViewCellStyle90.Format = "N0";
            dataGridViewCellStyle90.NullValue = null;
            this.EMP_CODIGO.DefaultCellStyle = dataGridViewCellStyle90;
            this.EMP_CODIGO.HeaderText = "Emp. ID";
            this.EMP_CODIGO.Name = "EMP_CODIGO";
            this.EMP_CODIGO.ReadOnly = true;
            // 
            // ENT_ID
            // 
            this.ENT_ID.DataPropertyName = "ENT_ID";
            this.ENT_ID.HeaderText = "Entrada ID";
            this.ENT_ID.MaxInputLength = 18;
            this.ENT_ID.Name = "ENT_ID";
            this.ENT_ID.ReadOnly = true;
            this.ENT_ID.Width = 95;
            // 
            // ENT_DTLANC
            // 
            this.ENT_DTLANC.DataPropertyName = "ENT_DTLANC";
            this.ENT_DTLANC.HeaderText = "Dt. Lancto.";
            this.ENT_DTLANC.MaxInputLength = 10;
            this.ENT_DTLANC.Name = "ENT_DTLANC";
            this.ENT_DTLANC.ReadOnly = true;
            // 
            // TIP_CODIGO
            // 
            this.TIP_CODIGO.DataPropertyName = "TIP_CODIGO";
            this.TIP_CODIGO.HeaderText = "Tipo de Docto.";
            this.TIP_CODIGO.MaxInputLength = 20;
            this.TIP_CODIGO.Name = "TIP_CODIGO";
            this.TIP_CODIGO.ReadOnly = true;
            this.TIP_CODIGO.Width = 120;
            // 
            // ENT_DTEMIS
            // 
            this.ENT_DTEMIS.DataPropertyName = "ENT_DTEMIS";
            this.ENT_DTEMIS.HeaderText = "Dt. Emissão";
            this.ENT_DTEMIS.MaxInputLength = 10;
            this.ENT_DTEMIS.Name = "ENT_DTEMIS";
            this.ENT_DTEMIS.ReadOnly = true;
            this.ENT_DTEMIS.Width = 110;
            // 
            // ENT_DOCTO
            // 
            this.ENT_DOCTO.DataPropertyName = "ENT_DOCTO";
            this.ENT_DOCTO.HeaderText = "N° Docto.";
            this.ENT_DOCTO.MaxInputLength = 15;
            this.ENT_DOCTO.Name = "ENT_DOCTO";
            this.ENT_DOCTO.ReadOnly = true;
            this.ENT_DOCTO.Width = 125;
            // 
            // ENT_SERIE
            // 
            this.ENT_SERIE.DataPropertyName = "ENT_SERIE";
            this.ENT_SERIE.HeaderText = "Série";
            this.ENT_SERIE.MaxInputLength = 5;
            this.ENT_SERIE.Name = "ENT_SERIE";
            this.ENT_SERIE.ReadOnly = true;
            this.ENT_SERIE.Width = 65;
            // 
            // PAG_DOCTO
            // 
            this.PAG_DOCTO.DataPropertyName = "PAG_DOCTO";
            this.PAG_DOCTO.HeaderText = "N° Fatura";
            this.PAG_DOCTO.MaxInputLength = 15;
            this.PAG_DOCTO.Name = "PAG_DOCTO";
            this.PAG_DOCTO.ReadOnly = true;
            // 
            // ENT_TOTAL
            // 
            this.ENT_TOTAL.DataPropertyName = "ENT_TOTAL";
            dataGridViewCellStyle91.Format = "C2";
            dataGridViewCellStyle91.NullValue = null;
            this.ENT_TOTAL.DefaultCellStyle = dataGridViewCellStyle91;
            this.ENT_TOTAL.HeaderText = "Valor Total";
            this.ENT_TOTAL.MaxInputLength = 18;
            this.ENT_TOTAL.Name = "ENT_TOTAL";
            this.ENT_TOTAL.ReadOnly = true;
            // 
            // ENT_NO_CODIGO
            // 
            this.ENT_NO_CODIGO.DataPropertyName = "NO_CODIGO";
            this.ENT_NO_CODIGO.HeaderText = "Nat. de Operação";
            this.ENT_NO_CODIGO.MaxInputLength = 20;
            this.ENT_NO_CODIGO.Name = "ENT_NO_CODIGO";
            this.ENT_NO_CODIGO.ReadOnly = true;
            this.ENT_NO_CODIGO.Width = 150;
            // 
            // PAG_COMPRADOR
            // 
            this.PAG_COMPRADOR.DataPropertyName = "PAG_COMPRADOR";
            this.PAG_COMPRADOR.HeaderText = "Comprador";
            this.PAG_COMPRADOR.MaxInputLength = 50;
            this.PAG_COMPRADOR.Name = "PAG_COMPRADOR";
            this.PAG_COMPRADOR.ReadOnly = true;
            this.PAG_COMPRADOR.Width = 200;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CNPJ";
            this.CF_DOCTO.MaxInputLength = 18;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.Width = 130;
            // 
            // ENT_OBS
            // 
            this.ENT_OBS.DataPropertyName = "ENT_OBS";
            this.ENT_OBS.HeaderText = "Observação";
            this.ENT_OBS.MaxInputLength = 1000;
            this.ENT_OBS.Name = "ENT_OBS";
            this.ENT_OBS.ReadOnly = true;
            this.ENT_OBS.Width = 300;
            // 
            // ENT_BASEICMS
            // 
            this.ENT_BASEICMS.DataPropertyName = "ENT_BASEICMS";
            dataGridViewCellStyle92.Format = "C2";
            dataGridViewCellStyle92.NullValue = null;
            this.ENT_BASEICMS.DefaultCellStyle = dataGridViewCellStyle92;
            this.ENT_BASEICMS.HeaderText = "Base ICMS";
            this.ENT_BASEICMS.MaxInputLength = 18;
            this.ENT_BASEICMS.Name = "ENT_BASEICMS";
            this.ENT_BASEICMS.ReadOnly = true;
            // 
            // ENT_IMPICMS
            // 
            this.ENT_IMPICMS.DataPropertyName = "ENT_IMPICMS";
            dataGridViewCellStyle93.Format = "C2";
            dataGridViewCellStyle93.NullValue = null;
            this.ENT_IMPICMS.DefaultCellStyle = dataGridViewCellStyle93;
            this.ENT_IMPICMS.HeaderText = "Valor ICMS";
            this.ENT_IMPICMS.MaxInputLength = 18;
            this.ENT_IMPICMS.Name = "ENT_IMPICMS";
            this.ENT_IMPICMS.ReadOnly = true;
            // 
            // ENT_ISENTOICMS
            // 
            this.ENT_ISENTOICMS.DataPropertyName = "ENT_BASESUBST";
            dataGridViewCellStyle94.Format = "C2";
            this.ENT_ISENTOICMS.DefaultCellStyle = dataGridViewCellStyle94;
            this.ENT_ISENTOICMS.HeaderText = "B. Cal. ICMS ST";
            this.ENT_ISENTOICMS.MaxInputLength = 18;
            this.ENT_ISENTOICMS.Name = "ENT_ISENTOICMS";
            this.ENT_ISENTOICMS.ReadOnly = true;
            this.ENT_ISENTOICMS.Width = 130;
            // 
            // ENT_OUTRAS
            // 
            this.ENT_OUTRAS.DataPropertyName = "ENT_SUBST";
            dataGridViewCellStyle95.Format = "C2";
            this.ENT_OUTRAS.DefaultCellStyle = dataGridViewCellStyle95;
            this.ENT_OUTRAS.HeaderText = "Valor ICMS ST";
            this.ENT_OUTRAS.MaxInputLength = 18;
            this.ENT_OUTRAS.Name = "ENT_OUTRAS";
            this.ENT_OUTRAS.ReadOnly = true;
            this.ENT_OUTRAS.Width = 120;
            // 
            // ENT_DIVERSAS
            // 
            this.ENT_DIVERSAS.DataPropertyName = "ENT_ISENTOIPI";
            dataGridViewCellStyle96.Format = "C2";
            this.ENT_DIVERSAS.DefaultCellStyle = dataGridViewCellStyle96;
            this.ENT_DIVERSAS.HeaderText = "Vl. Imp. Importação";
            this.ENT_DIVERSAS.MaxInputLength = 18;
            this.ENT_DIVERSAS.Name = "ENT_DIVERSAS";
            this.ENT_DIVERSAS.ReadOnly = true;
            this.ENT_DIVERSAS.Width = 160;
            // 
            // ENT_BASEIPI
            // 
            this.ENT_BASEIPI.DataPropertyName = "ENT_OUTIPI";
            dataGridViewCellStyle97.Format = "C2";
            this.ENT_BASEIPI.DefaultCellStyle = dataGridViewCellStyle97;
            this.ENT_BASEIPI.HeaderText = "Vl. ICMS UF Remet.";
            this.ENT_BASEIPI.MaxInputLength = 18;
            this.ENT_BASEIPI.Name = "ENT_BASEIPI";
            this.ENT_BASEIPI.ReadOnly = true;
            this.ENT_BASEIPI.Width = 160;
            // 
            // ENT_IMPIPI
            // 
            this.ENT_IMPIPI.DataPropertyName = "ENT_BASEISS";
            dataGridViewCellStyle98.Format = "C2";
            this.ENT_IMPIPI.DefaultCellStyle = dataGridViewCellStyle98;
            this.ENT_IMPIPI.HeaderText = "Valor do FCP";
            this.ENT_IMPIPI.MaxInputLength = 18;
            this.ENT_IMPIPI.Name = "ENT_IMPIPI";
            this.ENT_IMPIPI.ReadOnly = true;
            this.ENT_IMPIPI.Width = 130;
            // 
            // ENT_ISENTOIPI
            // 
            this.ENT_ISENTOIPI.DataPropertyName = "ENT_ISENTOICMS";
            dataGridViewCellStyle99.Format = "C2";
            this.ENT_ISENTOIPI.DefaultCellStyle = dataGridViewCellStyle99;
            this.ENT_ISENTOIPI.HeaderText = "Valor do PIS";
            this.ENT_ISENTOIPI.MaxInputLength = 18;
            this.ENT_ISENTOIPI.Name = "ENT_ISENTOIPI";
            this.ENT_ISENTOIPI.ReadOnly = true;
            this.ENT_ISENTOIPI.Width = 130;
            // 
            // ENT_OUTIPI
            // 
            this.ENT_OUTIPI.DataPropertyName = "ENT_DIVERSAS";
            dataGridViewCellStyle100.Format = "C2";
            this.ENT_OUTIPI.DefaultCellStyle = dataGridViewCellStyle100;
            this.ENT_OUTIPI.HeaderText = "Vl. Total Produtos";
            this.ENT_OUTIPI.MaxInputLength = 18;
            this.ENT_OUTIPI.Name = "ENT_OUTIPI";
            this.ENT_OUTIPI.ReadOnly = true;
            this.ENT_OUTIPI.Width = 160;
            // 
            // ENT_BASESUBST
            // 
            this.ENT_BASESUBST.DataPropertyName = "ENT_FRETE";
            dataGridViewCellStyle101.Format = "C2";
            this.ENT_BASESUBST.DefaultCellStyle = dataGridViewCellStyle101;
            this.ENT_BASESUBST.HeaderText = "Valor Frete";
            this.ENT_BASESUBST.MaxInputLength = 18;
            this.ENT_BASESUBST.Name = "ENT_BASESUBST";
            this.ENT_BASESUBST.ReadOnly = true;
            this.ENT_BASESUBST.Width = 120;
            // 
            // ENT_SUBST
            // 
            this.ENT_SUBST.DataPropertyName = "ENT_IMPFRETE";
            dataGridViewCellStyle102.Format = "C2";
            this.ENT_SUBST.DefaultCellStyle = dataGridViewCellStyle102;
            this.ENT_SUBST.HeaderText = "Valor do Seguro";
            this.ENT_SUBST.MaxInputLength = 18;
            this.ENT_SUBST.Name = "ENT_SUBST";
            this.ENT_SUBST.ReadOnly = true;
            this.ENT_SUBST.Width = 130;
            // 
            // ENT_BASEISS
            // 
            this.ENT_BASEISS.DataPropertyName = "ENT_DESCONTO";
            dataGridViewCellStyle103.Format = "C2";
            this.ENT_BASEISS.DefaultCellStyle = dataGridViewCellStyle103;
            this.ENT_BASEISS.HeaderText = "Desconto";
            this.ENT_BASEISS.MaxInputLength = 18;
            this.ENT_BASEISS.Name = "ENT_BASEISS";
            this.ENT_BASEISS.ReadOnly = true;
            // 
            // ENT_IMPISS
            // 
            this.ENT_IMPISS.DataPropertyName = "ENT_OUTRAS";
            dataGridViewCellStyle104.Format = "C2";
            this.ENT_IMPISS.DefaultCellStyle = dataGridViewCellStyle104;
            this.ENT_IMPISS.HeaderText = "Outras Despesas";
            this.ENT_IMPISS.MaxInputLength = 18;
            this.ENT_IMPISS.Name = "ENT_IMPISS";
            this.ENT_IMPISS.ReadOnly = true;
            this.ENT_IMPISS.Width = 140;
            // 
            // ENT_IRRF
            // 
            this.ENT_IRRF.DataPropertyName = "ENT_IMPIPI";
            dataGridViewCellStyle105.Format = "C2";
            this.ENT_IRRF.DefaultCellStyle = dataGridViewCellStyle105;
            this.ENT_IRRF.HeaderText = "Vl. Total IPI";
            this.ENT_IRRF.MaxInputLength = 18;
            this.ENT_IRRF.Name = "ENT_IRRF";
            this.ENT_IRRF.ReadOnly = true;
            // 
            // ENT_VFRETE
            // 
            this.ENT_VFRETE.DataPropertyName = "ENT_BASEIPI";
            dataGridViewCellStyle106.Format = "C2";
            this.ENT_VFRETE.DefaultCellStyle = dataGridViewCellStyle106;
            this.ENT_VFRETE.HeaderText = "Vl. ICMS UF Dest.";
            this.ENT_VFRETE.MaxInputLength = 18;
            this.ENT_VFRETE.Name = "ENT_VFRETE";
            this.ENT_VFRETE.ReadOnly = true;
            this.ENT_VFRETE.Width = 160;
            // 
            // ENT_IMPFRETE
            // 
            this.ENT_IMPFRETE.DataPropertyName = "ENT_IRRF";
            dataGridViewCellStyle107.Format = "C2";
            this.ENT_IMPFRETE.DefaultCellStyle = dataGridViewCellStyle107;
            this.ENT_IMPFRETE.HeaderText = "Vl. Total Trib.";
            this.ENT_IMPFRETE.MaxInputLength = 18;
            this.ENT_IMPFRETE.Name = "ENT_IMPFRETE";
            this.ENT_IMPFRETE.ReadOnly = true;
            this.ENT_IMPFRETE.Width = 130;
            // 
            // ENT_DESCONTO
            // 
            this.ENT_DESCONTO.DataPropertyName = "ENT_IMPISS";
            dataGridViewCellStyle108.Format = "C2";
            dataGridViewCellStyle108.NullValue = null;
            this.ENT_DESCONTO.DefaultCellStyle = dataGridViewCellStyle108;
            this.ENT_DESCONTO.HeaderText = "Valor da CONFIS";
            this.ENT_DESCONTO.MaxInputLength = 18;
            this.ENT_DESCONTO.Name = "ENT_DESCONTO";
            this.ENT_DESCONTO.ReadOnly = true;
            this.ENT_DESCONTO.Width = 150;
            // 
            // DAT_ALTERACAO
            // 
            this.DAT_ALTERACAO.DataPropertyName = "DAT_ALTERACAO";
            dataGridViewCellStyle109.Format = "G";
            this.DAT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle109;
            this.DAT_ALTERACAO.HeaderText = "Data Alteração";
            this.DAT_ALTERACAO.MaxInputLength = 20;
            this.DAT_ALTERACAO.Name = "DAT_ALTERACAO";
            this.DAT_ALTERACAO.ReadOnly = true;
            this.DAT_ALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle110.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle110;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 20;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // ENT_VIA_XML
            // 
            this.ENT_VIA_XML.DataPropertyName = "ENT_VIA_XML";
            this.ENT_VIA_XML.HeaderText = "XML";
            this.ENT_VIA_XML.Name = "ENT_VIA_XML";
            this.ENT_VIA_XML.ReadOnly = true;
            this.ENT_VIA_XML.Visible = false;
            // 
            // CHAVE_ACESSO
            // 
            this.CHAVE_ACESSO.DataPropertyName = "CHAVE_ACESSO";
            this.CHAVE_ACESSO.HeaderText = "CHAVE_ACESSO";
            this.CHAVE_ACESSO.Name = "CHAVE_ACESSO";
            this.CHAVE_ACESSO.ReadOnly = true;
            this.CHAVE_ACESSO.Visible = false;
            // 
            // DATA_HORA_EMISSAO
            // 
            this.DATA_HORA_EMISSAO.DataPropertyName = "DATA_HORA_EMISSAO";
            this.DATA_HORA_EMISSAO.HeaderText = "DATA_HORA_EMISSAO";
            this.DATA_HORA_EMISSAO.Name = "DATA_HORA_EMISSAO";
            this.DATA_HORA_EMISSAO.ReadOnly = true;
            this.DATA_HORA_EMISSAO.Visible = false;
            // 
            // cmsEntradas
            // 
            this.cmsEntradas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsEntradas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmExportar,
            this.tsmConfigurar});
            this.cmsEntradas.Name = "cmsEntradas";
            this.cmsEntradas.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.BackColor = System.Drawing.Color.White;
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(962, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox9);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 400);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(948, 64);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.pictureBox18);
            this.groupBox9.Controls.Add(this.label79);
            this.groupBox9.Controls.Add(this.label80);
            this.groupBox9.Controls.Add(this.pictureBox19);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(574, 10);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(368, 46);
            this.groupBox9.TabIndex = 193;
            this.groupBox9.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Location = new System.Drawing.Point(173, 21);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(16, 16);
            this.pictureBox18.TabIndex = 195;
            this.pictureBox18.TabStop = false;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Navy;
            this.label79.Location = new System.Drawing.Point(191, 21);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(136, 16);
            this.label79.TabIndex = 194;
            this.label79.Text = "Campo não Editável";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Navy;
            this.label80.Location = new System.Drawing.Point(34, 21);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(129, 16);
            this.label80.TabIndex = 192;
            this.label80.Text = "Campo Obrigatório";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(16, 21);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(16, 16);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox19.TabIndex = 191;
            this.pictureBox19.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 36);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 36);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.pnlProdutos);
            this.groupBox2.Controls.Add(this.txtChaveAcesso);
            this.groupBox2.Controls.Add(this.label91);
            this.groupBox2.Controls.Add(this.lblEmpresa);
            this.groupBox2.Controls.Add(this.btnNfe);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.txtValorCofins);
            this.groupBox2.Controls.Add(this.txtTotalTributos);
            this.groupBox2.Controls.Add(this.txtVICMSUFDest);
            this.groupBox2.Controls.Add(this.txtValorIpi);
            this.groupBox2.Controls.Add(this.txtOutrasDespesas);
            this.groupBox2.Controls.Add(this.txtValorDesconto);
            this.groupBox2.Controls.Add(this.txtValorSeguro);
            this.groupBox2.Controls.Add(this.txtFrete);
            this.groupBox2.Controls.Add(this.txtTotalProdutos);
            this.groupBox2.Controls.Add(this.txtValorPIS);
            this.groupBox2.Controls.Add(this.txtValorFCP);
            this.groupBox2.Controls.Add(this.txtICMSUFRemet);
            this.groupBox2.Controls.Add(this.txtVImpImportacao);
            this.groupBox2.Controls.Add(this.txtVICMSST);
            this.groupBox2.Controls.Add(this.txtIIcms);
            this.groupBox2.Controls.Add(this.txtVIcms);
            this.groupBox2.Controls.Add(this.txtBIcms);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.btnItens);
            this.groupBox2.Controls.Add(this.txtObs);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.txtNCompra);
            this.groupBox2.Controls.Add(this.cmbComp);
            this.groupBox2.Controls.Add(this.txtComp);
            this.groupBox2.Controls.Add(this.cmbNOper);
            this.groupBox2.Controls.Add(this.txtNTotal);
            this.groupBox2.Controls.Add(this.txtFatura);
            this.groupBox2.Controls.Add(this.txtSerie);
            this.groupBox2.Controls.Add(this.txtDocto);
            this.groupBox2.Controls.Add(this.dtEmissao);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cmbDocto);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dtLancamento);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.txtEntID);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(948, 391);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pnlProdutos
            // 
            this.pnlProdutos.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlProdutos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProdutos.Controls.Add(this.label84);
            this.pnlProdutos.Controls.Add(this.btnCancela);
            this.pnlProdutos.Controls.Add(this.btnOk);
            this.pnlProdutos.Controls.Add(this.dgProdutosNaoCadastrados);
            this.pnlProdutos.ForeColor = System.Drawing.Color.Black;
            this.pnlProdutos.Location = new System.Drawing.Point(15, 35);
            this.pnlProdutos.Name = "pnlProdutos";
            this.pnlProdutos.Size = new System.Drawing.Size(735, 312);
            this.pnlProdutos.TabIndex = 169;
            this.pnlProdutos.Visible = false;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.White;
            this.label84.Location = new System.Drawing.Point(-5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(681, 28);
            this.label84.TabIndex = 1;
            this.label84.Text = "Produtos não cadastrados no Sistema";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancela
            // 
            this.btnCancela.BackColor = System.Drawing.Color.White;
            this.btnCancela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancela.Image = ((System.Drawing.Image)(resources.GetObject("btnCancela.Image")));
            this.btnCancela.Location = new System.Drawing.Point(673, 267);
            this.btnCancela.Name = "btnCancela";
            this.btnCancela.Size = new System.Drawing.Size(56, 37);
            this.btnCancela.TabIndex = 3;
            this.btnCancela.UseVisualStyleBackColor = false;
            this.btnCancela.Click += new System.EventHandler(this.btnCancela_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.White;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
            this.btnOk.Location = new System.Drawing.Point(611, 267);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(56, 37);
            this.btnOk.TabIndex = 2;
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgProdutosNaoCadastrados
            // 
            this.dgProdutosNaoCadastrados.AllowUserToAddRows = false;
            this.dgProdutosNaoCadastrados.AllowUserToDeleteRows = false;
            this.dgProdutosNaoCadastrados.BackgroundColor = System.Drawing.Color.White;
            this.dgProdutosNaoCadastrados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutosNaoCadastrados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CAD_QTDE,
            this.CAD_PROD_CODIGO,
            this.CAD_PROD_DESCR,
            this.CAD_UNIDADE,
            this.CAD_PRECO,
            this.CUSTO});
            this.dgProdutosNaoCadastrados.GridColor = System.Drawing.Color.White;
            this.dgProdutosNaoCadastrados.Location = new System.Drawing.Point(3, 31);
            this.dgProdutosNaoCadastrados.Name = "dgProdutosNaoCadastrados";
            this.dgProdutosNaoCadastrados.RowHeadersWidth = 20;
            this.dgProdutosNaoCadastrados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutosNaoCadastrados.Size = new System.Drawing.Size(726, 230);
            this.dgProdutosNaoCadastrados.TabIndex = 0;
            // 
            // CAD_QTDE
            // 
            this.CAD_QTDE.DataPropertyName = "CAD_QTDE";
            this.CAD_QTDE.HeaderText = "";
            this.CAD_QTDE.Name = "CAD_QTDE";
            this.CAD_QTDE.Width = 30;
            // 
            // CAD_PROD_CODIGO
            // 
            this.CAD_PROD_CODIGO.DataPropertyName = "CAD_PROD_CODIGO";
            this.CAD_PROD_CODIGO.HeaderText = "Cód. Barra";
            this.CAD_PROD_CODIGO.Name = "CAD_PROD_CODIGO";
            this.CAD_PROD_CODIGO.Width = 130;
            // 
            // CAD_PROD_DESCR
            // 
            this.CAD_PROD_DESCR.DataPropertyName = "CAD_PROD_DESCR";
            this.CAD_PROD_DESCR.HeaderText = "Descrição";
            this.CAD_PROD_DESCR.Name = "CAD_PROD_DESCR";
            this.CAD_PROD_DESCR.Width = 365;
            // 
            // CAD_UNIDADE
            // 
            this.CAD_UNIDADE.DataPropertyName = "CAD_UNIDADE";
            this.CAD_UNIDADE.HeaderText = "Uni.";
            this.CAD_UNIDADE.Name = "CAD_UNIDADE";
            this.CAD_UNIDADE.Width = 50;
            // 
            // CAD_PRECO
            // 
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.CAD_PRECO.DefaultCellStyle = dataGridViewCellStyle1;
            this.CAD_PRECO.HeaderText = "Preço";
            this.CAD_PRECO.Name = "CAD_PRECO";
            this.CAD_PRECO.Visible = false;
            // 
            // CUSTO
            // 
            this.CUSTO.HeaderText = "CUSTO";
            this.CUSTO.Name = "CUSTO";
            this.CUSTO.Visible = false;
            // 
            // txtChaveAcesso
            // 
            this.txtChaveAcesso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChaveAcesso.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChaveAcesso.Location = new System.Drawing.Point(392, 265);
            this.txtChaveAcesso.MaxLength = 48;
            this.txtChaveAcesso.Name = "txtChaveAcesso";
            this.txtChaveAcesso.Size = new System.Drawing.Size(309, 22);
            this.txtChaveAcesso.TabIndex = 292;
            this.txtChaveAcesso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtChaveAcesso_KeyPress);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Navy;
            this.label91.Location = new System.Drawing.Point(389, 249);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(116, 16);
            this.label91.TabIndex = 291;
            this.label91.Text = "Chave de Acesso";
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.ForeColor = System.Drawing.Color.Black;
            this.lblEmpresa.Location = new System.Drawing.Point(487, 18);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(0, 16);
            this.lblEmpresa.TabIndex = 290;
            this.lblEmpresa.Tag = "";
            // 
            // btnNfe
            // 
            this.btnNfe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNfe.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNfe.Image = ((System.Drawing.Image)(resources.GetObject("btnNfe.Image")));
            this.btnNfe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNfe.Location = new System.Drawing.Point(706, 249);
            this.btnNfe.Name = "btnNfe";
            this.btnNfe.Size = new System.Drawing.Size(233, 38);
            this.btnNfe.TabIndex = 289;
            this.btnNfe.Text = "Importar NFe (XML)";
            this.btnNfe.UseVisualStyleBackColor = true;
            this.btnNfe.Click += new System.EventHandler(this.btnNfe_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(312, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 288;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(893, 44);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 287;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseHover += new System.EventHandler(this.pictureBox9_MouseHover);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(771, 44);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 286;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(674, 44);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 285;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(554, 44);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 284;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(332, 44);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 282;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // txtValorCofins
            // 
            this.txtValorCofins.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorCofins.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorCofins.Location = new System.Drawing.Point(741, 353);
            this.txtValorCofins.MaxLength = 10;
            this.txtValorCofins.Name = "txtValorCofins";
            this.txtValorCofins.Size = new System.Drawing.Size(90, 22);
            this.txtValorCofins.TabIndex = 280;
            this.txtValorCofins.Text = "0,00";
            this.txtValorCofins.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorCofins.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorCofins_KeyPress);
            this.txtValorCofins.Validated += new System.EventHandler(this.txtDesconto_Validated);
            // 
            // txtTotalTributos
            // 
            this.txtTotalTributos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalTributos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalTributos.Location = new System.Drawing.Point(648, 353);
            this.txtTotalTributos.MaxLength = 10;
            this.txtTotalTributos.Name = "txtTotalTributos";
            this.txtTotalTributos.Size = new System.Drawing.Size(88, 22);
            this.txtTotalTributos.TabIndex = 279;
            this.txtTotalTributos.Text = "0,00";
            this.txtTotalTributos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalTributos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalTributos_KeyPress);
            this.txtTotalTributos.Validated += new System.EventHandler(this.txtIFrete_Validated);
            // 
            // txtVICMSUFDest
            // 
            this.txtVICMSUFDest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVICMSUFDest.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVICMSUFDest.Location = new System.Drawing.Point(519, 353);
            this.txtVICMSUFDest.MaxLength = 10;
            this.txtVICMSUFDest.Name = "txtVICMSUFDest";
            this.txtVICMSUFDest.Size = new System.Drawing.Size(124, 22);
            this.txtVICMSUFDest.TabIndex = 278;
            this.txtVICMSUFDest.Text = "0,00";
            this.txtVICMSUFDest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVICMSUFDest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVICMSUFDest_KeyPress);
            this.txtVICMSUFDest.Validated += new System.EventHandler(this.txtVFrete_Validated);
            // 
            // txtValorIpi
            // 
            this.txtValorIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorIpi.Location = new System.Drawing.Point(392, 353);
            this.txtValorIpi.MaxLength = 10;
            this.txtValorIpi.Name = "txtValorIpi";
            this.txtValorIpi.Size = new System.Drawing.Size(123, 22);
            this.txtValorIpi.TabIndex = 277;
            this.txtValorIpi.Text = "0,00";
            this.txtValorIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorIpi_KeyPress);
            this.txtValorIpi.Validated += new System.EventHandler(this.txtVIrrf_Validated);
            // 
            // txtOutrasDespesas
            // 
            this.txtOutrasDespesas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOutrasDespesas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutrasDespesas.Location = new System.Drawing.Point(302, 353);
            this.txtOutrasDespesas.MaxLength = 10;
            this.txtOutrasDespesas.Name = "txtOutrasDespesas";
            this.txtOutrasDespesas.Size = new System.Drawing.Size(86, 22);
            this.txtOutrasDespesas.TabIndex = 276;
            this.txtOutrasDespesas.Text = "0,00";
            this.txtOutrasDespesas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOutrasDespesas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOutrasDespesas_KeyPress);
            this.txtOutrasDespesas.Validated += new System.EventHandler(this.txtVIss_Validated);
            // 
            // txtValorDesconto
            // 
            this.txtValorDesconto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorDesconto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorDesconto.Location = new System.Drawing.Point(193, 353);
            this.txtValorDesconto.MaxLength = 10;
            this.txtValorDesconto.Name = "txtValorDesconto";
            this.txtValorDesconto.Size = new System.Drawing.Size(105, 22);
            this.txtValorDesconto.TabIndex = 275;
            this.txtValorDesconto.Text = "0,00";
            this.txtValorDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorDesconto_KeyPress);
            this.txtValorDesconto.Validated += new System.EventHandler(this.txtBIss_Validated);
            // 
            // txtValorSeguro
            // 
            this.txtValorSeguro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorSeguro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorSeguro.Location = new System.Drawing.Point(105, 353);
            this.txtValorSeguro.MaxLength = 10;
            this.txtValorSeguro.Name = "txtValorSeguro";
            this.txtValorSeguro.Size = new System.Drawing.Size(85, 22);
            this.txtValorSeguro.TabIndex = 274;
            this.txtValorSeguro.Text = "0,00";
            this.txtValorSeguro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorSeguro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorSeguro_KeyPress);
            this.txtValorSeguro.Validated += new System.EventHandler(this.txtSTrib_Validated);
            // 
            // txtFrete
            // 
            this.txtFrete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrete.Location = new System.Drawing.Point(16, 353);
            this.txtFrete.MaxLength = 10;
            this.txtFrete.Name = "txtFrete";
            this.txtFrete.Size = new System.Drawing.Size(85, 22);
            this.txtFrete.TabIndex = 273;
            this.txtFrete.Text = "0,00";
            this.txtFrete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFrete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFrete_KeyPress);
            this.txtFrete.Validated += new System.EventHandler(this.txtBSubst_Validated);
            // 
            // txtTotalProdutos
            // 
            this.txtTotalProdutos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalProdutos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalProdutos.Location = new System.Drawing.Point(836, 309);
            this.txtTotalProdutos.MaxLength = 10;
            this.txtTotalProdutos.Name = "txtTotalProdutos";
            this.txtTotalProdutos.Size = new System.Drawing.Size(103, 22);
            this.txtTotalProdutos.TabIndex = 272;
            this.txtTotalProdutos.Text = "0,00";
            this.txtTotalProdutos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalProdutos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalProdutos_KeyPress);
            this.txtTotalProdutos.Validated += new System.EventHandler(this.txtIOutros_Validated);
            // 
            // txtValorPIS
            // 
            this.txtValorPIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorPIS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorPIS.Location = new System.Drawing.Point(741, 309);
            this.txtValorPIS.MaxLength = 10;
            this.txtValorPIS.Name = "txtValorPIS";
            this.txtValorPIS.Size = new System.Drawing.Size(90, 22);
            this.txtValorPIS.TabIndex = 271;
            this.txtValorPIS.Text = "0,00";
            this.txtValorPIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorPIS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorPIS_KeyPress);
            this.txtValorPIS.Validated += new System.EventHandler(this.txtIIpi_Validated);
            // 
            // txtValorFCP
            // 
            this.txtValorFCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorFCP.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorFCP.Location = new System.Drawing.Point(648, 309);
            this.txtValorFCP.MaxLength = 10;
            this.txtValorFCP.Name = "txtValorFCP";
            this.txtValorFCP.Size = new System.Drawing.Size(88, 22);
            this.txtValorFCP.TabIndex = 270;
            this.txtValorFCP.Text = "0,00";
            this.txtValorFCP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorFCP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorFCP_KeyPress);
            this.txtValorFCP.Validated += new System.EventHandler(this.txtVIpi_Validated);
            // 
            // txtICMSUFRemet
            // 
            this.txtICMSUFRemet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtICMSUFRemet.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtICMSUFRemet.Location = new System.Drawing.Point(519, 309);
            this.txtICMSUFRemet.MaxLength = 10;
            this.txtICMSUFRemet.Name = "txtICMSUFRemet";
            this.txtICMSUFRemet.Size = new System.Drawing.Size(124, 22);
            this.txtICMSUFRemet.TabIndex = 269;
            this.txtICMSUFRemet.Text = "0,00";
            this.txtICMSUFRemet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtICMSUFRemet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtICMSUFRemet_KeyPress);
            this.txtICMSUFRemet.Validated += new System.EventHandler(this.txtBIpi_Validated);
            // 
            // txtVImpImportacao
            // 
            this.txtVImpImportacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVImpImportacao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVImpImportacao.Location = new System.Drawing.Point(392, 309);
            this.txtVImpImportacao.MaxLength = 10;
            this.txtVImpImportacao.Name = "txtVImpImportacao";
            this.txtVImpImportacao.Size = new System.Drawing.Size(123, 22);
            this.txtVImpImportacao.TabIndex = 268;
            this.txtVImpImportacao.Text = "0,00";
            this.txtVImpImportacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVImpImportacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVImpImportacao_KeyPress);
            this.txtVImpImportacao.Validated += new System.EventHandler(this.txtVDiversos_Validated);
            // 
            // txtVICMSST
            // 
            this.txtVICMSST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVICMSST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVICMSST.Location = new System.Drawing.Point(302, 309);
            this.txtVICMSST.MaxLength = 10;
            this.txtVICMSST.Name = "txtVICMSST";
            this.txtVICMSST.Size = new System.Drawing.Size(86, 22);
            this.txtVICMSST.TabIndex = 267;
            this.txtVICMSST.Text = "0,00";
            this.txtVICMSST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVICMSST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVICMSST_KeyPress);
            this.txtVICMSST.Validated += new System.EventHandler(this.txtVOutras_Validated);
            // 
            // txtIIcms
            // 
            this.txtIIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIIcms.Location = new System.Drawing.Point(193, 309);
            this.txtIIcms.MaxLength = 10;
            this.txtIIcms.Name = "txtIIcms";
            this.txtIIcms.Size = new System.Drawing.Size(105, 22);
            this.txtIIcms.TabIndex = 266;
            this.txtIIcms.Text = "0,00";
            this.txtIIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIIcms_KeyPress);
            this.txtIIcms.Validated += new System.EventHandler(this.txtIIcms_Validated);
            // 
            // txtVIcms
            // 
            this.txtVIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVIcms.Location = new System.Drawing.Point(105, 309);
            this.txtVIcms.MaxLength = 10;
            this.txtVIcms.Name = "txtVIcms";
            this.txtVIcms.Size = new System.Drawing.Size(85, 22);
            this.txtVIcms.TabIndex = 265;
            this.txtVIcms.Text = "0,00";
            this.txtVIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVIcms_KeyPress);
            this.txtVIcms.Validated += new System.EventHandler(this.txtVIcms_Validated);
            // 
            // txtBIcms
            // 
            this.txtBIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBIcms.Location = new System.Drawing.Point(16, 309);
            this.txtBIcms.MaxLength = 10;
            this.txtBIcms.Name = "txtBIcms";
            this.txtBIcms.Size = new System.Drawing.Size(85, 22);
            this.txtBIcms.TabIndex = 264;
            this.txtBIcms.Text = "0,00";
            this.txtBIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBIcms_KeyPress);
            this.txtBIcms.Validated += new System.EventHandler(this.txtBIcms_Validated);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(738, 334);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(93, 16);
            this.label42.TabIndex = 263;
            this.label42.Text = "V. da COFINS";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Navy;
            this.label41.Location = new System.Drawing.Point(645, 334);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(79, 16);
            this.label41.TabIndex = 262;
            this.label41.Text = "V. Tot. Trib.";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Navy;
            this.label40.Location = new System.Drawing.Point(516, 334);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(113, 16);
            this.label40.TabIndex = 261;
            this.label40.Text = "V. ICMS UF Dest.";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Navy;
            this.label39.Location = new System.Drawing.Point(391, 334);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(97, 16);
            this.label39.TabIndex = 260;
            this.label39.Text = "Valor Total IPI";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Navy;
            this.label38.Location = new System.Drawing.Point(299, 334);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(89, 16);
            this.label38.TabIndex = 259;
            this.label38.Text = "Ot. Despesas";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(190, 334);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 16);
            this.label36.TabIndex = 258;
            this.label36.Text = "Desconto";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(102, 334);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 16);
            this.label26.TabIndex = 257;
            this.label26.Text = "V. do Seguro";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(13, 334);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 16);
            this.label35.TabIndex = 256;
            this.label35.Text = "Valor Frete";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(836, 290);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(107, 16);
            this.label34.TabIndex = 255;
            this.label34.Text = "V. Tot. Produtos";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(738, 290);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(87, 16);
            this.label33.TabIndex = 254;
            this.label33.Text = "Valor do PIS";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(645, 290);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(91, 16);
            this.label32.TabIndex = 253;
            this.label32.Text = "Valor do FCP";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(516, 290);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(127, 16);
            this.label31.TabIndex = 252;
            this.label31.Text = "V. ICMS UF Remet.";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(389, 290);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(128, 16);
            this.label30.TabIndex = 251;
            this.label30.Text = "V. Imp. Importação";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(299, 290);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 16);
            this.label29.TabIndex = 250;
            this.label29.Text = "Vl. ICMS ST";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(190, 290);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(108, 16);
            this.label28.TabIndex = 249;
            this.label28.Text = "B. Cal. ICMS ST";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(106, 290);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(78, 16);
            this.label27.TabIndex = 248;
            this.label27.Text = "Valor ICMS";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(13, 290);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 16);
            this.label25.TabIndex = 247;
            this.label25.Text = "B. Cal. ICMS";
            // 
            // btnItens
            // 
            this.btnItens.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItens.Image = ((System.Drawing.Image)(resources.GetObject("btnItens.Image")));
            this.btnItens.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnItens.Location = new System.Drawing.Point(837, 334);
            this.btnItens.Name = "btnItens";
            this.btnItens.Size = new System.Drawing.Size(102, 41);
            this.btnItens.TabIndex = 246;
            this.btnItens.Text = "Itens";
            this.btnItens.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnItens.UseVisualStyleBackColor = true;
            this.btnItens.Click += new System.EventHandler(this.btnItens_Click);
            // 
            // txtObs
            // 
            this.txtObs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObs.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObs.Location = new System.Drawing.Point(15, 265);
            this.txtObs.MaxLength = 1000;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(373, 22);
            this.txtObs.TabIndex = 245;
            this.txtObs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObs_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(13, 246);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 16);
            this.label24.TabIndex = 244;
            this.label24.Text = "Observação";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox10);
            this.groupBox6.Controls.Add(this.pictureBox4);
            this.groupBox6.Controls.Add(this.dgParcelas);
            this.groupBox6.Controls.Add(this.dtPVencimento);
            this.groupBox6.Controls.Add(this.txtPTotal);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.ForeColor = System.Drawing.Color.Navy;
            this.groupBox6.Location = new System.Drawing.Point(706, 89);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(233, 154);
            this.groupBox6.TabIndex = 243;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Parcelas";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(189, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 288;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(83, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 287;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // dgParcelas
            // 
            this.dgParcelas.AllowUserToAddRows = false;
            this.dgParcelas.AllowUserToResizeColumns = false;
            this.dgParcelas.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgParcelas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgParcelas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParcelas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ENT_VALPARC,
            this.ENT_VENCTO});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgParcelas.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgParcelas.GridColor = System.Drawing.Color.LightGray;
            this.dgParcelas.Location = new System.Drawing.Point(9, 65);
            this.dgParcelas.Name = "dgParcelas";
            this.dgParcelas.ReadOnly = true;
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle114.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle114.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle114.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle114.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle114.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle114.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgParcelas.RowHeadersDefaultCellStyle = dataGridViewCellStyle114;
            dataGridViewCellStyle115.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle115.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle115.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle115.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle115.SelectionForeColor = System.Drawing.Color.Black;
            this.dgParcelas.RowsDefaultCellStyle = dataGridViewCellStyle115;
            this.dgParcelas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgParcelas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgParcelas.Size = new System.Drawing.Size(218, 80);
            this.dgParcelas.TabIndex = 195;
            this.dgParcelas.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgParcelas_RowsRemoved);
            // 
            // ENT_VALPARC
            // 
            this.ENT_VALPARC.DataPropertyName = "ENT_VALPARC";
            dataGridViewCellStyle26.Format = "N2";
            dataGridViewCellStyle26.NullValue = null;
            this.ENT_VALPARC.DefaultCellStyle = dataGridViewCellStyle26;
            this.ENT_VALPARC.HeaderText = "Valor";
            this.ENT_VALPARC.MaxInputLength = 18;
            this.ENT_VALPARC.Name = "ENT_VALPARC";
            this.ENT_VALPARC.ReadOnly = true;
            this.ENT_VALPARC.Width = 70;
            // 
            // ENT_VENCTO
            // 
            this.ENT_VENCTO.DataPropertyName = "ENT_VENCTO";
            this.ENT_VENCTO.HeaderText = "Vencimento";
            this.ENT_VENCTO.MaxInputLength = 10;
            this.ENT_VENCTO.Name = "ENT_VENCTO";
            this.ENT_VENCTO.ReadOnly = true;
            this.ENT_VENCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_VENCTO.Width = 258;
            // 
            // dtPVencimento
            // 
            this.dtPVencimento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPVencimento.Location = new System.Drawing.Point(110, 40);
            this.dtPVencimento.Name = "dtPVencimento";
            this.dtPVencimento.Size = new System.Drawing.Size(117, 22);
            this.dtPVencimento.TabIndex = 194;
            this.dtPVencimento.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtPVencimento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtPVencimento_KeyPress);
            // 
            // txtPTotal
            // 
            this.txtPTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPTotal.Location = new System.Drawing.Point(9, 40);
            this.txtPTotal.MaxLength = 10;
            this.txtPTotal.Name = "txtPTotal";
            this.txtPTotal.Size = new System.Drawing.Size(95, 22);
            this.txtPTotal.TabIndex = 193;
            this.txtPTotal.Text = "0,00";
            this.txtPTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPTotal_KeyPress);
            this.txtPTotal.Validated += new System.EventHandler(this.txtPTotal_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(107, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 16);
            this.label16.TabIndex = 157;
            this.label16.Text = "Vencimento";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 156;
            this.label14.Text = "Valor Total";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.txtFBusca);
            this.groupBox4.Controls.Add(this.cmbFBusca);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(16, 129);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(685, 114);
            this.groupBox4.TabIndex = 242;
            this.groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pictureBox6);
            this.groupBox5.Controls.Add(this.txtFCidade);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.txtFCnpj);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.txtFNome);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.ForeColor = System.Drawing.Color.Navy;
            this.groupBox5.Location = new System.Drawing.Point(9, 37);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(669, 68);
            this.groupBox5.TabIndex = 166;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Fornecedor";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(315, 18);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 172;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // txtFCidade
            // 
            this.txtFCidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFCidade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFCidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFCidade.Location = new System.Drawing.Point(434, 36);
            this.txtFCidade.MaxLength = 50;
            this.txtFCidade.Name = "txtFCidade";
            this.txtFCidade.ReadOnly = true;
            this.txtFCidade.Size = new System.Drawing.Size(228, 22);
            this.txtFCidade.TabIndex = 156;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(431, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 16);
            this.label19.TabIndex = 155;
            this.label19.Text = "Cidade/UF";
            // 
            // txtFCnpj
            // 
            this.txtFCnpj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFCnpj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFCnpj.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFCnpj.Location = new System.Drawing.Point(267, 36);
            this.txtFCnpj.MaxLength = 18;
            this.txtFCnpj.Name = "txtFCnpj";
            this.txtFCnpj.ReadOnly = true;
            this.txtFCnpj.Size = new System.Drawing.Size(161, 22);
            this.txtFCnpj.TabIndex = 154;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(264, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 16);
            this.label18.TabIndex = 153;
            this.label18.Text = "Docto.";
            // 
            // txtFNome
            // 
            this.txtFNome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFNome.Location = new System.Drawing.Point(10, 36);
            this.txtFNome.MaxLength = 50;
            this.txtFNome.Name = "txtFNome";
            this.txtFNome.ReadOnly = true;
            this.txtFNome.Size = new System.Drawing.Size(251, 22);
            this.txtFNome.TabIndex = 152;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(7, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 16);
            this.label17.TabIndex = 151;
            this.label17.Text = "Nome";
            // 
            // txtFBusca
            // 
            this.txtFBusca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFBusca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFBusca.Location = new System.Drawing.Point(218, 14);
            this.txtFBusca.MaxLength = 50;
            this.txtFBusca.Name = "txtFBusca";
            this.txtFBusca.Size = new System.Drawing.Size(460, 22);
            this.txtFBusca.TabIndex = 165;
            this.txtFBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFBusca_KeyPress);
            // 
            // cmbFBusca
            // 
            this.cmbFBusca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFBusca.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFBusca.FormattingEnabled = true;
            this.cmbFBusca.Items.AddRange(new object[] {
            "NOME",
            "CNPJ"});
            this.cmbFBusca.Location = new System.Drawing.Point(111, 13);
            this.cmbFBusca.Name = "cmbFBusca";
            this.cmbFBusca.Size = new System.Drawing.Size(99, 24);
            this.cmbFBusca.TabIndex = 153;
            this.cmbFBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFBusca_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 16);
            this.label6.TabIndex = 152;
            this.label6.Text = "Pesquisar por:";
            // 
            // txtNCompra
            // 
            this.txtNCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNCompra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNCompra.Location = new System.Drawing.Point(489, 104);
            this.txtNCompra.MaxLength = 10;
            this.txtNCompra.Name = "txtNCompra";
            this.txtNCompra.Size = new System.Drawing.Size(211, 22);
            this.txtNCompra.TabIndex = 241;
            this.txtNCompra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNCompra_KeyPress);
            // 
            // cmbComp
            // 
            this.cmbComp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbComp.FormattingEnabled = true;
            this.cmbComp.Location = new System.Drawing.Point(291, 104);
            this.cmbComp.Name = "cmbComp";
            this.cmbComp.Size = new System.Drawing.Size(192, 24);
            this.cmbComp.TabIndex = 240;
            this.cmbComp.SelectedIndexChanged += new System.EventHandler(this.cmbComp_SelectedIndexChanged);
            this.cmbComp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbComp_KeyPress);
            // 
            // txtComp
            // 
            this.txtComp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComp.Location = new System.Drawing.Point(233, 104);
            this.txtComp.MaxLength = 10;
            this.txtComp.Name = "txtComp";
            this.txtComp.Size = new System.Drawing.Size(52, 22);
            this.txtComp.TabIndex = 239;
            this.txtComp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComp_KeyPress);
            this.txtComp.Validated += new System.EventHandler(this.txtComp_Validated);
            // 
            // cmbNOper
            // 
            this.cmbNOper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNOper.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNOper.FormattingEnabled = true;
            this.cmbNOper.Location = new System.Drawing.Point(16, 104);
            this.cmbNOper.Name = "cmbNOper";
            this.cmbNOper.Size = new System.Drawing.Size(210, 24);
            this.cmbNOper.TabIndex = 238;
            this.cmbNOper.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbNOper_KeyPress);
            // 
            // txtNTotal
            // 
            this.txtNTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNTotal.Location = new System.Drawing.Point(816, 61);
            this.txtNTotal.MaxLength = 10;
            this.txtNTotal.Name = "txtNTotal";
            this.txtNTotal.Size = new System.Drawing.Size(123, 22);
            this.txtNTotal.TabIndex = 237;
            this.txtNTotal.Text = "0,00";
            this.txtNTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNTotal_KeyPress);
            this.txtNTotal.Validated += new System.EventHandler(this.txtNTotal_Validated);
            // 
            // txtFatura
            // 
            this.txtFatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFatura.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFatura.Location = new System.Drawing.Point(706, 61);
            this.txtFatura.MaxLength = 10;
            this.txtFatura.Name = "txtFatura";
            this.txtFatura.Size = new System.Drawing.Size(104, 22);
            this.txtFatura.TabIndex = 236;
            this.txtFatura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFatura_KeyPress);
            // 
            // txtSerie
            // 
            this.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.Location = new System.Drawing.Point(635, 61);
            this.txtSerie.MaxLength = 5;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(65, 22);
            this.txtSerie.TabIndex = 235;
            this.txtSerie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSerie_KeyPress);
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(489, 61);
            this.txtDocto.MaxLength = 10;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(140, 22);
            this.txtDocto.TabIndex = 234;
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // dtEmissao
            // 
            this.dtEmissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtEmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEmissao.Location = new System.Drawing.Point(374, 61);
            this.dtEmissao.Name = "dtEmissao";
            this.dtEmissao.Size = new System.Drawing.Size(109, 22);
            this.dtEmissao.TabIndex = 233;
            this.dtEmissao.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtEmissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtEmissao_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(487, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 16);
            this.label13.TabIndex = 191;
            this.label13.Text = "N° Pedido de Compra";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(231, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 16);
            this.label9.TabIndex = 188;
            this.label9.Text = "Comprador";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(13, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 16);
            this.label7.TabIndex = 186;
            this.label7.Text = "Natureza da Operação";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(814, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(76, 16);
            this.label23.TabIndex = 185;
            this.label23.Text = "Valor Total";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(704, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 16);
            this.label22.TabIndex = 184;
            this.label22.Text = "N° Fatura";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(633, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 16);
            this.label21.TabIndex = 183;
            this.label21.Text = "Série";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(487, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 16);
            this.label3.TabIndex = 182;
            this.label3.Text = "N° Docto.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(231, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 177;
            this.label2.Text = "Tipo de Docto.";
            // 
            // cmbDocto
            // 
            this.cmbDocto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocto.FormattingEnabled = true;
            this.cmbDocto.Location = new System.Drawing.Point(234, 61);
            this.cmbDocto.Name = "cmbDocto";
            this.cmbDocto.Size = new System.Drawing.Size(131, 24);
            this.cmbDocto.TabIndex = 176;
            this.cmbDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDocto_KeyPress);
            this.cmbDocto.Validated += new System.EventHandler(this.cmbDocto_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(372, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 16);
            this.label5.TabIndex = 155;
            this.label5.Text = "Dt. Emissão";
            // 
            // dtLancamento
            // 
            this.dtLancamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtLancamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtLancamento.Location = new System.Drawing.Point(117, 63);
            this.dtLancamento.Name = "dtLancamento";
            this.dtLancamento.Size = new System.Drawing.Size(109, 22);
            this.dtLancamento.TabIndex = 154;
            this.dtLancamento.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtLancamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtLancamento_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(114, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 153;
            this.label4.Text = "Dt. Lancto.";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 22);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 148;
            this.lblEstab.Tag = "";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(85, 45);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 110;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // txtEntID
            // 
            this.txtEntID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEntID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEntID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntID.Location = new System.Drawing.Point(16, 63);
            this.txtEntID.MaxLength = 18;
            this.txtEntID.Name = "txtEntID";
            this.txtEntID.ReadOnly = true;
            this.txtEntID.Size = new System.Drawing.Size(95, 22);
            this.txtEntID.TabIndex = 81;
            this.txtEntID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Entrada ID";
            // 
            // tpItens
            // 
            this.tpItens.Controls.Add(this.groupBox7);
            this.tpItens.Location = new System.Drawing.Point(4, 25);
            this.tpItens.Name = "tpItens";
            this.tpItens.Size = new System.Drawing.Size(962, 470);
            this.tpItens.TabIndex = 2;
            this.tpItens.Text = "Itens";
            this.tpItens.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.txtCest);
            this.groupBox7.Controls.Add(this.label83);
            this.groupBox7.Controls.Add(this.txtPisCst);
            this.groupBox7.Controls.Add(this.txtPVIpi);
            this.groupBox7.Controls.Add(this.txtCstConfis);
            this.groupBox7.Controls.Add(this.label63);
            this.groupBox7.Controls.Add(this.label89);
            this.groupBox7.Controls.Add(this.label90);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this.label86);
            this.groupBox7.Controls.Add(this.txtCfop);
            this.groupBox7.Controls.Add(this.label82);
            this.groupBox7.Controls.Add(this.txtNCM);
            this.groupBox7.Controls.Add(this.label81);
            this.groupBox7.Controls.Add(this.cmbOrigem);
            this.groupBox7.Controls.Add(this.label88);
            this.groupBox7.Controls.Add(this.txtAliIpi);
            this.groupBox7.Controls.Add(this.txtBaseIpi);
            this.groupBox7.Controls.Add(this.txtCstIpi);
            this.groupBox7.Controls.Add(this.label87);
            this.groupBox7.Controls.Add(this.txtGDesc);
            this.groupBox7.Controls.Add(this.txtGTotal);
            this.groupBox7.Controls.Add(this.txtGEqtde);
            this.groupBox7.Controls.Add(this.txtGQtde);
            this.groupBox7.Controls.Add(this.label78);
            this.groupBox7.Controls.Add(this.pictureBox16);
            this.groupBox7.Controls.Add(this.pictureBox15);
            this.groupBox7.Controls.Add(this.pictureBox14);
            this.groupBox7.Controls.Add(this.pictureBox13);
            this.groupBox7.Controls.Add(this.pictureBox12);
            this.groupBox7.Controls.Add(this.pictureBox11);
            this.groupBox7.Controls.Add(this.dgItens);
            this.groupBox7.Controls.Add(this.btnIncluir);
            this.groupBox7.Controls.Add(this.txtEnqIpi);
            this.groupBox7.Controls.Add(this.txtICMSRet);
            this.groupBox7.Controls.Add(this.txtBcIcmsRet);
            this.groupBox7.Controls.Add(this.txtICMSST);
            this.groupBox7.Controls.Add(this.txtBIcmsSt);
            this.groupBox7.Controls.Add(this.txtCstIcms);
            this.groupBox7.Controls.Add(this.txtPVIcms);
            this.groupBox7.Controls.Add(this.txtVBIcms);
            this.groupBox7.Controls.Add(this.dtValidade);
            this.groupBox7.Controls.Add(this.cmbIcms);
            this.groupBox7.Controls.Add(this.txtLote);
            this.groupBox7.Controls.Add(this.txtPVenda);
            this.groupBox7.Controls.Add(this.txtPMargem);
            this.groupBox7.Controls.Add(this.txtPVTotal);
            this.groupBox7.Controls.Add(this.txtPDescUni);
            this.groupBox7.Controls.Add(this.txtPDesc);
            this.groupBox7.Controls.Add(this.txtVUni);
            this.groupBox7.Controls.Add(this.txtEQtde);
            this.groupBox7.Controls.Add(this.txtUn);
            this.groupBox7.Controls.Add(this.txtQtde);
            this.groupBox7.Controls.Add(this.label60);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Controls.Add(this.label62);
            this.groupBox7.Controls.Add(this.label64);
            this.groupBox7.Controls.Add(this.label65);
            this.groupBox7.Controls.Add(this.label66);
            this.groupBox7.Controls.Add(this.label67);
            this.groupBox7.Controls.Add(this.label68);
            this.groupBox7.Controls.Add(this.label69);
            this.groupBox7.Controls.Add(this.label58);
            this.groupBox7.Controls.Add(this.label57);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this.label55);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label53);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this.label51);
            this.groupBox7.Controls.Add(this.label50);
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this.label48);
            this.groupBox7.Controls.Add(this.txtComissao);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Controls.Add(this.txtDescr);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this.txtCodBarras);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this.txtItem);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.Black;
            this.groupBox7.Location = new System.Drawing.Point(6, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(945, 458);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Itens do Documento";
            // 
            // txtCest
            // 
            this.txtCest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCest.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCest.Location = new System.Drawing.Point(257, 169);
            this.txtCest.MaxLength = 10;
            this.txtCest.Name = "txtCest";
            this.txtCest.Size = new System.Drawing.Size(75, 22);
            this.txtCest.TabIndex = 323;
            this.txtCest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCest_KeyPress);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Navy;
            this.label83.Location = new System.Drawing.Point(253, 150);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(42, 16);
            this.label83.TabIndex = 322;
            this.label83.Text = "CEST";
            // 
            // txtPisCst
            // 
            this.txtPisCst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPisCst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPisCst.Location = new System.Drawing.Point(179, 169);
            this.txtPisCst.MaxLength = 10;
            this.txtPisCst.Name = "txtPisCst";
            this.txtPisCst.Size = new System.Drawing.Size(75, 22);
            this.txtPisCst.TabIndex = 321;
            this.txtPisCst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPisCst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPisCst_KeyPress);
            // 
            // txtPVIpi
            // 
            this.txtPVIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVIpi.Location = new System.Drawing.Point(97, 169);
            this.txtPVIpi.MaxLength = 10;
            this.txtPVIpi.Name = "txtPVIpi";
            this.txtPVIpi.Size = new System.Drawing.Size(75, 22);
            this.txtPVIpi.TabIndex = 320;
            this.txtPVIpi.Text = "0,00";
            this.txtPVIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVIpi_KeyPress);
            this.txtPVIpi.Validated += new System.EventHandler(this.txtPVIpi_Validated);
            // 
            // txtCstConfis
            // 
            this.txtCstConfis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstConfis.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstConfis.Location = new System.Drawing.Point(17, 169);
            this.txtCstConfis.MaxLength = 10;
            this.txtCstConfis.Name = "txtCstConfis";
            this.txtCstConfis.Size = new System.Drawing.Size(75, 22);
            this.txtCstConfis.TabIndex = 319;
            this.txtCstConfis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstConfis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstConfis_KeyPress);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Navy;
            this.label63.Location = new System.Drawing.Point(332, 109);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(55, 16);
            this.label63.TabIndex = 318;
            this.label63.Text = "CST IPI";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Navy;
            this.label89.Location = new System.Drawing.Point(253, 110);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(57, 16);
            this.label89.TabIndex = 317;
            this.label89.Text = "Enq. IPI";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Navy;
            this.label90.Location = new System.Drawing.Point(14, 151);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(77, 16);
            this.label90.TabIndex = 316;
            this.label90.Text = "CST Cofins";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Navy;
            this.label85.Location = new System.Drawing.Point(175, 150);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(60, 16);
            this.label85.TabIndex = 304;
            this.label85.Text = "CST PIS";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Navy;
            this.label86.Location = new System.Drawing.Point(94, 152);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(62, 16);
            this.label86.TabIndex = 303;
            this.label86.Text = "Valor IPI";
            // 
            // txtCfop
            // 
            this.txtCfop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCfop.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCfop.Location = new System.Drawing.Point(774, 127);
            this.txtCfop.Name = "txtCfop";
            this.txtCfop.Size = new System.Drawing.Size(75, 22);
            this.txtCfop.TabIndex = 313;
            this.txtCfop.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCfop_KeyPress);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Navy;
            this.label82.Location = new System.Drawing.Point(771, 109);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(44, 16);
            this.label82.TabIndex = 312;
            this.label82.Text = "CFOP";
            // 
            // txtNCM
            // 
            this.txtNCM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNCM.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNCM.Location = new System.Drawing.Point(688, 127);
            this.txtNCM.Name = "txtNCM";
            this.txtNCM.Size = new System.Drawing.Size(75, 22);
            this.txtNCM.TabIndex = 311;
            this.txtNCM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNCM_KeyPress);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Navy;
            this.label81.Location = new System.Drawing.Point(685, 109);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(37, 16);
            this.label81.TabIndex = 310;
            this.label81.Text = "NCM";
            // 
            // cmbOrigem
            // 
            this.cmbOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrigem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrigem.FormattingEnabled = true;
            this.cmbOrigem.Items.AddRange(new object[] {
            "Nacional",
            "Estrangeira - Imp. Direta",
            "Estrangeira - Merc. Interno",
            "Nacional - Sup. 40%",
            "Nacional - PPB",
            "Nacional - Inf. 40%",
            "Estrangeira - Imp. Direta sem similar nacional",
            "Estrangeira - Merc. Interno sem similar nacional",
            "Nacional - Sup. 70%"});
            this.cmbOrigem.Location = new System.Drawing.Point(576, 126);
            this.cmbOrigem.Name = "cmbOrigem";
            this.cmbOrigem.Size = new System.Drawing.Size(104, 24);
            this.cmbOrigem.TabIndex = 309;
            this.cmbOrigem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbOrigem_KeyPress);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Navy;
            this.label88.Location = new System.Drawing.Point(573, 109);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(55, 16);
            this.label88.TabIndex = 308;
            this.label88.Text = "Origem";
            // 
            // txtAliIpi
            // 
            this.txtAliIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAliIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAliIpi.Location = new System.Drawing.Point(495, 127);
            this.txtAliIpi.MaxLength = 10;
            this.txtAliIpi.Name = "txtAliIpi";
            this.txtAliIpi.Size = new System.Drawing.Size(75, 22);
            this.txtAliIpi.TabIndex = 307;
            this.txtAliIpi.Text = "0,00";
            this.txtAliIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAliIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAliIpi_KeyPress);
            this.txtAliIpi.Validated += new System.EventHandler(this.txtAliIpi_Validated);
            // 
            // txtBaseIpi
            // 
            this.txtBaseIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBaseIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBaseIpi.Location = new System.Drawing.Point(414, 127);
            this.txtBaseIpi.MaxLength = 10;
            this.txtBaseIpi.Name = "txtBaseIpi";
            this.txtBaseIpi.Size = new System.Drawing.Size(75, 22);
            this.txtBaseIpi.TabIndex = 306;
            this.txtBaseIpi.Text = "0,00";
            this.txtBaseIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBaseIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBaseIpi_KeyPress);
            this.txtBaseIpi.Validated += new System.EventHandler(this.txtBaseIpi_Validated);
            // 
            // txtCstIpi
            // 
            this.txtCstIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstIpi.Location = new System.Drawing.Point(335, 127);
            this.txtCstIpi.MaxLength = 10;
            this.txtCstIpi.Name = "txtCstIpi";
            this.txtCstIpi.Size = new System.Drawing.Size(75, 22);
            this.txtCstIpi.TabIndex = 305;
            this.txtCstIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstIpi_KeyPress);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Navy;
            this.label87.Location = new System.Drawing.Point(492, 110);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(58, 16);
            this.label87.TabIndex = 302;
            this.label87.Text = "Aliq. IPI";
            // 
            // txtGDesc
            // 
            this.txtGDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtGDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGDesc.Location = new System.Drawing.Point(734, 432);
            this.txtGDesc.Name = "txtGDesc";
            this.txtGDesc.ReadOnly = true;
            this.txtGDesc.Size = new System.Drawing.Size(93, 22);
            this.txtGDesc.TabIndex = 295;
            this.txtGDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGTotal
            // 
            this.txtGTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtGTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGTotal.Location = new System.Drawing.Point(835, 432);
            this.txtGTotal.Name = "txtGTotal";
            this.txtGTotal.ReadOnly = true;
            this.txtGTotal.Size = new System.Drawing.Size(99, 22);
            this.txtGTotal.TabIndex = 294;
            this.txtGTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGEqtde
            // 
            this.txtGEqtde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtGEqtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGEqtde.Location = new System.Drawing.Point(498, 432);
            this.txtGEqtde.Name = "txtGEqtde";
            this.txtGEqtde.ReadOnly = true;
            this.txtGEqtde.Size = new System.Drawing.Size(85, 22);
            this.txtGEqtde.TabIndex = 293;
            this.txtGEqtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGQtde
            // 
            this.txtGQtde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtGQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGQtde.Location = new System.Drawing.Point(368, 432);
            this.txtGQtde.Name = "txtGQtde";
            this.txtGQtde.ReadOnly = true;
            this.txtGQtde.Size = new System.Drawing.Size(50, 22);
            this.txtGQtde.TabIndex = 292;
            this.txtGQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Navy;
            this.label78.Location = new System.Drawing.Point(13, 435);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(49, 16);
            this.label78.TabIndex = 291;
            this.label78.Text = "Totais:";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(456, 67);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(16, 16);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox16.TabIndex = 289;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseHover += new System.EventHandler(this.pictureBox16_MouseHover);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(846, 25);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(16, 16);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox15.TabIndex = 288;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(156, 67);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(16, 16);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox14.TabIndex = 287;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(640, 25);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(16, 16);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox13.TabIndex = 286;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseHover += new System.EventHandler(this.pictureBox13_MouseHover);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(245, 25);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(16, 16);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox12.TabIndex = 285;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(113, 25);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 284;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover);
            // 
            // dgItens
            // 
            this.dgItens.AllowUserToAddRows = false;
            this.dgItens.AllowUserToResizeColumns = false;
            this.dgItens.AllowUserToResizeRows = false;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.Color.Black;
            this.dgItens.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle61;
            this.dgItens.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgItens.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgItens.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle62;
            this.dgItens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ENT_SEQUENCIA,
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.COMISSAO,
            this.QTDE,
            this.ENT_UNIDADE,
            this.ENT_QTDESTOQUE,
            this.ENT_UNITARIO,
            this.DESC_PORC,
            this.ENT_DESCONTOITEM,
            this.ENT_TOTITEM,
            this.PROD_MARGEM,
            this.PRE_VALOR,
            this.PROD_COMISSAO,
            this.VALOR_PMC,
            this.PROD_ESTATUAL,
            this.PROD_CONTROLADO,
            this.ENT_ICMS,
            this.ENT_ALIQICMS,
            this.ENT_VALOR_BC_ICMS,
            this.ENT_VALORICMS,
            this.PROD_CST,
            this.ENT_BC_ICMS_ST,
            this.ENT_ICMS_ST,
            this.ENT_REDUCAOBASEICMS,
            this.ENT_ICMS_RET,
            this.ENT_ENQ_IPI,
            this.ENT_CST_IPI,
            this.ENT_BASE_IPI,
            this.ENT_ALIQIPI,
            this.ENT_ORIGEM,
            this.ENT_NCM,
            this.PROD_CFOP,
            this.ENT_CST_COFINS,
            this.ENT_VALOR_IPI,
            this.ENT_CST_PIS,
            this.ENT_DTVALIDADE,
            this.NO_CODIGO,
            this.NO_COMP,
            this.COMP_NUMERO,
            this.PROD_CADASTRADO,
            this.EST_INDICE,
            this.PROD_PRECOMPRA,
            this.PROD_CEST,
            this.PROD_REGMS});
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgItens.DefaultCellStyle = dataGridViewCellStyle84;
            this.dgItens.GridColor = System.Drawing.Color.LightGray;
            this.dgItens.Location = new System.Drawing.Point(16, 199);
            this.dgItens.MultiSelect = false;
            this.dgItens.Name = "dgItens";
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle85.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle85.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle85.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle85.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgItens.RowHeadersDefaultCellStyle = dataGridViewCellStyle85;
            this.dgItens.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle86.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle86.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle86.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle86.SelectionForeColor = System.Drawing.Color.Black;
            this.dgItens.RowsDefaultCellStyle = dataGridViewCellStyle86;
            this.dgItens.Size = new System.Drawing.Size(918, 231);
            this.dgItens.TabIndex = 280;
            this.dgItens.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgItens_CellBeginEdit);
            this.dgItens.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgItens_CellClick);
            this.dgItens.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgItens_CellEndEdit);
            this.dgItens.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgItens_CellValidated);
            this.dgItens.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgItens_RowPrePaint);
            this.dgItens.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgItens_RowsRemoved);
            this.dgItens.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgItens_UserDeletingRow);
            // 
            // ENT_SEQUENCIA
            // 
            this.ENT_SEQUENCIA.HeaderText = "Item";
            this.ENT_SEQUENCIA.MaxInputLength = 18;
            this.ENT_SEQUENCIA.Name = "ENT_SEQUENCIA";
            this.ENT_SEQUENCIA.ReadOnly = true;
            this.ENT_SEQUENCIA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_SEQUENCIA.Visible = false;
            this.ENT_SEQUENCIA.Width = 50;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.HeaderText = "Código";
            this.PROD_CODIGO.MaxInputLength = 15;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_CODIGO.Width = 110;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.HeaderText = "Produto";
            this.PROD_DESCR.MaxInputLength = 50;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESCR.Width = 200;
            // 
            // COMISSAO
            // 
            this.COMISSAO.HeaderText = "Comissão";
            this.COMISSAO.Name = "COMISSAO";
            this.COMISSAO.Visible = false;
            // 
            // QTDE
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.QTDE.DefaultCellStyle = dataGridViewCellStyle63;
            this.QTDE.HeaderText = "Qtde.";
            this.QTDE.MaxInputLength = 18;
            this.QTDE.Name = "QTDE";
            this.QTDE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QTDE.Width = 50;
            // 
            // ENT_UNIDADE
            // 
            this.ENT_UNIDADE.HeaderText = "Un. Trib.";
            this.ENT_UNIDADE.MaxInputLength = 2;
            this.ENT_UNIDADE.Name = "ENT_UNIDADE";
            this.ENT_UNIDADE.ReadOnly = true;
            this.ENT_UNIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_UNIDADE.Width = 80;
            // 
            // ENT_QTDESTOQUE
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ENT_QTDESTOQUE.DefaultCellStyle = dataGridViewCellStyle64;
            this.ENT_QTDESTOQUE.HeaderText = "Qtde.Estoq.";
            this.ENT_QTDESTOQUE.MaxInputLength = 18;
            this.ENT_QTDESTOQUE.Name = "ENT_QTDESTOQUE";
            this.ENT_QTDESTOQUE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_QTDESTOQUE.Width = 85;
            // 
            // ENT_UNITARIO
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle65.Format = "N2";
            dataGridViewCellStyle65.NullValue = null;
            this.ENT_UNITARIO.DefaultCellStyle = dataGridViewCellStyle65;
            this.ENT_UNITARIO.HeaderText = "Vl. Unit.";
            this.ENT_UNITARIO.MaxInputLength = 18;
            this.ENT_UNITARIO.Name = "ENT_UNITARIO";
            this.ENT_UNITARIO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_UNITARIO.Width = 85;
            // 
            // DESC_PORC
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle66.Format = "N2";
            dataGridViewCellStyle66.NullValue = null;
            this.DESC_PORC.DefaultCellStyle = dataGridViewCellStyle66;
            this.DESC_PORC.HeaderText = "Desc. %";
            this.DESC_PORC.MaxInputLength = 18;
            this.DESC_PORC.Name = "DESC_PORC";
            this.DESC_PORC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DESC_PORC.Width = 80;
            // 
            // ENT_DESCONTOITEM
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle67.Format = "N2";
            dataGridViewCellStyle67.NullValue = null;
            this.ENT_DESCONTOITEM.DefaultCellStyle = dataGridViewCellStyle67;
            this.ENT_DESCONTOITEM.HeaderText = "Desc. Uni.";
            this.ENT_DESCONTOITEM.MaxInputLength = 18;
            this.ENT_DESCONTOITEM.Name = "ENT_DESCONTOITEM";
            this.ENT_DESCONTOITEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_DESCONTOITEM.Width = 80;
            // 
            // ENT_TOTITEM
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle68.Format = "N2";
            dataGridViewCellStyle68.NullValue = null;
            this.ENT_TOTITEM.DefaultCellStyle = dataGridViewCellStyle68;
            this.ENT_TOTITEM.HeaderText = "Valor Total";
            this.ENT_TOTITEM.MaxInputLength = 18;
            this.ENT_TOTITEM.Name = "ENT_TOTITEM";
            this.ENT_TOTITEM.ReadOnly = true;
            this.ENT_TOTITEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_TOTITEM.Width = 105;
            // 
            // PROD_MARGEM
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle69.Format = "N2";
            this.PROD_MARGEM.DefaultCellStyle = dataGridViewCellStyle69;
            this.PROD_MARGEM.HeaderText = "Margem";
            this.PROD_MARGEM.MaxInputLength = 18;
            this.PROD_MARGEM.Name = "PROD_MARGEM";
            this.PROD_MARGEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_MARGEM.Width = 70;
            // 
            // PRE_VALOR
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle70.Format = "N2";
            dataGridViewCellStyle70.NullValue = null;
            this.PRE_VALOR.DefaultCellStyle = dataGridViewCellStyle70;
            this.PRE_VALOR.HeaderText = "Vl. Venda";
            this.PRE_VALOR.MaxInputLength = 18;
            this.PRE_VALOR.Name = "PRE_VALOR";
            this.PRE_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRE_VALOR.Width = 95;
            // 
            // PROD_COMISSAO
            // 
            this.PROD_COMISSAO.DataPropertyName = "PROD_COMISSAO";
            this.PROD_COMISSAO.HeaderText = "Comissao";
            this.PROD_COMISSAO.Name = "PROD_COMISSAO";
            this.PROD_COMISSAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VALOR_PMC
            // 
            this.VALOR_PMC.DataPropertyName = "VALOR_PMC";
            dataGridViewCellStyle71.Format = "N2";
            dataGridViewCellStyle71.NullValue = null;
            this.VALOR_PMC.DefaultCellStyle = dataGridViewCellStyle71;
            this.VALOR_PMC.HeaderText = "PMC";
            this.VALOR_PMC.Name = "VALOR_PMC";
            this.VALOR_PMC.ReadOnly = true;
            this.VALOR_PMC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_ESTATUAL
            // 
            this.PROD_ESTATUAL.DataPropertyName = "PROD_ESTATUAL";
            this.PROD_ESTATUAL.HeaderText = "Qtde. Est.";
            this.PROD_ESTATUAL.Name = "PROD_ESTATUAL";
            this.PROD_ESTATUAL.ReadOnly = true;
            this.PROD_ESTATUAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_CONTROLADO
            // 
            this.PROD_CONTROLADO.HeaderText = "Lote";
            this.PROD_CONTROLADO.MaxInputLength = 20;
            this.PROD_CONTROLADO.Name = "PROD_CONTROLADO";
            this.PROD_CONTROLADO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_ICMS
            // 
            this.ENT_ICMS.HeaderText = "ICMS";
            this.ENT_ICMS.MaxInputLength = 2;
            this.ENT_ICMS.Name = "ENT_ICMS";
            this.ENT_ICMS.ReadOnly = true;
            this.ENT_ICMS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_ICMS.Width = 60;
            // 
            // ENT_ALIQICMS
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle72.Format = "N2";
            dataGridViewCellStyle72.NullValue = null;
            this.ENT_ALIQICMS.DefaultCellStyle = dataGridViewCellStyle72;
            this.ENT_ALIQICMS.HeaderText = "Aliq.";
            this.ENT_ALIQICMS.MaxInputLength = 18;
            this.ENT_ALIQICMS.Name = "ENT_ALIQICMS";
            this.ENT_ALIQICMS.ReadOnly = true;
            this.ENT_ALIQICMS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ENT_ALIQICMS.Visible = false;
            this.ENT_ALIQICMS.Width = 60;
            // 
            // ENT_VALOR_BC_ICMS
            // 
            dataGridViewCellStyle73.NullValue = "N2";
            this.ENT_VALOR_BC_ICMS.DefaultCellStyle = dataGridViewCellStyle73;
            this.ENT_VALOR_BC_ICMS.HeaderText = "Vl. BC ICMS";
            this.ENT_VALOR_BC_ICMS.Name = "ENT_VALOR_BC_ICMS";
            this.ENT_VALOR_BC_ICMS.Visible = false;
            this.ENT_VALOR_BC_ICMS.Width = 110;
            // 
            // ENT_VALORICMS
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle74.Format = "N2";
            this.ENT_VALORICMS.DefaultCellStyle = dataGridViewCellStyle74;
            this.ENT_VALORICMS.HeaderText = "Val. ICMS";
            this.ENT_VALORICMS.MaxInputLength = 18;
            this.ENT_VALORICMS.Name = "ENT_VALORICMS";
            this.ENT_VALORICMS.Visible = false;
            // 
            // PROD_CST
            // 
            this.PROD_CST.HeaderText = "CST ICMS";
            this.PROD_CST.Name = "PROD_CST";
            this.PROD_CST.Visible = false;
            // 
            // ENT_BC_ICMS_ST
            // 
            dataGridViewCellStyle75.NullValue = "N2";
            this.ENT_BC_ICMS_ST.DefaultCellStyle = dataGridViewCellStyle75;
            this.ENT_BC_ICMS_ST.HeaderText = "BC ICMS ST";
            this.ENT_BC_ICMS_ST.Name = "ENT_BC_ICMS_ST";
            this.ENT_BC_ICMS_ST.Visible = false;
            this.ENT_BC_ICMS_ST.Width = 110;
            // 
            // ENT_ICMS_ST
            // 
            dataGridViewCellStyle76.NullValue = "N2";
            this.ENT_ICMS_ST.DefaultCellStyle = dataGridViewCellStyle76;
            this.ENT_ICMS_ST.HeaderText = "ICMS ST";
            this.ENT_ICMS_ST.Name = "ENT_ICMS_ST";
            this.ENT_ICMS_ST.Visible = false;
            // 
            // ENT_REDUCAOBASEICMS
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle77.Format = "N2";
            this.ENT_REDUCAOBASEICMS.DefaultCellStyle = dataGridViewCellStyle77;
            this.ENT_REDUCAOBASEICMS.HeaderText = "Base Red. ICMS";
            this.ENT_REDUCAOBASEICMS.MaxInputLength = 18;
            this.ENT_REDUCAOBASEICMS.Name = "ENT_REDUCAOBASEICMS";
            this.ENT_REDUCAOBASEICMS.Visible = false;
            this.ENT_REDUCAOBASEICMS.Width = 150;
            // 
            // ENT_ICMS_RET
            // 
            dataGridViewCellStyle78.NullValue = "N2";
            this.ENT_ICMS_RET.DefaultCellStyle = dataGridViewCellStyle78;
            this.ENT_ICMS_RET.HeaderText = "ICMS RET.";
            this.ENT_ICMS_RET.Name = "ENT_ICMS_RET";
            this.ENT_ICMS_RET.Visible = false;
            // 
            // ENT_ENQ_IPI
            // 
            this.ENT_ENQ_IPI.HeaderText = "Enq. IPI";
            this.ENT_ENQ_IPI.Name = "ENT_ENQ_IPI";
            this.ENT_ENQ_IPI.Visible = false;
            // 
            // ENT_CST_IPI
            // 
            this.ENT_CST_IPI.HeaderText = "CST IPI";
            this.ENT_CST_IPI.Name = "ENT_CST_IPI";
            this.ENT_CST_IPI.Visible = false;
            // 
            // ENT_BASE_IPI
            // 
            dataGridViewCellStyle79.NullValue = "N2";
            this.ENT_BASE_IPI.DefaultCellStyle = dataGridViewCellStyle79;
            this.ENT_BASE_IPI.HeaderText = "Base IPI";
            this.ENT_BASE_IPI.Name = "ENT_BASE_IPI";
            this.ENT_BASE_IPI.Visible = false;
            // 
            // ENT_ALIQIPI
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle80.Format = "N2";
            dataGridViewCellStyle80.NullValue = null;
            this.ENT_ALIQIPI.DefaultCellStyle = dataGridViewCellStyle80;
            this.ENT_ALIQIPI.HeaderText = "Aliq. IPI";
            this.ENT_ALIQIPI.MaxInputLength = 18;
            this.ENT_ALIQIPI.Name = "ENT_ALIQIPI";
            this.ENT_ALIQIPI.Visible = false;
            // 
            // ENT_ORIGEM
            // 
            this.ENT_ORIGEM.HeaderText = "Origem";
            this.ENT_ORIGEM.Name = "ENT_ORIGEM";
            this.ENT_ORIGEM.ReadOnly = true;
            this.ENT_ORIGEM.Width = 80;
            // 
            // ENT_NCM
            // 
            this.ENT_NCM.DataPropertyName = "ENT_NCM";
            this.ENT_NCM.HeaderText = "NCM";
            this.ENT_NCM.MaxInputLength = 20;
            this.ENT_NCM.Name = "ENT_NCM";
            this.ENT_NCM.ReadOnly = true;
            // 
            // PROD_CFOP
            // 
            this.PROD_CFOP.HeaderText = "CFOP";
            this.PROD_CFOP.Name = "PROD_CFOP";
            this.PROD_CFOP.Visible = false;
            // 
            // ENT_CST_COFINS
            // 
            this.ENT_CST_COFINS.HeaderText = "CST COFINS";
            this.ENT_CST_COFINS.Name = "ENT_CST_COFINS";
            this.ENT_CST_COFINS.Visible = false;
            this.ENT_CST_COFINS.Width = 120;
            // 
            // ENT_VALOR_IPI
            // 
            dataGridViewCellStyle81.Format = "N2";
            dataGridViewCellStyle81.NullValue = null;
            this.ENT_VALOR_IPI.DefaultCellStyle = dataGridViewCellStyle81;
            this.ENT_VALOR_IPI.HeaderText = "Valor IPI";
            this.ENT_VALOR_IPI.Name = "ENT_VALOR_IPI";
            this.ENT_VALOR_IPI.Visible = false;
            // 
            // ENT_CST_PIS
            // 
            this.ENT_CST_PIS.HeaderText = "CST PIS";
            this.ENT_CST_PIS.Name = "ENT_CST_PIS";
            this.ENT_CST_PIS.Visible = false;
            // 
            // ENT_DTVALIDADE
            // 
            dataGridViewCellStyle82.Format = "d";
            dataGridViewCellStyle82.NullValue = null;
            this.ENT_DTVALIDADE.DefaultCellStyle = dataGridViewCellStyle82;
            this.ENT_DTVALIDADE.HeaderText = "Validade";
            this.ENT_DTVALIDADE.MaxInputLength = 10;
            this.ENT_DTVALIDADE.Name = "ENT_DTVALIDADE";
            this.ENT_DTVALIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NO_CODIGO
            // 
            this.NO_CODIGO.HeaderText = "Nat. Oper.";
            this.NO_CODIGO.MaxInputLength = 18;
            this.NO_CODIGO.Name = "NO_CODIGO";
            this.NO_CODIGO.Visible = false;
            // 
            // NO_COMP
            // 
            this.NO_COMP.HeaderText = "ItemDataNatOper";
            this.NO_COMP.MaxInputLength = 18;
            this.NO_COMP.Name = "NO_COMP";
            this.NO_COMP.Visible = false;
            this.NO_COMP.Width = 150;
            // 
            // COMP_NUMERO
            // 
            this.COMP_NUMERO.HeaderText = "Comp. Num";
            this.COMP_NUMERO.Name = "COMP_NUMERO";
            this.COMP_NUMERO.Visible = false;
            this.COMP_NUMERO.Width = 120;
            // 
            // PROD_CADASTRADO
            // 
            this.PROD_CADASTRADO.HeaderText = "Produto Cadastrado";
            this.PROD_CADASTRADO.Name = "PROD_CADASTRADO";
            this.PROD_CADASTRADO.Visible = false;
            // 
            // EST_INDICE
            // 
            this.EST_INDICE.DataPropertyName = "EST_INDICE";
            this.EST_INDICE.HeaderText = "Indice";
            this.EST_INDICE.Name = "EST_INDICE";
            this.EST_INDICE.Visible = false;
            // 
            // PROD_PRECOMPRA
            // 
            this.PROD_PRECOMPRA.DataPropertyName = "PROD_PRECOMPRA";
            dataGridViewCellStyle83.Format = "N2";
            dataGridViewCellStyle83.NullValue = null;
            this.PROD_PRECOMPRA.DefaultCellStyle = dataGridViewCellStyle83;
            this.PROD_PRECOMPRA.HeaderText = "Preço Compra";
            this.PROD_PRECOMPRA.Name = "PROD_PRECOMPRA";
            this.PROD_PRECOMPRA.Visible = false;
            // 
            // PROD_CEST
            // 
            this.PROD_CEST.DataPropertyName = "PROD_CEST";
            this.PROD_CEST.HeaderText = "CEST";
            this.PROD_CEST.Name = "PROD_CEST";
            this.PROD_CEST.Visible = false;
            // 
            // PROD_REGMS
            // 
            this.PROD_REGMS.DataPropertyName = "PROD_REGMS";
            this.PROD_REGMS.HeaderText = "Registro MS";
            this.PROD_REGMS.Name = "PROD_REGMS";
            this.PROD_REGMS.Width = 120;
            // 
            // btnIncluir
            // 
            this.btnIncluir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIncluir.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIncluir.Image = ((System.Drawing.Image)(resources.GetObject("btnIncluir.Image")));
            this.btnIncluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIncluir.Location = new System.Drawing.Point(818, 155);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(116, 33);
            this.btnIncluir.TabIndex = 278;
            this.btnIncluir.Text = "Incluir Itens";
            this.btnIncluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // txtEnqIpi
            // 
            this.txtEnqIpi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEnqIpi.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnqIpi.Location = new System.Drawing.Point(256, 127);
            this.txtEnqIpi.MaxLength = 10;
            this.txtEnqIpi.Name = "txtEnqIpi";
            this.txtEnqIpi.Size = new System.Drawing.Size(75, 22);
            this.txtEnqIpi.TabIndex = 269;
            this.txtEnqIpi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEnqIpi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnqIpi_KeyPress);
            // 
            // txtICMSRet
            // 
            this.txtICMSRet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtICMSRet.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtICMSRet.Location = new System.Drawing.Point(178, 127);
            this.txtICMSRet.MaxLength = 10;
            this.txtICMSRet.Name = "txtICMSRet";
            this.txtICMSRet.Size = new System.Drawing.Size(75, 22);
            this.txtICMSRet.TabIndex = 268;
            this.txtICMSRet.Text = "0,00";
            this.txtICMSRet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtICMSRet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtICMSRet_KeyPress);
            this.txtICMSRet.Validated += new System.EventHandler(this.txtICMSRet_Validated);
            // 
            // txtBcIcmsRet
            // 
            this.txtBcIcmsRet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBcIcmsRet.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBcIcmsRet.Location = new System.Drawing.Point(97, 127);
            this.txtBcIcmsRet.MaxLength = 10;
            this.txtBcIcmsRet.Name = "txtBcIcmsRet";
            this.txtBcIcmsRet.Size = new System.Drawing.Size(75, 22);
            this.txtBcIcmsRet.TabIndex = 267;
            this.txtBcIcmsRet.Text = "0,00";
            this.txtBcIcmsRet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBcIcmsRet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBcIcmsRet_KeyPress);
            this.txtBcIcmsRet.Validated += new System.EventHandler(this.txtBcIcmsRet_Validated);
            // 
            // txtICMSST
            // 
            this.txtICMSST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtICMSST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtICMSST.Location = new System.Drawing.Point(16, 127);
            this.txtICMSST.MaxLength = 10;
            this.txtICMSST.Name = "txtICMSST";
            this.txtICMSST.Size = new System.Drawing.Size(75, 22);
            this.txtICMSST.TabIndex = 265;
            this.txtICMSST.Text = "0,00";
            this.txtICMSST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtICMSST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtICMSST_KeyPress);
            this.txtICMSST.Validated += new System.EventHandler(this.txtPVIss_Validated);
            // 
            // txtBIcmsSt
            // 
            this.txtBIcmsSt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBIcmsSt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBIcmsSt.Location = new System.Drawing.Point(859, 86);
            this.txtBIcmsSt.MaxLength = 10;
            this.txtBIcmsSt.Name = "txtBIcmsSt";
            this.txtBIcmsSt.Size = new System.Drawing.Size(75, 22);
            this.txtBIcmsSt.TabIndex = 264;
            this.txtBIcmsSt.Text = "0,00";
            this.txtBIcmsSt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBIcmsSt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBIcmsSt_KeyPress);
            this.txtBIcmsSt.Validated += new System.EventHandler(this.txtBIcmsSt_Validated);
            // 
            // txtCstIcms
            // 
            this.txtCstIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCstIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCstIcms.Location = new System.Drawing.Point(774, 86);
            this.txtCstIcms.MaxLength = 10;
            this.txtCstIcms.Name = "txtCstIcms";
            this.txtCstIcms.Size = new System.Drawing.Size(75, 22);
            this.txtCstIcms.TabIndex = 263;
            this.txtCstIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCstIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCstIcms_KeyPress);
            // 
            // txtPVIcms
            // 
            this.txtPVIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVIcms.Location = new System.Drawing.Point(688, 86);
            this.txtPVIcms.MaxLength = 10;
            this.txtPVIcms.Name = "txtPVIcms";
            this.txtPVIcms.Size = new System.Drawing.Size(75, 22);
            this.txtPVIcms.TabIndex = 262;
            this.txtPVIcms.Text = "0,00";
            this.txtPVIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVIcms_KeyPress);
            this.txtPVIcms.Validated += new System.EventHandler(this.txtPVIcms_Validated);
            // 
            // txtVBIcms
            // 
            this.txtVBIcms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVBIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVBIcms.Location = new System.Drawing.Point(605, 86);
            this.txtVBIcms.MaxLength = 10;
            this.txtVBIcms.Name = "txtVBIcms";
            this.txtVBIcms.Size = new System.Drawing.Size(75, 22);
            this.txtVBIcms.TabIndex = 260;
            this.txtVBIcms.Text = "0,00";
            this.txtVBIcms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVBIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVBIcms_KeyPress);
            this.txtVBIcms.Validated += new System.EventHandler(this.txtVBIcms_Validated);
            // 
            // dtValidade
            // 
            this.dtValidade.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtValidade.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtValidade.Location = new System.Drawing.Point(859, 126);
            this.dtValidade.Name = "dtValidade";
            this.dtValidade.Size = new System.Drawing.Size(75, 21);
            this.dtValidade.TabIndex = 314;
            this.dtValidade.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtValidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtValidade_KeyPress);
            // 
            // cmbIcms
            // 
            this.cmbIcms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIcms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIcms.FormattingEnabled = true;
            this.cmbIcms.Items.AddRange(new object[] {
            "Tributada Integralmente",
            "Trib. e com cobrança Icms ST",
            "Red. Base de Cálculo",
            "Isenta/Não Trib. com cob. de Icms ST",
            "Isenta",
            "Não Tributada",
            "Com Suspensão",
            "Com Diferimento",
            "Icms cobrado anteriormente por ST",
            "Red. Base de Cálc. e cob. do Icms ST",
            "Outras"});
            this.cmbIcms.Location = new System.Drawing.Point(414, 84);
            this.cmbIcms.Name = "cmbIcms";
            this.cmbIcms.Size = new System.Drawing.Size(187, 24);
            this.cmbIcms.TabIndex = 259;
            this.cmbIcms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIcms_KeyPress);
            // 
            // txtLote
            // 
            this.txtLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLote.Location = new System.Drawing.Point(335, 86);
            this.txtLote.MaxLength = 10;
            this.txtLote.Name = "txtLote";
            this.txtLote.Size = new System.Drawing.Size(75, 22);
            this.txtLote.TabIndex = 258;
            this.txtLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLote_KeyPress);
            // 
            // txtPVenda
            // 
            this.txtPVenda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVenda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVenda.Location = new System.Drawing.Point(256, 86);
            this.txtPVenda.MaxLength = 10;
            this.txtPVenda.Name = "txtPVenda";
            this.txtPVenda.Size = new System.Drawing.Size(75, 22);
            this.txtPVenda.TabIndex = 257;
            this.txtPVenda.Text = "0,00";
            this.txtPVenda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVenda_KeyPress);
            this.txtPVenda.Validated += new System.EventHandler(this.txtPVenda_Validated);
            // 
            // txtPMargem
            // 
            this.txtPMargem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPMargem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPMargem.ForeColor = System.Drawing.Color.Black;
            this.txtPMargem.Location = new System.Drawing.Point(178, 86);
            this.txtPMargem.MaxLength = 10;
            this.txtPMargem.Name = "txtPMargem";
            this.txtPMargem.Size = new System.Drawing.Size(75, 22);
            this.txtPMargem.TabIndex = 256;
            this.txtPMargem.Text = "0,00";
            this.txtPMargem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPMargem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPMargem_KeyPress);
            this.txtPMargem.Validated += new System.EventHandler(this.txtPMargem_Validated);
            // 
            // txtPVTotal
            // 
            this.txtPVTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVTotal.Location = new System.Drawing.Point(97, 86);
            this.txtPVTotal.MaxLength = 10;
            this.txtPVTotal.Name = "txtPVTotal";
            this.txtPVTotal.Size = new System.Drawing.Size(75, 22);
            this.txtPVTotal.TabIndex = 255;
            this.txtPVTotal.Text = "0,00";
            this.txtPVTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVTotal_KeyPress);
            this.txtPVTotal.Validated += new System.EventHandler(this.txtPVTotal_Validated);
            // 
            // txtPDescUni
            // 
            this.txtPDescUni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDescUni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPDescUni.Location = new System.Drawing.Point(16, 86);
            this.txtPDescUni.MaxLength = 10;
            this.txtPDescUni.Name = "txtPDescUni";
            this.txtPDescUni.Size = new System.Drawing.Size(68, 22);
            this.txtPDescUni.TabIndex = 254;
            this.txtPDescUni.Text = "0,00";
            this.txtPDescUni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPDescUni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPDescUni_KeyPress);
            this.txtPDescUni.Validated += new System.EventHandler(this.txtPDescUni_Validated);
            // 
            // txtPDesc
            // 
            this.txtPDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPDesc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPDesc.Location = new System.Drawing.Point(877, 43);
            this.txtPDesc.MaxLength = 10;
            this.txtPDesc.Name = "txtPDesc";
            this.txtPDesc.Size = new System.Drawing.Size(57, 22);
            this.txtPDesc.TabIndex = 253;
            this.txtPDesc.Text = "0,00";
            this.txtPDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPDesc_KeyPress);
            this.txtPDesc.Validated += new System.EventHandler(this.txtPDesc_Validated);
            // 
            // txtVUni
            // 
            this.txtVUni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVUni.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVUni.Location = new System.Drawing.Point(790, 43);
            this.txtVUni.MaxLength = 10;
            this.txtVUni.Name = "txtVUni";
            this.txtVUni.Size = new System.Drawing.Size(81, 22);
            this.txtVUni.TabIndex = 252;
            this.txtVUni.Text = "0,00";
            this.txtVUni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVUni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVUni_KeyPress);
            this.txtVUni.Validated += new System.EventHandler(this.txtVUni_Validated);
            // 
            // txtEQtde
            // 
            this.txtEQtde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtEQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEQtde.Location = new System.Drawing.Point(699, 43);
            this.txtEQtde.MaxLength = 10;
            this.txtEQtde.Name = "txtEQtde";
            this.txtEQtde.ReadOnly = true;
            this.txtEQtde.Size = new System.Drawing.Size(85, 22);
            this.txtEQtde.TabIndex = 251;
            this.txtEQtde.Text = "0";
            this.txtEQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEQtde_KeyPress);
            // 
            // txtUn
            // 
            this.txtUn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUn.Location = new System.Drawing.Point(663, 43);
            this.txtUn.MaxLength = 10;
            this.txtUn.Name = "txtUn";
            this.txtUn.ReadOnly = true;
            this.txtUn.Size = new System.Drawing.Size(30, 22);
            this.txtUn.TabIndex = 250;
            this.txtUn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUn_KeyPress);
            // 
            // txtQtde
            // 
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(605, 43);
            this.txtQtde.MaxLength = 10;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(52, 22);
            this.txtQtde.TabIndex = 249;
            this.txtQtde.Text = "0";
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            this.txtQtde.Validated += new System.EventHandler(this.txtQtde_Validated);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Navy;
            this.label60.Location = new System.Drawing.Point(411, 111);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(60, 16);
            this.label60.TabIndex = 247;
            this.label60.Text = "Base IPI";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Navy;
            this.label61.Location = new System.Drawing.Point(175, 109);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(66, 16);
            this.label61.TabIndex = 246;
            this.label61.Text = "ICMS Ret";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Navy;
            this.label62.Location = new System.Drawing.Point(94, 109);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(77, 15);
            this.label62.TabIndex = 245;
            this.label62.Text = "BC ICMS Ret";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Navy;
            this.label64.Location = new System.Drawing.Point(13, 109);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(62, 16);
            this.label64.TabIndex = 243;
            this.label64.Text = "ICMS ST";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Navy;
            this.label65.Location = new System.Drawing.Point(856, 67);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(84, 16);
            this.label65.TabIndex = 242;
            this.label65.Text = "BC ICMS ST";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Navy;
            this.label66.Location = new System.Drawing.Point(771, 67);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(71, 16);
            this.label66.TabIndex = 241;
            this.label66.Text = "CST ICMS";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Navy;
            this.label67.Location = new System.Drawing.Point(685, 67);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(78, 16);
            this.label67.TabIndex = 240;
            this.label67.Text = "Valor ICMS";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Navy;
            this.label68.Location = new System.Drawing.Point(604, 67);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(84, 16);
            this.label68.TabIndex = 239;
            this.label68.Text = "Vl. BC ICMS";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Navy;
            this.label69.Location = new System.Drawing.Point(856, 108);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(64, 16);
            this.label69.TabIndex = 238;
            this.label69.Text = "Validade";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Navy;
            this.label58.Location = new System.Drawing.Point(411, 68);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 16);
            this.label58.TabIndex = 237;
            this.label58.Text = "ICMS";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Navy;
            this.label57.Location = new System.Drawing.Point(333, 66);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(36, 16);
            this.label57.TabIndex = 236;
            this.label57.Text = "Lote";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Navy;
            this.label56.Location = new System.Drawing.Point(253, 67);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(69, 16);
            this.label56.TabIndex = 235;
            this.label56.Text = "Vl. Venda";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Navy;
            this.label55.Location = new System.Drawing.Point(175, 67);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(60, 16);
            this.label55.TabIndex = 234;
            this.label55.Text = "Margem";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Navy;
            this.label54.Location = new System.Drawing.Point(94, 68);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(60, 16);
            this.label54.TabIndex = 233;
            this.label54.Text = "Vl. Total";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Navy;
            this.label53.Location = new System.Drawing.Point(13, 68);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(71, 16);
            this.label53.TabIndex = 232;
            this.label53.Text = "Desc. Uni.";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Navy;
            this.label52.Location = new System.Drawing.Point(874, 25);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(56, 16);
            this.label52.TabIndex = 231;
            this.label52.Text = "Desc. %";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Navy;
            this.label51.Location = new System.Drawing.Point(787, 26);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(58, 16);
            this.label51.TabIndex = 230;
            this.label51.Text = "Vl. Unit.";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Navy;
            this.label50.Location = new System.Drawing.Point(696, 25);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(88, 16);
            this.label50.TabIndex = 229;
            this.label50.Text = "Qtd. Estoque";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Navy;
            this.label49.Location = new System.Drawing.Point(660, 25);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(33, 16);
            this.label49.TabIndex = 228;
            this.label49.Text = "Uni.";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Navy;
            this.label48.Location = new System.Drawing.Point(602, 26);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(42, 16);
            this.label48.TabIndex = 227;
            this.label48.Text = "Qtde.";
            // 
            // txtComissao
            // 
            this.txtComissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComissao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComissao.Location = new System.Drawing.Point(516, 43);
            this.txtComissao.MaxLength = 10;
            this.txtComissao.Name = "txtComissao";
            this.txtComissao.Size = new System.Drawing.Size(85, 22);
            this.txtComissao.TabIndex = 224;
            this.txtComissao.Text = "0,00";
            this.txtComissao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtComissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComissao_KeyPress);
            this.txtComissao.Validated += new System.EventHandler(this.txtComissao_Validated);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Navy;
            this.label46.Location = new System.Drawing.Point(513, 25);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(83, 16);
            this.label46.TabIndex = 223;
            this.label46.Text = "Comissão %";
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(178, 43);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(331, 22);
            this.txtDescr.TabIndex = 222;
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            this.txtDescr.Validated += new System.EventHandler(this.txtDescr_Validated);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(173, 25);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(70, 16);
            this.label45.TabIndex = 221;
            this.label45.Text = "Descrição";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(59, 43);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(113, 22);
            this.txtCodBarras.TabIndex = 220;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarra_KeyPress);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(56, 25);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 16);
            this.label44.TabIndex = 219;
            this.label44.Text = "Produto";
            // 
            // txtItem
            // 
            this.txtItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItem.Location = new System.Drawing.Point(16, 43);
            this.txtItem.MaxLength = 10;
            this.txtItem.Name = "txtItem";
            this.txtItem.ReadOnly = true;
            this.txtItem.Size = new System.Drawing.Size(39, 22);
            this.txtItem.TabIndex = 218;
            this.txtItem.Text = "0";
            this.txtItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Navy;
            this.label43.Location = new System.Drawing.Point(13, 25);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(36, 16);
            this.label43.TabIndex = 217;
            this.label43.Text = "Item";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtPCMedio);
            this.groupBox8.Controls.Add(this.txtPUCusto);
            this.groupBox8.Controls.Add(this.txtPVCompra);
            this.groupBox8.Controls.Add(this.txtPEstoque);
            this.groupBox8.Controls.Add(this.label72);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Location = new System.Drawing.Point(335, 153);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(480, 40);
            this.groupBox8.TabIndex = 275;
            this.groupBox8.TabStop = false;
            // 
            // txtPCMedio
            // 
            this.txtPCMedio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPCMedio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPCMedio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCMedio.Location = new System.Drawing.Point(428, 12);
            this.txtPCMedio.MaxLength = 10;
            this.txtPCMedio.Name = "txtPCMedio";
            this.txtPCMedio.ReadOnly = true;
            this.txtPCMedio.Size = new System.Drawing.Size(45, 22);
            this.txtPCMedio.TabIndex = 273;
            this.txtPCMedio.Text = "0,00";
            this.txtPCMedio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPUCusto
            // 
            this.txtPUCusto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPUCusto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPUCusto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPUCusto.Location = new System.Drawing.Point(300, 12);
            this.txtPUCusto.MaxLength = 10;
            this.txtPUCusto.Name = "txtPUCusto";
            this.txtPUCusto.ReadOnly = true;
            this.txtPUCusto.Size = new System.Drawing.Size(45, 22);
            this.txtPUCusto.TabIndex = 272;
            this.txtPUCusto.Text = "0,00";
            this.txtPUCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPUCusto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPUCusto_KeyPress);
            // 
            // txtPVCompra
            // 
            this.txtPVCompra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPVCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPVCompra.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPVCompra.Location = new System.Drawing.Point(184, 11);
            this.txtPVCompra.MaxLength = 10;
            this.txtPVCompra.Name = "txtPVCompra";
            this.txtPVCompra.ReadOnly = true;
            this.txtPVCompra.Size = new System.Drawing.Size(45, 22);
            this.txtPVCompra.TabIndex = 271;
            this.txtPVCompra.Text = "0,00";
            this.txtPVCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPVCompra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPVCompra_KeyPress);
            // 
            // txtPEstoque
            // 
            this.txtPEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPEstoque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPEstoque.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPEstoque.Location = new System.Drawing.Point(63, 11);
            this.txtPEstoque.MaxLength = 10;
            this.txtPEstoque.Name = "txtPEstoque";
            this.txtPEstoque.ReadOnly = true;
            this.txtPEstoque.Size = new System.Drawing.Size(45, 22);
            this.txtPEstoque.TabIndex = 270;
            this.txtPEstoque.Text = "0";
            this.txtPEstoque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPEstoque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPEstoque_KeyPress);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Navy;
            this.label72.Location = new System.Drawing.Point(347, 15);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(82, 16);
            this.label72.TabIndex = 231;
            this.label72.Text = "Cus. Médio:";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Navy;
            this.label71.Location = new System.Drawing.Point(230, 15);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(72, 16);
            this.label71.TabIndex = 230;
            this.label71.Text = "Ult. Custo:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Navy;
            this.label70.Location = new System.Drawing.Point(112, 14);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(74, 16);
            this.label70.TabIndex = 229;
            this.label70.Text = "Vl. Comp.:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Navy;
            this.label59.Location = new System.Drawing.Point(4, 14);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(62, 16);
            this.label59.TabIndex = 228;
            this.label59.Text = "Estoque:";
            // 
            // frmEstEntradas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstEntradas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmEstEntradas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEstEntradas_Load);
            this.Shown += new System.EventHandler(this.frmEstEntradas_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcEntradas.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntradas)).EndInit();
            this.cmsEntradas.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlProdutos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutosNaoCadastrados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelas)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tpItens.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItens)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcEntradas;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgEntradas;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtLancamento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtEntID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDocto;
        private System.Windows.Forms.TabPage tpItens;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cmbIcms;
        private System.Windows.Forms.TextBox txtLote;
        private System.Windows.Forms.TextBox txtPVenda;
        private System.Windows.Forms.TextBox txtPMargem;
        private System.Windows.Forms.TextBox txtPVTotal;
        private System.Windows.Forms.TextBox txtPDescUni;
        private System.Windows.Forms.TextBox txtPDesc;
        private System.Windows.Forms.TextBox txtVUni;
        private System.Windows.Forms.TextBox txtEQtde;
        private System.Windows.Forms.TextBox txtUn;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtComissao;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtItem;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtICMSRet;
        private System.Windows.Forms.TextBox txtBcIcmsRet;
        private System.Windows.Forms.TextBox txtICMSST;
        private System.Windows.Forms.TextBox txtBIcmsSt;
        private System.Windows.Forms.TextBox txtCstIcms;
        private System.Windows.Forms.TextBox txtPVIcms;
        private System.Windows.Forms.TextBox txtVBIcms;
        private System.Windows.Forms.TextBox txtEnqIpi;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtPCMedio;
        private System.Windows.Forms.TextBox txtPUCusto;
        private System.Windows.Forms.TextBox txtPVCompra;
        private System.Windows.Forms.TextBox txtPEstoque;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtValorCofins;
        private System.Windows.Forms.TextBox txtTotalTributos;
        private System.Windows.Forms.TextBox txtVICMSUFDest;
        private System.Windows.Forms.TextBox txtValorIpi;
        private System.Windows.Forms.TextBox txtOutrasDespesas;
        private System.Windows.Forms.TextBox txtValorDesconto;
        private System.Windows.Forms.TextBox txtValorSeguro;
        private System.Windows.Forms.TextBox txtFrete;
        private System.Windows.Forms.TextBox txtTotalProdutos;
        private System.Windows.Forms.TextBox txtValorPIS;
        private System.Windows.Forms.TextBox txtValorFCP;
        private System.Windows.Forms.TextBox txtICMSUFRemet;
        private System.Windows.Forms.TextBox txtVImpImportacao;
        private System.Windows.Forms.TextBox txtVICMSST;
        private System.Windows.Forms.TextBox txtIIcms;
        private System.Windows.Forms.TextBox txtVIcms;
        private System.Windows.Forms.TextBox txtBIcms;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnItens;
        private System.Windows.Forms.TextBox txtObs;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgParcelas;
        private System.Windows.Forms.DateTimePicker dtPVencimento;
        private System.Windows.Forms.TextBox txtPTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.TextBox txtFCidade;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtFCnpj;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtFNome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFBusca;
        private System.Windows.Forms.ComboBox cmbFBusca;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNCompra;
        private System.Windows.Forms.ComboBox cmbComp;
        private System.Windows.Forms.TextBox txtComp;
        private System.Windows.Forms.ComboBox cmbNOper;
        private System.Windows.Forms.TextBox txtNTotal;
        private System.Windows.Forms.TextBox txtFatura;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.DateTimePicker dtEmissao;
        private System.Windows.Forms.DataGridView dgItens;
        private System.Windows.Forms.OpenFileDialog ofdXML;
        private System.Windows.Forms.MaskedTextBox txtBCnpj;
        private System.Windows.Forms.ComboBox cmbBComp;
        private System.Windows.Forms.ComboBox cmbBNOper;
        private System.Windows.Forms.TextBox txtBValor;
        private System.Windows.Forms.TextBox txtBFatura;
        private System.Windows.Forms.TextBox txtBSerie;
        private System.Windows.Forms.TextBox txtBNumero;
        private System.Windows.Forms.MaskedTextBox txtBEmissao;
        private System.Windows.Forms.ComboBox cmbBDocto;
        private System.Windows.Forms.MaskedTextBox txtBLancto;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ContextMenuStrip cmsEntradas;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VALPARC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VENCTO;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox txtGTotal;
        private System.Windows.Forms.TextBox txtGEqtde;
        private System.Windows.Forms.TextBox txtGQtde;
        private System.Windows.Forms.Button btnNfe;
        private System.Windows.Forms.TextBox txtGDesc;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.TextBox txtCfop;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox txtNCM;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ComboBox cmbOrigem;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox txtAliIpi;
        private System.Windows.Forms.TextBox txtBaseIpi;
        private System.Windows.Forms.TextBox txtCstIpi;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox txtPisCst;
        private System.Windows.Forms.TextBox txtPVIpi;
        private System.Windows.Forms.TextBox txtCstConfis;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.DateTimePicker dtValidade;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel pnlProdutos;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Button btnCancela;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridView dgProdutosNaoCadastrados;
        private System.Windows.Forms.TextBox txtCest;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAD_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAD_PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAD_PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAD_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAD_PRECO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CUSTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_SEQUENCIA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_QTDESTOQUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_UNITARIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESC_PORC;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DESCONTOITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_TOTITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_MARGEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRE_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_COMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALOR_PMC;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ESTATUAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CONTROLADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ALIQICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VALOR_BC_ICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VALORICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BC_ICMS_ST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ICMS_ST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_REDUCAOBASEICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ICMS_RET;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ENQ_IPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CST_IPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BASE_IPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ALIQIPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ORIGEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_NCM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CFOP;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CST_COFINS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VALOR_IPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_CST_PIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DTVALIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO_COMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn COMP_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CADASTRADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_INDICE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_PRECOMPRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CEST;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REGMS;
        private System.Windows.Forms.TextBox txtChaveAcesso;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DTLANC;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DTEMIS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_SERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_NO_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAG_COMPRADOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_OBS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BASEICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_IMPICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ISENTOICMS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_OUTRAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DIVERSAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BASEIPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_IMPIPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_ISENTOIPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_OUTIPI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BASESUBST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_SUBST;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_BASEISS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_IMPISS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_IRRF;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VFRETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_IMPFRETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_DESCONTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_VIA_XML;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHAVE_ACESSO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_HORA_EMISSAO;
    }
}