﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SNGPC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstRel : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Estoque");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();
  
        public frmEstRel(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void frmEstRel_Load(object sender, EventArgs e)
        {
            try
            {
                
                cbbTerapeutica.Items.Insert(0, "SEM CLASSE");
                cbbTerapeutica.Items.Insert(1,"ANTIMICROBIANOS");
                cbbTerapeutica.Items.Insert(2,"SUJEITO A CONTROLE ESPECIAL");
                cbbTerapeutica.SelectedIndex = 0;
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(120, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRel
                        >().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        //public void Limpar()
        //{
        //    filtroProd = "";
        //    chkZerados.Checked = false;
        //    dtData.Value = DateTime.Now;
        //    rdbPrecoVenda.Checked = true;
        //    rdbAlfabetica.Checked = true;
        //    rdbNenhum.Checked = true;
        //    gbTipoRelatorio.Enabled = false;
        //    chkDepartamento.Enabled = false;
        //    chkClasse.Enabled = false;
        //    chkSubclasse.Enabled = false;
        //    rdbLayout1.Enabled = false;
        //    dtData.Focus();
        //}

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var balanco = new Produto();
                int order = 0;
                if (rdbCodigo.Checked)
                    order = 0;
                else if (rdbDescricao.Checked)
                    order = 1;
                int terapeutica = cbbTerapeutica.SelectedIndex;
                if (rdbLayout1.Checked)
                {
                    dtRetorno = balanco.BuscaEstoqueAtual(Principal.empAtual, Principal.estAtual, terapeutica, order, chkZerados.Checked);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioEstoque relatorio = new frmRelatorioEstoque(this, dtRetorno);
                        relatorio.Text = "Relatório de Estoque";
                        relatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (rdbLayout2.Checked)
                {
                    dtRetorno = balanco.BuscaEstoqueSNGPC(terapeutica, order, chkZerados.Checked);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioEstoqueSngpc relatorio = new frmRelatorioEstoqueSngpc(this, dtRetorno);
                        relatorio.Text = "Relatório de Estoque";
                        relatorio.Show();
                    }else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if(rdbMovimentacao.Checked)
                {
                    var dados = new SNGPCRelatorios();
                    string dataInventario = dados.BuscaDataLancInventario();
                    
                    if(!String.IsNullOrEmpty(dataInventario))
                    {
                        dtRetorno = dados.BuscaEstoqueEntradaSaida(dataInventario, rdbDescricao.Checked ? 0 : 1);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            frmRelatorioMovimentos relatorio = new frmRelatorioMovimentos(this, dtRetorno);
                            relatorio.Text = "Relatório de Estoque";
                            relatorio.Show();
                        }
                        else
                        {
                            MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("SNGPC sem lançamento de inventário", "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rdbLayout1.Focus();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void rdbCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;

            }
        }

        private void rdbDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbLayout1_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbLayout2_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void cbbTerapeutica_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void chkZerados_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void btnPesquisar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmEstRel_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbDescricao.Focus();
        }

        private void rdbDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbLayout1.Focus();
        }

        private void rdbLayout1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbLayout2.Focus();
        }

        private void rdbLayout2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cbbTerapeutica.Focus();
        }

        private void cbbTerapeutica_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnPesquisar.PerformClick();
        }

        private void chkZerados_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnPesquisar.PerformClick();
        }

        public void Limpar()
        {
            rdbCodigo.Checked = true;
            cbbTerapeutica.SelectedIndex = -1;
            rdbLayout1.Checked = true;
            chkZerados.Checked = false;
            rdbCodigo.Focus();
        }
    }
    
}
