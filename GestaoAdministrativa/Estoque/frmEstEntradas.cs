﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Collections;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstEntradas : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private DataTable dtEntradas = new DataTable();
        private int contRegistros;
        private ToolStripButton tsbEntradas = new ToolStripButton("Entrada de Notas");
        private ToolStripSeparator tssEntradas = new ToolStripSeparator();
        private decimal valorTotal;
        private double wCusto;
        private double wMargem;
        private double wTot;
        private double wDif;
        private XmlNodeList xnResul;
        private XmlDocument xmlNFe = new XmlDocument();
        private bool emGrade;
        private string[,] dados = new string[32, 3];
        private string[,] logAtualiza = new string[6, 3];
        private int indiceGrid = 0;
        private string strOrdem;
        private bool decrescente;
        private double valorParcelas;
        private double totalIcms;
        private double totalIpi;
        private double valorItens;
        private double valorItensFatura;
        private string[] campos;
        private string retorno;
        private DataTable dtRetorno;
        private string caminhoXML;
        private bool possuiProdutosNaoCadastrados;
        private string entradaViaXml;
        private int qtdAnterior;
        private bool validaInsert;
        private bool validaAtualizacao;
        private string idNuvem;
        private string DataHoraEmissao;
        private string documentoXML;
        private string idNOTA;
        #endregion

        public frmEstEntradas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstEntradas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                FormularioFoco();
                txtBLancto.Focus();

                dgParcelas.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgParcelas.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool CarregaCombos()
        {
            var dtPesq = new DataTable();

            //CARREGA UNIDADES//
            dtPesq = Util.CarregarCombosPorEstabelecimento("TIP_CODIGO", "TIP_DESC_ABREV", "TIPO_DOCTO", false, "TIP_DESABILITADO = 'N'");
            if (dtPesq.Rows.Count == 0)
            {
                MessageBox.Show("Necessário cadastrar pelo menos um tipo de documento,\npara cadastrar uma entrada de documento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return false;
            }
            else
            {
                cmbDocto.DataSource = dtPesq;
                cmbDocto.DisplayMember = "TIP_DESC_ABREV";
                cmbDocto.ValueMember = "TIP_CODIGO";
                cmbDocto.SelectedText = "NF";

                dtPesq = Util.CarregarCombosPorEstabelecimento("TIP_CODIGO", "TIP_DESC_ABREV", "TIPO_DOCTO", false, "TIP_DESABILITADO = 'N'");

                cmbBDocto.DataSource = dtPesq;
                cmbBDocto.DisplayMember = "TIP_DESC_ABREV";
                cmbBDocto.ValueMember = "TIP_CODIGO";
                cmbBDocto.SelectedIndex = -1;
            }

            //CARREGA NATUREZA DE OPERAÇÃO//
            dtPesq = Util.CarregarCombosPorEmpresa("NO_CODIGO ||' /'||  NO_COMP ||' - '|| NO_DESCR AS NO_DESCR", "NO_CODIGO", "NAT_OPERACAO", true, "NO_DESABILITADO = 'N'");
            if (dtPesq.Rows.Count != 0)
            {
                cmbNOper.DataSource = dtPesq;
                cmbNOper.DisplayMember = "NO_DESCR";
                cmbNOper.ValueMember = "NO_CODIGO";
                cmbNOper.SelectedIndex = -1;

                dtPesq = Util.CarregarCombosPorEmpresa("NO_CODIGO ||' /'||  NO_COMP ||' - '|| NO_DESCR AS NO_DESCR", "NO_CODIGO", "NAT_OPERACAO", true, "NO_DESABILITADO = 'N'");
                cmbBNOper.DataSource = dtPesq;
                cmbBNOper.DisplayMember = "NO_DESCR";
                cmbBNOper.ValueMember = "NO_CODIGO";
                cmbBNOper.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Necessário cadastrar pelo menos uma Natureza de Operação,\npara cadastrar uma entrada de documento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return false;
            }

            //CARREGA COMPRADOR//
            dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_STATUS = 'C' AND COL_DESABILITADO = 'N'");
            if (dtPesq.Rows.Count != 0)
            {
                cmbComp.DataSource = dtPesq;
                cmbComp.DisplayMember = "COL_NOME";
                cmbComp.ValueMember = "COL_CODIGO";
                cmbComp.SelectedIndex = -1;

                dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_STATUS = 'C' AND COL_DESABILITADO = 'N'");
                cmbBComp.DataSource = dtPesq;
                cmbBComp.DisplayMember = "COL_NOME";
                cmbBComp.ValueMember = "COL_CODIGO";
                cmbBComp.SelectedIndex = -1;
            }
            else
            {
                MessageBox.Show("Necessário cadastrar pelo menos uma Comprador,\npara cadastrar uma entrada de documento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return false;
            }

            return true;
        }

        private void frmEstEntradas_Shown(object sender, EventArgs e)
        {
            CarregaCombos();

            CarregaSpool();
        }

        private void tcEntradas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEntradas.SelectedTab == tpGrade)
            {
                txtBLancto.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEntradas.SelectedTab == tpFicha)
            {

                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    Funcoes.BotoesCadastro("frmEstEntradas");
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    if (String.IsNullOrEmpty(txtDocto.Text))
                    {
                        Limpar();
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
                        dtLancamento.Focus();
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        cmbDocto.Text = "NF";
                        cmbNOper.SelectedIndex = 0;
                        cmbFBusca.SelectedIndex = 0;
                        dtLancamento.Value = DateTime.Now;
                        dtEmissao.Value = DateTime.Now;
                        dtValidade.Value = DateTime.Now;
                        dtPVencimento.Value = DateTime.Now;
                        emGrade = false;
                    }
                    else
                    {
                        txtDocto.Focus();
                    }
                }
            }
            else if (tcEntradas.SelectedTab == tpItens)
            {
                Funcoes.BotoesCadastro("frmEstEntradas");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    cmbDocto.Text = "NF";
                    cmbNOper.SelectedIndex = 0;
                    cmbFBusca.SelectedIndex = 0;
                    dtLancamento.Value = DateTime.Now;
                    dtEmissao.Value = DateTime.Now;
                    dtValidade.Value = DateTime.Now;
                    dtPVencimento.Value = DateTime.Now;
                    emGrade = false;
                }
                txtCodBarras.Focus();
            }
        }

        private void cmbFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFBusca.Focus();
        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                {
                    var buscaFornecedor = new Cliente();
                    List<Cliente> cliente = new List<Cliente>();
                    if (cmbFBusca.SelectedIndex == 0)
                    {
                        cliente = buscaFornecedor.BuscaDadosFornecedor(1, txtFBusca.Text.Trim().ToUpper(), 1);
                    }
                    else if (cmbFBusca.SelectedIndex == 1)
                    {
                        long cnpj = Convert.ToInt64(txtFBusca.Text);
                        cliente = buscaFornecedor.BuscaDadosFornecedor(2, String.Format(@"{0:00\.000\.000\/0000\-00}", cnpj), 1);
                    }
                    if (cliente.Count.Equals(1))
                    {
                        txtFNome.Text = cliente[0].CfNome;
                        txtFCnpj.Text = cliente[0].CfDocto;
                        txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                        cmbFBusca.SelectedIndex = 0;
                        txtObs.Focus();
                    }
                    else if (cliente.Count.Equals(0))
                    {
                        MessageBox.Show("Fornecedor não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbFBusca.SelectedIndex = 0;
                        txtFBusca.Text = "";
                        txtFBusca.Focus();
                    }
                    else
                    {
                        using (var t = new frmBuscaForn("fornecedor"))
                        {
                            for (int i = 0; i < cliente.Count; i++)
                            {
                                t.dgCliFor.Rows.Insert(t.dgCliFor.RowCount, new Object[] { cliente[i].CodigoConveniada, "0", cliente[i].CfCodigo, cliente[i].CfNome, cliente[i].CfApelido, cliente[i].CfObservacao,
                                    cliente[i].CfDocto, cliente[i].CfTelefone, cliente[i].CfEndereco, cliente[i].CfBairro, cliente[i].CfCidade, cliente[i].CfUF, cliente[i].CfId, "0", "0", "0", cliente[i].CfStatus, "0" });
                            }
                            t.ShowDialog();
                        }

                        if (!String.IsNullOrEmpty(Principal.nomeCliFor))
                        {
                            txtFNome.Text = Principal.nomeCliFor;
                            txtFCnpj.Text = Principal.cnpjCliFor;
                            txtFCidade.Text = Principal.cidadeCliFor;
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtObs.Focus();
                        }
                        else
                        {
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtFBusca.Focus();
                        }

                    }

                }
                else
                    txtObs.Focus();
            }
        }

        private void dtLancamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbDocto.Focus();
            }
        }

        private void cmbDocto_Validated(object sender, EventArgs e)
        {
            if (cmbDocto.SelectedIndex != -1)
            {
                retorno = Util.SelecionaCampoEspecificoDaTabela("TIPO_DOCTO", "TIP_FATURA", "TIP_CODIGO", Math.Round(Convert.ToDecimal(cmbDocto.SelectedValue), 0).ToString());
                if (!String.IsNullOrEmpty(retorno))
                {
                    if (retorno == "S")
                    {
                        txtFatura.Enabled = true;
                    }
                    else
                    {
                        txtFatura.Text = "";
                        txtFatura.Enabled = false;
                    }
                }
            }
        }
        #region formatação numeros 
        // permitir apenas numeros
        private void cmbDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtEmissao.Focus();
        }

        private void dtEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                DataHoraEmissao = Convert.ToDateTime(dtEmissao.Text).ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("hh:MM:ss");
                txtDocto.Focus();
            }
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtSerie.Focus();
        }

        private void txtSerie_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtFatura.Enabled == true)
                {
                    txtFatura.Focus();
                }
                else
                    txtNTotal.Focus();
            }
        }

        private void txtFatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtNTotal.Focus();
        }

        private void txtNTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbNOper.Focus();
        }

        private void txtComp_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbComp.Focus();
        }

        private void txtComp_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbComp, txtComp);
        }

        private void cmbComp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNCompra.Focus();
        }

        private void cmbNOper_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtComp.Focus();
        }

        private void cmbComp_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtComp.Text = Convert.ToString(cmbComp.SelectedValue);
            if (!txtComp.Text.Equals("System.Data.DataRowView") && !String.IsNullOrEmpty(txtComp.Text))
            {
                txtComp.Text = Math.Round(Convert.ToDecimal(txtComp.Text), 0).ToString();
            }
        }

        private void txtNCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbFBusca.Focus();
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtChaveAcesso.Focus();
        }

        private void txtBIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVIcms.Focus();
        }

        private void txtVIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtIIcms.Focus();
        }

        private void txtIIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVICMSST.Focus();
        }

        private void txtVICMSST_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVImpImportacao.Focus();
        }

        private void txtVImpImportacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtICMSUFRemet.Focus();
        }

        private void txtICMSUFRemet_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorFCP.Focus();
        }

        private void txtValorFCP_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorPIS.Focus();
        }

        private void txtValorPIS_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtTotalProdutos.Focus();
        }

        private void txtTotalProdutos_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtFrete.Focus();
        }

        private void txtFrete_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorSeguro.Focus();
        }

        private void txtValorSeguro_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorDesconto.Focus();
        }

        private void txtValorDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtOutrasDespesas.Focus();
        }

        private void txtOutrasDespesas_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorIpi.Focus();
        }

        private void txtValorIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVICMSUFDest.Focus();
        }

        private void txtVICMSUFDest_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtTotalTributos.Focus();
        }

        private void txtTotalTributos_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtValorCofins.Focus();
        }

        private void txtValorCofins_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPTotal.Focus();
        }

        private void txtPTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                dtPVencimento.Focus();
        }

        private void dtPVencimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (ConsisteCamposNota().Equals(true))
                {
                    if ((Convert.ToDecimal(txtPTotal.Text) <= 0))
                    {
                        Principal.mensagem = "Valor total da nota não pode ser zero.";
                        Funcoes.Avisa();
                        txtNTotal.Focus();
                    }
                    else
                    {
                        valorTotal = 0;
                        foreach (DataGridViewRow col in dgParcelas.Rows)
                        {
                            valorTotal = valorTotal + Convert.ToDecimal(col.Cells[0].Value);
                        }

                        dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { Convert.ToDecimal(txtPTotal.Text).ToString("##0.00"), dtPVencimento.Value.ToString("dd/MM/yyyy") });
                        txtPTotal.Text = "0,00";
                        txtPTotal.Focus();

                    }
                }
            }
        }

        #endregion

        public bool ConsisteCamposNota()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbDocto.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de documento.";
                Funcoes.Avisa();
                cmbDocto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDocto.Text.Trim()))
            {
                Principal.mensagem = "Número do documento não pode estar em branco.";
                Funcoes.Avisa();
                txtDocto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtSerie.Text.Trim()))
            {
                Principal.mensagem = "Série não pode estar em branco.";
                Funcoes.Avisa();
                txtSerie.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFatura.Text.Trim()) && txtFatura.Enabled == true)
            {
                Principal.mensagem = "N° da fatura não pode estar em branco.";
                Funcoes.Avisa();
                txtFatura.Focus();
                return false;
            }
            if ((Convert.ToDouble(txtNTotal.Text) <= 0))
            {
                Principal.mensagem = "Valor total da nota não pode ser zero.";
                Funcoes.Avisa();
                txtNTotal.Focus();
                return false;
            }
            if (Equals(cmbNOper.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione a natureza de operação.";
                Funcoes.Avisa();
                cmbNOper.Focus();
                return false;
            }
            if (Equals(cmbComp.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um comprador.";
                Funcoes.Avisa();
                cmbComp.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtFCnpj.Text.Trim()))
            {
                Principal.mensagem = "CNPJ do fornecedor não pode ser em branco.";
                Funcoes.Avisa();
                txtFBusca.Focus();
                return false;
            }
            return true;
        }

        private void btnItens_Click(object sender, EventArgs e)
        {
            if (ConsisteCamposNota().Equals(true))
            {
                valorTotal = 0;
                foreach (DataGridViewRow col in dgParcelas.Rows)
                {
                    valorTotal = valorTotal + Convert.ToDecimal(col.Cells[0].Value);
                }

                if (valorTotal == Convert.ToDecimal(txtNTotal.Text))
                {
                    InserirSpoolNF();
                    InserirSpoolParcelas();
                    tcEntradas.SelectedTab = tpItens;
                    txtCodBarras.Focus();
                }
                else if (dgParcelas.RowCount == 0)
                {
                    MessageBox.Show("Valor das parcelas não pode ser em branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPTotal.Focus();
                }
                else
                {
                    MessageBox.Show("Valor das parcelas está diferente do valor total da nota", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPTotal.Focus();
                }
            }
        }

        private void txtCodBarra_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            LimparItens();
            var dtLePrdutos = new DataTable();
            Cursor = Cursors.WaitCursor;

            var buscaProduto = new Produto();
            dtLePrdutos = buscaProduto.BuscaProdutosEntradaItens(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
            if (dtLePrdutos.Rows.Count != 0)
            {
                txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                txtUn.Text = dtLePrdutos.Rows[0]["PROD_UNIDADE"].ToString().ToUpper();
                txtNCM.Text = dtLePrdutos.Rows[0]["NCM"].ToString().ToUpper();
                txtPVenda.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PRE_VALOR"]);
                txtPEstoque.Text = dtLePrdutos.Rows[0]["PROD_ESTATUAL"].ToString() == "" ? "0" : dtLePrdutos.Rows[0]["PROD_ESTATUAL"].ToString();
                txtPVCompra.Text = dtLePrdutos.Rows[0]["PROD_PRECOMPRA"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_PRECOMPRA"]);
                txtPUCusto.Text = dtLePrdutos.Rows[0]["PROD_ULTCUSME"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ULTCUSME"]);
                txtPCMedio.Text = dtLePrdutos.Rows[0]["PROD_CUSME"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_CUSME"]);
                txtVUni.Text = dtLePrdutos.Rows[0]["PROD_ULTCUSME"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ULTCUSME"]);
                txtPMargem.Text = dtLePrdutos.Rows[0]["PROD_MARGEM"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_MARGEM"]);
                txtComissao.Text = dtLePrdutos.Rows[0]["PROD_COMISSAO"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_COMISSAO"]);
                txtEQtde.Text = dtLePrdutos.Rows[0]["PROD_POPULAR"].ToString() == "S" ? "1" : dtLePrdutos.Rows[0]["PROD_QTDE_UN_COMP"].ToString() == "" ? "1" : Math.Round(Convert.ToDecimal(dtLePrdutos.Rows[0]["PROD_QTDE_UN_COMP"]), 0).ToString();

                CarregaIcms(dtLePrdutos.Rows[0]["PROD_ICMS"].ToString());
                txtVBIcms.Text = dtLePrdutos.Rows[0]["PROD_VALOR_BC_ICMS"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_VALOR_BC_ICMS"]);
                txtPVIcms.Text = dtLePrdutos.Rows[0]["PROD_VALOR_ICMS"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_VALOR_ICMS"]);
                txtCstIcms.Text = dtLePrdutos.Rows[0]["PROD_CST"].ToString();
                txtBIcmsSt.Text = dtLePrdutos.Rows[0]["PROD_BC_ICMS_ST"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_BC_ICMS_ST"]);
                txtICMSST.Text = dtLePrdutos.Rows[0]["PROD_VALOR_ICMS_ST"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_VALOR_ICMS_ST"]);
                txtBcIcmsRet.Text = dtLePrdutos.Rows[0]["PROD_BC_ICMS_RET"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_BC_ICMS_RET"]);
                txtICMSRet.Text = dtLePrdutos.Rows[0]["PROD_ICMS_RET"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ICMS_RET"]);
                txtEnqIpi.Text = dtLePrdutos.Rows[0]["PROD_ENQ_IPI"].ToString();
                txtCstIpi.Text = dtLePrdutos.Rows[0]["PROD_CST_IPI"].ToString();
                txtBaseIpi.Text = dtLePrdutos.Rows[0]["PROD_BASE_IPI"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_BASE_IPI"]);
                txtAliIpi.Text = dtLePrdutos.Rows[0]["PROD_ALIQ_IPI"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ALIQ_IPI"]);
                if (!String.IsNullOrEmpty(dtLePrdutos.Rows[0]["PROD_ORIGEM"].ToString()))
                {
                    CarregaOrigem(Convert.ToInt32(dtLePrdutos.Rows[0]["PROD_ORIGEM"]));
                }
                txtCfop.Text = dtLePrdutos.Rows[0]["PROD_CFOP"].ToString();
                txtCstConfis.Text = dtLePrdutos.Rows[0]["PROD_CST_COFINS"].ToString();
                txtPVIpi.Text = dtLePrdutos.Rows[0]["PROD_VALOR_IPI"].ToString() == "" ? "0.00" : String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_VALOR_IPI"]);
                txtPisCst.Text = dtLePrdutos.Rows[0]["PROD_CST_PIS"].ToString();
                txtCest.Text = dtLePrdutos.Rows[0]["PROD_CEST"].ToString();

                //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                {
                    MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodBarras.Focus();
                    return false;
                }

                if (Funcoes.LeParametro(9, "37", true).Equals("S"))
                {
                    txtComissao.Enabled = true;
                    txtComissao.Focus();
                }
                else
                {
                    txtComissao.Enabled = false;
                    txtComissao.Text = "0,00";
                    txtQtde.Focus();
                }

                txtItem.Text = Convert.ToString(dgItens.Rows.Count + 1);
            }
            else
            {
                Cursor = Cursors.Default;
                MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescr.Text = "Produto não cadastrado";
                txtCodBarras.Focus();
                return false;
            }

            Cursor = Cursors.Default;
            return true;
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Funcoes.LeParametro(9, "37", true).Equals("S"))
                {
                    txtComissao.Enabled = true;
                    txtComissao.Focus();
                }
                else
                {
                    txtComissao.Enabled = false;
                    txtQtde.Focus();
                }
            }
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtUn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVUni.Focus();

        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtVUni.Focus();
        }

        private void txtEQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVUni.Focus();
        }

        private void txtVUni_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtVUni.Text.Trim() != "")
                {
                    txtVUni.Text = String.Format("{0:N}", Convert.ToDecimal(txtVUni.Text));
                    txtPDescUni.Text = String.Format("{0:N}", (Convert.ToDecimal(txtVUni.Text) / 100) * Convert.ToDecimal(txtPDesc.Text));
                    txtPVTotal.Text = String.Format("{0:N}", (Convert.ToDecimal(txtVUni.Text) - Convert.ToDecimal(txtPDescUni.Text)) * Convert.ToInt32(txtEQtde.Text));
                }
                else
                    txtVUni.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtVUni_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPDesc.Focus();
        }

        private void txtPDesc_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPDesc.Text.Trim() != "")
                {
                    txtPDesc.Text = String.Format("{0:N}", Convert.ToDouble(txtPDesc.Text));
                    if (Convert.ToDouble(txtPDesc.Text) > 100)
                    {
                        MessageBox.Show("Desconto não pode ser maior que 100%", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtPDesc.Focus();
                    }
                    else if (Convert.ToDecimal(txtPDesc.Text) > 0)
                    {
                        txtPDescUni.Text = String.Format("{0:N}", (Convert.ToDouble(txtVUni.Text) / 100) * Convert.ToDouble(txtPDesc.Text));
                        txtPVTotal.Text = String.Format("{0:N}", (Convert.ToDouble(txtVUni.Text) - Convert.ToDouble(txtPDescUni.Text)) * Convert.ToInt32(txtEQtde.Text));
                    }
                }
                else
                    txtPDesc.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPDescUni.Focus();
        }

        private void txtPDescUni_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPDescUni.Text.Trim() != "")
                {
                    txtPDescUni.Text = String.Format("{0:N}", Convert.ToDecimal(txtPDescUni.Text));
                    if (Convert.ToDecimal(txtPDescUni.Text) > Convert.ToDecimal(txtVUni.Text))
                    {
                        MessageBox.Show("ATENÇÃO!! Desconto maior que valor unitário", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtPDescUni.Text = "0,00";
                        txtPDescUni.Focus();
                    }
                    else
                    {
                        txtPVTotal.Text = String.Format("{0:N}", (Convert.ToDouble(txtVUni.Text) - Convert.ToDouble(txtPDescUni.Text)) * Convert.ToDouble(txtEQtde.Text));

                        if (Convert.ToDecimal(txtVUni.Text) == 0)
                        {
                            txtPDesc.Text = "0,00";
                        }
                        else
                        {
                            txtPDesc.Text = String.Format("{0:N}", (Convert.ToDouble(txtPDescUni.Text) * 100) / Convert.ToDouble(txtVUni.Text));
                        }
                    }
                }
                else
                    txtPDescUni.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPDescUni_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPVTotal.Focus();
        }

        private void txtComissao_Validated(object sender, EventArgs e)
        {
            if (txtComissao.Text.Trim() != "")
            {
                txtComissao.Text = String.Format("{0:N}", Convert.ToDouble(txtComissao.Text));
            }
            else
                txtComissao.Text = "0,00";
        }

        private void txtComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtPVTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPMargem.Focus();
        }

        private void txtPVTotal_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPVTotal.Text.Trim() != "")
                {
                    txtPVTotal.Text = String.Format("{0:N}", Convert.ToDecimal(txtPVTotal.Text));

                    wTot = Convert.ToInt32(txtEQtde.Text) * Convert.ToDouble(txtVUni.Text);
                    wDif = wTot - Convert.ToDouble(txtPVTotal.Text);
                    if (wTot > 0 && wDif > 0)
                    {
                        txtPDesc.Text = String.Format("{0:N}", Math.Round((wDif / wTot) * 100, 2));

                        txtPDescUni.Text = String.Format("{0:N}", (Convert.ToDecimal(txtVUni.Text) / 100) * Convert.ToDecimal(txtPDesc.Text));
                    }
                    else
                    {
                        txtPDesc.Text = "0,00";
                        txtPDescUni.Text = "0,00";
                    }

                    if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                    {
                        var dadosProduto = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = txtCodBarras.Text.Trim(),
                        };

                        dtRetorno = dadosProduto.BuscaPrecoCompraOuUltimoCusto(dadosProduto);
                        double xPrecoCompra = 0;
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["PROD_PRECOMPRA"].ToString()))
                        {
                            if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_PRECOMPRA"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_PRECOMPRA"]));
                            }
                            else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                            }
                        }
                        else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                        {
                            xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                        }

                        txtPMargem.Text = String.Format("{0:N}", Convert.ToDouble(String.Format("{0:0.00}", ((Convert.ToDouble(txtPVenda.Text) / xPrecoCompra) - 1) * 100)));
                    }
                    else
                    {
                        double wLucro = 0;
                        wCusto = Convert.ToDouble(txtPVTotal.Text) / Convert.ToInt32(txtEQtde.Text);
                        if (wCusto != 0)
                        {
                            wLucro = Convert.ToDouble(txtPVenda.Text) - wCusto;
                            wMargem = (wLucro / wCusto) * 100;
                            txtPMargem.Text = String.Format("{0:N}", wMargem);
                        }
                    }

                }
                else
                    txtPVTotal.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPMargem_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtPMargem.Text.Trim() != "")
                {
                    txtPMargem.Text = String.Format("{0:N}", Convert.ToDecimal(txtPMargem.Text));

                    if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                    {
                        var dadosProduto = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = txtCodBarras.Text.Trim(),
                        };

                        dtRetorno = dadosProduto.BuscaPrecoCompraOuUltimoCusto(dadosProduto);
                        double xPrecoCompra = 0;
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["PROD_PRECOMPRA"].ToString()))
                        {
                            if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_PRECOMPRA"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_PRECOMPRA"]));
                            }
                            else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                            }
                        }
                        else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                        {
                            xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                        }

                        txtPVenda.Text = String.Format("{0:N}", xPrecoCompra + ((Convert.ToDouble(txtPMargem.Text) / 100) * xPrecoCompra));

                    }
                    else
                    {
                        if (Convert.ToInt32(txtEQtde.Text) != 0)
                        {
                            wCusto = Convert.ToDouble(txtPVTotal.Text) / Convert.ToInt32(txtEQtde.Text);
                            if (wCusto != 0)
                            {
                                txtPVenda.Text = String.Format("{0:N}", wCusto + ((Convert.ToDouble(txtPMargem.Text) / 100) * wCusto));
                            }
                        }
                    }

                    if (Convert.ToDecimal(txtPMargem.Text) < 0)
                    {
                        txtPMargem.ForeColor = Color.Red;
                    }
                    else
                        txtPMargem.ForeColor = Color.Black;
                }
                else
                    txtPMargem.Text = "0,00";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPMargem_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPVenda.Focus();
        }


        private void txtPVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtLote.Focus();
            }
        }

        private void cmbIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtVBIcms.Focus();
        }

        private void dtValidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCstConfis.Focus();
        }

        private void txtVBIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPVIcms.Focus();
        }

        private void txtPVIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCstIcms.Focus();
        }

        private void txtPVIcms_Validated(object sender, EventArgs e)
        {
            if (txtPVIcms.Text.Trim() != "")
            {
                txtPVIcms.Text = String.Format("{0:N}", Convert.ToDecimal(txtPVIcms.Text));
            }
            else
                txtPVIcms.Text = "0,00";
        }

        private void txtBIcmsSt_Validated(object sender, EventArgs e)
        {
            if (txtBIcmsSt.Text.Trim() != "")
            {
                txtBIcmsSt.Text = String.Format("{0:N}", Convert.ToDecimal(txtBIcmsSt.Text));
            }
            else
                txtBIcmsSt.Text = "0,00";
        }

        private void txtBIcmsSt_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtICMSST.Focus();
        }

        private void txtICMSST_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtBcIcmsRet.Focus();
        }

        private void cmbPNOper_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBcIcmsRet.Focus();
        }

        private void txtBcIcmsRet_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtICMSRet.Focus();
        }

        private void txtICMSRet_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtEnqIpi.Focus();
        }

        private void txtICMSRet_Validated(object sender, EventArgs e)
        {
            if (txtICMSRet.Text.Trim() != "")
            {
                txtICMSRet.Text = String.Format("{0:N}", Convert.ToDecimal(txtICMSRet.Text));
            }
            else
                txtICMSRet.Text = "0,00";
        }

        private void txtEnqIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCstIpi.Focus();
        }

        private void txtNCM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCfop.Focus();
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbIcms.Focus();
        }

        private void txtPEstoque_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPVCompra.Focus();
        }

        private void txtPVCompra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPUCusto.Focus();
        }

        private void txtPUCusto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPCMedio.Focus();
        }

        private void txtPVIss_Validated(object sender, EventArgs e)
        {
            if (txtICMSST.Text.Trim() != "")
            {
                txtICMSST.Text = String.Format("{0:N}", Convert.ToDecimal(txtICMSST.Text));
            }
            else
                txtICMSST.Text = "0,00";
        }

        private void txtBcIcmsRet_Validated(object sender, EventArgs e)
        {
            if (txtBcIcmsRet.Text.Trim() != "")
            {
                txtBcIcmsRet.Text = String.Format("{0:N}", Convert.ToDecimal(txtBcIcmsRet.Text));
            }
            else
                txtBcIcmsRet.Text = "0,00";
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (entradaViaXml.Equals("N") || Funcoes.LeParametro(9, "47", false).Equals("S"))
                {
                    string icms = "";
                    icms = IdentificaTributoICMS();

                    campos = cmbNOper.Text.Split('-');
                    campos = campos[0].Split('/');

                    if (ConsisteCamposNota().Equals(true) && ConsisteCamposItens().Equals(true))
                    {
                        var dadosProduto = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = txtCodBarras.Text.Trim(),
                        };

                        dtRetorno = dadosProduto.BuscaPrecoCompraOuUltimoCusto(dadosProduto);
                        double xPrecoCompra = 0;
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["PROD_PRECOMPRA"].ToString()))
                        {
                            if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_PRECOMPRA"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_PRECOMPRA"]));
                            }
                            else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                            }
                        }
                        else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                        {
                            xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                        }

                        var produtos = new Produto();
                        dtRetorno = produtos.BuscaComissaoQtdeEstoqueRegistroMS(txtCodBarras.Text.Trim(), Principal.estAtual, Principal.empAtual);

                        dgItens.Rows.Insert(dgItens.RowCount, new Object[] { txtItem.Text.Trim(), txtCodBarras.Text.Trim(), txtDescr.Text.Trim().ToUpper(), txtComissao.Text,
                        txtQtde.Text, txtUn.Text, txtEQtde.Text,txtVUni.Text, txtPDescUni.Text, txtPDesc.Text, txtPVTotal.Text, txtPMargem.Text, txtPVenda.Text,
                        txtComissao.Text, xPrecoCompra, dtRetorno.Rows[0]["PROD_ESTATUAL"],
                        txtLote.Text, icms, 0,txtVBIcms.Text, txtPVIcms.Text, txtCstIcms.Text, txtBIcmsSt.Text, txtICMSST.Text, txtBcIcmsRet.Text,
                        txtICMSRet.Text, txtEnqIpi.Text, txtCstIpi.Text, txtBaseIpi.Text, txtAliIpi.Text, cmbOrigem.SelectedIndex == -1 ? 0 :cmbOrigem.SelectedIndex, txtNCM.Text,
                        txtCfop.Text,txtCstConfis.Text, txtPVIpi.Text, txtPisCst.Text, dtValidade.Value.ToString("dd/MM/yyyy"), campos[0],campos[1],txtNCompra.Text, "N", 0,dtRetorno.Rows[0]["PROD_PRECOMPRA"],
                        txtCest.Text, dtRetorno.Rows[0]["PROD_REGISTRO_MS"] });

                        totalIcms = totalIcms + (txtPVIcms.Text == "" ? 0 : Convert.ToDouble(txtPVIcms.Text));
                        totalIpi = totalIpi + (txtPVIpi.Text == "" ? 0 : Convert.ToDouble(txtPVIpi.Text));
                        valorItens = valorItens + (txtPVTotal.Text == "" ? 0 : Convert.ToDouble(txtPVTotal.Text));

                        var natOperacaoFatura = new NatOperacao();
                        if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                        {
                            valorItensFatura = valorItensFatura + Convert.ToDouble(txtPVTotal.Text) + Convert.ToDouble(txtPVIpi.Text);
                        }

                        InserirSpoolNF();
                        InserirSpoolParcelas();


                        InserirSpoolItens(dgItens.Rows.Count - 1);

                        LimparItens();

                        TotalizaGrid();
                        txtItem.Text = "0";
                        txtCodBarras.Text = "";
                        txtDescr.Text = "";

                    }
                }
                else
                    MessageBox.Show("ENTRADA VIA XML INCLUSÃO DE NOVOS PRODUTOS NÃO LIBERADA!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string IdentificaTributoICMS()
        {
            if (cmbIcms.SelectedIndex == 0)
            {
                return "00";
            }
            else if (cmbIcms.SelectedIndex == 1)
            {
                return "10";
            }
            else if (cmbIcms.SelectedIndex == 2)
            {
                return "20";
            }
            else if (cmbIcms.SelectedIndex == 3)
            {
                return "30";
            }
            else if (cmbIcms.SelectedIndex == 4)
            {
                return "40";
            }
            else if (cmbIcms.SelectedIndex == 5)
            {
                return "41";
            }
            else if (cmbIcms.SelectedIndex == 6)
            {
                return "50";
            }
            else if (cmbIcms.SelectedIndex == 7)
            {
                return "51";
            }
            else if (cmbIcms.SelectedIndex == 8)
            {
                return "60";
            }
            else if (cmbIcms.SelectedIndex == 9)
            {
                return "70";
            }
            else if (cmbIcms.SelectedIndex == 10)
            {
                return "90";
            }

            return string.Empty;
        }

        public bool ConsisteCamposItens()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()) && String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Produto não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if ((Convert.ToInt32(txtQtde.Text) == 0))
            {
                Principal.mensagem = "Informe a quantidade do produto conforme unidade do documento.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if ((Convert.ToDouble(txtPVTotal.Text) <= 0))
            {
                Principal.mensagem = "Valor total do item não pode ser zero.";
                Funcoes.Avisa();
                txtPVTotal.Focus();
                return false;
            }
            if ((Convert.ToInt32(txtEQtde.Text) == 0))
            {
                Principal.mensagem = "Informe a quantidade do produto conforme unidade do estoque.";
                Funcoes.Avisa();
                txtEQtde.Focus();
                return false;
            }
            if ((Convert.ToDouble(txtVUni.Text) <= 0))
            {
                Principal.mensagem = "Valor unitário do produto da não pode ser zero.";
                Funcoes.Avisa();
                txtVUni.Focus();
                return false;
            }
            if (Equals(cmbIcms.SelectedIndex, -1))
            {
                Principal.mensagem = "Informe o cálculo do ICMS.";
                Funcoes.Avisa();
                cmbIcms.Focus();
                return false;
            }
            return true;
        }



        private void dgItens_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                double desconto;
                Cursor = Cursors.WaitCursor;
                switch (dgItens.Columns[e.ColumnIndex].Name)
                {
                    case "QTDE":
                        dgItens.Rows[e.RowIndex].Cells[6].Value = dgItens.Rows[e.RowIndex].Cells["QTDE"].Value;
                        desconto = (qtdAnterior * Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value)) / Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                        dgItens.Rows[e.RowIndex].Cells[9].Value = String.Format("{0:N}", desconto);
                        dgItens.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * 100)
                            / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value));
                        dgItens.Rows[e.RowIndex].Cells[10].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value)
                            * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value))
                            - (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)));
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "ENT_QTDESTOQUE":
                        desconto = (qtdAnterior * Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value)) / Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                        dgItens.Rows[e.RowIndex].Cells[9].Value = String.Format("{0:N}", desconto);
                        dgItens.Rows[e.RowIndex].Cells[10].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value) *
                            Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)) - (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)));
                        dgItens.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * 100) / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value));
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "ENT_UNITARIO":
                        dgItens.Rows[e.RowIndex].Cells[7].Value = String.Format("{0:N}", Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value));
                        dgItens.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * 100) / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value));
                        dgItens.Rows[e.RowIndex].Cells[10].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value))
                            - (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)));
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "DESC_PORC":
                        dgItens.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["DESC_PORC"].Value));
                        dgItens.Rows[e.RowIndex].Cells[9].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value) * Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["DESC_PORC"].Value)) / 100);
                        dgItens.Rows[e.RowIndex].Cells[10].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value))
                            - (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)));
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "ENT_DESCONTOITEM":
                        dgItens.Rows[e.RowIndex].Cells[9].Value = String.Format("{0:N}", Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value));
                        dgItens.Rows[e.RowIndex].Cells[8].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * 100)
                            / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value));
                        dgItens.Rows[e.RowIndex].Cells[10].Value = String.Format("{0:N}", (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value))
                            - (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) * Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value)));

                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "PROD_MARGEM":
                        dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value = String.Format("{0:N}", Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value));
                        if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                        {
                            if (Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value) != 0)
                            {
                                if (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_PRECOMPRA"].Value) > Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value))
                                {
                                    wCusto = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value);
                                }
                                else
                                {
                                    wCusto = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_PRECOMPRA"].Value);
                                }
                                if (wCusto.Equals(0))
                                {
                                    wCusto = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value);
                                }
                            }
                            dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value = String.Format("{0:N}", wCusto + ((Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value) / 100) * wCusto));
                        }
                        else
                        {
                            wCusto = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_TOTITEM"].Value) / Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                            dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value = String.Format("{0:N}", wCusto + ((Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value) / 100) * wCusto));
                        }
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "PRE_VALOR":
                        dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value = String.Format("{0:N}", Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value));
                        if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                        {
                            if (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_PRECOMPRA"].Value) > Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value))
                            {
                                wMargem = (((Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value) / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value)) - 1) * 100);
                            }
                            else
                            {
                                wMargem = (((Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value) / Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_PRECOMPRA"].Value)) - 1) * 100);
                            }
                            dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value = String.Format("{0:N}", wMargem);
                        }
                        else
                        {
                            wCusto = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["ENT_TOTITEM"].Value) / Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                            if (wCusto != 0)
                            {
                                double wlucro;
                                wlucro = Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PRE_VALOR"].Value) - wCusto;
                                wMargem = wlucro / wCusto * 100;
                                dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value = String.Format("{0:N}", wMargem);
                            }
                        }
                        InserirSpoolItens(e.RowIndex);
                        break;
                    case "ENT_DTVALIDADE":
                        InserirSpoolItens(e.RowIndex);
                        break;
                }

                totalIcms = 0;
                totalIpi = 0;
                valorItens = 0;
                valorItensFatura = 0;
                valorItens = 0;
                for (int i = 0; i < dgItens.RowCount; i++)
                {
                    totalIpi = totalIpi + Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALOR_IPI"].Value);
                    valorItens = valorItens + Convert.ToDouble(dgItens.Rows[i].Cells["ENT_TOTITEM"].Value);

                    var natOperacaoFatura = new NatOperacao();
                    if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                    {
                        valorItensFatura = valorItensFatura + valorItens + totalIpi;
                    }
                }
                TotalizaGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtPVenda_Validated(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtEQtde.Text) != 0)
                {

                    if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                    {
                        var dadosProduto = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = txtCodBarras.Text.Trim(),
                        };

                        dtRetorno = dadosProduto.BuscaPrecoCompraOuUltimoCusto(dadosProduto);
                        double xPrecoCompra = 0;
                        if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["PROD_PRECOMPRA"].ToString()))
                        {
                            if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_PRECOMPRA"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_PRECOMPRA"]));
                            }
                            else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                            {
                                xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                            }
                        }
                        else if (!Convert.ToDouble(dtRetorno.Rows[0]["PROD_ULTCUSME"]).Equals(0))
                        {
                            xPrecoCompra = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_ULTCUSME"]));
                        }

                        txtPMargem.Text = String.Format("{0:N}", Convert.ToDouble(String.Format("{0:0.00}", ((Convert.ToDouble(txtPVenda.Text) / xPrecoCompra) - 1) * 100)));
                    }
                    else
                    {
                        double wLucro = 0;
                        wCusto = Convert.ToDouble(txtPVTotal.Text) / Convert.ToInt32(txtEQtde.Text);
                        if (wCusto != 0)
                        {
                            wLucro = Convert.ToDouble(txtPVenda.Text) - wCusto;
                            wMargem = (wLucro / wCusto) * 100;
                            txtPMargem.Text = String.Format("{0:N}", wMargem);
                        }
                    }

                    txtPVenda.Text = String.Format("{0:N}", Convert.ToDecimal(txtPVenda.Text));

                    if (Convert.ToDecimal(txtPMargem.Text) < 0)
                    {
                        txtPMargem.ForeColor = Color.Red;
                    }
                    else
                        txtPMargem.ForeColor = Color.Black;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgItens_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (dgItens.Columns[e.ColumnIndex].Name == "DESC_PORC" && (entradaViaXml.Equals("N") || Funcoes.LeParametro(9, "47", false).Equals("S")))
            {
                if (Convert.ToDecimal(dgItens.Rows[e.RowIndex].Cells["DESC_PORC"].Value) > 100)
                {
                    MessageBox.Show("Desconto não pode ser maior que 100%", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgItens.Rows[e.RowIndex].Cells["DESC_PORC"].Value = 0.00;
                }
            }
            else if (dgItens.Columns[e.ColumnIndex].Name == "ENT_DESCONTOITEM" && (entradaViaXml.Equals("N") || Funcoes.LeParametro(9, "47", false).Equals("S")))
            {
                if (Convert.ToDecimal(dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value) > Convert.ToDecimal(dgItens.Rows[e.RowIndex].Cells["ENT_UNITARIO"].Value))
                {
                    MessageBox.Show("ATENÇÃO!! Desconto maior que valor unitário", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgItens.Rows[e.RowIndex].Cells["ENT_DESCONTOITEM"].Value = 0.00;
                }
            }
            else if (dgItens.Columns[e.ColumnIndex].Name == "ENT_DTVALIDADE" && (entradaViaXml.Equals("N") || Funcoes.LeParametro(9, "47", false).Equals("S")))
            {
                if (dgItens.Rows[e.RowIndex].Cells["ENT_DTVALIDADE"].Value != null)
                {
                    if (DateTime.Compare(Convert.ToDateTime(dgItens.Rows[e.RowIndex].Cells["ENT_DTVALIDADE"].Value), DateTime.Now) < 0)
                    {
                        MessageBox.Show("ATENÇÃO!! Data de Validade inferior a data atual", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgItens.Rows[e.RowIndex].Cells["ENT_DTVALIDADE"].Value = "";
                    }
                }
            }
        }

        private void txtQtde_Validated(object sender, EventArgs e)
        {
            if (txtQtde.Text.Trim() != "" && txtCodBarras.Text.Trim() != "")
            {
                if (Convert.ToInt32(txtEQtde.Text) > 1)
                {
                    txtEQtde.Text = (Convert.ToInt32(txtEQtde.Text) * Convert.ToInt32(txtQtde.Text)).ToString();
                }
                else
                {
                    txtEQtde.Text = txtQtde.Text;
                }
                txtPVTotal.Text = String.Format("{0:N}", (Convert.ToDouble(txtVUni.Text) - Convert.ToDouble(txtPDescUni.Text)) * Convert.ToInt32(txtEQtde.Text));
            }
        }

        public bool LerXML()
        {
            #region DECLARAÇÃO DAS VARIÁVEIS
            int xItem;
            string xCodigo;
            string xProduto;
            string xNCM;
            string xIcms;
            string xCfop;
            int xQtde;
            int xQtdeTrib;
            int xQtEstoque;
            string xUnidade;
            string xCEST;
            double xDescontoUni;
            string xValidade;
            string xLote;
            double xPreValor;
            double xPrecoCompra;
            double xComissao;
            double xMargem;
            double xValorUni;
            double xValorTotal;
            double xTotal;
            double xDesconto;
            double xPorcentagem;
            double xAliq;
            double xVIcms;
            string xNOper;
            string xIOper;
            double xVBIcms;
            double xVIcmsST;
            double xPMC;
            string xRegistroMS;
            string xOrigem;
            string xImpCst;
            string xCEnq;
            string xIpiCst;
            string xPisCst;
            string xConfinsCst;
            double xVlBaseIcmsSt;
            double xVBIcmsRet;
            double xVIcmsRet;
            double xBaseIpi;
            double XAliqIpi;
            double xValorIpi;
            totalIcms = 0;
            totalIpi = 0;
            valorItens = 0;
            valorItensFatura = 0;
            cmbComp.SelectedIndex = 0;
            string produtoCadastradoRecente;
            xCodigo = "";
            #endregion

            try
            {
                Cursor = Cursors.WaitCursor;

                xmlNFe.Load(caminhoXML);

                string versaoXML = "";
                xnResul = xmlNFe.GetElementsByTagName("nfeProc");
                if (xnResul.Count == 1)
                {
                    for (int i = 0; i < xnResul[0].Attributes.Count; i++)
                    {
                        if (xnResul[0].Attributes[i].Name == "versao")
                        {
                            versaoXML = xnResul[0].Attributes[i].InnerText;
                        }
                    }
                }

                xnResul = xmlNFe.GetElementsByTagName("infNFe");
                if (xnResul.Count == 1)
                {
                    for (int i = 0; i < xnResul[0].Attributes.Count; i++)
                    {
                        if (xnResul[0].Attributes[i].Name == "Id")
                        {
                            txtChaveAcesso.Text = xnResul[0].Attributes[i].InnerText;
                        }
                    }
                }

                xnResul = xmlNFe.GetElementsByTagName("ide");

                for (int i = 0; i < xnResul[0].ChildNodes.Count; i++)
                {
                    switch (xnResul[0].ChildNodes[i].Name)
                    {
                        case "dhEmi":
                            dtEmissao.Value = Convert.ToDateTime(Convert.ToDateTime(xnResul[0]["dhEmi"].InnerText).ToString("dd/MM/yyyy"));
                            DataHoraEmissao = xnResul[0]["dhEmi"].InnerText.Substring(0, 19);
                            break;
                        case "nNF":
                            txtDocto.Text = xnResul[0]["nNF"].InnerText;
                            break;
                        case "serie":
                            txtSerie.Text = xnResul[0]["serie"].InnerText;
                            break;
                    }
                }

                xnResul = xmlNFe.GetElementsByTagName("emit");
                long cnpj = Convert.ToInt64(xnResul[0]["CNPJ"].InnerText);
                txtFCnpj.Text = String.Format(@"{0:00\.000\.000\/0000\-00}", cnpj);

                var buscaFornecedor = new Cliente();
                List<Cliente> cliente = buscaFornecedor.BuscaDadosFornecedor(2, txtFCnpj.Text, 1);

                if (!cliente.Count.Equals(0))
                {
                    txtFNome.Text = cliente[0].CfNome;
                    txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                }
                else
                {
                    MessageBox.Show("Fornecedor não cadastrado: " + txtFCnpj.Text + " - " + xnResul[0]["xNome"].InnerText, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                    txtEntID.Focus();
                    return false;
                }
                //DADOS DOS IMPOSTOS DA NOTA//
                xnResul = xmlNFe.GetElementsByTagName("total");
                for (int i = 0; i < xnResul[0]["ICMSTot"].ChildNodes.Count; i++)
                {
                    switch (xnResul[0]["ICMSTot"].ChildNodes[i].Name)
                    {
                        case "vBC":
                            txtBIcms.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vBC"].InnerText.Replace(".", ","));
                            break;
                        case "vICMS":
                            txtVIcms.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vICMS"].InnerText.Replace(".", ","));
                            break;
                        case "vBCST":
                            txtIIcms.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vBCST"].InnerText.Replace(".", ","));
                            break;
                        case "vST":
                            txtVICMSST.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vST"].InnerText.Replace(".", ","));
                            break;
                        case "vProd":
                            txtTotalProdutos.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vProd"].InnerText.Replace(".", ","));
                            break;
                        case "vII":
                            txtVImpImportacao.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vII"].InnerText.Replace(".", ","));
                            break;
                        case "vPIS":
                            txtValorPIS.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vPIS"].InnerText.Replace(".", ","));
                            break;
                        case "vIPI":
                            txtValorIpi.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vIPI"].InnerText.Replace(".", ","));
                            break;
                        case "vFrete":
                            txtFrete.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vFrete"].InnerText.Replace(".", ","));
                            break;
                        case "vNF":
                            txtNTotal.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vNF"].InnerText.Replace(".", ","));
                            break;
                        case "vSeg":
                            txtValorSeguro.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vSeg"].InnerText.Replace(".", ","));
                            break;
                        case "vDesc":
                            txtValorDesconto.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vDesc"].InnerText.Replace(".", ","));
                            break;
                        case "vOutro":
                            txtOutrasDespesas.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vOutro"].InnerText.Replace(".", ","));
                            break;
                        case "vCOFINS":
                            txtValorCofins.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vCOFINS"].InnerText.Replace(".", ","));
                            break;
                        case "vTotTrib":
                            txtTotalTributos.Text = String.Format("{0:0.00}", xnResul[0]["ICMSTot"]["vTotTrib"].InnerText.Replace(".", ","));
                            break;

                    }
                }
                //OBTEM O NUMERO DO PEDIDO DE COMPRA//
                xnResul = xmlNFe.GetElementsByTagName("compra");
                if (xnResul.Count > 0)
                {
                    if (xnResul[0].ChildNodes.Count > 0)
                        txtNCompra.Text = Funcoes.ChecaCampoVazio(xnResul[0]["xPed"].InnerText);
                }

                //OBTEM O NUMERO DA FATURA//
                txtFatura.Text = txtDocto.Text;
                
                //OBTEM O VENCIMENTO E O VALOR DA FATURA//
                xnResul = xmlNFe.GetElementsByTagName("dup");
                for (int x = 0; x < xnResul.Count; x++)
                {
                    dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { xnResul[x]["vDup"].InnerText.Replace(".", ","), Convert.ToDateTime(xnResul[x]["dVenc"].InnerText).ToString("dd/MM/yyyy") });
                    valorParcelas = 0;
                    valorParcelas = valorParcelas + Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["vDup"].InnerText.Replace(".", ",")));
                }
                xnResul = xmlNFe.GetElementsByTagName("infAdic");
                if (xnResul.Count > 0 && xnResul[0].ChildNodes.Count > 0)
                    txtObs.Text = Funcoes.ChecaCampoVazio(Funcoes.RemoverAspas(xnResul[0].ChildNodes[0].InnerText));

                InserirSpoolNF();
                InserirSpoolParcelas();

                #region CARREGA OS PRODUTOS
                xnResul = xmlNFe.GetElementsByTagName("det");
                for (int x = 0; x < xnResul.Count; x++)
                {
                    #region LIMPA AS VARIÁVEIS
                    xItem = 0;
                    xCodigo = "";
                    xProduto = "";
                    xComissao = 0;
                    xQtde = 0; ;
                    xUnidade = "";
                    xQtEstoque = 0; ;
                    xValorUni = 0; ;
                    xDesconto = 0; ;
                    xPorcentagem = 0;
                    xTotal = 0;
                    xMargem = 0;
                    xPreValor = 0;
                    xLote = "";
                    xIcms = "";
                    xAliq = 0;
                    xVBIcms = 0;
                    xVIcms = 0;
                    xImpCst = "";
                    xVlBaseIcmsSt = 0;
                    xVIcmsST = 0;
                    xVBIcmsRet = 0;
                    xVIcmsRet = 0;
                    xCEnq = "";
                    xIpiCst = "";
                    xBaseIpi = 0;
                    XAliqIpi = 0;
                    xOrigem = "";
                    xNCM = "";
                    xCfop = "";
                    xConfinsCst = "";
                    xValorIpi = 0;
                    xPisCst = "";
                    xValidade = "";
                    xComissao = 0;
                    xNOper = "";
                    xIOper = "";
                    xDescontoUni = 0;
                    xQtdeTrib = 0;
                    xValorTotal = 0;
                    produtoCadastradoRecente = "N";
                    xCEST = "";
                    xPMC = 0;
                    xPrecoCompra = 0;
                    xRegistroMS = "";
                    #endregion

                    for (int i = 0; i < xnResul[x]["prod"].ChildNodes.Count; i++)
                    {
                        switch (xnResul[x]["prod"].ChildNodes[i].Name)
                        {
                            case "cEAN":
                                if (!String.IsNullOrEmpty(xnResul[x]["prod"]["cEAN"].InnerText) && !xnResul[x]["prod"]["cEAN"].InnerText.Equals("SEM GTIN"))
                                {
                                    xCodigo = xnResul[x]["prod"]["cEAN"].InnerText;
                                }
                                else
                                {
                                    xCodigo = "0";
                                }
                                break;
                            case "xProd":
                                xProduto = Funcoes.RemoveCaracter(xnResul[x]["prod"]["xProd"].InnerText);
                                break;
                            case "NCM":
                                xNCM = xnResul[x]["prod"]["NCM"].InnerText;
                                break;
                            case "CFOP":
                                xCfop = xnResul[x]["prod"]["CFOP"].InnerText;
                                if (xCfop.Substring(0, 2).Equals("61"))
                                    xCfop = "5405";
                                break;
                            case "qCom":
                                campos = xnResul[x]["prod"]["qCom"].InnerText.Split('.');
                                xQtde = Convert.ToInt32(campos[0]);
                                break;
                            case "vUnTrib":
                                xValorUni = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["prod"]["vUnTrib"].InnerText.Replace(".", ",")));
                                break;
                            case "vDesc":
                                xDescontoUni = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["prod"]["vDesc"].InnerText.Replace(".", ",")));
                                break;
                            case "qTrib":
                                campos = xnResul[x]["prod"]["qTrib"].InnerText.Split('.');
                                xQtdeTrib = Convert.ToInt32(campos[0]);
                                break;
                            case "vProd":
                                xValorTotal = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["prod"]["vProd"].InnerText.Replace(".", ",")));
                                break;
                            case "rastro":
                                xValidade = Convert.ToDateTime(xnResul[x]["prod"]["rastro"]["dVal"].InnerText).ToString("dd/MM/yyyy");
                                xLote = xnResul[x]["prod"]["rastro"]["nLote"].InnerText;
                                break;
                            case "med":
                                if (!versaoXML.Equals("4.00"))
                                {
                                    xValidade = Convert.ToDateTime(xnResul[x]["prod"]["med"]["dVal"].InnerText).ToString("dd/MM/yyyy");
                                    xLote = xnResul[x]["prod"]["med"]["nLote"].InnerText;
                                }
                                else
                                    xRegistroMS = xnResul[x]["prod"]["med"]["cProdANVISA"].InnerText;

                                xPMC = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["prod"]["med"]["vPMC"].InnerText.Replace(".", ",")));
                                break;
                            case "uTrib":
                                xUnidade = Funcoes.FormataTamanhoString(xnResul[x]["prod"]["uTrib"].InnerText, 2);
                                break;
                            case "CEST":
                                xCEST = xnResul[x]["prod"]["CEST"].InnerText;
                                break;
                        }
                    }

                    if (!xCodigo.Equals("0"))
                    {
                        var entItens = new Produto();
                        dtRetorno = entItens.BuscaProdutosEntradaItens(Principal.estAtual, Principal.empAtual, xnResul[x]["prod"]["cEAN"].InnerText);

                        if (dtRetorno.Rows.Count > 0)
                        {
                            xPrecoCompra = xValorUni;
                            xPreValor = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PRE_VALOR"]));

                            if (Funcoes.LeParametro(9, "37", true).Equals("S"))
                            {
                                if (dtRetorno.Rows[0]["PROD_COMISSAO"].ToString() != "")
                                {
                                    xComissao = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_COMISSAO"]));
                                }
                                else
                                    xComissao = 0;
                            }
                            else
                            {
                                xComissao = 0;
                            }


                            if (xQtdeTrib != xQtde * (dtRetorno.Rows[0]["PROD_POPULAR"].ToString() == "S" ? 1 : Convert.ToInt32(dtRetorno.Rows[0]["PROD_QTDE_UN_COMP"]) == 0 ? 1 : Convert.ToInt32(dtRetorno.Rows[0]["PROD_QTDE_UN_COMP"])))
                            {
                                xQtdeTrib = Convert.ToInt32(dtRetorno.Rows[0]["PROD_QTDE_UN_COMP"]);
                            }

                            if (!xQtdeTrib.Equals(xQtde))
                            {
                                xQtEstoque = xQtde * xQtdeTrib;
                            }
                            else
                                xQtEstoque = xQtde;

                            xDesconto = Convert.ToDouble(Math.Round(Convert.ToDouble(xDescontoUni) / xQtEstoque, 2));

                            //Erro Divição por Zero
                            xDesconto = (xDesconto != xDesconto || xDesconto == 0) ? 1 : xDesconto; 
                            //Se for verdade ele só coloca o numero 1 pois depois havera divição usando essa variavel e pode dar o mesmo erro 

                            xPorcentagem = Convert.ToDouble(Math.Round(((Convert.ToDecimal(xDescontoUni) / xQtEstoque) * 100) / Convert.ToDecimal(xValorUni), 2));
                            xTotal = Convert.ToDouble(String.Format("{0:0.00}", xValorTotal - xDescontoUni));

                            if (xQtdeTrib != 0)
                            {
                                if (Funcoes.LeParametro(9, "48", false).Equals("S"))
                                {
                                    if (!String.IsNullOrEmpty(dtRetorno.Rows[0]["PROD_MARGEM"].ToString()))
                                    {
                                        xMargem = Convert.ToDouble(String.Format("{0:0.00}", dtRetorno.Rows[0]["PROD_MARGEM"]));

                                        if (xMargem.Equals(0) && xPrecoCompra > 0)
                                        {
                                            double xValidacao = Convert.ToDouble(String.Format("{0:0.00}", ((xPreValor / xPrecoCompra) - 1) * 100));

                                            if (xMargem != xValidacao)
                                                xMargem = xValidacao;
                                        }
                                    }
                                    else
                                    {
                                        if (xPrecoCompra > 0)
                                        {
                                            xMargem = Convert.ToDouble(String.Format("{0:0.00}", ((xPreValor / xPrecoCompra) - 1) * 100));
                                        }
                                        else
                                            xMargem = 0;
                                    }
                                }
                                else
                                {
                                    wCusto = xTotal / xQtEstoque;
                                    xMargem = (xPreValor - wCusto) / wCusto * 100;
                                }
                            }

                            xItem = dgItens.Rows.Count + 1;

                            for (int y = 0; y < xnResul[x]["imposto"].ChildNodes.Count; y++)
                            {
                                switch (xnResul[x]["imposto"].ChildNodes[y].Name)
                                {
                                    case "ICMS":
                                        for (int i = 0; i < xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes.Count; i++)
                                        {
                                            switch (xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].Name)
                                            {
                                                case "orig": //Origem da mercadoria
                                                    xOrigem = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    break;
                                                case "xImpCst": //CST do ICMS
                                                    xImpCst = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    break;
                                                case "pICMS": //Alíquota do imposto
                                                    xAliq = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "vBC": //Valor da Base do ICMS
                                                    xVBIcms = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "vICMS": //Valor do ICMS
                                                    xVIcms = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "CST": //Tributação do ICMS
                                                    xImpCst = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    xIcms = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    break;
                                                case "vBCST": //Valor da BC do ICMS ST
                                                    xVlBaseIcmsSt = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "vICMSST": //Valor do ICMS ST
                                                    xVIcmsST = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "vBCSTRet": //Valor da BC do ICMS ST retido
                                                    xVBIcmsRet = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "vICMSSTRet": //Valor do ICMS ST retido
                                                    xVIcmsRet = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText.Replace(".", ",")));
                                                    break;
                                                case "CSOSN": //Tributação do ICMS
                                                    xImpCst = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    xIcms = xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].ChildNodes[i].InnerText;
                                                    if (xImpCst.Equals("500"))
                                                    {
                                                        xImpCst = "60";
                                                        xIcms = "60";
                                                    }
                                                    if (xImpCst.Equals("102") || xImpCst.Equals("103") || xImpCst.Equals("101"))
                                                    {
                                                        xImpCst = "00";
                                                        xIcms = "00";
                                                    }
                                                    if (xImpCst.Equals("203"))
                                                    {
                                                        xImpCst = "10";
                                                        xIcms = "10";
                                                    }
                                                    if (xImpCst.Equals("900"))
                                                    {
                                                        xImpCst = "90";
                                                        xIcms = "90";
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                    case "IPI":
                                        //INFORMACOES SOBRE IPI//
                                        for (int i = 0; i < xnResul[x]["imposto"].ChildNodes[y].ChildNodes.Count; i++)
                                        {
                                            switch (xnResul[x]["imposto"].ChildNodes[y].ChildNodes[i].Name)
                                            {
                                                case "cEnq": //Código de Enquadramento Legal do IPI
                                                    xCEnq = xnResul[x]["imposto"].ChildNodes[y]["cEnq"].InnerText;
                                                    break;
                                                case "IPINT":
                                                    for (int z = 0; z < xnResul[x]["imposto"].ChildNodes[y]["IPINT"].ChildNodes.Count; z++)
                                                    {
                                                        switch (xnResul[x]["imposto"].ChildNodes[y]["IPINT"].ChildNodes[z].Name)
                                                        {
                                                            case "CST":
                                                                //Código da situação tributária do IPI
                                                                xIpiCst = xnResul[x]["imposto"].ChildNodes[y]["IPINT"]["CST"].InnerText;
                                                                break;
                                                            case "vBC": //Valor da base do IPI
                                                                xBaseIpi = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y]["IPINT"]["vBC"].InnerText.Replace(".", ",")));
                                                                break;
                                                            case "pIPI": //Alíquota do IPI
                                                                XAliqIpi = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y]["IPINT"]["pIPI"].InnerText.Replace(".", ",")));
                                                                break;
                                                            case "vIPI": //Valor do IPI
                                                                xValorIpi = Convert.ToDouble(String.Format("{0:0.00}", xnResul[x]["imposto"].ChildNodes[y]["IPINT"]["vIPI"].InnerText.Replace(".", ",")));
                                                                break;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                    case "PIS":
                                        //INFORMACOES SOBRE PIS//
                                        xPisCst = xnResul[x]["imposto"].ChildNodes[y][xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].Name]["CST"].InnerText; //Código de Situação Tributária do PIS
                                        break;
                                    case "COFINS":
                                        //INFORMACOES SOBRE COFINS//
                                        xConfinsCst = xnResul[x]["imposto"].ChildNodes[y][xnResul[x]["imposto"].ChildNodes[y].ChildNodes[0].Name]["CST"].InnerText;
                                        break;
                                }
                            }

                            campos = cmbNOper.Text.Split('-');
                            campos = campos[0].Split('/');
                            xNOper = campos[0];
                            xIOper = campos[1];

                            if (possuiProdutosNaoCadastrados.Equals(true))
                            {
                                for (int i = 0; i < dgProdutosNaoCadastrados.RowCount; i++)
                                {
                                    if (xCodigo.Equals(dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString()))
                                    {
                                        produtoCadastradoRecente = "S";
                                    }
                                }
                            }

                            if (Funcoes.LeParametro(9, "55", false).Equals("S"))
                            {
                                xPreValor = xPMC == 0 ? xPreValor : xPMC;
                            }

                            dgItens.Rows.Insert(dgItens.RowCount, new Object[] { xItem, xCodigo, xProduto, xComissao, xQtde, xUnidade, xQtEstoque, xValorUni, xPorcentagem,xDesconto,
                                xTotal, xMargem, xPreValor,dtRetorno.Rows[0]["PROD_COMISSAO"], xPMC, dtRetorno.Rows[0]["PROD_ESTATUAL"], xLote, xIcms, xAliq, xVBIcms, xVIcms, xImpCst, xVlBaseIcmsSt, xVIcmsST, xVBIcmsRet, xVIcmsRet, xCEnq, xIpiCst,
                                xBaseIpi, XAliqIpi, xOrigem, xNCM, xCfop, xConfinsCst, xValorIpi, xPisCst, xValidade,xNOper, xIOper, txtNCompra.Text,produtoCadastradoRecente,0, xPrecoCompra, xCEST,
                                        xRegistroMS == "" ? dtRetorno.Rows[0]["PROD_REGISTRO_MS"] : xRegistroMS });

                            totalIcms = totalIcms + Convert.ToDouble(xVIcms);
                            totalIpi = totalIpi + Convert.ToDouble(xValorIpi);
                            valorItens = valorItens + Convert.ToDouble(xTotal);
                            var natOperacaoFatura = new NatOperacao();
                            if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                            {
                                valorItensFatura = valorItensFatura + Convert.ToDouble(xTotal) + Convert.ToDouble(xValorIpi);
                            }

                            InserirSpoolItens(dgItens.Rows.Count - 1);

                            TotalizaGrid();
                            txtDocto.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Erro ao importar Produto: " + xCodigo + ".\nVerifique o cadastro ou dê entrada manual", "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            continue;
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message + " - Produto: " + xCodigo, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool IdentificaProdutoNaoCadastrados()
        {
            try
            {
                dgProdutosNaoCadastrados.Rows.Clear();
                possuiProdutosNaoCadastrados = false;
                bool produtosSemCodigo = false;
                int indiceProd = 0;
                int incideSemCod = 0;

                xnResul = xmlNFe.GetElementsByTagName("prod");
                string[,] produtosNaoCadastrados = new string[xnResul.Count, 5];
                string[] produtosSemCodBarra = new string[xnResul.Count];

                for (int i = 0; i < xnResul.Count; i++)
                {
                    if (!String.IsNullOrEmpty(xnResul[i]["cEAN"].InnerText) && !xnResul[i]["cEAN"].InnerText.Equals("SEM GTIN"))
                    {
                        var dadosProduto = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = xnResul[i]["cEAN"].InnerText,
                        };

                        if (dadosProduto.IndentificaProdutoCadastradoProdutoDetalhe(dadosProduto).Equals(false))
                        {
                            produtosNaoCadastrados[indiceProd, 0] = xnResul[i]["cEAN"].InnerText;
                            produtosNaoCadastrados[indiceProd, 1] = Funcoes.FormataTamanhoString(xnResul[i]["xProd"].InnerText, 50);
                            produtosNaoCadastrados[indiceProd, 2] = Funcoes.FormataTamanhoString(xnResul[i]["uCom"].InnerText, 2);
                            if (xnResul[i]["med"] != null)
                            {
                                if (Convert.ToDouble(xnResul[i]["med"]["vPMC"].InnerText) > 0)
                                {
                                    produtosNaoCadastrados[indiceProd, 3] = xnResul[i]["med"]["vPMC"].InnerText;
                                }
                                else
                                    produtosNaoCadastrados[indiceProd, 3] = xnResul[i]["vUnTrib"].InnerText;
                            }
                            else
                                produtosNaoCadastrados[indiceProd, 3] = xnResul[i]["vUnTrib"].InnerText;

                            produtosNaoCadastrados[indiceProd, 4] = xnResul[i]["vUnTrib"].InnerText;

                            possuiProdutosNaoCadastrados = true;
                            indiceProd += 1;
                        }
                    }
                    else
                    {
                        produtosSemCodBarra[incideSemCod] = xnResul[i]["xProd"].InnerText;
                        produtosSemCodigo = true;
                        incideSemCod += 1;
                    }
                }

                if (produtosSemCodigo.Equals(true))
                {
                    string mensagem = "";

                    for (int i = 0; i < incideSemCod; i++)
                    {
                        mensagem += produtosSemCodBarra[i] + "\n";
                    }

                  
                    using (StreamWriter sw = new StreamWriter(@"C:\BTI\Produto.txt"))
                    {
                        sw.Write(mensagem);
                        sw.Close();
                      
                    }
                      
                    if (MessageBox.Show("Verifique o Arquivo XML, produto sem Código de Barra!\n\n" + mensagem + "\nDeseja continuar a importação?", "Importação NFe", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.No)
                    {
                        return false;
                    }

                    System.Diagnostics.Process.Start("notepad", @"C:\BTI\Produto.txt");
                }

                if (possuiProdutosNaoCadastrados == true)
                {
                    pnlProdutos.Location = new Point((this.groupBox2.Size.Width / 2) - (this.pnlProdutos.Size.Width / 2), (this.groupBox2.Size.Height / 2) - (this.pnlProdutos.Size.Height / 2));
                    pnlProdutos.Visible = true;

                    for (int i = 0; i < indiceProd; i++)
                    {
                        dgProdutosNaoCadastrados.Rows.Insert(dgProdutosNaoCadastrados.RowCount, new Object[] { i + 1, produtosNaoCadastrados[i, 0],
                            produtosNaoCadastrados[i, 1], produtosNaoCadastrados[i, 2], produtosNaoCadastrados[i, 3], produtosNaoCadastrados[i, 4] });
                    }

                    if (dgProdutosNaoCadastrados.ColumnCount == 6)
                    {
                        DataTable dt = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, " DEP_DESABILITADO = 'N'");
                        DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                        combo.HeaderText = "Departamentos";
                        combo.Name = "Departamento";
                        ArrayList row = new ArrayList();

                        foreach (DataRow dr in dt.Rows)
                        {
                            row.Add(dr["DEP_DESCR"].ToString());
                        }

                        combo.Items.AddRange(row.ToArray());
                        dgProdutosNaoCadastrados.Columns.Add(combo);
                    }
                    btnOk.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmEstEntradas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
            if (tcEntradas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEntradas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEntID.Text) || String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("ENTRADA", "ENT_ID", "ENT_ID", txtEntID.Text, true, false, true)))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
            else if (tcEntradas.SelectedTab == tpItens)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEntID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEntradas.AutoSize = false;
                this.tsbEntradas.Image = Properties.Resources.estoque;
                this.tsbEntradas.Size = new System.Drawing.Size(150, 20);
                this.tsbEntradas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.tsbEntradas.BackColor = Color.WhiteSmoke;

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEntradas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEntradas);
                tsbEntradas.Click += delegate
                {
                    var estEntradas = Application.OpenForms.OfType<frmEstEntradas>().FirstOrDefault();
                    BotoesHabilitados();
                    estEntradas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtEntradas.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEntradas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEntradas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtEntradas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEntradas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEntradas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Incluir()
        {
            try
            {
                var registroEntradaNotas = new DataTable();
                Cursor = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(txtEntID.Text))
                {
                    txtEntID.Text = Funcoes.LeParametro(9, "50", true);
                    if (!String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("ENTRADA", "ENT_ID", "ENT_ID", txtEntID.Text,true,false, true)))
                    {
                        Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);

                        txtEntID.Text = Funcoes.LeParametro(9, "50", true);
                    }

                    Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);
                }

                if (ConsisteCamposNota())
                {
                    if (Funcoes.LeParametro(9, "59", true).Equals("S"))
                    {
                        if (MessageBox.Show("Deseja atualizar impostos dos produtos desta Nota?", "Impostos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            Principal.AtualizaImposto = "S";
                        }
                        else
                            Principal.AtualizaImposto = "N";
                    }
                    else
                        Principal.AtualizaImposto = "S";

                    //Carrega objeto da classe Entrada
                    var dadosEntrada = new Entrada
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EntId = Convert.ToInt32(txtEntID.Text),
                        CfDocto = txtFCnpj.Text,
                        EntDocto = txtDocto.Text,
                        EntSerie = txtSerie.Text,
                    };

                    //Verifica se ja existe a nota de entrada cadastrada
                    if (!String.IsNullOrEmpty(dadosEntrada.IdentificaEntradaCadastradaPorFornecedorDocto(dadosEntrada)))
                    {
                        MessageBox.Show("Entrada já cadastrada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tcEntradas.SelectedTab = tpFicha;
                        txtDocto.Focus();
                        return false;
                    }
                    else
                    {
                        if (txtFatura.Enabled == true)
                        {
                            var dadosPagar = new Pagar
                            {
                                EmpCodigo = Principal.empAtual,
                                PagDocto = txtFatura.Text,
                                CfDocto = txtFCnpj.Text,
                            };
                            if (!String.IsNullOrEmpty(dadosPagar.IdentificaFaturaCadastradaPorDoctoFornecedor(dadosPagar)))
                            {
                                MessageBox.Show("Fatura já cadastrada no contas a pagar.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                tcEntradas.SelectedTab = tpFicha;
                                txtFatura.Focus();
                                return false;
                            }
                        }
                        validaInsert = true;

                        IncluirRegistros();
                        if (validaInsert)
                        {
                            BancoDados.FecharTrans();
                            tcEntradas.SelectedTab = tpFicha;
                            emGrade = false;
                            tslRegistros.Text = "";
                        }
                        else
                            BancoDados.ErroTrans();
                    }

                    //if(entradaViaXml.Equals("S"))
                    //{
                    //    BancoDados.ExecuteNoQuery("UPDATE ENTRADA SET ENT_XML = RAWTOHEX('" + documentoXML + "') WHERE ENT_ID = " + idNOTA, null);
                    //}

                   
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async void ExcluirFilial(List<EntradaItens> listBuscaEntradaItens)
        {
            await DecrementaEstoqueFiliaisExcluir(listBuscaEntradaItens);
        }

        public bool Excluir()
        {
            try
            {
                if (ConsisteCamposNota().Equals(true))
                {
                    if (
                        MessageBox.Show(text: "Confirma a exclusão da entrada?", caption: "Exclusão tabela Entrada",
                            buttons: MessageBoxButtons.YesNo, icon: MessageBoxIcon.Question,
                            defaultButton: MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (var ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }

                        var excluirEntrada = new Entrada
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EntId = Convert.ToInt32(txtEntID.Text),
                            EntDocto = txtDocto.Text,
                            EntSerie = txtSerie.Text,
                            CfDocto = txtFCnpj.Text,
                        };
                        Cursor = Cursors.WaitCursor;
                        var entradaItens = new EntradaItens();

                        List<EntradaItens> listBuscaEntradaItens = entradaItens.CarregaEntradasItens(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text));
                        if (!RegrasEstoque.ExcluirEntradas(excluirEntrada).Equals(true))
                        {
                            return false;
                        }

                        if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                        {
                            ExcluirFilial(listBuscaEntradaItens);
                        }

                        Cursor = Cursors.Default;

                        Funcoes.GravaLogExclusao("ENT_ID", txtEntID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "ENTRADA",
                            txtDocto.Text.Trim().ToUpper(), Principal.motivo, Principal.estAtual, Principal.empAtual);
                        dtEntradas.Clear();
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dgEntradas.DataSource = dtEntradas;
                        emGrade = false;
                        tcEntradas.SelectedTab = tpGrade;
                        Limpar();
                        tslRegistros.Text = "";
                        txtBLancto.Focus();
                        return true;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Atualiza()
        {
            try
            {
                var registroEntradaNotas = new DataTable();
                Cursor = Cursors.WaitCursor;

                if (ConsisteCamposNota())
                {
                    if (!String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("ENTRADA", "ENT_ID", "ENT_ID", txtEntID.Text, true, false, true)))
                    {
                        if (Funcoes.LeParametro(9, "59", true).Equals("S"))
                        {
                            if (MessageBox.Show("Deseja atualizar impostos dos produtos desta Nota?", "Impostos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                Principal.AtualizaImposto = "S";
                            }
                            else
                                Principal.AtualizaImposto = "N";
                        }
                        else
                            Principal.AtualizaImposto = "S";

                        var dadosEntrada = new Entrada
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EntId = Convert.ToInt32(txtEntID.Text),
                            CfDocto = txtFCnpj.Text,
                            EntDocto = txtDocto.Text,
                            EntSerie = txtSerie.Text,
                        };

                        registroEntradaNotas = dadosEntrada.IdentificaEntradaCadastradaPorId(dadosEntrada);

                        validaAtualizacao = true;

                        AtualizaDadosEntradaNotas(registroEntradaNotas);

                        if (validaAtualizacao)
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Ent. de Notas");
                            dtEntradas.Clear();
                            dgEntradas.DataSource = dtEntradas;
                            emGrade = false;
                            tslRegistros.Text = "";
                            tcEntradas.SelectedTab = tpFicha;
                            Limpar();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                    {
                        MessageBox.Show("Entrada não cadastrada!", "Ent. de Notas");
                        txtEntID.Focus();
                        return false;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public async void AtualizaDadosEntradaNotas(DataTable registroEntradaNotas)
        {
            int id;

            id = Funcoes.IdentificaVerificaID("PAGAR", "PAG_CODIGO", 0, "", Principal.empAtual);

            var dadosFatura = new Pagar
            {
                EmpCodigo = Principal.empAtual,
                CfDocto = txtFCnpj.Text,
                PagCodigo = id,
                PagDocto = txtFatura.Text.Trim().ToUpper(),
                PagData = dtEmissao.Value,
                PagValorPrincipal = Convert.ToDouble(txtNTotal.Text),
                PagComprador = Convert.ToInt32(txtComp.Text),
                TipoCodigo = Convert.ToInt32(cmbDocto.SelectedValue),
                PagListou = "N",
                DtCadastro = DateTime.Now,
                OpCadastro = Principal.usuario,
                PagOrigem = "E",
                PagStatus = "N",
                DespCodigo = 4,
                Historico = "ENTRADA DE NOTAS"
            };

            retorno = dadosFatura.IdentificaFaturaCadastradaPorDoctoFornecedor(dadosFatura);
            if (String.IsNullOrEmpty(retorno))
            {
                var dadosEntradaDetalhe = new EntradaDetalhe
                {
                    EstCodigo = Principal.estAtual,
                    EmpCodigo = Principal.empAtual,
                    EntId = Convert.ToInt32(txtEntID.Text),
                    CfDocto = txtFCnpj.Text
                };

                if (!String.IsNullOrEmpty(dadosEntradaDetalhe.IdentificaFaturaLancada(dadosEntradaDetalhe)))
                {
                    MessageBox.Show("Entrada com parcela já lançada no contas a pagar,\nexclua o título antes para poder alterar a entrada.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tcEntradas.SelectedTab = tpFicha;
                    dtLancamento.Focus();
                    validaAtualizacao = false;
                    return;
                }

                if (txtFatura.Enabled == true)
                {
                    if (!String.IsNullOrEmpty(retorno))
                    {
                        MessageBox.Show("Fatura já cadastrada no contas a pagar.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tcEntradas.SelectedTab = tpFicha;
                        txtFatura.Focus();
                        validaAtualizacao = false;
                        return;
                    }
                }
            }
            else
            {
                var statusPagto = new PagarDetalhe
                {
                    EmpCodigo = Principal.empAtual,
                    PagDocto = txtFatura.Text,
                    CfDocto = txtFCnpj.Text,
                };

                dtRetorno = statusPagto.VerificaParcelasPagas(statusPagto);
                bool pagou = false;

                for (int i = 0; i < dtRetorno.Rows.Count; i++)
                {
                    if (dtRetorno.Rows[i]["PD_STATUS"].ToString() == "Q")
                        pagou = true;
                }

                if (pagou)
                {
                    MessageBox.Show("Título no contas a pagar já possui baixa, exclua as baixas para poder alterar.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tcEntradas.SelectedTab = tpFicha;
                    dtLancamento.Focus();
                    validaAtualizacao = false;
                    return;
                }
            }

            BancoDados.AbrirTrans();

            if (!String.IsNullOrEmpty(retorno))
            {
                idNuvem = Util.SelecionaCampoEspecificoDaTabela("PAGAR", "ID", "PAG_CODIGO", id.ToString(), false, true, true);

                var faturaDetalhe = new PagarDetalhe();
                if (faturaDetalhe.ExcluiFaturaDetalhe(Principal.empAtual, retorno, true).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    validaAtualizacao = false;
                    return;
                }

                if (dadosFatura.ExcluiFatura(Principal.empAtual, retorno, true).Equals(-1))
                {
                    BancoDados.ErroTrans();
                    validaAtualizacao = false;
                    return;
                }

                ExcluirPagaEPagarDetalheNuvem(idNuvem);
            }

            if (txtFatura.Enabled.Equals(true))
            {
                if (!dadosFatura.InsereRegistroFatura(dadosFatura))
                {
                    validaAtualizacao = false;
                    return;
                }
                else
                    Funcoes.GravaLogInclusao("PAG_CODIGO", dadosFatura.PagCodigo.ToString(), dadosFatura.OpCadastro, "PAGAR", dadosFatura.PagDocto, Principal.estAtual, dadosFatura.EmpCodigo, true);

                List<PagarDetalhe> detalhe = new List<PagarDetalhe>();
                for (int x = 0; x < dgParcelas.RowCount; x++)
                {
                    var dadosPagarDetalhe = new PagarDetalhe
                    {
                        EmpCodigo = Principal.empAtual,
                        PagCodigo = id,
                        PagDocto = txtFatura.Text.Trim().ToUpper(),
                        CfDocto = txtFCnpj.Text,
                        PdParcela = (x + 1),
                        PdValor = Convert.ToDouble(dgParcelas.Rows[x].Cells[0].Value),
                        PdVencimento = Convert.ToDateTime(dgParcelas.Rows[x].Cells[1].Value),
                        PdSaldo = Convert.ToDouble(dgParcelas.Rows[x].Cells[0].Value),
                        PdStatus = "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                    };

                    detalhe.Add(dadosPagarDetalhe);

                    if (!dadosPagarDetalhe.InsereRegistroFaturaDetalhe(dadosPagarDetalhe))
                    {
                        BancoDados.ErroTrans();
                        validaAtualizacao = false;
                        return;
                    }
                    else
                        if (!Funcoes.GravaLogInclusao("PAG_CODIGO", dadosPagarDetalhe.PagCodigo.ToString(), dadosPagarDetalhe.OpCadastro, "PAGAR_DETALHE", dadosPagarDetalhe.PdValor.ToString(), Principal.estAtual, dadosPagarDetalhe.EmpCodigo, true))
                    {
                        BancoDados.ErroTrans();
                        validaAtualizacao = false;
                        return;
                    }

                    //ENVIA DADOS PRA NUVEM//
                    IncluiPagaEPagarDetalheNuvem(dadosFatura, detalhe);
                }
            }

            campos = cmbNOper.Text.Split('-');
            campos = campos[0].Split('/');
            var entradaDados = new Entrada(
                Principal.estAtual,
                Principal.empAtual,
                Convert.ToInt32(txtEntID.Text),
                dtLancamento.Value.ToString(),
                Convert.ToInt32(cmbDocto.SelectedValue),
                dtEmissao.Value.ToString(),
                txtFCnpj.Text,
                txtDocto.Text.Trim(),
                txtSerie.Text.Trim().ToUpper(),
                Convert.ToInt32(campos[0].Trim()),
                Convert.ToInt32(campos[1].Trim()),
                txtFatura.Text.Trim().ToUpper(),
                Convert.ToDouble(txtNTotal.Text),
                Convert.ToDouble(txtBIcms.Text),
                Convert.ToDouble(txtVIcms.Text),
                Convert.ToDouble(txtIIcms.Text),
                Convert.ToDouble(txtVICMSST.Text),
                Convert.ToDouble(txtVImpImportacao.Text),
                Convert.ToDouble(txtICMSUFRemet.Text),
                Convert.ToDouble(txtValorFCP.Text),
                Convert.ToDouble(txtValorPIS.Text),
                Convert.ToDouble(txtTotalProdutos.Text),
                Convert.ToDouble(txtFrete.Text),
                Convert.ToDouble(txtValorSeguro.Text),
                Convert.ToDouble(txtValorDesconto.Text),
                Convert.ToDouble(txtOutrasDespesas.Text),
                Convert.ToDouble(txtValorIpi.Text),
                Convert.ToDouble(txtVICMSUFDest.Text),
                Convert.ToDouble(txtTotalTributos.Text),
                Convert.ToDouble(txtValorCofins.Text),
                Convert.ToInt32(cmbComp.SelectedValue),
                txtNCompra.Text.Trim(),
                "N",
                txtObs.Text.Trim().Length > 1000 ? txtObs.Text.Trim().Substring(0, 999) : txtObs.Text.Trim(),
                entradaViaXml,
                DateTime.Now,
                Principal.usuario,
                DateTime.Now,
                Principal.usuario,
                txtChaveAcesso.Text,
                DataHoraEmissao
            );

            if (!entradaDados.AtualizaDadosEntradaNotas(entradaDados, registroEntradaNotas))
            {
                BancoDados.ErroTrans();
                validaAtualizacao = false;
                return;
            }

            var entDetalheExcluir = new EntradaDetalhe();
            if (!entDetalheExcluir.ExcluiParcelasEntradaNotas(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text)))
            {
                BancoDados.ErroTrans();
                validaAtualizacao = false;
                return;
            }

            for (int i = 0; i < dgParcelas.RowCount; i++)
            {
                var entDetalhe = new EntradaDetalhe(
                    Principal.estAtual,
                    Principal.empAtual,
                    Convert.ToInt32(txtEntID.Text),
                    txtFCnpj.Text,
                    txtDocto.Text.Trim().ToUpper(),
                    txtSerie.Text.Trim().ToUpper(),
                    (i + 1),
                    Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value),
                    Convert.ToDateTime(dgParcelas.Rows[i].Cells[1].Value),
                    txtFatura.Text.Trim().ToUpper(),
                    DateTime.Now,
                    Principal.usuario,
                    DateTime.Now,
                    Principal.usuario
                );

                if (!entDetalhe.InsereRegistrosEntradaDetalhe(entDetalhe))
                {
                    BancoDados.ErroTrans();
                    validaAtualizacao = false;
                    return;
                }
                else
                    if (Funcoes.GravaLogInclusao("ENT_ID", txtEntID.Text, Principal.usuario, "ENTRADA_DETALHES", Funcoes.BValor(Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value)), Principal.estAtual, Principal.empAtual, true).Equals(false))
                {
                    BancoDados.ErroTrans();
                    validaAtualizacao = false;
                    return;
                }

                valorParcelas = valorParcelas + Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);

            }

            validaInsert = true;
            AtualizaDadosProdutosExcluidosParaAtualizacao();

            if (!validaInsert)
            {
                BancoDados.ErroTrans();
                validaAtualizacao = false;
                return;
            }

            if (!InsereRegistrosEntradaItens())
            {
                BancoDados.ErroTrans();
                validaAtualizacao = false;
                return;
            }

            BancoDados.FecharTrans();

            await AtualizarEstoqueFiliais();

            return;
        }

        public async void AtualizaDadosProdutosExcluidosParaAtualizacao()
        {
            var entradaItens = new EntradaItens();
            var dadosProdutos = new ProdutoDetalhe();

            List<EntradaItens> listBuscaEntradaItens = entradaItens.CarregaEntradasItens(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text));
            for (int i = 0; i < listBuscaEntradaItens.Count; i++)
            {
                var dadosProdutoDetalhe = new ProdutoDetalhe
                {
                    EmpCodigo = listBuscaEntradaItens[i].EmpCodigo,
                    EstCodigo = listBuscaEntradaItens[i].EstCodigo,
                    ProdCodigo = listBuscaEntradaItens[i].ProdCodigo,
                    ProdEstAtual = listBuscaEntradaItens[i].EntQtdeEstoque,
                    DtAlteracao = DateTime.Now,
                    OpAlteracao = Principal.usuario,
                };

                if (dadosProdutoDetalhe.AtualizaEstoqueAtual(dadosProdutoDetalhe, "-", true).Equals(-1))
                {
                    validaInsert = false;
                    return;
                }

                entradaItens.EmpCodigo = listBuscaEntradaItens[i].EmpCodigo;
                entradaItens.EstCodigo = listBuscaEntradaItens[i].EstCodigo;
                entradaItens.EntId = Convert.ToInt32(txtEntID.Text);
                entradaItens.ProdCodigo = listBuscaEntradaItens[i].ProdCodigo;

                var dadosMovimento = new MovEstoque
                {
                    EstCodigo = Principal.estAtual,
                    EmpCodigo = Principal.empAtual,
                    EstIndice = listBuscaEntradaItens[i].EstIndice,
                    ProdCodigo = listBuscaEntradaItens[i].ProdCodigo
                };

                if (!dadosMovimento.ManutencaoMovimentoEstoque("D", dadosMovimento))
                {
                    validaInsert = false;
                    return;
                }

                if (!dadosProdutoDetalhe.AtualizaCusto(dadosProdutoDetalhe))
                {
                    validaInsert = false;
                    return;
                }

                if (Funcoes.LeParametro(9, "29", true).Equals("S"))
                {
                    Produto dadosProduto = new Produto();
                    List<Produto> listProduto = dadosProduto.IdentificaProdutoControlado(listBuscaEntradaItens[i].ProdCodigo);
                    if (listProduto[0].ProdControlado.Equals("S"))
                    {
                        var dadosSngpcEntrada = new SngpcEntrada
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EntDocto = txtDocto.Text,
                            EntCNPJ = txtFCnpj.Text,
                            ProdCodigo = listBuscaEntradaItens[i].ProdCodigo,
                        };
                        if (dadosSngpcEntrada.ExcluiProdutoViaEntradaDeNotas(dadosSngpcEntrada, true).Equals(-1))
                        {
                            validaInsert = false;
                            return;
                        }

                        var identificaVenda = new SngpcVenda();
                        int qtde = identificaVenda.IdentificaSeJaTemLoteEProdutoVendido(listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].EntLote, dtLancamento.Value);

                        var excluiLote = new SngpcControleVendas();
                        excluiLote.ProCodigo = listBuscaEntradaItens[i].ProdCodigo;
                        excluiLote.Lote = listBuscaEntradaItens[i].EntLote;
                        excluiLote.Qtde = listBuscaEntradaItens[i].EntQtdeEstoque - qtde;
                        excluiLote.Data = DateTime.Now;

                        if (excluiLote.IdentificaSeJaTemLoteEProduto(listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].EntLote).Equals(listBuscaEntradaItens[i].EntQtdeEstoque + qtde))
                        {
                            if (!excluiLote.SubtraiQtdeMesmoLoteEProduto(excluiLote))
                            {
                                validaInsert = false;
                                return;
                            }
                        }
                        else
                        {
                            if (!excluiLote.SubtraiQtdeMesmoLoteEProduto(excluiLote))
                            {
                                validaInsert = false;
                                return;
                            }
                        }
                    }
                }

                if (entradaItens.ExcluiProdutoEntradasItens(entradaItens).Equals(-1))
                {
                    validaInsert = false;
                    return;
                }
            }

            if (Funcoes.LeParametro(6, "351", true).Equals("S"))
            {
                var vencimento = new ProdutoVencimento();
                if (vencimento.ExcluiProdutoViaEntradaDeNotas(Principal.estAtual, Principal.empAtual, txtDocto.Text).Equals(-1))
                {
                    validaInsert = false;
                    return;
                }
            }

            await DecrementaEstoqueFiliais();
        }

        public bool InsereRegistrosEntradaItens()
        {
            double positive = double.PositiveInfinity;
            double negative = double.NegativeInfinity;
            try
            {
                int idMovimentoEstoque = Convert.ToInt32(Funcoes.IdentificaVerificaID("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual));
                int idSngpcEntradas = Funcoes.IdentificaVerificaID("SNGPC_ENTRADAS", "ENT_SEQ", Principal.estAtual, "", Principal.empAtual);
                for (int i = 0; i < dgItens.RowCount; i++)
                {
                    /*Caso o campo de PROD_MARGEM tenha o simbolo de Infinito, não se sabe o motivo ainda, mas ele entra.*/
                    if (dgItens.Rows[i].Cells["PROD_MARGEM"].Value.Equals(positive) || dgItens.Rows[i].Cells["PROD_MARGEM"].Value.Equals(negative))
                        dgItens.Rows[i].Cells["PROD_MARGEM"].Value = 0; 

                    var dadosProdutoDetalhe = new ProdutoDetalhe
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        ProdCodigo = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        ProdComissao = Convert.ToDouble(dgItens.Rows[i].Cells["PROD_COMISSAO"].Value),
                        ProdMargem = Convert.ToDouble(dgItens.Rows[i].Cells["PROD_MARGEM"].Value),
                        ProdEstAtual = Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                        ProdEstIni = Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                        ProdCusIni = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_UNITARIO"].Value) - Convert.ToDouble(dgItens.Rows[i].Cells["ENT_DESCONTOITEM"].Value),
                        ProdCfop = dgItens.Rows[i].Cells["PROD_CFOP"].Value.ToString(),
                        ProdCst = dgItens.Rows[i].Cells["PROD_CST"].Value.ToString(),
                        ProdCest = dgItens.Rows[i].Cells["PROD_CEST"].Value.ToString(),
                        ProdIcms = dgItens.Rows[i].Cells["ENT_ICMS"].Value.ToString(),
                        ProdValorBaseIcms = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALOR_BC_ICMS"].Value),
                        ProdValorIcms = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALORICMS"].Value),
                        ProdAliqIcms = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ALIQICMS"].Value),
                        ProdBaseIcmsST = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_BC_ICMS_ST"].Value),
                        ProdIcmsRet = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ICMS_RET"].Value),
                        ProdBaseIcmsRet = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_REDUCAOBASEICMS"].Value),
                        ProdValorIcmsSt = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ICMS_ST"].Value),
                        ProdEnqIpi = dgItens.Rows[i].Cells["ENT_ENQ_IPI"].Value.ToString(),
                        ProdCstIpi = dgItens.Rows[i].Cells["ENT_CST_IPI"].Value.ToString(),
                        ProdBaseIpi = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_BASE_IPI"].Value),
                        ProdAliqIpi = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ALIQIPI"].Value),
                        ProdOrigem = dgItens.Rows[i].Cells["ENT_ORIGEM"].Value.ToString(),
                        ProdCstCofins = dgItens.Rows[i].Cells["ENT_CST_COFINS"].Value.ToString(),
                        ProdValorIpi = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALOR_IPI"].Value),
                        ProdCstPis = dgItens.Rows[i].Cells["ENT_CST_PIS"].Value.ToString(),
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                        ProdPreCompra = Convert.ToDouble(dgItens.Rows[i].Cells["PROD_PRECOMPRA"].Value),
                        ProdQtdeUnComp = Convert.ToInt32(dgItens.Rows[i].Cells["QTDE"].Value)
                    };

                    if (!dadosProdutoDetalhe.AtualizaComissaoMargemEstoqueAtualImpostosEntradaNota(dadosProdutoDetalhe))
                        return false;

                    if (!dadosProdutoDetalhe.AtualizaCusto(dadosProdutoDetalhe))
                        return false;

                    var dadosProduto = new Produto()
                    {
                        ProdCodBarras = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        ProdNCM = dgItens.Rows[i].Cells["ENT_NCM"].Value.ToString(),
                        ProdUnidade = dgItens.Rows[i].Cells["ENT_UNIDADE"].Value.ToString(),
                        ProdRegistroMS = dgItens.Rows[i].Cells["PROD_REGMS"].Value.ToString(),
                        OpAlteracao = Principal.usuario,
                    };

                    if (!dadosProduto.AtualizaNCMProdUnidadeEntradaNotas(dadosProduto, Principal.estAtual, Principal.empAtual))
                        return false;

                    var dadosPreco = new Preco
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        ProdCodigo = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        PreValor = Convert.ToDouble(dgItens.Rows[i].Cells["PRE_VALOR"].Value),
                        ProdPMC = Convert.ToDouble(dgItens.Rows[i].Cells["VALOR_PMC"].Value),
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                    };

                    if (!dadosPreco.AtualizaPreValorEntradaNotas(dadosPreco))
                        return false;

                    var dadosMovimento = new MovEstoque
                    {
                        EstCodigo = Principal.estAtual,
                        EmpCodigo = Principal.empAtual,
                        EstIndice = idMovimentoEstoque,
                        ProdCodigo = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        EntQtde = Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                        EntValor = Convert.ToDouble(dgItens.Rows[i].Cells["ENT_TOTITEM"].Value),
                        SaiQtde = 0,
                        SaiValor = 0,
                        OperacaoCodigo = "C",
                        DataMovimento = dtLancamento.Value,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                    };

                    if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        return false;


                    if (Funcoes.LeParametro(9, "29", true).Equals("S"))
                    {
                        List<Produto> listProduto = dadosProduto.IdentificaProdutoControlado(dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString());
                        if (listProduto[0].ProdControlado.Equals("S"))
                        {
                            var dadosSngpcEntrada = new SngpcEntrada
                            {
                                EstCodigo = Principal.estAtual,
                                EmpCodigo = Principal.empAtual,
                                EntSeq = idSngpcEntradas,
                                EntDocto = txtDocto.Text,
                                EntTipoOperacao = 7,
                                EntDataNF = dtEmissao.Value,
                                EntCNPJ = txtFCnpj.Text,
                                ProdCodigo = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                EntMedLote = dgItens.Rows[i].Cells["PROD_CONTROLADO"].Value.ToString(),
                                EntMedQtde = Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                                EntData = dtLancamento.Value,
                                EntRegistroMS = listProduto[0].ProdRegistroMS,
                                DtCadastro = DateTime.Now,
                                OpCadastro = Principal.usuario,

                            };

                            if (!dadosSngpcEntrada.InsereRegistroSngpcEntrada(dadosSngpcEntrada))
                                return false;
                            else
                                if (Funcoes.GravaLogInclusao("ENT_SEG", txtEntID.Text, Principal.usuario, "SNGPC_ENTRADAS", txtDocto.Text.Trim().ToUpper(), Principal.estAtual, Principal.empAtual, true).Equals(false))
                                return false;

                            idSngpcEntradas += 1;

                            var controleLote = new SngpcControleVendas
                                (
                                    dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                    dgItens.Rows[i].Cells["PROD_CONTROLADO"].Value.ToString(),
                                    Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                                    DateTime.Now
                                );

                            if (controleLote.IdentificaSeJaTemLoteEProduto(controleLote.ProCodigo, controleLote.Lote).Equals(0))
                            {
                                if (!controleLote.Incluir(controleLote))
                                    return false;
                            }
                            else
                            {
                                if (!controleLote.SomaQtdeMesmoLoteEProduto(controleLote))
                                    return false;
                            }
                        }
                    }

                    var entItens = new EntradaItens(
                        Principal.empAtual,
                        Principal.estAtual,
                        Convert.ToInt32(txtEntID.Text),
                        txtFCnpj.Text,
                        txtDocto.Text.Trim(),
                        txtSerie.Text.Trim(),
                        (i + 1),
                        dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        dgItens.Rows[i].Cells["PROD_DESCR"].Value.ToString(),
                        "E",
                        Convert.ToInt32(dgItens.Rows[i].Cells["ENT_QTDESTOQUE"].Value),
                        Convert.ToInt32(dgItens.Rows[i].Cells["QTDE"].Value),
                        dgItens.Rows[i].Cells["ENT_UNIDADE"].Value.ToString(),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_UNITARIO"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_DESCONTOITEM"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_TOTITEM"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["PROD_MARGEM"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["PRE_VALOR"].Value),
                        dgItens.Rows[i].Cells["PROD_CONTROLADO"].Value.ToString(),
                        dgItens.Rows[i].Cells["ENT_ICMS"].Value.ToString(),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ALIQICMS"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALOR_BC_ICMS"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALORICMS"].Value),
                        dgItens.Rows[i].Cells["PROD_CST"].Value.ToString(),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_BC_ICMS_ST"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ICMS_ST"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_REDUCAOBASEICMS"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ICMS_RET"].Value),
                        dgItens.Rows[i].Cells["ENT_ENQ_IPI"].Value.ToString(),
                        dgItens.Rows[i].Cells["ENT_CST_IPI"].Value.ToString(),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_BASE_IPI"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_ALIQIPI"].Value),
                        Convert.ToDouble(dgItens.Rows[i].Cells["ENT_VALOR_IPI"].Value),
                        Convert.ToInt32(dgItens.Rows[i].Cells["ENT_ORIGEM"].Value),
                        dgItens.Rows[i].Cells["ENT_NCM"].Value.ToString(),
                        dgItens.Rows[i].Cells["PROD_CFOP"].Value.ToString(),
                        dgItens.Rows[i].Cells["ENT_CST_COFINS"].Value.ToString(),
                        dgItens.Rows[i].Cells["ENT_CST_PIS"].Value.ToString(),
                        dgItens.Rows[i].Cells["NO_CODIGO"].Value.ToString(),
                        dgItens.Rows[i].Cells["NO_COMP"].Value.ToString(),
                        Convert.ToDouble(dgItens.Rows[i].Cells["PROD_COMISSAO"].Value),
                         dgItens.Rows[i].Cells["COMP_NUMERO"].Value.ToString(),
                        dgItens.Rows[i].Cells["ENT_DTVALIDADE"].Value.ToString() == "" ? Convert.ToDateTime("01/01/2001 00:00:00") : Convert.ToDateTime(dgItens.Rows[i].Cells["ENT_DTVALIDADE"].Value),
                        dgItens.Rows[i].Cells["PROD_CADASTRADO"].Value.ToString(),
                        idMovimentoEstoque,
                        Convert.ToDouble(dgItens.Rows[i].Cells["PROD_PRECOMPRA"].Value),
                        Funcoes.ChecaCampoVazio(dgItens.Rows[i].Cells["PROD_CEST"].Value.ToString()),
                        Convert.ToDouble(dgItens.Rows[i].Cells["VALOR_PMC"].Value),
                        dgItens.Rows[i].Cells["PROD_REGMS"].Value.ToString(),
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                        );
                    if (!entItens.InsereRegistrosEntradaItens(entItens))
                        return false;
                    else
                        Funcoes.GravaLogInclusao("ENT_ID", txtEntID.Text, Principal.usuario, "ENTRADA_ITENS", (i + 1).ToString(), Principal.estAtual, Principal.empAtual, true);


                    if (Funcoes.LeParametro(6, "351", true).Equals("S"))
                    {
                        var vencimento = new ProdutoVencimento
                            (
                                Principal.empAtual,
                                Principal.estAtual,
                                dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                dgItens.Rows[i].Cells["PROD_CONTROLADO"].Value.ToString(),
                                dgItens.Rows[i].Cells["ENT_DTVALIDADE"].Value.ToString() == "" ? Convert.ToDateTime("01/01/2001 00:00:00") : Convert.ToDateTime(dgItens.Rows[i].Cells["ENT_DTVALIDADE"].Value),
                                Convert.ToInt32(dgItens.Rows[i].Cells[6].Value),
                                txtDocto.Text
                            );

                        if (!vencimento.InsereRegistros(vencimento))
                            return false;
                    }

                    idMovimentoEstoque += 1;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
                return false;
            }
        }

        private async Task AtualizarEstoqueFiliais()
        {
            try
            {
                if (Funcoes.LeParametro(9, "51", false).Equals("S"))
                {
                    if (Util.TesteIPServidorNuvem())
                    {
                        var dadosProduto = new Produto();
                        for (int i = 0; i < dgItens.RowCount; i++)
                        {
                            List<Produto> listProduto = dadosProduto.IdentificaProdutoControlado(dgItens.Rows[i].Cells[1].Value.ToString());
                            if (listProduto[0].ProdControlado.Equals("N") || (listProduto[0].ProdControlado.Equals("S") && Funcoes.LeParametro(9, "29", true).Equals("S")))
                            {
                                using (var client = new HttpClient())
                                {
                                    Cursor = Cursors.WaitCursor;
                                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + dgItens.Rows[i].Cells[1].Value.ToString()
                                                    + "&descricao=" + dgItens.Rows[i].Cells[2].Value.ToString()
                                                    + "&qtde=" + Convert.ToInt32(dgItens.Rows[i].Cells[6].Value)
                                                    + "&precoCusto=" + (Convert.ToDouble(dgItens.Rows[i].Cells[10].Value) / Convert.ToInt32(dgItens.Rows[i].Cells[6].Value)).ToString().Replace(",", ".")
                                                    + "&precoVenda=" + Convert.ToDouble(dgItens.Rows[i].Cells[12].Value).ToString().Replace(",", ".") + "&codEstabelecimento=" + Funcoes.LeParametro(9, "52", true) + "&operacao=mais");
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\EntradNota.txt", true))
                                        {
                                            writer.WriteLine("COD. BARRAS: " + dgItens.Rows[i].Cells[1].Value.ToString() + " / QTDE: " + dgItens.Rows[i].Cells[6].Value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private async Task DecrementaEstoqueFiliais()
        {
            try
            {
                if (Funcoes.LeParametro(9, "51", false).Equals("S"))
                {
                    if (Util.TesteIPServidorNuvem())
                    {
                        var entradaItens = new EntradaItens();
                        List<EntradaItens> listBuscaEntradaItens = entradaItens.CarregaEntradasItens(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text));
                        for (int i = 0; i < listBuscaEntradaItens.Count; i++)
                        {
                            using (var client = new HttpClient())
                            {
                                Cursor = Cursors.WaitCursor;
                                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + listBuscaEntradaItens[i].ProdCodigo
                                                + "&descricao=" + listBuscaEntradaItens[i].ItemDescr
                                                + "&qtde=" + listBuscaEntradaItens[i].EntQtdeEstoque
                                                + "&precoCusto=" + (listBuscaEntradaItens[i].EntTotalItem / listBuscaEntradaItens[i].EntQtdeEstoque).ToString().Replace(",", ".")
                                                + "&precoVenda=" + listBuscaEntradaItens[i].EntValorVenda.ToString().Replace(",", ".") + "&codEstabelecimento=" + Funcoes.LeParametro(9, "52", true) + "&operacao=menos");
                                if (!response.IsSuccessStatusCode)
                                {
                                    using (StreamWriter writer = new StreamWriter(@"C:\BTI\EntradNota.txt", true))
                                    {
                                        writer.WriteLine("COD. BARRAS: " + listBuscaEntradaItens[i].ProdCodigo + " / QTDE: " + listBuscaEntradaItens[i].EntQtdeEstoque);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            var deletaSpool = new Spool();
            if (tcEntradas.SelectedTab == tpFicha)
            {
                dgItens.Rows.Clear();
                txtItem.Text = "0";
                txtCodBarras.Text = "";
                txtDescr.Text = "";
                LimparItens();
                LimparFicha();

                deletaSpool.ExcluiRegistrosSpoolEntradasTodas(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
            }
            else
                if (tcEntradas.SelectedTab == tpGrade)
            {
                LimparGrade();
                deletaSpool.ExcluiRegistrosSpoolEntradasTodas(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
            }
            else if (tcEntradas.SelectedTab == tpItens)
            {
                dgItens.Rows.Clear();
                txtItem.Text = "0";
                txtCodBarras.Text = "";
                txtDescr.Text = "";
                LimparItens();
                deletaSpool.ExcluiSpoolEntradasItens(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
            }

            Principal.AtualizaImposto = "S";

            BotoesHabilitados();
        }

        public void LimparItens()
        {
            #region LIMPA AS VARIÁVEIS
            txtDescr.Text = "";
            txtComissao.Text = "0,00";
            txtQtde.Text = "";
            txtUn.Text = "";
            txtEQtde.Text = "";
            txtVUni.Text = "0,00";
            txtPDesc.Text = "0,00";
            txtPDescUni.Text = "0,00";
            txtPVTotal.Text = "0,00";
            txtPMargem.Text = "0,00";
            txtPVenda.Text = "0,00";
            txtLote.Text = "";
            cmbIcms.SelectedIndex = 8;
            txtVBIcms.Text = "0,00";
            txtPVIcms.Text = "0,00";
            txtCstIcms.Text = "";
            txtBIcmsSt.Text = "0,00";
            txtICMSST.Text = "0,00";
            txtBcIcmsRet.Text = "0,00";
            txtICMSRet.Text = "0,00";
            txtEnqIpi.Text = "";
            txtCstIpi.Text = "";
            txtBaseIpi.Text = "0,00";
            txtAliIpi.Text = "0,00";
            cmbOrigem.SelectedIndex = 0;
            txtNCM.Text = "";
            txtCfop.Text = "";
            dtValidade.Value = DateTime.Now;
            txtCstConfis.Text = "";
            txtPVIpi.Text = "0,00";
            txtPisCst.Text = "";
            txtPEstoque.Text = "";
            txtPVCompra.Text = "0,00";
            txtPUCusto.Text = "0,00";
            txtPCMedio.Text = "0,00";
            txtGQtde.Text = "0,00";
            txtGEqtde.Text = "0,00";
            txtGDesc.Text = "0,00";
            txtGTotal.Text = "0,00";
            entradaViaXml = "N";
            txtCest.Text = "";
            Array.Clear(dados, 0, 96);
            cmbDocto.Text = "NF";
            txtCodBarras.Focus();
            #endregion
        }

        public void LimparGrade()
        {
            #region LIMPA AS VARIÁVEIS
            valorParcelas = 0;
            totalIcms = 0;
            totalIpi = 0;
            valorItensFatura = 0;
            dgItens.Rows.Clear();
            dgParcelas.Rows.Clear();
            dtEntradas.Clear();
            dgEntradas.DataSource = dtEntradas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            txtBValor.Text = "0,00";
            emGrade = false;
            Array.Clear(dados, 0, 96);
            LimparItens();
            txtBLancto.Focus();
            #endregion
        }

        public void LimparFicha()
        {
            #region LIMPA AS VARIÁVEIS
            dgParcelas.Rows.Clear();
            txtEntID.Text = "";
            dtLancamento.Focus();
            cmbDocto.Text = "NF";
            cmbNOper.SelectedIndex = 0;
            cmbFBusca.SelectedIndex = 0;
            dtLancamento.Value = DateTime.Now;
            dtEmissao.Value = DateTime.Now;
            dtValidade.Value = DateTime.Now;
            dtPVencimento.Value = DateTime.Now;
            txtDocto.Text = "";
            txtSerie.Text = "";
            txtFatura.Text = "";
            txtNTotal.Text = "0,00";
            txtComp.Text = "";
            cmbComp.SelectedIndex = -1;
            txtBIcms.Text = "0,00";
            txtVIcms.Text = "0,00";
            txtIIcms.Text = "0,00";
            txtVICMSST.Text = "0,00";
            txtVImpImportacao.Text = "0,00";
            txtICMSUFRemet.Text = "0,00";
            txtValorFCP.Text = "0,00";
            txtValorPIS.Text = "0,00";
            txtTotalProdutos.Text = "0,00";
            txtFrete.Text = "0,00";
            txtValorSeguro.Text = "0,00";
            txtValorDesconto.Text = "0,00";
            txtOutrasDespesas.Text = "0,00";
            txtValorIpi.Text = "0,00";
            txtVICMSUFDest.Text = "0,00";
            txtTotalTributos.Text = "0,00";
            txtValorCofins.Text = "0,00";
            txtChaveAcesso.Text = "";
            DataHoraEmissao = "";
            txtObs.Text = "";
            txtNCompra.Text = "";
            txtFBusca.Text = "";
            txtFNome.Text = "";
            txtFCnpj.Text = "";
            txtFCidade.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            txtPTotal.Text = "0,00";
            dtLancamento.Focus();
            valorParcelas = 0;
            totalIcms = 0;
            totalIpi = 0;
            valorItens = 0;
            valorItensFatura = 0;
            Array.Clear(dados, 0, 96);
            entradaViaXml = "N";
            #endregion
        }

        public void CarregarDados(int linha)
        {
            try
            {
                var deleteSpool = new Spool();
                deleteSpool.ExcluiRegistrosSpoolEntradasTodas(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);

                cmbFBusca.SelectedIndex = 0;
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                lblEmpresa.Text = "Empresa: " + Principal.empAtual + " - " + Principal.nomeAtual;
                txtEntID.Text = dtEntradas.Rows[linha]["ENT_ID"].ToString();
                dtLancamento.Value = Convert.ToDateTime(dtEntradas.Rows[linha]["ENT_DTLANC"]);
                cmbDocto.Text = dtEntradas.Rows[linha]["TIP_CODIGO"].ToString();
                dtEmissao.Value = Convert.ToDateTime(dtEntradas.Rows[linha]["ENT_DTEMIS"]);
                txtFCnpj.Text = dtEntradas.Rows[linha]["CF_DOCTO"].ToString();
                txtDocto.Text = dtEntradas.Rows[linha]["ENT_DOCTO"].ToString();
                txtSerie.Text = dtEntradas.Rows[linha]["ENT_SERIE"].ToString();
                txtFatura.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["PAG_DOCTO"].ToString());
                txtNTotal.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_TOTAL"]);
                cmbNOper.Text = dtEntradas.Rows[linha]["NO_CODIGO"].ToString();
                cmbComp.Text = dtEntradas.Rows[linha]["PAG_COMPRADOR"].ToString();
                txtComp.Text = Convert.ToString(Math.Round(Convert.ToDecimal(cmbComp.SelectedValue), 0));
                txtBIcms.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_BASEICMS"]);
                txtVIcms.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_IMPICMS"]);
                txtIIcms.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_BASESUBST"]);
                txtVICMSST.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_SUBST"]);
                txtVImpImportacao.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_ISENTOIPI"]);
                txtICMSUFRemet.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_OUTIPI"]);
                txtValorFCP.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_BASEISS"]);
                txtValorPIS.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_ISENTOICMS"]);
                txtTotalProdutos.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_DIVERSAS"]);
                txtFrete.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_FRETE"]);
                txtValorSeguro.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_IMPFRETE"]);
                txtValorDesconto.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_DESCONTO"]);
                txtOutrasDespesas.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_OUTRAS"]);
                txtValorIpi.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_IMPIPI"]);
                txtVICMSUFDest.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_BASEIPI"]);
                txtTotalTributos.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_IRRF"]);
                txtValorCofins.Text = String.Format("{0:N}", dtEntradas.Rows[linha]["ENT_IMPISS"]);
                txtObs.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["ENT_OBS"].ToString());
                dtPVencimento.Value = DateTime.Now;
                txtData.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["OPALTERACAO"].ToString());
                entradaViaXml = dtEntradas.Rows[linha]["ENT_VIA_XML"].ToString();
                txtChaveAcesso.Text = dtEntradas.Rows[linha]["CHAVE_ACESSO"].ToString();
                DataHoraEmissao = dtEntradas.Rows[linha]["DATA_HORA_EMISSAO"].ToString();
                var buscaFornecedor = new Cliente();
                List<Cliente> cliente = buscaFornecedor.BuscaDadosFornecedor(2, txtFCnpj.Text, 1);

                if (!cliente.Count.Equals(0))
                {
                    txtFNome.Text = cliente[0].CfNome;
                    txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                }

                retorno = Util.SelecionaCampoEspecificoDaTabela("TIPO_DOCTO", "TIP_FATURA", "TIP_CODIGO", Math.Round(Convert.ToDecimal(cmbDocto.SelectedValue), 0).ToString());
                if (!String.IsNullOrEmpty(retorno))
                {
                    if (retorno == "S")
                    {
                        txtFatura.Enabled = true;
                    }
                    else
                    {
                        txtFatura.Text = "";
                        txtFatura.Enabled = false;
                    }
                }

                //CARREGA DADOS DAS PARCELAS//
                dgParcelas.Rows.Clear();
                var entDetalhe = new EntradaDetalhe();

                List<EntradaDetalhe> listBuscaEntradaDetalhe = entDetalhe.CarregaEntradasDetalhe(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text));
                if (listBuscaEntradaDetalhe.Count != 0)
                {
                    valorParcelas = 0;

                    for (int i = 0; i < listBuscaEntradaDetalhe.Count; i++)
                    {
                        dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { listBuscaEntradaDetalhe[i].EntValParc, listBuscaEntradaDetalhe[i].EntVencto.ToString("dd/MM/yyyy") });

                        valorParcelas = valorParcelas + Convert.ToDouble(listBuscaEntradaDetalhe[i].EntValParc);
                    }
                }

                //CARREGA DADOS DOS ITENS//
                dgItens.Rows.Clear();
                totalIcms = 0;
                totalIpi = 0;
                valorItens = 0;
                valorItensFatura = 0;

                var entradaItens = new EntradaItens();
                List<EntradaItens> listBuscaEntradaItens = entradaItens.CarregaEntradasItens(Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text));
                string porcentagem = "";
                //CARREGA DADOS DOS ITENS DA TABELA ENTRADA ITENS//
                for (int i = 0; i < listBuscaEntradaItens.Count; i++)
                {
                    porcentagem = "";

                    txtNCompra.Text = listBuscaEntradaItens[0].CompNumero;

                    //CALCULA VALORES DE DESCONTOS//
                    if (listBuscaEntradaItens[i].EntDescontoItem == 0)
                    {
                        porcentagem = "0,00";
                    }
                    else
                    {
                        porcentagem = String.Format("{0:0.00}", ((listBuscaEntradaItens[i].EntDescontoItem * listBuscaEntradaItens[i].EntQtdeEstoque) * 100) /
                            (listBuscaEntradaItens[i].EntUnitario * listBuscaEntradaItens[i].EntQtdeEstoque));
                    }

                    var produtos = new Produto();
                    dtRetorno = produtos.BuscaComissaoQtdeEstoqueRegistroMS(listBuscaEntradaItens[i].ProdCodigo, Principal.estAtual, Principal.empAtual);

                    dgItens.Rows.Insert(dgItens.RowCount, new Object[] { listBuscaEntradaItens[i].EntSequencia, listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].ItemDescr,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntComissao), listBuscaEntradaItens[i].EntQtde, listBuscaEntradaItens[i].EntUnidade, listBuscaEntradaItens[i].EntQtdeEstoque,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntUnitario), String.Format("{0:N}",porcentagem), String.Format("{0:N}",listBuscaEntradaItens[i].EntDescontoItem),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntTotalItem), String.Format("{0:N}",listBuscaEntradaItens[i].EntMargem),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorVenda), dtRetorno.Rows[0]["PROD_COMISSAO"],String.Format("{0:N}",listBuscaEntradaItens[i].EntPmc),
                                dtRetorno.Rows[0]["PROD_ESTATUAL"], listBuscaEntradaItens[i].EntLote, listBuscaEntradaItens[i].EntICMS,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntAliqIcms), String.Format("{0:N}",listBuscaEntradaItens[i].EntValorBcIcms),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorIcms), listBuscaEntradaItens[i].ProdCST,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntBcIcmsSt), String.Format("{0:N}", listBuscaEntradaItens[i].EntIcmsSt),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntRedBaseIcms), String.Format("{0:N}",listBuscaEntradaItens[i].EntIcmsRet), listBuscaEntradaItens[i].EntEnqIpi,
                                listBuscaEntradaItens[i].EntCstIpi, String.Format("{0:N}",listBuscaEntradaItens[i].EntBaseIpi), String.Format("{0:N}",listBuscaEntradaItens[i].EntAliqIpi),
                                listBuscaEntradaItens[i].EntOrigem, listBuscaEntradaItens[i].ProdNCM, listBuscaEntradaItens[i].ProdCFOP, listBuscaEntradaItens[i].EntCstCofins,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorIpi), listBuscaEntradaItens[i].EntCstPis,
                                listBuscaEntradaItens[i].EntDataValidade == Convert.ToDateTime("01/01/2001 00:00:00") ? "" : listBuscaEntradaItens[i].EntDataValidade.ToString("dd/MM/yyyy"),
                                listBuscaEntradaItens[i].NoCodigo, listBuscaEntradaItens[i].NoComp, listBuscaEntradaItens[i].CompNumero, listBuscaEntradaItens[i].ProdutoNovo, listBuscaEntradaItens[i].EstIndice,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntPreCompra), listBuscaEntradaItens[i].EntProdCest,
                                listBuscaEntradaItens[i].EntRegistroMS == "" ? dtRetorno.Rows[0]["PROD_REGISTRO_MS"] : listBuscaEntradaItens[i].EntRegistroMS});

                    totalIcms = totalIcms + listBuscaEntradaItens[i].EntValorIcms;
                    totalIpi = totalIpi + listBuscaEntradaItens[i].EntValorIpi;
                    valorItens = valorItens + listBuscaEntradaItens[i].EntTotalItem;
                    var natOperacaoFatura = new NatOperacao();
                    if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                    {
                        valorItensFatura = valorItensFatura + listBuscaEntradaItens[i].EntTotalItem + listBuscaEntradaItens[i].EntValorIpi;

                    }
                    TotalizaGrid();
                }

                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, linha));
                txtDocto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNTotal_Validated(object sender, EventArgs e)
        {
            if (txtNTotal.Text.Trim() != "")
            {
                txtNTotal.Text = String.Format("{0:N}", Convert.ToDecimal(txtNTotal.Text));
            }
            else
                txtNTotal.Text = "0,00";
        }

        private void txtPTotal_Validated(object sender, EventArgs e)
        {
            if (txtPTotal.Text.Trim() != "")
            {
                txtPTotal.Text = String.Format("{0:N}", Convert.ToDecimal(txtPTotal.Text));
            }
            else
                txtPTotal.Text = "0,00";
        }

        private void txtBIcms_Validated(object sender, EventArgs e)
        {
            if (txtBIcms.Text.Trim() != "")
            {
                txtBIcms.Text = String.Format("{0:N}", Convert.ToDecimal(txtBIcms.Text));
            }
            else
                txtBIcms.Text = "0,00";
        }

        private void txtVIcms_Validated(object sender, EventArgs e)
        {
            if (txtVIcms.Text.Trim() != "")
            {
                txtVIcms.Text = String.Format("{0:N}", Convert.ToDecimal(txtVIcms.Text));
            }
            else
                txtVIcms.Text = "0,00";
        }

        private void txtIIcms_Validated(object sender, EventArgs e)
        {
            if (txtIIcms.Text.Trim() != "")
            {
                txtIIcms.Text = String.Format("{0:N}", Convert.ToDecimal(txtIIcms.Text));
            }
            else
                txtIIcms.Text = "0,00";
        }

        private void txtVOutras_Validated(object sender, EventArgs e)
        {
            if (txtVICMSST.Text.Trim() != "")
            {
                txtVICMSST.Text = String.Format("{0:N}", Convert.ToDecimal(txtVICMSST.Text));
            }
            else
                txtVICMSST.Text = "0,00";
        }

        private void txtVDiversos_Validated(object sender, EventArgs e)
        {
            if (txtVImpImportacao.Text.Trim() != "")
            {
                txtVImpImportacao.Text = String.Format("{0:N}", Convert.ToDecimal(txtVImpImportacao.Text));
            }
            else
                txtVImpImportacao.Text = "0,00";
        }

        private void txtBIpi_Validated(object sender, EventArgs e)
        {
            if (txtICMSUFRemet.Text.Trim() != "")
            {
                txtICMSUFRemet.Text = String.Format("{0:N}", Convert.ToDecimal(txtICMSUFRemet.Text));
            }
            else
                txtICMSUFRemet.Text = "0,00";
        }

        private void txtVIpi_Validated(object sender, EventArgs e)
        {
            if (txtValorFCP.Text.Trim() != "")
            {
                txtValorFCP.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorFCP.Text));
            }
            else
                txtValorFCP.Text = "0,00";
        }

        private void txtIIpi_Validated(object sender, EventArgs e)
        {
            if (txtValorPIS.Text.Trim() != "")
            {
                txtValorPIS.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorPIS.Text));
            }
            else
                txtValorPIS.Text = "0,00";
        }

        private void txtIOutros_Validated(object sender, EventArgs e)
        {
            if (txtTotalProdutos.Text.Trim() != "")
            {
                txtTotalProdutos.Text = String.Format("{0:N}", Convert.ToDecimal(txtTotalProdutos.Text));
            }
            else
                txtTotalProdutos.Text = "0,00";
        }

        private void txtBSubst_Validated(object sender, EventArgs e)
        {
            if (txtFrete.Text.Trim() != "")
            {
                txtFrete.Text = String.Format("{0:N}", Convert.ToDecimal(txtFrete.Text));
            }
            else
                txtFrete.Text = "0,00";
        }

        private void txtSTrib_Validated(object sender, EventArgs e)
        {
            if (txtValorSeguro.Text.Trim() != "")
            {
                txtValorSeguro.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorSeguro.Text));
            }
            else
                txtValorSeguro.Text = "0,00";
        }

        private void txtBIss_Validated(object sender, EventArgs e)
        {
            if (txtValorDesconto.Text.Trim() != "")
            {
                txtValorDesconto.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorDesconto.Text));
            }
            else
                txtValorDesconto.Text = "0,00";
        }

        private void txtVIss_Validated(object sender, EventArgs e)
        {
            if (txtOutrasDespesas.Text.Trim() != "")
            {
                txtOutrasDespesas.Text = String.Format("{0:N}", Convert.ToDecimal(txtOutrasDespesas.Text));
            }
            else
                txtOutrasDespesas.Text = "0,00";
        }

        private void txtVIrrf_Validated(object sender, EventArgs e)
        {
            if (txtValorIpi.Text.Trim() != "")
            {
                txtValorIpi.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorIpi.Text));
            }
            else
                txtValorIpi.Text = "0,00";
        }

        private void txtVFrete_Validated(object sender, EventArgs e)
        {
            if (txtVICMSUFDest.Text.Trim() != "")
            {
                txtVICMSUFDest.Text = String.Format("{0:N}", Convert.ToDecimal(txtVICMSUFDest.Text));
            }
            else
                txtVICMSUFDest.Text = "0,00";
        }

        private void txtIFrete_Validated(object sender, EventArgs e)
        {
            if (txtTotalTributos.Text.Trim() != "")
            {
                txtTotalTributos.Text = String.Format("{0:N}", Convert.ToDecimal(txtTotalTributos.Text));
            }
            else
                txtTotalTributos.Text = "0,00";
        }

        private void txtDesconto_Validated(object sender, EventArgs e)
        {
            if (txtValorCofins.Text.Trim() != "")
            {
                txtValorCofins.Text = String.Format("{0:N}", Convert.ToDecimal(txtValorCofins.Text));
            }
            else
                txtValorCofins.Text = "0,00";
        }


        public bool InserirSpoolParcelas()
        {
            valorParcelas = 0;
            var deleteSpool = new Spool();
            deleteSpool.ExcluiSpoolEntradasDetalhe(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);

            //INSERE PARCELAS NA TABELA SPOOL//
            for (int i = 0; i < dgParcelas.RowCount; i++)
            {
                var entDetalhe = new EntradaDetalhe(
                    Principal.estAtual,
                    Principal.empAtual,
                    Convert.ToInt32(txtEntID.Text),
                    Funcoes.RemoveCaracter(txtFCnpj.Text),
                    txtDocto.Text.Trim().ToUpper(),
                    txtSerie.Text.Trim().ToUpper(),
                    (i + 1),
                    Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value),
                    Convert.ToDateTime(dgParcelas.Rows[i].Cells[1].Value),
                    txtFatura.Text.Trim().ToUpper(),
                    DateTime.Now,
                    Principal.usuario,
                    DateTime.Now,
                    Principal.usuario
                );

                if (deleteSpool.InsereRegistrosSpoolEntradaDetalhe(entDetalhe, Principal.nomeEstacao).Equals(false))
                    return false;

                valorParcelas = valorParcelas + Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);

            }
            return true;
        }


        public bool InserirSpoolNF()
        {
            if (String.IsNullOrEmpty(txtEntID.Text))
            {
                txtEntID.Text = Funcoes.LeParametro(9, "50", true);
                Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString());
            }

            campos = cmbNOper.Text.Split('-');
            campos = campos[0].Split('/');
            var entSpool = new Entrada(
                Principal.estAtual,
                Principal.empAtual,
                Convert.ToInt32(txtEntID.Text),
                dtLancamento.Value.ToString(),
                Convert.ToInt32(cmbDocto.SelectedValue),
                dtEmissao.Value.ToString(),
                txtFCnpj.Text,
                txtDocto.Text.Trim(),
                txtSerie.Text.Trim().ToUpper(),
                Convert.ToInt32(campos[0].Trim()),
                Convert.ToInt32(campos[1].Trim()),
                txtFatura.Text.Trim().ToUpper(),
                Convert.ToDouble(txtNTotal.Text),
                Convert.ToDouble(txtBIcms.Text),
                Convert.ToDouble(txtVIcms.Text),
                Convert.ToDouble(txtIIcms.Text),
                Convert.ToDouble(txtVICMSST.Text),
                Convert.ToDouble(txtVImpImportacao.Text),
                Convert.ToDouble(txtICMSUFRemet.Text),
                Convert.ToDouble(txtValorFCP.Text),
                Convert.ToDouble(txtValorPIS.Text),
                Convert.ToDouble(txtTotalProdutos.Text),
                Convert.ToDouble(txtFrete.Text),
                Convert.ToDouble(txtValorSeguro.Text),
                Convert.ToDouble(txtValorDesconto.Text),
                Convert.ToDouble(txtOutrasDespesas.Text),
                Convert.ToDouble(txtValorIpi.Text),
                Convert.ToDouble(txtVICMSUFDest.Text),
                Convert.ToDouble(txtVICMSUFDest.Text),
                Convert.ToDouble(txtValorCofins.Text),
                Convert.ToInt32(cmbComp.SelectedValue),
                txtNCompra.Text.Trim(),
                "N",
                txtObs.Text.Trim().Length > 1000 ? txtObs.Text.Trim().Substring(0, 999) : txtObs.Text.Trim(),
                entradaViaXml,
                DateTime.Now,
                Principal.usuario,
                DateTime.Now,
                Principal.usuario,
                txtChaveAcesso.Text,
                DataHoraEmissao
            );

            var spoolEntrada = new Spool();
            spoolEntrada.ExcluiSpoolEntrada(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);

            if (spoolEntrada.InsereRegistrosSpoolEntrada(entSpool, Principal.nomeEstacao).Equals(false))
            {
                return false;
            }

            return true;
        }

        public bool InserirSpoolItens(int indice)
        {
            var spoolEntradaItens = new Spool();
            spoolEntradaItens.ExcluiSpoolEntradasItens(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual, 0, dgItens.Rows[indice].Cells[1].Value.ToString());

            var entItens = new EntradaItens(
                Principal.empAtual,
                Principal.estAtual,
                Convert.ToInt32(txtEntID.Text),
                txtFCnpj.Text,
                txtDocto.Text.Trim(),
                txtSerie.Text.Trim(),
                Convert.ToInt32(dgItens.Rows[indice].Cells["ENT_SEQUENCIA"].Value),
                dgItens.Rows[indice].Cells["PROD_CODIGO"].Value.ToString(),
                dgItens.Rows[indice].Cells["PROD_DESCR"].Value.ToString(),
                "E",
                Convert.ToInt32(dgItens.Rows[indice].Cells["ENT_QTDESTOQUE"].Value),
                Convert.ToInt32(dgItens.Rows[indice].Cells["QTDE"].Value),
                dgItens.Rows[indice].Cells["ENT_UNIDADE"].Value.ToString(),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_UNITARIO"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_DESCONTOITEM"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_TOTITEM"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["PROD_MARGEM"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["PRE_VALOR"].Value),
                dgItens.Rows[indice].Cells["PROD_CONTROLADO"].Value.ToString(),
                dgItens.Rows[indice].Cells["ENT_ICMS"].Value.ToString(),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_ALIQICMS"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_VALOR_BC_ICMS"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_VALORICMS"].Value),
                dgItens.Rows[indice].Cells["PROD_CST"].Value.ToString(),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_BC_ICMS_ST"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_ICMS_ST"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_REDUCAOBASEICMS"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_ICMS_RET"].Value),
                dgItens.Rows[indice].Cells["ENT_ENQ_IPI"].Value.ToString(),
                dgItens.Rows[indice].Cells["ENT_CST_IPI"].Value.ToString(),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_BASE_IPI"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_ALIQIPI"].Value),
                Convert.ToDouble(dgItens.Rows[indice].Cells["ENT_VALOR_IPI"].Value),
                Convert.ToInt32(dgItens.Rows[indice].Cells["ENT_ORIGEM"].Value),
                dgItens.Rows[indice].Cells["ENT_NCM"].Value.ToString(),
                dgItens.Rows[indice].Cells["PROD_CFOP"].Value.ToString(),
                dgItens.Rows[indice].Cells["ENT_CST_COFINS"].Value.ToString(),
                dgItens.Rows[indice].Cells["ENT_CST_PIS"].Value.ToString(),
                dgItens.Rows[indice].Cells["NO_CODIGO"].Value.ToString(),
                dgItens.Rows[indice].Cells["NO_COMP"].Value.ToString(),
                Convert.ToDouble(dgItens.Rows[indice].Cells["PROD_COMISSAO"].Value),
                 dgItens.Rows[indice].Cells["COMP_NUMERO"].Value.ToString(),
                dgItens.Rows[indice].Cells["ENT_DTVALIDADE"].Value == null ? Convert.ToDateTime("01/01/2001 00:00:00") : dgItens.Rows[indice].Cells["ENT_DTVALIDADE"].Value.ToString() == "" ? Convert.ToDateTime("01/01/2001 00:00:00") : Convert.ToDateTime(dgItens.Rows[indice].Cells["ENT_DTVALIDADE"].Value),
                dgItens.Rows[indice].Cells["PROD_CADASTRADO"].Value.ToString(),
                0,
                Convert.ToDouble(dgItens.Rows[indice].Cells["PROD_PRECOMPRA"].Value),
                (dgItens.Rows[indice].Cells["PROD_CEST"].Value.ToString() == "" ? "" : dgItens.Rows[indice].Cells["PROD_CEST"].Value.ToString()),
                Convert.ToDouble(dgItens.Rows[indice].Cells["VALOR_PMC"].Value),
                dgItens.Rows[indice].Cells["PROD_REGMS"].Value.ToString(),
                DateTime.Now,
                Principal.usuario,
                DateTime.Now,
                Principal.usuario
                );
            if (spoolEntradaItens.InsereRegistrosSpoolEntradaItens(entItens, Principal.nomeEstacao).Equals(false))
            {
                return false;
            }

            return true;
        }

        public bool CarregaSpool()
        {
            try
            {
                var buscaSpool = new Spool();
                List<Entrada> listBuscaEntrada = buscaSpool.CarregaSpoolEntrada(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
                List<EntradaDetalhe> listBuscaEntradaDetalhe = buscaSpool.CarregaSpoolEntradasDetalhe(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
                List<EntradaItens> listBuscaEntradaItens = buscaSpool.CarregaSpoolEntradasItens(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);


                if (listBuscaEntrada.Count != 0 || listBuscaEntradaDetalhe.Count != 0 || listBuscaEntradaItens.Count != 0)
                {
                    if (MessageBox.Show("Existem lançamentos não concluídos. Deseja recuperá-los?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                    {
                        tcEntradas.SelectedTab = tpFicha;
                        //CARREGA DADOS DA NOTA//
                        if (listBuscaEntrada.Count != 0)
                        {
                            txtEntID.Text = listBuscaEntrada[0].EntId.ToString();
                            dtLancamento.Value = Convert.ToDateTime(listBuscaEntrada[0].EntDtLanc);
                            cmbDocto.SelectedValue = listBuscaEntrada[0].TipCodigo;
                            dtEmissao.Value = Convert.ToDateTime(listBuscaEntrada[0].EntDtEmis);
                            txtDocto.Text = listBuscaEntrada[0].EntDocto.ToString();
                            txtSerie.Text = listBuscaEntrada[0].EntSerie.ToString();
                            txtFatura.Text = listBuscaEntrada[0].PagDocto.ToString();
                            txtNTotal.Text = String.Format("{0:N}", listBuscaEntrada[0].EntTotal);
                            txtComp.Text = listBuscaEntrada[0].ColCodigo.ToString();
                            if (!String.IsNullOrEmpty(listBuscaEntrada[0].ColCodigo.ToString()))
                            {
                                cmbComp.SelectedValue = listBuscaEntrada[0].ColCodigo.ToString();
                            }
                            txtBIcms.Text = String.Format("{0:N}", listBuscaEntrada[0].EntBaseIcms);
                            txtVIcms.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorIcms);
                            txtIIcms.Text = String.Format("{0:N}", listBuscaEntrada[0].EntBaseIcmsST);
                            txtVICMSST.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorIcmsSt);
                            txtVImpImportacao.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorImpImportacao);
                            txtICMSUFRemet.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorIcmsUFRemet);
                            txtValorFCP.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorFCP);
                            txtValorPIS.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorPIS);
                            txtTotalProdutos.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorProdutos);
                            txtFrete.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorFrete);
                            txtValorSeguro.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorSeguro);
                            txtValorDesconto.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorDesconto);
                            txtOutrasDespesas.Text = String.Format("{0:N}", listBuscaEntrada[0].EntOutasDespesas);
                            txtValorIpi.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorIPI);
                            txtVICMSUFDest.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorICMSUFDest);
                            txtTotalTributos.Text = String.Format("{0:N}", listBuscaEntrada[0].EntTotalTributos);
                            txtValorCofins.Text = String.Format("{0:N}", listBuscaEntrada[0].EntValorCofins);
                            txtObs.Text = listBuscaEntrada[0].EntObs;
                            entradaViaXml = listBuscaEntrada[0].EntViaXML.ToString();
                            dtPVencimento.Value = DateTime.Now;
                            txtData.Text = listBuscaEntrada[0].DtAlteracao.ToString();
                            txtUsuario.Text = listBuscaEntrada[0].OpAlteracao;
                            txtFCnpj.Text = listBuscaEntrada[0].CfDocto;
                            txtChaveAcesso.Text = listBuscaEntrada[0].ChaveAcesso;
                            var buscaFornecedor = new Cliente();
                            List<Cliente> cliente = buscaFornecedor.BuscaDadosFornecedor(2, txtFCnpj.Text, 1);
                            if (!cliente.Count.Equals(0))
                            {
                                txtFNome.Text = cliente[0].CfNome;
                                txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                            }
                            cmbNOper.SelectedValue = listBuscaEntrada[0].NoCodigo;

                            retorno = Util.SelecionaCampoEspecificoDaTabela("TIPO_DOCTO", "TIP_FATURA", "TIP_CODIGO", Math.Round(Convert.ToDecimal(cmbDocto.SelectedValue), 0).ToString());
                            if (!String.IsNullOrEmpty(retorno))
                            {
                                if (retorno == "S")
                                {
                                    txtFatura.Enabled = true;
                                }
                                else
                                {
                                    txtFatura.Text = "";
                                    txtFatura.Enabled = false;
                                }
                            }
                            dtLancamento.Focus();
                        }

                        //CARREGA DADOS DAS PARCELAS//
                        if (listBuscaEntradaDetalhe.Count != 0)
                        {
                            valorParcelas = 0;

                            for (int i = 0; i < listBuscaEntradaDetalhe.Count; i++)
                            {
                                dgParcelas.Rows.Insert(dgParcelas.RowCount, new Object[] { listBuscaEntradaDetalhe[i].EntValParc, listBuscaEntradaDetalhe[i].EntVencto.ToString("dd/MM/yyyy") });

                                valorParcelas = valorParcelas + Convert.ToDouble(listBuscaEntradaDetalhe[i].EntValParc);
                            }
                        }

                        string porcentagem = "";
                        //CARREGA DADOS DOS ITENS DA TABELA ENTRADA ITENS//
                        for (int i = 0; i < listBuscaEntradaItens.Count; i++)
                        {
                            porcentagem = "";

                            txtNCompra.Text = listBuscaEntradaItens[0].CompNumero;

                            //CALCULA VALORES DE DESCONTOS//
                            if (listBuscaEntradaItens[i].EntDescontoItem == 0)
                            {
                                porcentagem = "0,00";
                            }
                            else
                            {
                                porcentagem = String.Format("{0:0.00}", ((listBuscaEntradaItens[i].EntDescontoItem * listBuscaEntradaItens[i].EntQtdeEstoque) * 100) /
                                    (listBuscaEntradaItens[i].EntUnitario * listBuscaEntradaItens[i].EntQtdeEstoque));
                            }

                            var produtos = new Produto();
                            dtRetorno = produtos.BuscaComissaoQtdeEstoqueRegistroMS(listBuscaEntradaItens[i].ProdCodigo, Principal.estAtual, Principal.empAtual);

                            dgItens.Rows.Insert(dgItens.RowCount, new Object[] { listBuscaEntradaItens[i].EntSequencia, listBuscaEntradaItens[i].ProdCodigo, listBuscaEntradaItens[i].ItemDescr,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntComissao), listBuscaEntradaItens[i].EntQtde, listBuscaEntradaItens[i].EntUnidade, listBuscaEntradaItens[i].EntQtdeEstoque,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntUnitario), String.Format("{0:N}",porcentagem), String.Format("{0:N}",listBuscaEntradaItens[i].EntDescontoItem),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntTotalItem), String.Format("{0:N}",listBuscaEntradaItens[i].EntMargem),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorVenda), dtRetorno.Rows[0]["PROD_COMISSAO"],String.Format("{0:N}",listBuscaEntradaItens[i].EntPmc),
                                dtRetorno.Rows[0]["PROD_ESTATUAL"], listBuscaEntradaItens[i].EntLote, listBuscaEntradaItens[i].EntICMS,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntAliqIcms), String.Format("{0:N}",listBuscaEntradaItens[i].EntValorBcIcms),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorIcms), listBuscaEntradaItens[i].ProdCST,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntBcIcmsSt), String.Format("{0:N}", listBuscaEntradaItens[i].EntIcmsSt),
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntRedBaseIcms), String.Format("{0:N}",listBuscaEntradaItens[i].EntIcmsRet), listBuscaEntradaItens[i].EntEnqIpi,
                                listBuscaEntradaItens[i].EntCstIpi, String.Format("{0:N}",listBuscaEntradaItens[i].EntBaseIpi), String.Format("{0:N}",listBuscaEntradaItens[i].EntAliqIpi),
                                listBuscaEntradaItens[i].EntOrigem, listBuscaEntradaItens[i].ProdNCM, listBuscaEntradaItens[i].ProdCFOP, listBuscaEntradaItens[i].EntCstCofins,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntValorIpi), listBuscaEntradaItens[i].EntCstPis,
                                listBuscaEntradaItens[i].EntDataValidade == Convert.ToDateTime("01/01/2001 00:00:00") ? "" : listBuscaEntradaItens[i].EntDataValidade.ToString("dd/MM/yyyy"),
                                listBuscaEntradaItens[i].NoCodigo, listBuscaEntradaItens[i].NoComp, listBuscaEntradaItens[i].CompNumero, listBuscaEntradaItens[i].ProdutoNovo, listBuscaEntradaItens[i].EstIndice,
                                String.Format("{0:N}",listBuscaEntradaItens[i].EntPreCompra),listBuscaEntradaItens[i].EntProdCest,
                                listBuscaEntradaItens[i].EntRegistroMS == "" ? dtRetorno.Rows[0]["PROD_REGISTRO_MS"] : listBuscaEntradaItens[i].EntRegistroMS });

                            totalIcms = totalIcms + listBuscaEntradaItens[i].EntValorIcms;
                            totalIpi = totalIpi + listBuscaEntradaItens[i].EntValorIpi;
                            valorItens = valorItens + listBuscaEntradaItens[i].EntTotalItem;
                            var natOperacaoFatura = new NatOperacao();
                            if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                            {
                                valorItensFatura = valorItensFatura + listBuscaEntradaItens[i].EntTotalItem + listBuscaEntradaItens[i].EntValorIpi;

                            }
                            TotalizaGrid();
                        }
                    }
                    else
                    {
                        buscaSpool.ExcluiRegistrosSpoolEntradasTodas(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async void IncluirRegistros()
        {
            try
            {
                int id;
                BancoDados.AbrirTrans();

                #region INSERE OS DADOS DA NF NA TABELA ENTRADA
                campos = cmbNOper.Text.Split('-');
                campos = campos[0].Split('/');
                idNOTA = txtEntID.Text;
                var entradaDados = new Entrada(
                    Principal.estAtual,
                    Principal.empAtual,
                    Convert.ToInt32(txtEntID.Text),
                    dtLancamento.Value.ToString(),
                    Convert.ToInt32(cmbDocto.SelectedValue),
                    dtEmissao.Value.ToString(),
                    txtFCnpj.Text,
                    txtDocto.Text.Trim(),
                    txtSerie.Text.Trim().ToUpper(),
                    Convert.ToInt32(campos[0].Trim()),
                    Convert.ToInt32(campos[1].Trim()),
                    txtFatura.Text.Trim().ToUpper(),
                    Convert.ToDouble(txtNTotal.Text),
                    Convert.ToDouble(txtBIcms.Text),
                    Convert.ToDouble(txtVIcms.Text),
                    Convert.ToDouble(txtIIcms.Text),
                    Convert.ToDouble(txtVICMSST.Text),
                    Convert.ToDouble(txtVImpImportacao.Text),
                    Convert.ToDouble(txtICMSUFRemet.Text),
                    Convert.ToDouble(txtValorFCP.Text),
                    Convert.ToDouble(txtValorPIS.Text),
                    Convert.ToDouble(txtTotalProdutos.Text),
                    Convert.ToDouble(txtFrete.Text),
                    Convert.ToDouble(txtValorSeguro.Text),
                    Convert.ToDouble(txtValorDesconto.Text),
                    Convert.ToDouble(txtOutrasDespesas.Text),
                    Convert.ToDouble(txtValorIpi.Text),
                    Convert.ToDouble(txtVICMSUFDest.Text),
                    Convert.ToDouble(txtTotalTributos.Text),
                    Convert.ToDouble(txtValorCofins.Text),
                    Convert.ToInt32(cmbComp.SelectedValue),
                    txtNCompra.Text.Trim(),
                    "N",
                    txtObs.Text.Trim().Length > 1000 ? Funcoes.RemoverAcentuacao(txtObs.Text.Trim().Substring(0, 999)) : Funcoes.RemoverAcentuacao(txtObs.Text.Trim()),
                    entradaViaXml,
                    DateTime.Now,
                    Principal.usuario,
                    DateTime.Now,
                    Principal.usuario,
                    txtChaveAcesso.Text,
                    DataHoraEmissao
                );
                if (!entradaDados.InsereRegistrosEntrada(entradaDados))
                {
                    validaInsert = false;
                    return;
                }
                else
                {
                    if (!Funcoes.GravaLogInclusao("ENT_ID", txtEntID.Text, Principal.usuario, "ENTRADA", txtDocto.Text.Trim().ToUpper(), Principal.estAtual, Principal.empAtual, true))
                    {
                        validaInsert = false;
                        return;
                    }
                }
                #endregion

                #region INSERE OS DADOS DA NF NA TABELA ENTRADA DETALHE
                for (int i = 0; i < dgParcelas.RowCount; i++)
                {
                    var entDetalhe = new EntradaDetalhe(
                        Principal.estAtual,
                        Principal.empAtual,
                        Convert.ToInt32(txtEntID.Text),
                        txtFCnpj.Text,
                        txtDocto.Text.Trim().ToUpper(),
                        txtSerie.Text.Trim().ToUpper(),
                        (i + 1),
                        Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value),
                        Convert.ToDateTime(dgParcelas.Rows[i].Cells[1].Value),
                        txtFatura.Text.Trim().ToUpper(),
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario
                    );

                    if (!entDetalhe.InsereRegistrosEntradaDetalhe(entDetalhe))
                    {
                        validaInsert = false;
                        return;
                    }
                    else
                    {
                        if (!Funcoes.GravaLogInclusao("ENT_ID", txtEntID.Text, Principal.usuario, "ENTRADA_DETALHES", Funcoes.BValor(Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value)), Principal.estAtual, Principal.empAtual, true))
                        {
                            validaInsert = false;
                            return;
                        }
                    }

                    valorParcelas = valorParcelas + Convert.ToDouble(dgParcelas.Rows[i].Cells[0].Value);

                }
                #endregion

                if (!InsereRegistrosEntradaItens())
                {
                    validaInsert = false;
                    return;
                }

                if (txtFatura.Enabled.Equals(true))
                {
                    id = Funcoes.IdentificaVerificaID("PAGAR", "PAG_CODIGO", 0, "", Principal.empAtual);

                    var dadosPagar = new Pagar
                    {
                        EmpCodigo = Principal.empAtual,
                        CfDocto = txtFCnpj.Text,
                        PagCodigo = id,
                        PagDocto = txtFatura.Text.Trim().ToUpper(),
                        PagData = dtEmissao.Value,
                        PagValorPrincipal = Convert.ToDouble(txtNTotal.Text),
                        PagComprador = Convert.ToInt32(txtComp.Text),
                        TipoCodigo = Convert.ToInt32(cmbDocto.SelectedValue),
                        PagListou = "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        PagOrigem = "E",
                        PagStatus = "N",
                        DespCodigo = 4,
                        Historico = "ENTRADA DE NOTAS"
                    };

                    if (!dadosPagar.InsereRegistroFatura(dadosPagar))
                    {
                        validaInsert = false;
                        return;
                    }
                    else
                        Funcoes.GravaLogInclusao("PAG_CODIGO", dadosPagar.PagCodigo.ToString(), dadosPagar.OpCadastro, "PAGAR", dadosPagar.PagDocto, Principal.estAtual, dadosPagar.EmpCodigo, true);

                    List<PagarDetalhe> detalhe = new List<Negocio.PagarDetalhe>();
                    for (int x = 0; x < dgParcelas.RowCount; x++)
                    {
                        var dadosPagarDetalhe = new PagarDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            PagCodigo = id,
                            PagDocto = txtFatura.Text.Trim().ToUpper(),
                            CfDocto = txtFCnpj.Text,
                            PdParcela = (x + 1),
                            PdValor = Convert.ToDouble(dgParcelas.Rows[x].Cells[0].Value),
                            PdVencimento = Convert.ToDateTime(dgParcelas.Rows[x].Cells[1].Value),
                            PdSaldo = Convert.ToDouble(dgParcelas.Rows[x].Cells[0].Value),
                            PdStatus = "N",
                            DtCadastro = DateTime.Now,
                            OpCadastro = Principal.usuario,
                        };

                        detalhe.Add(dadosPagarDetalhe);

                        if (!dadosPagarDetalhe.InsereRegistroFaturaDetalhe(dadosPagarDetalhe))
                        {
                            validaInsert = false;
                            return;
                        }
                        else
                            Funcoes.GravaLogInclusao("PAG_CODIGO", dadosPagarDetalhe.PagCodigo.ToString(), dadosPagarDetalhe.OpCadastro, "PAGAR_DETALHE", dadosPagarDetalhe.PdValor.ToString(), Principal.estAtual, dadosPagarDetalhe.EmpCodigo, true);
                    }

                    //ENVIA DADOS PRA NUVEM//
                    IncluiPagaEPagarDetalheNuvem(dadosPagar, detalhe);

                }

                await AtualizarEstoqueFiliais();

                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                dtEntradas.Clear();
                dgEntradas.DataSource = dtEntradas;
                emGrade = false;
                tslRegistros.Text = "";
                tcEntradas.SelectedTab = tpFicha;
                Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);
                Limpar();
                return;
            }
            catch (Exception ex)
            {
                validaInsert = false;
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        public async void IncluiPagaEPagarDetalheNuvem(Pagar dados, List<PagarDetalhe> detalhePagto)
        {
            await EnviaOuRecebeDadosMysql.InserePagarEPagarDetalhe(dados, detalhePagto);
        }

        public async void ExcluirPagaEPagarDetalheNuvem(string IDNuvem)
        {
            await EnviaOuRecebeDadosMysql.ExcluiPagarEPagarDetalhe(IDNuvem);
        }

        private void dgItens_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgItens.RowCount != 0)
            {
                for (int i = 0; i < dgItens.Rows.Count; i++)
                {
                    dgItens.Rows[i].Cells[0].Value = (i + 1);

                    var prodEntItens = new EntradaItens
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        EntId = Convert.ToInt32(txtEntID.Text),
                        EntSequencia = Convert.ToInt32(dgItens.Rows[i].Cells["ENT_SEQUENCIA"].Value),
                        ProdCodigo = dgItens.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                    };

                    prodEntItens.AtualizaSequenciaEntradaItens(prodEntItens);

                    var atualizaSpool = new Spool();
                    atualizaSpool.AtualizaSequenciaSpoolEntradaItens(prodEntItens, Principal.nomeEstacao);
                }
            }
            TotalizaGrid();

        }

        private void dgItens_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void txtBLancto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBLancto.Text.Replace("/", "").Trim()))
                {
                    cmbBDocto.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!cmbBComp.SelectedIndex.Equals(-1))
                {
                    txtBEmissao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBEmissao.Text.Replace("/", "").Trim()))
                {
                    txtBNumero.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNumero.Text.Trim()))
                {
                    txtBSerie.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBSerie_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBSerie.Text.Trim()))
                {
                    txtBFatura.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBFatura_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBFatura.Text.Trim()))
                {
                    txtBValor.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtBValor.Text.Equals("0,00"))
                {
                    cmbBNOper.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBNOper_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!cmbBNOper.SelectedIndex.Equals(-1))
                {
                    cmbBComp.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBComp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!cmbBComp.SelectedIndex.Equals(-1))
                {
                    txtBCnpj.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var entrada = new Entrada();

                LimparFicha();
                LimparItens();

                bool todos = true;
                bool semFiltro = false;

                if (Funcoes.RemoveCaracter(txtBLancto.Text) == "" && cmbBDocto.SelectedIndex == -1 && Funcoes.RemoveCaracter(txtBEmissao.Text) == "" && txtBNumero.Text.Trim() == "" && txtBSerie.Text.Trim() == ""
                    && txtBFatura.Text.Trim() == "" && Convert.ToDouble(txtBValor.Text) == 0 && cmbBNOper.SelectedIndex == -1 && cmbBComp.SelectedIndex == -1
                        && Funcoes.RemoveCaracter(txtBCnpj.Text) == "")
                {
                    if (MessageBox.Show("Está solicitando uma busca sem passar por nenhum filtro\neste processo pode demorar dependendo da quantidade de\nregistros encontados.\nConfirma Solicitação?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        todos = true;
                        semFiltro = true;
                    }
                    else
                    {
                        txtBLancto.Focus();
                        todos = false;
                    }
                }

                if (todos.Equals(true))
                {
                    dtEntradas = entrada.BuscarEntradaDeNotas(Principal.empAtual, Principal.estAtual, txtBLancto.Text, cmbBDocto.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBDocto.SelectedValue),
                    txtBEmissao.Text, txtBNumero.Text.Trim(), txtBSerie.Text.Trim(), txtBFatura.Text.Trim(), Convert.ToDouble(txtBValor.Text),
                     cmbBNOper.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBNOper.SelectedValue), cmbBComp.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbBComp.SelectedValue),
                     txtBCnpj.Text, out strOrdem, semFiltro);
                    if (dtEntradas.Rows.Count > 0)
                    {
                        tslRegistros.Text = "Registros encontrados: " + dtEntradas.Rows.Count;
                        dgEntradas.DataSource = dtEntradas;
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, 0));
                        dgEntradas.Focus();
                        contRegistros = 0;
                        emGrade = true;
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado.", "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgEntradas.DataSource = dtEntradas;
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        Funcoes.LimpaFormularios(this);
                        txtBValor.Text = "0,00";
                        cmbBDocto.SelectedIndex = -1;
                        cmbBNOper.SelectedIndex = -1;
                        cmbBComp.SelectedIndex = -1;
                        emGrade = false;
                        tslRegistros.Text = "";
                        txtBLancto.Focus();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBValor_Validated(object sender, EventArgs e)
        {
            if (txtBValor.Text.Trim() != "")
            {
                txtBValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtBValor.Text));
            }
            else
                txtBValor.Text = "0,00";
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntradas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEntradas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEntradas.RowCount;
                    for (i = 0; i <= dgEntradas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEntradas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEntradas.ColumnCount; k++)
                    {
                        if (dgEntradas.Columns[k].Visible == true)
                        {
                            switch (dgEntradas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Base ICMS":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor ICMS":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "B. Cal. ICMS ST":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor ICMS ST":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. Diversos":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. Imp. Importação":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. ICMS UF Remet.":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor do FCP":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor do PIS":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. Total Produtos":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor Frete":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor do Seguro":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Desconto":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Outras Despesas":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. Total IPI":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. ICMS UF Dest.":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Vl. Total Trib.":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor da CONFIS":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                                case "Valor Total":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEntradas.ColumnCount : indice) + Convert.ToString(dgEntradas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgEntradas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
            emGrade = true;
        }

        private void dgEntradas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEntradas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEntradas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEntradas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEntradas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEntradas.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgEntradas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEntradas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEntradas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntradas.Columns[e.ColumnIndex].Name + " DESC");
                        dgEntradas.DataSource = dtEntradas;
                        decrescente = true;
                    }
                    else
                    {
                        dtEntradas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntradas.Columns[e.ColumnIndex].Name + " ASC");
                        dgEntradas.DataSource = dtEntradas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgItens_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (entradaViaXml.Equals("N") || Funcoes.LeParametro(9, "47", false).Equals("S"))
                {
                    Cursor = Cursors.WaitCursor;

                    var spoolEntrada = new Spool();

                    var entItens = new EntradaItens
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        EntId = Convert.ToInt32(txtEntID.Text),
                        EntSequencia = Convert.ToInt32(dgItens.Rows[indiceGrid].Cells["ENT_SEQUENCIA"].Value),
                    };
                    if (!String.IsNullOrEmpty(entItens.BuscarIndicePorSequenciaEntradaItens(entItens)))
                    {
                        var prodDetalhe = new ProdutoDetalhe
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            ProdCodigo = dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                            ProdEstAtual = Convert.ToInt32(dgItens.Rows[indiceGrid].Cells["ENT_QTDESTOQUE"].Value),
                            OpAlteracao = Principal.usuario,
                        };

                        BancoDados.AbrirTrans();

                        if (prodDetalhe.AtualizaEstoqueAtual(prodDetalhe, "-", true).Equals(-1))
                        {
                            BancoDados.ErroTrans();
                            e.Cancel = true;
                        }

                        var dadosMovimento = new MovEstoque
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EstIndice = Convert.ToInt32(dgItens.Rows[indiceGrid].Cells["EST_INDICE"].Value),
                            ProdCodigo = dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                        };

                        if (dadosMovimento.ManutencaoMovimentoEstoque("D", dadosMovimento).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            e.Cancel = true;
                        }

                        if (prodDetalhe.AtualizaCusto(prodDetalhe).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            e.Cancel = true;
                        }

                        spoolEntrada.ExcluiSpoolEntradasItens(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text), dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString());

                        if (Funcoes.LeParametro(9, "29", true).Equals("S"))
                        {
                            var buscaProdControlado = new Produto();
                            if (buscaProdControlado.IdentificaProdutoControlado(dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString()).Equals("S"))
                            {
                                var dadosProduto = new SngpcEntrada
                                {
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    EntDocto = txtDocto.Text,
                                    EntCNPJ = txtFCnpj.Text,
                                    ProdCodigo = dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),

                                };
                                if (dadosProduto.ExcluiProdutoViaEntradaDeNotas(dadosProduto, true).Equals(-1))
                                {
                                    BancoDados.ErroTrans();
                                    e.Cancel = true;
                                }
                            }
                        }

                        var prodEntItens = new EntradaItens
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            EntId = Convert.ToInt32(txtEntID.Text),
                            ProdCodigo = dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                        };
                        if (prodEntItens.ExcluiProdutoEntradasItens(prodEntItens).Equals(-1))
                        {
                            BancoDados.ErroTrans();
                            e.Cancel = true;
                        }

                        BancoDados.FecharTrans();
                    }
                    else
                    {
                        spoolEntrada.ExcluiSpoolEntradasItens(Principal.nomeEstacao, Principal.estAtual, Principal.empAtual, Convert.ToInt32(txtEntID.Text), dgItens.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString());
                    }

                    totalIpi = totalIpi - Convert.ToDouble(dgItens.Rows[indiceGrid].Cells["ENT_VALOR_IPI"].Value);
                    valorItens = valorItens - Convert.ToDouble(dgItens.Rows[indiceGrid].Cells["ENT_TOTITEM"].Value);

                    var natOperacaoFatura = new NatOperacao();
                    if (natOperacaoFatura.VerificaNatFatura(Principal.empAtual, 1101, 1).Equals(true))
                    {
                        valorItensFatura = valorItensFatura + valorItens + totalIpi;
                    }
                }
                else
                {
                    MessageBox.Show("EXCLUSÃO NÃO LIBERADA, ENTRADA FEITA VIA ARQUIVO XML.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox10, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox11, "Campo Obrigatório");
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox12, "Campo Obrigatório");
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox15, "Campo Obrigatório");
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox14, "Campo Obrigatório");
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox16, "Campo Obrigatório");
        }

        private void dgParcelas_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dgParcelas.RowCount != 0)
            {
                InserirSpoolParcelas();
            }
        }

        public void TotalizaGrid()
        {
            int qtde = 0;
            int eQtde = 0;
            double total = 0;
            double desconto = 0;

            for (int i = 0; i < dgItens.RowCount; i++)
            {
                qtde = qtde + Convert.ToInt32(dgItens.Rows[i].Cells[4].Value);
                eQtde = eQtde + Convert.ToInt32(dgItens.Rows[i].Cells[6].Value);
                desconto = desconto + Convert.ToDouble(dgItens.Rows[i].Cells[9].Value);
                total = total + Convert.ToDouble(dgItens.Rows[i].Cells[10].Value);
            }

            txtGQtde.Text = Convert.ToString(qtde);
            txtGEqtde.Text = Convert.ToString(eQtde);
            txtGDesc.Text = String.Format("{0:C}", desconto);
            txtGTotal.Text = String.Format("{0:C}", total);

        }

        private void btnNfe_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtDocto.Text))
                {
                    string id = txtEntID.Text;
                    Limpar();
                    txtEntID.Text = id;
                }

                ofdXML.Filter = "Todos os documentos XML (*.xml)|*.xml";
                ofdXML.Title = "Abrir";
                if (ofdXML.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    caminhoXML = ofdXML.FileName;
                    xmlNFe.Load(ofdXML.FileName);
                    documentoXML = xmlNFe.InnerXml;
                    xnResul = xmlNFe.GetElementsByTagName("ide");
                    
                    if (IdentificaProdutoNaoCadastrados().Equals(true))
                    {
                        txtEntID.Text = Funcoes.LeParametro(9, "50", true);
                        if (!String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("ENTRADA", "ENT_ID", "ENT_ID", txtEntID.Text, true, false, true)))
                        {
                            Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);

                            txtEntID.Text = Funcoes.LeParametro(9, "50", true);
                        }

                        Funcoes.GravaParametro(9, "50", (Convert.ToInt32(txtEntID.Text) + 1).ToString(), "GERAL", "ID da entrada de nota", "ESTOQUE", "Identifica o ID da entrada de nota", true);

                        entradaViaXml = "S";
                        LerXML();


                    }
                }

                /*Se por acaso tiver um Infinity no grid ele subistitui por zero*/ 
                double positive = double.PositiveInfinity;
                double negative = double.NegativeInfinity;
                for (int i = 0; i < dgItens.Rows.Count; i++) {
                    for (int j = 0; j < dgItens.Columns.Count; j++) {
                        if (dgItens.Rows[i].Cells[j].Value.Equals(positive) || dgItens.Rows[i].Cells[j].Value.Equals(negative))
                            dgItens.Rows[i].Cells[j].Value = "0,00"; 
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtItem") && (control.Name != "txtUn") && (control.Name != "txtEQtde") && (control.Name != "txtEntID") && (control.Name != "txtFNome")
                    && (control.Name != "txtFCnpj") && (control.Name != "txtFCidade") && (control.Name != "txtData") && (control.Name != "txtUsuario")
                    && (control.Name != "txtPCMedio") && (control.Name != "txtPEstoque") && (control.Name != "txtPUCusto") && (control.Name != "txtPVCompra")
                    && (control.Name != "txtGTotal") && (control.Name != "txtGDesc") && (control.Name != "txtGEqtde") && (control.Name != "txtGQtde"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    if (dgEntradas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEntradas.ColumnCount];
                string[] coluna = new string[dgEntradas.ColumnCount];

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    grid[i] = dgEntradas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    if (dgEntradas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (var grade = new frmConfGrade(grid, dgEntradas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEntradas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEntradas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEntradas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEntradas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEntradas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEntradas.ColumnCount; i++)
                        {
                            dgEntradas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcEntradas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEntradas.SelectedTab = tpFicha;
        }



        private void btnOk_Click(object sender, EventArgs e)
        {
            string codBarras = "";
            try
            {
                pnlProdutos.Visible = false;

                for (int i = 0; i < dgProdutosNaoCadastrados.RowCount; i++)
                {
                    int id = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID");
                    BancoDados.AbrirTrans();
                    codBarras = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString();

                    var cadProduto = new Produto
                    {
                        ProdCodBarras = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        ProdDescr = dgProdutosNaoCadastrados.Rows[i].Cells[2].Value.ToString(),
                        ProdUnidade = dgProdutosNaoCadastrados.Rows[i].Cells[3].Value.ToString(),
                        ProdTipo = "P",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadProduto.IdentificaSeProdutoEstaCadastrado(dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString()).Equals(false))
                    {
                        if (cadProduto.InsereProdutosNovos(cadProduto).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao Cadastrar Produto: " + codBarras, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                    }

                    int codDepartmento = Convert.ToInt32(Funcoes.LeParametro(9, "41", true));

                    if (!String.IsNullOrEmpty(dgProdutosNaoCadastrados.Rows[i].Cells[6].Value.ToString()))
                    {
                        codDepartmento = Convert.ToInt32(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("DEPARTAMENTOS", "DEP_CODIGO", "DEP_DESCR", dgProdutosNaoCadastrados.Rows[i].Cells[6].Value.ToString(), false, true)));
                    }
                    var cadDetalhe = new ProdutoDetalhe
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        DepCodigo = codDepartmento,
                        ProdPreCompra = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[5].Value.ToString().Replace(".", ","))),
                        ProdDTEstIni = DateTime.Now,
                        ProdLiberado = "A",
                        ProdEcf = "FF",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadDetalhe.IndentificaProdutoCadastradoProdutoDetalhe(cadDetalhe).Equals(false))
                    {
                        if (cadDetalhe.InsereProdutosDetalheNovos(cadDetalhe).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao Cadastrar Produto: " + codBarras, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                    }

                    var cadPreco = new Preco
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        ProdCodigo = dgProdutosNaoCadastrados.Rows[i].Cells[1].Value.ToString(),
                        PreValor = Convert.ToDouble(String.Format("{0:0.00}", dgProdutosNaoCadastrados.Rows[i].Cells[4].Value.ToString().Replace(".", ","))),
                        TabCodigo = 1,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        ProdID = id
                    };

                    if (cadPreco.IndentificaProdutoCadastradoNaPrecos(cadPreco).Equals(false))
                    {
                        if (cadPreco.InsereProdutosPrecosNovos(cadPreco).Equals(false))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao Cadastrar Produto: " + codBarras, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            continue;
                        }
                    }

                    BancoDados.FecharTrans();
                }


                entradaViaXml = "S";
                LerXML();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + " - Produto: " + codBarras, "Ent. de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            pnlProdutos.Visible = false;
            btnNfe.Focus();
        }

        private void txtCstIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtBaseIpi.Focus();
        }

        private void txtBaseIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtAliIpi.Focus();
        }

        private void txtAliIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbOrigem.Focus();
        }

        private void txtCfop_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtValidade.Focus();
        }

        private void cmbOrigem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNCM.Focus();
        }

        private void txtCst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCstConfis.Focus();
        }

        private void txtCstIcms_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtBIcmsSt.Focus();

        }

        private void txtCstConfis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPVIpi.Focus();
        }

        private void txtPVIpi_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtPisCst.Focus();
        }

        private void txtPisCst_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtCest.Focus();
        }

        private void txtPVIpi_Validated(object sender, EventArgs e)
        {
            if (txtPVIpi.Text.Trim() != "")
            {
                txtPVIpi.Text = String.Format("{0:N}", Convert.ToDecimal(txtPVIpi.Text));
            }
            else
                txtPVIpi.Text = "0,00";
        }

        private void txtAliIpi_Validated(object sender, EventArgs e)
        {
            if (txtAliIpi.Text.Trim() != "")
            {
                txtAliIpi.Text = String.Format("{0:N}", Convert.ToDecimal(txtAliIpi.Text));
            }
            else
                txtAliIpi.Text = "0,00";

        }

        private void txtBaseIpi_Validated(object sender, EventArgs e)
        {
            if (txtBaseIpi.Text.Trim() != "")
            {
                txtBaseIpi.Text = String.Format("{0:N}", Convert.ToDecimal(txtBaseIpi.Text));
            }
            else
                txtBaseIpi.Text = "0,00";
        }

        private void CarregaOrigem(int origem)
        {
            cmbOrigem.SelectedIndex = -1;
            switch (origem)
            {
                case 0:
                    cmbOrigem.SelectedIndex = 0;
                    break;
                case 1:
                    cmbOrigem.SelectedIndex = 1;
                    break;
                case 2:
                    cmbOrigem.SelectedIndex = 2;
                    break;
                case 3:
                    cmbOrigem.SelectedIndex = 3;
                    break;
                case 4:
                    cmbOrigem.SelectedIndex = 4;
                    break;
                case 5:
                    cmbOrigem.SelectedIndex = 4;
                    break;
                case 6:
                    cmbOrigem.SelectedIndex = 5;
                    break;
                case 7:
                    cmbOrigem.SelectedIndex = 6;
                    break;
                case 8:
                    cmbOrigem.SelectedIndex = 7;
                    break;

            }
        }

        private void CarregaIcms(string icms)
        {
            cmbIcms.SelectedIndex = -1;
            switch (icms)
            {
                case "00":
                    cmbIcms.SelectedIndex = 0;
                    break;
                case "10":
                    cmbIcms.SelectedIndex = 1;
                    break;
                case "20":
                    cmbIcms.SelectedIndex = 2;
                    break;
                case "30":
                    cmbIcms.SelectedIndex = 3;
                    break;
                case "40":
                    cmbIcms.SelectedIndex = 4;
                    break;
                case "41":
                    cmbIcms.SelectedIndex = 5;
                    break;
                case "50":
                    cmbIcms.SelectedIndex = 6;
                    break;
                case "51":
                    cmbIcms.SelectedIndex = 7;
                    break;
                case "60":
                    cmbIcms.SelectedIndex = 8;
                    break;
                case "70":
                    cmbIcms.SelectedIndex = 9;
                    break;
                case "90":
                    cmbIcms.SelectedIndex = 10;
                    break;

            }
        }

        private void txtVBIcms_Validated(object sender, EventArgs e)
        {
            if (txtVBIcms.Text.Trim() != "")
            {
                txtVBIcms.Text = String.Format("{0:N}", Convert.ToDouble(txtVBIcms.Text));
            }
            else
                txtVBIcms.Text = "0,00";
        }

        private void dgItens_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgItens.Rows[e.RowIndex].Cells["PROD_CADASTRADO"].Value.ToString() == "S")
            {
                dgItens.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
                dgItens.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.DarkGreen;
            }
            if (Convert.ToDouble(dgItens.Rows[e.RowIndex].Cells["PROD_MARGEM"].Value) < 0)
            {
                dgItens.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgItens.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void dgItens_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            switch (dgItens.Columns[e.ColumnIndex].Name)
            {
                case "DESC_PORC":
                    if (entradaViaXml.Equals("S") && Funcoes.LeParametro(9, "47", false).Equals("N"))
                    {
                        e.Cancel = true;
                    }
                    break;
                case "ENT_DESCONTOITEM":
                    if (entradaViaXml.Equals("S") && Funcoes.LeParametro(9, "47", false).Equals("N"))
                    {
                        e.Cancel = true;
                    }
                    break;
                case "PROD_MARGEM":
                    if (Funcoes.LeParametro(9, "46", false).Equals("N"))
                    {
                        e.Cancel = true;
                    }
                    break;
                case "QTDE":
                    qtdAnterior = Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                    break;
                case "ENT_QTDESTOQUE":
                    qtdAnterior = Convert.ToInt32(dgItens.Rows[e.RowIndex].Cells["ENT_QTDESTOQUE"].Value);
                    break;
            }

            if (Funcoes.LeParametro(9, "47", false).Equals("N"))
            {
                if (entradaViaXml.Equals("S") && !dgItens.Columns[e.ColumnIndex].Name.Equals("PROD_MARGEM") && !dgItens.Columns[e.ColumnIndex].Name.Equals("PRE_VALOR") &&
                    !dgItens.Columns[e.ColumnIndex].Name.Equals("QTDE") && !dgItens.Columns[e.ColumnIndex].Name.Equals("ENT_QTDESTOQUE") && !dgItens.Columns[e.ColumnIndex].Name.Equals("ENT_UNITARIO")
                    && !dgItens.Columns[e.ColumnIndex].Name.Equals("PROD_CONTROLADO") && !dgItens.Columns[e.ColumnIndex].Name.Equals("ENT_DTVALIDADE"))
                {
                    MessageBox.Show("ALTERAÇÃO NÃO LIBERADA, ENTRADA FEITA VIA ARQUIVO XML.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                }
            }
        }

        public void Ajuda()
        {
        }

        private void txtCest_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnIncluir.Focus();
        }

        private async Task DecrementaEstoqueFiliaisExcluir(List<EntradaItens> listBuscaEntradaItens)
        {
            try
            {
                for (int i = 0; i < listBuscaEntradaItens.Count; i++)
                {
                    using (var client = new HttpClient())
                    {
                        Cursor = Cursors.WaitCursor;
                        client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + listBuscaEntradaItens[i].ProdCodigo
                                        + "&descricao=" + listBuscaEntradaItens[i].ItemDescr
                                        + "&qtde=" + listBuscaEntradaItens[i].EntQtdeEstoque
                                        + "&precoCusto=" + (listBuscaEntradaItens[i].EntTotalItem / listBuscaEntradaItens[i].EntQtdeEstoque).ToString().Replace(",", ".")
                                        + "&precoVenda=" + listBuscaEntradaItens[i].EntValorVenda.ToString().Replace(",", ".") + "&codEstabelecimento=" + Funcoes.LeParametro(9, "52", true) + "&operacao=menos");
                        if (!response.IsSuccessStatusCode)
                        {
                            using (StreamWriter writer = new StreamWriter(@"C:\BTI\ExcluirEntradNota.txt", true))
                            {
                                writer.WriteLine("COD. BARRAS: " + listBuscaEntradaItens[i].ProdCodigo + " / QTDE: " + listBuscaEntradaItens[i].EntQtdeEstoque);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtChaveAcesso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtBIcms.Focus();
        }
    }
}
