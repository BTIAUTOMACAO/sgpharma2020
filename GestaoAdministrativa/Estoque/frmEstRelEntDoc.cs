﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstRelEntDoc : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Entrada de Notas");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmEstRelEntDoc(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbEmissao.Focus();
        }

        private void rdbLancamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAnalitico.Focus();
        }

        private void rdbAnalitico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbSintetico.Focus();
        }

        private void rdbSintetico_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbSintetico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void frmEstRelEntDoc_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtDocto.Text = "";
            rdbEmissao.Checked = true;
            rdbSintetico.Checked = true;
            dtInicial.Focus();
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRelEntDoc>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }
              
                Cursor = Cursors.WaitCursor;

                var busca = new Entrada();
                DataTable dtRetorno = new DataTable();
                if (rdbSintetico.Checked)
                {
                    dtRetorno = busca.RelatorioSintetico(Principal.empAtual, Principal.estAtual, rdbEmissao.Checked ? 0 : 1, dtInicial.Text, dtFinal.Text, txtDocto.Text);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioNotasSintetico relatorio = new frmRelatorioNotasSintetico(this, dtRetorno);
                        relatorio.Text = "Relatório de Entradas Sintético";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }
                else
                {
                    dtRetorno = busca.RelatorioSintetico(Principal.empAtual, Principal.estAtual, rdbEmissao.Checked ? 0 : 1, dtInicial.Text, dtFinal.Text, txtDocto.Text);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioNotaAnalitico relatorio = new frmRelatorioNotaAnalitico(this, dtRetorno);
                        relatorio.Text = "Relatório de Entradas Analítico";
                        relatorio.ShowDialog();
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Nenhum registro encontrado!", "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Entrada de Notas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbEmissao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbLancamento_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbEmissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbLancamento.Focus();
        }

        private void txtDocto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbAnalitico_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmEstRelEntDoc_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }
    }
}
