﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAlteracaoQuantidadeLinear : Form, Botoes
    {
        DataTable dtProdutos;

        private ToolStripButton tsbAlteracaoQuantidade = new ToolStripButton("Alteração de Qtd Linear");
        private ToolStripSeparator tssAlteracaoQuantidade = new ToolStripSeparator();

        public frmEstAlteracaoQuantidadeLinear(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            txtQtde.Text = "";
            BuscaProdutos();
        }

        public void BuscaProdutos()
        {
            Cursor = Cursors.WaitCursor;
            
            Produto pro = new Produto();
            dtProdutos = pro.BuscaQuantidadeProdutos(txtCodigoBarras.Text, txtDescicao.Text, Convert.ToInt32(cmbDepartamento.SelectedValue), Convert.ToInt32(cmbClasses.SelectedValue), rdbDecrescente.Checked ? 1 : 0);
            if (dtProdutos.Rows.Count > 0)
            {
                dgProdutos.DataSource = dtProdutos;
                lblTotalRegistros.Visible = true;
                lblTotalRegistros.Text = "Total de Registros " + dgProdutos.RowCount;

                if (dgProdutos.ColumnCount > 8)
                {
                    dgProdutos.Columns.Remove("Departamento");
                }

                DataTable dt = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, " DEP_DESABILITADO = 'N'");
                DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
                combo.HeaderText = "Departamentos";
                combo.Name = "Departamento";
                combo.Width = 140;
                ArrayList row = new ArrayList();

                foreach (DataRow dr in dt.Rows)
                {
                    row.Add(dr["DEP_DESCR"].ToString());
                }

                combo.Items.AddRange(row.ToArray());
                dgProdutos.Columns.Add(combo);
                
                for (int i=0; i < dgProdutos.RowCount; i++)
                {
                    dgProdutos.Rows[i].Cells[8].Value = dgProdutos.Rows[i].Cells["DEP_DESCR"].Value.ToString();
                }

                pbAtualiza.Visible = true;
            }
            else
            {
                dgProdutos.DataSource = dtProdutos;
                txtCodigoBarras.Focus();
                MessageBox.Show("Nenhum registro encontrado.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Cursor = Cursors.Default;
        }

        public void Limpar()
        {
            if (!dgProdutos.Rows.Count.Equals(0))
            {
                dtProdutos.Clear();
                dgProdutos.DataSource = dtProdutos;
            }
            txtCodigoBarras.Text = "";
            txtDescicao.Text = "";
            lblTotalRegistros.Visible = false;
            cmbDepartamento.SelectedValue = 0;
            cmbClasses.SelectedValue = 0;
            pbAtualiza.Visible = false;
            btnAtualiza.Enabled = false;
            pbAtualiza.Value = 0;
            lblTotalRegistros.Text = "";
            txtCodigoBarras.Text = "";
            rdbDecrescente.Checked = true;
        }

        private void frmEstAlteracaoQuantidadeLinear_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                rdbDecrescente.Checked = true;
                txtCodigoBarras.Focus();
                CarregaCompos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        public void CarregaCompos()
        {
            DataTable dtDepartamento = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            dtDepartamento.Rows.Add(0, "TODOS OS DEPARTAMENTOS");
            cmbDepartamento.DataSource = dtDepartamento;
            cmbDepartamento.DisplayMember = "DEP_DESCR";
            cmbDepartamento.ValueMember = "DEP_CODIGO";
            cmbDepartamento.SelectedValue = 0;


            DataTable dtClasse = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'S'");
            dtClasse.Rows.Add(0, "TODAS AS CLASSES");
            cmbClasses.DataSource = dtClasse;
            cmbClasses.DisplayMember = "CLAS_DESCR";
            cmbClasses.ValueMember = "CLAS_CODIGO";
            cmbClasses.SelectedValue = 0;
        }

        private void dgProdutos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            btnAtualiza.Enabled = true;
        }

        private void dgProdutos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private async void btnAtualiza_Click(object sender, EventArgs e)
        {
            if (AtualizaQuantidadeEstoqueLocal().Equals(true))
            {
                bool retorno = await AtualizaQuantidadeEstoqueExterno();
                if (retorno.Equals(true))
                {
                    MessageBox.Show("Atualização realizada com sucesso", "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }
            }
        }


        public bool AtualizaQuantidadeEstoqueLocal()
        {
            try
            {
                ProdutoDetalhe pro = new ProdutoDetalhe();
                BancoDados.AbrirTrans();
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = dgProdutos.RowCount;

                long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);

                int idAlteracao = Funcoes.IdentificaVerificaID("ALTERACAO_ESTOQUE", "ID", Principal.estAtual, "", Principal.empAtual);
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    pbAtualiza.Value = i;
                    lblTotalRegistros.Text = " Atualizando " + (i + 1) + " de " + dgProdutos.RowCount + " - Atualizando Estoque Local - Aguarde";
                    lblTotalRegistros.Refresh();

                    if (Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value) != Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTANTIGO"].Value))
                    {
                        if (pro.AtualizaQuantidade(Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ID"].Value), Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTANTIGO"].Value), Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value)).Equals(false))
                        {
                            MessageBox.Show("Erro ao tentar atualizar", "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }

                        var log = new LogAlteraQtde(
                                  Principal.empAtual,
                                  Principal.estAtual,
                                  dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                  DateTime.Now,
                                  Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTANTIGO"].Value),
                                  Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value),
                                  Principal.usuario
                                  );

                        if (!log.InsereRegistros(log))
                        {
                            MessageBox.Show("Erro ao tentar atualizar", "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }

                        var dadosAjuste = new AlteracaoEstoque(
                                   Principal.empAtual,
                                   Principal.estAtual,
                                   idAlteracao,
                                   dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                   "+",
                                   Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value),
                                   Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTANTIGO"].Value),
                                   "ALTERAÇÃO LINEAR QUATIDADE",
                                   id,
                                   Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value),
                                   Principal.nomeEstacao,
                                   DateTime.Now.ToString(),
                                   Principal.usuario);
                        if (!dadosAjuste.InsereRegistros(dadosAjuste))
                        {
                            MessageBox.Show("Erro ao tentar atualizar", "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }

                        var dadosMovimento = new MovEstoque
                        {
                            EstCodigo = Principal.estAtual,
                            EmpCodigo = Principal.empAtual,
                            EstIndice = id,
                            ProdCodigo = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                            EntQtde = Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value),
                            EntValor = 0,
                            SaiQtde = 0,
                            SaiValor = 0,
                            OperacaoCodigo = "A",
                            DataMovimento = DateTime.Now,
                            DtCadastro = DateTime.Now,
                            OpCadastro = Principal.usuario
                        };

                        if (!dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                        {
                            MessageBox.Show("Erro ao tentar atualizar", "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }

                        id = id + 1;

                        idAlteracao = idAlteracao + 1;
                    }
                    
                    int codDepartmento;

                    if (dgProdutos.Rows[i].Cells["DEP_DESCR"].Value.ToString() != dgProdutos.Rows[i].Cells[8].Value.ToString())
                    {
                        codDepartmento = Convert.ToInt32(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("DEPARTAMENTOS", "DEP_CODIGO", "DEP_DESCR", dgProdutos.Rows[i].Cells[8].Value.ToString(), false, true)));

                        if (!pro.AtualizaDepartamento(Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ID"].Value), codDepartmento, dgProdutos.Rows[i].Cells["DEP_DESCR"].Value.ToString(), dgProdutos.Rows[i].Cells[8].Value.ToString()))
                        {
                            MessageBox.Show("Erro ao tentar atualizar", "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }
                    }
                }
                
                BancoDados.FecharTrans();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro:" + ex.Message, "Atualização de Quantidade", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BancoDados.ErroTrans();
                return false;
            }
        }
        public async Task<bool> AtualizaQuantidadeEstoqueExterno()
        {
            try
            {
                ProdutoDetalhe pro = new ProdutoDetalhe();
                string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
                string enderecoWebService = Funcoes.LeParametro(9, "54", true);
                if (Funcoes.ChecaParamentos(9, "51").Equals("S"))
                {
                    for (int i = 0; i < dgProdutos.RowCount; i++)
                    {
                        pbAtualiza.Value = i;
                        lblTotalRegistros.Text = " Atualizando " + (i + 1) + " de " + dgProdutos.RowCount + " - Atualizando Estoque Externo - Aguarde";
                        lblTotalRegistros.Refresh();

                        if (Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value) != Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ESTANTIGO"].Value))
                        {
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(enderecoWebService);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoque?codBarra=" + dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value +
                                    "&descricao=" + dgProdutos.Rows[i].Cells["PROD_DESCR"].Value.ToString() +
                                    "&qtde=" + dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value.ToString() +
                                    "&precoCusto=" + dgProdutos.Rows[i].Cells["PROD_ULTCUSME"].Value.ToString().Replace(",",".") +
                                    "&precoVenda=" + dgProdutos.Rows[i].Cells["PRE_VALOR"].Value.ToString().Replace(",", ".") +
                                    "&codEstabelecimento=" + codEstabelecimento + "&operacao=igual");
                                if (!response.IsSuccessStatusCode)
                                {
                                    MessageBox.Show("Erro ao Cadastrar! \n" + response.RequestMessage, "Produtos Preço Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                        }
                    }
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro :" + ex.Message, "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAlteracaoQuantidade);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAlteracaoQuantidade);
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Alteração de Quantidade Linear", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAlteracaoQuantidade.AutoSize = false;
                this.tsbAlteracaoQuantidade.Image = Properties.Resources.estoque;
                this.tsbAlteracaoQuantidade.Size = new System.Drawing.Size(160, 20);
                this.tsbAlteracaoQuantidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAlteracaoQuantidade);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAlteracaoQuantidade);
                tsbAlteracaoQuantidade.Click += delegate
                {
                    var alteracaoQuantidade = Application.OpenForms.OfType<frmEstAlteracaoQuantidadeLinear>().FirstOrDefault();
                    Util.BotoesGenericos();
                    alteracaoQuantidade.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Alteração de Quantidade Linear", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtCodigoBarras.Text))
                {
                    txtDescicao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtDescicao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtDescicao.Text))
                {
                    cmbDepartamento.Focus();
                }
                else
                    btnBuscar.PerformClick();

            }
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnCalcularTodos.PerformClick();
        }

        private void btnCalcularTodos_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtQtde.Text))
            {
                MessageBox.Show("Qtde não pode estar em branco", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    dgProdutos.Rows[i].Cells["PROD_ESTATUAL"].Value = txtQtde.Text;
                }

                txtQtde.Text = "";
                btnAtualiza.Enabled = true;
            }
        }

        private void dgProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
