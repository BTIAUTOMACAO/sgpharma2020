﻿namespace GestaoAdministrativa.Estoque
{
    partial class frmEstRelBalanco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstRelBalanco));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.chkZerados = new System.Windows.Forms.CheckBox();
            this.gbTipoRelatorio = new System.Windows.Forms.GroupBox();
            this.rdbAnalitico = new System.Windows.Forms.RadioButton();
            this.rdbSintetico = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chkSubclasse = new System.Windows.Forms.CheckBox();
            this.chkClasse = new System.Windows.Forms.CheckBox();
            this.chkDepartamento = new System.Windows.Forms.CheckBox();
            this.rdbOutras = new System.Windows.Forms.RadioButton();
            this.rdbNenhum = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rdbNumerica = new System.Windows.Forms.RadioButton();
            this.rdbAlfabetica = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdbPrecoVenda = new System.Windows.Forms.RadioButton();
            this.rdbUltCusto = new System.Windows.Forms.RadioButton();
            this.rdbCusto = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbTipoRelatorio.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblRegistros);
            this.groupBox2.Controls.Add(this.btnPesquisar);
            this.groupBox2.Controls.Add(this.btnFiltrar);
            this.groupBox2.Controls.Add(this.chkZerados);
            this.groupBox2.Controls.Add(this.gbTipoRelatorio);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(947, 185);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtros";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(796, 128);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(138, 48);
            this.btnPesquisar.TabIndex = 162;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(702, 128);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(88, 48);
            this.btnFiltrar.TabIndex = 161;
            this.btnFiltrar.Text = "Filtros";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // chkZerados
            // 
            this.chkZerados.AutoSize = true;
            this.chkZerados.Location = new System.Drawing.Point(485, 156);
            this.chkZerados.Name = "chkZerados";
            this.chkZerados.Size = new System.Drawing.Size(137, 20);
            this.chkZerados.TabIndex = 7;
            this.chkZerados.Text = "Produtos Zerados";
            this.chkZerados.UseVisualStyleBackColor = true;
            // 
            // gbTipoRelatorio
            // 
            this.gbTipoRelatorio.Controls.Add(this.rdbAnalitico);
            this.gbTipoRelatorio.Controls.Add(this.rdbSintetico);
            this.gbTipoRelatorio.Enabled = false;
            this.gbTipoRelatorio.Location = new System.Drawing.Point(784, 21);
            this.gbTipoRelatorio.Name = "gbTipoRelatorio";
            this.gbTipoRelatorio.Size = new System.Drawing.Size(150, 100);
            this.gbTipoRelatorio.TabIndex = 4;
            this.gbTipoRelatorio.TabStop = false;
            this.gbTipoRelatorio.Text = "Tipo Relatório";
            // 
            // rdbAnalitico
            // 
            this.rdbAnalitico.AutoSize = true;
            this.rdbAnalitico.ForeColor = System.Drawing.Color.Navy;
            this.rdbAnalitico.Location = new System.Drawing.Point(24, 55);
            this.rdbAnalitico.Name = "rdbAnalitico";
            this.rdbAnalitico.Size = new System.Drawing.Size(82, 20);
            this.rdbAnalitico.TabIndex = 3;
            this.rdbAnalitico.TabStop = true;
            this.rdbAnalitico.Text = "Analítico";
            this.rdbAnalitico.UseVisualStyleBackColor = true;
            // 
            // rdbSintetico
            // 
            this.rdbSintetico.AutoSize = true;
            this.rdbSintetico.ForeColor = System.Drawing.Color.Navy;
            this.rdbSintetico.Location = new System.Drawing.Point(24, 29);
            this.rdbSintetico.Name = "rdbSintetico";
            this.rdbSintetico.Size = new System.Drawing.Size(82, 20);
            this.rdbSintetico.TabIndex = 2;
            this.rdbSintetico.TabStop = true;
            this.rdbSintetico.Text = "Sintético";
            this.rdbSintetico.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.chkSubclasse);
            this.groupBox6.Controls.Add(this.chkClasse);
            this.groupBox6.Controls.Add(this.chkDepartamento);
            this.groupBox6.Controls.Add(this.rdbOutras);
            this.groupBox6.Controls.Add(this.rdbNenhum);
            this.groupBox6.Location = new System.Drawing.Point(485, 21);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(293, 100);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Quebra Por";
            // 
            // chkSubclasse
            // 
            this.chkSubclasse.AutoSize = true;
            this.chkSubclasse.Enabled = false;
            this.chkSubclasse.Location = new System.Drawing.Point(200, 55);
            this.chkSubclasse.Name = "chkSubclasse";
            this.chkSubclasse.Size = new System.Drawing.Size(91, 20);
            this.chkSubclasse.TabIndex = 6;
            this.chkSubclasse.Text = "Subclasse";
            this.chkSubclasse.UseVisualStyleBackColor = true;
            this.chkSubclasse.CheckedChanged += new System.EventHandler(this.chkSubclasse_CheckedChanged);
            // 
            // chkClasse
            // 
            this.chkClasse.AutoSize = true;
            this.chkClasse.Enabled = false;
            this.chkClasse.Location = new System.Drawing.Point(127, 55);
            this.chkClasse.Name = "chkClasse";
            this.chkClasse.Size = new System.Drawing.Size(68, 20);
            this.chkClasse.TabIndex = 5;
            this.chkClasse.Text = "Classe";
            this.chkClasse.UseVisualStyleBackColor = true;
            this.chkClasse.CheckedChanged += new System.EventHandler(this.chkClasse_CheckedChanged);
            // 
            // chkDepartamento
            // 
            this.chkDepartamento.AutoSize = true;
            this.chkDepartamento.Enabled = false;
            this.chkDepartamento.Location = new System.Drawing.Point(6, 55);
            this.chkDepartamento.Name = "chkDepartamento";
            this.chkDepartamento.Size = new System.Drawing.Size(117, 20);
            this.chkDepartamento.TabIndex = 4;
            this.chkDepartamento.Text = "Departamento";
            this.chkDepartamento.UseVisualStyleBackColor = true;
            this.chkDepartamento.CheckedChanged += new System.EventHandler(this.chkDepartamento_CheckedChanged);
            // 
            // rdbOutras
            // 
            this.rdbOutras.AutoSize = true;
            this.rdbOutras.ForeColor = System.Drawing.Color.Navy;
            this.rdbOutras.Location = new System.Drawing.Point(99, 21);
            this.rdbOutras.Name = "rdbOutras";
            this.rdbOutras.Size = new System.Drawing.Size(192, 20);
            this.rdbOutras.TabIndex = 3;
            this.rdbOutras.TabStop = true;
            this.rdbOutras.Text = "Depto, Classe e Subclasse";
            this.rdbOutras.UseVisualStyleBackColor = true;
            this.rdbOutras.CheckedChanged += new System.EventHandler(this.rdbOutras_CheckedChanged);
            // 
            // rdbNenhum
            // 
            this.rdbNenhum.AutoSize = true;
            this.rdbNenhum.ForeColor = System.Drawing.Color.Navy;
            this.rdbNenhum.Location = new System.Drawing.Point(6, 21);
            this.rdbNenhum.Name = "rdbNenhum";
            this.rdbNenhum.Size = new System.Drawing.Size(87, 20);
            this.rdbNenhum.TabIndex = 2;
            this.rdbNenhum.TabStop = true;
            this.rdbNenhum.Text = "Nenhuma";
            this.rdbNenhum.UseVisualStyleBackColor = true;
            this.rdbNenhum.CheckedChanged += new System.EventHandler(this.rdbNenhum_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rdbNumerica);
            this.groupBox5.Controls.Add(this.rdbAlfabetica);
            this.groupBox5.Location = new System.Drawing.Point(376, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(103, 100);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ordem";
            // 
            // rdbNumerica
            // 
            this.rdbNumerica.AutoSize = true;
            this.rdbNumerica.ForeColor = System.Drawing.Color.Navy;
            this.rdbNumerica.Location = new System.Drawing.Point(10, 55);
            this.rdbNumerica.Name = "rdbNumerica";
            this.rdbNumerica.Size = new System.Drawing.Size(87, 20);
            this.rdbNumerica.TabIndex = 3;
            this.rdbNumerica.TabStop = true;
            this.rdbNumerica.Text = "Numérica";
            this.rdbNumerica.UseVisualStyleBackColor = true;
            // 
            // rdbAlfabetica
            // 
            this.rdbAlfabetica.AutoSize = true;
            this.rdbAlfabetica.ForeColor = System.Drawing.Color.Navy;
            this.rdbAlfabetica.Location = new System.Drawing.Point(10, 29);
            this.rdbAlfabetica.Name = "rdbAlfabetica";
            this.rdbAlfabetica.Size = new System.Drawing.Size(90, 20);
            this.rdbAlfabetica.TabIndex = 2;
            this.rdbAlfabetica.TabStop = true;
            this.rdbAlfabetica.Text = "Alfabética";
            this.rdbAlfabetica.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdbPrecoVenda);
            this.groupBox4.Controls.Add(this.rdbUltCusto);
            this.groupBox4.Controls.Add(this.rdbCusto);
            this.groupBox4.Location = new System.Drawing.Point(230, 21);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(140, 100);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Valor";
            // 
            // rdbPrecoVenda
            // 
            this.rdbPrecoVenda.AutoSize = true;
            this.rdbPrecoVenda.ForeColor = System.Drawing.Color.Navy;
            this.rdbPrecoVenda.Location = new System.Drawing.Point(6, 73);
            this.rdbPrecoVenda.Name = "rdbPrecoVenda";
            this.rdbPrecoVenda.Size = new System.Drawing.Size(127, 20);
            this.rdbPrecoVenda.TabIndex = 2;
            this.rdbPrecoVenda.TabStop = true;
            this.rdbPrecoVenda.Text = "Preço de Venda";
            this.rdbPrecoVenda.UseVisualStyleBackColor = true;
            // 
            // rdbUltCusto
            // 
            this.rdbUltCusto.AutoSize = true;
            this.rdbUltCusto.ForeColor = System.Drawing.Color.Navy;
            this.rdbUltCusto.Location = new System.Drawing.Point(6, 47);
            this.rdbUltCusto.Name = "rdbUltCusto";
            this.rdbUltCusto.Size = new System.Drawing.Size(106, 20);
            this.rdbUltCusto.TabIndex = 1;
            this.rdbUltCusto.TabStop = true;
            this.rdbUltCusto.Text = "Último Custo";
            this.rdbUltCusto.UseVisualStyleBackColor = true;
            // 
            // rdbCusto
            // 
            this.rdbCusto.AutoSize = true;
            this.rdbCusto.ForeColor = System.Drawing.Color.Navy;
            this.rdbCusto.Location = new System.Drawing.Point(6, 21);
            this.rdbCusto.Name = "rdbCusto";
            this.rdbCusto.Size = new System.Drawing.Size(104, 20);
            this.rdbCusto.TabIndex = 0;
            this.rdbCusto.TabStop = true;
            this.rdbCusto.Text = "Custo Médio";
            this.rdbCusto.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtData);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(6, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 100);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data de Referência p/ Balanço";
            // 
            // dtData
            // 
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(53, 30);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(111, 22);
            this.dtData.TabIndex = 1;
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Balanço do Estoque";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.ForeColor = System.Drawing.Color.Navy;
            this.lblRegistros.Location = new System.Drawing.Point(3, 160);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(0, 16);
            this.lblRegistros.TabIndex = 163;
            // 
            // frmEstRelBalanco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstRelBalanco";
            this.Text = "frmEstRelBalanco";
            this.Load += new System.EventHandler(this.frmEstRelBalanco_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbTipoRelatorio.ResumeLayout(false);
            this.gbTipoRelatorio.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rdbOutras;
        private System.Windows.Forms.RadioButton rdbNenhum;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rdbNumerica;
        private System.Windows.Forms.RadioButton rdbAlfabetica;
        private System.Windows.Forms.RadioButton rdbPrecoVenda;
        private System.Windows.Forms.RadioButton rdbUltCusto;
        private System.Windows.Forms.RadioButton rdbCusto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbTipoRelatorio;
        private System.Windows.Forms.RadioButton rdbAnalitico;
        private System.Windows.Forms.RadioButton rdbSintetico;
        private System.Windows.Forms.CheckBox chkZerados;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnFiltrar;
        public System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.CheckBox chkSubclasse;
        private System.Windows.Forms.CheckBox chkClasse;
        private System.Windows.Forms.CheckBox chkDepartamento;
        private System.Windows.Forms.Label lblRegistros;
    }
}