﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstManutencaoCodBarras : Form, Botoes
    {

        private ToolStripButton tsbEstManutencaoCodBarras = new ToolStripButton("Manutenção Cod. Barras");
        private ToolStripSeparator tssEstManutencaoCodBarras = new ToolStripSeparator();
        public frmEstManutencaoCodBarras(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);

        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }


        private void frmEstManutencaoCodBarras_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscaProdutoPorCodigo();
        }

        public void BuscaProdutoPorCodigo()
        {
            Produto pro = new Produto();
            DataTable dtProduto = pro.BuscaProdutos(txtCodigoBarras.Text);
            if (dtProduto.Rows.Count != 0)
            {
                txtDescricao.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                btnAtualiza.Enabled = true;
            }
            else
            {
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
                txtCodigoBarras.Focus();
            }
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == 13)
            {
                if (txtCodigoBarras.Text.Trim() != "")
                {
                    BuscaProdutoPorCodigo();

                }
            }
        }

        private async void btnAtualiza_Click(object sender, EventArgs e)
        {
            if (ConsisteCampos().Equals(true))
            {
                DialogResult resultado = MessageBox.Show("Tem certeza que deseja atualizar o código de barras deste produto", "Manutenção de Código de Barras", MessageBoxButtons.YesNoCancel);

                if (resultado == DialogResult.Yes)
                {
                    Produto pro = new Produto();
                    BancoDados.AbrirTrans();
                    if (pro.VerificaCodigoBarras(txtCodigoBarrasNovo.Text).Equals(true))
                    {
                        if (AtualizaCodBarras().Equals(true))
                        {
                            if (Funcoes.ChecaParamentos(9, "51").Equals("S").Equals(true))
                            {
                                bool retorno = await AtualizaCodBarrasFilial();
                                if (retorno.Equals(true))
                                {
                                    BancoDados.FecharTrans();
                                    MessageBox.Show("Atualização Local/Externa realizada com sucesso", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    Limpar();
                                }
                                else
                                {
                                    MessageBox.Show("Erro ao Atualizar!", " Código de Barras Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                }
                            }
                            else
                            {
                                BancoDados.FecharTrans();
                                MessageBox.Show("Atualização realizada com sucesso", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Limpar();
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao tentar atualizar Código de Barras", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Código de Barras já cadastrado", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtCodigoBarrasNovo.Text = String.Empty;
                    }
                }
                else
                {
                    txtCodigoBarrasNovo.Text = String.Empty;
                }
            }
        }

        public async Task<bool> AtualizaCodBarrasFilial()
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaCodigoBarras?codBarrasNova=" + txtCodigoBarrasNovo.Text + "&codBarrasAntigo=" + txtCodigoBarras.Text + "&codEstabelecimento=" + codEstabelecimento);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool AtualizaCodBarras()
        {
            try
            {
                Utilitarios util = new Utilitarios();
                if (util.AtualizaCodigoBarras("AJUSTES_ESTOQUE", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                {
                    if (util.AtualizaCodigoBarras("AJUSTES_ULT_CUSTO", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                    {
                        if (util.AtualizaCodigoBarras("CLIENTES_CONTINUO", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                        {
                            if (util.AtualizaCodigoBarras("COMANDA", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                            {
                                if (util.AtualizaCodigoBarras("ENTRADA_ITENS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                {
                                    if (util.AtualizaCodigoBarras("LOG_ALTERA_PRECO", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                    {
                                        if (util.AtualizaCodigoBarras("MOV_ESTOQUE", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                        {
                                            if (util.AtualizaCodigoBarras("VENDAS_ITENS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                            {
                                                if (util.AtualizaCodigoBarras("PRECOS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                {
                                                    if (util.AtualizaCodigoBarras("PRODUTOS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                    {
                                                        if (util.AtualizaCodigoBarras("PRODUTOS_ABCFARMA", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                        {
                                                            if (util.AtualizaCodigoBarras("PRODUTOS_DETALHE", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                            {
                                                                if (util.AtualizaCodigoBarras("SNGPC_ENTRADAS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                                {
                                                                    if (util.AtualizaCodigoBarras("SNGPC_INVENTARIO", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                                    {
                                                                        if (util.AtualizaCodigoBarras("SNGPC_PERDA", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                                        {
                                                                            if (util.AtualizaCodigoBarras("SNGPC_VENDAS", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                                            {
                                                                                if (util.AtualizaCodigoBarras("SPOOL_ENTRADA_ITENS_NF", txtCodigoBarrasNovo.Text, txtCodigoBarras.Text).Equals(true))
                                                                                {
                                                                                    return true;
                                                                                }
                                                                                else
                                                                                {
                                                                                    return false;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                return false;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            return false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        return false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    return false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                return false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return false;
                                                    }
                                                }
                                                else
                                                {
                                                    return false;
                                                }
                                            }
                                            else
                                            {
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool ConsisteCampos()
        {
            if (String.IsNullOrWhiteSpace(txtCodigoBarrasNovo.Text))
            {
                MessageBox.Show("O Campo de Codigo de Barras não pode estar vazio", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCodigoBarrasNovo.Focus();
                return false;
            }
            if (txtCodigoBarrasNovo.Text.Length <= 5)
            {
                MessageBox.Show("Código de Barras deve conter mais de 5 digitos", "Manutenção de Código de Barras", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCodigoBarrasNovo.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        #region Menu
        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            txtCodigoBarras.Text = String.Empty;
            txtDescricao.Text = String.Empty;
            txtCodigoBarrasNovo.Text = String.Empty;
            btnAtualiza.Enabled = false;
        }

        public void Sair()
        {
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEstManutencaoCodBarras);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEstManutencaoCodBarras);
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        #endregion

        private void txtCodigoBarrasNovo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Alteração de Preço Linear", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEstManutencaoCodBarras.AutoSize = false;
                this.tsbEstManutencaoCodBarras.Image = Properties.Resources.estoque;
                this.tsbEstManutencaoCodBarras.Size = new System.Drawing.Size(170, 20);
                this.tsbEstManutencaoCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEstManutencaoCodBarras);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEstManutencaoCodBarras);
                tsbEstManutencaoCodBarras.Click += delegate
                {
                    var manutencaoCodBarras = Application.OpenForms.OfType<frmEstManutencaoCodBarras>().FirstOrDefault();
                    Util.BotoesGenericos();
                    manutencaoCodBarras.Focus();

                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajustes de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
