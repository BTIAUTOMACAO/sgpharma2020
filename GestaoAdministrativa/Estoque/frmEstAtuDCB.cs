﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAtuDCB : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbAtualizaDCB = new ToolStripButton("At. Tabela DCB");
        private ToolStripSeparator tssAtualizaDCB = new ToolStripSeparator();
        private int contador;
        #endregion

        public frmEstAtuDCB(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmEstAtuDCB_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAtualizaDCB.AutoSize = false;
                this.tsbAtualizaDCB.Image = Properties.Resources.estoque;
                this.tsbAtualizaDCB.Size = new System.Drawing.Size(160, 20);
                this.tsbAtualizaDCB.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAtualizaDCB);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAtualizaDCB);
                tsbAtualizaDCB.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstAtuDCB>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAtualizaDCB);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAtualizaDCB);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnArquivo_Click(object sender, EventArgs e)
        {
            await VerificaAtualizacao();
        }

        public async Task<bool> VerificaAtualizacao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Atu. IBPT";

                List<EstabelecimentoFilial> dados = await IdentificaNovaVersao();
                if (dados.Count > 0)
                {
                    if (dados[0].AtualizaDCB == "S")
                    {
                        Principal.mensagem = "Existe uma nova atualização disponível!";
                        btnArquivo.Enabled = true;
                        btnTabelaDCB.Enabled = true;
                        btnTabelaDCB.Focus();
                    }
                    else
                    {
                        Principal.mensagem = "Não há nova versão disponível!";
                        btnArquivo.Enabled = true;
                    }

                    Funcoes.Avisa();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                btnArquivo.Enabled = true;
                btnTabelaDCB.Enabled = false;
                btnArquivo.Focus();
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<List<EstabelecimentoFilial>> IdentificaNovaVersao()
        {
            List<EstabelecimentoFilial> allItems = new List<Negocio.EstabelecimentoFilial>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaDadosDoEstabelecimentos?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        private async void btnTabelaDCB_Click(object sender, EventArgs e)
        {
            try
            {
                bool retorno = await AtualizaDCB();
                if (retorno)
                {
                    AtualizaStatusTabela();
                    MessageBox.Show("Atualização Concluída!", "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("Erro na Atualização. Contate o Suporte!", "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                btnArquivo.Enabled = true;
                btnTabelaDCB.Enabled = true;
                Cursor = Cursors.Default;
                btnTabelaDCB.Focus();
                pbAtualiza.Visible = false;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
            }
        }

        public async Task<bool> AtualizaDCB()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                btnArquivo.Enabled = false;
                btnTabelaDCB.Enabled = false;

                Application.DoEvents();
                tslRegistros.Text = "Atualizando informações da Tabela DCB. Aguarde...";
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;

                List<DCB> dcb = await BuscaDCB();
                if (dcb.Count > 0)
                {
                    if (dcb[0].CodErro == "00")
                    {
                        pbAtualiza.Maximum = dcb.Count;
                        for (int i = 0; i < dcb.Count; i++)
                        {
                            var dadosDcb = new DCB();
                            dadosDcb.NumeroDCB = dcb[i].NumeroDCB;
                            dadosDcb.Descricao = dcb[i].Descricao;
                            dadosDcb.NumCas = dcb[i].NumCas;

                            if (!dadosDcb.ExcluiRegistroPorDCB(dadosDcb))
                                return false;

                            if (!dadosDcb.InsereRegistros(dadosDcb))
                                return false;

                            pbAtualiza.Value = contador;
                            Application.DoEvents();
                            contador = contador + 1;
                            lblRegistros.Text = "Registros: " + Convert.ToString(contador);
                        }
                    }
                    else
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela DCB", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                btnArquivo.Enabled = true;
                btnTabelaDCB.Enabled = true;
                Cursor = Cursors.Default;
                btnTabelaDCB.Focus();
                pbAtualiza.Visible = false;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
            }
        }

        public async Task<List<DCB>> BuscaDCB()
        {
            List<DCB> allItems = new List<Negocio.DCB>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("DCB/BuscaDCB");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<DCB>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async void AtualizaStatusTabela()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false) + "&atualiza=N&campo=ATUALIZA_DCB");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Estabelecimento coluna Atualiza_DCB", "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
