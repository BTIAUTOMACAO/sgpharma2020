﻿namespace GestaoAdministrativa.Estoque
{
    partial class frmEstAtuAbcFarma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstAtuAbcFarma));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbPrecos = new System.Windows.Forms.GroupBox();
            this.btnNovos = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnDesmarcarAbc = new System.Windows.Forms.Button();
            this.btnMarcarAbc = new System.Windows.Forms.Button();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.MARCADO = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRE_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALOR_ABCFARMA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIFERENCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_FRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_PLA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COD_ABC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NCM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LISTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_REGISTRO_MS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_DCB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.txtCnpjFarmacia = new System.Windows.Forms.MaskedTextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblUltAtualizacao = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbAtualiza = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbPrecos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.gbPrecos);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.pbAtualiza);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // gbPrecos
            // 
            this.gbPrecos.Controls.Add(this.btnNovos);
            this.gbPrecos.Controls.Add(this.btnAtualizar);
            this.gbPrecos.Controls.Add(this.btnDesmarcarAbc);
            this.gbPrecos.Controls.Add(this.btnMarcarAbc);
            this.gbPrecos.Controls.Add(this.dgProdutos);
            this.gbPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrecos.ForeColor = System.Drawing.Color.Black;
            this.gbPrecos.Location = new System.Drawing.Point(6, 134);
            this.gbPrecos.Name = "gbPrecos";
            this.gbPrecos.Size = new System.Drawing.Size(947, 351);
            this.gbPrecos.TabIndex = 165;
            this.gbPrecos.TabStop = false;
            this.gbPrecos.Text = "Preços Diferentes ";
            this.gbPrecos.Visible = false;
            // 
            // btnNovos
            // 
            this.btnNovos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNovos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovos.Image = ((System.Drawing.Image)(resources.GetObject("btnNovos.Image")));
            this.btnNovos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNovos.Location = new System.Drawing.Point(585, 285);
            this.btnNovos.Name = "btnNovos";
            this.btnNovos.Size = new System.Drawing.Size(175, 60);
            this.btnNovos.TabIndex = 166;
            this.btnNovos.Text = "Inserir Produtos Novos(F2)";
            this.btnNovos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNovos.UseVisualStyleBackColor = true;
            this.btnNovos.Visible = false;
            this.btnNovos.Click += new System.EventHandler(this.btnNovos_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAtualizar.Location = new System.Drawing.Point(766, 285);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(175, 60);
            this.btnAtualizar.TabIndex = 165;
            this.btnAtualizar.Text = "Atualizar Preços (Insert)";
            this.btnAtualizar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            this.btnAtualizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAtualizar_KeyDown);
            // 
            // btnDesmarcarAbc
            // 
            this.btnDesmarcarAbc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesmarcarAbc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcarAbc.Image = ((System.Drawing.Image)(resources.GetObject("btnDesmarcarAbc.Image")));
            this.btnDesmarcarAbc.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDesmarcarAbc.Location = new System.Drawing.Point(168, 285);
            this.btnDesmarcarAbc.Name = "btnDesmarcarAbc";
            this.btnDesmarcarAbc.Size = new System.Drawing.Size(156, 60);
            this.btnDesmarcarAbc.TabIndex = 164;
            this.btnDesmarcarAbc.Text = "Desmarcar Todos (F8)";
            this.btnDesmarcarAbc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDesmarcarAbc.UseVisualStyleBackColor = true;
            this.btnDesmarcarAbc.Click += new System.EventHandler(this.btnDesmarcarAbc_Click);
            this.btnDesmarcarAbc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDesmarcarAbc_KeyDown);
            // 
            // btnMarcarAbc
            // 
            this.btnMarcarAbc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarcarAbc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcarAbc.Image = ((System.Drawing.Image)(resources.GetObject("btnMarcarAbc.Image")));
            this.btnMarcarAbc.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMarcarAbc.Location = new System.Drawing.Point(6, 285);
            this.btnMarcarAbc.Name = "btnMarcarAbc";
            this.btnMarcarAbc.Size = new System.Drawing.Size(156, 60);
            this.btnMarcarAbc.TabIndex = 163;
            this.btnMarcarAbc.Text = "Marcar Todos (F7)";
            this.btnMarcarAbc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMarcarAbc.UseVisualStyleBackColor = true;
            this.btnMarcarAbc.Click += new System.EventHandler(this.btnMarcarAbc_Click);
            this.btnMarcarAbc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnMarcarAbc_KeyDown);
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            this.dgProdutos.AllowUserToResizeColumns = false;
            this.dgProdutos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MARCADO,
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.PRE_VALOR,
            this.VALOR_ABCFARMA,
            this.DIFERENCA,
            this.MED_FRA,
            this.MED_PLA,
            this.COD_ABC,
            this.NCM,
            this.CEST,
            this.LISTA,
            this.PROD_REGISTRO_MS,
            this.MED_DCB});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgProdutos.GridColor = System.Drawing.Color.Black;
            this.dgProdutos.Location = new System.Drawing.Point(6, 21);
            this.dgProdutos.Name = "dgProdutos";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgProdutos.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgProdutos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdutos.Size = new System.Drawing.Size(935, 258);
            this.dgProdutos.TabIndex = 0;
            this.dgProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProdutos_KeyDown);
            // 
            // MARCADO
            // 
            this.MARCADO.DataPropertyName = "MARCADO";
            this.MARCADO.HeaderText = "";
            this.MARCADO.Name = "MARCADO";
            this.MARCADO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MARCADO.Width = 30;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. de Barras";
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_CODIGO.Width = 130;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.Width = 300;
            // 
            // PRE_VALOR
            // 
            this.PRE_VALOR.DataPropertyName = "PRE_VALOR";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.PRE_VALOR.DefaultCellStyle = dataGridViewCellStyle3;
            this.PRE_VALOR.HeaderText = "Valor de Venda";
            this.PRE_VALOR.Name = "PRE_VALOR";
            this.PRE_VALOR.Width = 120;
            // 
            // VALOR_ABCFARMA
            // 
            this.VALOR_ABCFARMA.DataPropertyName = "VALOR_ABCFARMA";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.VALOR_ABCFARMA.DefaultCellStyle = dataGridViewCellStyle4;
            this.VALOR_ABCFARMA.HeaderText = "Valor do ABCFarma";
            this.VALOR_ABCFARMA.Name = "VALOR_ABCFARMA";
            this.VALOR_ABCFARMA.Width = 150;
            // 
            // DIFERENCA
            // 
            this.DIFERENCA.DataPropertyName = "DIFERENCA";
            this.DIFERENCA.HeaderText = "Diferença";
            this.DIFERENCA.Name = "DIFERENCA";
            // 
            // MED_FRA
            // 
            this.MED_FRA.DataPropertyName = "MED_FRA";
            this.MED_FRA.HeaderText = "MED_FRA";
            this.MED_FRA.Name = "MED_FRA";
            this.MED_FRA.Visible = false;
            // 
            // MED_PLA
            // 
            this.MED_PLA.DataPropertyName = "MED_PLA";
            this.MED_PLA.HeaderText = "MED_PLA";
            this.MED_PLA.Name = "MED_PLA";
            this.MED_PLA.Visible = false;
            // 
            // COD_ABC
            // 
            this.COD_ABC.DataPropertyName = "COD_ABC";
            this.COD_ABC.HeaderText = "COD_ABC";
            this.COD_ABC.Name = "COD_ABC";
            this.COD_ABC.Visible = false;
            // 
            // NCM
            // 
            this.NCM.DataPropertyName = "NCM";
            this.NCM.HeaderText = "NCM";
            this.NCM.Name = "NCM";
            this.NCM.Visible = false;
            // 
            // CEST
            // 
            this.CEST.DataPropertyName = "CEST";
            this.CEST.HeaderText = "CEST";
            this.CEST.Name = "CEST";
            this.CEST.Visible = false;
            // 
            // LISTA
            // 
            this.LISTA.DataPropertyName = "LISTA";
            this.LISTA.HeaderText = "LISTA";
            this.LISTA.Name = "LISTA";
            this.LISTA.Visible = false;
            // 
            // PROD_REGISTRO_MS
            // 
            this.PROD_REGISTRO_MS.DataPropertyName = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.HeaderText = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.Name = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.Visible = false;
            // 
            // MED_DCB
            // 
            this.MED_DCB.DataPropertyName = "MED_DCB";
            this.MED_DCB.HeaderText = "MED_DCB";
            this.MED_DCB.Name = "MED_DCB";
            this.MED_DCB.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblRegistros);
            this.groupBox2.Controls.Add(this.txtCnpjFarmacia);
            this.groupBox2.Controls.Add(this.txtSenha);
            this.groupBox2.Controls.Add(this.btnConsulta);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.lblUltAtualizacao);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(947, 94);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Arquivo de Preços";
            // 
            // lblRegistros
            // 
            this.lblRegistros.ForeColor = System.Drawing.Color.Navy;
            this.lblRegistros.Location = new System.Drawing.Point(584, 69);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(357, 19);
            this.lblRegistros.TabIndex = 16;
            // 
            // txtCnpjFarmacia
            // 
            this.txtCnpjFarmacia.BackColor = System.Drawing.Color.White;
            this.txtCnpjFarmacia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCnpjFarmacia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCnpjFarmacia.ForeColor = System.Drawing.Color.Black;
            this.txtCnpjFarmacia.Location = new System.Drawing.Point(188, 38);
            this.txtCnpjFarmacia.Mask = "00,000,000/0000-00";
            this.txtCnpjFarmacia.Name = "txtCnpjFarmacia";
            this.txtCnpjFarmacia.Size = new System.Drawing.Size(198, 22);
            this.txtCnpjFarmacia.TabIndex = 15;
            this.txtCnpjFarmacia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCnpjFarmacia_KeyDown);
            this.txtCnpjFarmacia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCnpjFarmacia_KeyPress);
            // 
            // txtSenha
            // 
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenha.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(188, 66);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(198, 22);
            this.txtSenha.TabIndex = 14;
            this.txtSenha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSenha.UseSystemPasswordChar = true;
            this.txtSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSenha_KeyDown);
            this.txtSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSenha_KeyPress);
            // 
            // btnConsulta
            // 
            this.btnConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsulta.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsulta.Image = ((System.Drawing.Image)(resources.GetObject("btnConsulta.Image")));
            this.btnConsulta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsulta.Location = new System.Drawing.Point(392, 44);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(186, 44);
            this.btnConsulta.TabIndex = 13;
            this.btnConsulta.Text = "Consultar Preço - F1";
            this.btnConsulta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            this.btnConsulta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConsulta_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(80, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "Senha da Loja:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(87, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "CNPJ da Loja:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(4, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // lblUltAtualizacao
            // 
            this.lblUltAtualizacao.AutoSize = true;
            this.lblUltAtualizacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblUltAtualizacao.Location = new System.Drawing.Point(289, 18);
            this.lblUltAtualizacao.Name = "lblUltAtualizacao";
            this.lblUltAtualizacao.Size = new System.Drawing.Size(88, 16);
            this.lblUltAtualizacao.TabIndex = 1;
            this.lblUltAtualizacao.Text = "Janeiro/2014";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(80, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mês/Ano da última Atualização: ";
            // 
            // pbAtualiza
            // 
            this.pbAtualiza.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAtualiza.Location = new System.Drawing.Point(6, 103);
            this.pbAtualiza.Name = "pbAtualiza";
            this.pbAtualiza.Size = new System.Drawing.Size(947, 25);
            this.pbAtualiza.TabIndex = 164;
            this.pbAtualiza.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(263, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Atualiza Preços Produtos ABCFarma";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmEstAtuAbcFarma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstAtuAbcFarma";
            this.Text = "frmEstAtuAbcFarma";
            this.Load += new System.EventHandler(this.frmEstAtuAbcFarma_Load);
            this.Shown += new System.EventHandler(this.frmEstAtuAbcFarma_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEstAtuAbcFarma_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbPrecos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox txtCnpjFarmacia;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblUltAtualizacao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar pbAtualiza;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.GroupBox gbPrecos;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Button btnDesmarcarAbc;
        private System.Windows.Forms.Button btnMarcarAbc;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MARCADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRE_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALOR_ABCFARMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIFERENCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_FRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_PLA;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_ABC;
        private System.Windows.Forms.DataGridViewTextBoxColumn NCM;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEST;
        private System.Windows.Forms.DataGridViewTextBoxColumn LISTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REGISTRO_MS;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_DCB;
        private System.Windows.Forms.Button btnNovos;
    }
}