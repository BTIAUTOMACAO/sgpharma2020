﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAtuAbcFarma : Form, Botoes
    {
        private ToolStripButton tsbAtualiza = new ToolStripButton("At. Precos ABCFarma");
        private ToolStripSeparator tssAtualiza = new ToolStripSeparator();
        private string versaoABC;
        List<ListaAbcFarma> dados = new List<ListaAbcFarma>();
        bool inserir = false;
        int contador = 0;
        private DataTable dtProdNovos = new DataTable();

        public frmEstAtuAbcFarma(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
        private void frmEstAtuAbcFarma_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAtualiza.AutoSize = false;
                this.tsbAtualiza.Image = Properties.Resources.estoque;
                this.tsbAtualiza.Size = new System.Drawing.Size(160, 20);
                this.tsbAtualiza.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAtualiza);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAtualiza);
                tsbAtualiza.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstAtuAbcFarma>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAtualiza);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAtualiza);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            versaoABC = Funcoes.LeParametro(9, "57", false);
            lblUltAtualizacao.Text = Funcoes.MesExtenso(versaoABC.Substring(4, 2)) + "/" + versaoABC.Substring(0, 4);
            btnNovos.Visible = false;
            dgProdutos.Rows.Clear();
            pbAtualiza.Visible = false;
            tslRegistros.Text = "";
            lblRegistros.Text = "";
            txtCnpjFarmacia.Text = "";
            txtSenha.Text = "";
            gbPrecos.Visible = false;
            btnNovos.Visible = false;
            txtCnpjFarmacia.Focus();
        }
        private void txtCnpjFarmacia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSenha.Focus();
        }

        private void txtCnpjFarmacia_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnConsulta.PerformClick();
                    break;
                case Keys.F2:
                    btnNovos.PerformClick();
                    break;
                case Keys.F7:
                    btnMarcarAbc.PerformClick();
                    break;
                case Keys.F8:
                    btnDesmarcarAbc.PerformClick();
                    break;
                case Keys.Insert:
                    btnAtualizar.PerformClick();
                    break;
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConsulta.PerformClick();
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public bool TProdutosNovos()
        {
            if (dtProdNovos.Columns.Count == 0)
            {
                dtProdNovos.Columns.Add("PROD_CODIGO", typeof(string));
                dtProdNovos.Columns.Add("PROD_DESCR", typeof(string));
                dtProdNovos.Columns.Add("MED_PCO18", typeof(double));
                dtProdNovos.Columns.Add("MED_GENE", typeof(string));
                dtProdNovos.Columns.Add("PROD_REGISTRO_MS", typeof(string));
                dtProdNovos.Columns.Add("NCM", typeof(string));
                dtProdNovos.Columns.Add("PROD_LISTA", typeof(string));
                dtProdNovos.Columns.Add("PROD_CEST", typeof(string));
                dtProdNovos.Columns.Add("PROD_DCB", typeof(string));
                dtProdNovos.Columns.Add("ESCOLHA", typeof(string));
            }
            else
            {
                dtProdNovos.Rows.Clear();
            }
            return true;
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            try
            {
                dados.Clear();

                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpjFarmacia.Text)))
                {
                    MessageBox.Show("CNPJ não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpjFarmacia.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(txtSenha.Text))
                {
                    MessageBox.Show("Senha não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCnpjFarmacia.Focus();
                    return;
                }

                if (MessageBox.Show("Deseja cadastrar produtos não existentes no cadastro atual?", "Atualiza Preços ABCFarma", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    inserir = true;
                    btnNovos.Visible = true;
                    TProdutosNovos();

                    if(String.IsNullOrEmpty(Funcoes.LeParametro(9,"40", false)) || String.IsNullOrEmpty(Funcoes.LeParametro(9, "41", false)) || String.IsNullOrEmpty(Funcoes.LeParametro(9, "42", false)))
                    {
                        MessageBox.Show("Verifique os Parâmetros 9/40, 9/41, 9,42 antes de continuar. Contate o suporte!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnConsulta.Focus();
                        return;
                    }
                }
                else
                {
                    btnNovos.Visible = false;
                    inserir = false;
                }


                Cursor = Cursors.WaitCursor;

                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Minimum = 0;
                contador = 0;
                pbAtualiza.Visible = true;

                for (int i = 0; i < 16; i++)
                {
                    string url1 = "https://webserviceabcfarma.org.br/webservice/";
                    string parametros1 = "cnpj_cpf=" + Funcoes.RemoveCaracter(txtCnpjFarmacia.Text) + "&senha=" + txtSenha.Text + "&cnpj_sh=19300727000112&pagina=" + (i + 1);

                    var cli1 = new WebClient();
                    cli1.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    string response1 = cli1.UploadString(url1, parametros1);

                    RetornoABCFarma result = new RetornoABCFarma();

                    try
                    {
                        result = JsonConvert.DeserializeObject<RetornoABCFarma>(response1);
                        pbAtualiza.Maximum = result.total_itens + 1;
                        int v = result.data.Length;
                    }
                    catch
                    {
                        ErroAbcFarma retorno = JsonConvert.DeserializeObject<ErroAbcFarma>(response1);
                        MessageBox.Show("Erro " + retorno.error_code + ": " + retorno.error_message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtCnpjFarmacia.Focus();
                        return;
                    }

                    for (int x = 0; x < result.data.Length; x++)
                    {
                        if (result.data[x].ID_REGIME_PRECO != "H")
                        {
                            var arquivo = new ListaAbcFarma();
                            arquivo.MedAbc = result.data[x].ID_PRODUTO;
                            arquivo.LabNome = result.data[x].NOME_FABRICANTE;
                            arquivo.MedDescricao = result.data[x].NOME;
                            arquivo.MedApresentacao = result.data[x].DESCRICAO;
                            if (result.data[x].ID_TIPO_PRODUTO == "G")
                            {
                                arquivo.MepPla = result.data[x].PF_12.Replace(".", ",");
                                arquivo.MedPco = result.data[x].PMC_12.Replace(".", ",");
                                arquivo.MedFra = result.data[x].PMC_12.Replace(".", ",");
                                arquivo.MedGen = "GEN";
                            }
                            else
                            {
                                arquivo.MepPla = result.data[x].PF_18.Replace(".", ",");
                                arquivo.MedPco = result.data[x].PMC_18.Replace(".", ",");
                                arquivo.MedFra = result.data[x].PMC_18.Replace(".", ",");
                                arquivo.MedGen = "";
                            }
                            arquivo.MedBarra = result.data[x].EAN;
                            arquivo.MedRegistroMS = result.data[x].REGISTRO_ANVISA;
                            arquivo.MedLista = result.data[x].ID_LCCT;
                            arquivo.MedNcm = result.data[x].NCM;
                            string[] split;
                            if (result.data[x].CAS != null)
                            {
                                split = result.data[x].CAS.Split(';');
                                arquivo.MedCas = split[0];
                            }
                            else
                            {
                                arquivo.MedCas = "";
                            }
                            if (result.data[x].DCB != null)
                            {
                                split = result.data[x].DCB.Split(';');
                                arquivo.MedDCB = Funcoes.RemoveCaracter(split[0]);
                            }
                            else
                            {
                                arquivo.MedDCB = "";
                            }
                            arquivo.MedPco19 = result.data[x].PMC_20.Replace(".", ",");
                            arquivo.MedCest = result.data[x].CEST;
                            dados.Add(arquivo);
                        }
                        else
                        {
                            if (Convert.ToInt32(result.data[x].QTD_EMBALAGEM) > 0)
                            {
                                var arquivo = new ListaAbcFarma();
                                arquivo.MedAbc = result.data[x].ID_PRODUTO;
                                arquivo.LabNome = result.data[x].NOME_FABRICANTE;
                                arquivo.MedDescricao = result.data[x].NOME;
                                arquivo.MedApresentacao = result.data[x].DESCRICAO;
                                if (result.data[x].ID_TIPO_PRODUTO == "G")
                                {
                                    arquivo.MepPla = result.data[x].PF_12.Replace(".", ",");
                                    arquivo.MedPco = Math.Round(Convert.ToDouble(result.data[x].PF_12.Replace(".", ",")) / Convert.ToInt32(result.data[x].QTD_EMBALAGEM), 2).ToString();
                                    arquivo.MedFra = Math.Round(Convert.ToDouble(result.data[x].PF_12.Replace(".", ",")) / Convert.ToInt32(result.data[x].QTD_EMBALAGEM), 2).ToString();
                                    arquivo.MedGen = "GEN";
                                }
                                else
                                {
                                    arquivo.MepPla = result.data[x].PF_18.Replace(".", ",");
                                    arquivo.MedPco = Math.Round(Convert.ToDouble(result.data[x].PF_18.Replace(".", ",")) / Convert.ToInt32(result.data[x].QTD_EMBALAGEM), 2).ToString();
                                    arquivo.MedFra = Math.Round(Convert.ToDouble(result.data[x].PF_18.Replace(".", ",")) / Convert.ToInt32(result.data[x].QTD_EMBALAGEM), 2).ToString();
                                    arquivo.MedGen = "";
                                }
                                arquivo.MedBarra = result.data[x].EAN;
                                arquivo.MedRegistroMS = result.data[x].REGISTRO_ANVISA;
                                arquivo.MedLista = result.data[x].ID_LCCT;
                                arquivo.MedNcm = result.data[x].NCM;
                                string[] split;
                                if (result.data[x].CAS != null)
                                {
                                    split = result.data[x].CAS.Split(';');
                                    arquivo.MedCas = Funcoes.RemoveCaracter(split[0]);
                                }
                                else
                                {
                                    arquivo.MedCas = "";
                                }
                                if (result.data[x].DCB != null)
                                {
                                    split = result.data[x].DCB.Split(';');
                                    arquivo.MedDCB = Funcoes.RemoveCaracter(split[0]);
                                }
                                else
                                {
                                    arquivo.MedDCB = "";
                                }
                                arquivo.MedCest = result.data[x].CEST;
                                arquivo.MedPco19 = Math.Round(Convert.ToDouble(result.data[x].PF_20.Replace(".", ",")) / Convert.ToInt32(result.data[x].QTD_EMBALAGEM), 2).ToString();
                                dados.Add(arquivo);
                            }
                        }

                        pbAtualiza.Value = contador;
                        Application.DoEvents();
                        contador = contador + 1;

                        lblRegistros.Text = "Registros: " + contador;
                    }

                }

                Cursor = Cursors.WaitCursor;

                lblRegistros.Text = "Gravando Registros. Aguarde...";
                lblRegistros.Refresh();

                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Minimum = 0;
                pbAtualiza.Maximum = dados.Count;
                contador = 0;

                Application.DoEvents();

                //INSERE UMA COPIA NO BANCO//
                var inserirDados = new ListaAbcFarma();
                inserirDados.InsereAbcFarma(dados);

                BancoDados.ExecuteNoQuery("DELETE FROM PRODUTOS_ABCFARMA", null);

                lblRegistros.Text = "Consultando diferenças. Aguarde...";
                lblRegistros.Refresh();

                DataTable dtRetorno = new DataTable();
                var buscaProdutos = new Preco();
                var produtosABC = new ProdutosAbcFarma();
                var produtos = new Produto();

                for (int x = 0; x < dados.Count; x++)
                {
                    if (Convert.ToDouble(dados[x].MedPco) > 0)
                    {
                        dtRetorno = buscaProdutos.BuscaPrecoProduto(Principal.empAtual, Principal.estAtual, dados[x].MedBarra);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            if (Convert.ToDouble(dtRetorno.Rows[0]["PRE_VALOR"]) != Convert.ToDouble(dados[x].MedPco))
                            {
                                dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] { false, dados[x].MedBarra,
                                     dtRetorno.Rows[0]["PROD_DESCR"], dtRetorno.Rows[0]["PRE_VALOR"], dados[x].MedPco, Convert.ToDouble(dtRetorno.Rows[0]["PRE_VALOR"]) > Convert.ToDouble(dados[x].MedPco) ? "-" : "+",
                                     dados[x].MedFra, dados[x].MepPla, dados[x].MedAbc, dados[x].MedNcm, dados[x].MedCest, dados[x].MedLista == "O" ? "N" : dados[x].MedLista, dados[x].MedRegistroMS, dados[x].MedDCB });

                                Application.DoEvents();
                            }
                        }
                        else if (inserir.Equals(true))
                        {
                            Principal.dtRetorno = produtos.BuscaProdutosParaAbcFarma(Principal.estAtual, Principal.empAtual);
                            DataRow[] linha_produtos = Principal.dtRetorno.Select("PROD_CODIGO = '" + dados[x].MedBarra + "'");
                            if (linha_produtos.Length == 0)
                            {
                                DataRow row = dtProdNovos.NewRow();
                                row["PROD_CODIGO"] = dados[x].MedBarra;
                                row["PROD_DESCR"] = dados[x].MedDescricao + " " + dados[x].MedApresentacao;
                                row["MED_PCO18"] = dados[x].MedPco;
                                row["MED_GENE"] = "";
                                row["PROD_REGISTRO_MS"] = dados[x].MedRegistroMS;
                                row["NCM"] = dados[x].MedNcm;
                                row["PROD_LISTA"] = dados[x].MedLista;
                                row["PROD_CEST"] = dados[x].MedCest;
                                row["PROD_DCB"] = dados[x].MedDCB;
                                row["ESCOLHA"] = "False";

                                dtProdNovos.Rows.Add(row);
                            }
                        }
                    }

                    pbAtualiza.Value = x;
                    Application.DoEvents();

                    if (String.IsNullOrEmpty(produtosABC.BuscaCodigoAbcFarma(dados[x].MedBarra)))
                    {
                        produtosABC.InsereTabelaAbcFarma(Convert.ToDouble(dados[x].MedPco), dados[x].MedBarra, dados[x].MedDescricao, Convert.ToDouble(dados[x].MepPla), Convert.ToDouble(dados[x].MedFra), dados[x].MedAbc);
                    }
                }

                if (dgProdutos.Rows.Count == 0)
                {
                    MessageBox.Show("Nenhum produto com Preço Diferente", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gbPrecos.Visible = false;
                    txtCnpjFarmacia.Focus();
                }
                else
                {
                    lblRegistros.Text = dgProdutos.RowCount.ToString() + " Produtos com PMB Diferente";
                    lblRegistros.Refresh();
                    gbPrecos.Visible = true;
                }

                Cursor = Cursors.Default;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                contador = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                lblRegistros.Refresh();
            }
        }

        private void frmEstAtuAbcFarma_Shown(object sender, EventArgs e)
        {
            txtCnpjFarmacia.Text = Funcoes.LeParametro(9, "60", false);
            txtSenha.Text = Funcoes.LeParametro(9, "61", false);

            lblRegistros.Text = "";

            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCnpjFarmacia.Text)))
            {
                MessageBox.Show("Necessário preencher os Parametros 9/60 e 9/61 ou Digite os dados de Acesso. Contate o Suporte!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCnpjFarmacia.Focus();
            }
        }

        private void btnMarcarAbc_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgProdutos.RowCount; i++)
            {
                dgProdutos.Rows[i].Cells[0].Value = true;
                dgProdutos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
            }
            Cursor = Cursors.Default;
        }

        private void btnDesmarcarAbc_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgProdutos.RowCount; i++)
            {
                dgProdutos.Rows[i].Cells[0].Value = false;
                dgProdutos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
            }
            Cursor = Cursors.Default;
        }

        public bool IdentificaSelecionado()
        {
            bool selecionado = false;

            for (int i = 0; i < dgProdutos.RowCount; i++)
            {
                if (Convert.ToBoolean(dgProdutos.Rows[i].Cells[0].Value) == true)
                {
                    selecionado = true;
                }
            }

            return selecionado;
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (IdentificaSelecionado())
                {
                    if (MessageBox.Show("Deseja Atualizar os Preços?", "Atualiza Preços (ABCFarma)", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Cursor = Cursors.WaitCursor;

                        pbAtualiza.Value = 0;
                        pbAtualiza.Step = 1;
                        pbAtualiza.Minimum = 0;
                        pbAtualiza.Maximum = dgProdutos.RowCount;
                        contador = 0;

                        var precos = new Preco();
                        var produtos = new Produto();
                        var produtosDetalhe = new ProdutoDetalhe();

                        precos.AtualizaPrecoAbc(Principal.empAtual, Principal.estAtual);

                        precos.ExcluiLogPrecoAbcFarma(Principal.empAtual, Principal.estAtual);

                        for (int i = 0; i < dgProdutos.RowCount; i++)
                        {
                            if (Convert.ToBoolean(dgProdutos.Rows[i].Cells[0].Value) == true)
                            {
                                precos.EmpCodigo = Principal.empAtual;
                                precos.EstCodigo = Principal.estAtual;
                                precos.PreValor = Convert.ToDouble(dgProdutos.Rows[i].Cells["VALOR_ABCFARMA"].Value);
                                precos.ProdPMC = Convert.ToDouble(dgProdutos.Rows[i].Cells["VALOR_ABCFARMA"].Value);
                                precos.ProdCodigo = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString();
                                precos.DtAlteracao = DateTime.Now;
                                precos.OpAlteracao = Principal.usuario;
                                precos.AtualizadoABCFarma = "S";

                                if (!precos.AtualizaPrecoAbcFarmaPorProdCodigo(precos, Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value)))
                                    return;

                                produtos.ProdCodBarras = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString();
                                produtos.ProdNCM = dgProdutos.Rows[i].Cells["NCM"].Value == null ? "" : dgProdutos.Rows[i].Cells["NCM"].Value.ToString();
                                produtos.ProdRegistroMS = dgProdutos.Rows[i].Cells["PROD_REGISTRO_MS"].Value == null ? "" : dgProdutos.Rows[i].Cells["PROD_REGISTRO_MS"].Value.ToString();
                                produtos.OpAlteracao = Principal.usuario;
                                produtos.DtAlteracao = DateTime.Now;

                                if (!produtos.AtualizaNcmRegistroMSAbcFarma(produtos))
                                    return;

                                produtosDetalhe.EmpCodigo = Principal.empAtual;
                                produtosDetalhe.EstCodigo = Principal.estAtual;
                                produtosDetalhe.ProdCodigo = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString();
                                produtosDetalhe.ProdLista = dgProdutos.Rows[i].Cells["LISTA"].Value.ToString() == "+" ? "P" : dgProdutos.Rows[i].Cells["LISTA"].Value.ToString() == "-" ? "N" : "U";
                                produtosDetalhe.ProdCest = dgProdutos.Rows[i].Cells["CEST"].Value == null ? "" : dgProdutos.Rows[i].Cells["CEST"].Value.ToString();
                                produtosDetalhe.ProdDCB = dgProdutos.Rows[i].Cells["MED_DCB"].Value == null ? "" : dgProdutos.Rows[i].Cells["MED_DCB"].Value.ToString();
                                produtosDetalhe.OpAlteracao = Principal.usuario;
                                produtosDetalhe.DtAlteracao = DateTime.Now;

                                if (!produtosDetalhe.AtualizaProdListaAbcFarma(produtosDetalhe))
                                    return;

                            }

                            pbAtualiza.Value = contador;
                            Application.DoEvents();
                            contador = contador + 1;

                            lblRegistros.Text = "Registros " + contador;
                            lblRegistros.Refresh();
                        }

                        Funcoes.GravaParametro(9, "57", DateTime.Now.Year.ToString() + Funcoes.FormataZeroAEsquerda(DateTime.Now.Month.ToString(), 2));

                        MessageBox.Show("Atualização Realizada com Sucesso!", "Atualizador ABCFarma", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Limpar();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário selecionar ao menos um registro!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProdutos.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmEstAtuAbcFarma_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnMarcarAbc_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnDesmarcarAbc_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAtualizar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void btnNovos_Click(object sender, EventArgs e)
        {
            if (!CadProdNovos())
            {
                MessageBox.Show("Erro no Cadastro dos Produtos. Contate o Suporte!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnNovos.Enabled = true;
            }
            else
            {
                MessageBox.Show("Produtos Cadastrados com Sucesso!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tslRegistros.Text = "";
                lblRegistros.Text = "";
                lblRegistros.Refresh();
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                btnNovos.Visible = false;
            }
        }

        public bool CadProdNovos()
        {
            string prodCodigo = "";
            try
            {
                int id = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID");

                if (MessageBox.Show("Confirma o cadastro dos novos produtos?", "Atualiza Preços (ABCFarma)", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    btnNovos.Enabled = false;
                    Application.DoEvents();
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Cadastrando novos produtos. Aguarde...";
                    pbAtualiza.Value = 0;
                    pbAtualiza.Step = 1;
                    pbAtualiza.Minimum = 0;
                    pbAtualiza.Maximum = dtProdNovos.Rows.Count;
                    contador = 0;
                    string prodDescr;
                    int generico = Convert.ToInt32(Funcoes.LeParametro(9, "41", true));
                    int etico = Convert.ToInt32(Funcoes.LeParametro(9, "40", true));
                    int fabricante = Convert.ToInt32(Funcoes.LeParametro(9, "42", true));
                    for (int i = 0; i < dtProdNovos.Rows.Count; i++)
                    {
                        prodCodigo = dtProdNovos.Rows[i]["PROD_CODIGO"].ToString();
                        if (String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_CODIGO", "PROD_CODIGO", prodCodigo, false, true, false)) 
                            && String.IsNullOrEmpty(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS_DETALHE", "PROD_CODIGO", "PROD_CODIGO", prodCodigo, true, true, true)))
                        {
                            if (dtProdNovos.Rows[i]["PROD_DESCR"].ToString().Length > 50)
                            {
                                prodDescr = dtProdNovos.Rows[i]["PROD_DESCR"].ToString().Substring(0, 50);
                            }
                            else
                            {
                                prodDescr = dtProdNovos.Rows[i]["PROD_DESCR"].ToString();
                            }

                            var produtoNovo = new Produto();
                            produtoNovo.ProdDescr = prodDescr;
                            produtoNovo.ProdUnidade = "UN";
                            produtoNovo.ProdTipo = "P";
                            produtoNovo.ProdCodBarras = dtProdNovos.Rows[i]["PROD_CODIGO"].ToString();
                            produtoNovo.ProdNCM = dtProdNovos.Rows[i]["NCM"].ToString();
                            produtoNovo.DtCadastro = DateTime.Now;
                            produtoNovo.OpCadastro = Principal.usuario;
                            produtoNovo.ProdID = id;
                            produtoNovo.ProdRegistroMS = dtProdNovos.Rows[i]["PROD_REGISTRO_MS"].ToString();

                            BancoDados.AbrirTrans();

                            if (!produtoNovo.InsereProdutosNovosAbcFarma(produtoNovo))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var detalheNovo = new ProdutoDetalhe();
                            detalheNovo.EstCodigo = Principal.estAtual;
                            detalheNovo.EmpCodigo = Principal.empAtual;
                            detalheNovo.ProdID = id;
                            detalheNovo.ProdCodigo = dtProdNovos.Rows[i]["PROD_CODIGO"].ToString();
                            detalheNovo.ProdLiberado = "A";
                            if (String.IsNullOrEmpty(dtProdNovos.Rows[i]["MED_GENE"].ToString()) || !dtProdNovos.Rows[i]["MED_GENE"].ToString().Equals("GEN"))
                            {
                                detalheNovo.DepCodigo = etico;
                            }
                            else if (dtProdNovos.Rows[i]["MED_GENE"].ToString().Equals("GEN"))
                            {
                                detalheNovo.DepCodigo = generico;
                            }
                            detalheNovo.ProdDTEstIni = DateTime.Now;
                            detalheNovo.ProdLista = dtProdNovos.Rows[i]["PROD_LISTA"].ToString() == "+" ? "P" : dtProdNovos.Rows[i]["PROD_LISTA"].ToString() == "-" ? "N" : "U";
                            detalheNovo.ProdEcf = "FF";
                            detalheNovo.ProdCest = dtProdNovos.Rows[i]["PROD_CEST"].ToString();
                            detalheNovo.ProdDCB = dtProdNovos.Rows[i]["PROD_DCB"].ToString();
                            detalheNovo.DtCadastro = DateTime.Now;
                            detalheNovo.OpCadastro = Principal.usuario;
                            detalheNovo.FabCodigo = fabricante;

                            if (!detalheNovo.InsereProdutosDetalheNovosAbcFarna(detalheNovo))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            var precoNovo = new Preco();
                            precoNovo.EstCodigo = Principal.estAtual;
                            precoNovo.EmpCodigo = Principal.empAtual;
                            precoNovo.ProdCodigo = dtProdNovos.Rows[i]["PROD_CODIGO"].ToString();
                            precoNovo.ProdID = id;
                            precoNovo.PreValor = Convert.ToDouble(dtProdNovos.Rows[i]["MED_PCO18"]);
                            precoNovo.ProdPMC = Convert.ToDouble(dtProdNovos.Rows[i]["MED_PCO18"]);
                            precoNovo.TabCodigo = 1;
                            precoNovo.DtCadastro = DateTime.Now;
                            precoNovo.OpCadastro = Principal.usuario;

                            if (!precoNovo.InsereProdutosPrecosNovosAbcFarma(precoNovo))
                            {
                                BancoDados.ErroTrans();
                                continue;
                            }

                            BancoDados.FecharTrans();
                            
                            id = id + 1;
                           
                        }

                        pbAtualiza.Value = contador;
                        Application.DoEvents();
                        contador = contador + 1;
                        lblRegistros.Text = "Registros cadastrados: " + contador;
                    }

                    return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + " PROD_CODIGO = " + prodCodigo, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
