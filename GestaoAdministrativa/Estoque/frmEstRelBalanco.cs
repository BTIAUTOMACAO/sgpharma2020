﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstRelBalanco : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Balanço");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();
  
        public frmEstRelBalanco(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void frmEstRelBalanco_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(120, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRelBalanco>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Limpar()
        {
            filtroProd = "";
            chkZerados.Checked = false;
            dtData.Value = DateTime.Now;
            rdbPrecoVenda.Checked = true;
            rdbAlfabetica.Checked = true;
            rdbNenhum.Checked = true;
            gbTipoRelatorio.Enabled = false;
            chkDepartamento.Enabled = false;
            chkClasse.Enabled = false;
            chkSubclasse.Enabled = false;
            rdbSintetico.Enabled = false;
            lblRegistros.Text = "";
            dtData.Focus();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void rdbOutras_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbOutras.Checked == true)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
                gbTipoRelatorio.Enabled = true;
                rdbSintetico.Checked = true;
                rdbAnalitico.Enabled = true;
                rdbSintetico.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
                gbTipoRelatorio.Enabled = false;
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var balanco = new Produto();
                if (rdbNenhum.Checked)
                {
                    if (dtData.Text == DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        dtRetorno = balanco.BalancoSemQuebra(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            frmRelatorioBalancoSemQuebra relatorio = new frmRelatorioBalancoSemQuebra(this, dtRetorno);
                            relatorio.Text = "Relatório de Balanço";
                            relatorio.Show();
                        }
                        else
                        {
                            MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtData.Focus();
                        }
                    }
                    else
                    {
                        dtRetorno = balanco.BalancoSemQuebra(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked);
                        if (dtRetorno.Rows.Count > 0)
                        {
                            BuscaEstoqueRetroativo(dtRetorno);

                            dtRetorno = balanco.BalancoSemQuebraSpool(Principal.empAtual, Principal.estAtual, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked);

                            if (dtRetorno.Rows.Count > 0)
                            {
                                frmRelatorioBalancoSemQuebra relatorio = new frmRelatorioBalancoSemQuebra(this, dtRetorno);
                                relatorio.Text = "Relatório de Balanço";
                                relatorio.Show();
                            }
                            else
                            {
                                MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtData.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dtData.Focus();
                        }
                    }
                }
                else
                {
                    if (dtData.Text == DateTime.Now.ToString("dd/MM/yyyy"))
                    {
                        if (!chkDepartamento.Checked && !chkClasse.Checked && !chkSubclasse.Checked)
                        {
                            MessageBox.Show("Necessário selecionar uma quebra!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            chkDepartamento.Focus();
                            return;
                        }

                        if (rdbSintetico.Checked)
                        {
                            dtRetorno = balanco.BalancoQuebraSintetico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                frmRelatorioBalancoSintetico relatorio = new frmRelatorioBalancoSintetico(this, dtRetorno);
                                relatorio.Text = "Relatório de Balanço Sintético";
                                relatorio.Show();
                            }
                            else
                            {
                                MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtData.Focus();
                            }
                        }
                        else
                        {
                            dtRetorno = balanco.BalancoQuebraAnalitico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                frmRelatorioBalancoAnalitico relatorio = new frmRelatorioBalancoAnalitico(this, dtRetorno);
                                relatorio.Text = "Relatório de Balanço Analítico";
                                relatorio.Show();
                            }
                            else
                            {
                                MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtData.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (!chkDepartamento.Checked && !chkClasse.Checked && !chkSubclasse.Checked)
                        {
                            MessageBox.Show("Necessário selecionar uma quebra!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            chkDepartamento.Focus();
                            return;
                        }

                        if (rdbSintetico.Checked)
                        {
                            dtRetorno = balanco.BalancoQuebraSintetico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                BuscaEstoqueRetroativo(dtRetorno);

                                dtRetorno = balanco.BalancoSpoolQuebraSintetico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2, Principal.nomeEstacao);
                                if (dtRetorno.Rows.Count > 0)
                                {
                                    frmRelatorioBalancoSintetico relatorio = new frmRelatorioBalancoSintetico(this, dtRetorno);
                                    relatorio.Text = "Relatório de Balanço Sintético";
                                    relatorio.Show();
                                }
                                else
                                {
                                    MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    dtData.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtData.Focus();
                            }
                        }
                        else
                        {
                            dtRetorno = balanco.BalancoQuebraAnalitico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                            if (dtRetorno.Rows.Count > 0)
                            {
                                BuscaEstoqueRetroativo(dtRetorno);

                                dtRetorno = balanco.BalancoSpoolQuebraAnalitico(Principal.empAtual, Principal.estAtual, rdbCusto.Checked == true ? 0 : rdbUltCusto.Checked == true ? 1 : 2, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2, Principal.nomeEstacao);
                                if (dtRetorno.Rows.Count > 0)
                                {
                                    frmRelatorioBalancoAnalitico relatorio = new frmRelatorioBalancoAnalitico(this, dtRetorno);
                                    relatorio.Text = "Relatório de Balanço Analítico";
                                    relatorio.Show();
                                }
                                else
                                {
                                    MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    dtData.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dtData.Focus();
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Balanço", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                lblRegistros.Text = "";
                Cursor = Cursors.Default;
            }
        }

        public void BuscaEstoqueRetroativo(DataTable dtRetorno)
        {
            Produto balanco = new Produto();
            int saldo = 0;
            DateTime dataIni;
            int qtdeEstoque = 0;
            int registros = 0;

            BancoDados.ExecuteNoQuery("DELETE FROM SPOOL_BALANCO", null);

            for (int i = 0; i < dtRetorno.Rows.Count ; i++)
            {
                Application.DoEvents();

                saldo = Convert.ToInt32(dtRetorno.Rows[i]["PROD_ESTINI"]);
                dataIni = Convert.ToDateTime(dtRetorno.Rows[i]["PROD_DTESTINI"]);

                if (DateTime.Compare(Convert.ToDateTime(dtData.Text), dataIni) > 0)
                {
                    //VENDAS FINALIZADAS//
                    qtdeEstoque = balanco.BuscaVendasProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, "F", 0);

                    saldo = saldo - qtdeEstoque;

                    //VENDAS CANCELADAS//
                    qtdeEstoque = balanco.BuscaVendasProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                       Principal.empAtual, Principal.estAtual, "C", 0);

                    saldo = saldo + qtdeEstoque;

                    //ENTRADA DE NOTAS//
                    qtdeEstoque = balanco.BuscaEntradaProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, 0);

                    saldo = saldo + qtdeEstoque;

                    //AJUSTE +//
                    qtdeEstoque = balanco.BuscaAjusteProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, "+", 0);

                    saldo = saldo + qtdeEstoque;

                    //AJUSTE -//
                    qtdeEstoque = balanco.BuscaAjusteProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, "-", 0);

                    saldo = saldo - qtdeEstoque;

                    //TRANSFERENCIA LANÇAMENTO//
                    qtdeEstoque = balanco.BuscaTransferenciaProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, 0);

                    saldo = saldo - qtdeEstoque;

                    //TRANSFERENCIA IMPORTAÇÃO//
                    qtdeEstoque = balanco.BuscaTransferenciaImpProdutoBalanco(dataIni, Convert.ToDateTime(dtData.Text), dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, 0);

                    saldo = saldo + qtdeEstoque;

                    //ALTERAÇÃO LINEAR//
                }
                else if (DateTime.Compare(Convert.ToDateTime(dtData.Text), dataIni) < 0)
                {
                    //VENDAS FINALIZADAS//
                    qtdeEstoque = balanco.BuscaVendasProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, "F", 1);

                    saldo = saldo - qtdeEstoque;

                    //VENDAS CANCELADAS//
                    qtdeEstoque = balanco.BuscaVendasProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                       Principal.empAtual, Principal.estAtual, "C", 1);

                    saldo = saldo + qtdeEstoque;

                    //ENTRADA DE NOTAS//
                    qtdeEstoque = balanco.BuscaEntradaProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, 1);

                    saldo = saldo + qtdeEstoque;

                    //AJUSTE +//
                    qtdeEstoque = balanco.BuscaAjusteProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, "+", 1);

                    saldo = saldo + qtdeEstoque;

                    //AJUSTE -//
                    qtdeEstoque = balanco.BuscaAjusteProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                    Principal.empAtual, Principal.estAtual, "-", 1);

                    saldo = saldo - qtdeEstoque;

                    //TRANSFERENCIA LANÇAMENTO//
                    qtdeEstoque = balanco.BuscaTransferenciaProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, 1);

                    saldo = saldo - qtdeEstoque;

                    //TRANSFERENCIA IMPORTAÇÃO//
                    qtdeEstoque = balanco.BuscaTransferenciaImpProdutoBalanco(Convert.ToDateTime(dtData.Text), dataIni, dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                        Principal.empAtual, Principal.estAtual, 1);

                    saldo = saldo + qtdeEstoque;

                    //ALTERAÇÃO LINEAR//

                }

                //INSERIR NA SPOOL BALANÇO//
                Principal.dtPesq = balanco.BuscaSpoolBalanco(Principal.nomeEstacao, dtRetorno.Rows[i]["PROD_CODIGO"].ToString());

                SpoolBalanco dadosBalanco = new SpoolBalanco();
                dadosBalanco.Estacao = Principal.nomeEstacao;
                dadosBalanco.ProdCodigo = dtRetorno.Rows[i]["PROD_CODIGO"].ToString();
                dadosBalanco.ProdDescricao = Funcoes.RemoveCaracter(dtRetorno.Rows[i]["PROD_DESCR"].ToString());
                dadosBalanco.ProdCusme = Convert.ToDouble(dtRetorno.Rows[i]["UNITARIO"]);
                dadosBalanco.ProdEstatual = saldo;
                dadosBalanco.ColCodigo = 0;

                if (Principal.dtPesq.Rows.Count == 0)
                {
                    balanco.InserirSpoolBalanco(dadosBalanco);
                }
                else
                {
                    balanco.AtualizaSpoolBalanco(dadosBalanco);
                }

                registros = registros + 1;

                lblRegistros.Text = "Registros: " + registros + ". Aguarde...";
                lblRegistros.Refresh();
            }
        }

        private void chkDepartamento_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDepartamento.Checked)
            {
                chkClasse.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkClasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkSubclasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSubclasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkClasse.Checked = false;
            }
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                rdbPrecoVenda.Focus();
            }
        }

        private void rdbNenhum_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbNenhum.Checked == false)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
                gbTipoRelatorio.Enabled = true;
                rdbSintetico.Checked = true;
                rdbAnalitico.Enabled = true;
                rdbSintetico.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
                gbTipoRelatorio.Enabled = false;
            }
        }
    }
}
