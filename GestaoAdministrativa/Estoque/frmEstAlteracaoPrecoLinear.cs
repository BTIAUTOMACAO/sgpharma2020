﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAlteracaoPrecoLinear : Form, Botoes
    {
        DataTable dtProdutos;
        private ToolStripButton tsbAlteracaoPrecoLinear = new ToolStripButton("Alteração de Preço");
        private ToolStripSeparator tssAlteracaoPrecoLinear = new ToolStripSeparator();

        public frmEstAlteracaoPrecoLinear(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmEstAlteracaoPrecoLinear_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtCodigoBarras.Focus();
                CarregaCompos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregaCompos()
        {
            DataTable dtDepartamento = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            dtDepartamento.Rows.Add(0, "TODOS OS DEPARTAMENTOS");
            cmbDepartamento.DataSource = dtDepartamento;
            cmbDepartamento.DisplayMember = "DEP_DESCR";
            cmbDepartamento.ValueMember = "DEP_CODIGO";
            cmbDepartamento.SelectedValue = 0;

            DataTable dtClasse = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'S'");
            dtClasse.Rows.Add(0, "TODAS AS CLASSES");
            cmbClasses.DataSource = dtClasse;
            cmbClasses.DisplayMember = "CLAS_DESCR";
            cmbClasses.ValueMember = "CLAS_CODIGO";
            cmbClasses.SelectedValue = 0;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscaProdutos();
        }

        public void BuscaProdutos()
        {
            Produto pro = new Produto();
            dtProdutos = pro.BuscaProdutosPreco(txtCodigoBarras.Text, txtDescricao.Text, Convert.ToInt32(cmbDepartamento.SelectedValue), Convert.ToInt32(cmbClasses.SelectedValue));
            if (dtProdutos.Rows.Count > 0)
            {
                dgProdutos.DataSource = dtProdutos;
                btnCalcularTodos.Enabled = true;
                lblTotalRegistros.Visible = true;
                lblTotalRegistros.Text = "Total de Registros " + dgProdutos.RowCount;
            }
            else
            {
                dgProdutos.DataSource = dtProdutos;
                txtCodigoBarras.Focus();
                MessageBox.Show("Nenhum registro encontrado.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAtualizarTodos_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtPorcentagem.Text))
            {
                MessageBox.Show("% de Atualização não pode estar vazio", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                dgProdutos.Columns["VALOR_ATUALIZADO"].Visible = true;
                dgProdutos.Columns["MARGEM_ATUALIZADA"].Visible = true;
                dgProdutos.Columns["PRE_VALOR"].ReadOnly = true;
                Double calculaMargem;

                for (int i = 0; i < dgProdutos.RowCount; i++)
                {
                    dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value = String.Format("{0:0.00}", (Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value) * (Convert.ToDouble(txtPorcentagem.Text) / 100)) + Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value));
                    calculaMargem = Convert.ToDouble(((Convert.ToDouble(dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) / Convert.ToDouble(dgProdutos.Rows[i].Cells["PROD_ULTCUSME"].Value)) - 1) * 100);
                    dgProdutos.Rows[i].Cells["MARGEM_ATUALIZADA"].Value = Double.IsInfinity(calculaMargem) ? "0" : Double.IsNaN(calculaMargem) ? "0" : String.Format("{0:0.00}", calculaMargem);
                }
                btnAtualiza.Enabled = true;
                lblTotalRegistros.Visible = true;
            }
        }

        private void txtPorcentagem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if (e.KeyChar == 13)
                btnCalcularTodos.PerformClick();
        }

        private void dgProdutos_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            btnCalcularTodos.Enabled = false;
        }

        private async void btnAtualiza_Click(object sender, EventArgs e)
        {

            btnAtualiza.Enabled = false;
            btnCalcularTodos.Enabled = false;

            if (AtualizaEstoqueLocal().Equals(true))
            {
                bool retorno = await AtualizaEstoqueExterno();
                if (retorno.Equals(true))
                {
                    pbAtualiza.Visible = false;
                    btnAtualiza.Enabled = false;
                    btnCalcularTodos.Enabled = false;
                    dgProdutos.Columns["PRE_VALOR"].ReadOnly = false;
                    lblTotalRegistros.Text = "";
                    MessageBox.Show("Atualização realizada com sucesso", "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }
            }

        }

        public async Task<bool> AtualizaEstoqueExterno()
        {
            try
            {
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = dgProdutos.RowCount;

                string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
                string enderecoWebService = Funcoes.LeParametro(9, "54", true);
                if (Funcoes.ChecaParamentos(9, "51").Equals("S"))
                {
                    for (int i = 0; i < dgProdutos.RowCount; i++)
                    {
                        pbAtualiza.Value = i;
                        lblTotalRegistros.Text = " Atualizando " + (i + 1) + " de " + dgProdutos.RowCount + " - Atualizando Preço Externo - Aguarde";
                        lblTotalRegistros.Refresh();
                        if (dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value != null)
                        {
                            if (dgProdutos.Rows[i].Cells["PRE_VALOR"].Value != dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value)
                            {
                                using (var client = new HttpClient())
                                {
                                    client.BaseAddress = new Uri(enderecoWebService);
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaPreco?codBarra=" + dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value + "&precoCusto=" + dgProdutos.Rows[i].Cells["PROD_ULTCUSME"].Value.ToString().Replace(",", ".") + "&precoVenda=" + dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value.ToString().Replace(",", ".") + "&codEstabelecimento=" + codEstabelecimento);
                                    if (!response.IsSuccessStatusCode)
                                    {
                                        MessageBox.Show("Erro ao Cadastrar!", "Produtos Preço Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }


                                }
                            }
                        }
                    }

                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro :" + ex.Message, "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizaEstoqueLocal()
        {

            BancoDados.AbrirTrans();
            ProdutoDetalhe prod = new ProdutoDetalhe();
            Preco preco = new Preco();
            pbAtualiza.Visible = true;
            pbAtualiza.Value = 0;
            pbAtualiza.Step = 1;
            pbAtualiza.Maximum = dgProdutos.RowCount;

            for (int i = 0; i < dgProdutos.RowCount; i++)
            {
                pbAtualiza.Value = i;
                lblTotalRegistros.Text = " Atualizando " + (i + 1) + " de " + dgProdutos.RowCount + " - Atualizando Preço Local - Aguarde";
                lblTotalRegistros.Refresh();
                if (dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value != null)
                {
                    if (dgProdutos.Rows[i].Cells["PRE_VALOR"].Value != dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value)
                    {
                        if (prod.AtualizaMargem(Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ID"].Value), Convert.ToDouble(dgProdutos.Rows[i].Cells["PROD_MARGEM"].Value), Convert.ToDouble(dgProdutos.Rows[i].Cells["MARGEM_ATUALIZADA"].Value)).Equals(true))
                        {
                            if (preco.AtualizaPrecoPorID(Convert.ToInt32(dgProdutos.Rows[i].Cells["PROD_ID"].Value), Convert.ToDouble(dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value), Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value)).Equals(true))
                            {
                                LogAlteracaoPreco logPreco = new LogAlteracaoPreco()
                                {
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    ProCodigo = dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                    DataAlteracao = DateTime.Now,
                                    ValorAntigo = Convert.ToDouble(dgProdutos.Rows[i].Cells["PRE_VALOR"].Value),
                                    ValorNovo = Convert.ToDouble(dgProdutos.Rows[i].Cells["VALOR_ATUALIZADO"].Value),
                                    Usuario = Principal.usuario

                                };
                                if (logPreco.InsereLog(logPreco).Equals(false))
                                {
                                    MessageBox.Show("Erro ao tentar atualizar os Preços", "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    return false;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Erro ao tentar atualizar os Preços", "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                BancoDados.ErroTrans();
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Erro ao tentar atualizar as Margens", "Atualização de Preço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            BancoDados.ErroTrans();
                            return false;
                        }

                    }
                }
            }
            BancoDados.FecharTrans();
            return true;
        }

        public void Limpar()
        {

            if (!dgProdutos.Rows.Count.Equals(0))
            {
                dtProdutos.Clear();
                dgProdutos.DataSource = dtProdutos;
            }
            lblTotalRegistros.Visible = false;
            cmbDepartamento.SelectedValue = 0;
            cmbClasses.SelectedValue = 0;
            txtCodigoBarras.Text = "";
            txtDescricao.Text = "";
            txtPorcentagem.Text = "";
            btnCalcularTodos.Enabled = false;

        }

        private void dgProdutos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Double calculaMargem;

            calculaMargem = Convert.ToDouble(((Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["VALOR_ATUALIZADO"].Value) / Convert.ToDouble(dgProdutos.Rows[e.RowIndex].Cells["PROD_ULTCUSME"].Value)) - 1) * 100);
            dgProdutos.Columns["MARGEM_ATUALIZADA"].Visible = true;
            dgProdutos.Rows[e.RowIndex].Cells["MARGEM_ATUALIZADA"].Value = Double.IsInfinity(calculaMargem) ? "0" : Double.IsNaN(calculaMargem) ? "0" : String.Format("{0:0.00}", calculaMargem);
            btnAtualiza.Enabled = true;
        }

        public void Botao()
        {
            try
            {
                this.tsbAlteracaoPrecoLinear.AutoSize = false;
                this.tsbAlteracaoPrecoLinear.Image = Properties.Resources.estoque;
                this.tsbAlteracaoPrecoLinear.Size = new System.Drawing.Size(150, 20);
                this.tsbAlteracaoPrecoLinear.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAlteracaoPrecoLinear);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAlteracaoPrecoLinear);
                tsbAlteracaoPrecoLinear.Click += delegate
                {
                    var alteracaoPrecoLinear = Application.OpenForms.OfType<frmEstAlteracaoPrecoLinear>().FirstOrDefault();
                    Util.BotoesGenericos();
                    alteracaoPrecoLinear.Focus();

                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajustes de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Primeiro()
        {
        }

        public void Ultimo()
        {
        }

        public void Proximo()
        {
        }

        public void Anterior()
        {
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAlteracaoPrecoLinear);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAlteracaoPrecoLinear);
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Alteração de Preço Linear", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtCodigoBarras.Text))
                {
                    txtDescricao.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtDescricao.Text))
                {
                    cmbDepartamento.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }
    }
}
