﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Data.Odbc;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAtuIBPT : Form, Botoes 
    {

        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbAtualizaIBPT = new ToolStripButton("At. Tabela IBPT");
        private ToolStripSeparator tssAtualizaIBPT = new ToolStripSeparator();
        private int contador;
        #endregion

        public frmEstAtuIBPT(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmEstAtuIBPT_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstAtuIBPT_Shown(object sender, EventArgs e)
        {
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAtualizaIBPT.AutoSize = false;
                this.tsbAtualizaIBPT.Image = Properties.Resources.estoque;
                this.tsbAtualizaIBPT.Size = new System.Drawing.Size(160, 20);
                this.tsbAtualizaIBPT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAtualizaIBPT);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAtualizaIBPT);
                tsbAtualizaIBPT.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstAtuIBPT>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAtualizaIBPT);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAtualizaIBPT);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnArquivo_Click(object sender, EventArgs e)
        {
            await VerificaAtualizacao();
        }

        public async Task<List<EstabelecimentoFilial>> IdentificaNovaVersao()
        {
            List<EstabelecimentoFilial> allItems = new List<Negocio.EstabelecimentoFilial>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaDadosDoEstabelecimentos?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<bool> VerificaAtualizacao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Atu. IBPT";

                List<EstabelecimentoFilial> dados = await IdentificaNovaVersao();
                if (dados.Count > 0)
                {
                    if(dados[0].AtualizaIBPT == "S")
                    {
                        Principal.mensagem = "Existe uma nova atualização disponível!";
                        btnArquivo.Enabled = true;
                        btnTabelaIBPT.Enabled = true;
                        btnTabelaIBPT.Focus();
                    }
                    else
                    {
                        Principal.mensagem = "Não há nova versão disponível!";
                        btnArquivo.Enabled = true;
                    }

                    Funcoes.Avisa();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                btnArquivo.Enabled = true;
                btnTabelaIBPT.Enabled = false;
                btnArquivo.Focus();
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<List<Ibpt>> BuscaImpostos()
        {
            List<Ibpt> allItems = new List<Negocio.Ibpt>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("TabelaIBPT/BuscaImpostos");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Ibpt>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<bool> AtualizaIBPT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                btnArquivo.Enabled = false;
                btnTabelaIBPT.Enabled = false;

                Application.DoEvents();
                tslRegistros.Text = "Atualizando informações da Tabela IBPT. Aguarde...";
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;

                List<Ibpt> impostos = await BuscaImpostos();
                if (impostos.Count > 0)
                {
                    if (impostos[0].CodErro == "00")
                    {
                        pbAtualiza.Maximum = impostos.Count;
                        for (int i = 0; i < impostos.Count; i++)
                        {
                            var dadosIbpt = new Ibpt();
                            dadosIbpt.Ncm = impostos[i].Ncm;
                            dadosIbpt.Nacional = impostos[i].Nacional;
                            dadosIbpt.Importado = impostos[i].Importado;
                            dadosIbpt.Estadual = impostos[i].Estadual;
                            dadosIbpt.Municipal = impostos[i].Municipal;

                            if (!dadosIbpt.ExcluiRegistroPorNCM(dadosIbpt))
                                return false;

                            if (!dadosIbpt.InsereRegistros(dadosIbpt))
                                return false;

                            pbAtualiza.Value = contador;
                            Application.DoEvents();
                            contador = contador + 1;
                            lblRegistros.Text = "Registros: " + Convert.ToString(contador);
                        }
                    }
                    else
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                btnArquivo.Enabled = true;
                btnTabelaIBPT.Enabled = true;
                Cursor = Cursors.Default;
                btnTabelaIBPT.Focus();
                pbAtualiza.Visible = false;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
            }
        }

        public async void AtualizaStatusTabela()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false) + "&atualiza=N&campo=ATUALIZA_IBPT");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Estabelecimento coluna Atualiza_IBPT", "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnTabelaIBPT_Click(object sender, EventArgs e)
        {
            try
            {
                bool retorno = await AtualizaIBPT();
                if (retorno)
                {
                    AtualizaStatusTabela();
                    MessageBox.Show("Atualização Concluída!", "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("Erro na Atualização. Contate o Suporte!", "Atualiza Tabela IBPT", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                btnArquivo.Enabled = true;
                btnTabelaIBPT.Enabled = true;
                Cursor = Cursors.Default;
                btnTabelaIBPT.Focus();
                pbAtualiza.Visible = false;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
            }
        }
       
        public void ImprimirRelatorio() { }

        public void Limpar() { }

        public bool Excluir() { return true; }

        public bool Incluir() { return true; }

        public bool Atualiza() { return true; }

        public void Primeiro() { }

        public void Ultimo() { }

        public void Proximo() { }

        public void Anterior() { }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
