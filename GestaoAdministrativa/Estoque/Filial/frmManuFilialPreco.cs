﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque.Filial
{
    public partial class frmManuFilialPreco : Form, Botoes
    {
        private ToolStripButton tsbManutencao = new ToolStripButton("Manut. Filial Preço");
        private ToolStripSeparator tssManutencao = new ToolStripSeparator();
        List<EstabelecimentoFilial> dadosEstb = new List<EstabelecimentoFilial>();
        List<EstabelecimentoFilial> dadosEstbPesq = new List<EstabelecimentoFilial>();

        public frmManuFilialPreco(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if (control.Name != "txtPreco")
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmManuFilialPreco_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbManutencao.AutoSize = false;
                this.tsbManutencao.Image = Properties.Resources.estoque;
                this.tsbManutencao.Size = new System.Drawing.Size(140, 20);
                this.tsbManutencao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssManutencao);
                tsbManutencao.Click += delegate
                {
                    var transferencia = Application.OpenForms.OfType<frmManuFilialPreco>().FirstOrDefault();
                    Util.BotoesGenericos();
                    transferencia.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void frmManuFilialPreco_Shown(object sender, EventArgs e)
        {
            cmbGrupos.Enabled = true;
            dadosEstb = await EnviaOuRecebeDadosMysql.BuscaEstabelecimentos();

            dadosEstb.Add(new EstabelecimentoFilial
            {
                CodEstabelecimento = 0,
                NomeEstabelecimento = "TODAS AS FILIAIS"

            });

            cmbGrupos.DataSource = dadosEstb;
            cmbGrupos.ValueMember = "codEstabelecimento";
            cmbGrupos.DisplayMember = "nomeEstabelecimento";

            cmbGrupos.Text = "TODAS AS FILIAIS";

            dadosEstbPesq = await EnviaOuRecebeDadosMysql.BuscaEstabelecimentos();

            dadosEstbPesq.Add(new EstabelecimentoFilial
            {
                CodEstabelecimento = 0,
                NomeEstabelecimento = "TODAS AS FILIAIS"

            });


            cmbGruposPesquisa.DataSource = dadosEstbPesq;
            cmbGruposPesquisa.ValueMember = "codEstabelecimento";
            cmbGruposPesquisa.DisplayMember = "nomeEstabelecimento";

            cmbGruposPesquisa.Text = "TODAS AS FILIAIS";

            Principal.dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = -1;
            }

            Limpar();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }

                  
                    txtPreco.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PRE_VALOR"]);
                    txtValorNovo.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtValorNovo.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtValorNovo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbGrupos.Focus();
        }

        private void cmbGrupos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbVendedor.Focus();
        }

        private void cmbVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Produto não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtValorNovo.Text.Trim()))
            {
                Principal.mensagem = "Valor não pode ser em branco.";
                Funcoes.Avisa();
                txtValorNovo.Focus();
                return false;
            }

            if (Convert.ToDouble(txtValorNovo.Text.Trim()) == 0)
            {
                Principal.mensagem = "Valor não pode ser Zero.";
                Funcoes.Avisa();
                txtValorNovo.Focus();
                return false;
            }

            if (cmbVendedor.SelectedIndex == -1)
            {
                Principal.mensagem = "Selecione o Operador.";
                Funcoes.Avisa();
                cmbVendedor.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtObs.Text.Trim()))
            {
                Principal.mensagem = "Observação não pode ser em branco.";
                Funcoes.Avisa();
                txtObs.Focus();
                return false;
            }

            return true;
        }
        
        private async void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCampos())
                {
                    List<AtualizaPrecoFilial> listPrecos = new List<AtualizaPrecoFilial>();
                    if (cmbGrupos.Text == "TODAS AS FILIAIS")
                    {
                        for (int i = 0; i < dadosEstb.Count; i++)
                        {
                            if (dadosEstb[i].CodEstabelecimento != 0)
                            {
                                listPrecos.Add(new AtualizaPrecoFilial
                                {
                                    CodLojaOrigem = Convert.ToInt32(Funcoes.LeParametro(9, "52", false)),
                                    CodLojaDestino = dadosEstb[i].CodEstabelecimento,
                                    CodGrupo = Convert.ToInt32(Funcoes.LeParametro(9, "53", false)),
                                    CodDeBarras = txtCodBarras.Text,
                                    ValorDeVenda = Convert.ToDouble(txtValorNovo.Text),
                                    Atualiza = 'S',
                                    Operador = cmbVendedor.Text,
                                    Observacao = txtObs.Text
                                });
                            }
                        }
                    }
                    else
                    {
                        listPrecos.Add(new AtualizaPrecoFilial
                        {
                            CodLojaOrigem = Convert.ToInt32(Funcoes.LeParametro(9, "52", false)),
                            CodLojaDestino = Convert.ToInt32(cmbGrupos.SelectedValue),
                            CodGrupo = Convert.ToInt32(Funcoes.LeParametro(9, "53", false)),
                            CodDeBarras = txtCodBarras.Text,
                            ValorDeVenda = Convert.ToDouble(txtValorNovo.Text),
                            Atualiza = 'S',
                            Operador = cmbVendedor.Text,
                            Observacao = txtObs.Text
                        });
                    }

                    bool retorno = await EnviaOuRecebeDadosMysql.AtualizacaoDePrecoFilial(listPrecos);

                    if (retorno)
                    {
                        MessageBox.Show("Atualização Enviada para a(s) filial(is)", "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                    }
                    else
                    {
                        MessageBox.Show("Erro ao enviar atualização para a(s) filial(is). Contate o Suporte", "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Limpar()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            cmbGrupos.Text = "TODAS AS FILIAIS";
            cmbVendedor.SelectedIndex = -1;
            txtObs.Text = "";
            cmbGruposPesquisa.Text = "TODAS AS FILIAIS";
            chkAtualizado.Checked = true;
            txtValorNovo.Text = "";
            txtCodBarras.Focus();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            List<AtualizaPrecoFilial> retorno = await EnviaOuRecebeDadosMysql.BuscaAtualizacaoPreco((cmbGruposPesquisa.Text == "TODAS AS LOJAS" ? 0 : Convert.ToInt32(cmbGruposPesquisa.SelectedValue)),
               Convert.ToInt32(Funcoes.LeParametro(9,"53", false)), chkAtualizado.Checked == true ? 'E' : 'S', txtPesqCod.Text);

            if(retorno[0].CodErro == "00")
            {
                dgProdutos.DataSource = retorno;
            }
            else
            {
                dgProdutos.Rows.Clear();
                MessageBox.Show(retorno[0].Mensagem,"Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button1.Focus();
            }
        }

        private void cmbGruposPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPesqCod.Focus();
        }

        private void txtPesqCod_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkAtualizado.Focus();
        }

        private void chkAtualizado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                button1.PerformClick();
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    button1.PerformClick();
                    break;
            }
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtValorNovo_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbGrupos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAdicionar.PerformClick();
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbGruposPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtPesqCod_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void chkAtualizado_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void button1_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void frmManuFilialPreco_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        bool Botoes.Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssManutencao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Preço Filial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
