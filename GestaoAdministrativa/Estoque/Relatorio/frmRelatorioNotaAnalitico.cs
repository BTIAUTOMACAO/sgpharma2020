﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque.Relatorio
{
    public partial class frmRelatorioNotaAnalitico : Form
    {
        private frmEstRelEntDoc relatorio;
        private DataTable dtRelatorio;

        public frmRelatorioNotaAnalitico(frmEstRelEntDoc frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
            rwRelatorio.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        private void frmRelatorioNotaAnalitico_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[5];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", relatorio.dtInicial.Value.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", relatorio.dtFinal.Value.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("quebra", relatorio.rdbEmissao.Checked ? "Emissão" : "Lançamento");
            parametro[4] = new ReportParameter("data", relatorio.rdbEmissao.Checked ? "Lançamento" : "Emissão");
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var rel = new ReportDataSource("analitico", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(rel);
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            var tItens = new Entrada();
            DataTable dtItens = tItens.BuscaItensPorEntID(Principal.empAtual, Principal.estAtual, e.Parameters["END_ID"].Values.First());

            var itens = new ReportDataSource("dsItens", dtItens);
            e.DataSources.Add(itens);

            DataTable dtParcelas = tItens.BuscaParcelasPorEntID(Principal.empAtual, Principal.estAtual, e.Parameters["END_ID"].Values.First());

            var parcelas = new ReportDataSource("dsParcelas", dtParcelas);
            e.DataSources.Add(parcelas);
        }
    }
}
