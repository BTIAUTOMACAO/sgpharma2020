﻿namespace GestaoAdministrativa.Estoque.Relatorio
{
    partial class frmRelatorioEstoqueSngpc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rwRelatorio = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rwRelatorio
            // 
            this.rwRelatorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rwRelatorio.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Estoque.Relatorio.ArquivosReport.relEstoqueSngpc.rdlc";
            this.rwRelatorio.Location = new System.Drawing.Point(0, 0);
            this.rwRelatorio.Name = "rwRelatorio";
            this.rwRelatorio.Size = new System.Drawing.Size(978, 539);
            this.rwRelatorio.TabIndex = 1;
            this.rwRelatorio.Load += new System.EventHandler(this.frmRelatorioEstoqueSngpc_Load);
            // 
            // frmRelatorioEstoqueSngpc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 539);
            this.Controls.Add(this.rwRelatorio);
            this.Name = "frmRelatorioEstoqueSngpc";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rwRelatorio;
    }
}