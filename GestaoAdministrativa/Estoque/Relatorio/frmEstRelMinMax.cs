﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque.Relatorio
{
    public partial class frmEstRelMaxMin : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Estoque Mín./Máx.");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmEstRelMaxMin(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }


        private void frmEstRelMaxMin_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRelMaxMin>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            filtroProd = "";
            chkZerados.Checked = false;
            rdbAlfabetica.Checked = true;
            rdbNenhum.Checked = true;
            chkDepartamento.Enabled = false;
            chkClasse.Enabled = false;
            chkSubclasse.Enabled = false;
            rdbMinimo.Checked = true;
            rdbMinimo.Focus();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtRetorno = new DataTable();
                var estoque = new Produto();
                if (rdbNenhum.Checked)
                {
                    dtRetorno = estoque.EStoqueMinMaxSemQuebra(Principal.estAtual, Principal.empAtual, rdbMinimo.Checked ? 0 : 1, filtroProd, rdbAlfabetica.Checked ? 0 : 1, chkZerados.Checked);
                    if(dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioEstoqueMinMax relatorio = new frmRelatorioEstoqueMinMax(this, dtRetorno);
                        relatorio.Text = "Relatório de Estoque Mín./Máx.";
                        relatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rdbMinimo.Focus();
                    }
                }
                else
                {
                    if (!chkDepartamento.Checked && !chkClasse.Checked && !chkSubclasse.Checked)
                    {
                        MessageBox.Show("Necessário selecionar uma quebra!", "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        chkDepartamento.Focus();
                        return;
                    }

                    dtRetorno = estoque.EStoqueMinMaxComQuebra(Principal.empAtual, Principal.estAtual, rdbMinimo.Checked == true ? 0 : 1, filtroProd, rdbAlfabetica.Checked == true ? 0 : 1, chkZerados.Checked, chkDepartamento.Checked == true ? 0 : chkClasse.Checked == true ? 1 : 2);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        frmRelatorioMinMaxQuebra relatorio = new frmRelatorioMinMaxQuebra(this, dtRetorno);
                        relatorio.Text = "Relatório de Estoque Mín./Máx.";
                        relatorio.Show();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Encontado!", "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rdbMinimo.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Estoque Min./Máx.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void rdbNenhum_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbNenhum.Checked == false)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
            }
        }

        private void rdbOutras_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbOutras.Checked == true)
            {
                chkDepartamento.Enabled = true;
                chkClasse.Enabled = true;
                chkSubclasse.Enabled = true;
            }
            else
            {
                rdbNenhum.Checked = true;
                chkDepartamento.Enabled = false;
                chkClasse.Enabled = false;
                chkSubclasse.Enabled = false;
            }
        }

        private void chkDepartamento_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDepartamento.Checked)
            {
                chkClasse.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkClasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkClasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkSubclasse.Checked = false;
            }
        }

        private void chkSubclasse_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSubclasse.Checked)
            {
                chkDepartamento.Checked = false;
                chkClasse.Checked = false;
            }
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
