﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque.Relatorio
{
    public partial class frmRelatorioReposicaoProdutos : Form
    {
        DateTime dtInicial, dtFinal;
        DataTable dtProdReposicao;
        int valor;

        public frmRelatorioReposicaoProdutos(DateTime dtIni, DateTime dtFi, DataTable prod, int v)
        {
            InitializeComponent();
            dtInicial = dtIni;
            dtFinal = dtFi;
            dtProdReposicao = prod;
            valor = v;
        }

        private void frmRelatorioReposicaoProdutos_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            CarregaProdutos();

            this.rpwReposicaoProduto.RefreshReport();
        }
        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("valorCompra", valor.ToString());
            
            this.rpwReposicaoProduto.LocalReport.SetParameters(parametro);

        }

        public void CarregaProdutos()
        {
            var dsProdutosReposicao = new ReportDataSource("Reposicao", dtProdReposicao);
            rpwReposicaoProduto.LocalReport.DataSources.Clear();
            rpwReposicaoProduto.LocalReport.DataSources.Add(dsProdutosReposicao);

        }

    }
}
