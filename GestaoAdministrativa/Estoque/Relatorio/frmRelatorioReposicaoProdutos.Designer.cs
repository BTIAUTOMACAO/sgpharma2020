﻿namespace GestaoAdministrativa.Estoque.Relatorio
{
    partial class frmRelatorioReposicaoProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelatorioReposicaoProdutos));
            this.rpwReposicaoProduto = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwReposicaoProduto
            // 
            this.rpwReposicaoProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwReposicaoProduto.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Estoque.Relatorio.RelatorioReposicaoProdutoMaster.rdlc";
            this.rpwReposicaoProduto.Location = new System.Drawing.Point(0, 0);
            this.rpwReposicaoProduto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rpwReposicaoProduto.Name = "rpwReposicaoProduto";
            this.rpwReposicaoProduto.Size = new System.Drawing.Size(994, 578);
            this.rpwReposicaoProduto.TabIndex = 0;
            // 
            // frmRelatorioReposicaoProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwReposicaoProduto);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRelatorioReposicaoProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relatório de Reposição de Produtos";
            this.Load += new System.EventHandler(this.frmRelatorioReposicaoProdutos_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwReposicaoProduto;
    }
}