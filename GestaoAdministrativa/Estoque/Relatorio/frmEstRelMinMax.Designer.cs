﻿namespace GestaoAdministrativa.Estoque.Relatorio
{
    partial class frmEstRelMaxMin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstRelMaxMin));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.chkZerados = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chkSubclasse = new System.Windows.Forms.CheckBox();
            this.chkClasse = new System.Windows.Forms.CheckBox();
            this.chkDepartamento = new System.Windows.Forms.CheckBox();
            this.rdbOutras = new System.Windows.Forms.RadioButton();
            this.rdbNenhum = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rdbNumerica = new System.Windows.Forms.RadioButton();
            this.rdbAlfabetica = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbMaximo = new System.Windows.Forms.RadioButton();
            this.rdbMinimo = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnPesquisar);
            this.groupBox2.Controls.Add(this.btnFiltrar);
            this.groupBox2.Controls.Add(this.chkZerados);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(8, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(947, 185);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtros";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(431, 128);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(138, 48);
            this.btnPesquisar.TabIndex = 162;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.Image")));
            this.btnFiltrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFiltrar.Location = new System.Drawing.Point(337, 128);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(88, 48);
            this.btnFiltrar.TabIndex = 161;
            this.btnFiltrar.Text = "Filtros";
            this.btnFiltrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // chkZerados
            // 
            this.chkZerados.AutoSize = true;
            this.chkZerados.Location = new System.Drawing.Point(6, 156);
            this.chkZerados.Name = "chkZerados";
            this.chkZerados.Size = new System.Drawing.Size(137, 20);
            this.chkZerados.TabIndex = 7;
            this.chkZerados.Text = "Produtos Zerados";
            this.chkZerados.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.chkSubclasse);
            this.groupBox6.Controls.Add(this.chkClasse);
            this.groupBox6.Controls.Add(this.chkDepartamento);
            this.groupBox6.Controls.Add(this.rdbOutras);
            this.groupBox6.Controls.Add(this.rdbNenhum);
            this.groupBox6.Location = new System.Drawing.Point(276, 21);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(293, 100);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Quebra Por";
            // 
            // chkSubclasse
            // 
            this.chkSubclasse.AutoSize = true;
            this.chkSubclasse.Enabled = false;
            this.chkSubclasse.Location = new System.Drawing.Point(200, 55);
            this.chkSubclasse.Name = "chkSubclasse";
            this.chkSubclasse.Size = new System.Drawing.Size(91, 20);
            this.chkSubclasse.TabIndex = 6;
            this.chkSubclasse.Text = "Subclasse";
            this.chkSubclasse.UseVisualStyleBackColor = true;
            this.chkSubclasse.CheckedChanged += new System.EventHandler(this.chkSubclasse_CheckedChanged);
            // 
            // chkClasse
            // 
            this.chkClasse.AutoSize = true;
            this.chkClasse.Enabled = false;
            this.chkClasse.Location = new System.Drawing.Point(127, 55);
            this.chkClasse.Name = "chkClasse";
            this.chkClasse.Size = new System.Drawing.Size(68, 20);
            this.chkClasse.TabIndex = 5;
            this.chkClasse.Text = "Classe";
            this.chkClasse.UseVisualStyleBackColor = true;
            this.chkClasse.CheckedChanged += new System.EventHandler(this.chkClasse_CheckedChanged);
            // 
            // chkDepartamento
            // 
            this.chkDepartamento.AutoSize = true;
            this.chkDepartamento.Enabled = false;
            this.chkDepartamento.Location = new System.Drawing.Point(6, 55);
            this.chkDepartamento.Name = "chkDepartamento";
            this.chkDepartamento.Size = new System.Drawing.Size(117, 20);
            this.chkDepartamento.TabIndex = 4;
            this.chkDepartamento.Text = "Departamento";
            this.chkDepartamento.UseVisualStyleBackColor = true;
            this.chkDepartamento.CheckedChanged += new System.EventHandler(this.chkDepartamento_CheckedChanged);
            // 
            // rdbOutras
            // 
            this.rdbOutras.AutoSize = true;
            this.rdbOutras.ForeColor = System.Drawing.Color.Navy;
            this.rdbOutras.Location = new System.Drawing.Point(99, 21);
            this.rdbOutras.Name = "rdbOutras";
            this.rdbOutras.Size = new System.Drawing.Size(192, 20);
            this.rdbOutras.TabIndex = 3;
            this.rdbOutras.TabStop = true;
            this.rdbOutras.Text = "Depto, Classe e Subclasse";
            this.rdbOutras.UseVisualStyleBackColor = true;
            this.rdbOutras.CheckedChanged += new System.EventHandler(this.rdbOutras_CheckedChanged);
            // 
            // rdbNenhum
            // 
            this.rdbNenhum.AutoSize = true;
            this.rdbNenhum.ForeColor = System.Drawing.Color.Navy;
            this.rdbNenhum.Location = new System.Drawing.Point(6, 21);
            this.rdbNenhum.Name = "rdbNenhum";
            this.rdbNenhum.Size = new System.Drawing.Size(87, 20);
            this.rdbNenhum.TabIndex = 2;
            this.rdbNenhum.TabStop = true;
            this.rdbNenhum.Text = "Nenhuma";
            this.rdbNenhum.UseVisualStyleBackColor = true;
            this.rdbNenhum.CheckedChanged += new System.EventHandler(this.rdbNenhum_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rdbNumerica);
            this.groupBox5.Controls.Add(this.rdbAlfabetica);
            this.groupBox5.Location = new System.Drawing.Point(167, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(103, 100);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ordem";
            // 
            // rdbNumerica
            // 
            this.rdbNumerica.AutoSize = true;
            this.rdbNumerica.ForeColor = System.Drawing.Color.Navy;
            this.rdbNumerica.Location = new System.Drawing.Point(10, 55);
            this.rdbNumerica.Name = "rdbNumerica";
            this.rdbNumerica.Size = new System.Drawing.Size(87, 20);
            this.rdbNumerica.TabIndex = 3;
            this.rdbNumerica.TabStop = true;
            this.rdbNumerica.Text = "Numérica";
            this.rdbNumerica.UseVisualStyleBackColor = true;
            // 
            // rdbAlfabetica
            // 
            this.rdbAlfabetica.AutoSize = true;
            this.rdbAlfabetica.ForeColor = System.Drawing.Color.Navy;
            this.rdbAlfabetica.Location = new System.Drawing.Point(10, 29);
            this.rdbAlfabetica.Name = "rdbAlfabetica";
            this.rdbAlfabetica.Size = new System.Drawing.Size(90, 20);
            this.rdbAlfabetica.TabIndex = 2;
            this.rdbAlfabetica.TabStop = true;
            this.rdbAlfabetica.Text = "Alfabética";
            this.rdbAlfabetica.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdbMaximo);
            this.groupBox3.Controls.Add(this.rdbMinimo);
            this.groupBox3.Location = new System.Drawing.Point(6, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(155, 100);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo";
            // 
            // rdbMaximo
            // 
            this.rdbMaximo.AutoSize = true;
            this.rdbMaximo.ForeColor = System.Drawing.Color.Navy;
            this.rdbMaximo.Location = new System.Drawing.Point(19, 55);
            this.rdbMaximo.Name = "rdbMaximo";
            this.rdbMaximo.Size = new System.Drawing.Size(131, 20);
            this.rdbMaximo.TabIndex = 2;
            this.rdbMaximo.TabStop = true;
            this.rdbMaximo.Text = "Estoque Máximo";
            this.rdbMaximo.UseVisualStyleBackColor = true;
            // 
            // rdbMinimo
            // 
            this.rdbMinimo.AutoSize = true;
            this.rdbMinimo.ForeColor = System.Drawing.Color.Navy;
            this.rdbMinimo.Location = new System.Drawing.Point(19, 29);
            this.rdbMinimo.Name = "rdbMinimo";
            this.rdbMinimo.Size = new System.Drawing.Size(127, 20);
            this.rdbMinimo.TabIndex = 1;
            this.rdbMinimo.TabStop = true;
            this.rdbMinimo.Text = "Estoque Mínimo";
            this.rdbMinimo.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 152;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Estoque Máximo/Mínimo";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmEstRelMaxMin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstRelMaxMin";
            this.Text = "frmEstRelMinMax";
            this.Load += new System.EventHandler(this.frmEstRelMaxMin_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.CheckBox chkZerados;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chkSubclasse;
        private System.Windows.Forms.CheckBox chkClasse;
        private System.Windows.Forms.CheckBox chkDepartamento;
        private System.Windows.Forms.RadioButton rdbOutras;
        private System.Windows.Forms.RadioButton rdbNenhum;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rdbNumerica;
        private System.Windows.Forms.RadioButton rdbAlfabetica;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        public System.Windows.Forms.RadioButton rdbMaximo;
        public System.Windows.Forms.RadioButton rdbMinimo;
    }
}