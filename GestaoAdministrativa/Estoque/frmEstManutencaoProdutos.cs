﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstManutencaoProdutos : Form, Botoes
    {
        private ToolStripButton tsbEstManutencaoProdutos = new ToolStripButton("Manutenção Produtos");
        private ToolStripSeparator tssEstManutencaoProdutos = new ToolStripSeparator();
        DataTable dtProd;
        public frmEstManutencaoProdutos(MDIPrincipal menu)
        {
            Principal.mdiPrincipal = menu;
            InitializeComponent();
        }

        private void frmEstManutencaoProdutos_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            txtCodigoBarras.Focus();
            dgProdutos.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dgProdutos.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscaProdutos();
        }

        public void BuscaProdutos()
        {
            Cursor = Cursors.WaitCursor;
            Produto pro = new Produto();
            dtProd = pro.BuscaProdutos(txtCodigoBarras.Text, txtDescricao.Text, ckbCodBarras.Checked, (cmbDepartamento.SelectedIndex == -1 ? 0 : Convert.ToInt32(cmbDepartamento.SelectedValue)));
            if (dtProd.Rows.Count.Equals(0))
            {
                txtCodigoBarras.Focus();
                MessageBox.Show("Nenhum registro encontrado.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            dgProdutos.DataSource = dtProd;
            lblTotalRegistros.Visible = true;
            lblTotalRegistros.Text = "Total de Registros Encontrados: " + dtProd.Rows.Count;
            txtCodigoBarras.Text = "";
            txtDescricao.Text = "";
            dgProdutos.Focus();
            Cursor = Cursors.Default;
        }

        private void txtCodigoBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtCodigoBarras.Text))
                {
                    BuscaProdutos();
                }
                else
                    txtDescricao.Focus();
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && !String.IsNullOrEmpty(txtDescricao.Text.Trim()))
            {
                BuscaProdutos();
            }
            //else //Toda vez que eu digito uma letra ele tirava o foco; 
            //    cmbDepartamento.Focus();
        }

        private void btnMarcarTodos_Click(object sender, EventArgs e)
        {
            if (dgProdutos.Rows.Count.Equals(0))
            {
                txtCodigoBarras.Focus();
                MessageBox.Show("Nenhum registro encontrado.", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                bool marcar = dgProdutos.Rows[0].Cells["Excluir"].Value == null ? true : false;

                for (int i = 0; i < dgProdutos.Rows.Count; i++)
                {
                    dgProdutos.Rows[i].Cells["Excluir"].Value = marcar;
                }
                Cursor = Cursors.Default;

            }
        }

        private void dgProdutos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            btnExcluir.Enabled = true;
        }

        private void dgProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //btnExcluir.Enabled = true; //Mesmo se o produto não estivesse marcado e eu clicasse nele, ele iria ser excluido; 
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Tem certeza que deseja excluir esses produtos", "Produtos", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question).Equals(DialogResult.Yes))
                {
                    Cursor = Cursors.WaitCursor;
                    Produto pro = new Produto();
                    ProdutoDetalhe proDetalhe = new ProdutoDetalhe();
                    Preco preco = new Preco();
                    BancoDados.AbrirTrans();
                    pnlMsg.Visible = true;
                    pnlMsg.Location = new Point((this.Size.Width / 2) - (this.pnlMsg.Size.Width / 2), (this.Size.Height / 2) - (this.pnlMsg.Size.Height / 2));
                    pnlMsg.Refresh();
                    pbAtualiza.Visible = true;
                    pbAtualiza.Value = 0;
                    pbAtualiza.Step = 1;
                    pbAtualiza.Maximum = dgProdutos.RowCount;
                    lblMsg.Text = "Excluindo Produtos";

                    for (int i = 0; i < dgProdutos.Rows.Count; i++)
                    {
                        pbAtualiza.Value = i;

                        if (dgProdutos.Rows[i].Cells["Excluir"].Value != null)
                        {
                            if (pro.ExcluiPorCodigoBarras(dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString()).Equals(true))
                            {
                                if (proDetalhe.ExcluirPorCodigoBarras(dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString()).Equals(true))
                                {
                                    if (preco.ExcluirPorCodigoBarras(dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString()).Equals(true))
                                    {
                                        if (pro.InsereLogProdutos("EXCLUIR", dgProdutos.Rows[i].Cells["PROD_CODIGO"].Value.ToString(), dgProdutos.Rows[i].Cells["PROD_DESCR"].Value.ToString()).Equals(false))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("Tem certeza que deseja excluir esses produtos", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("Tem certeza que deseja excluir esses produtos", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Tem certeza que deseja excluir esses produtos", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                        }
                    }
                    BancoDados.FecharTrans();
                    MessageBox.Show("Exclusão realizada com sucesso", "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }

            }
            catch (Exception ex)
            {
                BancoDados.ErroTrans();
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                pnlMsg.Visible = false;
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEstManutencaoProdutos.AutoSize = false;
                this.tsbEstManutencaoProdutos.Image = Properties.Resources.estoque;
                this.tsbEstManutencaoProdutos.Size = new System.Drawing.Size(150, 20);
                this.tsbEstManutencaoProdutos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEstManutencaoProdutos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEstManutencaoProdutos);
                tsbEstManutencaoProdutos.Click += delegate
                {
                    var manutencaoProduto= Application.OpenForms.OfType<frmEstManutencaoProdutos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    manutencaoProduto.Focus();

                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajustes de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        bool Botoes.Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            if (!dgProdutos.Rows.Count.Equals(0))
            {
                dtProd.Clear();
                dgProdutos.DataSource = dtProd;
            }
            lblTotalRegistros.Visible = false;
            txtCodigoBarras.Text = "";
            txtDescricao.Text = "";

        }

        public void Sair()
        {
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEstManutencaoProdutos);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEstManutencaoProdutos);
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmEstManutencaoProdutos_Shown(object sender, EventArgs e)
        {
            //CARREGA DEPARTAMENTOS//
            Principal.dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count == 0)
            {
                MessageBox.Show("Necessário cadastrar pelo menos um Departameto,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return;
            }
            else
            {
                cmbDepartamento.DataSource = Principal.dtPesq;
                cmbDepartamento.DisplayMember = "DEP_DESCR";
                cmbDepartamento.ValueMember = "DEP_CODIGO";
                cmbDepartamento.SelectedIndex = -1;
            }
        }

        private void cmbDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                ckbCodBarras.Focus();
        }

        private void ckbCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }
    }
}
