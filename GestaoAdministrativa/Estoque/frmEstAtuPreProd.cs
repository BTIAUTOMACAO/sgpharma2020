﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAtuPreProd : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbAtualiza = new ToolStripButton("At. Precos ABCFarma");
        private ToolStripSeparator tssAtualiza = new ToolStripSeparator();
        private string versaoABC;
        private int contador;
        private DataTable dtProdNovos = new DataTable();
        private DataTable dtProdAbc = new DataTable();
        private int id;

        #endregion

        public frmEstAtuPreProd(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmEstAtuPreProd_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;

                dgNovos.Font = new System.Drawing.Font("Arial", 9.75F, FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgNovos.ForeColor = System.Drawing.Color.Black;
                dgNovos.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgNovos.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgProdABC.Font = new System.Drawing.Font("Arial", 9.75F, FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgProdABC.ForeColor = System.Drawing.Color.Black;
                dgProdABC.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgProdABC.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAtualiza.AutoSize = false;
                this.tsbAtualiza.Image = Properties.Resources.estoque;
                this.tsbAtualiza.Size = new System.Drawing.Size(160, 20);
                this.tsbAtualiza.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAtualiza);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAtualiza);
                tsbAtualiza.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstAtuPreProd>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtProdAbc.Clear();
                dtProdNovos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAtualiza);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAtualiza);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstAtuPreProd_Shown(object sender, EventArgs e)
        {
            try
            {
                versaoABC = Funcoes.LeParametro(9, "57", false);
                lblUltAtualizacao.Text = Funcoes.MesExtenso(versaoABC.Substring(4, 2)) + "/" + versaoABC.Substring(0, 4);

                Principal.dtBusca = BancoDados.selecionarRegistros("SELECT TAB_CODIGO, TAB_DESCR FROM TAB_PRECOS WHERE EMP_CODIGO = " + Principal.empAtual
                    + " AND TAB_DESABILITADO = 'N' ORDER BY TAB_DESCR");
                if (Principal.dtBusca.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Tabela de Preços,\npara poder atualizar os preços ABCFarma.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbTabPreco.DataSource = Principal.dtBusca;
                    cmbTabPreco.DisplayMember = "TAB_DESCR";
                    cmbTabPreco.ValueMember = "TAB_CODIGO";
                    cmbTabPreco.SelectedIndex = -1;
                }

                txtTabPrecos.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtTabPrecos_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbTabPreco, txtTabPrecos);
        }

        private void txtTabPrecos_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbTabPreco.Focus();
        }

        private void cmbTabPreco_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtTabPrecos.Text = Convert.ToString(cmbTabPreco.SelectedValue);
        }

        private async void btnArquivo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtTabPrecos.Text))
            {
                btnArquivo.Enabled = false;
                btnAtuPrecos.Enabled = false;
                btnProdNovos.Enabled = false;
                btnProdABC.Enabled = false;
                await VerificaAtualizacao();
            }
            else
            {
                MessageBox.Show("Selecione uma tabela de preços.", "Consitência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTabPrecos.Focus();
            }
        }

        public async Task<List<EstabelecimentoFilial>> IdentificaNovaVersao()
        {
            List<EstabelecimentoFilial> allItems = new List<Negocio.EstabelecimentoFilial>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaDadosDoEstabelecimentos?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<TabelaAbcFarma>> BuscaProdutos()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("TabelaAbcFarma/BuscaProdutosAbcFarma");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<TabelaAbcFarma> allItems = JsonConvert.DeserializeObject<List<TabelaAbcFarma>>(responseBody);

                return allItems;
            }
        }

        public async Task<bool> VerificaAtualizacao()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Atu. Preços ABCFarma";

                List<EstabelecimentoFilial> dados = await IdentificaNovaVersao();
                if (dados.Count > 0)
                {
                    if (Convert.ToInt32(Funcoes.LeParametro(9, "57", false)) <= Convert.ToInt32(dados[0].VersaoAbcFarma))
                    {
                        versaoABC = dados[0].VersaoAbcFarma;

                        if (Convert.ToInt32(Funcoes.LeParametro(9, "57", false)) < Convert.ToInt32(dados[0].VersaoAbcFarma))
                        {
                            Principal.mensagem = "Existe uma nova atualização disponível!";
                        }
                        else
                        {
                            Principal.mensagem = "A versão disponível e a mesma que está no sistema!";
                        }
                        btnArquivo.Enabled = true;
                        btnAtuPrecos.Enabled = true;
                        btnProdNovos.Enabled = true;
                        btnProdABC.Enabled = true;
                        btnProdABC.Focus();
                    }
                    else
                    {
                        Principal.mensagem = "Não há nova versão disponível!";
                        btnArquivo.Enabled = true;
                    }

                    Funcoes.Avisa();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                btnArquivo.Enabled = true;
                btnAtuPrecos.Enabled = false;
                btnProdNovos.Enabled = false;
                btnArquivo.Focus();
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnAtuPrecos_Click(object sender, EventArgs e)
        {
            gbProdABC.Visible = false;
            gbProdNovos.Visible = false;
            if (dtProdAbc.Rows.Count != 0)
            {
                dgProdABC.DataSource = dtProdAbc;
                gbProdABC.Visible = true;
            }
            else
            {
                MessageBox.Show("Não foi encontrado nenhum produto com preço diferente da Tabela ABCFarma", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public async Task<bool> AtualizaAbc()
        {
            try
            {
                bool inserir = false;
                Cursor = Cursors.WaitCursor;
                
                BancoDados.ExecuteNoQuery("DELETE FROM PRODUTOS_ABCFARMA", null);

                //BUSCA PRODUTOS NOVOS//
                if (MessageBox.Show("Deseja cadastrar produtos não existentes no cadastro atual?", "Atualiza Preços ABCFarma", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    inserir = true;
                    TProdutosNovos();
                }
                else
                    inserir = false;

                Application.DoEvents();
                tslRegistros.Text = "Atualizando informações dos Produtos ABCFarma. Aguarde...";
                pbAtualiza.Visible = true;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;

                List<TabelaAbcFarma> prodAbc = await BuscaProdutos();
                if (prodAbc.Count > 0)
                {
                    if (prodAbc[0].CodErro == "00")
                    {
                        Application.DoEvents();
                        pbAtualiza.Maximum = prodAbc.Count;

                        TProdutosABC();

                        //OBTEM TODOS OS CODIGOS DE BARRAS DA TABELA PRODUTOS_ABCFARMA//
                        var produtosAbc = new ProdutosAbcFarma();
                        Principal.dtBusca = produtosAbc.BuscaDados();

                        //OBTEM TODOS OS PRE_VALOR E CODIGO DE BARRA//
                        var produtos = new Produto();
                        Principal.dtRetorno = produtos.BuscaProdutosParaAbcFarma(Principal.estAtual, Principal.empAtual);

                        for (int i = 0; i < prodAbc.Count; i++)
                        {
                            produtosAbc.ProdCodigo = prodAbc[i].MedBarra;
                            produtosAbc.ProdDescricao = prodAbc[i].MedDescricao;
                            produtosAbc.MedPco18 = prodAbc[i].MedPco;
                            produtosAbc.MedPla18 = prodAbc[i].MedPla;
                            produtosAbc.MedFra18 = prodAbc[i].MedFra;
                            produtosAbc.CodAbcFarma = prodAbc[i].MedAbc;
                            produtosAbc.DtCadastro = DateTime.Now;
                            produtosAbc.OpCadastro = Principal.usuario;
                            
                            if(!String.IsNullOrEmpty(produtosAbc.BuscaCodigoAbcFarma(produtosAbc.ProdCodigo)))
                            {
                                produtosAbc.InserirDados(produtosAbc);
                            }

                            if (inserir.Equals(true))
                            {
                                DataRow[] linha_produtos = Principal.dtRetorno.Select("PROD_CODIGO = '" + prodAbc[i].MedBarra + "'");
                                if (linha_produtos.Length == 0)
                                {
                                    DataRow row = dtProdNovos.NewRow();
                                    row["PROD_CODIGO"] = prodAbc[i].MedBarra;
                                    row["PROD_DESCR"] = prodAbc[i].MedDescricao + " " + prodAbc[i].MedApresentacao;
                                    row["MED_PCO18"] = prodAbc[i].MedPco;
                                    row["MED_GENE"] = "";
                                    row["PROD_REGISTRO_MS"] = prodAbc[i].MedRegistroMS;
                                    row["NCM"] = prodAbc[i].MedNcm;
                                    row["PROD_LISTA"] = prodAbc[i].MedLista;
                                    row["PROD_CEST"] = prodAbc[i].MedCest;
                                    row["PROD_DCB"] = prodAbc[i].MedDCB;
                                    row["ESCOLHA"] = "False";

                                    dtProdNovos.Rows.Add(row);
                                }
                            }

                            DataRow[] linha_precos = Principal.dtRetorno.Select("PROD_CODIGO = '" + prodAbc[i].MedBarra + "'");
                            if (linha_precos.Length > 0)
                            {
                                if (Convert.ToDouble(linha_precos[0]["PRE_VALOR"]) != prodAbc[i].MedPco)
                                {
                                    DataRow row = dtProdAbc.NewRow();
                                    row["ESTACAO"] = Principal.nomeEstacao;
                                    row["NOME_FABRICANTE"] = prodAbc[i].LabNome;
                                    row["PROD_DESCR"] = prodAbc[i].MedDescricao + " " + prodAbc[i].MedApresentacao;
                                    row["PRECO_CONSUMO"] = prodAbc[i].MedPco;
                                    row["COD_BARRA"] = prodAbc[i].MedBarra;
                                    row["GENERICO"] = "";
                                    row["PROD_REGISTRO_MS"] = prodAbc[i].MedRegistroMS;
                                    row["PRECO_SIS"] = Convert.ToDouble(linha_precos[0]["PRE_VALOR"]);
                                    row["PROD_LISTA"] = prodAbc[i].MedLista;
                                    row["NCM"] = prodAbc[i].MedNcm;
                                    row["MED_GENE"] = prodAbc[i].MedGen;
                                    row["MED_CAS"] = prodAbc[i].MedCas;
                                    row["MED_DCB"] = prodAbc[i].MedDCB;
                                    row["MED_CEST"] = prodAbc[i].MedCest;
                                    row["ESCOLHA"] = "False";

                                    dtProdAbc.Rows.Add(row);
                                }
                            }
                            pbAtualiza.Value = contador;
                            Application.DoEvents();
                            contador = contador + 1;
                            lblRegistros.Text = "Registros: " + Convert.ToString(contador);
                        }

                        gbProdABC.Visible = false;
                        gbProdNovos.Visible = false;
                        if (dtProdAbc.Rows.Count != 0)
                        {
                            dgProdABC.DataSource = dtProdAbc;
                            gbProdABC.Visible = true;
                        }
                        else if (dtProdNovos.Rows.Count != 0)
                        {
                            dgNovos.DataSource = dtProdNovos;
                            gbProdNovos.Visible = true;
                        }
                        else
                        {
                            MessageBox.Show("Nenhum produto com Preço Diferente da Tabela AbcFarma e/ou/nNenhum produto novo para ser Cadastrado!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Erro: " + prodAbc[0].Mensagem, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                btnAtuPrecos.Enabled = true;
                btnProdNovos.Enabled = true;
                btnProdABC.Enabled = true;
                btnProdABC.Focus();
                pbAtualiza.Visible = false;
                pbAtualiza.Value = 0;
                pbAtualiza.Step = 1;
                pbAtualiza.Maximum = 0;
                contador = 0;
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
            }
        }

        public bool TProdutosNovos()
        {
            if (dtProdNovos.Columns.Count == 0)
            {
                dtProdNovos.Columns.Add("PROD_CODIGO", typeof(string));
                dtProdNovos.Columns.Add("PROD_DESCR", typeof(string));
                dtProdNovos.Columns.Add("MED_PCO18", typeof(double));
                dtProdNovos.Columns.Add("MED_GENE", typeof(string));
                dtProdNovos.Columns.Add("PROD_REGISTRO_MS", typeof(string));
                dtProdNovos.Columns.Add("NCM", typeof(string));
                dtProdNovos.Columns.Add("PROD_LISTA", typeof(string));
                dtProdNovos.Columns.Add("PROD_CEST", typeof(string));
                dtProdNovos.Columns.Add("PROD_DCB", typeof(string));
                dtProdNovos.Columns.Add("ESCOLHA", typeof(string));
            }
            else
            {
                dtProdNovos.Rows.Clear();
            }
            return true;
        }


        public bool TProdutosABC()
        {
            if (dtProdAbc.Columns.Count == 0)
            {
                dtProdAbc.Columns.Add("ESTACAO", typeof(string));
                dtProdAbc.Columns.Add("COD_BARRA", typeof(string));
                dtProdAbc.Columns.Add("NOME_FABRICANTE", typeof(string));
                dtProdAbc.Columns.Add("PROD_DESCR", typeof(string));
                dtProdAbc.Columns.Add("PRECO_CONSUMO", typeof(double));
                dtProdAbc.Columns.Add("PRECO_SIS", typeof(double));
                dtProdAbc.Columns.Add("GENERICO", typeof(string));
                dtProdAbc.Columns.Add("PROD_REGISTRO_MS", typeof(string));
                dtProdAbc.Columns.Add("NCM", typeof(string));
                dtProdAbc.Columns.Add("PROD_LISTA", typeof(string));
                dtProdAbc.Columns.Add("MED_GENE", typeof(string));
                dtProdAbc.Columns.Add("MED_CAS", typeof(string));
                dtProdAbc.Columns.Add("MED_DCB", typeof(string));
                dtProdAbc.Columns.Add("MED_CEST", typeof(string));
                dtProdAbc.Columns.Add("ESCOLHA", typeof(string));
            }
            else
            {
                dtProdAbc.Rows.Clear();
            }
            return true;
        }


        private void btnProdNovos_Click(object sender, EventArgs e)
        {
            gbProdABC.Visible = false;
            gbProdNovos.Visible = false;
            if (dtProdNovos.Rows.Count != 0)
            {
                dgNovos.DataSource = dtProdNovos;
                gbProdNovos.Visible = true;
            }
            else
            {
                MessageBox.Show("Não foi encontrado nenhum produto novo.", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnMarcar_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgNovos.RowCount; i++)
            {
                dtProdNovos.Rows[i].BeginEdit();
                dtProdNovos.Rows[i]["ESCOLHA"] = "True";
                dtProdNovos.Rows[i].EndEdit();
            }
            dgNovos.DataSource = dtProdNovos;
            for (int i = 0; i < dgNovos.RowCount; i++)
            {
                dgNovos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
            }
            Cursor = Cursors.Default;
        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgNovos.RowCount; i++)
            {
                dtProdNovos.Rows[i].BeginEdit();
                dtProdNovos.Rows[i]["ESCOLHA"] = "False";
                dtProdNovos.Rows[i].EndEdit();

            }
            dgNovos.DataSource = dtProdNovos;

            for (int i = 0; i < dgNovos.RowCount; i++)
            {
                dgNovos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
            }
            Cursor = Cursors.Default;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            if (!CadProdNovos())
            {
                MessageBox.Show("Erro no Cadastro dos Produtos. Contate o Suporte!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool GravarPrecos()
        {
            try
            {

                DataRow[] linhas = dtProdAbc.Select("ESCOLHA='True'");
                if (linhas.Length != 0)
                {
                    btnAtualizar.Enabled = false;
                    Application.DoEvents();
                    Cursor = Cursors.WaitCursor;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = linhas.Length;
                    tslRegistros.Text = "Atualizando valores dos produtos. Aguarde...";

                    var precos = new Preco();
                    var produtos = new Produto();
                    var produtosDetalhe = new ProdutoDetalhe();

                    precos.AtualizaPrecoAbc(Principal.empAtual, Principal.estAtual);

                    precos.ExcluiLogPrecoAbcFarma(Principal.empAtual, Principal.estAtual);

                    for (int i = 0; i < linhas.Length; i++)
                    {
                        precos.EmpCodigo = Principal.empAtual;
                        precos.EstCodigo = Principal.estAtual;
                        precos.PreValor = Convert.ToDouble(linhas[i]["PRECO_CONSUMO"]);
                        precos.ProdPMC = Convert.ToDouble(linhas[i]["PRECO_CONSUMO"]);
                        precos.ProdCodigo = linhas[i]["COD_BARRA"].ToString();
                        precos.DtAlteracao = DateTime.Now;
                        precos.OpAlteracao = Principal.usuario;
                        precos.AtualizadoABCFarma = "S";

                        if (!precos.AtualizaPrecoAbcFarmaPorProdCodigo(precos, Convert.ToDouble(linhas[i]["PRECO_CONSUMO"])))
                            return false;


                        produtos.ProdCodBarras = linhas[i]["COD_BARRA"].ToString();
                        produtos.ProdNCM = linhas[i]["NCM"].ToString();
                        produtos.ProdRegistroMS = linhas[i]["PROD_REGISTRO_MS"].ToString();
                        produtos.OpAlteracao = Principal.usuario;
                        produtos.DtAlteracao = DateTime.Now;

                        if (!produtos.AtualizaNcmRegistroMSAbcFarma(produtos))
                            return false;

                        produtosDetalhe.EmpCodigo = Principal.empAtual;
                        produtosDetalhe.EstCodigo = Principal.estAtual;
                        produtosDetalhe.ProdCodigo = linhas[i]["COD_BARRA"].ToString();
                        produtosDetalhe.ProdLista = linhas[i]["PROD_LISTA"].ToString() == "+" ? "P" : linhas[i]["PROD_LISTA"].ToString() == "-" ? "N" : "U";
                        produtosDetalhe.ProdCest = linhas[i]["MED_CEST"].ToString();
                        produtosDetalhe.ProdDCB = linhas[i]["MED_DCB"].ToString();
                        produtosDetalhe.OpAlteracao = Principal.usuario;
                        produtosDetalhe.DtAlteracao = DateTime.Now;

                        if (!produtosDetalhe.AtualizaProdListaAbcFarma(produtosDetalhe))
                            return false;
                        
                        DataRow[] linha_resultado = dtProdAbc.Select("COD_BARRA = '" + linhas[i]["COD_BARRA"] + "'");
                        linha_resultado[0].Delete();

                        linhas[i].Delete();

                        Application.DoEvents();
                        lblRegistros.Text = "Registros atualizados: " + i;
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    dgProdABC.DataSource = dtProdAbc;
                    return true;
                }
                else
                {
                    MessageBox.Show("Necessário selecionar ao menos um produto para realizadar a atualização de preço.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                btnAtualizar.Enabled = true;
            }
        }

        public bool CadProdNovos()
        {
            string prodCodigo = "";
            try
            {
                DataRow[] linhas = dtProdNovos.Select("ESCOLHA='True'");
                id = Funcoes.IdentificaVerificaID("PRODUTOS", "PROD_ID");
                if (linhas.Length != 0)
                {
                    if (MessageBox.Show("Confirma o cadastro dos novos produtos?", "Atualiza Preços (ABCFarma)", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                        == System.Windows.Forms.DialogResult.Yes)
                    {
                        btnCadastrar.Enabled = false;
                        Application.DoEvents();
                        Cursor = Cursors.WaitCursor;
                        tslRegistros.Text = "Cadastrando novos produtos. Aguarde...";
                        Principal.mdiPrincipal.pbMenu.Value = 0;
                        Principal.mdiPrincipal.pbMenu.Maximum = linhas.Length;
                        string prodDescr;

                        for (int i = 0; i < linhas.Length; i++)
                        {
                            prodCodigo = linhas[i]["PROD_CODIGO"].ToString();
                            if (linhas[i]["PROD_DESCR"].ToString().Length > 50)
                            {
                                prodDescr = linhas[i]["PROD_DESCR"].ToString().Substring(0, 50);
                            }
                            else
                            {
                                prodDescr = linhas[i]["PROD_DESCR"].ToString();
                            }

                            var produtoNovo = new Produto();
                            produtoNovo.ProdDescr = prodDescr;
                            produtoNovo.ProdUnidade = "UN";
                            produtoNovo.ProdTipo = "P";
                            produtoNovo.ProdCodBarras = linhas[i]["PROD_CODIGO"].ToString();
                            produtoNovo.ProdNCM = linhas[i]["NCM"].ToString();
                            produtoNovo.DtCadastro = DateTime.Now;
                            produtoNovo.OpCadastro = Principal.usuario;
                            produtoNovo.ProdID = id;
                            produtoNovo.ProdRegistroMS = linhas[i]["PROD_REGISTRO_MS"].ToString();

                            BancoDados.AbrirTrans();

                            if (!produtoNovo.InsereProdutosNovosAbcFarma(produtoNovo))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }

                            var detalheNovo = new ProdutoDetalhe();
                            detalheNovo.EstCodigo = Principal.estAtual;
                            detalheNovo.EmpCodigo = Principal.empAtual;
                            detalheNovo.ProdID = id;
                            detalheNovo.ProdCodigo = linhas[i]["PROD_CODIGO"].ToString();
                            detalheNovo.ProdLiberado = "A";
                            if (String.IsNullOrEmpty(linhas[i]["MED_GENE"].ToString()) || !linhas[i]["MED_GENE"].ToString().Equals("GEN"))
                            {
                                detalheNovo.DepCodigo = Convert.ToInt32(Funcoes.LeParametro(9, "41", true));
                            }
                            else if (linhas[i]["MED_GENE"].ToString().Equals("GEN"))
                            {
                                detalheNovo.DepCodigo = Convert.ToInt32(Funcoes.LeParametro(9, "40", true));
                            }
                            detalheNovo.ProdDTEstIni = DateTime.Now;
                            detalheNovo.ProdLista = linhas[i]["PROD_LISTA"].ToString() == "+" ? "P" : linhas[i]["PROD_LISTA"].ToString() == "-" ? "N" : "U";
                            detalheNovo.ProdEcf = "FF";
                            detalheNovo.ProdCest = linhas[i]["PROD_CEST"].ToString();
                            detalheNovo.ProdDCB = linhas[i]["PROD_DCB"].ToString();
                            detalheNovo.DtCadastro = DateTime.Now;
                            detalheNovo.OpCadastro = Principal.usuario;

                            if (!detalheNovo.InsereProdutosDetalheNovosAbcFarna(detalheNovo))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }

                            var precoNovo = new Preco();
                            precoNovo.EstCodigo = Principal.estAtual;
                            precoNovo.EmpCodigo = Principal.empAtual;
                            precoNovo.ProdCodigo = linhas[i]["PROD_CODIGO"].ToString();
                            precoNovo.ProdID = id;
                            precoNovo.PreValor = Convert.ToDouble(linhas[i]["MED_PCO18"]);
                            precoNovo.ProdPMC = Convert.ToDouble(linhas[i]["MED_PCO18"]);
                            precoNovo.TabCodigo = 1;
                            precoNovo.DtCadastro = DateTime.Now;
                            precoNovo.OpCadastro = Principal.usuario;

                            if (!precoNovo.InsereProdutosPrecosNovosAbcFarma(precoNovo))
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }

                            BancoDados.FecharTrans();

                            DataRow[] linha_resultado = dtProdNovos.Select("PROD_CODIGO = '" + linhas[i]["PROD_CODIGO"] + "'");
                            linha_resultado[0].Delete();

                            linhas[i].Delete();

                            id = id + 1;
                            Application.DoEvents();
                            lblRegistros.Text = "Registros cadastrados: " + i;
                            Principal.mdiPrincipal.pbMenu.Value = i;
                        }
                        dgNovos.DataSource = dtProdNovos;
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    MessageBox.Show("Necessário selecionar ao menos um produto para realizadar o cadastro.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + " PROD_CODIGO = " + prodCodigo, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                lblRegistros.Text = "";
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                btnCadastrar.Enabled = true;
            }
        }

        private void btnMarcarAbc_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgProdABC.RowCount; i++)
            {
                dtProdAbc.Rows[i].BeginEdit();
                dtProdAbc.Rows[i]["ESCOLHA"] = "True";
                dtProdAbc.Rows[i].EndEdit();
            }
            dgProdABC.DataSource = dtProdAbc;
            for (int i = 0; i < dgProdABC.RowCount; i++)
            {
                dgProdABC.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
            }
            Cursor = Cursors.Default;
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            if (GravarPrecos())
            {
                Funcoes.GravaParametro(9, "57", versaoABC);

                versaoABC = Funcoes.LeParametro(9, "57", false);
                lblUltAtualizacao.Text = Funcoes.MesExtenso(versaoABC.Substring(4, 2)) + "/" + versaoABC.Substring(0, 4);
            }
            else
            {
                MessageBox.Show("Erro na Atualização. Contate o Suporte!", "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnDesmarcarAbc_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dgProdABC.RowCount; i++)
            {
                dtProdAbc.Rows[i].BeginEdit();
                dtProdAbc.Rows[i]["ESCOLHA"] = "False";
                dtProdAbc.Rows[i].EndEdit();
            }
            dgProdABC.DataSource = dtProdAbc;

            for (int i = 0; i < dgProdABC.RowCount; i++)
            {
                dgProdABC.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
            }
            Cursor = Cursors.Default;
        }

        private async void btnProdABC_Click(object sender, EventArgs e)
        {
            gbProdABC.Visible = false;
            gbProdNovos.Visible = false;
            btnProdABC.Enabled = false;
            btnProdNovos.Enabled = false;
            btnAtuPrecos.Enabled = false;
            btnArquivo.Enabled = false;

            await AtualizaAbc();

            btnProdABC.Enabled = true;
            btnProdNovos.Enabled = true;
            btnAtuPrecos.Enabled = true;
            btnArquivo.Enabled = true;

        }

        public void Ultimo()
        {
        }

        public void Proximo()
        {
        }

        public void Primeiro()
        {
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        public void Anterior()
        {
        }

        public void Limpar()
        {
            versaoABC = Funcoes.LeParametro(9, "57", false);
            lblUltAtualizacao.Text = Funcoes.MesExtenso(versaoABC.Substring(4, 2)) + "/" + versaoABC.Substring(0, 4);

            btnArquivo.Enabled = true;
            btnAtuPrecos.Enabled = false;
            btnProdABC.Enabled = false;
            btnProdNovos.Enabled = false;
            txtTabPrecos.Text = "";
            cmbTabPreco.SelectedIndex = -1;
            pbAtualiza.Visible = false;
            gbProdABC.Visible = false;
            gbProdNovos.Visible = false;
            tslRegistros.Text = "";
            lblRegistros.Text = "";
            btnArquivo.Focus();
        }

        public bool Incluir()
        {
            return true;
        }

        public bool Excluir()
        {
            return true;
        }

        public bool Atualiza()
        {
            return true;
        }

        public void ImprimirRelatorio()
        {
        }

        private void dgNovos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgNovos.RowCount > 0)
                {
                    for (int i = 0; i < dgNovos.RowCount; i++)
                    {
                        if (Convert.ToString(dgNovos.Rows[i].Cells[4].Value).Equals("True"))
                        {
                            dgNovos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                        }
                        else if (Convert.ToString(dgNovos.Rows[i].Cells[4].Value).Equals("False"))
                        {
                            dgNovos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgProdABC_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgProdABC.RowCount > 0)
                {
                    for (int i = 0; i < dgProdABC.RowCount; i++)
                    {
                        if (Convert.ToString(dgProdABC.Rows[i].Cells[8].Value).Equals("True"))
                        {
                            dgProdABC.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                        }
                        else if (Convert.ToString(dgProdABC.Rows[i].Cells[8].Value).Equals("False"))
                        {
                            dgProdABC.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Atualiza Preços (ABCFarma)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstAtuPreProd_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtTabPrecos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                    if (gbProdNovos.Visible)
                    {
                        btnMarcar.PerformClick();
                    }
                    else
                        btnMarcarAbc.PerformClick();
                    break;
                case Keys.F8:
                    if (gbProdNovos.Visible)
                    {
                        btnDesmarcar.PerformClick();
                    }
                    else
                        btnDesmarcarAbc.PerformClick();
                    break;
                case Keys.F9:
                    btnArquivo.PerformClick();
                    break;
                case Keys.Escape:
                    Sair();
                    break;
                case Keys.F10:
                    btnProdABC.PerformClick();
                    break;
                case Keys.F11:
                    btnAtuPrecos.PerformClick();
                    break;
                case Keys.F12:
                    btnProdNovos.PerformClick();
                    break;
                case Keys.Insert:
                    if (gbProdNovos.Visible)
                    {
                        btnCadastrar.PerformClick();
                    }
                    else
                        btnAtuPrecos.PerformClick();
                    break;
            }
        }

        private void cmbTabPreco_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnProdABC_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnAtuPrecos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnProdNovos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void btnMarcar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnDesmarcar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnCadastrar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnArquivo_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnArquivo, "F9 - Verificar Atualização");
        }

        private void btnProdABC_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnProdABC, "F10 - Atualizar Tabela de Preços");
        }

        private void btnAtuPrecos_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAtuPrecos, "F11 - Visualizar Produtos ABCFARMA");
        }

        private void btnProdNovos_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnProdNovos, "F12 - Visualizar Produtos Novos");
        }

        private void btnMarcar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnMarcar, "F7 - Marcar Todos");
        }

        private void btnDesmarcar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnDesmarcar, "F8 - Desmarcar Todos");
        }

        private void btnCadastrar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCadastrar, "INSERT - Cadastrar Produtos Novos");
        }

        private void btnMarcarAbc_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnMarcarAbc_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnMarcarAbc, "F7 - Marcar Todos");
        }

        private void btnDesmarcarAbc_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void btnDesmarcarAbc_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnDesmarcarAbc, "F8 - Desmarcar Todos");
        }


        private void btnAtualizar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnAtualizar, "INSERT - Atualizar Preços");
        }

        private void btnAtualizar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }
    }
}
