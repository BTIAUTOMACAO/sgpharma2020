﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstManutencaoEstoque : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbAtualizaDCB = new ToolStripButton("Manut. Estoque");
        private ToolStripSeparator tssAtualizaDCB = new ToolStripSeparator();
        #endregion

        public frmEstManutencaoEstoque(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstManutencaoEstoque_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
                rdbTodos.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAtualizaDCB.AutoSize = false;
                this.tsbAtualizaDCB.Image = Properties.Resources.estoque;
                this.tsbAtualizaDCB.Size = new System.Drawing.Size(140, 20);
                this.tsbAtualizaDCB.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAtualizaDCB);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAtualizaDCB);
                tsbAtualizaDCB.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstManutencaoEstoque>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAtualizaDCB);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAtualizaDCB);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void frmEstManutencaoEstoque_Shown(object sender, EventArgs e)
        {
            //CARREGA DEPARTAMENTOS//
            Principal.dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count == 0)
            {
                MessageBox.Show("Necessário cadastrar pelo menos um Departameto,\npara zerar o estoque.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return;
            }
            else
            {
                cmbDepartamento.DataSource = Principal.dtPesq;
                cmbDepartamento.DisplayMember = "DEP_DESCR";
                cmbDepartamento.ValueMember = "DEP_CODIGO";
                cmbDepartamento.SelectedIndex = -1;
            }
        }

        private void rdbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if(rdbTodos.Checked)
            {
                cmbDepartamento.SelectedIndex = -1;
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if(rdbTodos.Checked)
                {
                    if(MessageBox.Show("Deseja ZERAR TODO O ESTOQUE?","Manutenção de Estoque",MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        rdbTodos.Checked = false;
                        cmbDepartamento.Focus();
                        return;
                    }
                }

                Cursor = Cursors.WaitCursor;
                var atualizacao = new ProdutoDetalhe();

                if (atualizacao.AtualizaEstoquePorDepartamentoOuNao(Principal.empAtual, Principal.estAtual, rdbTodos.Checked, Convert.ToInt32(cmbDepartamento.SelectedValue)))
                {
                    MessageBox.Show("Atualização realizada com Sucesso!", "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    string sql = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                       + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO)"
                       + " VALUES ('PROD_ESTATUAL','0','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PRODUTOS_DETALHE', 'PROD_ESTATUAL',0,0," + Principal.estAtual
                       + "," + Principal.empAtual + ")";
                    BancoDados.ExecuteNoQuery(sql, null);
                }
                
                cmbDepartamento.SelectedIndex = -1;
                rdbTodos.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbDepartamento.SelectedIndex != -1)
            {
                rdbTodos.Checked = false;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            cmbDepartamento.SelectedIndex = -1;
            rdbTodos.Checked = false;
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
