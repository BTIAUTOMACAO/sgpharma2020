﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstRelReposicaoProdutos : Form, Botoes
    {
        private string filtroProd;
        private ToolStripButton tsbRelatorio = new ToolStripButton("Repos. de Produtos");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();
        private int indice;
        private DataTable dtProdutos;

        public frmEstRelReposicaoProdutos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstRelReposicaoProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                groupBox2.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.estoque;
                this.tsbRelatorio.Size = new System.Drawing.Size(140, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRelReposicaoProdutos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }
                
                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }
                
                Cursor = Cursors.WaitCursor;
                dgReposicao.Rows.Clear();

                DataTable dtRetorno = new DataTable();
                if (rdbVendidos.Checked)
                {
                    var buscaReposicao = new Produto();
                    dtRetorno = buscaReposicao.BuscaProdutosParaReposicao(dtInicial.Text, dtFinal.Text, "00:00:00", "23:59:59", filtroProd, Principal.empAtual, Principal.estAtual);
                }
                else if (rdbFalta.Checked)
                {
                    var buscaFaltas = new LancamentoFaltas();
                    dtRetorno = buscaFaltas.BuscaProdutosParaReposicaoFalta(dtInicial.Text, dtFinal.Text, "00:00:00", "23:59:59", Principal.empAtual, Principal.estAtual,'P');
                }else{
                    var buscaFaltas = new LancamentoFaltas();
                    dtRetorno = buscaFaltas.BuscaProdutosParaReposicaoFalta(dtInicial.Text, dtFinal.Text, "00:00:00", "23:59:59", Principal.empAtual, Principal.estAtual,'C');
                }

                dtProdutos = dtRetorno.Clone();
                dtProdutos.Columns.Add("QTDE_COMPRA");

                if (dtRetorno.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRetorno.Rows.Count; i++)
                    {
                        double sugestao = 0;
                        if (rdbVendidos.Checked)
                        {
                            double estMin = Convert.ToDouble(dtRetorno.Rows[i]["PROD_ESTMIN"]) == 0 ? 1 : Convert.ToDouble(dtRetorno.Rows[i]["PROD_ESTMIN"]);

                            if (Convert.ToDouble(dtRetorno.Rows[i]["PROD_ESTATUAL"]) <= Convert.ToDouble(dtRetorno.Rows[i]["QTDE_VENDIDA"]))
                            {
                                sugestao = Convert.ToDouble(dtRetorno.Rows[i]["QTDE_VENDIDA"]);
                            }
                            else if ((Convert.ToDouble(dtRetorno.Rows[i]["PROD_ESTATUAL"]) - Convert.ToDouble(dtRetorno.Rows[i]["QTDE_VENDIDA"])) < estMin)
                            {
                                sugestao = Convert.ToDouble(dtRetorno.Rows[i]["QTDE_VENDIDA"]);

                                if (sugestao < estMin)
                                {
                                    sugestao = estMin;
                                }
                            }
                        }
                        else
                            sugestao = Convert.ToDouble(dtRetorno.Rows[i]["QTDE"]);

                        dgReposicao.Rows.Insert(dgReposicao.RowCount, new Object[] { false, dtRetorno.Rows[i]["PROD_CODIGO"], dtRetorno.Rows[i]["PROD_DESCR"], dtRetorno.Rows[i]["PROD_UNIDADE"],
                            dtRetorno.Rows[i]["PROD_ESTATUAL"], dtRetorno.Rows[i]["QTDE_VENDIDA"], sugestao, dtRetorno.Rows[i]["FAB_DESCRICAO"],  dtRetorno.Rows[i]["PROD_CUSTO"],
                             dtRetorno.Rows[i]["PRE_VALOR"], dtRetorno.Rows[i]["ID"]
                        });

                    }

                    lblRegistros.Text = "Registros: " + dtRetorno.Rows.Count;
                }
                else
                {
                    lblRegistros.Text = "";
                    filtroProd = "";
                    dgReposicao.Rows.Clear();
                    MessageBox.Show("Nenhum registro encontrado!", "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        
        private void horaInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void horaFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnContinuar.PerformClick();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("A", "A", "A", "A");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                    else
                        btnIncluir.Focus();
                }
                else
                    txtDescricao.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            var dtLePrdutos = new DataTable();
            Cursor = Cursors.WaitCursor;

            var buscaProduto = new Produto();
            dtLePrdutos = buscaProduto.BuscaDescricao(txtCodBarras.Text);
            if (dtLePrdutos.Rows.Count != 0)
            {
                txtDescricao.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
            }
            else
            {
                Cursor = Cursors.Default;
                MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescricao.Text = "Produto não cadastrado";
                txtCodBarras.Focus();
                return false;
            }

            Cursor = Cursors.Default;
            return true;
        }

        private void txtDescricao_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescricao.Text.Trim() != "")
                {
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescricao.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescricao.Text = "";
                            txtDescricao.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescricao.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescricao.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescricao.Text = "";
                                txtDescricao.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnIncluir.Focus();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCodBarras.Text))
            {
                if (ValidaProduto())
                {
                    var buscaReposicao = new Produto();

                    DataTable dtRetorno = buscaReposicao.BuscaProdutosParaReposicao(dtInicial.Text, dtFinal.Text, "00:00:00", "23:59:59", filtroProd, Principal.empAtual, Principal.estAtual, txtCodBarras.Text);
                    if (dtRetorno.Rows.Count > 0)
                    {
                        dgReposicao.Rows.Insert(dgReposicao.RowCount, new Object[] { false, dtRetorno.Rows[0]["PROD_CODIGO"], dtRetorno.Rows[0]["PROD_DESCR"], dtRetorno.Rows[0]["PROD_UNIDADE"], dtRetorno.Rows[0]["PROD_ESTATUAL"]
                        ,dtRetorno.Rows[0]["QTDE_VENDIDA"], 0, dtRetorno.Rows[0]["FAB_DESCRICAO"] , dtRetorno.Rows[0]["PROD_CUSTO"] , dtRetorno.Rows[0]["PRE_VALOR"] , dtRetorno.Rows[0]["ID"]});
                        txtCodBarras.Text = "";
                        txtDescricao.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado!", "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Produto já esta na lista. Inclusão não permitida!", "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodBarras.Text = "";
                    txtDescricao.Text = "";
                    txtCodBarras.Focus();
                }
            }
            else
            {
                MessageBox.Show("Necessário buscar um produto!", "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodBarras.Focus();
            }
        }

        public bool ValidaProduto()
        {
            for (int i = 0; i < dgReposicao.Rows.Count; i++)
            {
                if (dgReposicao.Rows[i].Cells[1].Value.ToString() == txtCodBarras.Text)
                {
                    return false;
                }
            }

            return true;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            txtCodBarras.Text = "";
            txtDescricao.Text = "";
            dgReposicao.Rows.Clear();
            rdbVendidos.Checked = true;
            indice = 0;
            lblRegistros.Text = "";
            rdbVendidos.Focus();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void frmEstRelReposicaoProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbVendidos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbFalta_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void horaInicial_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void horaFinal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnContinuar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnIncluir_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgReposicao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnSimulacao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F9:
                    MarcarTodos();
                    break;
                case Keys.F10:
                    DesmarcarTodos();
                    break;
                case Keys.F11:
                    MarcarDesmarcar();
                    break;
                case Keys.F1:
                    btnContinuar.PerformClick();
                    break;
                case Keys.F2:
                    btnIncluir.PerformClick();
                    break;
                case Keys.F3:
                    btnSimulacao.PerformClick();
                    break;
            }
        }

        public void MarcarTodos()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgReposicao.RowCount != 0)
                {
                    for (int i = 0; i < dgReposicao.RowCount; i++)
                    {
                        dgReposicao.Rows[i].Cells[0].Value = "true";
                    }

                    dgReposicao.Refresh();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para marca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void DesmarcarTodos()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgReposicao.RowCount != 0)
                {
                    for (int i = 0; i < dgReposicao.RowCount; i++)
                    {
                        dgReposicao.Rows[i].Cells[0].Value = "false";
                    }

                    dgReposicao.Refresh();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para desmarca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void MarcarDesmarcar()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgReposicao.RowCount != 0)
                {
                    if (dgReposicao.Rows[indice].Cells[0].Value.ToString() == "true")
                    {
                        dgReposicao.Rows[indice].Cells[0].Value = "false";
                    }
                    else
                    {
                        dgReposicao.Rows[indice].Cells[0].Value = "true";
                    }

                    dgReposicao.Refresh();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para marca-los/desmarca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgReposicao_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private async void btnSimulacao_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgReposicao.RowCount > 0)
                {
                    int valorCompra = 0;
                    dtProdutos.Clear();
                    for (var i = 0; i < dgReposicao.RowCount; i++)
                    {
                        if (!dgReposicao.Rows[i].Cells["MARCADO"].Value.Equals(false))
                        {
                            dtProdutos.Rows.Add();
                            int l = dtProdutos.Rows.Count - 1;
                            dtProdutos.Rows[l]["PROD_CODIGO"] = dgReposicao.Rows[i].Cells["PROD_CODIGO"].Value;
                            dtProdutos.Rows[l]["PROD_DESCR"] = dgReposicao.Rows[i].Cells["PROD_DESCR"].Value;
                            dtProdutos.Rows[l]["PROD_UNIDADE"] = dgReposicao.Rows[i].Cells["PROD_UNIDADE"].Value;
                            dtProdutos.Rows[l]["PROD_ESTATUAL"] = dgReposicao.Rows[i].Cells["PROD_ESTATUAL"].Value;
                            dtProdutos.Rows[l]["QTDE_VENDIDA"] = dgReposicao.Rows[i].Cells["QTDE_VENDIDA"].Value;
                            dtProdutos.Rows[l]["QTDE_COMPRA"] = Convert.ToInt32(dgReposicao.Rows[i].Cells["COMPRA"].Value);
                            dtProdutos.Rows[l]["FAB_DESCRICAO"] = dgReposicao.Rows[i].Cells["FAB_DESCRICAO"].Value;
                            dtProdutos.Rows[l]["PROD_CUSTO"] = dgReposicao.Rows[i].Cells["PROD_CUSTO"].Value;
                            dtProdutos.Rows[l]["PRE_VALOR"] = dgReposicao.Rows[i].Cells["PRE_VALOR"].Value;
                            valorCompra = valorCompra + Convert.ToInt32(dgReposicao.Rows[i].Cells["COMPRA"].Value);
                        }
                    }
                    if (dtProdutos.Rows.Count.Equals(0))
                    {
                        MessageBox.Show("Necessário selecionar pelo menos um registro", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgReposicao.Focus();
                    }
                    else
                    {
                        if (rdbFalta.Checked)
                        {
                            for (var i = 0; i < dgReposicao.RowCount; i++)
                            {
                                if (!dgReposicao.Rows[i].Cells["MARCADO"].Value.Equals(false))
                                {
                                    var atualiza = new LancamentoFaltas();
                                    atualiza.AtualizaStatusDaFaltaPorID(dgReposicao.Rows[i].Cells["PROD_CODIGO"].Value.ToString(),
                                        Convert.ToInt32(dgReposicao.Rows[i].Cells["ID_FALTA"].Value),
                                        Principal.estAtual, Principal.empAtual, "C");
                                }

                                await AtualizaStatus(Convert.ToInt32(dgReposicao.Rows[i].Cells["ID_FALTA"].Value));
                            }
                        }

                        frmRelatorioReposicaoProdutos relProd = new frmRelatorioReposicaoProdutos(dtInicial.Value, dtFinal.Value, dtProdutos, valorCompra);
                        relProd.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para gerar Simulação de Compra", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Reposição de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> AtualizaStatus(int idLoja)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Falteiro/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&idLoja=" + idLoja
                        + "&status=C");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status.", "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void dgReposicao_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex >=0)
            {
                dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value = Funcoes.RemoveCaracter(dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value.ToString());

                if(contemNumeros(dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value.ToString()) && contemLetras(dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value.ToString()))
                {
                    MessageBox.Show("Campo só aceita número!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value = 0;
                }
                else if(contemLetras(dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value.ToString()))
                {
                    MessageBox.Show("Campo só aceita número!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgReposicao.Rows[e.RowIndex].Cells["COMPRA"].Value = 0;
                }
            }
        }

        public bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
                return true;
            else
                return false;
        }

        public bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }
    }
}
