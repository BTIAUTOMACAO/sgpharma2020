﻿namespace GestaoAdministrativa.Estoque
{
    partial class frmEstAtuPreProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstAtuPreProd));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblEstab = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnProdNovos = new System.Windows.Forms.Button();
            this.btnAtuPrecos = new System.Windows.Forms.Button();
            this.btnProdABC = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.btnArquivo = new System.Windows.Forms.Button();
            this.cmbTabPreco = new System.Windows.Forms.ComboBox();
            this.txtTabPrecos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUltAtualizacao = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbAtualiza = new System.Windows.Forms.ProgressBar();
            this.gbProdABC = new System.Windows.Forms.GroupBox();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnDesmarcarAbc = new System.Windows.Forms.Button();
            this.btnMarcarAbc = new System.Windows.Forms.Button();
            this.dgProdABC = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ESTACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME_FABRICANTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRE_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GENERICO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMERO_NCM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LISTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_GENE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_CEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_DCB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_CAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbProdNovos = new System.Windows.Forms.GroupBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.btnDesmarcar = new System.Windows.Forms.Button();
            this.btnMarcar = new System.Windows.Forms.Button();
            this.dgNovos = new System.Windows.Forms.DataGridView();
            this.ESCOLHA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.COD_BARRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_PCO18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_REGISTRO_MS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NCM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_LISTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CEST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DCB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbProdABC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdABC)).BeginInit();
            this.gbProdNovos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNovos)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblEstab);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.pbAtualiza);
            this.panel2.Controls.Add(this.gbProdNovos);
            this.panel2.Controls.Add(this.gbProdABC);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(6, 6);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 152;
            this.lblEstab.Tag = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnProdNovos);
            this.groupBox2.Controls.Add(this.btnAtuPrecos);
            this.groupBox2.Controls.Add(this.btnProdABC);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.lblRegistros);
            this.groupBox2.Controls.Add(this.btnArquivo);
            this.groupBox2.Controls.Add(this.cmbTabPreco);
            this.groupBox2.Controls.Add(this.txtTabPrecos);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblUltAtualizacao);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(947, 118);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Arquivo de Preços";
            // 
            // btnProdNovos
            // 
            this.btnProdNovos.Enabled = false;
            this.btnProdNovos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdNovos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProdNovos.Location = new System.Drawing.Point(731, 64);
            this.btnProdNovos.Name = "btnProdNovos";
            this.btnProdNovos.Size = new System.Drawing.Size(210, 45);
            this.btnProdNovos.TabIndex = 13;
            this.btnProdNovos.Text = "Visualizar Produtos Novos (F12)";
            this.btnProdNovos.UseVisualStyleBackColor = true;
            this.btnProdNovos.Click += new System.EventHandler(this.btnProdNovos_Click);
            this.btnProdNovos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnProdNovos_KeyDown);
            this.btnProdNovos.MouseHover += new System.EventHandler(this.btnProdNovos_MouseHover);
            // 
            // btnAtuPrecos
            // 
            this.btnAtuPrecos.Enabled = false;
            this.btnAtuPrecos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtuPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtuPrecos.Location = new System.Drawing.Point(515, 64);
            this.btnAtuPrecos.Name = "btnAtuPrecos";
            this.btnAtuPrecos.Size = new System.Drawing.Size(210, 45);
            this.btnAtuPrecos.TabIndex = 12;
            this.btnAtuPrecos.Text = "Visualizar Produtos ABC (F11)";
            this.btnAtuPrecos.UseVisualStyleBackColor = true;
            this.btnAtuPrecos.Click += new System.EventHandler(this.btnAtuPrecos_Click);
            this.btnAtuPrecos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAtuPrecos_KeyDown);
            this.btnAtuPrecos.MouseHover += new System.EventHandler(this.btnAtuPrecos_MouseHover);
            // 
            // btnProdABC
            // 
            this.btnProdABC.Enabled = false;
            this.btnProdABC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdABC.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProdABC.Location = new System.Drawing.Point(299, 64);
            this.btnProdABC.Name = "btnProdABC";
            this.btnProdABC.Size = new System.Drawing.Size(210, 45);
            this.btnProdABC.TabIndex = 11;
            this.btnProdABC.Text = "Atualizar Tabela de Preços (F10)";
            this.btnProdABC.UseVisualStyleBackColor = true;
            this.btnProdABC.Click += new System.EventHandler(this.btnProdABC_Click);
            this.btnProdABC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnProdABC_KeyDown);
            this.btnProdABC.MouseHover += new System.EventHandler(this.btnProdABC_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(6, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.ForeColor = System.Drawing.Color.Navy;
            this.lblRegistros.Location = new System.Drawing.Point(713, 42);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(0, 16);
            this.lblRegistros.TabIndex = 7;
            // 
            // btnArquivo
            // 
            this.btnArquivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnArquivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArquivo.ForeColor = System.Drawing.Color.Black;
            this.btnArquivo.Location = new System.Drawing.Point(83, 64);
            this.btnArquivo.Name = "btnArquivo";
            this.btnArquivo.Size = new System.Drawing.Size(210, 45);
            this.btnArquivo.TabIndex = 5;
            this.btnArquivo.Text = "Verificar Novo Arquivo de Preços (F9)";
            this.btnArquivo.UseVisualStyleBackColor = true;
            this.btnArquivo.Click += new System.EventHandler(this.btnArquivo_Click);
            this.btnArquivo.MouseHover += new System.EventHandler(this.btnArquivo_MouseHover);
            // 
            // cmbTabPreco
            // 
            this.cmbTabPreco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTabPreco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTabPreco.FormattingEnabled = true;
            this.cmbTabPreco.Location = new System.Drawing.Point(276, 34);
            this.cmbTabPreco.Name = "cmbTabPreco";
            this.cmbTabPreco.Size = new System.Drawing.Size(420, 24);
            this.cmbTabPreco.TabIndex = 4;
            this.cmbTabPreco.SelectedIndexChanged += new System.EventHandler(this.cmbTabPreco_SelectedIndexChanged);
            this.cmbTabPreco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTabPreco_KeyDown);
            // 
            // txtTabPrecos
            // 
            this.txtTabPrecos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTabPrecos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTabPrecos.Location = new System.Drawing.Point(209, 36);
            this.txtTabPrecos.Name = "txtTabPrecos";
            this.txtTabPrecos.Size = new System.Drawing.Size(61, 22);
            this.txtTabPrecos.TabIndex = 3;
            this.txtTabPrecos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTabPrecos_KeyDown);
            this.txtTabPrecos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTabPrecos_KeyPress);
            this.txtTabPrecos.Validated += new System.EventHandler(this.txtTabPrecos_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(80, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tabela de Preços:";
            // 
            // lblUltAtualizacao
            // 
            this.lblUltAtualizacao.AutoSize = true;
            this.lblUltAtualizacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblUltAtualizacao.Location = new System.Drawing.Point(289, 18);
            this.lblUltAtualizacao.Name = "lblUltAtualizacao";
            this.lblUltAtualizacao.Size = new System.Drawing.Size(88, 16);
            this.lblUltAtualizacao.TabIndex = 1;
            this.lblUltAtualizacao.Text = "Janeiro/2014";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(80, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mês/Ano da última Atualização: ";
            // 
            // pbAtualiza
            // 
            this.pbAtualiza.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAtualiza.Location = new System.Drawing.Point(6, 147);
            this.pbAtualiza.Name = "pbAtualiza";
            this.pbAtualiza.Size = new System.Drawing.Size(947, 41);
            this.pbAtualiza.TabIndex = 164;
            this.pbAtualiza.Visible = false;
            // 
            // gbProdABC
            // 
            this.gbProdABC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProdABC.Controls.Add(this.btnAtualizar);
            this.gbProdABC.Controls.Add(this.btnDesmarcarAbc);
            this.gbProdABC.Controls.Add(this.btnMarcarAbc);
            this.gbProdABC.Controls.Add(this.dgProdABC);
            this.gbProdABC.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProdABC.ForeColor = System.Drawing.Color.Black;
            this.gbProdABC.Location = new System.Drawing.Point(6, 147);
            this.gbProdABC.Name = "gbProdABC";
            this.gbProdABC.Size = new System.Drawing.Size(947, 333);
            this.gbProdABC.TabIndex = 155;
            this.gbProdABC.TabStop = false;
            this.gbProdABC.Text = "Produtos ABCFarma";
            this.gbProdABC.Visible = false;
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAtualizar.Location = new System.Drawing.Point(766, 270);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(175, 60);
            this.btnAtualizar.TabIndex = 162;
            this.btnAtualizar.Text = "Atualizar Preços (Insert)";
            this.btnAtualizar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            this.btnAtualizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnAtualizar_KeyDown);
            this.btnAtualizar.MouseHover += new System.EventHandler(this.btnAtualizar_MouseHover);
            // 
            // btnDesmarcarAbc
            // 
            this.btnDesmarcarAbc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesmarcarAbc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcarAbc.Image = ((System.Drawing.Image)(resources.GetObject("btnDesmarcarAbc.Image")));
            this.btnDesmarcarAbc.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDesmarcarAbc.Location = new System.Drawing.Point(168, 270);
            this.btnDesmarcarAbc.Name = "btnDesmarcarAbc";
            this.btnDesmarcarAbc.Size = new System.Drawing.Size(156, 60);
            this.btnDesmarcarAbc.TabIndex = 161;
            this.btnDesmarcarAbc.Text = "Desmarcar Todos (F8)";
            this.btnDesmarcarAbc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDesmarcarAbc.UseVisualStyleBackColor = true;
            this.btnDesmarcarAbc.Click += new System.EventHandler(this.btnDesmarcarAbc_Click);
            this.btnDesmarcarAbc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDesmarcarAbc_KeyDown);
            this.btnDesmarcarAbc.MouseHover += new System.EventHandler(this.btnDesmarcarAbc_MouseHover);
            // 
            // btnMarcarAbc
            // 
            this.btnMarcarAbc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarcarAbc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcarAbc.Image = ((System.Drawing.Image)(resources.GetObject("btnMarcarAbc.Image")));
            this.btnMarcarAbc.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMarcarAbc.Location = new System.Drawing.Point(6, 270);
            this.btnMarcarAbc.Name = "btnMarcarAbc";
            this.btnMarcarAbc.Size = new System.Drawing.Size(156, 60);
            this.btnMarcarAbc.TabIndex = 160;
            this.btnMarcarAbc.Text = "Marcar Todos (F7)";
            this.btnMarcarAbc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMarcarAbc.UseVisualStyleBackColor = true;
            this.btnMarcarAbc.Click += new System.EventHandler(this.btnMarcarAbc_Click);
            this.btnMarcarAbc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnMarcarAbc_KeyDown);
            this.btnMarcarAbc.MouseHover += new System.EventHandler(this.btnMarcarAbc_MouseHover);
            // 
            // dgProdABC
            // 
            this.dgProdABC.AllowUserToAddRows = false;
            this.dgProdABC.AllowUserToDeleteRows = false;
            this.dgProdABC.AllowUserToResizeColumns = false;
            this.dgProdABC.AllowUserToResizeRows = false;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdABC.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgProdABC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProdABC.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdABC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgProdABC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.ESTACAO,
            this.dataGridViewTextBoxColumn1,
            this.NOME_FABRICANTE,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.PRE_VALOR,
            this.GENERICO,
            this.dataGridViewTextBoxColumn4,
            this.NUMERO_NCM,
            this.LISTA,
            this.MED_GENE,
            this.MED_CEST,
            this.MED_DCB,
            this.MED_CAS});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdABC.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgProdABC.GridColor = System.Drawing.Color.Black;
            this.dgProdABC.Location = new System.Drawing.Point(6, 21);
            this.dgProdABC.Name = "dgProdABC";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdABC.RowHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgProdABC.RowHeadersVisible = false;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgProdABC.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgProdABC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProdABC.Size = new System.Drawing.Size(935, 246);
            this.dgProdABC.TabIndex = 0;
            this.dgProdABC.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProdABC_CellEndEdit);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "ESCOLHA";
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 30;
            // 
            // ESTACAO
            // 
            this.ESTACAO.DataPropertyName = "ESTACAO";
            this.ESTACAO.HeaderText = "Estacao";
            this.ESTACAO.MaxInputLength = 50;
            this.ESTACAO.Name = "ESTACAO";
            this.ESTACAO.ReadOnly = true;
            this.ESTACAO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESTACAO.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "COD_BARRA";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn1.HeaderText = "Códido de Barra";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 13;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // NOME_FABRICANTE
            // 
            this.NOME_FABRICANTE.DataPropertyName = "NOME_FABRICANTE";
            this.NOME_FABRICANTE.HeaderText = "Nome Fabricante";
            this.NOME_FABRICANTE.MaxInputLength = 30;
            this.NOME_FABRICANTE.Name = "NOME_FABRICANTE";
            this.NOME_FABRICANTE.ReadOnly = true;
            this.NOME_FABRICANTE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NOME_FABRICANTE.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "PROD_DESCR";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn2.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 350;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PRECO_CONSUMO";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N2";
            dataGridViewCellStyle24.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn3.HeaderText = "Preço ABCFarma";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 130;
            // 
            // PRE_VALOR
            // 
            this.PRE_VALOR.DataPropertyName = "PRECO_SIS";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Format = "N2";
            dataGridViewCellStyle25.NullValue = null;
            this.PRE_VALOR.DefaultCellStyle = dataGridViewCellStyle25;
            this.PRE_VALOR.HeaderText = "Pr. do Sistema";
            this.PRE_VALOR.MaxInputLength = 18;
            this.PRE_VALOR.Name = "PRE_VALOR";
            this.PRE_VALOR.ReadOnly = true;
            this.PRE_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PRE_VALOR.Width = 130;
            // 
            // GENERICO
            // 
            this.GENERICO.DataPropertyName = "GENERICO";
            this.GENERICO.HeaderText = "Genérico";
            this.GENERICO.MaxInputLength = 3;
            this.GENERICO.Name = "GENERICO";
            this.GENERICO.ReadOnly = true;
            this.GENERICO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PROD_REGISTRO_MS";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn4.HeaderText = "Reg. Min. da Saúde";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 130;
            // 
            // NUMERO_NCM
            // 
            this.NUMERO_NCM.DataPropertyName = "NCM";
            this.NUMERO_NCM.HeaderText = "NCM";
            this.NUMERO_NCM.Name = "NUMERO_NCM";
            this.NUMERO_NCM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NUMERO_NCM.Width = 90;
            // 
            // LISTA
            // 
            this.LISTA.DataPropertyName = "PROD_LISTA";
            this.LISTA.HeaderText = "Lista";
            this.LISTA.Name = "LISTA";
            this.LISTA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LISTA.Width = 60;
            // 
            // MED_GENE
            // 
            this.MED_GENE.DataPropertyName = "MED_GENE";
            this.MED_GENE.HeaderText = "Generíco";
            this.MED_GENE.Name = "MED_GENE";
            // 
            // MED_CEST
            // 
            this.MED_CEST.DataPropertyName = "MED_CEST";
            this.MED_CEST.HeaderText = "CEST";
            this.MED_CEST.Name = "MED_CEST";
            // 
            // MED_DCB
            // 
            this.MED_DCB.DataPropertyName = "MED_DCB";
            this.MED_DCB.HeaderText = "DCB";
            this.MED_DCB.Name = "MED_DCB";
            // 
            // MED_CAS
            // 
            this.MED_CAS.DataPropertyName = "MED_CAS";
            this.MED_CAS.HeaderText = "CAS";
            this.MED_CAS.Name = "MED_CAS";
            // 
            // gbProdNovos
            // 
            this.gbProdNovos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProdNovos.Controls.Add(this.btnCadastrar);
            this.gbProdNovos.Controls.Add(this.btnDesmarcar);
            this.gbProdNovos.Controls.Add(this.btnMarcar);
            this.gbProdNovos.Controls.Add(this.dgNovos);
            this.gbProdNovos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProdNovos.ForeColor = System.Drawing.Color.Black;
            this.gbProdNovos.Location = new System.Drawing.Point(6, 147);
            this.gbProdNovos.Name = "gbProdNovos";
            this.gbProdNovos.Size = new System.Drawing.Size(947, 333);
            this.gbProdNovos.TabIndex = 154;
            this.gbProdNovos.TabStop = false;
            this.gbProdNovos.Text = "Produtos Novos";
            this.gbProdNovos.Visible = false;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCadastrar.Image")));
            this.btnCadastrar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCadastrar.Location = new System.Drawing.Point(766, 270);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(175, 60);
            this.btnCadastrar.TabIndex = 159;
            this.btnCadastrar.Text = "Cadastrar Produtos (Insert)";
            this.btnCadastrar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            this.btnCadastrar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCadastrar_KeyDown);
            this.btnCadastrar.MouseHover += new System.EventHandler(this.btnCadastrar_MouseHover);
            // 
            // btnDesmarcar
            // 
            this.btnDesmarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesmarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnDesmarcar.Image")));
            this.btnDesmarcar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDesmarcar.Location = new System.Drawing.Point(168, 270);
            this.btnDesmarcar.Name = "btnDesmarcar";
            this.btnDesmarcar.Size = new System.Drawing.Size(156, 60);
            this.btnDesmarcar.TabIndex = 158;
            this.btnDesmarcar.Text = "Desmarcar Todos (F8)";
            this.btnDesmarcar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDesmarcar.UseVisualStyleBackColor = true;
            this.btnDesmarcar.Click += new System.EventHandler(this.btnDesmarcar_Click);
            this.btnDesmarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDesmarcar_KeyDown);
            this.btnDesmarcar.MouseHover += new System.EventHandler(this.btnDesmarcar_MouseHover);
            // 
            // btnMarcar
            // 
            this.btnMarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnMarcar.Image")));
            this.btnMarcar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMarcar.Location = new System.Drawing.Point(6, 270);
            this.btnMarcar.Name = "btnMarcar";
            this.btnMarcar.Size = new System.Drawing.Size(156, 60);
            this.btnMarcar.TabIndex = 157;
            this.btnMarcar.Text = "Marcar Todos (F7)";
            this.btnMarcar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMarcar.UseVisualStyleBackColor = true;
            this.btnMarcar.Click += new System.EventHandler(this.btnMarcar_Click);
            this.btnMarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnMarcar_KeyDown);
            this.btnMarcar.MouseHover += new System.EventHandler(this.btnMarcar_MouseHover);
            // 
            // dgNovos
            // 
            this.dgNovos.AllowUserToAddRows = false;
            this.dgNovos.AllowUserToDeleteRows = false;
            this.dgNovos.AllowUserToResizeColumns = false;
            this.dgNovos.AllowUserToResizeRows = false;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.dgNovos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgNovos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgNovos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgNovos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgNovos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESCOLHA,
            this.COD_BARRA,
            this.PROD_DESCR,
            this.MED_PCO18,
            this.PROD_REGISTRO_MS,
            this.NCM,
            this.PROD_LISTA,
            this.PROD_CEST,
            this.PROD_DCB});
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgNovos.DefaultCellStyle = dataGridViewCellStyle36;
            this.dgNovos.GridColor = System.Drawing.Color.Black;
            this.dgNovos.Location = new System.Drawing.Point(6, 21);
            this.dgNovos.Name = "dgNovos";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgNovos.RowHeadersDefaultCellStyle = dataGridViewCellStyle37;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            this.dgNovos.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.dgNovos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgNovos.Size = new System.Drawing.Size(935, 246);
            this.dgNovos.TabIndex = 0;
            this.dgNovos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgNovos_CellEndEdit);
            // 
            // ESCOLHA
            // 
            this.ESCOLHA.DataPropertyName = "ESCOLHA";
            this.ESCOLHA.Frozen = true;
            this.ESCOLHA.HeaderText = "";
            this.ESCOLHA.Name = "ESCOLHA";
            this.ESCOLHA.Width = 50;
            // 
            // COD_BARRA
            // 
            this.COD_BARRA.DataPropertyName = "PROD_CODIGO";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.COD_BARRA.DefaultCellStyle = dataGridViewCellStyle32;
            this.COD_BARRA.HeaderText = "Códido de Barra";
            this.COD_BARRA.MaxInputLength = 13;
            this.COD_BARRA.Name = "COD_BARRA";
            this.COD_BARRA.ReadOnly = true;
            this.COD_BARRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COD_BARRA.Width = 150;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PROD_DESCR.DefaultCellStyle = dataGridViewCellStyle33;
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.MaxInputLength = 50;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESCR.Width = 450;
            // 
            // MED_PCO18
            // 
            this.MED_PCO18.DataPropertyName = "MED_PCO18";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.Format = "N2";
            dataGridViewCellStyle34.NullValue = null;
            this.MED_PCO18.DefaultCellStyle = dataGridViewCellStyle34;
            this.MED_PCO18.HeaderText = "Preço ABCFarma";
            this.MED_PCO18.MaxInputLength = 18;
            this.MED_PCO18.Name = "MED_PCO18";
            this.MED_PCO18.ReadOnly = true;
            this.MED_PCO18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MED_PCO18.Width = 150;
            // 
            // PROD_REGISTRO_MS
            // 
            this.PROD_REGISTRO_MS.DataPropertyName = "PROD_REGISTRO_MS";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.PROD_REGISTRO_MS.DefaultCellStyle = dataGridViewCellStyle35;
            this.PROD_REGISTRO_MS.HeaderText = "Reg. Min. da Saúde";
            this.PROD_REGISTRO_MS.MaxInputLength = 20;
            this.PROD_REGISTRO_MS.Name = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.ReadOnly = true;
            this.PROD_REGISTRO_MS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_REGISTRO_MS.Width = 180;
            // 
            // NCM
            // 
            this.NCM.DataPropertyName = "NCM";
            this.NCM.HeaderText = "NCM";
            this.NCM.Name = "NCM";
            this.NCM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_LISTA
            // 
            this.PROD_LISTA.DataPropertyName = "PROD_LISTA";
            this.PROD_LISTA.HeaderText = "Lista";
            this.PROD_LISTA.Name = "PROD_LISTA";
            this.PROD_LISTA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PROD_CEST
            // 
            this.PROD_CEST.DataPropertyName = "PROD_CEST";
            this.PROD_CEST.HeaderText = "CEST";
            this.PROD_CEST.Name = "PROD_CEST";
            // 
            // PROD_DCB
            // 
            this.PROD_DCB.DataPropertyName = "PROD_DCB";
            this.PROD_DCB.HeaderText = "DCB";
            this.PROD_DCB.Name = "PROD_DCB";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(284, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Atualiza Preços Produtos (PMC) Anvisa";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // frmEstAtuPreProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstAtuPreProd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmEstAtuPreProd";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEstAtuPreProd_Load);
            this.Shown += new System.EventHandler(this.frmEstAtuPreProd_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEstAtuPreProd_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbProdABC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProdABC)).EndInit();
            this.gbProdNovos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNovos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.TextBox txtTabPrecos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUltAtualizacao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTabPreco;
        private System.Windows.Forms.Button btnArquivo;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.GroupBox gbProdABC;
        private System.Windows.Forms.DataGridView dgProdABC;
        private System.Windows.Forms.GroupBox gbProdNovos;
        private System.Windows.Forms.Button btnDesmarcar;
        private System.Windows.Forms.Button btnMarcar;
        private System.Windows.Forms.DataGridView dgNovos;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Button btnDesmarcarAbc;
        private System.Windows.Forms.Button btnMarcarAbc;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnProdABC;
        private System.Windows.Forms.Button btnProdNovos;
        private System.Windows.Forms.Button btnAtuPrecos;
        private System.Windows.Forms.ProgressBar pbAtualiza;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESTACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME_FABRICANTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRE_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn GENERICO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMERO_NCM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LISTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_GENE;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_CEST;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_DCB;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_CAS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ESCOLHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_BARRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_PCO18;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REGISTRO_MS;
        private System.Windows.Forms.DataGridViewTextBoxColumn NCM;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_LISTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CEST;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DCB;
    }
}