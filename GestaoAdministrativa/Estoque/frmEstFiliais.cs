﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstFiliais : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbFiliais = new ToolStripButton("Prod. Filiais");
        private ToolStripSeparator tssFilais = new ToolStripSeparator();
        string enderecoWebService;
        #endregion

        public frmEstFiliais(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmEstFiliais_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                enderecoWebService = Funcoes.LeParametro(9, "54", true);
                lblRegistros.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbFiliais.AutoSize = false;
                this.tsbFiliais.Image = Properties.Resources.estoque;
                this.tsbFiliais.Size = new System.Drawing.Size(100, 20);
                this.tsbFiliais.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbFiliais);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssFilais);
                tsbFiliais.Click += delegate
                {
                    var estFilais = Application.OpenForms.OfType<frmEstFiliais>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estFilais.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbFiliais);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssFilais);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio() { }

        public void Limpar()
        {
            txtCodigo.Text = "";
            dgProdutos.Rows.Clear();
            pbAtualiza.Value = 0;
            txtCodigo.Focus();
        }

        public bool Excluir() { return true; }

        public bool Incluir() { return true; }

        public bool Atualiza() { return true; }

        public void Primeiro() { }

        public void Ultimo() { }

        public void Proximo() { }

        public void Anterior() { }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        
        private void btnGravaProd_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnGravaProd, "Cadastra Estoque");
        }

        private void btnExcluiProd_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnExcluiProd, "Exclui Estoque");
        }
        
        private async void btnGravaProd_Click(object sender, EventArgs e)
        {
            try
            {
                string codLoja = Funcoes.LeParametro(9, "52", true);
                string grupo = Funcoes.LeParametro(9, "53", true);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/IdentificaSePossuiProduto?grupoId=" + grupo
                        + "&codEstab=" + codLoja);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (responseBody.Equals("false"))
                        {
                            var produto = new Produto();
                            int contador;
                            Application.DoEvents();
                            pbAtualiza.Visible = true;
                            pbAtualiza.Value = 0;
                            pbAtualiza.Step = 1;
                            pbAtualiza.Maximum = 0;
                            contador = 0;

                            Principal.dtBusca = produto.BuscaProdutosEstoqueFilial(Principal.estAtual, Principal.empAtual);
                            if (Principal.dtBusca.Rows.Count > 0)
                            {
                                pbAtualiza.Maximum = Principal.dtBusca.Rows.Count;
                                Cursor = Cursors.WaitCursor;
                                for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                                {
                                    response = await client.GetAsync("Produtos/InsereEstoque?codBarras=" + Principal.dtBusca.Rows[i]["PROD_CODIGO"].ToString().Replace("'","").Replace("#","")
                                        + "&descricao=" + Principal.dtBusca.Rows[i]["PROD_DESCR"].ToString().Trim()
                                        + "&qtde=" + Convert.ToInt32(Principal.dtBusca.Rows[i]["PROD_ESTATUAL"]).ToString()
                                        + "&pCusto=" + Principal.dtBusca.Rows[i]["PROD_ULTCUSME"].ToString().Replace(',', '.')
                                        + "&pVenda=" + Principal.dtBusca.Rows[i]["PRE_VALOR"].ToString().Replace(',', '.') + "&codEstab=" + codLoja);
                                    if (response.IsSuccessStatusCode)
                                    {
                                        pbAtualiza.Value = contador;
                                        Application.DoEvents();
                                        contador = contador + 1;
                                        lblRegistros.Text = "Registros: " + Convert.ToString(contador);
                                    }
                                    else
                                    {
                                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\Produtos.txt", true))
                                        {
                                            writer.WriteLine(Principal.dtBusca.Rows[i]["PROD_CODIGO"].ToString());
                                        }
                                    }
                                }

                                MessageBox.Show("Cadastrado com sucesso!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                pbAtualiza.Visible = false;
                                pbAtualiza.Value = 0;
                                pbAtualiza.Step = 1;
                                pbAtualiza.Maximum = 0;
                                contador = 0;
                                lblRegistros.Text = "";
                            }
                            else
                            {
                                MessageBox.Show("Nenhum registro encontrado!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Estabelecimento já possui produtos cadastrados!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private async void btnExcluiProd_Click(object sender, EventArgs e)
        {
            try
            {
                string codLoja = Funcoes.LeParametro(9, "52", true);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/ExcluiProdutos?codEstab=" + codLoja);
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        if (responseBody.Equals("true"))
                        {
                            MessageBox.Show("Registros exluídos", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Erro ao excluir registros!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private async void btnBuscar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                dgProdutos.Rows.Clear();
                await ConsultarFiliais(txtCodigo.Text.Trim());
            }
            else
            {
                MessageBox.Show("Código de Barras não pode ser em branco!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodigo.Focus();
            }
        }

        private async Task ConsultarFiliais(string codBarras)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/BuscaProduto?grupoId=" + Funcoes.LeParametro(9,"53",false) + "&codBarras=" + codBarras + "&codEstab=" + 0);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    List<Negocio.EstoqueFilial> retorno = JsonConvert.DeserializeObject<List<Negocio.EstoqueFilial>>(responseBody);
                    if (retorno[0].codErro.Equals("00"))
                    {
                        dgProdutos.Rows.Clear();
                        for (int i = 0; i < retorno.Count; i++)
                        {
                            dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] {
                                retorno[i].codEstabelecimento, retorno[i].codBarras, retorno[i].quantidade, retorno[i].precoCusto, retorno[i].precoVenda,
                                retorno[i].descricao, retorno[i].descEstab, retorno[i].telefone
                            });
                        }
                        dgProdutos.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Produto não encontrado!", "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodigo.Text = "";
                        txtCodigo.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Produtos Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
