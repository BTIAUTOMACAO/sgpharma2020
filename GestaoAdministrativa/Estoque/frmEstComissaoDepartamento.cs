﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstComissaoDepartamento : Form
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private ToolStripButton tsbComissaoDepto = new ToolStripButton("Comissão por Dept.");
        private ToolStripSeparator tssComissaoDepto = new ToolStripSeparator();
        #endregion

        public frmEstComissaoDepartamento(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstComissaoDepartamento_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
                rdbTodos.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbComissaoDepto.AutoSize = false;
                this.tsbComissaoDepto.Image = Properties.Resources.estoque;
                this.tsbComissaoDepto.Size = new System.Drawing.Size(140, 20);
                this.tsbComissaoDepto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbComissaoDepto);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssComissaoDepto);
                tsbComissaoDepto.Click += delegate
                {
                    var estAtualiza = Application.OpenForms.OfType<frmEstComissaoDepartamento>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estAtualiza.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbComissaoDepto);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssComissaoDepto);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstComissaoDepartamento_Shown(object sender, EventArgs e)
        {
            //CARREGA DEPARTAMENTOS//
            Principal.dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count == 0)
            {
                MessageBox.Show("Necessário cadastrar pelo menos um Departameto,\npara lançar uma comissão.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Principal.mdiPrincipal.btnSair.PerformClick();
                return;
            }
            else
            {
                cmbDepartamento.DataSource = Principal.dtPesq;
                cmbDepartamento.DisplayMember = "DEP_DESCR";
                cmbDepartamento.ValueMember = "DEP_CODIGO";
                cmbDepartamento.SelectedIndex = -1;
            }
        }

        private void cmbDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (cmbDepartamento.SelectedIndex != -1)
                {
                    txtComissao.Focus();
                }
                else
                    rdbTodos.Focus();
            }
        }

        private void txtComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                btnAtualizar.PerformClick();
            }
        }

        private void txtComissao_Validated(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txtComissao.Text))
            {
                txtComissao.Text = "0";
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdbTodos.Checked)
                {
                    if (MessageBox.Show("Deseja ATUALIZAR TODA A COMISSÃO?", "Comissão por Departamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        rdbTodos.Checked = false;
                        cmbDepartamento.Focus();
                        return;
                    }
                }

                if (MessageBox.Show("Confirma a Atualização da Comissão?", "Comissão por Departamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Cursor = Cursors.WaitCursor;
                    var atualizacao = new ProdutoDetalhe();

                    if (atualizacao.AtualizaComissaoPorDepartamentoOuNao(Principal.empAtual, Principal.estAtual, rdbTodos.Checked, Convert.ToInt32(cmbDepartamento.SelectedValue), Convert.ToDouble(txtComissao.Text)))
                    {
                        MessageBox.Show("Atualização realizada com Sucesso!", "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string sql = "INSERT INTO LOG_ALTERACAO (LOG_ID, LOG_IDVALOR, LOG_OPERADOR, LOG_OPERACAO, LOG_DATA, LOG_TABELA, "
                           + "LOG_CAMPO, LOG_ANTERIOR, LOG_ALTERADO, EST_CODIGO, EMP_CODIGO)"
                           + " VALUES ('PROD_ESTATUAL','0','" + Principal.usuario + "', 'ALTERAÇÃO', " + Funcoes.BDataHora(DateTime.Now) + ", 'PRODUTOS_DETALHE', 'PROD_COMISSAO','0','"
                           + Funcoes.BValor(Convert.ToDouble(txtComissao.Text)) + "'," + Principal.estAtual
                           + "," + Principal.empAtual + ")";
                        BancoDados.ExecuteNoQuery(sql, null);
                    }

                    cmbDepartamento.SelectedIndex = -1;
                    rdbTodos.Checked = false;
                    txtComissao.Text = "0";
                    cmbDepartamento.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Comissão por Departamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
