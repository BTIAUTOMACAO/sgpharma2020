﻿namespace GestaoAdministrativa.Estoque
{
    partial class frmEstRel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstRel));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdbDescricao = new System.Windows.Forms.RadioButton();
            this.rdbCodigo = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbbTerapeutica = new System.Windows.Forms.ComboBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.chkZerados = new System.Windows.Forms.CheckBox();
            this.gbTipoRelatorio = new System.Windows.Forms.GroupBox();
            this.rdbLayout2 = new System.Windows.Forms.RadioButton();
            this.rdbLayout1 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.rdbMovimentacao = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gbTipoRelatorio.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(6, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 490);
            this.panel2.TabIndex = 43;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.btnPesquisar);
            this.groupBox2.Controls.Add(this.chkZerados);
            this.groupBox2.Controls.Add(this.gbTipoRelatorio);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(947, 166);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtros";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdbDescricao);
            this.groupBox4.Controls.Add(this.rdbCodigo);
            this.groupBox4.Location = new System.Drawing.Point(6, 21);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(124, 111);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ordem Por";
            // 
            // rdbDescricao
            // 
            this.rdbDescricao.AutoSize = true;
            this.rdbDescricao.ForeColor = System.Drawing.Color.Navy;
            this.rdbDescricao.Location = new System.Drawing.Point(17, 65);
            this.rdbDescricao.Name = "rdbDescricao";
            this.rdbDescricao.Size = new System.Drawing.Size(88, 20);
            this.rdbDescricao.TabIndex = 3;
            this.rdbDescricao.TabStop = true;
            this.rdbDescricao.Text = "Descrição";
            this.rdbDescricao.UseVisualStyleBackColor = true;
            this.rdbDescricao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbDescricao_KeyDown);
            this.rdbDescricao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbDescricao_KeyPress);
            // 
            // rdbCodigo
            // 
            this.rdbCodigo.AutoSize = true;
            this.rdbCodigo.Checked = true;
            this.rdbCodigo.ForeColor = System.Drawing.Color.Navy;
            this.rdbCodigo.Location = new System.Drawing.Point(17, 39);
            this.rdbCodigo.Name = "rdbCodigo";
            this.rdbCodigo.Size = new System.Drawing.Size(71, 20);
            this.rdbCodigo.TabIndex = 2;
            this.rdbCodigo.TabStop = true;
            this.rdbCodigo.Text = "Código";
            this.rdbCodigo.UseVisualStyleBackColor = true;
            this.rdbCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbCodigo_KeyDown);
            this.rdbCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbCodigo_KeyPress);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbbTerapeutica);
            this.groupBox5.Location = new System.Drawing.Point(437, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(318, 111);
            this.groupBox5.TabIndex = 163;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Classe Terapêutica";
            // 
            // cbbTerapeutica
            // 
            this.cbbTerapeutica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTerapeutica.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTerapeutica.ForeColor = System.Drawing.Color.Black;
            this.cbbTerapeutica.FormattingEnabled = true;
            this.cbbTerapeutica.Location = new System.Drawing.Point(12, 29);
            this.cbbTerapeutica.Name = "cbbTerapeutica";
            this.cbbTerapeutica.Size = new System.Drawing.Size(294, 24);
            this.cbbTerapeutica.TabIndex = 1;
            this.cbbTerapeutica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbbTerapeutica_KeyDown);
            this.cbbTerapeutica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbTerapeutica_KeyPress);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(761, 84);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(138, 48);
            this.btnPesquisar.TabIndex = 162;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            this.btnPesquisar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnPesquisar_KeyDown);
            // 
            // chkZerados
            // 
            this.chkZerados.AutoSize = true;
            this.chkZerados.Location = new System.Drawing.Point(6, 138);
            this.chkZerados.Name = "chkZerados";
            this.chkZerados.Size = new System.Drawing.Size(137, 20);
            this.chkZerados.TabIndex = 7;
            this.chkZerados.Text = "Produtos Zerados";
            this.chkZerados.UseVisualStyleBackColor = true;
            this.chkZerados.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkZerados_KeyDown);
            this.chkZerados.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkZerados_KeyPress);
            // 
            // gbTipoRelatorio
            // 
            this.gbTipoRelatorio.Controls.Add(this.rdbMovimentacao);
            this.gbTipoRelatorio.Controls.Add(this.rdbLayout2);
            this.gbTipoRelatorio.Controls.Add(this.rdbLayout1);
            this.gbTipoRelatorio.Location = new System.Drawing.Point(136, 21);
            this.gbTipoRelatorio.Name = "gbTipoRelatorio";
            this.gbTipoRelatorio.Size = new System.Drawing.Size(295, 111);
            this.gbTipoRelatorio.TabIndex = 4;
            this.gbTipoRelatorio.TabStop = false;
            this.gbTipoRelatorio.Text = "Layout";
            // 
            // rdbLayout2
            // 
            this.rdbLayout2.AutoSize = true;
            this.rdbLayout2.ForeColor = System.Drawing.Color.Navy;
            this.rdbLayout2.Location = new System.Drawing.Point(17, 55);
            this.rdbLayout2.Name = "rdbLayout2";
            this.rdbLayout2.Size = new System.Drawing.Size(253, 20);
            this.rdbLayout2.TabIndex = 3;
            this.rdbLayout2.TabStop = true;
            this.rdbLayout2.Text = "Quantidade por Controle de Vendas";
            this.rdbLayout2.UseVisualStyleBackColor = true;
            this.rdbLayout2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbLayout2_KeyDown);
            this.rdbLayout2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbLayout2_KeyPress);
            // 
            // rdbLayout1
            // 
            this.rdbLayout1.AutoSize = true;
            this.rdbLayout1.Checked = true;
            this.rdbLayout1.ForeColor = System.Drawing.Color.Navy;
            this.rdbLayout1.Location = new System.Drawing.Point(17, 29);
            this.rdbLayout1.Name = "rdbLayout1";
            this.rdbLayout1.Size = new System.Drawing.Size(253, 20);
            this.rdbLayout1.TabIndex = 2;
            this.rdbLayout1.TabStop = true;
            this.rdbLayout1.Text = "Quantidade por Produto Controlado";
            this.rdbLayout1.UseVisualStyleBackColor = true;
            this.rdbLayout1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbLayout1_KeyDown);
            this.rdbLayout1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbLayout1_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(158, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Relatório de Estoque";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // rdbMovimentacao
            // 
            this.rdbMovimentacao.AutoSize = true;
            this.rdbMovimentacao.ForeColor = System.Drawing.Color.Navy;
            this.rdbMovimentacao.Location = new System.Drawing.Point(17, 82);
            this.rdbMovimentacao.Name = "rdbMovimentacao";
            this.rdbMovimentacao.Size = new System.Drawing.Size(222, 20);
            this.rdbMovimentacao.TabIndex = 4;
            this.rdbMovimentacao.TabStop = true;
            this.rdbMovimentacao.Text = "Quantidade por Movimentação";
            this.rdbMovimentacao.UseVisualStyleBackColor = true;
            // 
            // frmEstRel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEstRel";
            this.Text = "frmEstRelBalanco";
            this.Load += new System.EventHandler(this.frmEstRel_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEstRel_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.gbTipoRelatorio.ResumeLayout(false);
            this.gbTipoRelatorio.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox gbTipoRelatorio;
        private System.Windows.Forms.RadioButton rdbLayout2;
        private System.Windows.Forms.RadioButton rdbLayout1;
        private System.Windows.Forms.CheckBox chkZerados;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdbDescricao;
        private System.Windows.Forms.RadioButton rdbCodigo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cbbTerapeutica;
        private System.Windows.Forms.RadioButton rdbMovimentacao;
    }
}