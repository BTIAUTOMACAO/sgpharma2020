﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstManutencaoUltCusto : Form, Botoes
    {
        private ToolStripButton tsbManutencao = new ToolStripButton("Mant. Últ. Custo");
        private ToolStripSeparator tssManutencao = new ToolStripSeparator();
        private string ID;

        public frmEstManutencaoUltCusto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstManutencaoUltCusto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssManutencao);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbManutencao.AutoSize = false;
                this.tsbManutencao.Image = Properties.Resources.estoque;
                this.tsbManutencao.Size = new System.Drawing.Size(140, 20);
                this.tsbManutencao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbManutencao);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssManutencao);
                tsbManutencao.Click += delegate
                {
                    var manutencao = Application.OpenForms.OfType<frmEstManutencaoUltCusto>().FirstOrDefault();
                    Util.BotoesGenericos();
                    manutencao.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if (control.Name != "txtCustoAtual")
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }

                    txtCustoAtual.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ULTCUSME"]);
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();
                    txtUltCusto.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtUltCusto.Focus();
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtUltCusto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtCustoAtual_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtUltCusto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtUltCusto.Text))
                {
                    MessageBox.Show("Último Custo não pode ser em Branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUltCusto.Focus();
                }
                else if (Convert.ToDouble(txtUltCusto.Text) == 0)
                {
                    MessageBox.Show("Último Custo não pode ser Zero!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUltCusto.Focus();
                }
                else
                    txtObs.Focus();
            }
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAdicionar.PerformClick();
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. Barras não pode ser em Branco!";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtUltCusto.Text.Trim()))
            {
                Principal.mensagem = "Último Custo não pode ser em Branco!";
                Funcoes.Avisa();
                txtUltCusto.Focus();
                return false;
            }
            if (Convert.ToDouble(txtUltCusto.Text) == 0)
            {
                Principal.mensagem = "Último Custo não pode ser Zero!";
                Funcoes.Avisa();
                txtUltCusto.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtObs.Text.Trim()))
            {
                Principal.mensagem = "Observação não pode ser em Branco!";
                Funcoes.Avisa();
                txtObs.Focus();
                return false;
            }
            return true;
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConsisteCampos())
                {
                    if (MessageBox.Show("Deseja alterar Último Custo?", "Consistência", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        BancoDados.AbrirTrans();

                        var produto = new ProdutoDetalhe();

                        if (!produto.AtualizaUltimoCusto(Principal.empAtual, Principal.estAtual, Convert.ToDouble(txtUltCusto.Text), txtCodBarras.Text))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao atualizar Último Custo", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        var dados = new LogUltimoCusto(
                            Principal.empAtual,
                            Principal.estAtual,
                            txtCodBarras.Text,
                            DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                            Convert.ToDouble(txtCustoAtual.Text),
                            Convert.ToDouble(txtUltCusto.Text),
                            Principal.usuario,
                            txtObs.Text
                            );

                        if (!dados.InsereRegistros(dados))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao atualizar Último Custo", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        BancoDados.FecharTrans();

                        MessageBox.Show("Atualização realizada com sucesso!", "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Limpar();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtCustoAtual.Text = "";
            txtUltCusto.Text = "";
            txtObs.Text = "";
            txtBCodBarras.Text = "";
            dtData.Text = "";
            cmbUsuario.SelectedIndex = -1;

            var busca = new LogUltimoCusto();
            busca.EmpCodigo = Principal.empAtual;
            busca.EstCodigo = Principal.estAtual;
            busca.ProdCodigo = txtBCodBarras.Text;
            busca.DtCadastro = dtData.Text;
            busca.OpCadastro = cmbUsuario.Text;

            DataTable dtRetorno = busca.BuscaDados(busca);
            dgBusca.DataSource = dtRetorno;

            txtCodBarras.Focus();
        }

        private void frmEstManutencaoUltCusto_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("LOGIN_ID", "ID", "USUARIOS", false, "LIBERADO= 'S'");

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "ID";
                cmbUsuario.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var busca = new LogUltimoCusto();
                busca.EmpCodigo = Principal.empAtual;
                busca.EstCodigo = Principal.estAtual;
                busca.ProdCodigo = txtBCodBarras.Text;
                busca.DtCadastro = dtData.Text;
                busca.OpCadastro = cmbUsuario.Text;

                DataTable dtRetorno = busca.BuscaDados(busca);

                if (dtRetorno.Rows.Count > 0)
                {
                    dgBusca.DataSource = dtRetorno;
                }
                else
                {
                    dgBusca.DataSource = dtRetorno;
                    MessageBox.Show("Nenhum registro encontrado!", "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBCodBarras.Text = "";
                    dtData.Text = "";
                    cmbUsuario.SelectedIndex = -1;
                    txtBCodBarras.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Manutenção do Último Custo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
