﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstAjustes : Form, Botoes
    {
        private ToolStripButton tsbAjuste = new ToolStripButton("Ajuste de Estoque");
        private ToolStripSeparator tssAjuste = new ToolStripSeparator();
        private string ID;
        private double custo;
        private double valor;

        public frmEstAjustes(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAjuste);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAjuste);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbAjuste.AutoSize = false;
                this.tsbAjuste.Image = Properties.Resources.estoque;
                this.tsbAjuste.Size = new System.Drawing.Size(140, 20);
                this.tsbAjuste.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAjuste);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAjuste);
                tsbAjuste.Click += delegate
                {
                    var ajuste = Application.OpenForms.OfType<frmEstAjustes>().FirstOrDefault();
                    Funcoes.BotoesCadastro(ajuste.Name);
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    ajuste.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtQtdeEstoque") && (control.Name != "txtPreco"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }


        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            try
            {
                valor = 0;
                var dtLePrdutos = new DataTable();
                Cursor = Cursors.WaitCursor;

                var buscaProduto = new Produto();
                dtLePrdutos = buscaProduto.BuscaProdutosAjusteEstoque(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
                if (dtLePrdutos.Rows.Count != 0)
                {
                    txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();

                    //VERIFICA SE PRODUTO ESTA LIBERADO OU NÃO//
                    if (dtLePrdutos.Rows[0]["PROD_SITUACAO"].ToString() == "I")
                    {
                        MessageBox.Show("Produto não liberado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodBarras.Focus();
                        return false;
                    }

                    txtPreco.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_CUSME"]);
                    txtQtdeEstoque.Text = (Convert.ToDouble(dtLePrdutos.Rows[0]["PROD_ESTATUAL"])).ToString();
                    txtQtde.Text = "0";
                    ID = dtLePrdutos.Rows[0]["PROD_ID"].ToString();
                    valor = Convert.ToDouble(dtLePrdutos.Rows[0]["PRE_VALOR"]);
                    txtQtde.Focus();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDescr.Text = "Produto não cadastrado";
                    txtCodBarras.Focus();
                    return false;
                }

                Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void txtCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtDescr_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtDescr.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    var buscaProdDescricao = new Produto();
                    using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                    {
                        if (dtBusca.Rows.Count == 1)
                        {
                            txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                            LeProdutosCod();
                        }
                        else if (dtBusca.Rows.Count == 0)
                        {
                            MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDescr.Text = "";
                            txtDescr.Focus();
                        }
                        else
                        {
                            using (var buscaProd = new frmBuscaProd(false))
                            {
                                buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                buscaProd.ShowDialog();
                            }
                            if (!String.IsNullOrEmpty(Principal.codBarra))
                            {
                                txtCodBarras.Text = Principal.codBarra;
                                txtDescr.Text = Principal.prodDescr;
                                LeProdutosCod();
                                Principal.codBarra = "";
                                Principal.prodDescr = "";
                            }
                            else
                            {
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtPreco.Text = "";
            txtQtdeEstoque.Text = "";
            txtQtde.Text = "";
            rdbMais.Checked = true;
            txtObs.Text = "";
            txtBCodBarras.Text = "";
            dtData.Text = "";
            cmbUsuario.SelectedIndex = -1;

            var busca = new AlteracaoEstoque();
            busca.EmpCodigo = Principal.empAtual;
            busca.EstCodigo = Principal.estAtual;
            busca.ProdCodigo = txtBCodBarras.Text;
            busca.DtCadastro = dtData.Text;
            busca.OpCadastro = cmbUsuario.Text;

            DataTable dtRetorno = busca.BuscaDados(busca);
            dgBusca.DataSource = dtRetorno;

            txtCodBarras.Focus();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void frmEstAjustes_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                rdbMais.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbMais_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && rdbMais.Checked)
                txtQtde.Focus();
            else
                rdbMenos.Focus();
        }

        private void rdbMais_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void rdbMenos_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void rdbMenos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtObs_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnAdicionar.PerformClick();
        }

        private void rdbMais_TextChanged(object sender, EventArgs e)
        {
            if (rdbMais.Checked)
                txtQtde.Focus();
        }

        private void rdbMenos_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbMenos.Checked)
                txtQtde.Focus();
        }

        private void txtQtde_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                if(ConsisteCampos())
                {
                    long id = Funcoes.GeraIDLong("MOV_ESTOQUE", "EST_INDICE", Principal.estAtual, "", Principal.empAtual);


                    var dadosAjuste = new AlteracaoEstoque(
                        Principal.empAtual,
                        Principal.estAtual,
                        Funcoes.IdentificaVerificaID("ALTERACAO_ESTOQUE", "ID", 0, "", 0),
                        txtCodBarras.Text,
                        rdbMais.Checked == true ? "+" : "-",
                        Convert.ToInt32(txtQtde.Text),
                        Convert.ToInt32(txtQtdeEstoque.Text),
                        txtObs.Text,
                        id,
                        Convert.ToDouble(txtPreco.Text),
                        Principal.nomeEstacao,
                        DateTime.Now.ToString(),
                        Principal.usuario);

                    BancoDados.AbrirTrans();

                    if(dadosAjuste.InsereRegistros(dadosAjuste))
                    {
                        var prod = new Produto();

                        if(prod.AtualizaEstoque(txtCodBarras.Text, Convert.ToInt32(txtQtde.Text), rdbMais.Checked == true ? "+" : "-",true))
                        {
                            var dadosMovimento = new MovEstoque
                            {
                                EstCodigo = Principal.estAtual,
                                EmpCodigo = Principal.empAtual,
                                EstIndice = id,
                                ProdCodigo = txtCodBarras.Text,
                                EntQtde = rdbMais.Checked == true ? Convert.ToInt32(txtQtde.Text) : 0,
                                EntValor = 0,
                                SaiQtde = rdbMenos.Checked == true ? Convert.ToInt32(txtQtde.Text) : 0,
                                SaiValor = 0,
                                OperacaoCodigo = "A",
                                DataMovimento = DateTime.Now,
                                DtCadastro = DateTime.Now,
                                OpCadastro = Principal.usuario
                            };

                            if (dadosMovimento.ManutencaoMovimentoEstoque("I", dadosMovimento))
                            {
                                int estoque = 0;
                                if (rdbMais.Checked)
                                    estoque = Convert.ToInt32(txtQtdeEstoque.Text) + Convert.ToInt32(txtQtde.Text);
                                else
                                    estoque = Convert.ToInt32(txtQtdeEstoque.Text) - Convert.ToInt32(txtQtde.Text);

                                var log = new LogAlteraQtde(
                                    Principal.empAtual,
                                    Principal.estAtual,
                                    txtCodBarras.Text,
                                    DateTime.Now,
                                    Convert.ToInt32(txtQtdeEstoque.Text),
                                    estoque,
                                    Principal.usuario
                                    );

                                if(log.InsereRegistros(log))
                                {
                                    BancoDados.FecharTrans();

                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        IncluiOuAtualizaProdutoFilial(rdbMais.Checked == true ? "mais" : "menos", Convert.ToInt32(txtQtde.Text), Convert.ToDouble(txtPreco.Text), valor);
                                    }
                                    
                                    MessageBox.Show("Ajuste realizado com Sucesso!", "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    txtCodBarras.Text = "";
                                    txtDescr.Text = "";
                                    txtPreco.Text = "";
                                    txtQtdeEstoque.Text = "";
                                    txtQtde.Text = "";
                                    rdbMais.Checked = true;
                                    txtObs.Text = "";
                                    txtBCodBarras.Text = "";
                                    dtData.Text = "";
                                    cmbUsuario.SelectedIndex = -1;
                                    txtCodBarras.Focus();
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    btnAdicionar.Focus();
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                btnAdicionar.Focus();
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            btnAdicionar.Focus();
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        btnAdicionar.Focus();
                    }

                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public async void IncluiOuAtualizaProdutoFilial(string operacao, int quantidade, double custo, double valor)
        {
            await InsereEstoqueFiliais(operacao, Funcoes.LeParametro(9, "52", true), quantidade, custo, valor);
        }

        private async Task InsereEstoqueFiliais(string operacao, string filial, int quantidade, double custo, double venda)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoque?codBarra=" + txtCodBarras.Text
                                    + "&descricao=" + txtDescr.Text
                                    + "&qtde=" + quantidade
                                    + "&precoCusto=" + custo.ToString().Replace(",", ".")
                                    + "&precoVenda=" + venda.ToString().Replace(",", ".") + "&codEstabelecimento=" + filial + "&operacao=" + operacao);
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\AjusteEstoque.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + quantidade.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void TeclasAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnAdicionar.PerformClick();
                    break;
                case Keys.F2:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. Barras não pode ser em Branco!";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()))
            {
                Principal.mensagem = "Qtde não pode ser em Branco!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (Convert.ToInt32(txtQtde.Text) == 0)
            {
                Principal.mensagem = "Qtde não pode ser Zero!";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            
            return true;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void btnAdicionar_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtBCodBarras_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void txtBCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtData.Focus();
        }

        private void dtData_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUsuario.Focus();
        }

        private void cmbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void cmbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void frmEstAjustes_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("LOGIN_ID", "ID", "USUARIOS", false, "LIBERADO= 'S'");

                DataRow row = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row, 0);
                cmbUsuario.DataSource = Principal.dtPesq;
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "ID";
                cmbUsuario.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var busca = new AlteracaoEstoque();
                busca.EmpCodigo = Principal.empAtual;
                busca.EstCodigo = Principal.estAtual;
                busca.ProdCodigo = txtBCodBarras.Text;
                busca.DtCadastro = dtData.Text;
                busca.OpCadastro = cmbUsuario.Text;

                DataTable dtRetorno = busca.BuscaDados(busca);

                if(dtRetorno.Rows.Count > 0)
                {
                    dgBusca.DataSource = dtRetorno;
                }
                else
                {
                    dgBusca.DataSource = dtRetorno;
                    MessageBox.Show("Nenhum registro encontrado!", "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBCodBarras.Text = "";
                    dtData.Text = "";
                    cmbUsuario.SelectedIndex = -1;
                    txtBCodBarras.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Ajuste de Estoque", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstAjustes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }

        private void dgBusca_KeyDown(object sender, KeyEventArgs e)
        {
            TeclasAtalho(e);
        }
    }
}
