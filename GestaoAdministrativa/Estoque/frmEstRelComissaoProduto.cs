﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque.Relatorio;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Estoque
{
    public partial class frmEstRelComissaoProduto : Form
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Comissão por Produto");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmEstRelComissaoProduto(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmEstRelComissaoProduto_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            cmbDepartamento.Text = "TODOS OS DEPARTAMENTOS";
            chkZerados.Checked = false;
            chkComissao.Checked = false;
            txtComissao.Text = "";
            rdbIgual.Checked = true;
            cmbDepartamento.Focus();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnBuscar.PerformClick();
                    break;
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(150, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var estRelatorio = Application.OpenForms.OfType<frmEstRelComissaoProduto>().FirstOrDefault();
                    Util.BotoesGenericos();
                    estRelatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmEstRelComissaoProduto_Shown(object sender, EventArgs e)
        {
            DataTable dtDepartamento = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
            dtDepartamento.Rows.Add(0, "TODOS OS DEPARTAMENTOS");
            cmbDepartamento.DataSource = dtDepartamento;
            cmbDepartamento.DisplayMember = "DEP_DESCR";
            cmbDepartamento.ValueMember = "DEP_CODIGO";
            cmbDepartamento.SelectedValue = 0;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                Cursor = Cursors.WaitCursor;
                var busca = new Produto();
                DataTable dtRetorno = new DataTable();

                dtRetorno = busca.BuscaProdutoPorComissao(Principal.estAtual, Principal.empAtual, Convert.ToInt32(cmbDepartamento.SelectedValue), chkZerados.Checked,
                    chkComissao.Checked, txtComissao.Text == "" ? 0 : Convert.ToInt32(txtComissao.Text), rdbIgual.Checked ? 1 : 2);
                if (dtRetorno.Rows.Count > 0)
                {
                    frmRelatorioComissao relatorio = new frmRelatorioComissao(this, dtRetorno);
                    relatorio.Text = "Comissão por Produto";
                    relatorio.ShowDialog();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Nenhum registro encontrado!", "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Comissão por Produto", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbDepartamento_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void chkZerados_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void chkComissao_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmEstRelComissaoProduto_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbDepartamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void chkZerados_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void chkComissao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }
    }
}
