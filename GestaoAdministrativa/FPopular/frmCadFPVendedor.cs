﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;

namespace GestaoAdministrativa.FPopular
{
    public partial class frmCadFPVendedor : Form, Botoes
    {
        private ToolStripButton tsbVendedor = new ToolStripButton("Vendedor FPopular");
        private ToolStripSeparator tssVendedor = new ToolStripSeparator();
        private DataTable dtColab = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private FPVendedores fpVendedor = new FPVendedores();
        private bool decrescente;

        public frmCadFPVendedor(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtColabID"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmCadFPVendedor_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadFPVendedor");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
            if (tcColab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcColab.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtColabID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbVendedor.AutoSize = false;
                this.tsbVendedor.Image = Properties.Resources.cadastro;
                this.tsbVendedor.Size = new System.Drawing.Size(140, 20);
                this.tsbVendedor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbVendedor);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssVendedor);
                tsbVendedor.Click += delegate
                {
                    var cadColab = Application.OpenForms.OfType<frmCadFPVendedor>().FirstOrDefault();
                    Funcoes.BotoesCadastro(cadColab.Name);
                    BotoesHabilitados();
                    cadColab.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCadFPVendedor_Shown(object sender, EventArgs e)
        {
            try
            {
                var dados = new Colaborador();
                Principal.dtPesq = dados.BuscaDadosColaborador(Principal.estAtual, "V");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Vendedor.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbVendedor.DataSource = Principal.dtPesq;
                    cmbVendedor.DisplayMember = "COL_NOME";
                    cmbVendedor.ValueMember = "COL_CODIGO";

                    cmbVendedor.SelectedIndex = -1;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcColab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcColab.SelectedTab == tpGrade)
            {
                dgColab.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else
                if (tcColab.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmCadFPVendedor");
                    lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;
                        Principal.mdiPrincipal.btnLimpar.Enabled = true;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnLimpar.Enabled = false;
                        emGrade = false;
                        txtColabID.Focus();
                    }
                }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtColabID.Text = (Convert.ToDouble(dtColab.Rows[linha]["COL_CODIGO"])).ToString();
                cmbVendedor.Text = dtColab.Rows[linha]["COL_NOME"].ToString();
                txtDataSus.Text = dtColab.Rows[linha]["USUARIO_DATASUS"].ToString();
               
                txtSenha.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["SENHA_DATASUS"].ToString()) == "" ? "" : dtColab.Rows[linha]["SENHA_DATASUS"].ToString();
     
                txtData.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtColab.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, linha));
                txtColabID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtColabID.Text = Convert.ToString(cmbVendedor.SelectedValue);
        }

        private void txtColabID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbVendedor.Focus();
        }

        private void txtColabID_Validated(object sender, EventArgs e)
        {
            Funcoes.AchaCombo(cmbVendedor, txtColabID);
        }

        public void Primeiro()
        {
            if (dtColab.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcColab.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = dtColab.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.bNaveg = false;
            Principal.indice = 0;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgColab.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgColab.CurrentCell = dgColab.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void cmbVendedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDataSus.Focus();
        }

        private void txtDataSus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSenha.Focus();
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtColabID.Text.Trim()))
            {
                Principal.mensagem = "Vendedor ID não pode ser branco.";
                Funcoes.Avisa();
                txtColabID.Focus();
                return false;
            }
            if (cmbVendedor.SelectedIndex == -1)
            {
                Principal.mensagem = "Selecione o vendedor.";
                Funcoes.Avisa();
                cmbVendedor.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDataSus.Text.Trim()))
            {
                Principal.mensagem = "Usuário DATASUS não pode ser em branco.";
                Funcoes.Avisa();
                txtDataSus.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtSenha.Text.Trim()))
            {
                Principal.mensagem = "Senha não pode ser em branco.";
                Funcoes.Avisa();
                txtSenha.Focus();
                return false;
            }

            return true;
        }

        public void AtalhoGrade()
        {
            tcColab.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcColab.SelectedTab = tpFicha;
        }

        public bool Atualiza()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;

                    fpVendedor.est_codigo = Principal.estAtual;
                    fpVendedor.col_codigo = txtColabID.Text.Trim();
                    fpVendedor.usuario_datasus = txtDataSus.Text.Trim();
                    fpVendedor.senha = txtSenha.Text.Trim();
                    fpVendedor.dtcadastro = DateTime.Now;
                    fpVendedor.opcadastro = Principal.usuario;
                    fpVendedor.opalteracao = Principal.usuario;
                    fpVendedor.dtalteracao = DateTime.Now;

                    Principal.dtBusca = FPVendedores.GetRegistros("COL_CODIGO", txtColabID.Text);
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        //VERIFICA SE USUARIO JA ESTA CADASTRADO//
                        Principal.dtPesq = FPVendedores.GetRegistros("USUARIO_DATASUS", txtDataSus.Text, true);
                        if (Principal.dtPesq.Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Usuário já cadastrado.";
                            Funcoes.Avisa();
                            txtDataSus.Text = "";
                            txtDataSus.Focus();
                            return false;
                        }

                        if (FPVendedores.InserirDados(fpVendedor).Equals(true))
                        {
                            MessageBox.Show("O cadastro do vendedor foi realizado com sucesso! Certifique-se de cadastrá-lo também no portal da Farmácia Popular!", "Farmácia Popular");
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("COL_CODIGO", txtColabID.Text, Principal.usuario, "FP_VENDEDORES", txtDataSus.Text, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtColab.Clear();
                            dgColab.DataSource = dtColab;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtColabID.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        if (FPVendedores.AtualizaDados(fpVendedor, Principal.dtBusca).Equals(true))
                        {
                            MessageBox.Show("Atualização realizada com sucesso!", "Farmácia Popular");
                        }
                       
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtColab.Clear();
                        dgColab.DataSource = dtColab;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;

                    fpVendedor.est_codigo = Principal.estAtual;
                    fpVendedor.col_codigo = txtColabID.Text.Trim();
                    fpVendedor.usuario_datasus = txtDataSus.Text.Trim();
                    fpVendedor.senha = txtSenha.Text.Trim();
                    fpVendedor.dtcadastro = DateTime.Now;
                    fpVendedor.opcadastro = Principal.usuario;
                    fpVendedor.opalteracao = Principal.usuario;
                    fpVendedor.dtalteracao = DateTime.Now;

                    Principal.dtBusca = FPVendedores.GetRegistros("COL_CODIGO", txtColabID.Text.Trim());
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        if (FPVendedores.InserirDados(fpVendedor).Equals(true))
                        {
                            MessageBox.Show("O cadastro do vendedor foi realizado com sucesso! Certifique-se de cadastrá-lo também no portal da Farmácia Popular!", "Farmácia Popular");
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("COL_CODIGO", txtColabID.Text, Principal.usuario, "FP_VENDEDORES", txtDataSus.Text, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtColab.Clear();
                            dgColab.DataSource = dtColab;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtColabID.Focus();
                            return false;
                        }
                    }
                    else
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Usuário já cadastrado.";
                        Funcoes.Avisa();
                        txtDataSus.Text = "";
                        txtDataSus.Focus();
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtColabID.Text.Trim() != "") && (txtDataSus.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do vendedor?", "Exclusão tabela FP Vendedor", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (FPVendedores.ExcluirDados(txtColabID.Text.Trim()).Equals(1))
                        {
                            Funcoes.GravaLogExclusao("COL_CODIGO", txtColabID.Text, Principal.usuario, DateTime.Now, "FP_VENDEDORES", txtDataSus.Text, Principal.motivo, Principal.estAtual);
                            dtColab.Clear();
                            MessageBox.Show("Vendedor removido com sucesso!", "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgColab.DataSource = dtColab;
                            emGrade = false;
                            tcColab.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Sair()
        {
            try
            {
                dtColab.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbVendedor);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssVendedor);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtColab.Clear();
            dgColab.DataSource = dtColab;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtColabID.Text = "";
            cmbVendedor.SelectedIndex = -1;
            txtDataSus.Text = "";
            txtSenha.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            txtColabID.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcColab.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void ImprimirRelatorio()
        {
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                fpVendedor.col_codigo = txtBID.Text.Trim();
                fpVendedor.usuario_datasus = txtBNome.Text.Trim();

                dtColab = FPVendedores.GetBuscar(fpVendedor);
                if (dtColab.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtColab.Rows.Count;
                    dgColab.DataSource = dtColab;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, 0));
                    dgColab.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgColab.DataSource = dtColab;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgColab_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
            emGrade = true;
        }

        private void dgColab_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcColab.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgColab_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgColab.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtColab = BancoDados.selecionarRegistros(Principal.strOrdem + " ORDER BY " + dgColab.Columns[e.ColumnIndex].Name + " DESC");
                        dgColab.DataSource = dtColab;
                        decrescente = true;
                    }
                    else
                    {
                        dtColab = BancoDados.selecionarRegistros(Principal.strOrdem + " ORDER BY " + dgColab.Columns[e.ColumnIndex].Name + " ASC");
                        dgColab.DataSource = dtColab;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtColab, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgColab_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgColab.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgColab.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgColab.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void dgColab_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtColab.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtColab.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtColab.Rows.Count != 1 && contRegistros > 0)
                {
                    contRegistros = contRegistros - 1;
                    CarregarDados(contRegistros);
                }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    if (dgColab.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgColab.ColumnCount];
                string[] coluna = new string[dgColab.ColumnCount];

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    grid[i] = dgColab.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgColab.ColumnCount; i++)
                {
                    if (dgColab.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgColab.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgColab.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgColab.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgColab.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgColab.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgColab.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgColab.ColumnCount; i++)
                        {
                            dgColab.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgColab.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgColab);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }
                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgColab.RowCount;
                    for (i = 0; i <= dgColab.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgColab[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgColab.ColumnCount; k++)
                    {
                        if (dgColab.Columns[k].Visible == true)
                        {
                            switch (dgColab.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgColab.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgColab.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgColab.ColumnCount : indice) + Convert.ToString(dgColab.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
