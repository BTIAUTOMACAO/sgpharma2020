﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace GestaoAdministrativa.FPopular
{
    public partial class frmCadPrecosFP : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIAVEIS
        private DataTable dtFarmaciaPopular = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbFarmaciaPopular = new ToolStripButton("Preços FP");
        private ToolStripSeparator tssFarmaciaPopular = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        #endregion

        public frmCadPrecosFP(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtData") && (control.Name != "txtUsuario") && (control.Name != "txtPreCompra") && (control.Name != "txtUltCusto")
                    && (control.Name != "txtQdeUni") && (control.Name != "txtPreValor"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
        private void txtBCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbLiberado.Focus();
        }

        private void frmCadPrecosFP_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmCadPrecosFP");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, contRegistros));
            if (tcClasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcClasse.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtCodBarras.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbFarmaciaPopular.AutoSize = false;
                this.tsbFarmaciaPopular.Image = Properties.Resources.cadastro;
                this.tsbFarmaciaPopular.Size = new System.Drawing.Size(125, 20);
                this.tsbFarmaciaPopular.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbFarmaciaPopular);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssFarmaciaPopular);
                tsbFarmaciaPopular.Click += delegate
                {
                    var cadPrecosFP = Application.OpenForms.OfType<frmCadPrecosFP>().FirstOrDefault();
                    BotoesHabilitados();
                    cadPrecosFP.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtFarmaciaPopular.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbFarmaciaPopular);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssFarmaciaPopular);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var fp = new FProdutosPrecos();

                dtFarmaciaPopular = fp.BuscaDados(txtBCodBarras.Text, cmbLiberado.Text, out strOrdem);
                if (dtFarmaciaPopular.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtFarmaciaPopular.Rows.Count;
                    dgPrecos.DataSource = dtFarmaciaPopular;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, 0));
                    dgPrecos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPrecos.DataSource = dtFarmaciaPopular;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBCodBarras.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Empresa: " + Principal.empAtual + " - " + Principal.nomeAtual;
                txtCodBarras.Text = dtFarmaciaPopular.Rows[linha]["PROD_CODIGO"].ToString();
                txtDescr.Text = Funcoes.ChecaCampoVazio(dtFarmaciaPopular.Rows[linha]["PROD_DESCR"].ToString());
                if (dtFarmaciaPopular.Rows[linha]["HABILITADO"].ToString() == "N")
                {
                    chkLiberado.Checked = false;
                }
                else
                    chkLiberado.Checked = true;

                txtPreCompra.Text = String.Format("{0:N}", dtFarmaciaPopular.Rows[linha]["PROD_PRECOMPRA"]);
                txtUltCusto.Text = String.Format("{0:N}", dtFarmaciaPopular.Rows[linha]["PROD_ULTCUSME"]);
                txtQdeUni.Text = Funcoes.ChecaCampoVazio(dtFarmaciaPopular.Rows[linha]["PROD_QTDE_UN_COMP_FP"].ToString());
                txtPreValor.Text = String.Format("{0:N}", dtFarmaciaPopular.Rows[linha]["PRE_VALOR"]);
                txtPrecoFP.Text = String.Format("{0:N}", dtFarmaciaPopular.Rows[linha]["PRE_COM_DESC"]);
                txtData.Text = Funcoes.ChecaCampoVazio(dtFarmaciaPopular.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtFarmaciaPopular.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, linha));
                txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcClasse.SelectedTab == tpGrade)
            {
                dgPrecos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
               if (tcClasse.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCadPrecosFP");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, contRegistros));
                    chkLiberado.Checked = false;
                    emGrade = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtCodBarras.Focus();
                }
            }
        }

        private void dgPrecos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, contRegistros));
            emGrade = true;
        }

        private void dgPrecos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcClasse.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        public void LimparGrade()
        {

            dtFarmaciaPopular.Clear();
            dgPrecos.DataSource = dtFarmaciaPopular;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            txtBCodBarras.Focus();
            cmbLiberado.SelectedIndex = 0;
        }

        public void LimparFicha()
        {
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtPreCompra.Text = "0";
            txtUltCusto.Text = "0";
            txtQdeUni.Text = "0";
            txtPreValor.Text = "0";
            txtPrecoFP.Text = "0";
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtCodBarras.Focus();

        }

        private void dgPrecos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtFarmaciaPopular.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtFarmaciaPopular.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtFarmaciaPopular.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgPrecos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgPrecos.Rows[e.RowIndex].Cells[2].Value.ToString() == "N")
            {
                dgPrecos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgPrecos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        public void Primeiro()
        {
            if (dtFarmaciaPopular.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgPrecos.CurrentCell = dgPrecos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcClasse.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtFarmaciaPopular.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrecos.CurrentCell = dgPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrecos.CurrentCell = dgPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPrecos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPrecos.CurrentCell = dgPrecos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    var fpPrecos = new FProdutosPrecos(
                       Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true, false)),
                       txtCodBarras.Text,
                       Convert.ToDouble(txtPrecoFP.Text),
                       chkLiberado.Checked ? "S" : "N",
                       Principal.usuario,
                       DateTime.Now,
                       Principal.usuario,
                       DateTime.Now
                   );



                    Principal.dtBusca = Util.RegistrosPorEmpresa("FP_PRODUTOS_PRECOS", "PROD_CODIGO", txtCodBarras.Text,false,true);

                    if (fpPrecos.AtualizaDados(fpPrecos, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Preços Farmácia Popular");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtFarmaciaPopular.Clear();
                        dgPrecos.DataSource = dtFarmaciaPopular;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        return true;
                    }
                    else
                    {

                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtPrecoFP.Text.Trim()))
            {
                Principal.mensagem = "Preço FP não pode ser em branco.";
                Funcoes.Avisa();
                txtPrecoFP.Focus();
                return false;
            }

            if (Convert.ToDouble(txtPrecoFP.Text) == 0)
            {
                Principal.mensagem = "Preço FP não pode ser Zero.";
                Funcoes.Avisa();
                txtPrecoFP.Focus();
                return false;
            }

            return true;
        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos())
                {
                    Cursor = Cursors.WaitCursor;
                    chkLiberado.Checked = true;

                    var fpPrecos = new FProdutosPrecos(
                        Convert.ToInt32(Util.SelecionaCampoEspecificoDaTabela("PRODUTOS", "PROD_ID", "PROD_CODIGO", txtCodBarras.Text, false, true, false)),
                        txtCodBarras.Text,
                        Convert.ToDouble(txtPrecoFP.Text),
                        chkLiberado.Checked ? "S" : "N",
                        Principal.usuario,
                        DateTime.Now,
                        Principal.usuario,
                        DateTime.Now
                    );

                    Principal.dtPesq = Util.RegistrosPorEmpresa("FP_PRODUTOS_PRECOS", "PROD_CODIGO", txtCodBarras.Text.ToUpper(),false, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Cursor = Cursors.Default;
                        Principal.mensagem = "Produto já cadastrado.";
                        Funcoes.Avisa();
                        txtDescr.Text = "";
                        txtDescr.Focus();
                        return false;
                    }
                    else
                    {
                        if (fpPrecos.InsereRegistros(fpPrecos).Equals(true))
                        {
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("PROD_CODIGO", txtCodBarras.Text, Principal.usuario, "FP_PRODUTOS_PRECOS", txtPrecoFP.Text, Principal.estAtual,Principal.empAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtFarmaciaPopular.Clear();
                            dgPrecos.DataSource = dtFarmaciaPopular;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Erro: ao efetuar o cadastro", "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Funcoes.Avisa();
                            txtDescr.Focus();
                            return false;
                        }
                    }
                }
                chkLiberado.Checked = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if ((txtCodBarras.Text.Trim() != "") && (txtPrecoFP.Text.Trim() != ""))
                {
                    if (MessageBox.Show("Confirma a exclusão do produto?", "Exclusão tabela Classes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            var preco = new FProdutosPrecos();
                            if (!preco.ExcluiProduto(txtCodBarras.Text).Equals(-1))
                            {
                                Funcoes.GravaLogExclusao("PROD_CODIGO", txtCodBarras.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "FP_PRODUTOS_PRECOS", txtDescr.Text, Principal.motivo, Principal.estAtual);
                                dtFarmaciaPopular.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgPrecos.DataSource = dtFarmaciaPopular;
                                tcClasse.SelectedTab = tpGrade;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBCodBarras.Focus();
                                cmbLiberado.SelectedIndex = 0;
                                return true;
                            
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcClasse.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            tcClasse.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcClasse.SelectedTab = tpFicha;
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (LeProdutosCod().Equals(false))
                    {
                        txtCodBarras.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool LeProdutosCod()
        {
            var dtLePrdutos = new DataTable();
            Cursor = Cursors.WaitCursor;

            var buscaProduto = new Produto();
            dtLePrdutos = buscaProduto.BuscaProdutosFP(Principal.estAtual, Principal.empAtual, txtCodBarras.Text);
            if (dtLePrdutos.Rows.Count != 0)
            {
                txtCodBarras.Text = dtLePrdutos.Rows[0]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtLePrdutos.Rows[0]["PROD_DESCR"].ToString();
                txtPreCompra.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_PRECOMPRA"]);
                txtUltCusto.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PROD_ULTCUSME"]);
                txtQdeUni.Text = dtLePrdutos.Rows[0]["PROD_QTDE_UN_COMP_FP"].ToString();
                txtPreValor.Text = String.Format("{0:N}", dtLePrdutos.Rows[0]["PRE_VALOR"]);
                txtPrecoFP.Focus();
            }
            else
            {
                Cursor = Cursors.Default;
                MessageBox.Show("Código não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescr.Text = "Produto não cadastrado";
                txtCodBarras.Focus();
                return false;
            }

            Cursor = Cursors.Default;
            return true;
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtDescr.Text.Trim() != "")
                    {
                        var buscaProdDescricao = new Produto();
                        using (DataTable dtBusca = buscaProdDescricao.BuscaProdutosDescricaoLike(txtDescr.Text.ToUpper()))
                        {
                            if (dtBusca.Rows.Count == 1)
                            {
                                txtCodBarras.Text = dtBusca.Rows[0]["PROD_CODIGO"].ToString();
                                LeProdutosCod();
                            }
                            else if (dtBusca.Rows.Count == 0)
                            {
                                MessageBox.Show("Não foi encontrado nenhum produto contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtDescr.Text = "";
                                txtDescr.Focus();
                            }
                            else
                            {
                                using (var buscaProd = new frmBuscaProd(false))
                                {
                                    buscaProd.BuscaProd(txtDescr.Text.ToUpper());
                                    buscaProd.ShowDialog();
                                }
                                if (!String.IsNullOrEmpty(Principal.codBarra))
                                {
                                    txtCodBarras.Text = Principal.codBarra;
                                    txtDescr.Text = Principal.prodDescr;
                                    LeProdutosCod();
                                    Principal.codBarra = "";
                                    Principal.prodDescr = "";
                                }
                                else
                                {
                                    txtDescr.Text = "";
                                    txtDescr.Focus();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {

            try
            {
                int colunas = 1;

                for (int i = 0; i < dgPrecos.ColumnCount; i++)
                {
                    if (dgPrecos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgPrecos.ColumnCount];
                string[] coluna = new string[dgPrecos.ColumnCount];

                for (int i = 0; i < dgPrecos.ColumnCount; i++)
                {
                    grid[i] = dgPrecos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgPrecos.ColumnCount; i++)
                {
                    if (dgPrecos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgPrecos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgPrecos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgPrecos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgPrecos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgPrecos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgPrecos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgPrecos.ColumnCount; i++)
                        {
                            dgPrecos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPrecos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgPrecos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgPrecos.RowCount;
                    for (i = 0; i <= dgPrecos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgPrecos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgPrecos.ColumnCount; k++)
                    {
                        if (dgPrecos.Columns[k].Visible == true)
                        {
                            switch (dgPrecos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgPrecos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPrecos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgPrecos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPrecos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgPrecos.ColumnCount : indice) + Convert.ToString(dgPrecos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void dgPrecos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgPrecos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtFarmaciaPopular = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPrecos.Columns[e.ColumnIndex].Name + " DESC");
                        dgPrecos.DataSource = dtFarmaciaPopular;
                        decrescente = true;
                    }
                    else
                    {
                        dtFarmaciaPopular = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPrecos.Columns[e.ColumnIndex].Name + " ASC");
                        dgPrecos.DataSource = dtFarmaciaPopular;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtFarmaciaPopular, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Preços Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
