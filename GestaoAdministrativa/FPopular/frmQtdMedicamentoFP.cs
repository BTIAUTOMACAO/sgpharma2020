﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Vendas;

namespace GestaoAdministrativa.FPopular
{
    public partial class frmQtdMedicamentoFP : Form
    {
        public bool fpValidou;
        DataTable dtMedicamentos;
        int i;

        public frmQtdMedicamentoFP(int indice, DataTable medicamentos)
        {
            i = indice;
            dtMedicamentos = medicamentos;
            InitializeComponent();
        }

        private void frmQtdMedicamentoFP_Load(object sender, EventArgs e)
        {
            try
            {
                txtNomeMedicamento.Text = dtMedicamentos.Rows[i][1].ToString();
                txtPreco.Text = dtMedicamentos.Rows[i][4].ToString();
                txtQtdSolicitada.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                fpValidou = false;
                if(String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtQtdSolicitada.Text)))
                {
                    MessageBox.Show("A quantidade solicitada pelo cliente não pode estar em branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fpValidou = false;
                    txtQtdSolicitada.Focus();
                }
                else if (String.IsNullOrEmpty(txtPosologia.Text.Trim()))
                {
                    MessageBox.Show("Informe a quantidade diária prescrita.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fpValidou = false;
                    txtQtdSolicitada.Focus();
                }
                else if (String.IsNullOrEmpty(txtPreco.Text.Trim()))
                {
                    MessageBox.Show("Informe o preço do medicamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fpValidou = false;
                    txtQtdSolicitada.Focus();
                }
                else
                {
                    fpValidou = true;
                    dtMedicamentos.Rows[i][2] = txtQtdSolicitada.Text;
                    dtMedicamentos.Rows[i][3] = txtPosologia.Text;
                    dtMedicamentos.Rows[i][4] = txtPreco.Text;
                    this.Hide();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmácia Popular", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void frmFPopVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtIDVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtPosologia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();
        }

        private void txtQtdSolicitada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirmar.PerformClick();
        }
    }
}
