﻿namespace GestaoAdministrativa.SNGPC
{
    partial class frmSngpcPerda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSngpcPerda));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcPerda = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBReg = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtBCodBarra = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbBMotivo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgPerda = new System.Windows.Forms.DataGridView();
            this.PER_SEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOTIVO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PER_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PER_MS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PER_MED_LOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PER_MED_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_UNIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CLASSE_TERAP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPerda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbLote = new System.Windows.Forms.ComboBox();
            this.txtQtdeEstoque = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbUnidade = new System.Windows.Forms.ComboBox();
            this.cmbClasseTera = new System.Windows.Forms.ComboBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.txtLote = new System.Windows.Forms.TextBox();
            this.txtReg = new System.Windows.Forms.TextBox();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.lblEstab = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.imgLote = new System.Windows.Forms.PictureBox();
            this.lblLote = new System.Windows.Forms.Label();
            this.cmbMotivo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtPerdaID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcPerda.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPerda)).BeginInit();
            this.cmsPerda.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcPerda);
            this.groupBox1.Location = new System.Drawing.Point(9, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(179, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Perda de Medicamentos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(970, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcPerda
            // 
            this.tcPerda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcPerda.Controls.Add(this.tpGrade);
            this.tcPerda.Controls.Add(this.tpFicha);
            this.tcPerda.Location = new System.Drawing.Point(6, 33);
            this.tcPerda.Name = "tcPerda";
            this.tcPerda.SelectedIndex = 0;
            this.tcPerda.Size = new System.Drawing.Size(967, 499);
            this.tcPerda.TabIndex = 40;
            this.tcPerda.SelectedIndexChanged += new System.EventHandler(this.tcPerda_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBReg);
            this.tpGrade.Controls.Add(this.label37);
            this.tpGrade.Controls.Add(this.txtBCodBarra);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.cmbBMotivo);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.dgPerda);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(959, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            this.tpGrade.Click += new System.EventHandler(this.tpGrade_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(728, 410);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 147;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBReg
            // 
            this.txtBReg.Location = new System.Drawing.Point(503, 426);
            this.txtBReg.MaxLength = 13;
            this.txtBReg.Name = "txtBReg";
            this.txtBReg.Size = new System.Drawing.Size(204, 22);
            this.txtBReg.TabIndex = 146;
            this.txtBReg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBReg_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Navy;
            this.label37.Location = new System.Drawing.Point(500, 408);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(113, 16);
            this.label37.TabIndex = 145;
            this.label37.Text = "Reg. Min. Saúde";
            // 
            // txtBCodBarra
            // 
            this.txtBCodBarra.Location = new System.Drawing.Point(324, 426);
            this.txtBCodBarra.MaxLength = 13;
            this.txtBCodBarra.Name = "txtBCodBarra";
            this.txtBCodBarra.Size = new System.Drawing.Size(164, 22);
            this.txtBCodBarra.TabIndex = 144;
            this.txtBCodBarra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCodBarra_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(321, 408);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 16);
            this.label15.TabIndex = 143;
            this.label15.Text = "Cód. de Barra";
            // 
            // cmbBMotivo
            // 
            this.cmbBMotivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBMotivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBMotivo.FormattingEnabled = true;
            this.cmbBMotivo.Location = new System.Drawing.Point(6, 424);
            this.cmbBMotivo.Name = "cmbBMotivo";
            this.cmbBMotivo.Size = new System.Drawing.Size(301, 24);
            this.cmbBMotivo.TabIndex = 142;
            this.cmbBMotivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbBMotivo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 408);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 16);
            this.label1.TabIndex = 141;
            this.label1.Text = "Motivo";
            // 
            // dgPerda
            // 
            this.dgPerda.AllowUserToAddRows = false;
            this.dgPerda.AllowUserToDeleteRows = false;
            this.dgPerda.AllowUserToOrderColumns = true;
            this.dgPerda.AllowUserToResizeColumns = false;
            this.dgPerda.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPerda.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPerda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPerda.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPerda.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgPerda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPerda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PER_SEQ,
            this.PROD_DESCR,
            this.MOTIVO,
            this.PER_DATA,
            this.PROD_CODIGO,
            this.PER_MS,
            this.PER_MED_LOTE,
            this.PER_MED_QTDE,
            this.PROD_UNIDADE,
            this.PROD_CLASSE_TERAP,
            this.DT_ALTERACAO,
            this.OP_ALTERACAO,
            this.DT_CADASTRO,
            this.OP_CADASTRO});
            this.dgPerda.ContextMenuStrip = this.cmsPerda;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPerda.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgPerda.GridColor = System.Drawing.Color.LightGray;
            this.dgPerda.Location = new System.Drawing.Point(6, 6);
            this.dgPerda.MultiSelect = false;
            this.dgPerda.Name = "dgPerda";
            this.dgPerda.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPerda.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgPerda.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPerda.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgPerda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPerda.Size = new System.Drawing.Size(947, 394);
            this.dgPerda.TabIndex = 1;
            this.dgPerda.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPerda_CellMouseClick);
            this.dgPerda.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPerda_CellMouseDoubleClick);
            this.dgPerda.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgPerda_ColumnHeaderMouseClick);
            this.dgPerda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPerda_KeyDown);
            // 
            // PER_SEQ
            // 
            this.PER_SEQ.DataPropertyName = "PER_SEQ";
            this.PER_SEQ.HeaderText = "Perda ID";
            this.PER_SEQ.MaxInputLength = 18;
            this.PER_SEQ.Name = "PER_SEQ";
            this.PER_SEQ.ReadOnly = true;
            this.PER_SEQ.Width = 90;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.Width = 190;
            // 
            // MOTIVO
            // 
            this.MOTIVO.DataPropertyName = "MOTIVO";
            this.MOTIVO.HeaderText = "Motivo";
            this.MOTIVO.MaxInputLength = 100;
            this.MOTIVO.Name = "MOTIVO";
            this.MOTIVO.ReadOnly = true;
            this.MOTIVO.Width = 250;
            // 
            // PER_DATA
            // 
            this.PER_DATA.DataPropertyName = "PER_DATA";
            this.PER_DATA.HeaderText = "Data";
            this.PER_DATA.MaxInputLength = 10;
            this.PER_DATA.Name = "PER_DATA";
            this.PER_DATA.ReadOnly = true;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. de Barras";
            this.PROD_CODIGO.MaxInputLength = 18;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 150;
            // 
            // PER_MS
            // 
            this.PER_MS.DataPropertyName = "PER_MS";
            this.PER_MS.HeaderText = "Reg. Min. da Saúde";
            this.PER_MS.MaxInputLength = 13;
            this.PER_MS.Name = "PER_MS";
            this.PER_MS.ReadOnly = true;
            this.PER_MS.Width = 150;
            // 
            // PER_MED_LOTE
            // 
            this.PER_MED_LOTE.DataPropertyName = "PER_MED_LOTE";
            this.PER_MED_LOTE.HeaderText = "Lote";
            this.PER_MED_LOTE.MaxInputLength = 10;
            this.PER_MED_LOTE.Name = "PER_MED_LOTE";
            this.PER_MED_LOTE.ReadOnly = true;
            this.PER_MED_LOTE.Width = 75;
            // 
            // PER_MED_QTDE
            // 
            this.PER_MED_QTDE.DataPropertyName = "PER_MED_QTDE";
            this.PER_MED_QTDE.HeaderText = "Qtde.";
            this.PER_MED_QTDE.MaxInputLength = 3;
            this.PER_MED_QTDE.Name = "PER_MED_QTDE";
            this.PER_MED_QTDE.ReadOnly = true;
            this.PER_MED_QTDE.Width = 75;
            // 
            // PROD_UNIDADE
            // 
            this.PROD_UNIDADE.DataPropertyName = "PROD_UNIDADE";
            this.PROD_UNIDADE.HeaderText = "Unidade";
            this.PROD_UNIDADE.Name = "PROD_UNIDADE";
            this.PROD_UNIDADE.ReadOnly = true;
            this.PROD_UNIDADE.Width = 80;
            // 
            // PROD_CLASSE_TERAP
            // 
            this.PROD_CLASSE_TERAP.DataPropertyName = "PROD_CLASSE_TERAP";
            this.PROD_CLASSE_TERAP.HeaderText = "Classes Terapêuticas";
            this.PROD_CLASSE_TERAP.Name = "PROD_CLASSE_TERAP";
            this.PROD_CLASSE_TERAP.ReadOnly = true;
            this.PROD_CLASSE_TERAP.Width = 170;
            // 
            // DT_ALTERACAO
            // 
            this.DT_ALTERACAO.DataPropertyName = "DT_ALTERACAO";
            dataGridViewCellStyle3.Format = "G";
            this.DT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle3;
            this.DT_ALTERACAO.HeaderText = "Data Alteração";
            this.DT_ALTERACAO.MaxInputLength = 20;
            this.DT_ALTERACAO.Name = "DT_ALTERACAO";
            this.DT_ALTERACAO.ReadOnly = true;
            this.DT_ALTERACAO.Width = 150;
            // 
            // OP_ALTERACAO
            // 
            this.OP_ALTERACAO.DataPropertyName = "OP_ALTERACAO";
            this.OP_ALTERACAO.HeaderText = "Usuário Alteração";
            this.OP_ALTERACAO.MaxInputLength = 25;
            this.OP_ALTERACAO.Name = "OP_ALTERACAO";
            this.OP_ALTERACAO.ReadOnly = true;
            this.OP_ALTERACAO.Width = 150;
            // 
            // DT_CADASTRO
            // 
            this.DT_CADASTRO.DataPropertyName = "DT_CADASTRO";
            dataGridViewCellStyle4.Format = "G";
            this.DT_CADASTRO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DT_CADASTRO.HeaderText = "Data do Cadastro";
            this.DT_CADASTRO.MaxInputLength = 20;
            this.DT_CADASTRO.Name = "DT_CADASTRO";
            this.DT_CADASTRO.ReadOnly = true;
            this.DT_CADASTRO.Width = 150;
            // 
            // OP_CADASTRO
            // 
            this.OP_CADASTRO.DataPropertyName = "OP_CADASTRO";
            this.OP_CADASTRO.HeaderText = "Usuário Cadastro";
            this.OP_CADASTRO.MaxInputLength = 25;
            this.OP_CADASTRO.Name = "OP_CADASTRO";
            this.OP_CADASTRO.ReadOnly = true;
            this.OP_CADASTRO.Width = 150;
            // 
            // cmsPerda
            // 
            this.cmsPerda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsPerda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.cmsPerda.Name = "cmsPerda";
            this.cmsPerda.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(959, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(945, 92);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.pictureBox11);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.pictureBox12);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(574, 23);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(365, 46);
            this.groupBox6.TabIndex = 197;
            this.groupBox6.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox11.Location = new System.Drawing.Point(173, 21);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.TabIndex = 195;
            this.pictureBox11.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(16, 21);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(16, 16);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox12.TabIndex = 191;
            this.pictureBox12.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 16);
            this.label11.TabIndex = 18;
            this.label11.Text = "Data da última alteração";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(225, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 19;
            this.label12.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.cmbLote);
            this.groupBox2.Controls.Add(this.txtQtdeEstoque);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.pictureBox10);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.cmbUnidade);
            this.groupBox2.Controls.Add(this.cmbClasseTera);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtQtde);
            this.groupBox2.Controls.Add(this.txtLote);
            this.groupBox2.Controls.Add(this.txtReg);
            this.groupBox2.Controls.Add(this.txtDescr);
            this.groupBox2.Controls.Add(this.txtCodBarras);
            this.groupBox2.Controls.Add(this.dtData);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.imgLote);
            this.groupBox2.Controls.Add(this.lblLote);
            this.groupBox2.Controls.Add(this.cmbMotivo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.pictureBox6);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.txtPerdaID);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 295);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // cmbLote
            // 
            this.cmbLote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLote.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cmbLote.FormattingEnabled = true;
            this.cmbLote.Location = new System.Drawing.Point(16, 232);
            this.cmbLote.Name = "cmbLote";
            this.cmbLote.Size = new System.Drawing.Size(215, 24);
            this.cmbLote.TabIndex = 207;
            this.cmbLote.SelectedIndexChanged += new System.EventHandler(this.cmbLote_SelectedIndexChanged);
            // 
            // txtQtdeEstoque
            // 
            this.txtQtdeEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtQtdeEstoque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtdeEstoque.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdeEstoque.Location = new System.Drawing.Point(246, 232);
            this.txtQtdeEstoque.MaxLength = 3;
            this.txtQtdeEstoque.Name = "txtQtdeEstoque";
            this.txtQtdeEstoque.ReadOnly = true;
            this.txtQtdeEstoque.Size = new System.Drawing.Size(114, 22);
            this.txtQtdeEstoque.TabIndex = 166;
            this.txtQtdeEstoque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(244, 212);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 16);
            this.label18.TabIndex = 187;
            this.label18.Text = "Qtde. em Estoque";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(58, 212);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 186;
            this.pictureBox10.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(13, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 16);
            this.label16.TabIndex = 185;
            this.label16.Text = "Lote";
            // 
            // cmbUnidade
            // 
            this.cmbUnidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnidade.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cmbUnidade.FormattingEnabled = true;
            this.cmbUnidade.Location = new System.Drawing.Point(16, 174);
            this.cmbUnidade.Name = "cmbUnidade";
            this.cmbUnidade.Size = new System.Drawing.Size(94, 24);
            this.cmbUnidade.TabIndex = 162;
            this.cmbUnidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUnidade_KeyPress);
            // 
            // cmbClasseTera
            // 
            this.cmbClasseTera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClasseTera.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cmbClasseTera.FormattingEnabled = true;
            this.cmbClasseTera.Items.AddRange(new object[] {
            "",
            "ANTIMICROBIANO",
            "SUJEITO A CONTROLE ESPECIAL"});
            this.cmbClasseTera.Location = new System.Drawing.Point(126, 174);
            this.cmbClasseTera.Name = "cmbClasseTera";
            this.cmbClasseTera.Size = new System.Drawing.Size(353, 24);
            this.cmbClasseTera.TabIndex = 163;
            this.cmbClasseTera.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbClasseTera_KeyPress);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(257, 155);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 181;
            this.pictureBox4.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(123, 155);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 16);
            this.label13.TabIndex = 180;
            this.label13.Text = "Classe Terapeutica";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(80, 155);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 179;
            this.pictureBox9.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(13, 155);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 16);
            this.label14.TabIndex = 178;
            this.label14.Text = "Unidade";
            // 
            // txtQtde
            // 
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(553, 232);
            this.txtQtde.MaxLength = 3;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(114, 22);
            this.txtQtde.TabIndex = 168;
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            // 
            // txtLote
            // 
            this.txtLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLote.Location = new System.Drawing.Point(375, 232);
            this.txtLote.MaxLength = 10;
            this.txtLote.Name = "txtLote";
            this.txtLote.Size = new System.Drawing.Size(165, 22);
            this.txtLote.TabIndex = 167;
            this.txtLote.Visible = false;
            this.txtLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLote_KeyPress);
            // 
            // txtReg
            // 
            this.txtReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReg.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReg.Location = new System.Drawing.Point(494, 174);
            this.txtReg.MaxLength = 13;
            this.txtReg.Name = "txtReg";
            this.txtReg.Size = new System.Drawing.Size(173, 22);
            this.txtReg.TabIndex = 164;
            this.txtReg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReg_KeyPress);
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(228, 71);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(439, 22);
            this.txtDescr.TabIndex = 158;
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(16, 71);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(199, 22);
            this.txtCodBarras.TabIndex = 157;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // dtData
            // 
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(549, 119);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(118, 22);
            this.dtData.TabIndex = 161;
            this.dtData.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 148;
            this.lblEstab.Tag = "";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(649, 212);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 147;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(550, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 16);
            this.label9.TabIndex = 145;
            this.label9.Text = "Qtde. Perdida";
            // 
            // imgLote
            // 
            this.imgLote.Image = ((System.Drawing.Image)(resources.GetObject("imgLote.Image")));
            this.imgLote.Location = new System.Drawing.Point(413, 212);
            this.imgLote.Name = "imgLote";
            this.imgLote.Size = new System.Drawing.Size(16, 16);
            this.imgLote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgLote.TabIndex = 144;
            this.imgLote.TabStop = false;
            this.imgLote.Visible = false;
            this.imgLote.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // lblLote
            // 
            this.lblLote.AutoSize = true;
            this.lblLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLote.ForeColor = System.Drawing.Color.Navy;
            this.lblLote.Location = new System.Drawing.Point(374, 212);
            this.lblLote.Name = "lblLote";
            this.lblLote.Size = new System.Drawing.Size(36, 16);
            this.lblLote.TabIndex = 142;
            this.lblLote.Text = "Lote";
            this.lblLote.Visible = false;
            // 
            // cmbMotivo
            // 
            this.cmbMotivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMotivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMotivo.FormattingEnabled = true;
            this.cmbMotivo.Location = new System.Drawing.Point(126, 119);
            this.cmbMotivo.Name = "cmbMotivo";
            this.cmbMotivo.Size = new System.Drawing.Size(406, 24);
            this.cmbMotivo.TabIndex = 160;
            this.cmbMotivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMotivo_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(123, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 16);
            this.label6.TabIndex = 138;
            this.label6.Text = "Motivo";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(184, 102);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 139;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(630, 157);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 137;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(225, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 132;
            this.label4.Text = "Descrição";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 16);
            this.label2.TabIndex = 129;
            this.label2.Text = "Código";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(72, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 131;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(491, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 16);
            this.label5.TabIndex = 123;
            this.label5.Text = "Reg. Min. da Saúde";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(546, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 111;
            this.label3.Text = "Data";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(589, 102);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 112;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(82, 103);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 110;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // txtPerdaID
            // 
            this.txtPerdaID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPerdaID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPerdaID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPerdaID.Location = new System.Drawing.Point(16, 121);
            this.txtPerdaID.MaxLength = 18;
            this.txtPerdaID.Name = "txtPerdaID";
            this.txtPerdaID.ReadOnly = true;
            this.txtPerdaID.Size = new System.Drawing.Size(94, 22);
            this.txtPerdaID.TabIndex = 159;
            this.txtPerdaID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Perda ID";
            // 
            // frmSngpcPerda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSngpcPerda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmSngpcPerda";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSngpcPerda_Load);
            this.Shown += new System.EventHandler(this.frmSngpcPerda_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcPerda.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPerda)).EndInit();
            this.cmsPerda.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcPerda;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox imgLote;
        private System.Windows.Forms.Label lblLote;
        private System.Windows.Forms.ComboBox cmbMotivo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtPerdaID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBReg;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtBCodBarra;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbBMotivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgPerda;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.TextBox txtLote;
        private System.Windows.Forms.TextBox txtReg;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.ContextMenuStrip cmsPerda;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.DataGridViewTextBoxColumn PER_SEQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOTIVO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PER_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PER_MS;
        private System.Windows.Forms.DataGridViewTextBoxColumn PER_MED_LOTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PER_MED_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CLASSE_TERAP;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CADASTRO;
        private System.Windows.Forms.ComboBox cmbUnidade;
        private System.Windows.Forms.ComboBox cmbClasseTera;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.TextBox txtQtdeEstoque;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbLote;
    }
}