﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelBalancoCompleto : Form
    {
        private frmSngpcRelBalanco relatorio;
        private DataTable dtRelatorio;

        public frmRelBalancoCompleto(frmSngpcRelBalanco frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmRelBalancoCompleto_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            this.reportViewer1.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {

            var dadosEstab = new Estabelecimento();
            List<Estabelecimento> busca = dadosEstab.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);

            ReportParameter[] parametro = new ReportParameter[10];
            parametro[0] = new ReportParameter("razaoSocial", busca[0].EstRazao);
            parametro[1] = new ReportParameter("endereco", busca[0].EstEndereco + " N°: " + busca[0].EstNum);
            parametro[2] = new ReportParameter("trimestral", relatorio.rdb1.Checked ? "( X )" : relatorio.rdb2.Checked ? "( X )" : relatorio.rdb3.Checked ? "( X )" : relatorio.rdb4.Checked ?
                 "( X )" : "(  )");
            parametro[3] = new ReportParameter("estado", Funcoes.ObtemEstado(busca[0].EstUf));
            parametro[4] = new ReportParameter("municipio", busca[0].EstCidade);
            string data = "";
            if(relatorio.rdbAnual.Checked)
            {
                data = "01/01/" + relatorio.txtAno.Text + " A " + "31/12/" + relatorio.txtAno.Text;
            }
            else if(relatorio.rdb1.Checked)
            {
                data = "01/01/" + relatorio.txtAno.Text + " A " + "31/03/" + relatorio.txtAno.Text;
            }
            else if(relatorio.rdb2.Checked)
            {
                data = "01/04/" + relatorio.txtAno.Text + " A " + "30/06/" + relatorio.txtAno.Text;
            }
            else if(relatorio.rdb3.Checked)
            {
                data = "01/07/" + relatorio.txtAno.Text + " A " + "30/09/" + relatorio.txtAno.Text;
            }
            else if(relatorio.rdb4.Checked)
            {
                data = "01/10/" + relatorio.txtAno.Text + " A " + "31/12/" + relatorio.txtAno.Text;
            }

            parametro[5] = new ReportParameter("ano", relatorio.txtAno.Text);
            parametro[6] = new ReportParameter("data", data);
            parametro[7] = new ReportParameter("anual", relatorio.rdbAnual.Checked ? "( X )" : "(  )");
            parametro[8] = new ReportParameter("cnpj", busca[0].EstCNPJ);
            parametro[9] = new ReportParameter("telefone", busca[0].EstFone);
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }
    }
}
