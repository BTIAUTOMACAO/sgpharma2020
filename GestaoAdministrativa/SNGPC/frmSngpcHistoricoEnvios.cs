﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcHistoricoEnvios : Form, Botoes
    {
        //private MDIPrincipal mDIPrincipal;
        private ToolStripButton tsbHistoricoEnvios = new ToolStripButton("Histórico Envios");
        private ToolStripSeparator tssHistoricoEnvios = new ToolStripSeparator();

        public frmSngpcHistoricoEnvios(MDIPrincipal menu)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            Principal.mdiPrincipal = menu;
        }
        public void Botao()
        {
            try
            {
                this.tsbHistoricoEnvios.AutoSize = false;
                this.tsbHistoricoEnvios.Image = Properties.Resources.sngpc;
                this.tsbHistoricoEnvios.Size = new System.Drawing.Size(125, 20);
                this.tsbHistoricoEnvios.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbHistoricoEnvios);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbHistoricoEnvios);
                tsbHistoricoEnvios.Click += delegate
                {
                    var cadHistoricoEnvios = Application.OpenForms.OfType<frmSngpcHistoricoEnvios>().FirstOrDefault();
                    Util.BotoesGenericos();
                    cadHistoricoEnvios.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Histórico de Envios SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcHistoricoEnvios_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                txtMensagemDeErro.Text = "";
                SNGPCRelatorios relatorio = new SNGPCRelatorios();
                VerificaPendentes();
                DataTable dtRelatorio = relatorio.BuscaEnviosPorTecnicos(Convert.ToInt32(cmbTecnico.SelectedValue), "TODOS");
                dgHistorio.DataSource = dtRelatorio;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Histório de Envio SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Histório de Envio SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbHistoricoEnvios);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbHistoricoEnvios);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Histório de Envio SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcHistoricoEnvios_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM SNGPC_TECNICOS WHERE TEC_DESABILITADO = 'N' ");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Não há nenhum responsável técnico cadastrado. Não é possível continuar com a transferência.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbTecnico.DataSource = Principal.dtPesq;
                    cmbTecnico.DisplayMember = "TEC_NOME";
                    cmbTecnico.ValueMember = "TEC_CODIGO";
                    cmbTecnico.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Histório de Envio SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            SNGPCRelatorios relatorio = new SNGPCRelatorios();
            VerificaPendentes();
            DataTable dtRelatorio = relatorio.BuscaEnviosPorTecnicos(Convert.ToInt32(cmbTecnico.SelectedValue), "TODOS");
            dgHistorio.DataSource = dtRelatorio;

        }

        public bool VerificaPendentes()
        {

            SNGPCTransferencia trans = new SNGPCTransferencia();
            DataTable dtEnvios = trans.BuscaPorStatus("P");

            WSAnvisa.sngpcSoapClient wSs = new WSAnvisa.sngpcSoapClient();

            if (dtEnvios.Rows.Count != 0)
            {
                string retornoAnvisa = wSs.ConsultaDadosArquivoSNGPC(dtEnvios.Rows[0]["TEC_LOGIN"].ToString(), dtEnvios.Rows[0]["TEC_SENHA"].ToString(), Funcoes.RemoveCaracter(dtEnvios.Rows[0]["EST_CGC"].ToString()), dtEnvios.Rows[0]["HASH_RETORNO"].ToString());
                string[] palavras = retornoAnvisa.Split(' ');
                if (palavras.Contains("<transmissaoSNGPC>"))
                {

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retornoAnvisa);

                    var node = xmlDoc.GetElementsByTagName("cabecalho").OfType<XmlNode>();
                    XmlNodeList xnResul = xmlDoc.GetElementsByTagName("cabecalho");

                    foreach (XmlNode item in node)
                    {
                        if (item["DATAVALIDACAO"] == null)
                        {
                            return false;
                        }
                        if (item["MENSAGEMVALIDACAO"] != null)
                        {

                            SNGPCEnvios env = new SNGPCEnvios()
                            {
                                HashRetorno = item["CODIGOHASH"].InnerText.ToString(),
                                Aceito = "N",
                                DataInicial = Convert.ToDateTime(item["INICIOREFERENCIA"].InnerText),
                                DataFinal = Convert.ToDateTime(item["FIMREFERENCIA"].InnerText),
                                DataRecebimento = Convert.ToDateTime(item["DATATRANSMISSAO"].InnerText),
                                DataValidacao = Convert.ToDateTime(item["DATAVALIDACAO"].InnerText),
                                MensagemValidacao = item["MENSAGEMVALIDACAO"].InnerText.ToString().Length > 4000 ? item["MENSAGEMVALIDACAO"].InnerText.ToString().Substring(0,3999) :
                                item["MENSAGEMVALIDACAO"].InnerText.ToString(),
                            };
                            env.AtualizaEnvios(env);
                            return true;
                        }
                        else
                        {
                            SNGPCEnvios env = new SNGPCEnvios()
                            {
                                HashRetorno = item["CODIGOHASH"].InnerText.ToString(),
                                Aceito = "S",
                                DataInicial = Convert.ToDateTime(item["INICIOREFERENCIA"].InnerText),
                                DataFinal = Convert.ToDateTime(item["FIMREFERENCIA"].InnerText),
                                DataRecebimento = Convert.ToDateTime(item["DATATRANSMISSAO"].InnerText),
                                DataValidacao = Convert.ToDateTime(item["DATAVALIDACAO"].InnerText),
                                MensagemValidacao = ""
                            };
                            env.AtualizaEnvios(env);
                            return true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(retornoAnvisa, "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            return true;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void dgHistorio_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgHistorio.RowCount > 0)
                {
                    txtMensagemDeErro.Text = dgHistorio.Rows[e.RowIndex].Cells["MENSAGEMVALIDACAO"].Value.ToString();
                }
                else
                {
                    txtMensagemDeErro.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }
    }
}
