﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcTecnicos : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtTecnicos = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTecnicos = new ToolStripButton("Farmacêuticos");
        private ToolStripSeparator tssTecnicos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmSngpcTecnicos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcTecnicos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcTecnicos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, contRegistros));
            if (tcTecnicos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTecnicos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtTecID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTecnicos.AutoSize = false;
                this.tsbTecnicos.Image = Properties.Resources.sngpc;
                this.tsbTecnicos.Size = new System.Drawing.Size(125, 20);
                this.tsbTecnicos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTecnicos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTecnicos);
                tsbTecnicos.Click += delegate
                {
                    var cadTecnicos = Application.OpenForms.OfType<frmSngpcTecnicos>().FirstOrDefault();
                    BotoesHabilitados();
                    cadTecnicos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTecnicos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTecnicos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                txtTecID.Text = dtTecnicos.Rows[linha]["TEC_CODIGO"].ToString();
                txtCpf.Text = dtTecnicos.Rows[linha]["TEC_CPF"].ToString();
                txtNome.Text = dtTecnicos.Rows[linha]["TEC_NOME"].ToString();
                txtLogin.Text = Funcoes.ChecaCampoVazio(dtTecnicos.Rows[linha]["TEC_LOGIN"].ToString());
                if (dtTecnicos.Rows[linha]["TEC_SENHA"].ToString() == "")
                {
                    txtSenha.Text = "";
                }
                else
                    // txtSenha.Text = Funcoes.DescriptografaSenha(dtTecnicos.Rows[linha]["TEC_SENHA"].ToString());
                    txtSenha.Text = dtTecnicos.Rows[linha]["TEC_SENHA"].ToString();

                if (dtTecnicos.Rows[linha]["TEC_DESABILITADO"].ToString() == "S")
                {
                    chkLiberado.Checked = true;
                }
                else
                    chkLiberado.Checked = false;

                txtData.Text = Funcoes.ChecaCampoVazio(dtTecnicos.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTecnicos.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, linha));
                txtNome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTecnicos.Clear();
            dgTecnicos.DataSource = dtTecnicos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtTecID.Text = "";
            txtCpf.Text = "";
            txtNome.Text = "";
            txtLogin.Text = "";
            txtSenha.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtCpf.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTecnicos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            contRegistros = contRegistros - 1;
            dgTecnicos.CurrentCell = dgTecnicos.Rows[contRegistros].Cells[0];
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            contRegistros = contRegistros + 1;
            dgTecnicos.CurrentCell = dgTecnicos.Rows[contRegistros].Cells[0];
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtTecnicos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTecnicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTecnicos.CurrentCell = dgTecnicos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTecnicos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTecnicos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTecnicos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTecnicos.CurrentCell = dgTecnicos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    SngpcTecnico tecnico = new SngpcTecnico()
                    {
                        TecCodigo = Convert.ToInt32(txtTecID.Text),
                        TecCpf = Funcoes.RemoveCaracter(txtCpf.Text),
                        TecNome = txtNome.Text.ToUpper().Trim(),
                        TecLogin = txtLogin.Text.Trim() == "" ? "" : txtLogin.Text,
                        TecSenha = txtSenha.Text,
                        TecLiberado = chkLiberado.Checked == true ? "N" : "S",
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_TECNICOS", "TEC_CODIGO", txtTecID.Text);

                    if (tecnico.AtualizaDados(tecnico, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Farmacêuticos");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTecnicos.Clear();
                        dgTecnicos.DataSource = dtTecnicos;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Excluir()
        {
            try
            {
                if (txtTecID.Text.Trim() != "" || Funcoes.RemoveCaracter(txtCpf.Text) != "")
                {
                    if (MessageBox.Show("Confirma a exclusão do farmacêutico?", "Exclusão tabela SNGPC Técnicos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }

                        SngpcTecnico tecnico = new SngpcTecnico();

                        if(tecnico.ExcluirDados(Convert.ToInt32(txtTecID.Text)).Equals(true))
                        {
                            dtTecnicos.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            MessageBox.Show("Farmacêutico excluido com sucesso!", "Farmacêuticos");
                            dgTecnicos.DataSource = dtTecnicos;
                            emGrade = false;
                            tcTecnicos.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            cmbLiberado.SelectedValue = 0;
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            return true;
                        }
                    }
                    return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtPesq = Util.RegistrosPorEmpresa("SNGPC_TECNICOS", "TEC_LOGIN",txtNome.Text.Trim().ToUpper(), false, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Principal.mensagem = "Farmacêutico já cadastrada.";
                        Funcoes.Avisa();
                        return false;
                    }
                    else
                    {

                        SngpcTecnico tecnico = new SngpcTecnico()
                        {
                            TecCodigo = Funcoes.IdentificaVerificaID("SNGPC_TECNICOS", "TEC_CODIGO", 0, string.Empty),
                            TecCpf = Funcoes.RemoveCaracter(txtCpf.Text),
                            TecNome = txtNome.Text.ToUpper().Trim(),
                            TecLogin = txtLogin.Text.Trim() == "" ? "" : txtLogin.Text,
                            TecSenha = txtSenha.Text,
                            TecLiberado = chkLiberado.Checked == true ? "N" : "S",
                        };

                        if (tecnico.InsereDados(tecnico).Equals(true))
                        {
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            MessageBox.Show("Cadastrado realizada com sucesso!", "Colaboradores");
                            dtTecnicos.Clear();
                            dgTecnicos.DataSource = dtTecnicos;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;

                        }
                        else
                        {
                            MessageBox.Show("Erro: Ao efetuar o Cadastro ", "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                    }
                }
                Limpar();
                chkLiberado.Checked = true;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCpf.Text)))
            {
                Principal.mensagem = "CPF não pode ser em branco.";
                Funcoes.Avisa();
                txtCpf.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome do usuário não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtLogin.Text.Trim()))
            {
                Principal.mensagem = "Login não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtSenha.Text.Trim()))
            {
                Principal.mensagem = "Senha não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            return true;
        }

        private void tcTecnicos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTecnicos.SelectedTab == tpGrade)
            {
                dgTecnicos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTecnicos.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcTecnicos");
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, contRegistros));
                    chkLiberado.Checked = true;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtCpf.Focus();
                    emGrade = false;
                }
            }
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtLogin.Focus();
        }

        private void txtLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtSenha.Focus();
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBCpf.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtBCpf.Text)))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SngpcTecnico tecnicos = new SngpcTecnico()
                {
                    TecCodigo = txtBID.Text.Trim() == "" ? 0 : Convert.ToInt16(txtBID.Text.Trim()),
                    TecCpf = Funcoes.RemoveCaracter(txtBCpf.Text),
                    TecLiberado = cmbLiberado.Text
                };
                dtTecnicos = tecnicos.BuscarTecnico(tecnicos, out strOrdem);
                if (dtTecnicos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTecnicos.Rows.Count;
                    dgTecnicos.DataSource = dtTecnicos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, 0));
                    dgTecnicos.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTecnicos.DataSource = dtTecnicos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                    cmbLiberado.SelectedIndex = 0;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgTecnicos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, contRegistros));
            emGrade = true;
        }

        private void dgTecnicos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTecnicos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTecnicos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTecnicos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTecnicos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTecnicos.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgTecnicos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTecnicos.Rows[e.RowIndex].Cells[4].Value.ToString() == "N")
            {
                dgTecnicos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTecnicos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTecnicos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTecnicos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTecnicos.RowCount;
                    for (i = 0; i <= dgTecnicos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTecnicos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTecnicos.ColumnCount; k++)
                    {
                        if (dgTecnicos.Columns[k].Visible == true)
                        {
                            switch (dgTecnicos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTecnicos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTecnicos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTecnicos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTecnicos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTecnicos.ColumnCount : indice) + Convert.ToString(dgTecnicos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgTecnicos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTecnicos.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtTecnicos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTecnicos.Columns[e.ColumnIndex].Name + " DESC");
                        dgTecnicos.DataSource = dtTecnicos;
                        decrescente = true;
                    }
                    else
                    {
                        dtTecnicos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTecnicos.Columns[e.ColumnIndex].Name + " ASC");
                        dgTecnicos.DataSource = dtTecnicos;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTecnicos, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void txtCpf_Validated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtCpf.Text)))
            {
                if (Funcoes.ValidaCpf(txtCpf.Text).Equals(false))
                {
                    MessageBox.Show("CPF incorreto! Digite novamente.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCpf.Focus();
                }
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtTecID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTecnicos.ColumnCount; i++)
                {
                    if (dgTecnicos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTecnicos.ColumnCount];
                string[] coluna = new string[dgTecnicos.ColumnCount];

                for (int i = 0; i < dgTecnicos.ColumnCount; i++)
                {
                    grid[i] = dgTecnicos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTecnicos.ColumnCount; i++)
                {
                    if (dgTecnicos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTecnicos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTecnicos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTecnicos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTecnicos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTecnicos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTecnicos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTecnicos.ColumnCount; i++)
                        {
                            dgTecnicos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Farmacêuticos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTecnicos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTecnicos.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
