﻿namespace GestaoAdministrativa.SNGPC.Relatorios
{
    partial class frmRelatorioPerda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpwRelatorioPerda = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwRelatorioPerda
            // 
            this.rpwRelatorioPerda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwRelatorioPerda.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.SNGPC.Relatorios.RelatorioPerda.rdlc";
            this.rpwRelatorioPerda.Location = new System.Drawing.Point(0, 0);
            this.rpwRelatorioPerda.Name = "rpwRelatorioPerda";
            this.rpwRelatorioPerda.Size = new System.Drawing.Size(994, 578);
            this.rpwRelatorioPerda.TabIndex = 0;
            // 
            // frmRelatorioPerda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwRelatorioPerda);
            this.Name = "frmRelatorioPerda";
            this.Text = "Relatório de Perda de Medicamentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRelatorioPerda_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwRelatorioPerda;
    }
}