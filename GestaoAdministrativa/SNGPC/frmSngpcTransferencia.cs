﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.Collections;
using System.Xml;
using System.IO;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcTransferencia : Form, Botoes
    {
        private ToolStripButton tsbTransferencia = new ToolStripButton("Transferência SNGPC");
        private ToolStripSeparator tssTransferencia = new ToolStripSeparator();
        private string sXml;

        public frmSngpcTransferencia(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmSngpcTransferencia_Load(object sender, EventArgs e)
        {
            try
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                this.WindowState = FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTransferencia.AutoSize = false;
                this.tsbTransferencia.Image = Properties.Resources.sngpc;
                this.tsbTransferencia.Size = new System.Drawing.Size(160, 20);
                this.tsbTransferencia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTransferencia);
                tsbTransferencia.Click += delegate
                {
                    var cadInventario = Application.OpenForms.OfType<frmSngpcTransferencia>().FirstOrDefault();
                    Util.BotoesGenericos();
                    cadInventario.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTransferencia);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTransferencia);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcTransferencia_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM SNGPC_TECNICOS WHERE TEC_DESABILITADO = 'N' ");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Não há nenhum responsável técnico cadastrado. Não é possível continuar com a transferência.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbTecnico.DataSource = Principal.dtPesq;
                    cmbTecnico.DisplayMember = "TEC_NOME";
                    cmbTecnico.ValueMember = "TEC_CODIGO";
                    cmbTecnico.SelectedIndex = 0;

                    dtFinal.Value = DateTime.Now;
                    SNGPCTransferencia trans = new SNGPCTransferencia();

                    if (!Funcoes.LeParametro(11, "01", true).Equals(String.Empty))
                    {
                        dtInicial.Value = Convert.ToDateTime(Funcoes.LeParametro(11, "01", true));
                    }

                    if (dtInicial.Value.AddDays(7) > DateTime.Now)
                    {
                        dtFinal.Value = DateTime.Now;
                    }
                    else
                    {
                        dtFinal.Value = dtInicial.Value.AddDays(6);
                    }

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        public void Ultimo()
        {
        }

        public void Proximo()
        {
        }

        public void Anterior()
        {
        }

        public bool Incluir()
        {
            return false;
        }

        public bool Excluir()
        {
            return false;
        }

        public bool Atualiza()
        {
            return false;
        }

        public void ImprimirRelatorio()
        {
        }

        public void Limpar()
        {
            gbPeriodo.Visible = false;
            if (!Funcoes.LeParametro(11, "01", true).Equals(String.Empty))
            {
                dtInicial.Value = Convert.ToDateTime(Funcoes.LeParametro(11, "01", false));
            }
            dtFinal.Value = DateTime.Now;
            cmbTecnico.SelectedIndex = 0;
            cmbTecnico.Focus();

        }

        private void btnPeriodo_Click(object sender, EventArgs e)
        {
            gbPeriodo.Visible = true;
            if (!Funcoes.LeParametro(11, "01", false).Equals(String.Empty))
            {
                dtPeriodo.Value = Convert.ToDateTime(Funcoes.LeParametro(11, "01", false)); ///dtPeriodo.Value = Convert.ToDateTime(Principal.paramentro);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            //GRAVA NO PARAMENTRO
            Funcoes.GravaParametro(11, "01", dtPeriodo.Value.ToString("dd/MM/yyyy"));
            MessageBox.Show("Data alterada com sucesso.", "Acerta Período", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dtInicial.Value = dtPeriodo.Value.AddDays(1);
            if (dtInicial.Value.AddDays(7) > DateTime.Now)
            {
                dtFinal.Value = DateTime.Now;
            }
            else
            {
                dtFinal.Value = dtInicial.Value.AddDays(6);
            }
            dtFinal.Focus();
            gbPeriodo.Visible = false;
        }

        private void btnVerifcaArquivo_Click(object sender, EventArgs e)
        {
            try
            {
                TimeSpan ts = dtFinal.Value - dtInicial.Value;
                int dias = ts.Days;

                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A data final deve ser maior que a data inicial", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                }
                else if (dias > 7)
                {
                    MessageBox.Show("O período de envio não pode ser maior que 7 dias", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                }

                else
                {
                    if (VerificaPendentes().Equals(true))
                    {
                        ValidaProdutos(dtInicial.Value, dtFinal.Value);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferencia SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public bool GeraXML()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                SngpcTecnico tec = new SngpcTecnico();

                DataTable dtTecnico = tec.BuscaTecnicoPorCod(Convert.ToInt32(cmbTecnico.SelectedValue));
                // VERIFICA SE EXISTA A PASTA "C:\SNGPC", CASO NÃO EXISTA CRIA A PASTA 
                if (!Directory.Exists(@"C:\SNGPC"))
                {
                    Directory.CreateDirectory(@"C:\SNGPC");
                }
                if (dtTecnico.Rows.Count != 0)
                {
                    WSAnvisa.sngpcSoapClient wSs = new WSAnvisa.sngpcSoapClient();
                    string retorno = wSs.ValidarUsuario(dtTecnico.Rows[0]["TEC_LOGIN"].ToString(), dtTecnico.Rows[0]["TEC_SENHA"].ToString());
                    SNGPCTransferencia trans = new SNGPCTransferencia();
                    if ((retorno).Equals("Ok"))
                    {
                        tslRegistros.Text = "Gerando arquivo de compra/venda. Aguarde...";

                        Estabelecimento est = new Estabelecimento();
                        DataTable dtEstab = est.BuscaDadosPorID(Principal.estAtual);


                        DataTable dtEntrada = trans.BuscaEntrada(dtInicial.Text, dtFinal.Text);
                        DataTable dtProVendas = trans.BuscaVendasPorPeriodo(dtInicial.Text, dtFinal.Text);

                        if (dtEstab.Rows.Count != 0)
                        {
                            Application.DoEvents();
                            string sXml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
                                        + "<mensagemSNGPC xmlns=\"urn:sngpc-schema\">"
                                        + "<cabecalho>"
                                        + "<cnpjEmissor>" + Funcoes.RemoveCaracter(dtEstab.Rows[0]["EST_CGC"].ToString()) + "</cnpjEmissor>"
                                        + "<cpfTransmissor>" + Funcoes.RemoveCaracter(dtTecnico.Rows[0]["TEC_CPF"].ToString()) + "</cpfTransmissor>"
                                        + "<dataInicio>" + dtInicial.Value.ToString("yyyy-MM-dd") + "</dataInicio>"
                                        + "<dataFim>" + dtFinal.Value.ToString("yyyy-MM-dd") + "</dataFim >"
                                        + "</cabecalho>"
                                        + "<corpo>"
                                        + "<medicamentos>";
                            for (int i = 0; i < dtEntrada.Rows.Count; i++)
                            {
                                sXml += "<entradaMedicamentos>"
                                    + "<notaFiscalEntradaMedicamento>"
                                    + "<numeroNotaFiscal>" + dtEntrada.Rows[i]["ENT_DOCTO"] + "</numeroNotaFiscal>"
                                    + "<tipoOperacaoNotaFiscal>" + dtEntrada.Rows[i]["TAB_CODIGO"] + "</tipoOperacaoNotaFiscal>"
                                    + "<dataNotaFiscal>" + Convert.ToDateTime(dtEntrada.Rows[i]["ENT_DATA_NF"]).ToString("yyyy-MM-dd") + "</dataNotaFiscal>"
                                    + "<cnpjOrigem>" + Funcoes.RemoveCaracter(dtEntrada.Rows[i]["ENT_CNPJ"].ToString()) + "</cnpjOrigem>"
                                    + "<cnpjDestino>" + Funcoes.RemoveCaracter(dtEstab.Rows[0]["EST_CGC"].ToString()) + "</cnpjDestino>"
                                    + "</notaFiscalEntradaMedicamento>"
                                    + "<medicamentoEntrada>"
                                    + "<classeTerapeutica>" + dtEntrada.Rows[i]["PROD_CLASSE_TERAP"] + "</classeTerapeutica>"
                                    + "<registroMSMedicamento>" + dtEntrada.Rows[i]["PROD_REGISTRO_MS"] + "</registroMSMedicamento>"
                                    + "<numeroLoteMedicamento>" + dtEntrada.Rows[i]["ENT_MED_LOTE"] + "</numeroLoteMedicamento>"
                                    + "<quantidadeMedicamento>" + dtEntrada.Rows[i]["ENT_MED_QTDE"] + "</quantidadeMedicamento>";
                                if (dtEntrada.Rows[i]["PROD_UNIDADE"].ToString().Equals("CX"))
                                {
                                    sXml += "<unidadeMedidaMedicamento>1</unidadeMedidaMedicamento>";
                                }
                                if (dtEntrada.Rows[i]["PROD_UNIDADE"].ToString().Equals("FR"))
                                {
                                    sXml += "<unidadeMedidaMedicamento>2</unidadeMedidaMedicamento>";
                                }
                                sXml += "</medicamentoEntrada>"
                                      + "<dataRecebimentoMedicamento>" + Convert.ToDateTime(dtEntrada.Rows[i]["ENT_DATA"]).ToString("yyyy-MM-dd") + "</dataRecebimentoMedicamento> "
                                      + "</entradaMedicamentos>";
                            };
                            var buscaTabSeq = new SngpcTabela();

                            for (int i = 0; i < dtProVendas.Rows.Count; i++)
                            {
                                sXml += "<saidaMedicamentoVendaAoConsumidor>"
                                      + "<tipoReceituarioMedicamento>" + dtProVendas.Rows[i]["TIPO_RECEITA"].ToString() + "</tipoReceituarioMedicamento>"
                                      + "<numeroNotificacaoMedicamento>" + dtProVendas.Rows[i]["VEN_NUM_NOT"] + "</numeroNotificacaoMedicamento>"
                                      + "<dataPrescricaoMedicamento>" + Convert.ToDateTime(dtProVendas.Rows[i]["VEN_PRESC_DT"]).ToString("yyyy-MM-dd") + "</dataPrescricaoMedicamento>"
                                      + "<prescritorMedicamento>"
                                      + "<nomePrescritor>" + dtProVendas.Rows[i]["VEN_PRESC_NOME"] + "</nomePrescritor>"
                                      + "<numeroRegistroProfissional>" + dtProVendas.Rows[i]["VEN_PRESC_NUMERO"] + "</numeroRegistroProfissional>"
                                      + "<conselhoProfissional>" + dtProVendas.Rows[i]["CONSELHO"] + "</conselhoProfissional>"
                                      + "<UFConselho>" + dtProVendas.Rows[i]["UF"] + "</UFConselho>"
                                      + "</prescritorMedicamento>"
                                      + "<usoMedicamento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_USO"].ToString()) + "</usoMedicamento>";

                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("2"))
                                {
                                    sXml += "<compradorMedicamento>"
                                        + "<nomeComprador>" + dtProVendas.Rows[i]["VEN_COMP_NOME"].ToString() + "</nomeComprador>"
                                        + "<tipoDocumento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_TD"].ToString()) + "</tipoDocumento>"
                                        + "<numeroDocumento>" + dtProVendas.Rows[i]["VEN_COMP_DOCTO"].ToString() + "</numeroDocumento>"
                                        + "<orgaoExpedidor>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_OE"].ToString()) + "</orgaoExpedidor>"
                                        + "<UFEmissaoDocumento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_UF"].ToString()) + "</UFEmissaoDocumento>"
                                        + "</compradorMedicamento>";
                                }

                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("1") && dtProVendas.Rows[i]["CONSELHO"].ToString() != "CRMV")
                                {
                                    sXml += "<pacienteMedicamento>"
                                      + "<nome>" + dtProVendas.Rows[i]["VEN_PAC_NOME"] + "</nome>";
                                    if (!String.IsNullOrEmpty(dtProVendas.Rows[i]["VEN_PAC_IDADE"].ToString()))
                                    {
                                        if (Convert.ToInt32(dtProVendas.Rows[i]["VEN_PAC_IDADE"]).Equals(0))
                                        {
                                            sXml += "<idade></idade>";
                                        }
                                        else
                                        {
                                            sXml += "<idade>" + dtProVendas.Rows[i]["VEN_PAC_IDADE"] + "</idade>";
                                        }
                                    }
                                    else
                                        sXml += "<idade></idade>";

                                    sXml += "<unidadeIdade>" + dtProVendas.Rows[i]["UNIDADE"] + "</unidadeIdade>"
                                          + "<sexo>" + dtProVendas.Rows[i]["SEXO"] + "</sexo>"
                                          + "<cid></cid>"
                                          + "</pacienteMedicamento>";
                                }

                                sXml += "<medicamentoVenda>";
                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("2"))
                                {
                                    sXml += "<usoProlongado></usoProlongado>";
                                }
                                else
                                    sXml += "<usoProlongado>" + dtProVendas.Rows[i]["PROD_USO_CONTINUO"] + "</usoProlongado>";

                                sXml += "<registroMSMedicamento>" + dtProVendas.Rows[i]["PROD_REGISTRO_MS"] + "</registroMSMedicamento>"
                                  + "<numeroLoteMedicamento>" + dtProVendas.Rows[i]["VEN_MED_LOTE"] + "</numeroLoteMedicamento>"
                                  + "<quantidadeMedicamento>" + dtProVendas.Rows[i]["VEN_MED_QTDE"] + "</quantidadeMedicamento>"
                                  + "<unidadeMedidaMedicamento>" + dtProVendas.Rows[i]["UNIDADE"] + "</unidadeMedidaMedicamento>"
                                  + "</medicamentoVenda>"
                                  + "<dataVendaMedicamento>" + Convert.ToDateTime(dtProVendas.Rows[i]["VEN_DATA"]).ToString("yyyy-MM-dd") + "</dataVendaMedicamento>"
                                  + "</saidaMedicamentoVendaAoConsumidor>";

                            }

                            //perda de medicamento//
                            var dadosPerda = new SngpcPerda();
                            Principal.dtBusca = dadosPerda.BuscaPerdasPorPeriodo(dtInicial.Text, dtFinal.Text, 'C');
                            if(Principal.dtBusca.Rows.Count > 0)
                            {
                                for (int i=0; i < Principal.dtBusca.Rows.Count; i++)
                                {
                                    sXml += "<saidaMedicamentoPerda>";
                                        sXml += "<motivoPerdaMedicamento>" + Convert.ToDouble(Principal.dtBusca.Rows[i]["TAB_CODIGO"]).ToString() + "</motivoPerdaMedicamento>";
                                        sXml += "<medicamentoPerda>";
                                            sXml += "<registroMSMedicamento>" + Principal.dtBusca.Rows[i]["PER_MS"] + "</registroMSMedicamento>";
                                            sXml += "<numeroLoteMedicamento>" + Principal.dtBusca.Rows[i]["PER_MED_LOTE"] + "</numeroLoteMedicamento> ";
                                            sXml += "<quantidadeMedicamento>" + Principal.dtBusca.Rows[i]["PER_MED_QTDE"] + "</quantidadeMedicamento> ";
                                    if (Principal.dtBusca.Rows[i]["PROD_UNIDADE"].ToString().Equals("CX"))
                                    {
                                        sXml += "<unidadeMedidaMedicamento>1</unidadeMedidaMedicamento>";
                                    }
                                    if (Principal.dtBusca.Rows[i]["PROD_UNIDADE"].ToString().Equals("FR"))
                                    {
                                        sXml += "<unidadeMedidaMedicamento>2</unidadeMedidaMedicamento>";
                                    }
                                            sXml += "</medicamentoPerda>";
                                        sXml += "<dataPerdaMedicamento>" + Convert.ToDateTime(Principal.dtBusca.Rows[i]["PER_DATA"]).ToString("yyyy-MM-dd") + "</dataPerdaMedicamento>";
                                    sXml += "</saidaMedicamentoPerda>";
                                }
                               
                            }

                            sXml += "</medicamentos>"
                                  + "<insumos></insumos>"
                                  + "</corpo>"
                                  + "</mensagemSNGPC>";

                            String nomeArquivo = @"C:\SNGPC\SNGPC_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                            //VERIFICA SE O ARQUIVO XML EXISTE SE EXISTIR DELETAR
                            if (System.IO.File.Exists(nomeArquivo))
                            {
                                System.IO.File.Delete(nomeArquivo);
                            }

                            System.IO.TextWriter arqXML = System.IO.File.AppendText(nomeArquivo);
                            arqXML.WriteLine(sXml);
                            arqXML.Close();

                            TransmissaoSNGPC transm = new TransmissaoSNGPC();

                            //GERA ARQUIVO ZIP COM O XML E RETORNO O CAMINHO COMPLETO DO ZIP
                            string caminhoZIP = transm.GerarArquivoZIP("C:\\SNGPC\\", "SNGPC_" + DateTime.Now.ToString("ddMMyyyy") + ".xml", "SNGPC_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                            //CONVERTE ARQUIVO EM BASE64
                            Byte[] arquivoBase64 = File.ReadAllBytes(caminhoZIP);

                            //CALCULA HASH MD5 APARTIR DA BASE64
                            string hash = transm.CalculateMD5Hash(Convert.ToBase64String(arquivoBase64));

                            //ENVIA ARQUIVO
                            string retornoAnvisa = wSs.EnviaArquivoSNGPC(dtTecnico.Rows[0]["TEC_LOGIN"].ToString(), dtTecnico.Rows[0]["TEC_SENHA"].ToString(), arquivoBase64, hash);

                            TrataRetorno(retornoAnvisa, hash, Convert.ToInt32(cmbTecnico.SelectedValue), dtFinal.Value.AddDays(1));

                            tslRegistros.Text = "";
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Não a Estabelecimento Cadastrados", "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show(retorno, "Transferência SNGPC", MessageBoxButtons.OK);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("Não a Técnicos Cadastrados", "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        public void TrataRetorno(string retorno, string hashEnvio, int codTecnico, DateTime dataTrans)
        {
            string[] palavras = retorno.Split(' ');
            if (palavras.Contains("sucesso,"))
            {
                SNGPCEnvios env = new SNGPCEnvios()
                {
                    HashEnviado = hashEnvio,
                    HashRetorno = palavras[11],
                    Aceito = "P",
                    TecCodigo = codTecnico,
                    EstCodigo = Principal.estAtual,
                    MensagemValidacao = ""
                };
                if (env.InsereEnvios(env).Equals(true))
                {
                    Funcoes.GravaParametro(11, "01", dataTrans.ToString("dd/MM/yyyy"), "GERAL", "Data da última transferência", "SNGPC", "Será gravada a data da última transferência efetuada pelo sistema");
                    MessageBox.Show("Envio Realizado com Sucesso");
                }
            }
            else
            {
                MessageBox.Show(retorno);
            }
            lblMsg.Text = String.Empty;
        }

        public bool VerificaPendentes()
        {

            SNGPCTransferencia trans = new SNGPCTransferencia();
            DataTable dtEnvios = trans.BuscaPorStatus("P");

            WSAnvisa.sngpcSoapClient wSs = new WSAnvisa.sngpcSoapClient();

            if (dtEnvios.Rows.Count != 0)
            {
                string retornoAnvisa = wSs.ConsultaDadosArquivoSNGPC(dtEnvios.Rows[0]["TEC_LOGIN"].ToString(), dtEnvios.Rows[0]["TEC_SENHA"].ToString(), Funcoes.RemoveCaracter(dtEnvios.Rows[0]["EST_CGC"].ToString()), dtEnvios.Rows[0]["HASH_RETORNO"].ToString());
                string[] palavras = retornoAnvisa.Split(' ');
                if (palavras.Contains("<transmissaoSNGPC>"))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retornoAnvisa);

                    var node = xmlDoc.GetElementsByTagName("cabecalho").OfType<XmlNode>();
                    XmlNodeList xnResul = xmlDoc.GetElementsByTagName("cabecalho");

                    foreach (XmlNode item in node)
                    {
                        if (item["DATAVALIDACAO"] == null)
                        {
                            MessageBox.Show("Último arquivo enviado esta pendente de validação da ANVISA, tente novamente mais tarde  ", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                        if (item["MENSAGEMVALIDACAO"] != null)
                        {
                            txtMsg.Visible = true;
                            btnTransferencia.Visible = false;
                            SNGPCEnvios env = new SNGPCEnvios()
                            {
                                HashRetorno = item["CODIGOHASH"].InnerText.ToString(),
                                Aceito = "N",
                                DataInicial = Convert.ToDateTime(item["INICIOREFERENCIA"].InnerText),
                                DataFinal = Convert.ToDateTime(item["FIMREFERENCIA"].InnerText),
                                DataRecebimento = Convert.ToDateTime(item["DATATRANSMISSAO"].InnerText),
                                DataValidacao = Convert.ToDateTime(item["DATAVALIDACAO"].InnerText),
                                MensagemValidacao = xnResul[0]["MENSAGEMVALIDACAO"].InnerText.ToString()
                            };
                            env.AtualizaEnvios(env);
                            txtMsg.Text = xnResul[0]["MENSAGEMVALIDACAO"].InnerText.ToString().Replace("[NOVA_LINHA]", System.Environment.NewLine);
                            return true;
                        }
                        else
                        {
                            txtMsg.Visible = false;
                            btnTransferencia.Visible = true;

                            SNGPCEnvios env = new SNGPCEnvios()
                            {
                                HashRetorno = item["CODIGOHASH"].InnerText.ToString(),
                                Aceito = "S",
                                DataInicial = Convert.ToDateTime(item["INICIOREFERENCIA"].InnerText),
                                DataFinal = Convert.ToDateTime(item["FIMREFERENCIA"].InnerText),
                                DataRecebimento = Convert.ToDateTime(item["DATATRANSMISSAO"].InnerText),
                                DataValidacao = Convert.ToDateTime(item["DATAVALIDACAO"].InnerText),
                                MensagemValidacao = ""
                            };
                            return true;
                        }
                    }
                }
                else
                {
                    btnTransferencia.Visible = false;
                    MessageBox.Show(retornoAnvisa, "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            return true;
        }
        public bool ValidaProdutos(DateTime dtInicial, DateTime dtFinal)
        {
            SNGPCTransferencia trans = new SNGPCTransferencia();
            DataTable dtEntrada = trans.BuscaProdutos(7, dtInicial, dtFinal);

            DataTable dtProdErro = dtEntrada.Clone();
            dtProdErro.Columns.Add("ERRO");

            if (dtEntrada.Rows.Count != 0)
            {
                for (var i = 0; i < dtEntrada.Rows.Count; i++)
                {
                    int contLinhasTabErro = dtProdErro.Rows.Count;
                    bool ContemErro = false;
                    if (dtEntrada.Rows[i]["PROD_REGISTRO_MS"].ToString().Length != 13 && ContemErro.Equals(false))
                    {
                        dtProdErro.ImportRow(dtEntrada.Rows[i]);
                        dtProdErro.Rows[contLinhasTabErro]["ERRO"] = "Registro MS inválido";
                        ContemErro = true;
                    }
                    if (dtEntrada.Rows[i]["PROD_UNIDADE"].ToString() != "CX" && dtEntrada.Rows[i]["PROD_UNIDADE"].ToString() != "FR" && ContemErro.Equals(false))
                    {
                        dtProdErro.ImportRow(dtEntrada.Rows[i]);
                        dtProdErro.Rows[contLinhasTabErro]["ERRO"] = "Unidade inválida";
                        ContemErro = true;
                    }
                    if (String.IsNullOrEmpty(dtEntrada.Rows[i]["PROD_CLASSE_TERAP"].ToString()) && ContemErro.Equals(false))
                    {
                        dtProdErro.ImportRow(dtEntrada.Rows[i]);
                        dtProdErro.Rows[contLinhasTabErro]["ERRO"] = "Classe Terapêutica inválida";
                        ContemErro = true;
                    }
                }

                lblMsg.Visible.Equals(true);
                if (dtProdErro.Rows.Count != 0)
                {
                    lblMsg.Text = "Verificação concluída , verefique as inconsistência nos produtos abaixo";
                    PreencheGrid(dtProdErro);

                    return false;
                }
                else
                {
                    lblMsg.Text = "Verificação concluída com sucesso, clique no botão \"Enviar Arquivo\" para enviar o arquivo ";
                    btnVerifcaArquivo.Visible = false;
                    btnTransferencia.Visible = true;
                    cmdProdErro.Visible = false;
                    return true;
                }
            }
            else
            {
                lblMsg.Text = "Verificação concluída com sucesso, clique no botão \"Enviar Arquivo\" para enviar o arquivo";
                btnVerifcaArquivo.Visible = false;
                btnTransferencia.Visible = true;
                cmdProdErro.Visible = false;
                return true;
            }

        }

        public void PreencheGrid(DataTable dtProdErro)
        {
            TipoUnidade uni = new TipoUnidade();
            DataTable dtUnidade = uni.TodosDados();

            PROD_UNIDADE.ValueMember = "UNI_DESC_ABREV";
            PROD_UNIDADE.DisplayMember = "UNI_DESC_ABREV";
            PROD_UNIDADE.DataSource = dtUnidade;

            cmdProdErro.Visible = true;
            cmdProdErro.DataSource = dtProdErro;

        }

        private void cmbTecnico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnVerifcaArquivo.PerformClick();
        }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        public void Ajuda() { }

        private void btnTransferencia_Click(object sender, EventArgs e)
        {
            GeraXML();
        }

        private void cmdProdErro_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (ConsisteCampos(e).Equals(true))
            {
                SNGPCTransferencia trans = new SNGPCTransferencia();
                trans.AtualizaProduto(cmdProdErro.Rows[e.RowIndex].Cells[0].Value.ToString(), cmdProdErro.Columns[e.ColumnIndex].Name, cmdProdErro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());

            }
        }

        public bool ConsisteCampos(DataGridViewCellEventArgs e)
        {

            if (cmdProdErro.Columns[e.ColumnIndex].Name == "PROD_UNIDADE")
            {
                if (cmdProdErro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() != "CX" && cmdProdErro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() != "FR")
                {
                    MessageBox.Show("Selecione CX para Caixa ou FR pra Frasco", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            if (cmdProdErro.Columns[e.ColumnIndex].Name == "PROD_CLASSE_TERAP")
            {
                if (String.IsNullOrEmpty(cmdProdErro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                {
                    MessageBox.Show("Classe Terapêutica nao pode estar em branco", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            if (cmdProdErro.Columns[e.ColumnIndex].Name == "PROD_REGISTRO_MS")
            {
                if (cmdProdErro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Length != 13)
                {
                    MessageBox.Show("O numero do Registro MS", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }

        public bool GerarSomenteArquivoXML() {
            try
            {
                Cursor = Cursors.WaitCursor;
                SngpcTecnico tec = new SngpcTecnico();
                SNGPCTransferencia trans = new SNGPCTransferencia();
                DataTable dtTecnico = tec.BuscaTecnicoPorCod(Convert.ToInt32(cmbTecnico.SelectedValue));

                // VERIFICA SE EXISTA A PASTA "C:\SNGPC", CASO NÃO EXISTA CRIA A PASTA 
                if (!Directory.Exists(@"C:\SNGPC"))
                    Directory.CreateDirectory(@"C:\SNGPC");
                
                if (dtTecnico.Rows.Count != 0)
                {
                    WSAnvisa.sngpcSoapClient wSs = new WSAnvisa.sngpcSoapClient();
                    string retorno = wSs.ValidarUsuario(dtTecnico.Rows[0]["TEC_LOGIN"].ToString(), dtTecnico.Rows[0]["TEC_SENHA"].ToString());
                    if ((retorno).Equals("Ok"))
                    {
                        tslRegistros.Text = "Gerando arquivo de compra/venda. Aguarde...";

                        Estabelecimento est = new Estabelecimento();
                        DataTable dtEstab = est.BuscaDadosPorID(Principal.estAtual);


                        DataTable dtEntrada = trans.BuscaEntrada(dtInicial.Text, dtFinal.Text);
                        DataTable dtProVendas = trans.BuscaVendasPorPeriodo(dtInicial.Text, dtFinal.Text);

                        if (dtEstab.Rows.Count != 0)
                        {
                            Application.DoEvents();
                            string sXml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
                                        + "<mensagemSNGPC xmlns=\"urn:sngpc-schema\">"
                                        + "<cabecalho>"
                                        + "<cnpjEmissor>" + Funcoes.RemoveCaracter(dtEstab.Rows[0]["EST_CGC"].ToString()) + "</cnpjEmissor>"
                                        + "<cpfTransmissor>" + Funcoes.RemoveCaracter(dtTecnico.Rows[0]["TEC_CPF"].ToString()) + "</cpfTransmissor>"
                                        + "<dataInicio>" + dtInicial.Value.ToString("yyyy-MM-dd") + "</dataInicio>"
                                        + "<dataFim>" + dtFinal.Value.ToString("yyyy-MM-dd") + "</dataFim >"
                                        + "</cabecalho>"
                                        + "<corpo>"
                                        + "<medicamentos>";
                            for (int i = 0; i < dtEntrada.Rows.Count; i++)
                            {
                                sXml += "<entradaMedicamentos>"
                                    + "<notaFiscalEntradaMedicamento>"
                                    + "<numeroNotaFiscal>" + dtEntrada.Rows[i]["ENT_DOCTO"] + "</numeroNotaFiscal>"
                                    + "<tipoOperacaoNotaFiscal>" + dtEntrada.Rows[i]["TAB_CODIGO"] + "</tipoOperacaoNotaFiscal>"
                                    + "<dataNotaFiscal>" + Convert.ToDateTime(dtEntrada.Rows[i]["ENT_DATA_NF"]).ToString("yyyy-MM-dd") + "</dataNotaFiscal>"
                                    + "<cnpjOrigem>" + Funcoes.RemoveCaracter(dtEntrada.Rows[i]["ENT_CNPJ"].ToString()) + "</cnpjOrigem>"
                                    + "<cnpjDestino>" + Funcoes.RemoveCaracter(dtEstab.Rows[0]["EST_CGC"].ToString()) + "</cnpjDestino>"
                                    + "</notaFiscalEntradaMedicamento>"
                                    + "<medicamentoEntrada>"
                                    + "<classeTerapeutica>" + dtEntrada.Rows[i]["PROD_CLASSE_TERAP"] + "</classeTerapeutica>"
                                    + "<registroMSMedicamento>" + dtEntrada.Rows[i]["PROD_REGISTRO_MS"] + "</registroMSMedicamento>"
                                    + "<numeroLoteMedicamento>" + dtEntrada.Rows[i]["ENT_MED_LOTE"] + "</numeroLoteMedicamento>"
                                    + "<quantidadeMedicamento>" + dtEntrada.Rows[i]["ENT_MED_QTDE"] + "</quantidadeMedicamento>";
                                if (dtEntrada.Rows[i]["PROD_UNIDADE"].ToString().Equals("CX"))
                                {
                                    sXml += "<unidadeMedidaMedicamento>1</unidadeMedidaMedicamento>";
                                }
                                if (dtEntrada.Rows[i]["PROD_UNIDADE"].ToString().Equals("FR"))
                                {
                                    sXml += "<unidadeMedidaMedicamento>2</unidadeMedidaMedicamento>";
                                }
                                sXml += "</medicamentoEntrada>"
                                      + "<dataRecebimentoMedicamento>" + Convert.ToDateTime(dtEntrada.Rows[i]["ENT_DATA"]).ToString("yyyy-MM-dd") + "</dataRecebimentoMedicamento> "
                                      + "</entradaMedicamentos>";
                            }
                            var buscaTabSeq = new SngpcTabela();

                            for (int i = 0; i < dtProVendas.Rows.Count; i++)
                            {
                                sXml += "<saidaMedicamentoVendaAoConsumidor>"
                                      + "<tipoReceituarioMedicamento>" + dtProVendas.Rows[i]["TIPO_RECEITA"].ToString() + "</tipoReceituarioMedicamento>"
                                      + "<numeroNotificacaoMedicamento>" + dtProVendas.Rows[i]["VEN_NUM_NOT"] + "</numeroNotificacaoMedicamento>"
                                      + "<dataPrescricaoMedicamento>" + Convert.ToDateTime(dtProVendas.Rows[i]["VEN_PRESC_DT"]).ToString("yyyy-MM-dd") + "</dataPrescricaoMedicamento>"
                                      + "<prescritorMedicamento>"
                                      + "<nomePrescritor>" + dtProVendas.Rows[i]["VEN_PRESC_NOME"] + "</nomePrescritor>"
                                      + "<numeroRegistroProfissional>" + dtProVendas.Rows[i]["VEN_PRESC_NUMERO"] + "</numeroRegistroProfissional>"
                                      + "<conselhoProfissional>" + dtProVendas.Rows[i]["CONSELHO"] + "</conselhoProfissional>"
                                      + "<UFConselho>" + dtProVendas.Rows[i]["UF"] + "</UFConselho>"
                                      + "</prescritorMedicamento>"
                                      + "<usoMedicamento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_USO"].ToString()) + "</usoMedicamento>";

                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("2"))
                                {
                                    sXml += "<compradorMedicamento>"
                                        + "<nomeComprador>" + dtProVendas.Rows[i]["VEN_COMP_NOME"].ToString() + "</nomeComprador>"
                                        + "<tipoDocumento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_TD"].ToString()) + "</tipoDocumento>"
                                        + "<numeroDocumento>" + dtProVendas.Rows[i]["VEN_COMP_DOCTO"].ToString() + "</numeroDocumento>"
                                        + "<orgaoExpedidor>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_OE"].ToString()) + "</orgaoExpedidor>"
                                        + "<UFEmissaoDocumento>" + buscaTabSeq.BuscaTabCodigo(dtProVendas.Rows[i]["VEN_COMP_UF"].ToString()) + "</UFEmissaoDocumento>"
                                        + "</compradorMedicamento>";
                                }

                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("1") && dtProVendas.Rows[i]["CONSELHO"].ToString() != "CRMV")
                                {
                                    sXml += "<pacienteMedicamento>"
                                      + "<nome>" + dtProVendas.Rows[i]["VEN_PAC_NOME"] + "</nome>";
                                    if (!String.IsNullOrEmpty(dtProVendas.Rows[i]["VEN_PAC_IDADE"].ToString()))
                                    {
                                        if (Convert.ToInt32(dtProVendas.Rows[i]["VEN_PAC_IDADE"]).Equals(0))
                                        {
                                            sXml += "<idade></idade>";
                                        }
                                        else
                                        {
                                            sXml += "<idade>" + dtProVendas.Rows[i]["VEN_PAC_IDADE"] + "</idade>";
                                        }
                                    }
                                    else
                                        sXml += "<idade></idade>";

                                    sXml += "<unidadeIdade>" + dtProVendas.Rows[i]["UNIDADE"] + "</unidadeIdade>"
                                          + "<sexo>" + dtProVendas.Rows[i]["SEXO"] + "</sexo>"
                                          + "<cid></cid>"
                                          + "</pacienteMedicamento>";
                                }

                                sXml += "<medicamentoVenda>";
                                if (dtProVendas.Rows[i]["PROD_CLASSE_TERAP"].ToString().Equals("2"))
                                {
                                    sXml += "<usoProlongado></usoProlongado>";
                                }
                                else
                                    sXml += "<usoProlongado>" + dtProVendas.Rows[i]["PROD_USO_CONTINUO"] + "</usoProlongado>";

                                sXml += "<registroMSMedicamento>" + dtProVendas.Rows[i]["PROD_REGISTRO_MS"] + "</registroMSMedicamento>"
                                  + "<numeroLoteMedicamento>" + dtProVendas.Rows[i]["VEN_MED_LOTE"] + "</numeroLoteMedicamento>"
                                  + "<quantidadeMedicamento>" + dtProVendas.Rows[i]["VEN_MED_QTDE"] + "</quantidadeMedicamento>"
                                  + "<unidadeMedidaMedicamento>" + dtProVendas.Rows[i]["UNIDADE"] + "</unidadeMedidaMedicamento>"
                                  + "</medicamentoVenda>"
                                  + "<dataVendaMedicamento>" + Convert.ToDateTime(dtProVendas.Rows[i]["VEN_DATA"]).ToString("yyyy-MM-dd") + "</dataVendaMedicamento>"
                                  + "</saidaMedicamentoVendaAoConsumidor>";

                            }

                            //perda de medicamento//
                            var dadosPerda = new SngpcPerda();
                            Principal.dtBusca = dadosPerda.BuscaPerdasPorPeriodo(dtInicial.Text, dtFinal.Text, 'C');
                            if (Principal.dtBusca.Rows.Count > 0)
                            {
                                for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                                {
                                    sXml += "<saidaMedicamentoPerda>";
                                    sXml += "<motivoPerdaMedicamento>" + Convert.ToDouble(Principal.dtBusca.Rows[i]["TAB_CODIGO"]).ToString() + "</motivoPerdaMedicamento>";
                                    sXml += "<medicamentoPerda>";
                                    sXml += "<registroMSMedicamento>" + Principal.dtBusca.Rows[i]["PER_MS"] + "</registroMSMedicamento>";
                                    sXml += "<numeroLoteMedicamento>" + Principal.dtBusca.Rows[i]["PER_MED_LOTE"] + "</numeroLoteMedicamento> ";
                                    sXml += "<quantidadeMedicamento>" + Principal.dtBusca.Rows[i]["PER_MED_QTDE"] + "</quantidadeMedicamento> ";
                                    if (Principal.dtBusca.Rows[i]["PROD_UNIDADE"].ToString().Equals("CX"))
                                    {
                                        sXml += "<unidadeMedidaMedicamento>1</unidadeMedidaMedicamento>";
                                    }
                                    if (Principal.dtBusca.Rows[i]["PROD_UNIDADE"].ToString().Equals("FR"))
                                    {
                                        sXml += "<unidadeMedidaMedicamento>2</unidadeMedidaMedicamento>";
                                    }
                                    sXml += "</medicamentoPerda>";
                                    sXml += "<dataPerdaMedicamento>" + Convert.ToDateTime(Principal.dtBusca.Rows[i]["PER_DATA"]).ToString("yyyy-MM-dd") + "</dataPerdaMedicamento>";
                                    sXml += "</saidaMedicamentoPerda>";
                                }

                            }

                            sXml += "</medicamentos>"
                                  + "<insumos></insumos>"
                                  + "</corpo>"
                                  + "</mensagemSNGPC>";

                            String nomeArquivo = @"C:\SNGPC\SNGPC_Arquivo" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                            //VERIFICA SE O ARQUIVO XML EXISTE, SE EXISTIR DELETAR
                            if (System.IO.File.Exists(nomeArquivo))
                                System.IO.File.Delete(nomeArquivo);

                            System.IO.TextWriter arqXML = System.IO.File.AppendText(nomeArquivo);
                            arqXML.WriteLine(sXml);
                            arqXML.Close();

                            tslRegistros.Text = "";
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Não a Estabelecimento Cadastrados", "Criação de arquivo SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show(retorno, "Criação de arquivo SNGPC", MessageBoxButtons.OK);
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("Não a Técnicos Cadastrados", "Criação de arquivo SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Criação de arquivo SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            String nomeArquivo = @"C:\SNGPC\SNGPC_Arquivo" + DateTime.Now.ToString("ddMMyyyy") + ".xml";
            if (GerarSomenteArquivoXML())
                MessageBox.Show("Arquivo Gerado com Sucesso \nSe encontra em: "+nomeArquivo,"Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmSngpcTransferencia_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void frmSngpcTransferencia_KeyDown(object sender, KeyEventArgs e)
        {
        }
    }
}
