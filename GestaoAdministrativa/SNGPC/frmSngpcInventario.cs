﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System.IO.Compression;

using System.IO;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcInventario : Form, Botoes
    {
        private ToolStripButton tsbInventario = new ToolStripButton("Inventário (XML)");
        private ToolStripSeparator tssInventario = new ToolStripSeparator();
        private string sXml;

        public frmSngpcInventario(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmSngpcInventario_Load(object sender, EventArgs e)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbInventario.AutoSize = false;
                this.tsbInventario.Image = Properties.Resources.sngpc;
                this.tsbInventario.Size = new System.Drawing.Size(125, 20);
                this.tsbInventario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbInventario);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssInventario);
                tsbInventario.Click += delegate
                {
                    var cadInventario = Application.OpenForms.OfType<frmSngpcInventario>().FirstOrDefault();
                    Util.BotoesGenericos();
                    cadInventario.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbInventario);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssInventario);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
 
        public void Primeiro()
        {
        }

        public void Ultimo()
        {
        }

        public void Proximo()
        {
        }

        public void Anterior()
        {
        }

        public bool Incluir()
        {
            return false;
        }

        public bool Excluir()
        {
            return false;
        }

        public bool Atualiza()
        {
            return false;
        }

        public void ImprimirRelatorio()
        {
        }

        public void Limpar()
        {
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            try
            {
                if (Util.SelecionaRegistrosTodosOuEspecifico("SNGPC_LAC_INVENTARIO", "LAC_INV_ENVIADO", "LAC_INV_ENVIADO", "'S'").Rows.Count == 0)
                {
                    Cursor = Cursors.WaitCursor;
                    SngpcTecnico tec = new SngpcTecnico();

                    DataTable dtTecnico = tec.BuscaTecnicoPorCod(Convert.ToInt32(cmbTecnico.SelectedValue));
                    // VERIFICA SE EXISTA A PASTA "C:\SNGPC", CASO NÃO EXISTA CRIA A PASTA 
                    if (!Directory.Exists(@"C:\SNGPC"))
                    {
                        Directory.CreateDirectory(@"C:\SNGPC");
                    }
                    if (dtTecnico.Rows.Count != 0)
                    {
                        WSAnvisa.sngpcSoapClient wSs = new WSAnvisa.sngpcSoapClient();
                        string retorno = wSs.ValidarUsuario(dtTecnico.Rows[0]["TEC_LOGIN"].ToString(), dtTecnico.Rows[0]["TEC_SENHA"].ToString());

                        if ((retorno).Equals("Ok"))
                        {
                            tslRegistros.Text = "Gerando inventário. Aguarde...";

                            Estabelecimento est = new Estabelecimento();
                            SngpcInventario inv = new SngpcInventario();

                            DataTable dtEstab = est.BuscaDadosPorID(Principal.estAtual);
                            DataTable dtProd = inv.BuscaInvetario(Principal.estAtual, Principal.empAtual);
                            if (dtEstab.Rows.Count != 0)
                            {
                                Application.DoEvents();
                                sXml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
                                sXml += "<mensagemSNGPCInventario xmlns=\"urn:sngpc-schema\">";
                                sXml += "<cabecalho>";
                                sXml += "<cnpjEmissor>" + Funcoes.RemoveCaracter(dtEstab.Rows[0]["EST_CGC"].ToString()) + "</cnpjEmissor>";
                                sXml += "<cpfTransmissor>" + Funcoes.RemoveCaracter(dtTecnico.Rows[0]["TEC_CPF"].ToString()) + "</cpfTransmissor>";
                                sXml += "<data>" + DateTime.Now.ToString("yyyy-MM-dd") + "</data>";
                                sXml += "</cabecalho>";
                                sXml += "<corpo>";
                                sXml += "<medicamentos>";
                                for (int i = 0; i < dtProd.Rows.Count; i++)
                                {
                                    sXml += "<entradaMedicamentos>";
                                    sXml += "<medicamentoEntrada>";
                                    sXml += "<classeTerapeutica>" + dtProd.Rows[i]["PROD_CLASSE_TERAP"].ToString() + "</classeTerapeutica>";
                                    sXml += "<registroMSMedicamento>" + dtProd.Rows[i]["PROD_REGISTRO_MS"].ToString() + "</registroMSMedicamento>";
                                    sXml += "<numeroLoteMedicamento>" + dtProd.Rows[i]["LAC_INV_LOTE"].ToString() + "</numeroLoteMedicamento>";
                                    sXml += "<quantidadeMedicamento>" + dtProd.Rows[i]["LAC_INV_QTDE"].ToString() + "</quantidadeMedicamento>";

                                    int unidade = 0;
                                    if (dtProd.Rows[i]["PROD_UNIDADE"].ToString() == "CX")
                                    {
                                        unidade = 1;
                                    }
                                    else if (dtProd.Rows[i]["PROD_UNIDADE"].ToString() == "FR")
                                    {
                                        unidade = 2;
                                    }
                                    sXml += "<unidadeMedidaMedicamento>" + unidade + "</unidadeMedidaMedicamento>";
                                    sXml += "</medicamentoEntrada>";
                                    sXml += "</entradaMedicamentos>";

                                }

                                sXml += "</medicamentos>";
                                sXml += "<insumos></insumos>";
                                sXml += "</corpo>";
                                sXml += "</mensagemSNGPCInventario>";

                                String nomeArquivo = @"C:\SNGPC\SNGPC_INVENTARIO_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                                //VERIFICA SE O ARQUIVO XML EXISTE SE EXISTIR DELETAR
                                if (System.IO.File.Exists(nomeArquivo))
                                {
                                    System.IO.File.Delete(nomeArquivo);
                                }

                                System.IO.TextWriter arqXML = System.IO.File.AppendText(nomeArquivo);
                                arqXML.WriteLine(sXml);
                                arqXML.Close();

                                TransmissaoSNGPC trans = new TransmissaoSNGPC();

                                //GERA ARQUIVO ZIP COM O XML E RETORNO O CAMINHO COMPLETO DO ZIP
                                string caminhoZIP = trans.GerarArquivoZIP("C:\\SNGPC\\", "SNGPC_INVENTARIO_" + DateTime.Now.ToString("ddMMyyyy") + ".xml", "SNGPC_INVENTARIO_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                                //CONVERTE ARQUIVO EM BASE64
                                Byte[] arquivoBase64 = File.ReadAllBytes(caminhoZIP);

                                //CALCULA HASH MD5 APARTIR DA BASE64
                                string hash = trans.CalculateMD5Hash(Convert.ToBase64String(arquivoBase64));

                                //ENVIA ARQUIVO
                                string retornoAnvisa = wSs.EnviaArquivoSNGPC(dtTecnico.Rows[0]["TEC_LOGIN"].ToString(), dtTecnico.Rows[0]["TEC_SENHA"].ToString(), arquivoBase64, hash);

                                TrataRetorno(retornoAnvisa, hash, Convert.ToInt32(cmbTecnico.SelectedValue));

                            }
                            else
                            {
                                MessageBox.Show(retorno, "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show(retorno, "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Arquivo de inventário já enviado para a ANVISA.\nCaso queira fazer novo envio necessário finalizar o inventário na Anvisa" + 
                        " e lançar novamente no sistema." , "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnInventario.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
                tslRegistros.Text = "";
            }
        }

        public void TrataRetorno(string retorno, string hashEnvio, int codTecnico)
        {

            string[] palavras = retorno.Split(' ');
            if (palavras.Contains("sucesso,"))
            {
                SNGPCEnvios env = new SNGPCEnvios()
                {
                    HashEnviado = hashEnvio,
                    HashRetorno = palavras[11],
                    Aceito = "P",
                    TecCodigo = codTecnico,
                    EstCodigo = Principal.estAtual
                };
                if (env.InsereEnvios(env).Equals(true))
                {
                    BancoDados.ExecuteNoQuery("UPDATE SNGPC_LAC_INVENTARIO SET LAC_INV_ENVIADO = 'S'", null);

                    Funcoes.GravaParametro(11, "01", DateTime.Now.ToString("dd/MM/yyyy"), "GERAL", "Data da última transferência", "SNGPC", "Será gravada a data da última transferência efetuada pelo sistema");
                    MessageBox.Show("Envio Realizado com Sucesso");
                }
            }
            else
            {
                MessageBox.Show(retorno);
            }
        }

        private void frmSngpcInventario_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = BancoDados.selecionarRegistros("SELECT * FROM SNGPC_TECNICOS  WHERE TEC_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Não há nenhum responsável técnico cadastrado. Não é possível continuar com a transferência.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbTecnico.DataSource = Principal.dtPesq;
                    cmbTecnico.DisplayMember = "TEC_NOME";
                    cmbTecnico.ValueMember = "TEC_CODIGO";
                    cmbTecnico.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Inventário SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbTecnico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnInventario.PerformClick();
        }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        public void Ajuda() { }

     
    }

}
