﻿namespace GestaoAdministrativa.SNGPC
{
    partial class frmSngpcHistoricoEnvios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSngpcHistoricoEnvios));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgHistorico = new System.Windows.Forms.GroupBox();
            this.dgHistorio = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTecnico = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMensagemDeErro = new System.Windows.Forms.TextBox();
            this.HASH_RETORNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACEITO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_INICIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_FINAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_RECEBIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_VALIDACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEC_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEC_LOGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEC_SENHA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HASH_ENVIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EST_CGC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MENSAGEMVALIDACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.dgHistorico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHistorio)).BeginInit();
            this.panel3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 560);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dgHistorico);
            this.panel2.Location = new System.Drawing.Point(5, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(940, 497);
            this.panel2.TabIndex = 157;
            // 
            // dgHistorico
            // 
            this.dgHistorico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgHistorico.Controls.Add(this.groupBox2);
            this.dgHistorico.Controls.Add(this.dgHistorio);
            this.dgHistorico.Controls.Add(this.btnBuscar);
            this.dgHistorico.Controls.Add(this.label6);
            this.dgHistorico.Controls.Add(this.cmbTecnico);
            this.dgHistorico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgHistorico.ForeColor = System.Drawing.Color.Black;
            this.dgHistorico.Location = new System.Drawing.Point(6, 9);
            this.dgHistorico.Name = "dgHistorico";
            this.dgHistorico.Size = new System.Drawing.Size(923, 483);
            this.dgHistorico.TabIndex = 156;
            this.dgHistorico.TabStop = false;
            this.dgHistorico.Text = "Principal";
            // 
            // dgHistorio
            // 
            this.dgHistorio.AllowUserToAddRows = false;
            this.dgHistorio.AllowUserToDeleteRows = false;
            this.dgHistorio.AllowUserToOrderColumns = true;
            this.dgHistorio.AllowUserToResizeColumns = false;
            this.dgHistorio.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgHistorio.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgHistorio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgHistorio.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHistorio.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgHistorio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHistorio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HASH_RETORNO,
            this.ACEITO,
            this.DATA_INICIAL,
            this.DATA_FINAL,
            this.DATA_RECEBIMENTO,
            this.DATA_VALIDACAO,
            this.TEC_NOME,
            this.TEC_LOGIN,
            this.TEC_SENHA,
            this.HASH_ENVIO,
            this.EST_CGC,
            this.MENSAGEMVALIDACAO});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgHistorio.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgHistorio.GridColor = System.Drawing.Color.LightGray;
            this.dgHistorio.Location = new System.Drawing.Point(16, 73);
            this.dgHistorio.MultiSelect = false;
            this.dgHistorio.Name = "dgHistorio";
            this.dgHistorio.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgHistorio.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgHistorio.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgHistorio.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgHistorio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgHistorio.Size = new System.Drawing.Size(886, 248);
            this.dgHistorio.TabIndex = 157;
            this.dgHistorio.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgHistorio_CellMouseClick);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(459, 29);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 156;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 16);
            this.label6.TabIndex = 154;
            this.label6.Text = "Responsável Técnico";
            // 
            // cmbTecnico
            // 
            this.cmbTecnico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTecnico.FormattingEnabled = true;
            this.cmbTecnico.Location = new System.Drawing.Point(16, 43);
            this.cmbTecnico.Name = "cmbTecnico";
            this.cmbTecnico.Size = new System.Drawing.Size(437, 24);
            this.cmbTecnico.TabIndex = 155;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(-1, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(951, 24);
            this.panel3.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(7, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 18);
            this.label1.TabIndex = 39;
            this.label1.Text = "Histórico de Envios";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(944, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtMensagemDeErro);
            this.groupBox2.Location = new System.Drawing.Point(16, 327);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(886, 150);
            this.groupBox2.TabIndex = 158;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mensagem de Erro do Arquivo";
            // 
            // txtMensagemDeErro
            // 
            this.txtMensagemDeErro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMensagemDeErro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMensagemDeErro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMensagemDeErro.ForeColor = System.Drawing.Color.Black;
            this.txtMensagemDeErro.Location = new System.Drawing.Point(6, 21);
            this.txtMensagemDeErro.Multiline = true;
            this.txtMensagemDeErro.Name = "txtMensagemDeErro";
            this.txtMensagemDeErro.Size = new System.Drawing.Size(874, 123);
            this.txtMensagemDeErro.TabIndex = 0;
            // 
            // HASH_RETORNO
            // 
            this.HASH_RETORNO.DataPropertyName = "HASH_RETORNO";
            this.HASH_RETORNO.HeaderText = "Código";
            this.HASH_RETORNO.MaxInputLength = 40;
            this.HASH_RETORNO.Name = "HASH_RETORNO";
            this.HASH_RETORNO.ReadOnly = true;
            this.HASH_RETORNO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.HASH_RETORNO.Width = 150;
            // 
            // ACEITO
            // 
            this.ACEITO.DataPropertyName = "ACEITO";
            this.ACEITO.HeaderText = "Aceito";
            this.ACEITO.Name = "ACEITO";
            this.ACEITO.ReadOnly = true;
            this.ACEITO.Width = 80;
            // 
            // DATA_INICIAL
            // 
            this.DATA_INICIAL.DataPropertyName = "DATA_INICIAL";
            this.DATA_INICIAL.HeaderText = "Data Incial";
            this.DATA_INICIAL.Name = "DATA_INICIAL";
            this.DATA_INICIAL.ReadOnly = true;
            this.DATA_INICIAL.Width = 120;
            // 
            // DATA_FINAL
            // 
            this.DATA_FINAL.DataPropertyName = "DATA_FINAL";
            this.DATA_FINAL.HeaderText = "Data Final";
            this.DATA_FINAL.Name = "DATA_FINAL";
            this.DATA_FINAL.ReadOnly = true;
            this.DATA_FINAL.Width = 120;
            // 
            // DATA_RECEBIMENTO
            // 
            this.DATA_RECEBIMENTO.DataPropertyName = "DATA_RECEBIMENTO";
            this.DATA_RECEBIMENTO.HeaderText = "Data Envio";
            this.DATA_RECEBIMENTO.Name = "DATA_RECEBIMENTO";
            this.DATA_RECEBIMENTO.ReadOnly = true;
            this.DATA_RECEBIMENTO.Width = 120;
            // 
            // DATA_VALIDACAO
            // 
            this.DATA_VALIDACAO.DataPropertyName = "DATA_VALIDACAO";
            this.DATA_VALIDACAO.HeaderText = "Data Validação";
            this.DATA_VALIDACAO.Name = "DATA_VALIDACAO";
            this.DATA_VALIDACAO.ReadOnly = true;
            this.DATA_VALIDACAO.Width = 130;
            // 
            // TEC_NOME
            // 
            this.TEC_NOME.DataPropertyName = "TEC_NOME";
            this.TEC_NOME.HeaderText = "Tec. Responsavel";
            this.TEC_NOME.Name = "TEC_NOME";
            this.TEC_NOME.ReadOnly = true;
            this.TEC_NOME.Width = 160;
            // 
            // TEC_LOGIN
            // 
            this.TEC_LOGIN.DataPropertyName = "TEC_LOGIN";
            this.TEC_LOGIN.HeaderText = "TEC_LOGIN";
            this.TEC_LOGIN.Name = "TEC_LOGIN";
            this.TEC_LOGIN.ReadOnly = true;
            this.TEC_LOGIN.Visible = false;
            // 
            // TEC_SENHA
            // 
            this.TEC_SENHA.DataPropertyName = "TEC_SENHA";
            this.TEC_SENHA.HeaderText = "TEC_SENHA";
            this.TEC_SENHA.Name = "TEC_SENHA";
            this.TEC_SENHA.ReadOnly = true;
            this.TEC_SENHA.Visible = false;
            // 
            // HASH_ENVIO
            // 
            this.HASH_ENVIO.DataPropertyName = "HASH_ENVIO";
            this.HASH_ENVIO.HeaderText = "HASH_ENVIO";
            this.HASH_ENVIO.Name = "HASH_ENVIO";
            this.HASH_ENVIO.ReadOnly = true;
            this.HASH_ENVIO.Visible = false;
            // 
            // EST_CGC
            // 
            this.EST_CGC.DataPropertyName = "EST_CGC";
            this.EST_CGC.HeaderText = "EST_CGC";
            this.EST_CGC.Name = "EST_CGC";
            this.EST_CGC.ReadOnly = true;
            this.EST_CGC.Visible = false;
            // 
            // MENSAGEMVALIDACAO
            // 
            this.MENSAGEMVALIDACAO.DataPropertyName = "MENSAGEMVALIDACAO";
            this.MENSAGEMVALIDACAO.HeaderText = "Mensagem De Erro";
            this.MENSAGEMVALIDACAO.Name = "MENSAGEMVALIDACAO";
            this.MENSAGEMVALIDACAO.ReadOnly = true;
            this.MENSAGEMVALIDACAO.Width = 1000;
            // 
            // frmSngpcHistoricoEnvios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSngpcHistoricoEnvios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmSngpcHistoricoEnvios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSngpcHistoricoEnvios_Load);
            this.Shown += new System.EventHandler(this.frmSngpcHistoricoEnvios_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.dgHistorico.ResumeLayout(false);
            this.dgHistorico.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHistorio)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox dgHistorico;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbTecnico;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgHistorio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtMensagemDeErro;
        private System.Windows.Forms.DataGridViewTextBoxColumn HASH_RETORNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACEITO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_INICIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_FINAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_RECEBIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_VALIDACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEC_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEC_LOGIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEC_SENHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn HASH_ENVIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CGC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MENSAGEMVALIDACAO;
    }
}