﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelatorioDeInventario : Form
    {
        public frmRelatorioDeInventario()
        {
            InitializeComponent();
        }

        private void frmRelatorioDeInventario_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.WindowState = FormWindowState.Maximized;
                MontaDadosEstabelecimento();
                MontaRelatorio();
                this.reportViewer1.RefreshReport();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void MontaDadosEstabelecimento()
        {
            Microsoft.Reporting.WinForms.ReportParameter[] parametro = new ReportParameter[1];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                var busca = new SngpcInventario();
                DataTable dtRelatorio = busca.RelatorioDeInventario(Principal.empAtual, Principal.estAtual);
                var comanda = new ReportDataSource("dsInventario", dtRelatorio);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(comanda);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
