﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC.Relatorios
{
    public partial class frmRelatorioPerda : Form
    {
        string dtInicial, dtFinal;
        char ordem;
        public frmRelatorioPerda(string dtIni, string dtFim, char ordenar)
        {
            InitializeComponent();
            dtInicial = dtIni;
            dtFinal = dtFim;
            ordem = ordenar;
        }

        private void frmRelatorioPerda_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioAnalitico();
            this.rpwRelatorioPerda.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial);
            parametro[2] = new ReportParameter("dtFinal", dtFinal);
            this.rpwRelatorioPerda.LocalReport.SetParameters(parametro);
        }
        public void MontaRelatorioAnalitico()
        {
            SngpcPerda perda = new SngpcPerda();
            DataTable dtPerdas = perda.BuscaPerdasPorPeriodo(dtInicial, dtFinal, ordem);

            var perdas = new ReportDataSource("PerdasMedicamentos", dtPerdas);
            rpwRelatorioPerda.LocalReport.DataSources.Clear();
            rpwRelatorioPerda.LocalReport.DataSources.Add(perdas);
        }
    }
}
