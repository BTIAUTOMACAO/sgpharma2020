﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcPerda : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtPerda = new DataTable();
        private int contRegistros;
        private string[,] dados = new string[8, 3];
        private bool emGrade;
        private ToolStripButton tsbPerda = new ToolStripButton("Perda de Medicamento");
        private ToolStripSeparator tssPerda = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private bool regMS;
        private DateTime data;

        public frmSngpcPerda(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcPerda_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbBMotivo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcPerda");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, contRegistros));
            if (tcPerda.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcPerda.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtPerdaID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbPerda.AutoSize = false;
                this.tsbPerda.Image = Properties.Resources.sngpc;
                this.tsbPerda.Size = new System.Drawing.Size(160, 20);
                this.tsbPerda.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbPerda);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssPerda);
                tsbPerda.Click += delegate
                {
                    var cadPerda = Application.OpenForms.OfType<frmSngpcPerda>().FirstOrDefault();
                    BotoesHabilitados();
                    cadPerda.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbPerda);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssPerda);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtPerdaID.Text = dtPerda.Rows[linha]["PER_SEQ"].ToString();
                cmbMotivo.Text = dtPerda.Rows[linha]["MOTIVO"].ToString();
                txtCodBarras.Text = dtPerda.Rows[linha]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtPerda.Rows[linha]["PROD_DESCR"].ToString();
                txtReg.Text = dtPerda.Rows[linha]["PER_MS"].ToString();

                txtQtde.Text = dtPerda.Rows[linha]["PER_MED_QTDE"].ToString();
                dtData.Value = Convert.ToDateTime(dtPerda.Rows[linha]["PER_DATA"]);
                txtData.Text = Funcoes.ChecaCampoVazio(dtPerda.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtPerda.Rows[linha]["OP_ALTERACAO"].ToString());
                cmbUnidade.Text = dtPerda.Rows[linha]["PROD_UNIDADE"].ToString();
                cmbClasseTera.Text = dtPerda.Rows[linha]["PROD_CLASSE_TERAP"].ToString();

                BuscaLotes();

                cmbLote.Text = dtPerda.Rows[linha]["PER_MED_LOTE"].ToString();
                cmbLote.Enabled = false;

                if (cmbLote.Text == "")
                {
                    txtLote.Visible = true;
                    lblLote.Visible = true;
                    txtLote.Text = dtPerda.Rows[linha]["PER_MED_LOTE"].ToString();
                }
                else
                {
                    txtLote.Visible = false;
                    lblLote.Visible = false;
                }
                txtQtdeEstoque.Text = Convert.ToString(Convert.ToInt32(cmbLote.SelectedValue) + Convert.ToInt32(dtPerda.Rows[linha]["PER_MED_QTDE"].ToString()));
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, linha));
                cmbMotivo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtPerda.Clear();
            dgPerda.DataSource = dtPerda;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            Array.Clear(dados, 0, 24);
            txtBCodBarra.Focus();
        }

        public void LimparFicha()
        {
            txtQtdeEstoque.Text = "0";
            txtPerdaID.Text = "";
            cmbMotivo.SelectedIndex = -1;
            dtData.Value = DateTime.Now;
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtReg.Text = "";
            txtLote.Text = "";
            txtQtde.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbLote.SelectedIndex = -1;
            cmbUnidade.SelectedIndex = -1;
            cmbClasseTera.SelectedIndex = -1;
            Array.Clear(dados, 0, 24);
            cmbMotivo.Focus();
            cmbLote.Enabled = true;
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcPerda.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPerda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPerda.CurrentCell = dgPerda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPerda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPerda.CurrentCell = dgPerda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtPerda.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgPerda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgPerda.CurrentCell = dgPerda.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcPerda.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
            else
            {

                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtPerda.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgPerda.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgPerda.CurrentCell = dgPerda.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    SngpcControleVendas controVenda = new SngpcControleVendas();
                    Produto prod = new Produto();
                    SngpcPerda perda = new SngpcPerda()
                    {
                        SngpcEmpCodigo = Principal.empAtual,
                        SngpcEstCodigo = Principal.estAtual,
                        SngpcPerSeq = Convert.ToInt32(txtPerdaID.Text),
                        SngpcMotivo = Convert.ToInt32(cmbMotivo.SelectedValue),
                        SngpcProdCodigo = txtCodBarras.Text,
                        SngpcMedLote = cmbLote.Text == "Informar Lote" ? txtLote.Text : cmbLote.Text == "" ? txtLote.Text : cmbLote.Text,
                        SngpMedQtde = Convert.ToInt32(txtQtde.Text),
                        SngpDataPerca = Convert.ToDateTime(dtData.Text),
                        SngpMS = txtReg.Text,
                    };

                    DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);
                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_PERDA", "PER_SEQ", txtPerdaID.Text);

                    //BUSCA A DATA DO ULTIMO ENVIO 
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.LeParametro(11, "01", true));

                    if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                    {
                        MessageBox.Show("Este lançamento não pode ser alterado, já foi feito a interface da perda deste medicamento.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbMotivo.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {

                        if (dtProd.Rows.Count == 1 && Principal.dtBusca.Rows.Count == 1 && dtProd.Rows[0]["PROD_CODIGO"].ToString() == Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString())
                        {
                            prod = new Produto()
                            {
                                ProdID = Convert.ToInt32(dtProd.Rows[0]["PROD_ID"]),
                                ProdClasTera = cmbClasseTera.Text == "ANTIMICROBIANO" ? "1" : "2",
                                ProdUnidade = cmbUnidade.Text,
                                ProdRegistroMS = txtReg.Text.Trim(),
                            };

                            BancoDados.AbrirTrans();

                            if (!cmbLote.Text.Equals("Informar Lote"))
                            {
                                controVenda = new SngpcControleVendas()
                                {
                                    ProCodigo = Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString(),
                                    Lote = Principal.dtBusca.Rows[0]["PER_MED_LOTE"].ToString(),
                                    Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["PER_MED_QTDE"]),
                                    Data = DateTime.Now
                                };

                                if (!controVenda.SomaQtdeMesmoLoteEProduto(controVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }

                                controVenda = new SngpcControleVendas()
                                {
                                    ProCodigo = txtCodBarras.Text,
                                    Lote = txtLote.Text,
                                    Qtde = Convert.ToInt32(txtQtde.Text),
                                    Data = DateTime.Now
                                };

                                if (!controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }

                            if (prod.AtualizaProdutoSNGPC(prod, dtProd).Equals(true))
                            {
                                if (perda.AtualizaPerda(perda, Principal.dtBusca).Equals(true))
                                {
                                    BancoDados.FecharTrans();

                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        //decremeta estoque 
                                        AtualizaEstoqueFilial(txtCodBarras.Text, Convert.ToInt32(Principal.dtBusca.Rows[0]["PER_MED_QTDE"]));
                                        //acrecenta estoque
                                        EstoqueFilial(prod.BuscaProdutos(txtCodBarras.Text), "menos");
                                    }

                                    MessageBox.Show("Atualização efetuada com sucesso!", "Perda de Medicamentos", MessageBoxButtons.OK);
                                    LimparFicha();
                                    LimparGrade();
                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar atualização  ", "Perda de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }

                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("101 - Erro ao efetuar atualização  ", "Perda de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }

                }

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Excluir()
        {
            try
            {
                DateTime dtDataLan = Convert.ToDateTime(Funcoes.LeParametro(11, "01", true));

                if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                {
                    MessageBox.Show("Este lançamento não pode ser excluído, já foi feito a interface para esta perda de medicamento.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbMotivo.Focus();
                    Cursor = Cursors.Default;
                    return false;
                }
                else
                {
                    if (txtPerdaID.Text.Trim() != "" || cmbMotivo.SelectedIndex != -1)
                    {
                        if (MessageBox.Show("Confirma exclusão do lançamento de perda de medicamentos?", "Exclusão tabela Perda de Medicamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            SngpcPerda perda = new SngpcPerda();
                            SngpcControleVendas controVenda = new SngpcControleVendas();

                            BancoDados.AbrirTrans();

                            controVenda = new SngpcControleVendas()
                            {
                                ProCodigo = txtCodBarras.Text,
                                Lote = cmbLote.Text,
                                Qtde = Convert.ToInt32(txtQtde.Text),
                                Data = DateTime.Now
                            };

                            if (controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda).Equals(false))
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuara a exclusão  ", "SNGPC Perca de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }

                            if (perda.ExcluirPerda(Convert.ToInt32(txtPerdaID.Text)).Equals(true))
                            {
                                if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                {
                                    AtualizaEstoqueFilial(txtCodBarras.Text, Convert.ToInt32(txtQtde.Text));
                                }

                                Funcoes.GravaLogExclusao("PER_SEQ", txtPerdaID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "SNGPC_PERDA", txtPerdaID.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Exclusão realizado com sucesso", "SNGPC Perca de Produtos", MessageBoxButtons.OK);
                                dtPerda.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgPerda.DataSource = dtPerda;
                                emGrade = false;
                                tcPerda.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                Funcoes.LimpaFormularios(this);
                                txtBCodBarra.Focus();
                                BancoDados.FecharTrans();
                                LimparFicha();
                                LimparGrade();
                                return true;
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                return false;
                            }
                        }
                        return false;
                    }
                    else
                    {
                        MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async void AtualizaEstoqueFilial(string codBarras, int qtd)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + codEstabelecimento + "&codBarras=" + codBarras + "&qtd=" + qtd + "&operacao=mais");
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\PerdasSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: Atualização de Estoque ");
                        }
                    }
                }
            }

        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ConsisteCampos().Equals(true))
                {
                    Produto prod = new Produto();
                    SngpcControleVendas controVenda = new SngpcControleVendas();
                    SngpcPerda perda = new SngpcPerda();
                    string lote = cmbLote.Text == "Informar Lote" ? txtLote.Text : cmbLote.Text;
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                    if (dtData.Value.Date <= dtDataLan)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência.\nCaso queria INCLUIR entre na Tela de Transfência e FAÇA A CORREÇÃO DO PERÍODO e tente novamente.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbMotivo.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);
                        if (dtProd.Rows.Count == 1)
                        {
                            prod = new Produto()
                            {
                                ProdID = Convert.ToInt32(dtProd.Rows[0]["PROD_ID"]),
                                ProdClasTera = cmbClasseTera.Text,
                                ProdUnidade = cmbUnidade.Text,
                                ProdRegistroMS = txtReg.Text.Trim(),
                            };

                            BancoDados.AbrirTrans();

                            if (!cmbLote.Text.Equals("Informar Lote"))
                            {
                                controVenda = new SngpcControleVendas()
                                {
                                    ProCodigo = txtCodBarras.Text,
                                    Lote = lote,
                                    Qtde = Convert.ToInt32(txtQtde.Text),
                                    Data = DateTime.Now
                                };

                                if (!controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }

                            if (prod.AtualizaProdutoSNGPC(prod, dtProd).Equals(true))
                            {
                                perda = new SngpcPerda()
                                {
                                    SngpcEmpCodigo = Principal.empAtual,
                                    SngpcEstCodigo = Principal.estAtual,
                                    SngpcPerSeq = Funcoes.IdentificaVerificaID("SNGPC_PERDA", "PER_SEQ", 0, ""),
                                    SngpcMotivo = Convert.ToInt32(cmbMotivo.SelectedValue),
                                    SngpcProdCodigo = txtCodBarras.Text,
                                    SngpcMedLote = lote,
                                    SngpMedQtde = Convert.ToInt32(txtQtde.Text),
                                    SngpDataPerca = Convert.ToDateTime(dtData.Text),
                                    SngpMS = txtReg.Text,
                                };

                                if (perda.IncluirPerda(perda).Equals(true))
                                {
                                    BancoDados.FecharTrans();

                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        EstoqueFilial(dtProd, "menos");
                                    }

                                    MessageBox.Show("Cadastrado efetuada com sucesso!", "Perda de Medicamentos", MessageBoxButtons.OK);
                                    LimparFicha();
                                    LimparGrade();
                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Perda de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("101 - Erro ao efetuar atualização  ", "Perda de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Código de Barras não encontrado  ", "Perda de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }
                return false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async void EstoqueFilial(DataTable dtProd, string operacao)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + txtCodBarras.Text + "&descricao=" + txtDescr.Text + "&qtde=" + txtQtde.Text + "&precoCusto=" + dtProd.Rows[0]["PROD_ULTCUSME"].ToString().Replace(",", ".") + "&precoVenda=" + dtProd.Rows[0]["PRE_VALOR"].ToString().Replace(",", ".") + "&codEstabelecimento=" + codEstabelecimento + "&operacao=" + operacao);
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\PerdaSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: " + operacao);
                        }
                    }
                }
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";


            if (Equals(cmbMotivo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um motivo.";
                Funcoes.Avisa();
                cmbMotivo.Focus();
                return false;
            }
            if (Equals(cmbClasseTera.Text, ""))
            {
                Principal.mensagem = "Selecione a Classe Terapêutica .";
                Funcoes.Avisa();
                cmbMotivo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtReg.Text.Trim()))
            {
                Principal.mensagem = "Reg. Min. da Saúde não pode ser em branco.";
                Funcoes.Avisa();
                txtReg.Focus();
                return false;

            }
            if (txtLote.Visible == true && String.IsNullOrEmpty(txtLote.Text.Trim()))
            {
                Principal.mensagem = "Lote não pode ser em branco.";
                Funcoes.Avisa();
                txtLote.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || Equals(txtQtde.Text, 0))
            {
                Principal.mensagem = "Qtde não pode ser zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (cmbLote.Text != "Informar Lote" && Convert.ToInt32(txtQtde.Text) > Convert.ToInt32(txtQtdeEstoque.Text))
            {
                Principal.mensagem = "Qtde não pode ser maior doque a Qtde em Estoque";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            return true;
        }

        private void tcPerda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcPerda.SelectedTab == tpGrade)
            {
                dgPerda.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcPerda.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcPerda");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, contRegistros));
                    cmbMotivo.Focus();
                    dtData.Value = DateTime.Now;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                }
            }
        }

        public bool CarregaCombos()
        {
            try
            {
                //CARREGA MOTIVOS DE PERDA//
                SngpcTabela tabela = new SngpcTabela();
                Principal.dtPesq = tabela.BuscaTabelaPorTipo(6);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um motivo de perda,\npara cadastrar uma perda de medicamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbMotivo.DataSource = Principal.dtPesq;
                    cmbMotivo.DisplayMember = "TAB_DESCRICAO";
                    cmbMotivo.ValueMember = "TAB_SEQ";
                    cmbMotivo.SelectedIndex = -1;


                    cmbBMotivo.DataSource = Principal.dtPesq;
                    cmbBMotivo.DisplayMember = "TAB_DESCRICAO";
                    cmbBMotivo.ValueMember = "TAB_SEQ";
                    cmbBMotivo.SelectedIndex = -1;

                }

                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("UNI_CODIGO", "UNI_DESC_ABREV", "TIPO_UNIDADE", false, "UNI_LIBERADO = 'S'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Unidade,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUnidade.DataSource = Principal.dtPesq;
                    cmbUnidade.DisplayMember = "UNI_DESC_ABREV";
                    cmbUnidade.ValueMember = "UNI_CODIGO";

                    cmbUnidade.SelectedIndex = -1;
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void frmSngpcPerda_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SngpcPerda perda = new SngpcPerda()
                {
                    SngpcMotivo = Convert.ToInt32(cmbBMotivo.SelectedValue),
                    SngpcProdCodigo = txtBCodBarra.Text,
                    SngpMS = txtBReg.Text,
                };

                dtPerda = perda.BuscarProdutos(perda, out strOrdem);

                if (dtPerda.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtPerda.Rows.Count;
                    dgPerda.DataSource = dtPerda;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, 0));
                    dgPerda.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPerda.DataSource = dtPerda;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    cmbBMotivo.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbMotivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtData.Focus();
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUnidade.Focus();
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (Principal.mdiPrincipal.btnIncluir.Enabled == true && txtCodBarras.Text.Trim() != "")
                {
                    if (BuscaProdutos(txtCodBarras.Text, "").Equals(true))
                    {
                        BuscaLotes();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        //public bool LeProdutos()
        //{
        //    try
        //    {
        //        Principal.strSql = "SELECT PROD_DESCR, PROD_REGISTRO_MS FROM PRODUTOS WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = "
        //            + "(SELECT PROD_CODIGO FROM PRODUTOS_BARRA WHERE EST_CODIGO = " + Principal.estAtual + " AND COD_BARRA = '" + txtCodBarras.Text + "') "
        //            + "AND PROD_LIBERADO = 'S' AND TAB_CODIGO = 1";
        //        Principal.dtPesq = BancoDados.selecionarRegistros(Principal.strSql);
        //        if (Principal.dtPesq.Rows.Count != 0)
        //        {
        //            if (Principal.dtPesq.Rows[0]["PROD_REGISTRO_MS"].ToString() == "")
        //            {
        //                MessageBox.Show("Registro do ministério da saúde não cadastrado para este produto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                txtCodBarras.Text = "";
        //                txtDescr.Text = "";
        //                txtReg.Text = "";
        //                txtCodBarras.Focus();
        //                regMS = true;
        //                return false;
        //            }
        //            else
        //                regMS = false;
        //                txtDescr.Text = Funcoes.ChecaCampoVazio(Principal.dtPesq.Rows[0]["PROD_DESCR"].ToString());
        //                txtReg.Text = Funcoes.ChecaCampoVazio(Principal.dtPesq.Rows[0]["PROD_REGISTRO_MS"].ToString());
        //                txtLote.Focus();
        //                return true;

        //        }
        //        else
        //        {
        //            regMS = false;
        //            txtCodBarras.Text = "";
        //            txtDescr.Text = "";
        //            txtReg.Text = "";
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        return false;
        //    }
        //}

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Principal.mdiPrincipal.btnIncluir.Enabled == true && txtCodBarras.Text.Trim() == "")
                {
                    if (BuscaProdutos("", txtDescr.Text).Equals(true))
                    {
                        BuscaLotes();
                    }

                }
                cmbMotivo.Focus();
            }

        }

        private void txtReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtLote.Focus();
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void cmbBMotivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBMotivo.SelectedIndex.Equals(-1))
                {
                    txtBCodBarra.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCodBarra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                txtBReg.Focus();
            }
        }

        private void txtBReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }


        private void dgPerda_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, contRegistros));
            emGrade = true;
        }

        private void dgPerda_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcPerda.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgPerda_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtPerda.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtPerda.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtPerda.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgPerda_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgPerda.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtPerda = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPerda.Columns[e.ColumnIndex].Name + " DESC");
                        dgPerda.DataSource = dtPerda;
                        decrescente = true;
                    }
                    else
                    {
                        dtPerda = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgPerda.Columns[e.ColumnIndex].Name + " ASC");
                        dgPerda.DataSource = dtPerda;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtPerda, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgPerda.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgPerda);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgPerda.RowCount;
                    for (i = 0; i <= dgPerda.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgPerda[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgPerda.ColumnCount; k++)
                    {
                        if (dgPerda.Columns[k].Visible == true)
                        {
                            switch (dgPerda.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgPerda.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPerda.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgPerda.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPerda.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Cód. de Barras":
                                    numCel = Principal.GetColunaExcel(dgPerda.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPerda.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Reg. Min. da Saúde":
                                    numCel = Principal.GetColunaExcel(dgPerda.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgPerda.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgPerda.ColumnCount : indice) + Convert.ToString(dgPerda.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.imgLote, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtPerdaID") && (control.Name != "txtQtdeEstoque") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgPerda.ColumnCount; i++)
                {
                    if (dgPerda.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgPerda.ColumnCount];
                string[] coluna = new string[dgPerda.ColumnCount];

                for (int i = 0; i < dgPerda.ColumnCount; i++)
                {
                    grid[i] = dgPerda.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgPerda.ColumnCount; i++)
                {
                    if (dgPerda.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgPerda.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgPerda.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgPerda.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgPerda.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgPerda.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgPerda.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgPerda.ColumnCount; i++)
                        {
                            dgPerda.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcPerda.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcPerda.SelectedTab = tpFicha;
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        public bool BuscaLotes()
        {

            SngpcControleVendas vendas = new SngpcControleVendas();
            DataTable dtLotes = vendas.BuscaLotes(txtCodBarras.Text);

            dtLotes.Rows.Add(0, "Informar Lote");
            cmbLote.DataSource = dtLotes;
            cmbLote.DisplayMember = "LOTE";
            cmbLote.ValueMember = "QTDE";
            cmbLote.SelectedIndex = -1;


            return true;

        }

        public bool BuscaProdutos(string prodCodigo, string descricao)
        {
            Produto pro = new Produto();

            if (descricao != "")
            {
                using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                {
                    buscaProd.BuscaProd(txtDescr.Text.ToUpper(), false);
                    buscaProd.ShowDialog();
                }
            }

            string codBarras = prodCodigo != "" ? prodCodigo : Principal.codBarra;

            DataTable dtProduto = pro.BuscaProdutos(codBarras);

            if (dtProduto.Rows.Count != 0)
            {
                txtCodBarras.Text = dtProduto.Rows[0]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                txtReg.Text = dtProduto.Rows[0]["PROD_REGISTRO_MS"].ToString();
                cmbUnidade.Text = dtProduto.Rows[0]["PROD_UNIDADE"].ToString();
                cmbClasseTera.Text = dtProduto.Rows[0]["PROD_CLASSE_TERAP"].ToString();


                return true;
            }
            else
            {
                LimparFicha();
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
                return false;
            }
        }

        private void cmbUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbClasseTera.Focus();
        }

        private void cmbClasseTera_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtReg.Focus();
        }

        private void tpGrade_Click(object sender, EventArgs e)
        {

        }

        private void cmbLote_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLote.Text == "Informar Lote")
            {
                txtLote.Visible = true;
                lblLote.Visible = true;
                imgLote.Visible = true;
                txtQtdeEstoque.Text = "0";

            }
            else
            {
                txtLote.Visible = false;
                lblLote.Visible = false;
                imgLote.Visible = false;

                txtQtdeEstoque.Text = Convert.ToString(cmbLote.SelectedValue);

            }
        }
    }
}
