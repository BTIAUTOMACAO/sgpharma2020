﻿namespace GestaoAdministrativa.SNGPC
{
    partial class frmSngpcVendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSngpcVendas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcVendas = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBCrm = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBData = new System.Windows.Forms.MaskedTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtBNome = new System.Windows.Forms.TextBox();
            this.txtBDocto = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgVendas = new System.Windows.Forms.DataGridView();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_SEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_COMP_TD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_COMP_OE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_COMP_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_COMP_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_COMP_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PAC_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PAC_IDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PAC_UNIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PAC_SEXO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PRESC_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PRESC_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PRESC_CP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_NUM_NOT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PRESC_DT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_PRESC_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_USO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_TP_RECEITA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_REGISTRO_MS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_MED_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VEN_MED_LOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_ALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsmVendas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtQtdeEstoque = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbLote = new System.Windows.Forms.ComboBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtLote = new System.Windows.Forms.TextBox();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.txtReg = new System.Windows.Forms.TextBox();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.cmbTipoRec = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbUMed = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cmbMUF = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.dtReceita = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.txtNReceita = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbConselho = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMedico = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCrm = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.cmbPSexo = new System.Windows.Forms.ComboBox();
            this.cmbPUnidade = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPIdade = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPNome = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dtData = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDocto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbUF = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbOrgao = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.imgLote = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblLote = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbDocto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtVenID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcVendas.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).BeginInit();
            this.tsmVendas.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcVendas);
            this.groupBox1.Location = new System.Drawing.Point(9, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(973, 560);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(973, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Venda de Medicamentos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(967, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcVendas
            // 
            this.tcVendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcVendas.Controls.Add(this.tpGrade);
            this.tcVendas.Controls.Add(this.tpFicha);
            this.tcVendas.Location = new System.Drawing.Point(6, 33);
            this.tcVendas.Name = "tcVendas";
            this.tcVendas.SelectedIndex = 0;
            this.tcVendas.Size = new System.Drawing.Size(964, 499);
            this.tcVendas.TabIndex = 40;
            this.tcVendas.SelectedIndexChanged += new System.EventHandler(this.tcVendas_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBCrm);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Controls.Add(this.txtBData);
            this.tpGrade.Controls.Add(this.label31);
            this.tpGrade.Controls.Add(this.txtBNome);
            this.tpGrade.Controls.Add(this.txtBDocto);
            this.tpGrade.Controls.Add(this.label20);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.dgVendas);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(956, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(736, 418);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 163;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBCrm
            // 
            this.txtBCrm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCrm.Location = new System.Drawing.Point(554, 434);
            this.txtBCrm.MaxLength = 20;
            this.txtBCrm.Name = "txtBCrm";
            this.txtBCrm.Size = new System.Drawing.Size(171, 22);
            this.txtBCrm.TabIndex = 161;
            this.txtBCrm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCrm_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(556, 416);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 16);
            this.label15.TabIndex = 159;
            this.label15.Text = "Registro Ms";
            // 
            // txtBData
            // 
            this.txtBData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBData.Location = new System.Drawing.Point(448, 434);
            this.txtBData.Mask = "00/00/0000";
            this.txtBData.Name = "txtBData";
            this.txtBData.Size = new System.Drawing.Size(100, 22);
            this.txtBData.TabIndex = 158;
            this.txtBData.ValidatingType = typeof(System.DateTime);
            this.txtBData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBData_KeyPress);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(445, 415);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 16);
            this.label31.TabIndex = 157;
            this.label31.Text = "Dt. da Venda";
            // 
            // txtBNome
            // 
            this.txtBNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNome.Location = new System.Drawing.Point(153, 434);
            this.txtBNome.MaxLength = 80;
            this.txtBNome.Name = "txtBNome";
            this.txtBNome.Size = new System.Drawing.Size(285, 22);
            this.txtBNome.TabIndex = 154;
            this.txtBNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNome_KeyPress);
            // 
            // txtBDocto
            // 
            this.txtBDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBDocto.Location = new System.Drawing.Point(6, 434);
            this.txtBDocto.MaxLength = 20;
            this.txtBDocto.Name = "txtBDocto";
            this.txtBDocto.Size = new System.Drawing.Size(139, 22);
            this.txtBDocto.TabIndex = 149;
            this.txtBDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBDocto_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(3, 416);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 16);
            this.label20.TabIndex = 148;
            this.label20.Text = "N° Documento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(150, 416);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 16);
            this.label1.TabIndex = 141;
            this.label1.Text = "Nome";
            // 
            // dgVendas
            // 
            this.dgVendas.AllowUserToAddRows = false;
            this.dgVendas.AllowUserToDeleteRows = false;
            this.dgVendas.AllowUserToOrderColumns = true;
            this.dgVendas.AllowUserToResizeColumns = false;
            this.dgVendas.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgVendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVendas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgVendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVendas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EST_CODIGO,
            this.VEN_SEQ,
            this.VEN_COMP_TD,
            this.VEN_COMP_OE,
            this.VEN_COMP_UF,
            this.VEN_COMP_DOCTO,
            this.VEN_COMP_NOME,
            this.VEN_DATA,
            this.VEN_PAC_NOME,
            this.VEN_PAC_IDADE,
            this.VEN_PAC_UNIDADE,
            this.VEN_PAC_SEXO,
            this.VEN_PRESC_NUMERO,
            this.VEN_PRESC_NOME,
            this.VEN_PRESC_CP,
            this.VEN_NUM_NOT,
            this.VEN_PRESC_DT,
            this.VEN_PRESC_UF,
            this.VEN_USO,
            this.VEN_TP_RECEITA,
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.PROD_REGISTRO_MS,
            this.VEN_MED_QTDE,
            this.VEN_MED_LOTE,
            this.DT_ALTERACAO,
            this.OP_ALTERACAO,
            this.DT_CADASTRO,
            this.OP_CADASTRO});
            this.dgVendas.ContextMenuStrip = this.tsmVendas;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgVendas.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgVendas.GridColor = System.Drawing.Color.LightGray;
            this.dgVendas.Location = new System.Drawing.Point(6, 6);
            this.dgVendas.MultiSelect = false;
            this.dgVendas.Name = "dgVendas";
            this.dgVendas.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgVendas.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgVendas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVendas.Size = new System.Drawing.Size(944, 394);
            this.dgVendas.TabIndex = 1;
            this.dgVendas.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgVendas_CellMouseClick);
            this.dgVendas.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgVendas_CellMouseDoubleClick);
            this.dgVendas.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgVendas_ColumnHeaderMouseClick);
            this.dgVendas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgVendas_KeyDown);
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "Estab. ID";
            this.EST_CODIGO.MaxInputLength = 3;
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.Width = 90;
            // 
            // VEN_SEQ
            // 
            this.VEN_SEQ.DataPropertyName = "VEN_SEQ";
            this.VEN_SEQ.HeaderText = "Venda ID";
            this.VEN_SEQ.MaxInputLength = 18;
            this.VEN_SEQ.Name = "VEN_SEQ";
            this.VEN_SEQ.ReadOnly = true;
            // 
            // VEN_COMP_TD
            // 
            this.VEN_COMP_TD.DataPropertyName = "VEN_COMP_TD";
            this.VEN_COMP_TD.HeaderText = "Tipo de Documento";
            this.VEN_COMP_TD.MaxInputLength = 100;
            this.VEN_COMP_TD.Name = "VEN_COMP_TD";
            this.VEN_COMP_TD.ReadOnly = true;
            this.VEN_COMP_TD.Width = 220;
            // 
            // VEN_COMP_OE
            // 
            this.VEN_COMP_OE.DataPropertyName = "VEN_COMP_OE";
            this.VEN_COMP_OE.HeaderText = "Orgão Expedidor";
            this.VEN_COMP_OE.MaxInputLength = 100;
            this.VEN_COMP_OE.Name = "VEN_COMP_OE";
            this.VEN_COMP_OE.ReadOnly = true;
            this.VEN_COMP_OE.Width = 250;
            // 
            // VEN_COMP_UF
            // 
            this.VEN_COMP_UF.DataPropertyName = "VEN_COMP_UF";
            this.VEN_COMP_UF.HeaderText = "UF Docto.";
            this.VEN_COMP_UF.MaxInputLength = 2;
            this.VEN_COMP_UF.Name = "VEN_COMP_UF";
            this.VEN_COMP_UF.ReadOnly = true;
            // 
            // VEN_COMP_DOCTO
            // 
            this.VEN_COMP_DOCTO.DataPropertyName = "VEN_COMP_DOCTO";
            this.VEN_COMP_DOCTO.HeaderText = "N° do Documento";
            this.VEN_COMP_DOCTO.MaxInputLength = 30;
            this.VEN_COMP_DOCTO.Name = "VEN_COMP_DOCTO";
            this.VEN_COMP_DOCTO.ReadOnly = true;
            this.VEN_COMP_DOCTO.Width = 200;
            // 
            // VEN_COMP_NOME
            // 
            this.VEN_COMP_NOME.DataPropertyName = "VEN_COMP_NOME";
            this.VEN_COMP_NOME.HeaderText = "Nome";
            this.VEN_COMP_NOME.MaxInputLength = 80;
            this.VEN_COMP_NOME.Name = "VEN_COMP_NOME";
            this.VEN_COMP_NOME.ReadOnly = true;
            this.VEN_COMP_NOME.Width = 300;
            // 
            // VEN_DATA
            // 
            this.VEN_DATA.DataPropertyName = "VEN_DATA";
            this.VEN_DATA.HeaderText = "Data da Venda";
            this.VEN_DATA.MaxInputLength = 10;
            this.VEN_DATA.Name = "VEN_DATA";
            this.VEN_DATA.ReadOnly = true;
            this.VEN_DATA.Width = 125;
            // 
            // VEN_PAC_NOME
            // 
            this.VEN_PAC_NOME.DataPropertyName = "VEN_PAC_NOME";
            this.VEN_PAC_NOME.HeaderText = "Nome do Paciente";
            this.VEN_PAC_NOME.MaxInputLength = 80;
            this.VEN_PAC_NOME.Name = "VEN_PAC_NOME";
            this.VEN_PAC_NOME.ReadOnly = true;
            this.VEN_PAC_NOME.Width = 300;
            // 
            // VEN_PAC_IDADE
            // 
            this.VEN_PAC_IDADE.DataPropertyName = "VEN_PAC_IDADE";
            this.VEN_PAC_IDADE.HeaderText = "Idade";
            this.VEN_PAC_IDADE.MaxInputLength = 3;
            this.VEN_PAC_IDADE.Name = "VEN_PAC_IDADE";
            this.VEN_PAC_IDADE.ReadOnly = true;
            // 
            // VEN_PAC_UNIDADE
            // 
            this.VEN_PAC_UNIDADE.DataPropertyName = "VEN_PAC_UNIDADE";
            this.VEN_PAC_UNIDADE.HeaderText = "Un. da Idade";
            this.VEN_PAC_UNIDADE.MaxInputLength = 10;
            this.VEN_PAC_UNIDADE.Name = "VEN_PAC_UNIDADE";
            this.VEN_PAC_UNIDADE.ReadOnly = true;
            this.VEN_PAC_UNIDADE.Width = 125;
            // 
            // VEN_PAC_SEXO
            // 
            this.VEN_PAC_SEXO.DataPropertyName = "VEN_PAC_SEXO";
            this.VEN_PAC_SEXO.HeaderText = "Sexo";
            this.VEN_PAC_SEXO.MaxInputLength = 1;
            this.VEN_PAC_SEXO.Name = "VEN_PAC_SEXO";
            this.VEN_PAC_SEXO.ReadOnly = true;
            this.VEN_PAC_SEXO.Width = 75;
            // 
            // VEN_PRESC_NUMERO
            // 
            this.VEN_PRESC_NUMERO.DataPropertyName = "VEN_PRESC_NUMERO";
            this.VEN_PRESC_NUMERO.HeaderText = "CRM";
            this.VEN_PRESC_NUMERO.MaxInputLength = 20;
            this.VEN_PRESC_NUMERO.Name = "VEN_PRESC_NUMERO";
            this.VEN_PRESC_NUMERO.ReadOnly = true;
            // 
            // VEN_PRESC_NOME
            // 
            this.VEN_PRESC_NOME.DataPropertyName = "VEN_PRESC_NOME";
            this.VEN_PRESC_NOME.HeaderText = "Nome do Médico";
            this.VEN_PRESC_NOME.MaxInputLength = 50;
            this.VEN_PRESC_NOME.Name = "VEN_PRESC_NOME";
            this.VEN_PRESC_NOME.ReadOnly = true;
            this.VEN_PRESC_NOME.Width = 300;
            // 
            // VEN_PRESC_CP
            // 
            this.VEN_PRESC_CP.DataPropertyName = "VEN_PRESC_CP";
            this.VEN_PRESC_CP.HeaderText = "Cons. Profissional";
            this.VEN_PRESC_CP.MaxInputLength = 10;
            this.VEN_PRESC_CP.Name = "VEN_PRESC_CP";
            this.VEN_PRESC_CP.ReadOnly = true;
            this.VEN_PRESC_CP.Width = 250;
            // 
            // VEN_NUM_NOT
            // 
            this.VEN_NUM_NOT.DataPropertyName = "VEN_NUM_NOT";
            this.VEN_NUM_NOT.HeaderText = "N° Receita";
            this.VEN_NUM_NOT.MaxInputLength = 10;
            this.VEN_NUM_NOT.Name = "VEN_NUM_NOT";
            this.VEN_NUM_NOT.ReadOnly = true;
            // 
            // VEN_PRESC_DT
            // 
            this.VEN_PRESC_DT.DataPropertyName = "VEN_PRESC_DT";
            this.VEN_PRESC_DT.HeaderText = "Dt. Receita";
            this.VEN_PRESC_DT.MaxInputLength = 10;
            this.VEN_PRESC_DT.Name = "VEN_PRESC_DT";
            this.VEN_PRESC_DT.ReadOnly = true;
            // 
            // VEN_PRESC_UF
            // 
            this.VEN_PRESC_UF.DataPropertyName = "VEN_PRESC_UF";
            this.VEN_PRESC_UF.HeaderText = "UF Receita";
            this.VEN_PRESC_UF.MaxInputLength = 2;
            this.VEN_PRESC_UF.Name = "VEN_PRESC_UF";
            this.VEN_PRESC_UF.ReadOnly = true;
            // 
            // VEN_USO
            // 
            this.VEN_USO.DataPropertyName = "VEN_USO";
            this.VEN_USO.HeaderText = "Uso do Medicamento";
            this.VEN_USO.MaxInputLength = 10;
            this.VEN_USO.Name = "VEN_USO";
            this.VEN_USO.ReadOnly = true;
            this.VEN_USO.Width = 200;
            // 
            // VEN_TP_RECEITA
            // 
            this.VEN_TP_RECEITA.DataPropertyName = "VEN_TP_RECEITA";
            this.VEN_TP_RECEITA.HeaderText = "Tipo da Receita";
            this.VEN_TP_RECEITA.MaxInputLength = 100;
            this.VEN_TP_RECEITA.Name = "VEN_TP_RECEITA";
            this.VEN_TP_RECEITA.ReadOnly = true;
            this.VEN_TP_RECEITA.Width = 300;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cod. Barras";
            this.PROD_CODIGO.MaxInputLength = 18;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 130;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.Width = 150;
            // 
            // PROD_REGISTRO_MS
            // 
            this.PROD_REGISTRO_MS.DataPropertyName = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.HeaderText = "Reg. Min. da Saúde";
            this.PROD_REGISTRO_MS.MaxInputLength = 13;
            this.PROD_REGISTRO_MS.Name = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.ReadOnly = true;
            this.PROD_REGISTRO_MS.Width = 150;
            // 
            // VEN_MED_QTDE
            // 
            this.VEN_MED_QTDE.DataPropertyName = "VEN_MED_QTDE";
            this.VEN_MED_QTDE.HeaderText = "Qtde.";
            this.VEN_MED_QTDE.MaxInputLength = 3;
            this.VEN_MED_QTDE.Name = "VEN_MED_QTDE";
            this.VEN_MED_QTDE.ReadOnly = true;
            this.VEN_MED_QTDE.Width = 75;
            // 
            // VEN_MED_LOTE
            // 
            this.VEN_MED_LOTE.DataPropertyName = "VEN_MED_LOTE";
            this.VEN_MED_LOTE.HeaderText = "Lote";
            this.VEN_MED_LOTE.MaxInputLength = 10;
            this.VEN_MED_LOTE.Name = "VEN_MED_LOTE";
            this.VEN_MED_LOTE.ReadOnly = true;
            this.VEN_MED_LOTE.Width = 75;
            // 
            // DT_ALTERACAO
            // 
            this.DT_ALTERACAO.DataPropertyName = "DT_ALTERACAO";
            dataGridViewCellStyle3.Format = "G";
            this.DT_ALTERACAO.DefaultCellStyle = dataGridViewCellStyle3;
            this.DT_ALTERACAO.HeaderText = "Data Alteração";
            this.DT_ALTERACAO.MaxInputLength = 20;
            this.DT_ALTERACAO.Name = "DT_ALTERACAO";
            this.DT_ALTERACAO.ReadOnly = true;
            this.DT_ALTERACAO.Width = 150;
            // 
            // OP_ALTERACAO
            // 
            this.OP_ALTERACAO.DataPropertyName = "OP_ALTERACAO";
            this.OP_ALTERACAO.HeaderText = "Usuário Alteração";
            this.OP_ALTERACAO.MaxInputLength = 25;
            this.OP_ALTERACAO.Name = "OP_ALTERACAO";
            this.OP_ALTERACAO.ReadOnly = true;
            this.OP_ALTERACAO.Width = 150;
            // 
            // DT_CADASTRO
            // 
            this.DT_CADASTRO.DataPropertyName = "DT_CADASTRO";
            dataGridViewCellStyle4.Format = "G";
            this.DT_CADASTRO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DT_CADASTRO.HeaderText = "Data do Cadastro";
            this.DT_CADASTRO.MaxInputLength = 20;
            this.DT_CADASTRO.Name = "DT_CADASTRO";
            this.DT_CADASTRO.ReadOnly = true;
            this.DT_CADASTRO.Width = 150;
            // 
            // OP_CADASTRO
            // 
            this.OP_CADASTRO.DataPropertyName = "OP_CADASTRO";
            this.OP_CADASTRO.HeaderText = "Usuário Cadastro";
            this.OP_CADASTRO.MaxInputLength = 25;
            this.OP_CADASTRO.Name = "OP_CADASTRO";
            this.OP_CADASTRO.ReadOnly = true;
            this.OP_CADASTRO.Width = 150;
            // 
            // tsmVendas
            // 
            this.tsmVendas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsmVendas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExportar});
            this.tsmVendas.Name = "tsmVendas";
            this.tsmVendas.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExportar
            // 
            this.tsmExportar.Name = "tsmExportar";
            this.tsmExportar.Size = new System.Drawing.Size(191, 22);
            this.tsmExportar.Text = "Exportar para Excel";
            this.tsmExportar.Click += new System.EventHandler(this.tsmExportar_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(956, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtQtdeEstoque);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.pictureBox20);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cmbLote);
            this.groupBox2.Controls.Add(this.pictureBox19);
            this.groupBox2.Controls.Add(this.pictureBox18);
            this.groupBox2.Controls.Add(this.pictureBox17);
            this.groupBox2.Controls.Add(this.pictureBox16);
            this.groupBox2.Controls.Add(this.pictureBox15);
            this.groupBox2.Controls.Add(this.pictureBox14);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.txtLote);
            this.groupBox2.Controls.Add(this.txtQtde);
            this.groupBox2.Controls.Add(this.txtReg);
            this.groupBox2.Controls.Add(this.txtDescr);
            this.groupBox2.Controls.Add(this.txtCodBarras);
            this.groupBox2.Controls.Add(this.cmbTipoRec);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.cmbUMed);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.cmbMUF);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.dtReceita);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.txtNReceita);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.cmbConselho);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.txtMedico);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.txtCrm);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.dtData);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDocto);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbUF);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbOrgao);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.imgLote);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Controls.Add(this.pictureBox8);
            this.groupBox2.Controls.Add(this.pictureBox7);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.lblLote);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cmbDocto);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.txtVenID);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 458);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // txtQtdeEstoque
            // 
            this.txtQtdeEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtQtdeEstoque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtdeEstoque.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdeEstoque.Location = new System.Drawing.Point(246, 392);
            this.txtQtdeEstoque.MaxLength = 3;
            this.txtQtdeEstoque.Name = "txtQtdeEstoque";
            this.txtQtdeEstoque.ReadOnly = true;
            this.txtQtdeEstoque.Size = new System.Drawing.Size(114, 22);
            this.txtQtdeEstoque.TabIndex = 207;
            this.txtQtdeEstoque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(244, 372);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 16);
            this.label11.TabIndex = 225;
            this.label11.Text = "Qtde. em Estoque";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(58, 372);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(16, 16);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox20.TabIndex = 224;
            this.pictureBox20.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(13, 372);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 16);
            this.label12.TabIndex = 223;
            this.label12.Text = "Lote";
            // 
            // cmbLote
            // 
            this.cmbLote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLote.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cmbLote.FormattingEnabled = true;
            this.cmbLote.Location = new System.Drawing.Point(16, 392);
            this.cmbLote.Name = "cmbLote";
            this.cmbLote.Size = new System.Drawing.Size(215, 24);
            this.cmbLote.TabIndex = 206;
            this.cmbLote.SelectedIndexChanged += new System.EventHandler(this.cmbLote_SelectedIndexChanged);
            this.cmbLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLote_KeyPress);
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(633, 274);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(16, 16);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox19.TabIndex = 220;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.MouseHover += new System.EventHandler(this.pictureBox19_MouseHover);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(469, 274);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(16, 16);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox18.TabIndex = 219;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.MouseHover += new System.EventHandler(this.pictureBox18_MouseHover);
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(283, 274);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(16, 16);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox17.TabIndex = 218;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.MouseHover += new System.EventHandler(this.pictureBox17_MouseHover);
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(283, 2746);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(16, 16);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox16.TabIndex = 217;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseHover += new System.EventHandler(this.pictureBox16_MouseHover);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(90, 274);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(16, 16);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox15.TabIndex = 216;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(729, 228);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(16, 16);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox14.TabIndex = 215;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(229, 228);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(16, 16);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox13.TabIndex = 214;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseHover += new System.EventHandler(this.pictureBox13_MouseHover);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(56, 228);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(16, 16);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox12.TabIndex = 213;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(621, 106);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 212;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(890, 58);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 209;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(622, 58);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 208;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // txtLote
            // 
            this.txtLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLote.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLote.Location = new System.Drawing.Point(376, 392);
            this.txtLote.MaxLength = 20;
            this.txtLote.Name = "txtLote";
            this.txtLote.Size = new System.Drawing.Size(109, 22);
            this.txtLote.TabIndex = 208;
            this.txtLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLote_KeyPress);
            // 
            // txtQtde
            // 
            this.txtQtde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtde.Location = new System.Drawing.Point(497, 392);
            this.txtQtde.MaxLength = 3;
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(58, 22);
            this.txtQtde.TabIndex = 209;
            this.txtQtde.Text = "0";
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtde_KeyPress);
            // 
            // txtReg
            // 
            this.txtReg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReg.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReg.Location = new System.Drawing.Point(729, 343);
            this.txtReg.MaxLength = 13;
            this.txtReg.Name = "txtReg";
            this.txtReg.ReadOnly = true;
            this.txtReg.Size = new System.Drawing.Size(195, 22);
            this.txtReg.TabIndex = 205;
            this.txtReg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReg_KeyPress);
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(171, 343);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(547, 22);
            this.txtDescr.TabIndex = 204;
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodBarras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodBarras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(16, 343);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(144, 22);
            this.txtCodBarras.TabIndex = 203;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // cmbTipoRec
            // 
            this.cmbTipoRec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoRec.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoRec.FormattingEnabled = true;
            this.cmbTipoRec.Location = new System.Drawing.Point(497, 291);
            this.cmbTipoRec.Name = "cmbTipoRec";
            this.cmbTipoRec.Size = new System.Drawing.Size(427, 24);
            this.cmbTipoRec.TabIndex = 202;
            this.cmbTipoRec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTipoRec_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(494, 274);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(133, 16);
            this.label30.TabIndex = 201;
            this.label30.Text = "Tipo do Receituário";
            // 
            // cmbUMed
            // 
            this.cmbUMed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUMed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUMed.FormattingEnabled = true;
            this.cmbUMed.Location = new System.Drawing.Point(329, 291);
            this.cmbUMed.Name = "cmbUMed";
            this.cmbUMed.Size = new System.Drawing.Size(156, 24);
            this.cmbUMed.TabIndex = 200;
            this.cmbUMed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUMed_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(326, 274);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 16);
            this.label29.TabIndex = 199;
            this.label29.Text = "Uso do Medicamento";
            // 
            // cmbMUF
            // 
            this.cmbMUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMUF.FormattingEnabled = true;
            this.cmbMUF.Location = new System.Drawing.Point(255, 291);
            this.cmbMUF.Name = "cmbMUF";
            this.cmbMUF.Size = new System.Drawing.Size(62, 24);
            this.cmbMUF.TabIndex = 198;
            this.cmbMUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMUF_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(252, 274);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 16);
            this.label28.TabIndex = 197;
            this.label28.Text = "UF";
            // 
            // dtReceita
            // 
            this.dtReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtReceita.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtReceita.Location = new System.Drawing.Point(123, 291);
            this.dtReceita.Name = "dtReceita";
            this.dtReceita.Size = new System.Drawing.Size(122, 22);
            this.dtReceita.TabIndex = 196;
            this.dtReceita.Value = new System.DateTime(2013, 11, 28, 0, 0, 0, 0);
            this.dtReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtReceita_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(122, 274);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 16);
            this.label27.TabIndex = 195;
            this.label27.Text = "Data da Receita";
            // 
            // txtNReceita
            // 
            this.txtNReceita.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNReceita.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNReceita.Location = new System.Drawing.Point(16, 291);
            this.txtNReceita.MaxLength = 10;
            this.txtNReceita.Name = "txtNReceita";
            this.txtNReceita.Size = new System.Drawing.Size(96, 22);
            this.txtNReceita.TabIndex = 194;
            this.txtNReceita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNReceita_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(13, 274);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 16);
            this.label26.TabIndex = 193;
            this.label26.Text = "N° Receita";
            // 
            // cmbConselho
            // 
            this.cmbConselho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConselho.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConselho.FormattingEnabled = true;
            this.cmbConselho.Location = new System.Drawing.Point(574, 247);
            this.cmbConselho.Name = "cmbConselho";
            this.cmbConselho.Size = new System.Drawing.Size(350, 24);
            this.cmbConselho.TabIndex = 192;
            this.cmbConselho.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbConselho_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(571, 228);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(145, 16);
            this.label25.TabIndex = 191;
            this.label25.Text = "Conselho Profissional";
            // 
            // txtMedico
            // 
            this.txtMedico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMedico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMedico.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedico.Location = new System.Drawing.Point(171, 247);
            this.txtMedico.MaxLength = 40;
            this.txtMedico.Name = "txtMedico";
            this.txtMedico.Size = new System.Drawing.Size(391, 22);
            this.txtMedico.TabIndex = 190;
            this.txtMedico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMedico_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(175, 228);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 16);
            this.label24.TabIndex = 189;
            this.label24.Text = "Médico";
            // 
            // txtCrm
            // 
            this.txtCrm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCrm.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCrm.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrm.Location = new System.Drawing.Point(16, 247);
            this.txtCrm.MaxLength = 13;
            this.txtCrm.Name = "txtCrm";
            this.txtCrm.Size = new System.Drawing.Size(144, 22);
            this.txtCrm.TabIndex = 188;
            this.txtCrm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCrm_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(13, 228);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 16);
            this.label23.TabIndex = 187;
            this.label23.Text = "CRM";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pictureBox22);
            this.groupBox4.Controls.Add(this.pictureBox21);
            this.groupBox4.Controls.Add(this.cmbPSexo);
            this.groupBox4.Controls.Add(this.cmbPUnidade);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.txtPIdade);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txtPNome);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(16, 152);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(908, 69);
            this.groupBox4.TabIndex = 186;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dados do Paciente";
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox22.Image")));
            this.pictureBox22.Location = new System.Drawing.Point(858, 22);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(16, 16);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox22.TabIndex = 228;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(724, 22);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(16, 16);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox21.TabIndex = 227;
            this.pictureBox21.TabStop = false;
            // 
            // cmbPSexo
            // 
            this.cmbPSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPSexo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPSexo.FormattingEnabled = true;
            this.cmbPSexo.Location = new System.Drawing.Point(814, 40);
            this.cmbPSexo.Name = "cmbPSexo";
            this.cmbPSexo.Size = new System.Drawing.Size(81, 24);
            this.cmbPSexo.TabIndex = 190;
            this.cmbPSexo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPSexo_KeyPress);
            // 
            // cmbPUnidade
            // 
            this.cmbPUnidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPUnidade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPUnidade.FormattingEnabled = true;
            this.cmbPUnidade.Location = new System.Drawing.Point(660, 40);
            this.cmbPUnidade.Name = "cmbPUnidade";
            this.cmbPUnidade.Size = new System.Drawing.Size(145, 24);
            this.cmbPUnidade.TabIndex = 189;
            this.cmbPUnidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPUnidade_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(811, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 16);
            this.label22.TabIndex = 188;
            this.label22.Text = "Sexo";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(657, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 16);
            this.label21.TabIndex = 187;
            this.label21.Text = "Unidade";
            // 
            // txtPIdade
            // 
            this.txtPIdade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPIdade.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPIdade.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPIdade.Location = new System.Drawing.Point(557, 40);
            this.txtPIdade.MaxLength = 40;
            this.txtPIdade.Name = "txtPIdade";
            this.txtPIdade.Size = new System.Drawing.Size(96, 22);
            this.txtPIdade.TabIndex = 186;
            this.txtPIdade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPIdade_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(555, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 16);
            this.label19.TabIndex = 185;
            this.label19.Text = "Idade";
            // 
            // txtPNome
            // 
            this.txtPNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPNome.Location = new System.Drawing.Point(9, 40);
            this.txtPNome.MaxLength = 40;
            this.txtPNome.Name = "txtPNome";
            this.txtPNome.Size = new System.Drawing.Size(537, 22);
            this.txtPNome.TabIndex = 184;
            this.txtPNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPNome_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(6, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 16);
            this.label18.TabIndex = 181;
            this.label18.Text = "Nome";
            // 
            // dtData
            // 
            this.dtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtData.Location = new System.Drawing.Point(574, 124);
            this.dtData.Name = "dtData";
            this.dtData.Size = new System.Drawing.Size(120, 22);
            this.dtData.TabIndex = 185;
            this.dtData.Value = new System.DateTime(2013, 11, 28, 0, 0, 0, 0);
            this.dtData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtData_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(573, 106);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 16);
            this.label17.TabIndex = 184;
            this.label17.Text = "Data";
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(171, 124);
            this.txtNome.MaxLength = 80;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(391, 22);
            this.txtNome.TabIndex = 183;
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(170, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 16);
            this.label6.TabIndex = 182;
            this.label6.Text = "Nome";
            // 
            // txtDocto
            // 
            this.txtDocto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocto.Location = new System.Drawing.Point(16, 124);
            this.txtDocto.MaxLength = 30;
            this.txtDocto.Name = "txtDocto";
            this.txtDocto.Size = new System.Drawing.Size(144, 22);
            this.txtDocto.TabIndex = 181;
            this.txtDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(15, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 180;
            this.label5.Text = "N° do Documento";
            // 
            // cmbUF
            // 
            this.cmbUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUF.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUF.FormattingEnabled = true;
            this.cmbUF.Location = new System.Drawing.Point(862, 77);
            this.cmbUF.Name = "cmbUF";
            this.cmbUF.Size = new System.Drawing.Size(62, 24);
            this.cmbUF.TabIndex = 179;
            this.cmbUF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbUF_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(859, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 16);
            this.label4.TabIndex = 178;
            this.label4.Text = "UF";
            // 
            // cmbOrgao
            // 
            this.cmbOrgao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrgao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrgao.FormattingEnabled = true;
            this.cmbOrgao.Location = new System.Drawing.Point(502, 77);
            this.cmbOrgao.Name = "cmbOrgao";
            this.cmbOrgao.Size = new System.Drawing.Size(352, 24);
            this.cmbOrgao.TabIndex = 177;
            this.cmbOrgao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbOrgao_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(502, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 176;
            this.label2.Text = "Orgão Expedidor";
            // 
            // imgLote
            // 
            this.imgLote.Image = ((System.Drawing.Image)(resources.GetObject("imgLote.Image")));
            this.imgLote.Location = new System.Drawing.Point(410, 372);
            this.imgLote.Name = "imgLote";
            this.imgLote.Size = new System.Drawing.Size(16, 16);
            this.imgLote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgLote.TabIndex = 175;
            this.imgLote.TabStop = false;
            this.imgLote.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(539, 372);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 174;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseHover += new System.EventHandler(this.pictureBox9_MouseHover);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(862, 324);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(16, 16);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 173;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(67, 324);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(16, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 172;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(272, 58);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 169;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // lblLote
            // 
            this.lblLote.AutoSize = true;
            this.lblLote.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLote.ForeColor = System.Drawing.Color.Navy;
            this.lblLote.Location = new System.Drawing.Point(373, 372);
            this.lblLote.Name = "lblLote";
            this.lblLote.Size = new System.Drawing.Size(36, 16);
            this.lblLote.TabIndex = 162;
            this.lblLote.Text = "Lote";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(494, 372);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 16);
            this.label14.TabIndex = 161;
            this.label14.Text = "Qtde.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(726, 324);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 16);
            this.label13.TabIndex = 160;
            this.label13.Text = "Reg. Min. da Saúde";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(170, 324);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 16);
            this.label9.TabIndex = 159;
            this.label9.Text = "Descrição";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(13, 324);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 16);
            this.label7.TabIndex = 158;
            this.label7.Text = "Código";
            // 
            // cmbDocto
            // 
            this.cmbDocto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDocto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDocto.FormattingEnabled = true;
            this.cmbDocto.Location = new System.Drawing.Point(138, 77);
            this.cmbDocto.Name = "cmbDocto";
            this.cmbDocto.Size = new System.Drawing.Size(355, 24);
            this.cmbDocto.TabIndex = 152;
            this.cmbDocto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDocto_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(135, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 16);
            this.label3.TabIndex = 151;
            this.label3.Text = "Tipo de Documento";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(13, 30);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 148;
            this.lblEstab.Tag = "";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(85, 59);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 110;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // txtVenID
            // 
            this.txtVenID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtVenID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVenID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVenID.Location = new System.Drawing.Point(16, 77);
            this.txtVenID.MaxLength = 18;
            this.txtVenID.Name = "txtVenID";
            this.txtVenID.ReadOnly = true;
            this.txtVenID.Size = new System.Drawing.Size(113, 22);
            this.txtVenID.TabIndex = 81;
            this.txtVenID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(13, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 16);
            this.label8.TabIndex = 61;
            this.label8.Text = "Venda ID";
            // 
            // frmSngpcVendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSngpcVendas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmSngpcVendas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSngpcVendas_Load);
            this.Shown += new System.EventHandler(this.frmSngpcVendas_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcVendas.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).EndInit();
            this.tsmVendas.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcVendas;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.TextBox txtBDocto;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgVendas;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtData;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDocto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbUF;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbOrgao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox imgLote;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblLote;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbDocto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtVenID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMedico;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCrm;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cmbPSexo;
        private System.Windows.Forms.ComboBox cmbPUnidade;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPIdade;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPNome;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtReceita;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtNReceita;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbConselho;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLote;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.TextBox txtReg;
        private System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.ComboBox cmbTipoRec;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbUMed;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cmbMUF;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBCrm;
        private System.Windows.Forms.MaskedTextBox txtBData;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtBNome;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip tsmVendas;
        private System.Windows.Forms.ToolStripMenuItem tsmExportar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_SEQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_COMP_TD;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_COMP_OE;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_COMP_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_COMP_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_COMP_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PAC_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PAC_IDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PAC_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PAC_SEXO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PRESC_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PRESC_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PRESC_CP;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_NUM_NOT;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PRESC_DT;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_PRESC_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_USO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_TP_RECEITA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REGISTRO_MS;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_MED_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn VEN_MED_LOTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_ALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CADASTRO;
        private System.Windows.Forms.TextBox txtQtdeEstoque;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbLote;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Label label15;
    }
}