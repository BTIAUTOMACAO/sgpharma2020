﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcTabelas : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtTabelas = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbTabelas = new ToolStripButton("Tabelas SNGPC");
        private ToolStripSeparator tssTabelas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        public frmSngpcTabelas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcTabelas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcTabelas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, contRegistros));
            if (tcTabelas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcTabelas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtTabID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbTabelas.AutoSize = false;
                this.tsbTabelas.Image = Properties.Resources.sngpc;
                this.tsbTabelas.Size = new System.Drawing.Size(140, 20);
                this.tsbTabelas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbTabelas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssTabelas);
                tsbTabelas.Click += delegate
                {
                    var cadTabelas = Application.OpenForms.OfType<frmSngpcTabelas>().FirstOrDefault();
                    BotoesHabilitados();
                    cadTabelas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbTabelas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssTabelas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                txtTabID.Text = dtTabelas.Rows[linha]["TAB_SEQ"].ToString();
                cmbTipo.SelectedIndex = Convert.ToInt16(dtTabelas.Rows[linha]["TAB_TIPO"]) - 1;
                txtCodigo.Text = dtTabelas.Rows[linha]["TAB_CODIGO"].ToString();
                txtDescr.Text = dtTabelas.Rows[linha]["TAB_DESCRICAO"].ToString();
                if (dtTabelas.Rows[linha]["TAB_DESABILITADO"].ToString() == "S")
                {
                    chkLiberado.Checked = true;
                }
                else
                    chkLiberado.Checked = false;

                txtData.Text = Funcoes.ChecaCampoVazio(dtTabelas.Rows[linha]["DAT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtTabelas.Rows[linha]["OP_ALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, linha));
                cmbTipo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtTabelas.Clear();
            dgTabelas.DataSource = dtTabelas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtTabID.Text = "";
            cmbTipo.SelectedIndex = -1;
            txtCodigo.Text = "";
            txtDescr.Text = "";
            chkLiberado.Checked = true;
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbTipo.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcTabelas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabelas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabelas.CurrentCell = dgTabelas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabelas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabelas.CurrentCell = dgTabelas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtTabelas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgTabelas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgTabelas.CurrentCell = dgTabelas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcTabelas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtTabelas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgTabelas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgTabelas.CurrentCell = dgTabelas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    SngpcTabela tabela = new SngpcTabela()
                    {
                        TabSeq = Convert.ToInt32(txtTabID.Text),
                        TabTipo = (cmbTipo.SelectedIndex + 1),
                        TabCodigo = txtCodigo.Text.ToUpper().Trim(),
                        TabDescr = txtDescr.Text.ToUpper().Trim(),
                        TabLiberado = chkLiberado.Checked == true ? "N" : "S",
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_TABELAS", "TAB_SEQ", txtTabID.Text);

                    if (tabela.AtualizaDados(tabela, Principal.dtBusca).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Tabelas SNGPC");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtTabelas.Clear();
                        dgTabelas.DataSource = dtTabelas;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Erro: Ao efetuar a atualização ", "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Excluir()
        {
            try
            {
                if (txtTabID.Text.Trim() != "" || cmbTipo.SelectedIndex != -1 || txtCodigo.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma a exclusão da tabela?", "Exclusão tabela SNGPC Tabelas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (RegrasCadastro.ExcluirTabSngpc(txtTabID.Text, Convert.ToString(cmbTipo.SelectedValue)).Equals(true))
                        {
                            using (frmOcorrencia ocorrencia = new frmOcorrencia())
                            {
                                ocorrencia.ShowDialog();
                            }
                            SngpcTabela sngpc = new SngpcTabela();
                            if (sngpc.ExcluirDados(Convert.ToInt32(txtTabID.Text)).Equals(true))
                            {
                                Funcoes.GravaLogExclusao("TAB_ID", txtTabID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "SNGPC_TABELAS", txtTabID.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Tabelas excluído com sucesso!", "Sngpc Tabelas", MessageBoxButtons.OK);
                                dtTabelas.Clear();
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dgTabelas.DataSource = dtTabelas;
                                emGrade = false;
                                tcTabelas.SelectedTab = tpGrade;
                                tslRegistros.Text = "";
                                cmbLiberado.SelectedIndex = 0;
                                txtBID.Focus();
                                return true;
                            }
                        }
                        return false;
                    }
                    return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool Incluir()
        {
            try
            {
                if (ConsisteCampos().Equals(true))
                {
                    Principal.dtPesq = Util.RegistrosPorEmpresa("SNGPC_TABELAS", "TAB_DESCRICAO", txtDescr.Text.Trim().ToUpper(), false, true);
                    if (Principal.dtPesq.Rows.Count != 0)
                    {
                        Principal.mensagem = "Tabela já cadastrada.";
                        Funcoes.Avisa();
                        return false;
                    }
                    else
                    {
                        SngpcTabela tabela = new SngpcTabela()
                        {
                            TabSeq = Funcoes.IdentificaVerificaID("SNGPC_TABELAS", "TAB_SEQ", 0, string.Empty),
                            TabTipo = (cmbTipo.SelectedIndex + 1),
                            TabCodigo = txtCodigo.Text.ToUpper().Trim(),
                            TabDescr = txtDescr.Text.ToUpper().Trim(),
                            TabLiberado = chkLiberado.Checked == true ? "N" : "S",
                        };

                        if (tabela.InsereDados(tabela).Equals(true))
                        {

                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            MessageBox.Show("Tabelas SNGPC realizada com sucesso!", "Tabelas SNGPC");
                            dtTabelas.Clear();
                            dgTabelas.DataSource = dtTabelas;
                            emGrade = false;
                            tslRegistros.Text = String.Empty;
                            Limpar();
                            return true;

                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbTipo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo.";
                Funcoes.Avisa();
                cmbTipo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                Principal.mensagem = "Código não pode ser em branco.";
                Funcoes.Avisa();
                txtCodigo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtDescr.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescr.Focus();
                return false;
            }
            return true;
        }

        private void tcTabelas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcTabelas.SelectedTab == tpGrade)
            {
                dgTabelas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcTabelas.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcTabelas");
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, contRegistros));
                    chkLiberado.Checked = true;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    cmbTipo.Focus();
                    emGrade = false;

                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SngpcTabela tabela = new SngpcTabela()
                {
                    TabSeq = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text),
                    TabTipo = cmbBTipo.SelectedIndex == -1 ? 0 : cmbBTipo.SelectedIndex + 1,
                    TabLiberado = cmbLiberado.Text
                };

                dtTabelas = tabela.BuscarDados(tabela, out strOrdem);
                if (dtTabelas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtTabelas.Rows.Count;
                    dgTabelas.DataSource = dtTabelas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, 0));
                    dgTabelas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgTabelas.DataSource = dtTabelas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                    cmbLiberado.SelectedIndex = 0;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    cmbBTipo.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBTipo.SelectedIndex.Equals(-1))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void cmbTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodigo.Focus();
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDescr.Focus();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkLiberado.Focus();
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTabelas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgTabelas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgTabelas.RowCount;
                    for (i = 0; i <= dgTabelas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgTabelas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgTabelas.ColumnCount; k++)
                    {
                        if (dgTabelas.Columns[k].Visible == true)
                        {
                            switch (dgTabelas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgTabelas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTabelas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgTabelas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgTabelas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgTabelas.ColumnCount : indice) + Convert.ToString(dgTabelas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgTabelas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, contRegistros));
            emGrade = true;
        }

        private void dgTabelas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcTabelas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgTabelas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtTabelas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtTabelas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtTabelas.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgTabelas_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgTabelas.Rows[e.RowIndex].Cells[4].Value.ToString() == "N")
            {
                dgTabelas.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgTabelas.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }

        }

        private void dgTabelas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgTabelas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtTabelas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTabelas.Columns[e.ColumnIndex].Name + " DESC");
                        dgTabelas.DataSource = dtTabelas;
                        decrescente = true;
                    }
                    else
                    {
                        dtTabelas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgTabelas.Columns[e.ColumnIndex].Name + " ASC");
                        dgTabelas.DataSource = dtTabelas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtTabelas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtTabID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgTabelas.ColumnCount; i++)
                {
                    if (dgTabelas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgTabelas.ColumnCount];
                string[] coluna = new string[dgTabelas.ColumnCount];

                for (int i = 0; i < dgTabelas.ColumnCount; i++)
                {
                    grid[i] = dgTabelas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgTabelas.ColumnCount; i++)
                {
                    if (dgTabelas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgTabelas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgTabelas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgTabelas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgTabelas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgTabelas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgTabelas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgTabelas.ColumnCount; i++)
                        {
                            dgTabelas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Tabelas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcTabelas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcTabelas.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
