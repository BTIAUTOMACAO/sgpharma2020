﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcRelatorioCompras : Form, Botoes
    {

        private ToolStripButton tsbRelCompras = new ToolStripButton("Rel Compras SNGPC");
        private ToolStripSeparator tssRelCompras = new ToolStripSeparator();

        public frmSngpcRelatorioCompras(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório de Compras SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            rdbCodigo.Checked = true;
            rdbTodos.Checked = true;
            cmbTipoMedicamento.SelectedIndex = 0;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelCompras);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelCompras);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Transferência SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
            {
                Principal.mensagem = "Data Inicial não pode ser em Branco!";
                Funcoes.Avisa();
                dtInicial.Focus();
                return;
            }

            if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
            {
                Principal.mensagem = "Data Final não pode ser em Branco!";
                Funcoes.Avisa();
                dtFinal.Focus();
                return;
            }

            if (dtInicial.Value > dtFinal.Value)
            {
                Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                Funcoes.Avisa();
                dtInicial.Focus();
                return;
            }

            char ondem = rdbCodigo.Checked.Equals(true) ? 'C' : 'D';
            string quebra = rdbTodos.Checked.Equals(true) ? "TODOS" : rdbCompra.Checked.Equals(true) ? "7" : "8";
            int TipoMed = cmbTipoMedicamento.Text.Equals("TODOS") ? 0 : cmbTipoMedicamento.Text.Equals("ANTIMICROBIANO") ? 1 : 2;
            frmRelatorioCompras compras = new frmRelatorioCompras(dtInicial.Text, dtFinal.Text, ondem, quebra, TipoMed);
            compras.Show();
        }

        private void frmSngpcRelatorioCompras_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Compras SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botoes()
        {
            try
            {
                this.tsbRelCompras.AutoSize = false;
                this.tsbRelCompras.Image = Properties.Resources.sngpc;
                this.tsbRelCompras.Size = new System.Drawing.Size(160, 20);
                this.tsbRelCompras.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelCompras);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelCompras);
                tsbRelCompras.Click += delegate
                {
                    var relCompras = Application.OpenForms.OfType<frmSngpcRelatorioCompras>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relCompras.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Compras SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcRelatorioCompras_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void cmbTipoMedicamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbTodos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbCompra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbTransferencia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void btnVisualizar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }
    }
}
