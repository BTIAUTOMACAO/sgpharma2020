﻿namespace GestaoAdministrativa.SNGPC
{
    partial class frmSngpcTransferencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSngpcTransferencia));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.btnTransferencia = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.gbPeriodo = new System.Windows.Forms.GroupBox();
            this.btnGravar = new System.Windows.Forms.Button();
            this.dtPeriodo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPeriodo = new System.Windows.Forms.Button();
            this.btnVerifcaArquivo = new System.Windows.Forms.Button();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTecnico = new System.Windows.Forms.ComboBox();
            this.cmdProdErro = new System.Windows.Forms.DataGridView();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_UNIDADE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CLASSE_TERAP = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PROD_REGISTRO_MS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_MED_LOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ENT_MED_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ERRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbPeriodo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProdErro)).BeginInit();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Location = new System.Drawing.Point(5, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(964, 497);
            this.panel2.TabIndex = 158;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.txtMsg);
            this.groupBox2.Controls.Add(this.btnTransferencia);
            this.groupBox2.Controls.Add(this.lblMsg);
            this.groupBox2.Controls.Add(this.gbPeriodo);
            this.groupBox2.Controls.Add(this.btnPeriodo);
            this.groupBox2.Controls.Add(this.btnVerifcaArquivo);
            this.groupBox2.Controls.Add(this.dtFinal);
            this.groupBox2.Controls.Add(this.dtInicial);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbTecnico);
            this.groupBox2.Controls.Add(this.cmdProdErro);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 483);
            this.groupBox2.TabIndex = 156;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // txtMsg
            // 
            this.txtMsg.BackColor = System.Drawing.Color.White;
            this.txtMsg.Location = new System.Drawing.Point(16, 217);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.ReadOnly = true;
            this.txtMsg.Size = new System.Drawing.Size(909, 261);
            this.txtMsg.TabIndex = 165;
            this.txtMsg.Visible = false;
            // 
            // btnTransferencia
            // 
            this.btnTransferencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransferencia.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransferencia.Image = ((System.Drawing.Image)(resources.GetObject("btnTransferencia.Image")));
            this.btnTransferencia.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransferencia.Location = new System.Drawing.Point(370, 137);
            this.btnTransferencia.Name = "btnTransferencia";
            this.btnTransferencia.Size = new System.Drawing.Size(155, 51);
            this.btnTransferencia.TabIndex = 163;
            this.btnTransferencia.Text = "Enviar Arquivo ";
            this.btnTransferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTransferencia.UseVisualStyleBackColor = true;
            this.btnTransferencia.Visible = false;
            this.btnTransferencia.Click += new System.EventHandler(this.btnTransferencia_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Navy;
            this.lblMsg.Location = new System.Drawing.Point(13, 198);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(145, 16);
            this.lblMsg.TabIndex = 162;
            this.lblMsg.Text = "Verificando Arquivo...";
            this.lblMsg.Visible = false;
            // 
            // gbPeriodo
            // 
            this.gbPeriodo.Controls.Add(this.btnGravar);
            this.gbPeriodo.Controls.Add(this.dtPeriodo);
            this.gbPeriodo.Controls.Add(this.label4);
            this.gbPeriodo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPeriodo.ForeColor = System.Drawing.Color.Black;
            this.gbPeriodo.Location = new System.Drawing.Point(685, 53);
            this.gbPeriodo.Name = "gbPeriodo";
            this.gbPeriodo.Size = new System.Drawing.Size(240, 135);
            this.gbPeriodo.TabIndex = 157;
            this.gbPeriodo.TabStop = false;
            this.gbPeriodo.Text = "Acerta Período";
            this.gbPeriodo.Visible = false;
            // 
            // btnGravar
            // 
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(67, 82);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(109, 43);
            this.btnGravar.TabIndex = 160;
            this.btnGravar.Text = "Atualizar";
            this.btnGravar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // dtPeriodo
            // 
            this.dtPeriodo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPeriodo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPeriodo.Location = new System.Drawing.Point(67, 48);
            this.dtPeriodo.Name = "dtPeriodo";
            this.dtPeriodo.Size = new System.Drawing.Size(109, 22);
            this.dtPeriodo.TabIndex = 159;
            this.dtPeriodo.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(10, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(220, 16);
            this.label4.TabIndex = 157;
            this.label4.Text = "Data final da última transferência";
            // 
            // btnPeriodo
            // 
            this.btnPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPeriodo.Image = ((System.Drawing.Image)(resources.GetObject("btnPeriodo.Image")));
            this.btnPeriodo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPeriodo.Location = new System.Drawing.Point(16, 137);
            this.btnPeriodo.Name = "btnPeriodo";
            this.btnPeriodo.Size = new System.Drawing.Size(152, 51);
            this.btnPeriodo.TabIndex = 161;
            this.btnPeriodo.Text = "Acerta Período";
            this.btnPeriodo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPeriodo.UseVisualStyleBackColor = true;
            this.btnPeriodo.Click += new System.EventHandler(this.btnPeriodo_Click);
            // 
            // btnVerifcaArquivo
            // 
            this.btnVerifcaArquivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerifcaArquivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerifcaArquivo.Image = ((System.Drawing.Image)(resources.GetObject("btnVerifcaArquivo.Image")));
            this.btnVerifcaArquivo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerifcaArquivo.Location = new System.Drawing.Point(191, 137);
            this.btnVerifcaArquivo.Name = "btnVerifcaArquivo";
            this.btnVerifcaArquivo.Size = new System.Drawing.Size(155, 51);
            this.btnVerifcaArquivo.TabIndex = 160;
            this.btnVerifcaArquivo.Text = "Verifcar Arquivo ";
            this.btnVerifcaArquivo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVerifcaArquivo.UseVisualStyleBackColor = true;
            this.btnVerifcaArquivo.Click += new System.EventHandler(this.btnVerifcaArquivo_Click);
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(138, 97);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(109, 22);
            this.dtFinal.TabIndex = 159;
            this.dtFinal.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            // 
            // dtInicial
            // 
            this.dtInicial.Enabled = false;
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(16, 97);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(109, 22);
            this.dtInicial.TabIndex = 158;
            this.dtInicial.Value = new System.DateTime(2013, 11, 26, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(135, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 157;
            this.label3.Text = "Data Final";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(13, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 156;
            this.label2.Text = "Data Inicial";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 16);
            this.label6.TabIndex = 154;
            this.label6.Text = "Responsável Técnico";
            // 
            // cmbTecnico
            // 
            this.cmbTecnico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTecnico.FormattingEnabled = true;
            this.cmbTecnico.Location = new System.Drawing.Point(16, 42);
            this.cmbTecnico.Name = "cmbTecnico";
            this.cmbTecnico.Size = new System.Drawing.Size(509, 24);
            this.cmbTecnico.TabIndex = 155;
            this.cmbTecnico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTecnico_KeyPress);
            // 
            // cmdProdErro
            // 
            this.cmdProdErro.AllowUserToAddRows = false;
            this.cmdProdErro.AllowUserToDeleteRows = false;
            this.cmdProdErro.AllowUserToOrderColumns = true;
            this.cmdProdErro.AllowUserToResizeColumns = false;
            this.cmdProdErro.AllowUserToResizeRows = false;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            this.cmdProdErro.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle26;
            this.cmdProdErro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdProdErro.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cmdProdErro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.cmdProdErro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cmdProdErro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PROD_DESCR,
            this.PROD_UNIDADE,
            this.PROD_CODIGO,
            this.PROD_CLASSE_TERAP,
            this.PROD_REGISTRO_MS,
            this.ENT_MED_LOTE,
            this.ENT_MED_QTDE,
            this.ERRO});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.cmdProdErro.DefaultCellStyle = dataGridViewCellStyle28;
            this.cmdProdErro.GridColor = System.Drawing.Color.LightGray;
            this.cmdProdErro.Location = new System.Drawing.Point(16, 217);
            this.cmdProdErro.MultiSelect = false;
            this.cmdProdErro.Name = "cmdProdErro";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cmdProdErro.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.cmdProdErro.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.cmdProdErro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cmdProdErro.Size = new System.Drawing.Size(909, 258);
            this.cmdProdErro.TabIndex = 157;
            this.cmdProdErro.Visible = false;
            this.cmdProdErro.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.cmdProdErro_CellEndEdit);
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.Width = 230;
            // 
            // PROD_UNIDADE
            // 
            this.PROD_UNIDADE.DataPropertyName = "PROD_UNIDADE";
            this.PROD_UNIDADE.HeaderText = "Unidade";
            this.PROD_UNIDADE.Name = "PROD_UNIDADE";
            this.PROD_UNIDADE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PROD_UNIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PROD_UNIDADE.Width = 70;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Código Barras";
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 130;
            // 
            // PROD_CLASSE_TERAP
            // 
            this.PROD_CLASSE_TERAP.DataPropertyName = "PROD_CLASSE_TERAP";
            this.PROD_CLASSE_TERAP.HeaderText = "Classe Terapêutica";
            this.PROD_CLASSE_TERAP.Items.AddRange(new object[] {
            "",
            "ANTIMICROBIANO",
            "SUJEITO A CONTROLE ESPECIAL"});
            this.PROD_CLASSE_TERAP.Name = "PROD_CLASSE_TERAP";
            this.PROD_CLASSE_TERAP.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PROD_CLASSE_TERAP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PROD_CLASSE_TERAP.Width = 190;
            // 
            // PROD_REGISTRO_MS
            // 
            this.PROD_REGISTRO_MS.DataPropertyName = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.HeaderText = "Registro MS";
            this.PROD_REGISTRO_MS.MaxInputLength = 13;
            this.PROD_REGISTRO_MS.Name = "PROD_REGISTRO_MS";
            this.PROD_REGISTRO_MS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ENT_MED_LOTE
            // 
            this.ENT_MED_LOTE.DataPropertyName = "ENT_MED_LOTE";
            this.ENT_MED_LOTE.HeaderText = "Lote";
            this.ENT_MED_LOTE.Name = "ENT_MED_LOTE";
            this.ENT_MED_LOTE.ReadOnly = true;
            // 
            // ENT_MED_QTDE
            // 
            this.ENT_MED_QTDE.DataPropertyName = "ENT_MED_QTDE";
            this.ENT_MED_QTDE.HeaderText = "Qtd";
            this.ENT_MED_QTDE.Name = "ENT_MED_QTDE";
            this.ENT_MED_QTDE.ReadOnly = true;
            this.ENT_MED_QTDE.Width = 50;
            // 
            // ERRO
            // 
            this.ERRO.DataPropertyName = "ERRO";
            this.ERRO.HeaderText = "Erro";
            this.ERRO.Name = "ERRO";
            this.ERRO.ReadOnly = true;
            this.ERRO.Width = 190;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(451, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Transferência de Arquivos de Compra/Venda de Medicamentos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::GestaoAdministrativa.Properties.Resources.sngpc;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(548, 147);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 35);
            this.button1.TabIndex = 166;
            this.button1.Text = "Gerar Somente XML";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmSngpcTransferencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSngpcTransferencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmSngpcTransferencia";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmSngpcTransferencia_Load);
            this.Shown += new System.EventHandler(this.frmSngpcTransferencia_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSngpcTransferencia_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmSngpcTransferencia_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbPeriodo.ResumeLayout(false);
            this.gbPeriodo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProdErro)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbTecnico;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbPeriodo;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.DateTimePicker dtPeriodo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPeriodo;
        private System.Windows.Forms.Button btnVerifcaArquivo;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView cmdProdErro;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnTransferencia;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewComboBoxColumn PROD_UNIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewComboBoxColumn PROD_CLASSE_TERAP;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_REGISTRO_MS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_MED_LOTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ENT_MED_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ERRO;
        private System.Windows.Forms.Button button1;
    }
}