﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Estoque;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelatorioMovimentos : Form
    {
        private frmEstRel relatorio;
        private DataTable dtRelatorio;

        public frmRelatorioMovimentos(frmEstRel frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmRelatorioMovimentos_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioAnalitico();
            this.reportViewer1.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[1];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            this.reportViewer1.LocalReport.SetParameters(parametro);
        }
        public void MontaRelatorioAnalitico()
        {
            var perdas = new ReportDataSource("estoque", dtRelatorio);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(perdas);
        }
    }
}
