﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SNGPC.Relatorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcRelBalanco : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Balanço SNGPC");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmSngpcRelBalanco(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }
        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }


        public void TeclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    btnPesquisar.PerformClick();
                    break;
            }
        }

        private void frmSngpcRelBalanco_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbCompleto_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAquisicoes_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbReceitasA_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbReceitasB_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void txtAno_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdbAnual_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdb1_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdb2_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdb3_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void rdb4_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        private void frmSngpcRelBalanco_KeyDown(object sender, KeyEventArgs e)
        {
            TeclaAtalho(e);
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.sngpc;
                this.tsbRelatorio.Size = new System.Drawing.Size(160, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmSngpcRelBalanco>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Limpar()
        {
            rdbCompleto.Checked = true;
            txtAno.Text = "";
            rdbAnual.Checked = true;
            rdbAquisicoes.Checked = false;
            rdbReceitasA.Checked = false;
            rdbReceitasB.Checked = false;
            rdb1.Checked = false;
            rdb2.Checked = false;
            rdb3.Checked = false;
            rdb4.Checked = false;
            cmbMes.Enabled = false;
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbCompleto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtAno.Focus();
        }

        private void rdbAquisicoes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtAno.Focus();
        }

        private void rdbReceitasA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtAno.Focus();
        }

        private void rdbReceitasB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtAno.Focus();
        }

        private void txtAno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                rdbAnual.Focus();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtAno.Text))
                {
                    MessageBox.Show("Ano não pode ser em Branco!", "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAno.Focus();
                    return;
                }

                if (txtAno.Text.Length != 4)
                {
                    MessageBox.Show("Formato do Ano Inválido (YYYY)!", "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAno.Focus();
                    return;
                }

                if ((rdbReceitasA.Checked || rdbReceitasB.Checked) && cmbMes.SelectedIndex == -1)
                {
                    MessageBox.Show("Selecione o Mês!", "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbMes.Focus();
                    return;
                }

                if (rdbCompleto.Checked && cmbClassTera.SelectedIndex == -1)
                {
                    MessageBox.Show("Selecione o tipo de Classe Terapeutica", "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbClassTera.Focus();
                    return; 
                }           

                Cursor = Cursors.WaitCursor;

                var relatorio = new SNGPCRelatorios();
                DataTable dtRetorno = new DataTable();
                if (rdbCompleto.Checked)
                {
                    dtRetorno = relatorio.BalancoCompleto(txtAno.Text, rdbAnual.Checked, rdb1.Checked ? 1 : rdb2.Checked ? 2 : rdb3.Checked ? 3 : rdb4.Checked ? 4 : 0, Principal.empAtual, Principal.estAtual, cmbClassTera.SelectedIndex);

                    frmRelBalancoCompleto relCompleto = new frmRelBalancoCompleto(this, dtRetorno);
                    relCompleto.Text = "Balanço Completo";
                    relCompleto.ShowDialog();

                    frmRelBalancoComItens relCompletoItens = new frmRelBalancoComItens(this, dtRetorno);
                    relCompletoItens.Text = "Balanço Completo Itens";
                    relCompletoItens.ShowDialog();
                }
                else if (rdbAquisicoes.Checked)
                {
                    dtRetorno = relatorio.BalancoAquisicoes(txtAno.Text, rdbAnual.Checked, rdb1.Checked ? 1 : rdb2.Checked ? 2 : rdb3.Checked ? 3 : rdb4.Checked ? 4 : 0, Principal.empAtual, Principal.estAtual);

                    frmRelBalancoAquisicoes relAquisicao = new frmRelBalancoAquisicoes(this, dtRetorno);
                    relAquisicao.Text = "Balanço das Aquisições de Medicamentos";
                    relAquisicao.ShowDialog();
                }
                else
                {
                    dtRetorno = relatorio.BalancoReceitasAouB(txtAno.Text, cmbMes.Text, Principal.empAtual, Principal.estAtual, rdbReceitasA.Checked ? 4 : 2);

                    frmRelBalancoReceitaAeB relReceita = new frmRelBalancoReceitaAeB(this, dtRetorno);
                    relReceita.Text = "Relação Mensal de Notificações de Receita";
                    relReceita.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Rel. Balanço SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void rdbReceitasA_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbReceitasA.Checked)
            {
                rdbAnual.Enabled = false;
                rdb1.Enabled = false;
                rdb2.Enabled = false;
                rdb3.Enabled = false;
                rdb4.Enabled = false;
                cmbMes.Enabled = true;
            }
            else
            {
                rdbAnual.Enabled = true;
                rdb1.Enabled = true;
                rdb2.Enabled = true;
                rdb3.Enabled = true;
                rdb4.Enabled = true;
                cmbMes.Enabled = true;
            }
        }

        private void rdbReceitasB_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbReceitasB.Checked)
            {
                rdbAnual.Enabled = false;
                rdb1.Enabled = false;
                rdb2.Enabled = false;
                rdb3.Enabled = false;
                rdb4.Enabled = false;
                cmbMes.Enabled = true;
            }
            else
            {
                rdbAnual.Enabled = true;
                rdb1.Enabled = true;
                rdb2.Enabled = true;
                rdb3.Enabled = true;
                rdb4.Enabled = true;
                cmbMes.Enabled = false;
            }
        }

        private void rdbCompleto_CheckedChanged(object sender, EventArgs e)
        {
            if (!rdbCompleto.Checked)
            {
                rdbAnual.Enabled = false;
                rdb1.Enabled = false;
                rdb2.Enabled = false;
                rdb3.Enabled = false;
                rdb4.Enabled = false;
                cmbMes.Enabled = true;
                cmbClassTera.Enabled = false;
            }
            else
            {
                rdbAnual.Enabled = true;
                rdb1.Enabled = true;
                rdb2.Enabled = true;
                rdb3.Enabled = true;
                rdb4.Enabled = true;
                cmbMes.Enabled = false;
                cmbClassTera.Enabled = true; 
            }
        }

        private void rdbAquisicoes_CheckedChanged(object sender, EventArgs e)
        {
            if (!rdbAquisicoes.Checked)
            {
                rdbAnual.Enabled = false;
                rdb1.Enabled = false;
                rdb2.Enabled = false;
                rdb3.Enabled = false;
                rdb4.Enabled = false;
                cmbMes.Enabled = true;
            }
            else
            {
                rdbAnual.Enabled = true;
                rdb1.Enabled = true;
                rdb2.Enabled = true;
                rdb3.Enabled = true;
                rdb4.Enabled = true;
                cmbMes.Enabled = false;
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
