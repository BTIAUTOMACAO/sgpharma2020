﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelBalancoReceitaAeB : Form
    {
        private frmSngpcRelBalanco relatorio;
        private DataTable dtRelatorio;

        public frmRelBalancoReceitaAeB(frmSngpcRelBalanco frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmRelBalancoReceitaAeB_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            var dadosEstab = new Estabelecimento();
            List<Estabelecimento> busca = dadosEstab.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
            
            ReportParameter[] parametro = new ReportParameter[9];
            parametro[0] = new ReportParameter("razaoSocial", busca[0].EstRazao);
            parametro[1] = new ReportParameter("endereco", busca[0].EstEndereco + " N°: " + busca[0].EstNum);
            parametro[2] = new ReportParameter("estado", Funcoes.ObtemEstado(busca[0].EstUf));
            parametro[3] = new ReportParameter("municipio", busca[0].EstCidade);
            parametro[4] = new ReportParameter("ano", relatorio.txtAno.Text);
            parametro[5] = new ReportParameter("mes", relatorio.cmbMes.Text);
            parametro[6] = new ReportParameter("tipoReceita", relatorio.rdbReceitasA.Checked ? "'A' (RMNRA)" : "'B2' (RMNRB2)");
            parametro[7] = new ReportParameter("letraReceita", relatorio.rdbReceitasA.Checked ? "'A'" : "B2");
            parametro[8] = new ReportParameter("semregistro", dtRelatorio.Rows.Count == 0 ? "NENHUM REGISTRO ENCONTRADO" : " ");
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var aquisicoes = new ReportDataSource("dsReceita", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(aquisicoes);
        }
    }
}
