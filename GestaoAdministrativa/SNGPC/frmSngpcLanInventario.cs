﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcLanInventario : Form, Botoes
    {
        #region DECLARACAO DAS VARIÁVEIS//
        private DataTable dtLancamento = new DataTable();
        private int contRegistros;
        private string[,] dados = new string[6, 3];
        private bool emGrade;
        private ToolStripButton tsbLancamento = new ToolStripButton("Lanc. Inventário");
        private ToolStripSeparator tssLancamento = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        #endregion

        public frmSngpcLanInventario(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcLanInventario_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBCodBarra.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcLanInventario");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, contRegistros));
            if (tcLancamento.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcLancamento.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtLanID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbLancamento.AutoSize = false;
                this.tsbLancamento.Image = Properties.Resources.sngpc;
                this.tsbLancamento.Size = new System.Drawing.Size(125, 20);
                this.tsbLancamento.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbLancamento);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssLancamento);
                tsbLancamento.Click += delegate
                {
                    var cadLancamento = Application.OpenForms.OfType<frmSngpcLanInventario>().FirstOrDefault();
                    BotoesHabilitados();
                    cadLancamento.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbLancamento);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssLancamento);
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtLanID.Text = dtLancamento.Rows[linha]["LAC_INV_ID"].ToString();
                txtCodBarras.Text = dtLancamento.Rows[linha]["LAC_INV_CODIGO"].ToString();
                txtDescr.Text = dtLancamento.Rows[linha]["PROD_DESCR"].ToString();
                txtReg.Text = dtLancamento.Rows[linha]["LAC_INV_MS"].ToString();
                txtLote.Text = dtLancamento.Rows[linha]["LAC_INV_LOTE"].ToString();
                txtQtde.Text = dtLancamento.Rows[linha]["LAC_INV_QTDE"].ToString();
                txtData.Text = Funcoes.ChecaCampoVazio(dtLancamento.Rows[linha]["DT_ALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtLancamento.Rows[linha]["OP_ALTERACAO"].ToString());
                cmbUnidade.Text = dtLancamento.Rows[linha]["PROD_UNIDADE"].ToString().ToUpper();
                cmbClasseTera.Text = dtLancamento.Rows[linha]["PROD_CLASSE_TERAP"].ToString();
                if (dtLancamento.Rows[linha]["PROD_CONTROLADO"].ToString() == "S")
                {
                    chkControlado.Checked = true;
                }
                else
                {
                    chkControlado.Checked = false;
                }
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, linha));
                txtCodBarras.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtLancamento.Clear();
            dgLancamento.DataSource = dtLancamento;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            Array.Clear(dados, 0, 18);
            txtBCodBarra.Focus();
        }

        public void LimparFicha()
        {
            txtLanID.Text = "";
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtReg.Text = "";
            txtLote.Text = "";
            txtQtde.Text = "0";
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbUnidade.SelectedIndex = -1;
            cmbClasseTera.SelectedIndex = -1;
            Array.Clear(dados, 0, 18);
            txtCodBarras.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcLancamento.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgLancamento.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgLancamento.CurrentCell = dgLancamento.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgLancamento.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgLancamento.CurrentCell = dgLancamento.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtLancamento.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgLancamento.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgLancamento.CurrentCell = dgLancamento.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcLancamento.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtLancamento.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgLancamento.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgLancamento.CurrentCell = dgLancamento.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            return false;
        }

        public bool Excluir()
        {
            try
            {
                if (txtLanID.Text.Trim() != "" || txtCodBarras.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma a exclusão do medicamento do inventário?", "Exclusão tabela Lançamento Inventário", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var inventario = new SngpcLanInventario();
                        Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_LAC_INVENTARIO", "LAC_INV_ID", txtLanID.Text);
                        var conVendas = new SngpcControleVendas
                        {
                            ProCodigo = Principal.dtBusca.Rows[0]["LAC_INV_CODIGO"].ToString(),
                            Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["LAC_INV_QTDE"]),
                            Lote = Principal.dtBusca.Rows[0]["LAC_INV_LOTE"].ToString(),
                            Data = DateTime.Now
                        };

                    
                        BancoDados.AbrirTrans();
                        if (inventario.ExcluiDados(Convert.ToInt32(txtLanID.Text)).Equals(true))
                        {
                            if(conVendas.IdentificaSeJaTemLoteEProduto(Principal.dtBusca.Rows[0]["LAC_INV_CODIGO"].ToString(), Principal.dtBusca.Rows[0]["LAC_INV_LOTE"].ToString()).Equals(Convert.ToInt32(txtQtde.Text)))
                            {
                                if(!conVendas.ExcluirDados(Principal.dtBusca.Rows[0]["LAC_INV_CODIGO"].ToString(), Principal.dtBusca.Rows[0]["LAC_INV_LOTE"].ToString()))
                                {
                                    MessageBox.Show("Erro ao Excluir o Lançamento de Inventário. Contate o Suporte!", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    return false;
                                }
                            }
                            else
                            {
                                if(!conVendas.SubtraiQtdeMesmoLoteEProduto(conVendas))
                                {
                                    MessageBox.Show("Erro ao Excluir o Lançamento de Inventário. Contate o Suporte!", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    BancoDados.ErroTrans();
                                    return false;
                                }
                            }
                            
                            BancoDados.FecharTrans();
                            Funcoes.GravaLogExclusao("LAC_INV_ID", txtLanID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "SNGPC_LAC_INVENTARIO", txtLanID.Text, Principal.motivo, Principal.estAtual);
                            dtLancamento.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgLancamento.DataSource = dtLancamento;
                            emGrade = false;
                            tcLancamento.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            Principal.mdiPrincipal.btnExcluir.Enabled = false;
                            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                            txtBCodBarra.Focus();
                            MessageBox.Show("Lançamento Inventário excluido com sucesso!", "Lançamento Inventário");
                            return true;
                            
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                        }
                    }
                    return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }



        public bool CarregaCombos()
        {
            try
            {
                DataTable dtPesq = new DataTable();

                //CARREGA UNIDADES//
                dtPesq = Util.CarregarCombosPorEstabelecimento("UNI_CODIGO", "UNI_DESC_ABREV", "TIPO_UNIDADE", false, "UNI_LIBERADO = 'S'");
                if (dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Unidade,\npara cadastrar um produto.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUnidade.DataSource = dtPesq;
                    cmbUnidade.DisplayMember = "UNI_DESC_ABREV";
                    cmbUnidade.ValueMember = "UNI_CODIGO";

                    cmbUnidade.SelectedIndex = -1;
                    return true;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    Produto prod = new Produto();
                    SngpcLanInventario inventario = new SngpcLanInventario()
                    {
                        SngpcLacID = Funcoes.IdentificaVerificaID("SNGPC_LAC_INVENTARIO", "LAC_INV_ID", 0, ""),
                        SngpcLacInvCodigo = txtCodBarras.Text,
                        SngpcLacInvLote = txtLote.Text,
                        SngpcLacInvQtde = Convert.ToInt32(txtQtde.Text),
                        SngpcLacInvMS = txtReg.Text,
                    };

                    SngpcControleVendas controleVenda = new SngpcControleVendas()
                    {
                        ProCodigo = txtCodBarras.Text,
                        Lote = txtLote.Text,
                        Qtde = Convert.ToInt32(txtQtde.Text),
                        Data = DateTime.Now
                    };

                    if (!String.IsNullOrEmpty(inventario.BuscaLote(txtCodBarras.Text, txtLote.Text)))
                    {
                        MessageBox.Show("Erro ao efetuar o cadastro. Lote já cadastro ", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);
                    if (dtProd.Rows.Count == 1)
                    {
                        prod = new Produto()
                        {
                            ProdID = Convert.ToInt32(dtProd.Rows[0]["PROD_ID"]),
                            ProdClasTera = cmbClasseTera.Text,
                            ProdControlado = chkControlado.Checked == true ? "S" : "N",
                            ProdUnidade = cmbUnidade.Text,
                            ProdRegistroMS = txtReg.Text.Trim(),
                        };
                        BancoDados.AbrirTrans();

                        if (prod.AtualizaProdutoSNGPC(prod, dtProd).Equals(true))
                        {
                            if (inventario.Incluir(inventario).Equals(true))
                            {
                                if (!controleVenda.Incluir(controleVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("103 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }

                                BancoDados.FecharTrans();
                                MessageBox.Show("Cadastrado efetuada com sucesso!", "Produtos SNGPC", MessageBoxButtons.OK);
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                dtLancamento.Clear();
                                dgLancamento.DataSource = dtLancamento;
                                emGrade = false;
                                tslRegistros.Text = "";
                                Limpar();
                                return true;
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Erro ao cadastrar o produto,  Código Barras não encontrado ", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtReg.Text.Trim()))
            {
                Principal.mensagem = "Reg. Min. da Saúde não pode ser em branco.";
                Funcoes.Avisa();
                txtReg.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtLote.Text.Trim()))
            {
                Principal.mensagem = "Lote não pode ser em branco.";
                Funcoes.Avisa();
                txtLote.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || Convert.ToInt32(txtQtde.Text) == 0)
            {
                Principal.mensagem = "Qtde não pode ser zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(cmbUnidade.Text))
            {
                Principal.mensagem = "Selecione a Unidade ";
                Funcoes.Avisa();
                cmbUnidade.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(cmbClasseTera.Text))
            {
                Principal.mensagem = "Selecione a Classe Terapêutica";
                Funcoes.Avisa();
                cmbClasseTera.Focus();
                return false;
            }
            if (chkControlado.Checked == false)
            {
                Principal.mensagem = "SNGPC apenas medicamento controlados ";
                Funcoes.Avisa();
                chkControlado.Focus();
                return false;
            }
            if (cmbUnidade.Text == "CX" || cmbUnidade.Text == "FR")
            {
                return true;
            }
            else
            {

                Principal.mensagem = "Tipo de unidade permitida para SNGPC  é CX (Caixa) ou FR (Frasco)";
                Funcoes.Avisa();
                cmbUnidade.Focus();
                return false;
            }
        }

        private void tcLancamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcLancamento.SelectedTab == tpGrade)
            {
                dgLancamento.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcLancamento.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcLanInventario");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, contRegistros));
                    txtCodBarras.Focus();
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                }
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);

            if (e.KeyChar == 13)
            {
                if (Principal.mdiPrincipal.btnIncluir.Enabled == true && txtCodBarras.Text.Trim() != "")
                {
                    BuscaProdutos(txtCodBarras.Text, "");
                }
                else
                    txtDescr.Focus();
            }
        }

        private void txtReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtLote.Focus();
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtBCodBarra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCodBarra.Text.Trim()))
                {
                    txtBReg.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBReg.Text.Trim()))
                {
                    txtBLote.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {

                Cursor = Cursors.WaitCursor;

                SngpcLanInventario inventario = new SngpcLanInventario
                {
                    SngpcLacInvMS = txtBReg.Text,
                    SngpcLacInvCodigo = txtBCodBarra.Text,
                    SngpcLacInvLote = txtBLote.Text,

                };
                dtLancamento = inventario.BuscaInventario(inventario, out strOrdem);
                
                if (dtLancamento.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtLancamento.Rows.Count;
                    dgLancamento.DataSource = dtLancamento;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, 0));
                    dgLancamento.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgLancamento.DataSource = dtLancamento;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBCodBarra.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgLancamento_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, contRegistros));
            emGrade = true;
        }

        private void dgLancamento_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcLancamento.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgLancamento_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgLancamento.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtLancamento = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgLancamento.Columns[e.ColumnIndex].Name + " DESC");
                        dgLancamento.DataSource = dtLancamento;
                        decrescente = true;
                    }
                    else
                    {
                        dtLancamento = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgLancamento.Columns[e.ColumnIndex].Name + " ASC");
                        dgLancamento.DataSource = dtLancamento;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtLancamento, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgLancamento_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtLancamento.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtLancamento.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtLancamento.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgLancamento.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgLancamento);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgLancamento.RowCount;
                    for (i = 0; i <= dgLancamento.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgLancamento[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgLancamento.ColumnCount; k++)
                    {
                        if (dgLancamento.Columns[k].Visible == true)
                        {
                            switch (dgLancamento.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgLancamento.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgLancamento.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgLancamento.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgLancamento.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Cód. de Barras":
                                    numCel = Principal.GetColunaExcel(dgLancamento.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgLancamento.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Reg. Min. da Saúde":
                                    numCel = Principal.GetColunaExcel(dgLancamento.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgLancamento.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgLancamento.ColumnCount : indice) + Convert.ToString(dgLancamento.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Principal.mdiPrincipal.btnIncluir.Enabled == true && txtCodBarras.Text.Trim() == "")
                {
                    BuscaProdutos("", txtDescr.Text);

                }
                txtLote.Focus();
            }
        }


        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtLanID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        public bool BuscaProdutos(string codigoBarras, string descricao)

        {
            Produto pro = new Produto();

            if (descricao != "")
            {
                using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                {
                    buscaProd.BuscaProd(txtDescr.Text.ToUpper(), false);
                    buscaProd.ShowDialog();
                }
            }

            string codBarras = codigoBarras != "" ? codigoBarras : Principal.codBarra;

            DataTable dtProduto = pro.BuscaProdutos(codBarras);

            if (dtProduto.Rows.Count != 0)
            {
                txtCodBarras.Text = dtProduto.Rows[0]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                txtReg.Text = dtProduto.Rows[0]["PROD_REGISTRO_MS"].ToString();
                cmbUnidade.Text = dtProduto.Rows[0]["PROD_UNIDADE"].ToString();
                cmbClasseTera.Text = dtProduto.Rows[0]["PROD_CLASSE_TERAP"].ToString();
                if (dtProduto.Rows[0]["PROD_CONTROLADO"].ToString() == "S")
                {
                    chkControlado.Checked = true;
                }
                else
                {
                    chkControlado.Checked = false;
                }
                txtLote.Focus();
                return true;
            }
            else
            {
                LimparFicha();
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
                return false;
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgLancamento.ColumnCount; i++)
                {
                    if (dgLancamento.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgLancamento.ColumnCount];
                string[] coluna = new string[dgLancamento.ColumnCount];

                for (int i = 0; i < dgLancamento.ColumnCount; i++)
                {
                    grid[i] = dgLancamento.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgLancamento.ColumnCount; i++)
                {
                    if (dgLancamento.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgLancamento.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgLancamento.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgLancamento.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgLancamento.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgLancamento.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgLancamento.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgLancamento.ColumnCount; i++)
                        {
                            dgLancamento.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lanc. Inventário", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcLancamento.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcLancamento.SelectedTab = tpFicha;
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if(e.KeyChar == 13)
            {
                cmbUnidade.Focus();
            }
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void frmSngpcLanInventario_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void cmbUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbClasseTera.Focus();
        }

        private void cmbClasseTera_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkControlado.Focus();
        }

        private void btnLimparInven_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Limpar Inventário para um novo Envio?\nLembrando que essa exclusão não significa que encerrou o mesmo na ANVISA", "Lanc. Inventário", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                BancoDados.AbrirTrans();

                var inventario = new SngpcLanInventario();
                if (!inventario.ExcluirTodosRegistros())
                {
                    BancoDados.ErroTrans();
                }
                else
                {
                    var conVendas = new SngpcControleVendas();
                    if (!conVendas.ExcluirTodosRegistros())
                        BancoDados.ErroTrans();
                    else
                        BancoDados.FecharTrans();

                    txtBCodBarra.Focus();
                }
            }
            else
                txtBCodBarra.Focus();
        }
    }
}
